﻿// litao@Copy Right 2006-2016
// 文件： Pacs_检查申请Model.cs
// 项目名称：HIS.InterFacePacs
// 创建时间：2018-04-21
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.InterFacePacs.Model
{
	/// <summary>
	///Pacs_检查申请数据实体
	/// </summary>
	[DataContract]
	public partial class Pacs_检查申请Model:INotifyPropertyChanged
	{
		#region 变量定义
		///<summary>
		///
		///</summary>
		private int _id = -1;
		///<summary>
		///
		///</summary>
		private string _病人id = String.Empty;
		///<summary>
		///
		///</summary>
		private string _申请单号 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _病人来源 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _病人姓名 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _性别 = String.Empty;
		///<summary>
		///
		///</summary>
		private int _病人年龄;
		///<summary>
		///
		///</summary>
		private string _检查部位名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private DateTime _申请日期 = DateTime.Now;
		///<summary>
		///
		///</summary>
		private string _申请科室 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _申请医生 = String.Empty;
		///<summary>
		///
		///</summary>
		private decimal _检查费;
		///<summary>
		///
		///</summary>
		private string _住院号 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _门诊号 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _床号 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _电话号码 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _类型 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _临床诊断名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _体征描述 = String.Empty;
		///<summary>
		///1执行0未执行
		///</summary>
		private int _状态;
		///<summary>
		///
		///</summary>
		private string _pacs检查id = String.Empty;
		///<summary>
		///
		///</summary>
		private DateTime _检查时间 = DateTime.Now;
		///<summary>
		///
		///</summary>
		private string _检查医生 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _检查所见 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _检查提示 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _检查结果 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _执行科室名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private int _医嘱id;
		///<summary>
		///
		///</summary>
		private string _检查目的 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _身份证号 = String.Empty;
		///<summary>
		///
		///</summary>
		private int _分院编码;
		///<summary>
		///
		///</summary>
		private DateTime _末次月经 = DateTime.Now;
		///<summary>
		///
		///</summary>
		private string _是否怀孕 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _孕周 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _孕天 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _家庭地址 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _检查部位编码 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _单位编码 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _收费编码 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _收费名称 = String.Empty;
		#endregion
		
		#region 构造函数

		///<summary>
		///
		///</summary>
		public Pacs_检查申请Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=病人id></param>
		///<param name=申请单号></param>
		///<param name=病人来源></param>
		///<param name=病人姓名></param>
		///<param name=性别></param>
		///<param name=病人年龄></param>
		///<param name=检查部位名称></param>
		///<param name=申请日期></param>
		///<param name=申请科室></param>
		///<param name=申请医生></param>
		///<param name=检查费></param>
		///<param name=住院号></param>
		///<param name=门诊号></param>
		///<param name=床号></param>
		///<param name=电话号码></param>
		///<param name=类型></param>
		///<param name=临床诊断名称></param>
		///<param name=体征描述></param>
		///<param name=状态>1执行0未执行</param>
		///<param name=pacs检查id></param>
		///<param name=检查时间></param>
		///<param name=检查医生></param>
		///<param name=检查所见></param>
		///<param name=检查提示></param>
		///<param name=检查结果></param>
		///<param name=执行科室名称></param>
		///<param name=医嘱id></param>
		///<param name=检查目的></param>
		///<param name=身份证号></param>
		///<param name=分院编码></param>
		///<param name=末次月经></param>
		///<param name=是否怀孕></param>
		///<param name=孕周></param>
		///<param name=孕天></param>
		///<param name=家庭地址></param>
		///<param name=检查部位编码></param>
		///<param name=单位编码></param>
		///<param name=收费编码></param>
		///<param name=收费名称></param>
		public Pacs_检查申请Model(int id,string 病人id,string 申请单号,string 病人来源,string 病人姓名,string 性别,int 病人年龄,string 检查部位名称,DateTime 申请日期,string 申请科室,string 申请医生,decimal 检查费,string 住院号,string 门诊号,string 床号,string 电话号码,string 类型,string 临床诊断名称,string 体征描述,int 状态,string pacs检查id,DateTime 检查时间,string 检查医生,string 检查所见,string 检查提示,string 检查结果,string 执行科室名称,int 医嘱id,string 检查目的,string 身份证号,int 分院编码,DateTime 末次月经,string 是否怀孕,string 孕周,string 孕天,string 家庭地址,string 检查部位编码,string 单位编码,string 收费编码,string 收费名称)
		{
			this._id = id;
			this._病人id = 病人id;
			this._申请单号 = 申请单号;
			this._病人来源 = 病人来源;
			this._病人姓名 = 病人姓名;
			this._性别 = 性别;
			this._病人年龄 = 病人年龄;
			this._检查部位名称 = 检查部位名称;
			this._申请日期 = 申请日期;
			this._申请科室 = 申请科室;
			this._申请医生 = 申请医生;
			this._检查费 = 检查费;
			this._住院号 = 住院号;
			this._门诊号 = 门诊号;
			this._床号 = 床号;
			this._电话号码 = 电话号码;
			this._类型 = 类型;
			this._临床诊断名称 = 临床诊断名称;
			this._体征描述 = 体征描述;
			this._状态 = 状态;
			this._pacs检查id = pacs检查id;
			this._检查时间 = 检查时间;
			this._检查医生 = 检查医生;
			this._检查所见 = 检查所见;
			this._检查提示 = 检查提示;
			this._检查结果 = 检查结果;
			this._执行科室名称 = 执行科室名称;
			this._医嘱id = 医嘱id;
			this._检查目的 = 检查目的;
			this._身份证号 = 身份证号;
			this._分院编码 = 分院编码;
			this._末次月经 = 末次月经;
			this._是否怀孕 = 是否怀孕;
			this._孕周 = 孕周;
			this._孕天 = 孕天;
			this._家庭地址 = 家庭地址;
			this._检查部位编码 = 检查部位编码;
			this._单位编码 = 单位编码;
			this._收费编码 = 收费编码;
			this._收费名称 = 收费名称;
		}
        
		#endregion
		
		#region 公共属性

		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "病人ID", IsRequired = false, Order = 1)]
		public string 病人ID
		{
			get {return _病人id;}
			set {
            _病人id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("病人ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "申请单号", IsRequired = false, Order = 2)]
		public string 申请单号
		{
			get {return _申请单号;}
			set {
            _申请单号 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("申请单号"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "病人来源", IsRequired = false, Order = 3)]
		public string 病人来源
		{
			get {return _病人来源;}
			set {
            _病人来源 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("病人来源"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "病人姓名", IsRequired = false, Order = 4)]
		public string 病人姓名
		{
			get {return _病人姓名;}
			set {
            _病人姓名 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("病人姓名"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "性别", IsRequired = false, Order = 5)]
		public string 性别
		{
			get {return _性别;}
			set {
            _性别 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("性别"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "病人年龄", IsRequired = false, Order = 6)]
		public int 病人年龄
		{
			get {return _病人年龄;}
			set {
            _病人年龄 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("病人年龄"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查部位名称", IsRequired = false, Order = 7)]
		public string 检查部位名称
		{
			get {return _检查部位名称;}
			set {
            _检查部位名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查部位名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "申请日期", IsRequired = false, Order = 8)]
		public DateTime 申请日期
		{
			get {return _申请日期;}
			set {
            _申请日期 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("申请日期"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "申请科室", IsRequired = false, Order = 9)]
		public string 申请科室
		{
			get {return _申请科室;}
			set {
            _申请科室 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("申请科室"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "申请医生", IsRequired = false, Order = 10)]
		public string 申请医生
		{
			get {return _申请医生;}
			set {
            _申请医生 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("申请医生"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查费", IsRequired = false, Order = 11)]
		public decimal 检查费
		{
			get {return _检查费;}
			set {
            _检查费 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查费"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "住院号", IsRequired = false, Order = 12)]
		public string 住院号
		{
			get {return _住院号;}
			set {
            _住院号 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("住院号"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "门诊号", IsRequired = false, Order = 13)]
		public string 门诊号
		{
			get {return _门诊号;}
			set {
            _门诊号 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("门诊号"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "床号", IsRequired = false, Order = 14)]
		public string 床号
		{
			get {return _床号;}
			set {
            _床号 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("床号"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "电话号码", IsRequired = false, Order = 15)]
		public string 电话号码
		{
			get {return _电话号码;}
			set {
            _电话号码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("电话号码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "类型", IsRequired = false, Order = 16)]
		public string 类型
		{
			get {return _类型;}
			set {
            _类型 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("类型"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "临床诊断名称", IsRequired = false, Order = 17)]
		public string 临床诊断名称
		{
			get {return _临床诊断名称;}
			set {
            _临床诊断名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("临床诊断名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "体征描述", IsRequired = false, Order = 18)]
		public string 体征描述
		{
			get {return _体征描述;}
			set {
            _体征描述 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("体征描述"));
            }
		}

		///<summary>
		///1执行0未执行
		///</summary>
		[DataMember(Name = "状态", IsRequired = false, Order = 19)]
		public int 状态
		{
			get {return _状态;}
			set {
            _状态 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("状态"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "Pacs检查ID", IsRequired = false, Order = 20)]
		public string Pacs检查ID
		{
			get {return _pacs检查id;}
			set {
            _pacs检查id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("Pacs检查ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查时间", IsRequired = false, Order = 21)]
		public DateTime 检查时间
		{
			get {return _检查时间;}
			set {
            _检查时间 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查时间"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查医生", IsRequired = false, Order = 22)]
		public string 检查医生
		{
			get {return _检查医生;}
			set {
            _检查医生 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查医生"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查所见", IsRequired = false, Order = 23)]
		public string 检查所见
		{
			get {return _检查所见;}
			set {
            _检查所见 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查所见"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查提示", IsRequired = false, Order = 24)]
		public string 检查提示
		{
			get {return _检查提示;}
			set {
            _检查提示 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查提示"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查结果", IsRequired = false, Order = 25)]
		public string 检查结果
		{
			get {return _检查结果;}
			set {
            _检查结果 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查结果"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "执行科室名称", IsRequired = false, Order = 26)]
		public string 执行科室名称
		{
			get {return _执行科室名称;}
			set {
            _执行科室名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("执行科室名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "医嘱ID", IsRequired = false, Order = 27)]
		public int 医嘱ID
		{
			get {return _医嘱id;}
			set {
            _医嘱id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("医嘱ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查目的", IsRequired = false, Order = 28)]
		public string 检查目的
		{
			get {return _检查目的;}
			set {
            _检查目的 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查目的"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "身份证号", IsRequired = false, Order = 29)]
		public string 身份证号
		{
			get {return _身份证号;}
			set {
            _身份证号 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("身份证号"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "分院编码", IsRequired = false, Order = 30)]
		public int 分院编码
		{
			get {return _分院编码;}
			set {
            _分院编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("分院编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "末次月经", IsRequired = false, Order = 31)]
		public DateTime 末次月经
		{
			get {return _末次月经;}
			set {
            _末次月经 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("末次月经"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否怀孕", IsRequired = false, Order = 32)]
		public string 是否怀孕
		{
			get {return _是否怀孕;}
			set {
            _是否怀孕 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否怀孕"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "孕周", IsRequired = false, Order = 33)]
		public string 孕周
		{
			get {return _孕周;}
			set {
            _孕周 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("孕周"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "孕天", IsRequired = false, Order = 34)]
		public string 孕天
		{
			get {return _孕天;}
			set {
            _孕天 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("孕天"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "家庭地址", IsRequired = false, Order = 35)]
		public string 家庭地址
		{
			get {return _家庭地址;}
			set {
            _家庭地址 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("家庭地址"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "检查部位编码", IsRequired = false, Order = 36)]
		public string 检查部位编码
		{
			get {return _检查部位编码;}
			set {
            _检查部位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("检查部位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单位编码", IsRequired = false, Order = 37)]
		public string 单位编码
		{
			get {return _单位编码;}
			set {
            _单位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "收费编码", IsRequired = false, Order = 38)]
		public string 收费编码
		{
			get {return _收费编码;}
			set {
            _收费编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("收费编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "收费名称", IsRequired = false, Order = 39)]
		public string 收费名称
		{
			get {return _收费名称;}
			set {
            _收费名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("收费名称"));
            }
		}
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
		#endregion
		
	}
}