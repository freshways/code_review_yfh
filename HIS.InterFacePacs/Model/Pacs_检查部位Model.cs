﻿// litao@Copy Right 2006-2016
// 文件： Pacs_检查部位Model.cs
// 项目名称：HIS.InterFacePacs
// 创建时间：2018-04-20
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.InterFacePacs.Model
{
	/// <summary>
	///Pacs_检查部位数据实体
	/// </summary>
	[DataContract]
	public partial class Pacs_检查部位Model:INotifyPropertyChanged
	{
		#region 变量定义
		///<summary>
		///
		///</summary>
		private int _id;
		///<summary>
		///
		///</summary>
		private long _部位编码;
		///<summary>
		///
		///</summary>
		private string _部位名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _部位索引 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _部位分类 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _是否停用 = String.Empty;
		#endregion
		
		#region 构造函数

		///<summary>
		///
		///</summary>
		public Pacs_检查部位Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=部位编码></param>
		///<param name=部位名称></param>
		///<param name=部位索引></param>
		///<param name=部位分类></param>
		///<param name=是否停用></param>
		public Pacs_检查部位Model(int id,long 部位编码,string 部位名称,string 部位索引,string 部位分类,string 是否停用)
		{
			this._id = id;
			this._部位编码 = 部位编码;
			this._部位名称 = 部位名称;
			this._部位索引 = 部位索引;
			this._部位分类 = 部位分类;
			this._是否停用 = 是否停用;
		}
        
		#endregion
		
		#region 公共属性

		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位编码", IsRequired = false, Order = 1)]
		public long 部位编码
		{
			get {return _部位编码;}
			set {
            _部位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位名称", IsRequired = false, Order = 2)]
		public string 部位名称
		{
			get {return _部位名称;}
			set {
            _部位名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位索引", IsRequired = false, Order = 3)]
		public string 部位索引
		{
			get {return _部位索引;}
			set {
            _部位索引 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位索引"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "部位分类", IsRequired = false, Order = 4)]
		public string 部位分类
		{
			get {return _部位分类;}
			set {
            _部位分类 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("部位分类"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否停用", IsRequired = false, Order = 5)]
		public string 是否停用
		{
			get {return _是否停用;}
			set {
            _是否停用 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否停用"));
            }
		}
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
		#endregion
		
	}
}
