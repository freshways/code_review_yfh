﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs申请录入 : DevExpress.XtraEditors.XtraForm
    {
        HIS.COMM.Person住院病人 zyperson;
        HIS.COMM.Person门诊病人 mzperson;
        public XtraFormPacs申请录入()
        {
            InitializeComponent();
        }
        String s申请单 = "";

        /// <summary>
        /// 门诊申请单
        /// </summary>
        /// <param name="dtPACS对照"></param>
        /// <param name="s申请单号"></param>
        /// <param name="_s疾病名称"></param>
        /// <param name="_Show末次"></param>
        public XtraFormPacs申请录入(DataTable dtPACS对照, string s申请单号, string _s疾病名称, bool _Show末次, HIS.COMM.Person门诊病人 _person)
        {
            InitializeComponent();

            mzperson = _person;
            s申请单 = s申请单号;
            textEdit身份证号.Text = mzperson.S身份证号;
            if (dtPACS对照 != null && dtPACS对照.Rows.Count > 0)
            {
                labelControl编码.Text = dtPACS对照.Rows[0]["收费编码"].ToString();
                labelControl名称.Text = dtPACS对照.Rows[0]["收费名称"].ToString();
            }
            textEdit诊断.Text = _s疾病名称;
            if (_Show末次)
            {
                layoutControlItem孕检.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutControlItem末次.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                layoutControlItem住址.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
        }
        /// <summary>
        /// 住院
        /// </summary>
        /// <param name="dtPACS对照"></param>
        /// <param name="s申请单号"></param>
        /// <param name="_person"></param>
        public XtraFormPacs申请录入(DataTable dtPACS对照, string s申请单号, HIS.COMM.Person住院病人 _person)
        {
            InitializeComponent();
            zyperson = _person;

            s申请单 = s申请单号;
            if (dtPACS对照 != null && dtPACS对照.Rows.Count > 0)
            {
                labelControl编码.Text = dtPACS对照.Rows[0]["收费编码"].ToString();
                labelControl名称.Text = dtPACS对照.Rows[0]["收费名称"].ToString();
            }
            textEdit诊断.Text = zyperson.s疾病名称;
        }
        
        /// <summary>
        /// Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XtraFormPacs申请录入_Load(object sender, EventArgs e)
        {


            string SQL执行科室 =
    "select distinct aa.执行科室编码, bb.科室名称 执行科室名称" + "\r\n" +
    "from   PACS执行科室 aa left outer join GY科室设置 bb on aa.执行科室编码 = bb.科室编码" + "\r\n" +
    "where aa.分院编码=" + WEISHENG.COMM.zdInfo.Model站点信息.分院编码 + " and  收费编码 = '" + labelControl编码.Text +"' ";
            DataTable dt执行科室 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL执行科室).Tables[0];
            if (dt执行科室 != null && dt执行科室.Rows.Count > 0)
            {
                searchLookUpEdit执行科室.Properties.DataSource = dt执行科室;
                searchLookUpEdit执行科室.Properties.DisplayMember = "执行科室名称";
                searchLookUpEdit执行科室.Properties.ValueMember = "执行科室编码";
                if (dt执行科室 != null && dt执行科室.Rows.Count > 0)//如果取到的科室不为空，默认第一个
                    searchLookUpEdit执行科室.EditValue = dt执行科室.Rows[0]["执行科室编码"];
                DataTable dt检查部位 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
                    "select 部位编码,部位名称,部位索引,部位分类 from Pacs_检查部位 where 是否停用='否' ").Tables[0];
                textEdit检查部位.Properties.DataSource = dt检查部位;
                textEdit检查部位.Properties.PopulateViewColumns();
                textEdit检查部位.Properties.DisplayMember = "部位名称";
                textEdit检查部位.Properties.ValueMember = "部位编码";
                textEdit检查部位.Properties.View.Columns["部位名称"].Width = 150;
                textEdit检查部位.Properties.View.Columns["部位分类"].Visible = true;
                textEdit检查部位.Properties.View.Columns["部位编码"].Visible = true;
                textEdit检查部位.Properties.View.Columns["部位索引"].Visible = true;
                textEdit检查部位.Properties.PopupFormSize = new System.Drawing.Size(700, 300);
            }

        }
        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                if (textEdit检查部位.Text == "")
                {
                    MessageBox.Show("请选择检查部位", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (searchLookUpEdit执行科室.Text == "")
                {
                    MessageBox.Show("请选择执行科室", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string update = " update Pacs_Inteface set 检查部位='" + textEdit检查部位.Text + "',检查部位编码='" + textEdit检查部位.EditValue + "',体征描述='" +
                textEdit主诉.Text + "',查体 = '" + textEdit查体目的.Text + "',临床诊断='" + textEdit诊断.Text + "'" +
                ",执行科室='" + searchLookUpEdit执行科室.Text + "' ";
                if (checkEdit孕检.Checked)
                {
                    if (dateEdit末次月经.Text.Trim() == "" || textEdit家庭地址.Text.Trim() == "" || textEdit身份证号.Text.Trim() == "")
                    {
                        MessageBox.Show("请填写末次月经", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    update += ",末次月经='" + dateEdit末次月经.Text + "',家庭地址='" + textEdit家庭地址.Text + "',身份证号='" + this.textEdit身份证号.Text + "' ";
                }
                update += "where 申请单号='" + s申请单 + "' ";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, update);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.Close();
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton诊断_Click(object sender, EventArgs e)
        {
            HIS.COMM.XtraFormICD10 icdForm = new HIS.COMM.XtraFormICD10();
            if (icdForm.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            textEdit诊断.Text = icdForm.selected_icd_info.ICD10Name;
            textEdit诊断.Tag = icdForm.selected_icd_info.ICD10;
        }

        private void checkEdit孕检_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit孕检.Checked)
            {
                dateEdit末次月经.Enabled = true;
                textEdit家庭地址.Enabled = true;
            }
            else
            {
                dateEdit末次月经.Enabled = false;
                textEdit家庭地址.Enabled = false;
            }
        }

        private void simpleButton主诉_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from Pacs_字典 where 类别='主诉' and 是否禁用='否' ";
            XtraFormPacs_选择病人就诊资料 frm = new XtraFormPacs_选择病人就诊资料(sql, "名称", "病人主诉/体征描述");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit主诉.Text = frm.s选择项目;
        }

        private void simpleButton查体_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from Pacs_字典 where 类别='查体' and 是否禁用='否'  ";
            XtraFormPacs_选择病人就诊资料 frm = new XtraFormPacs_选择病人就诊资料(sql, "名称", "病人检查目的/查体");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit查体目的.Text = frm.s选择项目;
        }

        private void textEdit诊断_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            }
        }
    }
}
