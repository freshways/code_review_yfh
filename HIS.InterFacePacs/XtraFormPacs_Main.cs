﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs_Main : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormPacs_Main()
        {
            InitializeComponent();
        }

        private void simpleButton字典维护_Click(object sender, EventArgs e)
        {
            XtarFormPacs_字典维护 frm = new XtarFormPacs_字典维护();
            frm.ShowDialog();
        }

        private void simpleButton部位维护_Click(object sender, EventArgs e)
        {
            XtarFormPacs_部位维护 frm = new XtarFormPacs_部位维护();
            frm.ShowDialog();
        }

        private void simpleButton接口维护_Click(object sender, EventArgs e)
        {
            //System.Diagnostics.Process ps = new System.Diagnostics.Process();
            //ps.StartInfo.FileName = "D:\\GoogleChromePortable\\App\\Google Chrome\\chrome.exe";
            //ps.StartInfo.Arguments = "http://www.baidu.com";
            //ps.Start();
            XtraFormPacs申请录入 frm = new XtraFormPacs申请录入();
            frm.ShowDialog();
        }

        private void simpleButton影像结果_Click(object sender, EventArgs e)
        {
            XtraFormPacs影像结果 frm = new XtraFormPacs影像结果();
            frm.ShowDialog();
        }

        private void simpleButton项目对照_Click(object sender, EventArgs e)
        {
            XtraFormPacs_申请项目维护 frm = new XtraFormPacs_申请项目维护();
            frm.ShowDialog();
        }

        private void simpleButton身体部位维护_Click(object sender, EventArgs e)
        {
            XtarFromPacs_身体部位维护 frm = new XtarFromPacs_身体部位维护();
            frm.ShowDialog();
        }

        private void simpleButton申请单打印_Click(object sender, EventArgs e)
        {
            XtraFormPacs_申请单打印 frm = new XtraFormPacs_申请单打印();
            frm.ShowDialog();
        }
    }
}
