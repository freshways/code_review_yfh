﻿namespace HIS.InterFacePacs
{
    partial class XtraFormPacs_申请项目维护添加
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormPacs_申请项目维护添加));
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.searchLookUpEdit检查部位 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt胶片 = new DevExpress.XtraEditors.TextEdit();
            this.txt数量 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit医嘱名称 = new DevExpress.XtraEditors.TextEdit();
            this.searchLookUpEdit项目对照 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView医嘱名称 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn收费编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn收费名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn拼音代码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit设备 = new DevExpress.XtraEditors.TextEdit();
            this.searchLookUpEdit执行科室 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton确定 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup检查申请 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem住址 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit检查部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胶片.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit医嘱名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit项目对照.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView医嘱名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit设备.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit执行科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup检查申请)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem住址)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton取消;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(58, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(68, 29);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton取消.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton取消.Image")));
            this.simpleButton取消.Location = new System.Drawing.Point(63, 5);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(64, 22);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 5;
            this.simpleButton取消.Text = "取消";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.searchLookUpEdit检查部位);
            this.layoutControl1.Controls.Add(this.txt胶片);
            this.layoutControl1.Controls.Add(this.txt数量);
            this.layoutControl1.Controls.Add(this.textEdit医嘱名称);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit项目对照);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit设备);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit执行科室);
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.simpleButton确定);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(678, 203, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(491, 460);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // searchLookUpEdit检查部位
            // 
            this.searchLookUpEdit检查部位.EditValue = "请选择...";
            this.searchLookUpEdit检查部位.Location = new System.Drawing.Point(108, 186);
            this.searchLookUpEdit检查部位.Name = "searchLookUpEdit检查部位";
            this.searchLookUpEdit检查部位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit检查部位.Properties.DisplayMember = "部位名称";
            this.searchLookUpEdit检查部位.Properties.NullText = "请选择部位...";
            this.searchLookUpEdit检查部位.Properties.ValueMember = "部位编码";
            this.searchLookUpEdit检查部位.Properties.View = this.gridView2;
            this.searchLookUpEdit检查部位.Size = new System.Drawing.Size(370, 20);
            this.searchLookUpEdit检查部位.StyleController = this.layoutControl1;
            this.searchLookUpEdit检查部位.TabIndex = 7;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "设备类型";
            this.gridColumn4.FieldName = "设备类型";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "部位编码";
            this.gridColumn1.FieldName = "部位编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "部位名称";
            this.gridColumn2.FieldName = "部位名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "简称";
            this.gridColumn3.FieldName = "部位名称_ENG";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // txt胶片
            // 
            this.txt胶片.Location = new System.Drawing.Point(108, 162);
            this.txt胶片.Name = "txt胶片";
            this.txt胶片.Size = new System.Drawing.Size(370, 20);
            this.txt胶片.StyleController = this.layoutControl1;
            this.txt胶片.TabIndex = 21;
            // 
            // txt数量
            // 
            this.txt数量.Location = new System.Drawing.Point(108, 114);
            this.txt数量.Name = "txt数量";
            this.txt数量.Size = new System.Drawing.Size(370, 20);
            this.txt数量.StyleController = this.layoutControl1;
            this.txt数量.TabIndex = 2;
            // 
            // textEdit医嘱名称
            // 
            this.textEdit医嘱名称.Location = new System.Drawing.Point(108, 90);
            this.textEdit医嘱名称.Name = "textEdit医嘱名称";
            this.textEdit医嘱名称.Size = new System.Drawing.Size(370, 20);
            this.textEdit医嘱名称.StyleController = this.layoutControl1;
            this.textEdit医嘱名称.TabIndex = 19;
            this.textEdit医嘱名称.ToolTip = "生成医嘱项目、打印显示的文字，可与收费项目不一样";
            this.textEdit医嘱名称.ToolTipTitle = "提示";
            // 
            // searchLookUpEdit项目对照
            // 
            this.searchLookUpEdit项目对照.EditValue = "请选择...";
            this.searchLookUpEdit项目对照.Location = new System.Drawing.Point(108, 66);
            this.searchLookUpEdit项目对照.Name = "searchLookUpEdit项目对照";
            this.searchLookUpEdit项目对照.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit项目对照.Properties.DisplayMember = "部位名称";
            this.searchLookUpEdit项目对照.Properties.NullText = "请选择收费项目...";
            this.searchLookUpEdit项目对照.Properties.ValueMember = "部位编码";
            this.searchLookUpEdit项目对照.Properties.View = this.gridView医嘱名称;
            this.searchLookUpEdit项目对照.Size = new System.Drawing.Size(370, 20);
            this.searchLookUpEdit项目对照.StyleController = this.layoutControl1;
            this.searchLookUpEdit项目对照.TabIndex = 20;
            // 
            // gridView医嘱名称
            // 
            this.gridView医嘱名称.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn收费编码,
            this.gridColumn收费名称,
            this.gridColumn拼音代码});
            this.gridView医嘱名称.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView医嘱名称.Name = "gridView医嘱名称";
            this.gridView医嘱名称.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView医嘱名称.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn收费编码
            // 
            this.gridColumn收费编码.Caption = "收费编码";
            this.gridColumn收费编码.FieldName = "收费编码";
            this.gridColumn收费编码.Name = "gridColumn收费编码";
            this.gridColumn收费编码.Visible = true;
            this.gridColumn收费编码.VisibleIndex = 0;
            // 
            // gridColumn收费名称
            // 
            this.gridColumn收费名称.Caption = "收费名称";
            this.gridColumn收费名称.FieldName = "收费名称";
            this.gridColumn收费名称.Name = "gridColumn收费名称";
            this.gridColumn收费名称.Visible = true;
            this.gridColumn收费名称.VisibleIndex = 1;
            // 
            // gridColumn拼音代码
            // 
            this.gridColumn拼音代码.Caption = "拼音代码";
            this.gridColumn拼音代码.FieldName = "拼音代码";
            this.gridColumn拼音代码.Name = "gridColumn拼音代码";
            this.gridColumn拼音代码.Visible = true;
            this.gridColumn拼音代码.VisibleIndex = 2;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(108, 138);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Size = new System.Drawing.Size(370, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 19;
            // 
            // textEdit设备
            // 
            this.textEdit设备.Location = new System.Drawing.Point(108, 234);
            this.textEdit设备.Name = "textEdit设备";
            this.textEdit设备.Size = new System.Drawing.Size(370, 20);
            this.textEdit设备.StyleController = this.layoutControl1;
            this.textEdit设备.TabIndex = 18;
            // 
            // searchLookUpEdit执行科室
            // 
            this.searchLookUpEdit执行科室.EditValue = "请选择执行科室...";
            this.searchLookUpEdit执行科室.Location = new System.Drawing.Point(108, 210);
            this.searchLookUpEdit执行科室.Name = "searchLookUpEdit执行科室";
            this.searchLookUpEdit执行科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit执行科室.Properties.NullText = "请选择执行科室...";
            this.searchLookUpEdit执行科室.Properties.View = this.gridView1;
            this.searchLookUpEdit执行科室.Size = new System.Drawing.Size(370, 20);
            this.searchLookUpEdit执行科室.StyleController = this.layoutControl1;
            this.searchLookUpEdit执行科室.TabIndex = 13;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButton确定
            // 
            this.simpleButton确定.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton确定.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton确定.Image")));
            this.simpleButton确定.Location = new System.Drawing.Point(5, 5);
            this.simpleButton确定.Name = "simpleButton确定";
            this.simpleButton确定.Size = new System.Drawing.Size(54, 22);
            this.simpleButton确定.StyleController = this.layoutControl1;
            this.simpleButton确定.TabIndex = 4;
            this.simpleButton确定.Text = "保存";
            this.simpleButton确定.Click += new System.EventHandler(this.simpleButton确定_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup检查申请,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(491, 460);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup检查申请
            // 
            this.layoutControlGroup检查申请.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 11F);
            this.layoutControlGroup检查申请.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup检查申请.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup检查申请.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup检查申请.CustomizationFormText = "检查申请单";
            this.layoutControlGroup检查申请.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlItem住址,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem13,
            this.layoutControlItem4});
            this.layoutControlGroup检查申请.Location = new System.Drawing.Point(0, 29);
            this.layoutControlGroup检查申请.Name = "layoutControlGroup检查申请";
            this.layoutControlGroup检查申请.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup检查申请.Size = new System.Drawing.Size(485, 425);
            this.layoutControlGroup检查申请.Text = "检查项目维护";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.searchLookUpEdit执行科室;
            this.layoutControlItem10.CustomizationFormText = "执行科室：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem10.Text = "执行科室：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem住址
            // 
            this.layoutControlItem住址.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem住址.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem住址.Control = this.textEdit设备;
            this.layoutControlItem住址.CustomizationFormText = "家庭住址：";
            this.layoutControlItem住址.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem住址.Name = "layoutControlItem住址";
            this.layoutControlItem住址.Size = new System.Drawing.Size(469, 217);
            this.layoutControlItem住址.Text = "设备编码：";
            this.layoutControlItem住址.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem住址.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem住址.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.searchLookUpEdit项目对照;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem14.Text = "收费项目:";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt胶片;
            this.layoutControlItem15.CustomizationFormText = "胶片";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem15.Text = "胶片数量:";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt数量;
            this.layoutControlItem5.CustomizationFormText = "数量";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem5.Text = "收费数量:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit医嘱名称;
            this.layoutControlItem6.CustomizationFormText = "医嘱名称:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem6.Text = "医嘱名称:";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.textEdit身份证号;
            this.layoutControlItem13.CustomizationFormText = "身份证号：";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem13.Text = "胶片收费代码:";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.searchLookUpEdit检查部位;
            this.layoutControlItem4.CustomizationFormText = "检查部位:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem4.Text = "检查部位:";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton确定;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(58, 29);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(126, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 29);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 29);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(359, 29);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // XtraFormPacs_申请项目维护添加
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 460);
            this.Controls.Add(this.layoutControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "XtraFormPacs_申请项目维护添加";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pacs_申请项目维护添加";
            this.Load += new System.EventHandler(this.XtraFormPacs_申请项目维护添加_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit检查部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胶片.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit医嘱名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit项目对照.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView医嘱名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit设备.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit执行科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup检查申请)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem住址)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt胶片;
        private DevExpress.XtraEditors.TextEdit txt数量;
        private DevExpress.XtraEditors.TextEdit textEdit医嘱名称;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit项目对照;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView医嘱名称;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn收费编码;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn收费名称;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn拼音代码;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit设备;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit执行科室;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton simpleButton确定;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup检查申请;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem住址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit检查部位;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;

    }
}