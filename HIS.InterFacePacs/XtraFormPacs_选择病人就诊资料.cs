﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs_选择病人就诊资料 : DevExpress.XtraEditors.XtraForm
    {
        public string s选择项目 = "";
        string StrSQL = "";
        string StrColunm = "";
        DataTable dt;
        public XtraFormPacs_选择病人就诊资料()
        {
            InitializeComponent();
        }

        public XtraFormPacs_选择病人就诊资料(string sql, string colunm, string title)
        {
            InitializeComponent();
            this.Text = title;
            StrSQL = sql;
            StrColunm = colunm;
        }

        private void XtraFormPacs_选择病人就诊资料_Load(object sender, EventArgs e)
        {
            BindList();
        }

        private void BindList()
        {
            try
            {
                if (StrSQL == "") return;
                dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, StrSQL).Tables[0];
                dt.Columns.Add("多选", System.Type.GetType("System.Boolean"));

                gridControl1.DataSource = dt;
                this.gridView1.BestFitColumns();
                //this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
                this.gridView1.OptionsSelection.MultiSelect = true; //设置多行选择
                this.gridView1.OptionsSelection.MultiSelectMode = GridMultiSelectMode.RowSelect;
                //列不可编辑
                for (int i = 0; i < gridView1.Columns.Count; i++)
                {
                    if (this.gridView1.Columns[i].Name != "col多选")
                        this.gridView1.Columns[i].OptionsColumn.AllowEdit = false;
                }
                //设置焦点列
                DevExpress.XtraGrid.Views.Base.ColumnView view = (DevExpress.XtraGrid.Views.Base.ColumnView)gridControl1.FocusedView;
                view.FocusedColumn = view.Columns["多选"]; 
            }
            catch (Exception)
            { 
                
            }
            
        }

        string GetSelectedRows(GridView view)
        {
            string ret = ""; int rowIndex = -1;
            try
            {
                if (view.OptionsSelection.MultiSelectMode == GridMultiSelectMode.RowSelect)
                {
                    foreach (int i in gridView1.GetSelectedRows())
                    {
                        DataRow row = gridView1.GetDataRow(i); if (ret != "") ret += "\r\n";
                        //ret += string.Format("Company Name: {0} (#{1})", row[StrColunm], i);
                        ret += row[StrColunm].ToString();
                    }
                }
                else
                {
                    //foreach (GridCell cell in view.GetSelectedCells())
                    //{
                    //    if (rowIndex != cell.RowHandle)
                    //    {
                    //        if (ret != "") ret += "\r\n";
                    //        ret += string.Format("Row: #{0}", cell.RowHandle);
                    //    }
                    //    ret += "\r\n    " + view.GetRowCellDisplayText(cell.RowHandle, cell.Column);
                    //    rowIndex = cell.RowHandle;
                    //}
                }
            }
            catch (Exception)
            {
                return "";
            }
            return ret;
        } 

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1 != null && gridView1.RowCount > 0)
                {
                    string value = "";
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        value = gridView1.GetDataRow(i)["多选"].ToString();
                        if (value == "True")
                        {
                            s选择项目 += gridView1.GetRowCellValue(i, StrColunm) + "\r\n";
                        }
                    }
                    if (s选择项目 == "")
                    {
                        MessageBox.Show("请选择项目！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    //s选择项目 = GetSelectedRows(this.gridView1);
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception)
            {
            }
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                DataRowView dr = gridView1.GetRow(gridView1.GetSelectedRows()[0]) as DataRowView;
                s选择项目 = dr[StrColunm].ToString();
                this.DialogResult = DialogResult.OK;
            }
            catch
            {
            }
        }
        
    }
}
