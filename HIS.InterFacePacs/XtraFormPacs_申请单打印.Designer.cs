﻿namespace HIS.InterFacePacs
{
    partial class XtraFormPacs_申请单打印
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormPacs_申请单打印));
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.cmb病人来源 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col申请单号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col住院号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病人来源 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病人姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col年龄 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col检查部位 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col申请日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col申请科室 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col申请医生 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col检查费 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col床号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col检查项目 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col临床诊断 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col执行科室 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colURL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton查看 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit截止时间 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit开始时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt申请科室 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb病人来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit截止时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit开始时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt申请科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.cmb病人来源;
            this.layoutControlItem8.CustomizationFormText = "病人来源";
            this.layoutControlItem8.Location = new System.Drawing.Point(400, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(142, 26);
            this.layoutControlItem8.Text = "病人来源";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(52, 14);
            // 
            // cmb病人来源
            // 
            this.cmb病人来源.EditValue = "住院";
            this.cmb病人来源.Location = new System.Drawing.Point(467, 12);
            this.cmb病人来源.Name = "cmb病人来源";
            this.cmb病人来源.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb病人来源.Properties.Items.AddRange(new object[] {
            "住院",
            "门诊",
            "体检",
            "全部"});
            this.cmb病人来源.Size = new System.Drawing.Size(83, 20);
            this.cmb病人来源.StyleController = this.layoutControl1;
            this.cmb病人来源.TabIndex = 29;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.cmb病人来源);
            this.layoutControl1.Controls.Add(this.simpleButton查看);
            this.layoutControl1.Controls.Add(this.textEdit截止时间);
            this.layoutControl1.Controls.Add(this.simpleButton刷新);
            this.layoutControl1.Controls.Add(this.textEdit开始时间);
            this.layoutControl1.Controls.Add(this.txt申请科室);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(241, 249, 250, 350);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(1023, 488);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(12, 38);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(999, 438);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col申请单号,
            this.col住院号,
            this.col病人来源,
            this.col病人姓名,
            this.col性别,
            this.col年龄,
            this.col检查部位,
            this.col申请日期,
            this.col申请科室,
            this.col申请医生,
            this.col检查费,
            this.col床号,
            this.col检查项目,
            this.col临床诊断,
            this.col执行科室,
            this.colURL});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // col申请单号
            // 
            this.col申请单号.Caption = "申请单号";
            this.col申请单号.FieldName = "申请单号";
            this.col申请单号.Name = "col申请单号";
            this.col申请单号.OptionsColumn.AllowEdit = false;
            this.col申请单号.Visible = true;
            this.col申请单号.VisibleIndex = 0;
            // 
            // col住院号
            // 
            this.col住院号.Caption = "住院号";
            this.col住院号.FieldName = "住院号";
            this.col住院号.Name = "col住院号";
            this.col住院号.OptionsColumn.AllowEdit = false;
            this.col住院号.Visible = true;
            this.col住院号.VisibleIndex = 1;
            // 
            // col病人来源
            // 
            this.col病人来源.Caption = "病人来源";
            this.col病人来源.FieldName = "病人来源";
            this.col病人来源.Name = "col病人来源";
            this.col病人来源.OptionsColumn.AllowEdit = false;
            this.col病人来源.Visible = true;
            this.col病人来源.VisibleIndex = 2;
            // 
            // col病人姓名
            // 
            this.col病人姓名.Caption = "病人姓名";
            this.col病人姓名.FieldName = "病人姓名";
            this.col病人姓名.Name = "col病人姓名";
            this.col病人姓名.OptionsColumn.AllowEdit = false;
            this.col病人姓名.Visible = true;
            this.col病人姓名.VisibleIndex = 3;
            // 
            // col性别
            // 
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 4;
            // 
            // col年龄
            // 
            this.col年龄.Caption = "年龄";
            this.col年龄.FieldName = "病人年龄";
            this.col年龄.Name = "col年龄";
            this.col年龄.OptionsColumn.AllowEdit = false;
            this.col年龄.Visible = true;
            this.col年龄.VisibleIndex = 5;
            // 
            // col检查部位
            // 
            this.col检查部位.Caption = "检查部位";
            this.col检查部位.FieldName = "检查部位名称";
            this.col检查部位.Name = "col检查部位";
            this.col检查部位.OptionsColumn.AllowEdit = false;
            this.col检查部位.Visible = true;
            this.col检查部位.VisibleIndex = 6;
            // 
            // col申请日期
            // 
            this.col申请日期.Caption = "申请日期";
            this.col申请日期.FieldName = "申请日期";
            this.col申请日期.Name = "col申请日期";
            this.col申请日期.OptionsColumn.AllowEdit = false;
            this.col申请日期.Visible = true;
            this.col申请日期.VisibleIndex = 7;
            // 
            // col申请科室
            // 
            this.col申请科室.Caption = "申请科室";
            this.col申请科室.FieldName = "申请科室";
            this.col申请科室.Name = "col申请科室";
            this.col申请科室.OptionsColumn.AllowEdit = false;
            this.col申请科室.Visible = true;
            this.col申请科室.VisibleIndex = 8;
            // 
            // col申请医生
            // 
            this.col申请医生.Caption = "申请医生";
            this.col申请医生.FieldName = "申请医生";
            this.col申请医生.Name = "col申请医生";
            this.col申请医生.OptionsColumn.AllowEdit = false;
            this.col申请医生.Visible = true;
            this.col申请医生.VisibleIndex = 9;
            // 
            // col检查费
            // 
            this.col检查费.Caption = "检查费";
            this.col检查费.FieldName = "金额";
            this.col检查费.Name = "col检查费";
            this.col检查费.OptionsColumn.AllowEdit = false;
            // 
            // col床号
            // 
            this.col床号.Caption = "床号";
            this.col床号.FieldName = "床号";
            this.col床号.Name = "col床号";
            this.col床号.OptionsColumn.AllowEdit = false;
            // 
            // col检查项目
            // 
            this.col检查项目.Caption = "检查项目";
            this.col检查项目.FieldName = "收费名称";
            this.col检查项目.Name = "col检查项目";
            this.col检查项目.Visible = true;
            this.col检查项目.VisibleIndex = 11;
            // 
            // col临床诊断
            // 
            this.col临床诊断.Caption = "临床诊断";
            this.col临床诊断.FieldName = "临床诊断名称";
            this.col临床诊断.Name = "col临床诊断";
            this.col临床诊断.OptionsColumn.AllowEdit = false;
            this.col临床诊断.Visible = true;
            this.col临床诊断.VisibleIndex = 10;
            // 
            // col执行科室
            // 
            this.col执行科室.Caption = "执行科室";
            this.col执行科室.FieldName = "执行科室";
            this.col执行科室.Name = "col执行科室";
            this.col执行科室.OptionsColumn.AllowEdit = false;
            // 
            // colURL
            // 
            this.colURL.Caption = "URL";
            this.colURL.FieldName = "URL";
            this.colURL.Name = "colURL";
            // 
            // simpleButton查看
            // 
            this.simpleButton查看.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton查看.ImageOptions.Image")));
            this.simpleButton查看.Location = new System.Drawing.Point(919, 12);
            this.simpleButton查看.Name = "simpleButton查看";
            this.simpleButton查看.Size = new System.Drawing.Size(92, 22);
            this.simpleButton查看.StyleController = this.layoutControl1;
            this.simpleButton查看.TabIndex = 28;
            this.simpleButton查看.Text = "打印";
            this.simpleButton查看.Click += new System.EventHandler(this.simpleButton打印_Click);
            // 
            // textEdit截止时间
            // 
            this.textEdit截止时间.Location = new System.Drawing.Point(267, 12);
            this.textEdit截止时间.Name = "textEdit截止时间";
            this.textEdit截止时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.textEdit截止时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit截止时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit截止时间.Size = new System.Drawing.Size(141, 20);
            this.textEdit截止时间.StyleController = this.layoutControl1;
            this.textEdit截止时间.TabIndex = 20;
            // 
            // simpleButton刷新
            // 
            this.simpleButton刷新.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton刷新.ImageOptions.Image")));
            this.simpleButton刷新.Location = new System.Drawing.Point(825, 12);
            this.simpleButton刷新.Name = "simpleButton刷新";
            this.simpleButton刷新.Size = new System.Drawing.Size(90, 22);
            this.simpleButton刷新.StyleController = this.layoutControl1;
            this.simpleButton刷新.TabIndex = 6;
            this.simpleButton刷新.Text = "刷新";
            this.simpleButton刷新.Click += new System.EventHandler(this.simpleButton刷新_Click);
            // 
            // textEdit开始时间
            // 
            this.textEdit开始时间.Location = new System.Drawing.Point(67, 12);
            this.textEdit开始时间.Name = "textEdit开始时间";
            this.textEdit开始时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.textEdit开始时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit开始时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit开始时间.Size = new System.Drawing.Size(141, 20);
            this.textEdit开始时间.StyleController = this.layoutControl1;
            this.textEdit开始时间.TabIndex = 19;
            // 
            // txt申请科室
            // 
            this.txt申请科室.Location = new System.Drawing.Point(609, 12);
            this.txt申请科室.Name = "txt申请科室";
            this.txt申请科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt申请科室.Properties.NullText = "请选择...";
            this.txt申请科室.Properties.PopupSizeable = true;
            this.txt申请科室.Size = new System.Drawing.Size(126, 20);
            this.txt申请科室.StyleController = this.layoutControl1;
            this.txt申请科室.TabIndex = 21;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem1,
            this.layoutControlItem8,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1023, 488);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit开始时间;
            this.layoutControlItem2.CustomizationFormText = "开始时间:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.Name = "layoutControlItem3";
            this.layoutControlItem2.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "开始时间:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton刷新;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem5.Location = new System.Drawing.Point(813, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(94, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.Name = "layoutControlItem2";
            this.layoutControlItem5.Size = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit截止时间;
            this.layoutControlItem6.CustomizationFormText = "截止时间:";
            this.layoutControlItem6.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.Name = "layoutControlItem4";
            this.layoutControlItem6.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "截止时间:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt申请科室;
            this.layoutControlItem7.CustomizationFormText = "病区选择:";
            this.layoutControlItem7.Location = new System.Drawing.Point(542, 0);
            this.layoutControlItem7.Name = "layoutControlItem5";
            this.layoutControlItem7.Size = new System.Drawing.Size(185, 26);
            this.layoutControlItem7.Text = "申请科室";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton查看;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(907, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(727, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(86, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1003, 442);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // XtraFormPacs_申请单打印
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 488);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraFormPacs_申请单打印";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "申请单打印";
            this.Load += new System.EventHandler(this.XtraFormPacs_申请单打印_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb病人来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit截止时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit开始时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt申请科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.ComboBoxEdit cmb病人来源;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn col申请单号;
        private DevExpress.XtraGrid.Columns.GridColumn col住院号;
        private DevExpress.XtraGrid.Columns.GridColumn col病人来源;
        private DevExpress.XtraGrid.Columns.GridColumn col病人姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col年龄;
        private DevExpress.XtraGrid.Columns.GridColumn col检查部位;
        private DevExpress.XtraGrid.Columns.GridColumn col申请日期;
        private DevExpress.XtraGrid.Columns.GridColumn col申请科室;
        private DevExpress.XtraGrid.Columns.GridColumn col申请医生;
        private DevExpress.XtraGrid.Columns.GridColumn col检查费;
        private DevExpress.XtraGrid.Columns.GridColumn col床号;
        private DevExpress.XtraGrid.Columns.GridColumn col检查项目;
        private DevExpress.XtraGrid.Columns.GridColumn col临床诊断;
        private DevExpress.XtraGrid.Columns.GridColumn col执行科室;
        private DevExpress.XtraGrid.Columns.GridColumn colURL;
        private DevExpress.XtraEditors.SimpleButton simpleButton查看;
        private DevExpress.XtraEditors.TextEdit textEdit截止时间;
        private DevExpress.XtraEditors.SimpleButton simpleButton刷新;
        private DevExpress.XtraEditors.TextEdit textEdit开始时间;
        private DevExpress.XtraEditors.ComboBoxEdit txt申请科室;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
    }
}