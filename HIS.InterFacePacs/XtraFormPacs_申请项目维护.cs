﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs_申请项目维护 : DevExpress.XtraEditors.XtraForm
    {
        //WEISHENG.COMM.interFace.ModelMark modelMark = new WEISHENG.COMM.interFace.ModelMark()
        //{
        //    GUID = "08F3074F-BC33-4CE4-AF20-35E14396AEF7",
        //    S父ID = "20000",
        //    S键ID = "20015"
        //};
        public XtraFormPacs_申请项目维护()
        {
            InitializeComponent();
        }

        IList<Model.yunRis_身体部位Model> models检查部位 = null;
        IList<Model.Pacs_执行科室Model> models执行科室 = null;
        IList<Model.GY收费小项Model> models收费小项 = null;
        DataTable dt收费项目 = null;

        private void XtraFormPacs_申请项目维护_Load(object sender, EventArgs e)
        {
            models检查部位 = new BLL.yunRis_身体部位BLL().GetyunRis_身体部位All("IID not in (select 父ID from yunRis_身体部位) and 父ID<>'0'");//是否停用='否'

            models执行科室 = new BLL.Pacs_执行科室BLL().GetPacs_执行科室All(string.Format("分院编码={0}", WEISHENG.COMM.zdInfo.Model站点信息.分院编码));

            models收费小项 = new BLL.GY收费小项BLL().GetGY收费小项All("是否禁用=0");

            //string SQL = "select 收费编码,收费名称 from GY收费小项 where 是否禁用=0";
            //dt收费项目 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
            //    SQL).Tables[0];

        }

        private void simpleButton查询_Click(object sender, EventArgs e)
        {
            IList<Model.Pacs_项目对照Model> models项目对照 = new BLL.Pacs_项目对照BLL().GetPacs_项目对照All(string.Format("执行科室名称='{0}' ", comboBoxEdit1.Text));
            gridControl1.DataSource = models项目对照;

            
        }

        private void simpleButton修改_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0) return;

            Model.Pacs_项目对照Model model = gridView1.GetFocusedRow() as Model.Pacs_项目对照Model;

            XtraFormPacs_申请项目维护添加 frm = new XtraFormPacs_申请项目维护添加(model);
            frm.models检查部位 = models检查部位;
            frm.models执行科室 = models执行科室;
            frm.models收费小项 = models收费小项;
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int istate = new BLL.Pacs_项目对照BLL().Update(model);
            }
        }

        private void simpleButton添加_Click(object sender, EventArgs e)
        {
            Model.Pacs_项目对照Model model = new Model.Pacs_项目对照Model();

            XtraFormPacs_申请项目维护添加 frm = new XtraFormPacs_申请项目维护添加(model);
            frm.models检查部位 = models检查部位;
            frm.models执行科室 = models执行科室;
            frm.models收费小项 = models收费小项;
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int istate = new BLL.Pacs_项目对照BLL().Insert(model);
            }
        }
    }
}
