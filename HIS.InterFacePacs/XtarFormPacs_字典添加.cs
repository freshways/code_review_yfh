﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFacePacs
{
    public partial class XtarFormPacs_字典添加 : DevExpress.XtraEditors.XtraForm
    {
        public delegate void MethodSave(m基础资料 s);
        public event MethodSave Save;

        m基础资料 m =new m基础资料();
        public XtarFormPacs_字典添加()
        {
            InitializeComponent();
        }
        public XtarFormPacs_字典添加(m基础资料 s)
        {
            InitializeComponent();
            m = s;//Edit修改绑定(s);            
        }

        private void XtarForm_字典添加_Load(object sender, EventArgs e)
        {
            Edit修改绑定();
        }

        private void XtarForm_字典添加_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            }
        }

        private void simpleButton保存_Click(object sender, EventArgs e)
        {
            //textEdit项目.DataBindings["Text"].WriteValue();
            //dt.AcceptChanges();
            //dt.TableName = "T_病人就诊主观资料";
            //ds.Tables.Add(dt);
            //DataSetUpdateByDataSet(ds, "T_病人就诊主观资料");
            //string sql = String.Format(StrSql,textEdit项目.Text,textEdit拼音代码.Text,comboBoxEdit是否禁用.Text);
            //int rows = SQLHelper.ExecuteSql(sql);
            //if (rows > 0)
            //    this.Close();
            if (Save != null)
                Save(m);
        }

        private void Edit修改绑定()
        {
            this.textEdit项目.DataBindings.Add("Text", m, "s项目", true);
            this.textEdit拼音代码.DataBindings.Add("Text", m, "s拼音");
            this.comboBoxEdit是否禁用.DataBindings.Add("Text", m, "s是否启用");
        }
        
    }

    public class m基础资料
    {
        public int ID { get; set; }

        public string s项目 { get; set; }

        public string s拼音 { get; set; }

        private string _s是否启用 = "否";
        public string s是否启用
        { 
            get 
            {
                return _s是否启用;
            }
            set
            {
                _s是否启用 = value;
            }
        }
    }

}
