﻿// litao@Copy Right 2006-2016
// 文件： DataAccessFactory.cs
// 创建时间：2018-07-15
// ===================================================================
using System;
using System.Reflection;
using System.Configuration;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.DALFactory
{
    /// <summary>
    /// 数据层工厂

    /// </summary>
    public sealed partial class DataAccess
    {

        private static readonly string _path ="HIS.InterFacePacs"; 	
		private static readonly string _dbtype ="SqlServer";// ConfigurationManager.AppSettings.Get("DbType");
		private DataAccess() { }
		private static object CreateObject(string path,string CacheKey)
		{			
			object objType = DataCache.GetCache(CacheKey);
			if (objType == null)
			{
				try
				{
					objType = Assembly.Load(path).CreateInstance(CacheKey);					
					DataCache.SetCache(CacheKey, objType);
				}
				catch(Exception ex)
                {
                    throw ex;
                }

			}
			return objType;
		}
		/// <summary>
    	/// 通过反射机制，实例化GY收费小项接口对象。
    	/// </summary>
		///<returns>GY收费小项接口对象</returns>
		public static IGY收费小项 CreateGY收费小项()
		{		
			string CacheKey = _path+"."+_dbtype+"DAL.GY收费小项DAL";	
			object objType=CreateObject(_path,CacheKey);			
			return (IGY收费小项)objType;		
		}

		
    }
}
