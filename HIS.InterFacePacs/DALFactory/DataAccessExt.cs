﻿// litao@Copy Right 2006-2016
// 文件： DataAccessFactory.cs
// 创建时间：2018-05-05
// ===================================================================
using System;
using System.Reflection;
using System.Configuration;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.DALFactory
{
    /// <summary>
    /// 数据层工厂

    /// </summary>
    public sealed partial class DataAccess
    {

        private static readonly string _path ="HIS.InterFacePacs"; 	
		private static readonly string _dbtype ="SqlServer";// ConfigurationManager.AppSettings.Get("DbType");
		private DataAccess() { }
		private static object CreateObject(string path,string CacheKey)
		{			
			object objType = DataCache.GetCache(CacheKey);
			if (objType == null)
			{
				try
				{
					objType = Assembly.Load(path).CreateInstance(CacheKey);					
					DataCache.SetCache(CacheKey, objType);
				}
				catch(Exception ex)
                {
                    throw ex;
                }

			}
			return objType;
		}
        /// <summary>
        /// 通过反射机制，实例化Pacs_申请模板接口对象。
        /// </summary>
        ///<returns>Pacs_申请模板接口对象</returns>
        public static IPacs_申请模板 CreatePacs_申请模板()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.Pacs_申请模板DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IPacs_申请模板)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化Pacs_检查部位接口对象。
        /// </summary>
        ///<returns>Pacs_检查部位接口对象</returns>
        public static IPacs_检查部位 CreatePacs_检查部位()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.Pacs_检查部位DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IPacs_检查部位)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化Pacs_检查申请接口对象。
        /// </summary>
        ///<returns>Pacs_检查申请接口对象</returns>
        public static IPacs_检查申请 CreatePacs_检查申请()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.Pacs_检查申请DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IPacs_检查申请)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化Pacs_项目对照接口对象。
        /// </summary>
        ///<returns>Pacs_项目对照接口对象</returns>
        public static IPacs_项目对照 CreatePacs_项目对照()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.Pacs_项目对照DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IPacs_项目对照)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化Pacs_执行科室接口对象。
        /// </summary>
        ///<returns>Pacs_执行科室接口对象</returns>
        public static IPacs_执行科室 CreatePacs_执行科室()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.Pacs_执行科室DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IPacs_执行科室)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化yunRis_身体部位接口对象。
        /// </summary>
        ///<returns>yunRis_身体部位接口对象</returns>
        public static IyunRis_身体部位 CreateyunRis_身体部位()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.yunRis_身体部位DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IyunRis_身体部位)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化yunRis_身体部位接口对象。
        /// </summary>
        ///<returns>yunRis_身体部位接口对象</returns>
        public static IyunRis_身体部位Ext CreateyunRis_身体部位Ext()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.yunRis_身体部位DALExt";
            object objType = CreateObject(_path, CacheKey);
            return (IyunRis_身体部位Ext)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化GY收费小项接口对象。
        /// </summary>
        ///<returns>GY收费小项接口对象</returns>
        public static IGY收费小项 CreateGY收费小项()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.GY收费小项DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IGY收费小项)objType;
        }
    }
}
