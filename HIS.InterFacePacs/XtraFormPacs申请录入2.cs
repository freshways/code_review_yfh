﻿using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using HIS.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs申请录入2 : DevExpress.XtraEditors.XtraForm
    {
        HIS.COMM.Person住院病人 zyperson;
        HIS.COMM.Person门诊病人 mzperson;
        private Pacs_项目对照 model项目对照 = new Pacs_项目对照();
        public Pacs_检查申请 model检查申请 = new Pacs_检查申请();
        String _s申请单号 = "";
        public string _医嘱内容 = "", _胶片编码;
        public int _胶片张数, _收费数量;
        DataTable dt收费项目 = null;

        /// <summary>
        /// 住院
        /// </summary>
        /// <param name="dtPACS对照"></param>
        /// <param name="s申请单号"></param>
        /// <param name="_person"></param>
        public XtraFormPacs申请录入2(HIS.COMM.Person住院病人 _person)
        {
            InitializeComponent();
            zyperson = _person;
            _s申请单号 = Pacs申请.Get申请单号New("1");//Pacs申请.s住院新申请单据号();
            layoutControlGroup检查申请.Text = string.Format("检查申请单☆{0}", _s申请单号);
            textEdit诊断.Text = zyperson.s疾病名称;
        }

        /// <summary>
        /// 申请单
        /// </summary>
        /// <param name="检查申请Model对照">初始化实体类</param>
        /// <param name="sTypes">申请单号开头类型</param>
        /// <param name="_打印申请单">是否打印申请单</param>
        public XtraFormPacs申请录入2(Pacs_检查申请 检查申请Model, string Types, bool _打印申请单)
        {
            InitializeComponent();
            model检查申请 = 检查申请Model;
            _s申请单号 = Pacs申请.Get申请单号New(Types);//Pacs申请.s住院新申请单据号();
            layoutControlGroup检查申请.Text = string.Format("检查申请单☆{0}", _s申请单号);
            textEdit诊断.Text = 检查申请Model.临床诊断名称;
            this.ch打印申请单.Checked = _打印申请单;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XtraFormPacs申请录入_Load(object sender, EventArgs e)
        {

            try
            {
                //BLL.Pacs_项目对照BLL bll项目对照 = new BLL.Pacs_项目对照BLL();
                IList<Pacs_项目对照> models项目对照 = dataHelper.chis.Pacs_项目对照.ToList();
                searchLookUpEdit项目对照.Properties.DataSource = models项目对照;
                searchLookUpEdit项目对照.Properties.DisplayMember = "收费名称";
                searchLookUpEdit项目对照.Properties.ValueMember = "ID";

                //IList<Model.Pacs_检查部位Model> models检查部位 = new BLL.Pacs_检查部位BLL().GetPacs_检查部位All("是否停用='否'");
                IList<Model.yunRis_身体部位Model> models检查部位 = new BLL.yunRis_身体部位BLL().GetyunRis_身体部位All("IID not in (select 父ID from yunRis_身体部位) and 父ID<>'0'");
                searchLookUpEdit检查部位.Properties.DataSource = models检查部位;
                searchLookUpEdit检查部位.Properties.PopulateViewColumns();
                searchLookUpEdit检查部位.Properties.DisplayMember = "部位名称";
                searchLookUpEdit检查部位.Properties.ValueMember = "部位编码";
                searchLookUpEdit检查部位.Properties.View.Columns["部位名称"].Width = 150;
                //searchLookUpEdit检查部位.Properties.View.Columns["设备类型"].Visible = true;
                //searchLookUpEdit检查部位.Properties.View.Columns["部位编码"].Visible = true;
                //searchLookUpEdit检查部位.Properties.View.Columns["部位名称_ENG"].Visible = true;
                searchLookUpEdit检查部位.Properties.PopupFormSize = new System.Drawing.Size(700, 300);

                IList<Model.Pacs_执行科室Model> models执行科室 = new BLL.Pacs_执行科室BLL().GetPacs_执行科室All(string.Format("分院编码={0}", WEISHENG.COMM.zdInfo.Model站点信息.分院编码));
                searchLookUpEdit执行科室.Properties.DataSource = models执行科室;
                searchLookUpEdit执行科室.Properties.DisplayMember = "执行科室名称";
                searchLookUpEdit执行科室.Properties.ValueMember = "执行科室编码";

                searchLookUpEdit检查部位.DataBindings.Add("EditValue", model项目对照, "部位编码");
                textEdit医嘱名称.DataBindings.Add("EditValue", model项目对照, "医嘱名称");
                txt数量.DataBindings.Add("EditValue", model项目对照, "收费数量");
                txt胶片.DataBindings.Add("EditValue", model项目对照, "胶片张数");
                searchLookUpEdit执行科室.DataBindings.Add("EditValue", model项目对照, "执行科室编码");

                dt收费项目 = HIS.COMM.Class录入提示.dt费用录入提示(HIS.COMM.ClassCflx.enCflx.检查治疗单, "");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            Pacs_项目对照 selectedModel = searchLookUpEdit项目对照.Properties.View.GetFocusedRow() as Pacs_项目对照;
            try
            {
                if (searchLookUpEdit检查部位.Text == "")
                {
                    MessageBox.Show("请选择检查部位", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (searchLookUpEdit执行科室.Text == "")
                {
                    MessageBox.Show("请选择执行科室", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (string.IsNullOrEmpty(txt数量.Text) && Convert.ToInt16(txt数量.Text) <= 0)
                {
                    MessageBox.Show("数量不能为空必须大于0！关系到计费！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //todo:这个地方取不到数据,凤海处理一下
                //Model.Pacs_检查部位Model model检查部位 = searchLookUpEdit检查部位.Properties.View.GetFocusedRow() as Model.Pacs_检查部位Model;
                string _str = searchLookUpEdit检查部位.Text;
                string _str2 = searchLookUpEdit检查部位.EditValue.ToString();
                if (zyperson != null && !string.IsNullOrEmpty(zyperson.sZYID))
                {
                    model检查申请.病人ID = zyperson.sZYID;
                    model检查申请.病人姓名 = zyperson.S姓名;
                    model检查申请.病人年龄 = Convert.ToInt16(zyperson.S年龄);
                    model检查申请.性别 = zyperson.S性别;
                    //modelSq.住院号 = zyperson.s住院号码;
                    model检查申请.门诊号 = "-1";
                    model检查申请.申请医生 = zyperson.s主治医生编码;
                    model检查申请.床号 = zyperson.S床位名称;
                    model检查申请.病人来源 = "住院";
                }
                else if (mzperson != null && !string.IsNullOrEmpty(mzperson.sMZID))
                {
                    model检查申请.病人ID = mzperson.sMZID;
                    model检查申请.病人姓名 = mzperson.S姓名;
                    model检查申请.病人年龄 = Convert.ToInt16(mzperson.S年龄);
                    model检查申请.性别 = mzperson.S性别;
                    model检查申请.住院号 = "-1";
                    //modelSq.门诊号 = mzperson.sMZID;
                    model检查申请.申请医生 = mzperson.S医生编码;
                    model检查申请.床号 = "-1";
                    model检查申请.病人来源 = "门诊";
                }
                //以下是通用部分
                model检查申请.检查部位名称 = searchLookUpEdit检查部位.Text;
                model检查申请.检查部位编码 = searchLookUpEdit检查部位.EditValue.ToString();
                model检查申请.收费名称 = selectedModel.收费名称.ToString();//searchLookUpEdit项目对照.Text;
                model检查申请.收费编码 = selectedModel.收费编码.ToString();//searchLookUpEdit项目对照.EditValue.ToString();
                model检查申请.体征描述 = textEdit主诉.Text.Trim();
                model检查申请.检查目的 = textEdit查体目的.Text.Trim();
                model检查申请.执行科室名称 = searchLookUpEdit执行科室.Text;
                model检查申请.临床诊断名称 = textEdit诊断.Text;
                model检查申请.申请单号 = _s申请单号;
                model检查申请.申请日期 = DateTime.Now;
                model检查申请.申请医生 = WEISHENG.COMM.zdInfo.ModelUserInfo.用户名;
                _医嘱内容 = textEdit医嘱名称.Text.ToString().Trim();
                _胶片编码 = model项目对照.胶片编码;
                _胶片张数 = Convert.ToInt16(model项目对照.胶片张数);
                _收费数量 = Convert.ToInt16(model项目对照.收费数量);

                if (ch打印申请单.Checked)
                {
                    //创建申请单打印
                    XtraReport影像申请单 report = new XtraReport影像申请单(model检查申请);
                    //report.CreateDocument();
                    ////添加到打印页母板
                    //report1.Pages.AddRange(report.Pages);
                    ReportPrintTool pt1 = new ReportPrintTool(report);
                    pt1.ShowPreviewDialog();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.Close();
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton诊断_Click(object sender, EventArgs e)
        {
            HIS.COMM.XtraFormICD10 icdForm = new HIS.COMM.XtraFormICD10();
            if (icdForm.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            textEdit诊断.Text = icdForm.selected_icd_info.ICD10Name;
            textEdit诊断.Tag = icdForm.selected_icd_info.ICD10;
        }


        private void simpleButton主诉_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from Pacs_字典 where 类别='主诉' and 是否禁用='否' ";
            XtraFormPacs_选择病人就诊资料 frm = new XtraFormPacs_选择病人就诊资料(sql, "名称", "病人主诉/体征描述");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit主诉.Text = frm.s选择项目;
        }

        private void simpleButton查体_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from Pacs_字典 where 类别='查体' and 是否禁用='否'  ";
            XtraFormPacs_选择病人就诊资料 frm = new XtraFormPacs_选择病人就诊资料(sql, "名称", "病人检查目的/查体");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit查体目的.Text = frm.s选择项目;
        }

        private void textEdit诊断_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            }
        }



        /// <summary>
        /// 因为没有扎到searchLookUpEdit项目对照和其他空间双向同步更新的办法，所以用实体类的Inotify接口实现同步显示。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchLookUpEdi项目对照_EditValueChanged(object sender, EventArgs e)
        {
            Pacs_项目对照 selectedModel = searchLookUpEdit项目对照.Properties.View.GetFocusedRow() as Pacs_项目对照;
            //方法1，遍历属性复制，界面实现同步。测试通过
            //var ParentType = typeof(Model.Pacs_项目对照Model);
            //var Properties = ParentType.GetProperties();
            //foreach (var Propertie in Properties)
            //{
            //    if (Propertie.CanRead && Propertie.CanWrite)
            //    {
            //        Propertie.SetValue(modelCurr对照, Propertie.GetValue(selectedModel, null), null);
            //    }
            //}
            HIS.Model.Helper.modelHelper.copyInstanceValue<Pacs_项目对照>(selectedModel, model项目对照);
            try
            {
                Decimal dec曝光金额 = 0, dec胶片金额 = 0;
                if (!string.IsNullOrEmpty(selectedModel.收费编码.ToString()))
                {
                    DataRow[] drs = dt收费项目.Select("收费编码 in('" + selectedModel.收费编码 + "')");
                    foreach (var dr in drs)
                    {
                        dec曝光金额 += Convert.ToDecimal(dr["单价"].ToString()) * Convert.ToDecimal(this.txt数量.Text);
                    }
                }
                if (!string.IsNullOrEmpty(selectedModel.胶片编码))
                {
                    DataRow[] drs2 = dt收费项目.Select("收费编码 in('" + selectedModel.胶片编码 + "')");
                    foreach (var dr in drs2)
                    {
                        dec胶片金额 += Convert.ToDecimal(dr["单价"].ToString()) * Convert.ToDecimal(this.txt胶片.Text);
                    }
                }
                this.txt金额.Text = "总金额：" + (dec曝光金额 + dec胶片金额).ToString() + " 其中（胶片费：" + dec胶片金额.ToString() + ")";
                model检查申请.检查费 = (dec曝光金额 + dec胶片金额); //计算检查费，门诊处方和住院医嘱屏蔽计算
            }
            catch (Exception)
            {
                this.txt金额.Text = "";
            }

        }

    }
}
