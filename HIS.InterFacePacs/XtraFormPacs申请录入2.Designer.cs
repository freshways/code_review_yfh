﻿namespace HIS.InterFacePacs
{
    partial class XtraFormPacs申请录入2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormPacs申请录入2));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton确定 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt金额 = new DevExpress.XtraEditors.TextEdit();
            this.txt胶片 = new DevExpress.XtraEditors.TextEdit();
            this.txt数量 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit医嘱名称 = new DevExpress.XtraEditors.TextEdit();
            this.searchLookUpEdit项目对照 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView医嘱名称 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit家庭地址 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton查体 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton主诉 = new DevExpress.XtraEditors.SimpleButton();
            this.searchLookUpEdit执行科室 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton诊断 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit诊断 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.searchLookUpEdit检查部位 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textEdit主诉 = new DevExpress.XtraEditors.MemoEdit();
            this.textEdit查体目的 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup检查申请 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem住址 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.gridColumn医嘱名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn收费名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn拼音代码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ch打印申请单 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt金额.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胶片.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit医嘱名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit项目对照.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView医嘱名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家庭地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit执行科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit诊断.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit检查部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主诉.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit查体目的.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup检查申请)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem住址)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch打印申请单.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlGroup检查申请,
            this.emptySpaceItem1,
            this.layoutControlItem17});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(462, 467);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton确定;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(340, 432);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(58, 29);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // simpleButton确定
            // 
            this.simpleButton确定.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton确定.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton确定.Image")));
            this.simpleButton确定.Location = new System.Drawing.Point(345, 437);
            this.simpleButton确定.Name = "simpleButton确定";
            this.simpleButton确定.Size = new System.Drawing.Size(54, 22);
            this.simpleButton确定.StyleController = this.layoutControl1;
            this.simpleButton确定.TabIndex = 4;
            this.simpleButton确定.Text = "确定";
            this.simpleButton确定.Click += new System.EventHandler(this.simpleButton确定_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.ch打印申请单);
            this.layoutControl1.Controls.Add(this.txt金额);
            this.layoutControl1.Controls.Add(this.txt胶片);
            this.layoutControl1.Controls.Add(this.txt数量);
            this.layoutControl1.Controls.Add(this.textEdit医嘱名称);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit项目对照);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit家庭地址);
            this.layoutControl1.Controls.Add(this.simpleButton查体);
            this.layoutControl1.Controls.Add(this.simpleButton主诉);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit执行科室);
            this.layoutControl1.Controls.Add(this.simpleButton诊断);
            this.layoutControl1.Controls.Add(this.textEdit诊断);
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.simpleButton确定);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit检查部位);
            this.layoutControl1.Controls.Add(this.textEdit主诉);
            this.layoutControl1.Controls.Add(this.textEdit查体目的);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(678, 203, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(479, 462);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt金额
            // 
            this.txt金额.Location = new System.Drawing.Point(76, 85);
            this.txt金额.Name = "txt金额";
            this.txt金额.Properties.ReadOnly = true;
            this.txt金额.Size = new System.Drawing.Size(373, 20);
            this.txt金额.StyleController = this.layoutControl1;
            this.txt金额.TabIndex = 20;
            // 
            // txt胶片
            // 
            this.txt胶片.Location = new System.Drawing.Point(108, 357);
            this.txt胶片.Name = "txt胶片";
            this.txt胶片.Size = new System.Drawing.Size(341, 20);
            this.txt胶片.StyleController = this.layoutControl1;
            this.txt胶片.TabIndex = 21;
            // 
            // txt数量
            // 
            this.txt数量.Location = new System.Drawing.Point(108, 333);
            this.txt数量.Name = "txt数量";
            this.txt数量.Size = new System.Drawing.Size(341, 20);
            this.txt数量.StyleController = this.layoutControl1;
            this.txt数量.TabIndex = 2;
            // 
            // textEdit医嘱名称
            // 
            this.textEdit医嘱名称.Location = new System.Drawing.Point(76, 61);
            this.textEdit医嘱名称.Name = "textEdit医嘱名称";
            this.textEdit医嘱名称.Size = new System.Drawing.Size(373, 20);
            this.textEdit医嘱名称.StyleController = this.layoutControl1;
            this.textEdit医嘱名称.TabIndex = 19;
            // 
            // searchLookUpEdit项目对照
            // 
            this.searchLookUpEdit项目对照.EditValue = "请选择...";
            this.searchLookUpEdit项目对照.Location = new System.Drawing.Point(76, 37);
            this.searchLookUpEdit项目对照.Name = "searchLookUpEdit项目对照";
            this.searchLookUpEdit项目对照.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit项目对照.Properties.DisplayMember = "部位名称";
            this.searchLookUpEdit项目对照.Properties.NullText = "请选择部位...";
            this.searchLookUpEdit项目对照.Properties.ValueMember = "部位编码";
            this.searchLookUpEdit项目对照.Properties.View = this.gridView医嘱名称;
            this.searchLookUpEdit项目对照.Size = new System.Drawing.Size(373, 20);
            this.searchLookUpEdit项目对照.StyleController = this.layoutControl1;
            this.searchLookUpEdit项目对照.TabIndex = 20;
            this.searchLookUpEdit项目对照.EditValueChanged += new System.EventHandler(this.searchLookUpEdi项目对照_EditValueChanged);
            // 
            // gridView医嘱名称
            // 
            this.gridView医嘱名称.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn医嘱名称,
            this.gridColumn收费名称,
            this.gridColumn拼音代码});
            this.gridView医嘱名称.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView医嘱名称.Name = "gridView医嘱名称";
            this.gridView医嘱名称.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView医嘱名称.OptionsView.ShowGroupPanel = false;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(108, 405);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Size = new System.Drawing.Size(341, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 19;
            // 
            // textEdit家庭地址
            // 
            this.textEdit家庭地址.Enabled = false;
            this.textEdit家庭地址.Location = new System.Drawing.Point(108, 381);
            this.textEdit家庭地址.Name = "textEdit家庭地址";
            this.textEdit家庭地址.Size = new System.Drawing.Size(341, 20);
            this.textEdit家庭地址.StyleController = this.layoutControl1;
            this.textEdit家庭地址.TabIndex = 18;
            // 
            // simpleButton查体
            // 
            this.simpleButton查体.Location = new System.Drawing.Point(423, 184);
            this.simpleButton查体.Name = "simpleButton查体";
            this.simpleButton查体.Size = new System.Drawing.Size(26, 71);
            this.simpleButton查体.StyleController = this.layoutControl1;
            this.simpleButton查体.TabIndex = 17;
            this.simpleButton查体.Text = "...";
            this.simpleButton查体.Click += new System.EventHandler(this.simpleButton查体_Click);
            // 
            // simpleButton主诉
            // 
            this.simpleButton主诉.Location = new System.Drawing.Point(423, 109);
            this.simpleButton主诉.Name = "simpleButton主诉";
            this.simpleButton主诉.Size = new System.Drawing.Size(26, 71);
            this.simpleButton主诉.StyleController = this.layoutControl1;
            this.simpleButton主诉.TabIndex = 16;
            this.simpleButton主诉.Text = "...";
            this.simpleButton主诉.Click += new System.EventHandler(this.simpleButton主诉_Click);
            // 
            // searchLookUpEdit执行科室
            // 
            this.searchLookUpEdit执行科室.EditValue = "";
            this.searchLookUpEdit执行科室.Location = new System.Drawing.Point(108, 309);
            this.searchLookUpEdit执行科室.Name = "searchLookUpEdit执行科室";
            this.searchLookUpEdit执行科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit执行科室.Properties.NullText = "";
            this.searchLookUpEdit执行科室.Properties.ReadOnly = true;
            this.searchLookUpEdit执行科室.Properties.View = this.gridView1;
            this.searchLookUpEdit执行科室.Size = new System.Drawing.Size(341, 20);
            this.searchLookUpEdit执行科室.StyleController = this.layoutControl1;
            this.searchLookUpEdit执行科室.TabIndex = 13;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButton诊断
            // 
            this.simpleButton诊断.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton诊断.Image")));
            this.simpleButton诊断.Location = new System.Drawing.Point(423, 259);
            this.simpleButton诊断.Name = "simpleButton诊断";
            this.simpleButton诊断.Size = new System.Drawing.Size(26, 22);
            this.simpleButton诊断.StyleController = this.layoutControl1;
            this.simpleButton诊断.TabIndex = 12;
            this.simpleButton诊断.Click += new System.EventHandler(this.simpleButton诊断_Click);
            // 
            // textEdit诊断
            // 
            this.textEdit诊断.Location = new System.Drawing.Point(108, 259);
            this.textEdit诊断.Name = "textEdit诊断";
            this.textEdit诊断.Size = new System.Drawing.Size(311, 20);
            this.textEdit诊断.StyleController = this.layoutControl1;
            this.textEdit诊断.TabIndex = 11;
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton取消.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton取消.Image")));
            this.simpleButton取消.Location = new System.Drawing.Point(403, 437);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(54, 22);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 5;
            this.simpleButton取消.Text = "取消";
            this.simpleButton取消.Click += new System.EventHandler(this.simpleButton取消_Click);
            // 
            // searchLookUpEdit检查部位
            // 
            this.searchLookUpEdit检查部位.EditValue = "请选择...";
            this.searchLookUpEdit检查部位.Location = new System.Drawing.Point(108, 285);
            this.searchLookUpEdit检查部位.Name = "searchLookUpEdit检查部位";
            this.searchLookUpEdit检查部位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit检查部位.Properties.DisplayMember = "部位名称";
            this.searchLookUpEdit检查部位.Properties.NullText = "请选择部位...";
            this.searchLookUpEdit检查部位.Properties.ValueMember = "部位编码";
            this.searchLookUpEdit检查部位.Properties.View = this.searchLookUpEdit1View;
            this.searchLookUpEdit检查部位.Size = new System.Drawing.Size(341, 20);
            this.searchLookUpEdit检查部位.StyleController = this.layoutControl1;
            this.searchLookUpEdit检查部位.TabIndex = 6;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // textEdit主诉
            // 
            this.textEdit主诉.Location = new System.Drawing.Point(108, 109);
            this.textEdit主诉.Name = "textEdit主诉";
            this.textEdit主诉.Size = new System.Drawing.Size(311, 71);
            this.textEdit主诉.StyleController = this.layoutControl1;
            this.textEdit主诉.TabIndex = 7;
            this.textEdit主诉.UseOptimizedRendering = true;
            // 
            // textEdit查体目的
            // 
            this.textEdit查体目的.Location = new System.Drawing.Point(108, 184);
            this.textEdit查体目的.Name = "textEdit查体目的";
            this.textEdit查体目的.Size = new System.Drawing.Size(311, 71);
            this.textEdit查体目的.StyleController = this.layoutControl1;
            this.textEdit查体目的.TabIndex = 10;
            this.textEdit查体目的.UseOptimizedRendering = true;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton取消;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(398, 432);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(58, 29);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup检查申请
            // 
            this.layoutControlGroup检查申请.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 11F);
            this.layoutControlGroup检查申请.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup检查申请.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup检查申请.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup检查申请.CustomizationFormText = "检查申请单";
            this.layoutControlGroup检查申请.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem11,
            this.layoutControlItem7,
            this.layoutControlItem12,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem3,
            this.layoutControlItem10,
            this.layoutControlItem住址,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem6,
            this.layoutControlItem15,
            this.layoutControlItem5,
            this.layoutControlItem16});
            this.layoutControlGroup检查申请.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup检查申请.Name = "layoutControlGroup检查申请";
            this.layoutControlGroup检查申请.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup检查申请.Size = new System.Drawing.Size(456, 432);
            this.layoutControlGroup检查申请.Text = "检查申请单";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem4.Control = this.textEdit主诉;
            this.layoutControlItem4.CustomizationFormText = "体征描述:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 75);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(101, 75);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(410, 75);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "主诉(病历摘要体征描述)：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton主诉;
            this.layoutControlItem11.CustomizationFormText = "...";
            this.layoutControlItem11.Location = new System.Drawing.Point(410, 72);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "...";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit查体目的;
            this.layoutControlItem7.CustomizationFormText = "查体";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 147);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 75);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(101, 75);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(410, 75);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "检查目的：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.simpleButton查体;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(410, 147);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit诊断;
            this.layoutControlItem8.CustomizationFormText = "诊断";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 222);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(410, 26);
            this.layoutControlItem8.Text = "临床诊断：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton诊断;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(410, 222);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.searchLookUpEdit检查部位;
            this.layoutControlItem3.CustomizationFormText = "新组号:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 248);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem3.Text = "检查部位：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.searchLookUpEdit执行科室;
            this.layoutControlItem10.CustomizationFormText = "执行科室：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 272);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem10.Text = "执行科室：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem住址
            // 
            this.layoutControlItem住址.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem住址.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem住址.Control = this.textEdit家庭地址;
            this.layoutControlItem住址.CustomizationFormText = "家庭住址：";
            this.layoutControlItem住址.Location = new System.Drawing.Point(0, 344);
            this.layoutControlItem住址.Name = "layoutControlItem住址";
            this.layoutControlItem住址.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem住址.Text = "家庭住址：";
            this.layoutControlItem住址.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem住址.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem住址.TextToControlDistance = 5;
            this.layoutControlItem住址.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.textEdit身份证号;
            this.layoutControlItem13.CustomizationFormText = "身份证号：";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 368);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem13.Text = "身份证号：";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.searchLookUpEdit项目对照;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem14.Text = "检查项目：";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit医嘱名称;
            this.layoutControlItem6.CustomizationFormText = "医嘱名称:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem6.Text = "医嘱名称:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt胶片;
            this.layoutControlItem15.CustomizationFormText = "胶片";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 320);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem15.Text = "胶片:";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt数量;
            this.layoutControlItem5.CustomizationFormText = "数量";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 296);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem5.Text = "数量:";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt金额;
            this.layoutControlItem16.CustomizationFormText = "金  额:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(440, 24);
            this.layoutControlItem16.Text = "金  额:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(60, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(170, 432);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 29);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 29);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(170, 29);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // gridColumn医嘱名称
            // 
            this.gridColumn医嘱名称.Caption = "医嘱名称";
            this.gridColumn医嘱名称.FieldName = "医嘱名称";
            this.gridColumn医嘱名称.Name = "gridColumn医嘱名称";
            this.gridColumn医嘱名称.Visible = true;
            this.gridColumn医嘱名称.VisibleIndex = 0;
            // 
            // gridColumn收费名称
            // 
            this.gridColumn收费名称.Caption = "收费名称";
            this.gridColumn收费名称.FieldName = "收费名称";
            this.gridColumn收费名称.Name = "gridColumn收费名称";
            this.gridColumn收费名称.Visible = true;
            this.gridColumn收费名称.VisibleIndex = 1;
            // 
            // gridColumn拼音代码
            // 
            this.gridColumn拼音代码.Caption = "拼音代码";
            this.gridColumn拼音代码.FieldName = "医嘱名称拼音代码";
            this.gridColumn拼音代码.Name = "gridColumn拼音代码";
            this.gridColumn拼音代码.Visible = true;
            this.gridColumn拼音代码.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "设备类型";
            this.gridColumn4.FieldName = "设备类型";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "部位编码";
            this.gridColumn1.FieldName = "部位编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "部位名称";
            this.gridColumn2.FieldName = "部位名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "简称";
            this.gridColumn3.FieldName = "部位名称_ENG";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // ch打印申请单
            // 
            this.ch打印申请单.Location = new System.Drawing.Point(5, 437);
            this.ch打印申请单.Name = "ch打印申请单";
            this.ch打印申请单.Properties.Caption = "打印申请单";
            this.ch打印申请单.Size = new System.Drawing.Size(166, 19);
            this.ch打印申请单.StyleController = this.layoutControl1;
            this.ch打印申请单.TabIndex = 22;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.ch打印申请单;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 432);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(170, 29);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // XtraFormPacs申请录入2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 462);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraFormPacs申请录入2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pacs申请录入";
            this.Load += new System.EventHandler(this.XtraFormPacs申请录入_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt金额.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胶片.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit医嘱名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit项目对照.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView医嘱名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家庭地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit执行科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit诊断.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit检查部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主诉.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit查体目的.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup检查申请)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem住址)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch打印申请单.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton simpleButton确定;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit检查部位;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraEditors.MemoEdit textEdit主诉;
        private DevExpress.XtraEditors.TextEdit textEdit诊断;
        private DevExpress.XtraEditors.MemoEdit textEdit查体目的;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton simpleButton诊断;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit执行科室;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton simpleButton查体;
        private DevExpress.XtraEditors.SimpleButton simpleButton主诉;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.TextEdit textEdit家庭地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem住址;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup检查申请;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit项目对照;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView医嘱名称;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn医嘱名称;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn收费名称;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit textEdit医嘱名称;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn拼音代码;
        private DevExpress.XtraEditors.TextEdit txt胶片;
        private DevExpress.XtraEditors.TextEdit txt数量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit txt金额;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.CheckEdit ch打印申请单;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
    }
}