﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace HIS.InterFacePacs
{
    public partial class XtarFormPacs_部位维护 : DevExpress.XtraEditors.XtraForm
    {
        public XtarFormPacs_部位维护()
        {
            InitializeComponent();
        }


        private void XtarFormPacs_部位维护_Load(object sender, EventArgs e)
        {
            BindType();
            BindList();
        }

        #region 菜单事件

        private void simpleButton查询_Click(object sender, EventArgs e)
        {
            BindList();
        }

        private void simpleButton启用_Click(object sender, EventArgs e)
        {
            string rowIDs = GetSelectedRows(this.gridView1);
            string Sql = "update Pacs_检查部位 set 是否停用='否' where ID in({0})";
            Sql = String.Format(Sql, rowIDs);
            int rows = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, Sql);
            if (rows > 0)
                MessageBox.Show("启用成功！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BindList();
        }

        private void simpleButton停用_Click(object sender, EventArgs e)
        {
            string rowIDs = GetSelectedRows(this.gridView1);
            string Sql = "update Pacs_检查部位 set 是否停用='是' where ID in({0})";
            Sql = String.Format(Sql, rowIDs);
            int rows = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, Sql);
            if (rows > 0)
                MessageBox.Show("停用成功！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BindList();
        }

        private void simpleButton同步_Click(object sender, EventArgs e)
        {
            #region 同步检查部位信息

            //获取远程服务器数据
            //string SQL = "select * from eris.vw_Pacs检查部位";

            //HIS.Model.Dal.OracleHelper.connectionString = ClassConnStrings.sConnOraPacsDB;
            //DataSet dtDnRemote = HIS.Model.Dal.OracleHelper.Query(SQL);
            //DataSet dtDnRemote = HIS.Model.Dal.OracleHelper.ExecuteDataset(ClassConnStrings.sConnOraPacsDB, CommandType.Text, SQL);

            string SQL = "select 设备类型 as 部位分类,部位编码,部位名称,部位名称_ENG as 部位索引 from  yunRis_身体部位 where IID not in (select 父ID from yunRis_身体部位) and 父ID<>'0'";
            DataSet dtDnRemote = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL);

            if (dtDnRemote == null || dtDnRemote.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("没有要更新的数据！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //获取本地数据
             string  SqlUser = @"SELECT * FROM [dbo].[Pacs_检查部位] where  1=1 ";

            // 获取本地数据
            DataSet dtDnLocal = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SqlUser);
            dtDnLocal.Tables[0].TableName = "Pacs_检查部位";//给table起名，方便更新
            int totalDnCount = dtDnRemote.Tables[0].Rows.Count;
            
            int InsertRow = 0; //新增行数
            int UpdateRow = 0;//更新行数

            for (int i = 0; i < totalDnCount; i++)
            {
                DataRow dr = dtDnRemote.Tables[0].Rows[i];
                DataRow[] drs = dtDnLocal.Tables[0].Select(string.Format(@"部位编码='{0}'", dr["部位编码"]));
                if (drs.Length < 1)
                {
                    DataRow drNew = dtDnLocal.Tables[0].NewRow();
                    //插入
                    drNew["部位编码"] = dr["部位编码"];
                    drNew["部位名称"] = dr["部位名称"];
                    drNew["部位索引"] = dr["部位索引"];
                    drNew["部位分类"] = dr["部位分类"];
                    drNew["是否停用"] = "否";
                    dtDnLocal.Tables[0].Rows.Add(drNew);
                    InsertRow += 1;
                }
                else
                {
                    DataRow drLocal = drs[0];
                    //更新
                    drLocal["部位编码"] = dr["部位编码"];
                    drLocal["部位名称"] = dr["部位名称"];
                    //drLocal["部位索引"] = dr["部位索引"];
                    drLocal["部位分类"] = dr["部位分类"];
                    UpdateRow += 1;
                }
                //System.Threading.Thread.Sleep(1);
            }
            #endregion
            DataTable dt修改 = dtDnLocal.Tables[0].GetChanges();
            if (dt修改 == null)
            {
                MessageBox.Show("未发现可保存信息", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string message = string.Format("数据处理完成！新增{0}条，更新{1}条！", InsertRow, UpdateRow);
            using (SqlConnection Conn = new SqlConnection(HIS.COMM.ClassDBConnstring.SConnHISDb))
            {
                SqlTransaction transaction = null;
                Conn.Open();
                transaction = Conn.BeginTransaction();
                try
                {                    
                    HIS.Model.Dal.autoDb.Update(dtDnLocal.Tables[0], transaction);
                    transaction.Commit();
                    MessageBox.Show(message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    string sErr = ex.Message;
                    WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                    MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    transaction.Rollback();
                }
                finally
                {
                    Conn.Close();
                }
            }
        }


        #endregion

        #region 方法

        DataTable dt;
        private void BindList()
        {
            string Sql = "";
            //switch (comboBoxEdit1.Text)
            //{
            //    case "主诉":
            //        Sql = "select  ID,部位编码,部位名称,部位索引,部位分类,是否停用 from Pacs_检查部位 where 部位分类='CT' ";
            //        break;
            //    case "查体":
            //        Sql = "select  ID,部位编码,部位名称,部位索引,部位分类,是否停用 from Pacs_检查部位 where 部位分类='US'  ";
            //        break;
            //    default:
            //        Sql = "select ID,部位编码,部位名称,部位索引,部位分类,是否停用 from Pacs_检查部位   ";
            //        break;
            //}
            if (string.IsNullOrEmpty(comboBoxEdit1.Text))
            {
                Sql = "select ID,部位编码,部位名称,部位索引,部位分类,是否停用 from Pacs_检查部位 ";
            }
            else
            {
                Sql = "select ID,部位编码,部位名称,部位索引,部位分类,是否停用 from Pacs_检查部位 where 部位分类='" + comboBoxEdit1.Text + "' ";
            }
            try
            {
                dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, Sql).Tables[0];
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.gridView1.Columns.Clear();
            this.gridControl1.DataSource = dt;
            this.gridView1.BestFitColumns();
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridView1.OptionsSelection.MultiSelect = true;//设置多行选择
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect;
        }

        private void BindType()
        {
            string Sql = "select distinct 部位分类 from Pacs_检查部位  ";
            try
            {
                dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, Sql).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    //DataTable dttype = dt.DefaultView.ToTable(true, "部位分类");
                    foreach (DataRow dr in dt.Rows)
                    {
                        this.comboBoxEdit1.Properties.Items.Add(dr["部位分类"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        string GetSelectedRows(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            string ret = "0"; 
            try
            {
                if (view.OptionsSelection.MultiSelectMode == DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect)
                {
                    foreach (int i in gridView1.GetSelectedRows())
                    {
                        DataRow row = gridView1.GetDataRow(i); 
                        //if (ret != "")  ret += "\r\n";
                        ret += string.Format(",{0}", row[0]);
                    }
                }               
            }
            catch (Exception)
            {
                return "";
            }
            return ret;
        } 

        #endregion

        private void comboBoxEdit1_TextChanged(object sender, EventArgs e)
        {
            BindList();
        }


    }
}
