﻿using DevExpress.Utils.Win;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Popup;
using DevExpress.XtraGrid.Editors;
using DevExpress.XtraLayout;
using DevExpress.XtraReports.UI;
using HIS.Model;
using HIS.Model.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WEISHENG.COMM;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs申请录入3 : DevExpress.XtraEditors.XtraForm
    {
        //HIS.COMM.Person住院病人 zyperson;
        //HIS.COMM.Person门诊病人 mzperson;
        private Pacs_项目对照 model项目对照 = new Pacs_项目对照();
        public Pacs_检查申请 model检查申请摘要 = new Pacs_检查申请();
        public List<Pacs_检查申请明细> list检查申请明细 = new List<Pacs_检查申请明细>();
        //String _s申请单号 = "";
        public string _医嘱内容 = "", _胶片编码;
        public int _胶片张数, _收费数量;





        //部位内容存储变量
        //private List<LookUpMultSelectValues> list选定部位 = new List<LookUpMultSelectValues>();
        //选择部位时，动态显示选中个数
        //public LabelControl label已选择 = new LabelControl() { Text = "已选择 0 个部位" };
        //部位选中的数据去重



        //弹出窗体,并动态添加控件（确认按钮，取消按钮，选中结果显示标签）
        //窗体弹出事件
        private void sleEstate_Popup(object sender, EventArgs e)
        {
            //得到当前SearchLookUpEdit弹出窗体
            PopupSearchLookUpEditForm form = (sender as IPopupControl).PopupWindow as PopupSearchLookUpEditForm;
            SearchEditLookUpPopup popup = form.Controls.OfType<SearchEditLookUpPopup>().FirstOrDefault();
            LayoutControl layout = popup.Controls.OfType<LayoutControl>().FirstOrDefault();
            //如果窗体内空间没有确认按钮，则自定义确认simplebutton，取消simplebutton，选中结果label
            if (layout.Controls.OfType<Control>().Where(ct => ct.Name == "btOK").FirstOrDefault() == null)
            {
                //得到空的空间
                EmptySpaceItem a = layout.Items.Where(it => it.TypeName == "EmptySpaceItem").FirstOrDefault() as EmptySpaceItem;

                //得到取消按钮，重写点击事件
                Control clearBtn = layout.Controls.OfType<Control>().Where(ct => ct.Name == "btClear").FirstOrDefault();
                LayoutControlItem clearLCI = (LayoutControlItem)layout.GetItemByControl(clearBtn);
                clearBtn.Click += btn_clear;

                //添加一个simplebutton控件(确认按钮)
                LayoutControlItem myLCI = (LayoutControlItem)clearLCI.Owner.CreateLayoutItem(clearLCI.Parent);
                myLCI.TextVisible = false;
                SimpleButton btOK = new SimpleButton() { Name = "btOK", Text = "确定" };
                btOK.Click += btOK_Click;
                myLCI.Control = btOK;
                myLCI.SizeConstraintsType = SizeConstraintsType.Custom;//控件的大小设置为自定义
                myLCI.MaxSize = clearLCI.MaxSize;
                myLCI.MinSize = clearLCI.MinSize;
                myLCI.Move(clearLCI, DevExpress.XtraLayout.Utils.InsertType.Left);

                //添加一个label控件（选中结果显示）
                //LayoutControlItem msgLCI = (LayoutControlItem)clearLCI.Owner.CreateLayoutItem(a.Parent);
                //msgLCI.TextVisible = false;
                //msgLCI.Control = label已选择;
                //msgLCI.Move(a, DevExpress.XtraLayout.Utils.InsertType.Left);
                //msgLCI.BestFitWeight = 100;
            }
            Pacs_项目对照 curr_项目对照 = searchLookUpEdit项目对照.Properties.View.GetFocusedRow() as Pacs_项目对照;


            //根据设备类型，动态过滤部位
            var item身体部位 = dataHelper.chis.yunRis_身体部位.Where(c => c.部位编码 == curr_项目对照.部位编码).FirstOrDefault();
            if (item身体部位 != null)
            {
                searchLookUpEdit身体部位.Properties.View.Columns["设备类型"].FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
                searchLookUpEdit身体部位.Properties.View.ActiveFilterString = "设备类型 = '" + item身体部位.设备类型 + "'";
                searchLookUpEdit身体部位.Properties.View.BestFitColumns();
                //searchLookUpEdit身体部位.Properties.View.ActiveFilterEnabled = true;
                //searchLookUpEdit身体部位.Properties.View.ApplyColumnsFilter();
            }
        }
        //部位清除按钮事件
        private void btn_clear(object sender, EventArgs e)
        {
            list检查申请明细.Clear();//将保存的数据清空
            searchLookUpEdit身体部位.EditValue = null;
            //label已选择.Text = "已选择 0 个部位";
        }
        //部位确定按钮事件
        private void btOK_Click(object sender, EventArgs e)
        {
            searchLookUpEdit身体部位.ClosePopup();
        }

        //按钮点击
        private void sleEstate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //清除
            if (e.Button.Kind == ButtonPredefines.Clear)
            {
                list检查申请明细.Clear();
                searchLookUpEdit身体部位.EditValue = null;
                //label已选择.Text = "已选择 0 个部位";
            }
        }


        private void searchLookUpEdit身体部位_EditValueChanged(object sender, EventArgs e)
        {


        }

        //关闭

        private void sleEstate_Closed(object sender, ClosedEventArgs e)
        {
            //list检查申请明细.Clear();
            var i选择行号es = searchLookUpEdit身体部位.Properties.View.GetSelectedRows();
            foreach (var item in i选择行号es)
            {
                var model = searchLookUpEdit身体部位.Properties.View.GetRow(item) as yunRis_身体部位;
                var item申请明细 = new Pacs_检查申请明细();
                item申请明细.检查部位编码 = model.部位编码;
                item申请明细.检查部位名称 = model.部位名称;
                item申请明细.申请单号 = model检查申请摘要.申请单号;
                list检查申请明细.Add(item申请明细);
            }
            searchLookUpEdit身体部位.EditValue = string.Join(",", list检查申请明细.Distinct().Select(c => c.检查部位编码).ToArray());
            searchLookUpEdit身体部位.ToolTip = string.Join(",", list检查申请明细.Distinct().Select(c => c.检查部位名称).ToArray());

        }

        //自定义显示内容
        private void sleEstate_CustomDisplayText(object sender, CustomDisplayTextEventArgs e)
        {
            e.DisplayText = string.Join(",", list检查申请明细.Select(c => c.检查部位名称).Distinct().ToArray());
        }



        /// <summary>
        /// 住院
        /// </summary>
        /// <param name="dtPACS对照"></param>
        /// <param name="s申请单号"></param>
        /// <param name="_person"></param>
        public XtraFormPacs申请录入3(HIS.COMM.Person住院病人 _person)
        {
            InitializeComponent();
            //zyperson = _person;
            //_s申请单号 = Pacs申请.Get申请单号New("1");//Pacs申请.s住院新申请单据号();
            //layoutControlGroup检查申请.Text = string.Format("检查申请单☆{0}", _s申请单号);
            //textEdit诊断.Text = zyperson.s疾病名称;
        }

        /// <summary>
        /// 申请单
        /// </summary>
        /// <param name="检查申请对照">初始化实体类</param>
        /// <param name="sTypes">申请单号开头类型</param>
        /// <param name="_b打印申请单">是否打印申请单</param>
        public XtraFormPacs申请录入3(Pacs_检查申请 _model检查申请摘要, string Types, bool _b打印申请单)
        {
            InitializeComponent();
            model检查申请摘要 = _model检查申请摘要;
            model检查申请摘要.申请单号 = Pacs申请.Get申请单号New(Types);
            layoutControlGroup检查申请.Text = string.Format("检查申请单☆{0}", model检查申请摘要.申请单号);
            textEdit诊断.Text = _model检查申请摘要.临床诊断名称;
            this.ch打印申请单.Checked = _b打印申请单;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XtraFormPacs申请录入_Load(object sender, EventArgs e)
        {

            try
            {
                IList<Pacs_项目对照> models项目对照 = dataHelper.chis.Pacs_项目对照.ToList();
                searchLookUpEdit项目对照.Properties.DataSource = models项目对照;
                searchLookUpEdit项目对照.Properties.DisplayMember = "收费名称";
                searchLookUpEdit项目对照.Properties.ValueMember = "ID";


                List<yunRis_身体部位> models身体部位 = dataHelper.chis.Database.SqlQuery<yunRis_身体部位>(@"select * from yunRis_身体部位 where IID not in (select 父ID from yunRis_身体部位) and 父ID<>'0'").ToList();

                searchLookUpEdit身体部位.Properties.DataSource = models身体部位;
                //searchLookUpEdit身体部位.Properties.PopulateViewColumns();
                //searchLookUpEdit身体部位.Properties.DisplayMember = "部位名称";
                //searchLookUpEdit身体部位.Properties.ValueMember = "部位编码";
                searchLookUpEdit身体部位.Properties.View.Columns["部位名称"].Width = 150;
                //searchLookUpEdit检查部位.Properties.View.Columns["设备类型"].Visible = true;
                //searchLookUpEdit检查部位.Properties.View.Columns["部位编码"].Visible = true;
                //searchLookUpEdit检查部位.Properties.View.Columns["部位名称_ENG"].Visible = true;
                //searchLookUpEdit身体部位.Properties.PopupFormSize = new System.Drawing.Size(700, 300);

                IList<Pacs_执行科室> list执行科室 = dataHelper.chis.Pacs_执行科室.Where(c => c.分院编码 == WEISHENG.COMM.zdInfo.Model站点信息.分院编码).ToList();
                searchLookUpEdit执行科室.Properties.DataSource = list执行科室;
                searchLookUpEdit执行科室.Properties.DisplayMember = "执行科室名称";
                searchLookUpEdit执行科室.Properties.ValueMember = "执行科室编码";

                //searchLookUpEdit身体部位.DataBindings.Add("EditValue", model项目对照, "部位编码");
                textEdit医嘱名称.DataBindings.Add("EditValue", model项目对照, "医嘱名称");
                txt曝光数量.DataBindings.Add("EditValue", model项目对照, "收费数量");
                txt胶片数量.DataBindings.Add("EditValue", model项目对照, "胶片张数");
                searchLookUpEdit执行科室.DataBindings.Add("EditValue", model项目对照, "执行科室编码");

                //dt收费项目 = HIS.COMM.Class录入提示.dt费用录入提示(HIS.COMM.ClassCflx.enCflx.检查治疗单, "");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            Pacs_项目对照 selected_model项目对照 = searchLookUpEdit项目对照.Properties.View.GetFocusedRow() as Pacs_项目对照;
            try
            {
                if (list检查申请明细.Count == 0)
                {
                    MessageBox.Show("请选择检查部位", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (searchLookUpEdit执行科室.Text == "")
                {
                    MessageBox.Show("请选择执行科室", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (string.IsNullOrEmpty(txt曝光数量.Text) && Convert.ToInt16(txt曝光数量.Text) <= 0)
                {
                    MessageBox.Show("数量不能为空必须大于0！关系到计费！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //string _str = searchLookUpEdit身体部位.Text;
                //string _str2 = searchLookUpEdit身体部位.EditValue.ToString();
                //if (zyperson != null && !string.IsNullOrEmpty(zyperson.sZYID))
                //{
                //    model检查申请摘要.病人ID = zyperson.sZYID;
                //    model检查申请摘要.病人姓名 = zyperson.S姓名;
                //    model检查申请摘要.病人年龄 = Convert.ToInt16(zyperson.S年龄);
                //    model检查申请摘要.性别 = zyperson.S性别;
                //    //modelSq.住院号 = zyperson.s住院号码;
                //    model检查申请摘要.门诊号 = "-1";
                //    model检查申请摘要.申请医生 = zyperson.s主治医生编码;
                //    model检查申请摘要.床号 = zyperson.S床位名称;
                //    model检查申请摘要.病人来源 = "住院";
                //}
                //else if (mzperson != null && !string.IsNullOrEmpty(mzperson.sMZID))
                //{
                //    model检查申请摘要.病人ID = mzperson.sMZID;
                //    model检查申请摘要.病人姓名 = mzperson.S姓名;
                //    model检查申请摘要.病人年龄 = Convert.ToInt16(mzperson.S年龄);
                //    model检查申请摘要.性别 = mzperson.S性别;
                //    model检查申请摘要.住院号 = "-1";
                //    //modelSq.门诊号 = mzperson.sMZID;
                //    model检查申请摘要.申请医生 = mzperson.S医生编码;
                //    model检查申请摘要.床号 = "-1";
                //    model检查申请摘要.病人来源 = "门诊";
                //}
                //以下是通用部分


                model检查申请摘要.检查部位名称 = stringHelper.mySplit(searchLookUpEdit身体部位.Text, ',', 0); //实现摘要表中保存主要部位，
                model检查申请摘要.检查部位编码 = stringHelper.mySplit(searchLookUpEdit身体部位.EditValue.ToString(), ',', 0);
                model检查申请摘要.收费名称 = selected_model项目对照.收费名称.ToString();//searchLookUpEdit项目对照.Text;
                model检查申请摘要.收费编码 = selected_model项目对照.收费编码.ToString();//searchLookUpEdit项目对照.EditValue.ToString();
                model检查申请摘要.体征描述 = textEdit主诉.Text.Trim();
                model检查申请摘要.检查目的 = textEdit查体目的.Text.Trim();
                model检查申请摘要.执行科室名称 = searchLookUpEdit执行科室.Text;
                model检查申请摘要.临床诊断名称 = textEdit诊断.Text;

                _医嘱内容 = textEdit医嘱名称.Text.ToString().Trim();
                _胶片编码 = model项目对照.胶片编码;
                _胶片张数 = Convert.ToInt16(model项目对照.胶片张数);
                _收费数量 = Convert.ToInt16(model项目对照.收费数量);

                if (ch打印申请单.Checked)
                {
                    //创建申请单打印
                    XtraReport影像申请单 report = new XtraReport影像申请单(model检查申请摘要);
                    //report.CreateDocument();
                    ////添加到打印页母板
                    //report1.Pages.AddRange(report.Pages);
                    ReportPrintTool pt1 = new ReportPrintTool(report);
                    pt1.ShowPreviewDialog();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.Close();
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton诊断_Click(object sender, EventArgs e)
        {
            HIS.COMM.XtraFormICD10 icdForm = new HIS.COMM.XtraFormICD10();
            if (icdForm.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            textEdit诊断.Text = icdForm.selected_icd_info.ICD10Name;
            textEdit诊断.Tag = icdForm.selected_icd_info.ICD10;
        }


        private void simpleButton主诉_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from Pacs_字典 where 类别='主诉' and 是否禁用='否' ";
            XtraFormPacs_选择病人就诊资料 frm = new XtraFormPacs_选择病人就诊资料(sql, "名称", "病人主诉/体征描述");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit主诉.Text = frm.s选择项目;
        }

        private void simpleButton查体_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from Pacs_字典 where 类别='查体' and 是否禁用='否'  ";
            XtraFormPacs_选择病人就诊资料 frm = new XtraFormPacs_选择病人就诊资料(sql, "名称", "病人检查目的/查体");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit查体目的.Text = frm.s选择项目;
        }




        private void textEdit诊断_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            }
        }



        /// <summary>
        /// 因为没有扎到searchLookUpEdit项目对照和其他空间双向同步更新的办法，所以用实体类的Inotify接口实现同步显示。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchLookUpEdi项目对照_EditValueChanged(object sender, EventArgs e)
        {
            Pacs_项目对照 curr_项目对照 = searchLookUpEdit项目对照.Properties.View.GetFocusedRow() as Pacs_项目对照;
            modelHelper.copyInstanceValue<Pacs_项目对照>(curr_项目对照, model项目对照);
            try
            {
                Decimal dec曝光金额 = 0, dec胶片金额 = 0;
                if (!string.IsNullOrEmpty(curr_项目对照.收费编码.ToString()))
                {
                    var item收费小项 = dataHelper.chis.GY收费小项.Where(c => c.收费编码 == curr_项目对照.收费编码).FirstOrDefault();
                    dec曝光金额 += Convert.ToDecimal(item收费小项.单价) * Convert.ToDecimal(this.txt曝光数量.Text);
                }
                if (!string.IsNullOrEmpty(curr_项目对照.胶片编码))
                {
                    int _胶片编码 = Convert.ToInt32(curr_项目对照.胶片编码);
                    var item收费小项 = dataHelper.chis.GY收费小项.Where(c => c.收费编码 == _胶片编码).FirstOrDefault();
                    dec胶片金额 += Convert.ToDecimal(item收费小项.单价) * Convert.ToDecimal(this.txt胶片数量.Text);
                }
                this.txt金额合计.Text = "总金额：" + (dec曝光金额 + dec胶片金额).ToString() + " 其中（胶片费：" + dec胶片金额.ToString() + ")";
                model检查申请摘要.检查费 = (dec曝光金额 + dec胶片金额); //计算检查费，门诊处方和住院医嘱屏蔽计算     


                //用项目对照中的部位 给身体部位部位赋值
                list检查申请明细.Clear();//更换医嘱时，清空原料选择的部位
                var item申请明细 = new Pacs_检查申请明细();
                item申请明细.检查部位编码 = curr_项目对照.部位编码;
                item申请明细.检查部位名称 = curr_项目对照.部位名称;
                item申请明细.申请单号 = model检查申请摘要.申请单号;
                list检查申请明细.Add(item申请明细);

                searchLookUpEdit身体部位.EditValue = string.Join(",", list检查申请明细.Select(c => c.检查部位编码).Distinct().ToArray());
                searchLookUpEdit身体部位.ToolTip = string.Join(",", list检查申请明细.Select(c => c.检查部位名称).Distinct().ToArray());
            }
            catch (Exception ex)
            {
                this.txt金额合计.Text = "";
                msgHelper.ShowInformation(ex.Message);
            }

        }

    }


}

