﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFacePacs
{
    public partial class XtarFromPacs_身体部位维护 : DevExpress.XtraEditors.XtraForm
    {
        //WEISHENG.COMM.interFace.ModelMark modelMark = new WEISHENG.COMM.interFace.ModelMark()
        //{
        //    GUID = "110E8C55-262A-4D94-B993-4438F3D60D25",
        //    S父ID = "20000",
        //    S键ID = "20014"
        //};
        public XtarFromPacs_身体部位维护()
        {
            InitializeComponent();
        }

        Model.yunRis_身体部位Model model身体部位 = new Model.yunRis_身体部位Model();

        public List<Model.yunRis_身体部位Model> models身体部位 = null;

        private void XtarFromPacs_身体部位维护_Load(object sender, EventArgs e)
        {
            models身体部位 = new BLL.yunRis_身体部位BLL().GetyunRis_身体部位All("").ToList();//是否停用='否'
            this.treeList1.DataSource = models身体部位;
            this.treeList1.KeyFieldName = "IID";
            this.treeList1.ParentFieldName = "父ID";
            this.treeList1.Columns[0].Caption = "设备类型";//树的名称  - 


            this.txt设备类型.DataBindings.Add("Text", model身体部位, "设备类型");
            this.txt部位编码.DataBindings.Add("Text", model身体部位, "部位编码");
            this.txt部位名称.DataBindings.Add("Text", model身体部位, "部位名称");
            this.txt拼音简码.DataBindings.Add("Text", model身体部位, "部位名称_ENG");
            this.txt排序.DataBindings.Add("Text", model身体部位, "排序ID");
            textEditIID.DataBindings.Add("Text", model身体部位, "IID");
            textEdit父ID.DataBindings.Add("Text", model身体部位, "父ID");

        }

        private void treeList1_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            var tree = this.treeList1.FocusedNode;
            if (tree != null)
            {
                Model.yunRis_身体部位Model _model = treeList1.GetDataRecordByNode(treeList1.FocusedNode) as Model.yunRis_身体部位Model;
                HIS.Model.Helper.modelHelper.copyInstanceValue<Model.yunRis_身体部位Model>(_model, model身体部位);
            }
        }



        private bool Check()
        {
            if (string.IsNullOrEmpty(model身体部位.设备类型))
            {
                MessageBox.Show("设备类型不能为空！");
                this.txt设备类型.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(model身体部位.部位编码))
            {
                MessageBox.Show("部位编码不能为空！");
                this.txt部位编码.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(model身体部位.部位名称))
            {
                MessageBox.Show("部位名称不能为空！");
                this.txt部位名称.Focus();
                return false;
            }
            return true;
        }

        private void simpleButton保存_Click(object sender, EventArgs e)
        {
            if (!Check()) return;          
            if (new BLL.yunRis_身体部位BLL().IsExist(model身体部位.IID))
            {
                if (new BLL.yunRis_身体部位BLL().Update(model身体部位) > 0)
                    MessageBox.Show("修改成功！");
                Model.yunRis_身体部位Model _model = models身体部位.Find(j => j.IID == model身体部位.IID);
                HIS.Model.Helper.modelHelper.copyInstanceValue<Model.yunRis_身体部位Model>(model身体部位, _model);
            }
            else
            {
                if (new BLL.yunRis_身体部位BLL().Insert(model身体部位) > 0)
                {
                    MessageBox.Show("添加成功！");

                    models身体部位.Add(model身体部位);
                    this.simpleButton新增.Enabled = true;
                }
            }
            treeList1.RefreshDataSource();
        }

        private void simpleButton新增_Click(object sender, EventArgs e)
        {
            model身体部位.父ID = Convert.ToDecimal(this.treeList1.FocusedNode.GetValue("IID").ToString());
            model身体部位.IID = 0;//自动生成？
            //model.部位编码 = new BLL.yunRis_身体部位BLLExt().GetMaxID(this.txt设备类型.Text);
            //model.部位名称 = "";
            //model.部位名称_ENG = "";
            //model.排序ID = 0;
            model身体部位.部位编码 = new BLL.yunRis_身体部位BLLExt().GetMaxID(this.txt设备类型.Text);
            model身体部位.部位名称 = "";
            model身体部位.排序ID = 1;
            this.simpleButton新增.Enabled = false;
        }

        private void simpleButton删除_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要进行删除操作？", "提醒", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                if (MessageBox.Show("确定要删除 部位【" + model身体部位.部位名称 + "】？", "提醒", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (new BLL.yunRis_身体部位BLL().Delete(model身体部位.IID) > 0)
                    {
                        MessageBox.Show("删除成功！");
                        models身体部位.Remove(model身体部位);
                        treeList1.RefreshDataSource();
                    }
                }
            }
        }

        private void textEdit父ID_DoubleClick(object sender, EventArgs e)
        {
            textEditIID.Properties.ReadOnly = false;
            textEdit父ID.Properties.ReadOnly = false;
        }
    }
}
