﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using HIS.Model;

namespace HIS.InterFacePacs
{
    public partial class XtraReport影像申请单 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport影像申请单(Pacs_检查申请 model)
        {
            InitializeComponent();
            Binding(model);
        }

        void Binding(Pacs_检查申请 model检查申请摘要)
        {
            var model身体部位 = dataHelper.chis.yunRis_身体部位.Where(c => c.部位编码 == model检查申请摘要.检查部位编码).FirstOrDefault();
            if (model身体部位 != null)
                this.xrLabel标题.Text = $"{WEISHENG.COMM.zdInfo.Model单位信息.sDwmc}{model身体部位.设备类型}申请单";
            else
                this.xrLabel标题.Text = $"{WEISHENG.COMM.zdInfo.Model单位信息.sDwmc}";

            this.xrBarCode1.Text = model检查申请摘要.申请单号;
            this.xrLabel申请单号.Text = model检查申请摘要.申请单号;
            this.xrLabel姓名.Text = model检查申请摘要.病人姓名;
            this.xrLabel年龄.Text = model检查申请摘要.病人年龄.ToString();
            this.xrLabel性别.Text = model检查申请摘要.性别;
            this.xrLabel住院号.Text = model检查申请摘要.住院号 == "-1" ? model检查申请摘要.门诊号 : model检查申请摘要.住院号;
            this.xrLabel病历摘要.Text = model检查申请摘要.体征描述;
            this.xrLabel临床诊断.Text = model检查申请摘要.临床诊断名称;
            //todo:这个地方应该在界面变量中，不是在数据库里面
            var arr = dataHelper.chis.Pacs_检查申请明细.Where(c => c.申请单号 == model检查申请摘要.申请单号).Select(c => c.检查部位名称).ToArray();
            if (arr.Length > 0)
            {
                this.xrLabel检查部位.Text = string.Join(",", arr);
            }
            else
            {
                this.xrLabel检查部位.Text = model检查申请摘要.检查部位名称;
            }
            this.xrLabel检查目的.Text = model检查申请摘要.检查目的;
            this.xrLabel申请医生.Text = model检查申请摘要.申请医生;
            this.xrLabel金额.Text = model检查申请摘要.检查费.ToString();
            this.xrLabel检查项目.Text = model检查申请摘要.收费名称;
            this.xrLabel执行科室.Text = model检查申请摘要.执行科室名称;
            this.xrLabel申请时间.Text = Convert.ToDateTime(model检查申请摘要.申请日期).ToString("yyyy-MM-dd HH:mm");
            this.xrLabel申请科室.Text = model检查申请摘要.申请科室;
            this.xrLabel床位.Text = model检查申请摘要.床号;
            if (model检查申请摘要.床号 == "-1")
            {
                xrLabelTitle床位.Visible = false;
                xrLabel床位.Visible = false;
            }
        }

    }
}
