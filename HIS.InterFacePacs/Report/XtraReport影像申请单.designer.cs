﻿namespace HIS.InterFacePacs
{
    partial class XtraReport影像申请单
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.QRCodeGenerator qrCodeGenerator1 = new DevExpress.XtraPrinting.BarCode.QRCodeGenerator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel申请科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel床位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTitle床位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel住院号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel病历摘要 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel临床诊断 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel检查部位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel检查目的 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel申请医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel执行科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel检查项目 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel金额 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel申请时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel申请单号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel标题 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15,
            this.xrLabel申请科室,
            this.xrTable1,
            this.xrLabel14,
            this.xrLabel申请时间});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 1060.979F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.Dpi = 254F;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(1061.479F, 1002.559F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(175.9481F, 58.42004F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "申请科室:";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel申请科室
            // 
            this.xrLabel申请科室.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel申请科室.Dpi = 254F;
            this.xrLabel申请科室.LocationFloat = new DevExpress.Utils.PointFloat(1237.427F, 1002.559F);
            this.xrLabel申请科室.Name = "xrLabel申请科室";
            this.xrLabel申请科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel申请科室.SizeF = new System.Drawing.SizeF(255.3752F, 58.42004F);
            this.xrLabel申请科室.StylePriority.UseBorders = false;
            this.xrLabel申请科室.StylePriority.UseTextAlignment = false;
            this.xrLabel申请科室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(108.4792F, 21.16667F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1870.604F, 976.3125F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.41666658656803646D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel床位,
            this.xrLabelTitle床位,
            this.xrLabel住院号,
            this.xrLabel性别,
            this.xrLabel年龄,
            this.xrLabel姓名,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2});
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Weight = 3D;
            // 
            // xrLabel床位
            // 
            this.xrLabel床位.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel床位.Dpi = 254F;
            this.xrLabel床位.LocationFloat = new DevExpress.Utils.PointFloat(1621.896F, 22.41328F);
            this.xrLabel床位.Name = "xrLabel床位";
            this.xrLabel床位.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel床位.SizeF = new System.Drawing.SizeF(177.2715F, 58.42004F);
            this.xrLabel床位.StylePriority.UseBorders = false;
            this.xrLabel床位.StylePriority.UseTextAlignment = false;
            this.xrLabel床位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelTitle床位
            // 
            this.xrLabelTitle床位.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelTitle床位.Dpi = 254F;
            this.xrLabelTitle床位.LocationFloat = new DevExpress.Utils.PointFloat(1510.771F, 22.41329F);
            this.xrLabelTitle床位.Name = "xrLabelTitle床位";
            this.xrLabelTitle床位.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelTitle床位.SizeF = new System.Drawing.SizeF(111.125F, 58.42001F);
            this.xrLabelTitle床位.StylePriority.UseBorders = false;
            this.xrLabelTitle床位.StylePriority.UseTextAlignment = false;
            this.xrLabelTitle床位.Text = "床位:";
            this.xrLabelTitle床位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel住院号
            // 
            this.xrLabel住院号.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel住院号.Dpi = 254F;
            this.xrLabel住院号.LocationFloat = new DevExpress.Utils.PointFloat(1174.75F, 22.41329F);
            this.xrLabel住院号.Name = "xrLabel住院号";
            this.xrLabel住院号.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel住院号.SizeF = new System.Drawing.SizeF(336.0209F, 58.42F);
            this.xrLabel住院号.StylePriority.UseBorders = false;
            this.xrLabel住院号.StylePriority.UseTextAlignment = false;
            this.xrLabel住院号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel性别.Dpi = 254F;
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(772.5831F, 22.41328F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(132.2915F, 58.42001F);
            this.xrLabel性别.StylePriority.UseBorders = false;
            this.xrLabel性别.StylePriority.UseTextAlignment = false;
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel年龄
            // 
            this.xrLabel年龄.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel年龄.Dpi = 254F;
            this.xrLabel年龄.LocationFloat = new DevExpress.Utils.PointFloat(529.1666F, 22.41328F);
            this.xrLabel年龄.Name = "xrLabel年龄";
            this.xrLabel年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel年龄.SizeF = new System.Drawing.SizeF(132.2915F, 58.42001F);
            this.xrLabel年龄.StylePriority.UseBorders = false;
            this.xrLabel年龄.StylePriority.UseTextAlignment = false;
            this.xrLabel年龄.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel姓名.Dpi = 254F;
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(227.5417F, 22.41328F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(190.5F, 58.42001F);
            this.xrLabel姓名.StylePriority.UseBorders = false;
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(904.8746F, 22.41328F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(269.875F, 58.42F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "住院号/门诊号:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(661.4581F, 22.41328F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(111.125F, 58.42001F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "性别:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(418.0417F, 22.41328F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(111.125F, 58.42001F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "年龄:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(116.4167F, 22.41328F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(111.125F, 58.42001F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 2.2083334134319634D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel病历摘要,
            this.xrLabel临床诊断,
            this.xrLabel检查部位,
            this.xrLabel检查目的,
            this.xrLabel申请医生,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6});
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Weight = 3D;
            // 
            // xrLabel病历摘要
            // 
            this.xrLabel病历摘要.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel病历摘要.Dpi = 254F;
            this.xrLabel病历摘要.LocationFloat = new DevExpress.Utils.PointFloat(380.9999F, 40.93413F);
            this.xrLabel病历摘要.Name = "xrLabel病历摘要";
            this.xrLabel病历摘要.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel病历摘要.SizeF = new System.Drawing.SizeF(1087.438F, 196.0033F);
            this.xrLabel病历摘要.StylePriority.UseBorders = false;
            this.xrLabel病历摘要.StylePriority.UseTextAlignment = false;
            this.xrLabel病历摘要.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel临床诊断
            // 
            this.xrLabel临床诊断.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel临床诊断.Dpi = 254F;
            this.xrLabel临床诊断.LocationFloat = new DevExpress.Utils.PointFloat(380.9999F, 252.6007F);
            this.xrLabel临床诊断.Name = "xrLabel临床诊断";
            this.xrLabel临床诊断.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel临床诊断.SizeF = new System.Drawing.SizeF(1087.437F, 58.42004F);
            this.xrLabel临床诊断.StylePriority.UseBorders = false;
            this.xrLabel临床诊断.StylePriority.UseTextAlignment = false;
            this.xrLabel临床诊断.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel检查部位
            // 
            this.xrLabel检查部位.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel检查部位.Dpi = 254F;
            this.xrLabel检查部位.LocationFloat = new DevExpress.Utils.PointFloat(381.001F, 344.9932F);
            this.xrLabel检查部位.Name = "xrLabel检查部位";
            this.xrLabel检查部位.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel检查部位.SizeF = new System.Drawing.SizeF(1087.437F, 58.4201F);
            this.xrLabel检查部位.StylePriority.UseBorders = false;
            this.xrLabel检查部位.StylePriority.UseTextAlignment = false;
            this.xrLabel检查部位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel检查目的
            // 
            this.xrLabel检查目的.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel检查目的.Dpi = 254F;
            this.xrLabel检查目的.LocationFloat = new DevExpress.Utils.PointFloat(381F, 429.8717F);
            this.xrLabel检查目的.Name = "xrLabel检查目的";
            this.xrLabel检查目的.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel检查目的.SizeF = new System.Drawing.SizeF(1087.438F, 200.0913F);
            this.xrLabel检查目的.StylePriority.UseBorders = false;
            this.xrLabel检查目的.StylePriority.UseTextAlignment = false;
            this.xrLabel检查目的.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel申请医生
            // 
            this.xrLabel申请医生.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel申请医生.Dpi = 254F;
            this.xrLabel申请医生.LocationFloat = new DevExpress.Utils.PointFloat(1506.802F, 638F);
            this.xrLabel申请医生.Name = "xrLabel申请医生";
            this.xrLabel申请医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel申请医生.SizeF = new System.Drawing.SizeF(292.3649F, 58.41998F);
            this.xrLabel申请医生.StylePriority.UseBorders = false;
            this.xrLabel申请医生.StylePriority.UseTextAlignment = false;
            this.xrLabel申请医生.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(1389.062F, 638F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(117.7399F, 58.41998F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "医师:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(116.4167F, 429.8716F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(254F, 58.41998F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "检查目的:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(116.4167F, 344.9932F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(254F, 58.42F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "检查部位:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(116.4167F, 252.6007F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(254F, 58.42F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "临床诊断:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(116.4167F, 43.57995F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(254F, 58.42F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "病历摘要:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.375D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel执行科室,
            this.xrLabel13,
            this.xrLabel检查项目,
            this.xrLabel12,
            this.xrLabel金额,
            this.xrLabel11});
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Weight = 3D;
            // 
            // xrLabel执行科室
            // 
            this.xrLabel执行科室.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel执行科室.Dpi = 254F;
            this.xrLabel执行科室.LocationFloat = new DevExpress.Utils.PointFloat(1129F, 14.47585F);
            this.xrLabel执行科室.Name = "xrLabel执行科室";
            this.xrLabel执行科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel执行科室.SizeF = new System.Drawing.SizeF(255.3232F, 58.41998F);
            this.xrLabel执行科室.StylePriority.UseBorders = false;
            this.xrLabel执行科室.StylePriority.UseTextAlignment = false;
            this.xrLabel执行科室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(953F, 14.47585F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(175.948F, 58.41998F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "执行科室:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel检查项目
            // 
            this.xrLabel检查项目.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel检查项目.Dpi = 254F;
            this.xrLabel检查项目.LocationFloat = new DevExpress.Utils.PointFloat(365.1252F, 14.47586F);
            this.xrLabel检查项目.Name = "xrLabel检查项目";
            this.xrLabel检查项目.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel检查项目.SizeF = new System.Drawing.SizeF(572.8232F, 58.4201F);
            this.xrLabel检查项目.StylePriority.UseBorders = false;
            this.xrLabel检查项目.StylePriority.UseTextAlignment = false;
            this.xrLabel检查项目.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Dpi = 254F;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(116.4167F, 14.47586F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(248.7084F, 58.41998F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "检查项目:";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel金额
            // 
            this.xrLabel金额.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel金额.Dpi = 254F;
            this.xrLabel金额.LocationFloat = new DevExpress.Utils.PointFloat(1506.802F, 14.47585F);
            this.xrLabel金额.Name = "xrLabel金额";
            this.xrLabel金额.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel金额.SizeF = new System.Drawing.SizeF(292.3649F, 58.41998F);
            this.xrLabel金额.StylePriority.UseBorders = false;
            this.xrLabel金额.StylePriority.UseTextAlignment = false;
            this.xrLabel金额.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(1389.062F, 14.47585F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(117.7397F, 58.41998F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "金额:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(108.4792F, 1002.559F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(232.8333F, 58.42001F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "申请时间:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel申请时间
            // 
            this.xrLabel申请时间.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel申请时间.Dpi = 254F;
            this.xrLabel申请时间.LocationFloat = new DevExpress.Utils.PointFloat(346.6042F, 1002.559F);
            this.xrLabel申请时间.Name = "xrLabel申请时间";
            this.xrLabel申请时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel申请时间.SizeF = new System.Drawing.SizeF(629.7083F, 58.42001F);
            this.xrLabel申请时间.StylePriority.UseBorders = false;
            this.xrLabel申请时间.StylePriority.UseTextAlignment = false;
            this.xrLabel申请时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel申请单号,
            this.xrLabel标题,
            this.xrBarCode1});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 312.2083F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseTextAlignment = false;
            this.ReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(108.4792F, 228.7884F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(232.8333F, 58.42001F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "申请单号:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel申请单号
            // 
            this.xrLabel申请单号.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel申请单号.Dpi = 254F;
            this.xrLabel申请单号.LocationFloat = new DevExpress.Utils.PointFloat(346.6042F, 228.7883F);
            this.xrLabel申请单号.Name = "xrLabel申请单号";
            this.xrLabel申请单号.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel申请单号.SizeF = new System.Drawing.SizeF(629.7083F, 58.42001F);
            this.xrLabel申请单号.StylePriority.UseBorders = false;
            this.xrLabel申请单号.StylePriority.UseTextAlignment = false;
            this.xrLabel申请单号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel标题
            // 
            this.xrLabel标题.Dpi = 254F;
            this.xrLabel标题.Font = new System.Drawing.Font("Times New Roman", 14F);
            this.xrLabel标题.LocationFloat = new DevExpress.Utils.PointFloat(472.2812F, 97.89584F);
            this.xrLabel标题.Name = "xrLabel标题";
            this.xrLabel标题.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel标题.SizeF = new System.Drawing.SizeF(1143F, 95.25F);
            this.xrLabel标题.StylePriority.UseFont = false;
            this.xrLabel标题.StylePriority.UseTextAlignment = false;
            this.xrLabel标题.Text = "沂南攀峰医院{0}申请单";
            this.xrLabel标题.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrBarCode1
            // 
            this.xrBarCode1.AutoModule = true;
            this.xrBarCode1.Dpi = 254F;
            this.xrBarCode1.LocationFloat = new DevExpress.Utils.PointFloat(1676.906F, 109.25F);
            this.xrBarCode1.Module = 5.08F;
            this.xrBarCode1.Name = "xrBarCode1";
            this.xrBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 254F);
            this.xrBarCode1.ShowText = false;
            this.xrBarCode1.SizeF = new System.Drawing.SizeF(187.0834F, 189.7292F);
            this.xrBarCode1.StylePriority.UsePadding = false;
            qrCodeGenerator1.Version = DevExpress.XtraPrinting.BarCode.QRCodeVersion.Version1;
            this.xrBarCode1.Symbology = qrCodeGenerator1;
            this.xrBarCode1.Text = "123456789012";
            // 
            // XtraReport影像申请单
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.DataMember = "DataTable1";
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 1480;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A5;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 31.75F;
            this.Version = "18.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel标题;
        private DevExpress.XtraReports.UI.XRLabel xrLabel住院号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel病历摘要;
        private DevExpress.XtraReports.UI.XRLabel xrLabel临床诊断;
        private DevExpress.XtraReports.UI.XRLabel xrLabel检查部位;
        private DevExpress.XtraReports.UI.XRLabel xrLabel检查目的;
        private DevExpress.XtraReports.UI.XRLabel xrLabel申请医生;
        private DevExpress.XtraReports.UI.XRLabel xrLabel金额;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel申请单号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel检查项目;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel执行科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel申请时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel申请科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTitle床位;
        private DevExpress.XtraReports.UI.XRLabel xrLabel床位;
        //private ds检验申请单 ds检验申请单1;
    }
}
