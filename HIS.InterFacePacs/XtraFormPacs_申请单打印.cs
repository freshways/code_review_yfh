﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using HIS.Model;
using LinqKit;
using WEISHENG.COMM;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs_申请单打印 : DevExpress.XtraEditors.XtraForm
    {
        //WEISHENG.COMM.interFace.ModelMark modelMark = new WEISHENG.COMM.interFace.ModelMark()
        //{
        //    GUID = "9FA6DC38-67DE-4AB0-972C-66B4A71EB6A0",
        //    S父ID = "37",
        //    S键ID = "31443"
        //};
        public XtraFormPacs_申请单打印()
        {
            InitializeComponent();
        }

        private void XtraFormPacs_申请单打印_Load(object sender, EventArgs e)
        {
            this.textEdit开始时间.Text = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            this.textEdit截止时间.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.cmb病人来源.SelectedIndex = 0;
            Get病人病区();
            txt申请科室.Text = WEISHENG.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        private void Get病人病区()
        {
            DataTable dt = new DataTable();
            dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
                "select 科室名称,科室编码 from GY科室设置 where 是否住院科室=1 ").Tables[0];

            txt申请科室.Properties.Items.Clear();

            foreach (DataRow row in dt.Rows)
            {
                txt申请科室.Properties.Items.Add(row["科室名称"]);
            }
            //txt申请科室.Properties.DataSource = dt;
            //txt申请科室.Properties.DisplayMember = "科室名称";
            //txt申请科室.Properties.ValueMember = "科室编码";
            //txt申请科室.ItemIndex = 0;
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            try
            {


                var where = PredicateBuilder.New<Pacs_检查申请>();

                if (textEdit开始时间.Text != "")
                {
                    var dtt开始时间 = Convert.ToDateTime(textEdit开始时间.Text);
                    where = where.And(c => c.申请日期 >= dtt开始时间);
                }
                if (textEdit截止时间.Text != "")
                {
                    var dtt截止时间 = Convert.ToDateTime(textEdit截止时间.Text + " 23:59:59");
                    where = where.And(c => c.申请日期 <= dtt截止时间);
                }
                //if (cmb病人来源.Text != "全部")
                //{
                //    where = where.And(c => c.病人来源 == cmb病人来源.Text);
                //}
                var _申请科室 = txt申请科室.Text;
                where = where.And(c => c.申请科室 == _申请科室);
                if (cmb病人来源.Text == "门诊")
                    this.col住院号.FieldName = "门诊号";
                else if (cmb病人来源.Text == "住院")
                {
                    this.col住院号.FieldName = "住院号";
                    //sqlwhere += " and 申请单号 in(select pacs申请单号 from YS住院医嘱)";
                }
                dataHelper.chis.Database.Log = s => LogHelper.LogToFile("ef6调试", s);
                var list申请 = dataHelper.chis.Pacs_检查申请.AsExpandable().Where(where);
                this.gridControl1.DataSource = list申请.OrderBy(o => o.ID).ToList();
                this.gridView1.BestFitColumns();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void simpleButton打印_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0) return;

                Pacs_检查申请 model申请摘要 = gridView1.GetFocusedRow() as Pacs_检查申请;
                //创建一个母板,把其他的report添加进来 -- 批量打印
                XtraReport report1 = new XtraReport();
                report1.CreateDocument();//必须
                report1.PrintingSystem.ContinuousPageNumbering = true;//必须

                //获取住院病人床号
                if (model申请摘要.病人来源 == "住院" && model申请摘要.床号 == "-1")
                {
                    try
                    {
                        DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, "select 病床 from zy病人信息 where ZYID='" + model申请摘要.病人ID + "'").Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            model申请摘要.床号 = dt.Rows[0][0].ToString();
                        }
                    }
                    catch
                    {
                    }
                }
                //创建申请单打印
                XtraReport影像申请单 report = new XtraReport影像申请单(model申请摘要);
                report.CreateDocument();
                //添加到打印页母板
                report1.Pages.AddRange(report.Pages);
                //不知道为啥报错,引用了  DevExpress.XtraReports.UI; 也不行
                ReportPrintTool pt1 = new ReportPrintTool(report1);
                pt1.ShowPreviewDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("打印失败！\n" + ex.Message, "消息");
            }
        }
    }
}
