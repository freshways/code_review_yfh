﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs_申请项目维护添加 : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormPacs_申请项目维护添加()
        {
            InitializeComponent();
        }

        Model.Pacs_项目对照Model _项目对照 = null;
        public IList<Model.yunRis_身体部位Model> models检查部位 = null;
        public IList<Model.Pacs_执行科室Model> models执行科室 = null;
        public IList<Model.GY收费小项Model> models收费小项 = null;
        public XtraFormPacs_申请项目维护添加(Model.Pacs_项目对照Model modl)
        {
            InitializeComponent();
            _项目对照 = modl;
        }
        
        private void XtraFormPacs_申请项目维护添加_Load(object sender, EventArgs e)
        {

            if (models检查部位 != null)
            {//绑定检查部位
                searchLookUpEdit检查部位.Properties.DataSource = models检查部位;
                searchLookUpEdit检查部位.Properties.PopulateViewColumns();
                searchLookUpEdit检查部位.Properties.DisplayMember = "部位名称";
                searchLookUpEdit检查部位.Properties.ValueMember = "部位编码";
                searchLookUpEdit检查部位.Properties.View.Columns["部位名称"].Width = 150;
                //searchLookUpEdit检查部位.Properties.View.Columns["部位分类"].Visible = true;
                //searchLookUpEdit检查部位.Properties.View.Columns["部位编码"].Visible = true;
                //searchLookUpEdit检查部位.Properties.View.Columns["部位索引"].Visible = true;
                searchLookUpEdit检查部位.Properties.PopupFormSize = new System.Drawing.Size(700, 300);
            }
            if (models执行科室 != null)
            {//绑定执行科室
                searchLookUpEdit执行科室.Properties.DataSource = models执行科室;
                searchLookUpEdit执行科室.Properties.DisplayMember = "执行科室名称";
                searchLookUpEdit执行科室.Properties.ValueMember = "执行科室编码";
            }
            if(models收费小项!=null)
            {//绑定收费项目
                searchLookUpEdit项目对照.Properties.DataSource = models收费小项;
                searchLookUpEdit项目对照.Properties.DisplayMember = "收费名称";
                searchLookUpEdit项目对照.Properties.ValueMember = "收费编码";
            }

            if (_项目对照 == null) //初始化model 类
                _项目对照 = new Model.Pacs_项目对照Model();
            BindingTxt(_项目对照);
        }

        /// <summary>
        /// 绑定显示
        /// </summary>
        /// <param name="modl"></param>
        private void BindingTxt(Model.Pacs_项目对照Model modl)
        {
            this.textEdit医嘱名称.DataBindings.Add("Text", modl, "医嘱名称");
            this.txt数量.DataBindings.Add("Text", modl, "收费数量");
            this.txt胶片.DataBindings.Add("Text", modl, "胶片张数");
            this.textEdit设备.DataBindings.Add("Text", modl, "设备编码");
            this.searchLookUpEdit检查部位.EditValue = modl.部位编码;
            this.searchLookUpEdit执行科室.EditValue = modl.执行科室编码;
            this.searchLookUpEdit项目对照.EditValue = modl.收费编码;
            //this.searchLookUpEdit检查部位.DataBindings.Add("Text", modl, "部位编码");
            //this.searchLookUpEdit执行科室.DataBindings.Add(new Binding("Text", modl, "执行科室编码"));
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            _项目对照.医嘱名称 = this.textEdit医嘱名称.Text;
            _项目对照.部位编码 = this.searchLookUpEdit检查部位.EditValue.ToString();
            _项目对照.部位名称 = this.searchLookUpEdit检查部位.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
