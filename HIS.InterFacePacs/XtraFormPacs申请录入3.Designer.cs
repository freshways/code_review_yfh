﻿using DevExpress.XtraLayout;

namespace HIS.InterFacePacs
{
    partial class XtraFormPacs申请录入3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormPacs申请录入3));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup检查申请 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEdit主诉 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ch打印申请单 = new DevExpress.XtraEditors.CheckEdit();
            this.txt金额合计 = new DevExpress.XtraEditors.TextEdit();
            this.txt胶片数量 = new DevExpress.XtraEditors.TextEdit();
            this.txt曝光数量 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit医嘱名称 = new DevExpress.XtraEditors.TextEdit();
            this.searchLookUpEdit项目对照 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView医嘱名称 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn医嘱名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn收费名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn拼音代码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton查体 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton主诉 = new DevExpress.XtraEditors.SimpleButton();
            this.searchLookUpEdit执行科室 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton诊断 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit诊断 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton确定 = new DevExpress.XtraEditors.SimpleButton();
            this.searchLookUpEdit身体部位 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit身体部位View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col设备类型 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit查体目的 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup检查申请)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主诉.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch打印申请单.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt金额合计.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胶片数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt曝光数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit医嘱名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit项目对照.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView医嘱名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit执行科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit诊断.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit身体部位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit身体部位View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit查体目的.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup检查申请,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem17});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(613, 367);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup检查申请
            // 
            this.layoutControlGroup检查申请.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 11F);
            this.layoutControlGroup检查申请.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup检查申请.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup检查申请.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup检查申请.CustomizationFormText = "检查申请单";
            this.layoutControlGroup检查申请.GroupStyle = DevExpress.Utils.GroupStyle.Title;
            this.layoutControlGroup检查申请.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem11,
            this.layoutControlItem7,
            this.layoutControlItem12,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem14,
            this.layoutControlItem6,
            this.splitterItem1,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem3,
            this.layoutControlItem16,
            this.layoutControlItem10,
            this.layoutControlItem15,
            this.layoutControlItem5});
            this.layoutControlGroup检查申请.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup检查申请.Name = "layoutControlGroup检查申请";
            this.layoutControlGroup检查申请.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup检查申请.Size = new System.Drawing.Size(607, 335);
            this.layoutControlGroup检查申请.Text = "检查申请单";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem4.Control = this.textEdit主诉;
            this.layoutControlItem4.CustomizationFormText = "体征描述:";
            this.layoutControlItem4.Location = new System.Drawing.Point(122, 48);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 75);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(101, 75);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(441, 75);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "主诉(病历摘要体征描述)：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // textEdit主诉
            // 
            this.textEdit主诉.Location = new System.Drawing.Point(229, 84);
            this.textEdit主诉.Name = "textEdit主诉";
            this.textEdit主诉.Size = new System.Drawing.Size(342, 71);
            this.textEdit主诉.StyleController = this.layoutControl1;
            this.textEdit主诉.TabIndex = 7;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Controls.Add(this.comboBoxEdit1);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.ch打印申请单);
            this.layoutControl1.Controls.Add(this.txt金额合计);
            this.layoutControl1.Controls.Add(this.txt胶片数量);
            this.layoutControl1.Controls.Add(this.txt曝光数量);
            this.layoutControl1.Controls.Add(this.textEdit医嘱名称);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit项目对照);
            this.layoutControl1.Controls.Add(this.simpleButton查体);
            this.layoutControl1.Controls.Add(this.simpleButton主诉);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit执行科室);
            this.layoutControl1.Controls.Add(this.simpleButton诊断);
            this.layoutControl1.Controls.Add(this.textEdit诊断);
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.simpleButton确定);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit身体部位);
            this.layoutControl1.Controls.Add(this.textEdit主诉);
            this.layoutControl1.Controls.Add(this.textEdit查体目的);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(167, 578, 512, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(613, 367);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(75, 60);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(50, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 25;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(75, 36);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Size = new System.Drawing.Size(50, 20);
            this.comboBoxEdit1.StyleController = this.layoutControl1;
            this.comboBoxEdit1.TabIndex = 24;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(12, 84);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(113, 245);
            this.gridControl1.TabIndex = 23;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // ch打印申请单
            // 
            this.ch打印申请单.EditValue = true;
            this.ch打印申请单.Location = new System.Drawing.Point(5, 340);
            this.ch打印申请单.Name = "ch打印申请单";
            this.ch打印申请单.Properties.Caption = "打印申请单";
            this.ch打印申请单.Size = new System.Drawing.Size(196, 19);
            this.ch打印申请单.StyleController = this.layoutControl1;
            this.ch打印申请单.TabIndex = 22;
            // 
            // txt金额合计
            // 
            this.txt金额合计.Location = new System.Drawing.Point(292, 309);
            this.txt金额合计.Name = "txt金额合计";
            this.txt金额合计.Properties.ReadOnly = true;
            this.txt金额合计.Size = new System.Drawing.Size(309, 20);
            this.txt金额合计.StyleController = this.layoutControl1;
            this.txt金额合计.TabIndex = 20;
            // 
            // txt胶片数量
            // 
            this.txt胶片数量.Location = new System.Drawing.Point(197, 284);
            this.txt胶片数量.Name = "txt胶片数量";
            this.txt胶片数量.Size = new System.Drawing.Size(172, 20);
            this.txt胶片数量.StyleController = this.layoutControl1;
            this.txt胶片数量.TabIndex = 21;
            // 
            // txt曝光数量
            // 
            this.txt曝光数量.Location = new System.Drawing.Point(436, 284);
            this.txt曝光数量.Name = "txt曝光数量";
            this.txt曝光数量.Size = new System.Drawing.Size(165, 20);
            this.txt曝光数量.StyleController = this.layoutControl1;
            this.txt曝光数量.TabIndex = 2;
            // 
            // textEdit医嘱名称
            // 
            this.textEdit医嘱名称.Location = new System.Drawing.Point(197, 60);
            this.textEdit医嘱名称.Name = "textEdit医嘱名称";
            this.textEdit医嘱名称.Size = new System.Drawing.Size(404, 20);
            this.textEdit医嘱名称.StyleController = this.layoutControl1;
            this.textEdit医嘱名称.TabIndex = 19;
            // 
            // searchLookUpEdit项目对照
            // 
            this.searchLookUpEdit项目对照.EditValue = "请选择...";
            this.searchLookUpEdit项目对照.Location = new System.Drawing.Point(197, 36);
            this.searchLookUpEdit项目对照.Name = "searchLookUpEdit项目对照";
            this.searchLookUpEdit项目对照.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit项目对照.Properties.DisplayMember = "部位名称";
            this.searchLookUpEdit项目对照.Properties.NullText = "请选择部位...";
            this.searchLookUpEdit项目对照.Properties.PopupView = this.gridView医嘱名称;
            this.searchLookUpEdit项目对照.Properties.ValueMember = "部位编码";
            this.searchLookUpEdit项目对照.Size = new System.Drawing.Size(404, 20);
            this.searchLookUpEdit项目对照.StyleController = this.layoutControl1;
            this.searchLookUpEdit项目对照.TabIndex = 20;
            this.searchLookUpEdit项目对照.EditValueChanged += new System.EventHandler(this.searchLookUpEdi项目对照_EditValueChanged);
            // 
            // gridView医嘱名称
            // 
            this.gridView医嘱名称.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn医嘱名称,
            this.gridColumn收费名称,
            this.gridColumn拼音代码,
            this.gridColumn4});
            this.gridView医嘱名称.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView医嘱名称.Name = "gridView医嘱名称";
            this.gridView医嘱名称.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView医嘱名称.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn医嘱名称
            // 
            this.gridColumn医嘱名称.Caption = "医嘱名称";
            this.gridColumn医嘱名称.FieldName = "医嘱名称";
            this.gridColumn医嘱名称.Name = "gridColumn医嘱名称";
            this.gridColumn医嘱名称.Visible = true;
            this.gridColumn医嘱名称.VisibleIndex = 0;
            // 
            // gridColumn收费名称
            // 
            this.gridColumn收费名称.Caption = "收费名称";
            this.gridColumn收费名称.FieldName = "收费名称";
            this.gridColumn收费名称.Name = "gridColumn收费名称";
            this.gridColumn收费名称.Visible = true;
            this.gridColumn收费名称.VisibleIndex = 1;
            // 
            // gridColumn拼音代码
            // 
            this.gridColumn拼音代码.Caption = "拼音代码";
            this.gridColumn拼音代码.FieldName = "医嘱名称拼音代码";
            this.gridColumn拼音代码.Name = "gridColumn拼音代码";
            this.gridColumn拼音代码.Visible = true;
            this.gridColumn拼音代码.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "部位名称";
            this.gridColumn4.FieldName = "部位名称";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // simpleButton查体
            // 
            this.simpleButton查体.Location = new System.Drawing.Point(575, 159);
            this.simpleButton查体.Name = "simpleButton查体";
            this.simpleButton查体.Size = new System.Drawing.Size(26, 71);
            this.simpleButton查体.StyleController = this.layoutControl1;
            this.simpleButton查体.TabIndex = 17;
            this.simpleButton查体.Text = "...";
            this.simpleButton查体.Click += new System.EventHandler(this.simpleButton查体_Click);
            // 
            // simpleButton主诉
            // 
            this.simpleButton主诉.Location = new System.Drawing.Point(575, 84);
            this.simpleButton主诉.Name = "simpleButton主诉";
            this.simpleButton主诉.Size = new System.Drawing.Size(26, 71);
            this.simpleButton主诉.StyleController = this.layoutControl1;
            this.simpleButton主诉.TabIndex = 16;
            this.simpleButton主诉.Text = "...";
            this.simpleButton主诉.Click += new System.EventHandler(this.simpleButton主诉_Click);
            // 
            // searchLookUpEdit执行科室
            // 
            this.searchLookUpEdit执行科室.EditValue = "";
            this.searchLookUpEdit执行科室.Location = new System.Drawing.Point(197, 309);
            this.searchLookUpEdit执行科室.Name = "searchLookUpEdit执行科室";
            this.searchLookUpEdit执行科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit执行科室.Properties.NullText = "";
            this.searchLookUpEdit执行科室.Properties.PopupView = this.gridView1;
            this.searchLookUpEdit执行科室.Properties.ReadOnly = true;
            this.searchLookUpEdit执行科室.Size = new System.Drawing.Size(91, 20);
            this.searchLookUpEdit执行科室.StyleController = this.layoutControl1;
            this.searchLookUpEdit执行科室.TabIndex = 13;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButton诊断
            // 
            this.simpleButton诊断.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton诊断.ImageOptions.Image")));
            this.simpleButton诊断.Location = new System.Drawing.Point(575, 234);
            this.simpleButton诊断.Name = "simpleButton诊断";
            this.simpleButton诊断.Size = new System.Drawing.Size(26, 22);
            this.simpleButton诊断.StyleController = this.layoutControl1;
            this.simpleButton诊断.TabIndex = 12;
            this.simpleButton诊断.Click += new System.EventHandler(this.simpleButton诊断_Click);
            // 
            // textEdit诊断
            // 
            this.textEdit诊断.Location = new System.Drawing.Point(229, 234);
            this.textEdit诊断.Name = "textEdit诊断";
            this.textEdit诊断.Size = new System.Drawing.Size(342, 20);
            this.textEdit诊断.StyleController = this.layoutControl1;
            this.textEdit诊断.TabIndex = 11;
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton取消.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton取消.ImageOptions.Image")));
            this.simpleButton取消.Location = new System.Drawing.Point(547, 340);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(61, 22);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 5;
            this.simpleButton取消.Text = "取消";
            this.simpleButton取消.Click += new System.EventHandler(this.simpleButton取消_Click);
            // 
            // simpleButton确定
            // 
            this.simpleButton确定.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton确定.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton确定.ImageOptions.Image")));
            this.simpleButton确定.Location = new System.Drawing.Point(482, 340);
            this.simpleButton确定.Name = "simpleButton确定";
            this.simpleButton确定.Size = new System.Drawing.Size(61, 22);
            this.simpleButton确定.StyleController = this.layoutControl1;
            this.simpleButton确定.TabIndex = 4;
            this.simpleButton确定.Text = "确定";
            this.simpleButton确定.Click += new System.EventHandler(this.simpleButton确定_Click);
            // 
            // searchLookUpEdit身体部位
            // 
            this.searchLookUpEdit身体部位.EditValue = "请选择...";
            this.searchLookUpEdit身体部位.Location = new System.Drawing.Point(229, 260);
            this.searchLookUpEdit身体部位.Name = "searchLookUpEdit身体部位";
            this.searchLookUpEdit身体部位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit身体部位.Properties.DisplayMember = "部位名称";
            this.searchLookUpEdit身体部位.Properties.NullText = "请选择部位...";
            this.searchLookUpEdit身体部位.Properties.PopupView = this.searchLookUpEdit身体部位View;
            this.searchLookUpEdit身体部位.Properties.ValueMember = "部位编码";
            this.searchLookUpEdit身体部位.Size = new System.Drawing.Size(372, 20);
            this.searchLookUpEdit身体部位.StyleController = this.layoutControl1;
            this.searchLookUpEdit身体部位.TabIndex = 6;
            this.searchLookUpEdit身体部位.Popup += new System.EventHandler(this.sleEstate_Popup);
            this.searchLookUpEdit身体部位.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.sleEstate_Closed);
            this.searchLookUpEdit身体部位.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.sleEstate_ButtonClick);
            this.searchLookUpEdit身体部位.EditValueChanged += new System.EventHandler(this.searchLookUpEdit身体部位_EditValueChanged);
            this.searchLookUpEdit身体部位.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.sleEstate_CustomDisplayText);
            // 
            // searchLookUpEdit身体部位View
            // 
            this.searchLookUpEdit身体部位View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col设备类型,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.searchLookUpEdit身体部位View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit身体部位View.Name = "searchLookUpEdit身体部位View";
            this.searchLookUpEdit身体部位View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit身体部位View.OptionsSelection.MultiSelect = true;
            this.searchLookUpEdit身体部位View.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.searchLookUpEdit身体部位View.OptionsView.ShowGroupPanel = false;
            // 
            // col设备类型
            // 
            this.col设备类型.Caption = "设备类型";
            this.col设备类型.FieldName = "设备类型";
            this.col设备类型.Name = "col设备类型";
            this.col设备类型.Visible = true;
            this.col设备类型.VisibleIndex = 4;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "部位编码";
            this.gridColumn1.FieldName = "部位编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "部位名称";
            this.gridColumn2.FieldName = "部位名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "简称";
            this.gridColumn3.FieldName = "部位名称_ENG";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            // 
            // textEdit查体目的
            // 
            this.textEdit查体目的.Location = new System.Drawing.Point(229, 159);
            this.textEdit查体目的.Name = "textEdit查体目的";
            this.textEdit查体目的.Size = new System.Drawing.Size(342, 71);
            this.textEdit查体目的.StyleController = this.layoutControl1;
            this.textEdit查体目的.TabIndex = 10;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton主诉;
            this.layoutControlItem11.CustomizationFormText = "...";
            this.layoutControlItem11.Location = new System.Drawing.Point(563, 48);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(30, 75);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "...";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit查体目的;
            this.layoutControlItem7.CustomizationFormText = "查体";
            this.layoutControlItem7.Location = new System.Drawing.Point(122, 123);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(109, 20);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(441, 75);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "检查目的：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.simpleButton查体;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(563, 123);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(30, 75);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit诊断;
            this.layoutControlItem8.CustomizationFormText = "诊断";
            this.layoutControlItem8.Location = new System.Drawing.Point(122, 198);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(441, 26);
            this.layoutControlItem8.Text = "临床诊断：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton诊断;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(563, 198);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(30, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.searchLookUpEdit项目对照;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(122, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(471, 24);
            this.layoutControlItem14.Text = "检查项目：";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit医嘱名称;
            this.layoutControlItem6.CustomizationFormText = "医嘱名称:";
            this.layoutControlItem6.Location = new System.Drawing.Point(122, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(471, 24);
            this.layoutControlItem6.Text = "医嘱名称:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 14);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(117, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 297);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.gridControl1;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(117, 249);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.comboBoxEdit1;
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(117, 24);
            this.layoutControlItem19.Text = "申请类型：";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit1;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(117, 24);
            this.layoutControlItem20.Text = "拼音代码:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.searchLookUpEdit身体部位;
            this.layoutControlItem3.CustomizationFormText = "新组号:";
            this.layoutControlItem3.Location = new System.Drawing.Point(122, 224);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(471, 24);
            this.layoutControlItem3.Text = "检查部位：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt金额合计;
            this.layoutControlItem16.CustomizationFormText = "金  额:";
            this.layoutControlItem16.Location = new System.Drawing.Point(280, 273);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(313, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "金额:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.searchLookUpEdit执行科室;
            this.layoutControlItem10.CustomizationFormText = "执行科室：";
            this.layoutControlItem10.Location = new System.Drawing.Point(122, 273);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(158, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "执行科室：";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.txt胶片数量;
            this.layoutControlItem15.CustomizationFormText = "胶片";
            this.layoutControlItem15.Location = new System.Drawing.Point(122, 248);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(239, 25);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "胶片:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt曝光数量;
            this.layoutControlItem5.CustomizationFormText = "数量";
            this.layoutControlItem5.Location = new System.Drawing.Point(361, 248);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(232, 25);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "曝光数量:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(200, 335);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(277, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton确定;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(477, 335);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(65, 26);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(65, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton取消;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(542, 335);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(65, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(65, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.ch打印申请单;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 335);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(86, 23);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // XtraFormPacs申请录入3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 367);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraFormPacs申请录入3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pacs申请录入";
            this.Load += new System.EventHandler(this.XtraFormPacs申请录入_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup检查申请)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主诉.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch打印申请单.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt金额合计.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胶片数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt曝光数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit医嘱名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit项目对照.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView医嘱名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit执行科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit诊断.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit身体部位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit身体部位View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit查体目的.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton simpleButton确定;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit身体部位;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit身体部位View;
        private DevExpress.XtraEditors.MemoEdit textEdit主诉;
        private DevExpress.XtraEditors.TextEdit textEdit诊断;
        private DevExpress.XtraEditors.MemoEdit textEdit查体目的;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton simpleButton诊断;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit执行科室;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton simpleButton查体;
        private DevExpress.XtraEditors.SimpleButton simpleButton主诉;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup检查申请;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit项目对照;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView医嘱名称;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn医嘱名称;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn收费名称;
        private DevExpress.XtraEditors.TextEdit textEdit医嘱名称;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn拼音代码;
        private DevExpress.XtraEditors.TextEdit txt胶片数量;
        private DevExpress.XtraEditors.TextEdit txt曝光数量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit txt金额合计;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn col设备类型;
        private DevExpress.XtraEditors.CheckEdit ch打印申请单;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private SplitterItem splitterItem1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private LayoutControlItem layoutControlItem18;
        private LayoutControlItem layoutControlItem19;
        private LayoutControlItem layoutControlItem20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
    }
}