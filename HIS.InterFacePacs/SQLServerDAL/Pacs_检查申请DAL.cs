﻿// litao@Copy Right 2006-2016
// 文件： Pacs_检查申请.cs
// 创建时间：2018-04-21
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.Pacs_检查申请.
    /// </summary>
    public partial class Pacs_检查申请DAL: IPacs_检查申请
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM Pacs_检查申请 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM Pacs_检查申请 WHERE ID=@ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM Pacs_检查申请 WHERE ID=@ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM Pacs_检查申请 WHERE ID=@ID";
		private static readonly string SQL_INSERTPacs_检查申请 = "INSERT INTO Pacs_检查申请 (病人ID,申请单号,病人来源,病人姓名,性别,病人年龄,检查部位名称,申请日期,申请科室,申请医生,检查费,住院号,门诊号,床号,电话号码,类型,临床诊断名称,体征描述,状态,Pacs检查ID,检查医生,检查所见,检查提示,检查结果,执行科室名称,医嘱ID,检查目的,身份证号,分院编码,是否怀孕,孕周,孕天,家庭地址,检查部位编码,单位编码,收费编码,收费名称) VALUES (@病人ID,@申请单号,@病人来源,@病人姓名,@性别,@病人年龄,@检查部位名称,@申请日期,@申请科室,@申请医生,@检查费,@住院号,@门诊号,@床号,@电话号码,@类型,@临床诊断名称,@体征描述,@状态,@Pacs检查ID,@检查医生,@检查所见,@检查提示,@检查结果,@执行科室名称,@医嘱ID,@检查目的,@身份证号,@分院编码,@是否怀孕,@孕周,@孕天,@家庭地址,@检查部位编码,@单位编码,@收费编码,@收费名称)";
		private static readonly string SQL_UPDATE_Pacs_检查申请_BY_PR = "UPDATE Pacs_检查申请 SET 病人ID=@病人ID,申请单号=@申请单号,病人来源=@病人来源,病人姓名=@病人姓名,性别=@性别,病人年龄=@病人年龄,检查部位名称=@检查部位名称,申请日期=@申请日期,申请科室=@申请科室,申请医生=@申请医生,检查费=@检查费,住院号=@住院号,门诊号=@门诊号,床号=@床号,电话号码=@电话号码,类型=@类型,临床诊断名称=@临床诊断名称,体征描述=@体征描述,状态=@状态,Pacs检查ID=@Pacs检查ID,检查时间=@检查时间,检查医生=@检查医生,检查所见=@检查所见,检查提示=@检查提示,检查结果=@检查结果,执行科室名称=@执行科室名称,医嘱ID=@医嘱ID,检查目的=@检查目的,身份证号=@身份证号,分院编码=@分院编码,末次月经=@末次月经,是否怀孕=@是否怀孕,孕周=@孕周,孕天=@孕天,家庭地址=@家庭地址,检查部位编码=@检查部位编码,单位编码=@单位编码,收费编码=@收费编码,收费名称=@收费名称 WHERE ID=@ID";
		private static readonly string PARM_PRM_Pacs_检查申请 = "@ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public Pacs_检查申请DAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="pacs_检查申请Model">Pacs_检查申请实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_检查申请Model pacs_检查申请Model)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value = pacs_检查申请Model.ID;
			_param[1].Value = pacs_检查申请Model.病人ID;
			_param[2].Value = pacs_检查申请Model.申请单号;
			_param[3].Value = pacs_检查申请Model.病人来源;
			_param[4].Value = pacs_检查申请Model.病人姓名;
			_param[5].Value = pacs_检查申请Model.性别;
			_param[6].Value = pacs_检查申请Model.病人年龄;
			_param[7].Value = pacs_检查申请Model.检查部位名称;
			_param[8].Value = pacs_检查申请Model.申请日期;
			_param[9].Value = pacs_检查申请Model.申请科室;
			_param[10].Value = pacs_检查申请Model.申请医生;
			_param[11].Value = pacs_检查申请Model.检查费;
			_param[12].Value = pacs_检查申请Model.住院号;
			_param[13].Value = pacs_检查申请Model.门诊号;
			_param[14].Value = pacs_检查申请Model.床号;
			_param[15].Value = pacs_检查申请Model.电话号码;
			_param[16].Value = pacs_检查申请Model.类型;
			_param[17].Value = pacs_检查申请Model.临床诊断名称;
			_param[18].Value = pacs_检查申请Model.体征描述;
			_param[19].Value = pacs_检查申请Model.状态;
			_param[20].Value = pacs_检查申请Model.Pacs检查ID;
			_param[21].Value = pacs_检查申请Model.检查时间;
			_param[22].Value = pacs_检查申请Model.检查医生;
			_param[23].Value = pacs_检查申请Model.检查所见;
			_param[24].Value = pacs_检查申请Model.检查提示;
			_param[25].Value = pacs_检查申请Model.检查结果;
			_param[26].Value = pacs_检查申请Model.执行科室名称;
			_param[27].Value = pacs_检查申请Model.医嘱ID;
			_param[28].Value = pacs_检查申请Model.检查目的;
			_param[29].Value = pacs_检查申请Model.身份证号;
			_param[30].Value = pacs_检查申请Model.分院编码;
			_param[31].Value = pacs_检查申请Model.末次月经;
			_param[32].Value = pacs_检查申请Model.是否怀孕;
			_param[33].Value = pacs_检查申请Model.孕周;
			_param[34].Value = pacs_检查申请Model.孕天;
			_param[35].Value = pacs_检查申请Model.家庭地址;
			_param[36].Value = pacs_检查申请Model.检查部位编码;
			_param[37].Value = pacs_检查申请Model.单位编码;
			_param[38].Value = pacs_检查申请Model.收费编码;
			_param[39].Value = pacs_检查申请Model.收费名称;
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_INSERTPacs_检查申请,_param);
			
		}
		/// <summary>
		/// 向数据表Pacs_检查申请更新一条记录。

		/// </summary>
		/// <param name="pacs_检查申请Model">pacs_检查申请Model</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_检查申请Model pacs_检查申请Model)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=pacs_检查申请Model.ID;
			_param[1].Value=pacs_检查申请Model.病人ID;
			_param[2].Value=pacs_检查申请Model.申请单号;
			_param[3].Value=pacs_检查申请Model.病人来源;
			_param[4].Value=pacs_检查申请Model.病人姓名;
			_param[5].Value=pacs_检查申请Model.性别;
			_param[6].Value=pacs_检查申请Model.病人年龄;
			_param[7].Value=pacs_检查申请Model.检查部位名称;
			_param[8].Value=pacs_检查申请Model.申请日期;
			_param[9].Value=pacs_检查申请Model.申请科室;
			_param[10].Value=pacs_检查申请Model.申请医生;
			_param[11].Value=pacs_检查申请Model.检查费;
			_param[12].Value=pacs_检查申请Model.住院号;
			_param[13].Value=pacs_检查申请Model.门诊号;
			_param[14].Value=pacs_检查申请Model.床号;
			_param[15].Value=pacs_检查申请Model.电话号码;
			_param[16].Value=pacs_检查申请Model.类型;
			_param[17].Value=pacs_检查申请Model.临床诊断名称;
			_param[18].Value=pacs_检查申请Model.体征描述;
			_param[19].Value=pacs_检查申请Model.状态;
			_param[20].Value=pacs_检查申请Model.Pacs检查ID;
			_param[21].Value=pacs_检查申请Model.检查时间;
			_param[22].Value=pacs_检查申请Model.检查医生;
			_param[23].Value=pacs_检查申请Model.检查所见;
			_param[24].Value=pacs_检查申请Model.检查提示;
			_param[25].Value=pacs_检查申请Model.检查结果;
			_param[26].Value=pacs_检查申请Model.执行科室名称;
			_param[27].Value=pacs_检查申请Model.医嘱ID;
			_param[28].Value=pacs_检查申请Model.检查目的;
			_param[29].Value=pacs_检查申请Model.身份证号;
			_param[30].Value=pacs_检查申请Model.分院编码;
			_param[31].Value=pacs_检查申请Model.末次月经;
			_param[32].Value=pacs_检查申请Model.是否怀孕;
			_param[33].Value=pacs_检查申请Model.孕周;
			_param[34].Value=pacs_检查申请Model.孕天;
			_param[35].Value=pacs_检查申请Model.家庭地址;
			_param[36].Value=pacs_检查申请Model.检查部位编码;
			_param[37].Value=pacs_检查申请Model.单位编码;
			_param[38].Value=pacs_检查申请Model.收费编码;
			_param[39].Value=pacs_检查申请Model.收费名称;
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_UPDATE_Pacs_检查申请_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表Pacs_检查申请中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  pacs_检查申请 数据实体
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>pacs_检查申请 数据实体</returns>
		private Pacs_检查申请Model GetModelFromDr(DataRow row)
		{
			Pacs_检查申请Model Obj = new Pacs_检查申请Model();
			if(row!=null)
			{
				Obj.ID        = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.病人ID      =  row["病人ID"].ToString();
				Obj.申请单号      =  row["申请单号"].ToString();
				Obj.病人来源      =  row["病人来源"].ToString();
				Obj.病人姓名      =  row["病人姓名"].ToString();
				Obj.性别        =  row["性别"].ToString();
				Obj.病人年龄      = (( row["病人年龄"])==DBNull.Value)?0:Convert.ToInt32( row["病人年龄"]);
				Obj.检查部位名称    =  row["检查部位名称"].ToString();
				Obj.申请日期      = (( row["申请日期"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["申请日期"]);
				Obj.申请科室      =  row["申请科室"].ToString();
				Obj.申请医生      =  row["申请医生"].ToString();
				Obj.检查费       = (( row["检查费"])==DBNull.Value)?0:Convert.ToDecimal( row["检查费"]);
				Obj.住院号       =  row["住院号"].ToString();
				Obj.门诊号       =  row["门诊号"].ToString();
				Obj.床号        =  row["床号"].ToString();
				Obj.电话号码      =  row["电话号码"].ToString();
				Obj.类型        =  row["类型"].ToString();
				Obj.临床诊断名称    =  row["临床诊断名称"].ToString();
				Obj.体征描述      =  row["体征描述"].ToString();
				Obj.状态        = (( row["状态"])==DBNull.Value)?0:Convert.ToInt32( row["状态"]);
				Obj.Pacs检查ID  =  row["Pacs检查ID"].ToString();
				Obj.检查时间      = (( row["检查时间"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["检查时间"]);
				Obj.检查医生      =  row["检查医生"].ToString();
				Obj.检查所见      =  row["检查所见"].ToString();
				Obj.检查提示      =  row["检查提示"].ToString();
				Obj.检查结果      =  row["检查结果"].ToString();
				Obj.执行科室名称    =  row["执行科室名称"].ToString();
				Obj.医嘱ID      = (( row["医嘱ID"])==DBNull.Value)?0:Convert.ToInt32( row["医嘱ID"]);
				Obj.检查目的      =  row["检查目的"].ToString();
				Obj.身份证号      =  row["身份证号"].ToString();
				Obj.分院编码      = (( row["分院编码"])==DBNull.Value)?0:Convert.ToInt32( row["分院编码"]);
				Obj.末次月经      = (( row["末次月经"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["末次月经"]);
				Obj.是否怀孕      =  row["是否怀孕"].ToString();
				Obj.孕周        =  row["孕周"].ToString();
				Obj.孕天        =  row["孕天"].ToString();
				Obj.家庭地址      =  row["家庭地址"].ToString();
				Obj.检查部位编码    =  row["检查部位编码"].ToString();
				Obj.单位编码      =  row["单位编码"].ToString();
				Obj.收费编码      =  row["收费编码"].ToString();
				Obj.收费名称      =  row["收费名称"].ToString();
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  pacs_检查申请 数据实体
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>pacs_检查申请 数据实体</returns>
		private Pacs_检查申请Model GetModelFromDr(IDataReader dr)
		{
			Pacs_检查申请Model Obj = new Pacs_检查申请Model();
			
				Obj.ID       = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.病人ID     =  dr["病人ID"].ToString();
				Obj.申请单号     =  dr["申请单号"].ToString();
				Obj.病人来源     =  dr["病人来源"].ToString();
				Obj.病人姓名     =  dr["病人姓名"].ToString();
				Obj.性别       =  dr["性别"].ToString();
				Obj.病人年龄     = (( dr["病人年龄"])==DBNull.Value)?0:Convert.ToInt32( dr["病人年龄"]);
				Obj.检查部位名称   =  dr["检查部位名称"].ToString();
				Obj.申请日期     = (( dr["申请日期"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["申请日期"]);
				Obj.申请科室     =  dr["申请科室"].ToString();
				Obj.申请医生     =  dr["申请医生"].ToString();
				Obj.检查费      = (( dr["检查费"])==DBNull.Value)?0:Convert.ToDecimal( dr["检查费"]);
				Obj.住院号      =  dr["住院号"].ToString();
				Obj.门诊号      =  dr["门诊号"].ToString();
				Obj.床号       =  dr["床号"].ToString();
				Obj.电话号码     =  dr["电话号码"].ToString();
				Obj.类型       =  dr["类型"].ToString();
				Obj.临床诊断名称   =  dr["临床诊断名称"].ToString();
				Obj.体征描述     =  dr["体征描述"].ToString();
				Obj.状态       = (( dr["状态"])==DBNull.Value)?0:Convert.ToInt32( dr["状态"]);
				Obj.Pacs检查ID =  dr["Pacs检查ID"].ToString();
				Obj.检查时间     = (( dr["检查时间"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["检查时间"]);
				Obj.检查医生     =  dr["检查医生"].ToString();
				Obj.检查所见     =  dr["检查所见"].ToString();
				Obj.检查提示     =  dr["检查提示"].ToString();
				Obj.检查结果     =  dr["检查结果"].ToString();
				Obj.执行科室名称   =  dr["执行科室名称"].ToString();
				Obj.医嘱ID     = (( dr["医嘱ID"])==DBNull.Value)?0:Convert.ToInt32( dr["医嘱ID"]);
				Obj.检查目的     =  dr["检查目的"].ToString();
				Obj.身份证号     =  dr["身份证号"].ToString();
				Obj.分院编码     = (( dr["分院编码"])==DBNull.Value)?0:Convert.ToInt32( dr["分院编码"]);
				Obj.末次月经     = (( dr["末次月经"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["末次月经"]);
				Obj.是否怀孕     =  dr["是否怀孕"].ToString();
				Obj.孕周       =  dr["孕周"].ToString();
				Obj.孕天       =  dr["孕天"].ToString();
				Obj.家庭地址     =  dr["家庭地址"].ToString();
				Obj.检查部位编码   =  dr["检查部位编码"].ToString();
				Obj.单位编码     =  dr["单位编码"].ToString();
				Obj.收费编码     =  dr["收费编码"].ToString();
				Obj.收费名称     =  dr["收费名称"].ToString();
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个Pacs_检查申请对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>Pacs_检查申请对象</returns>
		public Pacs_检查申请Model GetPacs_检查申请 (int id)
		{
			Pacs_检查申请Model _obj=null;			
			SqlParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表Pacs_检查申请所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_检查申请Model> GetPacs_检查申请All()
		{			
			return GetPacs_检查申请All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表Pacs_检查申请所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_检查申请Model> GetPacs_检查申请All(string sqlWhere)
		{
			IList<Pacs_检查申请Model> list=new List<Pacs_检查申请Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere;//.Replace("'","''");
			}
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			SqlParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(HIS.COMM.DbHelperSqlServer.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetPRMParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(PARM_PRM_Pacs_检查申请);

            if (parms == null) {
                parms = new SqlParameter[] {						
					new SqlParameter("@ID",SqlDbType.Int)
				};
                HIS.COMM.DbHelperSqlServer.CacheParameters(PARM_PRM_Pacs_检查申请, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetInsertParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_INSERTPacs_检查申请);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@病人ID",SqlDbType.VarChar),
					new SqlParameter("@申请单号",SqlDbType.VarChar),
					new SqlParameter("@病人来源",SqlDbType.VarChar),
					new SqlParameter("@病人姓名",SqlDbType.VarChar),
					new SqlParameter("@性别",SqlDbType.VarChar),
					new SqlParameter("@病人年龄",SqlDbType.Int),
					new SqlParameter("@检查部位名称",SqlDbType.VarChar),
					new SqlParameter("@申请日期",SqlDbType.DateTime),
					new SqlParameter("@申请科室",SqlDbType.VarChar),
					new SqlParameter("@申请医生",SqlDbType.VarChar),
					new SqlParameter("@检查费",SqlDbType.Decimal),
					new SqlParameter("@住院号",SqlDbType.VarChar),
					new SqlParameter("@门诊号",SqlDbType.VarChar),
					new SqlParameter("@床号",SqlDbType.VarChar),
					new SqlParameter("@电话号码",SqlDbType.VarChar),
					new SqlParameter("@类型",SqlDbType.VarChar),
					new SqlParameter("@临床诊断名称",SqlDbType.VarChar),
					new SqlParameter("@体征描述",SqlDbType.VarChar),
					new SqlParameter("@状态",SqlDbType.Int),
					new SqlParameter("@Pacs检查ID",SqlDbType.VarChar),
					new SqlParameter("@检查时间",SqlDbType.DateTime),
					new SqlParameter("@检查医生",SqlDbType.VarChar),
					new SqlParameter("@检查所见",SqlDbType.VarChar),
					new SqlParameter("@检查提示",SqlDbType.VarChar),
					new SqlParameter("@检查结果",SqlDbType.VarChar),
					new SqlParameter("@执行科室名称",SqlDbType.VarChar),
					new SqlParameter("@医嘱ID",SqlDbType.Int),
					new SqlParameter("@检查目的",SqlDbType.VarChar),
					new SqlParameter("@身份证号",SqlDbType.VarChar),
					new SqlParameter("@分院编码",SqlDbType.Int),
					new SqlParameter("@末次月经",SqlDbType.DateTime),
					new SqlParameter("@是否怀孕",SqlDbType.VarChar),
					new SqlParameter("@孕周",SqlDbType.VarChar),
					new SqlParameter("@孕天",SqlDbType.VarChar),
					new SqlParameter("@家庭地址",SqlDbType.VarChar),
					new SqlParameter("@检查部位编码",SqlDbType.VarChar),
					new SqlParameter("@单位编码",SqlDbType.VarChar),
					new SqlParameter("@收费编码",SqlDbType.VarChar),
					new SqlParameter("@收费名称",SqlDbType.VarChar)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_INSERTPacs_检查申请, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetUpdateParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_UPDATE_Pacs_检查申请_BY_PR);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@病人ID",SqlDbType.VarChar),
					new SqlParameter("@申请单号",SqlDbType.VarChar),
					new SqlParameter("@病人来源",SqlDbType.VarChar),
					new SqlParameter("@病人姓名",SqlDbType.VarChar),
					new SqlParameter("@性别",SqlDbType.VarChar),
					new SqlParameter("@病人年龄",SqlDbType.Int),
					new SqlParameter("@检查部位名称",SqlDbType.VarChar),
					new SqlParameter("@申请日期",SqlDbType.DateTime),
					new SqlParameter("@申请科室",SqlDbType.VarChar),
					new SqlParameter("@申请医生",SqlDbType.VarChar),
					new SqlParameter("@检查费",SqlDbType.Decimal),
					new SqlParameter("@住院号",SqlDbType.VarChar),
					new SqlParameter("@门诊号",SqlDbType.VarChar),
					new SqlParameter("@床号",SqlDbType.VarChar),
					new SqlParameter("@电话号码",SqlDbType.VarChar),
					new SqlParameter("@类型",SqlDbType.VarChar),
					new SqlParameter("@临床诊断名称",SqlDbType.VarChar),
					new SqlParameter("@体征描述",SqlDbType.VarChar),
					new SqlParameter("@状态",SqlDbType.Int),
					new SqlParameter("@Pacs检查ID",SqlDbType.VarChar),
					new SqlParameter("@检查时间",SqlDbType.DateTime),
					new SqlParameter("@检查医生",SqlDbType.VarChar),
					new SqlParameter("@检查所见",SqlDbType.VarChar),
					new SqlParameter("@检查提示",SqlDbType.VarChar),
					new SqlParameter("@检查结果",SqlDbType.VarChar),
					new SqlParameter("@执行科室名称",SqlDbType.VarChar),
					new SqlParameter("@医嘱ID",SqlDbType.Int),
					new SqlParameter("@检查目的",SqlDbType.VarChar),
					new SqlParameter("@身份证号",SqlDbType.VarChar),
					new SqlParameter("@分院编码",SqlDbType.Int),
					new SqlParameter("@末次月经",SqlDbType.DateTime),
					new SqlParameter("@是否怀孕",SqlDbType.VarChar),
					new SqlParameter("@孕周",SqlDbType.VarChar),
					new SqlParameter("@孕天",SqlDbType.VarChar),
					new SqlParameter("@家庭地址",SqlDbType.VarChar),
					new SqlParameter("@检查部位编码",SqlDbType.VarChar),
					new SqlParameter("@单位编码",SqlDbType.VarChar),
					new SqlParameter("@收费编码",SqlDbType.VarChar),
					new SqlParameter("@收费名称",SqlDbType.VarChar)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_UPDATE_Pacs_检查申请_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

