﻿// litao@Copy Right 2006-2016
// 文件： yunRis_身体部位.cs
// 创建时间：2018-07-15
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.yunRis_身体部位.
    /// </summary>
    public partial class yunRis_身体部位DAL: IyunRis_身体部位
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM yunRis_身体部位 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM yunRis_身体部位 WHERE IID=@IID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM yunRis_身体部位 WHERE IID=@IID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM yunRis_身体部位 WHERE IID=@IID";
		private static readonly string SQL_INSERTyunRis_身体部位 = "INSERT INTO yunRis_身体部位 (设备类型,部位编码,部位名称,部位名称_ENG,HIS部位编码,HIS部位名称,CDEPT,CAE_TITLE,排序ID,备注,父ID,价格,IBGCS,默认部位,医院编码,createTime) VALUES (@设备类型,@部位编码,@部位名称,@部位名称_ENG,@HIS部位编码,@HIS部位名称,@CDEPT,@CAE_TITLE,@排序ID,@备注,@父ID,@价格,@IBGCS,@默认部位,@医院编码,@createTime)";
		private static readonly string SQL_UPDATE_yunRis_身体部位_BY_PR = "UPDATE yunRis_身体部位 SET 设备类型=@设备类型,部位编码=@部位编码,部位名称=@部位名称,部位名称_ENG=@部位名称_ENG,HIS部位编码=@HIS部位编码,HIS部位名称=@HIS部位名称,CDEPT=@CDEPT,CAE_TITLE=@CAE_TITLE,排序ID=@排序ID,备注=@备注,父ID=@父ID,价格=@价格,IBGCS=@IBGCS,默认部位=@默认部位,医院编码=@医院编码 WHERE IID=@IID";
		private static readonly string PARM_PRM_yunRis_身体部位 = "@IID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public yunRis_身体部位DAL(){
         HIS.COMM.DbHelperSqlServer.clearParmCache();
        }
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="yunris_身体部位Model">yunRis_身体部位实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(yunRis_身体部位Model yunris_身体部位Model)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.IID);
			_param[1].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.设备类型);
			_param[2].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.部位编码);
			_param[3].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.部位名称);
			_param[4].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.部位名称_ENG);
			_param[5].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.HIS部位编码);
			_param[6].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.HIS部位名称);
			_param[7].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.CDEPT);
			_param[8].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.CAE_TITLE);
			_param[9].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.排序ID);
			_param[10].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.备注);
			_param[11].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.父ID);
			_param[12].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.价格);
			_param[13].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.IBGCS);
			_param[14].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.默认部位);
			_param[15].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.医院编码);
			_param[16].Value = WEISHENG.COMM.nullHelper.NullToDbNull( yunris_身体部位Model.createTime);
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_INSERTyunRis_身体部位,_param);
			
		}
        
        public int Insert(yunRis_身体部位Model yunris_身体部位Model,SqlTransaction connT)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.IID);
			_param[1].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.设备类型);
			_param[2].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位编码);
			_param[3].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位名称);
			_param[4].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位名称_ENG);
			_param[5].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.HIS部位编码);
			_param[6].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.HIS部位名称);
			_param[7].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.CDEPT);
			_param[8].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.CAE_TITLE);
			_param[9].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.排序ID);
			_param[10].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.备注);
			_param[11].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.父ID);
			_param[12].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.价格);
			_param[13].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.IBGCS);
			_param[14].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.默认部位);
			_param[15].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.医院编码);
			_param[16].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.createTime);
			
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_INSERTyunRis_身体部位,_param);
		}
        
		/// <summary>
		/// 向数据表yunRis_身体部位更新一条记录。

		/// </summary>
		/// <param name="yunris_身体部位Model">yunris_身体部位Model</param>
		/// <returns>影响的行数</returns>
		public int Update(yunRis_身体部位Model yunris_身体部位Model)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.IID);
			_param[1].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.设备类型);
			_param[2].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位编码);
			_param[3].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位名称);
			_param[4].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位名称_ENG);
			_param[5].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.HIS部位编码);
			_param[6].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.HIS部位名称);
			_param[7].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.CDEPT);
			_param[8].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.CAE_TITLE);
			_param[9].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.排序ID);
			_param[10].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.备注);
			_param[11].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.父ID);
			_param[12].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.价格);
			_param[13].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.IBGCS);
			_param[14].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.默认部位);
			_param[15].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.医院编码);
			_param[16].Value= WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.createTime);
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_UPDATE_yunRis_身体部位_BY_PR,_param);
		}
        
        public int Update(yunRis_身体部位Model yunris_身体部位Model,SqlTransaction connT)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.IID);
			_param[1].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.设备类型);
			_param[2].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位编码);
			_param[3].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位名称);
			_param[4].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.部位名称_ENG);
			_param[5].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.HIS部位编码);
			_param[6].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.HIS部位名称);
			_param[7].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.CDEPT);
			_param[8].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.CAE_TITLE);
			_param[9].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.排序ID);
			_param[10].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.备注);
			_param[11].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.父ID);
			_param[12].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.价格);
			_param[13].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.IBGCS);
			_param[14].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.默认部位);
			_param[15].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.医院编码);
			_param[16].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(yunris_身体部位Model.createTime);
			
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_UPDATE_yunRis_身体部位_BY_PR,_param);
		}
        
		/// <summary>
		/// 删除数据表yunRis_身体部位中的一条记录

		/// </summary>
	    /// <param name="IID">iid</param>
		/// <returns>影响的行数</returns>
		public int Delete(decimal iid)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=iid;
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
        public int Delete(decimal iid,SqlTransaction connT)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=iid;
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  yunris_身体部位 数据实体
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>yunris_身体部位 数据实体</returns>
		private yunRis_身体部位Model GetModelFromDr(DataRow row)
		{
			yunRis_身体部位Model Obj = new yunRis_身体部位Model();
			if(row!=null)
			{
				Obj.IID         = (( row["IID"])==DBNull.Value)?0:Convert.ToDecimal( row["IID"]);
				Obj.设备类型        =  row["设备类型"].ToString();
				Obj.部位编码        =  row["部位编码"].ToString();
				Obj.部位名称        =  row["部位名称"].ToString();
				Obj.部位名称_ENG    =  row["部位名称_ENG"].ToString();
				Obj.HIS部位编码     =  row["HIS部位编码"].ToString();
				Obj.HIS部位名称     =  row["HIS部位名称"].ToString();
				Obj.CDEPT       =  row["CDEPT"].ToString();
				Obj.CAE_TITLE   =  row["CAE_TITLE"].ToString();
				Obj.排序ID        = (( row["排序ID"])==DBNull.Value)?0:Convert.ToDecimal( row["排序ID"]);
				Obj.备注          =  row["备注"].ToString();
				Obj.父ID         = (( row["父ID"])==DBNull.Value)?0:Convert.ToDecimal( row["父ID"]);
				Obj.价格          = (( row["价格"])==DBNull.Value)?0:Convert.ToDecimal( row["价格"]);
				Obj.IBGCS       = (( row["IBGCS"])==DBNull.Value)?0:Convert.ToDecimal( row["IBGCS"]);
				Obj.默认部位        =  row["默认部位"].ToString();
				Obj.医院编码        =  row["医院编码"].ToString();
				Obj.createTime  = (( row["createTime"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["createTime"]);
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  yunris_身体部位 数据实体
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>yunris_身体部位 数据实体</returns>
		private yunRis_身体部位Model GetModelFromDr(IDataReader dr)
		{
			yunRis_身体部位Model Obj = new yunRis_身体部位Model();
			
				Obj.IID        = (( dr["IID"])==DBNull.Value)?0:Convert.ToDecimal( dr["IID"]);
				Obj.设备类型       =  dr["设备类型"].ToString();
				Obj.部位编码       =  dr["部位编码"].ToString();
				Obj.部位名称       =  dr["部位名称"].ToString();
				Obj.部位名称_ENG   =  dr["部位名称_ENG"].ToString();
				Obj.HIS部位编码    =  dr["HIS部位编码"].ToString();
				Obj.HIS部位名称    =  dr["HIS部位名称"].ToString();
				Obj.CDEPT      =  dr["CDEPT"].ToString();
				Obj.CAE_TITLE  =  dr["CAE_TITLE"].ToString();
				Obj.排序ID       = (( dr["排序ID"])==DBNull.Value)?0:Convert.ToDecimal( dr["排序ID"]);
				Obj.备注         =  dr["备注"].ToString();
				Obj.父ID        = (( dr["父ID"])==DBNull.Value)?0:Convert.ToDecimal( dr["父ID"]);
				Obj.价格         = (( dr["价格"])==DBNull.Value)?0:Convert.ToDecimal( dr["价格"]);
				Obj.IBGCS      = (( dr["IBGCS"])==DBNull.Value)?0:Convert.ToDecimal( dr["IBGCS"]);
				Obj.默认部位       =  dr["默认部位"].ToString();
				Obj.医院编码       =  dr["医院编码"].ToString();
				Obj.createTime = (( dr["createTime"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["createTime"]);
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个yunRis_身体部位对象
		/// </summary>
		/// <param name="iid">iid</param>
		/// <returns>yunRis_身体部位对象</returns>
		public yunRis_身体部位Model GetyunRis_身体部位 (decimal iid)
		{
			yunRis_身体部位Model _obj=null;			
			SqlParameter[] _param = GetPRMParameters();			
			_param[0].Value=iid;			
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表yunRis_身体部位所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<yunRis_身体部位Model> GetyunRis_身体部位All()
		{			
			return GetyunRis_身体部位All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表yunRis_身体部位所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<yunRis_身体部位Model> GetyunRis_身体部位All(string sqlWhere)
		{
			IList<yunRis_身体部位Model> list=new List<yunRis_身体部位Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere;//.Replace("'","''");
			}
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}

		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="iid">iid</param>
        /// <returns>是/否</returns>
		public bool IsExist(decimal iid)
		{			
			SqlParameter[] _param = GetPRMParameters();
            _param[0].Value=iid;
            int a = Convert.ToInt32(HIS.COMM.DbHelperSqlServer.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetPRMParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(PARM_PRM_yunRis_身体部位);

            if (parms == null) {
                parms = new SqlParameter[] {						
					new SqlParameter("@IID",SqlDbType.Decimal)
				};
                HIS.COMM.DbHelperSqlServer.CacheParameters(PARM_PRM_yunRis_身体部位, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetInsertParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_INSERTyunRis_身体部位);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@IID",SqlDbType.Decimal),
					new SqlParameter("@设备类型",SqlDbType.VarChar),
					new SqlParameter("@部位编码",SqlDbType.VarChar),
					new SqlParameter("@部位名称",SqlDbType.VarChar),
					new SqlParameter("@部位名称_ENG",SqlDbType.VarChar),
					new SqlParameter("@HIS部位编码",SqlDbType.VarChar),
					new SqlParameter("@HIS部位名称",SqlDbType.VarChar),
					new SqlParameter("@CDEPT",SqlDbType.VarChar),
					new SqlParameter("@CAE_TITLE",SqlDbType.VarChar),
					new SqlParameter("@排序ID",SqlDbType.Decimal),
					new SqlParameter("@备注",SqlDbType.VarChar),
					new SqlParameter("@父ID",SqlDbType.Decimal),
					new SqlParameter("@价格",SqlDbType.Decimal),
					new SqlParameter("@IBGCS",SqlDbType.Decimal),
					new SqlParameter("@默认部位",SqlDbType.VarChar),
					new SqlParameter("@医院编码",SqlDbType.VarChar),
					new SqlParameter("@createTime",SqlDbType.DateTime)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_INSERTyunRis_身体部位, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetUpdateParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_UPDATE_yunRis_身体部位_BY_PR);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@IID",SqlDbType.Decimal),
					new SqlParameter("@设备类型",SqlDbType.VarChar),
					new SqlParameter("@部位编码",SqlDbType.VarChar),
					new SqlParameter("@部位名称",SqlDbType.VarChar),
					new SqlParameter("@部位名称_ENG",SqlDbType.VarChar),
					new SqlParameter("@HIS部位编码",SqlDbType.VarChar),
					new SqlParameter("@HIS部位名称",SqlDbType.VarChar),
					new SqlParameter("@CDEPT",SqlDbType.VarChar),
					new SqlParameter("@CAE_TITLE",SqlDbType.VarChar),
					new SqlParameter("@排序ID",SqlDbType.Decimal),
					new SqlParameter("@备注",SqlDbType.VarChar),
					new SqlParameter("@父ID",SqlDbType.Decimal),
					new SqlParameter("@价格",SqlDbType.Decimal),
					new SqlParameter("@IBGCS",SqlDbType.Decimal),
					new SqlParameter("@默认部位",SqlDbType.VarChar),
					new SqlParameter("@医院编码",SqlDbType.VarChar),
					new SqlParameter("@createTime",SqlDbType.DateTime)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_UPDATE_yunRis_身体部位_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

