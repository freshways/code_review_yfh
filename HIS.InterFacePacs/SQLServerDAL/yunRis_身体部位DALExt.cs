﻿// litao@Copy Right 2006-2016
// 文件： yunRis_身体部位.cs
// 创建时间：2018-07-15
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.yunRis_身体部位.
    /// </summary>
    public partial class yunRis_身体部位DALExt: IyunRis_身体部位Ext
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM yunRis_身体部位 ";
        private static readonly string SQL_SELECT_BY_PR = "select MAX(convert(NUMERIC(18,0),部位编码))+1 from yunRis_身体部位 where ISNUMERIC(部位编码)=1";
        private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM yunRis_身体部位 WHERE 设备类型=@设备类型 AND 部位编码=@部位编码";

        private static readonly string PARM_PRM_yunRis_身体部位 = "@设备类型,@部位编码";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public yunRis_身体部位DALExt(){
         HIS.COMM.DbHelperSqlServer.clearParmCache();
        }
		#endregion

        #region -----------实例化接口函数-----------
		

		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="iid">iid</param>
        /// <returns>是/否</returns>
        public bool IsExist(string _设备类型,string _部位编码)
		{			
			SqlParameter[] _param = GetPRMParameters();
            _param[0].Value = _设备类型;
            _param[1].Value = _部位编码;
            int a = Convert.ToInt32(HIS.COMM.DbHelperSqlServer.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        public string GetMaxID(string _设备类型)
        {
            //SqlParameter[] _param = GetPRMParameters();
            //_param[0].Value = _设备类型;
            //_param[1].Value = "";
            string a = Convert.ToString(HIS.COMM.DbHelperSqlServer.GetSingle(SQL_SELECT_BY_PR));
            if (!string.IsNullOrEmpty(a))
            {
                return a;
            }
            else
            {
                return "0001";
            }
        }

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetPRMParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(PARM_PRM_yunRis_身体部位);

            if (parms == null) {
                parms = new SqlParameter[] {						
					new SqlParameter("@设备类型",SqlDbType.VarChar),
                    new SqlParameter("@部位编码",SqlDbType.VarChar)
				};
                HIS.COMM.DbHelperSqlServer.CacheParameters(PARM_PRM_yunRis_身体部位, parms);
            }
            return parms;
        }
		#endregion
		
    }
}

