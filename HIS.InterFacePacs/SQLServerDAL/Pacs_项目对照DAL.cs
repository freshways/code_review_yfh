﻿// litao@Copy Right 2006-2016
// 文件： Pacs_项目对照.cs
// 创建时间：2018-07-15
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.Pacs_项目对照.
    /// </summary>
    public partial class Pacs_项目对照DAL: IPacs_项目对照
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM Pacs_项目对照 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM Pacs_项目对照 WHERE ID=@ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM Pacs_项目对照 WHERE ID=@ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM Pacs_项目对照 WHERE ID=@ID";
		private static readonly string SQL_INSERTPacs_项目对照 = "INSERT INTO Pacs_项目对照 (医嘱编码,医嘱名称,医嘱名称拼音代码,收费编码,收费名称,执行科室编码,执行科室名称,设备编码,设备名称,分院编码,部位编码,部位名称,胶片编码,胶片名称,胶片张数,收费数量,GUID,createTime) VALUES (@医嘱编码,@医嘱名称,@医嘱名称拼音代码,@收费编码,@收费名称,@执行科室编码,@执行科室名称,@设备编码,@设备名称,@分院编码,@部位编码,@部位名称,@胶片编码,@胶片名称,@胶片张数,@收费数量,@GUID,@createTime)";
		private static readonly string SQL_UPDATE_Pacs_项目对照_BY_PR = "UPDATE Pacs_项目对照 SET 医嘱编码=@医嘱编码,医嘱名称=@医嘱名称,医嘱名称拼音代码=@医嘱名称拼音代码,收费编码=@收费编码,收费名称=@收费名称,执行科室编码=@执行科室编码,执行科室名称=@执行科室名称,设备编码=@设备编码,设备名称=@设备名称,分院编码=@分院编码,部位编码=@部位编码,部位名称=@部位名称,胶片编码=@胶片编码,胶片名称=@胶片名称,胶片张数=@胶片张数,收费数量=@收费数量 WHERE ID=@ID";
		private static readonly string PARM_PRM_Pacs_项目对照 = "@ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public Pacs_项目对照DAL(){
         HIS.COMM.DbHelperSqlServer.clearParmCache();
        }
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="pacs_项目对照Model">Pacs_项目对照实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_项目对照Model pacs_项目对照Model)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.ID);
			_param[1].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.医嘱编码);
			_param[2].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.医嘱名称);
			_param[3].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.医嘱名称拼音代码);
			_param[4].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.收费编码);
			_param[5].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.收费名称);
			_param[6].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.执行科室编码);
			_param[7].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.执行科室名称);
			_param[8].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.设备编码);
			_param[9].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.设备名称);
			_param[10].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.分院编码);
			_param[11].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.部位编码);
			_param[12].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.部位名称);
			_param[13].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.胶片编码);
			_param[14].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.胶片名称);
			_param[15].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.胶片张数);
			_param[16].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.收费数量);
			_param[17].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.GUID);
			_param[18].Value = WEISHENG.COMM.nullHelper.NullToDbNull( pacs_项目对照Model.createTime);
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_INSERTPacs_项目对照,_param);
			
		}
        
        public int Insert(Pacs_项目对照Model pacs_项目对照Model,SqlTransaction connT)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.ID);
			_param[1].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱编码);
			_param[2].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱名称);
			_param[3].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱名称拼音代码);
			_param[4].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费编码);
			_param[5].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费名称);
			_param[6].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.执行科室编码);
			_param[7].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.执行科室名称);
			_param[8].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.设备编码);
			_param[9].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.设备名称);
			_param[10].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.分院编码);
			_param[11].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.部位编码);
			_param[12].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.部位名称);
			_param[13].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片编码);
			_param[14].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片名称);
			_param[15].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片张数);
			_param[16].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费数量);
			_param[17].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.GUID);
			_param[18].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.createTime);
			
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_INSERTPacs_项目对照,_param);
		}
        
		/// <summary>
		/// 向数据表Pacs_项目对照更新一条记录。

		/// </summary>
		/// <param name="pacs_项目对照Model">pacs_项目对照Model</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_项目对照Model pacs_项目对照Model)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.ID);
			_param[1].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱编码);
			_param[2].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱名称);
			_param[3].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱名称拼音代码);
			_param[4].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费编码);
			_param[5].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费名称);
			_param[6].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.执行科室编码);
			_param[7].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.执行科室名称);
			_param[8].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.设备编码);
			_param[9].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.设备名称);
			_param[10].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.分院编码);
			_param[11].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.部位编码);
			_param[12].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.部位名称);
			_param[13].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片编码);
			_param[14].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片名称);
			_param[15].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片张数);
			_param[16].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费数量);
			_param[17].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.GUID);
			_param[18].Value= WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.createTime);
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_UPDATE_Pacs_项目对照_BY_PR,_param);
		}
        
        public int Update(Pacs_项目对照Model pacs_项目对照Model,SqlTransaction connT)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.ID);
			_param[1].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱编码);
			_param[2].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱名称);
			_param[3].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.医嘱名称拼音代码);
			_param[4].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费编码);
			_param[5].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费名称);
			_param[6].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.执行科室编码);
			_param[7].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.执行科室名称);
			_param[8].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.设备编码);
			_param[9].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.设备名称);
			_param[10].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.分院编码);
			_param[11].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.部位编码);
			_param[12].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.部位名称);
			_param[13].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片编码);
			_param[14].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片名称);
			_param[15].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.胶片张数);
			_param[16].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.收费数量);
			_param[17].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.GUID);
			_param[18].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(pacs_项目对照Model.createTime);
			
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_UPDATE_Pacs_项目对照_BY_PR,_param);
		}
        
		/// <summary>
		/// 删除数据表Pacs_项目对照中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
        public int Delete(int id,SqlTransaction connT)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  pacs_项目对照 数据实体
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>pacs_项目对照 数据实体</returns>
		private Pacs_项目对照Model GetModelFromDr(DataRow row)
		{
			Pacs_项目对照Model Obj = new Pacs_项目对照Model();
			if(row!=null)
			{
				Obj.ID          = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.医嘱编码        =  row["医嘱编码"].ToString();
				Obj.医嘱名称        =  row["医嘱名称"].ToString();
				Obj.医嘱名称拼音代码    =  row["医嘱名称拼音代码"].ToString();
				Obj.收费编码        = (( row["收费编码"])==DBNull.Value)?0:Convert.ToInt32( row["收费编码"]);
				Obj.收费名称        =  row["收费名称"].ToString();
				Obj.执行科室编码      = (( row["执行科室编码"])==DBNull.Value)?0:Convert.ToInt32( row["执行科室编码"]);
				Obj.执行科室名称      =  row["执行科室名称"].ToString();
				Obj.设备编码        =  row["设备编码"].ToString();
				Obj.设备名称        =  row["设备名称"].ToString();
				Obj.分院编码        = (( row["分院编码"])==DBNull.Value)?0:Convert.ToInt32( row["分院编码"]);
				Obj.部位编码        =  row["部位编码"].ToString();
				Obj.部位名称        =  row["部位名称"].ToString();
				Obj.胶片编码        =  row["胶片编码"].ToString();
				Obj.胶片名称        =  row["胶片名称"].ToString();
				Obj.胶片张数        = (( row["胶片张数"])==DBNull.Value)?0:Convert.ToInt32( row["胶片张数"]);
				Obj.收费数量        = (( row["收费数量"])==DBNull.Value)?0:Convert.ToInt32( row["收费数量"]);
				Obj.GUID        =  row["GUID"].ToString();
				Obj.createTime  = (( row["createTime"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["createTime"]);
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  pacs_项目对照 数据实体
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>pacs_项目对照 数据实体</returns>
		private Pacs_项目对照Model GetModelFromDr(IDataReader dr)
		{
			Pacs_项目对照Model Obj = new Pacs_项目对照Model();
			
				Obj.ID         = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.医嘱编码       =  dr["医嘱编码"].ToString();
				Obj.医嘱名称       =  dr["医嘱名称"].ToString();
				Obj.医嘱名称拼音代码   =  dr["医嘱名称拼音代码"].ToString();
				Obj.收费编码       = (( dr["收费编码"])==DBNull.Value)?0:Convert.ToInt32( dr["收费编码"]);
				Obj.收费名称       =  dr["收费名称"].ToString();
				Obj.执行科室编码     = (( dr["执行科室编码"])==DBNull.Value)?0:Convert.ToInt32( dr["执行科室编码"]);
				Obj.执行科室名称     =  dr["执行科室名称"].ToString();
				Obj.设备编码       =  dr["设备编码"].ToString();
				Obj.设备名称       =  dr["设备名称"].ToString();
				Obj.分院编码       = (( dr["分院编码"])==DBNull.Value)?0:Convert.ToInt32( dr["分院编码"]);
				Obj.部位编码       =  dr["部位编码"].ToString();
				Obj.部位名称       =  dr["部位名称"].ToString();
				Obj.胶片编码       =  dr["胶片编码"].ToString();
				Obj.胶片名称       =  dr["胶片名称"].ToString();
				Obj.胶片张数       = (( dr["胶片张数"])==DBNull.Value)?0:Convert.ToInt32( dr["胶片张数"]);
				Obj.收费数量       = (( dr["收费数量"])==DBNull.Value)?0:Convert.ToInt32( dr["收费数量"]);
				Obj.GUID       =  dr["GUID"].ToString();
				Obj.createTime = (( dr["createTime"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["createTime"]);
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个Pacs_项目对照对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>Pacs_项目对照对象</returns>
		public Pacs_项目对照Model GetPacs_项目对照 (int id)
		{
			Pacs_项目对照Model _obj=null;			
			SqlParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表Pacs_项目对照所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_项目对照Model> GetPacs_项目对照All()
		{			
			return GetPacs_项目对照All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表Pacs_项目对照所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_项目对照Model> GetPacs_项目对照All(string sqlWhere)
		{
			IList<Pacs_项目对照Model> list=new List<Pacs_项目对照Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere;//.Replace("'","''");
			}
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			SqlParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(HIS.COMM.DbHelperSqlServer.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetPRMParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(PARM_PRM_Pacs_项目对照);

            if (parms == null) {
                parms = new SqlParameter[] {						
					new SqlParameter("@ID",SqlDbType.Int)
				};
                HIS.COMM.DbHelperSqlServer.CacheParameters(PARM_PRM_Pacs_项目对照, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetInsertParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_INSERTPacs_项目对照);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@医嘱编码",SqlDbType.VarChar),
					new SqlParameter("@医嘱名称",SqlDbType.VarChar),
					new SqlParameter("@医嘱名称拼音代码",SqlDbType.VarChar),
					new SqlParameter("@收费编码",SqlDbType.Int),
					new SqlParameter("@收费名称",SqlDbType.VarChar),
					new SqlParameter("@执行科室编码",SqlDbType.Int),
					new SqlParameter("@执行科室名称",SqlDbType.VarChar),
					new SqlParameter("@设备编码",SqlDbType.VarChar),
					new SqlParameter("@设备名称",SqlDbType.VarChar),
					new SqlParameter("@分院编码",SqlDbType.Int),
					new SqlParameter("@部位编码",SqlDbType.VarChar),
					new SqlParameter("@部位名称",SqlDbType.VarChar),
					new SqlParameter("@胶片编码",SqlDbType.VarChar),
					new SqlParameter("@胶片名称",SqlDbType.VarChar),
					new SqlParameter("@胶片张数",SqlDbType.Int),
					new SqlParameter("@收费数量",SqlDbType.Int),
					new SqlParameter("@GUID",SqlDbType.VarChar),
					new SqlParameter("@createTime",SqlDbType.DateTime)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_INSERTPacs_项目对照, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetUpdateParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_UPDATE_Pacs_项目对照_BY_PR);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@医嘱编码",SqlDbType.VarChar),
					new SqlParameter("@医嘱名称",SqlDbType.VarChar),
					new SqlParameter("@医嘱名称拼音代码",SqlDbType.VarChar),
					new SqlParameter("@收费编码",SqlDbType.Int),
					new SqlParameter("@收费名称",SqlDbType.VarChar),
					new SqlParameter("@执行科室编码",SqlDbType.Int),
					new SqlParameter("@执行科室名称",SqlDbType.VarChar),
					new SqlParameter("@设备编码",SqlDbType.VarChar),
					new SqlParameter("@设备名称",SqlDbType.VarChar),
					new SqlParameter("@分院编码",SqlDbType.Int),
					new SqlParameter("@部位编码",SqlDbType.VarChar),
					new SqlParameter("@部位名称",SqlDbType.VarChar),
					new SqlParameter("@胶片编码",SqlDbType.VarChar),
					new SqlParameter("@胶片名称",SqlDbType.VarChar),
					new SqlParameter("@胶片张数",SqlDbType.Int),
					new SqlParameter("@收费数量",SqlDbType.Int),
					new SqlParameter("@GUID",SqlDbType.VarChar),
					new SqlParameter("@createTime",SqlDbType.DateTime)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_UPDATE_Pacs_项目对照_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

