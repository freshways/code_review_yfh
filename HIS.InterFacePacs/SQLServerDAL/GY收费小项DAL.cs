﻿// litao@Copy Right 2006-2016
// 文件： GY收费小项.cs
// 创建时间：2018-07-15
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.GY收费小项.
    /// </summary>
    public partial class GY收费小项DAL: IGY收费小项
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM GY收费小项 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM GY收费小项 WHERE ID=@ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM GY收费小项 WHERE ID=@ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM GY收费小项 WHERE ID=@ID";
		private static readonly string SQL_INSERTGY收费小项 = "INSERT INTO GY收费小项 (收费编码,收费名称,单位,单价,归并编码,归并名称,拼音代码,是否禁用,单位编码,新合编码,是否报销,新合门诊报销比例12,执行科室编码,输液贴打印标志,PACS检查项,LIS检查项) VALUES (@收费编码,@收费名称,@单位,@单价,@归并编码,@归并名称,@拼音代码,@是否禁用,@单位编码,@新合编码,@是否报销,@新合门诊报销比例12,@执行科室编码,@输液贴打印标志,@PACS检查项,@LIS检查项)";
		private static readonly string SQL_UPDATE_GY收费小项_BY_PR = "UPDATE GY收费小项 SET 收费编码=@收费编码,收费名称=@收费名称,单位=@单位,单价=@单价,归并编码=@归并编码,归并名称=@归并名称,拼音代码=@拼音代码,是否禁用=@是否禁用,单位编码=@单位编码,新合编码=@新合编码,是否报销=@是否报销,新合门诊报销比例12=@新合门诊报销比例12,执行科室编码=@执行科室编码,输液贴打印标志=@输液贴打印标志,PACS检查项=@PACS检查项,LIS检查项=@LIS检查项 WHERE ID=@ID";
		private static readonly string PARM_PRM_GY收费小项 = "@ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public GY收费小项DAL(){
         HIS.COMM.DbHelperSqlServer.clearParmCache();
        }
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="gy收费小项Model">GY收费小项实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(GY收费小项Model gy收费小项Model)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.ID);
			_param[1].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.收费编码);
			_param[2].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.收费名称);
			_param[3].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.单位);
			_param[4].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.单价);
			_param[5].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.归并编码);
			_param[6].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.归并名称);
			_param[7].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.拼音代码);
			_param[8].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.是否禁用);
			_param[9].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.单位编码);
			_param[10].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.新合编码);
			_param[11].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.是否报销);
			_param[12].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.新合门诊报销比例12);
			_param[13].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.执行科室编码);
			_param[14].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.输液贴打印标志);
			_param[15].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.PACS检查项);
			_param[16].Value = WEISHENG.COMM.nullHelper.NullToDbNull( gy收费小项Model.LIS检查项);
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_INSERTGY收费小项,_param);
			
		}
        
        public int Insert(GY收费小项Model gy收费小项Model,SqlTransaction connT)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.ID);
			_param[1].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.收费编码);
			_param[2].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.收费名称);
			_param[3].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单位);
			_param[4].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单价);
			_param[5].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.归并编码);
			_param[6].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.归并名称);
			_param[7].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.拼音代码);
			_param[8].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.是否禁用);
			_param[9].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单位编码);
			_param[10].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.新合编码);
			_param[11].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.是否报销);
			_param[12].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.新合门诊报销比例12);
			_param[13].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.执行科室编码);
			_param[14].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.输液贴打印标志);
			_param[15].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.PACS检查项);
			_param[16].Value =  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.LIS检查项);
			
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_INSERTGY收费小项,_param);
		}
        
		/// <summary>
		/// 向数据表GY收费小项更新一条记录。

		/// </summary>
		/// <param name="gy收费小项Model">gy收费小项Model</param>
		/// <returns>影响的行数</returns>
		public int Update(GY收费小项Model gy收费小项Model)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.ID);
			_param[1].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.收费编码);
			_param[2].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.收费名称);
			_param[3].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单位);
			_param[4].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单价);
			_param[5].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.归并编码);
			_param[6].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.归并名称);
			_param[7].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.拼音代码);
			_param[8].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.是否禁用);
			_param[9].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单位编码);
			_param[10].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.新合编码);
			_param[11].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.是否报销);
			_param[12].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.新合门诊报销比例12);
			_param[13].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.执行科室编码);
			_param[14].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.输液贴打印标志);
			_param[15].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.PACS检查项);
			_param[16].Value= WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.LIS检查项);
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_UPDATE_GY收费小项_BY_PR,_param);
		}
        
        public int Update(GY收费小项Model gy收费小项Model,SqlTransaction connT)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.ID);
			_param[1].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.收费编码);
			_param[2].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.收费名称);
			_param[3].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单位);
			_param[4].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单价);
			_param[5].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.归并编码);
			_param[6].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.归并名称);
			_param[7].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.拼音代码);
			_param[8].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.是否禁用);
			_param[9].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.单位编码);
			_param[10].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.新合编码);
			_param[11].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.是否报销);
			_param[12].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.新合门诊报销比例12);
			_param[13].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.执行科室编码);
			_param[14].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.输液贴打印标志);
			_param[15].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.PACS检查项);
			_param[16].Value=  WEISHENG.COMM.nullHelper.NullToDbNull(gy收费小项Model.LIS检查项);
			
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_UPDATE_GY收费小项_BY_PR,_param);
		}
        
		/// <summary>
		/// 删除数据表GY收费小项中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
        public int Delete(int id,SqlTransaction connT)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return HIS.COMM.SqlHelper.ExecuteNonQuery(connT,CommandType.Text,SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  gy收费小项 数据实体
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>gy收费小项 数据实体</returns>
		private GY收费小项Model GetModelFromDr(DataRow row)
		{
			GY收费小项Model Obj = new GY收费小项Model();
			if(row!=null)
			{
				Obj.ID          = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.收费编码        = (( row["收费编码"])==DBNull.Value)?0:Convert.ToInt32( row["收费编码"]);
				Obj.收费名称        =  row["收费名称"].ToString();
				Obj.单位          =  row["单位"].ToString();
				Obj.单价          = (( row["单价"])==DBNull.Value)?0:Convert.ToDecimal( row["单价"]);
				Obj.归并编码        = (( row["归并编码"])==DBNull.Value)?0:Convert.ToInt32( row["归并编码"]);
				Obj.归并名称        =  row["归并名称"].ToString();
				Obj.拼音代码        =  row["拼音代码"].ToString();
				Obj.是否禁用        =  row["是否禁用"] == DBNull.Value ? false :(bool) row["是否禁用"];
				Obj.单位编码        = (( row["单位编码"])==DBNull.Value)?0:Convert.ToInt32( row["单位编码"]);
				Obj.新合编码        =  row["新合编码"].ToString();
				Obj.是否报销        =  row["是否报销"] == DBNull.Value ? false :(bool) row["是否报销"];
				Obj.新合门诊报销比例12  = (( row["新合门诊报销比例12"])==DBNull.Value)?0:Convert.ToDecimal( row["新合门诊报销比例12"]);
				Obj.执行科室编码      = (( row["执行科室编码"])==DBNull.Value)?0:Convert.ToInt32( row["执行科室编码"]);
				Obj.输液贴打印标志     = (( row["输液贴打印标志"])==DBNull.Value)?0:Convert.ToInt32( row["输液贴打印标志"]);
				Obj.PACS检查项     =  row["PACS检查项"] == DBNull.Value ? false :(bool) row["PACS检查项"];
				Obj.LIS检查项      =  row["LIS检查项"] == DBNull.Value ? false :(bool) row["LIS检查项"];
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  gy收费小项 数据实体
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>gy收费小项 数据实体</returns>
		private GY收费小项Model GetModelFromDr(IDataReader dr)
		{
			GY收费小项Model Obj = new GY收费小项Model();
			
				Obj.ID         = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.收费编码       = (( dr["收费编码"])==DBNull.Value)?0:Convert.ToInt32( dr["收费编码"]);
				Obj.收费名称       =  dr["收费名称"].ToString();
				Obj.单位         =  dr["单位"].ToString();
				Obj.单价         = (( dr["单价"])==DBNull.Value)?0:Convert.ToDecimal( dr["单价"]);
				Obj.归并编码       = (( dr["归并编码"])==DBNull.Value)?0:Convert.ToInt32( dr["归并编码"]);
				Obj.归并名称       =  dr["归并名称"].ToString();
				Obj.拼音代码       =  dr["拼音代码"].ToString();
				Obj.是否禁用       =  dr["是否禁用"] == DBNull.Value ? false :(bool) dr["是否禁用"];
				Obj.单位编码       = (( dr["单位编码"])==DBNull.Value)?0:Convert.ToInt32( dr["单位编码"]);
				Obj.新合编码       =  dr["新合编码"].ToString();
				Obj.是否报销       =  dr["是否报销"] == DBNull.Value ? false :(bool) dr["是否报销"];
				Obj.新合门诊报销比例12 = (( dr["新合门诊报销比例12"])==DBNull.Value)?0:Convert.ToDecimal( dr["新合门诊报销比例12"]);
				Obj.执行科室编码     = (( dr["执行科室编码"])==DBNull.Value)?0:Convert.ToInt32( dr["执行科室编码"]);
				Obj.输液贴打印标志    = (( dr["输液贴打印标志"])==DBNull.Value)?0:Convert.ToInt32( dr["输液贴打印标志"]);
				Obj.PACS检查项    =  dr["PACS检查项"] == DBNull.Value ? false :(bool) dr["PACS检查项"];
				Obj.LIS检查项     =  dr["LIS检查项"] == DBNull.Value ? false :(bool) dr["LIS检查项"];
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个GY收费小项对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>GY收费小项对象</returns>
		public GY收费小项Model GetGY收费小项 (int id)
		{
			GY收费小项Model _obj=null;			
			SqlParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表GY收费小项所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY收费小项Model> GetGY收费小项All()
		{			
			return GetGY收费小项All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表GY收费小项所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY收费小项Model> GetGY收费小项All(string sqlWhere)
		{
			IList<GY收费小项Model> list=new List<GY收费小项Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere;//.Replace("'","''");
			}
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			SqlParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(HIS.COMM.DbHelperSqlServer.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetPRMParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(PARM_PRM_GY收费小项);

            if (parms == null) {
                parms = new SqlParameter[] {						
					new SqlParameter("@ID",SqlDbType.Int)
				};
                HIS.COMM.DbHelperSqlServer.CacheParameters(PARM_PRM_GY收费小项, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetInsertParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_INSERTGY收费小项);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@收费编码",SqlDbType.Int),
					new SqlParameter("@收费名称",SqlDbType.VarChar),
					new SqlParameter("@单位",SqlDbType.VarChar),
					new SqlParameter("@单价",SqlDbType.Decimal),
					new SqlParameter("@归并编码",SqlDbType.Int),
					new SqlParameter("@归并名称",SqlDbType.VarChar),
					new SqlParameter("@拼音代码",SqlDbType.VarChar),
					new SqlParameter("@是否禁用",SqlDbType.Bit),
					new SqlParameter("@单位编码",SqlDbType.Int),
					new SqlParameter("@新合编码",SqlDbType.VarChar),
					new SqlParameter("@是否报销",SqlDbType.Bit),
					new SqlParameter("@新合门诊报销比例12",SqlDbType.Decimal),
					new SqlParameter("@执行科室编码",SqlDbType.Int),
					new SqlParameter("@输液贴打印标志",SqlDbType.Int),
					new SqlParameter("@PACS检查项",SqlDbType.Bit),
					new SqlParameter("@LIS检查项",SqlDbType.Bit)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_INSERTGY收费小项, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetUpdateParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_UPDATE_GY收费小项_BY_PR);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@收费编码",SqlDbType.Int),
					new SqlParameter("@收费名称",SqlDbType.VarChar),
					new SqlParameter("@单位",SqlDbType.VarChar),
					new SqlParameter("@单价",SqlDbType.Decimal),
					new SqlParameter("@归并编码",SqlDbType.Int),
					new SqlParameter("@归并名称",SqlDbType.VarChar),
					new SqlParameter("@拼音代码",SqlDbType.VarChar),
					new SqlParameter("@是否禁用",SqlDbType.Bit),
					new SqlParameter("@单位编码",SqlDbType.Int),
					new SqlParameter("@新合编码",SqlDbType.VarChar),
					new SqlParameter("@是否报销",SqlDbType.Bit),
					new SqlParameter("@新合门诊报销比例12",SqlDbType.Decimal),
					new SqlParameter("@执行科室编码",SqlDbType.Int),
					new SqlParameter("@输液贴打印标志",SqlDbType.Int),
					new SqlParameter("@PACS检查项",SqlDbType.Bit),
					new SqlParameter("@LIS检查项",SqlDbType.Bit)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_UPDATE_GY收费小项_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

