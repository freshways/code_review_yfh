﻿// litao@Copy Right 2006-2016
// 文件： Pacs_执行科室.cs
// 创建时间：2018-04-20
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.Pacs_执行科室.
    /// </summary>
    public partial class Pacs_执行科室DAL: IPacs_执行科室
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM Pacs_执行科室 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM Pacs_执行科室 WHERE ID=@ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM Pacs_执行科室 WHERE ID=@ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM Pacs_执行科室 WHERE ID=@ID";
		private static readonly string SQL_INSERTPacs_执行科室 = "INSERT INTO Pacs_执行科室 (执行科室编码,执行科室名称,分院编码,单位编码,Create_Time) VALUES (@执行科室编码,@执行科室名称,@分院编码,@单位编码,@Create_Time)";
		private static readonly string SQL_UPDATE_Pacs_执行科室_BY_PR = "UPDATE Pacs_执行科室 SET 执行科室编码=@执行科室编码,执行科室名称=@执行科室名称,分院编码=@分院编码,单位编码=@单位编码,Create_Time=@Create_Time WHERE ID=@ID";
		private static readonly string PARM_PRM_Pacs_执行科室 = "@ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public Pacs_执行科室DAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="pacs_执行科室Model">Pacs_执行科室实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_执行科室Model pacs_执行科室Model)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value = pacs_执行科室Model.ID;
			_param[1].Value = pacs_执行科室Model.执行科室编码;
			_param[2].Value = pacs_执行科室Model.执行科室名称;
			_param[3].Value = pacs_执行科室Model.分院编码;
			_param[4].Value = pacs_执行科室Model.单位编码;
			_param[5].Value = pacs_执行科室Model.Create_Time;
			
			return DbHelperSqlServer.ExecuteSql(SQL_INSERTPacs_执行科室,_param);
			
		}
		/// <summary>
		/// 向数据表Pacs_执行科室更新一条记录。

		/// </summary>
		/// <param name="pacs_执行科室Model">pacs_执行科室Model</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_执行科室Model pacs_执行科室Model)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=pacs_执行科室Model.ID;
			_param[1].Value=pacs_执行科室Model.执行科室编码;
			_param[2].Value=pacs_执行科室Model.执行科室名称;
			_param[3].Value=pacs_执行科室Model.分院编码;
			_param[4].Value=pacs_执行科室Model.单位编码;
			_param[5].Value=pacs_执行科室Model.Create_Time;
			
			return DbHelperSqlServer.ExecuteSql(SQL_UPDATE_Pacs_执行科室_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表Pacs_执行科室中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return DbHelperSqlServer.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  pacs_执行科室 数据实体
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>pacs_执行科室 数据实体</returns>
		private Pacs_执行科室Model GetModelFromDr(DataRow row)
		{
			Pacs_执行科室Model Obj = new Pacs_执行科室Model();
			if(row!=null)
			{
				Obj.ID           = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.执行科室编码       = (( row["执行科室编码"])==DBNull.Value)?0:Convert.ToInt32( row["执行科室编码"]);
				Obj.执行科室名称       =  row["执行科室名称"].ToString();
				Obj.分院编码         = (( row["分院编码"])==DBNull.Value)?0:Convert.ToInt32( row["分院编码"]);
				Obj.单位编码         = (( row["单位编码"])==DBNull.Value)?0:Convert.ToInt32( row["单位编码"]);
				Obj.Create_Time  = (( row["Create_Time"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["Create_Time"]);
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  pacs_执行科室 数据实体
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>pacs_执行科室 数据实体</returns>
		private Pacs_执行科室Model GetModelFromDr(IDataReader dr)
		{
			Pacs_执行科室Model Obj = new Pacs_执行科室Model();
			
				Obj.ID          = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.执行科室编码      = (( dr["执行科室编码"])==DBNull.Value)?0:Convert.ToInt32( dr["执行科室编码"]);
				Obj.执行科室名称      =  dr["执行科室名称"].ToString();
				Obj.分院编码        = (( dr["分院编码"])==DBNull.Value)?0:Convert.ToInt32( dr["分院编码"]);
				Obj.单位编码        = (( dr["单位编码"])==DBNull.Value)?0:Convert.ToInt32( dr["单位编码"]);
				Obj.Create_Time = (( dr["Create_Time"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["Create_Time"]);
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个Pacs_执行科室对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>Pacs_执行科室对象</returns>
		public Pacs_执行科室Model GetPacs_执行科室 (int id)
		{
			Pacs_执行科室Model _obj=null;			
			SqlParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(SqlDataReader dr=DbHelperSqlServer.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表Pacs_执行科室所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_执行科室Model> GetPacs_执行科室All()
		{			
			return GetPacs_执行科室All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表Pacs_执行科室所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_执行科室Model> GetPacs_执行科室All(string sqlWhere)
		{
			IList<Pacs_执行科室Model> list=new List<Pacs_执行科室Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere;//.Replace("'","''");
			}
			using(SqlDataReader dr=DbHelperSqlServer.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			SqlParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(DbHelperSqlServer.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetPRMParameters() {
            SqlParameter[] parms = DbHelperSqlServer.GetCachedParameters(PARM_PRM_Pacs_执行科室);

            if (parms == null) {
                parms = new SqlParameter[] {						
					new SqlParameter("@ID",SqlDbType.Int)
				};
                DbHelperSqlServer.CacheParameters(PARM_PRM_Pacs_执行科室, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetInsertParameters() {
            SqlParameter[] parms = DbHelperSqlServer.GetCachedParameters(SQL_INSERTPacs_执行科室);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@执行科室编码",SqlDbType.Int),
					new SqlParameter("@执行科室名称",SqlDbType.VarChar),
					new SqlParameter("@分院编码",SqlDbType.Int),
					new SqlParameter("@单位编码",SqlDbType.Int),
					new SqlParameter("@Create_Time",SqlDbType.DateTime)
					};
                DbHelperSqlServer.CacheParameters(SQL_INSERTPacs_执行科室, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetUpdateParameters() {
            SqlParameter[] parms = DbHelperSqlServer.GetCachedParameters(SQL_UPDATE_Pacs_执行科室_BY_PR);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@执行科室编码",SqlDbType.Int),
					new SqlParameter("@执行科室名称",SqlDbType.VarChar),
					new SqlParameter("@分院编码",SqlDbType.Int),
					new SqlParameter("@单位编码",SqlDbType.Int),
					new SqlParameter("@Create_Time",SqlDbType.DateTime)
					};
                DbHelperSqlServer.CacheParameters(SQL_UPDATE_Pacs_执行科室_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

