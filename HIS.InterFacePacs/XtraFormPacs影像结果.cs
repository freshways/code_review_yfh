﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFacePacs
{
    public partial class XtraFormPacs影像结果 : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormPacs影像结果()
        {
            InitializeComponent();
        }

        private void XtraForm影像结果_Load(object sender, EventArgs e)
        {
            this.textEdit开始时间.Text = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            this.textEdit截止时间.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.txt病人来源.SelectedIndex = 0;
            Get病人病区();
            txt申请科室.Text = WEISHENG.COMM.zdInfo.ModelUserInfo.科室名称;
        }

        private void Get病人病区()
        {
            DataTable dt = new DataTable();
            dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
                "select 科室名称,科室编码 from GY科室设置 where 是否住院科室=1 ").Tables[0];

            txt申请科室.Properties.Items.Clear();

            foreach (DataRow row in dt.Rows)
            {
                txt申请科室.Properties.Items.Add(row["科室名称"]);
            }
            //txt申请科室.Properties.DataSource = dt;
            //txt申请科室.Properties.DisplayMember = "科室名称";
            //txt申请科室.Properties.ValueMember = "科室编码";
            //txt申请科室.ItemIndex = 0;
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            Binding结果();
        }

        private void simpleButton查看_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                {
                    return;
                }
                string url = gridView1.GetDataRow(gridView1.FocusedRowHandle)["URL"].ToString();
                string url21 = gridView1.GetDataRow(gridView1.FocusedRowHandle)["URL21"].ToString();
                string url22 = gridView1.GetDataRow(gridView1.FocusedRowHandle)["URL22"].ToString();
                string newurl = url21 + "LID=admin&LPW=nimda" + url22;
                                
                if (!string.IsNullOrEmpty(newurl))
                    System.Diagnostics.Process.Start("D:\\GoogleChromePortable\\App\\Google Chrome\\chrome.exe", newurl);
                else if (!string.IsNullOrEmpty(url))
                    System.Diagnostics.Process.Start("iexplore.exe", url);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private static string _sConnOraPacsDB = @"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.10.107)(PORT=1521)) (CONNECT_DATA=(SERVICE_NAME=SPECTRA)));User Id=hishis;Password=hishis";
        DataTable dtMain;
        private void Binding结果()
        {
            string strSql = @"select  distinct 住院号,病人姓名,性别,年龄,床号,申请科室名称,申请医生,检查项目,
                                        case 病人来源 when '2' then '住院' when '1' then '门诊' end 病人来源,医院代码,
                                        报告医生姓名,报告时间,临床诊断,诊断描述,诊断结论,URL,URL21,URL22 from eris.ris2his_report where 1=1  ";
            strSql += " and 医院代码='" + WEISHENG.COMM.zdInfo.Model单位信息.iDwid.ToString() + "' ";
            if (textEdit开始时间.Text != "")
                strSql += " and 报告时间>=to_date('" + textEdit开始时间.Text + "','yyyy-mm-dd') ";
            if (textEdit截止时间.Text != "")
                strSql += " and 报告时间<=to_date('" + textEdit截止时间.Text + " 23:59:59','yyyy-mm-dd hh24:mi:ss') ";
            if (txt申请科室.Text != "")
                strSql += " and 申请科室名称='" + txt申请科室.Text + "' ";
            if (txt病人来源.Text != "" && txt病人来源.Text == "住院")
                strSql += " and 病人来源='2' ";
            else if (txt病人来源.Text != "" && txt病人来源.Text == "门诊")
                strSql += " and 病人来源='1' ";
            strSql += " order by 住院号 desc";
            try
            {
                dtMain = HIS.Model.Dal.OracleHelper.ExecuteDataset(InterFacePacs.ClassConnStrings.sConnOraPacsDB, CommandType.Text, strSql).Tables[0];
                gridControl1.DataSource = dtMain;
                this.gridView1.BestFitColumns();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
