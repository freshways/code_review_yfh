﻿// litao@Copy Right 2006-2016
// 文件： Pacs_Inteface.cs
// 创建时间：2018-04-20
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.OracleDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.Pacs_Inteface.
    /// </summary>
    public partial class Pacs_IntefaceDAL : IPacs_Inteface
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM Pacs_Inteface ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM Pacs_Inteface WHERE 申请单号=:申请单号";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM Pacs_Inteface WHERE 申请单号=:申请单号";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM Pacs_Inteface WHERE 申请单号=:申请单号";
		private static readonly string SQL_INSERTPacs_Inteface = "INSERT INTO Pacs_Inteface (ID,病人ID,申请单号,病人来源,病人姓名,性别,病人年龄,检查部位,申请日期,申请科室,申请医生,检查费,住院号,门诊号,床号,电话号码,类型,临床诊断,体征描述,状态,Pacs检查ID,检查时间,检查医生,检查所见,检查提示,检查结果,执行科室,医嘱ID,查体,身份证号,分院编码,末次月经,是否怀孕,孕周,孕天,家庭地址,检查部位编码) VALUES (:ID,:病人ID,:申请单号,:病人来源,:病人姓名,:性别,:病人年龄,:检查部位,:申请日期,:申请科室,:申请医生,:检查费,:住院号,:门诊号,:床号,:电话号码,:类型,:临床诊断,:体征描述,:状态,:Pacs检查ID,:检查时间,:检查医生,:检查所见,:检查提示,:检查结果,:执行科室,:医嘱ID,:查体,:身份证号,:分院编码,:末次月经,:是否怀孕,:孕周,:孕天,:家庭地址,:检查部位编码)";
		private static readonly string SQL_UPDATE_Pacs_Inteface_BY_PR = "UPDATE Pacs_Inteface SET ID=:ID,病人ID=:病人ID,病人来源=:病人来源,病人姓名=:病人姓名,性别=:性别,病人年龄=:病人年龄,检查部位=:检查部位,申请日期=:申请日期,申请科室=:申请科室,申请医生=:申请医生,检查费=:检查费,住院号=:住院号,门诊号=:门诊号,床号=:床号,电话号码=:电话号码,类型=:类型,临床诊断=:临床诊断,体征描述=:体征描述,状态=:状态,Pacs检查ID=:Pacs检查ID,检查时间=:检查时间,检查医生=:检查医生,检查所见=:检查所见,检查提示=:检查提示,检查结果=:检查结果,执行科室=:执行科室,医嘱ID=:医嘱ID,查体=:查体,身份证号=:身份证号,分院编码=:分院编码,末次月经=:末次月经,是否怀孕=:是否怀孕,孕周=:孕周,孕天=:孕天,家庭地址=:家庭地址,检查部位编码=:检查部位编码 WHERE 申请单号=:申请单号";
		private static readonly string PARM_PRM_Pacs_Inteface = ":申请单号";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public Pacs_IntefaceDAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="pacs_intefaceModel">Pacs_Inteface实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_IntefaceModel pacs_intefaceModel)
		{
			
            OracleParameter[] _param = GetInsertParameters();
			
			_param[0].Value = pacs_intefaceModel.ID;
			_param[1].Value = pacs_intefaceModel.病人ID;
			_param[2].Value = pacs_intefaceModel.申请单号;
			_param[3].Value = pacs_intefaceModel.病人来源;
			_param[4].Value = pacs_intefaceModel.病人姓名;
			_param[5].Value = pacs_intefaceModel.性别;
			_param[6].Value = pacs_intefaceModel.病人年龄;
			_param[7].Value = pacs_intefaceModel.检查部位;
			_param[8].Value = pacs_intefaceModel.申请日期;
			_param[9].Value = pacs_intefaceModel.申请科室;
			_param[10].Value = pacs_intefaceModel.申请医生;
			_param[11].Value = pacs_intefaceModel.检查费;
			_param[12].Value = pacs_intefaceModel.住院号;
			_param[13].Value = pacs_intefaceModel.门诊号;
			_param[14].Value = pacs_intefaceModel.床号;
			_param[15].Value = pacs_intefaceModel.电话号码;
			_param[16].Value = pacs_intefaceModel.类型;
			_param[17].Value = pacs_intefaceModel.临床诊断;
			_param[18].Value = pacs_intefaceModel.体征描述;
			_param[19].Value = pacs_intefaceModel.状态;
			_param[20].Value = pacs_intefaceModel.Pacs检查ID;
			_param[21].Value = pacs_intefaceModel.检查时间;
			_param[22].Value = pacs_intefaceModel.检查医生;
			_param[23].Value = pacs_intefaceModel.检查所见;
			_param[24].Value = pacs_intefaceModel.检查提示;
			_param[25].Value = pacs_intefaceModel.检查结果;
			_param[26].Value = pacs_intefaceModel.执行科室;
			_param[27].Value = pacs_intefaceModel.医嘱ID;
			_param[28].Value = pacs_intefaceModel.查体;
			_param[29].Value = pacs_intefaceModel.身份证号;
			_param[30].Value = pacs_intefaceModel.分院编码;
			_param[31].Value = pacs_intefaceModel.末次月经;
			_param[32].Value = pacs_intefaceModel.是否怀孕;
			_param[33].Value = pacs_intefaceModel.孕周;
			_param[34].Value = pacs_intefaceModel.孕天;
			_param[35].Value = pacs_intefaceModel.家庭地址;
			_param[36].Value = pacs_intefaceModel.检查部位编码;
			
			return DbHelperOra.ExecuteSql(SQL_INSERTPacs_Inteface,_param);
			
		}
		/// <summary>
		/// 向数据表Pacs_Inteface更新一条记录。

		/// </summary>
		/// <param name="pacs_intefaceModel">pacs_intefaceModel</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_IntefaceModel pacs_intefaceModel)
		{
            OracleParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=pacs_intefaceModel.ID;
			_param[1].Value=pacs_intefaceModel.病人ID;
			_param[2].Value=pacs_intefaceModel.申请单号;
			_param[3].Value=pacs_intefaceModel.病人来源;
			_param[4].Value=pacs_intefaceModel.病人姓名;
			_param[5].Value=pacs_intefaceModel.性别;
			_param[6].Value=pacs_intefaceModel.病人年龄;
			_param[7].Value=pacs_intefaceModel.检查部位;
			_param[8].Value=pacs_intefaceModel.申请日期;
			_param[9].Value=pacs_intefaceModel.申请科室;
			_param[10].Value=pacs_intefaceModel.申请医生;
			_param[11].Value=pacs_intefaceModel.检查费;
			_param[12].Value=pacs_intefaceModel.住院号;
			_param[13].Value=pacs_intefaceModel.门诊号;
			_param[14].Value=pacs_intefaceModel.床号;
			_param[15].Value=pacs_intefaceModel.电话号码;
			_param[16].Value=pacs_intefaceModel.类型;
			_param[17].Value=pacs_intefaceModel.临床诊断;
			_param[18].Value=pacs_intefaceModel.体征描述;
			_param[19].Value=pacs_intefaceModel.状态;
			_param[20].Value=pacs_intefaceModel.Pacs检查ID;
			_param[21].Value=pacs_intefaceModel.检查时间;
			_param[22].Value=pacs_intefaceModel.检查医生;
			_param[23].Value=pacs_intefaceModel.检查所见;
			_param[24].Value=pacs_intefaceModel.检查提示;
			_param[25].Value=pacs_intefaceModel.检查结果;
			_param[26].Value=pacs_intefaceModel.执行科室;
			_param[27].Value=pacs_intefaceModel.医嘱ID;
			_param[28].Value=pacs_intefaceModel.查体;
			_param[29].Value=pacs_intefaceModel.身份证号;
			_param[30].Value=pacs_intefaceModel.分院编码;
			_param[31].Value=pacs_intefaceModel.末次月经;
			_param[32].Value=pacs_intefaceModel.是否怀孕;
			_param[33].Value=pacs_intefaceModel.孕周;
			_param[34].Value=pacs_intefaceModel.孕天;
			_param[35].Value=pacs_intefaceModel.家庭地址;
			_param[36].Value=pacs_intefaceModel.检查部位编码;
			
			return DbHelperOra.ExecuteSql(SQL_UPDATE_Pacs_Inteface_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表Pacs_Inteface中的一条记录

		/// </summary>
	    /// <param name="申请单号">申请单号</param>
		/// <returns>影响的行数</returns>
		public int Delete(string 申请单号)
		{
			OracleParameter[] _param = GetPRMParameters();
			_param[0].Value=申请单号;
			return DbHelperOra.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  pacs_inteface 数据实体1
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>pacs_inteface 数据实体</returns>
		private Pacs_IntefaceModel GetModelFromDr(DataRow row)
		{
			Pacs_IntefaceModel Obj = new Pacs_IntefaceModel();
			if(row!=null)
			{
				Obj.ID        = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.病人ID      =  row["病人ID"].ToString();
				Obj.申请单号      =  row["申请单号"].ToString();
				Obj.病人来源      =  row["病人来源"].ToString();
				Obj.病人姓名      =  row["病人姓名"].ToString();
				Obj.性别        =  row["性别"].ToString();
				Obj.病人年龄      = (( row["病人年龄"])==DBNull.Value)?0:Convert.ToInt32( row["病人年龄"]);
				Obj.检查部位      =  row["检查部位"].ToString();
				Obj.申请日期      = (( row["申请日期"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["申请日期"]);
				Obj.申请科室      =  row["申请科室"].ToString();
				Obj.申请医生      =  row["申请医生"].ToString();
				Obj.检查费       = (( row["检查费"])==DBNull.Value)?0:Convert.ToDecimal( row["检查费"]);
				Obj.住院号       =  row["住院号"].ToString();
				Obj.门诊号       =  row["门诊号"].ToString();
				Obj.床号        =  row["床号"].ToString();
				Obj.电话号码      =  row["电话号码"].ToString();
				Obj.类型        =  row["类型"].ToString();
				Obj.临床诊断      =  row["临床诊断"].ToString();
				Obj.体征描述      =  row["体征描述"].ToString();
				Obj.状态        = (( row["状态"])==DBNull.Value)?0:Convert.ToInt32( row["状态"]);
				Obj.Pacs检查ID  =  row["Pacs检查ID"].ToString();
				Obj.检查时间      = (( row["检查时间"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["检查时间"]);
				Obj.检查医生      =  row["检查医生"].ToString();
				Obj.检查所见      =  row["检查所见"].ToString();
				Obj.检查提示      =  row["检查提示"].ToString();
				Obj.检查结果      =  row["检查结果"].ToString();
				Obj.执行科室      =  row["执行科室"].ToString();
				Obj.医嘱ID      = (( row["医嘱ID"])==DBNull.Value)?0:Convert.ToInt32( row["医嘱ID"]);
				Obj.查体        =  row["查体"].ToString();
				Obj.身份证号      =  row["身份证号"].ToString();
				Obj.分院编码      = (( row["分院编码"])==DBNull.Value)?0:Convert.ToInt32( row["分院编码"]);
				Obj.末次月经      = (( row["末次月经"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["末次月经"]);
				Obj.是否怀孕      =  row["是否怀孕"].ToString();
				Obj.孕周        =  row["孕周"].ToString();
				Obj.孕天        =  row["孕天"].ToString();
				Obj.家庭地址      =  row["家庭地址"].ToString();
				Obj.检查部位编码    =  row["检查部位编码"].ToString();
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  pacs_inteface 数据实体2
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>pacs_inteface 数据实体</returns>
		private Pacs_IntefaceModel GetModelFromDr(IDataReader dr)
		{
			Pacs_IntefaceModel Obj = new Pacs_IntefaceModel();
			
				Obj.ID       = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.病人ID     =  dr["病人ID"].ToString();
				Obj.申请单号     =  dr["申请单号"].ToString();
				Obj.病人来源     =  dr["病人来源"].ToString();
				Obj.病人姓名     =  dr["病人姓名"].ToString();
				Obj.性别       =  dr["性别"].ToString();
				Obj.病人年龄     = (( dr["病人年龄"])==DBNull.Value)?0:Convert.ToInt32( dr["病人年龄"]);
				Obj.检查部位     =  dr["检查部位"].ToString();
				Obj.申请日期     = (( dr["申请日期"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["申请日期"]);
				Obj.申请科室     =  dr["申请科室"].ToString();
				Obj.申请医生     =  dr["申请医生"].ToString();
				Obj.检查费      = (( dr["检查费"])==DBNull.Value)?0:Convert.ToDecimal( dr["检查费"]);
				Obj.住院号      =  dr["住院号"].ToString();
				Obj.门诊号      =  dr["门诊号"].ToString();
				Obj.床号       =  dr["床号"].ToString();
				Obj.电话号码     =  dr["电话号码"].ToString();
				Obj.类型       =  dr["类型"].ToString();
				Obj.临床诊断     =  dr["临床诊断"].ToString();
				Obj.体征描述     =  dr["体征描述"].ToString();
				Obj.状态       = (( dr["状态"])==DBNull.Value)?0:Convert.ToInt32( dr["状态"]);
				Obj.Pacs检查ID =  dr["Pacs检查ID"].ToString();
				Obj.检查时间     = (( dr["检查时间"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["检查时间"]);
				Obj.检查医生     =  dr["检查医生"].ToString();
				Obj.检查所见     =  dr["检查所见"].ToString();
				Obj.检查提示     =  dr["检查提示"].ToString();
				Obj.检查结果     =  dr["检查结果"].ToString();
				Obj.执行科室     =  dr["执行科室"].ToString();
				Obj.医嘱ID     = (( dr["医嘱ID"])==DBNull.Value)?0:Convert.ToInt32( dr["医嘱ID"]);
				Obj.查体       =  dr["查体"].ToString();
				Obj.身份证号     =  dr["身份证号"].ToString();
				Obj.分院编码     = (( dr["分院编码"])==DBNull.Value)?0:Convert.ToInt32( dr["分院编码"]);
				Obj.末次月经     = (( dr["末次月经"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["末次月经"]);
				Obj.是否怀孕     =  dr["是否怀孕"].ToString();
				Obj.孕周       =  dr["孕周"].ToString();
				Obj.孕天       =  dr["孕天"].ToString();
				Obj.家庭地址     =  dr["家庭地址"].ToString();
				Obj.检查部位编码   =  dr["检查部位编码"].ToString();
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个Pacs_Inteface对象
		/// </summary>
		/// <param name="申请单号">申请单号</param>
		/// <returns>Pacs_Inteface对象</returns>
		public Pacs_IntefaceModel GetPacs_Inteface (string 申请单号)
		{
			Pacs_IntefaceModel _obj=null;			
			OracleParameter[] _param = GetPRMParameters();			
			_param[0].Value=申请单号;			
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表Pacs_Inteface所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_IntefaceModel> GetPacs_IntefaceAll()
		{			
			return GetPacs_IntefaceAll("");
		}
		/// <summary>
		/// 根据查询条件得到数据表Pacs_Inteface所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_IntefaceModel> GetPacs_IntefaceAll(string sqlWhere)
		{
			IList<Pacs_IntefaceModel> list=new List<Pacs_IntefaceModel>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere.Replace("'","''");
			}
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="申请单号">申请单号</param>
        /// <returns>是/否</returns>
		public bool IsExist(string 申请单号)
		{			
			OracleParameter[] _param = GetPRMParameters();
            _param[0].Value=申请单号;
            int a = Convert.ToInt32(DbHelperOra.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetPRMParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(PARM_PRM_Pacs_Inteface);

            if (parms == null) {
                parms = new OracleParameter[] {						
					new OracleParameter(":申请单号",OracleDbType.Varchar2)
				};
                DbHelperOra.CacheParameters(PARM_PRM_Pacs_Inteface, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetInsertParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_INSERTPacs_Inteface);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":病人ID",OracleDbType.Varchar2),
					new OracleParameter(":申请单号",OracleDbType.Varchar2),
					new OracleParameter(":病人来源",OracleDbType.Varchar2),
					new OracleParameter(":病人姓名",OracleDbType.Varchar2),
					new OracleParameter(":性别",OracleDbType.Varchar2),
					new OracleParameter(":病人年龄",OracleDbType.Int32),
					new OracleParameter(":检查部位",OracleDbType.Varchar2),
					new OracleParameter(":申请日期",OracleDbType.Date),
					new OracleParameter(":申请科室",OracleDbType.Varchar2),
					new OracleParameter(":申请医生",OracleDbType.Varchar2),
					new OracleParameter(":检查费",OracleDbType.Decimal),
					new OracleParameter(":住院号",OracleDbType.Varchar2),
					new OracleParameter(":门诊号",OracleDbType.Varchar2),
					new OracleParameter(":床号",OracleDbType.Varchar2),
					new OracleParameter(":电话号码",OracleDbType.Varchar2),
					new OracleParameter(":类型",OracleDbType.Varchar2),
					new OracleParameter(":临床诊断",OracleDbType.Varchar2),
					new OracleParameter(":体征描述",OracleDbType.Varchar2),
					new OracleParameter(":状态",OracleDbType.Int32),
					new OracleParameter(":Pacs检查ID",OracleDbType.Varchar2),
					new OracleParameter(":检查时间",OracleDbType.Date),
					new OracleParameter(":检查医生",OracleDbType.Varchar2),
					new OracleParameter(":检查所见",OracleDbType.Varchar2),
					new OracleParameter(":检查提示",OracleDbType.Varchar2),
					new OracleParameter(":检查结果",OracleDbType.Varchar2),
					new OracleParameter(":执行科室",OracleDbType.Varchar2),
					new OracleParameter(":医嘱ID",OracleDbType.Int32),
					new OracleParameter(":查体",OracleDbType.Varchar2),
					new OracleParameter(":身份证号",OracleDbType.Varchar2),
					new OracleParameter(":分院编码",OracleDbType.Int32),
					new OracleParameter(":末次月经",OracleDbType.Date),
					new OracleParameter(":是否怀孕",OracleDbType.Varchar2),
					new OracleParameter(":孕周",OracleDbType.Varchar2),
					new OracleParameter(":孕天",OracleDbType.Varchar2),
					new OracleParameter(":家庭地址",OracleDbType.Varchar2),
					new OracleParameter(":检查部位编码",OracleDbType.Varchar2)
					};
                DbHelperOra.CacheParameters(SQL_INSERTPacs_Inteface, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetUpdateParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_UPDATE_Pacs_Inteface_BY_PR);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":病人ID",OracleDbType.Varchar2),
					new OracleParameter(":申请单号",OracleDbType.Varchar2),
					new OracleParameter(":病人来源",OracleDbType.Varchar2),
					new OracleParameter(":病人姓名",OracleDbType.Varchar2),
					new OracleParameter(":性别",OracleDbType.Varchar2),
					new OracleParameter(":病人年龄",OracleDbType.Int32),
					new OracleParameter(":检查部位",OracleDbType.Varchar2),
					new OracleParameter(":申请日期",OracleDbType.Date),
					new OracleParameter(":申请科室",OracleDbType.Varchar2),
					new OracleParameter(":申请医生",OracleDbType.Varchar2),
					new OracleParameter(":检查费",OracleDbType.Decimal),
					new OracleParameter(":住院号",OracleDbType.Varchar2),
					new OracleParameter(":门诊号",OracleDbType.Varchar2),
					new OracleParameter(":床号",OracleDbType.Varchar2),
					new OracleParameter(":电话号码",OracleDbType.Varchar2),
					new OracleParameter(":类型",OracleDbType.Varchar2),
					new OracleParameter(":临床诊断",OracleDbType.Varchar2),
					new OracleParameter(":体征描述",OracleDbType.Varchar2),
					new OracleParameter(":状态",OracleDbType.Int32),
					new OracleParameter(":Pacs检查ID",OracleDbType.Varchar2),
					new OracleParameter(":检查时间",OracleDbType.Date),
					new OracleParameter(":检查医生",OracleDbType.Varchar2),
					new OracleParameter(":检查所见",OracleDbType.Varchar2),
					new OracleParameter(":检查提示",OracleDbType.Varchar2),
					new OracleParameter(":检查结果",OracleDbType.Varchar2),
					new OracleParameter(":执行科室",OracleDbType.Varchar2),
					new OracleParameter(":医嘱ID",OracleDbType.Int32),
					new OracleParameter(":查体",OracleDbType.Varchar2),
					new OracleParameter(":身份证号",OracleDbType.Varchar2),
					new OracleParameter(":分院编码",OracleDbType.Int32),
					new OracleParameter(":末次月经",OracleDbType.Date),
					new OracleParameter(":是否怀孕",OracleDbType.Varchar2),
					new OracleParameter(":孕周",OracleDbType.Varchar2),
					new OracleParameter(":孕天",OracleDbType.Varchar2),
					new OracleParameter(":家庭地址",OracleDbType.Varchar2),
					new OracleParameter(":检查部位编码",OracleDbType.Varchar2)
					};
                DbHelperOra.CacheParameters(SQL_UPDATE_Pacs_Inteface_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

