﻿// litao@Copy Right 2006-2016
// 文件： Pacs_检查部位.cs
// 创建时间：2018-04-20
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.OracleDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.Pacs_检查部位.
    /// </summary>
    public partial class Pacs_检查部位DAL : IPacs_检查部位
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM Pacs_检查部位 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM Pacs_检查部位 WHERE 部位编码=:部位编码";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM Pacs_检查部位 WHERE 部位编码=:部位编码";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM Pacs_检查部位 WHERE 部位编码=:部位编码";
		private static readonly string SQL_INSERTPacs_检查部位 = "INSERT INTO Pacs_检查部位 (ID,部位编码,部位名称,部位索引,部位分类,是否停用) VALUES (:ID,:部位编码,:部位名称,:部位索引,:部位分类,:是否停用)";
		private static readonly string SQL_UPDATE_Pacs_检查部位_BY_PR = "UPDATE Pacs_检查部位 SET ID=:ID,部位名称=:部位名称,部位索引=:部位索引,部位分类=:部位分类,是否停用=:是否停用 WHERE 部位编码=:部位编码";
		private static readonly string PARM_PRM_Pacs_检查部位 = ":部位编码";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public Pacs_检查部位DAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="pacs_检查部位Model">Pacs_检查部位实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_检查部位Model pacs_检查部位Model)
		{
			
            OracleParameter[] _param = GetInsertParameters();
			
			_param[0].Value = pacs_检查部位Model.ID;
			_param[1].Value = pacs_检查部位Model.部位编码;
			_param[2].Value = pacs_检查部位Model.部位名称;
			_param[3].Value = pacs_检查部位Model.部位索引;
			_param[4].Value = pacs_检查部位Model.部位分类;
			_param[5].Value = pacs_检查部位Model.是否停用;
			
			return DbHelperOra.ExecuteSql(SQL_INSERTPacs_检查部位,_param);
			
		}
		/// <summary>
		/// 向数据表Pacs_检查部位更新一条记录。

		/// </summary>
		/// <param name="pacs_检查部位Model">pacs_检查部位Model</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_检查部位Model pacs_检查部位Model)
		{
            OracleParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=pacs_检查部位Model.ID;
			_param[1].Value=pacs_检查部位Model.部位编码;
			_param[2].Value=pacs_检查部位Model.部位名称;
			_param[3].Value=pacs_检查部位Model.部位索引;
			_param[4].Value=pacs_检查部位Model.部位分类;
			_param[5].Value=pacs_检查部位Model.是否停用;
			
			return DbHelperOra.ExecuteSql(SQL_UPDATE_Pacs_检查部位_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表Pacs_检查部位中的一条记录

		/// </summary>
	    /// <param name="部位编码">部位编码</param>
		/// <returns>影响的行数</returns>
		public int Delete(long 部位编码)
		{
			OracleParameter[] _param = GetPRMParameters();
			_param[0].Value=部位编码;
			return DbHelperOra.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  pacs_检查部位 数据实体1
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>pacs_检查部位 数据实体</returns>
		private Pacs_检查部位Model GetModelFromDr(DataRow row)
		{
			Pacs_检查部位Model Obj = new Pacs_检查部位Model();
			if(row!=null)
			{
				Obj.ID    = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.部位编码  = (long) row["部位编码"];
				Obj.部位名称  =  row["部位名称"].ToString();
				Obj.部位索引  =  row["部位索引"].ToString();
				Obj.部位分类  =  row["部位分类"].ToString();
				Obj.是否停用  =  row["是否停用"].ToString();
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  pacs_检查部位 数据实体2
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>pacs_检查部位 数据实体</returns>
		private Pacs_检查部位Model GetModelFromDr(IDataReader dr)
		{
			Pacs_检查部位Model Obj = new Pacs_检查部位Model();
			
				Obj.ID   = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.部位编码 = (long) dr["部位编码"];
				Obj.部位名称 =  dr["部位名称"].ToString();
				Obj.部位索引 =  dr["部位索引"].ToString();
				Obj.部位分类 =  dr["部位分类"].ToString();
				Obj.是否停用 =  dr["是否停用"].ToString();
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个Pacs_检查部位对象
		/// </summary>
		/// <param name="部位编码">部位编码</param>
		/// <returns>Pacs_检查部位对象</returns>
		public Pacs_检查部位Model GetPacs_检查部位 (long 部位编码)
		{
			Pacs_检查部位Model _obj=null;			
			OracleParameter[] _param = GetPRMParameters();			
			_param[0].Value=部位编码;			
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表Pacs_检查部位所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_检查部位Model> GetPacs_检查部位All()
		{			
			return GetPacs_检查部位All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表Pacs_检查部位所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_检查部位Model> GetPacs_检查部位All(string sqlWhere)
		{
			IList<Pacs_检查部位Model> list=new List<Pacs_检查部位Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere.Replace("'","''");
			}
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="部位编码">部位编码</param>
        /// <returns>是/否</returns>
		public bool IsExist(long 部位编码)
		{			
			OracleParameter[] _param = GetPRMParameters();
            _param[0].Value=部位编码;
            int a = Convert.ToInt32(DbHelperOra.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetPRMParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(PARM_PRM_Pacs_检查部位);

            if (parms == null) {
                parms = new OracleParameter[] {						
					new OracleParameter(":部位编码",OracleDbType.BigInt)
				};
                DbHelperOra.CacheParameters(PARM_PRM_Pacs_检查部位, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetInsertParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_INSERTPacs_检查部位);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":部位编码",OracleDbType.BigInt),
					new OracleParameter(":部位名称",OracleDbType.Varchar2),
					new OracleParameter(":部位索引",OracleDbType.Varchar2),
					new OracleParameter(":部位分类",OracleDbType.Varchar2),
					new OracleParameter(":是否停用",OracleDbType.Varchar2)
					};
                DbHelperOra.CacheParameters(SQL_INSERTPacs_检查部位, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetUpdateParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_UPDATE_Pacs_检查部位_BY_PR);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":部位编码",OracleDbType.BigInt),
					new OracleParameter(":部位名称",OracleDbType.Varchar2),
					new OracleParameter(":部位索引",OracleDbType.Varchar2),
					new OracleParameter(":部位分类",OracleDbType.Varchar2),
					new OracleParameter(":是否停用",OracleDbType.Varchar2)
					};
                DbHelperOra.CacheParameters(SQL_UPDATE_Pacs_检查部位_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

