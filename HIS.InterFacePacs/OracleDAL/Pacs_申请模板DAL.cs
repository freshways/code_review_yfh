﻿// litao@Copy Right 2006-2016
// 文件： Pacs_申请模板.cs
// 创建时间：2018-04-21
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Text;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;

namespace HIS.InterFacePacs.OracleDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.Pacs_申请模板.
    /// </summary>
    public partial class Pacs_申请模板DAL : IPacs_申请模板
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM Pacs_申请模板 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM Pacs_申请模板 WHERE ID=:ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM Pacs_申请模板 WHERE ID=:ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM Pacs_申请模板 WHERE ID=:ID";
		private static readonly string SQL_INSERTPacs_申请模板 = "INSERT INTO Pacs_申请模板 (ID,名称,拼音代码,类别,是否禁用) VALUES (:ID,:名称,:拼音代码,:类别,:是否禁用)";
		private static readonly string SQL_UPDATE_Pacs_申请模板_BY_PR = "UPDATE Pacs_申请模板 SET ,名称=:名称,拼音代码=:拼音代码,类别=:类别,是否禁用=:是否禁用 WHERE ID=:ID";
		private static readonly string PARM_PRM_Pacs_申请模板 = ":ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public Pacs_申请模板DAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="pacs_申请模板Model">Pacs_申请模板实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_申请模板Model pacs_申请模板Model)
		{
			
            OracleParameter[] _param = GetInsertParameters();
			
			_param[0].Value = pacs_申请模板Model.ID;
			_param[1].Value = pacs_申请模板Model.名称;
			_param[2].Value = pacs_申请模板Model.拼音代码;
			_param[3].Value = pacs_申请模板Model.类别;
			_param[4].Value = pacs_申请模板Model.是否禁用;
			
			return DbHelperOra.ExecuteSql(SQL_INSERTPacs_申请模板,_param);
			
		}
		/// <summary>
		/// 向数据表Pacs_申请模板更新一条记录。

		/// </summary>
		/// <param name="pacs_申请模板Model">pacs_申请模板Model</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_申请模板Model pacs_申请模板Model)
		{
            OracleParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=pacs_申请模板Model.ID;
			_param[1].Value=pacs_申请模板Model.名称;
			_param[2].Value=pacs_申请模板Model.拼音代码;
			_param[3].Value=pacs_申请模板Model.类别;
			_param[4].Value=pacs_申请模板Model.是否禁用;
			
			return DbHelperOra.ExecuteSql(SQL_UPDATE_Pacs_申请模板_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表Pacs_申请模板中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			OracleParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return DbHelperOra.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  pacs_申请模板 数据实体1
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>pacs_申请模板 数据实体</returns>
		private Pacs_申请模板Model GetModelFromDr(DataRow row)
		{
			Pacs_申请模板Model Obj = new Pacs_申请模板Model();
			if(row!=null)
			{
				Obj.ID    = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.名称    =  row["名称"].ToString();
				Obj.拼音代码  =  row["拼音代码"].ToString();
				Obj.类别    =  row["类别"].ToString();
				Obj.是否禁用  =  row["是否禁用"].ToString();
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  pacs_申请模板 数据实体2
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>pacs_申请模板 数据实体</returns>
		private Pacs_申请模板Model GetModelFromDr(IDataReader dr)
		{
			Pacs_申请模板Model Obj = new Pacs_申请模板Model();
			
				Obj.ID   = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.名称   =  dr["名称"].ToString();
				Obj.拼音代码 =  dr["拼音代码"].ToString();
				Obj.类别   =  dr["类别"].ToString();
				Obj.是否禁用 =  dr["是否禁用"].ToString();
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个Pacs_申请模板对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>Pacs_申请模板对象</returns>
		public Pacs_申请模板Model GetPacs_申请模板 (int id)
		{
			Pacs_申请模板Model _obj=null;			
			OracleParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表Pacs_申请模板所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_申请模板Model> GetPacs_申请模板All()
		{			
			return GetPacs_申请模板All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表Pacs_申请模板所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<Pacs_申请模板Model> GetPacs_申请模板All(string sqlWhere)
		{
			IList<Pacs_申请模板Model> list=new List<Pacs_申请模板Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere.Replace("'","''");
			}
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			OracleParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(DbHelperOra.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetPRMParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(PARM_PRM_Pacs_申请模板);

            if (parms == null) {
                parms = new OracleParameter[] {						
					new OracleParameter(":ID",OracleDbType.Int32)
				};
                DbHelperOra.CacheParameters(PARM_PRM_Pacs_申请模板, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetInsertParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_INSERTPacs_申请模板);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":名称",OracleDbType.Varchar2),
					new OracleParameter(":拼音代码",OracleDbType.Varchar2),
					new OracleParameter(":类别",OracleDbType.Varchar2),
					new OracleParameter(":是否禁用",OracleDbType.Varchar2)
					};
                DbHelperOra.CacheParameters(SQL_INSERTPacs_申请模板, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetUpdateParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_UPDATE_Pacs_申请模板_BY_PR);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":名称",OracleDbType.Varchar2),
					new OracleParameter(":拼音代码",OracleDbType.Varchar2),
					new OracleParameter(":类别",OracleDbType.Varchar2),
					new OracleParameter(":是否禁用",OracleDbType.Varchar2)
					};
                DbHelperOra.CacheParameters(SQL_UPDATE_Pacs_申请模板_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

