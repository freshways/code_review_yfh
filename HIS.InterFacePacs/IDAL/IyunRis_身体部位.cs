﻿// litao@Copy Right 2006-2008
// 文件： yunRis_身体部位.cs
// 项目名称：项目管理 
// 创建时间：2018-07-15
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;

namespace HIS.InterFacePacs.IDAL
{
    /// <summary>
    /// 数据层 dbo.yunRis_身体部位 接口。

    /// </summary>
    public interface IyunRis_身体部位
    {
		#region 基本方法
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="yunris_身体部位">yunRis_身体部位实体</param>
		/// <returns>新插入记录的编号</returns>
		int Insert(yunRis_身体部位Model yunris_身体部位);
				
		/// <summary>
		/// 向数据表yunRis_身体部位更新一条记录。

		/// </summary>
		/// <param name="yunris_身体部位">yunris_身体部位</param>
		/// <returns>影响的行数</returns>
		int Update(yunRis_身体部位Model yunris_身体部位);
		
		
		/// <summary>
		/// 删除数据表yunRis_身体部位中的一条记录

		/// </summary>
	    /// <param name="iid">iid</param>
		/// <returns>影响的行数</returns>
		int Delete(decimal iid);

		
        /// <summary>
		/// 得到 yunris_身体部位 数据实体
		/// </summary>
		/// <param name="iid">iid</param>
		/// <returns>yunris_身体部位 数据实体</returns>
		yunRis_身体部位Model GetyunRis_身体部位(decimal iid);
		
		
		/// <summary>
		/// 得到数据表yunRis_身体部位所有记录

		/// </summary>
		/// <returns>数据实体</returns>
		IList<yunRis_身体部位Model> GetyunRis_身体部位All();
			
		/// <summary>
		/// 得到数据表yunRis_身体部位符合查询条件的记录

		/// </summary>
		/// <param name="sqlWhere">查询条件</param>
		/// <returns>数据实体</returns>
		IList<yunRis_身体部位Model> GetyunRis_身体部位All(string sqlWhere);		
		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="iid">iid</param>
        /// <returns>是/否</returns>
		bool IsExist(decimal iid);

        #endregion
    }
}
