﻿// litao@Copy Right 2006-2008
// 文件： Pacs_检查申请.cs
// 项目名称：项目管理 
// 创建时间：2018-04-21
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;

namespace HIS.InterFacePacs.IDAL
{
    /// <summary>
    /// 数据层 dbo.Pacs_检查申请 接口。

    /// </summary>
    public interface IPacs_检查申请
    {
		#region 基本方法
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="pacs_检查申请">Pacs_检查申请实体</param>
		/// <returns>新插入记录的编号</returns>
		int Insert(Pacs_检查申请Model pacs_检查申请);
				
		/// <summary>
		/// 向数据表Pacs_检查申请更新一条记录。

		/// </summary>
		/// <param name="pacs_检查申请">pacs_检查申请</param>
		/// <returns>影响的行数</returns>
		int Update(Pacs_检查申请Model pacs_检查申请);
		
		
		/// <summary>
		/// 删除数据表Pacs_检查申请中的一条记录

		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		int Delete(int id);

		
        /// <summary>
		/// 得到 pacs_检查申请 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>pacs_检查申请 数据实体</returns>
		Pacs_检查申请Model GetPacs_检查申请(int id);
		
		
		/// <summary>
		/// 得到数据表Pacs_检查申请所有记录

		/// </summary>
		/// <returns>数据实体</returns>
		IList<Pacs_检查申请Model> GetPacs_检查申请All();
			
		/// <summary>
		/// 得到数据表Pacs_检查申请符合查询条件的记录

		/// </summary>
		/// <param name="sqlWhere">查询条件</param>
		/// <returns>数据实体</returns>
		IList<Pacs_检查申请Model> GetPacs_检查申请All(string sqlWhere);
			
		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		bool IsExist(int id);

        #endregion
    }
}
