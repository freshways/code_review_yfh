﻿// litao@Copy Right 2006-2008
// 文件： Pacs_执行科室.cs
// 项目名称：项目管理 
// 创建时间：2018-04-20
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;

namespace HIS.InterFacePacs.IDAL
{
    /// <summary>
    /// 数据层 dbo.Pacs_执行科室 接口。

    /// </summary>
    public interface IPacs_执行科室
    {
		#region 基本方法
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="pacs_执行科室">Pacs_执行科室实体</param>
		/// <returns>新插入记录的编号</returns>
		int Insert(Pacs_执行科室Model pacs_执行科室);
				
		/// <summary>
		/// 向数据表Pacs_执行科室更新一条记录。

		/// </summary>
		/// <param name="pacs_执行科室">pacs_执行科室</param>
		/// <returns>影响的行数</returns>
		int Update(Pacs_执行科室Model pacs_执行科室);
		
		
		/// <summary>
		/// 删除数据表Pacs_执行科室中的一条记录

		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		int Delete(int id);

		
        /// <summary>
		/// 得到 pacs_执行科室 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>pacs_执行科室 数据实体</returns>
		Pacs_执行科室Model GetPacs_执行科室(int id);
		
		
		/// <summary>
		/// 得到数据表Pacs_执行科室所有记录

		/// </summary>
		/// <returns>数据实体</returns>
		IList<Pacs_执行科室Model> GetPacs_执行科室All();
			
		/// <summary>
		/// 得到数据表Pacs_执行科室符合查询条件的记录

		/// </summary>
		/// <param name="sqlWhere">查询条件</param>
		/// <returns>数据实体</returns>
		IList<Pacs_执行科室Model> GetPacs_执行科室All(string sqlWhere);
			
		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		bool IsExist(int id);

        #endregion
    }
}

