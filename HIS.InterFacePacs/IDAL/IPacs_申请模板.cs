﻿// litao@Copy Right 2006-2008
// 文件： Pacs_申请模板.cs
// 项目名称：项目管理 
// 创建时间：2018-04-21
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;

namespace HIS.InterFacePacs.IDAL
{
    /// <summary>
    /// 数据层 dbo.Pacs_申请模板 接口。

    /// </summary>
    public interface IPacs_申请模板
    {
		#region 基本方法
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="pacs_申请模板">Pacs_申请模板实体</param>
		/// <returns>新插入记录的编号</returns>
		int Insert(Pacs_申请模板Model pacs_申请模板);
				
		/// <summary>
		/// 向数据表Pacs_申请模板更新一条记录。

		/// </summary>
		/// <param name="pacs_申请模板">pacs_申请模板</param>
		/// <returns>影响的行数</returns>
		int Update(Pacs_申请模板Model pacs_申请模板);
		
		
		/// <summary>
		/// 删除数据表Pacs_申请模板中的一条记录

		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		int Delete(int id);

		
        /// <summary>
		/// 得到 pacs_申请模板 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>pacs_申请模板 数据实体</returns>
		Pacs_申请模板Model GetPacs_申请模板(int id);
		
		
		/// <summary>
		/// 得到数据表Pacs_申请模板所有记录

		/// </summary>
		/// <returns>数据实体</returns>
		IList<Pacs_申请模板Model> GetPacs_申请模板All();
			
		/// <summary>
		/// 得到数据表Pacs_申请模板符合查询条件的记录

		/// </summary>
		/// <param name="sqlWhere">查询条件</param>
		/// <returns>数据实体</returns>
		IList<Pacs_申请模板Model> GetPacs_申请模板All(string sqlWhere);
			
		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		bool IsExist(int id);

        #endregion
    }
}
