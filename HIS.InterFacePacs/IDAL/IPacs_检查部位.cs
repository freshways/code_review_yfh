﻿// litao@Copy Right 2006-2008
// 文件： Pacs_检查部位.cs
// 项目名称：项目管理 
// 创建时间：2018-04-20
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;

namespace HIS.InterFacePacs.IDAL
{
    /// <summary>
    /// 数据层 dbo.Pacs_检查部位 接口。

    /// </summary>
    public interface IPacs_检查部位
    {
		#region 基本方法
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="pacs_检查部位">Pacs_检查部位实体</param>
		/// <returns>新插入记录的编号</returns>
		int Insert(Pacs_检查部位Model pacs_检查部位);
				
		/// <summary>
		/// 向数据表Pacs_检查部位更新一条记录。

		/// </summary>
		/// <param name="pacs_检查部位">pacs_检查部位</param>
		/// <returns>影响的行数</returns>
		int Update(Pacs_检查部位Model pacs_检查部位);
		
		
		/// <summary>
		/// 删除数据表Pacs_检查部位中的一条记录

		/// </summary>
	    /// <param name="部位编码">部位编码</param>
		/// <returns>影响的行数</returns>
		int Delete(long 部位编码);

		
        /// <summary>
		/// 得到 pacs_检查部位 数据实体
		/// </summary>
		/// <param name="部位编码">部位编码</param>
		/// <returns>pacs_检查部位 数据实体</returns>
		Pacs_检查部位Model GetPacs_检查部位(long 部位编码);
		
		
		/// <summary>
		/// 得到数据表Pacs_检查部位所有记录

		/// </summary>
		/// <returns>数据实体</returns>
		IList<Pacs_检查部位Model> GetPacs_检查部位All();
			
		/// <summary>
		/// 得到数据表Pacs_检查部位符合查询条件的记录

		/// </summary>
		/// <param name="sqlWhere">查询条件</param>
		/// <returns>数据实体</returns>
		IList<Pacs_检查部位Model> GetPacs_检查部位All(string sqlWhere);
			
		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="部位编码">部位编码</param>
        /// <returns>是/否</returns>
		bool IsExist(long 部位编码);

        #endregion
    }
}

