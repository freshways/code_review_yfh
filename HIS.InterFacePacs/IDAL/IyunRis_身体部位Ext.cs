﻿// litao@Copy Right 2006-2008
// 文件： yunRis_身体部位.cs
// 项目名称：项目管理 
// 创建时间：2018-07-15
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;

namespace HIS.InterFacePacs.IDAL
{
    /// <summary>
    /// 数据层 dbo.yunRis_身体部位 接口。

    /// </summary>
    public interface IyunRis_身体部位Ext
    {
		#region 基本方法		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="iid">iid</param>
        /// <returns>是/否</returns>
        bool IsExist(string _设备类型, string _部位编码);

        /// <summary>
        /// 根据设备获取最大部位编号
        /// </summary>
        /// <param name="_设备类型"></param>
        /// <returns></returns>
        string GetMaxID(string _设备类型);
        #endregion
    }
}
