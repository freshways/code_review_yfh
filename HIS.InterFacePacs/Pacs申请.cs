﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;

namespace HIS.InterFacePacs
{
    public static class Pacs申请
    {
        public static void sPACS护理执行申请单写ORA(HIS.COMM.Person住院病人 zyPerson)
        {
            // 2016-03-18 17:54:23 于凤海-停用，传输方式改为视图由pacs直接取
            return;
            //从数据库参数中获取，如果是空则返回
            //string PacsOracleConn = @"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.10.107)(PORT=1521)) (CONNECT_DATA=(SERVICE_NAME=SPECTRA)));User Id=hishis;Password=hishis";
            if (!string.IsNullOrEmpty(ClassConnStrings.sConnOraPacsDB))
            {
                HIS.Model.Dal.OracleHelper.connectionString = ClassConnStrings.sConnOraPacsDB; //动态赋值数据库连接

                string sqlhis = @"select * from Pacs_Inteface where 状态=0 and 病人ID='" + zyPerson.sZYID + "' ";
                DataTable dtPACS申请列表 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sqlhis).Tables[0];
                if (dtPACS申请列表 != null && dtPACS申请列表.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPACS申请列表.Rows.Count; i++)
                    {
                        string _sHISorder = WEISHENG.COMM.zdInfo.Model单位信息.iDwid.ToString() +
                                     dtPACS申请列表.Rows[i]["医嘱ID"].ToString();
                        string sqlext = "select count(1)  from eris.eris_hisorder where IHISORDER_IID='" + _sHISorder + "' ";
                        int irowext = Convert.ToInt32(HIS.Model.Dal.OracleHelper.ExecuteScalar(ClassConnStrings.sConnOraPacsDB, CommandType.Text, sqlext));
                        if (irowext == 0)
                        {
                            string sql1 = @" insert into eris.eris_hisorder
                                  (IHISORDER_IID ,CORDER_INDEX ,CPATWL_KEY ,CTRIGGER_DTTM ,CREPLICA_DTTM ,CEXAM_STATUS ,CPATIENT_ID ,CNAME ,CSEX ,CAGE ,
                                   CAGEDW ,DDATE_OF_BIRTH ,CID_NO ,CWARD_NAME ,CBED_NO ,CREQ_DEPT ,CREQ_DOCTORID ,CREQ_PHYSICIAN ,CCLIN_SYNP ,CCLIN_DIAG ,
                                   CEXAM_CLASS ,CEXAM_ITEM ,CCOSTS ,CFSOURCE ,CHIS_TYPE ,CHIS_ID ,CHIS_PID ,CHIS_ZYH)
                                values ('" + _sHISorder + "','" +
                                         dtPACS申请列表.Rows[i]["申请单号"].ToString() + "','" +
                                         zyPerson.s住院号码 + "','" +
                                          DateTime.Now.ToString("yyyyMMddHHmmss") + "'," +
                                          "'ANY','0'," +
                                          zyPerson.s住院号码 + ",'" +
                                          zyPerson.S姓名 + "','" +
                                          zyPerson.S性别 + "','" +
                                          zyPerson.S年龄 + "','岁','" +
                                          zyPerson.s出生日期 + "','" +
                                          zyPerson.S身份证号 + "','" +
                                          zyPerson.s病区名称 + "','" +
                                          zyPerson.S床位名称 + "','" +
                                          zyPerson.S科室名称 + "','" +
                                          zyPerson.s主治医生编码 + "','" +
                                          zyPerson.s主治医生姓名 + "','" +
                                          dtPACS申请列表.Rows[i]["体征描述"].ToString() + "','" +
                                          zyPerson.s疾病名称 + "','" +
                                          dtPACS申请列表.Rows[i]["执行科室"].ToString() + "','" +
                                          dtPACS申请列表.Rows[i]["类型"].ToString() + "','" +
                                          dtPACS申请列表.Rows[i]["检查费"].ToString() + "','2','" +
                                          WEISHENG.COMM.zdInfo.Model单位信息.iDwid + "','" +
                                          dtPACS申请列表.Rows[i]["医嘱ID"].ToString() + "','" +
                                          zyPerson.sZYID + "','" +
                                          zyPerson.s住院号码 + "' ) ";
                            string sql2 = @"insert into eris.eris_hisorder_detail(IHISORDER_IID ,ISL ,IDJ ,CMC ,CEXAM_SUB_CLASS_CODE,CEXAM_SUB_CLASS ,CHIS_TYPE)
                                values('" + _sHISorder + "','1','" +
                                              dtPACS申请列表.Rows[i]["检查费"].ToString() + "','" +
                                              dtPACS申请列表.Rows[i]["类型"].ToString() + "','" +
                                              dtPACS申请列表.Rows[i]["检查部位编码"].ToString() + "','" +
                                              dtPACS申请列表.Rows[i]["检查部位"].ToString() + "','" +
                                              WEISHENG.COMM.zdInfo.Model单位信息.iDwid + "') ";

                            System.Collections.ArrayList alist = new System.Collections.ArrayList();
                            try
                            {
                                alist.Add(sql1);
                                alist.Add(sql2);
                                if (!HIS.Model.Dal.OracleHelper.ExecuteSqlTran(ClassConnStrings.sConnOraPacsDB, alist))
                                {
                                    WEISHENG.COMM.LogHelper.Info("英菲达pacs", "错误信息", "护理执行写ORA失败,事务提交失败");
                                }
                                //int irow = HIS.Model.Dal.OracleHelper.ExecuteNonQuery(ClassConnStrings.sConnOraPacsDB, CommandType.Text, "begin " + sql1 + sql2 +
                                //                    " end;");                                
                            }
                            catch (Exception ex)
                            {
                                WEISHENG.COMM.LogHelper.Info("英菲达pacs", "错误信息", "护理执行写ORA1次失败" + ex.Message);
                                try
                                {
                                    System.Threading.Thread.Sleep(1000); //让其等待1秒后再次执行
                                    HIS.Model.Dal.OracleHelper.ExecuteSqlTran(ClassConnStrings.sConnOraPacsDB, alist);
                                }
                                catch (Exception ex2)
                                {
                                    //DevExpress.XtraEditors.XtraMessageBox.Show("写入英菲达Pacs失败！" + ex2.Message, "消息");
                                    WEISHENG.COMM.LogHelper.Info("英菲达pacs", "错误信息", "护理执行写ORA2次失败" + ex2.Message);
                                    return;
                                }
                            }

                            try
                            {
                                string sqlup = "update Pacs_Inteface set 状态=1 where  id =" + dtPACS申请列表.Rows[i]["ID"].ToString();
                                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sqlup);
                            }
                            catch (Exception ex)
                            {
                                WEISHENG.COMM.LogHelper.Info("pacs接口", "错误信息", "更新状态失败" + ex.Message);
                            }
                        }
                    }
                }
                else { return; }

            }
        }

        public static void v住院PACS申请单(DataRow _dr医嘱记录, HIS.COMM.Person住院病人 person)
        {
            if (Convert.ToString(_dr医嘱记录["项目编码"]) != "" && Convert.ToString(_dr医嘱记录["执行时间"]) == "")
            {
                string sqlPACS对照 =
                    "select aa.*, bb.[科室名称]" + "\r\n" +
                    "from   gy收费小项 aa left outer join [GY科室设置] bb on aa.[执行科室编码] = bb.[科室编码]" + "\r\n" +
                    "where  aa.pacs检查项 = 1  and aa.[收费编码]=" + Convert.ToString(_dr医嘱记录["项目编码"]);

                DataTable dtPACS对照 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sqlPACS对照).Tables[0];
                if (dtPACS对照 != null && dtPACS对照.Rows.Count > 0)
                {
                    string s申请单号 = "";
                    //插入申请单，执行存储过程,马站单独处理
                    if (WEISHENG.COMM.zdInfo.Model单位信息.iDwid == 109)
                    {
                        string insert = @"uSp_检查申请单同步 @s操作标志=0,@s操作时间='" + Convert.ToString(_dr医嘱记录["开嘱时间"]) + "',@s操作人=" + WEISHENG.COMM.zdInfo.ModelUserInfo.用户名 +
                        ",@s病人类型=1,@s住院号=" + person.s住院号码 + ",@s病人ID=" + person.sZYID +
                        ",@s申请明细记录号=" + Convert.ToString(_dr医嘱记录["ID"]) + ",@s申请项目代码=" + Convert.ToString(_dr医嘱记录["项目编码"]);

                        DataTable dtRow = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, insert).Tables[0];
                        if (dtRow != null && dtRow.Rows.Count > 0)
                        {
                            if (dtRow.Rows[0][0].ToString() == "-1")
                            {
                                Exception ex = new Exception("检查申请单保存失败");
                                throw ex;
                                //XtraMessageBox.Show("！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //return;
                            }
                            if (dtRow.Rows[0][0].ToString() == "1")
                            {
                                s申请单号 = dtRow.Rows[0][1].ToString();
                            }
                            if (dtRow.Rows[0][0].ToString() == "9")//已申请，继续判断是否需要弹出录入检查部位
                            {
                                s申请单号 = dtRow.Rows[0][1].ToString();
                                if (dtRow.Rows[0][2].ToString() != "" && dtRow.Rows[0][3].ToString() != "")
                                {
                                    return;
                                }
                            }
                        }
                        if (s申请单号 != "")
                        {
                            //弹出维护部位和病人描述页面，更新申请单信息
                            XtraFormPacs申请录入 xtr = new XtraFormPacs申请录入(dtPACS对照, s申请单号, person);
                            xtr.ShowDialog();
                        }
                    }
                    else//英飞达接口医院
                    {
                        if (bInfinitt写住院申请单(_dr医嘱记录, person, out s申请单号) == false)
                        {
                            return;
                        }
                        XtraFormPacs申请录入 xtr = new XtraFormPacs申请录入(dtPACS对照, s申请单号, person);
                        xtr.ShowDialog();
                    }
                }
            }

        }


        public static void v门诊医生PACS申请单(DataTable _dt门诊处方, HIS.COMM.Person门诊病人 _mzPerson)
        {
            try
            {
                bool b是否提交 = true;
                DataTable dt检验申请 = HIS.Model.Dal.autoDb.GetNewDataTable(_dt门诊处方, "YPID=0 AND PACS检查项=1");
                DataTable dt申请序列 = dt检验申请.DefaultView.ToTable(true, "费用序号", "项目名称", "金额");
                foreach (DataRow row摘要 in dt申请序列.Rows)
                {
                    string sqlPACS对照 =
                      "select aa.*" + "\r\n" +
                      "from   gy收费小项 aa " + "\r\n" +
                      "where  aa.pacs检查项 = 1 and aa.[收费编码]=" + Convert.ToString(row摘要["费用序号"]);

                    DataTable dtPACS对照 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sqlPACS对照).Tables[0];
                    if (dtPACS对照 != null && dtPACS对照.Rows.Count > 0)
                    {
                        string s申请单号;
                        if (bInfinitt写门诊申请单(row摘要, _mzPerson, out s申请单号) == false)
                        {
                            return;
                        }
                        bool show = false;
                        if (_mzPerson.S性别 == "女" && (dtPACS对照.Rows[0]["归并名称"].ToString() == "B超费" || dtPACS对照.Rows[0]["归并名称"].ToString() == "彩超"))
                            show = true;
                        XtraFormPacs申请录入 xtr = new XtraFormPacs申请录入(dtPACS对照, s申请单号, _mzPerson.S临床诊断, show, _mzPerson);
                        xtr.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //return false;
            }
        }
        /// <summary>
        /// 流程修改，改为RIS调用HIS试图，不在更新RIS表
        /// </summary>
        /// <param name="mzPerson"></param>
        /// <returns></returns>
        public static bool v门诊收款写ORA(HIS.COMM.personYB mzPerson)
        {
            // 2016-03-18 17:54:23 于凤海-停用，传输方式改为视图由pacs直接取
            return true;

            bool re = false;
            if (!string.IsNullOrEmpty(ClassConnStrings.sConnOraPacsDB))
            {
                //HIS.Model.Dal.OracleHelper.connectionString = ClassConnStrings.sConnOraPacsDB; //动态赋值数据库连接
                string sqlhis = @"select * from Pacs_Inteface where 状态=0 and 病人ID='" + mzPerson.sMZID + "' ";
                DataTable dtPACS申请列表 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sqlhis).Tables[0];
                if (dtPACS申请列表 != null && dtPACS申请列表.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPACS申请列表.Rows.Count; i++)
                    {
                        string _sHISorder = WEISHENG.COMM.zdInfo.Model单位信息.iDwid.ToString() +
                                     dtPACS申请列表.Rows[i]["申请单号"].ToString();
                        string sqlext = "select count(1)  from eris.eris_hisorder where IHISORDER_IID='" + _sHISorder + "' ";
                        int irowext = Convert.ToInt32(HIS.Model.Dal.OracleHelper.ExecuteScalar(ClassConnStrings.sConnOraPacsDB, CommandType.Text, sqlext));
                        if (irowext == 0)
                        {
                            string sql1 = @" insert into eris.eris_hisorder
                                  (IHISORDER_IID ,CORDER_INDEX ,CPATWL_KEY ,CTRIGGER_DTTM ,CREPLICA_DTTM ,CEXAM_STATUS ,CPATIENT_ID ,CNAME ,CSEX ,CAGE ,
                                   CAGEDW ,DDATE_OF_BIRTH ,CID_NO ,CWARD_NAME ,CBED_NO ,CREQ_DEPT ,CREQ_DOCTORID ,CREQ_PHYSICIAN ,CCLIN_SYNP ,CCLIN_DIAG ,
                                   CEXAM_CLASS ,CEXAM_ITEM ,CCOSTS ,CFSOURCE ,CHIS_TYPE ,CHIS_ID ,CHIS_PID ,CHIS_MZH)
                                values ('" + _sHISorder + "','" +
                                         dtPACS申请列表.Rows[i]["申请单号"].ToString() + "','" +
                                         mzPerson.sMZID + "','" +
                                          DateTime.Now.ToString("yyyyMMddHHmmss") + "'," +
                                          "'ANY','0'," +
                                          mzPerson.sMZID + ",'" +
                                          mzPerson.S姓名 + "','" +
                                          mzPerson.S性别 + "','" +
                                          mzPerson.S年龄 + "','岁',to_date('" +
                                          mzPerson.s出生日期 + "','yyyy-MM-dd'),'" +
                                          mzPerson.S身份证号 + "','-1','-1','" +
                                          mzPerson.S科室 + "','" +
                                          mzPerson.S医生编码 + "','" +
                                          mzPerson.s医生姓名 + "','" +
                                          dtPACS申请列表.Rows[i]["体征描述"].ToString() + "','" +
                                          mzPerson.s疾病名称 + "','" +
                                          dtPACS申请列表.Rows[i]["执行科室"].ToString() + "','" +
                                          dtPACS申请列表.Rows[i]["类型"].ToString() + "','" +
                                          dtPACS申请列表.Rows[i]["检查费"].ToString() + "','1','" +
                                          WEISHENG.COMM.zdInfo.Model单位信息.iDwid + "','" +
                                          dtPACS申请列表.Rows[i]["申请单号"].ToString() + "','" +
                                          mzPerson.sMZID + "','" + mzPerson.sMZID + "' )  ";
                            string sql2 = @"insert into eris.eris_hisorder_detail(IHISORDER_IID ,ISL ,IDJ ,CMC ,CEXAM_SUB_CLASS_CODE, CEXAM_SUB_CLASS ,CHIS_TYPE)
                                values('" + _sHISorder + "','1','" +
                                              dtPACS申请列表.Rows[i]["检查费"].ToString() + "','" +
                                              dtPACS申请列表.Rows[i]["类型"].ToString() + "','" +
                                              dtPACS申请列表.Rows[i]["检查部位编码"].ToString() + "','" +
                                              dtPACS申请列表.Rows[i]["检查部位"].ToString() + "','" +
                                              WEISHENG.COMM.zdInfo.Model单位信息.iDwid + "') ";
                            System.Collections.ArrayList alist = new System.Collections.ArrayList();
                            try
                            {
                                alist.Add(sql1);
                                alist.Add(sql2);
                                if (!HIS.Model.Dal.OracleHelper.ExecuteSqlTran(ClassConnStrings.sConnOraPacsDB, alist))
                                {
                                    WEISHENG.COMM.LogHelper.Info("英菲达pacs", "错误信息", "门诊收款写ORA失败,事务提交失败");
                                    re = false;
                                }
                                //int irow = HIS.Model.Dal.OracleHelper.ExecuteNonQuery(ClassConnStrings.sConnOraPacsDB, CommandType.Text, "begin " + sql1 + sql2 +
                                //                    " end;");
                            }
                            catch (Exception ex)
                            {
                                WEISHENG.COMM.LogHelper.Info("英菲达pacs", "错误信息", "门诊收款写ORA1次失败" + ex.Message);
                                try
                                {
                                    System.Threading.Thread.Sleep(1000); //让其等待1秒后再次执行
                                    HIS.Model.Dal.OracleHelper.ExecuteSqlTran(ClassConnStrings.sConnOraPacsDB, alist);
                                }
                                catch (Exception ex2)
                                {
                                    //DevExpress.XtraEditors.XtraMessageBox.Show("写入英菲达Pacs失败！" + ex2.Message, "消息");
                                    WEISHENG.COMM.LogHelper.Info("英菲达pacs", "错误信息", "门诊收款写ORA2次失败" + ex2.Message);
                                    return false;
                                }
                            }
                            try
                            {
                                string sqlup = "update Pacs_Inteface set 状态=1 where  id =" + dtPACS申请列表.Rows[i]["ID"].ToString();
                                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, sqlup);
                            }
                            catch (Exception ex)
                            {
                                WEISHENG.COMM.LogHelper.Info("pacs接口", "错误信息", "更新状态失败" + ex.Message);
                            }
                        }
                    }
                    re = true;
                }
                else
                {
                    re = false;
                }
            }
            return re;
        }

        /// <summary>
        /// 生成住院PACS申请单号的算法
        /// </summary>
        /// <returns></returns>
        public static string s住院新申请单据号()
        {
            string SQL =
                "	DECLARE @s新单据号 VARCHAR(12),@s操作时间 varchar(20)  select @s操作时间=getdate()" + "\r\n" +
                "		SELECT @s新单据号=ISNULL(MAX(SUBSTRING(申请单号,LEN(申请单号)-4,5)),0)+1" + "\r\n" +
                "			FROM Pacs_检查申请 " + "\r\n" +
                "			WHERE DATEDIFF(dd,申请日期,getdate())=0  and LEFT(申请单号,1)='1'" + "\r\n" +
            "		SELECT  '1'+SUBSTRING(CONVERT(CHAR(8),CONVERT(DATETIME,@s操作时间),112),3,8)+" + "\r\n" +
            "		STUFF('00000',LEN('00000')-LEN(@s新单据号)+1,LEN(@s新单据号),@s新单据号)";

            string temp = HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL).ToString();
            return temp;
        }
        static string s门诊新申请单据号()
        {
            string SQL =
                "	DECLARE @s新单据号 VARCHAR(12),@s操作时间 varchar(20)  select @s操作时间=getdate()" + "\r\n" +
                "		SELECT @s新单据号=ISNULL(MAX(SUBSTRING(申请单号,LEN(申请单号)-4,5)),0)+1" + "\r\n" +
                "			FROM Pacs_检查申请 " + "\r\n" +
                "			WHERE DATEDIFF(dd,申请日期,getdate())=0  and LEFT(申请单号,1)='2'" + "\r\n" +
            "		SELECT  '2'+SUBSTRING(CONVERT(CHAR(8),CONVERT(DATETIME,@s操作时间),112),3,8)+" + "\r\n" +
            "		STUFF('00000',LEN('00000')-LEN(@s新单据号)+1,LEN(@s新单据号),@s新单据号)";

            string temp = HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL).ToString();
            return temp;
        }

        public static string Get申请单号New(string types)
        {
            return types + DateTime.Now.ToString("yyMMddss") + (new Random()).Next(100, 999).ToString();
        }

        static bool bInfinitt写住院申请单(DataRow _dr医嘱记录, HIS.COMM.Person住院病人 person, out string s申请单号)
        {
            try
            {
                s申请单号 = "";
                string SQL已保存 =
                     "SELECT count(1) FROM Pacs_Inteface WHERE 状态=0 and 病人来源='住院' AND 医嘱ID=" + Convert.ToString(_dr医嘱记录["ID"]);
                int iRE = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL已保存).Tables[0].Rows[0][0]);
                if (iRE > 0)
                {
                    return false;
                }
                //2016-03-11 14:43:10 于凤海 修改-只取单价
                string SQL = "SELECT  A.单价, A.收费名称 FROM GY收费小项 A WHERE A.收费编码 =" + Convert.ToString(_dr医嘱记录["项目编码"]);
                //2016-03-11 14:43:10 于凤海 修改-取pacs执行科室
                string SQL执行科室 = "select 收费编码,收费名称,科室名称 from PACS执行科室 a,GY科室设置 b where a.执行科室编码 = b.科室编码 " +
                    " and 收费编码=" + Convert.ToString(_dr医嘱记录["项目编码"]);
                DataSet ds = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL + SQL执行科室);
                DataTable dt申请项目 = ds.Tables[0];
                DataTable dt执行科室 = ds.Tables[1];
                string s单价 = "0", s收费名称 = "", s科室名称 = "未定义";
                if (dt申请项目 != null && dt申请项目.Rows.Count > 0)
                {
                    s单价 = dt申请项目.Rows[0]["单价"].ToString();
                    s收费名称 = dt申请项目.Rows[0]["收费名称"].ToString();
                }
                if (dt执行科室 != null && dt执行科室.Rows.Count > 0)
                    s科室名称 = dt执行科室.Rows[0]["科室名称"].ToString();

                s申请单号 = s住院新申请单据号();
                string SQL2 =
                    "INSERT INTO Pacs_Inteface" + "\r\n" +
                    "				   (病人ID ,申请单号 ,病人来源 ,病人姓名 ,性别 ,病人年龄" + "\r\n" +
                    "				   ,检查部位 ,申请日期 ,申请科室 ,申请医生 ,检查费 ,住院号" + "\r\n" +
                    "				   ,门诊号 ,床号 ,电话号码 ,类型 ,临床诊断 ,体征描述 ,执行科室,医嘱ID,状态)" + "\r\n" +
                    "	SELECT  A.ZYID ," + s申请单号 + " ,'住院' ,A.病人姓名 ,A.性别 ,DATEDIFF(YY,A.出生日期,GETDATE()) ," + "\r\n" +
                    "				'' JCBW,getdate() ,b.科室名称 , '" + WEISHENG.COMM.zdInfo.ModelUserInfo.用户名 + "' ," + s单价 + " JCF,a.住院号码 , '-1',a.病床 , a.联系电话 ," + "\r\n" +
                    "				'" + s收费名称 + "' LX,a.疾病名称 LCZD,'' ,'" + s科室名称 + "' ," + Convert.ToString(_dr医嘱记录["ID"]) + ",0" + "\r\n" +
                    "				FROM ZY病人信息 A,GY科室设置 B " + "\r\n" +
                    "				WHERE A.科室 = B.科室编码 AND A.zyid=" + person.sZYID;

                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL2);

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static bool bInfinitt写门诊申请单(DataRow _dr门诊处方, HIS.COMM.personYB person, out string s申请单号)
        {
            try
            {
                string s单价 = _dr门诊处方["金额"].ToString();// dt申请项目.Rows[0]["单价"].ToString();
                string s收费名称 = _dr门诊处方["项目名称"].ToString();//dt申请项目.Rows[0]["收费名称"].ToString();
                s申请单号 = "";
                string SQL已保存 =
                     "SELECT count(1) FROM Pacs_Inteface WHERE 状态=0 and 病人来源='门诊' AND 病人ID=" + person.sMZID + " and 类型='" + s收费名称 + "'";
                int iRE = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL已保存).Tables[0].Rows[0][0]);
                if (iRE > 0)
                {
                    return false;
                }

                string SQL执行科室 = "select 收费编码,收费名称,科室名称 from PACS执行科室 a,GY科室设置 b where a.执行科室编码 = b.科室编码 " +
                    " and 收费编码=" + Convert.ToString(_dr门诊处方["费用序号"]);
                DataTable dt执行科室 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL执行科室).Tables[0];
                string s科室名称 = "未定义";
                if (dt执行科室 != null && dt执行科室.Rows.Count > 0)
                    s科室名称 = dt执行科室.Rows[0]["科室名称"].ToString();

                s申请单号 = s门诊新申请单据号();
                string SQL2 =
                    "INSERT INTO Pacs_Inteface(病人ID, 申请单号, 病人来源, 病人姓名, 性别, 病人年龄, 检查部位, 申请日期," + "\r\n" +
                    "              申请科室, 申请医生, 检查费, 住院号, 门诊号, 床号, 身份证号, 类型," + "\r\n" +
                    "              临床诊断, 体征描述, 执行科室, 医嘱ID, 状态,分院编码)" + "\r\n" +
                    "  SELECT A.MZID 病人ID, " + s申请单号 + " 申请单号, '门诊' 病人来源, A.病人姓名, A.性别, a.年龄," + "\r\n" +
                    "         '' 检查部位, getdate() 申请日期, bb.科室名称, '" + WEISHENG.COMM.zdInfo.ModelUserInfo.用户名 + "' 申请医生, " + s单价 + " 检查费, '-1' 住院号," + "\r\n" +
                    "         a.MZID 门诊号, '-1' 床号, a.身份证号, '" + s收费名称 + "' 类型, a.临床诊断, '' 体征描述," + "\r\n" +
                    "         '" + s科室名称 + "' 执行科室, -1 医嘱ID, 0 状态," + WEISHENG.COMM.zdInfo.Model站点信息.分院编码 + "\r\n" +
                    "  FROM   MF门诊摘要 A left outer join pubUser BB on a.医生编码 = bb.用户编码" + "\r\n" +
                    "  WHERE  A.MZID = " + person.sMZID;
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL2);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
