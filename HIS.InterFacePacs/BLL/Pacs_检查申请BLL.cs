﻿using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;
using HIS.InterFacePacs.DALFactory;

namespace HIS.InterFacePacs.BLL
{
    /// <summary>
    /// BLL Layer For dbo.Pacs_检查申请.
    /// </summary>
    public partial class Pacs_检查申请BLL
    {
		public static readonly IPacs_检查申请 dal = DataAccess.CreatePacs_检查申请();		
		#region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public Pacs_检查申请BLL()
		{
		}
		#endregion

        #region ----------函数定义----------
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="pacs_检查申请">Pacs_检查申请??</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_检查申请Model pacs_检查申请)
		{
			// Validate input
			if (pacs_检查申请 == null) return 0;
			// Use the dal to insert a new record 
			return dal.Insert(pacs_检查申请);
		}
		
		/// <summary>
		/// 向数据表Pacs_检查申请更新一条记录。
		/// </summary>
		/// <param name="pacs_检查申请">pacs_检查申请</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_检查申请Model pacs_检查申请)
		{
            // Validate input
			if (pacs_检查申请==null) return -1;
			// Use the dal to update a new record 
			return dal.Update(pacs_检查申请);
		}
		
		/// <summary>
		/// 删除数据表Pacs_检查申请中的一条记录
		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			// Validate input
			if(id<0)	return 0;
			return dal.Delete(id);
		}
		#endregion
		
        /// <summary>
		/// 得到 pacs_检查申请 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>pacs_检查申请 数据实体</returns>
		public Pacs_检查申请Model GetPacs_检查申请(int id)
		{
			// Validate input
			if(id<0)	return null;

			// Use the dal to get a record 
			return dal.GetPacs_检查申请(id);
		}
		
		/// <summary>
		/// 得到数据表Pacs_检查申请所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_检查申请Model> GetPacs_检查申请All()
		{
			return dal.GetPacs_检查申请All();
		}
		
		/// <summary>
		/// 得到数据表Pacs_检查申请符合条件的所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_检查申请Model> GetPacs_检查申请All(string sqlWhere)
		{
			return dal.GetPacs_检查申请All(sqlWhere);
		}				
		
		/// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{
			return dal.IsExist(id);
		}

        #endregion		
    }
}

