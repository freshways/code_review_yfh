﻿using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;
using HIS.InterFacePacs.DALFactory;

namespace HIS.InterFacePacs.BLL
{
    /// <summary>
    /// BLL Layer For dbo.GY收费小项.
    /// </summary>
    public partial class GY收费小项BLL
    {
		public static readonly IGY收费小项 dal = DataAccess.CreateGY收费小项();		
		#region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public GY收费小项BLL()
		{
		}
		#endregion

        #region ----------函数定义----------
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="gy收费小项">GY收费小项??</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(GY收费小项Model gy收费小项)
		{
			// Validate input
			if (gy收费小项 == null) return 0;
			// Use the dal to insert a new record 
			return dal.Insert(gy收费小项);
		}
		
		/// <summary>
		/// 向数据表GY收费小项更新一条记录。
		/// </summary>
		/// <param name="gy收费小项">gy收费小项</param>
		/// <returns>影响的行数</returns>
		public int Update(GY收费小项Model gy收费小项)
		{
            // Validate input
			if (gy收费小项==null) return -1;
			// Use the dal to update a new record 
			return dal.Update(gy收费小项);
		}
		
		/// <summary>
		/// 删除数据表GY收费小项中的一条记录
		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			// Validate input
			if(id<0)	return 0;
			return dal.Delete(id);
		}
		#endregion
		
        /// <summary>
		/// 得到 gy收费小项 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>gy收费小项 数据实体</returns>
		public GY收费小项Model GetGY收费小项(int id)
		{
			// Validate input
			if(id<0)	return null;

			// Use the dal to get a record 
			return dal.GetGY收费小项(id);
		}
		
		/// <summary>
		/// 得到数据表GY收费小项所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<GY收费小项Model> GetGY收费小项All()
		{
			return dal.GetGY收费小项All();
		}
		
		/// <summary>
		/// 得到数据表GY收费小项符合条件的所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<GY收费小项Model> GetGY收费小项All(string sqlWhere)
		{
			return dal.GetGY收费小项All(sqlWhere);
		}				
		
		/// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{
			return dal.IsExist(id);
		}

        #endregion		
    }
}

