﻿using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;
using HIS.InterFacePacs.DALFactory;

namespace HIS.InterFacePacs.BLL
{
    /// <summary>
    /// BLL Layer For dbo.Pacs_申请模板.
    /// </summary>
    public partial class Pacs_申请模板BLL
    {
		public static readonly IPacs_申请模板 dal = DataAccess.CreatePacs_申请模板();		
		#region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public Pacs_申请模板BLL()
		{
		}
		#endregion

        #region ----------函数定义----------
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="pacs_申请模板">Pacs_申请模板??</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_申请模板Model pacs_申请模板)
		{
			// Validate input
			if (pacs_申请模板 == null) return 0;
			// Use the dal to insert a new record 
			return dal.Insert(pacs_申请模板);
		}
		
		/// <summary>
		/// 向数据表Pacs_申请模板更新一条记录。
		/// </summary>
		/// <param name="pacs_申请模板">pacs_申请模板</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_申请模板Model pacs_申请模板)
		{
            // Validate input
			if (pacs_申请模板==null) return -1;
			// Use the dal to update a new record 
			return dal.Update(pacs_申请模板);
		}
		
		/// <summary>
		/// 删除数据表Pacs_申请模板中的一条记录
		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			// Validate input
			if(id<0)	return 0;
			return dal.Delete(id);
		}
		#endregion
		
        /// <summary>
		/// 得到 pacs_申请模板 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>pacs_申请模板 数据实体</returns>
		public Pacs_申请模板Model GetPacs_申请模板(int id)
		{
			// Validate input
			if(id<0)	return null;

			// Use the dal to get a record 
			return dal.GetPacs_申请模板(id);
		}
		
		/// <summary>
		/// 得到数据表Pacs_申请模板所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_申请模板Model> GetPacs_申请模板All()
		{
			return dal.GetPacs_申请模板All();
		}
		
		/// <summary>
		/// 得到数据表Pacs_申请模板符合条件的所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_申请模板Model> GetPacs_申请模板All(string sqlWhere)
		{
			return dal.GetPacs_申请模板All(sqlWhere);
		}				
		
		/// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{
			return dal.IsExist(id);
		}

        #endregion		
    }
}

