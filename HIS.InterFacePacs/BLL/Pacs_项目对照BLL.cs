﻿using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;
using HIS.InterFacePacs.DALFactory;

namespace HIS.InterFacePacs.BLL
{
    /// <summary>
    /// BLL Layer For dbo.Pacs_项目对照.
    /// </summary>
    public partial class Pacs_项目对照BLL
    {
		public static readonly IPacs_项目对照 dal = DataAccess.CreatePacs_项目对照();		
		#region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public Pacs_项目对照BLL()
		{
		}
		#endregion

        #region ----------函数定义----------
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="pacs_项目对照">Pacs_项目对照??</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_项目对照Model pacs_项目对照)
		{
			// Validate input
			if (pacs_项目对照 == null) return 0;
			// Use the dal to insert a new record 
			return dal.Insert(pacs_项目对照);
		}
		
		/// <summary>
		/// 向数据表Pacs_项目对照更新一条记录。
		/// </summary>
		/// <param name="pacs_项目对照">pacs_项目对照</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_项目对照Model pacs_项目对照)
		{
            // Validate input
			if (pacs_项目对照==null) return -1;
			// Use the dal to update a new record 
			return dal.Update(pacs_项目对照);
		}
		
		/// <summary>
		/// 删除数据表Pacs_项目对照中的一条记录
		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			// Validate input
			if(id<0)	return 0;
			return dal.Delete(id);
		}
		#endregion
		
        /// <summary>
		/// 得到 pacs_项目对照 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>pacs_项目对照 数据实体</returns>
		public Pacs_项目对照Model GetPacs_项目对照(int id)
		{
			// Validate input
			if(id<0)	return null;

			// Use the dal to get a record 
			return dal.GetPacs_项目对照(id);
		}
		
		/// <summary>
		/// 得到数据表Pacs_项目对照所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_项目对照Model> GetPacs_项目对照All()
		{
			return dal.GetPacs_项目对照All();
		}
		
		/// <summary>
		/// 得到数据表Pacs_项目对照符合条件的所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_项目对照Model> GetPacs_项目对照All(string sqlWhere)
		{
			return dal.GetPacs_项目对照All(sqlWhere);
		}				
		
		/// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{
			return dal.IsExist(id);
		}

        #endregion		
    }
}

