﻿using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;
using HIS.InterFacePacs.DALFactory;

namespace HIS.InterFacePacs.BLL
{
    /// <summary>
    /// BLL Layer For dbo.Pacs_执行科室.
    /// </summary>
    public partial class Pacs_执行科室BLL
    {
		public static readonly IPacs_执行科室 dal = DataAccess.CreatePacs_执行科室();		
		#region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public Pacs_执行科室BLL()
		{
		}
		#endregion

        #region ----------函数定义----------
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="pacs_执行科室">Pacs_执行科室??</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_执行科室Model pacs_执行科室)
		{
			// Validate input
			if (pacs_执行科室 == null) return 0;
			// Use the dal to insert a new record 
			return dal.Insert(pacs_执行科室);
		}
		
		/// <summary>
		/// 向数据表Pacs_执行科室更新一条记录。
		/// </summary>
		/// <param name="pacs_执行科室">pacs_执行科室</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_执行科室Model pacs_执行科室)
		{
            // Validate input
			if (pacs_执行科室==null) return -1;
			// Use the dal to update a new record 
			return dal.Update(pacs_执行科室);
		}
		
		/// <summary>
		/// 删除数据表Pacs_执行科室中的一条记录
		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			// Validate input
			if(id<0)	return 0;
			return dal.Delete(id);
		}
		#endregion
		
        /// <summary>
		/// 得到 pacs_执行科室 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>pacs_执行科室 数据实体</returns>
		public Pacs_执行科室Model GetPacs_执行科室(int id)
		{
			// Validate input
			if(id<0)	return null;

			// Use the dal to get a record 
			return dal.GetPacs_执行科室(id);
		}
		
		/// <summary>
		/// 得到数据表Pacs_执行科室所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_执行科室Model> GetPacs_执行科室All()
		{
			return dal.GetPacs_执行科室All();
		}
		
		/// <summary>
		/// 得到数据表Pacs_执行科室符合条件的所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_执行科室Model> GetPacs_执行科室All(string sqlWhere)
		{
			return dal.GetPacs_执行科室All(sqlWhere);
		}				
		
		/// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{
			return dal.IsExist(id);
		}

        #endregion		
    }
}

