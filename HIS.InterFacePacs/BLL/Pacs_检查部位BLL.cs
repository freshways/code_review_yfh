﻿using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;
using HIS.InterFacePacs.DALFactory;

namespace HIS.InterFacePacs.BLL
{
    /// <summary>
    /// BLL Layer For dbo.Pacs_检查部位.
    /// </summary>
    public partial class Pacs_检查部位BLL
    {
		public static readonly IPacs_检查部位 dal = DataAccess.CreatePacs_检查部位();		
		#region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public Pacs_检查部位BLL()
		{
		}
		#endregion

        #region ----------函数定义----------
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="pacs_检查部位">Pacs_检查部位??</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(Pacs_检查部位Model pacs_检查部位)
		{
			// Validate input
			if (pacs_检查部位 == null) return 0;
			// Use the dal to insert a new record 
			return dal.Insert(pacs_检查部位);
		}
		
		/// <summary>
		/// 向数据表Pacs_检查部位更新一条记录。
		/// </summary>
		/// <param name="pacs_检查部位">pacs_检查部位</param>
		/// <returns>影响的行数</returns>
		public int Update(Pacs_检查部位Model pacs_检查部位)
		{
            // Validate input
			if (pacs_检查部位==null) return -1;
			// Use the dal to update a new record 
			return dal.Update(pacs_检查部位);
		}
		
		/// <summary>
		/// 删除数据表Pacs_检查部位中的一条记录
		/// </summary>
	    /// <param name="部位编码">部位编码</param>
		/// <returns>影响的行数</returns>
		public int Delete(long 部位编码)
		{
			// Validate input
			if(部位编码<0)	return 0;
			return dal.Delete(部位编码);
		}
		#endregion
		
        /// <summary>
		/// 得到 pacs_检查部位 数据实体
		/// </summary>
		/// <param name="部位编码">部位编码</param>
		/// <returns>pacs_检查部位 数据实体</returns>
		public Pacs_检查部位Model GetPacs_检查部位(long 部位编码)
		{
			// Validate input
			if(部位编码<0)	return null;

			// Use the dal to get a record 
			return dal.GetPacs_检查部位(部位编码);
		}
		
		/// <summary>
		/// 得到数据表Pacs_检查部位所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_检查部位Model> GetPacs_检查部位All()
		{
			return dal.GetPacs_检查部位All();
		}
		
		/// <summary>
		/// 得到数据表Pacs_检查部位符合条件的所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<Pacs_检查部位Model> GetPacs_检查部位All(string sqlWhere)
		{
			return dal.GetPacs_检查部位All(sqlWhere);
		}				
		
		/// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="部位编码">部位编码</param>
        /// <returns>是/否</returns>
		public bool IsExist(long 部位编码)
		{
			return dal.IsExist(部位编码);
		}

        #endregion		
    }
}

