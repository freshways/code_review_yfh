﻿using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;
using HIS.InterFacePacs.DALFactory;

namespace HIS.InterFacePacs.BLL
{
    /// <summary>
    /// BLL Layer For dbo.yunRis_身体部位.。。。
    /// </summary>
    public partial class yunRis_身体部位BLL
    {
		public static readonly IyunRis_身体部位 dal = DataAccess.CreateyunRis_身体部位();		
		#region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public yunRis_身体部位BLL()
		{
		}
		#endregion

        #region ----------函数定义----------
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="yunris_身体部位">yunRis_身体部位??</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(yunRis_身体部位Model yunris_身体部位)
		{
			// Validate input
			if (yunris_身体部位 == null) return 0;
			// Use the dal to insert a new record 
			return dal.Insert(yunris_身体部位);
		}
		
		/// <summary>
		/// 向数据表yunRis_身体部位更新一条记录。
		/// </summary>
		/// <param name="yunris_身体部位">yunris_身体部位</param>
		/// <returns>影响的行数</returns>
		public int Update(yunRis_身体部位Model yunris_身体部位)
		{
            // Validate input
			if (yunris_身体部位==null) return -1;
			// Use the dal to update a new record 
			return dal.Update(yunris_身体部位);
		}
		
		/// <summary>
		/// 删除数据表yunRis_身体部位中的一条记录
		/// </summary>
	    /// <param name="iid">iid</param>
		/// <returns>影响的行数</returns>
		public int Delete(decimal iid)
		{
			// Validate input
			if(iid<0)	return 0;
			return dal.Delete(iid);
		}
		#endregion
		
        /// <summary>
		/// 得到 yunris_身体部位 数据实体
		/// </summary>
		/// <param name="iid">iid</param>
		/// <returns>yunris_身体部位 数据实体</returns>
		public yunRis_身体部位Model GetyunRis_身体部位(decimal iid)
		{
			// Validate input
			if(iid<0)	return null;

			// Use the dal to get a record 
			return dal.GetyunRis_身体部位(iid);
		}
		
		/// <summary>
		/// 得到数据表yunRis_身体部位所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<yunRis_身体部位Model> GetyunRis_身体部位All()
		{
			return dal.GetyunRis_身体部位All();
		}
		
		/// <summary>
		/// 得到数据表yunRis_身体部位符合条件的所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<yunRis_身体部位Model> GetyunRis_身体部位All(string sqlWhere)
		{
			return dal.GetyunRis_身体部位All(sqlWhere);
		}				
		
		/// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="iid">iid</param>
        /// <returns>是/否</returns>
		public bool IsExist(decimal iid)
		{
			return dal.IsExist(iid);
		}

        #endregion		
    }
}

