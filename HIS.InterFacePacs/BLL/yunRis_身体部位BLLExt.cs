﻿using System;
using System.Collections.Generic;
using HIS.InterFacePacs.Model;
using HIS.InterFacePacs.IDAL;
using HIS.InterFacePacs.DALFactory;

namespace HIS.InterFacePacs.BLL
{
    /// <summary>
    /// BLL Layer For dbo.yunRis_身体部位.。。。
    /// </summary>
    public partial class yunRis_身体部位BLLExt
    {
		public static readonly IyunRis_身体部位Ext dal = DataAccess.CreateyunRis_身体部位Ext();
        public yunRis_身体部位BLL bll = null;
        #region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public yunRis_身体部位BLLExt()
		{
            bll = new yunRis_身体部位BLL();
		}

        /// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="iid">iid</param>
        /// <returns>是/否</returns>
        public bool IsExist(string _设备类型, string _部位编码)
        {
            return dal.IsExist(_设备类型, _部位编码);
        }

        public string GetMaxID(string _设备类型)
        {
            return dal.GetMaxID(_设备类型);
        }
		#endregion

    }
}

