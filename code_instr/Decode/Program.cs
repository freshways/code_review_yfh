﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Configuration;

namespace Decode
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                // add by wjz 20151209 加密数据库连接 ▽
                try
                {
                    //EncryptConfiguration();
                }
                catch//(Exception ex)
                {
                    MessageBox.Show("请尝试以管理员身份运行Lis解码程序。如果还出现此错误，请联系技术人员。");
                    return;
                }
                // add by wjz 20151209 加密数据库连接 △

                Application.Run(new DecodeMain());
            }
            catch(Exception e)
            {
                MessageBox.Show("数据解码发生意外错误,程序将退出.请重新启动\r\n" + e.ToString(), "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // add by wjz 20151209 加密数据库连接 ▽
        static void EncryptConfiguration()
        {
            // 使用什么类型的加密
            //支持两种类型的加密：
            //DPAPIProtectedConfigurationProvider：使用Windows Data Protection API (DPAPI)
            //RsaProtectedConfigurationProvider：使用RSA算法

            string provider = "RsaProtectedConfigurationProvider";
            Configuration config = null;
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            // 加密连接字符串
            ConfigurationSection section = config.ConnectionStrings;
            if ((section.SectionInformation.IsProtected == false) &&
                (section.ElementInformation.IsLocked == false))
            {
                section.SectionInformation.ProtectSection(provider);
                section.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Full);
            }
        }
        // add by wjz 20151209 加密数据库连接 △
    }
}