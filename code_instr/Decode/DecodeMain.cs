using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using Decode.Properties;
using System.Data.OleDb;
using System.Runtime.InteropServices;

using System.Reflection;
using DecodeInterface;

using System.Diagnostics;
using System.Threading;

namespace Decode
{
    public enum DataMode { Text, Hex }
    public enum LogMsgType { Incoming, Outgoing, Normal, Warning, Error };    

    public partial class DecodeMain : Form
    {
        private int iMSGcount = 0;           // 串口窗口发过来的消息数量
        private int iDecodeSamplCount;       // 累计解码
        private int iSaveCount;              // 累计存储
        private int iActualTransCount;       // 累计传输
        private int WaitTranseCount = 0;     // 待传计数
        private int LastWaitTranseCount = 0; // 上次待传,用于记录这个待传计数已被重试1次,不要再试了        

        /// <summary> 监听串口数据消息,触发解码 </summary>
        private bool WatchPortMsg = true;
        /// <summary> 监听串口文件消息,触发解码 </summary>
        private bool WatchPortFile = true;
        /// <summary> 本地存储失败时,重试次数 </summary>
        private int LocalSaveRetryCount = 3;
        /// <summary> 是否是独占消息 </summary>
        private bool bolIsEngross = false;

        private Color[] LogMsgTypeColor = { Color.Blue, Color.Green, Color.Black, Color.Orange, Color.Red };
        private BackgroundWorker bgwDecode, bgwServerTrans;

        private System.Timers.Timer timTransResult;

        private bool BilSound = true;

        IDDecodeObject IDecode;

        LocalDataBase LocalDataBase = new LocalDataBase(Application.StartupPath);
        ServerDataBase ServerDataBase = new ServerDataBase(Application.StartupPath);        

        const int WM_COPYDATA = 0x004A;

        private Msgbox Msgbox;

        /// <summary> 构造函数
        /// </summary>
        public DecodeMain()
        {
            InitializeComponent();            

            // 不处理线程间异常操作
            Form.CheckForIllegalCrossThreadCalls = false;
        }

        private void DecodeMain_Load(object sender, EventArgs e)
        {
            if (!Settings.Default.ShowSysLog) { tabControl1.TabPages.Remove(tbpSysLog); }
            pnlBuffterData.Visible = false;
            pnlPassword.Visible = false;

            // 注册与服务器联接端口
            if (!ServerDataBase.CreatRemotingOBJ())
            {
                MessageBox.Show("无法连接服务器,请检查网络是否连接。\r\n本程序可继续运行，但接收的数据在网络连通前不能传输到服务器。" + '\r' + '\n' + '\r' + '\n' +
                                "请检查是否已打开过本程序,并在关闭原来打开的程序后再打开本程序" + '\r' + '\n' +
                                "如何错误依然存在,请重启电脑", "警告 " + Settings.Default.InstrName + "解码程序", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //this.Dispose();
                //Application.Exit();
            }

            // 载入配置
            WatchPortMsg = Settings.Default.WatchPortMsg;
            WatchPortFile = Settings.Default.WatchPortFile;
            LocalSaveRetryCount = Settings.Default.LocalSaveRetryCount;
            bolIsEngross = Settings.Default.IsEngross;

            #region  压缩Access数据库 
            ComPressionAccess cpa = new ComPressionAccess();
            cpa = null;
            #endregion

            // 动态载入解码对象
            try
            {
                Assembly assembly = Assembly.LoadFrom(Application.StartupPath + "\\" + Settings.Default.DecodeName + ".dll");
                Type type = assembly.GetType("LIS.InstrDecode." + Settings.Default.DecodeName);
                object obj = Activator.CreateInstance(type);
                IDecode = (IDDecodeObject)obj;

                //单样解码完成订阅事件
                IDecode.oneDecodeEnd += new DecodeEndHandler(Decode_oneDecodeEnd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("载入解码库失败,程序将自动关闭" + '\r' + '\r' + ex.ToString(), "警告", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                this.Dispose();
                Application.Exit();
            }
            try
            {
                notifyIcon1.Icon = new Icon(Application.StartupPath + "\\Docode.ico");
            }
            catch
            {
 
            }
            //写入结果状态报告
            LocalDataBase.WriteResult += new LocalDataBase.WriteResultHandler(LocalDataBase_WriteResult);

            //服务器数据同步线程
            bgwServerTrans = new BackgroundWorker();
            bgwServerTrans.DoWork += new DoWorkEventHandler(RunbgwServerTrans);
            bgwServerTrans.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwServerTrans_RunWorkerCompleted);

            //服务器数据同步 调度计时器
            timTransResult = new System.Timers.Timer(10000);//实例化Timer类，设置间隔时间为10000毫秒；   
            timTransResult.Elapsed += new System.Timers.ElapsedEventHandler(timTransResult_Tick);//到达时间的时候执行事件；   
            timTransResult.AutoReset = false;//设置是执行一次（false）还是一直执行(true)；   
            timTransResult.Enabled = false;
           
            //解码线程
            bgwDecode = new BackgroundWorker();
            bgwDecode.DoWork += new DoWorkEventHandler(rDecode);
            bgwDecode.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwDecode_End);

            // 感觉某些时候串口收数据后不发消息,用监视文件改变来触发
            if (WatchPortFile)
            {
                FileSystemWatcher watcher = new FileSystemWatcher();
                watcher.Filter = Application.StartupPath + @"\ComData.mdb";
                watcher.Path = Application.StartupPath;
                watcher.NotifyFilter = NotifyFilters.LastWrite;
                watcher.Changed += new FileSystemEventHandler(watcher_Changed);
                watcher.EnableRaisingEvents = true;
            }

            // 设置窗口标题,初始界面
            this.Text += " - " + Settings.Default.InstrName;
            notifyIcon1.Text = this.Text;

            //toolStripStatusLabel2.Text = ServerDataBase.strClientIP + ":" + ServerDataBase.strClientPort + " -> " + ServerDataBase.strServerIP + ":" + ServerDataBase.strServerPort;

            // 获取并显示版本号
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                AssemblyName assemblyName = assembly.GetName();
                Version version = assemblyName.Version;
                Msgbox = new Msgbox();

                tllsVersion.Text = "V" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
            }
            catch 
            {
                MessageBox.Show("获取解码主程序版本号时出错" + '\r' + '\r', "警告", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            //启动与接口监控程序通信线程
            Thread threadMonitor = new Thread(new ThreadStart(CreatMonitorTimer));
            threadMonitor.Start();

            //打开程序时调用解码,同步
            StartbgwDecode();

            // 启动后执行服务数据同步
            // 看有无待传的
            RunNoTransResult();
        }

        #region 创建接口监视通信线程

        private System.Timers.Timer timer1;
        private void CreatMonitorTimer()
        {
            this.timer1 = new System.Timers.Timer();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(timer1_Elapsed);

            SendMessageToInstrMonitor();
        }

        void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            SendMessageToInstrMonitor();
        }

        #endregion
        //定义全局变量，判断是否验证检验日期与当前日期是否一致
        int tallybreak = 0, tally = 0;
        bool bl_是否判断日期一致 = true;
        bool bl_是否取消执行解码 = false;

        /// <summary> 解码线程 单样解码完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="Status">-1:解码错误 0:解码完成,无可用数据 1:成功</param>
        /// <param name="ex"></param>
        /// <param name="strType"></param>
        /// <param name="strSampleNo"></param>
        /// <param name="strDate"></param>
        /// <param name="ItemResults"></param>
        /// <param name="ItemImgResults"></param>
        /// <param name="strSampleData"></param>
        /// <param name="Heat"></param>
        /// <param name="XYDepth"></param>
        /// <param name="Haemoglobin"></param>
        void Decode_oneDecodeEnd(object sender, int Status, Exception ex, string strType, string strSampleNo, string strDate, List<ItemResult> ItemResults, List<ItemImgResult> ItemImgResults, string strSampleData, float Heat, float XYDepth, float Haemoglobin)
        {
            try
            {
                if (bl_是否取消执行解码) return;
                switch (Status)
                {
                    case -1:
                        SetDecodeStatus("解码错误!");
                        string ErrorMsg = "";
                        if (ex != null)
                        {
                            ErrorMsg = ex.ToString();
                        }
                        Log(LogMsgType.Error, "解码错误!" + '\n' + ErrorMsg + '\n' + strSampleData + '\n' + "如果你已连续多次看到这个错误,请重启电脑或与工程师联系解决。" + '\n');
                        ErrorSound();
                        break;
                    case 0:
                        SetDecodeStatus("解码完成,无可用数据");
                        break;
                    case 1:
                        SetDecodeCountStatus(strSampleNo);

                        // 判断样品时间与系统时间是否一致
                        DateTime dtSampleDate = Convert.ToDateTime(strDate);
                        
                        if (dtSampleDate.Date != System.DateTime.Now.Date && bl_是否判断日期一致)
                        {
                            if (tallybreak > 3 || tally > 3)
                            {
                                if (MessageBox.Show("号样品检测日期与当前系统日期不一至的数据过多，是否统一执行? ", "！注意 - 仪器接口数据提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    if (tallybreak > 3)
                                    {
                                        bl_是否取消执行解码 = true;
                                        return;
                                    }
                                    if (tally > 3)
                                        bl_是否判断日期一致 = false;
                                }
                            }
                            else
                            {
                                DialogResult dr = MessageBox.Show(" '" + strSampleNo + "' 号样品检测日期是:" + dtSampleDate.ToString() + " \r\n与当前系统日期不一至,请确认仪器时间或电脑时间是否正确 \r\n\r\n 确实要保存该样品数据吗？ ", "！注意 - 仪器接口数据提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                                if (dr == DialogResult.No)
                                {
                                    tallybreak += 1;
                                    break;
                                }
                                else
                                {
                                    tally += 1;
                                }
                            }
                        }

                        SetDecodeStatus("执行本地存储..." + strSampleNo);
                        LocalDataBase.addLocalResult(strType, strSampleNo, strDate, ItemResults, ItemImgResults, strSampleData, Heat, XYDepth, Haemoglobin, 0);

                        break;
                }
            }
            catch (Exception ex1)
            {
                SetDecodeStatus("单样解码完成,发生错误 " + ex1.Message.ToString());
            }
        }

        /// <summary> 启动异步数据解码线程
        /// </summary>
        private void StartbgwDecode()
        {
            ShowTransLog(textBox1, "bgw调度");

            bool bolBgwBusy = false;

            try
            {
                bolBgwBusy = bgwDecode.IsBusy;
                if (!bgwDecode.IsBusy)
                {
                    ShowTransLog(textBox1, "bgw启动");
                    bgwDecode.RunWorkerAsync();
                }
                else
                {                    
                    iMSGcount += 1;
                    ShowTransLog(textBox1, "bgw忙" + iMSGcount.ToString() + '\n');
                }
            }
            catch
            {
                Log(LogMsgType.Error, "启动异步数据解码线程失败,可能正忙...如果你已连续多次看到这个错误,请重启电脑或与工程师联系解决。" + bolBgwBusy.ToString() + '\n');
                ErrorSound();
                iMSGcount += 1;
                ShowTransLog(textBox1, "bgw忙" + iMSGcount.ToString());
            }
        }

        /// <summary>异步数据解码线程 解码方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rDecode(object sender, DoWorkEventArgs e)
        {
            // 读缓冲前写一条,否则有时读不到
            // 不能要,否则我监视文件变化就一直在变,一直在读
            // LocalDataBase.AddBufferData(" ");

            string strBufferData = LocalDataBase.ReaderBufferData();
            ShowBufferPortData(strBufferData);

            if (strBufferData != "")
            {
                if (strBufferData == "0")
                {
                    Log(LogMsgType.Error, "读取缓冲数据失败,10秒后自动重试...如果你已连续多次看到这个错误,请重启电脑或与工程师联系解决。" + '\n');
                    ErrorSound();
                    iMSGcount += 1;

                    System.Threading.Thread.Sleep(10000);
                }
                else if (strBufferData == "-1")
                {
                    Log(LogMsgType.Error, "删除缓冲数据失败,10秒后自动重试...如果你已连续多次看到这个错误,请重启电脑或与工程师联系解决。" + '\n');
                    ErrorSound();
                    iMSGcount += 1;

                    System.Threading.Thread.Sleep(10000);
                }
                else
                {
                    SetDecodeStatus("在正解码...");
                    IDecode.DataDecode(strBufferData);
                }
            }
            else
            {
                //Log(LogMsgType.Normal, "读取缓冲未发现可用数据。" + '\n');
                ShowTransLog(textBox1, ".无数据");
                SetDecodeStatus("等待...");
            }
        }

        /// <summary> 解码线程 结束
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bgwDecode_End(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    MessageBox.Show(e.Error.Message);
                }
                else if (e.Cancelled)
                {
                    MessageBox.Show("解码被取消");
                }
                else
                {
                    SetDecodeStatus("解码完成");
                    ShowTransLog(textBox1, "bgw完成" + '\r' + '\n');

                    if (iMSGcount > 0)
                    {                        
                        iMSGcount = 0;
                        StartbgwDecode();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary> 显示解码结果日志
        /// </summary>
        /// <param name="msgtype"></param>
        /// <param name="msg"></param>
        private void Log(LogMsgType msgtype, string msg)
        {
            int msgColorIndex = (int)msgtype;

            rtfTerminal.Invoke(new EventHandler(delegate
            {
                // 清除一定数量的数据
                // 让rtfTerminal保持只显示最新的部份数据
                // 如果某行出红色,当结果填满后会全部变红 bug

                int rtfLinesCount = rtfTerminal.Text.Length;
                const int iShowLinsCount = 10720;
                if (rtfLinesCount > iShowLinsCount)
                {
                    rtfTerminal.Text = rtfTerminal.Text.Remove(0, rtfLinesCount - iShowLinsCount);
                }
                int rtfCurrLinesCount = rtfTerminal.Text.Length;

                rtfTerminal.SelectionFont = new Font(rtfTerminal.SelectionFont, FontStyle.Regular);
                rtfTerminal.SelectionColor = LogMsgTypeColor[msgColorIndex];
                rtfTerminal.SelectionStart = rtfCurrLinesCount;
                rtfTerminal.SelectionLength = msg.Length;

                if (msgColorIndex == 2)
                {
                    rtfTerminal.SelectionColor = System.Drawing.Color.Black;
                }

                rtfTerminal.AppendText(msg);

                rtfTerminal.ScrollToCaret();
            }));
        }

        /// <summary>更新解码状态提示信息
        /// </summary>
        /// <param name="sStatusString">状态信息</param>
        private void SetDecodeStatus(string sStatusString)
        {
            try
            {
                tsslDecodeStatus.Text = sStatusString;
            }
            catch { }
        }

        /// <summary>更新已处理样品状态
        /// </summary>
        /// <param name="sStatusString">状态信息</param>
        private void SetDecodeCountStatus(string strCount)
        {
            SetDecodeStatus("解码完成,样品号:" + strCount);
            iDecodeSamplCount += 1;

            tsslDecodeCount.Text = iDecodeSamplCount.ToString();
        }

        /// <summary>解码后台线程解码结果显示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ItemInfo"></param>
        void LocalDataBase_WriteResult(object sender, string ItemInfo,Exception ex,object[] objSaveOLD)
        {
            try
            {
                if (ItemInfo == "WriteLocalResultOver")
                {
                    SetDecodeStatus("存储完毕.");

                    // 已存储计数+1
                    iSaveCount += 1;
                    tsslSaveCount.Text = "/" + iSaveCount.ToString();

                    // 存储本地解码结果完成,启动同步
                    // 待传+1
                    WaitTranseCount += 1;
                    ShowWaitTranseCount();
                    RunNoTransResult();
                }
                else if (ItemInfo == "NoResult")
                {
                    SetDecodeStatus("无结果存储.");
                }
                else
                {
                    if (ex != null)
                    {
                        ItemInfo = '\r' + ItemInfo + '\r' + ex.Message;
                    }

                    if (ItemInfo.IndexOf("失败") > -1)
                    {
                        Log(LogMsgType.Error, ItemInfo + '\r' + '\n');
                        ErrorSound();

                        // 再次调用存储,如果重试次数小于设定重试次数
                        int RetryCount = Convert.ToInt32(objSaveOLD[6].ToString());
                        string strSampleNo = "";
                        try { strSampleNo = objSaveOLD[1].ToString(); }
                        catch { }
                        if (RetryCount < LocalSaveRetryCount)
                        {
                            string strType = objSaveOLD[0].ToString();
                            string strDate = objSaveOLD[2].ToString();
                            List<ItemResult> ItemResults = (List<ItemResult>)objSaveOLD[3];
                            List<ItemImgResult> ItemImgResults = (List<ItemImgResult>)objSaveOLD[4];
                            string strSampleData = objSaveOLD[5].ToString();
                            float fltHeat = (float)objSaveOLD[7];
                            float fltXYDepth = (float)objSaveOLD[8];
                            float fltHaemoglobin = (float)objSaveOLD[9];

                            Log(LogMsgType.Incoming, "★1.5秒后重试...如果你已连续多次看到这个错误,请重启电脑或与工程师联系解决。" + '\r' + '\n');
                            System.Threading.Thread.Sleep(1500);

                            LocalDataBase.addLocalResult(strType, strSampleNo, strDate, ItemResults, ItemImgResults, strSampleData, fltHeat, fltXYDepth, fltHaemoglobin, RetryCount);
                        }
                        else
                        {
                            Log(LogMsgType.Incoming, "★连续重试" + RetryCount + "次,均失败,程序放弃重试,如果需要增加重试次数请修配置文件中的'LocalSaveRetryCount'参数。或联系工程师解决。" + '\r' + '\n');
                        }
                    }
                    else
                    {
                        Log(LogMsgType.Normal, ItemInfo);
                    }
                }
            }
            catch (Exception ex1)
            {
                Log(LogMsgType.Incoming, "解码完成后发生错误:" + ex1.Message.ToString());
            }
        }

        /// <summary> 后台数据同步 调度
        /// </summary>
        private void RunNoTransResult()
        {
            ShowTransLog(textBox3, "调度同步...");

            try
            {
                if (!bgwServerTrans.IsBusy)
                {
                    bgwServerTrans.RunWorkerAsync();
                }
                else
                {
                    ShowTransLog(textBox3, "同步线程正忙,10秒后重试..." + WaitTranseCount.ToString());
                    //启动定时再调度
                    StartTimer();
                }
            }
            catch
            {
                ShowTransLog(textBox3, "启动异步数据解码线程失败,可能正忙.,10秒后重试..." + WaitTranseCount.ToString());
                ErrorSound();
                //启动定时再调度
                StartTimer();
            }
        }

        /// <summary> 后台数据同步 启动
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RunbgwServerTrans(object sender, DoWorkEventArgs e)
        {
            Exception ex = new Exception();   // 传输错误消息
            int ActualTransCout=0;              // 成功传输计数

            timTransResult.Enabled = false;   // 关闭数据同步计时器            
            bool ServerTransResult=false;

            try
            {
                // 启动数据同步,返回成败
                ServerTransResult = ServerDataBase.DoTransResult(out ActualTransCout, out ex);

                // 成功或部份成功
                if (ActualTransCout > 0)
                {
                    iActualTransCount += ActualTransCout;

                    // 如果是第一次启动时有未传输数据
                    // 待传输将是0,所以不用减
                    WaitTranseCount -= ActualTransCout;
                    if (WaitTranseCount < 0)
                    {
                        WaitTranseCount = 0;
                    }

                    ShowWaitTranseCount();
                    tsslActualTrans.Text = iActualTransCount.ToString();
                    
                    if (BilSound)
                    {
                        //System.Console.Beep(1400, 200);
                        //System.Console.Beep(1000, 500);
                    }

                    ShowTransLog(textBox3, "同步成功" + ActualTransCout.ToString());
                    SendInsMsgToServer();
                    
                }

                // 失败或部份失败
                if (!ServerTransResult)
                {
                    ShowTransLog(textBox3, "同步失败");
                    Log(LogMsgType.Error, '\n' + "同步数据至服务器失败,10秒后重试...如果你已连续多次看到这个错误,请重启电脑或与工程师联系解决。" + '\n' + ex.Message + '\n');
                    ErrorSound();
                    //启动定时再调度
                    StartTimer();
                }
            }
            catch (Exception exa)
            {
                ShowTransLog(textBox3, "同步失败");
                Log(LogMsgType.Error, '\n' + "执行同步线程出错...如果你已连续多次看到这个错误,请重启电脑或与工程师联系解决。" + '\n' + exa.ToString() + '\n');
                ErrorSound();
                //启动定时再调度
                StartTimer();
            }

            // 判断本次同步成功或失败
            // 供线程结束后判断是否再运行
            if ((ServerTransResult) || (ActualTransCout>0))
            {
                e.Result = true;
            }
            else
            {
                e.Result = false;
            }
        }

        /// <summary> 显示待传达样品数
        /// </summary>
        private void ShowWaitTranseCount()
        {
            tsslWaitTranseCount.Text = WaitTranseCount.ToString() + "/";
        }

        /// <summary> 服务器数据同步完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bgwServerTrans_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ShowTransLog(textBox3, "线程完成...." + WaitTranseCount.ToString() + '\r' + '\n');

            try
            {
                if (e.Result != null)
                {
                    if ((bool)e.Result)
                    {
                        // 在执行是成功或部份成功的情况下
                        // 如果有待传输入的数据
                        if (WaitTranseCount > 0)
                        {
                            if (LastWaitTranseCount == WaitTranseCount)
                            {
                                // 本次待传等于上次待传 认为待传计数错误,将它清零
                                WaitTranseCount = 0;
                                ShowWaitTranseCount();

                                ShowTransLog(textBox3, "待传数清零");
                            }
                            LastWaitTranseCount = WaitTranseCount;
                            timTransResult.Enabled = false;
                            RunNoTransResult();
                        }
                    }
                    else
                    { }
                }
                else
                { }
            }
            catch (Exception ex)
            {
                Log(LogMsgType.Error, '\n' + "同步线程完成,判断同步结果时出错..." + '\n' + ex.ToString() + '\n');
            }
        }

        /// <summary> 启动同步计时器
        /// </summary>
        private void StartTimer()
        {
            if (!timTransResult.Enabled)
            {
                ShowTransLog(textBox3, "计时器开始计时");
                timTransResult.Enabled = true;
            }
        }

        /// <summary> 计时器定时调度后台同步
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timTransResult_Tick(object sender, EventArgs e)
        {
            ShowTransLog(textBox3, "计时器到时");
            timTransResult.Enabled = false;
            RunNoTransResult();
        }

        /// <summary> 显示同步日志
        /// </summary>
        /// <param name="TransLog"></param>
        private void ShowTransLog(TextBox textBox, string TransLog)
        {
            if (Settings.Default.ShowSysLog)
            {
                // 清除一定数量的数据
                // 让rtfTerminal保持只显示最新的部份数据
                int rtfLinesCount = textBox.Text.Length;
                const int iShowLinsCount = 1072;
                if (rtfLinesCount > iShowLinsCount)
                {
                    textBox.Text = textBox.Text.Remove(0, rtfLinesCount - iShowLinsCount);
                }

                textBox.Invoke(new EventHandler(delegate
                            {
                                textBox.AppendText(System.DateTime.Now.ToString() + " " + TransLog + '\r' + '\n');
                            }));
            }
        }

        /// <summary> 有声无声替换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            if (BilSound)
            {
                toolStripStatusLabel1.ToolTipText = "如果你想每次收到数据都“叮咚”的响，那就请你你点一下它";
                toolStripStatusLabel1.Image = Decode.Properties.Resources.NoSound;
                BilSound = false;
            }
            else
            {
                toolStripStatusLabel1.ToolTipText = "如果你觉得每次收到数据都“叮咚”的响很烦，那麻烦你点一下它就不响了";
                toolStripStatusLabel1.Image = Decode.Properties.Resources.Sound;
                BilSound = true;
            }
        }

        /// <summary> 错误声音
        /// </summary>
        private void ErrorSound()
        {
            System.Console.Beep(1400, 800);
        }

        /// <summary> 向服务器发送消息,通知仪器有新数据 </summary>        
        /// 
        private void SendInsMsgToServer()
        {
            try
            {
                // 发送Remoting消息 FC新版消息体
                string strRelayInstr = Settings.Default.RelayInstr;
                SendMessageToAddResultFrm(Settings.Default.InstrID);
                if (!string.IsNullOrEmpty(strRelayInstr))
                {
                    SendMessageToAddResultFrm(strRelayInstr);
                }


                ShowTransLog(textBox2, "发:" + bolIsEngross.ToString() + " " + Settings.Default.InstrID);
            }
            catch (Exception ex)
            {
                Log(LogMsgType.Error, '\n' + "发送仪器数据准备好消息时出错..." + '\n' + ex.ToString() + '\n');
            }
        }

        #region 托盘图标控制 部份

        private void 开机启动ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetSetupWindowOpenRun(Application.ExecutablePath, "Lis解码主程序", Settings.Default.InstrName);
        }

        /// <summary>
        /// 将文件放到启动文件夹中开机启动 Windows Script Host Object Model
        /// </summary>
        /// <param name="setupPath">启动程序</param>
        /// <param name="linkname">快捷方式名称</param>
        /// <param name="description">描述</param>
        public void SetSetupWindowOpenRun(string setupPath, string linkname, string description)
        {
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\" + linkname + ".lnk";
            if (System.IO.File.Exists(desktop))
                System.IO.File.Delete(desktop);
            IWshRuntimeLibrary.WshShell shell;
            IWshRuntimeLibrary.IWshShortcut shortcut;
            try
            {
                shell = new IWshRuntimeLibrary.WshShell();
                shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(desktop);
                shortcut.TargetPath = setupPath;//程序路径
                shortcut.Arguments = "";//参数
                shortcut.Description = description;//描述
                shortcut.WorkingDirectory = System.IO.Path.GetDirectoryName(setupPath);//程序所在目录
                shortcut.IconLocation = setupPath;//图标   
                shortcut.WindowStyle = 1;
                shortcut.Save();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "友情提示");
            }
            finally
            {
                shell = null;
                shortcut = null;
            }
        }

        private void 退出ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ComReceive_FormClosing(this, new FormClosingEventArgs(CloseReason.WindowsShutDown, true));
        }

        /// <summary> 关闭窗口时提示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComReceive_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown)
            {
                DialogResult Drs = MessageBox.Show("你真的要退出 " + this.Text + " 程序吗? 退出后将不能接收到仪器数据.\n ", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Drs == DialogResult.Yes)
                {
                    this.Dispose();
                }
                else
                {                   
                    e.Cancel = true;
                }
            }
            else
            {
                ChangeFormStatus();
                e.Cancel = true;
            }
        }

        private void DecodeMain_Shown(object sender, EventArgs e)
        {
            Visible = !Visible;
        }

        private void 显示窗体ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeFormStatus();
        }

        /// <summary>最小化时反转状态显示状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DecodeMain_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            { ChangeFormStatus(); }
        }

        /// <summary> 反转当前窗口显示状态
        /// </summary>
        private void ChangeFormStatus()
        {
            switch (this.Visible)
            {
                case true:
                    Visible = false;
                    contextMenuStrip1.Items["显示窗体ToolStripMenuItem"].Text = "显示窗体";
                    this.WindowState = FormWindowState.Minimized;
                    break;
                case false:
                    Visible = true;
                    contextMenuStrip1.Items["显示窗体ToolStripMenuItem"].Text = "隐藏窗体";
                    this.WindowState = FormWindowState.Normal;
                    break;
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            ChangeFormStatus();
        }

        #endregion

        #region 接收串口数据接收窗口消息,触发解码线程

        [StructLayout(LayoutKind.Sequential)]
        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        // 串口收到数据并写入Access后会发过来     
        protected override void DefWndProc(ref System.Windows.Forms.Message m)
        {
            switch (m.Msg)
            {
                //接收自定义消息 USER，并显示其参数
                case WM_COPYDATA:
                    if (WatchPortMsg)
                    {
                        COPYDATASTRUCT mystr = new COPYDATASTRUCT();
                        Type mytype = mystr.GetType();
                        mystr = (COPYDATASTRUCT)m.GetLParam(mytype);

                        switch (mystr.lpData)
                        {
                            case "1": //串口接收到数据并存储到缓冲中                            
                                ShowTransLog(textBox2, iMSGcount.ToString());
                                StartbgwDecode();
                                break;
                            case "ShowDecodeForm":
                                ChangeFormStatus();
                                break;
                        }                        
                    }
                    break;
                default:
                    base.DefWndProc(ref m);
                    break;
            }
        }

        // 感觉某些时候不发,干脆用监视文件改变来触发
        // 串口数据文件变化
        void watcher_Changed(object sender, FileSystemEventArgs e)
        {
            ShowTransLog(textBox2, "文件变化");
            StartbgwDecode();
        }

        private void tllsHandDecode_Click(object sender, EventArgs e)
        {
            Log(LogMsgType.Normal, '\n' + "启动手动解码..." + '\n');
            if (LocalDataBase.WriteNullBufferData() != 1)
            {
                Log(LogMsgType.Normal,"写入临时数据未成功" + '\n');
            }

            StartbgwDecode();
        }

        #endregion

        #region 调试窗口部份

        private void button2_Click(object sender, EventArgs e)
        {
            pnlBuffterData.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // 工程师密码
            string PassWort = "inphit";
            if (maskedTextBox1.Text == PassWort)
            {
                pnlPassword.Visible = false;
                pnlBuffterData.Visible = true;
            }
            else
            {
                MessageBox.Show("密码错误", "错误", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            maskedTextBox1.Text = "";
        }

        private void pnlBuffterData_MouseLeave(object sender, EventArgs e)
        {
            pnlBuffterData.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            pnlPassword.Visible = false;
        }

        #endregion

        #region CaiDan
        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            try
            {
                CaiDan.CaiDan.DoIt(1, 2);
            }
            catch { }
        }

        private void tsslDecodeCount_Click(object sender, EventArgs e)
        {
            try
            {
                CaiDan.CaiDan.DoIt(2, 3);
            }
            catch { }
        }

        private void tsslActualTrans_Click(object sender, EventArgs e)
        {
            try
            {
                CaiDan.CaiDan.DoIt(3, 4);
            }
            catch { }
        }

        private void tllsVersion_MouseHover(object sender, EventArgs e)
        {
            try
            {
                CaiDan.CaiDan.ShowDan();
            }
            catch { }
        }

        private void tllsVersion_Click(object sender, EventArgs e)
        {
            try
            {
                CaiDan.CaiDan.DoIt(0, 1);
            }
            catch { }
        }
        #endregion

        /// <summary> 对文本框中的数据进行解码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            IDecode.DataDecode(txbBufferPortData.Text);
        }

        /// <summary> 显示解码原数据
        /// </summary>
        /// <param name="TransLog"></param>
        private void ShowBufferPortData(string TransLog)
        {
            if (Settings.Default.ShowSysLog)
            {
                // 清除一定数量的数据
                // 让rtfTerminal保持只显示最新的部份数据
                int rtfLinesCount = txbBufferPortData.Text.Length;
                const int iShowLinsCount = 1072;
                if (rtfLinesCount > iShowLinsCount)
                {
                    txbBufferPortData.Text = txbBufferPortData.Text.Remove(0, rtfLinesCount - iShowLinsCount);
                }

                try
                {
                    txbBufferPortData.AppendText( TransLog );
                    txbBufferPortDataLog.AppendText("★" + System.DateTime.Now.ToString() + '\r' + '\n' + "   " + TransLog + '\r' + '\n' + '\r' + '\n');
                }
                catch
                {
                    txbBufferPortData.Invoke(new EventHandler(delegate
                                {
                                    txbBufferPortData.AppendText(System.DateTime.Now.ToString() + '\r' + '\n' + "   " + TransLog + '\r' + '\n' + '\r' + '\n');
                                    txbBufferPortData.AppendText(TransLog);
                                }));
                }
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            pnlPassword.Visible = true;
            maskedTextBox1.Focus();
        }

        private void ReDecodeTodayData(object sender, EventArgs e)
        {
            try
            {
                //初始化判断样本检验日期与当前日期不一致 情况
                tallybreak = 0;
                tally = 0;
                bl_是否判断日期一致 = true;
                bl_是否取消执行解码 = false;

                string strDate = System.DateTime.Now.ToString("yyMMdd");
                string allData;
                if (!Directory.Exists(Application.StartupPath + @"\raw"))
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(Application.StartupPath + @"\raw");
                    directoryInfo.Create();
                }
                if (!System.IO.File.Exists(Application.StartupPath + @"\raw\" + strDate + ".txt"))
                {
                    MessageBox.Show("执行重新解码今日数据时发生错误\r\n未找到" + strDate + ".txt 文件", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                using (FileStream PortalDataFile = new FileStream(Application.StartupPath + @"\raw\" + strDate + ".txt", FileMode.Open, FileAccess.Read))
                {
                    StreamReader sr = new StreamReader(PortalDataFile, Encoding.Default, true);
                    allData = sr.ReadToEnd();
                }

                if (!string.IsNullOrEmpty(allData))
                {
                    DateTime d1 = System.DateTime.Now;
                    int DLen = allData.Length;
                    int SLen = 150000;
                    while (DLen > 0)
                    {
                        if (!bgwDecode.IsBusy)
                        {
                            SetDecodeStatus("在正解码...");
                            Log(LogMsgType.Normal, "手动解码启动");

                            if (DLen < SLen)
                            {
                                SLen = DLen;
                            }

                            IDecode.DataDecode(allData.Substring(0, SLen));

                            allData = allData.Substring(SLen);
                            DLen = allData.Length;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(3000);
                        }
                    }
                    DateTime d2 = System.DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("执行重新解码今日数据时发生错误\r\n" + ex.Message.ToString(), "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void 清空数据解码日志ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtfTerminal.Clear();
        }

        #region 发送消息部份

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendWinMessage(
        int hWnd,  //  handle  to  destination  window  
        int Msg,  //  message  
        int wParam,  //  first  message  parameter  
        ref  COPYDATASTRUCT lParam  //  second  message  parameter  
        );

        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        private static extern int FindWindow(string lpClassName, string lpWindowName);

        private void SendMessageToInstrMonitor()
        {
            int WINDOW_HANDLER = FindWindow(null, @"InstrMonitor - " + Settings.Default.InstrName);
            if (WINDOW_HANDLER == 0)
            {
            }
            else
            {
                string strMSG = "InstrMonitorDecode" + "|1";
                byte[] sarr = System.Text.Encoding.Default.GetBytes(strMSG);
                int len = sarr.Length;

                COPYDATASTRUCT cds;
                cds.dwData = (IntPtr)100;
                cds.lpData = strMSG;
                cds.cbData = len + 1;
                SendWinMessage(WINDOW_HANDLER, WM_COPYDATA, 0, ref  cds);
            }
        }
        private void SendMessageToAddResultFrm(string strInstrID)
        {
            int WINDOW_HANDLER = FindWindow(null, strInstrID + "自动添加仪器结果");
            if (WINDOW_HANDLER == 0)
            {
            }
            else
            {
                string strMSG = "AddResult";
                byte[] sarr = System.Text.Encoding.Default.GetBytes(strMSG);
                int len = sarr.Length;

                COPYDATASTRUCT cds;
                cds.dwData = (IntPtr)100;
                cds.lpData = strMSG;
                cds.cbData = len + 1;
                SendWinMessage(WINDOW_HANDLER, WM_COPYDATA, 0, ref  cds);
            }
 
        }

        #endregion

        private void SendMessageToLis(string strInstrID)
        {
            int WINDOW_HANDLER = FindWindow(null, "检验报告输入");
            if(WINDOW_HANDLER == 0)
            {
            }
            else
            {
                string strMSG = "AddResult";
                byte[] sarr = System.Text.Encoding.Default.GetBytes(strMSG);
                int len = sarr.Length;

                COPYDATASTRUCT cds;
                cds.dwData = (IntPtr)100;
                cds.lpData = strMSG;
                cds.cbData = len + 1;
                SendWinMessage(WINDOW_HANDLER, WM_COPYDATA, 0, ref  cds); 
            }
        }

    }
}