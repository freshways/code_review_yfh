/// 本类负责处理与服务器数据库相关事务
/// zmjin
/// 2008-03-26 新增调用Remoting发送消息方法，向仪器发送仪器有新数据的消息

using System;
using System.Collections.Generic;
using System.Text;
using Decode.Properties;
using System.Data;
using System.Data.OleDb;

using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Serialization.Formatters;
using System.Net;
using System.Collections;

using ww.wwf.dao;

namespace Decode
{
    class ServerDataBase
    {

        private static string connstr = Decode.Tools.DecodeHelper.DecodeConnstr(System.Configuration.ConfigurationManager.ConnectionStrings["WW.DBConn"].ConnectionString);
        //string strConn = "WW.DBConn";        
        public string strClientIP;
        LocalDataBase LocalDataBase;
        DAO faxBus;

        /// <summary> 构造函数
        /// </summary>
        public ServerDataBase(string ApplicationPath)
		{
            LocalDataBase = new LocalDataBase(ApplicationPath);
        }

        /// <summary> 实例化远程对象
        /// 成功返回True,失败返回False
        /// </summary>
        /// <returns></returns>
        public bool CreatRemotingOBJ()
        {
            bool bolConection = false;

            try
            {
                faxBus = new DAO();

                //获取本机IP
                IPHostEntry ipHE = Dns.GetHostEntry(Dns.GetHostName());
                strClientIP = ipHE.AddressList[0].ToString();
                if (string.IsNullOrEmpty(strClientIP))
                {
                    strClientIP = "127.0.0.1";
                }
                //随便访问一下数据库以验证是否连接上服务器
                if (ConnRemotingOBJ())
                {
                    bolConection = true;
                }                
            }
            catch
            {
            }

            return bolConection;
        }

        /// <summary> 测试连接服务器
        /// </summary>
        /// <returns></returns>
        private bool ConnRemotingOBJ()
        {
            
            try
            {
                int ic = faxBus.DbExecuteNonQueryBySqlStringNew(connstr, "select FTaskID from Lis_Ins_Result where 1=2");

                if (ic != -100)
                { return true; }
                else
                { return false; }
            }
            catch
            {
                return false;
            }
        }

        /// <summary> 同步服务器仪器结果
        /// </summary>成功返回true,失败返回false
        /// <returns></returns>
        public bool DoTransResult(out int ActualTransCout, out Exception ex)
        {
            return GetLocalNoTransResult(out ActualTransCout, out ex);
        }

        /// <summary> 获取本地未传输至服务器的结果,并传输至服务器
        /// </summary>
        /// <returns></returns>
        //string LastResultID = "";
        private bool GetLocalNoTransResult(out int ActualTransCout,out Exception ex)
        {
            bool bolResult = true;
            ex = null;
            ActualTransCout = 0;

            try
            {
                // 查询本地未传输结果
                string strGetNoTransSql = " select * from Lis_ComPort_Result where FStatus = False ";
                OleDbDataAdapter daResult = new OleDbDataAdapter(strGetNoTransSql, LocalDataBase.conComDataBase);
                DataSet dsResult = new DataSet();
                daResult.Fill(dsResult, "Lis_ComPort_Result");
                LocalDataBase.Close();

                foreach (DataRow dr in dsResult.Tables[0].Rows)
                {
                    string FTaskID = dr["FTaskID"].ToString();
                    string ResultID = dr["FResultID"].ToString();

                    #region 处理结果主表

                    // 查询该结果对应服务器的结果
                    // 存在修改,不存在新增
                    string strServerResultSql = "select * from Lis_Ins_Result where FTaskID='" + FTaskID + "'";
                    DataSet dsServerResult = faxBus.DbExecuteDataSetBySqlStringNew(connstr, strServerResultSql);
                    DataTable dtServerResult = dsServerResult.Tables[0];
                    DataRow drResult;

                    if (dtServerResult.Rows.Count > 0)
                    {
                        drResult = dtServerResult.Rows[0];
                        drResult["FUpdateTime"] = System.DateTime.Now.ToString();
                    }
                    else
                    {
                        drResult = dtServerResult.NewRow();
                        drResult["FTaskID"] = FTaskID;
                        drResult["FCreateTime"] = System.DateTime.Now.ToString();
                    }

                    drResult["FInstrID"] = dr["FInstrID"];
                    drResult["FSample"] = dr["FSample"];
                    drResult["FResultID"] = dr["FResultID"];
                    drResult["FNumber"] = dr["FNumber"];
                    drResult["FValidity"] = 1;
                    drResult["FDateTime"] = dr["FDateTime"];
                    drResult["FSampleSort"] = dr["FSampleSort"];
                    drResult["FType"] = dr["FType"];
                    drResult["FTransFlag"] = 0;
                    drResult["FRXD"] = dr["FRXD"].ToString();

                    // 旧版本的数据库无这三个字段,所以判断一下
                    if (dsResult.Tables[0].Columns.Contains("FHeat"))
                    {
                        drResult["FHeat"] = dr["FHeat"];
                        drResult["FXYDepth"] = dr["FXYDepth"];
                        drResult["FHaemoglobin"] = dr["FHaemoglobin"];
                    }

                    // 新增的行要添到到表中,但修改的行不用
                    if (drResult.RowState == DataRowState.Detached)
                    {
                        dtServerResult.Rows.Add(drResult);
                    }

                    //执行远程对象传输结果 
                    int ie = faxBus.DbUpdateDataSetNew(connstr, dsServerResult, dtServerResult.TableName, "FTaskID", "Lis_Ins_Result_ADD", "Lis_Ins_Result_Delete", "Lis_Ins_Result_Update", 0);
                    if (ie > 0)
                    {
                        dsServerResult.AcceptChanges();

                        // 使用command 更新数据
                        // 不需要修改数据集了
                        //dr["FStatus"] = true;
                        //dr["FSendTime"] = System.DateTime.Now.ToString();
                    }
                    else
                    {
                        ex = new Exception("同步结果至服务器时失败,返回参数" + ie.ToString());
                        bolResult = false;
                    }
                    #endregion

                    if (bolResult)
                    {
                        #region 处理结果明细

                        // 查询该任务对应的本地结果
                        string strGetNoTransDetailSql = " select FID,FItem,FValue,FTime,FRemark from Lis_ComPort_Result_Detail where FTaskID='" + FTaskID + "'";
                        OleDbDataAdapter daResult_Detail = new OleDbDataAdapter(strGetNoTransDetailSql, LocalDataBase.conComDataBase);
                        DataSet dsResult_Detail = new DataSet();
                        daResult_Detail.Fill(dsResult_Detail, "Lis_ComPort_Result_Detail");
                        LocalDataBase.Close();
                        DataRowCollection drWaitTrans = dsResult_Detail.Tables[0].Rows;
                        int iWaitTrans = drWaitTrans.Count;

                        // 查询该任务对应的远程结果
                        string strServerResultDetailSql = "select * from Lis_Ins_Result_Detail where FTaskID='" + FTaskID + "'";
                        DataSet dsServerResulDetailt = faxBus.DbExecuteDataSetBySqlStringNew(connstr, strServerResultDetailSql);
                        DataTable dtServerResultDetail = dsServerResulDetailt.Tables[0];
                        dtServerResultDetail.PrimaryKey = new DataColumn[] { dtServerResultDetail.Columns["FID"] };

                        // 循环本地结果明细,
                        // 查找服务器有无此明细,有则修改,无则增加
                        foreach (DataRow drDetail in drWaitTrans)
                        {
                            string FID = drDetail["FID"].ToString();

                            DataRow drServerDetail = dtServerResultDetail.Rows.Find(FID);
                            if (drServerDetail == null)
                            {
                                drServerDetail = dtServerResultDetail.NewRow();

                                drServerDetail["FID"] = FID;
                                drServerDetail["FTaskID"] = FTaskID;
                                drServerDetail["FCreateTime"] = System.DateTime.Now.ToString();
                            }

                            drServerDetail["FItem"] = drDetail["FItem"];
                            drServerDetail["FValue"] = drDetail["FValue"];
                            drServerDetail["FTime"] = drDetail["FTime"];
                            drServerDetail["FRemark"] = drDetail["FRemark"];
                            drServerDetail["FOD"] = "";
                            drServerDetail["FCutoff"] = "";

                            // 新增的行要添到到表中,但修改的行不用
                            if (drServerDetail.RowState == DataRowState.Detached)
                            {
                                dtServerResultDetail.Rows.Add(drServerDetail);
                            }
                        }

                        // 执行远程对象存储结果明细
                        int iresult = faxBus.DbUpdateDataSetNew(connstr, dsServerResulDetailt, dtServerResultDetail.TableName, "FID", "Lis_Ins_Result_Detail_ADD", "Lis_Ins_Result_Detail_Delete", "Lis_Ins_Result_Detail_Update", 0);
                        if (iresult != iWaitTrans)
                        {
                            ex = new Exception("同步结果明细至服务器时失败,应同步数: " + iWaitTrans.ToString() + " ,实际同步数: " + iresult.ToString());
                            bolResult = false;
                        }

                        daResult_Detail = null;
                        dsResult_Detail = null;
                        drWaitTrans = null;
                        dsServerResulDetailt = null;
                        dtServerResultDetail = null;

                        #endregion

                        #region 处理图形

                        Exception exImg;
                        if (!GetLocalNoTransResult_ImgDetail(FTaskID, out exImg))
                        {
                            ex = new Exception("同步图形结果至服务器时失败", exImg);
                            bolResult = false;
                        }
                        #endregion

                        #region 存储明细后,更新远程结果处理状态

                        // 解决快速写两条时,处理状态可能不对的问题
                        // 仅当本次样品号与上次样品号相同时执行
                        // 是个预防
                        //if (LastResultID == ResultID)
                        //{
                        //    int iUpdate = UpdateTransFlag(FTaskID);
                        //    if (iUpdate < 0)
                        //    {
                        //        ex = new Exception("更新服务器结果处理状态时失败", exImg);
                        //        bolResult = false;
                        //    }
                        //}
                        //LastResultID = ResultID;

                        #endregion
                    }

                    #region 当前结果全部同步成功后,更新本地传输状态

                    if (bolResult)
                    {
                        try
                        {
                            // 记录成功传输数量
                            ActualTransCout += 1;

                            // 更新本地结果状态
                            OleDbCommand cmdNoTrans = new OleDbCommand();
                            cmdNoTrans.Connection = LocalDataBase.conComDataBase;
                            cmdNoTrans.CommandType = CommandType.Text;
                            string UpdateStatusCmd;
                            int ExecutCount;

                            LocalDataBase.OpenDataBase();
                            UpdateStatusCmd = "update Lis_ComPort_Result set FStatus=1,FSendTime='" + System.DateTime.Now.ToString() + "' where FTaskid='" + FTaskID + "'";
                            cmdNoTrans.CommandText = UpdateStatusCmd;
                            ExecutCount = cmdNoTrans.ExecuteNonQuery();
                            LocalDataBase.Close();

                            if (ExecutCount < 0)
                            {
                                ex = new Exception("更新本地数据状态时失败,更新数量:" + ExecutCount.ToString());
                                bolResult = false;
                            }

                            cmdNoTrans = null;

                        }
                        catch (Exception e)
                        {
                            ex = new Exception("更新本地数据状态时失败." + e.Message, e);
                            bolResult = false;
                        }
                    }
                    #endregion
                }

                daResult = null;
                dsResult = null;
            }
            catch (Exception e)
            {
                ex = new Exception("获取本地未传输至服务器的结果,并传输至服务器时失败." + e.Message, e);
                bolResult = false;
            }
            finally
            {
                GC.Collect(0);
                GC.Collect(1);
                GC.Collect(2);
                GC.Collect();
            }
            
            return bolResult;
        }

        /// <summary> 更新更新远程结果处理状态
        /// </summary>
        /// <param name="TaskID"></param>
        /// <returns></returns>
        public int UpdateTransFlag(string TaskID)
        {
            try
            {
                int iDelCount = faxBus.DbExecuteNonQueryBySqlStringNew(connstr, "update Lis_Ins_Result set FTransFlag = 0  where FTaskID='" + TaskID + "'");
                return iDelCount;
            }
            catch
            {
                return -1;
            }
        }

        /// <summary> 添加服务器图像结果
        /// 成功返回true,失败返回false
        /// </summary>
        /// <returns></returns>        
        private bool GetLocalNoTransResult_ImgDetail(string FTaskID,out Exception exImg)
        {
            bool bolResult = true;
            exImg = null;

            try
            {
                // 查询该任务对应的本地图像结果
                string strGetNoTransSql = "select FImgID,FTaskID,FImg,FImgType,FImgNmae,FOrder,FRemark from Lis_ComPort_Result_Img where FTaskID='" + FTaskID + "'";
                OleDbDataAdapter daResult_ImgDetail = new OleDbDataAdapter(strGetNoTransSql, LocalDataBase.conComDataBase);
                DataSet dsResult_ImgDetail = new DataSet();
                daResult_ImgDetail.Fill(dsResult_ImgDetail, "Lis_ComPort_Result_Img");
                LocalDataBase.Close();
                DataRowCollection drWaitTrans = dsResult_ImgDetail.Tables[0].Rows;
                int iWaitTrans = drWaitTrans.Count;

                // 查询服务器的图像
                DataSet dsImgResutl = faxBus.DbExecuteDataSetBySqlStringNew(connstr, "select FImgID,FTaskID,FImg,FImgType,FImgNmae,FOrder,FRemark from Lis_Ins_Result_Img where FTaskID='" + FTaskID + "'");
                DataTable dtImgResutl = dsImgResutl.Tables[0];
                dtImgResutl.PrimaryKey = new DataColumn[] { dtImgResutl.Columns["FImgID"] };

                // 循环本地图像结果
                // 查找服务器有无此明细,有则修改,无则增加
                foreach (DataRow drDetail in drWaitTrans)
                {
                    string FImgID = drDetail["FImgID"].ToString();

                    DataRow drServerDetail = dtImgResutl.Rows.Find(FImgID);
                    if (drServerDetail == null)
                    {
                        drServerDetail = dtImgResutl.NewRow();

                        drServerDetail["FImgID"] = FImgID;
                        drServerDetail["FTaskID"] = FTaskID;
                    }

                    drServerDetail["FImg"] = drDetail["FImg"];
                    drServerDetail["FImgType"] = drDetail["FImgType"];
                    drServerDetail["FImgNmae"] = drDetail["FImgNmae"];
                    drServerDetail["FOrder"] = drDetail["FOrder"];
                    drServerDetail["FRemark"] = drDetail["FRemark"];

                    if (drServerDetail.RowState == DataRowState.Detached)
                    {
                        dtImgResutl.Rows.Add(drServerDetail);
                    }
                }

                //执行远程对象传输图像
                int ie = faxBus.DbUpdateDataSetNew(connstr, dsImgResutl, dtImgResutl.TableName, "FImgID", "Lis_Ins_Result_Img_ADD", "Lis_Ins_Result_Img_Delete", "Lis_Ins_Result_Img_Update", 0);
                if (ie != iWaitTrans)
                {
                    exImg = new Exception("同步图形结果至服务器时失败,应同步数: " + iWaitTrans.ToString() + " ,实际同步数: " + ie.ToString());
                    bolResult = false;
                }

                daResult_ImgDetail = null;
                dsResult_ImgDetail = null;
                drWaitTrans = null;
                dsImgResutl = null;
                dtImgResutl = null;
            }
            catch(Exception e)
            {
                exImg = new Exception("同步图形结果至服务器时失败."+e.Message, e);
                bolResult = false;
            }

            return bolResult;
        }
    }
}
