using System;
using System.Collections.Generic;
using System.Text;

namespace Decode
{
    /// <summary> 仪器结果类型列表
    /// </summary>
    public static class ResultTypeList
    {
        private static ResultType normal = new ResultType("1401", "常规");
        private static ResultType emergency = new ResultType("1402", "急诊");
        private static ResultType quality = new ResultType("1403", "质控");

        /// <summary> 常规
        /// </summary>
        public static ResultType Normal
        {
            get
            {
                return normal;
            }
        }

        /// <summary> 急诊
        /// </summary>
        public static ResultType Emergency
        {
            get
            {
                return emergency;
            }
        }

        /// <summary> 质控
        /// </summary>
        public static ResultType Quality
        {
            get
            {
                return quality;
            }
        }
    
    
        /// <summary> 根据结果类型ID返回结果类型名称
        /// 供临时使用,由于前期部份代码结果类型未用对象
        /// </summary>
        /// <param name="ResultTypeID"></param>
        /// <returns></returns>
        public static string GetResultTypeNameByID(string ResultTypeID)
        {
            switch (ResultTypeID)
            {
                case "1402":
                    return "急诊";
                case "1403":
                    return "质控";
                default:
                    return "常规";
            }
        }
    }
}
