using System;
using System.Collections.Generic;
using System.Text;

namespace Decode
{
    /// <summary> 仪器结果类型
    /// </summary>
    public class ResultType
    {
        private string typeID;
        private string typeName;

        /// <summary> 结果类型ID
        /// </summary>
        public string TypeID
        {
            get
            {
                return typeID;
            }
            set
            {
                typeID = value;
            }
        }

        /// <summary> 结果类型名称
        /// </summary>
        public string TypeName
        {
            get
            {
                return typeName;
            }
            set
            {
                typeName = value;
            }
        }

        public ResultType(string cTypeID, string cTypeName)
        {
            TypeID = cTypeID;
            TypeName = cTypeName;
        }
    }
}
