﻿namespace Decode
{
    partial class DecodeMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DecodeMain));
            this.stspStatus = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tllsHandDecode = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslDecodeStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslLocalCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslDecodeCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslSaveCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslWaitTranseCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslActualTrans = new System.Windows.Forms.ToolStripStatusLabel();
            this.tllsVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPDecode = new System.Windows.Forms.TabPage();
            this.pnlPassword = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.pnlBuffterData = new System.Windows.Forms.Panel();
            this.txbBufferPortDataLog = new System.Windows.Forms.TextBox();
            this.txbBufferPortData = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.rtfTerminal = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.清空数据解码日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重新解码今日数据ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tbpSysLog = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示窗体ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.重新解码今日数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.退出ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.开机启动ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stspStatus.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPDecode.SuspendLayout();
            this.pnlPassword.SuspendLayout();
            this.pnlBuffterData.SuspendLayout();
            this.panel4.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.tbpSysLog.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // stspStatus
            // 
            this.stspStatus.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.stspStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tllsHandDecode,
            this.tsslDecodeStatus,
            this.toolStripStatusLabel2,
            this.tsslLocalCount,
            this.tsslDecodeCount,
            this.tsslSaveCount,
            this.toolStripStatusLabel3,
            this.tsslWaitTranseCount,
            this.tsslActualTrans,
            this.tllsVersion});
            this.stspStatus.Location = new System.Drawing.Point(0, 413);
            this.stspStatus.Name = "stspStatus";
            this.stspStatus.Size = new System.Drawing.Size(693, 25);
            this.stspStatus.TabIndex = 1;
            this.stspStatus.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripStatusLabel1.Image = global::Decode.Properties.Resources.Sound;
            this.toolStripStatusLabel1.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.LinkVisited = true;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(23, 20);
            this.toolStripStatusLabel1.Text = "是否在成功接收数据后发出声音";
            this.toolStripStatusLabel1.ToolTipText = "如果你觉得每次收到数据都“叮咚”的响很烦，那麻烦你点一下它就不响了";
            this.toolStripStatusLabel1.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // tllsHandDecode
            // 
            this.tllsHandDecode.AutoToolTip = true;
            this.tllsHandDecode.BackColor = System.Drawing.SystemColors.Control;
            this.tllsHandDecode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tllsHandDecode.Image = ((System.Drawing.Image)(resources.GetObject("tllsHandDecode.Image")));
            this.tllsHandDecode.ImageTransparentColor = System.Drawing.Color.White;
            this.tllsHandDecode.Name = "tllsHandDecode";
            this.tllsHandDecode.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.tllsHandDecode.Size = new System.Drawing.Size(23, 20);
            this.tllsHandDecode.Text = "手动解码";
            this.tllsHandDecode.ToolTipText = "手动解码";
            this.tllsHandDecode.Click += new System.EventHandler(this.tllsHandDecode_Click);
            // 
            // tsslDecodeStatus
            // 
            this.tsslDecodeStatus.AutoSize = false;
            this.tsslDecodeStatus.Name = "tsslDecodeStatus";
            this.tsslDecodeStatus.Size = new System.Drawing.Size(150, 20);
            this.tsslDecodeStatus.Spring = true;
            this.tsslDecodeStatus.Text = "解码状态";
            this.tsslDecodeStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.AutoSize = false;
            this.toolStripStatusLabel2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(260, 20);
            this.toolStripStatusLabel2.Text = "客户端信息";
            this.toolStripStatusLabel2.ToolTipText = "客户端信息";
            this.toolStripStatusLabel2.Click += new System.EventHandler(this.toolStripStatusLabel2_Click);
            // 
            // tsslLocalCount
            // 
            this.tsslLocalCount.AutoSize = false;
            this.tsslLocalCount.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.tsslLocalCount.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.tsslLocalCount.Name = "tsslLocalCount";
            this.tsslLocalCount.Size = new System.Drawing.Size(44, 20);
            this.tsslLocalCount.Text = "已处理:";
            this.tsslLocalCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsslLocalCount.ToolTipText = "已处理样品数量";
            // 
            // tsslDecodeCount
            // 
            this.tsslDecodeCount.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.tsslDecodeCount.Name = "tsslDecodeCount";
            this.tsslDecodeCount.Size = new System.Drawing.Size(15, 20);
            this.tsslDecodeCount.Text = "0";
            this.tsslDecodeCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tsslDecodeCount.ToolTipText = "已处理样品数量";
            this.tsslDecodeCount.Click += new System.EventHandler(this.tsslDecodeCount_Click);
            // 
            // tsslSaveCount
            // 
            this.tsslSaveCount.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.tsslSaveCount.Name = "tsslSaveCount";
            this.tsslSaveCount.Size = new System.Drawing.Size(20, 20);
            this.tsslSaveCount.Text = "/0";
            this.tsslSaveCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsslSaveCount.ToolTipText = "已传";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.AutoSize = false;
            this.toolStripStatusLabel3.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel3.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(48, 20);
            this.toolStripStatusLabel3.Text = "已传输:";
            this.toolStripStatusLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripStatusLabel3.ToolTipText = "已传输样品数量";
            // 
            // tsslWaitTranseCount
            // 
            this.tsslWaitTranseCount.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.tsslWaitTranseCount.ForeColor = System.Drawing.Color.Red;
            this.tsslWaitTranseCount.Name = "tsslWaitTranseCount";
            this.tsslWaitTranseCount.Size = new System.Drawing.Size(20, 20);
            this.tsslWaitTranseCount.Text = "0/";
            this.tsslWaitTranseCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tsslWaitTranseCount.ToolTipText = "待传";
            // 
            // tsslActualTrans
            // 
            this.tsslActualTrans.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.tsslActualTrans.Name = "tsslActualTrans";
            this.tsslActualTrans.Size = new System.Drawing.Size(15, 20);
            this.tsslActualTrans.Text = "0";
            this.tsslActualTrans.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsslActualTrans.ToolTipText = "已传";
            this.tsslActualTrans.Click += new System.EventHandler(this.tsslActualTrans_Click);
            // 
            // tllsVersion
            // 
            this.tllsVersion.AutoSize = false;
            this.tllsVersion.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.tllsVersion.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.tllsVersion.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tllsVersion.Name = "tllsVersion";
            this.tllsVersion.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tllsVersion.Size = new System.Drawing.Size(60, 20);
            this.tllsVersion.Text = "V0.0.0";
            this.tllsVersion.ToolTipText = "已处理样品";
            this.tllsVersion.Click += new System.EventHandler(this.tllsVersion_Click);
            this.tllsVersion.MouseHover += new System.EventHandler(this.tllsVersion_MouseHover);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPDecode);
            this.tabControl1.Controls.Add(this.tbpSysLog);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(693, 413);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPDecode
            // 
            this.tabPDecode.Controls.Add(this.pnlPassword);
            this.tabPDecode.Controls.Add(this.pnlBuffterData);
            this.tabPDecode.Controls.Add(this.rtfTerminal);
            this.tabPDecode.Location = new System.Drawing.Point(4, 22);
            this.tabPDecode.Name = "tabPDecode";
            this.tabPDecode.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPDecode.Size = new System.Drawing.Size(685, 387);
            this.tabPDecode.TabIndex = 1;
            this.tabPDecode.Text = "解码日志";
            this.tabPDecode.UseVisualStyleBackColor = true;
            // 
            // pnlPassword
            // 
            this.pnlPassword.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPassword.Controls.Add(this.button4);
            this.pnlPassword.Controls.Add(this.maskedTextBox1);
            this.pnlPassword.Controls.Add(this.label1);
            this.pnlPassword.Controls.Add(this.button3);
            this.pnlPassword.Location = new System.Drawing.Point(237, 100);
            this.pnlPassword.Name = "pnlPassword";
            this.pnlPassword.Size = new System.Drawing.Size(167, 89);
            this.pnlPassword.TabIndex = 20;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(79, 48);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(39, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "关闭";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(18, 21);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.PasswordChar = '*';
            this.maskedTextBox1.Size = new System.Drawing.Size(100, 21);
            this.maskedTextBox1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "请输入工程师密码";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(18, 48);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(55, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "确定";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pnlBuffterData
            // 
            this.pnlBuffterData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBuffterData.Controls.Add(this.txbBufferPortDataLog);
            this.pnlBuffterData.Controls.Add(this.txbBufferPortData);
            this.pnlBuffterData.Controls.Add(this.panel4);
            this.pnlBuffterData.Location = new System.Drawing.Point(25, 25);
            this.pnlBuffterData.Name = "pnlBuffterData";
            this.pnlBuffterData.Padding = new System.Windows.Forms.Padding(1);
            this.pnlBuffterData.Size = new System.Drawing.Size(631, 278);
            this.pnlBuffterData.TabIndex = 23;
            this.pnlBuffterData.MouseLeave += new System.EventHandler(this.pnlBuffterData_MouseLeave);
            // 
            // txbBufferPortDataLog
            // 
            this.txbBufferPortDataLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbBufferPortDataLog.Location = new System.Drawing.Point(1, 1);
            this.txbBufferPortDataLog.MaxLength = 107200;
            this.txbBufferPortDataLog.Multiline = true;
            this.txbBufferPortDataLog.Name = "txbBufferPortDataLog";
            this.txbBufferPortDataLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txbBufferPortDataLog.Size = new System.Drawing.Size(290, 238);
            this.txbBufferPortDataLog.TabIndex = 20;
            // 
            // txbBufferPortData
            // 
            this.txbBufferPortData.Dock = System.Windows.Forms.DockStyle.Right;
            this.txbBufferPortData.Location = new System.Drawing.Point(291, 1);
            this.txbBufferPortData.MaxLength = 107200;
            this.txbBufferPortData.Multiline = true;
            this.txbBufferPortData.Name = "txbBufferPortData";
            this.txbBufferPortData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txbBufferPortData.Size = new System.Drawing.Size(335, 238);
            this.txbBufferPortData.TabIndex = 19;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(1, 239);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(625, 34);
            this.panel4.TabIndex = 18;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(457, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "关闭本窗口";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(347, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "手动解码";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rtfTerminal
            // 
            this.rtfTerminal.ContextMenuStrip = this.contextMenuStrip2;
            this.rtfTerminal.DetectUrls = false;
            this.rtfTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtfTerminal.Location = new System.Drawing.Point(3, 3);
            this.rtfTerminal.MaxLength = 107200;
            this.rtfTerminal.Name = "rtfTerminal";
            this.rtfTerminal.Size = new System.Drawing.Size(679, 381);
            this.rtfTerminal.TabIndex = 10;
            this.rtfTerminal.Text = "";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.AccessibleDescription = "";
            this.contextMenuStrip2.AccessibleName = "";
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.清空数据解码日志ToolStripMenuItem,
            this.重新解码今日数据ToolStripMenuItem1});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(173, 70);
            this.contextMenuStrip2.Tag = "";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(172, 22);
            this.toolStripMenuItem2.Text = "打开调试窗口";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // 清空数据解码日志ToolStripMenuItem
            // 
            this.清空数据解码日志ToolStripMenuItem.Name = "清空数据解码日志ToolStripMenuItem";
            this.清空数据解码日志ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.清空数据解码日志ToolStripMenuItem.Text = "清空解码日志";
            this.清空数据解码日志ToolStripMenuItem.Click += new System.EventHandler(this.清空数据解码日志ToolStripMenuItem_Click);
            // 
            // 重新解码今日数据ToolStripMenuItem1
            // 
            this.重新解码今日数据ToolStripMenuItem1.Name = "重新解码今日数据ToolStripMenuItem1";
            this.重新解码今日数据ToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.重新解码今日数据ToolStripMenuItem1.Text = "重新解码今日数据";
            this.重新解码今日数据ToolStripMenuItem1.Click += new System.EventHandler(this.ReDecodeTodayData);
            // 
            // tbpSysLog
            // 
            this.tbpSysLog.Controls.Add(this.panel3);
            this.tbpSysLog.Controls.Add(this.splitter2);
            this.tbpSysLog.Controls.Add(this.panel2);
            this.tbpSysLog.Controls.Add(this.splitter1);
            this.tbpSysLog.Controls.Add(this.panel1);
            this.tbpSysLog.Location = new System.Drawing.Point(4, 22);
            this.tbpSysLog.Name = "tbpSysLog";
            this.tbpSysLog.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tbpSysLog.Size = new System.Drawing.Size(685, 392);
            this.tbpSysLog.TabIndex = 2;
            this.tbpSysLog.Text = "系统日志";
            this.tbpSysLog.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textBox3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(428, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(254, 386);
            this.panel3.TabIndex = 20;
            // 
            // textBox3
            // 
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.Location = new System.Drawing.Point(0, 0);
            this.textBox3.MaxLength = 100;
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox3.Size = new System.Drawing.Size(254, 386);
            this.textBox3.TabIndex = 15;
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(425, 3);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 386);
            this.splitter2.TabIndex = 19;
            this.splitter2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(209, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(216, 386);
            this.panel2.TabIndex = 18;
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Location = new System.Drawing.Point(0, 0);
            this.textBox2.MaxLength = 100;
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox2.Size = new System.Drawing.Size(216, 386);
            this.textBox2.TabIndex = 14;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(206, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 386);
            this.splitter1.TabIndex = 17;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 386);
            this.panel1.TabIndex = 16;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.MaxLength = 100;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(203, 386);
            this.textBox1.TabIndex = 13;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.AccessibleDescription = "";
            this.contextMenuStrip1.AccessibleName = "";
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示窗体ToolStripMenuItem,
            this.toolStripSeparator2,
            this.重新解码今日数据ToolStripMenuItem,
            this.toolStripSeparator1,
            this.开机启动ToolStripMenuItem,
            this.退出ToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(173, 126);
            this.contextMenuStrip1.Tag = "";
            // 
            // 显示窗体ToolStripMenuItem
            // 
            this.显示窗体ToolStripMenuItem.Name = "显示窗体ToolStripMenuItem";
            this.显示窗体ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.显示窗体ToolStripMenuItem.Text = "显示窗口";
            this.显示窗体ToolStripMenuItem.Click += new System.EventHandler(this.显示窗体ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(169, 6);
            // 
            // 重新解码今日数据ToolStripMenuItem
            // 
            this.重新解码今日数据ToolStripMenuItem.Name = "重新解码今日数据ToolStripMenuItem";
            this.重新解码今日数据ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.重新解码今日数据ToolStripMenuItem.Text = "重新解码今日数据";
            this.重新解码今日数据ToolStripMenuItem.Click += new System.EventHandler(this.ReDecodeTodayData);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(169, 6);
            // 
            // 退出ToolStripMenuItem1
            // 
            this.退出ToolStripMenuItem1.Name = "退出ToolStripMenuItem1";
            this.退出ToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.退出ToolStripMenuItem1.Text = "退出";
            this.退出ToolStripMenuItem1.Click += new System.EventHandler(this.退出ToolStripMenuItem1_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "数据接收";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // toolTip1
            // 
            this.toolTip1.Active = false;
            this.toolTip1.ToolTipTitle = "提示";
            // 
            // 开机启动ToolStripMenuItem
            // 
            this.开机启动ToolStripMenuItem.Name = "开机启动ToolStripMenuItem";
            this.开机启动ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.开机启动ToolStripMenuItem.Text = "开机启动";
            this.开机启动ToolStripMenuItem.Click += new System.EventHandler(this.开机启动ToolStripMenuItem_Click);
            // 
            // DecodeMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 438);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.stspStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DecodeMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "仪器数据解码";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ComReceive_FormClosing);
            this.Load += new System.EventHandler(this.DecodeMain_Load);
            this.Shown += new System.EventHandler(this.DecodeMain_Shown);
            this.Resize += new System.EventHandler(this.DecodeMain_Resize);
            this.stspStatus.ResumeLayout(false);
            this.stspStatus.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPDecode.ResumeLayout(false);
            this.pnlPassword.ResumeLayout(false);
            this.pnlPassword.PerformLayout();
            this.pnlBuffterData.ResumeLayout(false);
            this.pnlBuffterData.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.tbpSysLog.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip stspStatus;
        private System.Windows.Forms.ToolStripStatusLabel tsslDecodeStatus;
        private System.Windows.Forms.ToolStripStatusLabel tsslDecodeCount;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPDecode;
        private System.Windows.Forms.RichTextBox rtfTerminal;
        private System.Windows.Forms.TabPage tbpSysLog;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 显示窗体ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tsslActualTrans;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tllsVersion;
        private System.Windows.Forms.ToolStripStatusLabel tllsHandDecode;
        private System.Windows.Forms.ToolStripStatusLabel tsslWaitTranseCount;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripStatusLabel tsslLocalCount;
        private System.Windows.Forms.ToolStripStatusLabel tsslSaveCount;
        private System.Windows.Forms.Panel pnlBuffterData;
        private System.Windows.Forms.TextBox txbBufferPortData;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlPassword;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txbBufferPortDataLog;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 重新解码今日数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 清空数据解码日志ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 重新解码今日数据ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 开机启动ToolStripMenuItem;        
    }
}