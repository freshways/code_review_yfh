using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Decode
{
    public partial class Msgbox : Form
    {
        public Msgbox()
        {
            InitializeComponent();
        }

        /// <summary> 使用form显示消息
        /// </summary>
        /// <param name="msg">消息内容</param>
        public void Showmsg(string msg)
        {
            this.ShowDialog();
            txbMsg.Text = msg;
        }
    }
}