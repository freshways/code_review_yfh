using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;

using Decode.Properties;
using DecodeInterface;

namespace Decode
{
    class LocalDataBase
    {
        private OleDbConnection lconComDataBase = new OleDbConnection();
        private OleDbConnection lconComBuffer = new OleDbConnection();

        CommRuler CommRuler = new CommRuler();

        //增加解码结果后报告
        public delegate void WriteResultHandler(object sender, String ItemInfo, Exception ex, object[] objSaveOLD);
        public event WriteResultHandler WriteResult;

        //载入配置
        string RelayItems;
        string RelayInstr;
        bool OverwriteReItem;
        bool bolUpdateStatusOnOver;//解决快速写两条时,传输状态可能不对的问题

        /// <summary>
        /// 应用程序启动路径
        /// </summary>
        string ApplicationStartPath = "";

        public LocalDataBase(string ApplicationPath)
        {
            ApplicationStartPath = ApplicationPath;

            RelayItems = Settings.Default.RelayItems;
            RelayInstr = Settings.Default.RelayInstr;
            OverwriteReItem = Settings.Default.OverwriteReItem;
            bolUpdateStatusOnOver = Settings.Default.UpdateStatusOnOver;

            ConnectionPortDataBase();
            ConnectionComBufferDB();
        }

        /// <summary> 获取解码结果连接对象
        /// </summary>
        public OleDbConnection conComDataBase
        {
            get
            {
                OpenDataBase();
                return lconComDataBase;
            }
        }
        /// <summary> 打开数据库
        /// </summary>
        public void OpenDataBase()
        {
            if (lconComDataBase.State != ConnectionState.Open)
            {
                lconComDataBase.Open();
            }
        }

        /// <summary> 关闭 解码结果连接对象
        /// </summary>
        public void Close()
        {
            if (lconComDataBase.State == ConnectionState.Open)
            {
                lconComDataBase.Close();
            }
        }

        /// <summary> 获取 串口缓冲连接对象
        /// </summary>
        public OleDbConnection conComBuffer
        {
            get
            {
                if (lconComBuffer.State != ConnectionState.Open)
                {
                    lconComBuffer.Open();
                }
                return lconComBuffer;
            }
        }
        /// <summary> 关闭 串口缓冲连接对象
        /// </summary>
        public void CloseComBuffer()
        {
            if (lconComBuffer.State == ConnectionState.Open)
            {
                lconComBuffer.Close();
            }
        }

        /// <summary> 连接本地ACCESS解码结果数据库
        /// </summary>
        private void ConnectionPortDataBase()
        {
            try
            {
                if (lconComDataBase.State != ConnectionState.Open)
                {
                    //string strDBPath = System.IO.Path.GetFullPath("DecodeData.mdb");
                    string strDBPath = ApplicationStartPath + @"\DecodeData.mdb";
                    //strDBPath = @"D:\柯达950\ComData.mdb";
                    lconComDataBase.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Password=;Data Source=" + strDBPath + ";Persist Security Info=True";                    
                }
            }
            catch
            {                
            }
        }

        /// <summary> 连接本地ACCESS串口缓冲数据库
        /// </summary>
        private void ConnectionComBufferDB()
        {
            try
            {
                if (lconComBuffer.State != ConnectionState.Open)
                {
                    //string strDBPath = System.IO.Path.GetFullPath("ComData.mdb");
                    string strDBPath = ApplicationStartPath + @"\ComData.mdb";
                    lconComBuffer.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Password=;Data Source=" + strDBPath + ";Persist Security Info=True";
                }
            }
            catch
            {
            }
        }

        /// <summary> 存储解码结果
        /// </summary>
        /// <param name="strType"></param>
        /// <param name="strSampleNo"></param>
        /// <param name="strDate"></param>
        /// <param name="ItemResults"></param>
        /// <param name="ItemImgResults"></param>
        /// <param name="strSampleData"></param>
        /// <param name="Heat"></param>
        /// <param name="XYDepth"></param>
        /// <param name="Haemoglobin"></param>
        /// <param name="RetryCount"></param>
        //string LastSampleNo = "";
        public void addLocalResult(string strType, string strSampleNo, string strDate, List<ItemResult> ItemResults, List<ItemImgResult> ItemImgResults, string strSampleData, float Heat, float XYDepth, float Haemoglobin, int RetryCount)
        {
            // 记录原始对象,供返回时使用
            object[] objSaveOLD = new object[10];
            objSaveOLD[0] = strType;
            objSaveOLD[1] = strSampleNo;
            objSaveOLD[2] = strDate;
            objSaveOLD[3] = ItemResults;
            objSaveOLD[4] = ItemImgResults;
            objSaveOLD[5] = strSampleData;
            objSaveOLD[6] = RetryCount + 1;
            objSaveOLD[7] = Heat;
            objSaveOLD[8] = XYDepth;
            objSaveOLD[9] = Haemoglobin;
            
            try
            {
                // 为普通标本及急诊标本号加前辍
                int Samplenosub = Settings.Default.Samplenosub;
                int StatSamplenosub = Settings.Default.StatSamplenosub;

                if ((Samplenosub > 0) && (strType == "1401") && (!String.IsNullOrEmpty(strSampleNo)))
                {
                    strSampleNo = Convert.ToString(Samplenosub + Convert.ToInt32(strSampleNo));
                }
                if ((StatSamplenosub > 0) && (strType == "1402") && (!String.IsNullOrEmpty(strSampleNo)))
                {
                    strSampleNo = Convert.ToString(StatSamplenosub + Convert.ToInt32(strSampleNo));
                }

                //// 填充长度(不够3位前加0)
                //int SampleLen = strSampleNo.Length;
                //if (SampleLen < 3)
                //{
                //    for (int i = 0; i < 3 - SampleLen; i++)
                //    {
                //        strSampleNo = "0" + strSampleNo;
                //    }
                //}

                // 无结果的不存储
                if (ItemResults.Count == 0)
                {
                    OnWriteResult("NoResult", null, objSaveOLD);
                }

                Exception Taskex;

                #region 存储需要转发存储的结果
                //适用于某些仪器的某些项目要始终合并到其它仪器出报告时使用

                if ((!string.IsNullOrEmpty(RelayItems)) && (!string.IsNullOrEmpty(RelayInstr)))
                {
                    List<ItemResult> ItemResultsRelay = new List<ItemResult>();
                    string[] arryRelayItems = RelayItems.Split('|');
                    int iRelayItemCount = arryRelayItems.Length;
                    int iItemResultsCount = ItemResults.Count;
                    string strItemID;

                    // 循环结果
                    for (int i = 0; i < iItemResultsCount; i++)
                    {
                        strItemID = ItemResults[i].ItemID;

                        // 循环待转发
                        for (int j = 0; j < iRelayItemCount; j++)
                        {
                            if ((iRelayItemCount == 1) && (arryRelayItems[0].ToString() == "ALL"))
                            {
                                // 如果待转发项目为ALL,则转发所有
                                ItemResultsRelay.Add(ItemResults[i]);
                            }
                            else
                            {
                                if (strItemID == arryRelayItems[j].ToString())
                                {
                                    //将需要转发的结果存到待转发列表
                                    //从原始结果列表移出转发的结果
                                    ItemResultsRelay.Add(ItemResults[i]);
                                    ItemResults.RemoveAt(i);
                                    iItemResultsCount = ItemResults.Count;
                                    i = i - 1;

                                    break;
                                }
                            }
                        }
                    }

                    if (ItemResultsRelay.Count > 0)
                    {
                        addLocalResultBody(RelayInstr, strType, strSampleNo, strDate, ItemResultsRelay, ItemImgResults, strSampleData, Heat, XYDepth, Haemoglobin, out Taskex, objSaveOLD);
                    }
                }

                #endregion

                //存储本结果,如果已无本结果(全部转发了),则不存
                if ((ItemResults!=null&&ItemResults.Count > 0) || (ItemImgResults != null && ItemImgResults.Count > 0))
                {
                    addLocalResultBody(Settings.Default.InstrID, strType, strSampleNo, strDate, ItemResults, ItemImgResults, strSampleData, Heat, XYDepth, Haemoglobin, out Taskex, objSaveOLD);
                }
            }
            catch (Exception ex)
            {
                OnWriteResult("存储解码结果至本地数据库失败,结果号:" + strSampleNo, ex, objSaveOLD);
            }
            finally
            {
                objSaveOLD = null;

                GC.Collect(0);
                GC.Collect(1);
                GC.Collect(2);
                GC.Collect();
            }
        }

        /// <summary> 存储解码结果主过程
        /// </summary>
        /// <param name="InstrID"></param>
        /// <param name="strType"></param>
        /// <param name="strSampleNo"></param>
        /// <param name="strDate"></param>
        /// <param name="ItemResults"></param>
        /// <param name="ItemImgResults"></param>
        /// <param name="strSampleData"></param>
        /// <param name="Heat"></param>
        /// <param name="XYDepth"></param>
        /// <param name="Haemoglobin"></param>
        /// <param name="Taskex"></param>
        /// <param name="objSaveOLD"></param>
        string LastSampleNo = "";
        private void addLocalResultBody(string InstrID, string strType, string strSampleNo, string strDate, List<ItemResult> ItemResults, List<ItemImgResult> ItemImgResults, string strSampleData, float Heat, float XYDepth, float Haemoglobin, out Exception Taskex, object[] objSaveOLD)
        {
            #region 执行本地存储: 先存主表,再存从表

            string strTasikID = addLocalResult_Mast(InstrID, strType, strSampleNo, strDate, strSampleData, Heat, XYDepth, Haemoglobin, out Taskex);

            if (strTasikID != "0")
            {
                Exception Errorexp;

                // 存储结果
                bool bolSave = addLocalResult_Detail(strTasikID, strDate, ItemResults, out Errorexp);
                if (!bolSave)
                {
                    OnWriteResult("存储解码结果明细至本地数据库失败,结果号:" + strSampleNo, Errorexp, objSaveOLD);
                }

                // 存储图像
                if (ItemImgResults != null)
                {
                    bolSave = addLocalResult_Img(strTasikID, ItemImgResults, out Errorexp);

                    if (!bolSave)
                    {
                        OnWriteResult("存储解码结果图形至本地数据库失败,结果号:" + strSampleNo, Errorexp, objSaveOLD);
                    }
                }

                // 写空行分隔一下
                //OnWriteResult("11" + '\r' + '\n', null, null);

                #region 解决快速写两条时,传输状态可能不对的问题

                if (bolUpdateStatusOnOver)
                {
                    //仅当本次样品号与上次样品号相同时执行
                    //是个预防

                    if (LastSampleNo == strSampleNo)
                    {
                        // 再加个延时,避免上一条正在传输时,执行到下面的将状态改为未传输
                        // 当还没取到新加的数据时,它就变成已传输
                        // 这样就让上一条先传完成再走后半条
                        // 2008-11-25
                        OnWriteResult("启动改写结果状态 " + System.DateTime.Now.ToString(), null, null);
                        System.Threading.Thread.Sleep(1500);

                        int iUpdate = UpdateResultStatus(strTasikID);
                        if (iUpdate > 0)
                        {
                            OnWriteResult("改写结果状态成功 " + System.DateTime.Now.ToString() + "修改记录条数:" + iUpdate.ToString(), null, null);
                        }
                        else
                        {
                            OnWriteResult("更新本地结果传输入状态时出错,结果号:" + strSampleNo, Taskex, objSaveOLD);
                        }
                    }
                    LastSampleNo = strSampleNo;
                }

                #endregion

                OnWriteResult("WriteLocalResultOver", Taskex, objSaveOLD);
            }
            else
            {
                OnWriteResult("存储解码结果至本地数据库失败,结果号:" + strSampleNo, Taskex, objSaveOLD);
            }

            #endregion
        }

        /// <summary>写入解码结果主表
        /// 检查样品主记录是否存在
        /// 检查条件：类型+样品号+日期时间
        /// 存在则更新,并返回ID,否则新增返回新GUID
        /// 出错返回0
        /// </summary>
        /// <param name="strType"></param>
        /// <param name="strSampleID"></param>
        /// <param name="strDateTime"></param>
        /// <param name="strSampleData"></param>
        /// <param name="Heat"></param>
        /// <param name="XYDepth"></param>
        /// <param name="Haemoglobin"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        private string addLocalResult_Mast(string InstrID,string strType, string strSampleID, string strDateTime, string strSampleData, float Heat, float XYDepth, float Haemoglobin, out Exception ex)
        {
            ex = null;
            string strGUID;

            try
            {
                string strSql = "select * from Lis_ComPort_Result where FInstrID ='" + InstrID + "' and FType='" + strType + "' and FDateTime=CDate('" + strDateTime + "') and FResultID='" + strSampleID + "'";

                OleDbDataAdapter daResult = new OleDbDataAdapter(strSql, this.conComDataBase);
                OleDbCommandBuilder cb = new OleDbCommandBuilder(daResult);
                DataSet Task = new DataSet();
                daResult.Fill(Task, "Result");
                this.Close();

                if (Task.Tables[0].Rows.Count > 0)
                {
                    DataRow TaskRow = Task.Tables[0].Rows[0];
                    TaskRow["FRXD"] = TaskRow["FRXD"].ToString() + strSampleData;
                    TaskRow["FUpdateTime"] = System.DateTime.Now;
                    TaskRow["FStatus"] = 0;
                    TaskRow["FInstrID"] = InstrID;

                    TaskRow["FHeat"] = Heat;
                    TaskRow["FXYDepth"] = XYDepth;
                    TaskRow["FHaemoglobin"] = Haemoglobin;

                    daResult.Update(Task, "Result");

                    strGUID = TaskRow["FTaskID"].ToString();
                    TaskRow = null;
                }
                else
                {
                    strGUID = CommRuler.SysGetGUID();
                    DataRow TaskRow = Task.Tables[0].Rows.Add();
                    TaskRow["FTaskID"] = strGUID;
                    TaskRow["FInstrID"] = InstrID;
                    TaskRow["FResultID"] = strSampleID;
                    TaskRow["FDateTime"] = strDateTime;
                    TaskRow["FValidity"] = true;
                    TaskRow["FType"] = strType;
                    TaskRow["FRXD"] = strSampleData;
                    TaskRow["FStatus"] = 0;

                    // 旧版本的数据库无这三个字段,所以判断一下
                    if (Task.Tables[0].Columns.Contains("FHeat"))
                    {
                        TaskRow["FHeat"] = Heat;
                        TaskRow["FXYDepth"] = XYDepth;
                        TaskRow["FHaemoglobin"] = Haemoglobin;
                    }

                    TaskRow["FCreateTime"] = System.DateTime.Now;
                    daResult.Update(Task, "Result");
                    TaskRow = null;
                }

                string strTitle = "※样品号:" + ResultTypeList.GetResultTypeNameByID(strType) + " " + strSampleID + "  检验时间:" + strDateTime;
                string strExpTitle = "";
                // 增加显示扩充参数
                if (Heat > 0)
                {
                    strExpTitle = strExpTitle + "体温:" + Heat.ToString();
                }
                if (XYDepth > 0)
                {
                    strExpTitle = strExpTitle + " 吸氧浓度:" + XYDepth.ToString();
                }
                if (Haemoglobin > 0)
                {
                    strExpTitle = strExpTitle + " 血红蛋白:" + Haemoglobin.ToString();
                }
                if (Heat > 0 || XYDepth > 0 || Haemoglobin > 0)
                {
                    strTitle = strTitle + '\r' + strExpTitle;
                }
                
                OnWriteResult("\r\n\r\n" + strTitle, null, null);

                daResult = null;
                cb = null;
                Task = null;

                return strGUID;
            }
            catch (Exception e)
            {
                ex = e;
                return "0";
            }
        }

        /// <summary> 更新本地结果状态为未传输
        /// </summary>
        /// <param name="TaskID"></param>
        /// <returns></returns>
        public int UpdateResultStatus(string TaskID)
        {
            try
            {
                string strsql = "update Lis_ComPort_Result set FStatus=0 where FTaskID='" + TaskID + "'";
                OleDbCommand cmd = new OleDbCommand(strsql, this.conComDataBase);
                int iDelCount = cmd.ExecuteNonQuery();
                this.Close();
                cmd = null;
                return iDelCount;
            }
            catch
            {                
                return -1;
            }
        }

        /// <summary>写入解码结果明细
        /// 检查项目及结果是否存在
        /// 检查条件：结果ID+项目代号+项目结果+时间
        /// </summary>
        /// <param name="TaskID"></param>
        /// <param name="strDateTime"></param>
        /// <param name="ItemResults"></param>
        /// <param name="outEx"></param>
        /// <returns></returns>
        private bool addLocalResult_Detail(string TaskID, string strDateTime, List<ItemResult> ItemResults,out Exception outEx)
        {
            outEx = null;

            try
            {
                int iItemCount = ItemResults.Count;
                int iResultCount, iHaveCount;
                bool bolIsNewRow;
                string strWritResult = '\r' + "";
                string strSql = "select * from Lis_ComPort_Result_Detail where FTaskID='" + TaskID + "' and FTime=CDate('" + strDateTime + "')";

                OleDbDataAdapter daResult = new OleDbDataAdapter(strSql, this.conComDataBase);
                OleDbCommandBuilder cb = new OleDbCommandBuilder(daResult);
                DataSet Task = new DataSet();
                daResult.Fill(Task, "Detail");
                DataView dva = new DataView(Task.Tables[0]);
                this.Close();
                iResultCount = Task.Tables[0].Rows.Count;

                for (int i = 0; i < iItemCount; i++)
                {
                    string strGUID;
                    string strItem = ItemResults[i].ItemID;
                    string strResult = ItemResults[i].ItemValue;
                    bolIsNewRow = true;

                    if (strItem != null)
                    {
                        if (iResultCount > 0)
                        {
                            if (OverwriteReItem)
                            {
                                dva.RowFilter = "FItem='" + strItem + "'";

                                if (dva.ToTable().Rows.Count > 0)
                                {
                                    DataRow TaskRow = dva[0].Row;
                                    TaskRow["FValue"] = strResult;
                                    TaskRow["FTime"] = strDateTime;
                                    TaskRow["FCreateTime"] = System.DateTime.Now;
                                    daResult.Update(Task, "Detail");
                                    TaskRow = null;
                                }
                            }
                            else
                            {
                                dva.RowFilter = "FItem='" + strItem + "' AND FValue='" + strResult + "'";
                            }

                            iHaveCount = dva.ToTable().Rows.Count;
                            if (iHaveCount > 0)
                            {
                                bolIsNewRow = false;
                            }
                        }

                        if (bolIsNewRow)
                        {
                            strGUID = CommRuler.SysGetGUID();
                            DataRow TaskRow = Task.Tables[0].Rows.Add();
                            TaskRow["FID"] = strGUID;
                            TaskRow["FTaskID"] = TaskID;
                            TaskRow["FItem"] = strItem;
                            TaskRow["FValue"] = strResult;
                            TaskRow["FTime"] = strDateTime;
                            TaskRow["FCreateTime"] = System.DateTime.Now;
                            daResult.Update(Task, "Detail");
                            TaskRow = null;
                        }

                        strWritResult += strItem + ":" + strResult + "   ";                     
                    }
                }

                daResult = null;
                cb = null;
                Task = null;
                dva = null;

                OnWriteResult(strWritResult, null, null);
                return true;
            }
            catch (Exception ex)
            {                
                outEx = ex;
                return false;
            }
        }

        /// <summary>写入解码图形
        /// 检查图形是否存在
        /// 检查条件：结果ID+图形名称
        /// <param name="TaskID"></param>
        /// <param name="ItemImgResults"></param>
        /// <param name="outEx"></param>
        /// <returns></returns>
        private bool addLocalResult_Img(string TaskID, List<ItemImgResult> ItemImgResults,out Exception outEx)
        {
            outEx = null;

            try
            {
                string strWritResult = "";
                int iItemCount = ItemImgResults.Count;

                string strSql = "select * from Lis_ComPort_Result_Img where FTaskID='" + TaskID + "'";

                OleDbDataAdapter daImgResult = new OleDbDataAdapter(strSql, this.conComDataBase);
                OleDbCommandBuilder cbImg = new OleDbCommandBuilder(daImgResult);
                DataSet dsImgTask = new DataSet();
                daImgResult.Fill(dsImgTask, "ImgDetail");
                DataView dvImg = new DataView(dsImgTask.Tables[0]);
                this.Close();

                // 循环图像结果LIST
                for (int i = 0; i < iItemCount; i++)
                {
                    string strFImgNmae = ItemImgResults[i].FImgNmae;
                    string strFImgType = ItemImgResults[i].FImgType;
                    int iFOrder = ItemImgResults[i].FOrder;
                    string strFRemark = ItemImgResults[i].FRemark;
                    byte[] btFImg = ItemImgResults[i].FImg;

                    // 仅在有图时处理
                    if (btFImg.Length > 0)
                    {
                        dvImg.RowFilter = "FImgNmae='" + strFImgNmae + "'";
                        DataRow TaskRow = null;

                        // 查找行是否存在                        
                        if (dvImg.Count > 0)
                        {
                            TaskRow = dvImg[0].Row;
                        }

                        // 行不存在则新增
                        if (TaskRow == null)
                        {
                            TaskRow = dsImgTask.Tables[0].Rows.Add();
                            TaskRow["FImgID"] = CommRuler.SysGetGUID();
                            TaskRow["FTaskID"] = TaskID;
                            TaskRow["FImgNmae"] = strFImgNmae;
                        }

                        TaskRow["FImgType"] = strFImgType;
                        TaskRow["FOrder"] = iFOrder;
                        TaskRow["FRemark"] = strFRemark;
                        TaskRow["FImg"] = btFImg;

                        if (TaskRow.RowState == DataRowState.Detached)
                        {
                            dsImgTask.Tables[0].Rows.Add(TaskRow);
                        }

                        strWritResult += "图像: " + strFImgNmae + " " + strFImgType + "   ";
                    }
                }

                OnWriteResult(strWritResult + '\r', null, null);

                daImgResult.Update(dsImgTask, "ImgDetail");

                daImgResult = null;
                cbImg = null;
                dsImgTask = null;
                dvImg = null;

                return true;
            }
            catch (Exception ex)
            {
                outEx = ex;
                return false;
            }
        }

        /// <summary>写空缓冲数据
        /// 不写读不出旧的
        /// </summary>
        /// <returns></returns>
        public int WriteNullBufferData()
        {
            try
            {
                string strsql = "insert into Lis_ComPort_Buffer(FResult) values('')";
                OleDbCommand cmdBuffer = new OleDbCommand(strsql, this.conComBuffer);
                int iDelCount = cmdBuffer.ExecuteNonQuery();
                this.CloseComBuffer();
                cmdBuffer = null;
                return iDelCount;
            }
            catch
            {
                //SetSSLStatus("删除缓冲数据失败");
                return -1;
            }
        }

        /// <summary>读取缓冲数据
        /// 每次都读出库里所有缓冲数据
        /// 返回:成功 - 缓冲数据(可能为空)
        /// 读缓冲失败 - 0
        /// 删除缓冲失败 - -1
        /// </summary>
        /// <returns></returns>
        public string ReaderBufferData()
        {
            string strBuffer = "";
            string strID = "";

            try
            {
                string strsql = "select FID,FDate,FResult from Lis_ComPort_Buffer order by FID";
                OleDbCommand cmdBuffer = new OleDbCommand(strsql, this.conComBuffer);
           

                OleDbDataReader daBuffer = cmdBuffer.ExecuteReader();

                while (daBuffer.Read())
                {
                    if (daBuffer.IsDBNull(2) != true)
                    {
                        try
                        {
                            strBuffer += daBuffer.GetString(2);
                        }
                        catch { }
                    }
                    strID += System.Convert.ToString(daBuffer.GetInt32(0)) + ",";
                }

                this.CloseComBuffer();

                if ((strBuffer != "") && (strID != ""))
                {
                    if (DeleteBufferData(strID) > 0)
                    {
                        return strBuffer;
                    }
                    else
                    {
                        return "-1";
                    }
                }

                cmdBuffer = null;
                daBuffer = null;

                return "";
            }
            catch
            {
                //SetSSLStatus("读取缓冲数据失败");
                //MessageBox.Show(ex.ToString());
                return "0";
            }
        }

        /// <summary>删除缓冲数据
        /// 读取缓冲数据后,立刻删除缓冲数据
        /// 删除已读的数据
        /// 返回:成功 - 删除行数
        ///      失败 - 0
        /// </summary>
        /// <param name="strID">待删除ID集(如2,3,4)</param>
        /// <returns></returns>
        private int DeleteBufferData(string strID)
        {
            try
            {
                string strsql = "delete from Lis_ComPort_Buffer where FID in (" + strID + ")";
                OleDbCommand cmdBuffer = new OleDbCommand(strsql, this.conComBuffer);
                int iDelCount = cmdBuffer.ExecuteNonQuery();
                this.CloseComBuffer();
                
                cmdBuffer = null;
                return iDelCount;
            }
            catch
            {
                //SetSSLStatus("删除缓冲数据失败");
                return 0;
            }
        }

        /// <summary>写解码结果,如果信息是:WriteLocalResultOver,则表示解码完成,订阅事件是远程数据同步调度
        /// </summary>
        protected virtual void OnWriteResult(String ItemInfo,Exception ex,object[] objSaveOLD)
        {
            if (WriteResult != null)
            {
                WriteResult(this, ItemInfo, ex, objSaveOLD);
            }
        }
    }
}
