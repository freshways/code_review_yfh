using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using DecodeInterface;
using GetIniSettValue;        
using System.IO;

namespace LIS.InstrDecode
{

    /// <summary> MI921DT
    /// </summary>
    public class MI921DT : DecodeBase
    {
        /// <summary>
        /// 入口：指定一段标本的开始位和结束位
        /// </summary>
        /// <param name="strComData"></param>
        public override void DataDecode(string strComData)
        {
            string[] strFGF = { "\n", "\r" };
            string[] strDataLines = strComData.Split(strFGF, StringSplitOptions.RemoveEmptyEntries);
            foreach (string ss in strDataLines)
            {
                //对于没有标记的数据，只能手动给添加上标记，如果不这样要修改的地方会很多
                string newstrComData = (char)0x02 + "\n" + ss + "\n" + (char)0x03;
                base.DataDecodeComm((char)0x02, (char)0x03, 0, newstrComData);
            }
        }

        /// <summary> 数据解码过程
        /// </summary>
        public override void InstrDataDecode(string strSampleData, out int Status, out  string strType, out  Exception ex, out string strSampleNo, out  string strDate, out List<ItemResult> ItemResults, out List<ItemImgResult> ItemImgResults, out float Heat, out float XYDepth, out float Haemoglobin, out string strOtherSampleData)
        {
            strType = "1401";
            strSampleNo = "";
            strDate = "";
            Status = 0;
            ItemResults = new List<ItemResult>();                       //解码项目及结果
            ItemImgResults = new List<ItemImgResult>();                 //图形结果
            Heat = 0;
            XYDepth = 0;
            Haemoglobin = 0;
            strOtherSampleData = "";

            try
            {
                //这个型号的设备没有发送检验日期，并且只保留当天的数据，所以我把日期去掉了时分秒
                strDate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");      //默认标本日期为当前时间(在后面的代码中再换成真实的)
                strDate = strDate.Substring(0, strDate.Length - 2) + "00";  //去掉秒

                string[] strFGF = { "\n", "\r" };
                string[] strWhiteSpace = { " " };
                string[] strDataLines = strSampleData.Split(strFGF, StringSplitOptions.RemoveEmptyEntries);

                string[] Items = { "K", "Na", "Cl", "iCa", "pH", "CO2" };
                foreach (string rowstring in strDataLines)
                {
                    if (rowstring.Length < 60) continue;

                    string[] strNOandDate = rowstring.Split(strWhiteSpace, StringSplitOptions.RemoveEmptyEntries);

                    strSampleNo = strNOandDate[0];  //解析标本号
                    strSampleNo = Convert.ToInt64(strSampleNo).ToString();
                    //循环赋值
                    for (int i = 0; i < Items.Length; i++)
                    {
                        ItemResult newItemResult = new ItemResult();
                        newItemResult.ItemID = Items[i];                //解析项目代号
                        newItemResult.ItemValue = strNOandDate[i + 3];       //解析结果
                        ItemResults.Add(newItemResult);
                    }
                }

                Status = 1;
                ex = null;
            }
            catch (Exception e)
            {
                Status = -1;
                ex = e;
            }
        }
    }
}