﻿using AFTNet.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AFTNet.Database
{
   public class AftDataContext
    {
       
        private static string lisConnectionString = "";

        static AftDataContext()
        {
             
            lisConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["LisConnectionString"].ConnectionString;
            //lisConnectionString = Tools.Encode(lisConnectionString);
            lisConnectionString = Tools.Decode(lisConnectionString);
        }
        public static LisDatabase GetLisDatabase ()       {
             
            
                return new LisDatabase(lisConnectionString);
            
        }
    }
}
