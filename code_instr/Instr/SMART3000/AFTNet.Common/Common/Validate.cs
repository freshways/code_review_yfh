using System;

namespace AFTNet.Common
{
    public sealed partial class Tools
    {
        /// <summary>
        /// 是否匹配
        /// </summary>
        /// <param name="input">字符串</param>
        /// <param name="pattern">正则表达式</param>
        /// <returns></returns>
        public static bool IsMatch(string input, string pattern)
        {
            //if (string.IsNullOrEmpty(input))
            //{
            //    return false;
            //}
            return System.Text.RegularExpressions.Regex.IsMatch(input, pattern);
        }

        #region math

        /// <summary>
        /// 是否全数字
        /// </summary>
        public static bool IsNumber(string input)
        {
            return IsMatch(input, @"^\d+$");
        }

        /// <summary>
        /// 是否为整数
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsINT(string input)
        {
            return IsMatch(input, @"^([+-]?)\d+$");
        }

        /// <summary>
        /// 是否为浮点数
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsFloat(string input)
        {
            return IsMatch(input, @"^([+-]?)\d*\.\d+$");
        }

        /// <summary>
        /// 判断是否为整数
        /// </summary>
        /// <param name="sString"></param>
        /// <returns>true or false</returns>
        public static bool IsNumeric(string str)
        {
            try
            {
                Convert.ToInt32(str);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region DateTime

        /// <summary>
        /// 是否为日期格式
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsDateTime(string input)
        {
            try
            {
                DateTime.Parse(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 是否为日期格式
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsDate(string input)
        {
            return IsMatch(input, @"^\d{2,4}[\/\-]?((((0?[13578])|(1[02]))[\/|\-]?((0?[1-9]|[0-2][0-9])|(3[01])))|(((0?[469])|(11))[\/|\-]?((0?[1-9]|[0-2][0-9])|(30)))|(0?[2][\/\-]?(0?[1-9]|[0-2][0-9])))$");
        }

        /// <summary>
        /// 是否为时间格式
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsTime(string input)
        {
            return IsMatch(input, @"^(20|21|22|23|[01]\d|\d)(([:.][0-5]\d){1,2})$");
        }

        #endregion

        #region Web

        /// <summary>
        /// 是否为URL
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsURL(string input)
        {
            return IsMatch(input, @"^http[s]?:\/\/([\w-]+\.)+[\w-]+([\w-./?%&=]*)?$");
        }

        /// <summary>
        /// 是否为IPv4格式
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsIP(string input)
        {
			return IsMatch(input, @"^((0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.){3}(0?\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$");
        }

        /// <summary>
        /// 是否为邮箱
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsEmail(string input)
        {
            return IsMatch(input, @"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$");
        }

        #endregion

        #region Info

        /// <summary>
        /// 是否为身份证
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsIdentity(string input)
        {
            return IsMatch(input, @"^\d{15}(\d{2}[0-9X])?$");
        }

		/// <summary>
		/// 是否为18位身份证
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public static bool IsIdentity18(string input)
		{
			if (IsMatch(input, @"^[1-6][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$"))
			{
				int iS = 0;

				//加权因子常数 
				int[] iW = new int[] { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
				//校验码常数 
				string LastCode = "10X98765432";

				//进行加权求和 
				for (int i = 0; i < 17; i++)
				{
					iS += int.Parse(input.Substring(i, 1)) * iW[i];
				}

				//取模运算，得到模值 
				int iY = iS % 11;
				//从LastCode中取得以模为索引号的值，加到身份证的最后一位，即为新身份证号。 
				return input.Substring(17, 1).Equals(LastCode.Substring(iY, 1), StringComparison.OrdinalIgnoreCase);
			}

			return false;
		}

        /// <summary>
        /// 是否为手机号
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsMoblie(string input)
        {
            return IsMatch(input, @"^0{0,1}13\d{9}|15[389]\d{8}$");
        }

        #endregion

        #region Char

        /// <summary>
        /// 是否为中文
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsChinese(string input)
        {
            return IsMatch(input, @"^[\u4E00-\u9FA5\uF900-\uFA2D]+$");
        }

        /// <summary>
        /// 是否为字母
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsEnglish(string input)
        {
            return IsMatch(input, @"^[A-Za-z]+$");
        }

        /// <summary>
        /// 是否为数字、英文、下线
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNumEn(string input)
        {
            return IsMatch(input, @"[\\d_a-zA-Z]+");
        }

        /// <summary>
        /// 是否为数字、英文、下线、中文字符
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNumEnCn(string input)
        {
            return IsMatch(input, @"^[\\d_a-zA-Z\\u4E00-\\u9FA5\\uF900-\\uFA2D]+$");
        }

        #endregion

        #region File

        /// <summary>
        /// 是否为图片
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsPicture(string input)
        {
            return IsMatch(input, @"(.*)\.(jpg|bmp|gif|ico|pcx|jpeg|tif|png|raw|tga)$");
        }

        /// <summary>
        /// 是否为压缩文件
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsZip(string input)
        {
            return IsMatch(input, @"(.*)\.(rar|zip|7zip|tgz)$");
        }

        #endregion
    }
}
