﻿using AFTNet.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace AFTNet.Cofing
{
    public class Config
    {
        private static int socketPort = 0;

        public static int SocketPort
        {
            get
            {
                if (socketPort == 0)
                {
                    Tools.TryGetAppSetting("SocketPort", ref socketPort, 1001);

                }
                return socketPort;
            }
            set { socketPort = value; }
        }

        private static int connectionsNum = 0;
        /// <summary>
        /// 最多同时连接的socket数量
        /// </summary>
        public static int ConnectionsNum
        {
            get
            {
                if (connectionsNum == 0)
                {
                    Tools.TryGetAppSetting("ConnectionsNum", ref connectionsNum, 20);

                }
                return connectionsNum;
            }
            set { connectionsNum = value; }
        }

        private static int oneBufferSize = 0;
        /// <summary>
        /// socket连接缓存大小
        /// </summary>
        public static int OneBufferSize
        {
            get
            {
                if (oneBufferSize == 0)
                {
                    Tools.TryGetAppSetting("OneBufferSize", ref oneBufferSize, 1024 * 1024);

                }
                return oneBufferSize;
            }
            set { oneBufferSize = value; }
        }

        private static string deviceName = "";
        /// <summary>
        /// 设备名称
        /// </summary>
        public static string DeviceName
        {
            get
            {
                if (string.IsNullOrEmpty(deviceName))
                {
                    Tools.TryGetAppSetting("DeviceName", ref deviceName, "SMART3000");

                }
                return deviceName;
            }
            set { deviceName = value; }
        }


        private static string portName = "";

        public static string PortName
        {
            get

            {
                if (string.IsNullOrEmpty(portName))
                {
                    Tools.TryGetAppSetting("PortName", ref portName, "COM9");
                    portName = portName.ToLower();
                }
                return portName;
            }
        }

        private static int baudRate = 0;
        public static int BaudRate
        {
            get
            {

                if (baudRate == 0)
                {
                    Tools.TryGetAppSetting("BaudRate", ref baudRate, 19200);

                }

                return baudRate;
            }
        }
        private static Parity parity = System.IO.Ports.Parity.None;
        private static string stPparity = "";
        public static System.IO.Ports.Parity Parity
        {
            get
            {
                if (string.IsNullOrEmpty(stPparity))
                {
                    Tools.TryGetAppSetting("Parity", ref stPparity, "None");
                }
                parity = (System.IO.Ports.Parity)Enum.Parse(typeof(System.IO.Ports.Parity), stPparity, true);
                return parity;
            }
        }
        private static int dataBits = 0;
        public static int DataBits
        {
            get
            {
                if (dataBits == 0)
                {
                    Tools.TryGetAppSetting("DataBits", ref dataBits, 8);
                }

                return dataBits;
            }
        }
        private static StopBits stopBits = System.IO.Ports.StopBits.None;
        private static int iStopBits = 0;
        public static StopBits StopBits
        {
            get
            {
                if (iStopBits == 0)
                {
                    Tools.TryGetAppSetting("StopBits", ref iStopBits, 1);
                    stopBits = (System.IO.Ports.StopBits)iStopBits;
                }

                return stopBits;
            }
        }

        private static string sHandshake = "";
        private static System.IO.Ports.Handshake handshake = System.IO.Ports.Handshake.None;
        public static System.IO.Ports.Handshake Handshake
        {
            get
            {
                if (string.IsNullOrEmpty(sHandshake))
                {
                    Tools.TryGetAppSetting("Handshake", ref sHandshake, "None");
                }

                handshake = (Handshake)Enum.Parse(typeof(Handshake), sHandshake, true);
                return handshake;
            }
        }
    }
}
