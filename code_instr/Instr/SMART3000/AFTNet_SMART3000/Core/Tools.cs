﻿using AFTNet.Common;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AFTNet.Core
{
    public class ParseTool
    {

        public string ParseBytes2Message(byte[] datas, out byte[] remainBytes, out int orderNum)
        {
            remainBytes = new byte[0];
            orderNum = 0;
            if (datas == null || datas.Length <= 0)
            {
                return "";
            }
            byte[] sb = new byte[1];
            sb[0] = 0x0b;
            byte[] eb = new byte[2];
            eb[0] = 0x0d;
            eb[1] = 0x1c;
            int startPosition = Tools.ByteIndexOf(datas, sb, 0);
            if (startPosition < 0)
            {
                return "";
            }
            int endPosition = Tools.ByteIndexOf(datas, eb, startPosition);
            string strMsg = "";
            if (endPosition > startPosition)
            {

                int length = endPosition - startPosition;
                byte[] message = new byte[length];
                Array.Copy(datas, startPosition, message, 0, length);
                strMsg = System.Text.Encoding.UTF8.GetString(message);

                if (datas.Length > endPosition + 2)
                {
                    remainBytes = new byte[datas.Length - endPosition - 2];
                    Array.Copy(datas, endPosition + 2, remainBytes, 0, datas.Length - endPosition - 2);
                }
            }
            else
            {
                int length = datas.Length - startPosition;
                remainBytes = new byte[length];
                Array.Copy(datas, startPosition, remainBytes, 0, length);
                remainBytes = datas;
            }
            return strMsg;
        }

        public virtual byte[] ParseMessageToBytes(string message)
        {
            byte[] msg = System.Text.Encoding.UTF8.GetBytes(message);
            byte[] messages = new byte[msg.Length + 3];
            messages[0] = 0x0B;
            Array.Copy(msg, 0, messages, 1, msg.Length);
            messages[msg.Length + 1] = 0x1c;
            messages[msg.Length + 2] = 0x0d;
            return messages;
        }
    }
}
