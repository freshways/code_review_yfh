﻿
//using AFTNet.Database;
//using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;

namespace AFTNet
{
    public partial class Host : ServiceBase
    {
        public Host()
        {
            InitializeComponent();
        }
        //private ILog log;
        protected override void OnStart(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            SMART3000 instance =new SMART3000();
            instance.Start();
        }

        protected override void OnStop()
        {
            //ServiceHelper.StopControllerService();
        }
      

        public static void StartClientDataService()
        {
           
        }

        

        public void HostServiceStart()
        {
            this.OnStart(null);
        }

        public void HostServiceStop()
        {
            this.OnStop();
        }
    }
}
