﻿using AFTNet.Cofing;
using AFTNet.Common;
using AFTNet.Core;
using AFTNet.Database;
using AFTNet.HL7.Definition;
using AFTNet.HL7.Message;
using AFTNet.HL7_Basec.Message;
using AFTNet.HL7_Basec.Segment;
using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;

namespace AFTNet
{
    public class SMART3000
    {
        public SMART3000()
        {

        }
        private Timer reCTimer = null;
        private BlockingCollection<byte[]> caches = null;
        private byte[] buffer;
        private SerialPort serialPort;
        private ParseTool parseTool;
        private Lis_Ins_Result result = null;
        private List<Lis_Ins_Result_Detail> details = null;
        private List<Lis_Ins_Result_Img> images = null;

        string Mes = @"MSH|^~\&|KEYSMILE|SMART3000|40961||20190118195056||ORU^R01|15|P|2,3,1||||1||ASCII|||PID|1||||||0||||||||||||||||||||||||OBR|1|3|||KEYSMILE^SMART3000|||20190118130425|||||||||||||||||||||||||||||||||||||||||OBX|1||26|HBsAb|697254|mIU/ml|0|10|||||324.46|20190118130425||||";
        public void Start()
        {
            reCTimer = new Timer(Init, null, 0, 60000);
            //ProcessMessage(Mes.Replace("\v",""));
        }

        private int synInitObject = 0;
        private void Init(Object stateInfo)
        {
            if (0 == Interlocked.Exchange(ref synInitObject, 1))
            {
                try
                {
                    Thread readThread = new Thread(Process);
                    serialPort = new SerialPort();
                    serialPort.PortName = Config.PortName;
                    serialPort.BaudRate = Config.BaudRate;
                    serialPort.Parity = Config.Parity;
                    serialPort.DataBits = Config.DataBits;
                    serialPort.StopBits = Config.StopBits;
                    serialPort.Handshake = Config.Handshake;
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
                    serialPort.ReadTimeout = 500;
                    serialPort.WriteTimeout = 500;
                    ConcurrentQueue<byte[]> _caches = new ConcurrentQueue<byte[]>();
                    caches = new BlockingCollection<byte[]>(_caches);
                    buffer = new byte[0];
                    parseTool = new ParseTool();
                    serialPort.Open();
                    readThread.Start();
                    reCTimer.Dispose();
                }
                catch(Exception e)
                {
                    log.Warn(e.Message);
                }
                finally
                {
                    Interlocked.Exchange(ref synInitObject, 0);
                }
            }
        }
     

        byte[] ack = { 0x06 };
        private static ILog log = log4net.LogManager.GetLogger("lisnetlog");

        private int orderNum = 0;

        //private  
        public void Process()
        {
            while (true)
            {
                try
                {
                    byte[] rcvBytes = caches.Take();
                    if (rcvBytes.Length == 1)
                    {
                        if ((int)rcvBytes[0] == 0x05)
                        {
                            serialPort.Write(ack, 0, 1);
                            continue;
                        }

                        if ((int)rcvBytes[0] == 0x04)
                        {
                            orderNum = 0;
                            continue;
                        }
                    }

                    int i = buffer.Length + rcvBytes.Length;
                    byte[] newBuffer = new byte[i];
                    Buffer.BlockCopy(buffer, 0, newBuffer, 0, buffer.Length);
                    Buffer.BlockCopy(rcvBytes, 0, newBuffer, buffer.Length, rcvBytes.Length);

                    byte[] remainBytes;
                    int _orderNum;
                    String message = parseTool.ParseBytes2Message(newBuffer, out remainBytes, out _orderNum);
                    buffer = remainBytes;
                    if (!string.IsNullOrEmpty(message))
                    {
                        log.Info(message);
                        ProcessMessage(message.Replace("\v", ""));
                    }

                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                }
            }
        }


        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {

            int n = serialPort.BytesToRead;
            byte[] newBuffer = new byte[n];
            //if((n+offset) >(buffer.Length))
            //{
            //    int i = (n + offset) + Config.OneBufferSize;
            //    byte[] newBuffer = new byte[i];
            //    Buffer.BlockCopy(buffer, 0, newBuffer, 0, offset);
            //    buffer = newBuffer;
            //}
            serialPort.Read(newBuffer, 0, n);
            caches.Add(newBuffer);
            //offset = offset + n;
        }

        public void ProcessMessage(string msg)
        {

            try
            {
                log.Info("SMART3000---->LIS:  " + msg);
                if (!string.IsNullOrEmpty(msg))
                {
                    MessageBase message = new MessageBase(msg);
                    MSH msh = new MSH(message.MSH);
                    if (msh.MessageType != MessageType.ORU_R01)
                    {
                        return;
                    }

                    ORU_R01 oru_r01 = new ORU_R01(msg);

                    PID pid = new PID(oru_r01.PID, msh);
                    OBR obr = new OBR(oru_r01.OBR, msh);

                    if (oru_r01.OBX.Length <= 0)
                    {
                        return;
                    }

                    LisDatabase db = AftDataContext.GetLisDatabase();
                    DateTime createTime = DateTime.Now;
                    DateTime dateTimeOfTheObservation = DateTime.Now;
                    DateTime dateTime = DateTime.Now;
                    OBX obx1 = new OBX(oru_r01.OBX[0], msh);
                    //DateTime dateTimeOfTheObservation = Tools.GetDateTime(obx1.DateTimeOfTheObservation, Tools.dtOption.Day);
                    dateTimeOfTheObservation = Tools.GetDateTime(obr.ObservationDateTime, Tools.dtOption.Second);
                    dateTime = dateTimeOfTheObservation.Date;

                    Lis_Ins_Result result = (from g in db.Lis_Ins_Result where g.FInstrID == Cofing.Config.DeviceName && g.FDateTime.Value.Date == dateTime && g.FResultID == obr.PlastringrOrderNumber select g).FirstOrDefault();

                    if (result == null)
                    {
                        result = new Lis_Ins_Result();
                        result.FTaskID = Guid.NewGuid().ToString().Replace("-", "");
                        result.FInstrID = Cofing.Config.DeviceName;
                        result.FResultID = obr.PlastringrOrderNumber;
                        result.FDateTime = dateTime;
                        result.FCreateTime = createTime;
                        db.Lis_Ins_Result.InsertOnSubmit(result);
                    }
                    else
                    {
                        result.FUpdateTime = createTime;

                    }
                    List<Lis_Ins_Result_Detail> details = (from g in db.Lis_Ins_Result_Detail where g.FTaskID == result.FTaskID select g).ToList();
                    List<Lis_Ins_Result_Img> images = (from g in db.Lis_Ins_Result_Img where g.FTaskID == result.FTaskID select g).ToList();
                    if (images != null && images.Count > 0)
                    {
                        db.Lis_Ins_Result_Img.DeleteAllOnSubmit(images);
                    }
                    for (int i = 0; i < oru_r01.OBX.Length; i++)
                    {

                        string strObx = oru_r01.OBX[i];
                        OBX obx = new OBX(strObx, msh);

                        if (obx.ObservationIdentifier.Split(msh.ComponentSeparator)[0].StartsWith("J2000"))
                        {
                            continue;
                        }

                        if (obx.ObservationIdentifier.Split(msh.ComponentSeparator)[0].StartsWith("J1000"))
                        {
                            Lis_Ins_Result_Img img = new Lis_Ins_Result_Img();
                            img.FImgID = Guid.NewGuid().ToString().Replace("-", "");
                            img.FTaskID = result.FTaskID;
                            img.FImgNmae = obx.ObservationIdentifier.Split(msh.ComponentSeparator)[0];
                            img.FImgType = obx.ObservationIdentifier.Split(msh.ComponentSeparator)[1];
                            img.FImg = new System.Data.Linq.Binary(System.Text.Encoding.UTF8.GetBytes(obx.ObservationValue));
                            db.Lis_Ins_Result_Img.InsertOnSubmit(img);
                        }
                        else
                        {
                            Lis_Ins_Result_Detail detail = (from g in details where g.FItem == obx.ObservationIdentifier.Substring(obx.ObservationIdentifier.IndexOf("^") + 1) select g).FirstOrDefault();
                            if (detail == null)
                            {
                                detail = new Lis_Ins_Result_Detail();
                                detail.FID = Guid.NewGuid().ToString().Replace("-", "");
                                detail.FTaskID = result.FTaskID;
                                detail.FItem = obx.ObservationSubId;//根据截取的实际数据进行调整
                                detail.FValue = obx.UserDefinedAccessChecks;
                                detail.FTime = DateTime.Now.Date;
                                detail.FCreateTime = createTime;
                                db.Lis_Ins_Result_Detail.InsertOnSubmit(detail);
                            }
                            else
                            {
                                detail.FValue = obx.UserDefinedAccessChecks;
                            }
                        }
                    }

                    db.SubmitChanges();

                    ACK_R01 arcMessage = new ACK_R01();
                    MSH arcMsh = new MSH(msh.FieldSeparator);

                    arcMsh.FieldSeparator = msh.FieldSeparator;
                    arcMsh.SpecialCharacters = msh.SpecialCharacters;
                    arcMsh.ReceivingApplication = msh.SendingApplication;
                    arcMsh.TimeOfMessage = msh.TimeOfMessage;
                    arcMsh.MessageType = MessageType.ACK_R01;
                    arcMsh.MessageControlID = msh.MessageControlID;
                    //arcMsh.ProcessingID = msh.ProcessingID;
                    arcMsh.VersionID = msh.VersionID;
                    arcMsh.CharacterSet = msh.CharacterSet;

                    MSA arcMsa = new MSA(msh);
                    arcMsa.AcknowledgementCode = "AA";
                    arcMsa.MessageControlID = msh.MessageControlID;
                    arcMsa.TextMessage = "Message Accepted";
                    arcMsa.ErrorCondition = 0;

                    arcMessage.MSH = arcMsh.ToString();
                    arcMessage.MSA = arcMsa.ToString();
                    string strMessage = arcMessage.ToString();
                    log.Info("LIS---->SMART3000:  " + strMessage);
                    //base.SendPacket(token, Paser.ParseMessageToBytes(strMessage));

                    serialPort.Write(parseTool.ParseMessageToBytes(strMessage), 0, 1);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message);
            }
            finally
            {

            }

        }

    }
}
