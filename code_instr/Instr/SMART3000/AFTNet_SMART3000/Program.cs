﻿
using AFTNet.Database;
 
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace AFTNet
{

    static class Program
    {
        static ILog logCar = log4net.LogManager.GetLogger("AFTNetcar");
        static void Main(string[] args)
        {
            //快捷启动，安装服务和卸载服务
            if (args.Length > 0 && (args[0].ToLower() == "-install" || args[0].ToLower() == "-i"))
            {
                System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { "AFTNet.exe" });

                ServiceController c = new ServiceController("AFTNet_SMART3000");
                c.Start();
            }
            else if (args.Length > 0 && (args[0].ToLower() == "-uninstall" || args[0].ToLower() == "-u"))
            {
                System.Configuration.Install.ManagedInstallerClass.InstallHelper(new string[] { "/u", "AFTNet.exe" });
            }
            else
            {
                //MSH msh = new MSH();
                //string a = msh.ToString();
                //string[] b = a.Split("|".ToCharArray());
                if (Environment.UserInteractive)
                {
                    try
                    {
                        AftDataContext.GetLisDatabase();
                        Host s = new Host();
                        s.HostServiceStart();
                        Console.WriteLine("the service is started");

                        Console.ReadLine();

                        s.HostServiceStop();
                        Console.WriteLine("the service is stopped");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    Console.ReadLine();
                }
                else
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[] 
                 { 
                  new Host() 
                 };
                    ServiceBase.Run(ServicesToRun);
                }
            }
        }
    }
}
