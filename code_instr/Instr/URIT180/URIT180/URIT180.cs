using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using DecodeInterface;
using GetIniSettValue;        
using System.IO;

namespace LIS.InstrDecode
{

    /// <summary> URIT180
    /// </summary>
    public class URIT180 : DecodeBase
    {
        /// <summary>
        /// 入口：指定一段标本的开始位和结束位
        /// </summary>
        /// <param name="strComData"></param>
        public override void DataDecode(string strComData)
        {
            base.DataDecodeComm((char)0x02, (char)0x03, 0, strComData);
        }

        /// <summary> 数据解码过程
        /// </summary>
        public override void InstrDataDecode(string strSampleData, out int Status, out  string strType, out  Exception ex, out string strSampleNo, out  string strDate, out List<ItemResult> ItemResults, out List<ItemImgResult> ItemImgResults, out float Heat, out float XYDepth, out float Haemoglobin, out string strOtherSampleData)
        {
            strType = "1401";
            strSampleNo = "";
            strDate = "";
            Status = 0;
            ItemResults = new List<ItemResult>();                       //解码项目及结果
            ItemImgResults = new List<ItemImgResult>();                 //图形结果
            Heat = 0;
            XYDepth = 0;
            Haemoglobin = 0;
            strOtherSampleData = "";

            try
            {
                strDate = System.DateTime.Now.ToString();                     //默认标本日期为当前时间(在后面的代码中再换成真实的)
                //strDate = strDate.Substring(0, strDate.Length - 2) + "00";  //去掉秒
                //strSampleData = strSampleData.Replace("\r\n", "");          //将原始数据中换行符去掉

                string[] strFGF = { "\n","\r"};
                string[] strWhiteSpace = {" "};
                string[] strDataLines = strSampleData.Split(strFGF, StringSplitOptions.RemoveEmptyEntries);
                
                for (int index = 1; index < strDataLines.Length; index++ ) //第一行无法处理直接跳过
                {
                    if (strDataLines[index].Length < 2)
                    { }
                    else
                    {
                        if(strDataLines[index].Contains("NO.")) //判断样本号和时间
                        {
                            string[] strNOandDate = strDataLines[index].Split(strWhiteSpace, StringSplitOptions.RemoveEmptyEntries);
                            int lastloc = strNOandDate[0].LastIndexOf('.');
                            strSampleNo = strNOandDate[0].Substring(lastloc+1);
                            strSampleNo = Convert.ToInt64(strSampleNo).ToString();

                            strDate = strNOandDate[1]; //日期
                        }
                        else if (strDataLines[index].Contains(":")) //判断时分秒
                        {
                            string[] strTime = strDataLines[index].Split(strWhiteSpace, StringSplitOptions.RemoveEmptyEntries);
                            strDate += " " + strTime[0];
                            strDate = strDate.Substring(0, strDate.Length - 2) + "00";  //去掉秒
                        }
                        else
                        {
                            string[] strItemDetail = strDataLines[index].Split(strWhiteSpace, StringSplitOptions.RemoveEmptyEntries);
                            if (strItemDetail.Length >= 2)
                            {
                                //获取个项目的值
                                ItemResult newItemResult = new ItemResult();
                                newItemResult.ItemID = strItemDetail[0].Replace("*","");
                                newItemResult.ItemValue = strItemDetail[1];
                                ItemResults.Add(newItemResult);
                            }
                            else if (strItemDetail.Length == 1)
                            {
                                //获取个项目的值
                                ItemResult newItemResult = new ItemResult();
                                newItemResult.ItemID = strItemDetail[0];
                                newItemResult.ItemValue = "-";
                                ItemResults.Add(newItemResult);
                            }
                        }
                    }
                }


                Status = 1;
                ex = null;
            }
            catch (Exception e)
            {
                Status = -1;
                ex = e;
            }
        }
    }
}
