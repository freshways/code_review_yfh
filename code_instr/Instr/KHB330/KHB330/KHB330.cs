using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using DecodeInterface;
using GetIniSettValue;        
using System.IO;

namespace LIS.InstrDecode
{

    /// <summary> KHB330
    /// </summary>
    public class KHB330 : DecodeBase
    {
        /// <summary>
        /// 入口：指定一段标本的开始位和结束位
        /// </summary>
        /// <param name="strComData"></param>
        public override void DataDecode(string strComData)
        {
            base.DataDecodeComm((char)0x05, (char)0x04, 0, strComData);
        }

        /// <summary> 数据解码过程
        /// </summary>
        public override void InstrDataDecode(string strSampleData, out int Status, out  string strType, out  Exception ex, out string strSampleNo, out  string strDate, out List<ItemResult> ItemResults, out List<ItemImgResult> ItemImgResults, out float Heat, out float XYDepth, out float Haemoglobin, out string strOtherSampleData)
        {
            strType = "1401";
            strSampleNo = "";
            strDate = "";
            Status = 0;
            ItemResults = new List<ItemResult>();                       //解码项目及结果
            ItemImgResults = new List<ItemImgResult>();                 //图形结果
            Heat = 0;
            XYDepth = 0;
            Haemoglobin = 0;
            strOtherSampleData = "";

            try
            {
                strDate = System.DateTime.Now.ToString();                     //默认标本日期为当前时间(在后面的代码中再换成真实的)
                //strDate = strDate.Substring(0, strDate.Length - 2) + "00";  //去掉秒

                string[] strFGF = { "\n","\r"};
                string[] strWhiteSpace = {"|"};
                string[] strDataLines = strSampleData.Split(strFGF, StringSplitOptions.RemoveEmptyEntries);
                if (!strDataLines[0].Contains(@"1H|\^&"))
                {
                    ex =  new Exception("数据包不完整！");
                    return;
                }
                for (int index = 0; index < strDataLines.Length; index++ ) //第一行无法处理直接跳过
                {
                    if (strDataLines[index].Length < 10)
                    { }
                    else
                    {
                        string[] strNOandDate = strDataLines[index].Split(strWhiteSpace, StringSplitOptions.RemoveEmptyEntries);
                        if (strNOandDate[0].Contains("H")) //时间
                        {
                            strDate = DateTime.ParseExact(strNOandDate[5], "yyyyMMddHHmmss", null).ToString("yyyy-MM-dd HH:mm"); //日期
                        }
                        else if (strNOandDate[0].Contains("O")) //判断样本号
                        {
                            strSampleNo = strNOandDate[5];
                            strSampleNo = Convert.ToInt64(strSampleNo).ToString();
                        }
                        else if (strNOandDate[0].Contains("R")) //项目信息
                        {
                            //获取个项目的值
                            ItemResult newItemResult = new ItemResult();
                            newItemResult.ItemID = strNOandDate[2];
                            newItemResult.ItemValue = strNOandDate[3];
                            ItemResults.Add(newItemResult);
                        }
                    }
                }

                Status = 1;
                ex = null;
            }
            catch (Exception e)
            {
                Status = -1;
                ex = e;
            }
        }
    }
}
