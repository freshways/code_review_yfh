/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [fitem_id]
      ,[fitem_code]
      ,[finstr_id]
      ,[fcheck_type_id]
      ,[fsam_type_id]
      ,[fitem_type_id]
      ,[fcheck_method_id]
      ,[fname]
      ,[fname_j]
      ,[fhelp_code]
      ,[fhis_item_code]
      ,[fxh_code]
      ,[funit_name]
      ,[finstr_unit]
      ,[fuse_if]
      ,[fxs]
      ,[fqc_code]
      ,[fqc_check_type]
      ,[fvalue_type_id]
      ,[fround]
      ,[ftime]
      ,[fsample_type_id]
      ,[fuser_id]
      ,[fname_e]
      ,[fzhb_code]
      ,[freport_code]
      ,[fref_high]
      ,[fref_low]
      ,[fref]
      ,[fref_if_age]
      ,[fref_if_sex]
      ,[fref_if_sample]
      ,[fref_if_method]
      ,[fprint_type_id]
      ,[fprint_num]
      ,[fprint_num_group]
      ,[fvalue_remark]
      ,[fvalue_dd]
      ,[fjx_if]
      ,[fjx_formula]
      ,[fvalue_sx]
      ,[fvalue_xx]
      ,[fjg_value_sx]
      ,[fjg_value_xx]
      ,[fyy]
      ,[fvalue_day_num]
      ,[fvalue_day_no]
      ,[fcount]
      ,[fprice]
      ,[fprice_dd]
      ,[fjz_if]
      ,[fjz_no_if]
      ,[fjz_tat]
      ,[fjz_no_tat]
      ,[fzk_if]
      ,[fgroup_id]
      ,[fcheck_calendar]
      ,[funsure]
      ,[fyx_ljv]
      ,[fyx_pdf]
      ,[fyx_formula]
      ,[fperiod]
      ,[freagent_id]
      ,[freagent_num]
      ,[freagent_unit_id]
      ,[fhz_prepare]
      ,[fremark]
      ,[forder_by]
  FROM [LIS2219].[dbo].[SAM_ITEM]

  insert into [LIS2219].[dbo].[SAM_ITEM]
  select 
	 replace(newid(),'-','') --fitem_id
	,TestCode --fitem_code
	,'KHB450' --FINSTR_ID
	,'01' --fcheck_type_id
	,'' --fsam_type_id
	,'4' --fitem_type_id
	,'1' --fcheck_method_id
	,TestName --fname
	,'' --fname_j
	,'' --fhelp_code
	,'' fhis_item_code
	,''fxh_code
	,TestUnit --funit_name
	,'' finstr_unit
	,1 fuse_if
	,1 fxs
	,'' fqc_code
	,'' fqc_check_type
	,'' fvalue_type_id
	,'0' fround
	,''	ftime
	,''fsample_type_id
	,'' fuser_id
	,TestCode fname_e
	,'' fzhb_code
	,'' freport_code	
	,isnull(DefaultHigh,0) --fref_high
	,isnull(defaultlow,0) fref_low
	,case when (DefaultHigh is null or defaultlow is null) then '' else cast(defaultlow as varchar(40))+'--'+cast(DefaultHigh as varchar(40)) end --fref
	,'0' fref_if_age	
	,'0' fref_if_sex	
	,'0' fref_if_sample	
	,'0' fref_if_method
	,'1' fprint_type_id
	,ReportOrder --fprint_num
	,'0' fprint_num_group
	,'' fvalue_remark
	,'1' fvalue_dd
	,'0' fjx_if
	,'' fjx_formula
	,'0' fvalue_sx
	,'0' fvalue_xx	
	,'0' fjg_value_sx	
	,'0' fjg_value_xx
	,'' fyy
	,'0' fvalue_day_num
	,'0' fvalue_day_no
	,'0' fcount
	,'0'fprice	
	,'0'fprice_dd	
	,'0'fjz_if
	,'1' fjz_no_if
	,'1' fjz_tat	
	,'1' fjz_no_tat	
	,'1' fzk_if
	,'' fgroup_id	
	,'' fcheck_calendar	
	,'' funsure	
	,'' fyx_ljv	
	,'' fyx_pdf	
	,'' fyx_formula	
	,'' fperiod	
	,'' freagent_id
    ,'0' freagent_num	
	,'' freagent_unit_id	
	,'' fhz_prepare	
	,'' fremark	
	,'' forder_by

  FROM [LabConsole400].[dbo].[labTestItem]
  where TestCode<>'Detergent'