﻿using AFTNet.Cofing;
using AFTNet.Database;
using AFTNet.Poll;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace AFTNet
{
    public class DH604 : DatePollRuntime
    {
        private string m_DH604BeginNo = "1";
        private string m_DH604EndNo = "1";
        private string m_date = "2020-04-01";
        public DH604(System.ComponentModel.BackgroundWorker worker, DateTime date, int beginNo, int endNo):base(worker)
        {
            DeviceName = Config.DeviceName;
            m_date = date.ToString("yyyy-MM-dd");
            m_DH604BeginNo = beginNo.ToString();
            m_DH604EndNo = endNo.ToString();
        }
        
        protected override void DataPollProcess()
        {
            DataTable dt = new DataTable();

            string strsql = @" select aa.SampleID,convert(varchar(20),aa.RequestTime,23) RequestTime,aa.SampleNo,bb.Result,cc.TestCode,cc.TestName
 from dbo.labSample aa
	left outer join dbo.labResult bb on aa.SampleID=bb.SampleID
	left outer join dbo.labTestItem cc on bb.ReagentID=cc.ReagentID
 where convert(varchar(30),aa.RequestTime,23)=@date and cast(aa.SampleNo as int) >=@begin and cast(aa.SampleNo as int) <=@end
 order by aa.SampleID
";
            using (SqlConnection oleConnection = new SqlConnection(Cofing.Config.DeviceDB))
            {
                SqlCommand sqlcmd = new SqlCommand(strsql, oleConnection);
                sqlcmd.Parameters.Add(new SqlParameter("@date", m_date));
                sqlcmd.Parameters.Add(new SqlParameter("@begin", m_DH604BeginNo));
                sqlcmd.Parameters.Add(new SqlParameter("@end", m_DH604EndNo));
                try
                {
                    oleConnection.Open();
                    SqlDataAdapter SqlAdapter = new SqlDataAdapter(sqlcmd);
                    SqlAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                backgroundWorker.ReportProgress(0, "总项目数：" + dt.Rows.Count.ToString()+" "+ DateTime.Now.ToString());
            }
            //backgroundWorker.ReportProgress(0, DateTime.Now.ToString() + "access end");
            if (dt.Rows.Count > 0)
            {
                LisDatabase db = AftDataContext.GetLisDatabase();
                DateTime createTime = DateTime.Now;
                #region
                foreach (DataRow row in dt.Rows)
                {
                    DateTime dateTimeOfTheObservation = DateTime.Parse(row["RequestTime"].ToString());
                    DateTime dateTime = dateTimeOfTheObservation.Date;
                    string fillerOrderNumber = row["SampleNo"].ToString();
                    string SAMPLE_ID = fillerOrderNumber;
                    //if(fillerOrderNumber.Length == 12)
                    //{
                    //    fillerOrderNumber = fillerOrderNumber.Substring(8);
                    //    try
                    //    {
                    //        fillerOrderNumber = Convert.ToInt32(fillerOrderNumber).ToString();
                    //    }
                    //    catch
                    //    {

                    //    }
                    //}

                    Lis_Ins_Result result = (from g in Results where g.FDateTime == dateTime && g.FResultID == fillerOrderNumber select g).FirstOrDefault();
                    if (result == null)
                    {
                        result = new Lis_Ins_Result();
                        result.FTaskID = Guid.NewGuid().ToString().Replace("-", "");
                        result.FInstrID = Cofing.Config.DeviceName;
                        result.FResultID = fillerOrderNumber;
                        result.FDateTime = dateTime;
                        result.FCreateTime = createTime;
                        db.Lis_Ins_Result.InsertOnSubmit(result);
                        Results.Add(result);

                        //
                        backgroundWorker.ReportProgress(0, "\n"+SAMPLE_ID);
                    }


                    string item = row["TestCode"].ToString();
                    string value = row["Result"].ToString();
                    Lis_Ins_Result_Detail detail = (from g in Details where g.FItem == item && g.FTaskID == result.FTaskID select g).FirstOrDefault();
                    if (detail == null)
                    {
                        detail = new Lis_Ins_Result_Detail();
                        detail.FID = Guid.NewGuid().ToString().Replace("-", "");
                        detail.FTaskID = result.FTaskID;
                        detail.FItem = item;//obx.ObservationIdentifier.Substring(obx.ObservationIdentifier.IndexOf(msh.ComponentSeparator) + 1);
                        detail.FValue = value;
                        detail.FTime = dateTimeOfTheObservation;
                        detail.FCreateTime = createTime;
                        db.Lis_Ins_Result_Detail.InsertOnSubmit(detail);
                        Details.Add(detail);

                        backgroundWorker.ReportProgress(0, SAMPLE_ID+"  "+item + ":" + value);
                    }
                    else
                    {
                        if (detail.FValue != value)
                        {
                            Lis_Ins_Result_Detail newDetail = (from g in db.Lis_Ins_Result_Detail where g.FItem == item && g.FTaskID == result.FTaskID select g).FirstOrDefault();
                            if (detail.FValue != value)
                            {
                                string oldvalue = detail.FValue;
                                detail.FValue = value;
                                if (newDetail != null)
                                {
                                    oldvalue = newDetail.FValue;
                                    newDetail.FValue = value;
                                }

                                backgroundWorker.ReportProgress(0, SAMPLE_ID + "  " + item + ":" + value + "(旧值：" + oldvalue + ")");
                            }
                            else
                            {
                                backgroundWorker.ReportProgress(0, SAMPLE_ID + "  " + item + ":已有一致");
                            }
                        }
                        else
                        {
                            backgroundWorker.ReportProgress(0, SAMPLE_ID + "  " + item + ":已有一致");
                        }
                    }
                }
                #endregion
                db.SubmitChanges();

            }
            else
            {
                backgroundWorker.ReportProgress(0, "未找到样本数据");
            }
        }
    }
}
