﻿using AFTNet.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AFTNet.Cofing
{
    public class Config
    {

        private static int periodTime = 0;
        /// <summary>
        /// 轮询间隔时间
        /// </summary>
        public static int PeriodTime
        {
            get
            {
                if (periodTime == 0)
                {
                    Tools.TryGetAppSetting("PeriodTime", ref periodTime, 60);

                }
                return periodTime;
            }
            set { periodTime = value; }
        }

        private static string deviceDB = "";
        /// <summary>
        /// 设备数据库
        /// </summary>
        //public static string DeviceDB
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(deviceDB))
        //        {
        //            string path = "";
        //            Tools.TryGetAppSetting("DeviceDB", ref path, "");
        //            deviceDB = @"Provider=Microsoft.Jet.OLEDB.4.0;Password=;Data Source=" + path + ";Persist Security Info=True";

        //        }
        //        return deviceDB;
        //    }
        //    set { deviceDB = value; }
        //}

        public static string DeviceDB
        {
            get
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    Tools.TryGetAppSetting("DeviceDB", ref filePath, "");
                }

                //deviceDB = @"Provider=Microsoft.Jet.OLEDB.4.0;Password=;Data Source=" + filePath + ";Persist Security Info=True";
                deviceDB = filePath;

                return deviceDB;
            }
            //set { deviceDB = value; }
        }

        private static string filePath = "";
        public static string DBFilePath
        {
            get
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    Tools.TryGetAppSetting("DeviceDB", ref filePath, "");
                }
                return filePath;
            }
            set
            {
                filePath = value;
            }
        }


        private static string deviceName = "";
        /// <summary>
        /// 设备名称
        /// </summary>
        public static string DeviceName
        {
            get
            {
                if (string.IsNullOrEmpty(deviceName))
                {
                    Tools.TryGetAppSetting("DeviceName", ref deviceName, "DH-604");

                }
                return deviceName;
            }
            set { deviceName = value; }
        }

        public static DateTime MinTime
        {
            get
            {
                return DateTime.Parse("1990-12-12");
            }
        }

    }
}
