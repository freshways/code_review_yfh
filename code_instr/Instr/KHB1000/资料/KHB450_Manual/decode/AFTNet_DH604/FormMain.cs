﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using AFTNet.Cofing;

namespace AFTNet
{
    public partial class FormMain : Form
    {
        private const string instrConnStr = "AccessPath.txt";
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            ShowDefaultPath();
        }

        private void ShowDefaultPath()
        {
            string AccessPath = GetPath();
            if(!string.IsNullOrWhiteSpace(AccessPath))
            {
                this.textBoxFilePath.Text = AccessPath;
                Config.DBFilePath = AccessPath;
            }
            //string FilePath = Application.StartupPath + "\\"+ instrConnStr;
            //if (File.Exists(FilePath))
            //{
            //    try
            //    {
            //        StreamReader reader = new StreamReader(FilePath);
            //        string AccessPath = reader.ReadLine();
            //        if(File.Exists(AccessPath))
            //        {
            //            this.textBoxFilePath.Text = AccessPath;

            //            //
            //            Config.DBFilePath = AccessPath;
            //            //Config.DBFilePath = AccessPath.Replace("\\", "\\\\");
            //        }
            //        reader.Close();
            //    }
            //    catch
            //    { }
            //}
        }

        private string GetPath()
        {
            string ret = "";
            string FilePath = Application.StartupPath + "\\" + instrConnStr;
            if (File.Exists(FilePath))
            {
                try
                {
                    StreamReader reader = new StreamReader(FilePath);
                    string AccessPath = reader.ReadLine();
                    //if (File.Exists(AccessPath))
                    {
                        ret = AccessPath;
                    }
                    reader.Close();
                }
                catch
                { }
            }
            return ret;
        }


        private void SaveDefaultPath(string path)
        {
            string FilePath = Application.StartupPath + "\\" + instrConnStr;
            try
            {
                StreamWriter writer = new StreamWriter(FilePath,false);
                writer.WriteLine(path);
                writer.Close();
                Config.DBFilePath = path;
                //Config.DBFilePath = path.Replace("\\","\\\\");
            }
            catch
            { }
        }

        private void btnOpenfile_Click(object sender, EventArgs e)
        {
            string nowpath = GetPath();
            Frm设置连接 frm = new Frm设置连接(nowpath);
            DialogResult openfileresult = frm.ShowDialog();
            if(DialogResult.OK == openfileresult)
            {
                textBoxFilePath.Text = frm.newpath;
                string temp = frm.newpath;
                SaveDefaultPath(temp);
            }
            this.ActiveControl = STestNO;
        }

        

        private void btn退出_Click(object sender, EventArgs e)
        {
            if (backgroundWorkerDecode.IsBusy)
            {
                MessageBox.Show("解码操作正在进行中，请不要关闭窗口。");
                return;
            }
            this.Close();

            //DialogResult drExit = MessageBox.Show("您要关闭程序吗？", "关闭确认", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            //if (DialogResult.Yes == drExit)
            //{
            //    this.Close();
            //}
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            //this.Show();
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
            this.Focus();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if(this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                //notifyIconTray.Visible = true;
                //this.Hide();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(backgroundWorkerDecode.IsBusy)
            {
                MessageBox.Show("解码操作正在进行中，请不要关闭窗口。");
                e.Cancel = true;
                return;
            }

            DialogResult drExit = MessageBox.Show("您要关闭程序吗？", "关闭确认", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (DialogResult.Yes != drExit)
            {
                e.Cancel = true;
            }
        }

        private void AppendTextToRichTextBox(string msg)
        {
            richTextBoxMsg.AppendText(msg);
            richTextBoxMsg.ScrollToCaret();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxFilePath.Text))
            {
                btnOpenfile.Focus();
            }
            else
            {
                STestNO.Focus();
            }
        }

        private void STestNO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((this.ActiveControl == STestNO) && (e.KeyChar == (char)Keys.Enter))
            {
                this.ActiveControl = ETestNO;
                ETestNO.SelectAll();
                e.Handled = true;
            }
        }

        private void ETestNO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((this.ActiveControl == ETestNO) && (e.KeyChar == (char)Keys.Enter))
            {
                this.ActiveControl = btn开始处理;
                e.Handled = true;
            }
        }

        private void backgroundWorkerDecode_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            AppendTextToRichTextBox("处理结束。"+DateTime.Now.ToString()+"\n\n");
            SetControlEnableToTrue();
        }

        private void SetControlEnableToFalse()
        {
            bool value = false;
            this.btnOpenfile.Enabled = value;
            this.STestNO.Enabled = value;
            this.ETestNO.Enabled = value;
            this.dateTimePicker1.Enabled = value;
            this.btn开始处理.Enabled = value;
            this.btn退出.Enabled = value;
        }

        private void SetControlEnableToTrue()
        {
            bool value = true;
            this.btnOpenfile.Enabled = value;
            this.STestNO.Enabled = value;
            this.ETestNO.Enabled = value;
            this.dateTimePicker1.Enabled = value;
            this.btn开始处理.Enabled = value;
            this.btn退出.Enabled = value;
        }

        private void 退出程序ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 显示窗口ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
            this.Focus();
        }

        private void btn开始处理_Click(object sender, EventArgs e)
        {
            string strFilePath = Config.DBFilePath;
            //string strDateTime = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            int iBeginTestNo = 1;
            int iEndTestNo = 1;

            //if (!(File.Exists(strFilePath)))
            //{
            //    MessageBox.Show("请确认文件的路径是否正确。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            //    return;
            //}

            if (string.IsNullOrWhiteSpace(strFilePath))
            {
                MessageBox.Show("请确认仪器数据库连接串是否已设置。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            //if (!(Regex.IsMatch(strBeginTestNo, @"^\d*$")) || !(Regex.IsMatch(strEndTestNo, @"^\d*$")))
            //{
            //    MessageBox.Show("样本号必需是阿拉伯数字。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            //    return;
            //}
            try
            {  //目的是去掉多余的零
                iBeginTestNo = Convert.ToInt32(this.STestNO.Text.Trim());
                iEndTestNo = Convert.ToInt32(this.ETestNO.Text.Trim());
                if(iBeginTestNo > 9999 || iEndTestNo > 9999 || iBeginTestNo < 0 || iEndTestNo < 0)
                {
                    MessageBox.Show("样本号必需是介于0--9999之间的数值。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
            }
            catch
            {
                MessageBox.Show("样本号必需是介于0--9999之间的数值。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            SetControlEnableToFalse();

            //MoveDataFromAccessToLisDB(strFilePath, strDateTime, strBeginTestNo, strEndTestNo);
            //SetControlEnableToFalse();

            SampleControl sam = new SampleControl(this.backgroundWorkerDecode, dateTimePicker1.Value.Date, iBeginTestNo, iEndTestNo);
            this.backgroundWorkerDecode.RunWorkerAsync(sam);

            this.ActiveControl = STestNO;
            STestNO.SelectAll();
        }

        private void backgroundWorkerDecode_DoWork(object sender, DoWorkEventArgs e)
        {
            this.backgroundWorkerDecode.ReportProgress(0,"开始处理："+DateTime.Now.ToString());

            //string strDate = dateTimePicker1.Value.Date.ToString();
            //this.backgroundWorkerDecode.ReportProgress(0, strDate);

            SampleControl sam = e.Argument as SampleControl;

            DH604 dh604 = new DH604(sam.worker, sam.date, sam.strBeginNo, sam.strEndNo);
            dh604.StartDecode();

            //AppendTextToRichTextBox(DateTime.Now.ToString() + "\n");
        }

        private void backgroundWorkerDecode_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                AppendTextToRichTextBox(e.UserState.ToString()+"\n");
            }
        }
    }

    public class SampleControl
    {
        public DateTime date;
        public int strBeginNo;
        public int strEndNo;
        public BackgroundWorker worker;

        public SampleControl(BackgroundWorker work, DateTime dt, int begin, int end)
        {
            worker = work;
            date = dt;
            strBeginNo = begin;
            strEndNo = end;
        }
    }
}
