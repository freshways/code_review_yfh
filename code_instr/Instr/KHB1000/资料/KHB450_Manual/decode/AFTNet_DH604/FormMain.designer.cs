﻿namespace AFTNet
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxFilePath = new System.Windows.Forms.TextBox();
            this.btnOpenfile = new System.Windows.Forms.Button();
            this.richTextBoxMsg = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.STestNO = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ETestNO = new System.Windows.Forms.TextBox();
            this.btn开始处理 = new System.Windows.Forms.Button();
            this.btn退出 = new System.Windows.Forms.Button();
            this.notifyIconTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示窗口ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出程序ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorkerDecode = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStripTray.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "数据连接：";
            // 
            // textBoxFilePath
            // 
            this.textBoxFilePath.Location = new System.Drawing.Point(88, 13);
            this.textBoxFilePath.Name = "textBoxFilePath";
            this.textBoxFilePath.ReadOnly = true;
            this.textBoxFilePath.Size = new System.Drawing.Size(397, 21);
            this.textBoxFilePath.TabIndex = 3;
            // 
            // btnOpenfile
            // 
            this.btnOpenfile.Location = new System.Drawing.Point(506, 12);
            this.btnOpenfile.Name = "btnOpenfile";
            this.btnOpenfile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenfile.TabIndex = 4;
            this.btnOpenfile.Text = "设置连接";
            this.btnOpenfile.UseVisualStyleBackColor = true;
            this.btnOpenfile.Click += new System.EventHandler(this.btnOpenfile_Click);
            // 
            // richTextBoxMsg
            // 
            this.richTextBoxMsg.Location = new System.Drawing.Point(21, 81);
            this.richTextBoxMsg.Name = "richTextBoxMsg";
            this.richTextBoxMsg.ReadOnly = true;
            this.richTextBoxMsg.Size = new System.Drawing.Size(464, 262);
            this.richTextBoxMsg.TabIndex = 5;
            this.richTextBoxMsg.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "标本日期：";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(88, 43);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(115, 21);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(223, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "样本号区间：";
            // 
            // STestNO
            // 
            this.STestNO.Location = new System.Drawing.Point(296, 42);
            this.STestNO.Name = "STestNO";
            this.STestNO.Size = new System.Drawing.Size(77, 21);
            this.STestNO.TabIndex = 9;
            this.STestNO.Text = "1";
            this.STestNO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.STestNO_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(379, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "---";
            // 
            // ETestNO
            // 
            this.ETestNO.Location = new System.Drawing.Point(408, 43);
            this.ETestNO.Name = "ETestNO";
            this.ETestNO.Size = new System.Drawing.Size(77, 21);
            this.ETestNO.TabIndex = 11;
            this.ETestNO.Text = "1";
            this.ETestNO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ETestNO_KeyPress);
            // 
            // btn开始处理
            // 
            this.btn开始处理.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn开始处理.Location = new System.Drawing.Point(506, 81);
            this.btn开始处理.Name = "btn开始处理";
            this.btn开始处理.Size = new System.Drawing.Size(75, 37);
            this.btn开始处理.TabIndex = 12;
            this.btn开始处理.Text = "开始上传";
            this.btn开始处理.UseVisualStyleBackColor = true;
            this.btn开始处理.Click += new System.EventHandler(this.btn开始处理_Click);
            // 
            // btn退出
            // 
            this.btn退出.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn退出.Location = new System.Drawing.Point(506, 141);
            this.btn退出.Name = "btn退出";
            this.btn退出.Size = new System.Drawing.Size(75, 37);
            this.btn退出.TabIndex = 14;
            this.btn退出.Text = "退出";
            this.btn退出.UseVisualStyleBackColor = true;
            this.btn退出.Click += new System.EventHandler(this.btn退出_Click);
            // 
            // notifyIconTray
            // 
            this.notifyIconTray.ContextMenuStrip = this.contextMenuStripTray;
            this.notifyIconTray.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIconTray.Icon")));
            this.notifyIconTray.Text = "DH604解码程序";
            this.notifyIconTray.Visible = true;
            this.notifyIconTray.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenuStripTray
            // 
            this.contextMenuStripTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示窗口ToolStripMenuItem,
            this.退出程序ToolStripMenuItem});
            this.contextMenuStripTray.Name = "contextMenuStripTray";
            this.contextMenuStripTray.Size = new System.Drawing.Size(125, 48);
            // 
            // 显示窗口ToolStripMenuItem
            // 
            this.显示窗口ToolStripMenuItem.Name = "显示窗口ToolStripMenuItem";
            this.显示窗口ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.显示窗口ToolStripMenuItem.Text = "显示窗口";
            this.显示窗口ToolStripMenuItem.Click += new System.EventHandler(this.显示窗口ToolStripMenuItem_Click);
            // 
            // 退出程序ToolStripMenuItem
            // 
            this.退出程序ToolStripMenuItem.Name = "退出程序ToolStripMenuItem";
            this.退出程序ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.退出程序ToolStripMenuItem.Text = "退出程序";
            this.退出程序ToolStripMenuItem.Click += new System.EventHandler(this.退出程序ToolStripMenuItem_Click);
            // 
            // backgroundWorkerDecode
            // 
            this.backgroundWorkerDecode.WorkerReportsProgress = true;
            this.backgroundWorkerDecode.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerDecode_DoWork);
            this.backgroundWorkerDecode.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerDecode_ProgressChanged);
            this.backgroundWorkerDecode.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerDecode_RunWorkerCompleted);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 355);
            this.Controls.Add(this.btn退出);
            this.Controls.Add(this.btn开始处理);
            this.Controls.Add(this.ETestNO);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.STestNO);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.richTextBoxMsg);
            this.Controls.Add(this.btnOpenfile);
            this.Controls.Add(this.textBoxFilePath);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KHB450解码程序";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.contextMenuStripTray.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxFilePath;
        private System.Windows.Forms.Button btnOpenfile;
        private System.Windows.Forms.RichTextBox richTextBoxMsg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox STestNO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ETestNO;
        private System.Windows.Forms.Button btn开始处理;
        private System.Windows.Forms.Button btn退出;
        private System.Windows.Forms.NotifyIcon notifyIconTray;
        private System.ComponentModel.BackgroundWorker backgroundWorkerDecode;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTray;
        private System.Windows.Forms.ToolStripMenuItem 显示窗口ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出程序ToolStripMenuItem;

    }
}

