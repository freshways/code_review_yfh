﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AFTNet
{
    public partial class Frm设置连接 : Form
    {
        public string newpath = "";
        public Frm设置连接(string path)
        {
            InitializeComponent();
            newpath = path;
            this.textBox1.Text = path;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            newpath = textBox1.Text.Replace("\r","").Replace("\n","");
            this.DialogResult = DialogResult.OK;
        }
    }
}
