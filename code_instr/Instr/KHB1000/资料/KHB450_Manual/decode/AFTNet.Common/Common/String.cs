using System;
using System.Text;
using System.Text.RegularExpressions;

namespace AFTNet.Common
{
	public sealed partial class Tools
    {
		#region CompareString

		/// <summary>
		/// �ַ����Ƿ���ͬ
		/// </summary>
		/// <param name="str1">�ַ���1</param>
		/// <param name="str2">�ַ���2</param>
		/// <param name="ignoreCase">�Ƿ���Դ�Сд,trueΪ���Դ�Сд</param>
		/// <returns></returns>
		public static bool CompareString(string str1, string str2, bool ignoreCase)
		{
			return (string.Compare(str1, str2, ignoreCase) == 0) ? true : false;
		}

		/// <summary>
		/// �ַ����Ƿ���ͬ,���Դ�Сд
		/// </summary>
		/// <param name="str1">�ַ���1</param>
		/// <param name="str2">�ַ���2</param>
		/// <returns></returns>
		public static bool CompareString(string str1, string str2)
		{
			return CompareString(str1, str2, true);
		}

		#endregion

		#region Trim,TrimStart,TrimEnd

		/// <summary>
		/// ���ַ����Ŀ�ʼ��ĩβ��ȥָ���ַ���
		/// </summary>
		/// <param name="input">�������ַ���</param>
		/// <param name="pattern">Ҫ�Ƴ����ַ���</param>
		/// <returns>�������ַ���</returns>
		public static string Trim(string input, string pattern)
		{
			return Trim(input, pattern, "");
		}

		/// <summary>
		/// ���ַ����Ŀ�ʼ��ĩβ�滻ָ���ַ���
		/// </summary>
		/// <param name="input">�������ַ���</param>
		/// <param name="pattern">Ҫ�滻���ַ���</param>
		/// <param name="output">Ҫ�滻����ַ���</param>
		/// <returns></returns>
		public static string Trim(string input, string pattern, string output)
		{
			return TrimEnd(TrimStart(input, pattern), pattern, output);
		}

		/// <summary>
		/// ���ַ����Ŀ�ʼ��ȥָ���ַ���
		/// </summary>
		/// <param name="input">�������ַ���</param>
		/// <param name="pattern">Ҫ�Ƴ����ַ���</param>
		/// <returns>�������ַ���</returns>
		public static string TrimStart(string input, string pattern)
		{
			return TrimStart(input, pattern, "");
		}

		/// <summary>
		/// ���ַ����Ŀ�ʼ�滻ָ���ַ���
		/// </summary>
		/// <param name="input">�������ַ���</param>
		/// <param name="pattern">Ҫ�滻���ַ���</param>
		/// <param name="output">Ҫ�滻����ַ���</param>
		/// <returns></returns>
		public static string TrimStart(string input, string pattern, string output)
		{
			Regex rgx = new Regex("^(" + pattern + ")");
			return rgx.Replace(input, output);
		}

		/// <summary>
		/// ���ַ�����ĩβ��ȥָ���ַ���
		/// </summary>
		/// <param name="input">�������ַ���</param>
		/// <param name="pattern">Ҫ�Ƴ����ַ���</param>
		/// <returns>�������ַ���</returns>
		public static string TrimEnd(string input, string pattern)
		{
			return TrimEnd(input, pattern, "");
		}

		/// <summary>
		/// ���ַ�����ĩβ�滻ָ���ַ���
		/// </summary>
		/// <param name="input">�������ַ���</param>
		/// <param name="pattern">Ҫ�滻���ַ���</param>
		/// <param name="output">Ҫ�滻����ַ���</param>
		/// <returns></returns>
		public static string TrimEnd(string input, string pattern, string output)
		{
			Regex rgx = new Regex("(" + pattern + ")$");
			return rgx.Replace(input, output);
		}

		#endregion

		#region SubStirng,LeftString,RightString

		/// <summary>
		/// ��ȡ�ַ���
		/// </summary>
		/// <param name="input">����ȡ���ַ���</param>
		/// <param name="startIndex">��ʼ����λ��(0Ϊ��ʼλ��)</param>
		/// <param name="length">����</param>
		/// <returns></returns>
		public static string SubStirng(string input, int startIndex, int length)
		{
			string output = string.Empty;
			if (!string.IsNullOrEmpty(input))
			{
				if (input.Length < startIndex)
				{
					if (input.Length < startIndex + length)
					{
						output = input.Substring(startIndex);
					}
					else
					{
                        output = input.Substring(startIndex, length) + "...";
					}
				}
			}
			return output;
		}

		/// <summary>
		/// ��ȡ��߶���
		/// </summary>
		/// <param name="input">����ȡ���ַ���</param>
		/// <param name="length">����</param>
		/// <returns></returns>
		public static string LeftString(string input, int length)
		{
			string output = string.Empty;
			if (!string.IsNullOrEmpty(input))
			{
				if (input.Length <= length)
				{
					output = input;
				}
				else
				{
					output = input.Substring(0, length);
				}
			}
			return output;
		}

		/// <summary>
		/// ��ȡ�ұ߶���
		/// </summary>
		/// <param name="input">����ȡ���ַ���</param>
		/// <param name="length">����</param>
		/// <returns></returns>
		public static string RightString(string input, int length)
		{
			string output = string.Empty;
			if (!string.IsNullOrEmpty(input))
			{
				if (input.Length <= length)
				{
					output = input;
				}
				else
				{
                    output = input.Substring(input.Length - length) + "...";
				}
			}
			return output;
		}

		#endregion

		#region GetChineseNumber

		/// <summary>
		/// ��ȡ�����ַ�����
		/// </summary>
		/// <param name="input">�ַ���</param>
		/// <returns></returns>
		public static int GetChineseNumber(string input)
		{
			if (string.IsNullOrEmpty(input))
			{
				return 0;
			}
			else
			{
				Regex r = new Regex(@"[\u4E00-\u9FA5\uF900-\uFA2D]");
				MatchCollection m = r.Matches(input);
				return m.Count;
			}
		}

		public static string SubStringWithChinese(string str, int length)
		{
			Regex regex = new Regex(@"[\u4E00-\u9FA5\uF900-\uFA2D]", RegexOptions.Compiled);
			char[] stringChar = str.ToCharArray();
			StringBuilder sb = new StringBuilder();
			int nLength = 0;

			for (int i = 0; i < stringChar.Length; i++)
			{
				nLength += regex.IsMatch((stringChar[i]).ToString()) ? 2 : 1;

				if (nLength > length)
				{
					break;
				}
				else
				{
					sb.Append(stringChar[i]);
				}
			}

			return sb.ToString();
		}

		public static int GetEnCnStringLength(string Text)
		{
			int len = 0;
			for (int i = 0; i < Text.Length; i++)
			{
				byte[] byte_len = Encoding.Default.GetBytes(Text.Substring(i, 1));
				if (byte_len.Length > 1)
				{
					len += 2; //������ȴ���1�������ģ�ռ�����ֽڣ�+2
				}
				else
				{
					len += 1;  //������ȵ���1����Ӣ�ģ�ռһ���ֽڣ�+1
				}
			}

			return len;
		}

		#endregion

		/// <summary>
		/// ����ַ���
		/// </summary>
		/// <param name="input"></param>
		/// <param name="pattern"></param>
		/// <returns></returns>
		public static string[] Split(string input, string pattern)
		{
			return Regex.Split(input, pattern);
		}

        /// <summary>
        /// �� �� �ˣ�����
        /// ����ʱ�䣺2009-11-27
        /// </summary>
        /// <param name="input"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string IsNullOrEmpty(string input, string defaultValue)
        {
            return string.IsNullOrEmpty(input) ? defaultValue : input;
        }
	}
}