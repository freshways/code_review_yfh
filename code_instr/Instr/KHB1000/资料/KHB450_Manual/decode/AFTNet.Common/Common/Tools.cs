﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AFTNet.Common
{
    public partial class Tools
    {



        #region 读配置文件
        public static bool TryGetAppSetting(string key, ref string value)
        {
            return TryGetAppSetting(key, ref value, string.Empty);
        }


        public static bool TryGetAppSetting(string key, ref string value, string defaultValue)
        {
            bool success = !string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]);
            if (success)
                value = ConfigurationManager.AppSettings[key];
            else
                value = defaultValue;
            return success;
        }


        public static bool TryGetAppSetting(string key, ref int value, int defaultValue)
        {
            bool success = !string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]);
            if (success)
                value = Convert.ToInt32(ConfigurationManager.AppSettings[key]);
            else
                value = defaultValue;
            return success;
        }


        public static bool TryGetAppSetting(string key, ref int value)
        {
            return TryGetAppSetting(key, ref value, 0);
        }

        #endregion

        #region ip Helper
        //public static UInt32 IpToInt(string ip)
        //{
        //    char[] separator = new char[] { '.' };
        //    string[] items = ip.Split(separator);
        //    return UInt32.Parse(items[0]) << 24
        //            | UInt32.Parse(items[1]) << 16
        //            | UInt32.Parse(items[2]) << 8
        //            | UInt32.Parse(items[3]);
        //}

        //public static string IntToIp(UInt32 ipInt)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append((ipInt >> 24) & 0xFF).Append(".");
        //    sb.Append((ipInt >> 16) & 0xFF).Append(".");
        //    sb.Append((ipInt >> 8) & 0xFF).Append(".");
        //    sb.Append(ipInt & 0xFF);
        //    return sb.ToString();
        //}
        #endregion

        #region  字节数组转化成数值

        //将字节数组转化为数值
        public static int ConvertBytesToInt(byte[] arrByte, int offset)
        {
            return BitConverter.ToInt32(arrByte, offset);
        }

        //将字节数组转化为数值
        public static UInt32 ConvertBytesToUInt32(byte[] arrByte, int offset)
        {
            return BitConverter.ToUInt32(arrByte, offset);
        }



        //将字节数组转化为数值
        public static float ConvertBytesToFloat(byte[] arrByte, int offset)
        {
            return BitConverter.ToSingle(arrByte, offset);
        }



        //将数值转化为字节数组
        //第二个参数设置是不是需要把得到的字节数组反转，因为Windows操作系统中整形的高低位是反转转之后保存的。
        public static byte[] ConvertIntToBytes(int value, bool reverse)
        {
            byte[] ret = BitConverter.GetBytes(value);
            if (reverse)
                Array.Reverse(ret);
            return ret;
        }


        //将字节数组转化为16进制字符串
        //第二个参数的含义同上。
        public static string ConvertBytesToHex(byte[] arrByte, bool reverse)
        {
            StringBuilder sb = new StringBuilder();
            if (reverse)
                Array.Reverse(arrByte);
            foreach (byte b in arrByte) sb.AppendFormat("{0:x2}", b);
            return sb.ToString();
        }


        //将16进制字符串转化为字节数组
        public static byte[] ConvertHexToBytes(string value)
        {
            int len = value.Length / 2;
            byte[] ret = new byte[len];
            for (int i = 0; i < len; i++)
                ret[i] = (byte)(Convert.ToInt32(value.Substring(i * 2, 2), 16));
            return ret;
        }

        //将字符转换为16进制字符串
        public static string StrToHex(string str)
        {
            string strTemp = "";
            if (string.IsNullOrEmpty(str))
                return "";
            byte[] bTemp = System.Text.Encoding.Default.GetBytes(str);

            for (int i = 0; i < bTemp.Length; i++)
            {
                strTemp += bTemp[i].ToString("X");
            }
            return strTemp;
        }



        //将16进制字符串转换为字符
        public static string HexToStr(string hexStr, System.Text.Encoding encoding)
        {
            byte[] charbytes = new byte[hexStr.Length / 2];
            for (int i = 0, r = 0; i < hexStr.Length; i += 2, r++)
            {
                charbytes[r] = byte.Parse(hexStr.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return encoding.GetString(charbytes);
        }



        static char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };


        //将字节数组转化为16进制字符串
        public static string ToHexString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                int b = bytes[i];
                chars[i * 2] = hexDigits[b >> 4];
                chars[i * 2 + 1] = hexDigits[b & 0xF];
            }
            return new string(chars);
        }
        #endregion

        #region 字节数组常用操作
        public static int ByteIndexOf(byte[] searched, byte[] find, int start)
        {
            bool matched = false;
            int end = find.Length - 1;
            int skip = 0;
            for (int index = start; index <= searched.Length - find.Length; ++index)
            {
                matched = true;
                if (find[0] != searched[index] || find[end] != searched[index + end]) continue;
                else skip++;
                if (end > 10)
                    if (find[skip] != searched[index + skip] || find[end - skip] != searched[index + end - skip])
                        continue;
                    else skip++;
                for (int subIndex = skip; subIndex < find.Length - skip; ++subIndex)
                {
                    if (find[subIndex] != searched[index + subIndex])
                    {
                        matched = false;
                        break;
                    }
                }
                if (matched)
                {
                    return index;
                }
            }
            return -1;
        }

        #endregion


        #region 加解密
        private static string KEY_64 = "VavicApp";//注意了，是8个字符，64位
        private static string IV_64 = "VavicApp";

        public static string Encode(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return "";
            }
            byte[] byKey = System.Text.ASCIIEncoding.ASCII.GetBytes(KEY_64);
            byte[] byIV = System.Text.ASCIIEncoding.ASCII.GetBytes(IV_64);

            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            int i = cryptoProvider.KeySize;
            MemoryStream ms = new MemoryStream();
            CryptoStream cst = new CryptoStream(ms, cryptoProvider.CreateEncryptor(byKey, byIV), CryptoStreamMode.Write);

            StreamWriter sw = new StreamWriter(cst);
            sw.Write(data);
            sw.Flush();
            cst.FlushFinalBlock();
            sw.Flush();
            return Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);

        }

        public static string Decode(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return "";

            }
            byte[] byKey = System.Text.ASCIIEncoding.ASCII.GetBytes(KEY_64);
            byte[] byIV = System.Text.ASCIIEncoding.ASCII.GetBytes(IV_64);

            byte[] byEnc;
            try
            {
                byEnc = Convert.FromBase64String(data);
            }
            catch
            {
                return null;
            }

            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream ms = new MemoryStream(byEnc);
            CryptoStream cst = new CryptoStream(ms, cryptoProvider.CreateDecryptor(byKey, byIV), CryptoStreamMode.Read);
            StreamReader sr = new StreamReader(cst);
            return sr.ReadToEnd();
        }
        #endregion

        
    }
}
