using System;
using System.Text;
using System.Text.RegularExpressions;

namespace AFTNet.Common
{
    public sealed partial class Tools
	{
		#region ReturnNumber

		/// <summary>
		/// 返回INT
		/// </summary>
		/// <param name="object"></param>
		/// <returns>如不为整数则返回0</returns>
		public static byte ReturnByte(object input)
		{
			try
			{
				byte tmp = Convert.ToByte(input);
				return tmp;
			}
			catch
			{
				return 0;
			}
		}

		/// <summary>
		/// 返回Byte
		/// </summary>
		/// <param name="sString"></param>
		/// <returns>如不为整数则返回0</returns>
		public static byte ReturnByte(string input)
		{
			try
			{
				byte tmp = Convert.ToByte(input);
				return tmp;
			}
			catch
			{
				if (input.Equals("true", StringComparison.OrdinalIgnoreCase))
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
		}

		/// <summary>
		/// 返回INT
		/// </summary>
		/// <param name="object"></param>
		/// <returns>如不为整数则返回0</returns>
		public static int ReturnInt32(object input)
		{
			try
			{
				int tmp = Convert.ToInt32(input);
				return tmp;
			}
			catch
			{
				return 0;
			}
		}

        /// <summary>
        /// 返回INT
        /// </summary>
        /// <param name="sString"></param>
        /// <returns>如不为整数则返回0</returns>
        public static int ReturnInt32(string input)
        {
            try
            {
                int tmp = Convert.ToInt32(input);
                return tmp;
            }
            catch
            {
				if (input.Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

		/// <summary>
		/// 返回INT
		/// </summary>
		/// <param name="object"></param>
		/// <returns>如不为整数则返回0</returns>
		public static long ReturnInt64(object input)
		{
			try
			{
				long tmp = Convert.ToInt64(input);
				return tmp;
			}
			catch
			{
				return 0;
			}
		}

        /// <summary>
        /// 返回LONG
        /// </summary>
        /// <param name="sString"></param>
        /// <returns>如不为整数则返回0</returns>
        public static long ReturnInt64(string input)
        {
            try
            {
                long tmp = Convert.ToInt64(input);
                return tmp;
            }
            catch
            {
				if (input.Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }

        #endregion

		#region DateTime

		/// <summary>
		/// 时间枚举
		/// </summary>
		public enum dtOption
		{
			Default,
			Year,
			Month,
			Day,
			Hour,
			Minute,
			Second
		}

		/// <summary>
		/// 格式化日期为DateTime
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
        public static DateTime GetDateTime(string input)
        {
			return GetDateTime(input, dtOption.Second);
        }

		public static DateTime GetDateTime(string input, dtOption option)
		{
			string dtString = Tools.PrintfDateTime(input.Trim(), option);
			DateTime dt = DateTime.Parse(dtString);
			return dt;
		}

        public static DateTime GetLowerDateTime(string input)
        {
            return GetDateTime(input, dtOption.Day);
        }

        public static DateTime GetLowerDateTime(DateTime dt)
        {
            return GetLowerDateTime(dt.ToString());
        }

        public static DateTime GetUpperDateTime(string input)
        {
			return GetLowerDateTime(input).AddDays(1);
        }

        public static DateTime GetUpperDateTime(DateTime dt)
        {
			return GetLowerDateTime(dt).AddDays(1);
        }

        public static DateTime GetLowerMonth(string input)
        {
			return GetDateTime(input, dtOption.Month);
        }

        public static DateTime GetLowerMonth(DateTime dt)
        {
            return GetLowerMonth(dt.ToString());
        }

		public static DateTime GetUpperMonth(string input)
		{
			return GetLowerMonth(input).AddMonths(1);
		}

        public static DateTime GetUpperMonth(DateTime dt)
        {
			return GetLowerMonth(dt).AddMonths(1);
        }

		#region PrintfDateTime

		/// <summary>
        /// 格式化日期为yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string PrintfDateTime(DateTime dt)
        {
            return PrintfDateTime(dt, dtOption.Second);
        }

        /// <summary>
        /// 格式化日期为yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="option">格式设置</param>
        /// <returns></returns>
        public static string PrintfDateTime(DateTime dt, dtOption option)
        {
            switch (option)
            {
                case dtOption.Year:
                    return dt.ToString("yyyy");
                case dtOption.Month:
                    return dt.ToString("yyyy-MM");
                case dtOption.Day:
                    return dt.ToString("yyyy-MM-dd");
                case dtOption.Hour:
                    return dt.ToString("yyyy-MM-dd HH");
                case dtOption.Minute:
                    return dt.ToString("yyyy-MM-dd HH:mm");
                default:
                    return dt.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        /// <summary>
        /// 格式化日期为yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string PrintfDateTime(string input)
        {            
            return PrintfDateTime(input.Trim(), dtOption.Second);
        }

        /// <summary>
        /// 格式化日期为yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <param name="input"></param>
        /// <param name="option">格式设置</param>
        /// <returns></returns>
        public static string PrintfDateTime(string input, dtOption option)
        {
			if (IsNumber(input))
			{
				string tmp = input.PadRight(14, '0');
				try
				{
					switch (option)
					{
						case dtOption.Year:
							return tmp.Substring(0, 4);
						case dtOption.Month:
							return string.Format("{0}-{1}", tmp.Substring(0, 4), GetMonthFromString(tmp));
						case dtOption.Day:
							return string.Format("{0}-{1}-{2}", tmp.Substring(0, 4), GetMonthFromString(tmp), GetDayFromString(tmp));
						case dtOption.Hour:
							return string.Format("{0}-{1}-{2} {3}", tmp.Substring(0, 4), GetMonthFromString(tmp), GetDayFromString(tmp), GetHourFromString(tmp));
						case dtOption.Minute:
							return string.Format("{0}-{1}-{2} {3}:{4}", tmp.Substring(0, 4), GetMonthFromString(tmp), GetDayFromString(tmp), GetHourFromString(tmp), GetMinuteFromString(tmp));
						default:
							return string.Format("{0}-{1}-{2} {3}:{4}:{5}", tmp.Substring(0, 4), GetMonthFromString(tmp), GetDayFromString(tmp), GetHourFromString(tmp), GetMinuteFromString(tmp), GetSecondFromString(tmp));
					}
				}
				catch
				{
					return input;
				}
			}
			else
			{
				if(!string.IsNullOrEmpty(input))
					return PrintfDateTime(ScanfDateTime(input), option);
				return input;
			}

			#region #

			/*
            DateTime output;
            try
            {
                output = DateTime.Parse(input);
            }
            catch
            {
                if (IsNumber(input))
                {
                    string tmp = input.PadRight(14, '0');
                    try
                    {
                        output = DateTime.Parse(string.Format("{0}-{1}-{2} {3}:{4}:{5}",
                                                                tmp.Substring(0, 4),
                                                                tmp.Substring(4, 2),
                                                                tmp.Substring(6, 2),
                                                                tmp.Substring(8, 2),
                                                                tmp.Substring(10, 2),
                                                                tmp.Substring(12, 2)
                                                              )
                                               );
                    }
                    catch
                    {
                        return input;
                    }
                }
                else
                {
                    return input;
                }
            }
            return PrintfDateTime(output, option);
			*/

			#endregion
		}

		private static string GetMonthFromString(string input)
		{
			if (input.Length >= 8)
			{
				string result = input.Substring(4, 2);
				if (result.CompareTo("01") < 0)
				{
					result = "01";
				}
				else if(result.CompareTo("12") > 0)
				{
					result = "12";
				}
				return result;
			}
			return "";
		}

		private static string GetDayFromString(string input)
		{
			if (input.Length >= 8)
			{
				string result = input.Substring(6, 2);
				if (result.CompareTo("01") < 0)
				{
					result = "01";
				}
				else if (result.CompareTo("31") > 0)
				{
					result = "31";
				}
				return result;
			}
			return "";
		}

		private static string GetHourFromString(string input)
		{
			if (input.Length >= 8)
			{
				string result = input.Substring(8, 2);
                if (result.CompareTo("01") < 0 && input.Length < 10)
				{
					result = "01";
				}
				else if (result.CompareTo("23") > 0)
				{
					result = "23";
				}
				return result;
			}
			return "";
		}

		private static string GetMinuteFromString(string input)
		{
			if (input.Length >= 10)
			{
				string result = input.Substring(10, 2);
                if (result.CompareTo("01") < 0 && input.Length < 12)
				{
					result = "01";
				}
				else if (result.CompareTo("59") > 0)
				{
					result = "59";
				}
				return result;
			}
			return "";
		}

		private static string GetSecondFromString(string input)
		{
			if (input.Length >= 12)
			{
				string result = input.Substring(12, 2);
				if (result.CompareTo("01") < 0)
				{
					result = "01";
				}
				else if (result.CompareTo("59") > 0)
				{
					result = "59";
				}
				return result;
			}
			return "";
		}

        #endregion

        #region ScanfDateTime

        /// <summary>
        /// 格式化日期为yyyyMMddHHmmss
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ScanfDateTime(DateTime dt)
        {
            return ScanfDateTime(dt, dtOption.Second);
        }

        /// <summary>
        /// 格式化日期为yyyyMMddHHmmss
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="option">格式设置</param>
        /// <returns></returns>
        public static string ScanfDateTime(DateTime dt, dtOption option)
        {
			switch (option)
			{
				case dtOption.Year:
					return dt.ToString("yyyy");
				case dtOption.Month:
					return dt.ToString("yyyyMM");
				case dtOption.Day:
					return dt.ToString("yyyyMMdd");
				case dtOption.Hour:
					return dt.ToString("yyyyMMddHH");
				case dtOption.Minute:
					return dt.ToString("yyyyMMddHHmm");
				default:
					return dt.ToString("yyyyMMddHHmmss");
			}
        }

        /// <summary>
        /// 格式化日期为yyyyMMddHHmmss
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ScanfDateTime(string input)
        {
            return ScanfDateTime(input, dtOption.Second);
        }

        /// <summary>
        /// 格式化日期为yyyyMMddHHmmss
        /// </summary>
        /// <param name="input"></param>
        /// <param name="option">格式设置</param>
        /// <returns></returns>
        public static string ScanfDateTime(string input, dtOption option)
        {
            string output;
            try
            {
                output = DateTime.Parse(input).ToString("yyyyMMddHHmmss");
            }
            catch
            {
                if (IsNumber(input))
                {
                    output = input.PadRight(14, '0');
                }
                else
                {
                    return input;
                }
            }
            switch (option)
            {
                case dtOption.Year:
                    return output.Substring(0, 4);
                case dtOption.Month:
                    return output.Substring(0, 6);
                case dtOption.Day:
                    return output.Substring(0, 8);
                case dtOption.Hour:
                    return output.Substring(0, 10);
                case dtOption.Minute:
                    return output.Substring(0, 12);
                default:
                    return output;
            }
        }

        #endregion

		#region UnixDateTime

		/// <summary>
        /// 获得时间的Unix时间戳格式
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
		public static string ToUnixDateTime(DateTime dt)
        {
            TimeSpan ts = (TimeSpan)(dt - new DateTime(1970, 1, 1, 0, 0, 0));
			return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        /// <summary>
        /// 获得时间的Unix时间戳格式
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
		public static string ToUnixDateTime(string input)
        {
            try
            {
                TimeSpan ts = (TimeSpan)(DateTime.Parse(input) - new DateTime(1970, 1, 1, 0, 0, 0));
				return Convert.ToInt64(ts.TotalSeconds).ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

		/// <summary>
		/// 获得时间的Unix时间戳格式
		/// </summary>
		/// <param name="dt"></param>
		/// <returns></returns>
		public static DateTime FromUnixDateTime(string input)
		{
			return DateTime.Parse("1970-01-01 0:0:0").AddSeconds(Convert.ToDouble(input));
		}

        #endregion

		/// <summary>
		/// 计算年龄
		/// </summary>
		/// <param name="birthday">生日 支持yyyyMMdd格式</param>
		/// <returns></returns>
		public static int GetAgeFromBirthday(string birthday)
		{
			try
			{
				TimeSpan ts = DateTime.Now - DateTime.Parse(Tools.PrintfDateTime(birthday, dtOption.Day));
				return Convert.ToInt32(ts.TotalDays) / 365;
			}
			catch
			{
				return 0;
			}
		}

		#endregion

		#region IP

		/// <summary>
        /// 格式化字符串为IPv4 192.168.001.125格式
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string PrintfIP(string input)
        {
            if (IsIP(input))
            {
                string[] arry = input.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                if (arry.Length >= 3)
                {
                    return string.Format("{0}.{1}.{2}.{3}",
                                            arry[0].PadLeft(3, '0'),
                                            arry[1].PadLeft(3, '0'),
                                            arry[2].PadLeft(3, '0'),
                                            arry[3].PadLeft(3, '0')
                                        );

                }
                else
                {
                    return input;
                }
            }
            else
            {
                if (IsNumber(input))
                {
                    string result = input.PadRight(12, '0');
                    return string.Format("{0}.{1}.{2}.{3}",
                                            result.Substring(0, 3),
                                            result.Substring(3, 3),
                                            result.Substring(6, 3),
                                            result.Substring(9, 3)
                                        );
                }
                else
                {
                    return input;
                }
            }
        }

        /// <summary>
        /// 格式化字符串为IPv4 192168001125格式
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ScanfIP(string input)
        {
            if (IsIP(input))
            {
                string[] arry = input.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                if (arry.Length >= 3)
                {
                    return string.Format("{0}{1}{2}{3}",
                                            arry[0].PadLeft(3, '0'),
                                            arry[1].PadLeft(3, '0'),
                                            arry[2].PadLeft(3, '0'),
                                            arry[3].PadLeft(3, '0')
                                        );

                }
                else
                {
                    return input;
                }
            }
            else
            {
                if (!IsNumber(input))
                {
                    return input.PadRight(12, '0');
                }
                else
                {
                    return input;
                }
            }
        }

        #endregion

        #region HTML

        /// <summary>
        /// 去掉HTML标记
        ///</summary>
        /// <param name="strA">字符参数A</param>
        /// <returns>返回去掉HTML的串</returns>
        public static string ReplaceHtml(string strA)
        {
            Regex rgx = new Regex(@"<(.[^>]*)>");
            strA = rgx.Replace(strA, "");
            return strA;
        }

        #endregion

		#region Convert

		/// <summary>
		/// 获得格式化单位后的文件大小
		/// </summary>
		/// <param name="size">文件大小</param>
		/// <returns></returns>
		public static string Convert2FileSize(long size)
		{
			if (size < 1024)
			{
				return size.ToString() + "Bytes";
			}
			else
			{
				if (size < 1024 * 1024)
				{
					return Math.Round(size / 1024.0, 2).ToString() + "KB";
				}
				else
				{
					return Math.Round(size / (1024.0 * 1024.0), 2).ToString() + "MB";
				}
			}
		}

		/// <summary>
		/// 获取格式化单位后的时长
		/// </summary>
		public static string Convert2LifeTime(long lifetime)
		{
			long day = lifetime / 86400;
			long hour = (lifetime - day * 86400) / 3600;
			long minute = (lifetime - day * 86400 - hour * 3600) / 60;
			long second = lifetime - day * 86400 - hour * 3600 - minute * 60;

			StringBuilder sb = new StringBuilder();
			if (day > 0)
			{
				sb.Append(day.ToString() + "天");
			}
			if (hour > 0)
			{
				sb.Append(hour.ToString() + "小时");
			}
			if (minute > 0)
			{
				sb.Append(minute.ToString() + "分钟");
			}

			sb.Append(second.ToString() + "秒");

			return sb.ToString();
		}

		#endregion

		#region IdCard

		/// <summary>
		/// 身份证15转18位
		/// </summary>
		/// <param name="IDCard"></param>
		/// <returns></returns>
		public static string FormatIdCard15To18(string idCard)
		{
			string tmpIdCard = Tools.Trim(idCard, " ");
			if (tmpIdCard.Length == 15)
			{
				int iS = 0;

				//加权因子常数 
				int[] iW = new int[] { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
				//校验码常数 
				string LastCode = "10X98765432";
				//新身份证号 
				string perIDNew;

				perIDNew = tmpIdCard.Substring(0, 6);
				//填在第6位及第7位上填上‘1’，‘9’两个数字 
				perIDNew += "19";

				perIDNew += tmpIdCard.Substring(6, 9);

				//进行加权求和 
				for (int i = 0; i < 17; i++)
				{
					iS += int.Parse(perIDNew.Substring(i, 1)) * iW[i];
				}

				//取模运算，得到模值 
				int iY = iS % 11;
				//从LastCode中取得以模为索引号的值，加到身份证的最后一位，即为新身份证号。 
				perIDNew += LastCode.Substring(iY, 1);

				return perIDNew;
			}
			else
			{
				return tmpIdCard;
			}
		}

		/// <summary>
		/// 身份证18转15位
		/// </summary>
		/// <param name="IDCard"></param>
		/// <returns></returns>
		public static string FormatIdCard18To15(string idCard)
		{
			string tmpIdCard = Tools.Trim(idCard, " ");
			if (tmpIdCard.Length == 18)
			{
				return tmpIdCard.Remove(6, 2).Substring(0, 15);
			}
			else
			{
				return tmpIdCard;
			}
		}

		/// <summary>
		/// 根据身份证号计算年龄
		/// </summary>
		/// <param name="IDCard"></param>
		/// <returns></returns>
		public static int GetAgeFromIdCard(string idCard)
		{
			try
			{
				if (idCard.Length == 15)
				{
					DateTime _BirdthDay = DateTime.Parse(string.Format("19{0}-{1}-{2}", idCard.Substring(6, 2), idCard.Substring(8, 2), idCard.Substring(10, 2)));
					TimeSpan _Age = new TimeSpan(DateTime.Now.Ticks - _BirdthDay.Ticks);
					return (_Age.Days / 365);
				}
				else if (idCard.Length == 18)
				{
					DateTime _BirdthDay = DateTime.Parse(string.Format("{0}-{1}-{2}", idCard.Substring(6, 4), idCard.Substring(10, 2), idCard.Substring(12, 2)));
					TimeSpan _Age = new TimeSpan(DateTime.Now.Ticks - _BirdthDay.Ticks);
					return (_Age.Days / 365);
				}
				else
				{
					return 0;
				}
			}
			catch
			{
				return 0;
			}
		}

		#endregion

		#region byte[] string

		/// <summary>
		/// 转换byte[]为string
		/// </summary>
		/// <param name="encoding"></param>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public static string Bytes2String(Encoding encoding, byte[] bytes)
		{
			if (bytes != null)
			{
				return encoding.GetString(bytes);
			}
			return string.Empty;
		}

		/// <summary>
		/// 转换string为byte[]
		/// </summary>
		/// <param name="encoding"></param>
		/// <param name="input"></param>
		/// <returns></returns>
		public static byte[] StringToBytes(Encoding encoding, string input)
		{
			if (!string.IsNullOrEmpty(input))
			{
				return encoding.GetBytes(input);
			}
			return null;
		}

		#endregion

		/// <summary>
		/// 获取百分比
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		public static decimal GetPercent(int v1, int v2)
		{
			if (v1 == 0 || v2 == 0)
			{
				return 0;
			}

			return Convert.ToDecimal(v1 * 100) / v2;
		}

		/// <summary>
		/// 获取百分比
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		public static decimal GetPercent(long v1, long v2)
		{
			if (v1 == 0 || v2 == 0)
			{
				return 0;
			}

			return Convert.ToDecimal(v1 * 100) / v2;
		}
    }
}