using System;
using System.Collections.Generic;
using System.Text;

namespace SerialPortReceive
{
    /// v2 改了玩玩,试试vss
    /// 
    /// <summary> 条件应答类
    /// </summary>
    class ConditionAck
    {
        private int conditionStr;
        private char[] ackStr;

        /// <summary> 条件
        /// </summary>
        public int ConditionStr
        {
            get
            {
                return conditionStr;
            }
            set
            {
                conditionStr = value;
            }
        }

        /// <summary> 应答符
        /// </summary>
        public char[] ACKStr
        {
            get
            {
                return ackStr;
            }
            set
            {
                ackStr = value;
            }
        }

    }
}
