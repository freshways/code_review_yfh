using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SerialPortReceive
{

    class DelLogFiles
    {
        /// <summary> 按文件的最后修改时间,删除七天前的无用日志文件
        /// </summary>
        /// <param name="Logdir"></param>
        public void DelFiles(string Logdir)
        {
            try
            {
                FileSystemInfo info = new DirectoryInfo(Logdir);

                if (!info.Exists) return;
                DirectoryInfo dir = info as DirectoryInfo;
                if (dir == null) return; //不是目录

                FileSystemInfo[] files = dir.GetFileSystemInfos();

                DateTime dtLastWriteTime;
                DateTime dtNow = System.DateTime.Now;
                TimeSpan ts = new TimeSpan();

                foreach (FileSystemInfo fsi in files)
                {
                    try
                    {
                        dtLastWriteTime = fsi.LastWriteTime;
                        ts = dtNow - dtLastWriteTime;
                        if (ts.Days > 7)
                        {
                            fsi.Delete();
                        }
                    }
                    catch (Exception e)
                    {
                        System.Windows.Forms.MessageBox.Show("删除过期日志文件时出错" + fsi.Name + "\r\n" + e.ToString(), "错误", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                }

            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("删除过期日志文件时出错" + e.ToString(), "错误", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
    }
}
