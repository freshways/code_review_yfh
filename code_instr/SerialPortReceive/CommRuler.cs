using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SerialPortReceive
{
    class CommRuler
    {
        /// <summary> 取得32位GUID
        /// </summary>
        /// <returns></returns>
        public string SysGetGUID()
        {
            string strGuid = "";
            strGuid = System.Guid.NewGuid().ToString();
            return strGuid.Replace("-", "");
        }

        /// <summary> 写本地txt日志文件
        /// </summary>
        /// <param name="strLog"></param>
        public void WriteLog(string strLog)
        {
            string FileName;
            FileName = System.DateTime.Now.Year.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString();

            StreamWriter sw;
            sw = File.AppendText(FileName + ".log");

            sw.WriteLine(System.DateTime.Now.ToString());
            sw.Write(strLog);
            sw.WriteLine("");
            sw.Flush();
            sw.Close();
        }
    }
}
