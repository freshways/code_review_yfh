﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Data.OleDb;
using SerialPortReceive.Properties;
using System.Runtime.InteropServices;
using System.Reflection;

using System.Diagnostics;
using System.Threading;

namespace SerialPortReceive
{
    public enum DataMode { Text, Hex }
    public enum LogMsgType { Incoming, Outgoing, Normal, Warning, Error };

    /// <summary>
    /// 2008-08-15 签入
    /// 这两天用版本管理,也不知道好不好管
    /// v1
    /// v2 改了玩玩,试试vss
    /// </summary>
    public partial class ComReceive : Form
    {
        private Color[] LogMsgTypeColor = { Color.Blue, Color.Green, Color.Black, Color.Orange, Color.Red };
        private System.IO.Ports.SerialPort comport = new System.IO.Ports.SerialPort();
        CommRuler CommRuler = new CommRuler();
        LocalDataBase LocalDataBase = new LocalDataBase(Application.StartupPath);        
        const int WM_COPYDATA = 0x004A;
        private string InstrName;
        private long lonReceiveDateLen = 0;
        private long lonSaveDateLen = 0;
        private System.Timers.Timer timerSave;
        private BackgroundWorker bgwIcon;

        /// <summary> 后台串口数据存储线程
        /// </summary>
        private BackgroundWorker bgwPortDataSave;

        /// <summary> 本地存储失败时,重试次数
        /// </summary>
        private int LocalSaveRetryCount = 3;
        /// <summary> 本地存储失败时,已重试次数
        /// </summary>
        private int RetryCount = 0;

        #region 串口类参数

        /// <summary> 触发read事件后，等待多少毫秒后再读取数据
        /// 根据串口发数据的速度和缓冲写入条数的频率决定该值大小;频率太高,则等待时间可稍为长点,
        /// 同步将导到收数时间加长,否则写入缓冲的条数太多,反之待待时间要缩短,否则可能导到串口丢数据;
        /// 一般取0即可,如果串口接收频率过高则取100
        /// </summary>
        int WaitTimeToRead = 0;

        /// <summary> 在通信过程中是否启用请求发送(RTS)行
        /// </summary>
        bool RtsEnable = false;

        /// <summary>
        /// </summary>  在通信过程中是否启用数据终端就绪(DTR)行
        bool DtrEnable = false;

        /// <summary> 在添加到序列缓冲区前是否丢弃端口上接收的空字节
        /// </summary>
        bool DiscardNull = false;

        /// <summary> 用于数据交换中流控制的握手协议 None/RequestToSend/RequestToSendXOnXOff/XOnXOff
        /// </summary>
        Handshake uHandshake = Handshake.None;

        #endregion

        #region 应答定义

        /// <summary> 采数时间间隔
        /// 目前仅用于间隔发送应答字符
        /// 因为串口有数据后会自动采数据
        /// </summary>
        private Int32 ReadePortSpaceTime;
        /// <summary> 一直应答 </summary>
        private bool AllAckBool = false;
        /// <summary> 一直应答字符 </summary>
        private string AllAckStr = "";
        private char[] AllAckChars;
        /// <summary> 收到数据应答 </summary>
        private bool DataAckBool = false;
        /// <summary> 收到数据应答字符 </summary>
        private string DataAckStr = "";
        private char[] DataAckChars;
        /// <summary> 条件应答 </summary>
        private bool ConditionAckBool = false;
        /// <summary> 条件应答字符 </summary>
        private string ConditionAckStr = "";
        private List<ConditionAck> listConditionAck;

        #endregion

        public ComReceive()
        {
            InitializeComponent();

            // 不处理线程间异常操作
            Form.CheckForIllegalCrossThreadCalls = false;            

            //启动与接口监控程序通信线程
            Thread threadMonitor = new Thread(new ThreadStart(CreatMonitorTimer));
            threadMonitor.Start();

            //清除过期日志文件
            DelLogFiles DelTempLogFiles = new DelLogFiles();
            DelTempLogFiles.DelFiles(Application.StartupPath + @"\raw\");
            DelTempLogFiles = null;
        }

        #region 创建接口监视通信线程

        private System.Timers.Timer timer1;
        private void CreatMonitorTimer()
        {
            this.timer1 = new System.Timers.Timer();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(timer1_Elapsed);
            
            SendMessageToInstrMonitor();
        }

        void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            SendMessageToInstrMonitor();
        }

        #endregion

        Icon NullIcon,InstrIcon;
        private void ComReceive_Load(object sender, EventArgs e)
        {
            InitializeControlValues();
            this.Text += " - " + InstrName;
            notifyIcon1.Text = this.Text;
            comport.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            NullIcon = notifyIcon1.Icon;
            InstrIcon = new Icon(Application.StartupPath + "\\Port.ico");
            notifyIcon1.Icon = InstrIcon;
            pnlPassword.Visible = false;

            // 获取并显示版本号
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                AssemblyName assemblyName = assembly.GetName();
                Version version = assemblyName.Version;

                tllsVersion.Text = "V" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
            }
            catch
            {
                MessageBox.Show("获取主程序版本号时出错" + '\r' + '\r', "警告", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            if (Settings.Default.AutoOpenPort)
            {
                OpenClosePort();
            }

            // 根据读取串口时间间隔启动计时器
            if ((ReadePortSpaceTime != 0) && (AllAckBool == true) && (AllAckChars != null))
            {
                ReadPortTimer.Interval = ReadePortSpaceTime;
                ReadPortTimer.Enabled = true;
                ReadPortTimer.Tick += new EventHandler(ReadPortTimer_Tick);
            }

            //串口数据存储 调度计时器
            if (WaitTimeToRead > 0)
            {
                timerSave = new System.Timers.Timer(WaitTimeToRead);//实例化Timer类，设置间隔时间 
                timerSave.Elapsed += new System.Timers.ElapsedEventHandler(timerSave_Tick);//到达时间的时候执行事件；   
                timerSave.AutoReset = false;//设置是执行一次（false）还是一直执行(true)；   
                timerSave.Enabled = false;
            }

            //接收数据时闪动图标后台对象
            bgwIcon = new BackgroundWorker();
            bgwIcon.DoWork += new DoWorkEventHandler(bgwIcon_DoWork);            

            //串口数据后台存储线程
            bgwPortDataSave = new BackgroundWorker();
            bgwPortDataSave.DoWork += new DoWorkEventHandler(bgwPortDataSave_DoWork);
            bgwPortDataSave.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwPortDataSave_RunWorkerCompleted);
        }        
        
        /// <summary> //接收数据时闪动图标
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        int iok = 0;
        void bgwIcon_DoWork(object sender, DoWorkEventArgs e)
        {
            SetPortStatus("正在接收数据...");
            while (iok != 1)
            {
                notifyIcon1.Icon = NullIcon;
                System.Threading.Thread.Sleep(200);
                notifyIcon1.Icon = InstrIcon;
                System.Threading.Thread.Sleep(200);
            }
            SetPortStatus("接收就绪");
        }

        StringBuilder sbPortData = new StringBuilder();
        /// <summary> 接收串口数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool bolReceived = false;
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {            
            try
            {                
                bolReceived = true;
                if (!bgwIcon.IsBusy)
                {
                    iok = 0;
                    bgwIcon.RunWorkerAsync();
                }

                if (CurrentDataMode == DataMode.Text)
                {
                    timerSave.Stop();
                    string data = comport.ReadExisting();
                    sbPortData.Append(data);
                    
                    // 仅在配置了等待时间才启用计时器

                    if (WaitTimeToRead > 0)
                    {
                        timerSave.Start();
                        // 如果计时器未启动,则启动它
                        //if (!timerSave.Enabled)
                        //{                            
                        //    timerSave.Start();         //Start后计时器Enabled会为True,到时后就变为Flase
                        //}
                    }
                    else
                    {
                        SavePortData();
                    }

                    try
                    {
                        lonReceiveDateLen += data.Length;
                        tsslReceiveDataLen.Text = "已接收数:" + lonReceiveDateLen.ToString();
                    }
                    catch { }

                    #region 串口应答
                    //----------------------------------------
                    // 应答:收到数据时
                    if (DataAckBool)
                    {
                        WritePort(DataAckChars);
                    }

                    // 应答:根据条件
                    if (ConditionAckBool)
                    {
                        for (int i = data.Length - 1; i >= 0; i--)
                        {
                            char cstr = char.Parse(data[i].ToString());
                            int istr = (int)cstr;

                            // 使用委托从list中查到符合应答条件的应答字符组
                            ConditionAck lCondition = listConditionAck.Find(delegate(ConditionAck Condition) { return Condition.ConditionStr == istr; });
                            if (lCondition != null)
                            {
                                WritePort(lCondition.ACKStr);
                                break;
                            }
                        }
                    }
                    //----------------------------------------
                    #endregion
                }
                else
                {
                    // 流模式未写入到数据库中
                    int bytes = comport.BytesToRead;
                    byte[] buffer = new byte[bytes];
                    comport.Read(buffer, 0, bytes);
                    Log(LogMsgType.Normal, ByteArrayToHexString(buffer));
                    // 应答:收到数据时
                    if (DataAckBool)
                    {
                        WritePort(DataAckChars);
                    }
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show("接收数据时发生错误,请重新启动程序再试,错误消息" + '\n' + ex.ToString(), "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //接收完毕
                iok = 1;
                bolReceived = false;
            }
        }

        /// <summary> 串口数据存储计时器到时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerSave_Tick(object sender, EventArgs e)
        {
            if (!bolReceived)
            {
                SavePortData();
            }
            else
            {
                timerSave.Start();
            }
        }

        /// <summary> 存储串口数据
        /// </summary>
        private void SavePortData()
        {
            if (sbPortData.Length > 0)
            {
                // 启动后台存储线程
                if (!bgwPortDataSave.IsBusy)
                {
                    string strSaveData = sbPortData.ToString();
                    sbPortData.Remove(0, sbPortData.Length);

                    bgwPortDataSave.RunWorkerAsync(strSaveData);
                }
                //else
                //{
                //    timerSave.Start();
                //}
            }
        }

        /// <summary> 执行后台串口数据存储
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bgwPortDataSave_DoWork(object sender, DoWorkEventArgs e)
        {
            string strSaveData = e.Argument.ToString();
            
            if (!string.IsNullOrEmpty(strSaveData))
            {
                // 2008-10-08 移到此处 本来是要存储成功才记录的
                // 放到这儿便于分析导致存储失败的数据的原因
                WriteTxtData(strSaveData);

                AddBufferData(strSaveData);

                strSaveData = "";
            }
        }

        /// <summary> 串口数据后台存储线程完成后,再次启动计时器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bgwPortDataSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (sbPortData.Length > 0)
            {
                if (WaitTimeToRead > 0)
                {
                    // 如果计时器未启动,则启动它
                    if (!timerSave.Enabled)
                    {
                        timerSave.Start();         //Start后计时器Enabled会为True,到时后就变为Flase
                    }
                }
                else
                {
                    SavePortData();
                }                
            }
        }

        /// <summary> 记录串口接收数据日志
        /// </summary>
        /// <param name="strBufferData"></param>
        private void WriteTxtData(string strBufferData)
        {
            try
            {
                string strDate = System.DateTime.Now.ToString("yyMMdd");

                #region 记录串口接收数据日志
                if (Settings.Default.RecordPortalDataLog)
                {
                    using (StreamWriter sw = new StreamWriter(Application.StartupPath + @"\raw\" + strDate + "_log.txt", true, Encoding.UTF8))
                    {
                        sw.Write("★" + System.DateTime.Now.ToString() + '\r' + '\n');
                        sw.Write(strBufferData + '\r' + '\n');
                    }
                }
                #endregion

                #region 记录串口接收的数据
                if (Settings.Default.RecordPortalData)
                {
                    using (StreamWriter sw = new StreamWriter(Application.StartupPath + @"\raw\" + strDate + ".txt", true, Encoding.UTF8))
                    {
                        sw.Write(strBufferData);
                    }
                }
                #endregion

            }
            catch { }
        }

        /// <summary> 向ACCESS数据库写缓冲数据
        /// </summary>
        private void AddBufferData(string strBufferData)
        {
            int iAddRowCount;

            try
            {
                if (strBufferData != "")
                {
                    //ACCESS备注字段写入文本超过1908字符后不能删除
                    int iBufferDataLen = strBufferData.Length;
                    string strData;
                    if (iBufferDataLen > 1908)
                    { strData = strBufferData.Substring(0, 1907); }
                    else
                    { strData = strBufferData; }

                    // 使用sql方式插入数据
                    //string strsql = "Insert INTO Lis_ComPort_Buffer(FDate,FResult) values(CDate('" + System.DateTime.Now + "'),'" + strData + "')";
                    //OleDbCommand cmdBuffer = new OleDbCommand(strsql, LocalDataBase.conComDataBase);
                    // iAddRowCount = cmdBuffer.ExecuteNonQuery();

                    // 使用数据集插入数据
                    // 可避免结果字符串中有',符号时与sql语法冲突
                    string strComPortSql = "select * from Lis_ComPort_Buffer where 1=2";
                    OleDbDataAdapter daComPort = new OleDbDataAdapter(strComPortSql, LocalDataBase.conComDataBase);
                    OleDbCommandBuilder cb = new OleDbCommandBuilder(daComPort);
                    DataSet dsComPort = new DataSet();
                    daComPort.Fill(dsComPort, "Lis_ComPort_Buffer");
                    DataTable dtComPort = dsComPort.Tables[0];
                    DataRow drComPort = dtComPort.Rows.Add();

                    drComPort["FDate"] = System.DateTime.Now;
                    drComPort["FResult"] = strData;
                    iAddRowCount = daComPort.Update(dsComPort, "Lis_ComPort_Buffer");
                    
                    // end

                    LocalDataBase.Close();

                    if (iAddRowCount > 0)
                    {
                        //存储缓冲数据成功                        
                        try
                        {
                            lonSaveDateLen += strData.Length;
                            tsslSaveDataLen.Text = "已存储数据:" + lonSaveDateLen.ToString();
                        }
                        catch { }

                        Log(LogMsgType.Normal, strData);

                        SendMessageToDecode("1");
                        //SetPortStatus("串口就绪");
                        
                        RetryCount = 0;

                        // 再存剩余字符
                        try
                        {
                            strBufferData = strBufferData.Substring(strData.Length);
                            AddBufferData(strBufferData);
                        }
                        catch (Exception e)
                        {
                            Log(LogMsgType.Error, "再存剩余字符失败。" + e.ToString());
                        }
                    }
                    else
                    {
                        RetryCount += 1;

                        Log(LogMsgType.Error, "存储缓冲数据失败,返回值:" + iAddRowCount.ToString() + "程序将在10秒后重试,如果你多次看到这个消息,请重启程序或联系工程师解决");
                        LocalDataBase.Close();
                        ErrorSound();

                        if (RetryCount <= LocalSaveRetryCount)
                        {
                            System.Threading.Thread.Sleep(10000);

                            // 再次存失败前的全部字符
                            AddBufferData(strBufferData);
                        }
                        else
                        {
                            Log(LogMsgType.Error, "存储缓冲数据失败,连续重试 " + RetryCount + " 次均失败。放弃存储以下数据：\r\n" + strBufferData + "\r\n=============================请重启程序或联系工程师解决\r\n");
                            LocalDataBase.Close();
                            RetryCount = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RetryCount += 1;

                Log(LogMsgType.Error, "存储缓冲数据时发生异常错误,15秒后重试,错误信息" + '\r' + ex.ToString());
                LocalDataBase.Close();
                ErrorSound();

                if (RetryCount <= LocalSaveRetryCount)
                {
                    System.Threading.Thread.Sleep(15000);

                    // 再次存失败前的全部字符
                    AddBufferData(strBufferData);
                }
                else
                {
                    Log(LogMsgType.Error, "存储缓冲数据失败,连续重试 " + RetryCount + " 次均失败。放弃存储以下数据：\r\n" + strBufferData + "\r\n=============================请重启程序或联系工程师解决\r\n");
                    LocalDataBase.Close();
                    RetryCount = 0;
                }
            }
        }

        /// <summary>读取缓冲数据
        /// 每次都读出库里所有缓冲数据
        /// 返回:成功 - 缓冲数据(可能为空)
        /// </summary>
        /// <returns></returns>
        public string ReaderBufferData()
        {
            string strBuffer = "";
            string strID = "";

            try
            {
                string strsql = "select FID,FDate,FResult from Lis_ComPort_Buffer order by FID";
                OleDbCommand cmdBuffer = new OleDbCommand(strsql, LocalDataBase.conComDataBase);
                OleDbDataReader daBuffer = cmdBuffer.ExecuteReader();

                while (daBuffer.Read())
                {
                    if (daBuffer.IsDBNull(2) != true)
                    {
                        strBuffer += daBuffer.GetString(2);
                    }
                    strID += System.Convert.ToString(daBuffer.GetInt32(0)) + ",";
                }
                LocalDataBase.Close();

                return strBuffer;
            }
            catch
            {
                return "";
            }
        }

        /// <summary> 串口定时应答
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ReadPortTimer_Tick(object sender, EventArgs e)
        {
            WritePort(AllAckChars);
        }

        /// <summary> 向串口写入数据
        /// </summary>
        /// <param name="WriteChar"></param>
        private void WritePort(char[] WriteChar)
        {
            if (comport.IsOpen)
            {
                comport.Write(WriteChar, 0, WriteChar.Length);
            }
        }

        /// <summary> 获取端口配置
        /// </summary>
        private void InitializeControlValues()
        {
            try
            {
                cmbParity.Items.Clear(); cmbParity.Items.AddRange(Enum.GetNames(typeof(Parity)));
                cmbStopBits.Items.Clear(); cmbStopBits.Items.AddRange(Enum.GetNames(typeof(StopBits)));

                cmbParity.Text = Settings.Default.Parity.ToString();
                cmbStopBits.Text = Settings.Default.StopBits.ToString();
                cmbDataBits.Text = Settings.Default.DataBits.ToString();
                cmbParity.Text = Settings.Default.Parity.ToString();
                cmbBaudRate.Text = Settings.Default.BaudRate.ToString();
                CurrentDataMode = Settings.Default.DataMode;
                InstrName = Settings.Default.InstrName;

                WaitTimeToRead = Settings.Default.WaitTimeToRead;                
                RtsEnable = Settings.Default.RtsEnable;
                DtrEnable = Settings.Default.DtrEnable;
                DiscardNull = Settings.Default.DiscardNull;
                uHandshake = Settings.Default.uHandshake;

                cmbPortName.Items.Clear();
                foreach (string s in SerialPort.GetPortNames())
                    cmbPortName.Items.Add(s);

                if (cmbPortName.Items.Contains(Settings.Default.PortName)) cmbPortName.Text = Settings.Default.PortName;
                else if (cmbPortName.Items.Count > 0) cmbPortName.SelectedIndex = 0;
                else
                {
                    MessageBox.Show(this, "在本机未找到连接仪器的串口。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }

                AllAckBool = Settings.Default.AllAckBool;    // 一直应答
                AllAckStr = Settings.Default.AllAckStr;      // 一直应答字符
                DataAckBool = Settings.Default.DataAckBool;  // 收到数据应答
                DataAckStr = Settings.Default.DataAckStr;    // 收到数据应答字符
                ConditionAckBool = Settings.Default.ConditionAckBool;      // 条件应答
                ConditionAckStr = Settings.Default.ConditionAckStr;        // 条件应答字符
                ReadePortSpaceTime = Settings.Default.ReadePortSpaceTime;  // 串口取数时间间隔

                AllAckChars = GetCharsByString(AllAckStr);
                DataAckChars = GetCharsByString(DataAckStr);

                // 分析条件应答字符定义
                if (ConditionAckBool)
                {
                    string[] ConditionAckStrs = ConditionAckStr.Split(';');
                    listConditionAck = new List<ConditionAck>();

                    for (int i = 0; i < ConditionAckStrs.Length; i++)
                    {
                        string[] Conditions = ConditionAckStrs[i].Split('|');

                        if (Conditions.Length == 2)
                        {
                            ConditionAck cConditionAck = new ConditionAck();
                            cConditionAck.ConditionStr = Convert.ToInt32(Conditions[0]);
                            cConditionAck.ACKStr = GetCharsByString(Conditions[1]);

                            listConditionAck.Add(cConditionAck);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("读取配置时发生错误,请检查配置是否正确后重新启动程序再试,错误消息" + '\n' + e.ToString(), "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary> 根据字符串组返回字符数组
        /// </summary>
        /// <param name="strACK"></param>
        /// <returns></returns>
        private char[] GetCharsByString(string strACK)
        {
            char[] charACK = null;
            try
            {
                if (!string.IsNullOrEmpty(strACK))
                {
                    string[] sACKs = strACK.Split(',');

                    charACK = new char[sACKs.Length];

                    for (int i = 0; i < sACKs.Length; i++)
                    {
                        //int iACK = Convert.ToInt32(sACKs[i]);
                        // 16进制转10进制
                        int iACK = int.Parse(sACKs[i], System.Globalization.NumberStyles.AllowHexSpecifier);

                        char cACK = (char)iACK;

                        charACK[i] = cACK;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("解析应答符时发生错误:" + strACK + ",请检查配置是否正确后重新启动程序再试,错误消息" + '\n' + e.ToString(), "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return charACK;
        }


        /// <summary> 获取当前数据模式 
        /// </summary>
        private DataMode CurrentDataMode
        {
            get
            {
                if (rbHex.Checked) return DataMode.Hex;
                else return DataMode.Text;
            }
            set
            {
                if (value == DataMode.Text) rbText.Checked = true;
                else rbHex.Checked = true;
            }
        }

        /// <summary> 显示日志
        /// </summary>
        /// <param name="msgtype"></param>
        /// <param name="msg"></param>
        private void Log(LogMsgType msgtype, string msg)
        {

            rtfTerminal.Invoke(new EventHandler(delegate
            {
                //清除一定数量的数据
                //让rtfTerminal保持只显示最新的部份数据
                int rtfLinesCount = rtfTerminal.Text.Length;
                const int iShowLinsCount = 30720;
                if (rtfLinesCount > iShowLinsCount)
                {
                    rtfTerminal.Text = rtfTerminal.Text.Remove(0, rtfLinesCount - iShowLinsCount);
                }

                rtfTerminal.SelectedText = string.Empty;
                rtfTerminal.SelectionFont = new Font(rtfTerminal.SelectionFont, FontStyle.Regular);
                rtfTerminal.SelectionColor = LogMsgTypeColor[(int)msgtype];
                rtfTerminal.AppendText(msg);
                rtfTerminal.ScrollToCaret();

            }));
        }

        /// <summary> 转化十六进制
        /// </summary>
        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0').PadRight(3, ' '));
            return sb.ToString().ToUpper();
        }

        /// <summary> 打开/关闭串口
        /// </summary>
        private void OpenClosePort()
        {
            try
            {
                // 如果端口找开则关闭
                if (comport.IsOpen)
                {
                    comport.Close();
                    SetPortStatus("关闭");
                }
                else
                {
                    // 设置端口参数
                    comport.BaudRate = int.Parse(cmbBaudRate.Text);
                    comport.DataBits = int.Parse(cmbDataBits.Text);
                    comport.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cmbStopBits.Text);
                    comport.Parity = (Parity)Enum.Parse(typeof(Parity), cmbParity.Text);
                    comport.PortName = cmbPortName.Text;
                    comport.WriteBufferSize = 2048;
                    comport.ReadBufferSize = 8192 * 2;

                    comport.RtsEnable = RtsEnable;
                    comport.DtrEnable = DtrEnable;
                    comport.DiscardNull = DiscardNull;
                    comport.Handshake = uHandshake;

                    // 打开端口
                    comport.Open();
                    SetPortStatus("就绪");
                }

                // 改变控件状态
                EnableControls();
            }
            catch (Exception ex)
            {
                MessageBox.Show("打开串口时发生错误,程序即将关闭" + '\n' + ex.ToString(), "错误", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                this.Dispose();
                Application.Exit();
            }
        }

        /// <summary> 可用/禁用控件
        /// </summary>
        private void EnableControls()
        {
            gbPortSettings.Enabled = !comport.IsOpen;
            gbMode.Enabled = !comport.IsOpen;

            if (comport.IsOpen)
            {
                labPortStat.Text = "端口已打开";
                panPortStat.BackColor = System.Drawing.Color.Blue;
                btnOpenPort.Text = "关闭端口";
            }
            else
            {
                labPortStat.Text = "端口已关闭";
                panPortStat.BackColor = System.Drawing.Color.Red;
                btnOpenPort.Text = "打开端口";

                // 如果计时器在运行,则关闭它
                if (ReadPortTimer.Enabled)
                {
                    ReadPortTimer.Enabled = false;
                }
            }
        }

        /// <summary>设置为:文本模式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbText_CheckedChanged(object sender, EventArgs e)
        { if (rbText.Checked) CurrentDataMode = DataMode.Text; }

        /// <summary>设置为:十六进制模式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbHex_CheckedChanged(object sender, EventArgs e)
        { if (rbHex.Checked) CurrentDataMode = DataMode.Hex; }

        /// <summary>更新串口当前状态栏
        /// </summary>
        /// <param name="sStatusString">状态信息</param>
        private void SetPortStatus(string sStatusString)
        {
            try
            {
                tsslPortStatus.Text = "串口状态:" + sStatusString;
            }
            catch { }
        }

        /// <summary> 错误声音
        /// </summary>
        private void ErrorSound()
        {
            System.Console.Beep(1400, 800);
        }

        private void btnOpenPort_Click(object sender, EventArgs e)
        {
            OpenClosePort();
        }

        #region 发送消息部份

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(
        int hWnd,  //  handle  to  destination  window  
        int Msg,  //  message  
        int wParam,  //  first  message  parameter  
        ref  COPYDATASTRUCT lParam  //  second  message  parameter  
        );

        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        private static extern int FindWindow(string lpClassName, string lpWindowName);

        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        /// <summary> 发送消息至解码程序
        /// </summary>
        /// <param name="strMSG"></param>
        private void SendMessageToDecode(string strMSG)
        {
            int WINDOW_HANDLER = FindWindow(null, @"仪器数据解码 - " + InstrName);
            if (WINDOW_HANDLER == 0)
            {
            }
            else
            {
                byte[] sarr = System.Text.Encoding.Default.GetBytes(strMSG);
                int len = sarr.Length;

                COPYDATASTRUCT cds;
                cds.dwData = (IntPtr)100;
                cds.lpData = strMSG;
                cds.cbData = len + 1;
                SendMessage(WINDOW_HANDLER, WM_COPYDATA, 0, ref  cds);
            }
        }
        
        /// <summary> 发送消息至监视程序
        /// </summary>
        private void SendMessageToInstrMonitor()
        {
            int WINDOW_HANDLER = FindWindow(null, @"InstrMonitor - " + Settings.Default.InstrName);
            if (WINDOW_HANDLER == 0)
            {
            }
            else
            {
                string strMSG = "InstrMonitorPort" + "|1";
                byte[] sarr = System.Text.Encoding.Default.GetBytes(strMSG);
                int len = sarr.Length;

                COPYDATASTRUCT cds;
                cds.dwData = (IntPtr)100;
                cds.lpData = strMSG;
                cds.cbData = len + 1;
                SendMessage(WINDOW_HANDLER, WM_COPYDATA, 0, ref  cds);
            }
        }

        #endregion

        #region 接收消息

        /// <summary> 重写接收windows消息,接收被管理程序的消息
        /// </summary>
        /// <param name="m"></param>
        protected override void DefWndProc(ref System.Windows.Forms.Message m)
        {
            switch (m.Msg)
            {
                //接收自定义消息 USER，并显示其参数
                case WM_COPYDATA:
                    try
                    {
                        COPYDATASTRUCT mystr = new COPYDATASTRUCT();
                        Type mytype = mystr.GetType();
                        mystr = (COPYDATASTRUCT)m.GetLParam(mytype);

                        //消息 消息类型(解码or驱动)|消息内容

                        switch (mystr.lpData)
                        {
                            case "ShowPortForm":
                                ChangeFormStatus();
                                break;
                        }
                    }
                    catch { }
                    break;
                default:
                    base.DefWndProc(ref m);
                    break;
            }
        }

        #endregion

        #region 托盘图标控制 部份


        private void 开机启动ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetSetupWindowOpenRun(Application.ExecutablePath, "Lis数据接口程序", Settings.Default.InstrName);
        }
        /// <summary>
        /// 将文件放到启动文件夹中开机启动 Windows Script Host Object Model
        /// </summary>
        /// <param name="setupPath">启动程序</param>
        /// <param name="linkname">快捷方式名称</param>
        /// <param name="description">描述</param>
        public void SetSetupWindowOpenRun(string setupPath, string linkname, string description)
        {
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\" + linkname + ".lnk";
            if (System.IO.File.Exists(desktop))
                System.IO.File.Delete(desktop);
            IWshRuntimeLibrary.WshShell shell;
            IWshRuntimeLibrary.IWshShortcut shortcut;
            try
            {
                shell = new IWshRuntimeLibrary.WshShell();
                shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(desktop);
                shortcut.TargetPath = setupPath;//程序路径
                shortcut.Arguments = "";//参数
                shortcut.Description = description;//描述
                shortcut.WorkingDirectory = System.IO.Path.GetDirectoryName(setupPath);//程序所在目录
                shortcut.IconLocation = setupPath;//图标   
                shortcut.WindowStyle = 1;
                shortcut.Save();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "友情提示");
            }
            finally
            {
                shell = null;
                shortcut = null;
            }
        }

        private void 退出ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ComReceive_FormClosing(this, new FormClosingEventArgs(CloseReason.WindowsShutDown, true));
        }

        /// <summary> 关闭窗口时提示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComReceive_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown)
            {
                DialogResult Drs = MessageBox.Show("你真的要退出 " + this.Text + " 程序吗? 退出后将不能接收到仪器数据.\n ", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Drs == DialogResult.Yes)
                {
                    this.Dispose();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                ChangeFormStatus();
                e.Cancel = true;
            }
        }

        private void ComReceive_Shown(object sender, EventArgs e)
        {
            Visible = !Visible;
        }

        private void 显示窗体ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeFormStatus();
        }

        /// <summary>最小化时反转状态显示状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComReceive_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            { ChangeFormStatus(); }
        }

        /// <summary> 反转当前窗口显示状态
        /// </summary>
        private void ChangeFormStatus()
        {
            switch (this.Visible)
            {
                case true:
                    Visible = false;
                    contextMenuStrip1.Items["显示窗体ToolStripMenuItem"].Text = "显示窗体";
                    this.WindowState = FormWindowState.Minimized;
                    break;
                case false:
                    Visible = true;
                    contextMenuStrip1.Items["显示窗体ToolStripMenuItem"].Text = "隐藏窗体";
                    this.WindowState = FormWindowState.Normal;
                    break;
            }

        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            ChangeFormStatus();
        }

        #endregion

        /// <summary> 重存当前窗口的串口数据
        /// </summary>
        private void ReSavePortData()
        {
            int olelen = rtfTerminal.Text.Length;
            int newlen = 0;

            AddBufferData(rtfTerminal.Text);

            newlen = ReaderBufferData().Length;

            if (newlen < olelen)
            {
                MessageBox.Show("存储缓冲数据失败,请重试", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("手动存储缓冲数据成功", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            rtfTerminal.Text = "";
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            pnlPassword.Visible = true;
            maskedTextBox1.Focus();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // 工程师密码
            string PassWort = "inphit";
            if (maskedTextBox1.Text == PassWort)
            {
                pnlPassword.Visible = false;                
                ReSavePortData();
            }
            else
            {
                MessageBox.Show("密码错误", "错误", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            maskedTextBox1.Text = "";
        }

        private void 刷新端口ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cmbPortName.Items.Clear();
            foreach (string s in SerialPort.GetPortNames())
                cmbPortName.Items.Add(s);

            if (cmbPortName.Items.Contains(Settings.Default.PortName)) cmbPortName.Text = Settings.Default.PortName;
            else if (cmbPortName.Items.Count > 0) cmbPortName.SelectedIndex = 0;
            else
            {
                MessageBox.Show(this, "在本机未找到连接仪器的串口。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

    }
}