using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;

namespace SerialPortReceive
{
    class LocalDataBase
    {
        /// <summary>
        /// 应用程序启动路径
        /// </summary>
        string ApplicationStartPath = "";

        private OleDbConnection lconComDataBase = new OleDbConnection();

        public LocalDataBase(string ApplicationPath)
        {
            ApplicationStartPath = ApplicationPath;
            //ConnectionPortDataBase();
        }

        public OleDbConnection conComDataBase
        {
            get
            {
                if (lconComDataBase.State != ConnectionState.Open)
                {
                    ConnectionPortDataBase();                   
                }
                return lconComDataBase;
                
            }
        }

        public void Close()
        {

                if (lconComDataBase.State == ConnectionState.Open)
                {
                    lconComDataBase.Close();
                }

        }

        /// <summary> 连接本地ACCESS数据库
        /// </summary>
        private void ConnectionPortDataBase()
        {
            try
            {
                if (lconComDataBase.State != ConnectionState.Open)
                {                    
                    //string strDBPath = System.IO.Path.GetFullPath("ComData.mdb");
                    string strDBPath = ApplicationStartPath + @"\ComData.mdb";
                    lconComDataBase.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Password=;Data Source=" + strDBPath + ";Persist Security Info=True";
                    lconComDataBase.Open();
                }
            }
            catch
            {
            }
        }
    }
}
