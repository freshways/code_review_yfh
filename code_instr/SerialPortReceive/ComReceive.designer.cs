﻿namespace SerialPortReceive
{
    partial class ComReceive
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComReceive));
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新端口ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stspStatus = new System.Windows.Forms.StatusStrip();
            this.tsslPortStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslReceiveDataLen = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslSaveDataLen = new System.Windows.Forms.ToolStripStatusLabel();
            this.tllsVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示窗体ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.开机启动ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.ReadPortTimer = new System.Windows.Forms.Timer(this.components);
            this.tabpPort = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlPassword = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.rtfTerminal = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panPortStat = new System.Windows.Forms.Panel();
            this.labPortStat = new System.Windows.Forms.Label();
            this.btnOpenPort = new System.Windows.Forms.Button();
            this.gbPortSettings = new System.Windows.Forms.GroupBox();
            this.lblComPort = new System.Windows.Forms.Label();
            this.cmbPortName = new System.Windows.Forms.ComboBox();
            this.lblStopBits = new System.Windows.Forms.Label();
            this.cmbBaudRate = new System.Windows.Forms.ComboBox();
            this.cmbStopBits = new System.Windows.Forms.ComboBox();
            this.lblBaudRate = new System.Windows.Forms.Label();
            this.lblDataBits = new System.Windows.Forms.Label();
            this.cmbParity = new System.Windows.Forms.ComboBox();
            this.cmbDataBits = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbMode = new System.Windows.Forms.GroupBox();
            this.rbText = new System.Windows.Forms.RadioButton();
            this.rbHex = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.contextMenuStrip2.SuspendLayout();
            this.stspStatus.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabpPort.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlPassword.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panPortStat.SuspendLayout();
            this.gbPortSettings.SuspendLayout();
            this.gbMode.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.AccessibleDescription = "";
            this.contextMenuStrip2.AccessibleName = "";
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.toolStripMenuItem2,
            this.刷新端口ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(191, 82);
            this.contextMenuStrip2.Tag = "";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(190, 24);
            this.toolStripMenuItem1.Text = "清空窗口数据";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(187, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(190, 24);
            this.toolStripMenuItem2.Text = "重新存储窗口数据";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // 刷新端口ToolStripMenuItem
            // 
            this.刷新端口ToolStripMenuItem.Name = "刷新端口ToolStripMenuItem";
            this.刷新端口ToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.刷新端口ToolStripMenuItem.Text = "刷新端口";
            this.刷新端口ToolStripMenuItem.Click += new System.EventHandler(this.刷新端口ToolStripMenuItem_Click);
            // 
            // stspStatus
            // 
            this.stspStatus.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.stspStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslPortStatus,
            this.tsslReceiveDataLen,
            this.tsslSaveDataLen,
            this.tllsVersion});
            this.stspStatus.Location = new System.Drawing.Point(0, 386);
            this.stspStatus.Name = "stspStatus";
            this.stspStatus.Size = new System.Drawing.Size(725, 29);
            this.stspStatus.TabIndex = 3;
            this.stspStatus.Text = "statusStrip1";
            // 
            // tsslPortStatus
            // 
            this.tsslPortStatus.Name = "tsslPortStatus";
            this.tsslPortStatus.Size = new System.Drawing.Size(498, 24);
            this.tsslPortStatus.Spring = true;
            this.tsslPortStatus.Text = "串口状态         ";
            this.tsslPortStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsslReceiveDataLen
            // 
            this.tsslReceiveDataLen.Name = "tsslReceiveDataLen";
            this.tsslReceiveDataLen.Size = new System.Drawing.Size(79, 24);
            this.tsslReceiveDataLen.Text = "已接收数据";
            // 
            // tsslSaveDataLen
            // 
            this.tsslSaveDataLen.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.tsslSaveDataLen.Name = "tsslSaveDataLen";
            this.tsslSaveDataLen.Size = new System.Drawing.Size(83, 24);
            this.tsslSaveDataLen.Text = "已存储数据";
            // 
            // tllsVersion
            // 
            this.tllsVersion.AutoSize = false;
            this.tllsVersion.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.tllsVersion.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.tllsVersion.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tllsVersion.Name = "tllsVersion";
            this.tllsVersion.Size = new System.Drawing.Size(50, 24);
            this.tllsVersion.Text = "V1.1.0";
            this.tllsVersion.ToolTipText = "已处理样品";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.AccessibleDescription = "";
            this.contextMenuStrip1.AccessibleName = "";
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示窗体ToolStripMenuItem,
            this.toolStripSeparator2,
            this.开机启动ToolStripMenuItem,
            this.退出ToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(135, 82);
            this.contextMenuStrip1.Tag = "";
            // 
            // 显示窗体ToolStripMenuItem
            // 
            this.显示窗体ToolStripMenuItem.Name = "显示窗体ToolStripMenuItem";
            this.显示窗体ToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
            this.显示窗体ToolStripMenuItem.Text = "显示窗口";
            this.显示窗体ToolStripMenuItem.Click += new System.EventHandler(this.显示窗体ToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(131, 6);
            // 
            // 开机启动ToolStripMenuItem
            // 
            this.开机启动ToolStripMenuItem.Name = "开机启动ToolStripMenuItem";
            this.开机启动ToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
            this.开机启动ToolStripMenuItem.Text = "开机启动";
            this.开机启动ToolStripMenuItem.Click += new System.EventHandler(this.开机启动ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem1
            // 
            this.退出ToolStripMenuItem1.Name = "退出ToolStripMenuItem1";
            this.退出ToolStripMenuItem1.Size = new System.Drawing.Size(134, 24);
            this.退出ToolStripMenuItem1.Text = "退出";
            this.退出ToolStripMenuItem1.Click += new System.EventHandler(this.退出ToolStripMenuItem1_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "数据接收";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // tabpPort
            // 
            this.tabpPort.Controls.Add(this.panel3);
            this.tabpPort.Controls.Add(this.panel2);
            this.tabpPort.Location = new System.Drawing.Point(4, 22);
            this.tabpPort.Name = "tabpPort";
            this.tabpPort.Padding = new System.Windows.Forms.Padding(3);
            this.tabpPort.Size = new System.Drawing.Size(717, 360);
            this.tabpPort.TabIndex = 0;
            this.tabpPort.Text = "串口通信日志";
            this.tabpPort.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pnlPassword);
            this.panel3.Controls.Add(this.rtfTerminal);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(711, 282);
            this.panel3.TabIndex = 13;
            // 
            // pnlPassword
            // 
            this.pnlPassword.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPassword.Controls.Add(this.button4);
            this.pnlPassword.Controls.Add(this.maskedTextBox1);
            this.pnlPassword.Controls.Add(this.label2);
            this.pnlPassword.Controls.Add(this.button3);
            this.pnlPassword.Location = new System.Drawing.Point(264, 101);
            this.pnlPassword.Name = "pnlPassword";
            this.pnlPassword.Size = new System.Drawing.Size(167, 89);
            this.pnlPassword.TabIndex = 21;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(79, 48);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(39, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "关闭";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(18, 21);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.PasswordChar = '*';
            this.maskedTextBox1.Size = new System.Drawing.Size(100, 21);
            this.maskedTextBox1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "请输入工程师密码";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(18, 48);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(55, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "确定";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // rtfTerminal
            // 
            this.rtfTerminal.ContextMenuStrip = this.contextMenuStrip2;
            this.rtfTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtfTerminal.Location = new System.Drawing.Point(0, 0);
            this.rtfTerminal.MaxLength = 1024;
            this.rtfTerminal.Name = "rtfTerminal";
            this.rtfTerminal.Size = new System.Drawing.Size(711, 282);
            this.rtfTerminal.TabIndex = 9;
            this.rtfTerminal.Text = "";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panPortStat);
            this.panel2.Controls.Add(this.gbPortSettings);
            this.panel2.Controls.Add(this.gbMode);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(3, 285);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(711, 72);
            this.panel2.TabIndex = 12;
            // 
            // panPortStat
            // 
            this.panPortStat.BackColor = System.Drawing.Color.Red;
            this.panPortStat.Controls.Add(this.labPortStat);
            this.panPortStat.Controls.Add(this.btnOpenPort);
            this.panPortStat.Location = new System.Drawing.Point(577, 11);
            this.panPortStat.Name = "panPortStat";
            this.panPortStat.Size = new System.Drawing.Size(108, 57);
            this.panPortStat.TabIndex = 14;
            // 
            // labPortStat
            // 
            this.labPortStat.AutoSize = true;
            this.labPortStat.ForeColor = System.Drawing.SystemColors.Info;
            this.labPortStat.Location = new System.Drawing.Point(22, 7);
            this.labPortStat.Name = "labPortStat";
            this.labPortStat.Size = new System.Drawing.Size(65, 12);
            this.labPortStat.TabIndex = 14;
            this.labPortStat.Text = "端口已关闭";
            // 
            // btnOpenPort
            // 
            this.btnOpenPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenPort.BackColor = System.Drawing.SystemColors.Control;
            this.btnOpenPort.Location = new System.Drawing.Point(21, 25);
            this.btnOpenPort.Name = "btnOpenPort";
            this.btnOpenPort.Size = new System.Drawing.Size(66, 26);
            this.btnOpenPort.TabIndex = 13;
            this.btnOpenPort.Text = "打开端口";
            this.btnOpenPort.UseVisualStyleBackColor = false;
            this.btnOpenPort.Click += new System.EventHandler(this.btnOpenPort_Click);
            // 
            // gbPortSettings
            // 
            this.gbPortSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbPortSettings.Controls.Add(this.lblComPort);
            this.gbPortSettings.Controls.Add(this.cmbPortName);
            this.gbPortSettings.Controls.Add(this.lblStopBits);
            this.gbPortSettings.Controls.Add(this.cmbBaudRate);
            this.gbPortSettings.Controls.Add(this.cmbStopBits);
            this.gbPortSettings.Controls.Add(this.lblBaudRate);
            this.gbPortSettings.Controls.Add(this.lblDataBits);
            this.gbPortSettings.Controls.Add(this.cmbParity);
            this.gbPortSettings.Controls.Add(this.cmbDataBits);
            this.gbPortSettings.Controls.Add(this.label1);
            this.gbPortSettings.Location = new System.Drawing.Point(4, 5);
            this.gbPortSettings.Name = "gbPortSettings";
            this.gbPortSettings.Size = new System.Drawing.Size(452, 63);
            this.gbPortSettings.TabIndex = 11;
            this.gbPortSettings.TabStop = false;
            this.gbPortSettings.Text = "端口设置(&S)";
            // 
            // lblComPort
            // 
            this.lblComPort.AutoSize = true;
            this.lblComPort.Location = new System.Drawing.Point(12, 18);
            this.lblComPort.Name = "lblComPort";
            this.lblComPort.Size = new System.Drawing.Size(47, 12);
            this.lblComPort.TabIndex = 0;
            this.lblComPort.Text = "串口号:";
            // 
            // cmbPortName
            // 
            this.cmbPortName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPortName.FormattingEnabled = true;
            this.cmbPortName.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6"});
            this.cmbPortName.Location = new System.Drawing.Point(13, 35);
            this.cmbPortName.MinimumSize = new System.Drawing.Size(80, 0);
            this.cmbPortName.Name = "cmbPortName";
            this.cmbPortName.Size = new System.Drawing.Size(80, 20);
            this.cmbPortName.TabIndex = 1;
            // 
            // lblStopBits
            // 
            this.lblStopBits.AutoSize = true;
            this.lblStopBits.Location = new System.Drawing.Point(367, 18);
            this.lblStopBits.Name = "lblStopBits";
            this.lblStopBits.Size = new System.Drawing.Size(47, 12);
            this.lblStopBits.TabIndex = 8;
            this.lblStopBits.Text = "停止位:";
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.FormattingEnabled = true;
            this.cmbBaudRate.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "36000",
            "115000"});
            this.cmbBaudRate.Location = new System.Drawing.Point(101, 35);
            this.cmbBaudRate.MinimumSize = new System.Drawing.Size(80, 0);
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Size = new System.Drawing.Size(80, 20);
            this.cmbBaudRate.TabIndex = 3;
            // 
            // cmbStopBits
            // 
            this.cmbStopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStopBits.FormattingEnabled = true;
            this.cmbStopBits.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cmbStopBits.Location = new System.Drawing.Point(366, 35);
            this.cmbStopBits.MinimumSize = new System.Drawing.Size(80, 0);
            this.cmbStopBits.Name = "cmbStopBits";
            this.cmbStopBits.Size = new System.Drawing.Size(80, 20);
            this.cmbStopBits.TabIndex = 9;
            // 
            // lblBaudRate
            // 
            this.lblBaudRate.AutoSize = true;
            this.lblBaudRate.Location = new System.Drawing.Point(101, 18);
            this.lblBaudRate.Name = "lblBaudRate";
            this.lblBaudRate.Size = new System.Drawing.Size(47, 12);
            this.lblBaudRate.TabIndex = 2;
            this.lblBaudRate.Text = "波特率:";
            // 
            // lblDataBits
            // 
            this.lblDataBits.AutoSize = true;
            this.lblDataBits.Location = new System.Drawing.Point(276, 18);
            this.lblDataBits.Name = "lblDataBits";
            this.lblDataBits.Size = new System.Drawing.Size(47, 12);
            this.lblDataBits.TabIndex = 6;
            this.lblDataBits.Text = "数据位:";
            // 
            // cmbParity
            // 
            this.cmbParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParity.FormattingEnabled = true;
            this.cmbParity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd"});
            this.cmbParity.Location = new System.Drawing.Point(189, 35);
            this.cmbParity.MinimumSize = new System.Drawing.Size(80, 0);
            this.cmbParity.Name = "cmbParity";
            this.cmbParity.Size = new System.Drawing.Size(80, 20);
            this.cmbParity.TabIndex = 5;
            // 
            // cmbDataBits
            // 
            this.cmbDataBits.FormattingEnabled = true;
            this.cmbDataBits.Items.AddRange(new object[] {
            "7",
            "8",
            "9"});
            this.cmbDataBits.Location = new System.Drawing.Point(276, 35);
            this.cmbDataBits.MinimumSize = new System.Drawing.Size(80, 0);
            this.cmbDataBits.Name = "cmbDataBits";
            this.cmbDataBits.Size = new System.Drawing.Size(80, 20);
            this.cmbDataBits.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "校验位:";
            // 
            // gbMode
            // 
            this.gbMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbMode.Controls.Add(this.rbText);
            this.gbMode.Controls.Add(this.rbHex);
            this.gbMode.Location = new System.Drawing.Point(462, 5);
            this.gbMode.Name = "gbMode";
            this.gbMode.Size = new System.Drawing.Size(109, 63);
            this.gbMode.TabIndex = 12;
            this.gbMode.TabStop = false;
            this.gbMode.Text = "数据模式(&M)";
            // 
            // rbText
            // 
            this.rbText.AutoSize = true;
            this.rbText.Checked = true;
            this.rbText.Location = new System.Drawing.Point(12, 20);
            this.rbText.Name = "rbText";
            this.rbText.Size = new System.Drawing.Size(47, 16);
            this.rbText.TabIndex = 0;
            this.rbText.TabStop = true;
            this.rbText.Text = "文本";
            this.rbText.CheckedChanged += new System.EventHandler(this.rbText_CheckedChanged);
            // 
            // rbHex
            // 
            this.rbHex.AutoSize = true;
            this.rbHex.Location = new System.Drawing.Point(12, 36);
            this.rbHex.Name = "rbHex";
            this.rbHex.Size = new System.Drawing.Size(71, 16);
            this.rbHex.TabIndex = 1;
            this.rbHex.Text = "十六进制";
            this.rbHex.CheckedChanged += new System.EventHandler(this.rbHex_CheckedChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabpPort);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(725, 386);
            this.tabControl1.TabIndex = 4;
            // 
            // ComReceive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 415);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.stspStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ComReceive";
            this.Text = "InstrMonitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ComReceive_FormClosing);
            this.Load += new System.EventHandler(this.ComReceive_Load);
            this.Shown += new System.EventHandler(this.ComReceive_Shown);
            this.Resize += new System.EventHandler(this.ComReceive_Resize);
            this.contextMenuStrip2.ResumeLayout(false);
            this.stspStatus.ResumeLayout(false);
            this.stspStatus.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabpPort.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pnlPassword.ResumeLayout(false);
            this.pnlPassword.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panPortStat.ResumeLayout(false);
            this.panPortStat.PerformLayout();
            this.gbPortSettings.ResumeLayout(false);
            this.gbPortSettings.PerformLayout();
            this.gbMode.ResumeLayout(false);
            this.gbMode.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip stspStatus;
        private System.Windows.Forms.ToolStripStatusLabel tsslPortStatus;        
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 显示窗体ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripStatusLabel tsslReceiveDataLen;
        private System.Windows.Forms.Timer ReadPortTimer;
        private System.Windows.Forms.ToolStripStatusLabel tsslSaveDataLen;
        private System.Windows.Forms.ToolStripStatusLabel tllsVersion;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 刷新端口ToolStripMenuItem;
        private System.Windows.Forms.TabPage tabpPort;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pnlPassword;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RichTextBox rtfTerminal;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panPortStat;
        private System.Windows.Forms.Label labPortStat;
        private System.Windows.Forms.Button btnOpenPort;
        private System.Windows.Forms.GroupBox gbPortSettings;
        private System.Windows.Forms.Label lblComPort;
        private System.Windows.Forms.ComboBox cmbPortName;
        private System.Windows.Forms.Label lblStopBits;
        private System.Windows.Forms.ComboBox cmbBaudRate;
        private System.Windows.Forms.ComboBox cmbStopBits;
        private System.Windows.Forms.Label lblBaudRate;
        private System.Windows.Forms.Label lblDataBits;
        private System.Windows.Forms.ComboBox cmbParity;
        private System.Windows.Forms.ComboBox cmbDataBits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbMode;
        private System.Windows.Forms.RadioButton rbText;
        private System.Windows.Forms.RadioButton rbHex;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStripMenuItem 开机启动ToolStripMenuItem;
    }
}

