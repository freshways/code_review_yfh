using System;
using System.Collections.Generic;
using System.Text;

namespace DecodeInterface
{
    /// <summary> 项目结果类
    /// </summary>
    public  class ItemResult
    {
        private  string _ItemID;
        private  string _ItemValue;

        /// <summary> 项目代号
        /// </summary>
        public  string ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }

        /// <summary> 结果
        /// </summary>
        public  string ItemValue
        {
            get { return _ItemValue; }
            set { _ItemValue = value; }
        }
    }
}
