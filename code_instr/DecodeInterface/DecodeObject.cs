using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

// 本文件包含两个类
// 一、接口
// 二、解码基类:实现接口

namespace DecodeInterface
{
    /// <summary>  解码结束代理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="Status"></param>
    /// <param name="ex"></param>
    /// <param name="strType"></param>
    /// <param name="strSampleNo"></param>
    /// <param name="strDate"></param>
    /// <param name="ItemResults"></param>
    /// <param name="ItemImgResults"></param>
    /// <param name="strSampleData"></param>
    /// <param name="Heat"></param>
    /// <param name="XYDepth"></param>
    /// <param name="Haemoglobin"></param>
    public delegate void DecodeEndHandler(object sender, int Status, Exception ex, string strType, string strSampleNo, string strDate, List<ItemResult> ItemResults, List<ItemImgResult> ItemImgResults, string strSampleData, float Heat, float XYDepth, float Haemoglobin);

    /// <summary> 解码接口
    /// </summary>
    public interface IDDecodeObject
    {
        /// <summary> 数据解码
        /// </summary>
        /// <param name="strComData"></param>
        /// <returns></returns>
        void DataDecode(string strComData);

        /// <summary>一个样品解码完成事件   
        /// </summary>
        event DecodeEndHandler oneDecodeEnd;
    }

    /// <summary> 解码基类
    /// </summary>
    public abstract class DecodeBase : IDDecodeObject
    {
        /// <summary>解码缓冲数据 
        /// </summary>
        public string strBufferComData;
        private StringBuilder sbBufferComData = new StringBuilder();  //解码缓冲数据
        /// <summary>一个样品解码完成事件   
        /// </summary>
        public event DecodeEndHandler oneDecodeEnd;

        /// <summary> 启动数据解码 抽象方法 </summary>
        /// <param name="strComData"></param>
        public abstract void DataDecode(string strComData);

        /// <summary> 仪器数据解码主体方法 抽象方法
        /// 各仪器解码程序都要重写它来实现真正的解码
        /// 重写时将解码结果赋值给相应的out参数即可
        /// 解码成功 Status=1
        /// 解码失败 Status=-1
        /// </summary>
        /// <param name="strSampleData">单一样品待解码数据</param>
        /// <param name="Status">out:解码状态</param>
        /// <param name="strType">out:结果类型</param>
        /// <param name="ex">out:错误对象</param>
        /// <param name="strSampleNo">out:样品号</param>
        /// <param name="strDate">out:样品日期</param>
        /// <param name="ItemResults">out:解码项目结果</param>
        /// <param name="ItemImgResults">out:解码图形结果</param>
        /// <param name="Heat">体温</param>
        /// <param name="XYDepth">吸氧浓度</param>
        /// <param name="Haemoglobin">血红蛋白</param>
        /// <param name="strOtherSampleData">剩余未解码数据</param>
        public abstract void InstrDataDecode(string strSampleData, out int Status, out  string strType, out  Exception ex, out string strSampleNo, out  string strDate, out  List<ItemResult> ItemResults, out List<ItemImgResult> ItemImgResults, out float Heat, out float XYDepth, out float Haemoglobin, out string strOtherSampleData);


        /// <summary> 检查指定字符/字符串在整个字符串中的位置 </summary>
        /// <param name="TxtBody">待查找字符串</param>
        /// <param name="objTxt">查找关键字符</param>
        /// <returns></returns>
        private int PointIndexof(string TxtBody, object objTxt)
        {
            // 小便数据不能减一
            //return PointIndexof(TxtBody, objTxt, 0, TxtBody.Length - 1);

            return PointIndexof(TxtBody, objTxt, 0, TxtBody.Length);
        }
        
        /// <summary> 检查指定字符/字符串在整个字符串中的位置 </summary>
        /// <param name="TxtBody">待查找字符串</param>
        /// <param name="objTxt">查找关键字符</param>
        /// <param name="StartIndex">开始查找位置</param>
        /// <returns></returns>
        private int PointIndexof(string TxtBody, object objTxt, int StartIndex)
        {
            //小便不能减一
            //return PointIndexof(TxtBody, objTxt, StartIndex, TxtBody.Length - StartIndex - 1);
            return PointIndexof(TxtBody, objTxt, StartIndex, TxtBody.Length - StartIndex);
        }

        /// <summary> 检查指定字符/字符串在整个字符串中的位置 </summary>
        /// <param name="TxtBody">待查找字符串</param>
        /// <param name="objTxt">查找关键字符</param>
        /// <param name="StartIndex">开始查找位置</param>
        /// <param name="Count">查找字符数量</param>
        /// <returns></returns>
        private int PointIndexof(string TxtBody, object objTxt, int StartIndex, int Count)
        {            
            int Point = -1;

            try
            {
                string objType = objTxt.GetType().Name;
                switch (objType)
                {
                    case "String":
                        string strTxt = Convert.ToString(objTxt);
                        Point = TxtBody.IndexOf(strTxt, StartIndex, Count);
                        break;

                    case "Char":
                        char charTxt = Convert.ToChar(objTxt);
                        Point = TxtBody.IndexOf(charTxt, StartIndex, Count);
                        break;
                    case "Object[]":

                        object[] identifier = objTxt as object[];
                        int iLen = identifier.Length;
                        int iLenOk;

                        #region 循环主体字符每一位
                        // 从开始位找
                        // 找开始位起，查找开始位+Count的长度
                        // 因为有时候，主体数据长度-开始位,没有结束符长(kx-21n)
                        // 9:29 2008-11-15  如果数据已完全收到本地再解码,则不会出此问题,因为能找到结束位,但在联机时,先发了部份过来,就找结束位,就遇到主体找度还没结束符长的问题了
                        //for (int j = StartIndex; j < TxtBody.Length; j++)
                        for (int j = StartIndex; j < StartIndex + Count; j++)
                        {
                            //换到for上
                            // 判断原始数据,如果没有待比较字长则直接退出
                            //if (TxtBody.Length - StartIndex < iLen)
                            //{
                            //    break;
                            //}

                            // 循环关键符每一位,累加和主体符相同位置比较
                            iLenOk = 0;
                            for (int i = 0; i < iLen; i++)
                            {
                                object objIdent = identifier[i];
                                if (objIdent != null)
                                {
                                    char currIdent = Convert.ToChar(objIdent);                                    
                                    char currBody = Convert.ToChar(TxtBody[j + i]);

                                    if (currBody == currIdent)
                                    {
                                        // 记录已匹配到第几位
                                        iLenOk = i + 1;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // 关键字与主体字匹配长度 = 关键字长则 则认为找到关键字位
                            if (iLen == iLenOk)
                            {
                                Point = j;
                                break;
                            }
                        }
                        #endregion

                        break;
                    default:
                        // 未知类型
                        Point = -2;
                        break;
                }
            }
            catch 
            {
                Point = -3;
            }

            return Point;
        }

        /// <summary> 获取对象长度
        /// </summary>
        /// <param name="objTxt"></param>
        /// <returns></returns>
        private int GetObjLength(object objTxt)
        {
            int iLen = -1;
            string objType = objTxt.GetType().Name;

            switch (objType)
            {
                case "String":
                    string strTxt = Convert.ToString(objTxt);
                    iLen = strTxt.Length;
                    break;

                case "Char":
                    char charTxt = Convert.ToChar(objTxt);
                    iLen = 1;
                    break;
                case "Object[]":
                    object[] identifier = objTxt as object[];
                    iLen = identifier.Length;
                    break;
            }

            return iLen;
        }

        /// <summary> 数据解码公共方法 </summary>
        /// 各仪器解码程序首先都要调用这个方法
        /// 该方法对数据进行完整性分析后,再调用仪器的真正解码方法
        /// 默认:当数据包含多个开始符时,丢弃前面的开始符,仅保留与结束符最近的一个开始符
        /// 默认:不处理样品最小长度
        /// <param name="objstx">开始位标识,字符串或Char(用强类型)</param>
        /// <param name="objetx">结束位标识,字符串或Char(用强类型)</param>
        /// <param name="etxExLen">结束位后还有多少字符是本样品的,可为0</param>
        /// <param name="strComData">待解码样品数据</param>        
        public void DataDecodeComm(object objstx, object objetx, int etxExLen, string strComData)
        {
            DataDecodeComm(objstx, objetx, etxExLen, strComData, false, 0);
        }

        /// <summary> 数据解码公共方法 </summary>
        /// 各仪器解码程序首先都要调用这个方法
        /// 该方法对数据进行完整性分析后,再调用仪器的真正解码方法
        /// 默认:当数据包含多个开始符时,丢弃前面的开始符,仅保留与结束符最近的一个开始符
        /// <param name="objstx">开始位标识,字符串或Char(用强类型)</param>
        /// <param name="objetx">结束位标识,字符串或Char(用强类型)</param>
        /// <param name="etxExLen">结束位后还有多少字符是本样品的,可为0</param>
        /// <param name="strComData">待解码样品数据</param>
        /// <param name="iStdLen">每个样品数据最小长度,如果小于这个度度即便找到结束符也认为不对</param>
        public void DataDecodeComm(object objstx, object objetx, int etxExLen, string strComData, int iStdLen)
        {
            DataDecodeComm(objstx, objetx, etxExLen, strComData, false, iStdLen);
        }

        /// <summary> 数据解码公共方法 </summary>
        /// 各仪器解码程序首先都要调用这个方法
        /// 该方法对数据进行完整性分析后,再调用仪器的真正解码方法
        /// <param name="objstx">开始位标识,字符串或Char(用强类型)</param>
        /// <param name="objetx">结束位标识,字符串或Char(用强类型)</param>
        /// <param name="etxExLen">结束位后还有多少字符是本样品的,可为0</param>
        /// <param name="strComData">待解码样品数据</param>
        /// <param name="bolSexRepteatable">当数据包含多个开始符时,以第一次找到的开始符位置为准</param>
        /// <param name="iStdLen">每个样品数据最小长度,如果小于这个度度即便找到结束符也认为不对</param>
        public void DataDecodeComm(object objstx, object objetx, int etxExLen, string strComData,bool bolSexRepteatable,int iStdLen)
        {
            int iStartPosition;
            int iTowStartPosition;
            int iEndPosition = -1;
            string strSampleData;
            int iSampleEtxLen;
            int iSampleDataLen;

            if (strComData != "")
            {
                sbBufferComData.Append(strComData);
            }

            strBufferComData = sbBufferComData.ToString();
            //strBufferComData += strComData;

            if (strBufferComData == "") { return; }

            try
            {
                #region 分析开始结束符长度

                int iStxLen = GetObjLength(objstx);
                int iEtxLen = GetObjLength(objetx);

                #endregion

                #region 检验开始结束符,确定位置

                // 寻找第一个开始符位置
                iStartPosition = PointIndexof(strBufferComData, objstx);

                if (iStartPosition > -1)
                {
                    // 从第一个开始符起,录找第一个结束符位置
                    iEndPosition = PointIndexof(strBufferComData, objetx, iStartPosition);

                    #region  判断已找到的样品数据长度是否符合要求

                    if (iStdLen > 0)
                    {
                        int iTowEndPosition;
                        int iSampleLen = iEndPosition - iStartPosition;

                        while ((iSampleLen < iStdLen) && (iEndPosition != -1))
                        {
                            // 从上一结束点后开始再找结束点,直到结束点-开始点不小于标准长度
                            // 如果没找到结束点,则直接退出,结束点会为-1
                            // 解决4500有时候会出现一个在中间的假结束点                            
                            iTowEndPosition = PointIndexof(strBufferComData, objetx, iEndPosition + 1);
                            iSampleLen = iTowEndPosition - iStartPosition;
                            iEndPosition = iTowEndPosition;

                            //换到while上
                            //if (iTowEndPosition < 0)
                            //{
                            //    break;
                            //}
                        }
                    }

                    #endregion

                    #region 查找第一个开始点及结束点间的第二个开始点
                      
                    // 处理类如 start....start.....end 格式的错误数据
                    if ((iEndPosition > -1) && (!bolSexRepteatable))
                    {
                        iTowStartPosition = iStartPosition;
                        while (iTowStartPosition != -1)
                        {
                            // 从上一开始点后,直到结束点,查找有无开始点,如有则认为第一个开始点的数据不完整
                            // 弃之,使用第二个开始点                            
                            iTowStartPosition = PointIndexof(strBufferComData, objstx, iStartPosition + 1, iEndPosition - 1 - iStartPosition);
                            if (iTowStartPosition > -1)
                            {
                                iStartPosition = iTowStartPosition;
                            }
                        }
                    }
                    #endregion
                }

                #endregion

                #region 验证开始结束符的正确性

                if ((iStartPosition == -1 || iEndPosition == -1))
                {
                    //未找到开始符及结束符则返回,未处理的数据保留在strBufferComData中
                    BuffDetaNot();
                    return;
                }
                if ((iStartPosition == -2) || (iEndPosition == -2))
                {   //传入开始结束符类型错误
                    BuffDetaNot();
                    return;
                }
                if ((iStartPosition == -3) || (iEndPosition == -3))
                {   //查找开始结束符时出错
                    Exception e = new Exception("查找开始结束符期间发生错误");
                    DecodeException(e, strBufferComData);
                    return;
                }

                #endregion                

                #region 根据开始结束符位置,确定样品数据,并调用解码

                if ((iStartPosition > -1) && (iEndPosition > iStartPosition))
                {
                    int strBufferComDataLen = strBufferComData.Length;
                    iSampleEtxLen = iEtxLen + etxExLen;  //从结束点后取多少位仍算当前样品数据

                    if (strBufferComDataLen < iStxLen + iEtxLen + iSampleEtxLen) { return; } //原始数据不完整
                    
                    int icSampleDataLen = iEndPosition + iSampleEtxLen - iStartPosition;

                    if (icSampleDataLen + iStartPosition > strBufferComDataLen)
                    {
                        // 有的时候设置结束点后有内容,但有时会无内容了,如果还加就出错了
                        icSampleDataLen = strBufferComDataLen - iStartPosition;
                    }

                    strSampleData = strBufferComData.Substring(iStartPosition, icSampleDataLen);
                    iSampleDataLen = strSampleData.Length;
                    //调用仪器数据解码
                    int Status;
                    string strType, strSampleNo, strDate;
                    float fltHeat, fltXYDepth, fltHaemoglobin;

                    List<ItemResult> ItemResults;
                    List<ItemImgResult> ItemImgResults;
                    System.Exception ex = new Exception();

                    //某些时候,一个开始结束符内有两个样品的结果,但可使用其它标识区分开来
                    //所以增加个返回参数,由内部解码程序将不是同一标本的数据传回来并再次解码
                    string strOtherSampleData;
                    InstrDataDecode(strSampleData, out Status, out strType, out ex, out strSampleNo, out strDate, out ItemResults, out ItemImgResults, out fltHeat, out fltXYDepth, out fltHaemoglobin, out strOtherSampleData);
                    
                    if (Status == 1)
                    {
                        try
                        {
                            // 解码成功,触发解码结束事件
                            ONoneDecodeEnd(Status, strType, ex, strSampleNo, strDate, ItemResults, ItemImgResults, strSampleData, fltHeat, fltXYDepth, fltHaemoglobin);
                        }
                        catch { }
                    }
                    else if (Status == 0)
                    {
                        // 虽然有开始符,但解码时无可解码数据
                        BuffDetaNot();
                    }
                    else
                    {
                        // 解码失败
                        DecodeException(ex, strBufferComData);
                    }

                    // 删除已成功解码数据,失败的也删除
                    sbBufferComData.Remove(0, icSampleDataLen + iStartPosition);                    //2012-10-19 应该是移除起始位+长度,以前只移了长度,导致如果开始位与字符开始位离得过长,会被重复解码
                    //sbBufferComData.Remove(0, icSampleDataLen );                    //2012-10-19 应该是移除起始位+长度,以前只移了长度,导致如果开始位与字符开始位离得过长,会被重复解码
                    if (!string.IsNullOrEmpty(strOtherSampleData))
                    {
                        sbBufferComData.Append(strOtherSampleData);
                    }

                    //缓冲数据剩余未解码部份
                    if (sbBufferComData.Length > 0)
                    {
                        //如果还有数据未解析，则再次调用数据解码
                        DataDecodeComm(objstx, objetx, etxExLen, "", bolSexRepteatable, iStdLen);
                    }
                }
                else
                {
                    //查找开始结束符时出错
                    Exception e = new Exception("开始结束符有误,开始符位置: " + iStartPosition.ToString() + "结束符位置:" + iEndPosition.ToString());
                    DecodeException(e, strBufferComData);
                    return;
                }

                #endregion
            }
            catch (Exception ex)
            {
                DecodeException(ex, strBufferComData);
                sbBufferComData.Remove(0, sbBufferComData.Length); //2010-03-31 zmjin clear 否则如果数据错误发生异常,会导致之后传来的数据也不能解码
            }
        }

        /// <summary>解码结束事件
        /// </summary>
        public void ONoneDecodeEnd(int Status, string strType, Exception ex, string strSampleNo, string strDate, List<ItemResult> ItemResults, List<ItemImgResult> ItemImgResults, string strSampleData, float Heat, float XYDepth, float Haemoglobin)
        {
            try
            {
                if (oneDecodeEnd != null)
                {
                    oneDecodeEnd(this, Status, ex, strType, strSampleNo, strDate, ItemResults, ItemImgResults, strSampleData, Heat, XYDepth, Haemoglobin);
                }
            }
            catch { }
        }

        /// <summary> 缓冲无数据,返回无数据提示
        /// </summary>
        public void BuffDetaNot()
        {
            ONoneDecodeEnd(0, "", null, "", "", null, null, strBufferComData, 0, 0, 0);
        }

        /// <summary> 解码异常
        /// </summary>
        public void DecodeException(Exception ex, string strBufferComData)
        {
            //解码失败            
            ONoneDecodeEnd(-1, "", ex, "", "", null, null, strBufferComData, 0, 0, 0);
            strBufferComData = "";
        }

        /// <summary>读取XML节点值
        /// </summary>
        /// <param name="xmlPath"></param>
        /// <param name="xmlRootName"></param>
        /// <param name="xmlnode"></param>
        /// <returns></returns>
        public string GetXmlNodeValue(string xmlPath, string xmlRootName, string xmlnode)
        {
            string retstr = "";
            try
            {
                //如果传入的文件名中不包含路径,则增加
                string CurrentDirectory = Environment.CurrentDirectory;
                if (xmlPath.IndexOf(CurrentDirectory) < 0)
                {
                    xmlPath = CurrentDirectory + @"\" + xmlPath;
                }

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlPath);
                XmlNodeList nodeList = xmlDoc.SelectSingleNode(xmlRootName).ChildNodes;
                foreach (XmlNode xn in nodeList)
                {
                    //XmlElement xmlE = (XmlElement)xn; 
                    if (xn.Name == xmlnode)
                    {
                        retstr = xn.InnerText;
                        break;
                    }
                }
            }
            catch{}

            return retstr;
        }

        /// <summary> 返回字符串中的数字部份
        /// </summary>
        /// <param name="Allstr"></param>
        /// <returns></returns>
        public string GetNumericByStr(string Allstr)
        {
            string pattern = "^(-?[0-9]*[.]*[0-9]{0,3})$";
            string NewStr = "";

            if (Allstr != "")
            {
                foreach (char charone in Allstr)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(charone.ToString(), pattern))
                    {
                        NewStr += charone;
                    }
                }

            }

            return NewStr;
        }
    }
}
