using System;
using System.Collections.Generic;
using System.Text;

namespace DecodeInterface
{
    /// <summary> 样品图形结果
    /// </summary>
    public class ItemImgResult
    {
        private string _FImgType;
        private string _FImgNmae;
        private int _FOrder;
        private string _FRemark;        
        private byte[] _FImg;

        /// <summary> 图象类型
        /// </summary>
        public string FImgType
        {
            get { return _FImgType; }
            set { _FImgType = value; }
        }

        /// <summary> 图象名称
        /// </summary>
        public string FImgNmae
        {
            get { return _FImgNmae; }
            set { _FImgNmae = value; }
        }

        /// <summary> 排序
        /// </summary>
        public int FOrder
        {
            get { return _FOrder; }
            set { _FOrder = value; }
        }

        /// <summary> 图象
        /// </summary>
        public byte[] FImg
        {
            get { return _FImg; }
            set { _FImg = value; }
        }

        /// <summary> 备注
        /// </summary>
        public string FRemark
        {
            get { return _FRemark; }
            set { _FRemark = value; }
        }
    }
}
