﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.IO;
using HIS.InterFaceJKDAN.Class;
using WEISHENG.COMM;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_Main : DevExpress.XtraEditors.XtraForm //Form
    {
        public Frm_Main()
        {
            InitializeComponent();
        }

        public Frm_Main(String UserID)
        {
            InitializeComponent();
            //MessageBox.Show(UserID);
            //初始化用户信息
            DefaultInfo.GetUserInfo(UserID, "HIS", WEISHENG.COMM.zdInfo.Model单位信息.iDwid.ToString());
        }
        public string cardId;
        private void Frm_Main_Load(object sender, EventArgs e)
        {
            //这一步获取用户对照信息和构造函数里面的初始化不冲突
            //如果构造函数里面初始化了用户信息，所属机构不为空，这一步不执行
            //如果是直接调用该窗体，比如his系统，则配合weishengcom使用，根据单位编码取用户对照           
            if (string.IsNullOrEmpty(DefaultInfo.s所属机构))
            {
                DefaultInfo.GetUserInfo(zdInfo.ModelUserInfo.用户编码.ToString(), "HIS", zdInfo.Model单位信息.iDwid.ToString());
            }
            this.textEdit机构.Text = DefaultInfo.s所属机构;
            if (!string.IsNullOrEmpty(this.cardId))
            {
                this.textEdit身份证.Text = this.cardId;
            }
            //Form1 frm = new Form1();
            //WebBro frm = new WebBro();
            //frm.Show();           

        }
        DataTable dt档案列表;
        private void BindList()
        {
            try
            {
                if (string.IsNullOrEmpty(DefaultInfo.s所属机构)) return;
                if (textEdit姓名.Text == "" && textEdit档案号.Text == "" && textEdit身份证.Text == "")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("请至少输入一种查询条件！系统将默认加载前100条数据！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                dt档案列表 = new DataTable();
                string sql = "SELECT Top 100 个人档案编号 ,姓名 , 性别,出生日期 ,身份证号 ,居住地址 ,本人电话,联系人电话 , 所属机构,所属机构名称 ,创建时间 ,修改时间 ,创建人 ,修改人 ,创建机构,是否高血压,是否糖尿病    FROM View_个人信息表 where 1=1 ";
                if (textEdit机构.Text.Length == 12)
                {
                    sql += " and ( [所属机构]=  '" + textEdit机构.Text + "' or substring( [所属机构],1,7)+'1'+substring( [所属机构],9,7) like '" + textEdit机构.Text + "%')";
                }
                else
                {
                    sql += " and  [所属机构] like '" + textEdit机构.Text + "%'";
                }
                if (textEdit姓名.Text != "")
                    sql += " and (姓名 like'%" + Common.DES加密(textEdit姓名.Text) + "%' or 姓名 like'" + textEdit姓名.Text + "%') ";
                if (textEdit档案号.Text != "")
                    sql += " and 个人档案编号='" + textEdit档案号.Text + "' ";
                if (textEdit身份证.Text != "")
                    sql += " and 身份证号='" + textEdit身份证.Text + "' ";
                sql += " order by 创建时间 desc ";
                SqlDataReader dataReader = SqlHelper.ExecuteReader(DefaultInfo.SQLConn, CommandType.Text, sql);

                //DataTable dt = new DataTable();

                DataRow dr = dt档案列表.NewRow();
                if (dataReader.HasRows)
                {
                    //初始化列
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        dt档案列表.Columns.Add(new DataColumn(dataReader.GetName(i)));
                    }
                    while (dataReader.Read())
                    {
                        dr = dt档案列表.NewRow();
                        for (int i = 0; i < dataReader.FieldCount; i++)
                        {
                            if (dataReader.GetName(i).ToString() == "姓名")
                                dr[i] = Common.DES解密(dataReader.GetValue(i).ToString());
                            else
                                dr[i] = dataReader.GetValue(i);
                        }
                        dt档案列表.Rows.Add(dr);
                    }
                    dataReader.Close();
                }
                dt档案列表.Columns.Add("booll", System.Type.GetType("System.Boolean"));
                gridControl1.DataSource = dt档案列表;
                this.gridView1.BestFitColumns();
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载失败！异常信息：\n" + ex.Message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //throw ex;
            }

        }

        #region 菜单事件

        private void simpleButton查询_Click(object sender, EventArgs e)
        {
            BindList();
        }

        private void simpleButton接诊记录_Click(object sender, EventArgs e)
        {
            if (gridView1.GetSelectedRows() != null && gridView1.GetSelectedRows().Length > 0)
            {
                string s档案号 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "个人档案编号").ToString();
                Frm_Edit接诊记录 frm = new Frm_Edit接诊记录(s档案号);
                frm.ShowDialog();
            }
            else
            {
                if (MessageBox.Show("没有人员信息！是否查询?", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    BindList();
            }
        }

        private void simpleButton导出_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.GetSelectedRows() != null && gridView1.GetSelectedRows().Length > 0)
                {
                    SaveFileDialog fileDialog = new SaveFileDialog();
                    fileDialog.Title = "导出Excel";
                    fileDialog.Filter = "Excel文件(*.xls)|*.xls";
                    fileDialog.RestoreDirectory = true;
                    DialogResult dialogResult = fileDialog.ShowDialog(this);
                    if (dialogResult == DialogResult.OK)
                    {
                        DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions();

                        gridView1.ExportToXls(fileDialog.FileName);

                        DevExpress.XtraEditors.XtraMessageBox.Show("保存成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("异常信息：\n" + ex.Message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void simpleButton打印_Click(object sender, EventArgs e)
        {
            string s档案号 = "";
            string s姓名 = "";
            string s性别 = "";
            string s年龄 = "";
            string s居住地址 = "";
            DataTable dt接诊信息;
            if (gridView1.GetSelectedRows() != null && gridView1.GetSelectedRows().Length > 0)
            {
                s档案号 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "个人档案编号").ToString();
                s姓名 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "姓名").ToString();
                s性别 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "性别").ToString();
                s年龄 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "出生日期").ToString();
                s年龄 = (System.DateTime.Now.Year - Convert.ToDateTime(s年龄).Year).ToString();
                s居住地址 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "居住地址").ToString();
                string sql = "select ID,个人档案编号,接诊医生,主观资料,客观资料,评估,处置计划,接诊时间,创建人,修改人 from tb_接诊记录 " +
                        "where 个人档案编号='" + s档案号 + "' order by 创建时间 desc ";
                dt接诊信息 = SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, sql).Tables[0];
                if (dt接诊信息 != null && dt接诊信息.Rows.Count > 0)
                {
                    XtraReport接诊记录单 xrpt = new XtraReport接诊记录单(dt接诊信息, s姓名, s性别, s年龄, s居住地址);
                    xrpt.ShowPreviewDialog();
                }
                else
                {
                    if (MessageBox.Show("请完善接诊信息！", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        Frm_Edit接诊记录 frm = new Frm_Edit接诊记录(s档案号);
                        frm.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("没有人员信息！", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
        }

        private void simpleButton关闭_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton选择机构_Click(object sender, EventArgs e)
        {
            Frm_选择机构 frm = new Frm_选择机构();
            frm.Definite += delegate (string s)
            {
                this.textEdit机构.Text = s;
            };
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            BindList();
        }

        #endregion

        private void Frm_Main_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            }
        }

        private void simpleButton维护_Click(object sender, EventArgs e)
        {
            Frm_资料维护 frm = new Frm_资料维护();
            frm.ShowDialog();
        }

        private void simpleButton个人信息_Click(object sender, EventArgs e)
        {
            //DevExpress.XtraEditors.XtraMessageBox.Show("该功能正在升级！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //return;
            if (gridView1.GetSelectedRows() != null && gridView1.GetSelectedRows().Length > 0)
            {
                string s档案号 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "个人档案编号").ToString();
                Frm_个人档案 frm = new Frm_个人档案(s档案号, "");
                //WebBro frm = new WebBro();
                //frm.MyUrl = s档案号;
                if (Application.OpenForms[frm.Name] != null)
                {
                    //frm = (WebBro)Application.OpenForms[frm.Name];
                    frm.Activate();
                    if (frm.WindowState == FormWindowState.Minimized)//如果当前窗体已经最小化
                    {
                        frm.WindowState = FormWindowState.Maximized; //还原窗体
                    }
                }
                else
                    frm.Show();
            }
        }

        private void simpleButton用户_Click(object sender, EventArgs e)
        {
            Frm_用户参照设置 frm = new Frm_用户参照设置();
            frm.Show();
        }
        private void btn查看体检信息_Click(object sender, EventArgs e)
        {
            if (this.gridView1.GetSelectedRows() != null && this.gridView1.GetSelectedRows().Length > 0)
            {
                string docNo = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "个人档案编号").ToString();
                Frm_健康体检表 frm_健康体检表 = new Frm_健康体检表(docNo);
                if (Application.OpenForms[frm_健康体检表.Name] != null)
                {
                    frm_健康体检表.Activate();
                    if (frm_健康体检表.WindowState == FormWindowState.Minimized)
                    {
                        frm_健康体检表.WindowState = FormWindowState.Maximized;
                    }
                }
                else
                {
                    frm_健康体检表.Show();
                }
            }
        }
        private void btn查看高血压随访_Click(object sender, EventArgs e)
        {
            if (this.gridView1.GetSelectedRows() != null && this.gridView1.GetSelectedRows().Length > 0)
            {
                string docNo = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "个人档案编号").ToString();
                string value = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "是否高血压").ToString();
                if (!string.IsNullOrEmpty(value))
                {
                    Frm_高血压患者随访记录表 frm_高血压患者随访记录表 = new Frm_高血压患者随访记录表(docNo);
                    if (Application.OpenForms[frm_高血压患者随访记录表.Name] != null)
                    {
                        frm_高血压患者随访记录表.Activate();
                        if (frm_高血压患者随访记录表.WindowState == FormWindowState.Minimized)
                        {
                            frm_高血压患者随访记录表.WindowState = FormWindowState.Maximized;
                        }
                    }
                    else
                    {
                        frm_高血压患者随访记录表.Show();
                    }
                }
                else
                {
                    MessageBox.Show("此人不是高血压，请确认！");
                }
            }
        }
        private void btn查看糖尿病随访_Click(object sender, EventArgs e)
        {
            if (this.gridView1.GetSelectedRows() != null && this.gridView1.GetSelectedRows().Length > 0)
            {
                string docNo = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "个人档案编号").ToString();
                string value = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "是否糖尿病").ToString();
                if (!string.IsNullOrEmpty(value))
                {
                    Frm_糖尿病患者随访记录表 frm_糖尿病患者随访记录表 = new Frm_糖尿病患者随访记录表(docNo);
                    if (Application.OpenForms[frm_糖尿病患者随访记录表.Name] != null)
                    {
                        frm_糖尿病患者随访记录表.Activate();
                        if (frm_糖尿病患者随访记录表.WindowState == FormWindowState.Minimized)
                        {
                            frm_糖尿病患者随访记录表.WindowState = FormWindowState.Maximized;
                        }
                    }
                    else
                    {
                        frm_糖尿病患者随访记录表.Show();
                    }
                }
                else
                {
                    MessageBox.Show("此人不是糖尿病，请确认！");
                }
            }
        }

    }
}
