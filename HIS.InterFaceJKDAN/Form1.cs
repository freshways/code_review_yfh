﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace InterFaceJKDAN
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private const string dsn = "DSN=OutBound";
        private const string conn = "Driver={IBM DB2 ODBC DRIVER};Database=YSDB;hostname=192.168.10.101;port=60000;protocol=TCPIP; uid=db2inst1; pwd=ysxwsj001";
       
        private void button1_Click(object sender, EventArgs e)
        {
            //         OdbcDataReader reader = ExecuteReader(dsn, "select * from admin.vi_cs_precalloutcust");
            OdbcDataReader reader = ExecuteReader(conn, "select * from db2admin.T_BS_JWH where B_RGID='37170210008' ;");
            
            while (reader.Read())
            {

                if (!reader.IsDBNull(0))
                {
                    this.textBox1.Text += reader[0].ToString() + "\r\n";
                }
            }
            reader.Close();
        }

        public static OdbcDataReader ExecuteReader(string connectionStr, string strSQL)
        {
            OdbcConnection connection = new OdbcConnection(connectionStr);
            connection.Open();
            OdbcCommand cmd = new OdbcCommand(strSQL, connection);
            try
            {
                
                OdbcDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return myReader;
            }
            catch (SqlException E)
            {
               // WriteLogFile.WriteMessage("DBOper.log", "error DBHelper.ExecuteReader " + E.Message.ToString());
            }
            return null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //DB2Helper db = new DB2Helper();
            //db.ConnectionString = @"Database=YSDB;UserID=db2inst1; Password=ysxwsj001;Server=192.168.10.101:60000";
            //db.CommandText = "select * from db2admin.T_BS_JWH where B_RGID='37170210008' ;";
            //DataSet ds = db.Execute();
            //this.textBox1.Text = ds.Tables[0].Rows[0][1].ToString();
            //MessageBox.Show(ds.Tables.Count.ToString());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //DB2 db = new DB2();
            //db.ConnectionString = Properties.Settings.Default.db2ConnString; // @"Database=sample;UserID=db2admin; Password=sasa;Server=127.0.0.1:50000";
            //db.CommandText = "insert into db2admin.bh values('" + textBox1.Text.Trim() + "','" + textBox2.Text.Trim() +"')";
            //int rows = db.ExecuteNonQuery();
            //MessageBox.Show(rows.ToString());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox3.Text.Length > 3)
                textBox4.Text = DESEncrypt.DESEnCode(textBox3.Text);
            else
            {
                String jiemi = "";
                int len = textBox3.Text.Length;
                for (int i = 0; i < len; i++)
                {
                    jiemi += DESEncrypt.DESEnCode(textBox3.Text.Substring(i, 1));
                }
                textBox4.Text = jiemi;//Encrypt.DESEnCode(textBox3.Text);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            String jiemi = "";
            int len = textBox3.Text.Length/16;
            for (int i = 0; i < len; i++)
            {
                jiemi += DESEncrypt.DESDeCode(textBox3.Text.Substring(i * 16, 16));
            }
            textBox4.Text = DESEncrypt.DESDeCode(textBox3.Text);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

    }
}
