using System;
using System.Collections;
using System.Data;
using System.Reflection;
namespace HIS.InterFaceJKDAN
{
	public class DataConverter
	{
		public static DataTable CreateTable(Type t)
		{
			return DataConverter.BuiltTable(t.GetProperties());
		}
		private static DataTable BuiltTable(PropertyInfo[] pinfo)
		{
			DataTable result;
			try
			{
				if (pinfo == null)
				{
					result = null;
				}
				else
				{
					DataTable dataTable = new DataTable();
					for (int i = 0; i < pinfo.Length; i++)
					{
						PropertyInfo propertyInfo = pinfo[i];
						Type dataType = propertyInfo.PropertyType;
						if (propertyInfo.PropertyType.IsGenericType)
						{
							dataType = propertyInfo.PropertyType.GetGenericArguments()[0];
						}
						DataColumn dataColumn = new DataColumn(propertyInfo.Name, dataType);
						dataColumn.AllowDBNull = true;
						dataTable.Columns.Add(dataColumn);
					}
					result = dataTable;
				}
			}
			catch
			{
				result = null;
			}
			return result;
		}
		public static object CopyProperties(object source, Type destination)
		{
			object result;
			try
			{
				if (source == null)
				{
					result = null;
				}
				else
				{
					object obj = destination.Assembly.CreateInstance(destination.FullName);
					PropertyInfo[] properties = obj.GetType().GetProperties();
					PropertyInfo[] array = properties;
					for (int i = 0; i < array.Length; i++)
					{
						PropertyInfo propertyInfo = array[i];
						object valueOfObject = DataConverter.GetValueOfObject(source, propertyInfo.Name);
						if (DataConverter.CanShallowCopyProperty(valueOfObject))
						{
							DataConverter.SetPropertyValue(obj, propertyInfo, valueOfObject);
						}
					}
					result = obj;
				}
			}
			catch
			{
				result = null;
			}
			return result;
		}
		private static bool CanShallowCopyProperty(object propValue)
		{
			return propValue == null || (propValue.GetType().IsValueType || propValue is string);
		}
		public static void CopyProperties(object source, object destObj)
		{
			try
			{
				if (source != null && destObj != null)
				{
					PropertyInfo[] properties = destObj.GetType().GetProperties();
					PropertyInfo[] array = properties;
					for (int i = 0; i < array.Length; i++)
					{
						PropertyInfo propertyInfo = array[i];
						object valueOfObject = DataConverter.GetValueOfObject(source, propertyInfo.Name);
						if (DataConverter.CanShallowCopyProperty(valueOfObject))
						{
							DataConverter.SetPropertyValue(destObj, propertyInfo, valueOfObject);
						}
					}
				}
			}
			catch
			{
			}
		}
		public static object CloneObject(object source)
		{
			object result;
			try
			{
				if (source == null)
				{
					result = null;
				}
				else
				{
					Type type = source.GetType();
					object obj = type.Assembly.CreateInstance(type.FullName);
					PropertyInfo[] properties = type.GetProperties();
					PropertyInfo[] array = properties;
					for (int i = 0; i < array.Length; i++)
					{
						PropertyInfo propertyInfo = array[i];
						object valueOfObject = DataConverter.GetValueOfObject(source, propertyInfo.Name);
						if (DataConverter.CanShallowCopyProperty(valueOfObject))
						{
							DataConverter.SetPropertyValue(obj, propertyInfo, valueOfObject);
						}
					}
					result = obj;
				}
			}
			catch
			{
				result = null;
			}
			return result;
		}
		public static ArrayList CloneObjects(IList source)
		{
			ArrayList result;
			if (source == null)
			{
				result = null;
			}
			else
			{
				ArrayList arrayList = new ArrayList();
				foreach (object current in source)
				{
					arrayList.Add(DataConverter.CloneObject(current));
				}
				result = arrayList;
			}
			return result;
		}
		public static object GetValueOfObject(object obj, string property)
		{
			object result;
			try
			{
				if (obj == null)
				{
					result = null;
				}
				else
				{
					Type type = obj.GetType();
					PropertyInfo[] properties = type.GetProperties();
					PropertyInfo[] array = properties;
					for (int i = 0; i < array.Length; i++)
					{
						PropertyInfo propertyInfo = array[i];
						if (propertyInfo.Name.ToUpper() == property.ToUpper())
						{
							result = propertyInfo.GetValue(obj, null);
							return result;
						}
					}
					result = null;
				}
			}
			catch
			{
				result = null;
			}
			return result;
		}
		public static object GetObjectValueByKey(IList objects, string keyPropName, object keyValue, string returnPropName)
		{
			object objectByKey = DataConverter.GetObjectByKey(objects, keyPropName, keyValue);
			object result;
			if (objectByKey != null)
			{
				result = DataConverter.GetValueOfObject(objectByKey, returnPropName);
			}
			else
			{
				result = null;
			}
			return result;
		}
		public static object GetObjectByKey(IList objects, string keyPropName, object keyValue)
		{
			object result;
			foreach (object current in objects)
			{
				object valueOfObject = DataConverter.GetValueOfObject(current, keyPropName);
				if (valueOfObject != null)
				{
					if (valueOfObject.ToString().ToLower() == keyValue.ToString().ToLower())
					{
						result = current;
						return result;
					}
				}
			}
			result = null;
			return result;
		}
		public static bool FindProperty(object obj, string property)
		{
			bool result;
			try
			{
				if (obj == null)
				{
					result = false;
				}
				else
				{
					Type type = obj.GetType();
					PropertyInfo[] properties = type.GetProperties();
					PropertyInfo[] array = properties;
					for (int i = 0; i < array.Length; i++)
					{
						PropertyInfo propertyInfo = array[i];
						if (propertyInfo.Name.ToUpper() == property.ToUpper())
						{
							result = true;
							return result;
						}
					}
					result = false;
				}
			}
			catch
			{
				result = false;
			}
			return result;
		}
		public static void SetValueofDataRow(DataRow dr, string field, object value)
		{
			try
			{
				if (dr != null)
				{
					dr[field] = value;
				}
			}
			catch
			{
			}
		}
		public static void SetValueOfObject(object obj, string property, object value)
		{
			try
			{
				if (obj != null)
				{
					Type type = obj.GetType();
					PropertyInfo[] properties = type.GetProperties();
					PropertyInfo[] array = properties;
					for (int i = 0; i < array.Length; i++)
					{
						PropertyInfo propertyInfo = array[i];
						if (propertyInfo.Name.ToUpper() == property.ToUpper())
						{
							DataConverter.SetPropertyValue(obj, propertyInfo, value);
							break;
						}
					}
				}
			}
			catch
			{
			}
		}
		public static void SetPropertyValue(object instance, PropertyInfo prop, object value)
		{
			try
			{
				if (!(prop == null))
				{
					if (!(prop.PropertyType.ToString() == "System.String"))
					{
						if (prop.PropertyType.ToString() == "System.Decimal")
						{
							value = decimal.Parse(value.ToString());
						}
						else
						{
							if (prop.PropertyType.ToString() == "System.Int32")
							{
								value = int.Parse(value.ToString());
							}
							else
							{
								if (prop.PropertyType.ToString() == "System.Single")
								{
									value = float.Parse(value.ToString());
								}
								else
								{
									if (prop.PropertyType.ToString() == "System.DateTime")
									{
										value = DateTime.Parse(value.ToString());
									}
								}
							}
						}
					}
					prop.SetValue(instance, value, null);
				}
			}
			catch
			{
			}
		}
		public static IList CSharpDataTypes()
		{
			return new ArrayList
			{
				typeof(DateTime),
				typeof(byte),
				typeof(sbyte),
				typeof(short),
				typeof(int),
				typeof(long),
				typeof(IntPtr),
				typeof(ushort),
				typeof(uint),
				typeof(ulong),
				typeof(UIntPtr),
				typeof(float),
				typeof(double),
				typeof(decimal),
				typeof(bool),
				typeof(char),
				typeof(string)
			};
		}
		public static SqlDbType SqlTypeString2SqlType(string sqlTypeString)
		{
			SqlDbType result = SqlDbType.Variant;
			switch (sqlTypeString)
			{
			case "int":
				result = SqlDbType.Int;
				break;
			case "varchar":
				result = SqlDbType.VarChar;
				break;
			case "bit":
				result = SqlDbType.Bit;
				break;
			case "datetime":
				result = SqlDbType.DateTime;
				break;
			case "decimal":
				result = SqlDbType.Decimal;
				break;
			case "float":
				result = SqlDbType.Float;
				break;
			case "image":
				result = SqlDbType.Image;
				break;
			case "money":
				result = SqlDbType.Money;
				break;
			case "ntext":
				result = SqlDbType.NText;
				break;
			case "nvarchar":
				result = SqlDbType.NVarChar;
				break;
			case "smalldatetime":
				result = SqlDbType.SmallDateTime;
				break;
			case "smallint":
				result = SqlDbType.SmallInt;
				break;
			case "text":
				result = SqlDbType.Text;
				break;
			case "bigint":
				result = SqlDbType.BigInt;
				break;
			case "binary":
				result = SqlDbType.Binary;
				break;
			case "char":
				result = SqlDbType.Char;
				break;
			case "nchar":
				result = SqlDbType.NChar;
				break;
			case "numeric":
				result = SqlDbType.Decimal;
				break;
			case "real":
				result = SqlDbType.Real;
				break;
			case "smallmoney":
				result = SqlDbType.SmallMoney;
				break;
			case "sql_variant":
				result = SqlDbType.Variant;
				break;
			case "timestamp":
				result = SqlDbType.Timestamp;
				break;
			case "tinyint":
				result = SqlDbType.TinyInt;
				break;
			case "uniqueidentifier":
				result = SqlDbType.UniqueIdentifier;
				break;
			case "varbinary":
				result = SqlDbType.VarBinary;
				break;
			case "xml":
				result = SqlDbType.Xml;
				break;
			}
			return result;
		}
		public static bool ColumnExists(DataTable dt, string columnName)
		{
			bool result;
			if (dt == null)
			{
				result = false;
			}
			else
			{
				foreach (DataColumn dataColumn in dt.Columns)
				{
					if (dataColumn.ColumnName.ToLower() == columnName.ToLower())
					{
						result = true;
						return result;
					}
				}
				result = false;
			}
			return result;
		}
		private static object GetFieldValue(DataRow row, string propertyName)
		{
			object result;
			if (row == null)
			{
				result = null;
			}
			else
			{
				if (row.Table.Columns.IndexOf(propertyName) >= 0)
				{
					object obj = row[propertyName];
					if (obj != null && obj is DateTime)
					{
						if ((DateTime)obj <= DateTime.MinValue.AddDays(1.0))
						{
							obj = null;
						}
					}
					result = obj;
				}
				else
				{
					result = null;
				}
			}
			return result;
		}
		public static DataRow UpdateDataRowFromObject(DataRow row, object o)
		{
			PropertyInfo[] properties = o.GetType().GetProperties();
			PropertyInfo[] array = properties;
			for (int i = 0; i < array.Length; i++)
			{
				PropertyInfo propertyInfo = array[i];
				if (row.Table.Columns.IndexOf(propertyInfo.Name) >= 0)
				{
					DataConverter.SetDataRowValue(row, propertyInfo.Name, propertyInfo.GetValue(o, null));
				}
			}
			return row;
		}
		public static DataRow AddDataRowFromObject(DataTable dt, object o)
		{
			DataRow dataRow = dt.NewRow();
			PropertyInfo[] properties = o.GetType().GetProperties();
			PropertyInfo[] array = properties;
			for (int i = 0; i < array.Length; i++)
			{
				PropertyInfo propertyInfo = array[i];
				if (dt.Columns.IndexOf(propertyInfo.Name) >= 0)
				{
					DataConverter.SetDataRowValue(dataRow, propertyInfo.Name, propertyInfo.GetValue(o, null));
				}
			}
			dt.Rows.Add(dataRow);
			return dataRow;
		}
		public static void SetDataRowValue(DataRow row, string fieldName, object value)
		{
			row[fieldName] = ((value == null || ConvertEx.ToString(value) == "NULL") ? DBNull.Value : value);
		}
		public static void SetTwoRowSameColValue(DataRow drSource, DataRow drTo)
		{
			for (int i = 0; i < drSource.Table.Columns.Count; i++)
			{
				string columnName = drSource.Table.Columns[i].ColumnName;
				DataColumn dataColumn = drTo.Table.Columns[columnName];
				if (dataColumn != null)
				{
					drTo[columnName] = drSource[columnName];
				}
			}
		}
		public static object DataRowToObject(DataRow row, Type type)
		{
			object result;
			if (null == row)
			{
				result = null;
			}
			else
			{
				try
				{
					object obj = type.Assembly.CreateInstance(type.FullName);
					PropertyInfo[] properties = type.GetProperties();
					PropertyInfo[] array = properties;
					for (int i = 0; i < array.Length; i++)
					{
						PropertyInfo propertyInfo = array[i];
						DataConverter.SetPropertyValue(obj, propertyInfo, DataConverter.GetFieldValue(row, propertyInfo.Name));
					}
					result = obj;
				}
				catch
				{
					result = null;
				}
			}
			return result;
		}
		public static object[] ToObjects(IList source)
		{
			object[] result;
			if (null == source)
			{
				result = null;
			}
			else
			{
				object[] array = new object[source.Count];
				for (int i = 0; i < source.Count; i++)
				{
					array[i] = source[i];
				}
				result = array;
			}
			return result;
		}
		public static ArrayList ToArrayList(IList list)
		{
			ArrayList result;
			if (list == null)
			{
				result = null;
			}
			else
			{
				ArrayList arrayList = new ArrayList();
				foreach (object current in list)
				{
					arrayList.Add(current);
				}
				result = arrayList;
			}
			return result;
		}
		public static ArrayList ToArrayList(object[] source)
		{
			ArrayList result;
			if (null != source)
			{
				result = new ArrayList(source);
			}
			else
			{
				result = new ArrayList();
			}
			return result;
		}
		public static string ToSQLInDataFormat(string input)
		{
			string text = string.Empty;
			string result;
			if (input == string.Empty)
			{
				result = text;
			}
			else
			{
				string[] array = input.Split(new char[]
				{
					','
				});
				string[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					string text2 = array2[i];
					if (text2.Length != 0)
					{
						text = text + "'" + text2 + "',";
					}
				}
				if (text.Substring(text.Length - 1, 1) == ",")
				{
					text = text.Substring(0, text.Length - 1);
				}
				result = text;
			}
			return result;
		}
		public static string ToSQLInDataFormatTwo(string input)
		{
			string text = string.Empty;
			string result;
			if (input == string.Empty)
			{
				result = text;
			}
			else
			{
				string[] array = input.Split(new char[]
				{
					','
				});
				string[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					string text2 = array2[i];
					if (text2.Length != 0)
					{
						text = text + "''" + text2 + "'',";
					}
				}
				if (text.Substring(text.Length - 1, 1) == ",")
				{
					text = text.Substring(0, text.Length - 1);
				}
				result = text;
			}
			return result;
		}
		public static bool UpdateTableCol(DataTable dt, string fieldName, object value)
		{
			bool result;
			try
			{
				if (dt.Columns.IndexOf(fieldName) < 0)
				{
					throw new Exception("表没有" + fieldName + "列！");
				}
				foreach (DataRow dataRow in dt.Rows)
				{
					dataRow[fieldName] = value;
				}
				result = true;
			}
			catch
			{
				result = false;
			}
			return result;
		}
		public static string[] ToStringSplit(string str)
		{
			if (str.Length > 0)
			{
				if (str[0] == ',')
				{
					str = str.Substring(1, str.Length - 1);
				}
				if (str[str.Length - 1] == ',')
				{
					str = str.Substring(0, str.Length - 1);
				}
			}
			return str.Split(new char[]
			{
				','
			});
		}
		public static DataTable AddTableRowByRow(DataTable dt, DataRow dr)
		{
			bool flag = false;
			DataRow dataRow = dt.NewRow();
			for (int i = 0; i < dr.Table.Columns.Count; i++)
			{
				string columnName = dr.Table.Columns[i].ColumnName;
				if (dt.Columns.IndexOf(columnName) >= 0)
				{
					dataRow[columnName] = dr[columnName];
					flag = true;
				}
			}
			if (flag)
			{
				dt.Rows.Add(dataRow);
			}
			return dt;
		}
	}
}
