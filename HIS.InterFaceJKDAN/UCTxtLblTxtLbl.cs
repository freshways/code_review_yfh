﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HIS.InterFaceJKDAN
{
    public partial class UCTxtLblTxtLbl : UserControl
    {
        public UCTxtLblTxtLbl()
        {
            InitializeComponent();
        }

        #region MyRegion
        [Category("Customer")]
        public TextEdit Txt1
        {
            get { return this.txt1; }
            set { this.txt1 = value; }
        }
        [Category("Customer")]
        public TextEdit Txt2
        {
            get { return this.txt2; }
            set { this.txt2 = value; }
        }
        [Category("Customer")]
        public LabelControl Lbl1
        {
            get { return this.lbl1; }
            set { this.lbl1 = value; }
        }
        [Category("Customer")]
        public LabelControl Lbl2
        {
            get { return this.lbl2; }
            set { this.lbl2 = value; }
        }
        [Category("Custom Size")]
        public Size Txt1Size
        {
            get { return this.txt1.Size; }
            set { this.txt1.Size = value; }
        }
        [Category("Custom Size")]
        public Size Txt2Size
        {
            get { return this.txt2.Size; }
            set { this.txt2.Size = value; }
        }
        [Category("Custom Size")]
        public Size Lbl1Size
        {
            get { return this.lbl1.Size; }
            set { this.lbl1.Size = value; }
        }
        [Category("Customer")]
        public Size Lbl2Size
        {
            get { return this.lbl2.Size; }
            set { this.lbl2.Size = value; }
        }
        [Category("Customer")]
        public string Lbl1Text
        {
            get { return this.lbl1.Text; }
            set
            {
                this.lbl1.Text = value;
            }
        }
        [Category("Customer")]
        public string Lbl2Text
        {
            get { return this.lbl2.Text; }
            set
            {
                this.lbl2.Text = value;
            }
        }
        [Category("Customer")]
        public object Txt1EditValue
        {
            get { return this.txt1.EditValue; }
            set
            {
                this.txt1.EditValue = value;
            }
        }
        [Category("Customer")]
        public object Txt2EditValue
        {
            get { return this.txt2.EditValue; }
            set
            {
                this.txt2.EditValue = value;
            }
        }
        #endregion

        private void txt1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                //if (txt1.Text.Trim() != "" && txt1.Text.Trim() != "0.00" && txt1.Text.Trim() != "0")
                if (txt1.Text.Trim() != "")
                    this.Tag = "1";
                else
                    this.Tag = null;
                //if (txt2.Text.Trim() != "" && txt2.Text.Trim() != "0.00" && txt2.Text.Trim() != "0")
                if (txt2.Text.Trim() != "")
                    this.Tag = "1";
                else
                    this.Tag = null;

                if (txt1.EditValue == null || txt1.EditValue.ToString() == "")
                    txt1.EditValue = null;
                if (txt2.EditValue == null || txt2.EditValue.ToString() == "")
                    txt2.EditValue = null;
            }
            catch { this.Tag = null; }
        }
    }
}
