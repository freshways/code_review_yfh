﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HIS.InterFaceJKDAN
{
    public partial class UCTxtLbl : UserControl
    {
        public UCTxtLbl()
        {
            InitializeComponent();
        }
        #region MyRegion
        [Category("Customer")]
        public TextEdit Txt1
        {
            get { return this.txt1; }
            set { this.txt1 = value; }
        }
        [Category("Customer")]
        public LabelControl Lbl1
        {
            get { return this.lbl1; }
            set { this.lbl1 = value; }
        }

        [Category("Custom Size")]
        public Size Txt1Size
        {
            get { return this.txt1.Size; }
            set { this.txt1.Size = value; }
        }
        [Category("Custom Size")]
        public Size Lbl1Size
        {
            get { return this.lbl1.Size; }
            set { this.lbl1.Size = value; }
        }
        [Category("Customer")]
        public string Lbl1Text
        {
            get { return this.lbl1.Text; }
            set
            {
                this.lbl1.Text = value;
            }
        }
        #endregion

        private void txt1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                //if (txt1.Text.Trim() != "" && txt1.Text.Trim() != "0.00" && txt1.Text.Trim() != "0")
                if (txt1.Text.Trim() != "")
                {
                    this.Tag = "1";
                    //this.txt1.EditValue = null;
                }
                else
                    this.Tag = null;

                if (txt1.EditValue == null || txt1.EditValue.ToString() == "")
                    txt1.EditValue = null;
            }
            catch { this.Tag = null; }
        }
    }
}
