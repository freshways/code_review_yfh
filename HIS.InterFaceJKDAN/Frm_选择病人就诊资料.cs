﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using HIS.InterFaceJKDAN.Class;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_选择病人就诊资料 : DevExpress.XtraEditors.XtraForm
    {
        public string s选择项目 = "";
        string StrSQL = "";
        string StrColunm = "";
        DataTable dt;
        public Frm_选择病人就诊资料()
        {
            InitializeComponent();
        }

        public Frm_选择病人就诊资料(string sql , string colunm,string title)
        {
            InitializeComponent();
            this.Text = title;
            StrSQL = sql;
            StrColunm = colunm;
        }

        private void Frm_DA_病人就诊客观资料_Load(object sender, EventArgs e)
        {
            BindList();
        }

        private void BindList()
        {
            try
            {
                if (StrSQL == "") return;
                dt = SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, StrSQL).Tables[0];
                dt.Columns.Add("多选", System.Type.GetType("System.Boolean"));

                gridControl1.DataSource = dt;
                this.gridView1.BestFitColumns();
                //this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
                this.gridView1.OptionsSelection.MultiSelect = true; //设置多行选择
                this.gridView1.OptionsSelection.MultiSelectMode = GridMultiSelectMode.RowSelect;
                //列不可编辑
                for (int i = 0; i < gridView1.Columns.Count; i++)
                {
                    if (this.gridView1.Columns[i].Name != "col多选")
                        this.gridView1.Columns[i].OptionsColumn.AllowEdit = false;
                }
                //设置焦点列
                DevExpress.XtraGrid.Views.Base.ColumnView view = (DevExpress.XtraGrid.Views.Base.ColumnView)gridControl1.FocusedView;
                view.FocusedColumn = view.Columns["多选"]; 
            }
            catch (Exception)
            { 
                
            }
            
        }

        string GetSelectedRows(GridView view)
        {
            string ret = ""; int rowIndex = -1;
            try
            {
                if (view.OptionsSelection.MultiSelectMode == GridMultiSelectMode.RowSelect)
                {
                    foreach (int i in gridView1.GetSelectedRows())
                    {
                        DataRow row = gridView1.GetDataRow(i); if (ret != "") ret += "\r\n";
                        //ret += string.Format("Company Name: {0} (#{1})", row[StrColunm], i);
                        ret += row[StrColunm].ToString();
                    }
                }
                else
                {
                    //foreach (GridCell cell in view.GetSelectedCells())
                    //{
                    //    if (rowIndex != cell.RowHandle)
                    //    {
                    //        if (ret != "") ret += "\r\n";
                    //        ret += string.Format("Row: #{0}", cell.RowHandle);
                    //    }
                    //    ret += "\r\n    " + view.GetRowCellDisplayText(cell.RowHandle, cell.Column);
                    //    rowIndex = cell.RowHandle;
                    //}
                }
            }
            catch (Exception)
            {
                return "";
            }
            return ret;
        } 

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (gridView1 != null && gridView1.RowCount> 0)
            {
                string value = "";
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    value = gridView1.GetDataRow(i)["多选"].ToString();
                    if (value == "True")
                    {
                        s选择项目 += gridView1.GetRowCellValue(i, StrColunm) + "\r\n";
                    }
                }
                if (s选择项目 == "")
                {
                    MessageBox.Show("请选择项目！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //s选择项目 = GetSelectedRows(this.gridView1);
                this.DialogResult=DialogResult.OK;
            }
        }

        private void simpleButton添加_Click(object sender, EventArgs e)
        {
            Frm_基础资料维护 frm = new Frm_基础资料维护();
            frm.Save += frm_Save;
            frm.ShowDialog();
        }

        void frm_Save(m基础资料 s)
        {
            //throw new NotImplementedException();
            string Beginsql = "";
            switch (StrColunm)
            {
                case "主观资料":
                    Beginsql = "insert into T_病人就诊主观资料(主观资料,拼音代码,是否禁用) values('{0}','{1}','{2}')";
                    break;
                case "客观资料":
                    Beginsql = "insert into T_病人就诊客观资料(客观资料,拼音代码,是否禁用) values('{0}','{1}','{2}')";
                    break;
                case "评估":
                    Beginsql = "insert into T_病人评估(评估,拼音代码,是否禁用) values('{0}','{1}','{2}')";
                    break;
                case "处置计划":
                    Beginsql = "insert into T_病人处置计划(处置计划,拼音代码,是否禁用) values('{0}','{1}','{2}')";
                    break;
                default:
                    break;
            }
            //MessageBox.Show(s.s拼音);
            Beginsql = string.Format(Beginsql, s.s项目, s.s拼音, s.s是否启用);
            int rows = SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, Beginsql);
            if (rows > 0)
            {
                //插入data
                DataRow dr = dt.NewRow();
                dr["ID"] = dt.Rows.Count + 1;
                dr[StrColunm] = s.s项目;
                dr["拼音代码"] = s.s拼音;
                dr["是否禁用"] = s.s是否启用;
                dt.Rows.Add(dr);
            }
        }

    }
}
