﻿namespace HIS.InterFaceJKDAN
{
    partial class Frm_Edit接诊记录
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Edit接诊记录));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton删除 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton修改 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton关闭 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton新增 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit接诊时间 = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton处置计划 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton评估 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton客观资料 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton主观资料 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton打印 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton保存 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit接诊医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit性别 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit主观资料 = new DevExpress.XtraEditors.MemoEdit();
            this.textEdit客观资料 = new DevExpress.XtraEditors.MemoEdit();
            this.textEdit评估 = new DevExpress.XtraEditors.MemoEdit();
            this.textEdit处置计划 = new DevExpress.XtraEditors.MemoEdit();
            this.LookUpEdit接诊历史 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit接诊时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit接诊时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit接诊医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主观资料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit客观资料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit处置计划.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit接诊历史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.groupControl1);
            this.layoutControl1.Controls.Add(this.simpleButton删除);
            this.layoutControl1.Controls.Add(this.simpleButton修改);
            this.layoutControl1.Controls.Add(this.simpleButton关闭);
            this.layoutControl1.Controls.Add(this.simpleButton新增);
            this.layoutControl1.Controls.Add(this.dateEdit接诊时间);
            this.layoutControl1.Controls.Add(this.simpleButton处置计划);
            this.layoutControl1.Controls.Add(this.simpleButton评估);
            this.layoutControl1.Controls.Add(this.simpleButton客观资料);
            this.layoutControl1.Controls.Add(this.simpleButton主观资料);
            this.layoutControl1.Controls.Add(this.simpleButton打印);
            this.layoutControl1.Controls.Add(this.simpleButton保存);
            this.layoutControl1.Controls.Add(this.textEdit修改人);
            this.layoutControl1.Controls.Add(this.textEdit创建人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit所属机构);
            this.layoutControl1.Controls.Add(this.textEdit更新时间);
            this.layoutControl1.Controls.Add(this.textEdit创建时间);
            this.layoutControl1.Controls.Add(this.textEdit接诊医生);
            this.layoutControl1.Controls.Add(this.textEdit居住地址);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit性别);
            this.layoutControl1.Controls.Add(this.textEdit姓名);
            this.layoutControl1.Controls.Add(this.textEdit档案号);
            this.layoutControl1.Controls.Add(this.textEdit主观资料);
            this.layoutControl1.Controls.Add(this.textEdit客观资料);
            this.layoutControl1.Controls.Add(this.textEdit评估);
            this.layoutControl1.Controls.Add(this.textEdit处置计划);
            this.layoutControl1.Controls.Add(this.LookUpEdit接诊历史);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(385, 271, 293, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(746, 616);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton删除
            // 
            this.simpleButton删除.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton删除.Image")));
            this.simpleButton删除.Location = new System.Drawing.Point(552, 7);
            this.simpleButton删除.Name = "simpleButton删除";
            this.simpleButton删除.Size = new System.Drawing.Size(61, 24);
            this.simpleButton删除.StyleController = this.layoutControl1;
            this.simpleButton删除.TabIndex = 31;
            this.simpleButton删除.Text = "删 除";
            this.simpleButton删除.Click += new System.EventHandler(this.simpleButton删除_Click);
            // 
            // simpleButton修改
            // 
            this.simpleButton修改.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton修改.Image")));
            this.simpleButton修改.Location = new System.Drawing.Point(74, 7);
            this.simpleButton修改.Name = "simpleButton修改";
            this.simpleButton修改.Size = new System.Drawing.Size(62, 24);
            this.simpleButton修改.StyleController = this.layoutControl1;
            this.simpleButton修改.TabIndex = 29;
            this.simpleButton修改.Text = "修 改";
            this.simpleButton修改.Click += new System.EventHandler(this.simpleButton修改_Click);
            // 
            // simpleButton关闭
            // 
            this.simpleButton关闭.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton关闭.Appearance.Options.UseFont = true;
            this.simpleButton关闭.AutoWidthInLayoutControl = true;
            this.simpleButton关闭.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton关闭.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton关闭.Image")));
            this.simpleButton关闭.Location = new System.Drawing.Point(272, 7);
            this.simpleButton关闭.Name = "simpleButton关闭";
            this.simpleButton关闭.Size = new System.Drawing.Size(61, 24);
            this.simpleButton关闭.StyleController = this.layoutControl1;
            this.simpleButton关闭.TabIndex = 4;
            this.simpleButton关闭.Text = "关闭";
            // 
            // simpleButton新增
            // 
            this.simpleButton新增.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton新增.Image")));
            this.simpleButton新增.Location = new System.Drawing.Point(7, 7);
            this.simpleButton新增.Name = "simpleButton新增";
            this.simpleButton新增.Size = new System.Drawing.Size(63, 24);
            this.simpleButton新增.StyleController = this.layoutControl1;
            this.simpleButton新增.TabIndex = 0;
            this.simpleButton新增.Text = "添 加";
            this.simpleButton新增.Visible = false;
            this.simpleButton新增.Click += new System.EventHandler(this.simpleButton新增_Click);
            // 
            // dateEdit接诊时间
            // 
            this.dateEdit接诊时间.EditValue = null;
            this.dateEdit接诊时间.Location = new System.Drawing.Point(106, 163);
            this.dateEdit接诊时间.Name = "dateEdit接诊时间";
            this.dateEdit接诊时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit接诊时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit接诊时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit接诊时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit接诊时间.Size = new System.Drawing.Size(254, 20);
            this.dateEdit接诊时间.StyleController = this.layoutControl1;
            this.dateEdit接诊时间.TabIndex = 13;
            // 
            // simpleButton处置计划
            // 
            this.simpleButton处置计划.AutoWidthInLayoutControl = true;
            this.simpleButton处置计划.Location = new System.Drawing.Point(716, 457);
            this.simpleButton处置计划.Name = "simpleButton处置计划";
            this.simpleButton处置计划.Size = new System.Drawing.Size(23, 46);
            this.simpleButton处置计划.StyleController = this.layoutControl1;
            this.simpleButton处置计划.TabIndex = 22;
            this.simpleButton处置计划.Text = "...";
            this.simpleButton处置计划.Click += new System.EventHandler(this.simpleButton处置计划_Click);
            // 
            // simpleButton评估
            // 
            this.simpleButton评估.AutoWidthInLayoutControl = true;
            this.simpleButton评估.Location = new System.Drawing.Point(716, 367);
            this.simpleButton评估.Name = "simpleButton评估";
            this.simpleButton评估.Size = new System.Drawing.Size(23, 46);
            this.simpleButton评估.StyleController = this.layoutControl1;
            this.simpleButton评估.TabIndex = 20;
            this.simpleButton评估.Text = "...";
            this.simpleButton评估.Click += new System.EventHandler(this.simpleButton评估_Click);
            // 
            // simpleButton客观资料
            // 
            this.simpleButton客观资料.AutoWidthInLayoutControl = true;
            this.simpleButton客观资料.Location = new System.Drawing.Point(716, 277);
            this.simpleButton客观资料.Name = "simpleButton客观资料";
            this.simpleButton客观资料.Size = new System.Drawing.Size(23, 46);
            this.simpleButton客观资料.StyleController = this.layoutControl1;
            this.simpleButton客观资料.TabIndex = 18;
            this.simpleButton客观资料.Text = "...";
            this.simpleButton客观资料.Click += new System.EventHandler(this.simpleButton客观资料_Click);
            // 
            // simpleButton主观资料
            // 
            this.simpleButton主观资料.AutoWidthInLayoutControl = true;
            this.simpleButton主观资料.Location = new System.Drawing.Point(716, 187);
            this.simpleButton主观资料.Name = "simpleButton主观资料";
            this.simpleButton主观资料.Size = new System.Drawing.Size(23, 46);
            this.simpleButton主观资料.StyleController = this.layoutControl1;
            this.simpleButton主观资料.TabIndex = 16;
            this.simpleButton主观资料.Text = "...";
            this.simpleButton主观资料.Click += new System.EventHandler(this.simpleButton主观资料_Click);
            // 
            // simpleButton打印
            // 
            this.simpleButton打印.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton打印.Appearance.Options.UseFont = true;
            this.simpleButton打印.AutoWidthInLayoutControl = true;
            this.simpleButton打印.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton打印.Image")));
            this.simpleButton打印.Location = new System.Drawing.Point(205, 7);
            this.simpleButton打印.Name = "simpleButton打印";
            this.simpleButton打印.Size = new System.Drawing.Size(63, 24);
            this.simpleButton打印.StyleController = this.layoutControl1;
            this.simpleButton打印.TabIndex = 2;
            this.simpleButton打印.Text = "打 印";
            this.simpleButton打印.Click += new System.EventHandler(this.simpleButton打印_Click);
            // 
            // simpleButton保存
            // 
            this.simpleButton保存.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton保存.Appearance.Options.UseFont = true;
            this.simpleButton保存.AutoWidthInLayoutControl = true;
            this.simpleButton保存.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton保存.Image")));
            this.simpleButton保存.Location = new System.Drawing.Point(140, 7);
            this.simpleButton保存.Name = "simpleButton保存";
            this.simpleButton保存.Size = new System.Drawing.Size(61, 24);
            this.simpleButton保存.StyleController = this.layoutControl1;
            this.simpleButton保存.TabIndex = 1;
            this.simpleButton保存.Text = "保 存 ";
            this.simpleButton保存.Click += new System.EventHandler(this.simpleButton保存_Click);
            // 
            // textEdit修改人
            // 
            this.textEdit修改人.Location = new System.Drawing.Point(463, 591);
            this.textEdit修改人.Name = "textEdit修改人";
            this.textEdit修改人.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit修改人.Properties.ReadOnly = true;
            this.textEdit修改人.Size = new System.Drawing.Size(276, 18);
            this.textEdit修改人.StyleController = this.layoutControl1;
            this.textEdit修改人.TabIndex = 28;
            // 
            // textEdit创建人
            // 
            this.textEdit创建人.Location = new System.Drawing.Point(106, 591);
            this.textEdit创建人.Name = "textEdit创建人";
            this.textEdit创建人.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit创建人.Properties.ReadOnly = true;
            this.textEdit创建人.Size = new System.Drawing.Size(254, 18);
            this.textEdit创建人.StyleController = this.layoutControl1;
            this.textEdit创建人.TabIndex = 27;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(463, 569);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(276, 18);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 26;
            // 
            // textEdit所属机构
            // 
            this.textEdit所属机构.Location = new System.Drawing.Point(106, 569);
            this.textEdit所属机构.Name = "textEdit所属机构";
            this.textEdit所属机构.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit所属机构.Properties.ReadOnly = true;
            this.textEdit所属机构.Size = new System.Drawing.Size(254, 18);
            this.textEdit所属机构.StyleController = this.layoutControl1;
            this.textEdit所属机构.TabIndex = 25;
            // 
            // textEdit更新时间
            // 
            this.textEdit更新时间.Location = new System.Drawing.Point(463, 547);
            this.textEdit更新时间.Name = "textEdit更新时间";
            this.textEdit更新时间.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit更新时间.Properties.ReadOnly = true;
            this.textEdit更新时间.Size = new System.Drawing.Size(276, 18);
            this.textEdit更新时间.StyleController = this.layoutControl1;
            this.textEdit更新时间.TabIndex = 24;
            // 
            // textEdit创建时间
            // 
            this.textEdit创建时间.Location = new System.Drawing.Point(106, 547);
            this.textEdit创建时间.Name = "textEdit创建时间";
            this.textEdit创建时间.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit创建时间.Properties.ReadOnly = true;
            this.textEdit创建时间.Size = new System.Drawing.Size(254, 18);
            this.textEdit创建时间.StyleController = this.layoutControl1;
            this.textEdit创建时间.TabIndex = 23;
            // 
            // textEdit接诊医生
            // 
            this.textEdit接诊医生.Location = new System.Drawing.Point(463, 163);
            this.textEdit接诊医生.Name = "textEdit接诊医生";
            this.textEdit接诊医生.Size = new System.Drawing.Size(276, 20);
            this.textEdit接诊医生.StyleController = this.layoutControl1;
            this.textEdit接诊医生.TabIndex = 14;
            this.textEdit接诊医生.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Edit接诊记录_KeyPress);
            // 
            // textEdit居住地址
            // 
            this.textEdit居住地址.Location = new System.Drawing.Point(106, 139);
            this.textEdit居住地址.Name = "textEdit居住地址";
            this.textEdit居住地址.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit居住地址.Properties.ReadOnly = true;
            this.textEdit居住地址.Size = new System.Drawing.Size(633, 18);
            this.textEdit居住地址.StyleController = this.layoutControl1;
            this.textEdit居住地址.TabIndex = 12;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(463, 115);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(276, 18);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 11;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(106, 115);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(254, 18);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 10;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(463, 91);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(276, 18);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 9;
            // 
            // textEdit性别
            // 
            this.textEdit性别.Location = new System.Drawing.Point(106, 91);
            this.textEdit性别.Name = "textEdit性别";
            this.textEdit性别.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit性别.Properties.ReadOnly = true;
            this.textEdit性别.Size = new System.Drawing.Size(254, 18);
            this.textEdit性别.StyleController = this.layoutControl1;
            this.textEdit性别.TabIndex = 8;
            // 
            // textEdit姓名
            // 
            this.textEdit姓名.Location = new System.Drawing.Point(463, 67);
            this.textEdit姓名.Name = "textEdit姓名";
            this.textEdit姓名.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit姓名.Properties.ReadOnly = true;
            this.textEdit姓名.Size = new System.Drawing.Size(276, 18);
            this.textEdit姓名.StyleController = this.layoutControl1;
            this.textEdit姓名.TabIndex = 7;
            // 
            // textEdit档案号
            // 
            this.textEdit档案号.Location = new System.Drawing.Point(106, 67);
            this.textEdit档案号.Name = "textEdit档案号";
            this.textEdit档案号.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit档案号.Properties.ReadOnly = true;
            this.textEdit档案号.Size = new System.Drawing.Size(254, 18);
            this.textEdit档案号.StyleController = this.layoutControl1;
            this.textEdit档案号.TabIndex = 6;
            // 
            // textEdit主观资料
            // 
            this.textEdit主观资料.Location = new System.Drawing.Point(106, 187);
            this.textEdit主观资料.Name = "textEdit主观资料";
            this.textEdit主观资料.Size = new System.Drawing.Size(606, 86);
            this.textEdit主观资料.StyleController = this.layoutControl1;
            this.textEdit主观资料.TabIndex = 15;
            this.textEdit主观资料.UseOptimizedRendering = true;
            this.textEdit主观资料.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Edit接诊记录_KeyPress);
            // 
            // textEdit客观资料
            // 
            this.textEdit客观资料.Location = new System.Drawing.Point(106, 277);
            this.textEdit客观资料.Name = "textEdit客观资料";
            this.textEdit客观资料.Size = new System.Drawing.Size(606, 86);
            this.textEdit客观资料.StyleController = this.layoutControl1;
            this.textEdit客观资料.TabIndex = 17;
            this.textEdit客观资料.UseOptimizedRendering = true;
            this.textEdit客观资料.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Edit接诊记录_KeyPress);
            // 
            // textEdit评估
            // 
            this.textEdit评估.Location = new System.Drawing.Point(106, 367);
            this.textEdit评估.Name = "textEdit评估";
            this.textEdit评估.Size = new System.Drawing.Size(606, 86);
            this.textEdit评估.StyleController = this.layoutControl1;
            this.textEdit评估.TabIndex = 19;
            this.textEdit评估.UseOptimizedRendering = true;
            this.textEdit评估.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Edit接诊记录_KeyPress);
            // 
            // textEdit处置计划
            // 
            this.textEdit处置计划.Location = new System.Drawing.Point(106, 457);
            this.textEdit处置计划.Name = "textEdit处置计划";
            this.textEdit处置计划.Size = new System.Drawing.Size(606, 86);
            this.textEdit处置计划.StyleController = this.layoutControl1;
            this.textEdit处置计划.TabIndex = 21;
            this.textEdit处置计划.UseOptimizedRendering = true;
            this.textEdit处置计划.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Edit接诊记录_KeyPress);
            // 
            // LookUpEdit接诊历史
            // 
            this.LookUpEdit接诊历史.Location = new System.Drawing.Point(389, 6);
            this.LookUpEdit接诊历史.Name = "LookUpEdit接诊历史";
            this.LookUpEdit接诊历史.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.LookUpEdit接诊历史.Properties.Appearance.Options.UseFont = true;
            this.LookUpEdit接诊历史.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit接诊历史.Properties.NullText = "";
            this.LookUpEdit接诊历史.Properties.PopupSizeable = false;
            this.LookUpEdit接诊历史.Size = new System.Drawing.Size(160, 24);
            this.LookUpEdit接诊历史.StyleController = this.layoutControl1;
            this.LookUpEdit接诊历史.TabIndex = 30;
            this.LookUpEdit接诊历史.EditValueChanged += new System.EventHandler(this.LookUpEdit接诊历史_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem17,
            this.layoutControlItem19,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem27,
            this.layoutControlItem9,
            this.emptySpaceItem3,
            this.layoutControlItem22,
            this.layoutControlItem21,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem28});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(746, 616);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit档案号;
            this.layoutControlItem2.CustomizationFormText = "个人档案号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem2.Text = "个人档案号:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit性别;
            this.layoutControlItem4.CustomizationFormText = "性别";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem4.Text = "性别:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit出生日期;
            this.layoutControlItem6.CustomizationFormText = "出生日期";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 108);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem6.Text = "出生日期:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit居住地址;
            this.layoutControlItem8.CustomizationFormText = "居住地址";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 132);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(736, 24);
            this.layoutControlItem8.Text = "居住地址:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.textEdit主观资料;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 180);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(50, 90);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(709, 90);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "就诊者的主观资料";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.textEdit客观资料;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 270);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(50, 90);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(709, 90);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "就诊者的客观资料";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.textEdit评估;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 360);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(50, 90);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(709, 90);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "评估";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit姓名;
            this.layoutControlItem3.CustomizationFormText = "姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(357, 60);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem3.Text = "姓名:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit身份证号;
            this.layoutControlItem5.CustomizationFormText = "身份证号";
            this.layoutControlItem5.Location = new System.Drawing.Point(357, 84);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem5.Text = "身份证号:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit联系电话;
            this.layoutControlItem7.CustomizationFormText = "联系电话";
            this.layoutControlItem7.Location = new System.Drawing.Point(357, 108);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem7.Text = "联系电话:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit接诊医生;
            this.layoutControlItem10.CustomizationFormText = "接诊医生";
            this.layoutControlItem10.Location = new System.Drawing.Point(357, 156);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem10.Text = "接诊医生:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.textEdit处置计划;
            this.layoutControlItem14.CustomizationFormText = "处置计划";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 450);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(50, 90);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(709, 90);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "处置计划";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.textEdit创建时间;
            this.layoutControlItem15.CustomizationFormText = "创建时间:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 540);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(357, 22);
            this.layoutControlItem15.Text = "创建时间:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit所属机构;
            this.layoutControlItem17.CustomizationFormText = "当前所属机构:";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 562);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(357, 22);
            this.layoutControlItem17.Text = "当前所属机构:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textEdit创建人;
            this.layoutControlItem19.CustomizationFormText = "创建人:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 584);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(357, 22);
            this.layoutControlItem19.Text = "创建人:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.simpleButton主观资料;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(709, 180);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(27, 50);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(27, 50);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(27, 90);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.simpleButton客观资料;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(709, 270);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(27, 50);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(27, 50);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(27, 90);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.simpleButton评估;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(709, 360);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(27, 50);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(27, 50);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(27, 90);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.simpleButton处置计划;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(709, 450);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(27, 50);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(27, 50);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(27, 90);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "layoutControlItem26";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.textEdit更新时间;
            this.layoutControlItem16.CustomizationFormText = "最近更新时间:";
            this.layoutControlItem16.Location = new System.Drawing.Point(357, 540);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(379, 22);
            this.layoutControlItem16.Text = "最近更新时间:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.textEdit创建机构;
            this.layoutControlItem18.CustomizationFormText = "创建机构:";
            this.layoutControlItem18.Location = new System.Drawing.Point(357, 562);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(379, 22);
            this.layoutControlItem18.Text = "创建机构:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textEdit修改人;
            this.layoutControlItem20.CustomizationFormText = "最近修改人:";
            this.layoutControlItem20.Location = new System.Drawing.Point(357, 584);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(379, 22);
            this.layoutControlItem20.Text = "最近修改人:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.dateEdit接诊时间;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem27.Text = "接诊时间:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton新增;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(67, 28);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(610, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(126, 28);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.simpleButton打印;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(198, 0);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(67, 28);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.simpleButton保存;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(133, 0);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(65, 28);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.simpleButton关闭;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(265, 0);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(65, 28);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.simpleButton修改;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(67, 0);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(66, 28);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "layoutControlItem30";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextToControlDistance = 0;
            this.layoutControlItem30.TextVisible = false;
            this.layoutControlItem30.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.LookUpEdit接诊历史;
            this.layoutControlItem31.CustomizationFormText = "接诊历史";
            this.layoutControlItem31.Location = new System.Drawing.Point(330, 0);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem31.Size = new System.Drawing.Size(215, 28);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "接诊历史";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.simpleButton删除;
            this.layoutControlItem32.CustomizationFormText = "layoutControlItem32";
            this.layoutControlItem32.Location = new System.Drawing.Point(545, 0);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(65, 28);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "layoutControlItem32";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextToControlDistance = 0;
            this.layoutControlItem32.TextVisible = false;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Options.UseTextOptions = true;
            this.groupControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.Location = new System.Drawing.Point(7, 35);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(732, 28);
            this.groupControl1.TabIndex = 32;
            this.groupControl1.Text = "接诊记录表";
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.groupControl1;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(736, 32);
            this.layoutControlItem28.Text = "layoutControlItem28";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextToControlDistance = 0;
            this.layoutControlItem28.TextVisible = false;
            // 
            // Frm_Edit接诊记录
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 616);
            this.Controls.Add(this.layoutControl1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(762, 697);
            this.MinimumSize = new System.Drawing.Size(762, 497);
            this.Name = "Frm_Edit接诊记录";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "接诊记录";
            this.Load += new System.EventHandler(this.Frm_Edit接诊记录_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Edit接诊记录_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit接诊时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit接诊时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit接诊医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主观资料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit客观资料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit处置计划.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit接诊历史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit接诊医生;
        private DevExpress.XtraEditors.TextEdit textEdit居住地址;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit性别;
        private DevExpress.XtraEditors.TextEdit textEdit姓名;
        private DevExpress.XtraEditors.TextEdit textEdit档案号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit textEdit修改人;
        private DevExpress.XtraEditors.TextEdit textEdit创建人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit更新时间;
        private DevExpress.XtraEditors.TextEdit textEdit创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.SimpleButton simpleButton保存;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.SimpleButton simpleButton打印;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.SimpleButton simpleButton处置计划;
        private DevExpress.XtraEditors.SimpleButton simpleButton评估;
        private DevExpress.XtraEditors.SimpleButton simpleButton客观资料;
        private DevExpress.XtraEditors.SimpleButton simpleButton主观资料;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.DateEdit dateEdit接诊时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.MemoEdit textEdit主观资料;
        private DevExpress.XtraEditors.MemoEdit textEdit客观资料;
        private DevExpress.XtraEditors.MemoEdit textEdit评估;
        private DevExpress.XtraEditors.MemoEdit textEdit处置计划;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.SimpleButton simpleButton新增;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton simpleButton关闭;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.SimpleButton simpleButton修改;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit接诊历史;
        private DevExpress.XtraEditors.SimpleButton simpleButton删除;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
    }
}