﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace HIS.InterFaceJKDAN
{
    public partial class WebBro : Form
    {
        /// <summary>
        /// 打开web健康档案，有两种打开方式
        /// 1.MyUrl赋值打开个人档案
        /// 2.sSub赋值定位到接诊记录
        /// </summary>
        public WebBro()
        {
            InitializeComponent();            
        }

        #region 变量
        private string _myUrl = "";
        /// <summary>
        /// 传入档案号或身份证号，定位到个人信息
        /// </summary>
        public string MyUrl
        {
            get
            {
                return _myUrl;
            }
            set
            {
                string danh = s获取档案号(value);
                if (danh != "")
                    _myUrl = "http://192.168.10.216:9080/sdcsm/healthArchives/content.action?dJtdabh=" + danh + "&dGrdabh=" + danh + "&status=/sdcsm/healthArchives/showGr.action?dGrdabh=" + danh;
                else
                    _myUrl = "";
            }
        }

        private string _sSub = "";

        /// <summary>
        /// 传入档案号或身份证号，定位到接诊记录
        /// </summary>
        public string sSub
        {
            set
            {
                string danh = s获取档案号(value);
                if (danh != "")
                    _sSub = "http://192.168.10.216:9080/sdcsm/healthArchives/content.action?dJtdabh=" + danh + "&dGrdabh=" + danh + "&status=/sdcsm/reception/showTjkjktjSkip.action?grdabh=" + danh;
                else
                    _sSub = "";
            }
        }

        public string s获取档案号(string s身份证号)
        {
            try
            {
                if (s身份证号 == "")
                {
                    MessageBox.Show("传入身份证号或档案号不能为空！", "异常消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return "";
                }
                string sql = "select 档案号码 from T_健康档案 where 身份证号='" + s身份证号 + "' or  档案号码='" + s身份证号 + "'";
                return InterFaceJKDAN.Class.SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, sql).Tables[0].Rows[0]["档案号码"].ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("获取个人健康档案号失败！", "异常消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return "";
        }

        
        #endregion

        string strURL = "";
        string LoginUrl = ""; 
        private void WebBro_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            strURL = "http://192.168.10.216:9080/sdcsm/logout.action";
            LoginUrl = "http://192.168.10.216:9080/sdcsm/index.action";
            this.webBrowser1.Navigate(new Uri(strURL));
        }        

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.ToString().Trim() == strURL.Trim())
            {
                //登陆页已经加载完成进行登陆
                webBrowser1.Document.GetElementById("loginname").InnerText = "sp";//txtuser.Text.ToString().Trim();//fill name
                webBrowser1.Document.GetElementById("password").InnerText = "aaaaaa";// txtpwd.Text.ToString().Trim();//fill pwd
                HtmlElement formLogin = webBrowser1.Document.Forms["loginForm"];
                formLogin.InvokeMember("submit");
                Thread.Sleep(500);
                //herfclick("http://192.168.10.216:9080/sdcsm/healthArchives/showGr.action?dGrdabh=371323190050021802");//这是登录成功的操作
                //注意不是直接跳过去的,模拟点击链接
                //SESSION不会丢失
                if (_myUrl != "")
                    webBrowser1.Navigate(new Uri(_myUrl));
                if (_sSub!="")
                    webBrowser1.Navigate(new Uri(_sSub));
            }
            if (e.Url.ToString().Trim() == _myUrl.Trim())//判断登陆后的url,验证是否登陆成功
            {
                
            }
        }
        private void herfclick(string url)
        {
            for (int i = 0; i < webBrowser1.Document.All.Count; i++)
            {
                //if (webBrowser1.Document.All[i].TagName == "A" && webBrowser1.Document.All[i].GetAttribute("href").ToString().Trim() == url)
                //{
                //    webBrowser1.Document.All[i].InvokeMember("click");//引发”CLICK”事件
                //    break;
                //}
                if (webBrowser1.Document.All[i].TagName == "A" )
                {
                    if (webBrowser1.Document.All[i].GetAttribute("href").ToString().Trim() == url)
                    {
                        webBrowser1.Document.All[i].InvokeMember("click");//引发”CLICK”事件
                        break;
                    }
                }
            }
        }

    }
}
