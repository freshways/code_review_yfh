﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace HIS.InterFaceJKDAN
{
    /// <summary>
    /// 接口基础类，登陆时加载
    /// 调用时需要传入用户ID，调用方法GetUserInfo(用户ID)
    /// </summary>
    public class DefaultInfo
    {
        public static string s所属机构 { get; set; }

        public static string s登陆人编码 { get; set; }

        public static string s登陆人名称 { get; set; }

        //DESEncrypt.DESDeCode(Properties.Settings.Default.sqlConnString)
        public static String SQLConn
        {
            get
            {
                return "Data Source=192.168.10.121;Initial Catalog=AtomEHR.YSDB;User ID=yggsuser;Password=yggsuser";
                //return "Data Source=192.168.10.100;Initial Catalog=YSDB;User ID=yggsuser;Password=yggsuser";
            }
        }

        /// <summary>
        /// 根据传入的ID筛选用户对照表，取出用户属性
        /// </summary>
        /// <param name="s用户ID">用户ID</param>
        /// <param name="SysTemp">对应系统</param>
        /// <param name="sDwbm">如果是HIS系统，需要传入单位编码</param>
        public static void GetUserInfo(string s用户ID,string SysTemp="HIS",string sDwbm="")
        {
            string userID = s用户ID == "" ? "admin" : s用户ID;
            string sql = "select 对应用户id,对应用户姓名,拼音代码,所属机构 from InterFace_用户对照 "+
                                "where 对应系统='" + SysTemp + "'  ";
            if (!string.IsNullOrEmpty(sDwbm))
                sql += " and 单位编码='" + sDwbm + "' ";
            if (!string.IsNullOrEmpty(s用户ID))
                sql += " and 用户id='" + userID + "' ";
            try
            {
                DataTable dt = InterFaceJKDAN.Class.SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, sql).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    DefaultInfo.s所属机构 = dt.Rows[0]["所属机构"].ToString();//"371323B10019";
                    DefaultInfo.s登陆人编码 = dt.Rows[0]["对应用户id"].ToString();//"371323B100190001";
                    DefaultInfo.s登陆人名称 = dt.Rows[0]["对应用户姓名"].ToString();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("未找到用户对照关系！请联系管理员进行设置！", "提示");                    
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "提示");
            }             
        }
    }
}
