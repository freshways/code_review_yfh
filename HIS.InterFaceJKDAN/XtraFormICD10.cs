﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using System.Diagnostics;
using DevExpress.Data;

namespace HIS.InterFaceJKDAN
{
    public partial class XtraFormICD10 : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtICD10 = null;
        private bool b可否筛选 = false;
        public ICDinfo icd = new ICDinfo();

        public XtraFormICD10()
        {
            InitializeComponent();
        }

        private void simpleButton退出_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void treeList分类_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            try
            {
                if (b可否筛选 == false)
                {
                    return;
                }
                int iKind;
                int.TryParse(treeList分类.FocusedNode.Tag.ToString(), out iKind);
                dtICD10.DefaultView.RowFilter = "kindid=" + iKind.ToString();
                refreshGrid();
            }
            catch (Exception ex)
            {

                string sErr = ex.Message;
                //Comm.WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
        private void refreshGrid()
        {
            gridView1.BestFitMaxRowCount = 100;
            gridView1.BestFitColumns();
        }
        private void XtraFormICD10_Load(object sender, EventArgs e)
        {
            DataTable dtICD中类;
            DataTable dtICD大类;
            try
            {
                string SQL_大类 =
                "SELECT BigKindCode, BigKindName" + "\r\n" +
                "FROM   emr_pub_icd10大分类";
                dtICD大类 = InterFaceJKDAN.Class.SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, SQL_大类).Tables[0];

                string SQL_中类 =
                "SELECT ID, ParentID, theLevel, KindName, KindHeadPy, BigKindCode, ICDRange " + "\r\n" +
                "FROM EMR_PUB_ICD10中分类";
                dtICD中类 = InterFaceJKDAN.Class.SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, SQL_中类).Tables[0];
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                //Comm.WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                foreach (DataRowView rowMbIndex in dtICD大类.DefaultView)
                {
                    TreeListNode nodeICD大类 = this.treeList分类.AppendNode(new object[] { rowMbIndex["BigKindName"].ToString() }, null);
                    nodeICD大类.ImageIndex = 4;
                    nodeICD大类.SelectImageIndex = 4;
                    nodeICD大类.Tag = rowMbIndex["BigKindCode"].ToString();

                    DataRow[] rows0 = dtICD中类.Select("thelevel=0 and BigKindCode='" + nodeICD大类.Tag + "'");
                    if (rows0.Length > 0)
                    {
                        foreach (DataRow row_0 in rows0)
                        {
                            TreeListNode nodeICD中类_0 = this.treeList分类.AppendNode(new object[] { row_0["KindName"].ToString() }, nodeICD大类);
                            nodeICD中类_0.ImageIndex = 1;
                            nodeICD中类_0.SelectImageIndex = 0;
                            nodeICD中类_0.Tag = row_0["ID"].ToString();


                            DataRow[] rows1 = dtICD中类.Select("thelevel=1 and parentid='" + nodeICD中类_0.Tag + "'");
                            if (rows1.Length > 0)
                            {
                                foreach (DataRow row_1 in rows1)
                                {
                                    TreeListNode nodeICD中类_1 = this.treeList分类.AppendNode(new object[] { row_1["KindName"].ToString() }, nodeICD中类_0);
                                    nodeICD中类_1.ImageIndex = 1;
                                    nodeICD中类_1.SelectImageIndex = 0;
                                    nodeICD中类_1.Tag = row_1["ID"].ToString();


                                    DataRow[] rows2 = dtICD中类.Select("thelevel=2 and parentid='" + nodeICD中类_1.Tag + "'");
                                    if (rows2.Length > 0)
                                    {
                                        foreach (DataRow row_2 in rows2)
                                        {
                                            TreeListNode nodeICD中类_2 = this.treeList分类.AppendNode(new object[] { row_2["KindName"].ToString() }, nodeICD中类_1);
                                            nodeICD中类_2.ImageIndex = 1;
                                            nodeICD中类_2.SelectImageIndex = 0;
                                            nodeICD中类_2.Tag = row_2["ID"].ToString();


                                            DataRow[] rows3 = dtICD中类.Select("thelevel=3 and parentid='" + nodeICD中类_2.Tag + "'");
                                            if (rows3.Length > 0)
                                            {
                                                foreach (DataRow row_3 in rows3)
                                                {
                                                    TreeListNode nodeICD中类_3 = this.treeList分类.AppendNode(new object[] { row_3["KindName"].ToString() }, nodeICD中类_2);
                                                    nodeICD中类_3.ImageIndex = 1;
                                                    nodeICD中类_0.SelectImageIndex = 0;
                                                    nodeICD中类_3.Tag = row_3["ID"].ToString();
                                                }
                                            }

                                        }
                                    }

                                }
                            }

                        }
                    }

                }
                dtICD10 = ClassICD10.getICD10();
                gridControl1.DataSource = dtICD10;
                gridView1.PopulateColumns();
                gridView1.Columns["KindID"].Visible = false;
                this.gridView1.Columns["编码"].SummaryItem.FieldName = "编码";
                this.gridView1.Columns["编码"].SummaryItem.SummaryType = SummaryItemType.Count;
                gridView1.BestFitMaxRowCount = 1;
                gridView1.BestFitColumns();

                refreshGrid();
                b可否筛选 = true;
            }
            catch (Exception ex)
            {

                string sErr = ex.Message;
                //Comm.WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void simpleButton查询_Click(object sender, EventArgs e)
        {
            try
            {
                string sFilter = "";
                if (textEdit编码.Text != "")
                {
                    sFilter += "and 编码 like '*" + textEdit编码.Text.ToUpper() + "*'";
                }
                if (textEdit名称.Text != "")
                {
                    sFilter += "and 名称 like '*" + textEdit名称.Text + "*'";
                }
                if (textEdit拼音.Text != "")
                {
                    sFilter += "and 拼音首字母 like '*" + textEdit拼音.Text + "*'";
                }
                if (sFilter.Substring(0, 3) == "and")
                {
                    sFilter = sFilter.Substring(4, sFilter.Length - 4);
                }
                dtICD10.DefaultView.RowFilter = sFilter;
                refreshGrid();

            }
            catch (Exception ex)
            {

                string sErr = ex.Message;
                //Comm.WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                int iSelected = gridView1.SelectedRowsCount;
                if (iSelected == 0)
                {
                    MessageBox.Show("请选择病种", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                icd.ICD10 = gridView1.GetDataRow(gridView1.FocusedRowHandle)["编码"].ToString();
                icd.ICD10Name = gridView1.GetDataRow(gridView1.FocusedRowHandle)["名称"].ToString();
                icd.s首字母拼音 = gridView1.GetDataRow(gridView1.FocusedRowHandle)["拼音首字母"].ToString();

                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                //Comm.WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
        }

    }
}