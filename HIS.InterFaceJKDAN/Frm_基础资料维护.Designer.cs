﻿namespace HIS.InterFaceJKDAN
{
    partial class Frm_基础资料维护
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_基础资料维护));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton保存 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit是否禁用 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit拼音代码 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.textEdit项目 = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否禁用.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit拼音代码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit项目.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.simpleButton保存);
            this.layoutControl1.Controls.Add(this.comboBoxEdit是否禁用);
            this.layoutControl1.Controls.Add(this.textEdit拼音代码);
            this.layoutControl1.Controls.Add(this.textEdit项目);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(520, 264, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(375, 159);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton取消.Appearance.Options.UseFont = true;
            this.simpleButton取消.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton取消.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton取消.Image")));
            this.simpleButton取消.Location = new System.Drawing.Point(292, 121);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(76, 31);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 8;
            this.simpleButton取消.Text = "取 消";
            // 
            // simpleButton保存
            // 
            this.simpleButton保存.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton保存.Appearance.Options.UseFont = true;
            this.simpleButton保存.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton保存.Image")));
            this.simpleButton保存.Location = new System.Drawing.Point(212, 121);
            this.simpleButton保存.Name = "simpleButton保存";
            this.simpleButton保存.Size = new System.Drawing.Size(76, 31);
            this.simpleButton保存.StyleController = this.layoutControl1;
            this.simpleButton保存.TabIndex = 7;
            this.simpleButton保存.Text = "保 存";
            this.simpleButton保存.Click += new System.EventHandler(this.simpleButton保存_Click);
            // 
            // comboBoxEdit是否禁用
            // 
            this.comboBoxEdit是否禁用.EditValue = "否";
            this.comboBoxEdit是否禁用.Location = new System.Drawing.Point(58, 97);
            this.comboBoxEdit是否禁用.Name = "comboBoxEdit是否禁用";
            this.comboBoxEdit是否禁用.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit是否禁用.Properties.Items.AddRange(new object[] {
            "是",
            "否"});
            this.comboBoxEdit是否禁用.Size = new System.Drawing.Size(310, 20);
            this.comboBoxEdit是否禁用.StyleController = this.layoutControl1;
            this.comboBoxEdit是否禁用.TabIndex = 6;
            this.comboBoxEdit是否禁用.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_基础资料维护_KeyPress);
            // 
            // textEdit拼音代码
            // 
            this.textEdit拼音代码.Location = new System.Drawing.Point(58, 73);
            this.textEdit拼音代码.Name = "textEdit拼音代码";
            this.textEdit拼音代码.Size = new System.Drawing.Size(310, 20);
            this.textEdit拼音代码.StyleController = this.layoutControl1;
            this.textEdit拼音代码.TabIndex = 5;
            this.textEdit拼音代码.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_基础资料维护_KeyPress);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(375, 159);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit项目;
            this.layoutControlItem1.CustomizationFormText = "项      目";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(365, 66);
            this.layoutControlItem1.Text = "项      目";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit拼音代码;
            this.layoutControlItem2.CustomizationFormText = "拼音代码";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 66);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem2.Text = "拼音代码";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.comboBoxEdit是否禁用;
            this.layoutControlItem3.CustomizationFormText = "是否禁用";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem3.Text = "是否禁用";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton保存;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(205, 114);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(80, 35);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(80, 35);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(80, 35);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton取消;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(285, 114);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(80, 35);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(80, 35);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(80, 35);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 114);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(205, 35);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // textEdit项目
            // 
            this.textEdit项目.Location = new System.Drawing.Point(58, 7);
            this.textEdit项目.Name = "textEdit项目";
            this.textEdit项目.Size = new System.Drawing.Size(310, 62);
            this.textEdit项目.StyleController = this.layoutControl1;
            this.textEdit项目.TabIndex = 4;
            this.textEdit项目.UseOptimizedRendering = true;
            this.textEdit项目.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_基础资料维护_KeyPress);
            // 
            // Frm_基础资料维护
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 159);
            this.Controls.Add(this.layoutControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_基础资料维护";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "基础资料维护";
            this.Load += new System.EventHandler(this.Frm_基础资料维护_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_基础资料维护_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit是否禁用.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit拼音代码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit项目.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraEditors.SimpleButton simpleButton保存;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit是否禁用;
        private DevExpress.XtraEditors.TextEdit textEdit拼音代码;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.MemoEdit textEdit项目;
    }
}