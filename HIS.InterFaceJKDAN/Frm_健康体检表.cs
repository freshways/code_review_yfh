﻿using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using HIS.InterFaceJKDAN.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_健康体检表 : XtraForm
    {
        #region Fields

        DataSet _ds健康体检;
        string docNo;
        string familyNo;
        string date;

        #endregion

        public Frm_健康体检表()
        {
            InitializeComponent();
        }
        public Frm_健康体检表(string docNo)
        {
            this.InitializeComponent();
            DataSet ds健康体检 = this.Get体检数据(docNo);
            this.DoBindingDataSource(ds健康体检);
        }
        public DataSet Get体检数据(string docNo)
        {
            string commandText = "SELECT TOP 1 *\r\n                                          FROM [dbo].[View_健康体检表] a\r\n                                          where a.个人档案编号 ='" + docNo + "' order by a.体检日期 desc";
            return SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, commandText);
        }
        #region Handle Events
        private void frm健康体检表2_Load(object sender, EventArgs e)
        {
            InitView();
        }
     

        #endregion

        #region Private Method
        /// <summary>
        /// 第一个checkbox是否选中来决定 后续的checkbook的enable状态
        /// </summary>
        /// <param name="firstChk"></param>
        public void SetControlEnable(CheckEdit firstChk)
        {
            bool flag = firstChk.Checked;
            FlowLayoutPanel container = firstChk.Parent as FlowLayoutPanel;
            if (container == null) return;
            Control.ControlCollection controlList = container.Controls;
            for (int i = 1; i < controlList.Count; i++)
            {
                if (controlList[i] is CheckEdit)
                {
                    CheckEdit item = (CheckEdit)controlList[i];
                    item.Enabled = !flag;
                    if (flag)//选中了第一个控件
                    {
                        item.Checked = false;
                    }
                }
                else
                {
                    controlList[i].Enabled = false;
                }
            }
        }

        public void SetTextEditEnable(Control control, TextEdit textEdit)
        {
            if (control is CheckEdit)//CheckEdit 类型的其他
            {
                CheckEdit check = (CheckEdit)control;
                textEdit.Enabled = check.Checked;
            }
            else if (true)
            {

            }
        }
        private void DoBindingDataSource(DataSet _ds健康体检)
        {
            if (_ds健康体检 != null)
            {
                DataTable dataTable = _ds健康体检.Tables[0];
                DataRow dataRow = dataTable.Rows[0];
                string text = dataRow["出生日期"].ToString();
                if (!string.IsNullOrEmpty(text))
                {
                    DateTime now = DateTime.Now;
                    if (DateTime.TryParse(text, out now))
                    {
                        if (DateTime.Now.Year - now.Year >= 65)
                        {
                            this.group老年人.Visibility = LayoutVisibility.Always;
                        }
                        else
                        {
                            this.group老年人.Visibility = LayoutVisibility.Never;
                        }
                    }
                }
                string a = dataRow["性别"].ToString();
                if (a == "男")
                {
                    this.group妇科.Visibility = LayoutVisibility.Never;
                }
                else
                {
                    this.group妇科.Visibility = LayoutVisibility.Always;
                }
                this.DoBindingSummaryEditor(dataRow);
            }
        }
        private void DoBindingSummaryEditor(object dataSource)
        {
            DataRow dataRow = (DataRow)dataSource;
            this.date = dataRow["创建时间"].ToString();
            this.txt个人档案编号.Text = dataRow["个人档案编号"].ToString();
            this.txt姓名.Text = Common.DES解密(dataRow["姓名"].ToString());
            this.txt性别.Text = dataRow["性别"].ToString();
            this.txt身份证号.Text = dataRow["身份证号"].ToString();
            this.txt出生日期.Text = dataRow["出生日期"].ToString();
            this.txt联系电话.Text = dataRow["本人电话"].ToString();
            this.txt居住地址.Text = string.Concat(new string[]
			{
				dataRow["市"].ToString().PadRight(10),
				dataRow["区"].ToString().PadRight(10),
				dataRow["街道"].ToString().PadRight(10),
				dataRow["居委会"].ToString().PadRight(10),
				dataRow["居住地址"].ToString()
			});
            this.txt体检日期.Text = dataRow["体检日期"].ToString();
            this.txt责任医生.Text = dataRow["FIELD2"].ToString();
            this.txt症状.Text = dataRow["症状"].ToString() + dataRow["症状其他"].ToString();
            this.txt体温.Text = ((dataRow["体温"] == DBNull.Value) ? "" : dataRow["体温"].ToString());
            this.txt脉率.Text = ((dataRow["脉搏"] == DBNull.Value) ? "" : dataRow["脉搏"].ToString());
            this.txt呼吸频率.Text = ((dataRow["呼吸"] == DBNull.Value) ? "" : dataRow["呼吸"].ToString());
            this.txt左侧血压.Text = dataRow["血压左侧1"].ToString() + "/" + dataRow["血压左侧2"].ToString() + "  mmHg";
            this.txt左侧原因.Text = dataRow["左侧原因"].ToString();
            this.txt血压右侧.Text = dataRow["血压右侧1"].ToString() + "/" + dataRow["血压右侧2"].ToString() + "  mmHg";
            this.txt右侧原因.Text = dataRow["右侧原因"].ToString();
            this.txt身高.Text = ((dataRow["身高"] == DBNull.Value) ? "" : dataRow["身高"].ToString());
            this.txt体重.Text = ((dataRow["体重"] == DBNull.Value) ? "" : dataRow["体重"].ToString());
            this.txt腰围.Text = ((dataRow["腰围"] == DBNull.Value) ? "" : dataRow["腰围"].ToString());
            this.txt体重指数.Text = dataRow["体重指数"].ToString();
            this.txt老年人健康状态自我评估.Text = dataRow["老年人状况评估"].ToString();
            this.txt老年人生活自理能力自我评估.Text = dataRow["老年人自理评估"].ToString();
            this.txt老年人认知能力.Text = dataRow["老年人认知"].ToString().PadRight(5) + dataRow["老年人认知分"].ToString();
            this.txt老年人情感状态.Text = dataRow["老年人情感"].ToString().PadRight(5) + dataRow["老年人情感分"].ToString();
            this.txt锻炼频率.Text = dataRow["锻炼频率"].ToString();
            if (this.txt锻炼频率.Text == "不锻炼")
            {
                this.group体育锻炼.Visibility = LayoutVisibility.Never;
            }
            else
            {
                this.group体育锻炼.Visibility = LayoutVisibility.Always;
            }
            this.txt每次锻炼时间.Text = dataRow["每次锻炼时间"].ToString() + "分钟";
            this.txt坚持锻炼时间.Text = dataRow["坚持锻炼时间"].ToString() + "年";
            this.txt锻炼方式.Text = dataRow["锻炼方式"].ToString();
            this.txt饮食习惯.Text = dataRow["饮食习惯"].ToString();
            this.txt吸烟状况.Text = dataRow["吸烟状况"].ToString();
            if (this.txt吸烟状况.Text == "不吸烟")
            {
                this.group吸烟情况.Visibility = LayoutVisibility.Never;
            }
            else
            {
                this.group吸烟情况.Visibility = LayoutVisibility.Always;
            }
            this.txt日吸烟量.Text = "平均" + dataRow["日吸烟量"].ToString() + "（支）";
            this.txt开始吸烟年龄.Text = dataRow["开始吸烟年龄"].ToString() + "岁";
            this.txt戒烟年龄.Text = dataRow["戒烟年龄"].ToString() + "岁";
            this.txt饮酒频率.Text = dataRow["饮酒频率"].ToString();
            if (this.txt饮酒频率.Text == "从不")
            {
                this.group饮酒情况.Visibility = LayoutVisibility.Never;
            }
            else
            {
                this.group饮酒情况.Visibility = LayoutVisibility.Always;
            }
            this.txt日饮酒量.Text = "平均" + dataRow["日饮酒量"].ToString() + "（两）";
            this.txt是否戒酒.Text = dataRow["是否戒酒"].ToString();
            this.txt戒酒年龄.Text = dataRow["戒酒年龄"].ToString() + "岁";
            this.txt开始饮酒年龄.Text = dataRow["开始饮酒年龄"].ToString() + "岁";
            this.txt近一年内是否曾醉酒.Text = dataRow["近一年内是否曾醉酒"].ToString();
            this.txt饮酒种类.Text = dataRow["饮酒种类"].ToString() + dataRow["饮酒种类其它"].ToString();
            string text = dataRow["有无职业病"].ToString();
            this.lbl职业病有无.Text = text;
            if (text == "有")
            {
                this.group职业病.Visibility = LayoutVisibility.Always;
                this.lbl工种及从业时间.Text = string.Concat(new object[]
				{
					dataRow["具体职业"].ToString().PadRight(5),
					"从业时间",
					dataRow["从业时间"],
					"  年"
				});
                this.txt粉尘.Text = dataRow["粉尘"].ToString();
                this.txt粉尘防护措施.Text = dataRow["粉尘防护有无"].ToString().PadRight(5) + dataRow["粉尘防护措施"];
                this.txt放射物质.Text = dataRow["放射物质"].ToString();
                this.txt放射物质防护措施.Text = dataRow["放射物质防护措施有无"].ToString().PadRight(5) + dataRow["放射物质防护措施其他"];
                this.txt物理因素.Text = dataRow["物理因素"].ToString();
                this.txt物理因素防护措施.Text = dataRow["物理防护有无"].ToString().PadRight(5) + dataRow["物理防护措施"];
                this.txt化学物质.Text = dataRow["化学物质"].ToString();
                this.txt化学物质防护措施.Text = dataRow["化学物质防护"].ToString().PadRight(5) + dataRow["化学物质具体防护"];
                this.txt职业病其他.Text = dataRow["职业病其他"].ToString();
                this.txt职业病其他防护措施.Text = dataRow["其他防护有无"].ToString().PadRight(5) + dataRow["其他防护措施"];
            }
            else
            {
                this.group职业病.Visibility = LayoutVisibility.Never;
            }
            this.txt口唇.Text = dataRow["口唇"].ToString() + dataRow["口唇其他"];
            this.txt齿列有无.EditValue = dataRow["齿列"];
            if (dataRow["齿列"].ToString() != "正常,")
            {
                this.group齿列.Visibility = LayoutVisibility.Always;
                this.lbl齿列正常.Visibility = LayoutVisibility.Never;
                if (!string.IsNullOrEmpty(dataRow["齿列缺齿"].ToString()) && dataRow["齿列缺齿"].ToString() != "@@@")
                {
                    string[] array = dataRow["齿列缺齿"].ToString().Split(new char[]
					{
						'@'
					});
                    this.txt缺齿1.Text = array[0];
                    this.txt缺齿2.Text = array[1];
                    this.txt缺齿3.Text = array[2];
                    this.txt缺齿4.Text = array[3];
                }
                else
                {
                    this.lbl缺齿.Visibility = LayoutVisibility.Never;
                }
                if (!string.IsNullOrEmpty(dataRow["齿列龋齿"].ToString()) && dataRow["齿列龋齿"].ToString() != "@@@")
                {
                    string[] array2 = dataRow["齿列龋齿"].ToString().Split(new char[]
					{
						'@'
					});
                    this.txt龋齿1.Text = array2[0];
                    this.txt龋齿2.Text = array2[1];
                    this.txt龋齿3.Text = array2[2];
                    this.txt龋齿4.Text = array2[3];
                }
                else
                {
                    this.lbl龋齿.Visibility = LayoutVisibility.Never;
                }
                if (!string.IsNullOrEmpty(dataRow["齿列义齿"].ToString()) && dataRow["齿列义齿"].ToString() != "@@@")
                {
                    string[] array3 = dataRow["齿列义齿"].ToString().Split(new char[]
					{
						'@'
					});
                    this.txt义齿1.Text = array3[0];
                    this.txt义齿2.Text = array3[1];
                    this.txt义齿3.Text = array3[2];
                    this.txt义齿4.Text = array3[3];
                }
                else
                {
                    this.lbl义齿.Visibility = LayoutVisibility.Never;
                }
                if (!string.IsNullOrEmpty(dataRow["齿列其他"].ToString()))
                {
                    string text2 = dataRow["齿列其他"].ToString();
                    this.txt齿列其他.Text = text2;
                }
                else
                {
                    this.lbl齿列其他.Visibility = LayoutVisibility.Never;
                }
            }
            else
            {
                this.group齿列.Visibility = LayoutVisibility.Never;
                this.lbl齿列正常.Visibility = LayoutVisibility.Always;
            }
            this.txt咽部.Text = dataRow["咽部"].ToString() + dataRow["咽部其他"];
            this.txt视力.Text = string.Concat(new object[]
			{
				"左眼 ",
				dataRow["左眼视力"],
				" 右眼 ",
				dataRow["右眼视力"],
				"  （矫正视力：左眼 ",
				dataRow["左眼矫正"],
				"  右眼 ",
				dataRow["右眼矫正"],
				")"
			});
            this.txt听力.Text = dataRow["听力"].ToString();
            this.txt运动功能.Text = dataRow["运动功能"].ToString();
            this.txt眼底.Text = dataRow["眼底"].ToString().PadRight(5) + dataRow["眼底异常"];
            this.txt皮肤.Text = dataRow["皮肤"].ToString().PadRight(5) + dataRow["皮肤其他"];
            this.txt巩膜.Text = dataRow["巩膜"].ToString().PadRight(5) + dataRow["巩膜其他"];
            this.txt淋巴结.Text = dataRow["淋巴结"].ToString().PadRight(5) + dataRow["淋巴结其他"];
            this.txt桶状胸.Text = dataRow["桶状胸"].ToString();
            this.txt呼吸音.Text = dataRow["呼吸音"].ToString().PadRight(5) + dataRow["呼吸音异常"];
            this.txt罗音.Text = dataRow["罗音"].ToString().PadRight(5) + dataRow["罗音异常"];
            this.txt心率.Text = dataRow["心率"].ToString() + "  次/分";
            this.txt心律.Text = dataRow["心律"].ToString();
            this.txt杂音.Text = dataRow["杂音"].ToString().PadRight(5) + dataRow["杂音有"];
            this.txt压痛.Text = dataRow["压痛"].ToString().PadRight(5) + dataRow["压痛有"];
            this.txt包块.Text = dataRow["包块"].ToString().PadRight(5) + dataRow["包块有"];
            this.txt肝大.Text = dataRow["肝大"].ToString().PadRight(5) + dataRow["肝大有"];
            this.txt脾大.Text = dataRow["脾大"].ToString().PadRight(5) + dataRow["脾大有"];
            this.txt移动性浊音.Text = dataRow["浊音"].ToString().PadRight(5) + dataRow["浊音有"];
            this.txt下肢水肿.Text = dataRow["下肢水肿"].ToString();
            this.txt足背动脉搏动.Text = dataRow["足背动脉搏动"].ToString();
            this.txt肛门指诊.Text = dataRow["肛门指诊"].ToString().PadRight(5) + dataRow["肛门指诊异常"];
            this.txt乳腺.Text = dataRow["乳腺"].ToString().PadRight(5) + dataRow["乳腺其他"];
            this.txt外阴.Text = dataRow["外阴"].ToString().PadRight(5) + dataRow["外阴异常"];
            this.txt阴道.Text = dataRow["阴道"].ToString().PadRight(5) + dataRow["阴道异常"];
            this.txt宫颈.Text = dataRow["宫颈"].ToString().PadRight(5) + dataRow["宫颈异常"];
            this.txt宫体.Text = dataRow["宫体"].ToString().PadRight(5) + dataRow["宫体异常"];
            this.txt附件.Text = dataRow["附件"].ToString().PadRight(5) + dataRow["附件异常"];
            this.txt查体其他.Text = dataRow["查体其他"].ToString();
            this.txt血红蛋白.Text = dataRow["血红蛋白"].ToString() + "g/L";
            this.txt白细胞.Text = dataRow["白细胞"].ToString() + "  ×10^9/L";
            this.txt血小板.Text = dataRow["血小板"].ToString() + "  ×10^9/L";
            this.txt血常规其他.EditValue = dataRow["血常规其他"];
            this.txt尿常规.EditValue = string.Concat(new object[]
			{
				"尿蛋白 ",
				dataRow["尿蛋白"],
				" 尿糖",
				dataRow["尿糖"],
				"  尿酮体  ",
				dataRow["尿酮体"],
				"  尿潜血  ",
				dataRow["尿潜血"],
				"  其他  ",
				dataRow["尿常规其他"]
			});
            this.txt空腹血糖.EditValue = dataRow["空腹血糖"].ToString() + "  mmol/L";
            this.txt餐后2H血糖.EditValue = dataRow["餐后2H血糖"].ToString() + "  mmol/L";
            this.txt心电图.EditValue = dataRow["心电图"].ToString().PadRight(5) + dataRow["心电图异常"];
            this.txt尿微量白蛋白.EditValue = dataRow["尿微量白蛋白"].ToString() + " mg/dL";
            this.txt大便潜血.EditValue = dataRow["大便潜血"];
            this.txt糖化血红蛋白.EditValue = dataRow["糖化血红蛋白"].ToString() + " %";
            this.txt乙型肝炎表面抗原.EditValue = dataRow["乙型肝炎表面抗原"];
            this.txt血清谷丙转氨酶.EditValue = dataRow["血清谷丙转氨酶"] + "  U/L";
            this.txt血清谷草转氨酶.EditValue = dataRow["血清谷草转氨酶"] + "  U/L";
            this.txt白蛋白.EditValue = dataRow["白蛋白"] + "  g/L";
            this.txt总胆红素.EditValue = dataRow["总胆红素"] + "  umol/L";
            this.txt结合胆红素.EditValue = dataRow["结合胆红素"] + "  umol/L";
            this.txt血清肌酐.EditValue = dataRow["血清肌酐"] + "  umol/L";
            this.txt血尿素氮.EditValue = dataRow["血尿素氮"] + "  mmol/L";
            this.txt血钾浓度.EditValue = dataRow["血钾浓度"] + "  umol/L";
            this.txt血钠浓度.EditValue = dataRow["血钠浓度"] + "  mmol/L";
            this.txt总胆固醇.EditValue = dataRow["总胆固醇"] + "  mmol/L";
            this.txt甘油三酯.EditValue = dataRow["甘油三酯"] + "  mmol/L";
            this.txt血清低密度脂蛋白胆固醇.EditValue = dataRow["血清低密度脂蛋白胆固醇"] + "  mmol/L";
            this.txt血清高密度脂蛋白胆固醇.EditValue = dataRow["血清高密度脂蛋白胆固醇"] + "  mmol/L";
            this.txt胸部X线片.EditValue = dataRow["胸部X线片"].ToString().PadRight(5) + dataRow["胸部X线片异常"];
            this.txtB超.EditValue = dataRow["B超"].ToString().PadRight(5) + dataRow["B超其他"];
            this.txt宫颈涂片.EditValue = dataRow["宫颈涂片"].ToString().PadRight(5) + dataRow["宫颈涂片异常"];
            this.txt辅助检查其他.EditValue = dataRow["辅助检查其他"];
            this.txt平和质.EditValue = dataRow["平和质"];
            this.txt气虚质.EditValue = dataRow["气虚质"];
            this.txt阳虚质.EditValue = dataRow["阳虚质"];
            this.txt阴虚质.EditValue = dataRow["阴虚质"];
            this.txt痰湿质.EditValue = dataRow["痰湿质"];
            this.txt湿热质.EditValue = dataRow["湿热质"];
            this.txt血瘀质.EditValue = dataRow["血瘀质"];
            this.txt气郁质.EditValue = dataRow["气郁质"];
            this.txt特秉质.EditValue = dataRow["特禀质"];
            this.txt脑血管疾病.EditValue = dataRow["脑血管疾病"].ToString().PadRight(5) + dataRow["脑血管疾病其他"];
            this.txt肾脏疾病.EditValue = dataRow["肾脏疾病"].ToString().PadRight(5) + dataRow["肾脏疾病其他"];
            this.txt心脏疾病.EditValue = dataRow["心脏疾病"].ToString().PadRight(5) + dataRow["心脏疾病其他"];
            this.txt血管疾病.EditValue = dataRow["血管疾病"].ToString().PadRight(5) + dataRow["血管疾病其他"];
            this.txt眼部疾病.EditValue = dataRow["眼部疾病"].ToString().PadRight(5) + dataRow["眼部疾病其他"];
            this.txt神经系统疾病.EditValue = dataRow["神经系统疾病"].ToString().PadRight(5) + dataRow["神经系统疾病其他"];
            this.txt其他系统疾病.EditValue = dataRow["其他系统疾病"].ToString().PadRight(5) + dataRow["其他系统疾病其他"];
            DataSet userValueByKey = this.GetUserValueByKey(this.docNo, this.date);
            if (userValueByKey != null)
            {
                this.gc非免疫规划预防接种史.DataSource = userValueByKey.Tables["非免疫规划预防接种史"];
                this.gc家庭病床史.DataSource = userValueByKey.Tables["家庭病床史"];
                this.gc住院史.DataSource = userValueByKey.Tables["住院史"];
                this.gc主要用药情况.DataSource = userValueByKey.Tables["用药情况表"];
            }
            if (dataRow["健康评价"].ToString() == "体检无异常" || string.IsNullOrEmpty(dataRow["健康评价"].ToString()))
            {
                this.txt健康评价.EditValue = "体检无异常";
            }
            else
            {
                if (dataRow["健康评价"].ToString() == "有异常")
                {
                    this.txt健康评价.EditValue = string.Concat(new object[]
					{
						dataRow["健康评价"].ToString(),
						Environment.NewLine,
						"异常1：",
						dataRow["健康评价异常1"],
						Environment.NewLine,
						"异常2：",
						dataRow["健康评价异常2"],
						Environment.NewLine,
						"异常3：",
						dataRow["健康评价异常3"],
						Environment.NewLine,
						"异常4：",
						dataRow["健康评价异常4"]
					});
                    this.lbl健康评价.MinSize = new Size(0, 60);
                }
            }
            this.txt健康指导.EditValue = dataRow["健康指导"];
            string text3 = dataRow["危险因素控制"].ToString();
            if (text3.IndexOf("减体重") != -1)
            {
                string text4 = dataRow["危险因素控制体重"].ToString();
                if (!string.IsNullOrEmpty(text4))
                {
                    text3 = text3.Insert(text3.IndexOf("减体重") + 4, "目标：" + text4 + "\r\n");
                }
            }
            if (text3.IndexOf("建议疫苗接种") != -1)
            {
                string text5 = dataRow["危险因素控制疫苗"].ToString();
                if (!string.IsNullOrEmpty(text5))
                {
                    text3 = text3.Insert(text3.IndexOf("建议疫苗接种") + 7, "：" + text5 + "\r\n");
                }
            }
            if (text3.IndexOf("其他") != -1)
            {
                string text5 = dataRow["危险因素控制其他"].ToString();
                if (!string.IsNullOrEmpty(text5))
                {
                    text3 = text3.Insert(text3.IndexOf("其他") + 2, "：" + text5);
                }
            }
            this.txt危险因素控制.Text = text3;
            this.txt创建人.EditValue = dataRow["创建人"].ToString();
            this.txt创建时间.EditValue = dataRow["创建时间"];
            this.txt最近更新时间.EditValue = dataRow["修改时间"];
            this.txt最近修改人.EditValue = dataRow["修改人"].ToString();
            this.txt创建机构.EditValue = dataRow["创建机构"].ToString();
            this.txt当前所属机构.EditValue = dataRow["所属机构"].ToString();
            this.layoutControl1.VerticalScroll.Value = 0;
        }

        public DataSet GetUserValueByKey(string docNo, string date)
        {
            string str = string.Concat(new string[]
			{
				" select * from [tb_健康体检_住院史]    where 个人档案编号 ='",
				docNo,
				"' and 创建日期 = '",
				date,
				"' and 类型 = '1'"
			});
            string str2 = string.Concat(new string[]
			{
				" select * from [tb_健康体检_住院史]    where 个人档案编号 ='",
				docNo,
				"' and 创建日期 = '",
				date,
				"' and 类型 = '2'"
			});
            string str3 = string.Concat(new string[]
			{
				" select * from [tb_健康体检_用药情况表]    where 个人档案编号 ='",
				docNo,
				"' and 创建时间 = '",
				date,
				"'"
			});
            string str4 = string.Concat(new string[]
			{
				" select * from [tb_健康体检_非免疫规划预防接种史]    where 个人档案编号 ='",
				docNo,
				"' and 创建日期 = '",
				date,
				"'"
			});
            DataSet dataSet = SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, str + str2 + str3 + str4);
            dataSet.Tables[0].TableName = "住院史";
            dataSet.Tables[1].TableName = "家庭病床史";
            dataSet.Tables[2].TableName = "用药情况表";
            dataSet.Tables[3].TableName = "非免疫规划预防接种史";
            return dataSet;
        }

        private void InitView()
        {
            DataTable dataTable = new DataTable();
            DataColumn column = new DataColumn("P_CODE", Type.GetType("System.String"));
            dataTable.Columns.Add(column);
            DataColumn column2 = new DataColumn("P_DESC", Type.GetType("System.String"));
            dataTable.Columns.Add(column2);
            DataRow dataRow = dataTable.NewRow();
            dataRow["P_CODE"] = "1";
            dataRow["P_DESC"] = "规律";
            dataTable.Rows.Add(dataRow);
            DataRow dataRow2 = dataTable.NewRow();
            dataRow2["P_CODE"] = "2";
            dataRow2["P_DESC"] = "间断";
            dataTable.Rows.Add(dataRow2);
            DataRow dataRow3 = dataTable.NewRow();
            dataRow3["P_CODE"] = "3";
            dataRow3["P_DESC"] = "不服药";
            dataTable.Rows.Add(dataRow3);
            this.lkp服药依从性.DisplayMember = "P_DESC";
            this.lkp服药依从性.ValueMember = "P_CODE";
            this.lkp服药依从性.DataSource = dataTable;
            this.txt呼吸频率.Margin = new System.Windows.Forms.Padding(0, 10, 0, 3);
            this.txt呼吸频率.Margin = new System.Windows.Forms.Padding(0, 10, 0, 3);
            this.txt体温.Properties.ReadOnly = true;
            this.txt体温.BackColor = Color.Empty;
            this.txt脉率.Properties.ReadOnly = true;
            this.txt脉率.BackColor = Color.Empty;
            this.txt呼吸频率.Properties.ReadOnly = true;
            this.txt呼吸频率.BackColor = Color.Empty;
            this.txt身高.Properties.ReadOnly = true;
            this.txt身高.BackColor = Color.Empty;
            this.txt体重.Properties.ReadOnly = true;
            this.txt体重.BackColor = Color.Empty;
            this.txt腰围.Properties.ReadOnly = true;
            this.txt腰围.BackColor = Color.Empty;
            this.txt体重指数.Properties.ReadOnly = true;
            this.txt体重指数.BackColor = Color.Empty;
        }
        #endregion

    }
}
