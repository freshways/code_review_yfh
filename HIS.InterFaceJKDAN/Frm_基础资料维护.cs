﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_基础资料维护 : DevExpress.XtraEditors.XtraForm
    {
        public delegate void MethodSave(m基础资料 s);
        public event MethodSave Save;

        m基础资料 m =new m基础资料();
        public Frm_基础资料维护()
        {
            InitializeComponent();
        }
        public Frm_基础资料维护(m基础资料 s)
        {
            InitializeComponent();
            m = s;//Edit修改绑定(s);            
        }

        private void Frm_基础资料维护_Load(object sender, EventArgs e)
        {
            Edit修改绑定();
        }

        private void Frm_基础资料维护_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            }
        }

        private void simpleButton保存_Click(object sender, EventArgs e)
        {
            //textEdit项目.DataBindings["Text"].WriteValue();
            //dt.AcceptChanges();
            //dt.TableName = "T_病人就诊主观资料";
            //ds.Tables.Add(dt);
            //DataSetUpdateByDataSet(ds, "T_病人就诊主观资料");
            //string sql = String.Format(StrSql,textEdit项目.Text,textEdit拼音代码.Text,comboBoxEdit是否禁用.Text);
            //int rows = SQLHelper.ExecuteSql(sql);
            //if (rows > 0)
            //    this.Close();
            if (Save != null)
                Save(m);
        }

        private void Edit修改绑定()
        {
            this.textEdit项目.DataBindings.Add("Text", m, "s项目", true);
            this.textEdit拼音代码.DataBindings.Add("Text", m, "s拼音");
            this.comboBoxEdit是否禁用.DataBindings.Add("Text", m, "s是否启用");
        }

        public DataSet DataSetUpdateByDataSet(DataSet ds, string strTblName)
        {
            SqlConnection conn = new SqlConnection(DefaultInfo.SQLConn);
            SqlDataAdapter myAdapter = new SqlDataAdapter();
            
            SqlCommand myCommand = new SqlCommand(("select * from " + strTblName), (SqlConnection)conn);
            myAdapter.SelectCommand = myCommand;
            SqlCommandBuilder myCommandBuilder = new SqlCommandBuilder(myAdapter);
            try
            {
                myAdapter.Update(ds, strTblName);
            }
            catch (Exception err)
            {
                conn.Close();
                throw err;
            } return ds;
        }

    }

    public class m基础资料
    {
        public int ID { get; set; }

        public string s项目 { get; set; }

        public string s拼音 { get; set; }

        private string _s是否启用 = "否";
        public string s是否启用
        { 
            get 
            {
                return _s是否启用;
            }
            set
            {
                _s是否启用 = value;
            }
        }
    }

}
