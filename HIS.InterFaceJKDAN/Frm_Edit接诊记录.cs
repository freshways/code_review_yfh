﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using HIS.InterFaceJKDAN.Class;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_Edit接诊记录 : DevExpress.XtraEditors.XtraForm
    {
        #region 变量

        string s操作人 = DefaultInfo.s登陆人编码;

        string s操作人姓名 = WEISHENG.COMM.zdInfo.ModelUserInfo.用户名 == "" ? DefaultInfo.s登陆人名称 : WEISHENG.COMM.zdInfo.ModelUserInfo.用户名; //如果weishengcom的登录人为空，则用对照表里面的用户名

        string s机构 = DefaultInfo.s所属机构;

        DataTable dt个人档案 = null;

        private String _s档案号 = "";
        private String _s机构号 = "";

        public String s档案号
        {
            set {
                _s档案号 = Get档案号(value);
            }
        }

        private string Get档案号(string sValue)
        {
            try
            {
                if (sValue == "")
                {
                    MessageBox.Show("传入身份证号或档案号错误！", "异常消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return "";
                }
                string dn编号 ="";
                dt个人档案 = Class.SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, 
                    "SELECT 个人档案编号 ,姓名 , 性别,出生日期 ,身份证号 ,居住地址 ,本人电话,联系人电话 , 所属机构 ,创建时间 ,"+
                    "修改时间 ,创建人 ,修改人 ,创建机构 from tb_健康档案 where 身份证号='" + sValue + "' or 个人档案编号='" + sValue + "'").Tables[0];
                if (dt个人档案 != null && dt个人档案.Rows.Count == 1)
                {
                    dn编号 = dt个人档案.Rows[0]["个人档案编号"].ToString();
                    _s机构号 = dt个人档案.Rows[0]["所属机构"].ToString();
                }
                if (dn编号 == "")
                {
                    MessageBox.Show("[" + sValue + "]未找到该病人信息！");
                    return "";
                }
                return dn编号;
                //string sql = "SELECT 档案号码 from T_健康档案 where 身份证号='" + sValue + "' or 档案号码='" + sValue + "'";
                //return Class.SqlHelper.ExecuteScalar(DefaultInfo.SQLConn, CommandType.Text, sql).ToString();
            }
            catch (Exception)
            {
            } return "";
        }

        bool i是否打印 = false;

        bool i修改 = false;//默认增加

        #endregion

        #region Load

        public Frm_Edit接诊记录(string s_Grdnh)
        {
            InitializeComponent();
            this.s档案号 = s_Grdnh;
        }


        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Frm_Edit接诊记录_Load(object sender, EventArgs e)
        {
            dateEdit接诊时间.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            this.textEdit接诊医生.Text = s操作人姓名;//.Focus();
            EditBind();
        }

        #endregion

        #region 菜单事件

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton新增_Click(object sender, EventArgs e)
        {
            f控件初始化();
            f控件可用();
        }

        private void simpleButton修改_Click(object sender, EventArgs e)
        {
            f控件可用();
            i修改 = true;
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton保存_Click(object sender, EventArgs e)
        {
            if (i修改)
                U修改保存();
            else
                S新增保存();
            EditBind();
        }        

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton打印_Click(object sender, EventArgs e)
        {
            if (!i是否打印)
            {
                MessageBox.Show("请先完善病人信息！" , "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Print();
        }


        private void simpleButton删除_Click(object sender, EventArgs e)
        {
            try
            {
                if (LookUpEdit接诊历史.Text == "")
                {
                    MessageBox.Show("没有可删除信息！" , "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string delSql = "delete from tb_接诊记录 where 个人档案编号='" + this.textEdit档案号.Text + "' and ID='" + this.LookUpEdit接诊历史.GetColumnValue("ID").ToString() + "' ";
                if (MessageBox.Show("是否要删除该接诊记录？此操作不可逆，请谨慎操作！", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    int Rows = SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, delSql);
                    if (Rows > 0)
                    {
                        MessageBox.Show("删除接诊记录成功！档案号码：" + textEdit档案号.Text, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Common.Log("删除接诊记录成功！档案号码：" + textEdit档案号.Text + " ID：" + this.LookUpEdit接诊历史.GetColumnValue("ID").ToString());
                    }
                    this.OnLoad(e);//EditBind();
                }
                else
                    return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("删除接诊记录失败！异常信息：" + ex.Message, "异常消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Common.Log("删除接诊记录失败！异常信息：" + ex.Message);
            }
        }

        #endregion

        #region 方法

        //private DataTable dt个人档案;
        private DataTable dt接诊信息;
        
        private void EditBind()
        {
            try
            {
                if (_s档案号 == "") return;

                //dt个人档案 = new DataTable();
                //string sql = "SELECT ID ,档案号码 ,姓名 ,case 性别 when '1' then '男' else '女' end 性别,出生日期 ," +
                //    "身份证号 ,居住地址 ,本人电话,联系人电话 ,所属机构 ,创建时间 ,修改时间 ,创建人 ,修改人 ,创建机构  " +
                //    "FROM YSDB.dbo.T_健康档案  where 1=1  and (档案号码='" + this._s档案号 + "')";

                //dt个人档案 = SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, sql).Tables[0];
                if (dt个人档案 != null && dt个人档案.Rows.Count > 0)
                {
                    this.textEdit档案号.Text = dt个人档案.Rows[0]["个人档案编号"].ToString();
                    this.textEdit姓名.Text = Common.DES解密(dt个人档案.Rows[0]["姓名"].ToString());
                    this.textEdit性别.Text = dt个人档案.Rows[0]["性别"].ToString();
                    this.textEdit身份证号.Text = dt个人档案.Rows[0]["身份证号"].ToString();
                    this.textEdit出生日期.Text = dt个人档案.Rows[0]["出生日期"].ToString();
                    this.textEdit联系电话.Text = dt个人档案.Rows[0]["本人电话"].ToString();
                    this.textEdit居住地址.Text = dt个人档案.Rows[0]["居住地址"].ToString();
                    //dt个人档案.Rows[0]["所属机构"].ToString()
                    this.textEdit所属机构.Text = Common.RetrunColunm("select 机构编号,机构名称 from dbo.tb_机构信息 where 机构编号='" + _s机构号 + "'; ", "机构名称");
                    //dt个人档案.Rows[0]["创建机构"].ToString()
                    this.textEdit创建机构.Text = Common.RetrunColunm("select 机构编号,机构名称 from dbo.tb_机构信息 where 机构编号='" + _s机构号 + "'; ", "机构名称");
                     //绑定已有的接诊信息
                    Bind接诊信息(dt个人档案.Rows[0]["个人档案编号"].ToString());
                    Common.Log("查看病人接诊信息！操作人：" + s操作人);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("打开病人信息失败！异常信息：" + e.Message, "异常消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Common.Log("打开病人信息失败！异常信息：" + e.Message);
            }
        }

        private void Bind接诊信息(string s档案号)
        {
            dt接诊信息 = new DataTable();
            string sql = "select ID,个人档案编号,接诊医生,主观资料,客观资料,评估,处置计划,接诊时间,创建人,修改人,创建时间,修改时间,创建机构,"+
                    "所属机构,缺项,完整度 from tb_接诊记录  " +
                    "where 个人档案编号='" + s档案号 + "' order by 接诊时间 desc, 创建时间 desc;";
            dt接诊信息 = SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, sql).Tables[0];

            if (dt接诊信息 != null && dt接诊信息.Rows.Count > 0)
            {
                //绑定历史选项
                LookUpEdit接诊历史.Properties.Columns.Clear();//绑定前先清空一下

                for (int i = 0; i < dt接诊信息.Columns.Count; i++)
                {
                    this.LookUpEdit接诊历史.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(dt接诊信息.Columns[i].ColumnName.ToString()));
                    if (dt接诊信息.Columns[i].ColumnName.ToString() != "接诊时间")
                        LookUpEdit接诊历史.Properties.Columns[dt接诊信息.Columns[i].ColumnName.ToString()].Visible = false;
                }
                //this.LookUpEdit接诊历史.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(dt接诊信息.Columns["ID"].ColumnName.ToString()));
                this.LookUpEdit接诊历史.Properties.DataSource = dt接诊信息;
                this.LookUpEdit接诊历史.Properties.DisplayMember = "接诊时间";
                this.LookUpEdit接诊历史.Properties.ValueMember = "ID";
                this.LookUpEdit接诊历史.ItemIndex = 0;

                //绑定
                i是否打印 = true;
                simpleButton保存.Visible = false;
                simpleButton新增.Visible = true;
                this.textEdit创建人.Text = Common.RetrunColunm("select 用户编码,UserName from dbo.tb_MyUser where 用户编码='" + dt接诊信息.Rows[0]["创建人"].ToString() + "'; ", "UserName");
                this.textEdit修改人.Text = Common.RetrunColunm("select 用户编码,UserName from dbo.tb_MyUser where 用户编码='" + dt接诊信息.Rows[0]["修改人"].ToString() + "'; ", "UserName");
                f控件不可用();
                layoutControlItem30.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;//隐藏修改按钮
            }
            else
            {
                LookUpEdit接诊历史.Properties.Columns.Clear();
                this.LookUpEdit接诊历史.Properties.DataSource = dt接诊信息;
                this.LookUpEdit接诊历史.ItemIndex = -1;
                f控件初始化();
                f控件可用();
            }
        }

        private void S新增保存()
        {
            try
            {
                if (!CheckSave())
                    return;
                string sql = "INSERT INTO  tb_接诊记录 (个人档案编号 ,接诊医生 ,主观资料 ,客观资料 ,评估 ,处置计划 ,创建机构 ,创建人 ," +
                    "修改人 , PRESENTREGION ,所属机构 ,创建时间 ,修改时间 ,接诊时间 ,缺项 ,完整度)  VALUES ('" +
                    textEdit档案号.Text + "','" + textEdit接诊医生.Text + "','" + textEdit主观资料.Text + "','" + textEdit客观资料.Text + "','"+
                    textEdit评估.Text + "','" + textEdit处置计划.Text + "','" + s机构 + "','" + s操作人 + "','" + s操作人 + "',null,'" + s机构 + "','" +
                    System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'," +
                    "convert(char(10),'" + dateEdit接诊时间.Text + "',120),'0','100') ";
                int rows = SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, sql);
                if (rows > 0)
                {
                    Common.Log("接诊记录保存成功！操作人：" + s操作人);
                    if (MessageBox.Show("保存成功！是否关闭？", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        this.Close();
                        return;
                    }
                }
                f控件不可用();
                //Print();
            }
            catch (Exception e)
            {
                MessageBox.Show("保存失败！异常信息：" + e.Message, "异常消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Common.Log("保存失败！异常信息：" + e.Message);
            }           
        }

        private void U修改保存()
        {
            try
            {
                if (!CheckSave())
                    return;
                string sql = "update   tb_接诊记录   set 接诊医生 ='" + textEdit接诊医生.Text + "',主观资料 ='" + textEdit主观资料.Text + "'," +
                    "客观资料 ='" + textEdit客观资料.Text + "',评估 ='" + textEdit评估.Text + "',处置计划 ='" + textEdit处置计划.Text + "' ," +
                    "修改人 ='" + s操作人 + "'  ,修改时间 ='" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'," +
                    "接诊时间 =convert(char(10),'" + dateEdit接诊时间.Text + "',120)  " +
                    " where 个人档案编号='" + textEdit档案号.Text + "' and ID='" + this.LookUpEdit接诊历史.GetColumnValue("ID").ToString() + "'";
                int rows = SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, sql);
                if (rows > 0)
                {
                    Common.Log("接诊记录修改成功！操作人：" + s操作人);
                    if (MessageBox.Show("修改成功,是否关闭？", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        this.Close();
                        return;
                    }
                }
                f控件不可用();
                i修改 = false;
            }
            catch (Exception e)
            {
                MessageBox.Show("保存失败！异常信息：" + e.Message, "异常消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Common.Log("保存失败！异常信息：" + e.Message);
            }      
        }

        private bool CheckSave()
        {
            if (dateEdit接诊时间.Text == "")
            {
                MessageBox.Show("请填写接诊时间！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (textEdit接诊医生.Text == "")
            {
                MessageBox.Show("请填写接诊医生！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.textEdit接诊医生.Focus();
                return false;
            }
            if (textEdit主观资料.Text == "")
            {
                MessageBox.Show("请填写主观资料！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.textEdit主观资料.Focus();
                return false;
            }
            if (textEdit客观资料.Text == "")
            {
                MessageBox.Show("请填写客观资料！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.textEdit客观资料.Focus();
                return false;
            }
            if (textEdit评估.Text == "")
            {
                MessageBox.Show("请填写评估！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.textEdit评估.Focus();
                return false;
            }
            if (textEdit处置计划.Text == "")
            {
                MessageBox.Show("请填写处置计划！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.textEdit处置计划.Focus();
                return false;
            }
            return true;
        }
        
        private void Print()
        {
            try
            {
                int age = System.DateTime.Now.Year - Convert.ToDateTime(textEdit出生日期.Text).Year;
                XtraReport接诊记录单 xrpt = new XtraReport接诊记录单(dt接诊信息, textEdit姓名.Text, textEdit性别.Text, age.ToString(), textEdit居住地址.Text);
                xrpt.ShowPreviewDialog();
                Common.Log("打印病人接诊信息！操作人：" + s操作人);
            }
            catch (Exception ex)
            {
                MessageBox.Show("打印失败！异常信息:\n" + ex.Message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void f控件不可用()
        {
            this.dateEdit接诊时间.Properties.ReadOnly = true;
            this.textEdit接诊医生.Properties.ReadOnly = true;
            this.textEdit主观资料.Properties.ReadOnly = true;
            this.textEdit客观资料.Properties.ReadOnly = true;
            this.textEdit评估.Properties.ReadOnly = true;
            this.textEdit处置计划.Properties.ReadOnly = true;
            simpleButton新增.Enabled = true;
            simpleButton修改.Enabled = true;
            simpleButton保存.Enabled = false;
        }

        private void f控件可用()
        {
            this.dateEdit接诊时间.Properties.ReadOnly = false;
            this.textEdit接诊医生.Properties.ReadOnly = false;
            this.textEdit主观资料.Properties.ReadOnly = false;
            this.textEdit客观资料.Properties.ReadOnly = false;
            this.textEdit评估.Properties.ReadOnly = false;
            this.textEdit处置计划.Properties.ReadOnly = false;
            simpleButton新增.Enabled = false;
            simpleButton修改.Enabled = false;
            simpleButton保存.Enabled = true;
        }

        private void f控件初始化()
        {
            this.dateEdit接诊时间.Text = System.DateTime.Now.ToString("yyyy-MM-dd ");
            this.textEdit接诊医生.Text = s操作人姓名;
            this.textEdit主观资料.Text = "";
            this.textEdit客观资料.Text = "";
            this.textEdit评估.Text = "";
            this.textEdit处置计划.Text = "";
            this.textEdit创建时间.Text = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            this.textEdit更新时间.Text = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            this.textEdit创建人.Text = DefaultInfo.s登陆人名称;
            this.textEdit修改人.Text = DefaultInfo.s登陆人名称;
            simpleButton保存.Enabled = true;
        }

        #endregion

        #region 按钮事件

        private void simpleButton主观资料_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from InterFace_接诊记录字典 where 类别='主观资料' ";
            Frm_选择病人就诊资料 frm = new Frm_选择病人就诊资料(sql, "名称", "病人就诊主观资料");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit主观资料.Text = frm.s选择项目;
            SendKeys.Send("{Tab}");
        }

        private void simpleButton客观资料_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from InterFace_接诊记录字典 where 类别='客观资料' ";
            Frm_选择病人就诊资料 frm = new Frm_选择病人就诊资料(sql, "名称", "病人就诊客观资料");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit客观资料.Text = frm.s选择项目;
            SendKeys.Send("{Tab}");
        }

        private void simpleButton评估_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from InterFace_接诊记录字典 where 类别='评估'  ";
            Frm_选择病人就诊资料 frm = new Frm_选择病人就诊资料(sql, "名称", "病人就诊评估");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit评估.Text = frm.s选择项目;
            SendKeys.Send("{Tab}");
        }

        private void simpleButton处置计划_Click(object sender, EventArgs e)
        {
            string sql = "select ID,名称,拼音代码,是否禁用 from InterFace_接诊记录字典 where 类别='处置计划'  ";
            Frm_选择病人就诊资料 frm = new Frm_选择病人就诊资料(sql, "名称", "病人就诊处置计划");
            if (frm.ShowDialog() == DialogResult.Cancel)
                return;
            if (frm.s选择项目 != "")
                textEdit处置计划.Text = frm.s选择项目;
            simpleButton保存.Focus();
            //simpleButton保存.PerformClick();
        }

        #endregion

        private void Frm_Edit接诊记录_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{Tab}");
            }
        }

        private void LookUpEdit接诊历史_EditValueChanged(object sender, EventArgs e)
        {
            if (LookUpEdit接诊历史.Properties.DataSource != null)
            {
                this.dateEdit接诊时间.Text = this.LookUpEdit接诊历史.GetColumnValue("接诊时间").ToString();
                this.textEdit接诊医生.Text = this.LookUpEdit接诊历史.GetColumnValue("接诊医生").ToString();
                this.textEdit主观资料.Text = this.LookUpEdit接诊历史.GetColumnValue("主观资料").ToString();
                this.textEdit客观资料.Text = this.LookUpEdit接诊历史.GetColumnValue("客观资料").ToString();
                this.textEdit评估.Text = this.LookUpEdit接诊历史.GetColumnValue("评估").ToString();
                this.textEdit处置计划.Text = this.LookUpEdit接诊历史.GetColumnValue("处置计划").ToString();
                this.textEdit创建时间.Text = this.LookUpEdit接诊历史.GetColumnValue("创建时间").ToString();
                this.textEdit更新时间.Text = this.LookUpEdit接诊历史.GetColumnValue("修改时间").ToString();
            }
        }        

    }
}
