﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HIS.InterFaceJKDAN.Class;

namespace HIS.InterFaceJKDAN
{
    public class Common
    {
        /// <summary>
        /// 操作日志
        /// </summary>
        /// <param name="s内容"></param>
        public static void Log(string s内容)
        {
            string sql = "insert into InterFace_日志(内容,备注,时间) values('" + s内容 + "','','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')";
            try
            {
                SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, sql);
            }
            catch  { } 
        }

        public static void Log(string s内容, string s备注)
        {
            string sql = "insert into InterFace_日志(内容,备注,时间) values('" + s内容 + "','" + s备注 + "','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')";
            try
            {
                SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, sql);
            }
            catch { }
        }

        /// <summary>
        /// 获取指定列值
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="colunm"></param>
        /// <returns></returns>
        public static String RetrunColunm(string sql,string colunm)
        {
            return SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, sql).Tables[0].Rows[0][colunm].ToString();
        }

        /// <summary>
        /// 个人姓名加密
        /// </summary>
        /// <param name="s原始字符串"></param>
        /// <returns></returns>
        public static String DES加密(string s原始字符串)
        {
            //if (s原始字符串.Length > 3)
            //{//return DESEncrypt.DESEnCode(s原始字符串);
            //}
            //else
            //{
            //    String jiemi = "";
            //    int len = s原始字符串.Length;
            //    for (int i = 0; i < len; i++)
            //    {
            //        jiemi += DESEncrypt.DESEnCode(s原始字符串.Substring(i, 1));
            //    }
            //    return jiemi;
            //}
            String jiemi = "";
            int len = s原始字符串.Length;
            for (int i = 0; i < len; i++)
            {
                jiemi += DESEncrypt.DESEnCode(s原始字符串.Substring(i, 1));
            }
            return jiemi;
        }

        /// <summary>
        /// 个人姓名解密
        /// </summary>
        /// <param name="s加密字符串"></param>
        /// <returns></returns>
        public static String DES解密(string s加密字符串)
        {
            if (s加密字符串.Length < 16) return s加密字符串;
            String jiemi = "";
            int len = s加密字符串.Length / 16;
            for (int i = 0; i < len; i++)
            {
                jiemi += DESEncrypt.DESDeCode(s加密字符串.Substring(i * 16, 16));
            }
            return jiemi;
            //textBox4.Text = Encrypt.DESDeCode(textBox3.Text);
        }

        private static DataTable _常用字典Property;
        public static DataTable 常用字典Property 
        { 
            get
            {
                if (_常用字典Property != null && _常用字典Property.Rows.Count > 0)
                    return _常用字典Property;
                else
                {
                    string sql = "SELECT * FROM [dbo].tb_常用字典  where P_FLAG=1";
                    _常用字典Property = InterFaceJKDAN.Class.SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, sql).Tables[0];
                    return _常用字典Property;
                }
            }
            set
            {
                _常用字典Property = value;
            }
        }
        /// <summary>
        /// 获取字典中的名称
        /// 2015-09-16 21:19:17 yufh 添加
        /// </summary>
        /// <param name="_类型"></param>
        /// <param name="_编码"></param>
        /// <returns></returns>
        public static string ReturnDis字典显示(string _类型, string _编码)
        {
            if (常用字典Property != null)
            {
                DataRow[] dr = 常用字典Property.Select("(P_FUN_CODE='" + _类型 + "' or P_FUN_DESC = '" + _类型 + "') and P_CODE='" + _编码 + "' ");

                if (dr.Length > 0)
                {
                    return dr[0]["P_DESC"].ToString();
                }
            }
            return _编码;
        }

    }
}
