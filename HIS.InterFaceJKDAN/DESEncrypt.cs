﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace HIS.InterFaceJKDAN
{
    public class DESEncrypt
    {
        #region DESjiava互通解密

        public const string THE_KEY = "zhonglianjiayu";
        public const string IV_KEY = "291a4100b5a51b49";

        #region 加密
        /// <summary>
        /// DES加密New
        /// </summary>
        /// <param name="pToEncrypt">要加密的字符串</param>
        /// <returns></returns>
        public static string DESEnCode(string pToEncrypt)
        {
            return DESEnCode(pToEncrypt, THE_KEY);
        }

        /// <summary>
        /// 加密算法
        /// </summary>
        /// <param name="pToEncrypt">要加密的字符串</param>
        /// <param name="Keys">密钥</param>
        /// <returns></returns>
        public static string DESEnCode(string pToEncrypt, string Keys)
        {
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            provider.Mode = CipherMode.ECB;//C#默认CBC模式，解密java需要设置ECB模式
            byte[] inputByteArray = Encoding.GetEncoding("GB2312").GetBytes(pToEncrypt);

            //建立加密对象的密钥和偏移量    
            //原文使用ASCIIEncoding.ASCII方法的GetBytes方法    
            //使得输入密码必须输入英文文本    
            provider.Key = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
            provider.IV = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, provider.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return byteArr2HexStr(ms.ToArray());
        }

        /// <summary>
        /// 将byte数组转换为表示16进制值的字符串 和public static byte[]    hexStr2ByteArr(String strIn) 互为可逆的转换过程 
        /// </summary>
        /// <param name="arrB"></param>
        /// <returns></returns>
        public static String byteArr2HexStr(byte[] arrB)
        {
            int iLen = arrB.Length;
            // 每个byte用两个字符才能表示，所以字符串的长度是数组长度的两倍   
            StringBuilder sb = new StringBuilder(iLen * 2);
            for (int i = 0; i < iLen; i++)
            {
                int intTmp = arrB[i];
                // 把负数转换为正数   
                while (intTmp < 0)
                {
                    intTmp = intTmp + 256;
                }
                // 小于0F的数需要在前面补0   
                if (intTmp < 16)
                {
                    sb.Append("0");
                }
                sb.Append(Convert.ToString(intTmp, 16));
            }
            return sb.ToString();
        } 
        #endregion

        #region 解密

        /// <summary>
        /// DES解密 -java des加密字符串
        /// </summary>
        /// <param name="str">加密字符串</param>
        /// <returns></returns>
        public static string DESDeCode(string pToDecrypt)
        {
            return DESDeCode(pToDecrypt, THE_KEY);
        }

        /// <summary>
        /// 解密java des加密字符串+1
        /// </summary>
        /// <param name="str">加密字符串</param>
        ///<param name="Keys">密钥</param>
        /// <returns></returns>
        public static string DESDeCode(string pToDecrypt, string Keys)
        {
            try
            {
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                provider.Mode = CipherMode.ECB;//C#默认CBC模式，解密java需要设置ECB模式
                // 密钥
                provider.Key = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                // 偏移量
                provider.IV = Encoding.ASCII.GetBytes(Keys.Substring(0, 8));
                byte[] buffer = hexStr2ByteArr(pToDecrypt);
                //byte[] buffer = new byte[pToDecrypt.Length / 2];
                //for (int i = 0; i < (pToDecrypt.Length / 2); i++)
                //{
                //    int num2 = Convert.ToInt32(pToDecrypt.Substring(i * 2, 2), 0x10);
                //    buffer[i] = (byte)num2;
                //}
                MemoryStream stream = new MemoryStream();
                CryptoStream stream2 = new CryptoStream(stream, provider.CreateDecryptor(), CryptoStreamMode.Write);
                stream2.Write(buffer, 0, buffer.Length);
                stream2.FlushFinalBlock();
                stream.Close();
                return Encoding.GetEncoding("GB2312").GetString(stream.ToArray());
            }
            catch (Exception) { return ""; }
        }

        /// <summary>
        ///将表示16进制值的字符串转换为byte数组 和public static String byteArr2HexStr(byte[] arrB)  * 互为可逆的转换过程 
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        public static byte[] hexStr2ByteArr(String strIn)
        {
            // 两个字符表示一个字节，所以字节数组长度是字符串长度除以2   
            int iLen = strIn.Length / 2;            
            byte[] arrOut = new byte[iLen];
            for (int i = 0; i < iLen; i++)
            {
                arrOut[i] = (byte)Convert.ToInt32(strIn.Substring(i * 2, 2), 16);
            }
            return arrOut;
        }
        #endregion

        #endregion

        #region DES对称加密解密

        public const string DEFAULT_ENCRYPT_KEY = "yufenghai";       

        /// <summary>
        /// 使用默认加密
        /// </summary>
        /// <param name="strText">加密的字符串</param>
        /// <returns></returns>
        public static string DesEncrypt(string strText)
        {
            try
            {
                return DesEncrypt(strText, DEFAULT_ENCRYPT_KEY);
            }
            catch
            {
                return "";
            }
        }        

        /// <summary> 
        /// 加密过程
        /// 注意:key 必须是8位
        /// </summary> 
        /// <param name="strText">加密的字符串</param> 
        /// <param name="strEncrKey">key</param> 
        /// <returns></returns> 
        public static string DesEncrypt(string strText, string strEncrKey)
        {
            byte[] byKey = null;
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

            byKey = Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Convert.ToBase64String(ms.ToArray());
        }

        /// <summary>
        /// 使用默认解密
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public static string DesDecrypt(string strText)
        {
            try
            {
                return DesDecrypt(strText, DEFAULT_ENCRYPT_KEY);
            }
            catch
            {
                return "";
            }
        }

        /// <summary> 
        /// 解密过程 
        /// 注意:key 必须是8位
        /// </summary> 
        /// <param name="strText">解密的字符串</param> 
        /// <param name="sDecrKey">key</param> 
        /// <returns>output string</returns> 
        public static string DesDecrypt(string strText, string sDecrKey)
        {
            byte[] byKey = null;
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
            byte[] inputByteArray = new Byte[strText.Length];

            byKey = Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(strText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            Encoding encoding = new UTF8Encoding();
            return encoding.GetString(ms.ToArray());
        }

        /// <summary> 
        /// 文件加密 
        /// 注意:key 必须是8位
        /// </summary> 
        /// <param name="m_InFilePath">加密文件路径</param> 
        /// <param name="m_OutFilePath">文件输出路径</param> 
        /// <param name="strEncrKey">key</param> 
        public static void DesEncrypt(string m_InFilePath, string m_OutFilePath, string strEncrKey)
        {
            byte[] byKey = null;
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

            byKey = Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));
            FileStream fin = new FileStream(m_InFilePath, FileMode.Open, FileAccess.Read);
            FileStream fout = new FileStream(m_OutFilePath, FileMode.OpenOrCreate, FileAccess.Write);
            fout.SetLength(0);
            //Create variables to help with read and write. 
            byte[] bin = new byte[100]; //This is intermediate storage for the encryption. 
            long rdlen = 0; //This is the total number of bytes written. 
            long totlen = fin.Length; //This is the total length of the input file. 
            int len; //This is the number of bytes to be written at a time. 

            DES des = new DESCryptoServiceProvider();
            CryptoStream encStream = new CryptoStream(fout, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);

            //Read from the input file, then encrypt and write to the output file. 
            while (rdlen < totlen)
            {
                len = fin.Read(bin, 0, 100);
                encStream.Write(bin, 0, len);
                rdlen = rdlen + len;
            }
            encStream.Close();
            fout.Close();
            fin.Close();
        }

        /// <summary> 
        /// 文件解密
        /// A注意:key 必须是8位
        /// </summary> 
        /// <param name="m_InFilePath">解密文件路径</param> 
        /// <param name="m_OutFilePath">文件输出路径</param> 
        /// <param name="sDecrKey">key</param> 
        public static void DesDecrypt(string m_InFilePath, string m_OutFilePath, string sDecrKey)
        {
            byte[] byKey = null;
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

            byKey = Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
            FileStream fin = new FileStream(m_InFilePath, FileMode.Open, FileAccess.Read);
            FileStream fout = new FileStream(m_OutFilePath, FileMode.OpenOrCreate, FileAccess.Write);
            fout.SetLength(0);
            //Create variables to help with read and write. 
            byte[] bin = new byte[100]; //This is intermediate storage for the encryption. 
            long rdlen = 0; //This is the total number of bytes written. 
            long totlen = fin.Length; //This is the total length of the input file. 
            int len; //This is the number of bytes to be written at a time. 

            DES des = new DESCryptoServiceProvider();
            CryptoStream encStream = new CryptoStream(fout, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

            //Read from the input file, then encrypt and write to the output file. 
            while (rdlen < totlen)
            {
                len = fin.Read(bin, 0, 100);
                encStream.Write(bin, 0, len);
                rdlen = rdlen + len;
            }
            encStream.Close();
            fout.Close();
            fin.Close();
        }
        #endregion

        #region Base64加密解密
        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="input">需要加密的字符串</param>
        /// <returns></returns>
        public static string Base64Encrypt(string input)
        {
            return Base64Encrypt(input, new UTF8Encoding());
        }

        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="input">需要加密的字符串</param>
        /// <param name="encode">字符编码</param>
        /// <returns></returns>
        public static string Base64Encrypt(string input, Encoding encode)
        {
            return Convert.ToBase64String(encode.GetBytes(input));
        }

        /// <summary>
        /// Base64解密
        /// </summary>
        /// <param name="input">需要解密的字符串</param>
        /// <returns></returns>
        public static string Base64Decrypt(string input)
        {
            return Base64Decrypt(input, new UTF8Encoding());
        }

        /// <summary>
        /// Base64解密
        /// </summary>
        /// <param name="input">需要解密的字符串</param>
        /// <param name="encode">字符的编码</param>
        /// <returns></returns>
        public static string Base64Decrypt(string input, Encoding encode)
        {
            return encode.GetString(Convert.FromBase64String(input));
        }
        #endregion

        #region MD5加密
        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="input">需要加密的字符串</param>
        /// <returns></returns>
        public static string MD5Encrypt(string input)
        {
            return MD5Encrypt(input, new UTF8Encoding());
        }

        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="input">需要加密的字符串</param>
        /// <param name="encode">字符的编码</param>
        /// <returns></returns>
        public static string MD5Encrypt(string input, Encoding encode)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] t = md5.ComputeHash(encode.GetBytes(input));
            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < t.Length; i++)
                sb.Append(t[i].ToString("x").PadLeft(2, '0'));
            return sb.ToString();
        }

        /// <summary>
        /// MD5对文件流加密
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        public static string MD5Encrypt(Stream stream)
        {
            MD5 md5serv = MD5CryptoServiceProvider.Create();
            byte[] buffer = md5serv.ComputeHash(stream);
            StringBuilder sb = new StringBuilder();
            foreach (byte var in buffer)
                sb.Append(var.ToString("x2"));
            return sb.ToString();
        }

        #endregion

        #region Md5加密,生成16位或32位,生成的密文都是大写
        /// <summary>  
        /// MD5　16位加密  
        /// </summary>  
        /// <param name="str"></param>  
        /// <returns></returns>  
        public static string Md5To16(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            string pwd = BitConverter.ToString(md5.ComputeHash(UTF8Encoding.Default.GetBytes(str)), 4, 8);
            pwd = pwd.Replace("-", "");
            return pwd;
        }

        /// <summary>  
        /// MD5　32位加密  
        /// </summary>  
        /// <param name="str"></param>  
        /// <returns></returns>  
        public static string Md5To32(string str)
        {
            string pwd = "";
            MD5 md5 = MD5.Create();
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(str));
            for (int i = 0; i < s.Length; i++)
            {
                pwd = pwd + s[i].ToString("X2");
            }
            return pwd;
        }
        #endregion  
    }
}
