﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HIS.InterFaceJKDAN.Class;
using DevExpress.XtraLayout.Utils;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_高血压患者随访记录表: XtraForm
    {
        private DataSet ds;
        private string _ID = "";
        private string docNo = "";
        public Frm_高血压患者随访记录表()
        {
            InitializeComponent();
        }
        public Frm_高血压患者随访记录表(string _docNo)
        {
            this.InitializeComponent();
            this.docNo = _docNo;
            this.ds = this.GetBusinessByKey(this.docNo);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                this.txt个人档案号.Text = this.ds.Tables[0].Rows[0]["个人档案编号"].ToString();
                this.txt姓名.Text = this.ds.Tables[0].Rows[0]["姓名"].ToString();
                this.txt性别.Text = this.ds.Tables[0].Rows[0]["性别"].ToString();
                this.txt身份证号.Text = this.ds.Tables[0].Rows[0]["身份证号"].ToString();
                this.txt出生日期.Text = this.ds.Tables[0].Rows[0]["出生日期"].ToString();
                this.txt联系电话.Text = this.ds.Tables[0].Rows[0]["联系电话"].ToString();
                this.txt居住地址.Text = this.ds.Tables[0].Rows[0]["居住地址"].ToString();
                this.txt职业.Text = this.ds.Tables[0].Rows[0]["职业"].ToString();
                this.DoBindingSummaryEditor(this.ds);
            }
        }
        public DataSet GetBusinessByKey(string docNo)
        {
            string str = " select top 1 * from [vw_高血压随访详细]    where 个人档案编号 = '" + docNo + "'  order by 发生时间 desc ";
            string str2 = " select * from [tb_MXB高血压随访表_用药情况]   where 个人档案编号 ='" + docNo + "' ";
            DataSet dataSet = SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, str + str2);
            dataSet.Tables[0].TableName = "tb_MXB高血压随访表";
            dataSet.Tables[1].TableName = "tb_MXB高血压随访表_用药情况";
            return dataSet;
        }
        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected void DoBindingSummaryEditor(DataSet objdataSource)
        {
            DataTable dataTable = objdataSource.Tables[0];
            this._ID = dataTable.Rows[0]["ID"].ToString();
            DataBinder.BindingTextEdit(this.txt发生时间, dataTable, "发生时间");
            DataBinder.BindingTextEdit(this.txt血压值.Txt1, dataTable, "收缩压");
            DataBinder.BindingTextEdit(this.txt血压值.Txt2, dataTable, "舒张压");
            DataBinder.BindingTextEdit(this.txt体重.Txt1, dataTable, "体重");
            DataBinder.BindingTextEdit(this.txt体重.Txt2, dataTable, "体重2");
            DataBinder.BindingTextEdit(this.txt身高.Txt1, dataTable, "身高");
            DataBinder.BindingTextEdit(this.txt体质指数.Txt1, dataTable, "BMI");
            DataBinder.BindingTextEdit(this.txt体质指数.Txt2, dataTable, "BMI2");
            DataBinder.BindingTextEdit(this.txt心率, dataTable, "心率");
            DataBinder.BindingTextEdit(this.txt体征其他, dataTable, "体征其他");
            DataBinder.BindingTextEdit(this.txt日吸烟量.Txt1, dataTable, "吸烟数量");
            DataBinder.BindingTextEdit(this.txt日吸烟量.Txt2, dataTable, "吸烟数量2");
            DataBinder.BindingTextEdit(this.txt饮酒情况.Txt1, dataTable, "饮酒数量");
            DataBinder.BindingTextEdit(this.txt饮酒情况.Txt2, dataTable, "饮酒数量2");
            DataBinder.BindingTextEdit(this.txt运动频率.Txt1, dataTable, "运动频率");
            DataBinder.BindingTextEdit(this.txt运动频率.Txt2, dataTable, "运动频率2");
            DataBinder.BindingTextEdit(this.txt持续时间.Txt1, dataTable, "运动持续时间");
            DataBinder.BindingTextEdit(this.txt持续时间.Txt2, dataTable, "运动持续时间2");
            DataBinder.BindingTextEdit(this.txt辅助检查, dataTable, "辅助检查");
            DataBinder.BindingTextEdit(this.txt药物副作用详述, dataTable, "副作用详述");
            DataBinder.BindingTextEdit(this.txt医生建议, dataTable, "随访医生建议");
            DataBinder.BindingTextEdit(this.txt转诊科别, dataTable, "转诊科别");
            DataBinder.BindingTextEdit(this.txt转诊原因, dataTable, "转诊原因");
            DataBinder.BindingTextEdit(this.txt下次随访时间, dataTable, "下次随访时间");
            DataBinder.BindingTextEdit(this.txt医生签名, dataTable, "随访医生");
            this.txt随访方式.Text = dataTable.Rows[0]["随访方式"].ToString();
            this.txt不良反应.Text = dataTable.Rows[0]["药物副作用"].ToString();
            this.txt用药情况.Text = dataTable.Rows[0]["降压药"].ToString();
            if (this.txt用药情况.Text == "使用")
            {
                this.ds.Tables[1].DefaultView.RowFilter = "创建时间='" + dataTable.Rows[0]["创建时间"].ToString() + "'";
                this.gcDetail.DataSource = this.ds.Tables[1];
                this.layout药物列表.Visibility = LayoutVisibility.Always;
            }
            else
            {
                this.layout药物列表.Visibility = LayoutVisibility.Never;
            }
            this.txt心理调整.Text = dataTable.Rows[0]["心理调整"].ToString();
            this.txt摄盐情况1.Text = dataTable.Rows[0]["摄盐情况"].ToString();
            this.txt摄盐情况2.Text = dataTable.Rows[0]["摄盐情况2"].ToString();
            this.txt遵医行为.Text = dataTable.Rows[0]["遵医行为"].ToString();
            this.txt服药依从性.Text = dataTable.Rows[0]["服药依从性"].ToString();
            this.txt转诊情况.Text = dataTable.Rows[0]["转诊情况"].ToString();
            this.txt目前症状.Text = ((dataTable.Rows[0]["目前症状"].ToString() == "") ? "无" : dataTable.Rows[0]["目前症状"].ToString());
            if (dataTable.Rows[0]["目前症状其他"].ToString() != "")
            {
                TextEdit expr_4C3 = this.txt目前症状;
                expr_4C3.Text = expr_4C3.Text + "(" + dataTable.Rows[0]["目前症状其他"].ToString() + ")";
            }
            this.txt随访分类.Text = dataTable.Rows[0]["本次随访分类"].ToString();
            if (dataTable.Rows[0]["慢病并发症"].ToString() != "")
            {
                TextEdit expr_554 = this.txt随访分类;
                expr_554.Text = expr_554.Text + "(" + dataTable.Rows[0]["慢病并发症"].ToString() + ")";
            }
        }


    }
}
