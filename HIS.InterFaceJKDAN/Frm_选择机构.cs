﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HIS.InterFaceJKDAN.Class;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_选择机构 : DevExpress.XtraEditors.XtraForm
    {
        public delegate void myDelegate(string s);
        public event myDelegate Definite;

        public Frm_选择机构()
        {
            InitializeComponent();
        }

        private void Frm_选择机构_Load(object sender, EventArgs e)
        {
            BindList();
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            if (gridView1.GetSelectedRows() != null && gridView1.GetSelectedRows().Length > 0)
            {
                Definite(gridView1.GetRowCellValue(gridView1.GetSelectedRows()[0], "机构编号").ToString());
                this.DialogResult=System.Windows.Forms.DialogResult.OK;
            }
            else
                MessageBox.Show("请选择机构！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void BindList()
        {
            string Sql = @"select 机构编号,机构名称 from tb_机构信息 where 机构编号 ='" + DefaultInfo.s所属机构 + "' or substring( [机构编号],1,7)+'1'+substring( [机构编号],9,7) like '" + DefaultInfo.s所属机构 + "%' ";
            DataTable dt = SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, Sql).Tables[0];
            this.gridControl1.DataSource = dt;
            this.gridView1.BestFitColumns();
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
        }
    }
}
