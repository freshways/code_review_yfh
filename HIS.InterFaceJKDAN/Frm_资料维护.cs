﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HIS.InterFaceJKDAN.Class;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_资料维护 : DevExpress.XtraEditors.XtraForm
    {
        public Frm_资料维护()
        {
            InitializeComponent();
        }


        private void Frm_资料维护_Load(object sender, EventArgs e)
        {
            BindList();
        }

        #region 菜单事件

        private void simpleButton查询_Click(object sender, EventArgs e)
        {
            BindList();
        }

        private void simpleButton修改_Click(object sender, EventArgs e)
        {
            if (gridView1.GetSelectedRows() != null && gridView1.GetSelectedRows().Length > 0)
            {
                m基础资料 s = new m基础资料();
                s.ID = (int)this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], "ID");
                s.s项目 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0],this.gridView1.Columns[1]).ToString();
                s.s拼音 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], this.gridView1.Columns[2]).ToString();
                s.s是否启用 = this.gridView1.GetRowCellValue(this.gridView1.GetSelectedRows()[0], this.gridView1.Columns[3]).ToString();
                Frm_基础资料维护 frm = new Frm_基础资料维护(s);
                frm.Save += delegate(m基础资料 send)
                {
                    EditUpdate(send);
                    this.gridView1.SetRowCellValue(this.gridView1.GetSelectedRows()[0], this.gridView1.Columns[1], send.s项目);
                    this.gridView1.SetRowCellValue(this.gridView1.GetSelectedRows()[0], this.gridView1.Columns[2], send.s拼音);
                    this.gridView1.SetRowCellValue(this.gridView1.GetSelectedRows()[0], this.gridView1.Columns[3], send.s是否启用);
                    frm.Close();
                };
                frm.ShowDialog();
            }
            else
                MessageBox.Show("没有可修改信息！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void simpleButton添加_Click(object sender, EventArgs e)
        {
            Frm_基础资料维护 frm = new Frm_基础资料维护();
            frm.Save += delegate(m基础资料 s)
            {
                Save保存(s);
            };
            frm.ShowDialog();
        }

        private void simpleButton停用_Click(object sender, EventArgs e)
        {
            string rowIDs = GetSelectedRows(this.gridView1);
            string Sql = "";
            switch (comboBoxEdit1.Text)
            {
                case "主观资料":
                    Sql = "update InterFace_接诊记录字典 set 是否禁用='是' where ID in({0})";
                    break;
                case "客观资料":
                    Sql = "update InterFace_接诊记录字典 set 是否禁用='是' where ID in({0})";
                    break;
                case "评估":
                    Sql = "update InterFace_接诊记录字典 set 是否禁用='是' where ID in({0})";
                    break;
                case "处置计划":
                    Sql = "update InterFace_接诊记录字典 set 是否禁用='是'  where ID in({0})";
                    break;
                default:
                    break;
            }
            Sql = String.Format(Sql, rowIDs);
            int rows = SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, Sql);
            if (rows > 0)
                MessageBox.Show("停用成功！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            BindList();
        } 

        #endregion

        #region 方法

        DataTable dt;
        private void BindList()
        {
            string Sql = "";
            switch (comboBoxEdit1.Text)
            {
                case "主观资料":
                    Sql = "select ID,名称,拼音代码,是否禁用 from InterFace_接诊记录字典 where 类别='主观资料' ";
                    break;
                case "客观资料":
                    Sql = "select ID,名称,拼音代码,是否禁用 from InterFace_接诊记录字典 where 类别='客观资料'  ";
                    break;
                case "评估":
                    Sql = "select ID,名称,拼音代码,是否禁用 from InterFace_接诊记录字典 where 类别='评估'  ";
                    break;
                case "处置计划":
                    Sql = "select ID,名称,拼音代码,是否禁用 from InterFace_接诊记录字典 where 类别='处置计划'  ";
                    break;
                default:
                    break;
            }
            try
            {
                dt = SqlHelper.ExecuteDataset(DefaultInfo.SQLConn, CommandType.Text, Sql).Tables[0];
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.gridView1.Columns.Clear();
            this.gridControl1.DataSource = dt;
            this.gridView1.BestFitColumns();
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gridView1.OptionsSelection.MultiSelect = true;//设置多行选择
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect;
        }

        private void Save保存(m基础资料 s)
        { 
             string Beginsql = "";
             switch (comboBoxEdit1.Text)
            {
                case "主观资料":
                    Beginsql = "insert into InterFace_接诊记录字典(名称,拼音代码,是否禁用,类别) values('{0}','{1}','{2}','主观资料')";
                    break;
                case "客观资料":
                    Beginsql = "insert into InterFace_接诊记录字典(名称,拼音代码,是否禁用,类别) values('{0}','{1}','{2}','客观资料')";
                    break;
                case "评估":
                    Beginsql = "insert into InterFace_接诊记录字典(名称,拼音代码,是否禁用,类别) values('{0}','{1}','{2}','评估')";
                    break;
                case "处置计划":
                    Beginsql = "insert into InterFace_接诊记录字典(名称,拼音代码,是否禁用,类别) values('{0}','{1}','{2}','处置计划')";
                    break;
                default:
                    break;
            }
            Beginsql = string.Format(Beginsql, s.s项目, s.s拼音, s.s是否启用);
            int rows = SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, Beginsql);
            //int rows = SQLHelper.ExecuteSql(Beginsql);
            if (rows > 0)
            {
                MessageBox.Show("保存成功！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DataRow dr = dt.NewRow();
                dr[0] = dt.Rows.Count + 1;
                dr[1] = s.s项目;
                dr["拼音代码"] = s.s拼音;
                dr["是否禁用"] = s.s是否启用;
                dt.Rows.Add(dr);                
            }
        }

        private void EditUpdate(m基础资料 s)
        { 
            string Sql = "";
            switch (comboBoxEdit1.Text)
            {
                case "主观资料":
                    Sql = "update InterFace_接诊记录字典 set 名称='{0}',拼音代码='{1}',是否禁用='{2}'  where ID={3}";
                    break;
                case "客观资料":
                    Sql = "update InterFace_接诊记录字典 set 名称='{0}',拼音代码='{1}',是否禁用='{2}'  where ID={3}";
                    break;
                case "评估":
                    Sql = "update InterFace_接诊记录字典 set 名称='{0}',拼音代码='{1}',是否禁用='{2}'  where ID={3}";
                    break;
                case "处置计划":
                    Sql = "update InterFace_接诊记录字典 set 名称='{0}',拼音代码='{1}',是否禁用='{2}'  where ID={3}";
                    break;
                default:
                    break;
            }
            Sql = String.Format(Sql, s.s项目, s.s拼音, s.s是否启用, s.ID);

            try
            {
                int rows = SqlHelper.ExecuteNonQuery(DefaultInfo.SQLConn, CommandType.Text, Sql);
                //int rows = SQLHelper.ExecuteSql(Sql);
                if ((int)rows > 0)
                    MessageBox.Show("修改成功！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        string GetSelectedRows(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            string ret = "0"; 
            try
            {
                if (view.OptionsSelection.MultiSelectMode == DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.RowSelect)
                {
                    foreach (int i in gridView1.GetSelectedRows())
                    {
                        DataRow row = gridView1.GetDataRow(i); 
                        //if (ret != "")  ret += "\r\n";
                        ret += string.Format(",{0}", row[0]);
                    }
                }               
            }
            catch (Exception)
            {
                return "";
            }
            return ret;
        } 

        #endregion

        private void comboBoxEdit1_TextChanged(object sender, EventArgs e)
        {
            BindList();
        }

    }
}
