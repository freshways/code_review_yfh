using System;
using System.Globalization;
namespace HIS.InterFaceJKDAN
{
	public class ConvertEx
	{
		public static string ToDateString(DateTime dt, string format)
		{
			return dt.ToString(format, DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYYYYMMDDLong(DateTime dt)
		{
			return dt.ToString("yyyy/MM/dd", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYYMM(DateTime dt)
		{
			return dt.ToString("yyMM", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYY(DateTime dt)
		{
			return dt.ToString("yy", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYYYY(DateTime dt)
		{
			return dt.ToString("yyyy", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYYYYMM(DateTime dt)
		{
			return dt.ToString("yyyyMM", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYYYYMMDD(DateTime dt)
		{
			string text = dt.ToString("yyyyMMdd", DateTimeFormatInfo.CurrentInfo);
			if (text == "00010101")
			{
				text = "";
			}
			return text;
		}
		public static string ToCharYYYYMMDDHHMMSS(DateTime dt)
		{
			return dt.ToString("yyyyMMddHHmmss", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharDD_MM_YYYY_HHMMSS(DateTime dt)
		{
			return dt.ToString("dd/MM/yyyy HH:mm:ss", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYYYY_MM_DD_HHMMSS(DateTime dt)
		{
			return dt.ToString("yyyy/MM/dd HH:mm:ss", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYYYYMMDDHHMMSS000(DateTime dt)
		{
			return dt.ToString("yyyyMMdd HH:mm:ss:000", DateTimeFormatInfo.CurrentInfo);
		}
		public static string ToCharYYMMDD(DateTime dt)
		{
			return dt.ToString("yyMMdd", DateTimeFormatInfo.CurrentInfo);
		}
		public static bool ToBoolean(object o)
		{
			bool result;
			if (null == o)
			{
				result = false;
			}
			else
			{
				try
				{
					result = Convert.ToBoolean(o.ToString());
				}
				catch
				{
					result = false;
				}
			}
			return result;
		}
		public static float ToFloat(object o)
		{
			float result;
			if (null == o)
			{
				result = 0f;
			}
			else
			{
				try
				{
					result = (float)Convert.ToDouble(o.ToString());
				}
				catch
				{
					result = 0f;
				}
			}
			return result;
		}
		public static decimal ToDecimal(object o)
		{
			decimal result;
			if (null == o)
			{
				result = 0m;
			}
			else
			{
				try
				{
					result = Convert.ToDecimal(o.ToString());
				}
				catch
				{
					result = 0m;
				}
			}
			return result;
		}
		public static object ToDecimalFormat(decimal d)
		{
			return decimal.Round(d, 2).ToString("0.00");
		}
		public static DateTime ToDateTime(DateTime? o)
		{
			DateTime result;
			if (!o.HasValue)
			{
				result = DateTime.MinValue;
			}
			else
			{
				try
				{
					result = DateTime.Parse(o.ToString());
				}
				catch
				{
					result = DateTime.MinValue;
				}
			}
			return result;
		}
		public static string ToDateTimeString(object o, string dateFormat, bool quotationMark)
		{
			string result;
			try
			{
				DateTime? dateTime = ConvertEx.ToDateTime(o);
				string text = string.Empty;
				if (!dateTime.HasValue)
				{
					result = "null";
				}
				else
				{
					text = DateTime.Parse(dateTime.ToString()).ToString(dateFormat, DateTimeFormatInfo.CurrentInfo);
					if (quotationMark)
					{
						result = "'" + text + "'";
					}
					else
					{
						result = text;
					}
				}
			}
			catch
			{
				result = "null";
			}
			return result;
		}
		public static DateTime? ToDateTime(object o)
		{
			DateTime? result;
			if (null == o)
			{
				result = null;
			}
			else
			{
				try
				{
					result = new DateTime?(Convert.ToDateTime(o.ToString()));
				}
				catch
				{
					result = null;
				}
			}
			return result;
		}
		public static int ToInt(object o)
		{
			int result;
			if (null == o)
			{
				result = 0;
			}
			else
			{
				try
				{
					result = Convert.ToInt32(o.ToString());
				}
				catch
				{
					result = 0;
				}
			}
			return result;
		}
		public static string ToString(object obj)
		{
			string result;
			if (obj == null)
			{
				result = string.Empty;
			}
			else
			{
				result = obj.ToString().Trim();
			}
			return result;
		}
	}
}
