﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;
using IBM.Data.DB2;

namespace InterFaceJKDAN
{
    /// <summary>
    /// db2helper类
    /// </summary>
    public class DB2Helper
    {
        private string _ConnectionString = "";

        private string _CommandText = "";

        private DB2Connection Conn = null;

        private DataSet _DSAFDataSet = null;
        /// <summary>
        /// 获取或设置数据库连接字串
        /// </summary>
        public string ConnectionString { get { return this._ConnectionString; } set { this._ConnectionString = value; } }
        /// <summary>
        /// 获取或设置SQL语句
        /// </summary>
        public string CommandText { get { return this._CommandText; } set { this._CommandText = value; } }

        public DB2Helper()
        {
            this._DSAFDataSet = new DataSet();
        }
        /// <summary>
        /// 执行 SQL 语句，并把执行结果集填充到一个DataSet容器中
        /// </summary>
        /// <returns></returns>
        public DataSet Execute()
        {
            if (string.IsNullOrEmpty(_ConnectionString)) { throw (new Exception("无传入数据连接字串")); }

            if (string.IsNullOrEmpty(_CommandText)) { throw (new Exception("无传入SQL语句")); }

            if (!DSAFConnectionTest()) { throw (new Exception("尝试数据库连接失败")); }

            try
            {
                Conn = new DB2Connection();
                Conn.ConnectionString = _ConnectionString;

                DB2DataAdapter DB2Da = new DB2DataAdapter(_CommandText, Conn);

                //DB2Da.SelectCommand = new DB2Command();
                //DB2Da.SelectCommand.CommandText = _CommandText;
                //DB2Da.SelectCommand.Connection = Conn;

                DB2Da.Fill(_DSAFDataSet); DB2Da.Dispose();
            }
            catch (DB2Exception DB2Ex) { throw DB2Ex; }
            catch (Exception ex) { throw ex; }

            return _DSAFDataSet;
        }
        /// <summary>
        /// 执行 SQL 语句，并返回受影响的行数
        /// </summary>
        /// <returns></returns>
        public int ExecuteNonQuery()
        {
            if (string.IsNullOrEmpty(_ConnectionString)) { throw (new Exception("无传入数据连接字串")); }

            if (string.IsNullOrEmpty(_CommandText)) { throw (new Exception("无传入SQL语句")); }

            if (!DSAFConnectionTest()) { throw (new Exception("尝试数据库连接失败")); }

            int iQuery = 0; DB2Transaction Transaction = null;

            try
            {
                Conn = new DB2Connection(_ConnectionString);
                DB2Command Comm = new DB2Command();
                Conn.Open();
                //Conn.ConnectionString = _ConnectionString;
                Comm.Connection = Conn;
                Comm.CommandText = _CommandText;

                Transaction = Conn.BeginTransaction(); 
                Comm.Transaction = Transaction;

                iQuery = Comm.ExecuteNonQuery(); Transaction.Commit(); 
                return iQuery;
            }
            catch (DB2Exception DB2Ex) { Transaction.Rollback(); throw DB2Ex; }
            catch (Exception ex) { throw ex; }
            finally { Transaction.Dispose(); Conn.Close(); Conn.Dispose(); }
        }

        /// <summary>
        /// 执行 SQL 语句，并返回结果集中的第一行第一列。忽略其它行或列
        /// </summary>
        /// <returns></returns>
        public object ExecuteScalar()
        {
            if (string.IsNullOrEmpty(_ConnectionString)) { throw (new Exception("无传入数据连接字串")); }

            if (string.IsNullOrEmpty(_CommandText)) { throw (new Exception("无传入SQL语句")); }

            if (!DSAFConnectionTest()) { throw (new Exception("尝试数据库连接失败")); }

            object iQuery = null;
            DB2Transaction Transaction = null;

            try
            {
                DB2Command Comm = new DB2Command();
                Conn = new DB2Connection();

                Conn.ConnectionString = _ConnectionString;
                Comm.Connection = Conn;
                Comm.CommandText = _CommandText;

                Transaction = Conn.BeginTransaction();
                Comm.Transaction = Transaction;

                iQuery = Comm.ExecuteScalar();
                Transaction.Commit();
                return iQuery;
            }
            catch (DB2Exception DB2Ex)
            {
                Transaction.Rollback();
                throw DB2Ex;
            }
            catch (Exception ex) { throw ex; }
            finally
            {
                Transaction.Dispose();
                Conn.Close();
                Conn.Dispose();
            }
        }
        /// <summary>
        /// 测试数据库连接
        /// </summary>
        /// <returns></returns>
        private bool DSAFConnectionTest()
        {
            try
            {
                Conn = new DB2Connection();
                Conn.ConnectionString = _ConnectionString;

                Conn.Open(); Conn.Close();
            }
            catch { return (false); }

            return (true);
        }

        /// <summary>
        /// 释放由 DevSDK.Data.DB2 使用的所有资源
        /// </summary>
        public void Dispose()
        {
            if (Conn != null) { this.Conn.Dispose(); }

            if (_DSAFDataSet != null) { this._DSAFDataSet = null; }
        }
    }
}
