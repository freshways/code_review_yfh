﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;

namespace HIS.InterFaceJKDAN
{
    class DB2OdbcHelper
    {
        private const string dsn = "DSN=OutBound";
        private const string conn = "Driver={IBM DB2 ODBC DRIVER};Database=YSDB;hostname=192.168.10.101;port=60000;protocol=TCPIP; uid=db2inst1; pwd=ysxwsj001";

        #region 执行SQL
        /// <summary>
        /// 返回影响行数
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string strSQL)
        {
            return ExecuteNonQuery(conn, strSQL);
        }

        /// <summary>
        /// 返回影响行数
        /// </summary>
        /// <param name="connectionStr"></param>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string connectionStr, string strSQL)
        {
            OdbcConnection connection = new OdbcConnection(connectionStr);
            connection.Open();
            OdbcCommand cmd = new OdbcCommand(strSQL, connection);
            try
            {
                return cmd.ExecuteNonQuery();
            }
            catch (Exception E)
            {

            }
            return -1;
        }
        #endregion

        #region DataSet
        /// <summary>
        /// 返回DataSet
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public static DataSet ExecuteDs(string strSQL)
        {
            return ExecuteDs(conn, strSQL);
        }

        /// <summary>
        /// 返回DataSet
        /// </summary>
        /// <param name="connectionStr"></param>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public static DataSet ExecuteDs(string connectionStr, string strSQL)
        {
            OdbcConnection connection = new OdbcConnection(connectionStr);
            connection.Open();
            OdbcCommand cmd = new OdbcCommand(strSQL, connection);
            try
            {
                DataSet ds = new DataSet();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(ds);
                return ds;
            }
            catch (SqlException E)
            {
                // WriteLogFile.WriteMessage("DBOper.log", "error DBHelper.ExecuteReader " + E.Message.ToString());
            }
            return null;
        }
        #endregion

        #region OdbcDataReader
        /// <summary>
        /// 返回OdbcDataReader
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public static OdbcDataReader ExecuteReader(string strSQL)
        {
            return ExecuteReader(conn, strSQL);
        }

        /// <summary>
        /// 返回OdbcDataReader
        /// </summary>
        /// <param name="connectionStr"></param>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public static OdbcDataReader ExecuteReader(string connectionStr, string strSQL)
        {
            OdbcConnection connection = new OdbcConnection(connectionStr);
            connection.Open();
            OdbcCommand cmd = new OdbcCommand(strSQL, connection);
            try
            {
                OdbcDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return myReader;
            }
            catch (SqlException E)
            {
                // WriteLogFile.WriteMessage("DBOper.log", "error DBHelper.ExecuteReader " + E.Message.ToString());
            }
            return null;
        }
        #endregion

    }
}
