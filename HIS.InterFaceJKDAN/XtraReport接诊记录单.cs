﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace HIS.InterFaceJKDAN
{
    public partial class XtraReport接诊记录单 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport接诊记录单()
        {
            InitializeComponent();
        }

        public XtraReport接诊记录单(DataTable dt,string s姓名,string s性别,string s年龄,string s地址)
        {
            InitializeComponent();
            xrLabel姓名.Text += s姓名;
            xrLabel性别.Text += s性别;
            xrLabel年龄.Text += s年龄;
            xrLabel住址.Text += s地址;
            xrRichText1.Text = dt.Rows[0]["主观资料"].ToString();
            xrRichText2.Text = dt.Rows[0]["客观资料"].ToString();
            xrRichText3.Text = dt.Rows[0]["评估"].ToString();
            xrRichText4.Text = dt.Rows[0]["处置计划"].ToString();
            xrLabel签字.Text += dt.Rows[0]["接诊医生"].ToString();
            xrLabel日期.Text = dt.Rows[0]["接诊时间"].ToString();
        }

    }
}
