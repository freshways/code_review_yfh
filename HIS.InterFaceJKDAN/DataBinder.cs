using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
namespace HIS.InterFaceJKDAN
{
	public class DataBinder
	{
		public static void BindingLookupEditDataSource(LookUpEdit edit, object dataSource, string displayMember, string valueMember)
		{
			DataBinder.BindingLookupEditDataSource(edit.Properties, dataSource, displayMember, valueMember);
		}
		public static void BindingLookupEditDataSource(RepositoryItemLookUpEdit edit, object dataSource, string displayMember, string valueMember)
		{
			edit.DisplayMember = displayMember;
			edit.ValueMember = valueMember;
			edit.DataSource = dataSource;
		}
		public static void BindingRadioEdit(RadioGroup edit, object dataSource, string bindField)
		{
			try
			{
				edit.DataBindings.Clear();
				Binding binding = new Binding("EditValue", dataSource, bindField, false, DataSourceUpdateMode.OnPropertyChanged);
				edit.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingControl(Control ctl, object dataSource, string bindField, string propertyName)
		{
			try
			{
				ctl.DataBindings.Clear();
				Binding binding = new Binding(propertyName, dataSource, bindField);
				ctl.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingTextEdit(TextEdit edit, object dataSource, string bindField)
		{
			try
			{
				edit.DataBindings.Clear();
				Binding binding = new Binding("EditValue", dataSource, bindField, false, DataSourceUpdateMode.OnPropertyChanged);
				edit.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingCheckedListBox(Control control, object dataSource, string bindField)
		{
			try
			{
				control.DataBindings.Clear();
				Binding binding = new Binding("EditValue", dataSource, bindField);
				control.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingListBoxLookupData(ListBoxControl listBoxControl, object dataSource, string displayMember)
		{
			listBoxControl.DisplayMember = displayMember;
			listBoxControl.DataSource = dataSource;
		}
		public static void BindingComboEdit(ComboBoxEdit edit, object dataSource, string bindField)
		{
			try
			{
				edit.DataBindings.Clear();
				Binding binding = new Binding("EditValue", dataSource, bindField);
				edit.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingCheckEdit(CheckEdit edit, object dataSource, string bindField)
		{
			try
			{
				edit.DataBindings.Clear();
				Binding binding = new Binding("Checked", dataSource, bindField);
				binding.NullValue = "N";
				edit.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingTextEditDateTime(TimeEdit edit, object dataSource, string bindField)
		{
			try
			{
				edit.DataBindings.Clear();
				Binding binding = new Binding("EditValue", dataSource, bindField);
				binding.NullValue = null;
				binding.Parse += new ConvertEventHandler(DataBinder.DateStringToDate);
				binding.Format += new ConvertEventHandler(DataBinder.DateToDateString);
				edit.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingTextEditDateTime(DateEdit edit, object dataSource, string bindField)
		{
			try
			{
				edit.DataBindings.Clear();
				Binding binding = new Binding("EditValue", dataSource, bindField, false, DataSourceUpdateMode.OnPropertyChanged);
				binding.NullValue = null;
				binding.Parse += new ConvertEventHandler(DataBinder.DateStringToDate);
				binding.Format += new ConvertEventHandler(DataBinder.DateToDateString);
				edit.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingImageEdit(PictureEdit edit, object dataSource, string bindField)
		{
			try
			{
				edit.DataBindings.Clear();
				Binding binding = new Binding("Image", dataSource, bindField);
				binding.Parse += new ConvertEventHandler(DataBinder.ParseImageToByteArray);
				binding.Format += new ConvertEventHandler(DataBinder.FormatByteArrayToImage);
				edit.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void BindingTextEditAmount(TextEdit edit, object dataSource, string bindField)
		{
			try
			{
				edit.DataBindings.Clear();
				Binding binding = new Binding("EditValue", dataSource, bindField);
				binding.NullValue = null;
				binding.Parse += new ConvertEventHandler(DataBinder.CurrencyStringToDecimal);
				binding.Format += new ConvertEventHandler(DataBinder.DecimalToCurrencyString);
				edit.DataBindings.Add(binding);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void OnDateEditValueChange(object sender, EventArgs e)
		{
			try
			{
				DateEdit dateEdit = (DateEdit)sender;
				if (dateEdit.DataBindings.Count > 0)
				{
					object dataSource = dateEdit.DataBindings[0].DataSource;
					if (dataSource != null)
					{
						string bindingField = dateEdit.DataBindings[0].BindingMemberInfo.BindingField;
						DataConverter.SetValueOfObject(dataSource, bindingField, dateEdit.EditValue);
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		public static void FormatByteArrayToImage(object sender, ConvertEventArgs e)
		{
            //try
            //{
            //    if (e.Value != null)
            //    {
            //        Image value = (Image)ZipTools.DecompressionObject((byte[])e.Value);
            //        e.Value = value;
            //    }
            //    else
            //    {
            //        e.Value = null;
            //    }
            //}
            //catch
            //{
            //    e.Value = null;
            //}
		}
		public static void ParseImageToByteArray(object sender, ConvertEventArgs e)
		{
            //try
            //{
            //    if (e.Value != null)
            //    {
            //        e.Value = ZipTools.CompressionObject(e.Value as Image);
            //    }
            //    else
            //    {
            //        e.Value = DBNull.Value;
            //    }
            //}
            //catch
            //{
            //    e.Value = DBNull.Value;
            //}
		}
		public static void BindingDateEditValueChangeEvent(DateEdit dateEdit)
		{
			dateEdit.EditValueChanged += new EventHandler(DataBinder.OnDateEditValueChange);
		}
		public static void BoolBitToBool(object sender, ConvertEventArgs cevent)
		{
			try
			{
				if (!(cevent.DesiredType != typeof(int)))
				{
					cevent.Value = bool.Parse(cevent.Value.ToString());
				}
			}
			catch (Exception ex)
			{
				DataBinder.ShowError("数据转换错误!/n" + ex.Message, "错误");
			}
		}
		public static void BoolToBoolBit(object sender, ConvertEventArgs cevent)
		{
			try
			{
				if (!(cevent.DesiredType != typeof(bool)))
				{
					if (cevent.Value.ToString() == string.Empty)
					{
						cevent.Value = false;
					}
					cevent.Value = (bool)cevent.Value;
				}
			}
			catch (Exception ex)
			{
				DataBinder.ShowError("数据转换错误!/n" + ex.Message, "错误");
			}
		}
		public static void DateStringToDate(object sender, ConvertEventArgs cevent)
		{
			try
			{
				Type left = cevent.DesiredType;
				if (cevent.DesiredType.IsGenericType)
				{
					left = cevent.DesiredType.GetGenericArguments()[0];
				}
				if (cevent.Value == null || left != typeof(DateTime) || cevent.Value.ToString() == string.Empty)
				{
					cevent.Value = null;
				}
				else
				{
					cevent.Value = DateTime.Parse(cevent.Value.ToString(), DateTimeFormatInfo.CurrentInfo);
				}
			}
			catch (Exception ex)
			{
				DataBinder.ShowError("数据转换错误!/n" + ex.Message, "错误");
			}
		}
		public static void DateToDateString(object sender, ConvertEventArgs cevent)
		{
			try
			{
				if (cevent.Value == null || cevent.DesiredType != typeof(string) || cevent.Value.ToString() == string.Empty)
				{
					cevent.Value = null;
				}
				else
				{
					cevent.Value = ((DateTime)cevent.Value).ToString("yyyy-MM-dd", DateTimeFormatInfo.CurrentInfo);
				}
			}
			catch (Exception ex)
			{
				DataBinder.ShowError("数据转换错误!/n" + ex.Message, "错误");
			}
		}
		public static void DecimalToCurrencyString(object sender, ConvertEventArgs cevent)
		{
			try
			{
				if (!(cevent.DesiredType != typeof(string)))
				{
					if (cevent.Value.ToString() == string.Empty)
					{
						cevent.Value = 0m.ToString("n");
					}
					else
					{
						cevent.Value = ((decimal)cevent.Value).ToString("n");
					}
				}
			}
			catch (Exception ex)
			{
				DataBinder.ShowError("数据转换错误!/n" + ex.Message, "错误");
			}
		}
		public static void CurrencyStringToDecimal(object sender, ConvertEventArgs cevent)
		{
			try
			{
				if (!(cevent.DesiredType != typeof(decimal)))
				{
					if (string.Empty == cevent.Value.ToString())
					{
						cevent.Value = 0;
					}
					else
					{
						cevent.Value = decimal.Parse(cevent.Value.ToString(), NumberStyles.Currency, null);
					}
				}
			}
			catch (Exception ex)
			{
				DataBinder.ShowError("数据转换错误!/n" + ex.Message, "错误");
			}
		}
		private static void ShowError(string msg, string title)
		{
			MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
		}
	}
}
