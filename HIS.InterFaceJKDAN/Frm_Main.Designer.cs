﻿namespace HIS.InterFaceJKDAN
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton查询 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton用户 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton个人信息 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton维护 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton选择机构 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton关闭 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton打印 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton导出 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton接诊记录 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit身份证 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit机构 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btn查看体检信息 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btn查看糖尿病随访 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查看高血压随访 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.Transparent;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.emptySpaceItem3,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem12,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem17,
            this.layoutControlItem18});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1190, 564);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton查询;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(83, 28);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // simpleButton查询
            // 
            this.simpleButton查询.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton查询.Appearance.Options.UseFont = true;
            this.simpleButton查询.AutoWidthInLayoutControl = true;
            this.simpleButton查询.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton查询.Image")));
            this.simpleButton查询.Location = new System.Drawing.Point(7, 67);
            this.simpleButton查询.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton查询.Name = "simpleButton查询";
            this.simpleButton查询.Size = new System.Drawing.Size(79, 24);
            this.simpleButton查询.StyleController = this.layoutControl1;
            this.simpleButton查询.TabIndex = 8;
            this.simpleButton查询.Text = "查 询";
            this.simpleButton查询.Click += new System.EventHandler(this.simpleButton查询_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btn查看糖尿病随访);
            this.layoutControl1.Controls.Add(this.btn查看高血压随访);
            this.layoutControl1.Controls.Add(this.btn查看体检信息);
            this.layoutControl1.Controls.Add(this.groupControl1);
            this.layoutControl1.Controls.Add(this.simpleButton用户);
            this.layoutControl1.Controls.Add(this.simpleButton个人信息);
            this.layoutControl1.Controls.Add(this.simpleButton维护);
            this.layoutControl1.Controls.Add(this.simpleButton选择机构);
            this.layoutControl1.Controls.Add(this.simpleButton关闭);
            this.layoutControl1.Controls.Add(this.simpleButton打印);
            this.layoutControl1.Controls.Add(this.simpleButton导出);
            this.layoutControl1.Controls.Add(this.simpleButton接诊记录);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.simpleButton查询);
            this.layoutControl1.Controls.Add(this.textEdit身份证);
            this.layoutControl1.Controls.Add(this.textEdit档案号);
            this.layoutControl1.Controls.Add(this.textEdit姓名);
            this.layoutControl1.Controls.Add(this.textEdit机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(296, 301, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1190, 564);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.Location = new System.Drawing.Point(7, 7);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1176, 26);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "个人健康档案 ";
            // 
            // simpleButton用户
            // 
            this.simpleButton用户.Location = new System.Drawing.Point(677, 67);
            this.simpleButton用户.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton用户.Name = "simpleButton用户";
            this.simpleButton用户.Size = new System.Drawing.Size(77, 24);
            this.simpleButton用户.StyleController = this.layoutControl1;
            this.simpleButton用户.TabIndex = 19;
            this.simpleButton用户.Text = "用户对照";
            this.simpleButton用户.Click += new System.EventHandler(this.simpleButton用户_Click);
            // 
            // simpleButton个人信息
            // 
            this.simpleButton个人信息.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton个人信息.Image")));
            this.simpleButton个人信息.Location = new System.Drawing.Point(521, 67);
            this.simpleButton个人信息.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton个人信息.Name = "simpleButton个人信息";
            this.simpleButton个人信息.Size = new System.Drawing.Size(152, 24);
            this.simpleButton个人信息.StyleController = this.layoutControl1;
            this.simpleButton个人信息.TabIndex = 18;
            this.simpleButton个人信息.Text = "查看个人档案信息";
            this.simpleButton个人信息.Click += new System.EventHandler(this.simpleButton个人信息_Click);
            // 
            // simpleButton维护
            // 
            this.simpleButton维护.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton维护.Image")));
            this.simpleButton维护.Location = new System.Drawing.Point(393, 67);
            this.simpleButton维护.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton维护.Name = "simpleButton维护";
            this.simpleButton维护.Size = new System.Drawing.Size(124, 24);
            this.simpleButton维护.StyleController = this.layoutControl1;
            this.simpleButton维护.TabIndex = 17;
            this.simpleButton维护.Text = "基础资料维护";
            this.simpleButton维护.Click += new System.EventHandler(this.simpleButton维护_Click);
            // 
            // simpleButton选择机构
            // 
            this.simpleButton选择机构.Location = new System.Drawing.Point(302, 37);
            this.simpleButton选择机构.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton选择机构.Name = "simpleButton选择机构";
            this.simpleButton选择机构.Size = new System.Drawing.Size(26, 26);
            this.simpleButton选择机构.StyleController = this.layoutControl1;
            this.simpleButton选择机构.TabIndex = 16;
            this.simpleButton选择机构.Text = "...";
            this.simpleButton选择机构.Click += new System.EventHandler(this.simpleButton选择机构_Click);
            // 
            // simpleButton关闭
            // 
            this.simpleButton关闭.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton关闭.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton关闭.Image")));
            this.simpleButton关闭.Location = new System.Drawing.Point(328, 67);
            this.simpleButton关闭.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton关闭.Name = "simpleButton关闭";
            this.simpleButton关闭.Size = new System.Drawing.Size(61, 24);
            this.simpleButton关闭.StyleController = this.layoutControl1;
            this.simpleButton关闭.TabIndex = 15;
            this.simpleButton关闭.Text = "关 闭";
            this.simpleButton关闭.Click += new System.EventHandler(this.simpleButton关闭_Click);
            // 
            // simpleButton打印
            // 
            this.simpleButton打印.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton打印.Appearance.Options.UseFont = true;
            this.simpleButton打印.AutoWidthInLayoutControl = true;
            this.simpleButton打印.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton打印.Image")));
            this.simpleButton打印.Location = new System.Drawing.Point(263, 67);
            this.simpleButton打印.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton打印.Name = "simpleButton打印";
            this.simpleButton打印.Size = new System.Drawing.Size(61, 24);
            this.simpleButton打印.StyleController = this.layoutControl1;
            this.simpleButton打印.TabIndex = 14;
            this.simpleButton打印.Text = "打 印";
            this.simpleButton打印.Visible = false;
            this.simpleButton打印.Click += new System.EventHandler(this.simpleButton打印_Click);
            // 
            // simpleButton导出
            // 
            this.simpleButton导出.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton导出.Appearance.Options.UseFont = true;
            this.simpleButton导出.AutoWidthInLayoutControl = true;
            this.simpleButton导出.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton导出.Image")));
            this.simpleButton导出.Location = new System.Drawing.Point(198, 67);
            this.simpleButton导出.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton导出.Name = "simpleButton导出";
            this.simpleButton导出.Size = new System.Drawing.Size(61, 24);
            this.simpleButton导出.StyleController = this.layoutControl1;
            this.simpleButton导出.TabIndex = 13;
            this.simpleButton导出.Text = "导 出";
            this.simpleButton导出.Visible = false;
            this.simpleButton导出.Click += new System.EventHandler(this.simpleButton导出_Click);
            // 
            // simpleButton接诊记录
            // 
            this.simpleButton接诊记录.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButton接诊记录.Appearance.Options.UseFont = true;
            this.simpleButton接诊记录.AutoWidthInLayoutControl = true;
            this.simpleButton接诊记录.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton接诊记录.Image")));
            this.simpleButton接诊记录.Location = new System.Drawing.Point(90, 67);
            this.simpleButton接诊记录.Margin = new System.Windows.Forms.Padding(4);
            this.simpleButton接诊记录.Name = "simpleButton接诊记录";
            this.simpleButton接诊记录.Size = new System.Drawing.Size(104, 24);
            this.simpleButton接诊记录.StyleController = this.layoutControl1;
            this.simpleButton接诊记录.TabIndex = 12;
            this.simpleButton接诊记录.Text = "接诊记录";
            this.simpleButton接诊记录.Click += new System.EventHandler(this.simpleButton接诊记录_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.gridControl1.Location = new System.Drawing.Point(7, 95);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.CheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1176, 462);
            this.gridControl1.TabIndex = 10;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "选择";
            this.gridColumn1.ColumnEdit = this.CheckEdit1;
            this.gridColumn1.FieldName = "booll";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 39;
            // 
            // CheckEdit1
            // 
            this.CheckEdit1.AutoHeight = false;
            this.CheckEdit1.Caption = "Check";
            this.CheckEdit1.Name = "CheckEdit1";
            this.CheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "档案号";
            this.gridColumn2.FieldName = "个人档案编号";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 98;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "姓名";
            this.gridColumn3.FieldName = "姓名";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 98;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "性别";
            this.gridColumn4.FieldName = "性别";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 98;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "出生日期";
            this.gridColumn5.FieldName = "出生日期";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 98;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "身份证号";
            this.gridColumn6.FieldName = "身份证号";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 98;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "居住地址";
            this.gridColumn7.FieldName = "居住地址";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            this.gridColumn7.Width = 98;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "本人电话";
            this.gridColumn8.FieldName = "本人电话";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            this.gridColumn8.Width = 98;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "联系人电话";
            this.gridColumn9.FieldName = "联系人电话";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 8;
            this.gridColumn9.Width = 98;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "所属机构";
            this.gridColumn10.FieldName = "所属机构名称";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 9;
            this.gridColumn10.Width = 128;
            // 
            // textEdit身份证
            // 
            this.textEdit身份证.Location = new System.Drawing.Point(728, 37);
            this.textEdit身份证.Margin = new System.Windows.Forms.Padding(4);
            this.textEdit身份证.Name = "textEdit身份证";
            this.textEdit身份证.Properties.AutoHeight = false;
            this.textEdit身份证.Size = new System.Drawing.Size(206, 26);
            this.textEdit身份证.StyleController = this.layoutControl1;
            this.textEdit身份证.TabIndex = 7;
            this.textEdit身份证.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Main_KeyPress);
            // 
            // textEdit档案号
            // 
            this.textEdit档案号.Location = new System.Drawing.Point(1023, 37);
            this.textEdit档案号.Margin = new System.Windows.Forms.Padding(4);
            this.textEdit档案号.Name = "textEdit档案号";
            this.textEdit档案号.Properties.AutoHeight = false;
            this.textEdit档案号.Size = new System.Drawing.Size(160, 26);
            this.textEdit档案号.StyleController = this.layoutControl1;
            this.textEdit档案号.TabIndex = 6;
            this.textEdit档案号.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Main_KeyPress);
            // 
            // textEdit姓名
            // 
            this.textEdit姓名.Location = new System.Drawing.Point(417, 37);
            this.textEdit姓名.Margin = new System.Windows.Forms.Padding(4);
            this.textEdit姓名.Name = "textEdit姓名";
            this.textEdit姓名.Properties.AutoHeight = false;
            this.textEdit姓名.Size = new System.Drawing.Size(222, 26);
            this.textEdit姓名.StyleController = this.layoutControl1;
            this.textEdit姓名.TabIndex = 5;
            this.textEdit姓名.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Main_KeyPress);
            // 
            // textEdit机构
            // 
            this.textEdit机构.Location = new System.Drawing.Point(92, 37);
            this.textEdit机构.Margin = new System.Windows.Forms.Padding(4);
            this.textEdit机构.Name = "textEdit机构";
            this.textEdit机构.Properties.AutoHeight = false;
            this.textEdit机构.Size = new System.Drawing.Size(206, 26);
            this.textEdit机构.StyleController = this.layoutControl1;
            this.textEdit机构.TabIndex = 4;
            this.textEdit机构.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Main_KeyPress);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gridControl1;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(1180, 466);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton接诊记录;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(83, 60);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(85, 28);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(108, 28);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.simpleButton导出;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(191, 60);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(65, 28);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.simpleButton打印;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(256, 60);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(65, 28);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(1093, 60);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(87, 28);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton关闭;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(321, 60);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(65, 28);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(65, 28);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.simpleButton维护;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(386, 60);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(87, 28);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(128, 28);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.simpleButton个人信息;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(514, 60);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(100, 28);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(156, 28);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.simpleButton用户;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(670, 60);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(63, 28);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(81, 28);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            this.layoutControlItem15.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.groupControl1;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(1180, 30);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit机构;
            this.layoutControlItem1.CustomizationFormText = "机构";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(93, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(295, 30);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "机构(模糊查询)";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名";
            this.layoutControlItem2.Location = new System.Drawing.Point(325, 30);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(93, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(311, 30);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "姓名(模糊查询)";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(82, 14);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.simpleButton选择机构;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(295, 30);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(30, 30);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(30, 30);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(30, 30);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            this.layoutControlItem12.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit身份证;
            this.layoutControlItem4.CustomizationFormText = "身份证";
            this.layoutControlItem4.Location = new System.Drawing.Point(636, 30);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(93, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(295, 30);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证(精确)";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(82, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit档案号;
            this.layoutControlItem3.CustomizationFormText = "档案号";
            this.layoutControlItem3.Location = new System.Drawing.Point(931, 30);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(93, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(249, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "档案号(精确)";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(82, 14);
            // 
            // btn查看体检信息
            // 
            this.btn查看体检信息.Image = ((System.Drawing.Image)(resources.GetObject("btn查看体检信息.Image")));
            this.btn查看体检信息.Location = new System.Drawing.Point(758, 67);
            this.btn查看体检信息.Name = "btn查看体检信息";
            this.btn查看体检信息.Size = new System.Drawing.Size(102, 22);
            this.btn查看体检信息.StyleController = this.layoutControl1;
            this.btn查看体检信息.TabIndex = 22;
            this.btn查看体检信息.Text = "查看体检信息";
            this.btn查看体检信息.Click += new System.EventHandler(this.btn查看体检信息_Click);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btn查看体检信息;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(751, 60);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(106, 28);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // btn查看糖尿病随访
            // 
            this.btn查看糖尿病随访.Image = ((System.Drawing.Image)(resources.GetObject("btn查看糖尿病随访.Image")));
            this.btn查看糖尿病随访.Location = new System.Drawing.Point(864, 67);
            this.btn查看糖尿病随访.Name = "btn查看糖尿病随访";
            this.btn查看糖尿病随访.Size = new System.Drawing.Size(114, 22);
            this.btn查看糖尿病随访.StyleController = this.layoutControl1;
            this.btn查看糖尿病随访.TabIndex = 25;
            this.btn查看糖尿病随访.Text = "查看糖尿病随访";
            this.btn查看糖尿病随访.Click += new System.EventHandler(this.btn查看糖尿病随访_Click);
            // 
            // btn查看高血压随访
            // 
            this.btn查看高血压随访.Image = ((System.Drawing.Image)(resources.GetObject("btn查看高血压随访.Image")));
            this.btn查看高血压随访.Location = new System.Drawing.Point(982, 67);
            this.btn查看高血压随访.Name = "btn查看高血压随访";
            this.btn查看高血压随访.Size = new System.Drawing.Size(114, 22);
            this.btn查看高血压随访.StyleController = this.layoutControl1;
            this.btn查看高血压随访.TabIndex = 24;
            this.btn查看高血压随访.Text = "查看高血压随访";
            this.btn查看高血压随访.Click += new System.EventHandler(this.btn查看高血压随访_Click);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.btn查看糖尿病随访;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(857, 60);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(118, 28);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.btn查看高血压随访;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(975, 60);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(118, 28);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1190, 564);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Frm_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Frm_Main_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEdit机构;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton查询;
        private DevExpress.XtraEditors.TextEdit textEdit身份证;
        private DevExpress.XtraEditors.TextEdit textEdit档案号;
        private DevExpress.XtraEditors.TextEdit textEdit姓名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.SimpleButton simpleButton接诊记录;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.SimpleButton simpleButton打印;
        private DevExpress.XtraEditors.SimpleButton simpleButton导出;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton simpleButton关闭;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.SimpleButton simpleButton选择机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.SimpleButton simpleButton维护;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SimpleButton simpleButton个人信息;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.SimpleButton simpleButton用户;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit CheckEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.SimpleButton btn查看糖尿病随访;
        private DevExpress.XtraEditors.SimpleButton btn查看高血压随访;
        private DevExpress.XtraEditors.SimpleButton btn查看体检信息;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;


    }
}