﻿using DevExpress.XtraTabbedMdi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.InterFaceJKDAN
{
    public partial class Frm_个人档案Main : DevExpress.XtraEditors.XtraForm
    {
        public Frm_个人档案Main()
        {
            InitializeComponent();
        }

        public Frm_个人档案Main(string IDCard)
        {
            InitializeComponent();
            if (!string.IsNullOrEmpty(IDCard))
            {
                Frm_个人档案 frm = new Frm_个人档案(IDCard);
                frm.OpenLink += OpenLink;
                OpenLink(frm, "个人档案");
            }
        }

        private void Frm_TabMain_Load(object sender, EventArgs e)
        {

        }


        private void OpenLink(Form itemForm, string itemHeader)
        {
            int itemCount = 0;
            ////遍历已打开的TabPage,若发现已打开过则不再重复打开并设置为Active
            itemCount = xtraTabbedMdiManager1.Pages.Count;
            foreach (XtraMdiTabPage itemPage in xtraTabbedMdiManager1.Pages)
            {
                if (itemHeader == itemPage.Text)
                {
                    //MessageBox.Show("the page has been openned");
                    //设置活动Tab Page
                    xtraTabbedMdiManager1.SelectedPage = itemPage;
                    return;
                }
            }
            itemForm.MdiParent = this;
            itemForm.Text = itemHeader;
            itemForm.Show();
            //设置为Active
            xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages[itemCount];
        }

    }
}
