﻿using DevExpress.XtraEditors;
namespace DXTempLate
{
    partial class frm后台操作
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm后台操作));
            this.btn查看未上传体质辨识数据 = new DevExpress.XtraEditors.SimpleButton();
            this.btn未上传体检数据 = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEx蓝牙配置 = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEx3 = new DevExpress.XtraEditors.SimpleButton();
            this.btn上传体检信息 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出数据 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查看已体检数据 = new DevExpress.XtraEditors.SimpleButton();
            this.btn已体检中医辨识 = new DevExpress.XtraEditors.SimpleButton();
            this.btn关闭 = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btn查看未上传体质辨识数据
            // 
            this.btn查看未上传体质辨识数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn查看未上传体质辨识数据.Appearance.Options.UseFont = true;
            this.btn查看未上传体质辨识数据.Appearance.Options.UseTextOptions = true;
            this.btn查看未上传体质辨识数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn查看未上传体质辨识数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn查看未上传体质辨识数据.Image = ((System.Drawing.Image)(resources.GetObject("btn查看未上传体质辨识数据.Image")));
            this.btn查看未上传体质辨识数据.Location = new System.Drawing.Point(0, 251);
            this.btn查看未上传体质辨识数据.Name = "btn查看未上传体质辨识数据";
            this.btn查看未上传体质辨识数据.Size = new System.Drawing.Size(367, 52);
            this.btn查看未上传体质辨识数据.TabIndex = 6;
            this.btn查看未上传体质辨识数据.Text = "查看未上传体质辨识数据";
            this.btn查看未上传体质辨识数据.Click += new System.EventHandler(this.btn查看未上传体质辨识数据_Click);
            // 
            // btn未上传体检数据
            // 
            this.btn未上传体检数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn未上传体检数据.Appearance.Options.UseFont = true;
            this.btn未上传体检数据.Appearance.Options.UseTextOptions = true;
            this.btn未上传体检数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn未上传体检数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn未上传体检数据.Image = ((System.Drawing.Image)(resources.GetObject("btn未上传体检数据.Image")));
            this.btn未上传体检数据.Location = new System.Drawing.Point(0, 199);
            this.btn未上传体检数据.Name = "btn未上传体检数据";
            this.btn未上传体检数据.Size = new System.Drawing.Size(367, 52);
            this.btn未上传体检数据.TabIndex = 5;
            this.btn未上传体检数据.Text = "查看未上传体检数据";
            this.btn未上传体检数据.Click += new System.EventHandler(this.btn未上传体检数据_Click);
            // 
            // buttonEx蓝牙配置
            // 
            this.buttonEx蓝牙配置.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.buttonEx蓝牙配置.Appearance.Options.UseFont = true;
            this.buttonEx蓝牙配置.Appearance.Options.UseTextOptions = true;
            this.buttonEx蓝牙配置.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.buttonEx蓝牙配置.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonEx蓝牙配置.Image = ((System.Drawing.Image)(resources.GetObject("buttonEx蓝牙配置.Image")));
            this.buttonEx蓝牙配置.Location = new System.Drawing.Point(0, 0);
            this.buttonEx蓝牙配置.Name = "buttonEx蓝牙配置";
            this.buttonEx蓝牙配置.Size = new System.Drawing.Size(367, 52);
            this.buttonEx蓝牙配置.TabIndex = 1;
            this.buttonEx蓝牙配置.Text = "蓝牙/系统配置";
            this.buttonEx蓝牙配置.Click += new System.EventHandler(this.buttonEx蓝牙配置_Click);
            // 
            // buttonEx3
            // 
            this.buttonEx3.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.buttonEx3.Appearance.Options.UseFont = true;
            this.buttonEx3.Appearance.Options.UseTextOptions = true;
            this.buttonEx3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.buttonEx3.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonEx3.Image = ((System.Drawing.Image)(resources.GetObject("buttonEx3.Image")));
            this.buttonEx3.Location = new System.Drawing.Point(0, 148);
            this.buttonEx3.Name = "buttonEx3";
            this.buttonEx3.Size = new System.Drawing.Size(367, 51);
            this.buttonEx3.TabIndex = 4;
            this.buttonEx3.Text = "上传体质辨识信息";
            this.buttonEx3.Click += new System.EventHandler(this.btn上传体质辨识_Click);
            // 
            // btn上传体检信息
            // 
            this.btn上传体检信息.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn上传体检信息.Appearance.Options.UseFont = true;
            this.btn上传体检信息.Appearance.Options.UseTextOptions = true;
            this.btn上传体检信息.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn上传体检信息.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn上传体检信息.Image = ((System.Drawing.Image)(resources.GetObject("btn上传体检信息.Image")));
            this.btn上传体检信息.Location = new System.Drawing.Point(0, 100);
            this.btn上传体检信息.Name = "btn上传体检信息";
            this.btn上传体检信息.Size = new System.Drawing.Size(367, 48);
            this.btn上传体检信息.TabIndex = 2;
            this.btn上传体检信息.Text = "上传体检信息";
            this.btn上传体检信息.Click += new System.EventHandler(this.btn上传体检信息_Click);
            // 
            // btn导出数据
            // 
            this.btn导出数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn导出数据.Appearance.Options.UseFont = true;
            this.btn导出数据.Appearance.Options.UseTextOptions = true;
            this.btn导出数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn导出数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn导出数据.Image = ((System.Drawing.Image)(resources.GetObject("btn导出数据.Image")));
            this.btn导出数据.Location = new System.Drawing.Point(0, 52);
            this.btn导出数据.Name = "btn导出数据";
            this.btn导出数据.Size = new System.Drawing.Size(367, 48);
            this.btn导出数据.TabIndex = 0;
            this.btn导出数据.Text = "导入基础信息到本地数据库";
            this.btn导出数据.ToolTip = "档案、用户、机构等基础信息";
            this.btn导出数据.Click += new System.EventHandler(this.btn导出数据_Click);
            // 
            // btn查看已体检数据
            // 
            this.btn查看已体检数据.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn查看已体检数据.Appearance.Options.UseFont = true;
            this.btn查看已体检数据.Appearance.Options.UseTextOptions = true;
            this.btn查看已体检数据.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn查看已体检数据.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn查看已体检数据.Image = ((System.Drawing.Image)(resources.GetObject("btn查看已体检数据.Image")));
            this.btn查看已体检数据.Location = new System.Drawing.Point(0, 303);
            this.btn查看已体检数据.Name = "btn查看已体检数据";
            this.btn查看已体检数据.Size = new System.Drawing.Size(367, 52);
            this.btn查看已体检数据.TabIndex = 8;
            this.btn查看已体检数据.Text = "查看已体检数据";
            this.btn查看已体检数据.Click += new System.EventHandler(this.btn查看已体检数据_Click);
            // 
            // btn已体检中医辨识
            // 
            this.btn已体检中医辨识.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn已体检中医辨识.Appearance.Options.UseFont = true;
            this.btn已体检中医辨识.Appearance.Options.UseTextOptions = true;
            this.btn已体检中医辨识.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btn已体检中医辨识.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn已体检中医辨识.Image = ((System.Drawing.Image)(resources.GetObject("btn已体检中医辨识.Image")));
            this.btn已体检中医辨识.Location = new System.Drawing.Point(0, 355);
            this.btn已体检中医辨识.Name = "btn已体检中医辨识";
            this.btn已体检中医辨识.Size = new System.Drawing.Size(367, 52);
            this.btn已体检中医辨识.TabIndex = 9;
            this.btn已体检中医辨识.Text = "查看已体检中医辨识数据";
            this.btn已体检中医辨识.Click += new System.EventHandler(this.btn已体检中医辨识_Click);
            // 
            // btn关闭
            // 
            this.btn关闭.Appearance.Font = new System.Drawing.Font("Tahoma", 19F, System.Drawing.FontStyle.Bold);
            this.btn关闭.Appearance.Options.UseFont = true;
            this.btn关闭.Appearance.Options.UseTextOptions = true;
            this.btn关闭.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn关闭.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn关闭.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn关闭.Location = new System.Drawing.Point(0, 407);
            this.btn关闭.Name = "btn关闭";
            this.btn关闭.Size = new System.Drawing.Size(367, 52);
            this.btn关闭.TabIndex = 10;
            this.btn关闭.Text = "关 闭";
            this.btn关闭.Click += new System.EventHandler(this.btn关闭_Click);
            // 
            // frm后台操作
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 458);
            this.Controls.Add(this.btn关闭);
            this.Controls.Add(this.btn已体检中医辨识);
            this.Controls.Add(this.btn查看已体检数据);
            this.Controls.Add(this.btn查看未上传体质辨识数据);
            this.Controls.Add(this.btn未上传体检数据);
            this.Controls.Add(this.buttonEx3);
            this.Controls.Add(this.btn上传体检信息);
            this.Controls.Add(this.btn导出数据);
            this.Controls.Add(this.buttonEx蓝牙配置);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm后台操作";
            this.Text = "后台操作";
            this.Load += new System.EventHandler(this.frm后台操作_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButton btn导出数据;
        private SimpleButton btn上传体检信息;
        private SimpleButton buttonEx3;
        private SimpleButton buttonEx蓝牙配置;
        private SimpleButton btn未上传体检数据;
        private SimpleButton btn查看未上传体质辨识数据;
        private SimpleButton btn查看已体检数据;
        private SimpleButton btn已体检中医辨识;
        private SimpleButton btn关闭;
    }
}