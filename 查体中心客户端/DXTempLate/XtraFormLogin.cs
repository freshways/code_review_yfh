﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;

namespace DXTempLate
{
    public partial class XtraFormLogin : DevExpress.XtraEditors.XtraForm
    {
        public XtraFormLogin()
        {
            InitializeComponent();
        }
        private string strIniFile = "";

        private void XtraFormLogin_Load(object sender, EventArgs e)
        {
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;

            strIniFile = Application.StartupPath + @"\Config.ini";
            IniFile iniFile = new IniFile(strIniFile);
            StaticClass.g_TitleName = iniFile.ReadIni("SysInfo", "Title", "收文登记表");
            this.lblTitle.Text = StaticClass.g_TitleName;
            this.Text = StaticClass.g_TitleName;

        }
        
        private void sBOK_Click(object sender, EventArgs e)
        {
            string strUserName = this.txtUserName.Text.Trim();
            string strPassword = this.txtPassword.Text.Trim();
            if (strUserName == "")
            {
                //XtraMessageBox.Show("用户名不能为空!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.None);
                LblMsg.Visible = true;
            }
            else if (strPassword == "")
            {
                //XtraMessageBox.Show("用户密码不能为空!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.None);
                LblMsg.Visible = true;
            }
            else
            {
                SqliteClass sqlite = new SqliteClass();

                int nUserID = 0;
                if (sqlite.CheckUserLogin(strUserName, strPassword, out nUserID))
                {
                    this.DialogResult = DialogResult.Yes;
                    StaticClass.g_CurUserID = nUserID;
                    StaticClass.g_CurUserName = strUserName;

                }
                else
                {
                    XtraMessageBox.Show("输入的用户名或密码不正确!", "系统提示");
                }
            }
        }

    }
}