﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DXTempLate
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //*************************直接启动登陆界面***************************
            //检查当前软件是否已经启动，如果已经启动，提示信息不能重复启动
            bool openExe = false;
            System.Threading.Mutex mm = new System.Threading.Mutex(true, Application.ProductName, out openExe);
            if (!openExe)
            {
                XtraMessageBox.Show("你已经启动该程序！", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //显示登陆用户界面
            XtraFormLogin login = new XtraFormLogin();
            if (login.ShowDialog() == DialogResult.Yes)
            {
                //汉化控件信息
                if (!DevExpress.Skins.SkinManager.AllowFormSkins)
                {                   
                    //引用系统皮肤控件
                    //DevExpress.UserSkins.OfficeSkins.Register();//进行皮肤组件注册
                    DevExpress.Skins.SkinManager.EnableFormSkins();//进行皮肤组件注册
                    DevExpress.UserSkins.BonusSkins.Register();//进行皮肤组件注册
                }
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("zh-CN");
                Application.Run(new FrmMain());
            }
        }
    }
}
