﻿namespace DXTempLate
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.btn首页 = new DevExpress.XtraBars.BarButtonItem();
            this.btn人员维护 = new DevExpress.XtraBars.BarButtonItem();
            this.btn密码修改 = new DevExpress.XtraBars.BarButtonItem();
            this.btn用户信息 = new DevExpress.XtraBars.BarButtonItem();
            this.btn报告打印 = new DevExpress.XtraBars.BarButtonItem();
            this.btn中医体质辨识 = new DevExpress.XtraBars.BarButtonItem();
            this.barBtomExit = new DevExpress.XtraBars.BarButtonItem();
            this.btn数据字典 = new DevExpress.XtraBars.BarButtonItem();
            this.btn工作量统计 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem2 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.btn体检登记 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem3 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.btn数据同步 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup系统管理 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem连接状态 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.btn初始化 = new DevExpress.XtraBars.BarButtonItem();
            this.btn读取配置 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.barHeaderItem1 = new DevExpress.XtraBars.BarHeaderItem();
            this.panelControlMain = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重新连接 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMain)).BeginInit();
            this.panelControlMain.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ribbonControl1.Images = this.imageList1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.ribbonGalleryBarItem1,
            this.btn首页,
            this.btn人员维护,
            this.btn密码修改,
            this.btn用户信息,
            this.btn报告打印,
            this.btn中医体质辨识,
            this.barBtomExit,
            this.btn数据字典,
            this.btn工作量统计,
            this.barButtonItem1,
            this.ribbonGalleryBarItem2,
            this.btn体检登记,
            this.ribbonGalleryBarItem3,
            this.barStaticItem3,
            this.btn数据同步,
            this.btn重新连接});
            this.ribbonControl1.LargeImages = this.imageList1;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 21;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage2,
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(1020, 147);
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbonControl1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "首页.png");
            this.imageList1.Images.SetKeyName(1, "人员管理.png");
            this.imageList1.Images.SetKeyName(2, "密码修改.png");
            this.imageList1.Images.SetKeyName(3, "统计.png");
            this.imageList1.Images.SetKeyName(4, "关于.png");
            this.imageList1.Images.SetKeyName(5, "退出.png");
            this.imageList1.Images.SetKeyName(6, "系统日志.png");
            this.imageList1.Images.SetKeyName(7, "用户信息.png");
            this.imageList1.Images.SetKeyName(8, "字典编码.png");
            this.imageList1.Images.SetKeyName(9, "身高体重.png");
            this.imageList1.Images.SetKeyName(10, "视力.png");
            this.imageList1.Images.SetKeyName(11, "随访.png");
            this.imageList1.Images.SetKeyName(12, "体温.png");
            this.imageList1.Images.SetKeyName(13, "血糖.png");
            this.imageList1.Images.SetKeyName(14, "血压.png");
            this.imageList1.Images.SetKeyName(15, "中医体质辨识.png");
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            // 
            // 
            // 
            this.ribbonGalleryBarItem1.Gallery.ItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.ribbonGalleryBarItem1_Gallery_ItemClick);
            this.ribbonGalleryBarItem1.Id = 1;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // btn首页
            // 
            this.btn首页.Caption = "首页";
            this.btn首页.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn首页.Id = 6;
            this.btn首页.ImageIndex = 0;
            this.btn首页.LargeImageIndex = 0;
            this.btn首页.LargeWidth = 180;
            this.btn首页.Name = "btn首页";
            this.btn首页.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn首页_ItemClick);
            // 
            // btn人员维护
            // 
            this.btn人员维护.Caption = "人员维护";
            this.btn人员维护.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn人员维护.Id = 10;
            this.btn人员维护.ImageIndex = 1;
            this.btn人员维护.LargeImageIndex = 1;
            this.btn人员维护.Name = "btn人员维护";
            this.btn人员维护.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn人员维护_ItemClick);
            // 
            // btn密码修改
            // 
            this.btn密码修改.Caption = "密码修改";
            this.btn密码修改.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn密码修改.Id = 11;
            this.btn密码修改.ImageIndex = 2;
            this.btn密码修改.LargeImageIndex = 2;
            this.btn密码修改.Name = "btn密码修改";
            this.btn密码修改.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn密码修改_ItemClick);
            // 
            // btn用户信息
            // 
            this.btn用户信息.Caption = "用户信息";
            this.btn用户信息.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn用户信息.Id = 13;
            this.btn用户信息.ImageIndex = 1;
            this.btn用户信息.LargeImageIndex = 1;
            this.btn用户信息.Name = "btn用户信息";
            this.btn用户信息.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btn用户信息.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn用户信息_ItemClick);
            // 
            // btn报告打印
            // 
            this.btn报告打印.Caption = "打印报告";
            this.btn报告打印.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn报告打印.Id = 14;
            this.btn报告打印.ImageIndex = 8;
            this.btn报告打印.LargeImageIndex = 8;
            this.btn报告打印.LargeWidth = 100;
            this.btn报告打印.Name = "btn报告打印";
            this.btn报告打印.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn报告打印_ItemClick);
            // 
            // btn中医体质辨识
            // 
            this.btn中医体质辨识.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.btn中医体质辨识.Caption = "中医体质辨识";
            this.btn中医体质辨识.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn中医体质辨识.Id = 15;
            this.btn中医体质辨识.ImageIndex = 15;
            this.btn中医体质辨识.LargeImageIndex = 15;
            this.btn中医体质辨识.LargeWidth = 100;
            this.btn中医体质辨识.Name = "btn中医体质辨识";
            this.btn中医体质辨识.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn中医体质辨识_ItemClick);
            // 
            // barBtomExit
            // 
            this.barBtomExit.Caption = "退出";
            this.barBtomExit.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barBtomExit.Id = 16;
            this.barBtomExit.ImageIndex = 5;
            this.barBtomExit.LargeImageIndex = 5;
            this.barBtomExit.LargeWidth = 90;
            this.barBtomExit.Name = "barBtomExit";
            this.barBtomExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtomExit_ItemClick);
            // 
            // btn数据字典
            // 
            this.btn数据字典.Caption = "数据字典";
            this.btn数据字典.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn数据字典.Id = 17;
            this.btn数据字典.ImageIndex = 6;
            this.btn数据字典.LargeImageIndex = 6;
            this.btn数据字典.Name = "btn数据字典";
            this.btn数据字典.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn数据字典_ItemClick);
            // 
            // btn工作量统计
            // 
            this.btn工作量统计.Caption = "体检统计";
            this.btn工作量统计.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btn工作量统计.Id = 18;
            this.btn工作量统计.ImageIndex = 3;
            this.btn工作量统计.LargeImageIndex = 3;
            this.btn工作量统计.Name = "btn工作量统计";
            this.btn工作量统计.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn工作量统计_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "血压";
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.LargeWidth = 100;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.SmallWithTextWidth = 200;
            // 
            // ribbonGalleryBarItem2
            // 
            this.ribbonGalleryBarItem2.Caption = "ribbonGalleryBarItem2";
            this.ribbonGalleryBarItem2.Id = 8;
            this.ribbonGalleryBarItem2.Name = "ribbonGalleryBarItem2";
            // 
            // btn体检登记
            // 
            this.btn体检登记.Caption = "体检登记";
            this.btn体检登记.Id = 9;
            this.btn体检登记.ImageIndex = 1;
            this.btn体检登记.ItemAppearance.Disabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.btn体检登记.ItemAppearance.Disabled.Options.UseBackColor = true;
            this.btn体检登记.ItemAppearance.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.btn体检登记.ItemAppearance.Hovered.Options.UseBackColor = true;
            this.btn体检登记.ItemAppearance.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.btn体检登记.ItemAppearance.Normal.Options.UseBackColor = true;
            this.btn体检登记.ItemAppearance.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.btn体检登记.ItemAppearance.Pressed.Options.UseBackColor = true;
            this.btn体检登记.ItemInMenuAppearance.Disabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.btn体检登记.ItemInMenuAppearance.Disabled.Options.UseBackColor = true;
            this.btn体检登记.ItemInMenuAppearance.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.btn体检登记.ItemInMenuAppearance.Hovered.Options.UseBackColor = true;
            this.btn体检登记.ItemInMenuAppearance.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.btn体检登记.ItemInMenuAppearance.Normal.Options.UseBackColor = true;
            this.btn体检登记.ItemInMenuAppearance.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.btn体检登记.ItemInMenuAppearance.Pressed.Options.UseBackColor = true;
            this.btn体检登记.LargeImageIndex = 1;
            this.btn体检登记.LargeWidth = 100;
            this.btn体检登记.Name = "btn体检登记";
            this.btn体检登记.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn体检登记_ItemClick);
            // 
            // ribbonGalleryBarItem3
            // 
            this.ribbonGalleryBarItem3.Caption = "ribbonGalleryBarItem3";
            // 
            // 
            // 
            galleryItemGroup1.Caption = "Group1";
            galleryItem1.Caption = "Item1";
            galleryItem1.Description = "1";
            galleryItem1.Hint = "1";
            galleryItem2.Caption = "Item2";
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2});
            this.ribbonGalleryBarItem3.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.ribbonGalleryBarItem3.Id = 13;
            this.ribbonGalleryBarItem3.Name = "ribbonGalleryBarItem3";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "当前时间";
            this.barStaticItem3.Id = 17;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btn数据同步
            // 
            this.btn数据同步.Caption = "数据同步";
            this.btn数据同步.Id = 18;
            this.btn数据同步.ImageIndex = 4;
            this.btn数据同步.LargeImageIndex = 4;
            this.btn数据同步.LargeWidth = 80;
            this.btn数据同步.Name = "btn数据同步";
            this.btn数据同步.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn数据同步_ItemClick);
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup5,
            this.ribbonPageGroup1,
            this.ribbonPageGroup3,
            this.ribbonPageGroup6});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "健康体检";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barBtomExit);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btn首页);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btn体检登记);
            this.ribbonPageGroup3.ItemLinks.Add(this.btn报告打印);
            this.ribbonPageGroup3.ItemLinks.Add(this.btn中医体质辨识);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.btn数据同步);
            this.ribbonPageGroup6.ItemLinks.Add(this.btn重新连接);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4,
            this.ribbonPageGroup2,
            this.ribbonPageGroup系统管理});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "系统管理";
            this.ribbonPage1.Visible = false;
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btn工作量统计);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.ribbonGalleryBarItem1);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            // 
            // ribbonPageGroup系统管理
            // 
            this.ribbonPageGroup系统管理.ItemLinks.Add(this.btn密码修改);
            this.ribbonPageGroup系统管理.ItemLinks.Add(this.btn用户信息);
            this.ribbonPageGroup系统管理.ItemLinks.Add(this.btn人员维护);
            this.ribbonPageGroup系统管理.ItemLinks.Add(this.btn数据字典);
            this.ribbonPageGroup系统管理.Name = "ribbonPageGroup系统管理";
            this.ribbonPageGroup系统管理.ShowCaptionButton = false;
            this.ribbonPageGroup系统管理.Text = "系统管理";
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem连接状态,
            this.btn初始化,
            this.barStaticItem2,
            this.barHeaderItem1,
            this.btn读取配置});
            this.barManager1.MaxItemId = 9;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem连接状态),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn初始化),
            new DevExpress.XtraBars.LinkPersistInfo(this.btn读取配置)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "连接状态:";
            this.barStaticItem2.Id = 5;
            this.barStaticItem2.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Blue;
            this.barStaticItem2.ItemAppearance.Normal.Options.UseForeColor = true;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem连接状态
            // 
            this.barStaticItem连接状态.Caption = "等待连接...";
            this.barStaticItem连接状态.Id = 3;
            this.barStaticItem连接状态.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Green;
            this.barStaticItem连接状态.ItemAppearance.Normal.Options.UseForeColor = true;
            this.barStaticItem连接状态.Name = "barStaticItem连接状态";
            this.barStaticItem连接状态.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "          技术支持：临沂市阳光科技有限公司          ";
            this.barStaticItem1.Id = 1;
            this.barStaticItem1.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Purple;
            this.barStaticItem1.ItemAppearance.Normal.Options.UseForeColor = true;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btn初始化
            // 
            this.btn初始化.Caption = "连接设备";
            this.btn初始化.Id = 4;
            this.btn初始化.Name = "btn初始化";
            this.btn初始化.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn初始化_ItemClick);
            // 
            // btn读取配置
            // 
            this.btn读取配置.Caption = "读取配置";
            this.btn读取配置.Id = 8;
            this.btn读取配置.Name = "btn读取配置";
            this.btn读取配置.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn读取配置_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1020, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 583);
            this.barDockControlBottom.Size = new System.Drawing.Size(1020, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 583);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1020, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 583);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // barHeaderItem1
            // 
            this.barHeaderItem1.Caption = "barHeaderItem1";
            this.barHeaderItem1.Id = 6;
            this.barHeaderItem1.Name = "barHeaderItem1";
            // 
            // panelControlMain
            // 
            this.panelControlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.panelControlMain.Controls.Add(this.flowLayoutPanel1);
            this.panelControlMain.Location = new System.Drawing.Point(265, 153);
            this.panelControlMain.Name = "panelControlMain";
            this.panelControlMain.Size = new System.Drawing.Size(669, 407);
            this.panelControlMain.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton2);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton3);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton4);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton5);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton6);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(39, 96);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(546, 306);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(188)))), ((int)(((byte)(154)))));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton1.Image = global::DXTempLate.Properties.Resources.身高体重;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton1.Location = new System.Drawing.Point(3, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(120, 125);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "身高体重";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(200)))), ((int)(((byte)(83)))));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton2.Image = global::DXTempLate.Properties.Resources.体温;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton2.Location = new System.Drawing.Point(129, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(120, 125);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "体温";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton3.Image = global::DXTempLate.Properties.Resources.视力;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton3.Location = new System.Drawing.Point(255, 3);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(120, 125);
            this.simpleButton3.TabIndex = 0;
            this.simpleButton3.Text = "视力";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(88)))), ((int)(((byte)(181)))));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton4.Image = global::DXTempLate.Properties.Resources.血压;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton4.Location = new System.Drawing.Point(381, 3);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(120, 125);
            this.simpleButton4.TabIndex = 0;
            this.simpleButton4.Text = "血压";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(0)))));
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton5.Image = global::DXTempLate.Properties.Resources.血糖;
            this.simpleButton5.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton5.Location = new System.Drawing.Point(3, 134);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(120, 125);
            this.simpleButton5.TabIndex = 0;
            this.simpleButton5.Text = "血糖";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(76)))), ((int)(((byte)(61)))));
            this.simpleButton6.Appearance.Options.UseBackColor = true;
            this.simpleButton6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton6.Image = global::DXTempLate.Properties.Resources.中医体质辨识;
            this.simpleButton6.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton6.Location = new System.Drawing.Point(129, 134);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(120, 125);
            this.simpleButton6.TabIndex = 0;
            this.simpleButton6.Text = "中医体质辨识";
            // 
            // btn重新连接
            // 
            this.btn重新连接.Caption = "重新连接设备";
            this.btn重新连接.Id = 20;
            this.btn重新连接.ImageIndex = 4;
            this.btn重新连接.LargeImageIndex = 4;
            this.btn重新连接.Name = "btn重新连接";
            this.btn重新连接.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btn重新连接_ItemClick);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 610);
            this.Controls.Add(this.panelControlMain);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMain";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "[阳光软件]";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMain)).EndInit();
            this.panelControlMain.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.BarButtonItem btn首页;
        private DevExpress.XtraBars.BarButtonItem btn人员维护;
        private DevExpress.XtraBars.BarButtonItem btn密码修改;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarButtonItem btn用户信息;
        private DevExpress.XtraBars.BarButtonItem btn报告打印;
        private DevExpress.XtraBars.BarButtonItem btn中医体质辨识;
        private DevExpress.XtraBars.BarButtonItem barBtomExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup系统管理;
        private DevExpress.XtraEditors.PanelControl panelControlMain;
        private DevExpress.XtraBars.BarButtonItem btn数据字典;
        private DevExpress.XtraBars.BarButtonItem btn工作量统计;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraBars.BarButtonItem btn体检登记;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem3;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem连接状态;
        private DevExpress.XtraBars.BarButtonItem btn初始化;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarHeaderItem barHeaderItem1;
        private DevExpress.XtraBars.BarButtonItem btn读取配置;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem btn数据同步;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem btn重新连接;
    }
}

