﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DXTempLate
{
    public class tb_中医体质指导
    {
        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _编号;
        /// <summary>
        /// 获取或设置 身份证号 的值
        /// </summary>
        public string 编号
        {
            get { return _编号; }
            set { _编号 = value; }
        }

        private string _类型;
        /// <summary>
        /// 获取或设置 姓名 的值
        /// </summary>
        public string 类型
        {
            get { return _类型; }
            set { _类型 = value; }
        }

        private string _内容;
        /// <summary>
        /// 获取或设置 性别 的值
        /// </summary>
        public string 内容
        {
            get { return _内容; }
            set { _内容 = value; }
        }
    }
}
