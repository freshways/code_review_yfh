﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DXTempLate
{
    public class tb_健康体检Info
    {
        public int RowID { get; set; }

        private string _ID;
        /// <summary>
        /// 获取或设置 ID 的值
        /// </summary>
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _身份证号;
        /// <summary>
        /// 获取或设置 身份证号 的值
        /// </summary>
        public string 身份证号
        {
            get { return _身份证号; }
            set { _身份证号 = value; }
        }

        private string _姓名;
        /// <summary>
        /// 获取或设置 姓名 的值
        /// </summary>
        public string 姓名
        {
            get { return _姓名; }
            set { _姓名 = value; }
        }

        private string _性别;
        /// <summary>
        /// 获取或设置 性别 的值
        /// </summary>
        public string 性别
        {
            get { return _性别; }
            set { _性别 = value; }
        }

        private string _出生日期;
        /// <summary>
        /// 获取或设置 出生日期 的值
        /// </summary>
        public string 出生日期
        {
            get { return _出生日期; }
            set { _出生日期 = value; }
        }

        public string  住址 { get; set; }

        public string  联系电话 { get; set; }

        public string 备注 { get; set; }

        private string _症状;
        /// <summary>
        /// 获取或设置 症状 的值
        /// </summary>
        public string 症状
        {
            get { return _症状; }
            set { _症状 = value; }
        }

        private double _体温;
        /// <summary>
        /// 获取或设置 体温 的值
        /// </summary>
        public double 体温
        {
            get { return _体温; }
            set { _体温 = value; }
        }

        private int _呼吸;
        /// <summary>
        /// 获取或设置 呼吸 的值
        /// </summary>
        public int 呼吸
        {
            get { return _呼吸; }
            set { _呼吸 = value; }
        }

        private int _脉搏;
        /// <summary>
        /// 获取或设置 脉搏 的值
        /// </summary>
        public int 脉搏
        {
            get { return _脉搏; }
            set { _脉搏 = value; }
        }

        private string _血压右侧1;
        /// <summary>
        /// 获取或设置 血压右侧1 的值
        /// </summary>
        public string 血压右侧1
        {
            get { return _血压右侧1; }
            set { _血压右侧1 = value; }
        }

        private string _血压右侧2;
        /// <summary>
        /// 获取或设置 血压右侧2 的值
        /// </summary>
        public string 血压右侧2
        {
            get { return _血压右侧2; }
            set { _血压右侧2 = value; }
        }

        private string _血压左侧1;
        /// <summary>
        /// 获取或设置 血压左侧1 的值
        /// </summary>
        public string 血压左侧1
        {
            get { return _血压左侧1; }
            set { _血压左侧1 = value; }
        }

        private string _血压左侧2;
        /// <summary>
        /// 获取或设置 血压左侧2 的值
        /// </summary>
        public string 血压左侧2
        {
            get { return _血压左侧2; }
            set { _血压左侧2 = value; }
        }

        private double _身高;
        /// <summary>
        /// 获取或设置 身高 的值
        /// </summary>
        public double 身高
        {
            get { return _身高; }
            set { _身高 = value; }
        }

        private double _腰围;
        /// <summary>
        /// 获取或设置 腰围 的值
        /// </summary>
        public double 腰围
        {
            get { return _腰围; }
            set { _腰围 = value; }
        }

        private double _体重;
        /// <summary>
        /// 获取或设置 体重 的值
        /// </summary>
        public double 体重
        {
            get { return _体重; }
            set { _体重 = value; }
        }

        private string _体重指数;
        /// <summary>
        /// 获取或设置 体重指数 的值
        /// </summary>
        public string 体重指数
        {
            get { return _体重指数; }
            set { _体重指数 = value; }
        }

        private string _老年人认知;
        /// <summary>
        /// 获取或设置 老年人认知 的值
        /// </summary>
        public string 老年人认知
        {
            get { return _老年人认知; }
            set { _老年人认知 = value; }
        }

        private string _老年人情感;
        /// <summary>
        /// 获取或设置 老年人情感 的值
        /// </summary>
        public string 老年人情感
        {
            get { return _老年人情感; }
            set { _老年人情感 = value; }
        }

        private string _老年人认知分;
        /// <summary>
        /// 获取或设置 老年人认知分 的值
        /// </summary>
        public string 老年人认知分
        {
            get { return _老年人认知分; }
            set { _老年人认知分 = value; }
        }

        private string _老年人情感分;
        /// <summary>
        /// 获取或设置 老年人情感分 的值
        /// </summary>
        public string 老年人情感分
        {
            get { return _老年人情感分; }
            set { _老年人情感分 = value; }
        }

        private double _左眼视力;
        /// <summary>
        /// 获取或设置 左眼视力 的值
        /// </summary>
        public double 左眼视力
        {
            get { return _左眼视力; }
            set { _左眼视力 = value; }
        }

        private double _右眼视力;
        /// <summary>
        /// 获取或设置 右眼视力 的值
        /// </summary>
        public double 右眼视力
        {
            get { return _右眼视力; }
            set { _右眼视力 = value; }
        }

        private string _左眼矫正;
        /// <summary>
        /// 获取或设置 左眼矫正 的值
        /// </summary>
        public string 左眼矫正
        {
            get { return _左眼矫正; }
            set { _左眼矫正 = value; }
        }

        private string _右眼矫正;
        /// <summary>
        /// 获取或设置 右眼矫正 的值
        /// </summary>
        public string 右眼矫正
        {
            get { return _右眼矫正; }
            set { _右眼矫正 = value; }
        }

        private string _听力;
        /// <summary>
        /// 获取或设置 听力 的值
        /// </summary>
        public string 听力
        {
            get { return _听力; }
            set { _听力 = value; }
        }

        private string _运动功能;
        /// <summary>
        /// 获取或设置 运动功能 的值
        /// </summary>
        public string 运动功能
        {
            get { return _运动功能; }
            set { _运动功能 = value; }
        }

        private string _皮肤;
        /// <summary>
        /// 获取或设置 皮肤 的值
        /// </summary>
        public string 皮肤
        {
            get { return _皮肤; }
            set { _皮肤 = value; }
        }

        private string _淋巴结;
        /// <summary>
        /// 获取或设置 淋巴结 的值
        /// </summary>
        public string 淋巴结
        {
            get { return _淋巴结; }
            set { _淋巴结 = value; }
        }

        private string _淋巴结其他;
        /// <summary>
        /// 获取或设置 淋巴结其他 的值
        /// </summary>
        public string 淋巴结其他
        {
            get { return _淋巴结其他; }
            set { _淋巴结其他 = value; }
        }

        private string _桶状胸;
        /// <summary>
        /// 获取或设置 桶状胸 的值
        /// </summary>
        public string 桶状胸
        {
            get { return _桶状胸; }
            set { _桶状胸 = value; }
        }

        private string _呼吸音;
        /// <summary>
        /// 获取或设置 呼吸音 的值
        /// </summary>
        public string 呼吸音
        {
            get { return _呼吸音; }
            set { _呼吸音 = value; }
        }

        private string _呼吸音异常;
        /// <summary>
        /// 获取或设置 呼吸音异常 的值
        /// </summary>
        public string 呼吸音异常
        {
            get { return _呼吸音异常; }
            set { _呼吸音异常 = value; }
        }

        private string _罗音;
        /// <summary>
        /// 获取或设置 罗音 的值
        /// </summary>
        public string 罗音
        {
            get { return _罗音; }
            set { _罗音 = value; }
        }

        private string _罗音异常;
        /// <summary>
        /// 获取或设置 罗音异常 的值
        /// </summary>
        public string 罗音异常
        {
            get { return _罗音异常; }
            set { _罗音异常 = value; }
        }

        private int _心率;
        /// <summary>
        /// 获取或设置 心率 的值
        /// </summary>
        public int 心率
        {
            get { return _心率; }
            set { _心率 = value; }
        }

        private string _心律;
        /// <summary>
        /// 获取或设置 心律 的值
        /// </summary>
        public string 心律
        {
            get { return _心律; }
            set { _心律 = value; }
        }

        private string _杂音;
        /// <summary>
        /// 获取或设置 杂音 的值
        /// </summary>
        public string 杂音
        {
            get { return _杂音; }
            set { _杂音 = value; }
        }

        private string _杂音有;
        /// <summary>
        /// 获取或设置 杂音有 的值
        /// </summary>
        public string 杂音有
        {
            get { return _杂音有; }
            set { _杂音有 = value; }
        }

        private string _压痛;
        /// <summary>
        /// 获取或设置 压痛 的值
        /// </summary>
        public string 压痛
        {
            get { return _压痛; }
            set { _压痛 = value; }
        }

        private string _压痛有;
        /// <summary>
        /// 获取或设置 压痛有 的值
        /// </summary>
        public string 压痛有
        {
            get { return _压痛有; }
            set { _压痛有 = value; }
        }

        private string _包块;
        /// <summary>
        /// 获取或设置 包块 的值
        /// </summary>
        public string 包块
        {
            get { return _包块; }
            set { _包块 = value; }
        }

        private string _包块有;
        /// <summary>
        /// 获取或设置 包块有 的值
        /// </summary>
        public string 包块有
        {
            get { return _包块有; }
            set { _包块有 = value; }
        }

        private string _肝大;
        /// <summary>
        /// 获取或设置 肝大 的值
        /// </summary>
        public string 肝大
        {
            get { return _肝大; }
            set { _肝大 = value; }
        }

        private string _肝大有;
        /// <summary>
        /// 获取或设置 肝大有 的值
        /// </summary>
        public string 肝大有
        {
            get { return _肝大有; }
            set { _肝大有 = value; }
        }

        private string _脾大;
        /// <summary>
        /// 获取或设置 脾大 的值
        /// </summary>
        public string 脾大
        {
            get { return _脾大; }
            set { _脾大 = value; }
        }

        private string _脾大有;
        /// <summary>
        /// 获取或设置 脾大有 的值
        /// </summary>
        public string 脾大有
        {
            get { return _脾大有; }
            set { _脾大有 = value; }
        }

        private string _浊音;
        /// <summary>
        /// 获取或设置 浊音 的值
        /// </summary>
        public string 浊音
        {
            get { return _浊音; }
            set { _浊音 = value; }
        }

        private string _浊音有;
        /// <summary>
        /// 获取或设置 浊音有 的值
        /// </summary>
        public string 浊音有
        {
            get { return _浊音有; }
            set { _浊音有 = value; }
        }

        private string _下肢水肿;
        /// <summary>
        /// 获取或设置 下肢水肿 的值
        /// </summary>
        public string 下肢水肿
        {
            get { return _下肢水肿; }
            set { _下肢水肿 = value; }
        }

        private string _肛门指诊;
        /// <summary>
        /// 获取或设置 肛门指诊 的值
        /// </summary>
        public string 肛门指诊
        {
            get { return _肛门指诊; }
            set { _肛门指诊 = value; }
        }

        private string _肛门指诊异常;
        /// <summary>
        /// 获取或设置 肛门指诊异常 的值
        /// </summary>
        public string 肛门指诊异常
        {
            get { return _肛门指诊异常; }
            set { _肛门指诊异常 = value; }
        }

        private string _G_QLX;
        /// <summary>
        /// 获取或设置 G_QLX 的值
        /// </summary>
        public string G_QLX
        {
            get { return _G_QLX; }
            set { _G_QLX = value; }
        }

        private string _查体其他;
        /// <summary>
        /// 获取或设置 查体其他 的值
        /// </summary>
        public string 查体其他
        {
            get { return _查体其他; }
            set { _查体其他 = value; }
        }

        private string _白细胞;
        /// <summary>
        /// 获取或设置 白细胞 的值
        /// </summary>
        public string 白细胞
        {
            get { return _白细胞; }
            set { _白细胞 = value; }
        }

        private string _血红蛋白;
        /// <summary>
        /// 获取或设置 血红蛋白 的值
        /// </summary>
        public string 血红蛋白
        {
            get { return _血红蛋白; }
            set { _血红蛋白 = value; }
        }

        private string _血小板;
        /// <summary>
        /// 获取或设置 血小板 的值
        /// </summary>
        public string 血小板
        {
            get { return _血小板; }
            set { _血小板 = value; }
        }

        private string _血常规其他;
        /// <summary>
        /// 获取或设置 血常规其他 的值
        /// </summary>
        public string 血常规其他
        {
            get { return _血常规其他; }
            set { _血常规其他 = value; }
        }

        private string _尿蛋白;
        /// <summary>
        /// 获取或设置 尿蛋白 的值
        /// </summary>
        public string 尿蛋白
        {
            get { return _尿蛋白; }
            set { _尿蛋白 = value; }
        }

        private string _尿糖;
        /// <summary>
        /// 获取或设置 尿糖 的值
        /// </summary>
        public string 尿糖
        {
            get { return _尿糖; }
            set { _尿糖 = value; }
        }

        private string _尿酮体;
        /// <summary>
        /// 获取或设置 尿酮体 的值
        /// </summary>
        public string 尿酮体
        {
            get { return _尿酮体; }
            set { _尿酮体 = value; }
        }

        private string _尿潜血;
        /// <summary>
        /// 获取或设置 尿潜血 的值
        /// </summary>
        public string 尿潜血
        {
            get { return _尿潜血; }
            set { _尿潜血 = value; }
        }

        private string _尿常规其他;
        /// <summary>
        /// 获取或设置 尿常规其他 的值
        /// </summary>
        public string 尿常规其他
        {
            get { return _尿常规其他; }
            set { _尿常规其他 = value; }
        }

        private string _大便潜血;
        /// <summary>
        /// 获取或设置 大便潜血 的值
        /// </summary>
        public string 大便潜血
        {
            get { return _大便潜血; }
            set { _大便潜血 = value; }
        }

        private string _血清谷丙转氨酶;
        /// <summary>
        /// 获取或设置 血清谷丙转氨酶 的值
        /// </summary>
        public string 血清谷丙转氨酶
        {
            get { return _血清谷丙转氨酶; }
            set { _血清谷丙转氨酶 = value; }
        }

        private string _血清谷草转氨酶;
        /// <summary>
        /// 获取或设置 血清谷草转氨酶 的值
        /// </summary>
        public string 血清谷草转氨酶
        {
            get { return _血清谷草转氨酶; }
            set { _血清谷草转氨酶 = value; }
        }

        private string _白蛋白;
        /// <summary>
        /// 获取或设置 白蛋白 的值
        /// </summary>
        public string 白蛋白
        {
            get { return _白蛋白; }
            set { _白蛋白 = value; }
        }

        private string _总胆红素;
        /// <summary>
        /// 获取或设置 总胆红素 的值
        /// </summary>
        public string 总胆红素
        {
            get { return _总胆红素; }
            set { _总胆红素 = value; }
        }

        private string _结合胆红素;
        /// <summary>
        /// 获取或设置 结合胆红素 的值
        /// </summary>
        public string 结合胆红素
        {
            get { return _结合胆红素; }
            set { _结合胆红素 = value; }
        }

        private string _血清肌酐;
        /// <summary>
        /// 获取或设置 血清肌酐 的值
        /// </summary>
        public string 血清肌酐
        {
            get { return _血清肌酐; }
            set { _血清肌酐 = value; }
        }

        private string _血尿素氮;
        /// <summary>
        /// 获取或设置 血尿素氮 的值
        /// </summary>
        public string 血尿素氮
        {
            get { return _血尿素氮; }
            set { _血尿素氮 = value; }
        }

        private string _总胆固醇;
        /// <summary>
        /// 获取或设置 总胆固醇 的值
        /// </summary>
        public string 总胆固醇
        {
            get { return _总胆固醇; }
            set { _总胆固醇 = value; }
        }

        private string _甘油三酯;
        /// <summary>
        /// 获取或设置 甘油三酯 的值
        /// </summary>
        public string 甘油三酯
        {
            get { return _甘油三酯; }
            set { _甘油三酯 = value; }
        }

        private string _血清低密度脂蛋白胆固醇;
        /// <summary>
        /// 获取或设置 血清低密度脂蛋白胆固醇 的值
        /// </summary>
        public string 血清低密度脂蛋白胆固醇
        {
            get { return _血清低密度脂蛋白胆固醇; }
            set { _血清低密度脂蛋白胆固醇 = value; }
        }

        private string _血清高密度脂蛋白胆固醇;
        /// <summary>
        /// 获取或设置 血清高密度脂蛋白胆固醇 的值
        /// </summary>
        public string 血清高密度脂蛋白胆固醇
        {
            get { return _血清高密度脂蛋白胆固醇; }
            set { _血清高密度脂蛋白胆固醇 = value; }
        }

        private string _空腹血糖;
        /// <summary>
        /// 获取或设置 空腹血糖 的值
        /// </summary>
        public string 空腹血糖
        {
            get { return _空腹血糖; }
            set { _空腹血糖 = value; }
        }

        private string _乙型肝炎表面抗原;
        /// <summary>
        /// 获取或设置 乙型肝炎表面抗原 的值
        /// </summary>
        public string 乙型肝炎表面抗原
        {
            get { return _乙型肝炎表面抗原; }
            set { _乙型肝炎表面抗原 = value; }
        }

        private string _眼底;
        /// <summary>
        /// 获取或设置 眼底 的值
        /// </summary>
        public string 眼底
        {
            get { return _眼底; }
            set { _眼底 = value; }
        }

        private string _眼底异常;
        /// <summary>
        /// 获取或设置 眼底异常 的值
        /// </summary>
        public string 眼底异常
        {
            get { return _眼底异常; }
            set { _眼底异常 = value; }
        }

        private string _心电图;
        /// <summary>
        /// 获取或设置 心电图 的值
        /// </summary>
        public string 心电图
        {
            get { return _心电图; }
            set { _心电图 = value; }
        }

        private string _心电图异常;
        /// <summary>
        /// 获取或设置 心电图异常 的值
        /// </summary>
        public string 心电图异常
        {
            get { return _心电图异常; }
            set { _心电图异常 = value; }
        }

        private string _胸部X线片;
        /// <summary>
        /// 获取或设置 胸部X线片 的值
        /// </summary>
        public string 胸部X线片
        {
            get { return _胸部X线片; }
            set { _胸部X线片 = value; }
        }

        private string _胸部X线片异常;
        /// <summary>
        /// 获取或设置 胸部X线片异常 的值
        /// </summary>
        public string 胸部X线片异常
        {
            get { return _胸部X线片异常; }
            set { _胸部X线片异常 = value; }
        }

        private string _B超;
        /// <summary>
        /// 获取或设置 B超 的值
        /// </summary>
        public string B超
        {
            get { return _B超; }
            set { _B超 = value; }
        }

        private string _B超其他;
        /// <summary>
        /// 获取或设置 B超其他 的值
        /// </summary>
        public string B超其他
        {
            get { return _B超其他; }
            set { _B超其他 = value; }
        }

        private string _辅助检查其他;
        /// <summary>
        /// 获取或设置 辅助检查其他 的值
        /// </summary>
        public string 辅助检查其他
        {
            get { return _辅助检查其他; }
            set { _辅助检查其他 = value; }
        }

        private string _G_JLJJY;
        /// <summary>
        /// 获取或设置 G_JLJJY 的值
        /// </summary>
        public string G_JLJJY
        {
            get { return _G_JLJJY; }
            set { _G_JLJJY = value; }
        }

        private string _个人档案编号;
        /// <summary>
        /// 获取或设置 个人档案编号 的值
        /// </summary>
        public string 个人档案编号
        {
            get { return _个人档案编号; }
            set { _个人档案编号 = value; }
        }

        private string _创建机构;
        /// <summary>
        /// 获取或设置 创建机构 的值
        /// </summary>
        public string 创建机构
        {
            get { return _创建机构; }
            set { _创建机构 = value; }
        }

        private string _创建时间;
        /// <summary>
        /// 获取或设置 创建时间 的值
        /// </summary>
        public string 创建时间
        {
            get { return _创建时间; }
            set { _创建时间 = value; }
        }

        private string _创建人;
        /// <summary>
        /// 获取或设置 创建人 的值
        /// </summary>
        public string 创建人
        {
            get { return _创建人; }
            set { _创建人 = value; }
        }

        private string _修改时间;
        /// <summary>
        /// 获取或设置 修改时间 的值
        /// </summary>
        public string 修改时间
        {
            get { return _修改时间; }
            set { _修改时间 = value; }
        }

        private string _修改人;
        /// <summary>
        /// 获取或设置 修改人 的值
        /// </summary>
        public string 修改人
        {
            get { return _修改人; }
            set { _修改人 = value; }
        }

        private string _体检日期;
        /// <summary>
        /// 获取或设置 体检日期 的值
        /// </summary>
        public string 体检日期
        {
            get { return _体检日期; }
            set { _体检日期 = value; }
        }

        private string _所属机构;
        /// <summary>
        /// 获取或设置 所属机构 的值
        /// </summary>
        public string 所属机构
        {
            get { return _所属机构; }
            set { _所属机构 = value; }
        }

        private string _FIELD1;
        /// <summary>
        /// 获取或设置 FIELD1 的值
        /// </summary>
        public string FIELD1
        {
            get { return _FIELD1; }
            set { _FIELD1 = value; }
        }

        private string _FIELD2;
        /// <summary>
        /// 获取或设置 FIELD2 的值
        /// </summary>
        public string FIELD2
        {
            get { return _FIELD2; }
            set { _FIELD2 = value; }
        }

        private string _FIELD3;
        /// <summary>
        /// 获取或设置 FIELD3 的值
        /// </summary>
        public string FIELD3
        {
            get { return _FIELD3; }
            set { _FIELD3 = value; }
        }

        private string _FIELD4;
        /// <summary>
        /// 获取或设置 FIELD4 的值
        /// </summary>
        public string FIELD4
        {
            get { return _FIELD4; }
            set { _FIELD4 = value; }
        }

        private int _WBC_SUP;
        /// <summary>
        /// 获取或设置 WBC_SUP 的值
        /// </summary>
        public int WBC_SUP
        {
            get { return _WBC_SUP; }
            set { _WBC_SUP = value; }
        }

        private int _PLT_SUP;
        /// <summary>
        /// 获取或设置 PLT_SUP 的值
        /// </summary>
        public int PLT_SUP
        {
            get { return _PLT_SUP; }
            set { _PLT_SUP = value; }
        }

        private string _G_TUNWEI;
        /// <summary>
        /// 获取或设置 G_TUNWEI 的值
        /// </summary>
        public string G_TUNWEI
        {
            get { return _G_TUNWEI; }
            set { _G_TUNWEI = value; }
        }

        private string _G_YTWBZ;
        /// <summary>
        /// 获取或设置 G_YTWBZ 的值
        /// </summary>
        public string G_YTWBZ
        {
            get { return _G_YTWBZ; }
            set { _G_YTWBZ = value; }
        }

        private string _锻炼频率;
        /// <summary>
        /// 获取或设置 锻炼频率 的值
        /// </summary>
        public string 锻炼频率
        {
            get { return _锻炼频率; }
            set { _锻炼频率 = value; }
        }

        private string _每次锻炼时间;
        /// <summary>
        /// 获取或设置 每次锻炼时间 的值
        /// </summary>
        public string 每次锻炼时间
        {
            get { return _每次锻炼时间; }
            set { _每次锻炼时间 = value; }
        }

        private string _坚持锻炼时间;
        /// <summary>
        /// 获取或设置 坚持锻炼时间 的值
        /// </summary>
        public string 坚持锻炼时间
        {
            get { return _坚持锻炼时间; }
            set { _坚持锻炼时间 = value; }
        }

        private string _锻炼方式;
        /// <summary>
        /// 获取或设置 锻炼方式 的值
        /// </summary>
        public string 锻炼方式
        {
            get { return _锻炼方式; }
            set { _锻炼方式 = value; }
        }

        private string _饮食习惯;
        /// <summary>
        /// 获取或设置 饮食习惯 的值
        /// </summary>
        public string 饮食习惯
        {
            get { return _饮食习惯; }
            set { _饮食习惯 = value; }
        }

        private string _吸烟状况;
        /// <summary>
        /// 获取或设置 吸烟状况 的值
        /// </summary>
        public string 吸烟状况
        {
            get { return _吸烟状况; }
            set { _吸烟状况 = value; }
        }

        private string _日吸烟量;
        /// <summary>
        /// 获取或设置 日吸烟量 的值
        /// </summary>
        public string 日吸烟量
        {
            get { return _日吸烟量; }
            set { _日吸烟量 = value; }
        }

        private string _开始吸烟年龄;
        /// <summary>
        /// 获取或设置 开始吸烟年龄 的值
        /// </summary>
        public string 开始吸烟年龄
        {
            get { return _开始吸烟年龄; }
            set { _开始吸烟年龄 = value; }
        }

        private string _戒烟年龄;
        /// <summary>
        /// 获取或设置 戒烟年龄 的值
        /// </summary>
        public string 戒烟年龄
        {
            get { return _戒烟年龄; }
            set { _戒烟年龄 = value; }
        }

        private string _饮酒频率;
        /// <summary>
        /// 获取或设置 饮酒频率 的值
        /// </summary>
        public string 饮酒频率
        {
            get { return _饮酒频率; }
            set { _饮酒频率 = value; }
        }

        private string _日饮酒量;
        /// <summary>
        /// 获取或设置 日饮酒量 的值
        /// </summary>
        public string 日饮酒量
        {
            get { return _日饮酒量; }
            set { _日饮酒量 = value; }
        }

        private string _是否戒酒;
        /// <summary>
        /// 获取或设置 是否戒酒 的值
        /// </summary>
        public string 是否戒酒
        {
            get { return _是否戒酒; }
            set { _是否戒酒 = value; }
        }

        private string _戒酒年龄;
        /// <summary>
        /// 获取或设置 戒酒年龄 的值
        /// </summary>
        public string 戒酒年龄
        {
            get { return _戒酒年龄; }
            set { _戒酒年龄 = value; }
        }

        private string _开始饮酒年龄;
        /// <summary>
        /// 获取或设置 开始饮酒年龄 的值
        /// </summary>
        public string 开始饮酒年龄
        {
            get { return _开始饮酒年龄; }
            set { _开始饮酒年龄 = value; }
        }

        private string _近一年内是否曾醉酒;
        /// <summary>
        /// 获取或设置 近一年内是否曾醉酒 的值
        /// </summary>
        public string 近一年内是否曾醉酒
        {
            get { return _近一年内是否曾醉酒; }
            set { _近一年内是否曾醉酒 = value; }
        }

        private string _饮酒种类;
        /// <summary>
        /// 获取或设置 饮酒种类 的值
        /// </summary>
        public string 饮酒种类
        {
            get { return _饮酒种类; }
            set { _饮酒种类 = value; }
        }

        private string _饮酒种类其它;
        /// <summary>
        /// 获取或设置 饮酒种类其它 的值
        /// </summary>
        public string 饮酒种类其它
        {
            get { return _饮酒种类其它; }
            set { _饮酒种类其它 = value; }
        }

        private string _有无职业病;
        /// <summary>
        /// 获取或设置 有无职业病 的值
        /// </summary>
        public string 有无职业病
        {
            get { return _有无职业病; }
            set { _有无职业病 = value; }
        }

        private string _具体职业;
        /// <summary>
        /// 获取或设置 具体职业 的值
        /// </summary>
        public string 具体职业
        {
            get { return _具体职业; }
            set { _具体职业 = value; }
        }

        private string _从业时间;
        /// <summary>
        /// 获取或设置 从业时间 的值
        /// </summary>
        public string 从业时间
        {
            get { return _从业时间; }
            set { _从业时间 = value; }
        }

        private string _化学物质;
        /// <summary>
        /// 获取或设置 化学物质 的值
        /// </summary>
        public string 化学物质
        {
            get { return _化学物质; }
            set { _化学物质 = value; }
        }

        private string _化学物质防护;
        /// <summary>
        /// 获取或设置 化学物质防护 的值
        /// </summary>
        public string 化学物质防护
        {
            get { return _化学物质防护; }
            set { _化学物质防护 = value; }
        }

        private string _化学物质具体防护;
        /// <summary>
        /// 获取或设置 化学物质具体防护 的值
        /// </summary>
        public string 化学物质具体防护
        {
            get { return _化学物质具体防护; }
            set { _化学物质具体防护 = value; }
        }

        private string _毒物;
        /// <summary>
        /// 获取或设置 毒物 的值
        /// </summary>
        public string 毒物
        {
            get { return _毒物; }
            set { _毒物 = value; }
        }

        private string _G_DWFHCS;
        /// <summary>
        /// 获取或设置 G_DWFHCS 的值
        /// </summary>
        public string G_DWFHCS
        {
            get { return _G_DWFHCS; }
            set { _G_DWFHCS = value; }
        }

        private string _G_DWFHCSQT;
        /// <summary>
        /// 获取或设置 G_DWFHCSQT 的值
        /// </summary>
        public string G_DWFHCSQT
        {
            get { return _G_DWFHCSQT; }
            set { _G_DWFHCSQT = value; }
        }

        private string _放射物质;
        /// <summary>
        /// 获取或设置 放射物质 的值
        /// </summary>
        public string 放射物质
        {
            get { return _放射物质; }
            set { _放射物质 = value; }
        }

        private string _放射物质防护措施有无;
        /// <summary>
        /// 获取或设置 放射物质防护措施有无 的值
        /// </summary>
        public string 放射物质防护措施有无
        {
            get { return _放射物质防护措施有无; }
            set { _放射物质防护措施有无 = value; }
        }

        private string _放射物质防护措施其他;
        /// <summary>
        /// 获取或设置 放射物质防护措施其他 的值
        /// </summary>
        public string 放射物质防护措施其他
        {
            get { return _放射物质防护措施其他; }
            set { _放射物质防护措施其他 = value; }
        }

        private string _口唇;
        /// <summary>
        /// 获取或设置 口唇 的值
        /// </summary>
        public string 口唇
        {
            get { return _口唇; }
            set { _口唇 = value; }
        }

        private string _齿列;
        /// <summary>
        /// 获取或设置 齿列 的值
        /// </summary>
        public string 齿列
        {
            get { return _齿列; }
            set { _齿列 = value; }
        }

        private string _咽部;
        /// <summary>
        /// 获取或设置 咽部 的值
        /// </summary>
        public string 咽部
        {
            get { return _咽部; }
            set { _咽部 = value; }
        }

        private string _皮肤其他;
        /// <summary>
        /// 获取或设置 皮肤其他 的值
        /// </summary>
        public string 皮肤其他
        {
            get { return _皮肤其他; }
            set { _皮肤其他 = value; }
        }

        private string _巩膜;
        /// <summary>
        /// 获取或设置 巩膜 的值
        /// </summary>
        public string 巩膜
        {
            get { return _巩膜; }
            set { _巩膜 = value; }
        }

        private string _巩膜其他;
        /// <summary>
        /// 获取或设置 巩膜其他 的值
        /// </summary>
        public string 巩膜其他
        {
            get { return _巩膜其他; }
            set { _巩膜其他 = value; }
        }

        private string _足背动脉搏动;
        /// <summary>
        /// 获取或设置 足背动脉搏动 的值
        /// </summary>
        public string 足背动脉搏动
        {
            get { return _足背动脉搏动; }
            set { _足背动脉搏动 = value; }
        }

        private string _乳腺;
        /// <summary>
        /// 获取或设置 乳腺 的值
        /// </summary>
        public string 乳腺
        {
            get { return _乳腺; }
            set { _乳腺 = value; }
        }

        private string _乳腺其他;
        /// <summary>
        /// 获取或设置 乳腺其他 的值
        /// </summary>
        public string 乳腺其他
        {
            get { return _乳腺其他; }
            set { _乳腺其他 = value; }
        }

        private string _外阴;
        /// <summary>
        /// 获取或设置 外阴 的值
        /// </summary>
        public string 外阴
        {
            get { return _外阴; }
            set { _外阴 = value; }
        }

        private string _外阴异常;
        /// <summary>
        /// 获取或设置 外阴异常 的值
        /// </summary>
        public string 外阴异常
        {
            get { return _外阴异常; }
            set { _外阴异常 = value; }
        }

        private string _阴道;
        /// <summary>
        /// 获取或设置 阴道 的值
        /// </summary>
        public string 阴道
        {
            get { return _阴道; }
            set { _阴道 = value; }
        }

        private string _阴道异常;
        /// <summary>
        /// 获取或设置 阴道异常 的值
        /// </summary>
        public string 阴道异常
        {
            get { return _阴道异常; }
            set { _阴道异常 = value; }
        }

        private string _宫颈;
        /// <summary>
        /// 获取或设置 宫颈 的值
        /// </summary>
        public string 宫颈
        {
            get { return _宫颈; }
            set { _宫颈 = value; }
        }

        private string _宫颈异常;
        /// <summary>
        /// 获取或设置 宫颈异常 的值
        /// </summary>
        public string 宫颈异常
        {
            get { return _宫颈异常; }
            set { _宫颈异常 = value; }
        }

        private string _宫体;
        /// <summary>
        /// 获取或设置 宫体 的值
        /// </summary>
        public string 宫体
        {
            get { return _宫体; }
            set { _宫体 = value; }
        }

        private string _宫体异常;
        /// <summary>
        /// 获取或设置 宫体异常 的值
        /// </summary>
        public string 宫体异常
        {
            get { return _宫体异常; }
            set { _宫体异常 = value; }
        }

        private string _附件;
        /// <summary>
        /// 获取或设置 附件 的值
        /// </summary>
        public string 附件
        {
            get { return _附件; }
            set { _附件 = value; }
        }

        private string _附件异常;
        /// <summary>
        /// 获取或设置 附件异常 的值
        /// </summary>
        public string 附件异常
        {
            get { return _附件异常; }
            set { _附件异常 = value; }
        }

        private string _血钾浓度;
        /// <summary>
        /// 获取或设置 血钾浓度 的值
        /// </summary>
        public string 血钾浓度
        {
            get { return _血钾浓度; }
            set { _血钾浓度 = value; }
        }

        private string _血钠浓度;
        /// <summary>
        /// 获取或设置 血钠浓度 的值
        /// </summary>
        public string 血钠浓度
        {
            get { return _血钠浓度; }
            set { _血钠浓度 = value; }
        }

        private string _糖化血红蛋白;
        /// <summary>
        /// 获取或设置 糖化血红蛋白 的值
        /// </summary>
        public string 糖化血红蛋白
        {
            get { return _糖化血红蛋白; }
            set { _糖化血红蛋白 = value; }
        }

        private string _宫颈涂片;
        /// <summary>
        /// 获取或设置 宫颈涂片 的值
        /// </summary>
        public string 宫颈涂片
        {
            get { return _宫颈涂片; }
            set { _宫颈涂片 = value; }
        }

        private string _宫颈涂片异常;
        /// <summary>
        /// 获取或设置 宫颈涂片异常 的值
        /// </summary>
        public string 宫颈涂片异常
        {
            get { return _宫颈涂片异常; }
            set { _宫颈涂片异常 = value; }
        }

        private string _平和质;
        /// <summary>
        /// 获取或设置 平和质 的值
        /// </summary>
        public string 平和质
        {
            get { return _平和质; }
            set { _平和质 = value; }
        }

        private string _气虚质;
        /// <summary>
        /// 获取或设置 气虚质 的值
        /// </summary>
        public string 气虚质
        {
            get { return _气虚质; }
            set { _气虚质 = value; }
        }

        private string _阳虚质;
        /// <summary>
        /// 获取或设置 阳虚质 的值
        /// </summary>
        public string 阳虚质
        {
            get { return _阳虚质; }
            set { _阳虚质 = value; }
        }

        private string _阴虚质;
        /// <summary>
        /// 获取或设置 阴虚质 的值
        /// </summary>
        public string 阴虚质
        {
            get { return _阴虚质; }
            set { _阴虚质 = value; }
        }

        private string _痰湿质;
        /// <summary>
        /// 获取或设置 痰湿质 的值
        /// </summary>
        public string 痰湿质
        {
            get { return _痰湿质; }
            set { _痰湿质 = value; }
        }

        private string _湿热质;
        /// <summary>
        /// 获取或设置 湿热质 的值
        /// </summary>
        public string 湿热质
        {
            get { return _湿热质; }
            set { _湿热质 = value; }
        }

        private string _血瘀质;
        /// <summary>
        /// 获取或设置 血瘀质 的值
        /// </summary>
        public string 血瘀质
        {
            get { return _血瘀质; }
            set { _血瘀质 = value; }
        }

        private string _气郁质;
        /// <summary>
        /// 获取或设置 气郁质 的值
        /// </summary>
        public string 气郁质
        {
            get { return _气郁质; }
            set { _气郁质 = value; }
        }

        private string _特禀质;
        /// <summary>
        /// 获取或设置 特禀质 的值
        /// </summary>
        public string 特禀质
        {
            get { return _特禀质; }
            set { _特禀质 = value; }
        }

        private string _脑血管疾病;
        /// <summary>
        /// 获取或设置 脑血管疾病 的值
        /// </summary>
        public string 脑血管疾病
        {
            get { return _脑血管疾病; }
            set { _脑血管疾病 = value; }
        }

        private string _脑血管疾病其他;
        /// <summary>
        /// 获取或设置 脑血管疾病其他 的值
        /// </summary>
        public string 脑血管疾病其他
        {
            get { return _脑血管疾病其他; }
            set { _脑血管疾病其他 = value; }
        }

        private string _肾脏疾病;
        /// <summary>
        /// 获取或设置 肾脏疾病 的值
        /// </summary>
        public string 肾脏疾病
        {
            get { return _肾脏疾病; }
            set { _肾脏疾病 = value; }
        }

        private string _肾脏疾病其他;
        /// <summary>
        /// 获取或设置 肾脏疾病其他 的值
        /// </summary>
        public string 肾脏疾病其他
        {
            get { return _肾脏疾病其他; }
            set { _肾脏疾病其他 = value; }
        }

        private string _心脏疾病;
        /// <summary>
        /// 获取或设置 心脏疾病 的值
        /// </summary>
        public string 心脏疾病
        {
            get { return _心脏疾病; }
            set { _心脏疾病 = value; }
        }

        private string _心脏疾病其他;
        /// <summary>
        /// 获取或设置 心脏疾病其他 的值
        /// </summary>
        public string 心脏疾病其他
        {
            get { return _心脏疾病其他; }
            set { _心脏疾病其他 = value; }
        }

        private string _血管疾病;
        /// <summary>
        /// 获取或设置 血管疾病 的值
        /// </summary>
        public string 血管疾病
        {
            get { return _血管疾病; }
            set { _血管疾病 = value; }
        }

        private string _血管疾病其他;
        /// <summary>
        /// 获取或设置 血管疾病其他 的值
        /// </summary>
        public string 血管疾病其他
        {
            get { return _血管疾病其他; }
            set { _血管疾病其他 = value; }
        }

        private string _眼部疾病;
        /// <summary>
        /// 获取或设置 眼部疾病 的值
        /// </summary>
        public string 眼部疾病
        {
            get { return _眼部疾病; }
            set { _眼部疾病 = value; }
        }

        private string _眼部疾病其他;
        /// <summary>
        /// 获取或设置 眼部疾病其他 的值
        /// </summary>
        public string 眼部疾病其他
        {
            get { return _眼部疾病其他; }
            set { _眼部疾病其他 = value; }
        }

        private string _神经系统疾病;
        /// <summary>
        /// 获取或设置 神经系统疾病 的值
        /// </summary>
        public string 神经系统疾病
        {
            get { return _神经系统疾病; }
            set { _神经系统疾病 = value; }
        }

        private string _神经系统疾病其他;
        /// <summary>
        /// 获取或设置 神经系统疾病其他 的值
        /// </summary>
        public string 神经系统疾病其他
        {
            get { return _神经系统疾病其他; }
            set { _神经系统疾病其他 = value; }
        }

        private string _其他系统疾病;
        /// <summary>
        /// 获取或设置 其他系统疾病 的值
        /// </summary>
        public string 其他系统疾病
        {
            get { return _其他系统疾病; }
            set { _其他系统疾病 = value; }
        }

        private string _其他系统疾病其他;
        /// <summary>
        /// 获取或设置 其他系统疾病其他 的值
        /// </summary>
        public string 其他系统疾病其他
        {
            get { return _其他系统疾病其他; }
            set { _其他系统疾病其他 = value; }
        }

        private string _健康评价;
        /// <summary>
        /// 获取或设置 健康评价 的值
        /// </summary>
        public string 健康评价
        {
            get { return _健康评价; }
            set { _健康评价 = value; }
        }

        private string _健康评价异常1;
        /// <summary>
        /// 获取或设置 健康评价异常1 的值
        /// </summary>
        public string 健康评价异常1
        {
            get { return _健康评价异常1; }
            set { _健康评价异常1 = value; }
        }

        private string _健康评价异常2;
        /// <summary>
        /// 获取或设置 健康评价异常2 的值
        /// </summary>
        public string 健康评价异常2
        {
            get { return _健康评价异常2; }
            set { _健康评价异常2 = value; }
        }

        private string _健康评价异常3;
        /// <summary>
        /// 获取或设置 健康评价异常3 的值
        /// </summary>
        public string 健康评价异常3
        {
            get { return _健康评价异常3; }
            set { _健康评价异常3 = value; }
        }

        private string _健康评价异常4;
        /// <summary>
        /// 获取或设置 健康评价异常4 的值
        /// </summary>
        public string 健康评价异常4
        {
            get { return _健康评价异常4; }
            set { _健康评价异常4 = value; }
        }

        private string _健康指导;
        /// <summary>
        /// 获取或设置 健康指导 的值
        /// </summary>
        public string 健康指导
        {
            get { return _健康指导; }
            set { _健康指导 = value; }
        }

        private string _危险因素控制;
        /// <summary>
        /// 获取或设置 危险因素控制 的值
        /// </summary>
        public string 危险因素控制
        {
            get { return _危险因素控制; }
            set { _危险因素控制 = value; }
        }

        private string _危险因素控制体重;
        /// <summary>
        /// 获取或设置 危险因素控制体重 的值
        /// </summary>
        public string 危险因素控制体重
        {
            get { return _危险因素控制体重; }
            set { _危险因素控制体重 = value; }
        }

        private string _危险因素控制疫苗;
        /// <summary>
        /// 获取或设置 危险因素控制疫苗 的值
        /// </summary>
        public string 危险因素控制疫苗
        {
            get { return _危险因素控制疫苗; }
            set { _危险因素控制疫苗 = value; }
        }

        private string _危险因素控制其他;
        /// <summary>
        /// 获取或设置 危险因素控制其他 的值
        /// </summary>
        public string 危险因素控制其他
        {
            get { return _危险因素控制其他; }
            set { _危险因素控制其他 = value; }
        }

        private string _FIELD5;
        /// <summary>
        /// 获取或设置 FIELD5 的值
        /// </summary>
        public string FIELD5
        {
            get { return _FIELD5; }
            set { _FIELD5 = value; }
        }

        private string _症状其他;
        /// <summary>
        /// 获取或设置 症状其他 的值
        /// </summary>
        public string 症状其他
        {
            get { return _症状其他; }
            set { _症状其他 = value; }
        }

        private string _G_XYYC;
        /// <summary>
        /// 获取或设置 G_XYYC 的值
        /// </summary>
        public string G_XYYC
        {
            get { return _G_XYYC; }
            set { _G_XYYC = value; }
        }

        private string _G_XYZC;
        /// <summary>
        /// 获取或设置 G_XYZC 的值
        /// </summary>
        public string G_XYZC
        {
            get { return _G_XYZC; }
            set { _G_XYZC = value; }
        }

        private string _G_QTZHZH;
        /// <summary>
        /// 获取或设置 G_QTZHZH 的值
        /// </summary>
        public string G_QTZHZH
        {
            get { return _G_QTZHZH; }
            set { _G_QTZHZH = value; }
        }

        private string _缺项;
        /// <summary>
        /// 获取或设置 缺项 的值
        /// </summary>
        public string 缺项
        {
            get { return _缺项; }
            set { _缺项 = value; }
        }

        private string _口唇其他;
        /// <summary>
        /// 获取或设置 口唇其他 的值
        /// </summary>
        public string 口唇其他
        {
            get { return _口唇其他; }
            set { _口唇其他 = value; }
        }

        private string _齿列其他;
        /// <summary>
        /// 获取或设置 齿列其他 的值
        /// </summary>
        public string 齿列其他
        {
            get { return _齿列其他; }
            set { _齿列其他 = value; }
        }

        private string _咽部其他;
        /// <summary>
        /// 获取或设置 咽部其他 的值
        /// </summary>
        public string 咽部其他
        {
            get { return _咽部其他; }
            set { _咽部其他 = value; }
        }

        private string _YDGNQT;
        /// <summary>
        /// 获取或设置 YDGNQT 的值
        /// </summary>
        public string YDGNQT
        {
            get { return _YDGNQT; }
            set { _YDGNQT = value; }
        }

        private string _餐后2H血糖;
        /// <summary>
        /// 获取或设置 餐后2H血糖 的值
        /// </summary>
        public string 餐后2H血糖
        {
            get { return _餐后2H血糖; }
            set { _餐后2H血糖 = value; }
        }

        private string _老年人状况评估;
        /// <summary>
        /// 获取或设置 老年人状况评估 的值
        /// </summary>
        public string 老年人状况评估
        {
            get { return _老年人状况评估; }
            set { _老年人状况评估 = value; }
        }

        private string _老年人自理评估;
        /// <summary>
        /// 获取或设置 老年人自理评估 的值
        /// </summary>
        public string 老年人自理评估
        {
            get { return _老年人自理评估; }
            set { _老年人自理评估 = value; }
        }

        private string _粉尘;
        /// <summary>
        /// 获取或设置 粉尘 的值
        /// </summary>
        public string 粉尘
        {
            get { return _粉尘; }
            set { _粉尘 = value; }
        }

        private string _物理因素;
        /// <summary>
        /// 获取或设置 物理因素 的值
        /// </summary>
        public string 物理因素
        {
            get { return _物理因素; }
            set { _物理因素 = value; }
        }

        private string _职业病其他;
        /// <summary>
        /// 获取或设置 职业病其他 的值
        /// </summary>
        public string 职业病其他
        {
            get { return _职业病其他; }
            set { _职业病其他 = value; }
        }

        private string _粉尘防护有无;
        /// <summary>
        /// 获取或设置 粉尘防护有无 的值
        /// </summary>
        public string 粉尘防护有无
        {
            get { return _粉尘防护有无; }
            set { _粉尘防护有无 = value; }
        }

        private string _物理防护有无;
        /// <summary>
        /// 获取或设置 物理防护有无 的值
        /// </summary>
        public string 物理防护有无
        {
            get { return _物理防护有无; }
            set { _物理防护有无 = value; }
        }

        private string _其他防护有无;
        /// <summary>
        /// 获取或设置 其他防护有无 的值
        /// </summary>
        public string 其他防护有无
        {
            get { return _其他防护有无; }
            set { _其他防护有无 = value; }
        }

        private string _粉尘防护措施;
        /// <summary>
        /// 获取或设置 粉尘防护措施 的值
        /// </summary>
        public string 粉尘防护措施
        {
            get { return _粉尘防护措施; }
            set { _粉尘防护措施 = value; }
        }

        private string _物理防护措施;
        /// <summary>
        /// 获取或设置 物理防护措施 的值
        /// </summary>
        public string 物理防护措施
        {
            get { return _物理防护措施; }
            set { _物理防护措施 = value; }
        }

        private string _其他防护措施;
        /// <summary>
        /// 获取或设置 其他防护措施 的值
        /// </summary>
        public string 其他防护措施
        {
            get { return _其他防护措施; }
            set { _其他防护措施 = value; }
        }

        private string _TNBFXJF;
        /// <summary>
        /// 获取或设置 TNBFXJF 的值
        /// </summary>
        public string TNBFXJF
        {
            get { return _TNBFXJF; }
            set { _TNBFXJF = value; }
        }

        private string _左侧原因;
        /// <summary>
        /// 获取或设置 左侧原因 的值
        /// </summary>
        public string 左侧原因
        {
            get { return _左侧原因; }
            set { _左侧原因 = value; }
        }

        private string _右侧原因;
        /// <summary>
        /// 获取或设置 右侧原因 的值
        /// </summary>
        public string 右侧原因
        {
            get { return _右侧原因; }
            set { _右侧原因 = value; }
        }

        private string _尿微量白蛋白;
        /// <summary>
        /// 获取或设置 尿微量白蛋白 的值
        /// </summary>
        public string 尿微量白蛋白
        {
            get { return _尿微量白蛋白; }
            set { _尿微量白蛋白 = value; }
        }

        private string _完整度;
        /// <summary>
        /// 获取或设置 完整度 的值
        /// </summary>
        public string 完整度
        {
            get { return _完整度; }
            set { _完整度 = value; }
        }

        private string _齿列缺齿;
        /// <summary>
        /// 获取或设置 齿列缺齿 的值
        /// </summary>
        public string 齿列缺齿
        {
            get { return _齿列缺齿; }
            set { _齿列缺齿 = value; }
        }

        private string _齿列龋齿;
        /// <summary>
        /// 获取或设置 齿列龋齿 的值
        /// </summary>
        public string 齿列龋齿
        {
            get { return _齿列龋齿; }
            set { _齿列龋齿 = value; }
        }

        private string _齿列义齿;
        /// <summary>
        /// 获取或设置 齿列义齿 的值
        /// </summary>
        public string 齿列义齿
        {
            get { return _齿列义齿; }
            set { _齿列义齿 = value; }
        }

        public byte b心电结果 { get; set; }

        public string RowState { get; set; }
    }
}