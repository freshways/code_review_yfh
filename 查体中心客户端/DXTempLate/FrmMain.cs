﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraSplashScreen;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars;

using DXTempLate.Controls;
using DXTempLate.Properties;
using DXTempLate.Class;

namespace DXTempLate
{
    public partial class FrmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FrmMain()
        {
            InitializeComponent();

            //string strSkinName = Properties.Settings.Default.SkinName;//获得皮肤的值
            //UserLookAndFeel.Default.SetSkinStyle(strSkinName);//默认系统界面皮肤

            SkinHelper.InitSkinGallery(ribbonGalleryBarItem1, true); //添加界面皮肤
        }

        BackgroundWorker BgWork = new BackgroundWorker();

        public static CL身高 cl身高 = new CL身高();

        public static CL血压 cl血压 = new CL血压();

        public static Cl体温 cl体温 = new Cl体温();

        private void FrmMain_Load(object sender, EventArgs e)
        {
            if (StaticClass.g_CurUserName == "admin")
            {
                ribbonPage1.Visible = true;
            }
            this.Text += "-"+StaticClass.g_TitleName;

            this.panelControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            //清空容器，加载主页
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XtraUserControlIndex.ItSelf);
            XtraUserControlIndex.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
            XtraUserControlIndex.ItSelf.OpenOptions += ItSelf_OpenOptions;
            //另起后台初始化蓝牙及身份证等连接
            IDReadHelper.SendToSFZMsg += IDReadHelper_SendToSFZMsg;
            if (Init蓝牙COM())
            {
                cl身高.SendToSGMsg += cl身高_SendToSGMsg;
                cl血压.SendToXYMsg += cl血压_SendToXYMsg;
                cl体温.SendToTWMsg += cl体温_SendToXYMsg;

                BgWork.DoWork += BgWork_DoWork;
                BgWork.RunWorkerAsync();
            }
        }

        void IDReadHelper_SendToSFZMsg(string Info)
        {
            this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = Info; }));
        }

        void cl体温_SendToXYMsg(string Info)
        {
            this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = Info; }));
        }

        void cl身高_SendToSGMsg(CL身高Info Info)
        {
            this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = Info._消息; }));
        }

        void cl血压_SendToXYMsg(CL血压Info Info)
        {
            this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = Info._消息; }));
        }

        //用于记录身份证读卡器是否初始化成功，初始化成功的情况下，才进行身份证号的读取
        public static bool bIDReaderInitResult = false;
        public static bool bl身高InitResult = false;
        public static bool bl血压InitResult = false;
        public static bool bl体温InitResult = false;
        void BgWork_DoWork(object sender, DoWorkEventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                bIDReaderInitResult = IDReadHelper.InitReader();
                if (bIDReaderInitResult)
                {
                    this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = "身份证读卡连接成功!"; }));
                }
                else
                    this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = "身份证读卡连接失败!"; }));

                bl身高InitResult = cl身高.init身高(_身高COM);
                //if (bl身高InitResult)
                //{
                //    this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = "身高体重连接成功!"; }));
                //}
                //else
                //    this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = "身高体重连接失败!"; }));
                //初始化血压
                bl血压InitResult = cl血压.init血压(_血压COM);

                bl体温InitResult = cl体温.Init体温(_体温COM);
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show("初始化失败！\n" + ex.Message, "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            }
        }

        #region 工作管理

        //首页
        private void btn首页_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XtraUserControlIndex.ItSelf);
            XtraUserControlIndex.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
            XtraUserControlIndex.ItSelf.OpenOptions += ItSelf_OpenOptions;
        }

        void ItSelf_OpenOptions(string Types)
        {
            switch (Types)
            {
                case "成人体检":
                    btn体检登记_ItemClick(null, null);
                    break;
                case "儿童体检":                   
                    break;
                case "中医体质辨识":
                    btn中医体质辨识_ItemClick(null, null);
                    break;
                case "报告单":
                    btn报告打印_ItemClick(null, null);
                    break;
                default:
                    break;
            }
        }

        private void btn体检登记_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControl成人体检登记.ItSelf);
            XUControl成人体检登记.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
            XUControl成人体检登记.ItSelf.CloseThis += ItSelf_CloseThis;
        }
        //关闭体检页面
        void ItSelf_CloseThis()
        {
            btn首页_ItemClick(null, null);
        }

        private void btn报告打印_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControl成人体检登记统计.ItSelf);
            XUControl成人体检登记统计.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        private void btn中医体质辨识_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControl中医体质辨识.ItSelf);
            XUControl中医体质辨识.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
            XUControl中医体质辨识.ItSelf.CloseThis += ItSelf_CloseThis;
        }

        //工作量统计
        private void btn工作量统计_ItemClick(object sender, ItemClickEventArgs e)
        {
            //this.panelControlMain.Controls.Clear();
            //this.panelControlMain.Controls.Add(XUC_GZDTJMap.ItSelf);
            //XUC_GZDTJMap.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        } 
        #endregion

        #region 系统管理
        //用户信息
        private void btn用户信息_ItemClick(object sender, ItemClickEventArgs e)
        {
            XtraFormUserInfo user = new XtraFormUserInfo(1, true);
            user.ShowDialog();
        }

        //人员维护
        private void btn人员维护_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControlUserList.ItSelf);
            XUControlUserList.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        //数据字典
        private void btn数据字典_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControlSysDic.ItSelf);
            XUControlSysDic.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        //密码修改
        private void btn密码修改_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.panelControlMain.Controls.Clear();
            this.panelControlMain.Controls.Add(XUControlPassword.ItSelf);
            XUControlPassword.ItSelf.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        //退出
        private void barBtomExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            //FrmExit exit = new FrmExit();
            //if (exit.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            if (XtraMessageBox.Show("你确认需要退出吗？", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Application.Exit();
            }
        } 
        #endregion

        #region 主题
        //主题
        private void ribbonGalleryBarItem1_Gallery_ItemClick(object sender, DevExpress.XtraBars.Ribbon.GalleryItemClickEventArgs e)
        {
            string caption = string.Empty;
            if (ribbonGalleryBarItem1.Gallery == null) return;
            caption = ribbonGalleryBarItem1.Gallery.GetCheckedItems()[0].Caption;//主题的描述
            caption = caption.Replace("主题：", "");

            if (XtraMessageBox.Show("你确定要保存当前皮肤吗?", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                Properties.Settings.Default.SkinName = caption;
                Properties.Settings.Default.Save();
            }
        } 
        #endregion       

        

        /// <summary>
        /// 重写系统事件，捕获关闭窗口消息，阻止关闭
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            //新增：重写捕获窗口关闭消息，下有原作者连接
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_CLOSE = 0xF060;
            if (m.Msg == WM_SYSCOMMAND && (int)m.WParam == SC_CLOSE)
            {
                //捕捉关闭窗体消息 http://www.cnblogs.com/sosoft/
                barBtomExit_ItemClick(null, null);
                return;
            }
            base.WndProc(ref m);
        }

        private void btn初始化_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!BgWork.IsBusy)
            {
                BgWork.RunWorkerAsync();
            }
        }

        private void btn读取配置_ItemClick(object sender, ItemClickEventArgs e)
        {
            Init蓝牙COM();
        }


        private int _身高COM = 0;
        private int _血压COM = 0;
        private int _体温COM = 0;
        private int _血糖COM = 0;
        bool Init蓝牙COM()
        {
            try
            {
                DBClass dbConstring = new DBClass(DbFilePath.FilePathName);
                string SQL = "select Name,Code from Sys_Dic where TypeName='蓝牙设备' ";
                DataTable dt = DBManager.ExecuteDataSet(dbConstring.m_ConnString, SQL, CommandType.Text).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    _身高COM = Convert.ToInt32(dt.Select("Name='身高体重'")[0]["Code"].ToString());
                    _血压COM = Convert.ToInt32(dt.Select("Name='血压'")[0]["Code"].ToString());
                    _体温COM = Convert.ToInt32(dt.Select("Name='体温'")[0]["Code"].ToString());
                    _血糖COM = Convert.ToInt32(dt.Select("Name='血糖'")[0]["Code"].ToString());
                }
                return true;
            }
            catch(Exception ex)
            {
                this.Invoke((EventHandler)(delegate { this.barStaticItem连接状态.Caption = "初始化COM口失败!" + ex.Message; }));
            }
            return false;
        }

        private void btn数据同步_ItemClick(object sender, ItemClickEventArgs e)
        {
            frm后台操作 frm = new frm后台操作();
            frm.ShowDialog();
        }

        private void btn重新连接_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!BgWork.IsBusy)
            {
                BgWork.RunWorkerAsync();
            }
        }

    }
}
