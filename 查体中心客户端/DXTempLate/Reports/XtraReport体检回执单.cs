﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DXTempLate
{
    public partial class XtraReport体检回执单 : DevExpress.XtraReports.UI.XtraReport
    {
        private List<string> result;

        public XtraReport体检回执单()
        {
            InitializeComponent();
        }

        public XtraReport体检回执单(System.Collections.Generic.List<string> result)
        {
            InitializeComponent();
            //this.xrPictureBox1.Image = Image.FromFile(Application.StartupPath + "\\Resources\\1111.jpg");
            //this.xrPictureBox2.Image = Image.FromFile(Application.StartupPath + "\\Resources\\2222.jpg");
            this.result = result;
            this.cell姓名.Text = result[0];
            this.cell性别.Text = result[1];
            this.cell年龄.Text = result[2];
            this.cell身份证号.Text = result[3];
            this.cell体检项目.Text = "体检项目：" + result[6];
            this.cell结论.Text = result[4];
            label检查时间.Text = result[5].ToString();
        }
    }
}
