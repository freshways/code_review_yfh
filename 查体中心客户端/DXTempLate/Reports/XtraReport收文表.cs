﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;
using DevExpress.XtraReports.UI;

namespace DXTempLate
{
    public partial class XtraReport收文表 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport收文表()
        {
            InitializeComponent();
        }

        public XtraReport收文表(DataTable dt)
        {
            InitializeComponent();

            //this.PrintingSystem.ShowMarginsWarning = false; 

            this.DataSource = dt;
            this.xrTableCell时间.DataBindings.Add("Text", dt, "收到时间");
            this.xrTableCell机关.DataBindings.Add("Text", dt, "来文机关");
            this.xrTableCell文号.DataBindings.Add("Text", dt, "文号");
            this.xrTableCell名称.DataBindings.Add("Text", dt, "文件名称");
            this.xrTableCell领导.DataBindings.Add("Text", dt, "批办领导、科室");
        }

    }
}
