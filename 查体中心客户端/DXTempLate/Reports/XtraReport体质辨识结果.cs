﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;

namespace DXTempLate
{
    public partial class report体质辨识结果 : DevExpress.XtraReports.UI.XtraReport
    {
        private tb_老年人中医药特征管理Info tab_体质辨识;
        private System.Collections.Generic.Dictionary<string, string> dict;

        public report体质辨识结果()
        {
            InitializeComponent();
        }

        public report体质辨识结果(tb_老年人中医药特征管理Info tab_体质辨识, System.Collections.Generic.Dictionary<string, string> dict)
        {
            InitializeComponent();

            //this.xrLabel医院名称.Text = Program._bRGName;
            this.tab_体质辨识 = tab_体质辨识;
            this.dict = dict;

            List<tb_中医体质指导> list = new List<tb_中医体质指导>();
            list = tb_中医辨识指导DAL.Gettb_老年人中医药特征管理InfoList("");

            xrLabel平和质.Text = string.IsNullOrEmpty(dict["平和质"]) ? "否" : dict["平和质"];
            xrLabel特禀质.Text = string.IsNullOrEmpty(dict["特禀质"]) ? "否" : dict["特禀质"];
            xrLabel气郁质.Text = string.IsNullOrEmpty(dict["气郁质"]) ? "否" : dict["气郁质"];
            xrLabel血瘀质.Text = string.IsNullOrEmpty(dict["血瘀质"]) ? "否" : dict["血瘀质"];
            xrLabel湿热质.Text = string.IsNullOrEmpty(dict["湿热质"]) ? "否" : dict["湿热质"];
            xrLabel痰湿质.Text = string.IsNullOrEmpty(dict["痰湿质"]) ? "否" : dict["痰湿质"];
            xrLabel阴虚质.Text = string.IsNullOrEmpty(dict["阴虚质"]) ? "否" : dict["阴虚质"];
            xrLabel阳虚质.Text = string.IsNullOrEmpty(dict["阳虚质"]) ? "否" : dict["阳虚质"];
            xrLabel气虚质.Text = string.IsNullOrEmpty(dict["气虚质"]) ? "否" : dict["气虚质"];
            xrLabel姓名.Text = tab_体质辨识.姓名;
            xrLabel性别.Text = tab_体质辨识.性别;
            xrLabel平和质分数.Text = tab_体质辨识.平和质得分.ToString();
            xrLabel气虚质得分.Text = tab_体质辨识.气虚质得分.ToString();
            xrLabel气郁质得分.Text = tab_体质辨识.气郁质得分.ToString();
            xrLabel湿热质得分.Text = tab_体质辨识.湿热质得分.ToString();
            xrLabel痰湿质得分.Text = tab_体质辨识.痰湿质得分.ToString();
            xrLabel特禀质得分.Text = tab_体质辨识.特禀质得分.ToString();
            xrLabel血瘀质得分.Text = tab_体质辨识.血瘀质得分.ToString();
            xrLabel阴虚质得分.Text = tab_体质辨识.阴虚质得分.ToString();
            xrLabel阳虚质得分.Text = tab_体质辨识.阳虚质得分.ToString();

            this.报告日期.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.xrLabel3.Text = tab_体质辨识.身份证号;
            DateTime now = DateTime.Today;
            DateTime bday = DateTime.Parse(tab_体质辨识.出生日期);
            int age = now.Year - bday.Year;

            if (bday > now.AddYears(-age)) age--;

            this.xrLabel年龄.Text = age.ToString();

            string result = string.Empty;

            foreach (KeyValuePair<string, string> kvp in dict)
            {
                string key = kvp.Key;
                string value = kvp.Value;

                if (!string.IsNullOrEmpty(value) && (value == "是" || value == "倾向是" || value == "基本是"))
                {

                    string zhidao = list.Find(x => x.类型 == key).内容;
                    result += zhidao;
                    result += "\r\t";
                }
            }
            //result += "该报告仅作为参考资料，不作为确诊使用。如有疑问请和您的责任医生联系。";
            //result += "\r\t";
            //result += "\r\t";
            //result += "根据《老年人健康管理服务规范》和《中医药健康福安里服务规范》要求，辖区内65岁及以上老年人每年应到卫生院接受一次免费健康体检和中医药健康管理服务，在中医体质辨识的基础上对不同体质老年人从情志调节、饮食调养、居民调摄、运动保健、穴位保健等方面进行相应的中医药保健指导，通过指导可以有效改善其健康状况。";
            //result += "\r\t";
            //result += "                              如果您接收本次服务，请签字：";
            //result = "1211111111111111111111111111111111111111111111111111111111111111";
            this.xrRichText1.Text = result;

        }
    }
}
