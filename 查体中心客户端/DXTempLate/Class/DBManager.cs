using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.SQLite;
using System.Runtime.InteropServices;
using DXTempLate.Class;

namespace DXTempLate
{
    /// <summary>
    /// DBManager类
    /// BY：狂人代码生成器 V1.0
    /// 作者：金属狂人
    /// 日期：2016年04月12日 03:22:55
    /// </summary>
    public static class DBManager
    {

        /// <summary>
        /// 连接字符串
        /// </summary>
        private static string conn121Str = @"Server=192.168.10.121;uid=yggsuser;pwd=yggsuser;database=AtomEHR.YSDB";

        /// <summary>
        /// 获取一个新的连接
        /// </summary>
        /// <param name="connStr">连接字符串</param>
        /// <returns></returns>
        public static SqlConnection Conn121
        {
            get
            {
                return new SqlConnection(conn121Str);
            }
        }
        public static SQLiteConnection SQLiteConn
        {
            get
            {
                DBClass dbClass;
                dbClass = new DBClass(DbFilePath.FilePathName);
                SQLiteConnection oraConn = new SQLiteConnection(dbClass.m_ConnString);
                return oraConn;
            }
        }

        #region ExecuteNonQuery
        /// <summary>
        /// 执行增、删、改 SQL语句
        /// </summary>
        /// <param name="sqlStr">SQL语句</param>
        /// <param name="param">参数列表</param>
        /// <returns>影响的行数</returns>
        public static int ExecuteUpdate(SqlConnection connType, string sqlStr, params object[] param)
        {
            SqlCommand cmd = new SqlCommand(sqlStr, connType);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            int rows = 0;
            try
            {
                rows = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return rows;
        }

        public static int ExecuteUpdate(SQLiteConnection connType, string sqlStr, params object[] param)
        {
            SQLiteCommand cmd = new SQLiteCommand(sqlStr, connType);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            int rows = 0;
            try
            {
                rows = cmd.ExecuteNonQuery();
                //Beep();//保存成功后提示音
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            return rows;
        } 
        #endregion

        #region ExecuteReader
        /// <summary>
        /// 执行查询SQL语句
        /// </summary>
        /// <param name="sqlStr">SQL语句</param>
        /// <param name="param">参数列表</param>
        /// <returns></returns>
        public static SqlDataReader ExecuteQuery(string sqlStr, params object[] param)
        {
            SqlCommand cmd = new SqlCommand(sqlStr, Conn121);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {
                cmd.Connection.Close();
                throw;
            }
        }
        public static SQLiteDataReader ExecuteQueryForSQLite(string sqlStr, params object[] param)
        {
            SQLiteCommand cmd = new SQLiteCommand(sqlStr, SQLiteConn);
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {
                cmd.Connection.Close();
                throw;
            }
        } 
        #endregion

        #region ExecuteDataSet
        /// <summary>
        /// 执行数据库查询，返回DataSet对象
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="cmd">SqlCommand对象</param>
        /// <returns>DataSet对象</returns>
        public static DataSet ExecuteDataSet(string connectionString, SQLiteCommand cmd)
        {
            DataSet ds = new DataSet();
            SQLiteConnection con = new SQLiteConnection(connectionString);
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, cmd.CommandType, cmd.CommandText);
            try
            {
                SQLiteDataAdapter sda = new SQLiteDataAdapter(cmd);
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmd.Connection != null)
                {
                    if (cmd.Connection.State == ConnectionState.Open)
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return ds;
        }

        /// <summary>
        /// 执行数据库查询，返回DataSet对象
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型</param>
        /// <returns>DataSet对象</returns>
        public static DataSet ExecuteDataSet(string connectionString, string commandText, CommandType commandType)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException("connectionString");
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");
            DataSet ds = new DataSet();
            SQLiteConnection con = new SQLiteConnection(connectionString);
            SQLiteCommand cmd = new SQLiteCommand();
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, commandType, commandText);
            try
            {
                SQLiteDataAdapter sda = new SQLiteDataAdapter(cmd);
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null)
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            return ds;
        }

        /// <summary>
        /// 执行数据库查询，返回DataSet对象
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型</param>
        /// <param name="cmdParms">SQL参数对象</param>
        /// <returns>DataSet对象</returns>
        public static DataSet ExecuteDataSet(string connectionString, string commandText, CommandType commandType, params SQLiteParameter[] cmdParms)
        {
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException("connectionString");
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");
            DataSet ds = new DataSet();
            SQLiteConnection con = new SQLiteConnection(connectionString);
            SQLiteCommand cmd = new SQLiteCommand();
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, commandType, commandText, cmdParms);
            try
            {
                SQLiteDataAdapter sda = new SQLiteDataAdapter(cmd);
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null)
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            return ds;
        }
        /// <summary>
        /// 预处理Command对象,数据库链接,事务,需要执行的对象,参数等的初始化
        /// </summary>
        /// <param name="cmd">Command对象</param>
        /// <param name="conn">Connection对象</param>
        /// <param name="trans">Transcation对象</param>
        /// <param name="useTrans">是否使用事务</param>
        /// <param name="cmdType">SQL字符串执行类型</param>
        /// <param name="cmdText">SQL Text</param>
        /// <param name="cmdParms">SQLiteParameters to use in the command</param>
        private static void PrepareCommand(SQLiteCommand cmd, SQLiteConnection conn, ref SQLiteTransaction trans, bool useTrans, CommandType cmdType, string cmdText, params SQLiteParameter[] cmdParms)
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();

            cmd.Connection = conn;
            cmd.CommandText = cmdText;

            if (useTrans)
            {
                trans = conn.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
            }


            cmd.CommandType = cmdType;

            if (cmdParms != null)
            {
                foreach (SQLiteParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }
        #endregion

        #region 执行存储过程
        /// <summary>
        /// 执行存储过程 - 无结果集
        /// </summary>
        /// <param name="paroName">存储过程名称</param>
        /// <param name="param">参数列表</param>
        /// <returns></returns>
        public static int ExecuteProc(SqlConnection connType, string procName, params object[] param)
        {
            SqlCommand cmd = new SqlCommand(procName, connType);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        /// <summary>
        /// 执行存储过程 - 有结果集
        /// </summary>
        /// <param name="paroName">存储过程名称</param>
        /// <param name="param">参数列表</param>
        /// <returns></returns>
        public static SqlDataReader ExecuteProcByReader(SqlConnection conn, string procName, params object[] param)
        {
            SqlCommand cmd = new SqlCommand(procName, conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddRange(param);
            cmd.Connection.Open();
            try
            {
                return cmd.ExecuteReader();
            }
            catch
            {
                cmd.Connection.Close();
                throw;
            }
        } 
        #endregion
        


        /// <summary>
        /// 更新MSSQL数据源
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="strTblName"></param>
        /// <returns></returns>
        public static int UpdateByDataSet(DataSet ds, string strTblName)
        {
            try
            {
                //SqlConnection  conn = new SqlConnection(Conn));
                SqlDataAdapter myAdapter = new SqlDataAdapter();
                SqlCommand myCommand = new SqlCommand(("select * from " + strTblName), Conn121);
                myAdapter.SelectCommand = myCommand;
                SqlCommandBuilder myCommandBuilder = new SqlCommandBuilder(myAdapter);
                myAdapter.Update(ds, strTblName);
                return 0;
            }
            catch (Exception errBU)
            {
                throw errBU;
            }

        }


        private static DBClass dbConstring;
        /// <summary>
        /// 添加、修改
        /// </summary>
        /// <param name="Info"></param>
        /// <returns></returns>
        public static bool UpdateDataSetForSqlite(DataSet ds, string strTblName)
        {
            bool bBool = false;
            dbConstring = new DBClass(DbFilePath.FilePathName);
            try
            {
                SQLiteConnection oraConn = new SQLiteConnection(dbConstring.m_ConnString);
                SQLiteDataAdapter myAdapter = new SQLiteDataAdapter();
                SQLiteCommand myCommand = new SQLiteCommand(("select * from " + strTblName), oraConn);
                myAdapter.SelectCommand = myCommand;
                SQLiteCommandBuilder myCommandBuilder = new SQLiteCommandBuilder(myAdapter);
                myAdapter.Update(ds, strTblName);
                bBool = true;
                Beep();//保存成功后提示音
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return bBool;
        }

        /// <param name="iFrequency">声音频率（从37Hz到32767Hz）。在windows95中忽略</param>  
        /// <param name="iDuration">声音的持续时间，以毫秒为单位。</param>  
        [DllImport("Kernel32.dll")] //引入命名空间 using System.Runtime.InteropServices;  
        public static extern bool Beep(int frequency, int duration);

        /// <summary>
        /// 调用系统发音
        /// </summary>
        public static void Beep()
        {
            Beep(1500, 500);
        }

    }
}
