﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft;

namespace DXTempLate.Class
{
    /// <summary>
    /// 通用类函数
    /// </summary>
    public class PublicClass
    {

        private DBClass dbConn = null;

        /// <summary>
        /// 日期的星期进行中文和英文互换
        /// </summary>
        /// <param name="DateTostring">日期字符串</param>
        /// <param name="nType">为1的时候，英文转中文，2为中文转英文</param>
        /// <returns></returns>
        public string DateWeekChToEn(string DateTostring, int nType)
        {
            string strWeek = "";
            if (nType == 1)
            {
                switch (DateTostring)
                {
                    case "Monday":
                        strWeek = "星期一";
                        break;
                    case "Tuesday":
                        strWeek = "星期二";
                        break;
                    case "Wednesday":
                        strWeek = "星期三";
                        break;
                    case "Thursday":
                        strWeek = "星期四";
                        break;
                    case "Friday":
                        strWeek = "星期五";
                        break;
                    case "Saturday":
                        strWeek = "星期六";
                        break;
                    case "Sunday":
                        strWeek = "星期日";
                        break;
                }
            }
            else
            {
                switch (DateTostring)
                {
                    case "星期一":
                        strWeek = "Monday";
                        break;
                    case "星期二":
                        strWeek = "Tuesday";
                        break;
                    case "星期三":
                        strWeek = "Wednesday";
                        break;
                    case "星期四":
                        strWeek = "Thursday";
                        break;
                    case "星期五":
                        strWeek = "Friday";
                        break;
                    case "星期六":
                        strWeek = "Saturday";
                        break;
                    case "星期日":
                        strWeek = "Sunday";
                        break;
                }
            }
            return strWeek;
        }


        /// <summary>
        /// 获得表最大ID
        /// </summary>
        /// <param name="strTableName">表名</param>
        /// <returns></returns>
        public int GetMaxID(string strTableName)
        {
            int nMaxID = 0;
            string strSql = "";
            strSql = "SELECT MAX(ID) AS MAXID FROM " + strTableName;
            dbConn = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection conn = new SQLiteConnection(dbConn.m_ConnString);
            SQLiteCommand command = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = default(SQLiteDataReader);

            try
            {
                conn.Open();
                datRead = command.ExecuteReader();
                if (datRead.Read())
                {
                    if (datRead["MAXID"] == System.DBNull.Value)
                    {
                        nMaxID = 0;
                    }
                    else
                    {
                        nMaxID = Convert.ToInt32(datRead["MAXID"]);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("执行数据库SQL语句错误：" + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return nMaxID;
        }

        /// <summary>
        /// 根据字典编码查询字典名称
        /// </summary>
        /// <param name="Code">字典编码</param>
        /// <param name="Type">字典类型
        /// 2性别，3学历，4职务，1部门
        /// </param>
        /// <returns></returns>
        public string GetNameByCode(int Code, int Type)
        {
            string strName = "";
            string strSql = "SELECT NAME FROM SYS_DIC T WHERE T.TYPECODE=" + Type + " AND CODE=" + Code;

            dbConn = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection conn = new SQLiteConnection(dbConn.m_ConnString);
            SQLiteCommand command = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = default(SQLiteDataReader);

            try
            {
                conn.Open();
                datRead = command.ExecuteReader();
                if (datRead.Read())
                {
                    strName = datRead["NAME"].ToString();
                }
            }
            catch { }
            finally
            {
                conn.Dispose();
                command.Dispose();
                conn.Close();
            }
            return strName;
        }


        /// <summary>
        /// 根据字典名称得到字典编码
        /// </summary>
        /// <param name="strName">字典名称</param>
        /// <param name="Type">字典类型
        ///2性别，3学历，4职务，1部门</param>
        /// <returns></returns>
        public int GetCodeByName(string strName, int Type)
        {
            int nCode = 0;
            string strSql = "SELECT CODE FROM SYS_DIC T WHERE T.TYPECODE=" + Type + " AND NAME='" + strName + "'";
            dbConn = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection conn = new SQLiteConnection(dbConn.m_ConnString);
            SQLiteCommand command = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = default(SQLiteDataReader);
            try
            {
                conn.Open();
                datRead = command.ExecuteReader();
                if (datRead.Read())
                {
                    nCode = Convert.ToInt32(datRead["CODE"].ToString());
                }
            }
            catch { }
            finally
            {
                conn.Dispose();
                command.Dispose();
                conn.Close();
            }
            return nCode;
        }

        /// <summary>
        /// 根据类型code和名称获取备注信息
        /// </summary>
        /// <param name="Code"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        public string GetDicReamrk(int Code, string Name)
        {
            string strRemark = "";
            if (Name == "") return strRemark;
            string strSql = "SELECT REMARK FROM SYS_DIC T WHERE T.TYPECODE=" + Code + " AND NAME='" + Name + "'";

            dbConn = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection conn = new SQLiteConnection(dbConn.m_ConnString);
            SQLiteCommand command = new SQLiteCommand(strSql, conn);
            SQLiteDataReader datRead = default(SQLiteDataReader);

            try
            {
                conn.Open();
                datRead = command.ExecuteReader();
                if (datRead.Read())
                {
                    strRemark = datRead["REMARK"].ToString();
                }
            }
            catch { }
            finally
            {
                conn.Dispose();
                command.Dispose();
                conn.Close();
            }
            return strRemark;
        }

        /// <summary>
        /// 根据ID的值删除记录
        /// </summary>
        /// <param name="nID">ID</param>
        /// <param name="strTableName">表名</param>
        /// <returns>返回布尔值，True时为删除成功</returns>
        public bool DeleteByID(int nID, string strTableName)
        {
            bool bBool = false;
            if (nID == 0 && strTableName == string.Empty) return bBool;
            string strSql = "";
            strSql = "DELETE FROM " + strTableName + " WHERE ID=" + nID;

            dbConn = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection conn = new SQLiteConnection(dbConn.m_ConnString);
            SQLiteCommand command = new SQLiteCommand(strSql, conn);
            conn.Open();

            int nAffected = -1;
            try
            {
                nAffected = command.ExecuteNonQuery();
                if (nAffected != -1)
                {
                    bBool = true;
                }
            }
            catch
            { }
            finally
            {
                conn.Close();
            }
            return bBool;
        }

        #region "MD5加密"

        /// <summary>
        /// 获得MD5加密结果
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetMD5Hash(String input)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            byte[] res = md5.ComputeHash(Encoding.Default.GetBytes(input), 0, input.Length);
            char[] temp = new char[res.Length];

            System.Array.Copy(res, temp, res.Length);

            return new String(temp);
        }

        /// <summary>
        /// 获得MD5的16进制结果
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetMD5HashHex(String input)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            DES des = new DESCryptoServiceProvider();
            byte[] res = md5.ComputeHash(Encoding.Default.GetBytes(input), 0, input.Length);

            String returnThis = "";

            for (int i = 0; i < res.Length; i++)
            {
                returnThis += System.Uri.HexEscape((char)res[i]);
            }
            returnThis = returnThis.Replace("%", "");
            returnThis = returnThis.ToLower();

            return returnThis;
        }

        #endregion


        /// <summary>
        /// 判断字符串是否为数字
        /// </summary>
        /// <param name="str">字符串值</param>
        /// <returns>
        /// true时为数字，false时非数字
        /// </returns>
        public bool InStrint(string str)
        {
            bool nflag = true;
            if (str != "")
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (!char.IsNumber(str, i))
                    {
                        nflag = false;
                        break;
                    }
                }
            }
            return nflag;

        }

        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <param name="strSql"></param>
        public bool ExceuteSql(string strSql)
        {
            bool bTrue = false;
            dbConn = new DBClass(DbFilePath.FilePathName);
            SQLiteConnection conn = new SQLiteConnection(dbConn.m_ConnString);
            SQLiteCommand command = new SQLiteCommand(strSql, conn);
            conn.Open();

            int nAffected = -1;
            try
            {
                nAffected = command.ExecuteNonQuery();
                bTrue = true;
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            finally
            {
                conn.Close();
                command.Dispose();
            }
            return bTrue;
        }


        /// <summary>
        /// 导出数据到Excel中
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public void ExportToExcel(System.Data.DataTable dt)
        {
            if (dt == null) return;
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            if (xlApp == null)
            {
                XtraMessageBox.Show("无法创建Excel对象，可能您的电脑未安装Excel", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            System.Windows.Forms.SaveFileDialog saveDia = new SaveFileDialog();
            saveDia.Filter = "Excel2003(*.xls)|*.xls|Excel2007(*.xlsx)|*.xlsx|Excel2010(*.xlsx)|*.xlsx";
            saveDia.Title = "导出为Excel文件";
            if (saveDia.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.Empty.Equals(saveDia.FileName))
            {
                Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
                Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
                Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];//取得sheet1 
                Microsoft.Office.Interop.Excel.Range range = null;
                long totalCount = dt.Rows.Count;
                long rowRead = 0;
                float percent = 0;
                string fileName = saveDia.FileName;

                //写入标题 
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    worksheet.Cells[1, i + 1] = dt.Columns[i].ColumnName;
                    range = (Microsoft.Office.Interop.Excel.Range)worksheet.Cells[1, i + 1];
                    //range.Interior.ColorIndex = 15;//背景颜色 
                    range.Font.Bold = true;//粗体 
                    range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;//居中 
                    //加边框 
                    range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, null);
                    //range.ColumnWidth = 4.63;//设置列宽 
                    //range.EntireColumn.AutoFit();//自动调整列宽 
                    //r1.EntireRow.AutoFit();//自动调整行高 
                    
                }

                //写入内容 
                for (int r = 0; r < dt.DefaultView.Count; r++)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        worksheet.Cells[r + 2, i + 1] = dt.DefaultView[r][i];
                        range = (Microsoft.Office.Interop.Excel.Range)worksheet.Cells[r + 2, i + 1];
                        range.Font.Size = 9;//字体大小 
                        //加边框 
                        range.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, null);
                        range.EntireColumn.AutoFit();//自动调整列宽 
                    }
                    rowRead++;
                    percent = ((float)(100 * rowRead)) / totalCount;
                    System.Windows.Forms.Application.DoEvents();
                }

                range.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                if (dt.Columns.Count > 1)
                {
                    range.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                }

                try
                {
                    workbook.Saved = true;
                    workbook.SaveCopyAs(fileName);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("导出文件时出错,文件可能正被打开！\n" + ex.Message, "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                workbooks.Close();
                if (xlApp != null)
                {
                    xlApp.Workbooks.Close();
                    xlApp.Quit();
                    int generation = System.GC.GetGeneration(xlApp);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
                    xlApp = null;
                    System.GC.Collect(generation);
                }
                GC.Collect();//强行销毁 

                #region 强行杀死最近打开的Excel进程
                System.Diagnostics.Process[] excelProc = System.Diagnostics.Process.GetProcessesByName("EXCEL");
                System.DateTime startTime = new DateTime();
                int m, killId = 0;
                for (m = 0; m < excelProc.Length; m++)
                {
                    if (startTime < excelProc[m].StartTime)
                    {
                        startTime = excelProc[m].StartTime;
                        killId = m;
                    }
                }
                if (excelProc[killId].HasExited == false)
                {
                    excelProc[killId].Kill();
                }
                #endregion
                XtraMessageBox.Show("导出成功", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

    }
}
