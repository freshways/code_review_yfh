﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace DXTempLate.Class
{
    public class CL身高
    {
        int nCOMID = 0; //端口

        private SerialPort comm = new SerialPort();
        private long received_count = 0;//接收计数

        private bool Listening = false;//是否没有执行完invoke相关操作
        private bool Closing = false;//是否正在关闭串口，执行Application.DoEvents，并阻止再次invoke
        private List<byte> buffer = new List<byte>(4096);//默认分配1页内存，并始终限制不允许超过
        private byte[] binary_data_1 = new byte[17];

        //private string BlueAddr = "";

        #region 委托/事件

        public delegate void SendToSGEventHandler(CL身高Info Info);

        public event SendToSGEventHandler SendToSGMsg;

        public event SendToSGEventHandler SendToSGResult;

        #endregion

        CL身高Info info = new CL身高Info();
        public bool init身高(int COMINT = 17)
        {
            nCOMID = COMINT;//此处变量17
            try
            {
                if (!comm.IsOpen)
                {
                    comm.DataReceived += comm_DataReceived;
                    //关闭时点击，则设置好端口，波特率后打开
                    comm.PortName = string.Format("COM{0}", nCOMID);
                    comm.BaudRate = 9600;//外接蓝牙时，用9600能获取到
                    comm.Open();
                }

                info._消息 = "身高连接成功！";
                if (SendToSGMsg != null)
                    SendToSGMsg(info);
                return true;
            }
            catch (Exception ex)
            {
                comm.Dispose();
                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                comm = new SerialPort();
                info._消息 = "身高连接失败！";
                if (SendToSGMsg != null)
                    SendToSGMsg(info);
                return false;
                //现实异常信息给客户。
                throw new Exception(ex.Message);
            }
        }

        void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //CL身高Info info = new CL身高Info();
            if (Closing) return;//如果正在关闭，忽略操作，直接返回，尽快的完成串口监听线程的一次循环
            try
            {
                Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                received_count += n;//增加接收计数
                comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                buffer.AddRange(buf);
                //2.完整性判断
                while (buffer.Count > 4)//至少要包含头（2字节）
                {
                    //请不要担心使用>=，因为>=已经和>,<,=一样，是独立操作符，并不是解析成>和=2个符号
                    //2.1 查找数据头
                    if (buffer[0] == 0x57 && buffer[1] == 0x3A)
                    {
                        if (buffer[buffer.Count - 1] == 0x0A)//遇到测试结果直接跳出，并进行数据位校验
                        {
                            if (buffer.Count < 17) break;
                            buffer.CopyTo(0, binary_data_1, 0, buffer.Count);//复制一条完整数据到具体的数据缓存
                            data_1_catched = true;
                            buffer.RemoveRange(0, buffer.Count);//正确分析一条数据，从缓存中移除数据。
                            continue;//继续下一次循环
                        }
                        else
                        {
                            break;//如果没遇到结束位，直接返回什么都不做
                        }
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    string ss = Encoding.ASCII.GetString(binary_data_1);
                    string[] sss = ss.Replace("\r\n", "").Split(new char[] { ' ' });

                    foreach (string s in sss)
                    {
                        string[] s1 = s.Split(new char[] { ':' });
                        if (s1[0] == "H")
                            info._身高 = Convert.ToDouble(s1[1]).ToString(); 
                        else if (s1[0] == "W")
                            info._体重 = Convert.ToDouble(s1[1]).ToString();
                    }
                    if (SendToSGResult!=null)
                        SendToSGResult(info);
                }
            }
            finally
            {
                Listening = false;//我用完了，ui可以关闭串口了。
                ////本着用完就关的原则
                //if (comm.IsOpen)
                //    comm.Close();
            }
        }

    }

    public class CL身高Info
    {
        public string _身高 { get; set; }
        public string _体重 { get; set; }
        public string _BMI { get; set; }
        public string _消息 { get; set; }
    }
}
