﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace DXTempLate.Class
{
    /// <summary>
    /// 蓝牙设备 结构体
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct BLUETOOTH_DEVICE
    {
        public string Name;//蓝牙名称，如：”PC_300SNT”。
        public string Addr;//蓝牙地址，如：”(00:1D:07:3E:F5:60)”
    }
    /// <summary>
    /// 设备类型代码 枚举
    /// </summary>
    public enum DEVICE_TYPE_PC_80B
    {
        DEVICE_TYPE_PC_80B = 7010,//PC_80B 设备的代码为 7010
    }

    public enum nTransMode
    { 
        TRANSMIT_MODE_CONTINUOUS = 5000,//连续测量模式
        TRANSMIT_MODE_FILE = 5001,//文件传输模式
        TRANSMIT_MODE_QUICK = 5002//快速测量模式
    }
    
    public class EcgHelper  // 调用 CreativeHealthSDK 解析SCP文件
    {        
        #region 委托

        public delegate void OnDiscoveryCompleted(int nCount, IntPtr devices);

        #endregion

        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FingerOximeterSetParamAction(IntPtr hCOMPort, bool bFlag);

        #region 操作蓝牙
        /// <summary>
        /// 获取用于蓝牙操作的句柄
        /// </summary>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern IntPtr BluetoothOperationOpen();

        /// <summary>
        /// 搜索附近蓝牙设备
        /// </summary>
        /// <param name="hBluetooth">用于蓝牙相关操作的句柄。</param>
        /// <param name="timeOut">搜索超时时间，如输入 15，则超时时间为 15s</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern Boolean BluetoothOperationDiscovery(IntPtr hBluetooth, int timeOut);

        /// <summary>
        /// 注册 ON_DISCOVERY_COMPLETED 回调函数
        /// </summary>
        /// <param name="handler">hBluetooth: 用于蓝牙相关操作的句柄</param>
        /// <param name="func">回调函数地址。</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern Boolean BluetoothOperationRegisterDiscoveryCallBack(IntPtr handler, OnDiscoveryCompleted func);

        /// <summary>
        /// 与指定蓝牙设备进行连接。
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="Addr"></param>
        /// <param name="nCOMID"></param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool BluetoothOperationConnect(IntPtr handler, string Addr, ref int nCOMID);

        /// <summary>
        /// 断开蓝牙连接。
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool BluetoothOperationDisConnect(IntPtr handler);

        /// <summary>
        /// 关闭蓝牙句柄
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool BluetoothOperationClose(IntPtr handler);


        /// <summary>
        /// 获取错误代码
        /// </summary>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern int CreativeHealthGetLastError();
        [DllImport("CreativeHealthSDK.dll")]
        public static extern int CreativeDeviceGetLastError();
        #endregion

        #region 操作串口
        /// <summary>
        /// 获取用于串口操作的句柄
        /// </summary>
        /// <param name="nDeviceType"></param>
        /// <returns>不为NULL则调用成功，如果失败通过CreativeHealthGet LastError 获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern IntPtr SerialPortOpen(int nDeviceType);
        /// <summary>
        /// 连接串口
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <param name="nCOMID">需要连接的串口号，由 BluetoothOperationConnect 获得</param>
        /// <returns>如果返回CREATIVE_TRUE则调用成功，返回CREATIVE_FALSE则调用失败，通过CreativeDeviceGetLastError获得错误代码</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool SerialPortConnect(IntPtr hCOMPort, int nCOMID);
        /// <summary>
        /// 与串口断开连接
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool SerialPortDisconnect(IntPtr hCOMPort);
        /// <summary>
        /// 关闭串口句柄
        /// </summary>
        /// <param name="hCOMPort">用于串口相关操作的句柄</param>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern void SerialPortClose(IntPtr hCOMPort);
        #endregion

        public struct SYSTEMTIME
        {
            public ushort wYear;
            public ushort wMonth;
            public ushort wDayOfWeek;
            public ushort wDay;
            public ushort wHour;
            public ushort wMinute;
            public ushort wSecond;
            public ushort wMilliseconds;
        }
        //Ecg数据结构
        public struct ECG_FILE_DATA
        {
            public SYSTEMTIME stTime; //开始测量时间
            public int nMeasureMode; //心电测量模式
            public int nSmoothingMode;//滤波模式
            public int nAverageHR;    //平均心率值
            public int nGain; //心电波形增益值，取值为0，1，2，4。0 代表增益为1/2。
            public int nAnalysis;//心电分析结果
            public int nIndex;//在整个测量中的录的索引。第一个记录的索引为1。当nMeasureMode为MEASUREMODE_CONTINUOUS时有效。
            public int DataLen;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4500)]
            public int[] ecgData;
        }

        /// <summary>
        /// 获取到设备版本信息时调用
        /// </summary>
        /// <param name="hwMajor">硬件主版本</param>
        /// <param name="hwMinor">硬件子版本</param>
        /// <param name="swMajor">软件主版本</param>
        /// <param name="swMinor">软件子版本</param>
        /// <param name="alMajor">算法主版本</param>
        /// <param name="alMinor">算法子版本</param>
        public delegate void OnGetDeviceVerEcg(int hwMajor, int hwMinor,int swMajor, int swMinor,int alMajor, int alMinor);

        /// <summary>
        /// 注册 OnGetDeviceVerEcg 回调函数
        /// </summary>
        /// <param name="comPort">用于串口相关操作的句柄</param>
        /// <param name="Func">回调函数地址</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool ECGRegisterDeviceVerCallBack(IntPtr comPort, OnGetDeviceVerEcg Func);

        /// <summary>
        /// 查询设备版本
        /// </summary>
        /// <param name="comPort">用于串口相关操作的句柄</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool ECGQueryDeviceVer(IntPtr comPort);

        /// <summary>
        /// 获取到请求消息时调用。该消息将在接收文件数据或连续测量时获取到
        /// </summary>
        /// <param name="deviceID">设备ID</param>
        /// <param name="productID">产品ID</param>
        /// <param name="smoothingMode">滤波模式。分别为正常模式和增强模式</param>
        /// <param name="transMode">传输模式。分别为文件传输模式、连续测量模式和快速测量模式。</param>
        public delegate void OnGetRequest(string deviceID, string productID,int smoothingMode, int transMode);

        /// <summary>
        /// 注册 OnGetRequest 回调函数
        /// </summary>
        /// <param name="comPort">用于串口相关操作的句柄</param>
        /// <param name="Func">回调函数地址</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool ECGRegisterRequestCallBack(IntPtr comPort, OnGetRequest Func);

        /// <summary>
        /// 获取文件数据时调用。 通过本函数， 应用将会收到来自快速测量的 30 秒记录数据
        /// </summary>
        /// <param name="nResult">文件传输结果</param>
        /// <param name="nLen">数据的大小</param>
        /// <param name="arrData">设备发送的心电记录数据</param>
        public delegate void OnGetFileTransmit(int nResult, int nLen,[MarshalAs(UnmanagedType.LPArray, SizeConst = 10240)]byte[] arrData);

        /// <summary>
        /// 注册 OnGetFileTransmit 回调函数。
        /// </summary>
        /// <param name="comPort">用于串口相关操作的句柄</param>
        /// <param name="Func">回调函数地址</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool ECGRegisterFileTransmitCallBack(IntPtr comPort,OnGetFileTransmit Func);

        /// <summary>
        /// 获取到准备阶段实时数据时调用。调用频率为 6Hz。1mV 幅值对应 416 采样值。
        /// </summary>
        /// <param name="bLeadoff">导联脱落时为 true，未脱落时为 false。</param>
        /// <param name="nGain">当前心电波形增益，取值为 0，1，2，4。0 代表增益为 1/2</param>
        /// <param name="nLen">数据的大小</param>
        /// <param name="arrData">心电波形数据</param>
        public delegate void OnGetRealtimePrepare(int bLeadoff, int nGain, int nLen, [MarshalAs(UnmanagedType.LPArray, SizeConst = 25)]int[] arrData);

        /// <summary>
        /// 注册 OnGetRealtimePrepare 回调函数
        /// </summary>
        /// <param name="comPort">用于串口相关操作的句柄</param>
        /// <param name="Func">回调函数地址</param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool ECGRegisterRealTimePrepareCallBack(IntPtr comPort,OnGetRealtimePrepare Func);

        /// <summary>
        /// 获取到测量阶段实时时调用。调用频率为 6Hz。1mV 幅值对应 416 采样值
        /// </summary>
        /// <param name="bLeadoff">导联脱落时为 true，未脱落时为 false</param>
        /// <param name="nGain">当前心电波形增益，取值为 0，1，2，4。0 代表增益为 1/2。当nTransMode为TRANSMIT_MODE_QUICK时该参数有效</param>
        /// <param name="transMode">传输模式。分别为文件传输模式、连续测量模式和快速测量模式</param>
        /// <param name="nHR">心率值，当nTransMode为TRANSMIT_MODE_CONTINUOUS时该参数有效</param>
        /// <param name="nPower">电池电量，单位为 mV。当 nTransMode 为TRANSMIT_MODE_CONTINUOUS 时该参数有效</param>
        /// <param name="nLen">数据的大小</param>
        /// <param name="arrData">心电波形数据</param>
        public delegate void OnGetRealtimeMeasure(int bLeadoff, int nGain, int transMode, int nHR, int nPower, int nLen, int nDataLength, 
            [MarshalAs(UnmanagedType.LPArray, SizeConst = 25)]int[] arrData);

        /// <summary>
        /// 注册 OnGetRealtimeMeasure 回调函数。
        /// </summary>
        /// <param name="comPort">用于串口相关操作的句柄</param>
        /// <param name="Func">回调函数地址</param>
       /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool ECGRegisterRealTimeMeasureCallBack(IntPtr comPort,OnGetRealtimeMeasure Func);

        /// <summary>
        /// 测量结束时调用
        /// </summary>
        /// <param name="stTime">测量开始时间。当 nTransMode 为 TRANSMIT_MODE_QUICK时该参数有效</param>
        /// <param name="transMode">传输模式。分别为文件传输模式、连续测量模式和快速测量模式</param>
        /// <param name="nResult">心电分析结果。当 nTransMode 为 TRANSMIT_MODE_QUICK时该参数有效</param>
        public delegate void OnGetRealtimeResult(SYSTEMTIME stTime,int transMode, int nResult);

        /// <summary>
        /// 注册 OnGetRealtimeResult 回调函数。
        /// </summary>
        /// <param name="comPort">用于串口相关操作的句柄</param>
        /// <param name="Func">回调函数地址</param>
        /// <returns></returns>
         [DllImport("CreativeHealthSDK.dll")]
        public static extern bool ECGRegisterRealTimeResultCallBack(IntPtr comPort,OnGetRealtimeResult Func);

        /// <summary>
        /// 获取到电池电量等级时调用
        /// </summary>
        /// <param name="nPower"></param>
        public delegate void OnGetPower(int nPower);

        /// <summary>
        /// 注册查询电池电量等级
        /// </summary>
        /// <param name="comPort"></param>
        /// <param name="Func"></param>
        /// <returns></returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool ECGRegisterPowerCallBack(IntPtr comPort, OnGetPower Func);

        
        #region 7.2API 说明
        /// <summary>
        /// 解析 SCP 文件，该 SCP 文件由设备发送而来或从设备磁盘拷贝而来
        /// </summary>
        /// <param name="pFileData">SCP 文件数据</param>
        /// <param name="nFileLen">数据大小</param>
        /// <param name="efd">获取到得心电相关数据</param>
        /// <returns>如果返回true则调用成功， 返回false则调用失败</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FileOperationAnalyseSCPFile([MarshalAs(UnmanagedType.LPArray, SizeConst = 9796)]byte[] pFileData, 
            int nFileLen, ref ECG_FILE_DATA efd);


        /// <summary>
        /// 说明：打包 ECG 文件。将 ECG_FILE_DATA 结构打包成 ECG 文件到指定路径下。参照
        /// </summary>
        /// <param name="efd">efd数据</param>
        /// <param name="Addr">保存文件的路径</param>
        /// <returns>如果返回true则调用成功， 返回false则调用失败</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FileOperationPackECGFile(ECG_FILE_DATA efd, string Addr);

        /// <summary>
        /// 解析ECG文件。
        /// </summary>
        /// <param name="arrFileData">ECG 文件数据</param>
        /// <param name="nLen">数据大小</param>
        /// <param name="efd">获取到得心电相关数据</param>
        /// <returns>如果返回true则调用成功， 返回false则调用失败， 通过CreativeDeviceGetLastError获得错误代码。</returns>
        [DllImport("CreativeHealthSDK.dll")]
        public static extern bool FileOperationAnalyseECGFile([MarshalAs(UnmanagedType.LPArray, SizeConst = 10040)]sbyte[] arrFileData, 
            int nLen, ref ECG_FILE_DATA efd);
        
        #endregion



        #region 传输模式
        public const int TRANSMIT_MODE_CONTINUOUS = 5000;//连续测量模式
        public const int TRANSMIT_MODE_FILE = 5001;//文件传输模式
        public const int TRANSMIT_MODE_QUICK = 5002;//快速测量模式 
        #endregion

        #region 滤波模式
        public const int SMOOTHING_MODE_NORMAL = 5003;//说明：正常模式。
        public const int SMOOTHING_MODE_GAIN = 5004;//说明：增强模式。 
        #endregion

        private static Dictionary<int, string> _dic = new Dictionary<int, string>();

        public static Dictionary<int,string> Mydic 
        {
            get 
            {
                if (_dic.Count > 0) return _dic;
                else
                {
                    _dic.Add(5025, "节律无异常。");
                    _dic.Add(5026, "疑似心跳稍快，请注意休息。");
                    _dic.Add(5027, "疑似心跳过快，请注意休息。");
                    _dic.Add(5028, "疑似阵发性心跳过快，请咨询医生。");
                    _dic.Add(5029, "疑似心跳稍缓，请注意休息。");
                    _dic.Add(5030, "疑似心跳过缓，请注意休息。");
                    _dic.Add(5031, "疑似心跳间期缩短，请咨询医生。");
                    _dic.Add(5032, "疑似心跳间期不规则，请咨询医生。");
                    _dic.Add(5033, "疑似心跳稍快伴有心跳间期缩短，请咨询医生。");
                    _dic.Add(5034, "疑似心跳稍缓伴有心跳间期缩短，请咨询医生。");
                    _dic.Add(5035, "疑似心跳稍缓伴有心跳间期不规则，请咨询医生。");
                    _dic.Add(5036, "波形有漂移。");
                    _dic.Add(5037, "疑似心跳过快伴有波形漂移，请咨询医生。");
                    _dic.Add(5038, "疑似心跳过缓伴有波形漂移，请咨询医生。");
                    _dic.Add(5039, "疑似心跳间期缩短伴有波形漂移，请咨询医生。");
                    _dic.Add(5040, "疑似心跳间期不规则伴有波形漂移，请咨询医生。");
                    _dic.Add(5041, "信号较差，请重新测量。");
                }
                return _dic;
            }
        }

        #region 心电分析结果
        public const int ECG_RESULT_00 = 5025;//说明：节律无异常。
        public const int ECG_RESULT_01 = 5026;//说明：疑似心跳稍快，请注意休息。
        public const int ECG_RESULT_02 = 5027;//说明：疑似心跳过快，请注意休息。
        public const int ECG_RESULT_03 = 5028;//说明：疑似阵发性心跳过快，请咨询医生。
        public const int ECG_RESULT_04 = 5029;//说明：疑似心跳稍缓，请注意休息。
        public const int ECG_RESULT_05 = 5030;//说明：疑似心跳过缓，请注意休息。
        public const int ECG_RESULT_06 = 5031;//说明：疑似心跳间期缩短，请咨询医生。
        public const int ECG_RESULT_07 = 5032;//说明：疑似心跳间期不规则，请咨询医生。
        public const int ECG_RESULT_08 = 5033;//说明：疑似心跳稍快伴有心跳间期缩短，请咨询医生。
        public const int ECG_RESULT_09 = 5034;//说明：疑似心跳稍缓伴有心跳间期缩短，请咨询医生。
        public const int ECG_RESULT_0A = 5035;//说明：疑似心跳稍缓伴有心跳间期不规则，请咨询医生。
        public const int ECG_RESULT_0B = 5036;//说明：波形有漂移。
        public const int ECG_RESULT_0C = 5037;//说明：疑似心跳过快伴有波形漂移，请咨询医生。
        public const int ECG_RESULT_0D = 5038;//说明：疑似心跳过缓伴有波形漂移，请咨询医生。
        public const int ECG_RESULT_0E = 5039;//说明：疑似心跳间期缩短伴有波形漂移，请咨询医生。
        public const int ECG_RESULT_0F = 5040;//说明：疑似心跳间期不规则伴有波形漂移，请咨询医生。
        public const int ECG_RESULT_FF = 5041;//说明：信号较差，请重新测量。 
        #endregion

        #region 文件传输结果
        public const int FILE_TRANSMIT_START = 6554;//说明：文件传输开始。
        public const int FILE_TRANSMIT_SUCCESS = 6555;//说明：文件传输完成。
        public const int FILE_TRANSMIT_SUSPEND_OR_TIMEOUT = 6556;//说明：文件传输中断或超时。
        public const int FILE_TRANSMIT_ERROR = 6557;//说明：接收到数据异常 
        #endregion

        #region 测量模式
        public const int MEASUREMODE_QUICK = 5046;//说明：快速测量。
        public const int MEASUREMODE_CONTINUOUS = 5047; //说明：连续测量。 
        #endregion

    }
}
