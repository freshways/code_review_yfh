﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace DXTempLate.Class
{
    /// <summary>
    /// 读写ini配置文件通用类
    /// </summary>
    public class IniFile
    {
        private string IniFileName;

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileInt(string lpAppName, string lpKeyName,
         int nDefault, string lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string lpAppName, string lpKeyName,
         string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

        [DllImport("kernel32")]
        private static extern bool WritePrivateProfileString(string lpAppName,
         string lpKeyName, string lpString, string lpFileName);

        public IniFile(string Filename)
        {
            IniFileName = Filename;
        }
        /// <summary>
        /// 读取ini配置文件中的 int的值
        /// </summary>
        /// <param name="Section">标志标签</param>
        /// <param name="Key">值</param>
        /// <param name="Default">默认值</param>
        /// <returns></returns>
        public int ReadIni(string Section, string Key, int Default)
        {
            return GetPrivateProfileInt(Section, Key, Default, IniFileName);
        }
        /// <summary>
        /// 读取ini配置文件中的 string的值
        /// </summary>
        /// <param name="Section">标志标签</param>
        /// <param name="Key">值</param>
        /// <param name="Default">默认值</param>
        /// <returns></returns>
        public string ReadIni(string Section, string Key, string Default)
        {
            StringBuilder temp = new StringBuilder(1024);
            int rec = GetPrivateProfileString(Section, Key, Default, temp, 1024, IniFileName);
            return temp.ToString();
        }
        /// <summary>
        /// 写入ini文件值
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="intValue"></param>
        public void WriteIni(string Section, string Key, int intValue)
        {
            WritePrivateProfileString(Section, Key, intValue.ToString(), IniFileName);
        }
        /// <summary>
        /// 写入ini文件值
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="strValue"></param>
        public void WriteIni(string Section, string Key, string strValue)
        {
            WritePrivateProfileString(Section, Key, strValue, IniFileName);
        }

    }
}
