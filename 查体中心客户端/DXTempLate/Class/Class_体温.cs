﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DXTempLate.Class
{
    public class Cl体温
    {
        int nCOMID = 0; //端口

        private SerialPort comm = new SerialPort();
        private long received_count = 0;//接收计数
        private bool Listening = false;//是否没有执行完invoke相关操作
        private bool Closing = false;//是否正在关闭串口，执行Application.DoEvents，并阻止再次invoke
        private List<byte> buffer = new List<byte>(4096);//默认分配1页内存，并始终限制不允许超过
        private byte[] binary_data_1 = new byte[4];

        #region 委托/事件

        public delegate void SendToTWEventHandler(string Info); //返回结果

        public event SendToTWEventHandler SendToTWResult;

        public event SendToTWEventHandler SendToTWMsg;

        #endregion

        public bool Init体温(int COMINT = 4)
        {
            nCOMID = COMINT;
            try
            {
                if (comm.IsOpen)
                {
                    Closing = true;
                    while (Listening) System.Windows.Forms.Application.DoEvents();
                    comm.DataReceived -= comm_DataReceived;
                    comm.Close();
                    //System.Threading.Thread.Sleep(1000);
                    comm = new SerialPort();
                }
                if (!comm.IsOpen)
                {
                    //添加事件注册
                    comm.DataReceived += comm_DataReceived;

                    //设置好端口，波特率后打开
                    comm.PortName = string.Format("COM{0}", nCOMID);
                    comm.BaudRate = 4800;
                    comm.WriteTimeout = 500;
                    comm.Open();
                }
                if (SendToTWMsg != null)
                    SendToTWMsg("体温连接成功！");
                return true;
            }
            catch (Exception ex)
            {
                if (SendToTWMsg != null)
                    SendToTWMsg("体温连接失败！" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 体温返回结果处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (Closing) return;//如果正在关闭，忽略操作，直接返回，尽快的完成串口监听线程的一次循环
            try
            {
                Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                received_count += n;//增加接收计数
                comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                buffer.AddRange(buf);
                //2.完整性判断
                while (buffer.Count > 1)//至少要包含头（2字节）+长度（1字节）+校验（1字节）
                {
                    if (buffer[0] == 0xFF)
                    {
                        int len = 2;//数据长度
                        if (buffer.Count < len + 1) break;//数据不够的时候什么都不做
                        //异或校验，逐个字节异或得到校验码
                        byte checksum = 0;
                        for (int i = 1; i < len + 1; i++)//len+3表示校验之前的位置
                        {
                            checksum ^= buffer[i];
                        }
                        if (checksum != buffer[3]) //如果数据校验失败，丢弃这一包数据
                        {
                            buffer.RemoveRange(0, 4);//从缓存中删除错误数据
                            continue;//继续下一次循环
                        }
                        //至此，已经被找到了一条完整数据。我们将数据直接分析，或是缓存起来一起分析
                        //我们这里采用的办法是缓存一次，好处就是如果你某种原因，数据堆积在缓存buffer中
                        //已经很多了，那你需要循环的找到最后一组，只分析最新数据，过往数据你已经处理不及时
                        //了，就不要浪费更多时间了，这也是考虑到系统负载能够降低。
                        buffer.CopyTo(0, binary_data_1, 0, 4);//复制一条完整数据到具体的数据缓存
                        data_1_catched = true;
                        buffer.RemoveRange(0, 4);//正确分析一条数据，从缓存中移除数据。
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    //我们的数据都是定好格式的，所以当我们找到分析出的数据1，就知道固定位置一定是这些数据，我们只要显示就可以了
                    string data = binary_data_1[1].ToString("X2") + " " + binary_data_1[2].ToString("X2") + " ";

                    double d = Convert.ToInt32(data.Replace(" ", ""), 16) * 0.1;

                    if (SendToTWResult != null)
                        SendToTWResult(d.ToString());
                }

            }
            finally
            {
                Listening = false;//我用完了，ui可以关闭串口了。
            }
        }


    }
}
