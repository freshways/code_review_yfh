﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace DXTempLate.Class
{
    class IDReadHelper
    {
        static int iRetUSB = 0;
        static int iRetCOM = 0;

        #region 委托/事件

        public  delegate void SendToSGEventHandler(string Info);

        public static event SendToSGEventHandler SendToSFZMsg;

        #endregion
        /// <summary>
        /// 此方法最好在From的Load事件里调用，与读卡器建立连接
        /// </summary>
        /// <returns>
        /// 1	正确
        /// 2	端口打开失败
        /// 0	动态库加载失败
        ///</returns>
        public static bool InitReader()
        {
            //记录是否初始化成功
            bool initResult = false;

            try
            {
                int iPort;
                for (iPort = 1001; iPort <= 1016; iPort++)
                {
                    iRetUSB = CVRSDK.CVR_InitComm(iPort);
                    if (iRetUSB == 1)
                    {
                        break;
                    }
                }
                //if (iRetUSB != 1) //此处判断其他连接方式，com暂时不需要所以注释，为了更快的判断是否连接成功
                //{
                //    for (iPort = 1; iPort <= 4; iPort++)
                //    {
                //        iRetCOM = CVRSDK.CVR_InitComm(iPort);
                //        if (iRetCOM == 1)
                //        {
                //            break;
                //        }
                //    }
                //}

                if ((iRetCOM == 1) || (iRetUSB == 1))
                {
                    initResult = true;
                }
                else
                {
                    initResult = false;
                }
            }
            catch (Exception ex)
            {
                if (SendToSFZMsg != null)
                    SendToSFZMsg("身份证读卡器 InitReader:" + ex.Message);
                //MessageBox.Show("身份证读卡器 InitReader:" + ex.Message);
                //LogHelper.WriteErrorLog("身份证读卡器 InitReader:" + ex.Message);
                initResult = false;
            }

            return initResult;
        }


        /// <summary>
        /// 如果ReaderInit被调用过，在程序结束前必须调用CloseReader方法。
        /// </summary>
        /// <returns>1:正确  0：错误</returns>
        public static int CloseReader()
        {
            int isSuccess = 0;
            try
            {
                isSuccess = CVRSDK.CVR_CloseComm();

            }
            catch (Exception ex)
            {
                isSuccess = 0;
                //LogHelper.WriteErrorLog("关闭读卡器 CloseReader：" + ex.Message);
            }
            return isSuccess;
        }

        /// <summary>
        /// 从读卡器中读取身份证信息, 0表示失败，1表示读卡成功
        /// </summary>
        /// <returns>0表示失败，1表示读卡成功</returns>
        public static int ReadIDInfoFromInstr()
        {
            int iRet = 0;
            try
            {
                if ((iRetCOM == 1) || (iRetUSB == 1))
                {
                    //CVR_Authenticate返回值如下：
                    //1	正确	卡片认证成功
                    //2	错误	寻卡失败
                    //3	错误	选卡失败
                    //0	错误	初始化失败
                    int authenticate = CVRSDK.CVR_Authenticate();
                    if (authenticate == 1)
                    {
                        //CVR_Read_Content返回值如下，CVR_Read_Content的参数无实际作用
                        //1	正确
                        //0	错误
                        //99	异常
                        int readContent = CVRSDK.CVR_Read_Content(4);
                        if (readContent == 1)
                        {
                            //label10.Text = "读卡操作成功！";
                            iRet = GetIDInfoFromTempFile(); 
                            if (SendToSFZMsg != null)
                                SendToSFZMsg("读卡操作成功！");
                        }
                        //else
                        //{
                        //    label10.Text = "读卡操作失败！";
                        //}
                    }
                    else
                    {
                        if (SendToSFZMsg != null)
                            SendToSFZMsg("未放卡或卡片放置不正确！");
                        //MessageBox.Show("未放卡或卡片放置不正确");
                    }
                }
                else
                {
                    if (SendToSFZMsg!=null)
                        SendToSFZMsg("初始化失败！");
                    //MessageBox.Show("初始化失败！");
                    //LogHelper.WriteErrorLog("身份证读卡器 ReaderID:初始化失败！");
                }
            }
            catch (Exception ex)
            {
                //LogHelper.WriteErrorLog("身份证读卡器 ReaderID:" + ex.Message);
                iRet = 0;
            }
            return iRet;
        }

        #region 姓名、性别、年龄的对外接口
        private static string m_name = "";
        //private static string m_sex = "";
        private static string m_birday = "";
        private static string m_sfzh = "";
        public static string Name
        {
            get { return m_name; }
        }
        public static string Sex
        {
            get { return m_sex; }
        }
        public static string Age
        {
            get
            {
                try
                {
                    DateTime b = DateTime.Parse(m_birday);
                    double a = (DateTime.Now - b).TotalDays;
                    double aYear = Math.Floor(a / 365);
                    double C = a % 365;
                    double aMonth = Math.Floor(C / 30);
                    double aDay = Math.Floor(C % 30);
                    // strRetAge = aYear.ToString() + "岁   零" + aMonth.ToString() + "个月   " + aDay.ToString() + "天";                
                    if (aDay > 15)
                    {
                        aMonth = aMonth + 1;
                        aDay = 0;
                    }
                    if (aMonth > 6)
                    {
                        aYear = aYear + 1;
                        aMonth = 0;
                    }

                    if (aYear == 0 & aMonth == 0 & aDay != 0)
                        return aDay.ToString() + "天";//2 天
                    else if (aYear == 0 & aMonth != 0 & aDay <= 15)
                        return aMonth.ToString() + "月";//1 月
                    else if (aYear != 0 & aMonth <= 6)
                        return aYear.ToString() + "岁";//0 岁
                    else
                        return "";
                }
                catch
                {
                    return "";
                }
            }
        }
        public static string Birth
        {
            get { return m_birday; }
        }

        public static string Minzu
        {
            get { return m_minzu; }
        }
        public static string Qixian
        {
            get { return m_qixian; }
        }
        public static string Addr
        {
            get { return m_address; }
        }
        public static string SFZH
        {
            get
            {
                return m_sfzh;
            }
        }
        #endregion

        /// <summary>
        /// 0:异常  1：表示正常
        /// 读卡成功后，程序目录下回生成wz.txt的临时文件，身份证中的信息都在此文件中
        /// </summary>
        private static int GetIDInfoFromTempFile()
        {
            //0:异常  1：表示正常
            int iRet = 0;
            try
            {
                using (StreamReader reader = new StreamReader(Application.StartupPath + "\\wz.txt", Encoding.GetEncoding("GB2312")))
                {
                    //==========================wz.txt中文件内容的格式如下====================
                    //张红叶                                        //第一行是姓名
                    //女                                            //第二行是性别
                    //汉                                            //第三行是民族
                    //1988-11-18                                    //第四行是出生日期
                    //河北省邯郸市临漳县称勾镇称勾东村复兴路25号
                    //130423198811184328
                    //临漳县公安局
                    //2011.03.30-2021.03.30
                    m_name = reader.ReadLine();
                    m_sex = reader.ReadLine();
                    m_minzu = reader.ReadLine();//我们不需要民族信息，所以不存储
                    m_birday = reader.ReadLine();
                    m_address = reader.ReadLine();//过滤掉地址信息
                    m_sfzh = reader.ReadLine();
                    reader.ReadLine();
                    m_qixian = reader.ReadLine();
                    iRet = 1;
                }
            }
            catch (Exception ex)
            {
                iRet = 0;
                //获取身份证信息出现错误时，将姓名、性别、出生日期清空
                m_name = "";
                //m_sex = "";
                m_birday = "";

                //记录日志
                //LogHelper.WriteErrorLog("身份证读卡器 GetIDInfo:" + ex.Message);
            }

            return iRet;
        }

        private static string m_sex { get; set; }

        private static string m_minzu { get; set; }

        private static string m_address { get; set; }

        private static string m_qixian { get; set; }
    }
}
