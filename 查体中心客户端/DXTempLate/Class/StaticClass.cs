﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DXTempLate.Class
{
    /// <summary>
    /// 静态常量
    /// </summary>
    public class StaticClass
    {
        /// <summary>
        /// 当前用户名称
        /// </summary>
        public static string g_CurUserName;

        /// <summary>
        /// 当前用户ID
        /// </summary>
        public static int g_CurUserID;

        /// <summary>
        /// 系统名称
        /// </summary>
        public static string g_TitleName;
    }
}
