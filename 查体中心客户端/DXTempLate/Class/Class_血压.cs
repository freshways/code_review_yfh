﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DXTempLate.Class
{
    public class CL血压
    {
        int nCOMID = 0; //端口

        private SerialPort comm = new SerialPort();
        private long received_count = 0;//接收计数
        private long send_count = 0;//发送计数
        private bool Listening = false;//是否没有执行完invoke相关操作
        private bool Closing = false;//是否正在关闭串口，执行Application.DoEvents，并阻止再次invoke
        private bool IsSend = false;//是否发送握手
        private List<byte> buffer = new List<byte>(4096);//默认分配1页内存，并始终限制不允许超过
        private byte[] binary_data_1 = new byte[20];
        private string SenMsg = "CC-80-02-03-01-01-00";

        #region 委托/事件

        public delegate void SendToXYEventHandler(CL血压Info Info); //返回结果

        public event SendToXYEventHandler SendToXYResult;

        //public delegate void SendToXYMessage(CL血压Info Info); //返回结果

        public event SendToXYEventHandler SendToXYMsg;

        #endregion

        CL血压Info info = new CL血压Info();
        public bool init血压(int COMINT = 4)
        {
            try
            {
                if (comm.IsOpen)
                {
                    //Send(SenMsg);
                }
                nCOMID = COMINT;//此处变量17
                //添加事件注册
                comm.DataReceived += comm_DataReceived;
            
                if (!comm.IsOpen)
                {
                    //串口关闭时，设置好端口，波特率后打开
                    comm.PortName = string.Format("COM{0}", nCOMID);
                    comm.BaudRate = 115200;
                    comm.WriteTimeout = 500;
                    comm.Open();
                }

                info._消息 = "血压计连接成功!";
                if (SendToXYMsg != null)
                    SendToXYMsg(info);
                if (IsSend) //是否握手
                    Send(SenMsg);
                return true;
            }
            catch (Exception ex)
            {
                comm.Dispose();
                //捕获到异常信息，创建一个新的comm对象，之前的不能用了。
                comm = new SerialPort();
                info._消息 = "血压计断开连接!";
                if (SendToXYMsg != null)
                    SendToXYMsg(info);
                return false;
            }
        }

        void comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            CL血压Info info = new CL血压Info();
            if (Closing) return;//如果正在关闭，忽略操作，直接返回，尽快的完成串口监听线程的一次循环
            try
            {
                Listening = true;//设置标记，说明我已经开始处理数据，一会儿要使用系统UI的。
                int n = comm.BytesToRead;//先记录下来，避免某种原因，人为的原因，操作几次之间时间长，缓存不一致
                byte[] buf = new byte[n];//声明一个临时数组存储当前来的串口数据
                received_count += n;//增加接收计数
                comm.Read(buf, 0, n);//读取缓冲数据

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //<协议解析>
                bool data_1_catched = false;//缓存记录数据是否捕获到
                //1.缓存数据
                buffer.AddRange(buf);
                //2.完整性判断
                while (buffer.Count > 5)//至少要包含头（2字节）+长度（1字节）+校验（1字节）
                {
                    //请不要担心使用>=，因为>=已经和>,<,=一样，是独立操作符，并不是解析成>和=2个符号
                    //2.1 查找数据头
                    if (buffer[0] == 0xAA && buffer[1] == 0x80)
                    {
                        //2.2 探测缓存数据是否有一条数据的字节，如果不够，就不用费劲的做其他验证了
                        //前面已经限定了剩余长度>=4，那我们这里一定能访问到buffer[2]这个长度
                        int len = buffer[3];//数据长度
                        //数据完整判断第一步，长度是否足够
                        //len是数据段长度,4个字节是while行注释的3部分长度
                        if (buffer.Count < len + 5) break;//数据不够的时候什么都不做
                        //这里确保数据长度足够，数据头标志找到，我们开始计算校验
                        //2.3 校验数据，确认数据正确
                        //异或校验，逐个字节异或得到校验码
                        byte checksum = 0;
                        for (int i = 2; i <= len + 3; i++)//len+1表示校验之前的位置
                        {
                            checksum ^= buffer[i];
                        }
                        if (checksum != buffer[len + 4]) //如果数据校验失败，丢弃这一包数据
                        {
                            buffer.RemoveRange(0, len + 5);//从缓存中删除错误数据
                            continue;//继续下一次循环
                        }
                        //至此，已经被找到了一条完整数据。我们将数据直接分析，或是缓存起来一起分析
                        //我们这里采用的办法是缓存一次，好处就是如果你某种原因，数据堆积在缓存buffer中
                        //已经很多了，那你需要循环的找到最后一组，只分析最新数据，过往数据你已经处理不及时
                        //了，就不要浪费更多时间了，这也是考虑到系统负载能够降低。
                        buffer.CopyTo(0, binary_data_1, 0, len + 5);//复制一条完整数据到具体的数据缓存
                        data_1_catched = true;
                        buffer.RemoveRange(0, len + 5);//正确分析一条数据，从缓存中移除数据。                       
                    }
                    else
                    {
                        //这里是很重要的，如果数据开始不是头，则删除数据
                        buffer.RemoveAt(0);
                    }
                }
                //分析数据
                if (data_1_catched)
                {
                    //下面开始分析数据
                    if (binary_data_1[4] == 0x01)//从这个位置是判断标识，01是测量阶段
                    {//1-4是应答，5是测量数据，6是结果数据，7是错误信息
                        if (binary_data_1[5] == 0x01)
                        {
                            string connstate = "成功";
                            if (binary_data_1[6] == 0x01)
                            {
                                connstate = "失败";                                
                            }
                            info._消息 = string.Format("连接血压计:{0}！", connstate);
                        }
                        else if (binary_data_1[5] == 0x02)
                        {
                            string connstate = "成功";
                            if (binary_data_1[6] == 0x01)
                            {
                                connstate = "失败";
                            }
                            info._消息 = string.Format("启动测量:{0}！", connstate);
                        }
                        else if (binary_data_1[5] == 0x05) //实时数据
                        {
                            string data = "";
                            if (binary_data_1[3] == 0x08)
                                data = (binary_data_1[10] ^ binary_data_1[11]).ToString("X2");
                            else if (binary_data_1[3] == 0x04)
                                data = (binary_data_1[7]).ToString("X2");
                            //string data = (binary_data_1[10] ^ binary_data_1[11]).ToString("X2");
                            //更新界面 0x00<<8+0x7e^0x00
                             info._消息 = (Convert.ToInt32(data, 16)).ToString();
                        }
                        else if (binary_data_1[5] == 0x06)//测试结果直接跳出
                        {
                            //我们的数据都是定好格式的，所以当我们找到分析出的数据1，就知道固定位置一定是这些数据，我们只要显示就可以了
                            string s收缩压 = (binary_data_1[13] ^ binary_data_1[14]).ToString("X2");
                            string s舒张压 = (binary_data_1[15] ^ binary_data_1[16]).ToString("X2");
                            string s心率 = (binary_data_1[17] ^ binary_data_1[18]).ToString("X2");

                            //更新界面
                            info._收缩压 = (Convert.ToInt32(s收缩压, 16)).ToString();

                            info._舒张压 = (Convert.ToInt32(s舒张压, 16)).ToString();

                            info._心率 = (Convert.ToInt32(s心率, 16)).ToString();

                            info._消息 = "血压测量完毕";
                            if (SendToXYResult!=null)
                                SendToXYResult(info);
                        }
                        else if (buffer[5] == 0x07)//遇到异常直接跳出
                        {
                            //错误数据分析过程 1-12
                            //我们的数据都是定好格式的，所以当我们找到分析出的数据1，就知道固定位置一定是这些数据，我们只要显示就可以了
                            string data = buffer[6].ToString("X2");
                            //更新界面
                             info._消息 = "测量失败:" + data; 
                        }
                    }
                    if (SendToXYMsg != null)
                        SendToXYMsg(info);
                }
                //如果需要别的协议，只要扩展这个data_n_catched就可以了。往往我们协议多的情况下，还会包含数据编号，给来的数据进行
                //编号，协议优化后就是： 头+编号+长度+数据+校验
                //</协议解析>
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////

            }
            finally
            {
                Listening = false;//我用完了，ui可以关闭串口了。
            }
        }

        public void Send(string treaty) //treaty
        {
            if (!comm.IsOpen)
            {
                init血压();
            }
            try
            {
                //定义一个变量，记录发送了几个字节
                int n = 0;
                //16进制发送
                if (true)
                {
                    string SendString = treaty.Replace("-", "");
                    //我们不管规则了。如果写错了一些，我们允许的，只用正则得到有效的十六进制数
                    MatchCollection mc = Regex.Matches(SendString, @"(?i)[\da-f]{2}");
                    List<byte> buf = new List<byte>();//填充到这个临时列表中
                    //依次添加到列表中
                    byte checksum = 0;
                    foreach (Match m in mc)
                    {
                        //buf.Add(byte.Parse(m.Value));
                        buf.Add(Convert.ToByte(m.Value, 16));
                        if (m.Value.Equals("AA") || m.Value.Equals("CC") || m.Value.Equals("80"))
                        { }
                        else
                            checksum ^= Convert.ToByte(m.Value, 16);//得到异或校验值
                    }
                    buf.Add(checksum);//把异或校验值添加到最后
                    //转换列表为数组后发送
                    comm.Write(buf.ToArray(), 0, buf.Count);
                    //记录发送的字节数
                    n = buf.Count;
                }
            }
            catch (Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }

    }
    public class CL血压Info
    {
        public string _收缩压 { get; set; }
        public string _舒张压 { get; set; }
        public string _心率 { get; set; }
        public string _消息 { get; set; }
    }

}
