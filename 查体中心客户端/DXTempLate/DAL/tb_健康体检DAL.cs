using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.SQLite;
using DXTempLate.Class;

namespace DXTempLate
{
    /// <summary>
    /// tb_健康体检DAL类
    /// BY：狂人代码生成器 V1.0
    /// 作者：金属狂人
    /// 日期：2016年04月12日 03:22:54
    /// </summary>
    public class tb_健康体检DAL
    {

        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="where">条件</param>
        /// <returns>结果集</returns>
        public static SQLiteDataReader Gettb_健康体检Info(string where)
        {
            string sqlStr = "SELECT * FROM tb_健康体检 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 获取所有信息集合
        /// </summary>
        /// <returns>List集合</returns>
        public static List<tb_健康体检Info> Gettb_健康体检InfoList(string where)
        {
            List<tb_健康体检Info> infoList = new List<tb_健康体检Info>();

            SQLiteDataReader reader = null;
            try
            {
                reader = Gettb_健康体检Info(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_健康体检Info info = new tb_健康体检Info();
                info.ID = reader["ID"].ToString();
                info.身份证号 = reader["身份证号"].ToString();
                info.姓名 = reader["姓名"].ToString();
                info.性别 = reader["性别"].ToString();
                info.出生日期 = reader["出生日期"].ToString();
                info.症状 = reader["症状"].ToString();
                info.体温 = string.IsNullOrEmpty(reader["体温"].ToString()) ? 0 : Convert.ToDouble(reader["体温"].ToString());
                info.呼吸 = string.IsNullOrEmpty(reader["呼吸"].ToString()) ? 0 : Convert.ToInt32(reader["呼吸"].ToString());
                info.脉搏 = string.IsNullOrEmpty(reader["脉搏"].ToString()) ? 0 : Convert.ToInt32(reader["脉搏"].ToString());
                info.血压右侧1 = reader["血压右侧1"].ToString();
                info.血压右侧2 = reader["血压右侧2"].ToString();
                info.血压左侧1 = reader["血压左侧1"].ToString();
                info.血压左侧2 = reader["血压左侧2"].ToString();
                info.身高 = string.IsNullOrEmpty(reader["身高"].ToString()) ? 0 : Convert.ToDouble(reader["身高"].ToString());
                info.腰围 = string.IsNullOrEmpty(reader["腰围"].ToString()) ? 0 : Convert.ToDouble(reader["腰围"].ToString());
                info.体重 = string.IsNullOrEmpty(reader["体重"].ToString()) ? 0 : Convert.ToDouble(reader["体重"].ToString());
                info.体重指数 = reader["体重指数"].ToString();
                info.老年人认知 = reader["老年人认知"].ToString();
                info.老年人情感 = reader["老年人情感"].ToString();
                info.老年人认知分 = reader["老年人认知分"].ToString();
                info.老年人情感分 = reader["老年人情感分"].ToString();
                info.左眼视力 = string.IsNullOrEmpty(reader["左眼视力"].ToString()) ? 0 : Convert.ToDouble(reader["左眼视力"].ToString());
                info.右眼视力 = string.IsNullOrEmpty(reader["右眼视力"].ToString()) ? 0 : Convert.ToDouble(reader["右眼视力"].ToString());
                info.左眼矫正 = reader["左眼矫正"].ToString();
                info.右眼矫正 = reader["右眼矫正"].ToString();
                info.听力 = reader["听力"].ToString();
                info.运动功能 = reader["运动功能"].ToString();
                info.皮肤 = reader["皮肤"].ToString();
                info.淋巴结 = reader["淋巴结"].ToString();
                info.淋巴结其他 = reader["淋巴结其他"].ToString();
                info.桶状胸 = reader["桶状胸"].ToString();
                info.呼吸音 = reader["呼吸音"].ToString();
                info.呼吸音异常 = reader["呼吸音异常"].ToString();
                info.罗音 = reader["罗音"].ToString();
                info.罗音异常 = reader["罗音异常"].ToString();
                info.心率 = string.IsNullOrEmpty(reader["心率"].ToString()) ? 0 : Convert.ToInt32(reader["心率"].ToString());
                info.心律 = reader["心律"].ToString();
                info.杂音 = reader["杂音"].ToString();
                info.杂音有 = reader["杂音有"].ToString();
                info.压痛 = reader["压痛"].ToString();
                info.压痛有 = reader["压痛有"].ToString();
                info.包块 = reader["包块"].ToString();
                info.包块有 = reader["包块有"].ToString();
                info.肝大 = reader["肝大"].ToString();
                info.肝大有 = reader["肝大有"].ToString();
                info.脾大 = reader["脾大"].ToString();
                info.脾大有 = reader["脾大有"].ToString();
                info.浊音 = reader["浊音"].ToString();
                info.浊音有 = reader["浊音有"].ToString();
                info.下肢水肿 = reader["下肢水肿"].ToString();
                info.肛门指诊 = reader["肛门指诊"].ToString();
                info.肛门指诊异常 = reader["肛门指诊异常"].ToString();
                info.G_QLX = reader["G_QLX"].ToString();
                info.查体其他 = reader["查体其他"].ToString();
                info.白细胞 = reader["白细胞"].ToString();
                info.血红蛋白 = reader["血红蛋白"].ToString();
                info.血小板 = reader["血小板"].ToString();
                info.血常规其他 = reader["血常规其他"].ToString();
                info.尿蛋白 = reader["尿蛋白"].ToString();
                info.尿糖 = reader["尿糖"].ToString();
                info.尿酮体 = reader["尿酮体"].ToString();
                info.尿潜血 = reader["尿潜血"].ToString();
                info.尿常规其他 = reader["尿常规其他"].ToString();
                info.大便潜血 = reader["大便潜血"].ToString();
                info.血清谷丙转氨酶 = reader["血清谷丙转氨酶"].ToString();
                info.血清谷草转氨酶 = reader["血清谷草转氨酶"].ToString();
                info.白蛋白 = reader["白蛋白"].ToString();
                info.总胆红素 = reader["总胆红素"].ToString();
                info.结合胆红素 = reader["结合胆红素"].ToString();
                info.血清肌酐 = reader["血清肌酐"].ToString();
                info.血尿素氮 = reader["血尿素氮"].ToString();
                info.总胆固醇 = reader["总胆固醇"].ToString();
                info.甘油三酯 = reader["甘油三酯"].ToString();
                info.血清低密度脂蛋白胆固醇 = reader["血清低密度脂蛋白胆固醇"].ToString();
                info.血清高密度脂蛋白胆固醇 = reader["血清高密度脂蛋白胆固醇"].ToString();
                info.空腹血糖 = reader["空腹血糖"].ToString();
                info.乙型肝炎表面抗原 = reader["乙型肝炎表面抗原"].ToString();
                info.眼底 = reader["眼底"].ToString();
                info.眼底异常 = reader["眼底异常"].ToString();
                info.心电图 = reader["心电图"].ToString();
                info.心电图异常 = reader["心电图异常"].ToString();
                info.胸部X线片 = reader["胸部X线片"].ToString();
                info.胸部X线片异常 = reader["胸部X线片异常"].ToString();
                info.B超 = reader["B超"].ToString();
                info.B超其他 = reader["B超其他"].ToString();
                info.辅助检查其他 = reader["辅助检查其他"].ToString();
                info.G_JLJJY = reader["G_JLJJY"].ToString();
                info.个人档案编号 = reader["个人档案编号"].ToString();
                info.创建机构 = reader["创建机构"].ToString();
                info.创建时间 = reader["创建时间"].ToString();
                info.创建人 = reader["创建人"].ToString();
                info.修改时间 = reader["修改时间"].ToString();
                info.修改人 = reader["修改人"].ToString();
                info.体检日期 = reader["体检日期"].ToString();
                info.所属机构 = reader["所属机构"].ToString();
                info.FIELD1 = reader["FIELD1"].ToString();
                info.FIELD2 = reader["FIELD2"].ToString();
                info.FIELD3 = reader["FIELD3"].ToString();
                info.FIELD4 = reader["FIELD4"].ToString();
                info.WBC_SUP = string.IsNullOrEmpty(reader["WBC_SUP"].ToString()) ? 0 : Convert.ToInt32(reader["WBC_SUP"].ToString());
                info.PLT_SUP = string.IsNullOrEmpty(reader["PLT_SUP"].ToString()) ? 0 : Convert.ToInt32(reader["PLT_SUP"].ToString());
                info.G_TUNWEI = reader["G_TUNWEI"].ToString();
                info.G_YTWBZ = reader["G_YTWBZ"].ToString();
                info.锻炼频率 = reader["锻炼频率"].ToString();
                info.每次锻炼时间 = reader["每次锻炼时间"].ToString();
                info.坚持锻炼时间 = reader["坚持锻炼时间"].ToString();
                info.锻炼方式 = reader["锻炼方式"].ToString();
                info.饮食习惯 = reader["饮食习惯"].ToString();
                info.吸烟状况 = reader["吸烟状况"].ToString();
                info.日吸烟量 = reader["日吸烟量"].ToString();
                info.开始吸烟年龄 = reader["开始吸烟年龄"].ToString();
                info.戒烟年龄 = reader["戒烟年龄"].ToString();
                info.饮酒频率 = reader["饮酒频率"].ToString();
                info.日饮酒量 = reader["日饮酒量"].ToString();
                info.是否戒酒 = reader["是否戒酒"].ToString();
                info.戒酒年龄 = reader["戒酒年龄"].ToString();
                info.开始饮酒年龄 = reader["开始饮酒年龄"].ToString();
                info.近一年内是否曾醉酒 = reader["近一年内是否曾醉酒"].ToString();
                info.饮酒种类 = reader["饮酒种类"].ToString();
                info.饮酒种类其它 = reader["饮酒种类其它"].ToString();
                info.有无职业病 = reader["有无职业病"].ToString();
                info.具体职业 = reader["具体职业"].ToString();
                info.从业时间 = reader["从业时间"].ToString();
                info.化学物质 = reader["化学物质"].ToString();
                info.化学物质防护 = reader["化学物质防护"].ToString();
                info.化学物质具体防护 = reader["化学物质具体防护"].ToString();
                info.毒物 = reader["毒物"].ToString();
                info.G_DWFHCS = reader["G_DWFHCS"].ToString();
                info.G_DWFHCSQT = reader["G_DWFHCSQT"].ToString();
                info.放射物质 = reader["放射物质"].ToString();
                info.放射物质防护措施有无 = reader["放射物质防护措施有无"].ToString();
                info.放射物质防护措施其他 = reader["放射物质防护措施其他"].ToString();
                info.口唇 = reader["口唇"].ToString();
                info.齿列 = reader["齿列"].ToString();
                info.咽部 = reader["咽部"].ToString();
                info.皮肤其他 = reader["皮肤其他"].ToString();
                info.巩膜 = reader["巩膜"].ToString();
                info.巩膜其他 = reader["巩膜其他"].ToString();
                info.足背动脉搏动 = reader["足背动脉搏动"].ToString();
                info.乳腺 = reader["乳腺"].ToString();
                info.乳腺其他 = reader["乳腺其他"].ToString();
                info.外阴 = reader["外阴"].ToString();
                info.外阴异常 = reader["外阴异常"].ToString();
                info.阴道 = reader["阴道"].ToString();
                info.阴道异常 = reader["阴道异常"].ToString();
                info.宫颈 = reader["宫颈"].ToString();
                info.宫颈异常 = reader["宫颈异常"].ToString();
                info.宫体 = reader["宫体"].ToString();
                info.宫体异常 = reader["宫体异常"].ToString();
                info.附件 = reader["附件"].ToString();
                info.附件异常 = reader["附件异常"].ToString();
                info.血钾浓度 = reader["血钾浓度"].ToString();
                info.血钠浓度 = reader["血钠浓度"].ToString();
                info.糖化血红蛋白 = reader["糖化血红蛋白"].ToString();
                info.宫颈涂片 = reader["宫颈涂片"].ToString();
                info.宫颈涂片异常 = reader["宫颈涂片异常"].ToString();
                info.平和质 = reader["平和质"].ToString();
                info.气虚质 = reader["气虚质"].ToString();
                info.阳虚质 = reader["阳虚质"].ToString();
                info.阴虚质 = reader["阴虚质"].ToString();
                info.痰湿质 = reader["痰湿质"].ToString();
                info.湿热质 = reader["湿热质"].ToString();
                info.血瘀质 = reader["血瘀质"].ToString();
                info.气郁质 = reader["气郁质"].ToString();
                info.特禀质 = reader["特禀质"].ToString();
                info.脑血管疾病 = reader["脑血管疾病"].ToString();
                info.脑血管疾病其他 = reader["脑血管疾病其他"].ToString();
                info.肾脏疾病 = reader["肾脏疾病"].ToString();
                info.肾脏疾病其他 = reader["肾脏疾病其他"].ToString();
                info.心脏疾病 = reader["心脏疾病"].ToString();
                info.心脏疾病其他 = reader["心脏疾病其他"].ToString();
                info.血管疾病 = reader["血管疾病"].ToString();
                info.血管疾病其他 = reader["血管疾病其他"].ToString();
                info.眼部疾病 = reader["眼部疾病"].ToString();
                info.眼部疾病其他 = reader["眼部疾病其他"].ToString();
                info.神经系统疾病 = reader["神经系统疾病"].ToString();
                info.神经系统疾病其他 = reader["神经系统疾病其他"].ToString();
                info.其他系统疾病 = reader["其他系统疾病"].ToString();
                info.其他系统疾病其他 = reader["其他系统疾病其他"].ToString();
                info.健康评价 = reader["健康评价"].ToString();
                info.健康评价异常1 = reader["健康评价异常1"].ToString();
                info.健康评价异常2 = reader["健康评价异常2"].ToString();
                info.健康评价异常3 = reader["健康评价异常3"].ToString();
                info.健康评价异常4 = reader["健康评价异常4"].ToString();
                info.健康指导 = reader["健康指导"].ToString();
                info.危险因素控制 = reader["危险因素控制"].ToString();
                info.危险因素控制体重 = reader["危险因素控制体重"].ToString();
                info.危险因素控制疫苗 = reader["危险因素控制疫苗"].ToString();
                info.危险因素控制其他 = reader["危险因素控制其他"].ToString();
                info.FIELD5 = reader["FIELD5"].ToString();
                info.症状其他 = reader["症状其他"].ToString();
                info.G_XYYC = reader["G_XYYC"].ToString();
                info.G_XYZC = reader["G_XYZC"].ToString();
                info.G_QTZHZH = reader["G_QTZHZH"].ToString();
                info.缺项 = reader["缺项"].ToString();
                info.口唇其他 = reader["口唇其他"].ToString();
                info.齿列其他 = reader["齿列其他"].ToString();
                info.咽部其他 = reader["咽部其他"].ToString();
                info.YDGNQT = reader["YDGNQT"].ToString();
                info.餐后2H血糖 = reader["餐后2H血糖"].ToString();
                info.老年人状况评估 = reader["老年人状况评估"].ToString();
                info.老年人自理评估 = reader["老年人自理评估"].ToString();
                info.粉尘 = reader["粉尘"].ToString();
                info.物理因素 = reader["物理因素"].ToString();
                info.职业病其他 = reader["职业病其他"].ToString();
                info.粉尘防护有无 = reader["粉尘防护有无"].ToString();
                info.物理防护有无 = reader["物理防护有无"].ToString();
                info.其他防护有无 = reader["其他防护有无"].ToString();
                info.粉尘防护措施 = reader["粉尘防护措施"].ToString();
                info.物理防护措施 = reader["物理防护措施"].ToString();
                info.其他防护措施 = reader["其他防护措施"].ToString();
                info.TNBFXJF = reader["TNBFXJF"].ToString();
                info.左侧原因 = reader["左侧原因"].ToString();
                info.右侧原因 = reader["右侧原因"].ToString();
                info.尿微量白蛋白 = reader["尿微量白蛋白"].ToString();
                info.完整度 = reader["完整度"].ToString();
                info.齿列缺齿 = reader["齿列缺齿"].ToString();
                info.齿列龋齿 = reader["齿列龋齿"].ToString();
                info.齿列义齿 = reader["齿列义齿"].ToString();
                info.RowState = reader["RowState"].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }

        /// <summary>
        /// 根据 主键 获取一个实体对象
        /// <param name="ID">主键</param>
        /// </summary>
        public static tb_健康体检Info Gettb_健康体检InfoById(string ID)
        {
            string strWhere = " ID ='" + ID + "'";
            List<tb_健康体检Info> list = Gettb_健康体检InfoList(strWhere);
            if (list.Count > 0)
                return list[0];
            return null;
        }

        private static DBClass dbConstring;
        /// <summary>
        /// 添加、修改
        /// </summary>
        /// <param name="Info"></param>
        /// <returns></returns>
        public static bool UpdateSys_血压(tb_健康体检Info Info)
        {
            bool bBool = false;
            DataSet datSet;
            DataTable datTable;
            isExists(Info.身份证号, out datSet, out datTable);
            DataRow datRow = null;

            bool bAdd = false;
            if (datTable.Rows.Count > 0)
            {
                datRow = datTable.Rows[0];
            }
            else
            {
                bAdd = true;
            }
            if (bAdd)
            {
                //增加一行
                datRow = datTable.NewRow();
                //datRow["ID"] = Info.ID;
                GetNewDataRow(datRow);
            }
            
            //基本信息
            datRow["身份证号"] = Info.身份证号;
            datRow["姓名"] = Info.姓名;
            datRow["性别"] = Info.性别;
            datRow["出生日期"] = Info.出生日期;
            datRow["住址"] = Info.住址;
            datRow["联系电话"] = Info.联系电话;
            datRow["备注"] = Info.备注;
            //身高体重
            datRow["身高"] = Info.身高;
            datRow["体重"] = Info.体重;
            datRow["体重指数"] = Info.体重指数;
            datRow["腰围"] = Info.腰围;
            datRow["危险因素控制体重"] = Info.危险因素控制体重;
            //血压
            datRow["血压右侧1"] = Info.血压右侧1;
            datRow["血压右侧2"] = Info.血压右侧2;
            datRow["心率"] = Info.心率;
            //体温
            datRow["体温"] = Info.体温;
            datRow["呼吸"] = Info.呼吸;
            //血糖
            datRow["空腹血糖"] = Info.空腹血糖;
            //视力
            datRow["左眼视力"] = Info.左眼视力;
            datRow["左眼矫正"] = Info.左眼矫正;
            datRow["右眼视力"] = Info.右眼视力;
            datRow["右眼矫正"] = Info.右眼矫正;

            datRow["体检日期"] = Info.体检日期;

            if (bAdd)
            {
                datTable.Rows.Add(datRow);
            }
            try
            {
                DBManager.UpdateDataSetForSqlite(datSet, "tb_健康体检");
                bBool = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return bBool;
        }

        private static void isExists(string IDCard,out DataSet datSet, out DataTable datTable)
        {
            dbConstring = new DBClass(DbFilePath.FilePathName);

            string strSql = "", strTableName = "tb_健康体检";
            //strSql = "SELECT * FROM  " + strTableName + " WHERE  (个人档案编号='" + Program.currentUser.DocNo  +
            //    "' or 身份证号='" + Program.currentUser.ID + "') AND 体检日期='" + DateTime.Now.ToString("yyyy-MM-dd") + "'  ";
            strSql = "SELECT * FROM  " + strTableName + " WHERE   体检日期='" + DateTime.Now.ToString("yyyy-MM-dd") + "' ";
            if (!string.IsNullOrEmpty(IDCard))
                strSql += " AND 身份证号='" + IDCard + "' ";
            datSet = DBManager.ExecuteDataSet(dbConstring.m_ConnString, strSql, CommandType.Text);
            datTable = datSet.Tables[0];
            datTable.TableName = strTableName;
        }

        private static void GetNewDataRow(DataRow dataRow)
        {
            //dataRow["身份证号"] = Program.currentUser.ID;
            //dataRow["个人档案编号"] = Program.currentUser.DocNo;
            //dataRow["姓名"] = Program.currentUser.Name;
            //dataRow["性别"] = Program.currentUser.Gender;
            //dataRow["出生日期"] = Program.currentUser.Birth;
            dataRow["体检日期"] = DateTime.Now.ToString("yyyy-MM-dd");
            dataRow["创建时间"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            dataRow["修改时间"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //dataRow["所属机构"] = Program.currentUser.Prgid;
            //dataRow["创建机构"] = Program.currentUser.Prgid;
            //dataRow["创建人"] = Program.currentUser.创建人;
            //dataRow["修改人"] = Program.currentUser.创建人;
            dataRow["症状"] = "1";
            dataRow["听力"] = "1";
            dataRow["运动功能"] = "1";
            dataRow["皮肤"] = "1";
            dataRow["淋巴结"] = "1";
            dataRow["桶状胸"] = "2";
            dataRow["呼吸音"] = "1";
            dataRow["罗音"] = "1";
            dataRow["心律"] = "1";
            dataRow["杂音"] = "1";
            dataRow["压痛"] = "1";
            dataRow["包块"] = "1";
            dataRow["肝大"] = "1";
            dataRow["脾大"] = "1";
            dataRow["浊音"] = "1";
            dataRow["下肢水肿"] = "1";
            dataRow["锻炼频率"] = "4";
            dataRow["饮食习惯"] = "1";
            dataRow["吸烟状况"] = "1";
            dataRow["饮酒频率"] = "1";
            dataRow["是否戒酒"] = "1";
            dataRow["有无职业病"] = "1";
            dataRow["口唇"] = "1";
            dataRow["齿列"] = "1";
            dataRow["咽部"] = "1";
            dataRow["巩膜"] = "1";
            dataRow["足背动脉搏动"] = "2";
            dataRow["脑血管疾病"] = "1";
            dataRow["肾脏疾病"] = "1";
            dataRow["心脏疾病"] = "1";
            dataRow["血管疾病"] = "1";
            dataRow["眼部疾病"] = "1";
            dataRow["神经系统疾病"] = "1";
            dataRow["其他系统疾病"] = "1";
            dataRow["健康评价"] = "1";
        }

        /// <summary>
        /// 删除一个对象
        /// </summary>
        /// <param name="ID">主键</param>
        /// <returns></returns>
        public static bool Deletetb_健康体检Info(tb_健康体检Info info)
        {
            bool rst = false;
            string sqlStr = "DELETE FROM tb_健康体检 WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.Conn121, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        
        /// <summary>
        /// 更新行状态
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        internal static bool UpdateSys_RowState(tb_健康体检Info info)
        {

            bool rst = false;
            string sqlStr = "update tb_健康体检 set rowstate = '1' WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }
        
        /// <summary>
        /// 更新机构
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        internal static bool UpdateSys_DocNoPrgid(tb_健康体检Info info)
        {

            bool rst = false;
            string sqlStr = "update tb_健康体检 set 个人档案编号=@DocNO,所属机构=@Prgid,创建机构=@Prgid WHERE ID=" + info.ID;
            int rows = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr, new object[] { new SQLiteParameter("@DocNO", info.个人档案编号), new SQLiteParameter("@Prgid", info.所属机构) });
            //int  = DBManager.ExecuteUpdate(DBManager.SQLiteConn, sqlStr);
            if (rows > 0)
            {
                rst = true;
            }
            return rst;
        }

    }
}
