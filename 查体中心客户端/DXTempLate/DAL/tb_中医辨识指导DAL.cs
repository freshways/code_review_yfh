﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace DXTempLate
{
    public class tb_中医辨识指导DAL
    {

        public static SQLiteDataReader Gettb_老年人中医药特征指导Info(string where)
        {
            string sqlStr = "SELECT * FROM tb_老年人中医保健知识指导 where ";
            if (String.IsNullOrEmpty(where))
            {
                sqlStr += "1=1";
            }
            else
            {
                sqlStr += where;
            }
            SQLiteDataReader reader = null;
            try
            {
                reader = DBManager.ExecuteQueryForSQLite(sqlStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        public static List<tb_中医体质指导> Gettb_老年人中医药特征管理InfoList(string where)
        {
            List<tb_中医体质指导> infoList = new List<tb_中医体质指导>();

            SQLiteDataReader reader = null;
            try
            {
                reader = Gettb_老年人中医药特征指导Info(where);
            }
            catch (Exception)
            {
                throw;
            }

            while (reader.Read())
            {
                tb_中医体质指导 info = new tb_中医体质指导();
                info.ID = reader[0].ToString();
                info.编号 = reader[1].ToString();
                info.内容 = reader[2].ToString();
                if(reader.FieldCount>3)
                    info.类型 = reader[3].ToString();
                infoList.Add(info);
            }
            reader.Close();
            return infoList;
        }
    }
}
