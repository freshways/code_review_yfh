﻿namespace DXTempLate
{
    partial class XtraFormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormLogin));
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtUserName = new DevExpress.XtraEditors.TextEdit();
            this.txtPassword = new DevExpress.XtraEditors.TextEdit();
            this.sBOK = new DevExpress.XtraEditors.SimpleButton();
            this.sBCancle = new DevExpress.XtraEditors.SimpleButton();
            this.LblMsg = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lblTitle.Location = new System.Drawing.Point(3, 33);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(569, 55);
            this.lblTitle.TabIndex = 6;
            this.lblTitle.Text = "沂水县牛岭埠查体中心";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtUserName
            // 
            this.txtUserName.EditValue = "demo";
            this.txtUserName.Location = new System.Drawing.Point(351, 155);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(156, 20);
            this.txtUserName.TabIndex = 7;
            // 
            // txtPassword
            // 
            this.txtPassword.EditValue = "123";
            this.txtPassword.Location = new System.Drawing.Point(351, 190);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Properties.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(156, 20);
            this.txtPassword.TabIndex = 8;
            // 
            // sBOK
            // 
            this.sBOK.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.sBOK.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.sBOK.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.sBOK.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.sBOK.Appearance.Options.UseBackColor = true;
            this.sBOK.Appearance.Options.UseBorderColor = true;
            this.sBOK.Appearance.Options.UseForeColor = true;
            this.sBOK.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.sBOK.Image = global::DXTempLate.Properties.Resources.ok;
            this.sBOK.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBOK.Location = new System.Drawing.Point(351, 227);
            this.sBOK.Name = "sBOK";
            this.sBOK.Size = new System.Drawing.Size(75, 24);
            this.sBOK.TabIndex = 9;
            this.sBOK.Click += new System.EventHandler(this.sBOK_Click);
            // 
            // sBCancle
            // 
            this.sBCancle.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.sBCancle.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.sBCancle.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.sBCancle.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.sBCancle.Appearance.Options.UseBackColor = true;
            this.sBCancle.Appearance.Options.UseBorderColor = true;
            this.sBCancle.Appearance.Options.UseForeColor = true;
            this.sBCancle.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.sBCancle.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sBCancle.Image = global::DXTempLate.Properties.Resources.Cancle;
            this.sBCancle.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sBCancle.Location = new System.Drawing.Point(432, 227);
            this.sBCancle.Name = "sBCancle";
            this.sBCancle.Size = new System.Drawing.Size(75, 24);
            this.sBCancle.TabIndex = 10;
            // 
            // LblMsg
            // 
            this.LblMsg.Appearance.ForeColor = System.Drawing.Color.Red;
            this.LblMsg.Location = new System.Drawing.Point(351, 135);
            this.LblMsg.Name = "LblMsg";
            this.LblMsg.Size = new System.Drawing.Size(108, 14);
            this.LblMsg.TabIndex = 11;
            this.LblMsg.Text = "用户名或密码错误！";
            this.LblMsg.Visible = false;
            // 
            // XtraFormLogin
            // 
            this.AcceptButton = this.sBOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
            this.BackgroundImageStore = global::DXTempLate.Properties.Resources.Login;
            this.CancelButton = this.sBCancle;
            this.ClientSize = new System.Drawing.Size(575, 281);
            this.Controls.Add(this.LblMsg);
            this.Controls.Add(this.sBCancle);
            this.Controls.Add(this.sBOK);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "XtraFormLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "日常财务管理软件";
            this.Load += new System.EventHandler(this.XtraFormLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassword.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private DevExpress.XtraEditors.TextEdit txtUserName;
        private DevExpress.XtraEditors.TextEdit txtPassword;
        private DevExpress.XtraEditors.SimpleButton sBOK;
        private DevExpress.XtraEditors.SimpleButton sBCancle;
        private DevExpress.XtraEditors.LabelControl LblMsg;
    }
}