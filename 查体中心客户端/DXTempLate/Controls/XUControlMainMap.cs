﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;
using DXTempLate.Class;

namespace DXTempLate.Controls
{
    public partial class XUControlMainMap : DevExpress.XtraEditors.XtraUserControl
    {
        public XUControlMainMap()
        {
            InitializeComponent();
        }
        private static XUControlMainMap _ItSelf;
        public static XUControlMainMap ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUControlMainMap();
                }
                return _ItSelf;
            }
        }
        private SqliteClass sqlite = new SqliteClass();

        private void XUControlMainMap_Load(object sender, EventArgs e)
        {

            //绑定年份
            int nYear = 2014;
            this.comboBoxYear.Properties.Items.Clear();
            for (int i = 0; i < 30; i++)
            {
                this.comboBoxYear.Properties.Items.Add(nYear + i);
            }
            this.comboBoxYear.Text = DateTime.Now.Year.ToString();

            //绑定统计图类型
            this.comboBoxEdit1.Properties.Items.Clear();
            this.comboBoxEdit1.Properties.Items.Add("直方图");
            this.comboBoxEdit1.Properties.Items.Add("3D直方图");
            this.comboBoxEdit1.Properties.Items.Add("圆饼图");
            this.comboBoxEdit1.Properties.Items.Add("3D圆饼图");
            this.comboBoxEdit1.Properties.Items.Add("线图");
            this.comboBoxEdit1.Properties.Items.Add("点图");
            this.comboBoxEdit1.SelectedIndex = 0;

            BindData(0, 0);//默认显示当前年的统计数据

            //设置图表标题  
            ChartTitle ct = new ChartTitle();
            ct.Text = "公司收入支出统计图";
            ct.TextColor = Color.Black;//颜色  
            ct.Font = new Font("Tahoma", 12);//字体  
            ct.Dock = ChartTitleDockStyle.Top;//停靠在上方  
            ct.Alignment = StringAlignment.Center;//居中显示  
            this.chartControl1.Titles.Add(ct);

            LoadChartInfo();
        }

        //统计查询
        private void simpleBtnQuery_Click(object sender, EventArgs e)
        {
            int nYear = 0, nMonth = 0;
            if (this.comboBoxYear.Text != "")
            {
                nYear = Convert.ToInt32(this.comboBoxYear.Text.Trim());
            }
            if (this.comboBoxMonth.Text != "")
            {
                nMonth = Convert.ToInt32(this.comboBoxMonth.Text.Trim());
            }

            BindData(nYear, nMonth);

            LoadChartInfo();
        }

        //当年查询
        private void simpleBtnYear_Click(object sender, EventArgs e)
        {
            int nYear = DateTime.Now.Year;
            this.comboBoxYear.Text = nYear.ToString();
            this.comboBoxMonth.SelectedIndex = 0;
            BindData(nYear, 0);
            LoadChartInfo();
        }

        /// <summary>
        /// 总收入
        /// </summary>
        private double mdtSumGSMoneyIN = 0;
        /// <summary>
        /// 总支出
        /// </summary>
        private double mdtSumGSMoneyOUT = 0;
        /// <summary>
        /// 总盈利
        /// </summary>
        private double mdtSumGSMoney = 0;

        /// <summary>
        /// 统计查询数据
        /// </summary>
        /// <param name="nYear">统计年份</param>
        /// <param name="nMonth">统计月份</param>
        private void BindData(int nYear, int nMonth)
        {
            double dtSumGSMoneyIN =  sqlite.GetSumGSMoney(0, nYear, nMonth,"Sys_MoneyIn"); //营销总收入
            double dtSumGSMoneyOUT = sqlite.GetSumGSMoney(1, nYear, nMonth,"Sys_MoneyOut");//营销总支出
            double dtSumGSMoneyOutTJ = 0;// sqlite.GetSumGSMoneyTJ(nYear, nMonth);//管理费用各项总支出和
            double dtSumGSMoney = dtSumGSMoneyIN - (dtSumGSMoneyOUT + dtSumGSMoneyOutTJ);

            mdtSumGSMoneyIN = dtSumGSMoneyIN;
            mdtSumGSMoneyOUT = dtSumGSMoneyOUT + dtSumGSMoneyOutTJ;
            mdtSumGSMoney = dtSumGSMoney;

            //总收入=营销总收入
            //总支出=营销总支出 + 管理费用统计
            //盈利=总收入-总支出

            this.txtSumGSMoneyIN.Text = Math.Round(dtSumGSMoneyIN, 2).ToString();
            this.txtSumGSMoneyOUT.Text = Math.Round(dtSumGSMoneyOUT + dtSumGSMoneyOutTJ, 2).ToString();
            this.txtSumGSMoney.Text = Math.Round(dtSumGSMoney, 2).ToString();

            this.lblMoneyOutType.Text = "【营销费用支出:" + dtSumGSMoneyOUT + "】";
            this.lblMoneyOutType2.Text = "【管理费用支出:" + dtSumGSMoneyOutTJ + "】";
        }

        /// <summary>
        /// 统计图
        /// </summary>
        private Series mySeries;

        /// <summary>
        /// 手动加载统计图信息
        /// </summary>
        private void LoadChartInfo()
        {
            this.chartControl1.Series.Clear();

            //新建Series
            switch (comboBoxEdit1.Text)
            {
                case "直方图":
                    mySeries = new Series("直方图", ViewType.Bar);
                    break;
                case "3D直方图":
                    mySeries = new Series("3D直方图", ViewType.Bar3D);
                    break;
                case "圆饼图":
                    mySeries = new Series("圆饼图", ViewType.Pie);
                    ((PiePointOptions)mySeries.PointOptions).PointView = PointView.ArgumentAndValues;
                    break;
                case "3D圆饼图":
                    mySeries = new Series("3D圆饼图", ViewType.Pie3D);
                    break;
                case "线图":
                    mySeries = new Series("线图", ViewType.Line);
                    ((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
                    break;
                case "点图":
                    mySeries = new Series("点图", ViewType.Point);
                    ((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
                    break;
                default:
                    mySeries = new Series("直方图", ViewType.Bar);
                    break;
            }

            //设置Series样式  
            mySeries.ArgumentScaleType = ScaleType.Qualitative;//定性的
            mySeries.ValueScaleType = ScaleType.Numerical;//数字类型  


            mySeries.PointOptions.PointView = PointView.ArgumentAndValues;//显示表示的信息和数据  
            mySeries.PointOptions.ValueNumericOptions.Format = NumericFormat.Number;//NumericFormat.Percent;//用百分比表示
            //mySeries.PointOptions.ValueNumbericOptions.Precision = 0;//百分号前面的数字不跟小数点

            //绑定数据源          
            DataTable datTable = new DataTable();
            datTable = GetTable();
            mySeries.DataSource = datTable.DefaultView;// 获取到的数据
            mySeries.ArgumentDataMember = "收支类型";//绑定的文字信息（名称）
            mySeries.ValueDataMembers[0] = "收支金额";//绑定的值（数据）                      

            if (comboBoxEdit1.Text == "圆饼图" || comboBoxEdit1.Text == "3D圆饼图")
            {
                mySeries.PointOptions.ValueNumericOptions.Format = NumericFormat.Percent;//用百分比表示
                //for (int i = 0; i < datTable.Rows.Count; i++)
                //{
                //    string strType = datTable.Rows[i]["收支类型"].ToString();
                //    double dtJE = Convert.ToDouble(datTable.Rows[i]["收支金额"].ToString());
                //    mySeries.Points.Add(new SeriesPoint(strType, new double[] { dtJE}));
                //}   
            }
            else if (comboBoxEdit1.Text == "线图")
            {
                //mySeries.ArgumentScaleType = ScaleType.Qualitative;//设置Series样式  
                //for (int i = 0; i < datTable.Rows.Count; i++)
                //{
                //    string strType = datTable.Rows[i]["收支类型"].ToString();
                //    double dtJE = Convert.ToDouble(datTable.Rows[i]["收支金额"].ToString());
                //    mySeries.Points.Add(new SeriesPoint(strType, new double[] { dtJE }));
                //}                 
                //((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
            }

            //添加到统计图上
            this.chartControl1.Series.Clear();
            this.chartControl1.Series.Add(mySeries);

            if (comboBoxEdit1.Text == "线图" || comboBoxEdit1.Text == "点图")
            {
                this.chartControl1.Legend.Visible = true;
            }

            //图例设置
            SimpleDiagram3D diagram = new SimpleDiagram3D();
            diagram.RuntimeRotation = true;
            diagram.RuntimeScrolling = true;
            diagram.RuntimeZooming = true;
        }

        private DataTable GetTable()
        {
            DataTable datTable = new DataTable();
            DataColumn dc1 = new DataColumn("收支类型");
            DataColumn dc2 = new DataColumn("收支金额", typeof(double));

            datTable.Columns.Add(dc1);
            datTable.Columns.Add(dc2);

            //填充数据
            DataRow datRow0 = datTable.NewRow();//创建新行
            datRow0[0] = "总收入";
            datRow0[1] = mdtSumGSMoneyIN;
            datTable.Rows.Add(datRow0);

            DataRow datRow1 = datTable.NewRow();//创建新行
            datRow1[0] = "总支出";
            datRow1[1] = mdtSumGSMoneyOUT;
            datTable.Rows.Add(datRow1);

            DataRow datRow2 = datTable.NewRow();//创建新行
            datRow2[0] = "总盈利";
            datRow2[1] = mdtSumGSMoney;
            datTable.Rows.Add(datRow2);

            return datTable;
        }

        private Dictionary<string, double> chartPieDataDic = new Dictionary<string, double>();

        private void comboBoxEdit1_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadChartInfo();
        }




    }
}
