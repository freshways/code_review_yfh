﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DXTempLate.Class;

namespace DXTempLate.Controls
{
    public partial class XUControlMain : DevExpress.XtraEditors.XtraUserControl
    {
        public XUControlMain()
        {
            InitializeComponent();
        }

        private static XUControlMain _ItSelf;
        public static XUControlMain ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUControlMain();
                }
                return _ItSelf;
            }
        }
        //用于记录身份证读卡器是否初始化成功，初始化成功的情况下，才进行身份证号的读取
        bool bIDReaderInitResult = false;
        private void XUControlMain_Load(object sender, EventArgs e)
        {
            bIDReaderInitResult = IDReadHelper.InitReader(); 
        }

        private void windowsUIView1_QueryControl(object sender, DevExpress.XtraBars.Docking2010.Views.QueryControlEventArgs e)
        {
            e.Control = new Panel();
            if (e.Document == document体温)
            {
                //if (Program.BlueType != "体温")
                //{
                //    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                //    return;
                //}
                //e.Control = new uc体温(PublicSP);
            }
            //else if (e.Document == document血氧)
            //{
            //    if (Program.BlueType != "脉搏")
            //    {
            //        MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
            //        return;
            //    }
            //    e.Control = new uc血氧检测();
            //    e.Control.Dock = DockStyle.Fill;
            //}
            //else if (e.Document == document心电)
            //{
            //    if (Program.BlueType != "心电")
            //    {
            //        MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
            //        return;
            //    }
            //    e.Control = new uc心电PC80B();
            //    e.Control.Dock = DockStyle.Fill;
            //}
            else if (e.Document == document血压)
            {
                //if (Program.BlueType != "血压" && Program.BlueType != "血压RBP9804")
                //{
                //    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                //    return;
                //}
                //if (Program.BlueType == "血压")
                //    e.Control = new uc血压(PublicSP);
                //else if (Program.BlueType == "血压RBP9804")
                //    e.Control = new uc血压RBP9804();
                //e.Control.Dock = DockStyle.Fill;
            }
            //else if (e.Document == document尿常规)
            //{
            //    //if (Program.BlueType != "尿常规")
            //    //{
            //    //    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
            //    //    return;
            //    //}
            //    //e.Control = new uc尿常规(PublicSP);
            //    //e.Control.Dock = DockStyle.Fill;
            //}
            else if (e.Document == document视力)
            {
                //e.Control = new UC视力();
                //e.Control.Dock = DockStyle.Fill;
            }
            else if (e.Document == document身高体重)
            {
                e.Control = new Panel();
                //if (Program.BlueType != "身高体重")
                //{
                //    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                //    return;
                //}
                //e.Control = new uc身高体重(PublicSP);
                //e.Control.Dock = DockStyle.Fill;
            }
            else if (e.Document == document体质辨识)
            {
                //string id = Program.currentUser.ID;
                //if (!string.IsNullOrEmpty(id))
                //{
                //    if (id.Length < 6) { MessageBox.Show("此人身份证号格式错误，不能进行中医辨识体质！"); e.Control = new Panel(); return; }
                //    string birthYear = id.Substring(6, 4);
                //    int _year = DateTime.Now.Year;
                //    if (_year - Convert.ToInt32(birthYear) < 65)
                //    {
                //        if (MessageBox.Show("此人年龄未达65岁，是否进行中医辨识体质？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.No)
                //        {
                //            return;
                //        }
                //    }
                //}
                //e.Control = new uc中医体质辨识();
            }
            //else if (e.Document == document体检1)
            //{
            //    e.Control = new uc健康体检表1();
            //}
            //else if (e.Document == document体检2)
            //{
            //    e.Control = new uc健康体检表2();
            //}
            //else if (e.Document == document体检3)
            //{
            //    e.Control = new uc健康体检表3();
            //}
            //else if (e.Document == document随访表)
            //{
            //    string id = Program.currentUser.ID;
            //    if (!string.IsNullOrEmpty(id))
            //    {
            //        string birthYear = id.Substring(6, 4);
            //        int _year = DateTime.Now.Year;
            //        if (_year - Convert.ToInt32(birthYear) < 65)
            //        {
            //            MessageBox.Show("此人年龄未达65岁，不能进行自理评估！");
            //            return;
            //        }
            //    }
            //    e.Control = new uc老年人随访();
            //}
            else if (e.Document == document血糖)
            {
                //if (Program.BlueType != "血糖")
                //{
                //    MessageBox.Show("不支持此模块！请使用【" + Program.BlueType + "】模块！");
                //    return;
                //}
                //e.Control = new uc血糖(PublicSP);
            }
            else if (e.Document == document腰围)
            { 
                
            }
            else
                return;
        }

        private void btn读卡_Click(object sender, EventArgs e)
        {
            if (bIDReaderInitResult)
            {
                //int result = IDReadHelper.ReadIDInfoFromInstr();
                //if (result == 1)
                //{
                //    txt身份证号.Text = IDReadHelper.SFZH;
                //    txt姓名.Text = IDReadHelper.Name;
                //    //textEdit民族.Text = IDReadHelper.Minzu;
                //    txt居住地址.Text = IDReadHelper.Addr;
                //    //textEdit有效期限.Text = IDReadHelper.Qixian;
                //    txt性别.Text = IDReadHelper.Sex;
                //    txt出生日期.Text = IDReadHelper.Birth;
                //}
            }
            else
            {
                MessageBox.Show("初始化身份证读卡器出现问题，请重新插拔读卡器");
            }
        }
    }
}
