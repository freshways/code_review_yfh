﻿namespace DXTempLate.Controls
{
    partial class XUControl成人体检登记统计
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XUControl成人体检登记统计));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControlTop = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txt家庭地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txt性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleBtnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.txt结束日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt开始日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelUser = new DevExpress.XtraEditors.LabelControl();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.panelControlButtom = new DevExpress.XtraEditors.PanelControl();
            this.txtMoneyAll = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btn直接打印 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnExportExcel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTop)).BeginInit();
            this.panelControlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结束日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结束日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlButtom)).BeginInit();
            this.panelControlButtom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoneyAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Controls.Add(this.panelControlTop);
            this.groupControl1.Controls.Add(this.panelControlButtom);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(996, 482);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "体检人员管理-> 查询";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Font = new System.Drawing.Font("Tahoma", 19F);
            this.gridControl1.Location = new System.Drawing.Point(2, 111);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(992, 320);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.RowHeight = 50;
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // panelControlTop
            // 
            this.panelControlTop.Controls.Add(this.labelControl7);
            this.panelControlTop.Controls.Add(this.txt家庭地址);
            this.panelControlTop.Controls.Add(this.txt身份证号);
            this.panelControlTop.Controls.Add(this.labelControl5);
            this.panelControlTop.Controls.Add(this.labelControl6);
            this.panelControlTop.Controls.Add(this.labelControl4);
            this.panelControlTop.Controls.Add(this.txt性别);
            this.panelControlTop.Controls.Add(this.simpleBtnQuery);
            this.panelControlTop.Controls.Add(this.txt结束日期);
            this.panelControlTop.Controls.Add(this.labelControl2);
            this.panelControlTop.Controls.Add(this.labelControl1);
            this.panelControlTop.Controls.Add(this.txt开始日期);
            this.panelControlTop.Controls.Add(this.labelUser);
            this.panelControlTop.Controls.Add(this.txt姓名);
            this.panelControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControlTop.Location = new System.Drawing.Point(2, 22);
            this.panelControlTop.Name = "panelControlTop";
            this.panelControlTop.Size = new System.Drawing.Size(992, 89);
            this.panelControlTop.TabIndex = 4;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl7.Location = new System.Drawing.Point(894, 41);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(82, 19);
            this.labelControl7.TabIndex = 30;
            this.labelControl7.Text = "(模糊查询)";
            // 
            // txt家庭地址
            // 
            this.txt家庭地址.Location = new System.Drawing.Point(612, 46);
            this.txt家庭地址.Name = "txt家庭地址";
            this.txt家庭地址.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt家庭地址.Properties.Appearance.Options.UseFont = true;
            this.txt家庭地址.Size = new System.Drawing.Size(155, 30);
            this.txt家庭地址.TabIndex = 29;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(405, 46);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt身份证号.Properties.Appearance.Options.UseFont = true;
            this.txt身份证号.Size = new System.Drawing.Size(155, 30);
            this.txt身份证号.TabIndex = 29;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Location = new System.Drawing.Point(566, 49);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(38, 19);
            this.labelControl5.TabIndex = 28;
            this.labelControl5.Text = "地址:";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl6.Location = new System.Drawing.Point(296, 49);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(70, 19);
            this.labelControl6.TabIndex = 28;
            this.labelControl6.Text = "身份证号:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Location = new System.Drawing.Point(77, 49);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(38, 19);
            this.labelControl4.TabIndex = 25;
            this.labelControl4.Text = "性别:";
            // 
            // txt性别
            // 
            this.txt性别.EditValue = "请选择...";
            this.txt性别.Location = new System.Drawing.Point(125, 46);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt性别.Properties.Appearance.Options.UseFont = true;
            this.txt性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt性别.Properties.Items.AddRange(new object[] {
            "请选择...",
            "男",
            "女",
            "未知"});
            this.txt性别.Size = new System.Drawing.Size(155, 30);
            this.txt性别.TabIndex = 24;
            // 
            // simpleBtnQuery
            // 
            this.simpleBtnQuery.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.simpleBtnQuery.Appearance.Options.UseFont = true;
            this.simpleBtnQuery.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnQuery.Image")));
            this.simpleBtnQuery.Location = new System.Drawing.Point(783, 8);
            this.simpleBtnQuery.Name = "simpleBtnQuery";
            this.simpleBtnQuery.Size = new System.Drawing.Size(80, 52);
            this.simpleBtnQuery.TabIndex = 23;
            this.simpleBtnQuery.Text = "查询";
            this.simpleBtnQuery.Click += new System.EventHandler(this.simpleBtnQuery_Click);
            // 
            // txt结束日期
            // 
            this.txt结束日期.EditValue = null;
            this.txt结束日期.Location = new System.Drawing.Point(612, 7);
            this.txt结束日期.Name = "txt结束日期";
            this.txt结束日期.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt结束日期.Properties.Appearance.Options.UseFont = true;
            this.txt结束日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt结束日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt结束日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt结束日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt结束日期.Size = new System.Drawing.Size(155, 30);
            this.txt结束日期.TabIndex = 22;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Location = new System.Drawing.Point(566, 11);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(40, 19);
            this.labelControl2.TabIndex = 21;
            this.labelControl2.Text = "至(≤)";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(296, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(94, 19);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "体检日期:(≥)";
            // 
            // txt开始日期
            // 
            this.txt开始日期.EditValue = null;
            this.txt开始日期.Location = new System.Drawing.Point(405, 7);
            this.txt开始日期.Name = "txt开始日期";
            this.txt开始日期.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt开始日期.Properties.Appearance.Options.UseFont = true;
            this.txt开始日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt开始日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt开始日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt开始日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt开始日期.Size = new System.Drawing.Size(155, 30);
            this.txt开始日期.TabIndex = 19;
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelUser.Location = new System.Drawing.Point(77, 8);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(38, 19);
            this.labelUser.TabIndex = 3;
            this.labelUser.Text = "姓名:";
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(125, 7);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt姓名.Properties.Appearance.Options.UseFont = true;
            this.txt姓名.Size = new System.Drawing.Size(155, 30);
            this.txt姓名.TabIndex = 2;
            // 
            // panelControlButtom
            // 
            this.panelControlButtom.Controls.Add(this.txtMoneyAll);
            this.panelControlButtom.Controls.Add(this.labelControl3);
            this.panelControlButtom.Controls.Add(this.btn直接打印);
            this.panelControlButtom.Controls.Add(this.simpleBtnPrint);
            this.panelControlButtom.Controls.Add(this.simpleBtnExportExcel);
            this.panelControlButtom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControlButtom.Location = new System.Drawing.Point(2, 431);
            this.panelControlButtom.Name = "panelControlButtom";
            this.panelControlButtom.Size = new System.Drawing.Size(992, 49);
            this.panelControlButtom.TabIndex = 3;
            // 
            // txtMoneyAll
            // 
            this.txtMoneyAll.Location = new System.Drawing.Point(81, 2);
            this.txtMoneyAll.Name = "txtMoneyAll";
            this.txtMoneyAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtMoneyAll.Properties.Appearance.Options.UseFont = true;
            this.txtMoneyAll.Properties.ReadOnly = true;
            this.txtMoneyAll.Size = new System.Drawing.Size(66, 24);
            this.txtMoneyAll.TabIndex = 27;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl3.Location = new System.Drawing.Point(4, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(40, 19);
            this.labelControl3.TabIndex = 26;
            this.labelControl3.Text = "总数:";
            // 
            // btn直接打印
            // 
            this.btn直接打印.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn直接打印.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.btn直接打印.Appearance.Options.UseFont = true;
            this.btn直接打印.Image = ((System.Drawing.Image)(resources.GetObject("btn直接打印.Image")));
            this.btn直接打印.Location = new System.Drawing.Point(517, 6);
            this.btn直接打印.Name = "btn直接打印";
            this.btn直接打印.Size = new System.Drawing.Size(149, 41);
            this.btn直接打印.TabIndex = 25;
            this.btn直接打印.Text = "直接打印";
            this.btn直接打印.Click += new System.EventHandler(this.btn直接打印_Click);
            // 
            // simpleBtnPrint
            // 
            this.simpleBtnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.simpleBtnPrint.Appearance.Options.UseFont = true;
            this.simpleBtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnPrint.Image")));
            this.simpleBtnPrint.Location = new System.Drawing.Point(672, 6);
            this.simpleBtnPrint.Name = "simpleBtnPrint";
            this.simpleBtnPrint.Size = new System.Drawing.Size(149, 41);
            this.simpleBtnPrint.TabIndex = 25;
            this.simpleBtnPrint.Text = "打印预览";
            this.simpleBtnPrint.Click += new System.EventHandler(this.simpleBtnPrint_Click);
            // 
            // simpleBtnExportExcel
            // 
            this.simpleBtnExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnExportExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.simpleBtnExportExcel.Appearance.Options.UseFont = true;
            this.simpleBtnExportExcel.Enabled = false;
            this.simpleBtnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnExportExcel.Image")));
            this.simpleBtnExportExcel.Location = new System.Drawing.Point(827, 6);
            this.simpleBtnExportExcel.Name = "simpleBtnExportExcel";
            this.simpleBtnExportExcel.Size = new System.Drawing.Size(149, 41);
            this.simpleBtnExportExcel.TabIndex = 24;
            this.simpleBtnExportExcel.Text = "导出Excel";
            this.simpleBtnExportExcel.Click += new System.EventHandler(this.simpleBtnExportExcel_Click);
            // 
            // XUControl成人体检登记统计
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Name = "XUControl成人体检登记统计";
            this.Size = new System.Drawing.Size(996, 482);
            this.Load += new System.EventHandler(this.XUControl成人体检登记统计_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTop)).EndInit();
            this.panelControlTop.ResumeLayout(false);
            this.panelControlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结束日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结束日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlButtom)).EndInit();
            this.panelControlButtom.ResumeLayout(false);
            this.panelControlButtom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoneyAll.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControlTop;
        private DevExpress.XtraEditors.SimpleButton simpleBtnQuery;
        private DevExpress.XtraEditors.DateEdit txt结束日期;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit txt开始日期;
        private DevExpress.XtraEditors.LabelControl labelUser;
        private DevExpress.XtraEditors.PanelControl panelControlButtom;
        private DevExpress.XtraEditors.SimpleButton simpleBtnExportExcel;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit txt性别;
        private DevExpress.XtraEditors.SimpleButton simpleBtnPrint;
        private DevExpress.XtraEditors.TextEdit txtMoneyAll;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txt家庭地址;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.SimpleButton btn直接打印;
    }
}
