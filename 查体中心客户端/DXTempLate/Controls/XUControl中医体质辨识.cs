﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Speech.Synthesis; //用于生成响应的事件
using System.Speech;
using System.Speech.Recognition;
using DXTempLate.Class;
using System.IO;

namespace DXTempLate.Controls
{
    public partial class XUControl中医体质辨识 : UserControl
    {
        public static bool editEnd = false;//是否保存完成

        private static XUControl中医体质辨识 _ItSelf;
        public static XUControl中医体质辨识 ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUControl中医体质辨识();
                }
                else if (editEnd)
                {
                    _ItSelf.Dispose();
                    _ItSelf = new XUControl中医体质辨识();
                    editEnd = false;
                }

                return _ItSelf;
            }
        }

        #region 委托/事件

        public delegate void CloseEventHandler(); //返回结果

        public event CloseEventHandler CloseThis;

        #endregion

        #region Fields
        string _serverDateTime;
        tb_老年人中医药特征管理Info tab_体质辨识 = new tb_老年人中医药特征管理Info();

        int t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t7 = 0, t8 = 0, t9 = 0, t10 = 0, t11 = 0, t12 = 0, t13 = 0, t14 = 0, t15 = 0, t16 = 0, t17 = 0, t18 = 0, t19 = 0, t20 = 0, t21 = 0, t22 = 0, t23 = 0, t24 = 0, t25 = 0, t26 = 0, t27 = 0, t28 = 0, t29 = 0, t30 = 0, t31 = 0, t32 = 0, t33 = 0;


        string is气虚质, is阳虚质, is阴虚质, is痰湿质, is湿热质, is血瘀质, is气郁质, is特禀质, is平和质;
        int jf1 = 0, jf2 = 0, jf3 = 0, jf4 = 0, jf5 = 0, jf6 = 0, jf7 = 0, jf8 = 0, jf9 = 0;
        #endregion

        #region 问卷列表

        List<Question> list = new List<Question>()
        {
            new Question(){Index=0, QuestionName="(01)您精力充沛吗？（指精神头足，乐于做事）",Score = 0},
            new Question(){Index=1, QuestionName="(02)您容易疲乏吗？（指体力如何，是否稍微活动一下或做一点家务劳动就感到累）",Score = 0},
            new Question(){Index=2, QuestionName="(03)您容易气短，呼吸短促，接不上气吗？",Score = 0},
            new Question(){Index=3, QuestionName="(04)您说话声音低弱无力吗?（指说话没有力气）",Score = 0},
            new Question(){Index=4, QuestionName="(05)您感到闷闷不乐、情绪低沉吗?（指心情不愉快，情绪低落）",Score = 0},
            new Question(){Index=5, QuestionName="(06)您容易精神紧张、焦虑不安吗?（指遇事是否心情紧张）",Score = 0},
            new Question(){Index=6, QuestionName="(07)您因为生活状态改变而感到孤独、失落吗？",Score = 0},
            new Question(){Index=7, QuestionName="(08)您容易感到害怕或受到惊吓吗?",Score = 0},
            new Question("BMI<24","24<=BMI<25","25<=BMI<26","26<=BMI<28","BMI>=28"){Index=8, QuestionName="(09)您感到身体超重不轻松吗?(感觉身体沉重)\r\n[BMI指数=体重（kg）/身高2（m）]",Score = 0},
            new Question(){Index=9, QuestionName="(10)您眼睛干涩吗?",Score = 0},
            new Question(){Index=10, QuestionName="(11)您手脚发凉吗?（不包含因周围温度低或穿的少导致的手脚发冷）",Score = 0},
            new Question(){Index=11, QuestionName="(12)您胃脘部、背部或腰膝部怕冷吗？（指上腹部、背部、腰部或膝关节等，有一处或多处怕冷）",Score = 0},
            new Question(){Index=12, QuestionName="(13)您比一般人耐受不了寒冷吗？（指比别人容易害怕冬天或是夏天的冷空调、电扇等）",Score = 0},
            new Question("(一年＜2次)","(一年感冒2-4次)","(一年感冒5-6次)","(一年8次以上)","(几乎每月都感冒)"){Index=13, QuestionName="(14)您容易患感冒吗?（指每年感冒的次数）",Score = 0},
            new Question(){Index=14, QuestionName="(15)您没有感冒时也会鼻塞、流鼻涕吗?",Score = 0},
            new Question(){Index=15, QuestionName="(16)您有口粘口腻，或睡眠打鼾吗？",Score = 0},
            new Question("(从来没有)","(一年1、2次)","(一年3、4次)","(一年5、6次)","(每次遇到上述原因都过敏)"){Index=16, QuestionName="(17)您容易过敏(对药物、食物、气味、花粉或在季节交替、气候变化时)吗?",Score = 0},
            new Question(){Index=17, QuestionName="(18)您的皮肤容易起荨麻疹吗? (包括风团、风疹块、风疙瘩)",Score = 0},
            new Question(){Index=18, QuestionName="(19)您的皮肤在不知不觉中会出现青紫瘀斑、皮下出血吗?（指皮肤在没有外伤的情况下出现青一块紫一块的情况）",Score = 0},
            new Question(){Index=19, QuestionName="(20)您的皮肤一抓就红，并出现抓痕吗?（指被指甲或钝物划过后皮肤的反应）",Score = 0},
            new Question(){Index=20, QuestionName="(21)您皮肤或口唇干吗?",Score = 0},
            new Question(){Index=21, QuestionName="(22)您有肢体麻木或固定部位疼痛的感觉吗？",Score = 0},
            new Question(){Index=22, QuestionName="(23)您面部或鼻部有油腻感或者油亮发光吗?（指脸上或鼻子）",Score = 0},
            new Question(){Index=23, QuestionName="(24)您面色或目眶晦黯，或出现褐色斑块/斑点吗?",Score = 0},
            new Question(){Index=24, QuestionName="(25)您有皮肤湿疹、疮疖吗？",Score = 0},
            new Question(){Index=25, QuestionName="(26)您感到口干咽燥、总想喝水吗？",Score = 0},
            new Question(){Index=26, QuestionName="(27)您感到口苦或嘴里有异味吗?（指口苦或口臭）",Score = 0},
            new Question("（腹围<80cm，相当于2.4尺）","(腹围80-85cm，2.4-2.55尺)","(腹围86-90cm，2.56-2.7尺)","(腹围91-105cm，2.71-3.15尺)","（腹围>105cm或3.15尺）"){Index=27, QuestionName="(28)您腹部肥大吗?（指腹部脂肪肥厚）",Score = 0},
            new Question(){Index=28, QuestionName="(29)您吃(喝)凉的东西会感到不舒服或者怕吃(喝)凉的东西吗？（指不喜欢吃凉的食物，或吃了凉的食物后会不舒服）",Score = 0},
            new Question(){Index=29, QuestionName="(30)您有大便黏滞不爽、解不尽的感觉吗?(大便容易粘在马桶或便坑壁上)",Score = 0},
            new Question(){Index=30, QuestionName="(31)您容易大便干燥吗?",Score = 0},
            new Question(){Index=31, QuestionName="(32)您舌苔厚腻或有舌苔厚厚的感觉吗?（如果自我感觉不清楚可由调查员观察后填写）",Score = 0},
            new Question(){Index=32, QuestionName="(33)您舌下静脉瘀紫或增粗吗？（可由调查员辅助观察后填写）",Score = 0}
        };

        #endregion

        //语音
        SpeechSynthesizer speak = new SpeechSynthesizer();

        public XUControl中医体质辨识()
        {
            InitializeComponent();
        }

        private void frm中医体质辨识_Load(object sender, EventArgs e)
        {
            this.lblQuestion.Text = list[0].QuestionName;
            SpeakdelegateMethod(list[0].QuestionName.Substring(4));
            this.lblQuestion.Tag = list[0].Index;
            SetRadioText(list[0]);
            SetBtnEnable(list[0].Index);
            this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dte填表日期.EditValue = DateTime.Now.ToString("yyyy-MM-dd");
            //this.txt医生签名.Text = Program._bDoctor;
        }

        private void SetRadioText(Question question)
        {
            if (question == null) return;
            for (int i = 0; i < this.radioGroup1.Properties.Items.Count; i++)
            {
                this.radioGroup1.Properties.Items[0].Description = question.Option1;
                this.radioGroup1.Properties.Items[1].Description = question.Option2;
                this.radioGroup1.Properties.Items[2].Description = question.Option3;
                this.radioGroup1.Properties.Items[3].Description = question.Option4;
                this.radioGroup1.Properties.Items[4].Description = question.Option5;
            }
        }
        private void 计算体质()
        {
            if (t1 != 0 && t2 != 0 && t3 != 0 && t4 != 0 && t5 != 0 && t6 != 0 && t7 != 0 && t8 != 0 && t9 != 0 && t10 != 0 && t11 != 0 && t12 != 0 && t13 != 0 && t14 != 0 && t15 != 0 && t16 != 0 && t17 != 0 && t18 != 0 && t19 != 0 && t20 != 0 && t21 != 0 && t22 != 0 && t23 != 0 && t24 != 0 && t25 != 0 && t26 != 0 && t27 != 0 && t28 != 0 && t29 != 0 && t30 != 0 && t31 != 0 && t32 != 0 && t33 != 0)
            {
                //算每种体质的积分
                jf1 = t2 + t3 + t4 + t14;
                jf2 = t11 + t12 + t13 + t29;
                jf3 = t10 + t21 + t26 + t31;
                jf4 = t9 + t16 + t28 + t32;
                jf5 = t23 + t25 + t27 + t30;
                jf6 = t19 + t22 + t24 + t33;
                jf7 = t5 + t6 + t7 + t8;
                jf8 = t15 + t17 + t18 + t20;
                jf9 = t1 + (6 - t2) + (6 - t4) + (6 - t5) + (6 - t13);
                //赋积分
                this.textBox1.Text = jf1.ToString();
                this.textBox2.Text = jf2.ToString();
                this.textBox3.Text = jf3.ToString();
                this.textBox4.Text = jf4.ToString();
                this.textBox5.Text = jf5.ToString();
                this.textBox6.Text = jf6.ToString();
                this.textBox7.Text = jf7.ToString();
                this.textBox8.Text = jf8.ToString();
                this.textBox9.Text = jf9.ToString();

                //根据积分赋体质辨识
                //根据积分赋体质辨识
                if (jf1 >= 9 && jf1 <= 10)
                {
                    is气虚质 = "2";
                    this.labelControl1.Text = "倾向是";
                    this.sp1.ForeColor = System.Drawing.Color.Blue;
                    tab_体质辨识.气虚质指导 = "1,2,3,4,5";
                }
                else if (jf1 >= 11)
                {
                    is气虚质 = "1";
                    this.labelControl1.Text = "是";
                    this.sp1.ForeColor = System.Drawing.Color.Red;
                    tab_体质辨识.气虚质指导 = "1,2,3,4,5";
                }
                else
                {
                    is气虚质 = "";
                    this.labelControl1.Text = "";
                    this.sp1.ForeColor = System.Drawing.Color.Black;
                    tab_体质辨识.气虚质指导 = "";
                }
                if (jf2 >= 9 && jf2 <= 10)
                {
                    is阳虚质 = "2";
                    this.labelControl2.Text = "倾向是";
                    this.sp2.ForeColor = System.Drawing.Color.Blue;
                    tab_体质辨识.阳虚质指导 = "1,2,3,4,5";
                }
                else if (jf2 >= 11)
                {
                    is阳虚质 = "1";
                    this.labelControl2.Text = "是";
                    this.sp2.ForeColor = System.Drawing.Color.Red;
                    tab_体质辨识.阳虚质指导 = "1,2,3,4,5";
                }
                else
                {
                    is阳虚质 = "";
                    this.labelControl2.Text = "";
                    this.sp2.ForeColor = System.Drawing.Color.Black;
                    tab_体质辨识.阳虚质指导 = "";
                }
                if (jf3 >= 9 && jf3 <= 10)
                {
                    is阴虚质 = "2";
                    this.labelControl3.Text = "倾向是";
                    this.sp3.ForeColor = System.Drawing.Color.Blue;
                    tab_体质辨识.阴虚质指导 = "1,2,3,4,5";
                }
                else if (jf3 >= 11)
                {
                    is阴虚质 = "1";
                    this.labelControl3.Text = "是";
                    this.sp3.ForeColor = System.Drawing.Color.Red;
                    tab_体质辨识.阴虚质指导 = "1,2,3,4,5";
                }
                else
                {
                    is阴虚质 = "";
                    this.labelControl3.Text = "";
                    this.sp3.ForeColor = System.Drawing.Color.Black;
                    tab_体质辨识.阴虚质指导 = "";
                }
                if (jf4 >= 9 && jf4 <= 10)
                {
                    is痰湿质 = "2";
                    this.labelControl4.Text = "倾向是";
                    this.sp4.ForeColor = System.Drawing.Color.Blue;
                    tab_体质辨识.痰湿质指导 = "1,2,3,4,5";
                }
                else if (jf4 >= 11)
                {
                    is痰湿质 = "1";
                    this.labelControl4.Text = "是";
                    this.sp4.ForeColor = System.Drawing.Color.Red;
                    tab_体质辨识.痰湿质指导 = "1,2,3,4,5";
                }
                else
                {
                    is痰湿质 = "";
                    this.labelControl4.Text = "";
                    this.sp4.ForeColor = System.Drawing.Color.Black;
                    tab_体质辨识.痰湿质指导 = "";
                }
                if (jf5 >= 9 && jf5 <= 10)
                {
                    is湿热质 = "2";
                    this.labelControl5.Text = "倾向是";
                    this.sp5.ForeColor = System.Drawing.Color.Blue;
                    tab_体质辨识.湿热质指导 = "1,2,3,4,5";
                }
                else if (jf5 >= 11)
                {
                    is湿热质 = "1";
                    this.labelControl5.Text = "是";
                    this.sp5.ForeColor = System.Drawing.Color.Red;
                    tab_体质辨识.湿热质指导 = "1,2,3,4,5";
                }
                else
                {
                    is湿热质 = "";
                    this.labelControl5.Text = "";
                    this.sp5.ForeColor = System.Drawing.Color.Black;
                    tab_体质辨识.湿热质指导 = "";
                }
                if (jf6 >= 9 && jf6 <= 10)
                {
                    is血瘀质 = "2";
                    this.labelControl6.Text = "倾向是";
                    this.sp6.ForeColor = System.Drawing.Color.Blue;
                    tab_体质辨识.血瘀质指导 = "1,2,3,4,5";
                }
                else if (jf6 >= 11)
                {
                    is血瘀质 = "1";
                    this.labelControl6.Text = "是";
                    this.sp6.ForeColor = System.Drawing.Color.Red;
                    tab_体质辨识.血瘀质指导 = "1,2,3,4,5";
                }
                else
                {
                    is血瘀质 = "";
                    this.labelControl6.Text = "";
                    this.sp6.ForeColor = System.Drawing.Color.Black;
                    tab_体质辨识.血瘀质指导 = "";
                }
                if (jf7 >= 9 && jf7 <= 10)
                {
                    is气郁质 = "2";
                    this.labelControl7.Text = "倾向是";
                    this.sp7.ForeColor = System.Drawing.Color.Blue;
                    tab_体质辨识.气郁质指导 = "1,2,3,4,5";
                }
                else if (jf7 >= 11)
                {
                    is气郁质 = "1";
                    this.labelControl7.Text = "是";
                    this.sp7.ForeColor = System.Drawing.Color.Red;
                    tab_体质辨识.气郁质指导 = "1,2,3,4,5";
                }
                else
                {
                    is气郁质 = "";
                    this.labelControl7.Text = "";
                    this.sp7.ForeColor = System.Drawing.Color.Black;
                    tab_体质辨识.气郁质指导 = "";
                }
                if (jf8 >= 9 && jf8 <= 10)
                {
                    is特禀质 = "2";
                    this.labelControl8.Text = "倾向是";
                    this.sp8.ForeColor = System.Drawing.Color.Blue;
                    tab_体质辨识.特禀质指导 = "1,2,3,4,5";
                }
                else if (jf8 >= 11)
                {
                    is特禀质 = "1";
                    this.labelControl8.Text = "是";
                    this.sp8.ForeColor = System.Drawing.Color.Red;
                    tab_体质辨识.特禀质指导 = "1,2,3,4,5";
                }
                else
                {
                    is特禀质 = "";
                    this.labelControl8.Text = "";
                    this.sp8.ForeColor = System.Drawing.Color.Black;
                    tab_体质辨识.特禀质指导 = "";
                }
                if (jf9 >= 17)
                {
                    if (jf1 < 8 && jf2 < 8 && jf3 < 8 && jf4 < 8 && jf5 < 8 && jf6 < 8 && jf7 < 8 && jf8 < 8)
                    {
                        is平和质 = "1";
                        this.labelControl9.Text = "是";
                        this.sp9.ForeColor = System.Drawing.Color.Red;
                        tab_体质辨识.平和质指导 = "1,2,3,4,5";
                    }
                    else if (jf1 < 10 && jf2 < 10 && jf3 < 10 && jf4 < 10 && jf5 < 10 && jf6 < 10 && jf7 < 10 && jf8 < 10)
                    {
                        is平和质 = "2";
                        this.labelControl9.Text = "基本是";
                        this.sp9.ForeColor = System.Drawing.Color.Blue;
                        tab_体质辨识.平和质指导 = "1,2,3,4,5";
                    }
                    else
                    {
                        is平和质 = "";
                        this.labelControl9.Text = "";
                        this.sp9.ForeColor = System.Drawing.Color.Black;
                        tab_体质辨识.平和质指导 = "";
                    }
                }
                else
                {
                    is平和质 = "";
                    this.labelControl9.Text = "";
                    this.sp9.ForeColor = System.Drawing.Color.Black;
                }
            }
            else
            {
                Clear();
            }
        }
        private void Clear()
        {
            this.textBox1.Text = "";
            this.textBox2.Text = "";
            this.textBox3.Text = "";
            this.textBox4.Text = "";
            this.textBox5.Text = "";
            this.textBox6.Text = "";
            this.textBox7.Text = "";
            this.textBox8.Text = "";
            this.textBox9.Text = "";

            this.labelControl1.Text = "";
            this.labelControl2.Text = "";
            this.labelControl3.Text = "";
            this.labelControl4.Text = "";
            this.labelControl5.Text = "";
            this.labelControl6.Text = "";
            this.labelControl7.Text = "";
            this.labelControl8.Text = "";
            this.labelControl9.Text = "";

            this.sp1.ForeColor = System.Drawing.Color.Black;
            this.sp2.ForeColor = System.Drawing.Color.Black;
            this.sp3.ForeColor = System.Drawing.Color.Black;
            this.sp4.ForeColor = System.Drawing.Color.Black;
            this.sp5.ForeColor = System.Drawing.Color.Black;
            this.sp6.ForeColor = System.Drawing.Color.Black;
            this.sp7.ForeColor = System.Drawing.Color.Black;
            this.sp8.ForeColor = System.Drawing.Color.Black;
            this.sp9.ForeColor = System.Drawing.Color.Black;
        }
        private void dockPanel1_Click(object sender, EventArgs e)
        {

        }
        private void btn上一项_Click(object sender, EventArgs e)
        {
            int index = (int)this.lblQuestion.Tag;
            //SetBtnEnable(index - 1);
            Question lastQues = list.Find(x => x.Index == index - 1);
            if (lastQues == null) return;
            SetBtnEnable(lastQues);
            SetRadioText(lastQues);
            SetRadioValue(lastQues.Score);
            if (index != 0)
                SetLblQuestion(index - 1);

        }
        private void SetLblQuestion(Question question)
        {
            if (question == null) return;
            this.lblQuestion.Text = question.QuestionName;
            this.lblQuestion.Tag = question.Index;
            this.btn上一项.Enabled = false;
        }
        private void SetLblQuestion(int index)
        {
            if (index == 33) return;
            Question question = list.Find(x => x.Index == index);
            this.lblQuestion.Text = question.QuestionName;
            SpeakdelegateMethod(question.QuestionName.Substring(4));
            this.lblQuestion.Tag = question.Index;
            //this.btn上一项.Enabled = false;
        }
        private void SetBtnEnable(int index)
        {
            if (index == 0)
            {
                this.btn上一项.Enabled = false;
                this.btn下一项.Enabled = false;
            }
            else if (index == 33)
            {
                this.btn上一项.Enabled = true;
                this.btn下一项.Enabled = false;
            }
            else
            {
                this.btn上一项.Enabled = true;
                //this.btn下一项.Enabled = true;
            }
        }
        private void btn下一项_Click(object sender, EventArgs e)
        {
            int index = (int)this.lblQuestion.Tag;
            Question nextQuestion = list.Find(x => x.Index == index + 1);
            if (nextQuestion == null) return;
            SetRadioText(nextQuestion);
            SetBtnEnable(nextQuestion);
            SetRadioValue(nextQuestion.Score);
            if (index != 32)
                SetLblQuestion(index + 1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="score"></param>
        private void SetRadioValue(int score)
        {
            this.radioGroup1.SelectedIndexChanged -= this.radioGroup1_SelectedIndexChanged;
            this.radioGroup1.EditValue = score;
            this.radioGroup1.SelectedIndexChanged += this.radioGroup1_SelectedIndexChanged;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (fun必填项检查())
            {
                #region tb_老年人中医药特征管理

                tab_体质辨识.身份证号 = this.txt身份证号.Text.Trim();
                tab_体质辨识.姓名 = this.txt姓名.Text.Trim();
                tab_体质辨识.性别 = this.txt性别.Text.Trim();
                tab_体质辨识.出生日期 = this.txt出生日期.Text.Trim();
                tab_体质辨识.居住地址 = this.txt居住地址.Text.Trim();
                //tab_体质辨识.个人档案编号 = this.txt身份证号.Text.Trim();
                tab_体质辨识.气虚质辨识 = is气虚质;
                tab_体质辨识.气虚质得分 = jf1;

                tab_体质辨识.阳虚质辨识 = is阳虚质;
                tab_体质辨识.阳虚质得分 = jf2;

                tab_体质辨识.阴虚质辨识 = is阴虚质;
                tab_体质辨识.阴虚质得分 = jf3;

                tab_体质辨识.痰湿质辨识 = is痰湿质;
                tab_体质辨识.痰湿质得分 = jf4;

                tab_体质辨识.湿热质辨识 = is湿热质;
                tab_体质辨识.湿热质得分 = jf5;

                tab_体质辨识.血瘀质辨识 = is血瘀质;
                tab_体质辨识.血瘀质得分 = jf6;

                tab_体质辨识.气郁质辨识 = is气郁质;
                tab_体质辨识.气郁质得分 = jf7;

                tab_体质辨识.特禀质辨识 = is特禀质;
                tab_体质辨识.特禀质得分 = jf8;

                tab_体质辨识.平和质辨识 = is平和质;
                tab_体质辨识.平和质得分 = jf9;

                tab_体质辨识.特征1 = t1;
                tab_体质辨识.特征2 = t2;
                tab_体质辨识.特征3 = t3;
                tab_体质辨识.特征4 = t4;
                tab_体质辨识.特征5 = t5;
                tab_体质辨识.特征6 = t6;
                tab_体质辨识.特征7 = t7;
                tab_体质辨识.特征8 = t8;
                tab_体质辨识.特征9 = t9;
                tab_体质辨识.特征10 = t10;
                tab_体质辨识.特征11 = t11;
                tab_体质辨识.特征12 = t12;
                tab_体质辨识.特征13 = t13;
                tab_体质辨识.特征14 = t14;
                tab_体质辨识.特征15 = t15;
                tab_体质辨识.特征16 = t16;
                tab_体质辨识.特征17 = t17;
                tab_体质辨识.特征18 = t18;
                tab_体质辨识.特征19 = t19;
                tab_体质辨识.特征20 = t20;
                tab_体质辨识.特征21 = t21;
                tab_体质辨识.特征22 = t22;
                tab_体质辨识.特征23 = t23;
                tab_体质辨识.特征24 = t24;
                tab_体质辨识.特征25 = t25;
                tab_体质辨识.特征26 = t26;
                tab_体质辨识.特征27 = t27;
                tab_体质辨识.特征28 = t28;
                tab_体质辨识.特征29 = t29;
                tab_体质辨识.特征30 = t30;
                tab_体质辨识.特征31 = t31;
                tab_体质辨识.特征32 = t32;
                tab_体质辨识.特征33 = t33;

                //    //TODO:保存修改人等
                //    tbNew.创建时间 = _serverDateTime;
                //    tbNew.修改时间 = _serverDateTime;
                tab_体质辨识.发生时间 = this.dte填表日期.Text.Trim();
                tab_体质辨识.医生签名 = this.txt医生签名.Text.Trim();

                #endregion
                string sqlWhere = " 身份证号 = '" + tab_体质辨识.身份证号 + "' and substr(创建时间,0,10) = substr('" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +"',0,10)";
                List<tb_老年人中医药特征管理Info> tbExists = tb_老年人中医药特征管理DAL.Gettb_老年人中医药特征管理InfoList(sqlWhere);
                bool result = false;
                if (tbExists.Count == 1)
                {
                    result = tb_老年人中医药特征管理DAL.Updatetb_老年人中医药特征管理Info(tab_体质辨识);
                }
                else
                {
                    result = tb_老年人中医药特征管理DAL.Addtb_老年人中医药特征管理(tab_体质辨识);
                }
                //bool result = Save(currentUser);

                if (result)
                {
                    DBManager.Beep();
                    MessageBox.Show("保存数据成功！");
                    editEnd = true;
                    this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
                    //this.Close();
                    if (CloseThis != null)
                        CloseThis();
                }
            }
        }

        private void btn打印_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["平和质"] = this.labelControl9.Text;
            dict["特禀质"] = this.labelControl8.Text;
            dict["气郁质"] = this.labelControl7.Text;
            dict["血瘀质"] = this.labelControl6.Text;
            dict["湿热质"] = this.labelControl5.Text;
            dict["痰湿质"] = this.labelControl4.Text;
            dict["阴虚质"] = this.labelControl3.Text;
            dict["阳虚质"] = this.labelControl2.Text;
            dict["气虚质"] = this.labelControl1.Text;

            tab_体质辨识.身份证号 = this.txt身份证号.Text.Trim();
            tab_体质辨识.姓名 = this.txt姓名.Text.Trim();
            tab_体质辨识.性别 = this.txt性别.Text.Trim();
            tab_体质辨识.出生日期 = this.txt出生日期.Text.Trim();
            tab_体质辨识.居住地址 = this.txt居住地址.Text.Trim();
            //tab_体质辨识.个人档案编号 = Program.currentUser.DocNo;
            tab_体质辨识.气虚质辨识 = is气虚质;
            tab_体质辨识.气虚质得分 = jf1;

            tab_体质辨识.阳虚质辨识 = is阳虚质;
            tab_体质辨识.阳虚质得分 = jf2;

            tab_体质辨识.阴虚质辨识 = is阴虚质;
            tab_体质辨识.阴虚质得分 = jf3;

            tab_体质辨识.痰湿质辨识 = is痰湿质;
            tab_体质辨识.痰湿质得分 = jf4;

            tab_体质辨识.湿热质辨识 = is湿热质;
            tab_体质辨识.湿热质得分 = jf5;

            tab_体质辨识.血瘀质辨识 = is血瘀质;
            tab_体质辨识.血瘀质得分 = jf6;

            tab_体质辨识.气郁质辨识 = is气郁质;
            tab_体质辨识.气郁质得分 = jf7;

            tab_体质辨识.特禀质辨识 = is特禀质;
            tab_体质辨识.特禀质得分 = jf8;

            tab_体质辨识.平和质辨识 = is平和质;
            tab_体质辨识.平和质得分 = jf9;

            tab_体质辨识.特征1 = t1;
            tab_体质辨识.特征2 = t2;
            tab_体质辨识.特征3 = t3;
            tab_体质辨识.特征4 = t4;
            tab_体质辨识.特征5 = t5;
            tab_体质辨识.特征6 = t6;
            tab_体质辨识.特征7 = t7;
            tab_体质辨识.特征8 = t8;
            tab_体质辨识.特征9 = t9;
            tab_体质辨识.特征10 = t10;
            tab_体质辨识.特征11 = t11;
            tab_体质辨识.特征12 = t12;
            tab_体质辨识.特征13 = t13;
            tab_体质辨识.特征14 = t14;
            tab_体质辨识.特征15 = t15;
            tab_体质辨识.特征16 = t16;
            tab_体质辨识.特征17 = t17;
            tab_体质辨识.特征18 = t18;
            tab_体质辨识.特征19 = t19;
            tab_体质辨识.特征20 = t20;
            tab_体质辨识.特征21 = t21;
            tab_体质辨识.特征22 = t22;
            tab_体质辨识.特征23 = t23;
            tab_体质辨识.特征24 = t24;
            tab_体质辨识.特征25 = t25;
            tab_体质辨识.特征26 = t26;
            tab_体质辨识.特征27 = t27;
            tab_体质辨识.特征28 = t28;
            tab_体质辨识.特征29 = t29;
            tab_体质辨识.特征30 = t30;
            tab_体质辨识.特征31 = t31;
            tab_体质辨识.特征32 = t32;
            tab_体质辨识.特征33 = t33;

            //    //TODO:保存修改人等
            //    //row[tb_老年人中医药特征管理.创建机构] = Loginer.CurrentUser.所属机构;
            //    //row[tb_老年人中医药特征管理.创建人] = Loginer.CurrentUser.用户编码;
            //    //row[tb_老年人中医药特征管理.修改人] = Loginer.CurrentUser.用户编码;
            //    //row[tb_老年人中医药特征管理.所属机构] = Loginer.CurrentUser.所属机构;
            //    tbNew.创建时间 = _serverDateTime;
            //    tbNew.修改时间 = _serverDateTime;
            tab_体质辨识.发生时间 = this.dte填表日期.Text.Trim();
            tab_体质辨识.医生签名 = this.txt医生签名.Text.Trim();

            report体质辨识结果 report = new report体质辨识结果(tab_体质辨识, dict);
            DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(report);
            tool.ShowPreviewDialog();

        }
        private bool fun必填项检查()
        {
            if (string.IsNullOrEmpty(this.dte填表日期.Text.Trim()))
            {
                MessageBox.Show("填表日期必填项，请确认！");
                this.txt医生签名.Focus();
                return false;
            }
            //if (string.IsNullOrEmpty(this.txt医生签名.Text.Trim()))
            //{
            //    MessageBox.Show("医生签名是必填项，请确认！");
            //    this.txt医生签名.Focus();
            //    return false;
            //}
            return true;
        }
        /// <summary>
        /// 指导按钮功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn指导_Click(object sender, EventArgs e)
        {
            SimpleButton btn = (SimpleButton)sender;
            if (btn == null) return;
            SetBtn指导(btn);
            int index = Convert.ToInt32(btn.Tag);
            EnableFlowCtrl(true);



            int directIndex = Convert.ToInt32(this.flow指导.Tag);
            this.flow指导.Tag = index;
            //flow的tag不等于btn的tag时，证明是点击了另外体质的按钮，需要先进行保存
            if (directIndex != 0 && directIndex != index)
            {
                string zhidao = ControlsHelper.GetFlowLayoutResult(flow指导).ToString();
                switch (directIndex)
                {
                    case 1:
                        tab_体质辨识.气虚质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.气虚质指导 = zhidao;
                        break;
                    case 2:
                        tab_体质辨识.阳虚质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.阳虚质指导 = zhidao;
                        break;
                    case 3:
                        tab_体质辨识.阴虚质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.阴虚质指导 = zhidao;
                        break;
                    case 4:
                        tab_体质辨识.痰湿质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.痰湿质指导 = zhidao;
                        break;
                    case 5:
                        tab_体质辨识.湿热质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.湿热质指导 = zhidao;
                        break;
                    case 6:
                        tab_体质辨识.血瘀质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.血瘀质指导 = zhidao;
                        break;
                    case 7:
                        tab_体质辨识.气郁质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.气郁质指导 = zhidao;
                        break;
                    case 8:
                        tab_体质辨识.特禀质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.特禀质指导 = zhidao;
                        break;
                    case 9:
                        tab_体质辨识.平和质其他 = this.txt指导.Text.Trim();
                        tab_体质辨识.平和质指导 = zhidao;
                        break;
                    default:
                        break;
                }
            }
            SetFlowResult(index);

            //ClearFlowResult();//情况

        }

        private void SetBtn指导(SimpleButton btn)
        {
            int tag = Convert.ToInt32(btn.Tag);
            for (int i = 0; i < this.layoutControl1.Controls.Count; i++)
            {
                if (this.layoutControl1.Controls[i].GetType() == typeof(SimpleButton))
                {
                    SimpleButton simpleButton = (SimpleButton)this.layoutControl1.Controls[i];
                    if (simpleButton == null) continue;
                    if (simpleButton.Tag == null) continue;
                    int _tag = Convert.ToInt32(simpleButton.Tag);
                    if (_tag != tag)
                    {
                        simpleButton.ForeColor = System.Drawing.Color.Black;
                        simpleButton.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
                    }
                }
            }
            btn.Appearance.ForeColor = System.Drawing.Color.DarkRed;
            btn.Font = new System.Drawing.Font("华文中宋", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        }
        private void EnableFlowCtrl(bool isTrue)
        {
            this.checkEdit1.Enabled = this.checkEdit2.Enabled = this.checkEdit3.Enabled = this.checkEdit4.Enabled = this.checkEdit5.Enabled = this.checkEdit6.Enabled = this.txt指导.Enabled = isTrue;
        }
        /// <summary>
        /// 根据index设置flow的值
        /// </summary>
        /// <param name="index"></param>
        private void SetFlowResult(int index)
        {
            switch (index)
            {
                case 1:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.气虚质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.气虚质其他;
                    break;
                case 2:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.阳虚质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.阳虚质其他;
                    break;
                case 3:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.阴虚质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.阴虚质其他;
                    break;
                case 4:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.痰湿质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.痰湿质其他;
                    break;
                case 5:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.湿热质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.湿热质其他;
                    break;
                case 6:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.血瘀质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.血瘀质其他;
                    break;
                case 7:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.气郁质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.气郁质其他;
                    break;
                case 8:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.特禀质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.特禀质其他;
                    break;
                case 9:
                    ControlsHelper.SetFlowLayoutResult(tab_体质辨识.平和质指导, flow指导);
                    this.txt指导.Text = tab_体质辨识.平和质其他;
                    break;
                default:
                    break;
            }

        }
        private void ClearFlowResult()
        {
            this.checkEdit1.Checked = this.checkEdit2.Checked = this.checkEdit3.Checked = this.checkEdit4.Checked = this.checkEdit5.Checked = this.checkEdit6.Checked = false;
            this.txt指导.Text = "";
        }
        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RadioButton radio = (RadioButton)sender;
            //if (radio == null) return;
            int questionIndex = (int)this.lblQuestion.Tag;
            int tag = (int)this.radioGroup1.EditValue;
            Question currentQues = list.Find(x => x.Index == questionIndex);
            currentQues.Score = tag;
            #region 变量赋值
            switch (questionIndex)
            {
                case 0:
                    this.t1 = tag;
                    break;
                case 1:
                    this.t2 = tag;
                    break;
                case 2:
                    this.t3 = tag;
                    break;
                case 3:
                    this.t4 = tag;
                    break;
                case 4:
                    this.t5 = tag;
                    break;
                case 5:
                    this.t6 = tag;
                    break;
                case 6:
                    this.t7 = tag; break;
                case 7:
                    this.t8 = tag; break;
                case 8:
                    this.t9 = tag; break;
                case 9:
                    this.t10 = tag; break;
                case 10:
                    this.t11 = tag; break;
                case 11:
                    this.t12 = tag; break;
                case 12:
                    this.t13 = tag; break;
                case 13:
                    this.t14 = tag; break;
                case 14:
                    this.t15 = tag; break;
                case 15:
                    this.t16 = tag; break;
                case 16:
                    this.t17 = tag; break;
                case 17:
                    this.t18 = tag; break;
                case 18:
                    this.t19 = tag; break;
                case 19:
                    this.t20 = tag; break;
                case 20:
                    this.t21 = tag; break;
                case 21:
                    this.t22 = tag; break;
                case 22:
                    this.t23 = tag; break;
                case 23:
                    this.t24 = tag; break;
                case 24:
                    this.t25 = tag; break;
                case 25:
                    this.t26 = tag; break;
                case 26:
                    this.t27 = tag; break;
                case 27:
                    this.t28 = tag; break;
                case 28:
                    this.t29 = tag; break;
                case 29:
                    this.t30 = tag; break;
                case 30:
                    this.t31 = tag; break;
                case 31:
                    this.t32 = tag; break;
                case 32:
                    this.t33 = tag; break;
                default:
                    break;
            }

            #endregion

            SetLblQuestion(questionIndex + 1);

            //最后一项时 进行体质计算
            if (questionIndex == 32)
            {
                计算体质();
                this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                Question nextQuestion = list.Find(x => x.Index == questionIndex + 1);
                SetRadioText(nextQuestion);
                if (nextQuestion.Score == 0)//表示下一题没有进行作答
                {
                    SetBtnEnable(nextQuestion);
                    this.radioGroup1.SelectedIndexChanged -= this.radioGroup1_SelectedIndexChanged;
                    this.radioGroup1.SelectedIndex = -1;
                    this.radioGroup1.SelectedIndexChanged += this.radioGroup1_SelectedIndexChanged;
                }
                else //下一题已做答
                {
                    this.radioGroup1.SelectedIndexChanged -= this.radioGroup1_SelectedIndexChanged;
                    this.radioGroup1.EditValue = nextQuestion.Score;
                    this.radioGroup1.SelectedIndexChanged += this.radioGroup1_SelectedIndexChanged;
                }

                //btn下一项_Click(null, null);
            }
        }

        private void SetBtnEnable(Question nextQuestion)
        {
            if (nextQuestion == null) return;
            int index = nextQuestion.Index;
            int score = nextQuestion.Score;
            if (index == 0 && score == 0)
            {
                this.btn上一项.Enabled = false;
                this.btn下一项.Enabled = false;
            }
            else if (index == 0 && score != 0)
            {
                this.btn上一项.Enabled = false;
                this.btn下一项.Enabled = true;
            }
            else if (index != 0 && score == 0)
            {
                this.btn上一项.Enabled = true;
                this.btn下一项.Enabled = false;
            }
            else if (index == 32)
                this.btn下一项.Enabled = false;
            else
            {
                this.btn上一项.Enabled = true;
                this.btn下一项.Enabled = true;
            }
        }

        //语音播报
        void SpeakdelegateMethod(string speaksing)
        {

            try
            {
                speak.SpeakAsyncCancelAll();
                speak.SpeakAsync(speaksing);
            }
            catch
            {
            }
        }


        Timer tm = new Timer();
        private void btn开始_Click(object sender, EventArgs e)
        {
            try
            {
                if (FrmMain.bIDReaderInitResult)
                {
                    tm.Interval = 1000;
                    tm.Tick += tm_Tick;
                    this.BeginInvoke((EventHandler)(delegate { tm.Start(); }));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool PicBoxLoadPic(PictureEdit pictureBox, string picFileName, int picshow)
        {
            bool ret = false;
            try
            {
                if (!File.Exists(picFileName))
                {
                    pictureBox.Image = null;
                    return ret;
                }
                pictureBox.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Clip;
                using (FileStream fs = new FileStream(picFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryReader br = new BinaryReader(fs);
                    MemoryStream ms = new MemoryStream(br.ReadBytes((int)fs.Length));
                    pictureBox.Image = Image.FromStream(ms);
                }
                ret = true;
            }
            catch
            {
            }
            return ret;
        }

        void tm_Tick(object sender, EventArgs e)
        {
            int result = IDReadHelper.ReadIDInfoFromInstr();
            if (result == 1)
            {
                tm.Stop();
                txt姓名.Text = IDReadHelper.Name;
                txt性别.Text = IDReadHelper.Sex;
                txt出生日期.Text = IDReadHelper.Birth;
                txt居住地址.Text = IDReadHelper.Addr;
                txt身份证号.Text = IDReadHelper.SFZH;
                //textEdit民族.Text = IDReadHelper.Minzu;
                //textEdit有效期限.Text = IDReadHelper.Qixian;
                string PhotoPath = Application.StartupPath + "\\" + "zp.bmp";
                PicBoxLoadPic(pictureEdit1, PhotoPath, 0);
                DBManager.Beep();
            }
        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            editEnd = true;
            if (CloseThis != null)
                CloseThis();
        }

    }


    public class Question
    {

        public Question(string _option1 = "没有（根本不）", string _option2 = "很少（有一点）", string _option3 = "有时（有些）", string _option4 = "经常（相当）", string _option5 = "总是（非常）")
        {
            option1 = _option1;
            option2 = _option2;
            option3 = _option3;
            option4 = _option4;
            option5 = _option5;
        }

        int index;
        string question;
        int score;
        string option1;

        public string Option1
        {
            get { return option1; }
            set { option1 = value; }
        }
        string option2;

        public string Option2
        {
            get { return option2; }
            set { option2 = value; }
        }
        string option3;

        public string Option3
        {
            get { return option3; }
            set { option3 = value; }
        }
        string option4;

        public string Option4
        {
            get { return option4; }
            set { option4 = value; }
        }
        string option5;

        public string Option5
        {
            get { return option5; }
            set { option5 = value; }
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public string QuestionName
        {
            get { return question; }
            set { question = value; }
        }

        public int Score
        {
            get { return score; }
            set { score = value; }
        }
    }
}
