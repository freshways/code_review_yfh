﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;
using DXTempLate.Class;

namespace DXTempLate.Controls
{
    /// <summary>
    /// 工作单查询
    /// </summary>
    public partial class XUC_GZDTJMap : DevExpress.XtraEditors.XtraUserControl
    {
        public XUC_GZDTJMap()
        {
            InitializeComponent();
        }

        private static XUC_GZDTJMap _ItSelf;
        public static XUC_GZDTJMap ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUC_GZDTJMap();
                }
                return _ItSelf;
            }
        }

        private void XUC_GZDTJMap_Load(object sender, EventArgs e)
        {
            this.dateEditStartDate.Text = System.DateTime.Now.ToString("yyyy-MM-01");
            this.dateEditEndDate.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            //绑定统计图类型
            this.comboBoxEdit1.Properties.Items.Clear();
            this.comboBoxEdit1.Properties.Items.Add("直方图");
            this.comboBoxEdit1.Properties.Items.Add("3D直方图");
            this.comboBoxEdit1.Properties.Items.Add("圆饼图");
            this.comboBoxEdit1.Properties.Items.Add("3D圆饼图");
            this.comboBoxEdit1.Properties.Items.Add("线图");
            this.comboBoxEdit1.Properties.Items.Add("点图");
            this.comboBoxEdit1.SelectedIndex = 0;

            BindData();

            //设置图表标题  
            ChartTitle ct = new ChartTitle();
            ct.Text = "人员工作量统计图";
            ct.TextColor = Color.Black;//颜色  
            ct.Font = new Font("Tahoma", 12);//字体  
            ct.Dock = ChartTitleDockStyle.Top;//停靠在上方  
            ct.Alignment = StringAlignment.Center;//居中显示  
            this.chartControl1.Titles.Add(ct);

            LoadChartInfo();
        }


        private Dictionary<string, int> dicUserGZL = new Dictionary<string, int>();
        //查询统计
        private void simpleBtnQuery_Click(object sender, EventArgs e)
        {
            string dtStartTime, dtEndTime;
            dtStartTime = this.dateEditStartDate.Text.Trim();
            dtEndTime = this.dateEditEndDate.Text.Trim();

            BindData(dtStartTime,dtEndTime);

            LoadChartInfo();
        }
        private void BindData(string dtStartTime = "", string dtEndTime="")
        {
            SqliteClass sqldb = new SqliteClass();
            List<int> listid = new List<int>();
            listid = sqldb.GetAllTablesID("Sys_Employee");
            if (listid.Count > 0)
            {
                dicUserGZL.Clear();
                for (int i = 0; i < listid.Count; i++)
                {
                    //if (listid[i] == 1) continue;
                    Entity_Sys_Employee objinfo = new Entity_Sys_Employee();
                    objinfo = sqldb.GetSys_Employee(listid[i]);
                    string strUserName = objinfo.Name;

                    List<int> listidCount = new List<int>();
                    listidCount = sqldb.GetGSGZDID(dtStartTime, dtEndTime,"", strUserName, "","");
                    dicUserGZL.Add(strUserName, listidCount.Count);
                }
            }
        }

        /// <summary>
        /// 统计图
        /// </summary>
        private Series mySeries;

        /// <summary>
        /// 手动加载统计图信息
        /// </summary>
        private void LoadChartInfo()
        {
            this.chartControl1.Series.Clear();
            //新建Series
            switch (comboBoxEdit1.Text)
            {
                case "直方图":
                    mySeries = new Series("直方图", ViewType.Bar);
                    break;
                case "3D直方图":
                    mySeries = new Series("3D直方图", ViewType.Bar3D);
                    break;
                case "圆饼图":
                    mySeries = new Series("圆饼图", ViewType.Pie);
                    ((PiePointOptions)mySeries.LegendPointOptions).PointView = PointView.ArgumentAndValues;
                    break;
                case "3D圆饼图":
                    mySeries = new Series("3D圆饼图", ViewType.Pie3D);
                    break;
                case "线图":
                    mySeries = new Series("线图", ViewType.Line);
                    ((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
                    break;
                case "点图":
                    mySeries = new Series("点图", ViewType.Point);
                    ((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
                    break;
                default:
                    mySeries = new Series("直方图", ViewType.Bar);
                    break;
            }

            //设置Series样式  
            mySeries.ArgumentScaleType = ScaleType.Auto;//.Qualitative;//定性的
            mySeries.ValueScaleType = ScaleType.Numerical;//数字类型  

            mySeries.LegendPointOptions.PointView = PointView.ArgumentAndValues;//显示表示的信息和数据  
            mySeries.LegendPointOptions.ValueNumericOptions.Format = NumericFormat.Number;//NumericFormat.Percent;//用百分比表示
            //mySeries.PointOptions.ValueNumbericOptions.Precision = 0;//百分号前面的数字不跟小数点

            //绑定数据源          
            DataTable datTable = new DataTable();
            datTable = GetTable();
            mySeries.DataSource = datTable.DefaultView;// 获取到的数据
            mySeries.ArgumentDataMember = "人员名称";//绑定的文字信息（名称）
            mySeries.ValueDataMembers[0] = "收到文件数";//绑定的值（数据）                      

            if (comboBoxEdit1.Text == "圆饼图" || comboBoxEdit1.Text == "3D圆饼图")
            {
                mySeries.LegendPointOptions.ValueNumericOptions.Format = NumericFormat.Number;//用百分比表示
                //for (int i = 0; i < datTable.Rows.Count; i++)
                //{
                //    string strType = datTable.Rows[i]["收支类型"].ToString();
                //    double dtJE = Convert.ToDouble(datTable.Rows[i]["收支金额"].ToString());
                //    mySeries.Points.Add(new SeriesPoint(strType, new double[] { dtJE}));
                //}   
            }
            else if (comboBoxEdit1.Text == "线图")
            {
                mySeries.ArgumentScaleType = ScaleType.Qualitative;//设置Series样式  
                //for (int i = 0; i < datTable.Rows.Count; i++)
                //{
                //    string strType = datTable.Rows[i]["收支类型"].ToString();
                //    double dtJE = Convert.ToDouble(datTable.Rows[i]["收支金额"].ToString());
                //    mySeries.Points.Add(new SeriesPoint(strType, new double[] { dtJE }));
                //}                 
                //((PointSeriesView)mySeries.View).PointMarkerOptions.Kind = MarkerKind.Triangle;
            }

            //添加到统计图上
            this.chartControl1.Series.Clear();
            this.chartControl1.Series.Add(mySeries);

            if (comboBoxEdit1.Text == "线图" || comboBoxEdit1.Text == "点图")
            {
                this.chartControl1.Legend.Visible = true;
            }

            //图例设置
            SimpleDiagram3D diagram = new SimpleDiagram3D();
            diagram.RuntimeRotation = true;
            diagram.RuntimeScrolling = true;
            diagram.RuntimeZooming = true;
        }

        private DataTable GetTable()
        {
            DataTable datTable = new DataTable();
            DataColumn dc1 = new DataColumn("人员名称");
            DataColumn dc2 = new DataColumn("收到文件数", typeof(int));

            datTable.Columns.Add(dc1);
            datTable.Columns.Add(dc2);

            if (dicUserGZL.Count > 0)
            {
                foreach (KeyValuePair<string, int> kv in dicUserGZL)
                {
                    DataRow datRow0 = datTable.NewRow();//创建新行
                    datRow0[0] = kv.Key;//用户名称
                    datRow0[1] = kv.Value;//工作量总数
                    datTable.Rows.Add(datRow0);
                }
            }           
            return datTable;
        }

        private void comboBoxEdit1_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadChartInfo();
        }

    }
}
