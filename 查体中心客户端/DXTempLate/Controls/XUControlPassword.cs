﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;

namespace DXTempLate.Controls
{
    public partial class XUControlPassword : DevExpress.XtraEditors.XtraUserControl
    {
        public XUControlPassword()
        {
            InitializeComponent();
        }
        private static XUControlPassword _ItSelf;
        public static XUControlPassword ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUControlPassword();
                }
                return _ItSelf;
            }
        }
        private void simpleBtnSave_Click(object sender, EventArgs e)
        {
            if (this.txtOldPwd.Text.Trim() == "")
            {
                XtraMessageBox.Show("请先输入原密码!","系统提示");
                return;
            }
            if (this.txtNewPwd.Text.Trim() == "")
            {
                XtraMessageBox.Show("请先输入新密码!", "系统提示");
                return;
            }
            if (this.txtNewPwd2.Text.Trim() == "")
            {
                XtraMessageBox.Show("请先重复输入新密码!", "系统提示");
                return;
            }

            SqliteClass sqlite = new SqliteClass();
            int nUserID = 0;
            if (sqlite.CheckUserLogin(StaticClass.g_CurUserName, this.txtOldPwd.Text.Trim(), out nUserID)) //判断输入的原始密码正确
            {
                //修改新密码
                if (sqlite.UpdateUserPwd(StaticClass.g_CurUserName, this.txtNewPwd.Text.Trim()))
                {
                    XtraMessageBox.Show("密码修改成功!", "系统提示");
                    return;
                }
                else
                {
                    XtraMessageBox.Show("密码修改失败!", "系统提示");
                    return;
                }
            }
            else
            {
                XtraMessageBox.Show("你输入的原密码不正确，请重新输入!", "系统提示");
                return;
            }

        }
    }
}
