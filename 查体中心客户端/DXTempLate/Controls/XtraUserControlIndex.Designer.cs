﻿namespace DXTempLate.Controls
{
    partial class XtraUserControlIndex
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn成人体检 = new DevExpress.XtraEditors.SimpleButton();
            this.btn儿童体检 = new DevExpress.XtraEditors.SimpleButton();
            this.btn中医体质辨识 = new DevExpress.XtraEditors.SimpleButton();
            this.btn报告单 = new DevExpress.XtraEditors.SimpleButton();
            this.btn体检统计 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.groupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.SlateBlue;
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl1.Controls.Add(this.flowLayoutPanel1);
            this.groupControl1.Location = new System.Drawing.Point(21, 21);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(764, 497);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "查体操作";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.btn成人体检);
            this.flowLayoutPanel1.Controls.Add(this.btn儿童体检);
            this.flowLayoutPanel1.Controls.Add(this.btn中医体质辨识);
            this.flowLayoutPanel1.Controls.Add(this.btn报告单);
            this.flowLayoutPanel1.Controls.Add(this.btn体检统计);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(20, 86);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(725, 359);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btn成人体检
            // 
            this.btn成人体检.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(188)))), ((int)(((byte)(154)))));
            this.btn成人体检.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn成人体检.Appearance.Options.UseBackColor = true;
            this.btn成人体检.Appearance.Options.UseFont = true;
            this.btn成人体检.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btn成人体检.Image = global::DXTempLate.Properties.Resources.身高体重;
            this.btn成人体检.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn成人体检.Location = new System.Drawing.Point(8, 8);
            this.btn成人体检.Name = "btn成人体检";
            this.btn成人体检.Size = new System.Drawing.Size(226, 161);
            this.btn成人体检.TabIndex = 0;
            this.btn成人体检.Text = "成人体检登记";
            this.btn成人体检.Click += new System.EventHandler(this.btn成人体检_Click);
            // 
            // btn儿童体检
            // 
            this.btn儿童体检.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(188)))), ((int)(((byte)(154)))));
            this.btn儿童体检.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn儿童体检.Appearance.Options.UseBackColor = true;
            this.btn儿童体检.Appearance.Options.UseFont = true;
            this.btn儿童体检.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btn儿童体检.Image = global::DXTempLate.Properties.Resources.身高体重;
            this.btn儿童体检.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn儿童体检.Location = new System.Drawing.Point(240, 8);
            this.btn儿童体检.Name = "btn儿童体检";
            this.btn儿童体检.Size = new System.Drawing.Size(226, 161);
            this.btn儿童体检.TabIndex = 0;
            this.btn儿童体检.Text = "儿童体检登记";
            this.btn儿童体检.Click += new System.EventHandler(this.btn儿童体检_Click);
            // 
            // btn中医体质辨识
            // 
            this.btn中医体质辨识.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(88)))), ((int)(((byte)(181)))));
            this.btn中医体质辨识.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn中医体质辨识.Appearance.Options.UseBackColor = true;
            this.btn中医体质辨识.Appearance.Options.UseFont = true;
            this.btn中医体质辨识.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btn中医体质辨识.Image = global::DXTempLate.Properties.Resources.中医体质辨识;
            this.btn中医体质辨识.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn中医体质辨识.Location = new System.Drawing.Point(472, 8);
            this.btn中医体质辨识.Name = "btn中医体质辨识";
            this.btn中医体质辨识.Size = new System.Drawing.Size(226, 161);
            this.btn中医体质辨识.TabIndex = 0;
            this.btn中医体质辨识.Text = "中医体质辨识";
            this.btn中医体质辨识.Click += new System.EventHandler(this.btn中医体质辨识_Click);
            // 
            // btn报告单
            // 
            this.btn报告单.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(0)))));
            this.btn报告单.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn报告单.Appearance.Options.UseBackColor = true;
            this.btn报告单.Appearance.Options.UseFont = true;
            this.btn报告单.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btn报告单.Image = global::DXTempLate.Properties.Resources.血压;
            this.btn报告单.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn报告单.Location = new System.Drawing.Point(8, 175);
            this.btn报告单.Name = "btn报告单";
            this.btn报告单.Size = new System.Drawing.Size(226, 161);
            this.btn报告单.TabIndex = 0;
            this.btn报告单.Text = "打印报告单";
            this.btn报告单.Click += new System.EventHandler(this.btn报告单_Click);
            // 
            // btn体检统计
            // 
            this.btn体检统计.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(200)))), ((int)(((byte)(83)))));
            this.btn体检统计.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn体检统计.Appearance.Options.UseBackColor = true;
            this.btn体检统计.Appearance.Options.UseFont = true;
            this.btn体检统计.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btn体检统计.Image = global::DXTempLate.Properties.Resources.随访;
            this.btn体检统计.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btn体检统计.Location = new System.Drawing.Point(240, 175);
            this.btn体检统计.Name = "btn体检统计";
            this.btn体检统计.Size = new System.Drawing.Size(226, 161);
            this.btn体检统计.TabIndex = 0;
            this.btn体检统计.Text = "体检数据统计";
            this.btn体检统计.Click += new System.EventHandler(this.btn体检统计_Click);
            // 
            // XtraUserControlIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Name = "XtraUserControlIndex";
            this.Size = new System.Drawing.Size(806, 538);
            this.Load += new System.EventHandler(this.XtraUserControlIndex_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn成人体检;
        private DevExpress.XtraEditors.SimpleButton btn体检统计;
        private DevExpress.XtraEditors.SimpleButton btn中医体质辨识;
        private DevExpress.XtraEditors.SimpleButton btn报告单;
        private DevExpress.XtraEditors.SimpleButton btn儿童体检;
    }
}
