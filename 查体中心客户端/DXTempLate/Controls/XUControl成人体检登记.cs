﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DXTempLate.Class;
using System.IO;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;

namespace DXTempLate.Controls
{
    public partial class XUControl成人体检登记 : DevExpress.XtraEditors.XtraUserControl
    {
        public XUControl成人体检登记()
        {
            InitializeComponent();
        }

        #region 委托/事件

        public delegate void CloseEventHandler(); //返回结果

        public event CloseEventHandler CloseThis;

        #endregion

        public static bool editEnd = false;//是否保存完成

        private static XUControl成人体检登记 _ItSelf;
        public static XUControl成人体检登记 ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUControl成人体检登记();
                }
                else if (editEnd)
                {
                    //_ItSelf.Dispose();
                    _ItSelf = new XUControl成人体检登记();
                    editEnd = false;
                }

                return _ItSelf;
            }
        }

        private void XUControlTJMain_Load(object sender, EventArgs e)
        {
           
            //System.Threading.Thread.Sleep(2000);
            btn开始_Click(sender, e);
        }

        #region 实时结果
        void cl体温_SendToTWResult(string Info)
        {
            //更新界面
            this.Invoke((EventHandler)(delegate { this.txt体温.Text = Info; }));
        }

        void cl血压_SendToXYResult(CL血压Info Info)
        {
            //更新界面
            this.Invoke((EventHandler)(delegate
            {
                this.txt收缩压.Text = Info._收缩压;
                this.txt舒张压.Text = Info._舒张压;
                this.txt心率.Text = Info._心率;
            }));
        }

        void cl身高_SendToResult(CL身高Info Info)
        {
            //更新界面
            this.Invoke((EventHandler)(delegate
            {
                txt身高.Text = Info._身高;
                txt体重.Text = Info._体重;
                ComputBMI();
            }));
        }

        void ComputBMI()
        {
            Decimal 身高 = 0;
            Decimal 体重 = 0;
            Decimal BMI = 0;
            Decimal BZBMI = 24.0m;
            if (string.IsNullOrEmpty(this.txt身高.Text) || string.IsNullOrEmpty(this.txt体重.Text)) return;
            if (Decimal.TryParse(this.txt身高.Text, out 身高) && Decimal.TryParse(this.txt体重.Text, out 体重))
            {
                BMI = Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2);
                this.txtBMI.Text = BMI.ToString();
                if (BMI > 24)
                {
                    体重 = Math.Floor(BZBMI * 身高 / 100 * 身高 / 100);
                    this.txt建议体重.Text = 体重.ToString() + "KG";
                }
                else
                {
                    this.txt建议体重.Text = "";
                }
            }
        }
        #endregion


        Timer tm = new Timer();
        public bool PicBoxLoadPic(PictureEdit pictureBox, string picFileName, int picshow)
        {
            bool ret = false;
            try
            {
                if (!File.Exists(picFileName))
                {
                    pictureBox.Image = null;
                    return ret;
                }
                pictureBox.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Clip;
                //这种方式不好，载入文件后一直占有，所以改用流的方式
                //Bitmap myImage = new Bitmap(picFileName);
                //pictureBox.Size = myImage.Size;//new Size(300,400);
                //pictureBox.Image = (Image)myImage;
                //pictureBox.Image = Bitmap.FromFile(picFileName); //这种方式不好，载入文件后一直占有
                using (FileStream fs = new FileStream(picFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryReader br = new BinaryReader(fs);
                    MemoryStream ms = new MemoryStream(br.ReadBytes((int)fs.Length));
                    pictureBox.Image = Image.FromStream(ms);
                }
                ret = true;
            }
            catch
            {
            }
            return ret;
        }
        
        private void btn开始_Click(object sender, EventArgs e)
        {
            try
            {
                if (FrmMain.bIDReaderInitResult)
                {
                    tm.Interval = 1000;
                    tm.Tick += tm_Tick;
                    this.BeginInvoke((EventHandler)(delegate { tm.Start(); }));
                }

                if (FrmMain.bl身高InitResult)
                {
                    FrmMain.cl身高.SendToSGResult += cl身高_SendToResult;
                }
                if (FrmMain.bl血压InitResult)
                {
                    FrmMain.cl血压.SendToXYResult += cl血压_SendToXYResult;
                    //FrmMain.cl血压.Send("CC-80-02-03-01-01-00");
                    //FrmMain.cl血压.Send("CC-80-02-03-01-02-00");
                }
                if (FrmMain.bl体温InitResult)
                {
                    FrmMain.cl体温.SendToTWResult += cl体温_SendToTWResult;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        void tm_Tick(object sender, EventArgs e)
        {
            int result = IDReadHelper.ReadIDInfoFromInstr();
            if (result == 1)
            {
                tm.Stop();
                txt姓名.Text = IDReadHelper.Name;
                txt性别.Text = IDReadHelper.Sex;
                txt出生日期.Text = IDReadHelper.Birth;
                txt居住地址.Text = IDReadHelper.Addr;
                txt身份证号.Text = IDReadHelper.SFZH;
                //textEdit民族.Text = IDReadHelper.Minzu;
                //textEdit有效期限.Text = IDReadHelper.Qixian;
                string PhotoPath = Application.StartupPath + "\\" + "zp.bmp";
                PicBoxLoadPic(pictureEdit1, PhotoPath, 0);
                DBManager.Beep();
            }
        }

        tb_健康体检Info objinfo = new tb_健康体检Info(); //打印用
        private void btn保存_Click(object sender, EventArgs e)
        {
            editEnd = true;
           
            try
            {
                tb_健康体检Info tb_new = new tb_健康体检Info();
                if (string.IsNullOrEmpty(this.txt舒张压.Text) || string.IsNullOrEmpty(this.txt收缩压.Text) || string.IsNullOrEmpty(this.txt心率.Text))
                {
                    MessageBox.Show("不存在血压值，请重新测量！");
                    return;
                }
                if (string.IsNullOrEmpty(txt身份证号.Text) || string.IsNullOrEmpty(txt姓名.Text.Trim()))
                {
                    MessageBox.Show("姓名、身份证号等不能为空！");
                    return;
                }
                //基本信息
                tb_new.身份证号 = this.txt身份证号.Text.Trim();
                tb_new.姓名 = this.txt姓名.Text.Trim();
                tb_new.性别 = this.txt性别.Text.Trim();
                tb_new.出生日期 = this.txt出生日期.Text.Trim();
                tb_new.住址 = this.txt居住地址.Text.Trim();
                tb_new.联系电话 = this.txt联系电话.Text.Trim();
                tb_new.备注 = this.txt备注.Text.Trim();
                //身高体重
                tb_new.身高 = Convert.ToDouble(string.IsNullOrEmpty(this.txt身高.Text.Trim()) ? "0" : this.txt身高.Text.Trim());
                tb_new.体重 = Convert.ToDouble(string.IsNullOrEmpty(this.txt体重.Text.Trim()) ? "0" : this.txt体重.Text.Trim());
                tb_new.体重指数 = this.txtBMI.Text.Trim();
                tb_new.腰围 = Convert.ToDouble(string.IsNullOrEmpty(this.txt腰围.Text.Trim()) ? "0" : this.txt腰围.Text.Trim());
                tb_new.危险因素控制体重 = this.txt建议体重.Text.Trim();
                //血压
                tb_new.血压右侧1 = this.txt收缩压.Text.Trim();
                tb_new.血压右侧2 = this.txt舒张压.Text.Trim();
                tb_new.心率 = Convert.ToInt32(string.IsNullOrEmpty(this.txt心率.Text.Trim()) ? "0" : this.txt心率.Text.Trim());
                //体温
                tb_new.体温 = Convert.ToDouble(this.txt体温.Text.Trim());
                tb_new.呼吸 = Convert.ToInt32(this.txt呼吸.Text.Trim());
                //血糖
                tb_new.空腹血糖 = this.txt血糖.Text.Trim();
                //视力
                tb_new.左眼视力 = Convert.ToDouble(string.IsNullOrEmpty(this.txt左眼视力.Text.Trim()) ? "0" : this.txt左眼视力.Text.Trim());
                tb_new.左眼矫正 = this.txt左眼矫正视力.Text.Trim();
                tb_new.右眼视力 = Convert.ToDouble(string.IsNullOrEmpty(this.txt右眼视力.Text.Trim()) ? "0" : this.txt右眼视力.Text.Trim());
                tb_new.右眼矫正 = this.txt右眼矫正视力.Text.Trim();

                tb_new.体检日期 = DateTime.Now.ToString("yyyy-MM-dd");

                if (tb_健康体检DAL.UpdateSys_血压(tb_new))
                {
                    if (MessageBox.Show("保存数据成功！是否打印？", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        objinfo = tb_new;
                        btn打印_Click(null, null);
                    }
                    tm.Stop(); //如果身份证读卡没停，先停掉
                    if (CloseThis != null) //保存完后关闭
                        CloseThis();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否放弃保存本次体检数据！", "消息", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                editEnd = true;
                tm.Stop(); //如果身份证读卡没停，先停掉
                if (CloseThis!=null)
                    CloseThis();
            }
        }

        private void btn打印_Click(object sender, EventArgs e)
        {
            if (objinfo != null)
            {
                string text = Calculate(objinfo);
                List<string> result = new List<string>();
                result.Add(objinfo.姓名);
                result.Add(objinfo.性别);
                result.Add(objinfo.出生日期);
                result.Add(objinfo.身份证号);
                result.Add(text);
                result.Add(objinfo.体检日期);
                string s_体检内容 = "身高：" + objinfo.身高 + "cm  体重：" + objinfo.体重 + "KG 腰围："+objinfo.腰围+" 体温："
                    + objinfo.体温 + "℃  \n血糖：" + objinfo.空腹血糖 + "mmol/l  血压：" + objinfo.血压右侧1 + "/" + objinfo.血压右侧2
                    + "  视力（左）：" + objinfo.左眼视力 + "/" + objinfo.左眼矫正 + "  视力（右）：" + objinfo.右眼视力 + "/" + objinfo.右眼矫正;
                result.Add(s_体检内容);
                XtraReport体检回执单 xrpt = new XtraReport体检回执单(result);
                xrpt.ShowPreviewDialog();
            }
        }

        private string Calculate(tb_健康体检Info info)
        {
            StringBuilder builder = new StringBuilder();

            #region BMI
            //BMI
            /*
                过轻：低于18.5
                正常：18.5-23.9
                超重：≥24
             * 24～27.9 偏胖
                肥胖：28-32
                非常肥胖, 高于32
             */
            if (!string.IsNullOrEmpty(info.体重指数))
            {
                double bmi = double.Parse(info.体重指数);
                if (bmi < 18.5)
                {
                    builder.AppendLine("[体重偏轻]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议：太瘦也有一定的原因：饮食不当、减肥过度、疾病因素，瘦人脂肪过少，更易发生胃下垂、肾下垂，生病后恢复的速度也较常人慢；饮食可一直消瘦，一定要检查排除疾病引起的。");
                }
                else if (bmi >= 24)
                {
                    builder.AppendLine("[体重超重]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议：\n1.控制好饮食量，不能没有饥饱的进食；\n2.注意饮食习惯，少吃肉类、油炸类、含糖量较高的食物，还有少吃盐和辣、高热量如巧克力、面包、可乐等；\n3.饭后适量运动；\n4.多参加消耗大量体能的运动。");
                }
                else if (bmi >= 24 && bmi < 27.9)
                {
                    builder.AppendLine("[体重偏胖]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议：\n1.控制好饮食量，不能没有饥饱的进食；\n2.注意饮食习惯，少吃肉类、油炸类、含糖量较高的食物，还有少吃盐和辣、高热量如巧克力、面包、可乐等；\n3.饭后适量运动；\n4.多参加消耗大量体能的运动。");
                }
                else if (bmi >= 28 && bmi < 32)
                {
                    builder.AppendLine("[体重肥胖]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议： <是指身高和体重比例失衡，不健康的肥胖，体重超重往往会引起很多疾病发生。>指导：\n1.控制好饮食量，不能没有饥饱的进食；\n2.注意饮食习惯，少吃肉类、油炸类、含糖量较高的食物，还有少吃盐和辣、高热量如巧克力、面包、可乐等；\n3.饭后适量运动；\n4.多参加消耗大量体能的运动。");
                }
                else if (bmi > 32)
                {
                    builder.AppendLine("[体重非常肥胖]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议：<是指身高和体重比例失衡，不健康的肥胖，体重超重往往会引起很多疾病发生。>指导：\n1.控制好饮食量，不能没有饥饱的进食；\n2.注意饮食习惯，少吃肉类、油炸类、含糖量较高的食物，还有少吃盐和辣、高热量如巧克力、面包、可乐等；\n3.饭后适量运动；\n4.多参加消耗大量体能的运动。");
                }
            }

            #endregion

            #region 糖尿病

            if (!string.IsNullOrEmpty(info.空腹血糖))
            {
                string xuetang = info.空腹血糖;
                decimal _xuetang = 0m;
                if (Decimal.TryParse(xuetang, out _xuetang))
                {
                    if (_xuetang != 0m && (_xuetang > 6.40m || _xuetang < 3.89m))
                    {
                        builder.AppendLine("[血糖偏高](GLU:" + _xuetang + "mmol/l)（3.89-6.40）");
                        builder.AppendLine("  建议复查空腹血糖。建议：\n1、糖尿病者积极配合医院规范化治疗；\n2、接受多种形式的健康教育知识；\n3、调整饮食，控制热量的摄入，按医嘱进行自我监测；\n4、成人35岁以后都应每年查血糖和尿糖，早发现早治疗；\n5、改变不良生活方式，忌烟酒，加强运动；\n6、保持心态平衡，克服各种紧张压力，培养高尚情操，多爬山郊游等；\n7、定期复查。");
                    }
                }
            }

            #endregion

            #region 血压

            if (!string.IsNullOrEmpty(info.血压右侧1) || !string.IsNullOrEmpty(info.血压左侧1))
            {
                //右侧
                if (!string.IsNullOrEmpty(info.血压右侧1))
                {
                    string you1 = info.血压右侧1;
                    string you2 = info.血压右侧2;
                    int _you1; int _you2;
                    if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
                    {
                        if (_you1 > 140 || _you2 > 90)
                        {
                            builder.AppendLine("[血压偏高]（" + you1 + "/" + you2 + ")");
                            builder.AppendLine("  建议连续三日测血压以明确诊断。建议：\n1、注意劳逸结合，保持心境平和；\n2、坚持有氧运动；\n3、戒烟限酒；\n4、少吃盐；\n5、控制体重。\n治疗：上述非药物治疗后血压仍高者，应考虑用降压药。定期测量血压。");
                        }
                    }
                }
                //左侧
                else if (!string.IsNullOrEmpty(info.血压左侧1))
                {
                    string you1 = info.血压左侧1;
                    string you2 = info.血压左侧2;
                    int _you1; int _you2;
                    if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
                    {
                        if (_you1 > 140 || _you2 > 90)
                        {
                            builder.AppendLine("[血压偏高]（" + you1 + "/" + you2 + ")");
                            builder.AppendLine("  建议连续三日测血压以明确诊断。建议：\n1、注意劳逸结合，保持心境平和；\n2、坚持有氧运动；\n3、戒烟限酒；\n4、少吃盐；\n5、控制体重。\n治疗：上述非药物治疗后血压仍高者，应考虑用降压药。定期测量血压。");
                        }
                    }
                }
            }

            #endregion

            #region 体温
            //
            /*瞎编的
             */
            if (!string.IsNullOrEmpty(info.体温.ToString()))
            {
                if (info.体温 < 35.5)
                {
                    builder.AppendLine("[体温偏低]（体温：" + info.体温 + ")");
                    builder.AppendLine("  建议：重新测量。");
                }
                else if (info.体温 >= 37)
                {
                    builder.AppendLine("[体温偏高]（体温：" + info.体温 + ")");
                    builder.AppendLine("  建议：重新测量，如体温过高请赶快就医。");
                }
            }

            #endregion

            if (builder.Length == 0)
            {
                builder.AppendLine("体检未见明显异常");
            }

            #region 体质辨识
            //if (!string.IsNullOrEmpty(uc平和质.Txt1.Text) || !string.IsNullOrEmpty(uc气虚质.Txt1.Text) || !string.IsNullOrEmpty(uc阳虚质.Txt1.Text) || !string.IsNullOrEmpty(uc阴虚质.Txt1.Text) || !string.IsNullOrEmpty(uc痰湿质.Txt1.Text) || !string.IsNullOrEmpty(uc湿热质.Txt1.Text) || !string.IsNullOrEmpty(uc血瘀质.Txt1.Text) || !string.IsNullOrEmpty(uc气郁质.Txt1.Text) || !string.IsNullOrEmpty(uc特禀质.Txt1.Text))
            //{
            //    builder.AppendLine("中医体质辨识结果：");
            //    if (!string.IsNullOrEmpty(uc平和质.Txt1.Text))
            //    { builder.AppendLine("平和质：" + uc平和质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc气虚质.Txt1.Text))
            //    { builder.AppendLine("气虚质：" + uc气虚质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc阳虚质.Txt1.Text))
            //    { builder.AppendLine("阳虚质：" + uc阳虚质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc阴虚质.Txt1.Text))
            //    { builder.AppendLine("阴虚质：" + uc阴虚质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc痰湿质.Txt1.Text))
            //    { builder.AppendLine("痰湿质：" + uc痰湿质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc湿热质.Txt1.Text))
            //    { builder.AppendLine("湿热质：" + uc湿热质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc血瘀质.Txt1.Text))
            //    { builder.AppendLine("血瘀质：" + uc血瘀质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc气郁质.Txt1.Text))
            //    { builder.AppendLine("气郁质：" + uc气郁质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc特禀质.Txt1.Text))
            //    { builder.AppendLine("特禀质：" + uc特禀质.Txt1.Text); }
            //}

            #endregion

            return builder.ToString();
        }

        private void txt身高_EditValueChanged(object sender, EventArgs e)
        {
            ComputBMI();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            frm查询姓名 frm = new frm查询姓名();
            frm.ShowDialog();
        }


    }
}
