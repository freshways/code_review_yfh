﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
//using UDF.GUI.PrintControl;

namespace DXTempLate.Controls
{
    /// <summary>
    /// 工作单统计查询
    /// </summary>
    public partial class XUC_GSGZDTJ : DevExpress.XtraEditors.XtraUserControl
    {
        public XUC_GSGZDTJ()
        {
            InitializeComponent();
        }

        private static XUC_GSGZDTJ _ItSelf;
        public static XUC_GSGZDTJ ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUC_GSGZDTJ();
                }
                return _ItSelf;
            }
        }
        private FormClass formClass = new FormClass();
        private SqliteClass sqlClass = new SqliteClass();

        private void XUC_GSGZDTJ_Load(object sender, EventArgs e)
        {
            this.txt开始日期.Text = System.DateTime.Now.ToString("yyyy-MM-01");
            this.txt结束日期.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            Bind批办领导();
            Bind来文机关();
            BindData();
        }

        private void Bind来文机关()
        {
            List<string> list = new List<string>();
            list = sqlClass.GetAll来文机关();
            if (list.Count > 0)
            {
                this.txt来文机关.Properties.Items.Clear();
                for (int i = 0; i < list.Count; i++)
                {
                    this.txt来文机关.Properties.Items.Add(list[i]);
                }
            }
        }

        private void Bind批办领导()
        {
            List<string> list = new List<string>();
            list = sqlClass.GetAll批办领导();
            if (list.Count > 0)
            {
                this.txt批办领导.Properties.Items.Clear();
                for (int i = 0; i < list.Count; i++)
                {
                    this.txt批办领导.Properties.Items.Add(list[i]);
                }
            }
        }

        //查询统计
        private void simpleBtnQuery_Click(object sender, EventArgs e)
        {
            string d开始日期, d结束日期, str来文机关, str批办领导, str文件名称,str文号;
            d开始日期 = this.txt开始日期.Text.Trim();
            d结束日期 = this.txt结束日期.Text.Trim();
            str来文机关 = this.txt来文机关.Text.Trim();
            str批办领导 = this.txt批办领导.Text.Trim();
            str文件名称 = this.txt文件名称.Text.Trim();
            str文号 = this.txt文号.Text.Trim();

            BindData(d开始日期, d结束日期, str来文机关, str批办领导, str文件名称, str文号);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleBtnPrint_Click(object sender, EventArgs e)
        {            
            try
            {
                if (gridView1.DataSource != null && gridView1.DataRowCount > 0)
                {
                    XtraReport收文表 xrpt = new XtraReport收文表(((System.Data.DataView)(gridView1.DataSource)).Table);
                    xrpt.ShowPreviewDialog();
                }
                else
                {
                    XtraMessageBox.Show("没有可打印的内容!", "系统提示");                    
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("未发现打印设备!", "系统提示" + ex.Message);
            }
        }


        //导出Excel
        private void simpleBtnExportExcel_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("目前没有数据可以导出!", "系统提示");
                return;
            }

            PublicClass pub = new PublicClass();

            System.Data.DataTable datTable = new DataTable();
            DataColumn dc1 = new DataColumn("序号");
            DataColumn dc2 = new DataColumn("收到时间");
            DataColumn dc3 = new DataColumn("来文机关");
            DataColumn dc4 = new DataColumn("文号");
            DataColumn dc5 = new DataColumn("文件名称");
            DataColumn dc6 = new DataColumn("批办领导、科室");
            DataColumn dc7 = new DataColumn("收到文件数");
            DataColumn dc8 = new DataColumn("备注");

            datTable.Columns.Add(dc1);
            datTable.Columns.Add(dc2);
            datTable.Columns.Add(dc3);
            datTable.Columns.Add(dc4);
            datTable.Columns.Add(dc5);
            datTable.Columns.Add(dc6);
            datTable.Columns.Add(dc7);
            datTable.Columns.Add(dc8);

            for (int i = 0; i < ExportDataTable.Rows.Count; i++)
            {
                DataRow datRow = datTable.NewRow();//创建新行              
                datRow[0] = ExportDataTable.Rows[i]["序号"];
                datRow[1] = ExportDataTable.Rows[i]["收到时间"];
                datRow[2] = ExportDataTable.Rows[i]["来文机关"];
                datRow[3] = ExportDataTable.Rows[i]["文号"];
                datRow[4] = ExportDataTable.Rows[i]["文件名称"];
                datRow[5] = ExportDataTable.Rows[i]["批办领导、科室"];
                datRow[6] = ExportDataTable.Rows[i]["收到文件数"];
                datRow[7] = ExportDataTable.Rows[i]["备注"];
                datTable.Rows.Add(datRow);
            }
            
            pub.ExportToExcel(datTable);
        }

        private System.Data.DataTable ExportDataTable = new DataTable();

        /// <summary>
        /// 绑定值
        /// </summary>
        private void BindData(string d开始日期 = "", string d结束日期 = "", string str来文机关 = "", string str批办领导 = "", 
            string str文件名称 = "",string str文号="")
        {
            System.Data.DataTable datTable = new DataTable();
            #region 创建表头
            DataColumn dc1 = new DataColumn("选择", typeof(Boolean));
            DataColumn dc2 = new DataColumn("ID", typeof(int));
            DataColumn dc3 = new DataColumn("序号", typeof(int));
            DataColumn dc4 = new DataColumn("收到时间");
            DataColumn dc5 = new DataColumn("来文机关");
            DataColumn dc6 = new DataColumn("文号");
            DataColumn dc7 = new DataColumn("文件名称");
            DataColumn dc8 = new DataColumn("批办领导、科室");
            DataColumn dc9 = new DataColumn("收到文件数");
            DataColumn dc10 = new DataColumn("打印状态");
            DataColumn dc11 = new DataColumn("备注");
            DataColumn dc12 = new DataColumn("创建人");
            DataColumn dc13 = new DataColumn("创建时间");
            DataColumn dc14 = new DataColumn("修改人");
            DataColumn dc15 = new DataColumn("修改日期"); ;

            datTable.Columns.Add(dc1);
            datTable.Columns.Add(dc2);
            datTable.Columns.Add(dc3);
            datTable.Columns.Add(dc4);
            datTable.Columns.Add(dc5);
            datTable.Columns.Add(dc6);
            datTable.Columns.Add(dc7);
            datTable.Columns.Add(dc8);
            datTable.Columns.Add(dc9);
            datTable.Columns.Add(dc10);
            datTable.Columns.Add(dc11);
            datTable.Columns.Add(dc12);
            datTable.Columns.Add(dc13);
            datTable.Columns.Add(dc14);
            datTable.Columns.Add(dc15);  
            #endregion

            #region 绑定
            SqliteClass sqldb = new SqliteClass();
            List<int> listid = new List<int>();
            listid = sqldb.GetGSGZDID(d开始日期, d结束日期, str来文机关, str批办领导, str文件名称, str文号);
            
            if (listid.Count > 0)
            {
                for (int i = 0; i < listid.Count; i++)
                {
                    Entity_Sys_收文登记表 objinfo = new Entity_Sys_收文登记表();
                    objinfo = sqldb.GetSys_收文登记表ByID(listid[i]);
                    DataRow datRow = datTable.NewRow();//创建新行
                    datRow[0] = false;
                    datRow[1] = listid[i];
                    datRow[2] = i + 1;
                    datRow[3] = objinfo.收到时间.ToShortDateString();
                    datRow[4] = objinfo.来文机关;
                    datRow[5] = objinfo.文号;
                    datRow[6] = objinfo.文件名称;
                    datRow[7] = objinfo.批办领导;
                    datRow[8] = objinfo.收到文件数.ToString();
                    datRow[9] = objinfo.打印状态.ToString();
                    datRow[10] = objinfo.备注;
                    datRow[11] = objinfo.创建人;
                    datRow[12] = objinfo.创建时间.ToString();
                    datRow[13] = objinfo.修改人.ToString();
                    datRow[14] = objinfo.修改时间.ToString();
                    datTable.Rows.Add(datRow);
                }
                //DataTable按照排序字段进行升序排列
                datTable.DefaultView.Sort = "序号 asc";
                DataTable dtT = datTable.DefaultView.ToTable();
                datTable.Clear();
                datTable = dtT;
                ExportDataTable = datTable;

                this.gridControl1.DataSource = new DataView(datTable);
                this.gridControl1.Refresh();
                this.gridControl1.RefreshDataSource();

                this.txtMoneyAll.Text = datTable.Rows.Count.ToString();//
            }
            else
            {
                this.gridControl1.DataSource = new DataView(datTable);
                this.gridControl1.Refresh();
                this.gridControl1.RefreshDataSource();
            } 
            #endregion

            #region grid 样式
            //设置列宽
            this.gridView1.Columns[0].Width = 28;
            this.gridView1.Columns[2].Width = 30;
            this.gridView1.Columns[3].Width = 75;
            this.gridView1.Columns[3].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[4].Width = 150;
            this.gridView1.Columns[4].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[5].Width = 150;
            this.gridView1.Columns[5].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[6].Width = 300;
            this.gridView1.Columns[6].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[7].Width = 80;
            this.gridView1.Columns[7].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            //隐藏列
            this.gridView1.Columns[1].Visible = false;
            this.gridView1.Columns[8].Visible = false;
            this.gridView1.Columns[9].Visible = false;
            this.gridView1.Columns[10].Visible = false;
            this.gridView1.Columns[11].Visible = false;
            this.gridView1.Columns[12].Visible = false;
            this.gridView1.Columns[13].Visible = false;
            this.gridView1.Columns[14].Visible = false;

            //设置列不允许编辑
            this.gridView1.Columns[2].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[3].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[4].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[5].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[6].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[7].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[8].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[9].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[10].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[11].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[12].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[13].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[14].OptionsColumn.AllowEdit = false;
            #endregion
        }

        #region gridEvent
        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                string str = "暂无工作单数据!";
                Font f = new Font("宋体", 10, FontStyle.Bold);
                Rectangle r = new Rectangle(e.Bounds.Left + 25, e.Bounds.Top + 5, e.Bounds.Width - 5, e.Bounds.Height - 5);
                e.Graphics.DrawString(str, f, Brushes.Black, r);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            int nCurRow = 0;
            GridHitInfo hi = this.gridView1.CalcHitInfo(this.gridControl1.PointToClient(MousePosition));
            if (hi.InRow)
            {
                nCurRow = hi.RowHandle;
            }
            if (nCurRow < 0) return;

            int nGridViewId = Convert.ToInt32(this.gridView1.GetRowCellValue(nCurRow, "ID"));

            XtraFormGSGZD gsyp = new XtraFormGSGZD(nGridViewId, true);
            gsyp.ShowDialog();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            FormClass frmClass = new FormClass();
            int nCurRow = 0;
            nCurRow = frmClass.XGridViewMouseDown(this.gridView1, e, 0);
        } 
        #endregion

    }
}
