﻿namespace DXTempLate.Controls
{
    partial class XUControlUserList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XUControlUserList));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleBSelAll = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.simpleBSelAll);
            this.panelControl1.Controls.Add(this.simpleBtnAdd);
            this.panelControl1.Controls.Add(this.simpleBtnDelete);
            this.panelControl1.Controls.Add(this.simpleBtnUpdate);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 410);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(761, 44);
            this.panelControl1.TabIndex = 1;
            // 
            // simpleBSelAll
            // 
            this.simpleBSelAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBSelAll.Image = ((System.Drawing.Image)(resources.GetObject("simpleBSelAll.Image")));
            this.simpleBSelAll.Location = new System.Drawing.Point(433, 11);
            this.simpleBSelAll.Name = "simpleBSelAll";
            this.simpleBSelAll.Size = new System.Drawing.Size(75, 23);
            this.simpleBSelAll.TabIndex = 7;
            this.simpleBSelAll.Text = "全选";
            this.simpleBSelAll.Click += new System.EventHandler(this.simpleBSelAll_Click);
            // 
            // simpleBtnAdd
            // 
            this.simpleBtnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnAdd.Image")));
            this.simpleBtnAdd.Location = new System.Drawing.Point(514, 11);
            this.simpleBtnAdd.Name = "simpleBtnAdd";
            this.simpleBtnAdd.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnAdd.TabIndex = 6;
            this.simpleBtnAdd.Text = "添加用户";
            this.simpleBtnAdd.Click += new System.EventHandler(this.simpleBtnAdd_Click);
            // 
            // simpleBtnDelete
            // 
            this.simpleBtnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnDelete.Image")));
            this.simpleBtnDelete.Location = new System.Drawing.Point(676, 11);
            this.simpleBtnDelete.Name = "simpleBtnDelete";
            this.simpleBtnDelete.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnDelete.TabIndex = 4;
            this.simpleBtnDelete.Text = "删除";
            this.simpleBtnDelete.Click += new System.EventHandler(this.simpleBtnDelete_Click);
            // 
            // simpleBtnUpdate
            // 
            this.simpleBtnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnUpdate.Image")));
            this.simpleBtnUpdate.Location = new System.Drawing.Point(595, 11);
            this.simpleBtnUpdate.Name = "simpleBtnUpdate";
            this.simpleBtnUpdate.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnUpdate.TabIndex = 3;
            this.simpleBtnUpdate.Text = "修改用户";
            this.simpleBtnUpdate.Click += new System.EventHandler(this.simpleBtnUserInfo_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(761, 410);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "用户列表";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 22);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(757, 386);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // XUControlUserList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "XUControlUserList";
            this.Size = new System.Drawing.Size(761, 454);
            this.Load += new System.EventHandler(this.XUControlUserList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleBtnAdd;
        private DevExpress.XtraEditors.SimpleButton simpleBtnDelete;
        private DevExpress.XtraEditors.SimpleButton simpleBtnUpdate;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton simpleBSelAll;
    }
}
