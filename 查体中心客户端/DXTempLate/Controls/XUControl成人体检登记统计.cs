﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
//using UDF.GUI.PrintControl;

namespace DXTempLate.Controls
{
    /// <summary>
    /// 工作单统计查询
    /// </summary>
    public partial class XUControl成人体检登记统计 : DevExpress.XtraEditors.XtraUserControl
    {
        public XUControl成人体检登记统计()
        {
            InitializeComponent();
        }

        private static XUControl成人体检登记统计 _ItSelf;
        public static XUControl成人体检登记统计 ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUControl成人体检登记统计();
                }
                return _ItSelf;
            }
        }
        
        private void XUControl成人体检登记统计_Load(object sender, EventArgs e)
        {
            this.txt开始日期.Text = System.DateTime.Now.ToString("yyyy-MM-01");
            this.txt结束日期.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            BindData();
        }
        
        //查询统计
        private void simpleBtnQuery_Click(object sender, EventArgs e)
        {
            string d开始日期, d结束日期, str来文机关, str批办领导, str文件名称,str文号;
            d开始日期 = this.txt开始日期.Text.Trim();
            d结束日期 = this.txt结束日期.Text.Trim();
            str来文机关 = this.txt姓名.Text.Trim();
            str批办领导 = this.txt性别.Text.Trim();
            str文件名称 = this.txt身份证号.Text.Trim();
            str文号 = this.txt家庭地址.Text.Trim();

            BindData("", "", "", "", "");
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleBtnPrint_Click(object sender, EventArgs e)
        {            
            try
            {
                if (gridView1.DataSource != null && gridView1.DataRowCount > 0)
                {//((System.Data.DataView)(gridView1.DataSource)).Table
                    DataRow dr = gridView1.GetFocusedDataRow();
                    tb_健康体检Info objinfo = new tb_健康体检Info();
                    objinfo = tb_健康体检DAL.Gettb_健康体检InfoById(dr["ID"].ToString());
                    if (objinfo != null)
                    {
                        string text  = Calculate(objinfo);
                        List<string> result = new List<string>();
                        result.Add(objinfo.姓名);
                        result.Add(objinfo.性别);
                        result.Add(objinfo.出生日期);
                        result.Add(objinfo.身份证号);
                        result.Add(text);
                        result.Add(objinfo.体检日期);
                        string s_体检内容 = "身高：" + objinfo.身高 + "cm   体重：" + objinfo.体重 + "KG 体温："
                            + objinfo.体温 + "℃  血糖：" + objinfo.空腹血糖 + "mmol/l  \n血压：" + objinfo.血压右侧1 + "/" + objinfo.血压右侧2
                            + "  视力（左）：" + objinfo.左眼视力 + "/" + objinfo.左眼矫正 + "  视力（右）：" + objinfo.右眼视力 + "/" + objinfo.右眼矫正;
                        result.Add(s_体检内容);
                        XtraReport体检回执单 xrpt = new XtraReport体检回执单(result);
                        xrpt.ShowPreviewDialog();
                    }
                }
                else
                {
                    XtraMessageBox.Show("没有可打印的内容!", "系统提示");                    
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("未发现打印设备!", "系统提示" + ex.Message);
            }
        }
        
        private void btn直接打印_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.DataSource != null && gridView1.DataRowCount > 0)
                {//((System.Data.DataView)(gridView1.DataSource)).Table
                    DataRow dr = gridView1.GetFocusedDataRow();
                    tb_健康体检Info objinfo = new tb_健康体检Info();
                    objinfo = tb_健康体检DAL.Gettb_健康体检InfoById(dr["ID"].ToString());
                    if (objinfo != null)
                    {
                        string text = Calculate(objinfo);
                        List<string> result = new List<string>();
                        result.Add(objinfo.姓名);
                        result.Add(objinfo.性别);
                        result.Add(objinfo.出生日期);
                        result.Add(objinfo.身份证号);
                        result.Add(text);
                        result.Add(objinfo.体检日期);

                        XtraReport体检回执单 xrpt = new XtraReport体检回执单(result);
                        xrpt.Print();
                    }
                }
                else
                {
                    XtraMessageBox.Show("没有可打印的内容!", "系统提示");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("未发现打印设备!", "系统提示" + ex.Message);
            }
        }

        private string Calculate(tb_健康体检Info info)
        {
            StringBuilder builder = new StringBuilder();

            #region BMI
            //BMI
            /*
                过轻：低于18.5
                正常：18.5-23.9
                超重：≥24
             * 24～27.9 偏胖
                肥胖：28-32
                非常肥胖, 高于32
             */
            if (!string.IsNullOrEmpty(info.体重指数))
            {
                double bmi = double.Parse(info.体重指数);
                if (bmi < 18.5)
                {
                    builder.AppendLine("[体重偏轻]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议：太瘦也有一定的原因：饮食不当、减肥过度、疾病因素，瘦人脂肪过少，更易发生胃下垂、肾下垂，生病后恢复的速度也较常人慢；饮食可一直消瘦，一定要检查排除疾病引起的。");
                }
                else if (bmi >= 24)
                {
                    builder.AppendLine("[体重超重]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议：\n1.控制好饮食量，不能没有饥饱的进食；\n2.注意饮食习惯，少吃肉类、油炸类、含糖量较高的食物，还有少吃盐和辣、高热量如巧克力、面包、可乐等；\n3.饭后适量运动；\n4.多参加消耗大量体能的运动。");
                }
                else if (bmi >= 24 && bmi < 27.9)
                {
                    builder.AppendLine("[体重偏胖]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议：\n1.控制好饮食量，不能没有饥饱的进食；\n2.注意饮食习惯，少吃肉类、油炸类、含糖量较高的食物，还有少吃盐和辣、高热量如巧克力、面包、可乐等；\n3.饭后适量运动；\n4.多参加消耗大量体能的运动。");
                }
                else if (bmi >= 28 && bmi < 32)
                {
                    builder.AppendLine("[体重肥胖]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议： <是指身高和体重比例失衡，不健康的肥胖，体重超重往往会引起很多疾病发生。>指导：\n1.控制好饮食量，不能没有饥饱的进食；\n2.注意饮食习惯，少吃肉类、油炸类、含糖量较高的食物，还有少吃盐和辣、高热量如巧克力、面包、可乐等；\n3.饭后适量运动；\n4.多参加消耗大量体能的运动。");
                }
                else if (bmi > 32)
                {
                    builder.AppendLine("[体重非常肥胖]（体重指数：" + bmi + ")");
                    builder.AppendLine("  建议：<是指身高和体重比例失衡，不健康的肥胖，体重超重往往会引起很多疾病发生。>指导：\n1.控制好饮食量，不能没有饥饱的进食；\n2.注意饮食习惯，少吃肉类、油炸类、含糖量较高的食物，还有少吃盐和辣、高热量如巧克力、面包、可乐等；\n3.饭后适量运动；\n4.多参加消耗大量体能的运动。");
                }
            }

            #endregion

            #region 糖尿病

            if (!string.IsNullOrEmpty(info.空腹血糖))
            {
                string xuetang = info.空腹血糖;
                decimal _xuetang = 0m;
                if (Decimal.TryParse(xuetang, out _xuetang))
                {
                    if (_xuetang != 0m && (_xuetang > 6.40m || _xuetang < 3.89m))
                    {
                        builder.AppendLine("[血糖偏高](GLU:" + _xuetang + "mmol/l)（3.89-6.40）");
                        builder.AppendLine("  建议复查空腹血糖。建议：\n1、糖尿病者积极配合医院规范化治疗；\n2、接受多种形式的健康教育知识；\n3、调整饮食，控制热量的摄入，按医嘱进行自我监测；\n4、成人35岁以后都应每年查血糖和尿糖，早发现早治疗；\n5、改变不良生活方式，忌烟酒，加强运动；\n6、保持心态平衡，克服各种紧张压力，培养高尚情操，多爬山郊游等；\n7、定期复查。");
                    }
                }
            }

            #endregion

            #region 血压

            if (!string.IsNullOrEmpty(info.血压右侧1) || !string.IsNullOrEmpty(info.血压左侧1))
            {
                //右侧
                if (!string.IsNullOrEmpty(info.血压右侧1))
                {
                    string you1 = info.血压右侧1;
                    string you2 = info.血压右侧2;
                    int _you1; int _you2;
                    if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
                    {
                        if (_you1 > 140 || _you2 > 90)
                        {
                            builder.AppendLine("[血压偏高]（" + you1 + "/" + you2 + ")");
                            builder.AppendLine("  建议连续三日测血压以明确诊断。建议：\n1、注意劳逸结合，保持心境平和；\n2、坚持有氧运动；\n3、戒烟限酒；\n4、少吃盐；\n5、控制体重。\n治疗：上述非药物治疗后血压仍高者，应考虑用降压药。定期测量血压。");
                        }
                    }
                }
                //左侧
                else if (!string.IsNullOrEmpty(info.血压左侧1))
                {
                    string you1 = info.血压左侧1;
                    string you2 = info.血压左侧2;
                    int _you1; int _you2;
                    if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
                    {
                        if (_you1 > 140 || _you2 > 90)
                        {
                            builder.AppendLine("[血压偏高]（" + you1 + "/" + you2 + ")");
                            builder.AppendLine("  建议连续三日测血压以明确诊断。建议：\n1、注意劳逸结合，保持心境平和；\n2、坚持有氧运动；\n3、戒烟限酒；\n4、少吃盐；\n5、控制体重。\n治疗：上述非药物治疗后血压仍高者，应考虑用降压药。定期测量血压。");
                        }
                    }
                }
            }

            #endregion

            #region 体温
            //
            /*瞎编的
             */
            if (!string.IsNullOrEmpty(info.体温.ToString()))
            {
                if (info.体温 < 35.5)
                {
                    builder.AppendLine("[体温偏低]（体温：" + info.体温 + ")");
                    builder.AppendLine("  建议：重新测量。");
                }
                else if (info.体温 >= 37)
                {
                    builder.AppendLine("[体温偏高]（体温：" + info.体温 + ")");
                    builder.AppendLine("  建议：重新测量，如体温过高请赶快就医。");
                }
            }

            #endregion

            if (builder.Length == 0)
            {
                builder.AppendLine("体检未见明显异常");
            }

            #region 体质辨识
            //if (!string.IsNullOrEmpty(uc平和质.Txt1.Text) || !string.IsNullOrEmpty(uc气虚质.Txt1.Text) || !string.IsNullOrEmpty(uc阳虚质.Txt1.Text) || !string.IsNullOrEmpty(uc阴虚质.Txt1.Text) || !string.IsNullOrEmpty(uc痰湿质.Txt1.Text) || !string.IsNullOrEmpty(uc湿热质.Txt1.Text) || !string.IsNullOrEmpty(uc血瘀质.Txt1.Text) || !string.IsNullOrEmpty(uc气郁质.Txt1.Text) || !string.IsNullOrEmpty(uc特禀质.Txt1.Text))
            //{
            //    builder.AppendLine("中医体质辨识结果：");
            //    if (!string.IsNullOrEmpty(uc平和质.Txt1.Text))
            //    { builder.AppendLine("平和质：" + uc平和质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc气虚质.Txt1.Text))
            //    { builder.AppendLine("气虚质：" + uc气虚质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc阳虚质.Txt1.Text))
            //    { builder.AppendLine("阳虚质：" + uc阳虚质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc阴虚质.Txt1.Text))
            //    { builder.AppendLine("阴虚质：" + uc阴虚质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc痰湿质.Txt1.Text))
            //    { builder.AppendLine("痰湿质：" + uc痰湿质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc湿热质.Txt1.Text))
            //    { builder.AppendLine("湿热质：" + uc湿热质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc血瘀质.Txt1.Text))
            //    { builder.AppendLine("血瘀质：" + uc血瘀质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc气郁质.Txt1.Text))
            //    { builder.AppendLine("气郁质：" + uc气郁质.Txt1.Text); }
            //    if (!string.IsNullOrEmpty(uc特禀质.Txt1.Text))
            //    { builder.AppendLine("特禀质：" + uc特禀质.Txt1.Text); }
            //}

            #endregion

            return builder.ToString();
        }


        //导出Excel
        private void simpleBtnExportExcel_Click(object sender, EventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                XtraMessageBox.Show("目前没有数据可以导出!", "系统提示");
                return;
            }

            PublicClass pub = new PublicClass();

            System.Data.DataTable datTable = new DataTable();
            DataColumn dc1 = new DataColumn("序号");
            DataColumn dc2 = new DataColumn("收到时间");
            DataColumn dc3 = new DataColumn("来文机关");
            DataColumn dc4 = new DataColumn("文号");
            DataColumn dc5 = new DataColumn("文件名称");
            DataColumn dc6 = new DataColumn("批办领导、科室");
            DataColumn dc7 = new DataColumn("收到文件数");
            DataColumn dc8 = new DataColumn("备注");

            datTable.Columns.Add(dc1);
            datTable.Columns.Add(dc2);
            datTable.Columns.Add(dc3);
            datTable.Columns.Add(dc4);
            datTable.Columns.Add(dc5);
            datTable.Columns.Add(dc6);
            datTable.Columns.Add(dc7);
            datTable.Columns.Add(dc8);

            for (int i = 0; i < ExportDataTable.Rows.Count; i++)
            {
                DataRow datRow = datTable.NewRow();//创建新行              
                datRow[0] = ExportDataTable.Rows[i]["序号"];
                datRow[1] = ExportDataTable.Rows[i]["收到时间"];
                datRow[2] = ExportDataTable.Rows[i]["来文机关"];
                datRow[3] = ExportDataTable.Rows[i]["文号"];
                datRow[4] = ExportDataTable.Rows[i]["文件名称"];
                datRow[5] = ExportDataTable.Rows[i]["批办领导、科室"];
                datRow[6] = ExportDataTable.Rows[i]["收到文件数"];
                datRow[7] = ExportDataTable.Rows[i]["备注"];
                datTable.Rows.Add(datRow);
            }
            
            pub.ExportToExcel(datTable);
        }

        private System.Data.DataTable ExportDataTable = new DataTable();

        /// <summary>
        /// 绑定值
        /// </summary>
        private void BindData(string d开始日期 = "", string d结束日期 = "", string str姓名 = "", string str身份证号 = "",string str家庭地址="")
        {
            System.Data.DataTable datTable = new DataTable();
            #region 创建表头
            DataColumn dc1 = new DataColumn("选择", typeof(Boolean));
            DataColumn dc2 = new DataColumn("ID", typeof(int));
            DataColumn dc3 = new DataColumn("序号", typeof(int));
            DataColumn dc4 = new DataColumn("姓名");
            DataColumn dc5 = new DataColumn("性别");
            DataColumn dc6 = new DataColumn("出生日期");
            DataColumn dc7 = new DataColumn("身份证号");
            DataColumn dc8 = new DataColumn("打印状态");
            //DataColumn dc9 = new DataColumn("备注");
            //DataColumn dc10 = new DataColumn("创建人");
            //DataColumn dc11 = new DataColumn("创建时间");
            //DataColumn dc12 = new DataColumn("修改人");
            //DataColumn dc13 = new DataColumn("修改日期"); ;

            datTable.Columns.Add(dc1);
            datTable.Columns.Add(dc2);
            datTable.Columns.Add(dc3);
            datTable.Columns.Add(dc4);
            datTable.Columns.Add(dc5);
            datTable.Columns.Add(dc6);
            datTable.Columns.Add(dc7);
            datTable.Columns.Add(dc8);
            //datTable.Columns.Add(dc9);
            //datTable.Columns.Add(dc10);
            //datTable.Columns.Add(dc11);
            //datTable.Columns.Add(dc12);
            //datTable.Columns.Add(dc13);
            #endregion

            #region 绑定
            SqliteClass sqldb = new SqliteClass();
            List<int> listid = new List<int>();
            listid = sqldb.Get健康体检ID(d开始日期, d结束日期, str姓名, str身份证号, str家庭地址);
            
            if (listid.Count > 0)
            {
                for (int i = 0; i < listid.Count; i++)
                {
                    tb_健康体检Info objinfo = new tb_健康体检Info();
                    objinfo = tb_健康体检DAL.Gettb_健康体检InfoById(listid[i].ToString());
                    DataRow datRow = datTable.NewRow();//创建新行
                    datRow[0] = false;
                    datRow[1] = listid[i];
                    datRow[2] = i + 1;
                    datRow[3] = objinfo.姓名;
                    datRow[4] = objinfo.性别;
                    datRow[5] = objinfo.出生日期;
                    datRow[6] = objinfo.身份证号;
                    //datRow[7] = objinfo.批办领导;
                    //datRow[8] = objinfo.收到文件数.ToString();
                    datTable.Rows.Add(datRow);
                }
                //DataTable按照排序字段进行升序排列
                datTable.DefaultView.Sort = "序号 asc";
                DataTable dtT = datTable.DefaultView.ToTable();
                datTable.Clear();
                datTable = dtT;
                ExportDataTable = datTable;

                this.gridControl1.DataSource = new DataView(datTable);
                this.gridControl1.Refresh();
                this.gridControl1.RefreshDataSource();

                this.txtMoneyAll.Text = datTable.Rows.Count.ToString();//
            }
            else
            {
                this.gridControl1.DataSource = new DataView(datTable);
                this.gridControl1.Refresh();
                this.gridControl1.RefreshDataSource();
            } 
            #endregion

            #region grid 样式
            //设置列宽
            this.gridView1.Columns[0].Width = 28;
            this.gridView1.Columns[2].Width = 30;
            this.gridView1.Columns[3].Width = 75;
            this.gridView1.Columns[3].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[4].Width = 150;
            this.gridView1.Columns[4].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[5].Width = 150;
            this.gridView1.Columns[5].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[6].Width = 300;
            this.gridView1.Columns[6].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[7].Width = 80;
            this.gridView1.Columns[7].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            for (int i = 0; i < gridView1.Columns.Count; i++)
            {
                this.gridView1.Columns[i].AppearanceCell.Font = new Font("宋体", 25, FontStyle.Bold);
            }

            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.RowHeight = 40;
            //隐藏列
            this.gridView1.Columns[1].Visible = false;
            //this.gridView1.Columns[8].Visible = false;
            //this.gridView1.Columns[9].Visible = false;
            //this.gridView1.Columns[10].Visible = false;
            //this.gridView1.Columns[11].Visible = false;
            //this.gridView1.Columns[12].Visible = false;
            //this.gridView1.Columns[13].Visible = false;
            //this.gridView1.Columns[14].Visible = false;

            //设置列不允许编辑
            this.gridView1.Columns[2].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[3].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[4].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[5].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[6].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[7].OptionsColumn.AllowEdit = false;
            //this.gridView1.Columns[8].OptionsColumn.AllowEdit = false;
            //this.gridView1.Columns[9].OptionsColumn.AllowEdit = false;
            //this.gridView1.Columns[10].OptionsColumn.AllowEdit = false;
            //this.gridView1.Columns[11].OptionsColumn.AllowEdit = false;
            //this.gridView1.Columns[12].OptionsColumn.AllowEdit = false;
            //this.gridView1.Columns[13].OptionsColumn.AllowEdit = false;
            //this.gridView1.Columns[14].OptionsColumn.AllowEdit = false;
            #endregion
        }

        #region gridEvent
        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                string str = "暂无数据!";
                Font f = new Font("宋体", 25, FontStyle.Bold);
                Rectangle r = new Rectangle(e.Bounds.Left + 25, e.Bounds.Top + 5, e.Bounds.Width - 5, e.Bounds.Height - 5);
                e.Graphics.DrawString(str, f, Brushes.Black, r);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            FormClass frmClass = new FormClass();
            int nCurRow = 0;
            nCurRow = frmClass.XGridViewMouseDown(this.gridView1, e, 0);
        } 
        #endregion

        private void btn中医_Click(object sender, EventArgs e)
        {

        }


    }
}
