﻿namespace DXTempLate.Controls
{
    partial class XUC_GSGZDTJ
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XUC_GSGZDTJ));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControlTop = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txt文号 = new DevExpress.XtraEditors.TextEdit();
            this.txt文件名称 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txt批办领导 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleBtnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.txt结束日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt开始日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelUser = new DevExpress.XtraEditors.LabelControl();
            this.txt来文机关 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControlButtom = new DevExpress.XtraEditors.PanelControl();
            this.txtMoneyAll = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleBtnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnExportExcel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTop)).BeginInit();
            this.panelControlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt文号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt文件名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt批办领导.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结束日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结束日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt来文机关.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlButtom)).BeginInit();
            this.panelControlButtom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoneyAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Controls.Add(this.panelControlTop);
            this.groupControl1.Controls.Add(this.panelControlButtom);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(996, 482);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "体检人员管理-> 查询";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 92);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(992, 360);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // panelControlTop
            // 
            this.panelControlTop.Controls.Add(this.labelControl7);
            this.panelControlTop.Controls.Add(this.txt文号);
            this.panelControlTop.Controls.Add(this.txt文件名称);
            this.panelControlTop.Controls.Add(this.labelControl5);
            this.panelControlTop.Controls.Add(this.labelControl6);
            this.panelControlTop.Controls.Add(this.labelControl4);
            this.panelControlTop.Controls.Add(this.txt批办领导);
            this.panelControlTop.Controls.Add(this.simpleBtnQuery);
            this.panelControlTop.Controls.Add(this.txt结束日期);
            this.panelControlTop.Controls.Add(this.labelControl2);
            this.panelControlTop.Controls.Add(this.labelControl1);
            this.panelControlTop.Controls.Add(this.txt开始日期);
            this.panelControlTop.Controls.Add(this.labelUser);
            this.panelControlTop.Controls.Add(this.txt来文机关);
            this.panelControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControlTop.Location = new System.Drawing.Point(2, 22);
            this.panelControlTop.Name = "panelControlTop";
            this.panelControlTop.Size = new System.Drawing.Size(992, 70);
            this.panelControlTop.TabIndex = 4;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl7.Location = new System.Drawing.Point(783, 41);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(82, 19);
            this.labelControl7.TabIndex = 30;
            this.labelControl7.Text = "(模糊查询)";
            // 
            // txt文号
            // 
            this.txt文号.Location = new System.Drawing.Point(612, 38);
            this.txt文号.Name = "txt文号";
            this.txt文号.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt文号.Properties.Appearance.Options.UseFont = true;
            this.txt文号.Size = new System.Drawing.Size(155, 26);
            this.txt文号.TabIndex = 29;
            // 
            // txt文件名称
            // 
            this.txt文件名称.Location = new System.Drawing.Point(405, 38);
            this.txt文件名称.Name = "txt文件名称";
            this.txt文件名称.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt文件名称.Properties.Appearance.Options.UseFont = true;
            this.txt文件名称.Size = new System.Drawing.Size(155, 26);
            this.txt文件名称.TabIndex = 29;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl5.Location = new System.Drawing.Point(566, 41);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(38, 19);
            this.labelControl5.TabIndex = 28;
            this.labelControl5.Text = "地址:";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl6.Location = new System.Drawing.Point(296, 41);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(70, 19);
            this.labelControl6.TabIndex = 28;
            this.labelControl6.Text = "身份证号:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl4.Location = new System.Drawing.Point(77, 41);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(38, 19);
            this.labelControl4.TabIndex = 25;
            this.labelControl4.Text = "性别:";
            // 
            // txt批办领导
            // 
            this.txt批办领导.Location = new System.Drawing.Point(125, 38);
            this.txt批办领导.Name = "txt批办领导";
            this.txt批办领导.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt批办领导.Properties.Appearance.Options.UseFont = true;
            this.txt批办领导.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt批办领导.Size = new System.Drawing.Size(155, 26);
            this.txt批办领导.TabIndex = 24;
            // 
            // simpleBtnQuery
            // 
            this.simpleBtnQuery.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnQuery.Image")));
            this.simpleBtnQuery.Location = new System.Drawing.Point(783, 8);
            this.simpleBtnQuery.Name = "simpleBtnQuery";
            this.simpleBtnQuery.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnQuery.TabIndex = 23;
            this.simpleBtnQuery.Text = "查询";
            this.simpleBtnQuery.Click += new System.EventHandler(this.simpleBtnQuery_Click);
            // 
            // txt结束日期
            // 
            this.txt结束日期.EditValue = null;
            this.txt结束日期.Location = new System.Drawing.Point(612, 7);
            this.txt结束日期.Name = "txt结束日期";
            this.txt结束日期.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt结束日期.Properties.Appearance.Options.UseFont = true;
            this.txt结束日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt结束日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt结束日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt结束日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt结束日期.Size = new System.Drawing.Size(155, 26);
            this.txt结束日期.TabIndex = 22;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Location = new System.Drawing.Point(566, 11);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(40, 19);
            this.labelControl2.TabIndex = 21;
            this.labelControl2.Text = "至(≤)";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(296, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(94, 19);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "体检日期:(≥)";
            // 
            // txt开始日期
            // 
            this.txt开始日期.EditValue = null;
            this.txt开始日期.Location = new System.Drawing.Point(405, 7);
            this.txt开始日期.Name = "txt开始日期";
            this.txt开始日期.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt开始日期.Properties.Appearance.Options.UseFont = true;
            this.txt开始日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt开始日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt开始日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt开始日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt开始日期.Size = new System.Drawing.Size(155, 26);
            this.txt开始日期.TabIndex = 19;
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelUser.Location = new System.Drawing.Point(77, 8);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(38, 19);
            this.labelUser.TabIndex = 3;
            this.labelUser.Text = "姓名:";
            // 
            // txt来文机关
            // 
            this.txt来文机关.Location = new System.Drawing.Point(125, 7);
            this.txt来文机关.Name = "txt来文机关";
            this.txt来文机关.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt来文机关.Properties.Appearance.Options.UseFont = true;
            this.txt来文机关.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt来文机关.Size = new System.Drawing.Size(155, 26);
            this.txt来文机关.TabIndex = 2;
            // 
            // panelControlButtom
            // 
            this.panelControlButtom.Controls.Add(this.txtMoneyAll);
            this.panelControlButtom.Controls.Add(this.labelControl3);
            this.panelControlButtom.Controls.Add(this.simpleBtnPrint);
            this.panelControlButtom.Controls.Add(this.simpleBtnExportExcel);
            this.panelControlButtom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControlButtom.Location = new System.Drawing.Point(2, 452);
            this.panelControlButtom.Name = "panelControlButtom";
            this.panelControlButtom.Size = new System.Drawing.Size(992, 28);
            this.panelControlButtom.TabIndex = 3;
            // 
            // txtMoneyAll
            // 
            this.txtMoneyAll.Location = new System.Drawing.Point(81, 2);
            this.txtMoneyAll.Name = "txtMoneyAll";
            this.txtMoneyAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtMoneyAll.Properties.Appearance.Options.UseFont = true;
            this.txtMoneyAll.Properties.ReadOnly = true;
            this.txtMoneyAll.Size = new System.Drawing.Size(66, 24);
            this.txtMoneyAll.TabIndex = 27;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl3.Location = new System.Drawing.Point(4, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(40, 19);
            this.labelControl3.TabIndex = 26;
            this.labelControl3.Text = "总数:";
            // 
            // simpleBtnPrint
            // 
            this.simpleBtnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnPrint.Image")));
            this.simpleBtnPrint.Location = new System.Drawing.Point(820, 3);
            this.simpleBtnPrint.Name = "simpleBtnPrint";
            this.simpleBtnPrint.Size = new System.Drawing.Size(79, 23);
            this.simpleBtnPrint.TabIndex = 25;
            this.simpleBtnPrint.Text = "打印预览";
            this.simpleBtnPrint.Click += new System.EventHandler(this.simpleBtnPrint_Click);
            // 
            // simpleBtnExportExcel
            // 
            this.simpleBtnExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnExportExcel.Image")));
            this.simpleBtnExportExcel.Location = new System.Drawing.Point(905, 3);
            this.simpleBtnExportExcel.Name = "simpleBtnExportExcel";
            this.simpleBtnExportExcel.Size = new System.Drawing.Size(79, 23);
            this.simpleBtnExportExcel.TabIndex = 24;
            this.simpleBtnExportExcel.Text = "导出Excel";
            this.simpleBtnExportExcel.Click += new System.EventHandler(this.simpleBtnExportExcel_Click);
            // 
            // XUC_GSGZDTJ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Name = "XUC_GSGZDTJ";
            this.Size = new System.Drawing.Size(996, 482);
            this.Load += new System.EventHandler(this.XUC_GSGZDTJ_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlTop)).EndInit();
            this.panelControlTop.ResumeLayout(false);
            this.panelControlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt文号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt文件名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt批办领导.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结束日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结束日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt来文机关.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlButtom)).EndInit();
            this.panelControlButtom.ResumeLayout(false);
            this.panelControlButtom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMoneyAll.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControlTop;
        private DevExpress.XtraEditors.SimpleButton simpleBtnQuery;
        private DevExpress.XtraEditors.DateEdit txt结束日期;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit txt开始日期;
        private DevExpress.XtraEditors.LabelControl labelUser;
        private DevExpress.XtraEditors.ComboBoxEdit txt来文机关;
        private DevExpress.XtraEditors.PanelControl panelControlButtom;
        private DevExpress.XtraEditors.SimpleButton simpleBtnExportExcel;
        private DevExpress.XtraEditors.TextEdit txt文件名称;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.ComboBoxEdit txt批办领导;
        private DevExpress.XtraEditors.SimpleButton simpleBtnPrint;
        private DevExpress.XtraEditors.TextEdit txtMoneyAll;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txt文号;
        private DevExpress.XtraEditors.LabelControl labelControl5;
    }
}
