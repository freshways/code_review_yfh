﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DXTempLate.Controls
{
    public partial class XtraUserControlIndex : DevExpress.XtraEditors.XtraUserControl
    {
        public XtraUserControlIndex()
        {
            InitializeComponent();
        }

        #region 委托/事件

        public delegate void OpenEventHandler(string Types); //返回结果

        public event OpenEventHandler OpenOptions;

        #endregion

        private static XtraUserControlIndex _ItSelf;
        public static XtraUserControlIndex ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XtraUserControlIndex();
                }
                return _ItSelf;
            }
        }

        private void XtraUserControlIndex_Load(object sender, EventArgs e)
        {

        }

        private void btn成人体检_Click(object sender, EventArgs e)
        {
            if (OpenOptions != null)
                OpenOptions("成人体检");
        }

        private void btn儿童体检_Click(object sender, EventArgs e)
        {
            if (OpenOptions != null)
                OpenOptions("儿童体检");
        }

        private void btn中医体质辨识_Click(object sender, EventArgs e)
        {
            if (OpenOptions != null)
                OpenOptions("中医体质辨识");
        }

        private void btn报告单_Click(object sender, EventArgs e)
        {
            if (OpenOptions != null)
                OpenOptions("报告单");
        }

        private void btn体检统计_Click(object sender, EventArgs e)
        {
            
        }
    }
}
