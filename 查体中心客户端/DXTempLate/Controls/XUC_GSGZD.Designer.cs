﻿namespace DXTempLate.Controls
{
    partial class XUC_GSGZD
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XUC_GSGZD));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtAll = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.btn刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBSelAll = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnUpdate = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Controls.Add(this.panelControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(859, 436);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "体检人员管理-> 登记";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 22);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(855, 377);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.txtAll);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Controls.Add(this.simpleBtnPrint);
            this.panelControl2.Controls.Add(this.btn刷新);
            this.panelControl2.Controls.Add(this.simpleBSelAll);
            this.panelControl2.Controls.Add(this.simpleBtnAdd);
            this.panelControl2.Controls.Add(this.simpleBtnDelete);
            this.panelControl2.Controls.Add(this.simpleBtnUpdate);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(2, 399);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(855, 35);
            this.panelControl2.TabIndex = 1;
            // 
            // txtAll
            // 
            this.txtAll.Location = new System.Drawing.Point(82, 7);
            this.txtAll.Name = "txtAll";
            this.txtAll.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtAll.Properties.Appearance.Options.UseFont = true;
            this.txtAll.Properties.ReadOnly = true;
            this.txtAll.Size = new System.Drawing.Size(100, 24);
            this.txtAll.TabIndex = 29;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl3.Location = new System.Drawing.Point(3, 9);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(40, 19);
            this.labelControl3.TabIndex = 28;
            this.labelControl3.Text = "总数:";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(439, 7);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 19;
            this.simpleButton1.Text = "直接打印";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleBtnPrint
            // 
            this.simpleBtnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnPrint.Image")));
            this.simpleBtnPrint.Location = new System.Drawing.Point(531, 7);
            this.simpleBtnPrint.Name = "simpleBtnPrint";
            this.simpleBtnPrint.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnPrint.TabIndex = 18;
            this.simpleBtnPrint.Text = "打印预览";
            this.simpleBtnPrint.Click += new System.EventHandler(this.simpleBtnPrint_Click);
            // 
            // btn刷新
            // 
            this.btn刷新.Image = ((System.Drawing.Image)(resources.GetObject("btn刷新.Image")));
            this.btn刷新.Location = new System.Drawing.Point(269, 7);
            this.btn刷新.Name = "btn刷新";
            this.btn刷新.Size = new System.Drawing.Size(75, 23);
            this.btn刷新.TabIndex = 17;
            this.btn刷新.Text = "刷新";
            this.btn刷新.Click += new System.EventHandler(this.btn刷新_Click);
            // 
            // simpleBSelAll
            // 
            this.simpleBSelAll.Image = ((System.Drawing.Image)(resources.GetObject("simpleBSelAll.Image")));
            this.simpleBSelAll.Location = new System.Drawing.Point(188, 7);
            this.simpleBSelAll.Name = "simpleBSelAll";
            this.simpleBSelAll.Size = new System.Drawing.Size(75, 23);
            this.simpleBSelAll.TabIndex = 17;
            this.simpleBSelAll.Text = "全选";
            this.simpleBSelAll.Click += new System.EventHandler(this.simpleBSelAll_Click);
            // 
            // simpleBtnAdd
            // 
            this.simpleBtnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnAdd.Image")));
            this.simpleBtnAdd.Location = new System.Drawing.Point(612, 7);
            this.simpleBtnAdd.Name = "simpleBtnAdd";
            this.simpleBtnAdd.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnAdd.TabIndex = 16;
            this.simpleBtnAdd.Text = "添加";
            this.simpleBtnAdd.Click += new System.EventHandler(this.simpleBtnAdd_Click);
            // 
            // simpleBtnDelete
            // 
            this.simpleBtnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnDelete.Image")));
            this.simpleBtnDelete.Location = new System.Drawing.Point(774, 7);
            this.simpleBtnDelete.Name = "simpleBtnDelete";
            this.simpleBtnDelete.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnDelete.TabIndex = 15;
            this.simpleBtnDelete.Text = "删除";
            this.simpleBtnDelete.Click += new System.EventHandler(this.simpleBtnDelete_Click);
            // 
            // simpleBtnUpdate
            // 
            this.simpleBtnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleBtnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("simpleBtnUpdate.Image")));
            this.simpleBtnUpdate.Location = new System.Drawing.Point(693, 7);
            this.simpleBtnUpdate.Name = "simpleBtnUpdate";
            this.simpleBtnUpdate.Size = new System.Drawing.Size(75, 23);
            this.simpleBtnUpdate.TabIndex = 14;
            this.simpleBtnUpdate.Text = "修改";
            this.simpleBtnUpdate.Click += new System.EventHandler(this.simpleBtnUpdate_Click);
            // 
            // XUC_GSGZD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Name = "XUC_GSGZD";
            this.Size = new System.Drawing.Size(859, 436);
            this.Load += new System.EventHandler(this.XUC_GSGZD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAll.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleBSelAll;
        private DevExpress.XtraEditors.SimpleButton simpleBtnAdd;
        private DevExpress.XtraEditors.SimpleButton simpleBtnDelete;
        private DevExpress.XtraEditors.SimpleButton simpleBtnUpdate;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton simpleBtnPrint;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.TextEdit txtAll;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btn刷新;
    }
}
