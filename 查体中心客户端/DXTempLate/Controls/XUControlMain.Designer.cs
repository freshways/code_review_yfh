﻿namespace DXTempLate.Controls
{
    partial class XUControlMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btn读卡 = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.txt连续扫码 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.windowsUIView1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.WindowsUIView(this.components);
            this.tileContainer1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.TileContainer(this.components);
            this.documentTile身高体重 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document身高体重 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile视力 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document视力 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile血糖 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document血糖 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile体质辨识 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document体质辨识 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile体温 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document体温 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile血压 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document血压 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.documentTile腰围 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.document腰围 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.directorySearcher1 = new System.DirectoryServices.DirectorySearcher();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt连续扫码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUIView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile身高体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document身高体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile视力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document视力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血糖)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血糖)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体质辨识)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体质辨识)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体温)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体温)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血压)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血压)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile腰围)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document腰围)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("500ba5f5-1285-464f-a95a-06aeccf49aaf");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowFloating = false;
            this.dockPanel1.Options.ShowAutoHideButton = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(382, 200);
            this.dockPanel1.Size = new System.Drawing.Size(382, 554);
            this.dockPanel1.Text = "基本资料";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(374, 527);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btn读卡);
            this.layoutControl2.Controls.Add(this.pictureEdit1);
            this.layoutControl2.Controls.Add(this.txt连续扫码);
            this.layoutControl2.Controls.Add(this.txt姓名);
            this.layoutControl2.Controls.Add(this.txt居住地址);
            this.layoutControl2.Controls.Add(this.txt出生日期);
            this.layoutControl2.Controls.Add(this.txt身份证号);
            this.layoutControl2.Controls.Add(this.txt性别);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(707, 261, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(374, 527);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btn读卡
            // 
            this.btn读卡.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btn读卡.Appearance.Options.UseFont = true;
            this.btn读卡.Location = new System.Drawing.Point(264, 373);
            this.btn读卡.Name = "btn读卡";
            this.btn读卡.Size = new System.Drawing.Size(103, 26);
            this.btn读卡.StyleController = this.layoutControl2;
            this.btn读卡.TabIndex = 10;
            this.btn读卡.Text = "读 卡";
            this.btn读卡.Click += new System.EventHandler(this.btn读卡_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::DXTempLate.Properties.Resources.People;
            this.pictureEdit1.Location = new System.Drawing.Point(7, 7);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(360, 204);
            this.pictureEdit1.StyleController = this.layoutControl2;
            this.pictureEdit1.TabIndex = 25;
            this.pictureEdit1.ToolTip = "可进行上传用户照片";
            // 
            // txt连续扫码
            // 
            this.txt连续扫码.Location = new System.Drawing.Point(58, 373);
            this.txt连续扫码.Name = "txt连续扫码";
            this.txt连续扫码.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.txt连续扫码.Properties.Appearance.Options.UseFont = true;
            this.txt连续扫码.Size = new System.Drawing.Size(202, 28);
            this.txt连续扫码.StyleController = this.layoutControl2;
            this.txt连续扫码.TabIndex = 6;
            // 
            // txt姓名
            // 
            this.txt姓名.EditValue = "11111";
            this.txt姓名.Enabled = false;
            this.txt姓名.Location = new System.Drawing.Point(58, 215);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.txt姓名.Properties.Appearance.Options.UseFont = true;
            this.txt姓名.Properties.AutoHeight = false;
            this.txt姓名.Size = new System.Drawing.Size(138, 36);
            this.txt姓名.StyleController = this.layoutControl2;
            this.txt姓名.TabIndex = 4;
            // 
            // txt居住地址
            // 
            this.txt居住地址.EditValue = "11111";
            this.txt居住地址.Enabled = false;
            this.txt居住地址.Location = new System.Drawing.Point(58, 331);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.txt居住地址.Properties.Appearance.Options.UseFont = true;
            this.txt居住地址.Properties.AutoHeight = false;
            this.txt居住地址.Size = new System.Drawing.Size(309, 38);
            this.txt居住地址.StyleController = this.layoutControl2;
            this.txt居住地址.TabIndex = 8;
            // 
            // txt出生日期
            // 
            this.txt出生日期.EditValue = "11111";
            this.txt出生日期.Enabled = false;
            this.txt出生日期.Location = new System.Drawing.Point(58, 255);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.txt出生日期.Properties.Appearance.Options.UseFont = true;
            this.txt出生日期.Properties.AutoHeight = false;
            this.txt出生日期.Size = new System.Drawing.Size(309, 34);
            this.txt出生日期.StyleController = this.layoutControl2;
            this.txt出生日期.TabIndex = 9;
            // 
            // txt身份证号
            // 
            this.txt身份证号.EditValue = "11111";
            this.txt身份证号.Enabled = false;
            this.txt身份证号.Location = new System.Drawing.Point(58, 293);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.txt身份证号.Properties.Appearance.Options.UseFont = true;
            this.txt身份证号.Properties.AutoHeight = false;
            this.txt身份证号.Size = new System.Drawing.Size(309, 34);
            this.txt身份证号.StyleController = this.layoutControl2;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt性别
            // 
            this.txt性别.EditValue = "11111";
            this.txt性别.Enabled = false;
            this.txt性别.Location = new System.Drawing.Point(251, 215);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.txt性别.Properties.Appearance.Options.UseFont = true;
            this.txt性别.Properties.AutoHeight = false;
            this.txt性别.Size = new System.Drawing.Size(116, 36);
            this.txt性别.StyleController = this.layoutControl2;
            this.txt性别.TabIndex = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(374, 527);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.pictureEdit1;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(364, 208);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt姓名;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 208);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(193, 40);
            this.layoutControlItem1.Text = "姓名";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt性别;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(193, 208);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(171, 40);
            this.layoutControlItem2.Text = "性别";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txt出生日期;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 248);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(364, 38);
            this.layoutControlItem6.Text = "出生日期";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 286);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(364, 38);
            this.layoutControlItem4.Text = "身份证号";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt居住地址;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 324);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(364, 42);
            this.layoutControlItem5.Text = "居住地址";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt连续扫码;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 366);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(257, 32);
            this.layoutControlItem7.Text = "连续扫码";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btn读卡;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(257, 366);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(107, 32);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 398);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(364, 119);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // documentManager1
            // 
            this.documentManager1.ContainerControl = this;
            this.documentManager1.View = this.windowsUIView1;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.windowsUIView1});
            // 
            // windowsUIView1
            // 
            this.windowsUIView1.ContentContainers.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.IContentContainer[] {
            this.tileContainer1});
            this.windowsUIView1.Documents.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseDocument[] {
            this.document体温,
            this.document血糖,
            this.document血压,
            this.document身高体重,
            this.document视力,
            this.document体质辨识,
            this.document腰围});
            this.windowsUIView1.Tiles.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.BaseTile[] {
            this.documentTile体温,
            this.documentTile血糖,
            this.documentTile血压,
            this.documentTile身高体重,
            this.documentTile视力,
            this.documentTile体质辨识,
            this.documentTile腰围});
            this.windowsUIView1.QueryControl += new DevExpress.XtraBars.Docking2010.Views.QueryControlEventHandler(this.windowsUIView1_QueryControl);
            // 
            // tileContainer1
            // 
            this.tileContainer1.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.BaseTile[] {
            this.documentTile身高体重,
            this.documentTile视力,
            this.documentTile血糖,
            this.documentTile体质辨识,
            this.documentTile体温,
            this.documentTile血压,
            this.documentTile腰围});
            this.tileContainer1.Name = "tileContainer1";
            this.tileContainer1.Properties.HeaderOffset = -20;
            this.tileContainer1.Properties.IndentBetweenItems = 4;
            // 
            // documentTile身高体重
            // 
            this.documentTile身高体重.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(188)))), ((int)(((byte)(154)))));
            this.documentTile身高体重.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile身高体重.Document = this.document身高体重;
            tileItemElement1.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 15F);
            tileItemElement1.Appearance.Normal.Options.UseFont = true;
            tileItemElement1.Image = global::DXTempLate.Properties.Resources.身高体重;
            tileItemElement1.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement1.Text = "身高体重";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile身高体重.Elements.Add(tileItemElement1);
            this.documentTile身高体重.Group = "";
            this.tileContainer1.SetID(this.documentTile身高体重, 3);
            this.documentTile身高体重.Name = "document4Tile";
            this.documentTile身高体重.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document身高体重
            // 
            this.document身高体重.Caption = "身高体重";
            this.document身高体重.ControlName = "身高体重";
            // 
            // documentTile视力
            // 
            this.documentTile视力.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.documentTile视力.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile视力.Document = this.document视力;
            tileItemElement2.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 15F);
            tileItemElement2.Appearance.Normal.Options.UseFont = true;
            tileItemElement2.Image = global::DXTempLate.Properties.Resources.视力;
            tileItemElement2.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement2.Text = "视力";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile视力.Elements.Add(tileItemElement2);
            this.documentTile视力.Group = "";
            this.tileContainer1.SetID(this.documentTile视力, 4);
            this.documentTile视力.Name = "document5Tile";
            this.documentTile视力.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document视力
            // 
            this.document视力.Caption = "视力";
            this.document视力.ControlName = "视力";
            // 
            // documentTile血糖
            // 
            this.documentTile血糖.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(0)))));
            this.documentTile血糖.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile血糖.Document = this.document血糖;
            tileItemElement3.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 15F);
            tileItemElement3.Appearance.Normal.Options.UseFont = true;
            tileItemElement3.Image = global::DXTempLate.Properties.Resources.血糖;
            tileItemElement3.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement3.Text = "血糖";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile血糖.Elements.Add(tileItemElement3);
            this.tileContainer1.SetID(this.documentTile血糖, 1);
            this.documentTile血糖.Name = "document2Tile";
            this.documentTile血糖.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document血糖
            // 
            this.document血糖.Caption = "血糖";
            this.document血糖.ControlName = "血糖";
            // 
            // documentTile体质辨识
            // 
            this.documentTile体质辨识.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(76)))), ((int)(((byte)(61)))));
            this.documentTile体质辨识.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile体质辨识.Document = this.document体质辨识;
            tileItemElement4.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 15F);
            tileItemElement4.Appearance.Normal.Options.UseFont = true;
            tileItemElement4.Image = global::DXTempLate.Properties.Resources.中医体质辨识;
            tileItemElement4.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement4.Text = "体质辨识";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile体质辨识.Elements.Add(tileItemElement4);
            this.documentTile体质辨识.Group = "";
            this.tileContainer1.SetID(this.documentTile体质辨识, 5);
            this.documentTile体质辨识.Name = "document1Tile";
            this.documentTile体质辨识.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document体质辨识
            // 
            this.document体质辨识.Caption = "体质辨识";
            this.document体质辨识.ControlName = "体质辨识";
            // 
            // documentTile体温
            // 
            this.documentTile体温.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(200)))), ((int)(((byte)(83)))));
            this.documentTile体温.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile体温.Document = this.document体温;
            tileItemElement5.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 15F);
            tileItemElement5.Appearance.Normal.Options.UseFont = true;
            tileItemElement5.Image = global::DXTempLate.Properties.Resources.体温;
            tileItemElement5.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement5.Text = "体温";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile体温.Elements.Add(tileItemElement5);
            this.documentTile体温.Group = "";
            this.tileContainer1.SetID(this.documentTile体温, 4);
            this.documentTile体温.Name = "document1Tile";
            this.documentTile体温.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document体温
            // 
            this.document体温.Caption = "体温";
            this.document体温.ControlName = "体温";
            // 
            // documentTile血压
            // 
            this.documentTile血压.Appearances.Normal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(88)))), ((int)(((byte)(181)))));
            this.documentTile血压.Appearances.Normal.Options.UseBackColor = true;
            this.documentTile血压.Document = this.document血压;
            tileItemElement6.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 15F);
            tileItemElement6.Appearance.Normal.Options.UseFont = true;
            tileItemElement6.Image = global::DXTempLate.Properties.Resources.血压;
            tileItemElement6.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement6.Text = "血压";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile血压.Elements.Add(tileItemElement6);
            this.documentTile血压.Group = "";
            this.tileContainer1.SetID(this.documentTile血压, 2);
            this.documentTile血压.Name = "document3Tile";
            this.documentTile血压.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document血压
            // 
            this.document血压.Caption = "血压";
            this.document血压.ControlName = "血压";
            // 
            // documentTile腰围
            // 
            this.documentTile腰围.Document = this.document腰围;
            tileItemElement7.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 15F);
            tileItemElement7.Appearance.Normal.Options.UseFont = true;
            tileItemElement7.Image = global::DXTempLate.Properties.Resources.随访;
            tileItemElement7.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement7.Text = "腰围";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.documentTile腰围.Elements.Add(tileItemElement7);
            this.documentTile腰围.Group = "";
            this.tileContainer1.SetID(this.documentTile腰围, 6);
            this.documentTile腰围.Name = "document2Tile";
            this.documentTile腰围.Properties.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            // 
            // document腰围
            // 
            this.document腰围.Caption = "腰围";
            this.document腰围.ControlName = "腰围";
            // 
            // directorySearcher1
            // 
            this.directorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // XUControlMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dockPanel1);
            this.Name = "XUControlMain";
            this.Size = new System.Drawing.Size(1148, 554);
            this.Load += new System.EventHandler(this.XUControlMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt连续扫码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUIView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile身高体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document身高体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile视力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document视力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血糖)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血糖)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体质辨识)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体质辨识)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile体温)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document体温)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile血压)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document血压)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentTile腰围)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document腰围)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.SimpleButton btn读卡;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.TextEdit txt连续扫码;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.WindowsUIView windowsUIView1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.TileContainer tileContainer1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile体温;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document体温;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile血糖;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document血糖;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile血压;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document血压;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile身高体重;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document身高体重;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile视力;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document视力;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile体质辨识;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document体质辨识;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile documentTile腰围;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document document腰围;
        private System.DirectoryServices.DirectorySearcher directorySearcher1;



    }
}
