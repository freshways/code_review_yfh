﻿namespace DXTempLate.Controls
{
    partial class XUControlMainMap
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxYear = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxMonth = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblMoneyOutType2 = new DevExpress.XtraEditors.LabelControl();
            this.lblMoneyOutType = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtSumGSMoney = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.simpleBtnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.simpleBtnYear = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtSumGSMoneyIN = new DevExpress.XtraEditors.TextEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.txtSumGSMoneyOUT = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumGSMoney.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumGSMoneyIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumGSMoneyOUT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Location = new System.Drawing.Point(25, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(70, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "统计年份:";
            // 
            // comboBoxYear
            // 
            this.comboBoxYear.Location = new System.Drawing.Point(102, 8);
            this.comboBoxYear.Name = "comboBoxYear";
            this.comboBoxYear.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.comboBoxYear.Properties.Appearance.Options.UseFont = true;
            this.comboBoxYear.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxYear.Size = new System.Drawing.Size(100, 26);
            this.comboBoxYear.TabIndex = 1;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(101, 87);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.comboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Size = new System.Drawing.Size(100, 26);
            this.comboBoxEdit1.TabIndex = 5;
            this.comboBoxEdit1.SelectedValueChanged += new System.EventHandler(this.comboBoxEdit1_SelectedValueChanged);
            this.comboBoxEdit1.Click += new System.EventHandler(this.comboBoxEdit1_SelectedValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Location = new System.Drawing.Point(25, 49);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 19);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "统计月份:";
            // 
            // comboBoxMonth
            // 
            this.comboBoxMonth.EditValue = "0";
            this.comboBoxMonth.Location = new System.Drawing.Point(102, 48);
            this.comboBoxMonth.Name = "comboBoxMonth";
            this.comboBoxMonth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.comboBoxMonth.Properties.Appearance.Options.UseFont = true;
            this.comboBoxMonth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxMonth.Properties.DropDownRows = 13;
            this.comboBoxMonth.Properties.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboBoxMonth.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxMonth.Size = new System.Drawing.Size(100, 26);
            this.comboBoxMonth.TabIndex = 3;
            // 
            // lblMoneyOutType2
            // 
            this.lblMoneyOutType2.Location = new System.Drawing.Point(10, 177);
            this.lblMoneyOutType2.Name = "lblMoneyOutType2";
            this.lblMoneyOutType2.Size = new System.Drawing.Size(107, 14);
            this.lblMoneyOutType2.TabIndex = 15;
            this.lblMoneyOutType2.Text = "【管理费用支出:0】";
            // 
            // lblMoneyOutType
            // 
            this.lblMoneyOutType.Location = new System.Drawing.Point(10, 157);
            this.lblMoneyOutType.Name = "lblMoneyOutType";
            this.lblMoneyOutType.Size = new System.Drawing.Size(107, 14);
            this.lblMoneyOutType.TabIndex = 14;
            this.lblMoneyOutType.Text = "【营销费用支出:0】";
            this.lblMoneyOutType.Visible = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl7.Location = new System.Drawing.Point(130, 24);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(71, 19);
            this.labelControl7.TabIndex = 13;
            this.labelControl7.Text = "(单位:个)";
            this.labelControl7.Visible = false;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl6.Location = new System.Drawing.Point(10, 42);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(57, 19);
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "总收文:";
            // 
            // txtSumGSMoney
            // 
            this.txtSumGSMoney.Location = new System.Drawing.Point(10, 231);
            this.txtSumGSMoney.Name = "txtSumGSMoney";
            this.txtSumGSMoney.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSumGSMoney.Properties.Appearance.Options.UseFont = true;
            this.txtSumGSMoney.Properties.ReadOnly = true;
            this.txtSumGSMoney.Size = new System.Drawing.Size(190, 26);
            this.txtSumGSMoney.TabIndex = 12;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl5.Location = new System.Drawing.Point(8, 104);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(57, 19);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "总批办:";
            // 
            // simpleBtnQuery
            // 
            this.simpleBtnQuery.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.simpleBtnQuery.Appearance.Options.UseFont = true;
            this.simpleBtnQuery.Location = new System.Drawing.Point(122, 126);
            this.simpleBtnQuery.Name = "simpleBtnQuery";
            this.simpleBtnQuery.Size = new System.Drawing.Size(76, 30);
            this.simpleBtnQuery.TabIndex = 6;
            this.simpleBtnQuery.Text = "统计查询";
            this.simpleBtnQuery.Click += new System.EventHandler(this.simpleBtnQuery_Click);
            // 
            // simpleBtnYear
            // 
            this.simpleBtnYear.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.simpleBtnYear.Appearance.Options.UseFont = true;
            this.simpleBtnYear.Location = new System.Drawing.Point(21, 126);
            this.simpleBtnYear.Name = "simpleBtnYear";
            this.simpleBtnYear.Size = new System.Drawing.Size(76, 30);
            this.simpleBtnYear.TabIndex = 7;
            this.simpleBtnYear.Text = "当年查询";
            this.simpleBtnYear.Click += new System.EventHandler(this.simpleBtnYear_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleBtnQuery);
            this.panelControl1.Controls.Add(this.simpleBtnYear);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.comboBoxYear);
            this.panelControl1.Controls.Add(this.comboBoxEdit1);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.comboBoxMonth);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 22);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(212, 175);
            this.panelControl1.TabIndex = 8;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl3.Location = new System.Drawing.Point(8, 89);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(86, 19);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "统计图类型:";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl4.Location = new System.Drawing.Point(10, 206);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(91, 19);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "未处理文件:";
            // 
            // txtSumGSMoneyIN
            // 
            this.txtSumGSMoneyIN.Location = new System.Drawing.Point(8, 65);
            this.txtSumGSMoneyIN.Name = "txtSumGSMoneyIN";
            this.txtSumGSMoneyIN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSumGSMoneyIN.Properties.Appearance.Options.UseFont = true;
            this.txtSumGSMoneyIN.Properties.ReadOnly = true;
            this.txtSumGSMoneyIN.Size = new System.Drawing.Size(192, 26);
            this.txtSumGSMoneyIN.TabIndex = 6;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.lblMoneyOutType2);
            this.groupControl3.Controls.Add(this.lblMoneyOutType);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Controls.Add(this.labelControl6);
            this.groupControl3.Controls.Add(this.txtSumGSMoney);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Controls.Add(this.txtSumGSMoneyIN);
            this.groupControl3.Controls.Add(this.txtSumGSMoneyOUT);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(2, 197);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(212, 353);
            this.groupControl3.TabIndex = 8;
            this.groupControl3.Text = "统计结果数据";
            // 
            // txtSumGSMoneyOUT
            // 
            this.txtSumGSMoneyOUT.Location = new System.Drawing.Point(10, 127);
            this.txtSumGSMoneyOUT.Name = "txtSumGSMoneyOUT";
            this.txtSumGSMoneyOUT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSumGSMoneyOUT.Properties.Appearance.Options.UseFont = true;
            this.txtSumGSMoneyOUT.Properties.ReadOnly = true;
            this.txtSumGSMoneyOUT.Size = new System.Drawing.Size(190, 26);
            this.txtSumGSMoneyOUT.TabIndex = 7;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.panelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControl1.Location = new System.Drawing.Point(2, 22);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(216, 552);
            this.groupControl1.TabIndex = 13;
            this.groupControl1.Text = "统计条件";
            // 
            // groupControl4
            // 
            this.groupControl4.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl4.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl4.Controls.Add(this.chartControl1);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(218, 22);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(538, 552);
            this.groupControl4.TabIndex = 14;
            this.groupControl4.Text = "统计图表";
            // 
            // chartControl1
            // 
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.Location = new System.Drawing.Point(2, 22);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl1.Size = new System.Drawing.Size(534, 528);
            this.chartControl1.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl2.Controls.Add(this.groupControl4);
            this.groupControl2.Controls.Add(this.groupControl1);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(758, 576);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "收文登记概况";
            // 
            // XUControlMainMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl2);
            this.Name = "XUControlMainMap";
            this.Size = new System.Drawing.Size(758, 576);
            this.Load += new System.EventHandler(this.XUControlMainMap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumGSMoney.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumGSMoneyIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumGSMoneyOUT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxYear;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxMonth;
        private DevExpress.XtraEditors.LabelControl lblMoneyOutType2;
        private DevExpress.XtraEditors.LabelControl lblMoneyOutType;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtSumGSMoney;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleBtnQuery;
        private DevExpress.XtraEditors.SimpleButton simpleBtnYear;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtSumGSMoneyIN;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.TextEdit txtSumGSMoneyOUT;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraCharts.ChartControl chartControl1;
    }
}
