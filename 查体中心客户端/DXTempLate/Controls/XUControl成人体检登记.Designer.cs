﻿namespace DXTempLate.Controls
{
    partial class XUControl成人体检登记
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XUControl成人体检登记));
            this.txt备注 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.btn开始 = new DevExpress.XtraEditors.SimpleButton();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControlFill = new DevExpress.XtraEditors.GroupControl();
            this.btn取消 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.txt血糖 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.txt收缩压 = new DevExpress.XtraEditors.TextEdit();
            this.txt舒张压 = new DevExpress.XtraEditors.TextEdit();
            this.txt心率 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.txt左眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.txt右眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.txt左眼矫正视力 = new DevExpress.XtraEditors.TextEdit();
            this.txt右眼矫正视力 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.txt体温 = new DevExpress.XtraEditors.TextEdit();
            this.txt呼吸 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl6 = new DevExpress.XtraLayout.LayoutControl();
            this.txt建议体重 = new DevExpress.XtraEditors.MemoEdit();
            this.txt身高 = new DevExpress.XtraEditors.TextEdit();
            this.txt体重 = new DevExpress.XtraEditors.TextEdit();
            this.txtBMI = new DevExpress.XtraEditors.TextEdit();
            this.txt腰围 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControlLeft = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlFill)).BeginInit();
            this.groupControlFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt血糖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt收缩压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt舒张压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼矫正视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼矫正视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt体温.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt呼吸.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).BeginInit();
            this.layoutControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt建议体重.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身高.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体重.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBMI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt腰围.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlLeft)).BeginInit();
            this.groupControlLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt备注
            // 
            this.txt备注.Location = new System.Drawing.Point(130, 423);
            this.txt备注.Name = "txt备注";
            this.txt备注.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txt备注.Properties.Appearance.Options.UseFont = true;
            this.txt备注.Size = new System.Drawing.Size(286, 52);
            this.txt备注.StyleController = this.layoutControl1;
            this.txt备注.TabIndex = 23;
            this.txt备注.UseOptimizedRendering = true;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.txt备注);
            this.layoutControl1.Controls.Add(this.pictureEdit1);
            this.layoutControl1.Controls.Add(this.btn开始);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 22);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(723, 1, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(421, 642);
            this.layoutControl1.TabIndex = 26;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(353, 183);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(63, 36);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 27;
            this.simpleButton1.Text = "...";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::DXTempLate.Properties.Resources.People;
            this.pictureEdit1.Location = new System.Drawing.Point(5, 5);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(411, 174);
            this.pictureEdit1.StyleController = this.layoutControl1;
            this.pictureEdit1.TabIndex = 24;
            this.pictureEdit1.ToolTip = "可进行上传用户照片";
            // 
            // btn开始
            // 
            this.btn开始.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.btn开始.Appearance.Options.UseFont = true;
            this.btn开始.Image = ((System.Drawing.Image)(resources.GetObject("btn开始.Image")));
            this.btn开始.Location = new System.Drawing.Point(5, 479);
            this.btn开始.Name = "btn开始";
            this.btn开始.Size = new System.Drawing.Size(411, 71);
            this.btn开始.StyleController = this.layoutControl1;
            this.btn开始.TabIndex = 10;
            this.btn开始.Text = "开 始";
            this.btn开始.Click += new System.EventHandler(this.btn开始_Click);
            // 
            // txt出生日期
            // 
            this.txt出生日期.EditValue = "";
            this.txt出生日期.Location = new System.Drawing.Point(130, 263);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt出生日期.Properties.Appearance.Options.UseFont = true;
            this.txt出生日期.Properties.AutoHeight = false;
            this.txt出生日期.Size = new System.Drawing.Size(286, 36);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 9;
            // 
            // txt居住地址
            // 
            this.txt居住地址.EditValue = "";
            this.txt居住地址.Location = new System.Drawing.Point(130, 343);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt居住地址.Properties.Appearance.Options.UseFont = true;
            this.txt居住地址.Properties.AutoHeight = false;
            this.txt居住地址.Size = new System.Drawing.Size(286, 36);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 8;
            // 
            // txt身份证号
            // 
            this.txt身份证号.EditValue = "";
            this.txt身份证号.Location = new System.Drawing.Point(130, 303);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt身份证号.Properties.Appearance.Options.UseFont = true;
            this.txt身份证号.Properties.AutoHeight = false;
            this.txt身份证号.Size = new System.Drawing.Size(286, 36);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt姓名
            // 
            this.txt姓名.EditValue = "";
            this.txt姓名.Location = new System.Drawing.Point(130, 183);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt姓名.Properties.Appearance.Options.UseFont = true;
            this.txt姓名.Properties.AutoHeight = false;
            this.txt姓名.Size = new System.Drawing.Size(219, 36);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 4;
            // 
            // txt性别
            // 
            this.txt性别.EditValue = "";
            this.txt性别.Location = new System.Drawing.Point(130, 223);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt性别.Properties.Appearance.Options.UseFont = true;
            this.txt性别.Properties.AutoHeight = false;
            this.txt性别.Size = new System.Drawing.Size(286, 36);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 5;
            // 
            // txt联系电话
            // 
            this.txt联系电话.EditValue = "";
            this.txt联系电话.Location = new System.Drawing.Point(130, 383);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt联系电话.Properties.Appearance.Options.UseFont = true;
            this.txt联系电话.Size = new System.Drawing.Size(286, 36);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 26;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem8,
            this.layoutControlItem25});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(421, 642);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt出生日期;
            this.layoutControlItem6.CustomizationFormText = "出生日期";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 258);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(415, 40);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(179, 40);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(415, 40);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "出生日期";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(120, 22);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt居住地址;
            this.layoutControlItem5.CustomizationFormText = "居住地址";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 338);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(415, 40);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(179, 40);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(415, 40);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "地 址";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(120, 22);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 298);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(415, 40);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(179, 40);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(415, 40);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(120, 22);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt姓名;
            this.layoutControlItem1.CustomizationFormText = "姓名";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 178);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(348, 40);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(179, 40);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(348, 40);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "姓 名";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(120, 22);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.pictureEdit1;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(415, 178);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 549);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(415, 87);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt性别;
            this.layoutControlItem2.CustomizationFormText = "性别";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 218);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(415, 40);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(179, 40);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(415, 40);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "性 别";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(120, 22);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt联系电话;
            this.layoutControlItem9.CustomizationFormText = "联系电话";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 378);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(415, 40);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(179, 40);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(415, 40);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "联系电话";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(120, 22);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt备注;
            this.layoutControlItem10.CustomizationFormText = "备注";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 418);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(109, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(415, 56);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "备注";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(120, 22);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btn开始;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 474);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 75);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(114, 75);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(415, 75);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.simpleButton1;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(348, 178);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(27, 40);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(67, 40);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // groupControlFill
            // 
            this.groupControlFill.Controls.Add(this.btn取消);
            this.groupControlFill.Controls.Add(this.labelControl2);
            this.groupControlFill.Controls.Add(this.labelControl1);
            this.groupControlFill.Controls.Add(this.groupControl4);
            this.groupControlFill.Controls.Add(this.btn保存);
            this.groupControlFill.Controls.Add(this.groupControl5);
            this.groupControlFill.Controls.Add(this.groupControl3);
            this.groupControlFill.Controls.Add(this.groupControl2);
            this.groupControlFill.Controls.Add(this.groupControl1);
            this.groupControlFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlFill.Location = new System.Drawing.Point(425, 0);
            this.groupControlFill.Name = "groupControlFill";
            this.groupControlFill.Size = new System.Drawing.Size(714, 666);
            this.groupControlFill.TabIndex = 9;
            this.groupControlFill.Text = "实时查体数据";
            // 
            // btn取消
            // 
            this.btn取消.Appearance.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.btn取消.Appearance.Options.UseFont = true;
            this.btn取消.Image = ((System.Drawing.Image)(resources.GetObject("btn取消.Image")));
            this.btn取消.Location = new System.Drawing.Point(306, 560);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(164, 64);
            this.btn取消.TabIndex = 29;
            this.btn取消.Text = "取 消";
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Green;
            this.labelControl2.Location = new System.Drawing.Point(36, 635);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(468, 22);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "体温设备如果没有开启，请在开启后重新建立，并点击开始";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Green;
            this.labelControl1.Location = new System.Drawing.Point(36, 351);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(468, 31);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "血糖仪等设备建议在工作人员协助下完成";
            // 
            // groupControl4
            // 
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 13F);
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.layoutControl2);
            this.groupControl4.Location = new System.Drawing.Point(36, 253);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(298, 92);
            this.groupControl4.TabIndex = 28;
            this.groupControl4.Text = "血糖测量结果";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.txt血糖);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 29);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(294, 61);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // txt血糖
            // 
            this.txt血糖.Location = new System.Drawing.Point(117, 12);
            this.txt血糖.Name = "txt血糖";
            this.txt血糖.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt血糖.Properties.Appearance.Options.UseFont = true;
            this.txt血糖.Size = new System.Drawing.Size(165, 36);
            this.txt血糖.StyleController = this.layoutControl2;
            this.txt血糖.TabIndex = 15;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(294, 61);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.txt血糖;
            this.layoutControlItem7.CustomizationFormText = "血糖：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(274, 41);
            this.layoutControlItem7.Text = "血糖：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(100, 31);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // btn保存
            // 
            this.btn保存.Appearance.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.btn保存.Appearance.Options.UseFont = true;
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(36, 560);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(164, 64);
            this.btn保存.TabIndex = 29;
            this.btn保存.Text = "保 存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // groupControl5
            // 
            this.groupControl5.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 13F);
            this.groupControl5.AppearanceCaption.Options.UseFont = true;
            this.groupControl5.Controls.Add(this.layoutControl4);
            this.groupControl5.Location = new System.Drawing.Point(387, 172);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(289, 178);
            this.groupControl5.TabIndex = 28;
            this.groupControl5.Text = "血压测量结果";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.txt收缩压);
            this.layoutControl4.Controls.Add(this.txt舒张压);
            this.layoutControl4.Controls.Add(this.txt心率);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(2, 29);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(285, 147);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // txt收缩压
            // 
            this.txt收缩压.Location = new System.Drawing.Point(119, 12);
            this.txt收缩压.Name = "txt收缩压";
            this.txt收缩压.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt收缩压.Properties.Appearance.Options.UseFont = true;
            this.txt收缩压.Size = new System.Drawing.Size(154, 36);
            this.txt收缩压.StyleController = this.layoutControl4;
            this.txt收缩压.TabIndex = 15;
            // 
            // txt舒张压
            // 
            this.txt舒张压.Location = new System.Drawing.Point(119, 52);
            this.txt舒张压.Name = "txt舒张压";
            this.txt舒张压.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt舒张压.Properties.Appearance.Options.UseFont = true;
            this.txt舒张压.Size = new System.Drawing.Size(154, 36);
            this.txt舒张压.StyleController = this.layoutControl4;
            this.txt舒张压.TabIndex = 15;
            // 
            // txt心率
            // 
            this.txt心率.Location = new System.Drawing.Point(119, 92);
            this.txt心率.Name = "txt心率";
            this.txt心率.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt心率.Properties.Appearance.Options.UseFont = true;
            this.txt心率.Size = new System.Drawing.Size(154, 36);
            this.txt心率.StyleController = this.layoutControl4;
            this.txt心率.TabIndex = 15;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(285, 147);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.txt收缩压;
            this.layoutControlItem13.CustomizationFormText = "收缩压：";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(265, 40);
            this.layoutControlItem13.Text = "收缩压：";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(104, 31);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.Control = this.txt舒张压;
            this.layoutControlItem14.CustomizationFormText = "舒张压：";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(265, 40);
            this.layoutControlItem14.Text = "舒张压：";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(104, 31);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.Control = this.txt心率;
            this.layoutControlItem15.CustomizationFormText = "心率：";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(265, 47);
            this.layoutControlItem15.Text = "心率：";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(104, 31);
            // 
            // groupControl3
            // 
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 13F);
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.layoutControl5);
            this.groupControl3.Location = new System.Drawing.Point(36, 399);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(644, 143);
            this.groupControl3.TabIndex = 27;
            this.groupControl3.Text = "视力测验结果";
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.txt左眼视力);
            this.layoutControl5.Controls.Add(this.txt右眼视力);
            this.layoutControl5.Controls.Add(this.txt左眼矫正视力);
            this.layoutControl5.Controls.Add(this.txt右眼矫正视力);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(2, 29);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup5;
            this.layoutControl5.Size = new System.Drawing.Size(640, 112);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // txt左眼视力
            // 
            this.txt左眼视力.Location = new System.Drawing.Point(506, 12);
            this.txt左眼视力.Name = "txt左眼视力";
            this.txt左眼视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt左眼视力.Properties.Appearance.Options.UseFont = true;
            this.txt左眼视力.Size = new System.Drawing.Size(122, 36);
            this.txt左眼视力.StyleController = this.layoutControl5;
            this.txt左眼视力.TabIndex = 15;
            // 
            // txt右眼视力
            // 
            this.txt右眼视力.Location = new System.Drawing.Point(197, 52);
            this.txt右眼视力.Name = "txt右眼视力";
            this.txt右眼视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt右眼视力.Properties.Appearance.Options.UseFont = true;
            this.txt右眼视力.Size = new System.Drawing.Size(120, 36);
            this.txt右眼视力.StyleController = this.layoutControl5;
            this.txt右眼视力.TabIndex = 15;
            // 
            // txt左眼矫正视力
            // 
            this.txt左眼矫正视力.Location = new System.Drawing.Point(197, 12);
            this.txt左眼矫正视力.Name = "txt左眼矫正视力";
            this.txt左眼矫正视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt左眼矫正视力.Properties.Appearance.Options.UseFont = true;
            this.txt左眼矫正视力.Size = new System.Drawing.Size(120, 36);
            this.txt左眼矫正视力.StyleController = this.layoutControl5;
            this.txt左眼矫正视力.TabIndex = 15;
            // 
            // txt右眼矫正视力
            // 
            this.txt右眼矫正视力.Location = new System.Drawing.Point(506, 52);
            this.txt右眼矫正视力.Name = "txt右眼矫正视力";
            this.txt右眼矫正视力.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt右眼矫正视力.Properties.Appearance.Options.UseFont = true;
            this.txt右眼矫正视力.Size = new System.Drawing.Size(122, 36);
            this.txt右眼矫正视力.StyleController = this.layoutControl5;
            this.txt右眼矫正视力.TabIndex = 15;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem16,
            this.layoutControlItem19});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(640, 112);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.Control = this.txt左眼矫正视力;
            this.layoutControlItem17.CustomizationFormText = "左眼视力：";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(309, 40);
            this.layoutControlItem17.Text = "左眼视力：";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(182, 31);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.Control = this.txt右眼视力;
            this.layoutControlItem18.CustomizationFormText = "右眼视力：";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(309, 52);
            this.layoutControlItem18.Text = "右眼视力：";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(182, 31);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.Control = this.txt左眼视力;
            this.layoutControlItem16.CustomizationFormText = "左眼矫正视力：";
            this.layoutControlItem16.Location = new System.Drawing.Point(309, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(311, 40);
            this.layoutControlItem16.Text = "左眼矫正视力：";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(182, 31);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.Control = this.txt右眼矫正视力;
            this.layoutControlItem19.CustomizationFormText = "右眼矫正视力：";
            this.layoutControlItem19.Location = new System.Drawing.Point(309, 40);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(311, 52);
            this.layoutControlItem19.Text = "右眼矫正视力：";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(182, 31);
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 13F);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.layoutControl3);
            this.groupControl2.Location = new System.Drawing.Point(389, 25);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(287, 137);
            this.groupControl2.TabIndex = 26;
            this.groupControl2.Text = "体温/呼吸频率";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.txt体温);
            this.layoutControl3.Controls.Add(this.txt呼吸);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 29);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(283, 106);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // txt体温
            // 
            this.txt体温.EditValue = "36.5";
            this.txt体温.Location = new System.Drawing.Point(93, 12);
            this.txt体温.Name = "txt体温";
            this.txt体温.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt体温.Properties.Appearance.Options.UseFont = true;
            this.txt体温.Size = new System.Drawing.Size(178, 36);
            this.txt体温.StyleController = this.layoutControl3;
            this.txt体温.TabIndex = 15;
            // 
            // txt呼吸
            // 
            this.txt呼吸.EditValue = "18";
            this.txt呼吸.Location = new System.Drawing.Point(93, 52);
            this.txt呼吸.Name = "txt呼吸";
            this.txt呼吸.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 19F);
            this.txt呼吸.Properties.Appearance.Options.UseFont = true;
            this.txt呼吸.Size = new System.Drawing.Size(178, 36);
            this.txt呼吸.StyleController = this.layoutControl3;
            this.txt呼吸.TabIndex = 15;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(283, 106);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.txt体温;
            this.layoutControlItem11.CustomizationFormText = "体温：";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(263, 40);
            this.layoutControlItem11.Text = "体温：";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(78, 31);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 19F);
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.txt呼吸;
            this.layoutControlItem12.CustomizationFormText = "呼吸：";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(263, 46);
            this.layoutControlItem12.Text = "呼吸：";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(78, 31);
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 13F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.layoutControl6);
            this.groupControl1.Location = new System.Drawing.Point(36, 26);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(298, 222);
            this.groupControl1.TabIndex = 25;
            this.groupControl1.Text = "身高体重";
            // 
            // layoutControl6
            // 
            this.layoutControl6.Controls.Add(this.txt建议体重);
            this.layoutControl6.Controls.Add(this.txt身高);
            this.layoutControl6.Controls.Add(this.txt体重);
            this.layoutControl6.Controls.Add(this.txtBMI);
            this.layoutControl6.Controls.Add(this.txt腰围);
            this.layoutControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl6.Location = new System.Drawing.Point(2, 29);
            this.layoutControl6.Name = "layoutControl6";
            this.layoutControl6.Root = this.layoutControlGroup6;
            this.layoutControl6.Size = new System.Drawing.Size(294, 191);
            this.layoutControl6.TabIndex = 0;
            this.layoutControl6.Text = "layoutControl6";
            // 
            // txt建议体重
            // 
            this.txt建议体重.Location = new System.Drawing.Point(115, 148);
            this.txt建议体重.Name = "txt建议体重";
            this.txt建议体重.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txt建议体重.Properties.Appearance.ForeColor = System.Drawing.Color.Green;
            this.txt建议体重.Properties.Appearance.Options.UseFont = true;
            this.txt建议体重.Properties.Appearance.Options.UseForeColor = true;
            this.txt建议体重.Size = new System.Drawing.Size(167, 31);
            this.txt建议体重.StyleController = this.layoutControl6;
            this.txt建议体重.TabIndex = 24;
            this.txt建议体重.UseOptimizedRendering = true;
            // 
            // txt身高
            // 
            this.txt身高.Location = new System.Drawing.Point(115, 12);
            this.txt身高.Name = "txt身高";
            this.txt身高.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.txt身高.Properties.Appearance.Options.UseFont = true;
            this.txt身高.Size = new System.Drawing.Size(167, 30);
            this.txt身高.StyleController = this.layoutControl6;
            this.txt身高.TabIndex = 15;
            this.txt身高.EditValueChanged += new System.EventHandler(this.txt身高_EditValueChanged);
            // 
            // txt体重
            // 
            this.txt体重.Location = new System.Drawing.Point(115, 46);
            this.txt体重.Name = "txt体重";
            this.txt体重.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.txt体重.Properties.Appearance.Options.UseFont = true;
            this.txt体重.Size = new System.Drawing.Size(167, 30);
            this.txt体重.StyleController = this.layoutControl6;
            this.txt体重.TabIndex = 17;
            this.txt体重.EditValueChanged += new System.EventHandler(this.txt身高_EditValueChanged);
            // 
            // txtBMI
            // 
            this.txtBMI.Location = new System.Drawing.Point(115, 80);
            this.txtBMI.Name = "txtBMI";
            this.txtBMI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.txtBMI.Properties.Appearance.Options.UseFont = true;
            this.txtBMI.Size = new System.Drawing.Size(167, 30);
            this.txtBMI.StyleController = this.layoutControl6;
            this.txtBMI.TabIndex = 19;
            // 
            // txt腰围
            // 
            this.txt腰围.Location = new System.Drawing.Point(115, 114);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.txt腰围.Properties.Appearance.Options.UseFont = true;
            this.txt腰围.Size = new System.Drawing.Size(167, 30);
            this.txt腰围.StyleController = this.layoutControl6;
            this.txt腰围.TabIndex = 19;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(294, 191);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            this.layoutControlGroup6.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F);
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.Control = this.txt身高;
            this.layoutControlItem20.CustomizationFormText = "身高：";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(274, 34);
            this.layoutControlItem20.Text = "身高：";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(100, 24);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F);
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.Control = this.txt体重;
            this.layoutControlItem21.CustomizationFormText = "体重：";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(274, 34);
            this.layoutControlItem21.Text = "体重：";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(100, 24);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F);
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.Control = this.txtBMI;
            this.layoutControlItem22.CustomizationFormText = "体质指数：";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(274, 34);
            this.layoutControlItem22.Text = "体质指数：";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(100, 24);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F);
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.Control = this.txt腰围;
            this.layoutControlItem23.CustomizationFormText = "腰围：";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(274, 34);
            this.layoutControlItem23.Text = "腰围：";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(100, 24);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 15F);
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.Control = this.txt建议体重;
            this.layoutControlItem24.CustomizationFormText = "建议体重：";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 136);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(274, 35);
            this.layoutControlItem24.Text = "建议体重：";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(100, 24);
            // 
            // groupControlLeft
            // 
            this.groupControlLeft.Controls.Add(this.layoutControl1);
            this.groupControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControlLeft.Location = new System.Drawing.Point(0, 0);
            this.groupControlLeft.Name = "groupControlLeft";
            this.groupControlLeft.Size = new System.Drawing.Size(425, 666);
            this.groupControlLeft.TabIndex = 25;
            this.groupControlLeft.Text = "基本资料";
            // 
            // XUControl成人体检登记
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlFill);
            this.Controls.Add(this.groupControlLeft);
            this.Name = "XUControl成人体检登记";
            this.Size = new System.Drawing.Size(1139, 666);
            this.Load += new System.EventHandler(this.XUControlTJMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlFill)).EndInit();
            this.groupControlFill.ResumeLayout(false);
            this.groupControlFill.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt血糖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt收缩压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt舒张压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼矫正视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼矫正视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt体温.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt呼吸.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).EndInit();
            this.layoutControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt建议体重.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身高.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体重.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBMI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt腰围.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlLeft)).EndInit();
            this.groupControlLeft.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit txt备注;
        private DevExpress.XtraEditors.GroupControl groupControlFill;
        private DevExpress.XtraEditors.MemoEdit txt建议体重;
        private DevExpress.XtraEditors.TextEdit txtBMI;
        private DevExpress.XtraEditors.TextEdit txt体重;
        private DevExpress.XtraEditors.TextEdit txt身高;
        private DevExpress.XtraEditors.TextEdit txt心率;
        private DevExpress.XtraEditors.TextEdit txt血糖;
        private DevExpress.XtraEditors.TextEdit txt舒张压;
        private DevExpress.XtraEditors.TextEdit txt呼吸;
        private DevExpress.XtraEditors.TextEdit txt收缩压;
        private DevExpress.XtraEditors.TextEdit txt体温;
        private DevExpress.XtraEditors.TextEdit txt右眼矫正视力;
        private DevExpress.XtraEditors.TextEdit txt右眼视力;
        private DevExpress.XtraEditors.TextEdit txt左眼矫正视力;
        private DevExpress.XtraEditors.TextEdit txt左眼视力;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt腰围;
        private DevExpress.XtraEditors.GroupControl groupControlLeft;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.SimpleButton btn开始;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn取消;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControl layoutControl6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}
