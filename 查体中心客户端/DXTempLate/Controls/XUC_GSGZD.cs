﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
//using UDF.GUI.PrintControl;

namespace DXTempLate.Controls
{
    /// <summary>
    ///  工作单
    /// </summary>
    public partial class XUC_GSGZD : DevExpress.XtraEditors.XtraUserControl
    {
        public XUC_GSGZD()
        {
            InitializeComponent();
        }

        private static XUC_GSGZD _ItSelf;
        public static XUC_GSGZD ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new XUC_GSGZD();
                }
                return _ItSelf;
            }
        }

        private void XUC_GSGZD_Load(object sender, EventArgs e)
        {
            BindData();

            ////if (StaticClass.g_CurUserRoles == 2)
            ////{
            //    this.simpleBtnAdd.Visible = false;
            //    this.simpleBtnUpdate.Visible = false;
            //    this.simpleBtnDelete.Visible = false;
            ////}
        }

        /// <summary>
        /// 绑定数据
        /// </summary>
        private void BindData()
        {
            System.Data.DataTable datTable = new DataTable();

            #region 创建表头
            DataColumn dc1 = new DataColumn("选择", typeof(Boolean));
            DataColumn dc2 = new DataColumn("ID", typeof(int));
            DataColumn dc3 = new DataColumn("序号", typeof(int));
            DataColumn dc4 = new DataColumn("收到时间");
            DataColumn dc5 = new DataColumn("来文机关");
            DataColumn dc6 = new DataColumn("文号");
            DataColumn dc7 = new DataColumn("文件名称");
            DataColumn dc8 = new DataColumn("批办领导、科室");
            DataColumn dc9 = new DataColumn("收到文件数");
            DataColumn dc10 = new DataColumn("打印状态");
            DataColumn dc11 = new DataColumn("备注");
            DataColumn dc12 = new DataColumn("创建人");
            DataColumn dc13 = new DataColumn("创建时间");
            DataColumn dc14 = new DataColumn("修改人");
            DataColumn dc15 = new DataColumn("修改日期"); ;

            datTable.Columns.Add(dc1);
            datTable.Columns.Add(dc2);
            datTable.Columns.Add(dc3);
            datTable.Columns.Add(dc4);
            datTable.Columns.Add(dc5);
            datTable.Columns.Add(dc6);
            datTable.Columns.Add(dc7);
            datTable.Columns.Add(dc8);
            datTable.Columns.Add(dc9);
            datTable.Columns.Add(dc10);
            datTable.Columns.Add(dc11);
            datTable.Columns.Add(dc12);
            datTable.Columns.Add(dc13);
            datTable.Columns.Add(dc14);
            datTable.Columns.Add(dc15); 
            #endregion

            #region 绑定
            SqliteClass sqldb = new SqliteClass();
            List<int> listid = new List<int>();
            listid = sqldb.GetAllTablesID("Sys_收文登记表", "收到时间");

            if (listid.Count > 0)
            {
                for (int i = 0; i < listid.Count; i++)
                {
                    Entity_Sys_收文登记表 objinfo = new Entity_Sys_收文登记表();
                    objinfo = sqldb.GetSys_收文登记表ByID(listid[i]);
                    DataRow datRow = datTable.NewRow();//创建新行
                    datRow[0] = false;
                    datRow[1] = listid[i];
                    datRow[2] = i + 1;
                    datRow[3] = objinfo.收到时间.ToShortDateString();
                    datRow[4] = objinfo.来文机关;
                    datRow[5] = objinfo.文号;
                    datRow[6] = objinfo.文件名称;
                    datRow[7] = objinfo.批办领导;
                    datRow[8] = objinfo.收到文件数.ToString();
                    datRow[9] = objinfo.打印状态.ToString();
                    datRow[10] = objinfo.备注;
                    datRow[11] = objinfo.创建人;
                    datRow[12] = objinfo.创建时间.ToString();
                    datRow[13] = objinfo.修改人.ToString();
                    datRow[14] = objinfo.修改时间.ToString();
                    datTable.Rows.Add(datRow);
                }
                //DataTable按照排序字段进行升序排列
                datTable.DefaultView.Sort = "序号 asc";
                DataTable dtT = datTable.DefaultView.ToTable();
                datTable.Clear();
                datTable = dtT;


                this.gridControl1.DataSource = new DataView(datTable);
                this.gridControl1.Refresh();
                this.gridControl1.RefreshDataSource();
                //ExportDataTable = datTable;
                this.txtAll.Text = datTable.Rows.Count.ToString();
            }
            else
            {
                this.gridControl1.DataSource = new DataView(datTable);
                this.gridControl1.Refresh();
                this.gridControl1.RefreshDataSource();
            } 
            #endregion

            #region grid 样式
            //设置列宽
            this.gridView1.Columns[0].Width = 28;
            this.gridView1.Columns[2].Width = 30;
            this.gridView1.Columns[3].Width = 75;
            this.gridView1.Columns[3].AppearanceHeader.TextOptions.HAlignment =  DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[4].Width = 150;
            this.gridView1.Columns[4].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[5].Width = 150;
            this.gridView1.Columns[5].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[6].Width = 300;
            this.gridView1.Columns[6].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns[7].Width = 80;
            this.gridView1.Columns[7].AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;

            //隐藏列
            this.gridView1.Columns[1].Visible = false;
            this.gridView1.Columns[8].Visible = false;
            this.gridView1.Columns[9].Visible = false;
            this.gridView1.Columns[10].Visible = false;
            this.gridView1.Columns[11].Visible = false;
            this.gridView1.Columns[12].Visible = false;
            this.gridView1.Columns[13].Visible = false;
            this.gridView1.Columns[14].Visible = false;

            //设置列不允许编辑
            this.gridView1.Columns[2].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[3].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[4].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[5].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[6].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[7].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[8].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[9].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[10].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[11].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[12].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[13].OptionsColumn.AllowEdit = false;
            this.gridView1.Columns[14].OptionsColumn.AllowEdit = false; 
            #endregion

        }


        #region ButtonClick
        //全选
        private void simpleBSelAll_Click(object sender, EventArgs e)
        {
            FormClass frmClass = new FormClass();
            if (this.simpleBSelAll.Text == "全选")
            {
                frmClass.SelCurRowCheckAllAndUnAll(this.gridView1, 0);
                this.simpleBSelAll.Text = "全不选";
            }
            else
            {
                frmClass.SelCurRowCheckAllAndUnAll(this.gridView1, 1);
                this.simpleBSelAll.Text = "全选";
            }
        }


        private void btn刷新_Click(object sender, EventArgs e)
        {
            this.BindData();
        }

        //添加
        private void simpleBtnAdd_Click(object sender, EventArgs e)
        {
            XtraFormGSGZD gsgzd = new XtraFormGSGZD();
            if (gsgzd.ShowDialog() == DialogResult.OK)
            {
                this.BindData();
            }
        }
        //修改
        private void simpleBtnUpdate_Click(object sender, EventArgs e)
        {
            int nGridViewId = GetRowViewCheckID();
            if (nGridViewId == 0)
            {
                XtraMessageBox.Show("请选择你要修改的收入记录!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            XtraFormGSGZD gsgzd = new XtraFormGSGZD(nGridViewId);
            if (gsgzd.ShowDialog() == DialogResult.OK)
            {
                this.BindData();
            }
        }

        private int GetRowViewCheckID()
        {
            int nRowId = 0;
            for (int nRow = 0; nRow <= this.gridView1.RowCount - 1; nRow++)
            {
                if (Convert.ToBoolean(gridView1.GetRowCellValue(nRow, "选择")) == true)
                {
                    nRowId = Convert.ToInt32(this.gridView1.GetRowCellValue(nRow, "ID"));
                    break;
                }
            }
            return nRowId;
        }

        //删除
        private void simpleBtnDelete_Click(object sender, EventArgs e)
        {
            List<int> listID = new List<int>();
            int nRowId = 0;
            for (int nRow = 0; nRow <= this.gridView1.RowCount - 1; nRow++)
            {
                if (Convert.ToBoolean(gridView1.GetRowCellValue(nRow, "选择")) == true)
                {
                    nRowId = Convert.ToInt32(this.gridView1.GetRowCellValue(nRow, "ID"));
                    listID.Add(nRowId);
                }
            }

            if (listID.Count == 0)
            {
                XtraMessageBox.Show("请选择你需要删除的记录!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (XtraMessageBox.Show("你确定要删除吗?", "系统提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
            {
                PublicClass pubClass = new PublicClass();
                for (int i = 0; i < listID.Count; i++)
                {
                    if (pubClass.DeleteByID(listID[i], "Sys_收文登记表"))
                    {
                    }
                }
                XtraMessageBox.Show("删除成功", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.BindData();
            }
        }

        //打印预览
        private void simpleBtnPrint_Click(object sender, EventArgs e)
        {
            List<int> listID = new List<int>();
            int nRowId = 0;
            for (int nRow = 0; nRow <= this.gridView1.RowCount - 1; nRow++)
            {
                if (Convert.ToBoolean(gridView1.GetRowCellValue(nRow, "选择")) == true)
                {
                    nRowId = Convert.ToInt32(this.gridView1.GetRowCellValue(nRow, "ID"));
                    listID.Add(nRowId);
                }
            }

            if (listID.Count == 0 || listID.Count > 1)
            {
                XtraMessageBox.Show("请选择你需要打印的一条记录!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
            SqliteClass sqlClass = new SqliteClass();
            Entity_Sys_收文登记表 objGS = new Entity_Sys_收文登记表();
            objGS = sqlClass.GetSys_收文登记表ByID(listID[0]);
            try
            {
                RowID = listID[0];
                XtraReport批办单_New xrpt = new XtraReport批办单_New(Properties.Settings.Default.pbdTitle, objGS.来文机关, objGS.文号, objGS.收到时间.ToString("yyyy-MM-dd"),
                    objGS.文件名称, objGS.备注 == "" ? "请李局长阅示" : objGS.备注);
                xrpt.PrintProgress += xrpt_PrintProgress;
                xrpt.ShowPreviewDialog();
            }
            catch(Exception ex)
            {
                MessageBox.Show("未发现打印设备!", "系统提示" + ex.Message);
            }
        }

        int RowID = 0;
        void xrpt_PrintProgress(object sender, PrintProgressEventArgs e)
        {
            SqliteClass sqlClass = new SqliteClass();
            sqlClass.UpdateSys_收文登记表打印状态(RowID);
            //throw new NotImplementedException();
        }

        //直接打印
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            List<int> listID = new List<int>();
            int nRowId = 0;
            for (int nRow = 0; nRow <= this.gridView1.RowCount - 1; nRow++)
            {
                if (Convert.ToBoolean(gridView1.GetRowCellValue(nRow, "选择")) == true)
                {
                    nRowId = Convert.ToInt32(this.gridView1.GetRowCellValue(nRow, "ID"));
                    listID.Add(nRowId);
                }
            }

            if (listID.Count == 0 || listID.Count > 1)
            {
                XtraMessageBox.Show("请选择你需要打印的一条记录!", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //FormatPrint myFormatPrint = new FormatPrint();
            //myFormatPrint.loadPrintTemplate("PrtTemplate.xml");
            //for (int i = 1; i <= 9; i++)
            //{
            //    myFormatPrint.SetValue("F" + i.ToString(), "value" + i.ToString());
            //}
            SqliteClass sqlClass = new SqliteClass();
            Entity_Sys_收文登记表 objGS = new Entity_Sys_收文登记表();
            objGS = sqlClass.GetSys_收文登记表ByID(listID[0]);
            try
            {
                RowID = listID[0];
                XtraReport批办单_New xrpt = new XtraReport批办单_New(Properties.Settings.Default.pbdTitle, objGS.来文机关, objGS.文号, objGS.收到时间.ToString("yyyy-MM-dd"),
                    objGS.文件名称, objGS.备注 == "" ? "请李局长阅示" : objGS.备注);
                xrpt.PrintProgress += xrpt_PrintProgress;
                xrpt.Print();
            }
            catch(Exception ex)
            {
                MessageBox.Show("未发现打印设备!", "系统提示" + ex.Message);
            }
        } 
        #endregion

        #region Grid Event
        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            if (this.gridView1.RowCount == 0)
            {
                string str = "暂无工作单数据!";
                Font f = new Font("宋体", 10, FontStyle.Bold);
                Rectangle r = new Rectangle(e.Bounds.Left + 25, e.Bounds.Top + 5, e.Bounds.Width - 5, e.Bounds.Height - 5);
                e.Graphics.DrawString(str, f, Brushes.Black, r);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            int nCurRow = 0;
            GridHitInfo hi = this.gridView1.CalcHitInfo(this.gridControl1.PointToClient(MousePosition));
            if (hi.InRow)
            {
                nCurRow = hi.RowHandle;
            }
            if (nCurRow < 0) return;

            int nGridViewId = Convert.ToInt32(this.gridView1.GetRowCellValue(nCurRow, "ID"));

            XtraFormGSGZD gsyp = new XtraFormGSGZD(nGridViewId, true);
            gsyp.ShowDialog();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            FormClass frmClass = new FormClass();
            int nCurRow = 0;
            nCurRow = frmClass.XGridViewMouseDown(this.gridView1, e, 0);
        } 
        #endregion

    }

}
