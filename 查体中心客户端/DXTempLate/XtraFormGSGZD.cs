﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;

namespace DXTempLate
{
    /// <summary>
    /// 维护工作单信息
    /// </summary>
    public partial class XtraFormGSGZD : DevExpress.XtraEditors.XtraForm
    {
        private int mID = 0;
        private bool read = false;

        private FormClass formClass = new FormClass();
        private SqliteClass sqlClass = new SqliteClass();

        public XtraFormGSGZD(int nID = 0, bool isRead = false)
        {
            InitializeComponent();
            mID = nID;
            read = isRead;
        }

        private void XtraFormGSGZD_Load(object sender, EventArgs e)
        {
            formClass.BindComBoxUser(this.txt批办领导);
            BindKHMC();

            if (read)
            {
                this.simpleBtnOK.Visible = false;
            }

            if (mID > 0)
            {
                InivitForm();
            }
            else
            {
                //CreateDH();
                txt收到时间.Text = DateTime.Now.ToString("yyyy-MM-dd");
                txt来文机关.SelectedIndex = 0;
                txt批办领导.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// 生成工作单号
        /// </summary>
        private void CreateDH()
        {
            int nYear, nMonth;         
            nYear = DateTime.Now.Year;
            nMonth = DateTime.Now.Month;

            string strDt = nYear.ToString() + nMonth.ToString().PadLeft(2, '0');
            int nXH = 0;
            nXH = sqlClass.GetDHXH(strDt);
            string strNewDH = "";
            if (nXH == 0)
            {
                strNewDH = strDt + ((nXH + 1).ToString()).PadLeft(4, '0');
            }
            else
            {
                strNewDH = (nXH + 1).ToString();
            }
            this.txt文号.Text = strNewDH;
        }

        /// <summary>
        /// 绑定来文机关
        /// </summary>
        private void BindKHMC()
        {
            List<string> list = new List<string>();
            list = sqlClass.GetAll来文机关();
            if (list.Count > 0)
            {
                this.txt来文机关.Properties.Items.Clear();
                for (int i = 0; i < list.Count; i++)
                {
                    this.txt来文机关.Properties.Items.Add(list[i]);
                }
            }
        }


        //保存
        private void simpleBtnOK_Click(object sender, EventArgs e)
        {
            #region 保存验证
            if (txt收到时间.Text.Trim() == "")
            {
                XtraMessageBox.Show("收到时间不能为空!", "系统提示");
                return;
            }
            if (this.txt来文机关.Text.Trim() == "")
            {
                XtraMessageBox.Show("来文机关不能为空!", "系统提示");
                return;
            }

            //if (this.txt文号.Text.Trim() == "")
            //{
            //    XtraMessageBox.Show("文号不能为空!", "系统提示");
            //    return;
            //}

            if (this.txt文件名称.Text.Trim() == "")
            {
                XtraMessageBox.Show("文件名称不能为空!", "系统提示");
                return;
            }

            //if (this.txt批办领导.Text.Trim() == "")
            //{
            //    XtraMessageBox.Show("批办领导不能为空!", "系统提示");
            //    return;
            //}

            //收到文件数
            if (this.txt收文数.Text.Trim() != "")
            {
                try
                {
                    Convert.ToInt32(this.txt收文数.Text.Trim());
                }
                catch
                {
                    XtraMessageBox.Show("收到文件数必须为数字!", "系统提示");
                    return;
                }
            } 
            #endregion

            Entity_Sys_收文登记表 objGS = new Entity_Sys_收文登记表();
            PublicClass pub = new PublicClass();
            if (mID > 0)
            {
                objGS.ID = mID;
                objGS.SSUserID = sqlClass.GetSys_收文登记表ByID(mID).SSUserID;
            }
            else
            {
                objGS.ID = pub.GetMaxID("Sys_收文登记表") + 1;
                objGS.SSUserID = StaticClass.g_CurUserID;

                if (objGS.ID == 1000)
                {
                    //XtraMessageBox.Show("你试用期限已结束，请联系软件设计人员!", "系统提示");
                    //return;
                }
            }
            string 当前时间 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            objGS.收到时间 = Convert.ToDateTime(this.txt收到时间.Text);
            objGS.来文机关 = this.txt来文机关.Text.Trim();
            objGS.文号 = this.txt文号.Text;
            objGS.文件名称 = this.txt文件名称.Text;
            objGS.批办领导 = this.txt批办领导.Text;
            objGS.收到文件数 = Convert.ToInt32(this.txt收文数.Text);
            objGS.打印状态 = 0;
            objGS.备注 = this.txt备注.Text.Trim();
            if (mID == 0)
            {
                objGS.创建人 = StaticClass.g_CurUserName;
                objGS.创建时间 = Convert.ToDateTime(当前时间);
            }
            objGS.修改人 = StaticClass.g_CurUserName;
            objGS.修改时间 = Convert.ToDateTime(当前时间);// pub.GetServerDateTime();            

            if (sqlClass.UpdateSys_收文登记表(objGS))
            {
                string strMsg = "添加成功!";
                if (mID > 0)
                { strMsg = "修改成功!"; }
                XtraMessageBox.Show(strMsg, "系统提示");
                this.DialogResult = DialogResult.OK;
            }
        }

        //修改绑定
        private void InivitForm()
        {
            SqliteClass sqlDb = new SqliteClass();
            Entity_Sys_收文登记表 objGS = new Entity_Sys_收文登记表();
            objGS = sqlDb.GetSys_收文登记表ByID(mID);
            
            this.txt收到时间.Text = objGS.收到时间.ToShortDateString();
            this.txt来文机关.Text = objGS.来文机关;
            this.txt文号.Text = objGS.文号;
            this.txt文件名称.Text = objGS.文件名称;
            this.txt批办领导.Text = objGS.批办领导;
            this.txt收文数.Text = objGS.收到文件数.ToString();
            this.txt备注.Text = objGS.备注;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.F5))
            {
                this.simpleBtnOK.PerformClick();
                return true;
            }
            if (keyData == (Keys.Escape))
            {
                this.simpleBtnCancel.PerformClick();
                return true;
            }
            if (keyData == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
                //if (this.ActiveControl.Parent.Parent.Name == "dataGridViewResult")
                //    SendKeys.Send("{Down}");
                //else
                //    SendKeys.Send("{TAB}");
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        
    }
}