﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DXTempLate.Class;

namespace DXTempLate
{
    public partial class XtraFormUserInfo : DevExpress.XtraEditors.XtraForm
    {
        private SqliteClass oraClass;
        private PublicClass pubClass;

        private int mUserID;

        public XtraFormUserInfo(int nUserID = 0,bool isRead =false)
        {
            InitializeComponent();
            mUserID = nUserID;
            oraClass = new SqliteClass();
            pubClass = new PublicClass();

            if (isRead)
            {
                this.simpleBtnOK.Visible = false;                
            }
        }

        private void XtraFormUserInfo_Load(object sender, EventArgs e)
        {
            FormClass formClass = new FormClass();
            formClass.BindComboBox(this.comboBoxEditSex, 2);//性别
            formClass.BindComboBox(this.comboBoxXL, 3);//学历
            formClass.BindComboBox(this.comboBoxZW, 4);//职务
            formClass.BindComboBox(this.comboBoxEditBM, 1);//部门

            if (mUserID > 0)
            {
                InvitFormValue(mUserID);
            }
            else
            {
                this.txtUserName.Properties.ReadOnly = false;
                this.dateEditBirthday.Text = DateTime.Now.ToShortDateString();
            }
        }
        /// <summary>
        /// 初始化值
        /// </summary>
        /// <param name="nUserID"></param>
        private void InvitFormValue(int nUserID)
        {
            Entity_Sys_Employee objUser = new Entity_Sys_Employee();
            objUser = oraClass.GetSys_Employee(nUserID);
            if (objUser != null)
            {
                this.txtUserName.Text = objUser.Name;
                this.comboBoxEditSex.Text = pubClass.GetNameByCode(objUser.SEX, 2);
                this.dateEditBirthday.Text = objUser.BirtyDay.ToShortDateString();
                this.comboBoxXL.Text = pubClass.GetNameByCode(objUser.XL, 3);
                this.comboBoxZW.Text = pubClass.GetNameByCode(objUser.ZW, 4);
                this.comboBoxEditBM.Text = pubClass.GetNameByCode(objUser.BM, 1);
                this.memoEditRemark.Text = objUser.Remark;

                this.txtPhone.Text = objUser.Photo;
                this.txtTel.Text = objUser.SJ;
                this.txtEmail.Text = objUser.Email;
                this.memoEditAddress.Text = objUser.Address;
            }
        }

        private void simpleBtnImg_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "上传图片文件";
            fileDialog.Filter = "图片文件(*.jpg)|*.jpg|图片文件(*.gif)|*.gif|图片文件(*.bmp)|*.bmp|所有文件(*.*)|*.*";
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                this.pictureEdit1.Image = System.Drawing.Image.FromFile(fileDialog.FileName);
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleBtnOK_Click(object sender, EventArgs e)
        {
            Entity_Sys_Employee objUser = new Entity_Sys_Employee();
            objUser.Name = this.txtUserName.Text.Trim();
            objUser.SEX = pubClass.GetCodeByName(this.comboBoxEditSex.Text, 2);
            objUser.BirtyDay = Convert.ToDateTime(this.dateEditBirthday.Text);
            objUser.XL = pubClass.GetCodeByName(this.comboBoxXL.Text, 3);
            objUser.ZW = pubClass.GetCodeByName(this.comboBoxZW.Text, 4);
            objUser.BM = pubClass.GetCodeByName(this.comboBoxEditBM.Text, 1);
            objUser.Remark = this.memoEditRemark.Text.Trim();

            objUser.Photo = this.txtPhone.Text.Trim();
            objUser.SJ = this.txtTel.Text.Trim();
            objUser.Email = this.txtEmail.Text.Trim();
            objUser.Address = this.memoEditAddress.Text.Trim();

            if (mUserID > 0)
            {
                objUser.ID = mUserID;
            }
            else {
                objUser.ID = pubClass.GetMaxID("Sys_Employee") + 1;
            }

            Entity_Sys_Employee objcur = new Entity_Sys_Employee();
            objcur = oraClass.GetSys_Employee(mUserID);
            objUser.LoginType = 0;
            objUser.Password = "00";
            objUser.ImgName = objcur.ImgName;

            if (oraClass.UpdateSys_Employee(objUser))
            {
                string strMessage = "";
                if (mUserID > 0)
                {
                    strMessage = "用户信息修改成功";
                }
                else
                {
                    strMessage = "用户信息添加成功";
                
                }
                XtraMessageBox.Show(strMessage, "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}