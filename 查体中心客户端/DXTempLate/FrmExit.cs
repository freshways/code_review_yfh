﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DXTempLate
{
    public partial class FrmExit : DevExpress.XtraEditors.XtraForm
    {
        public FrmExit()
        {
            InitializeComponent();
        }

        private void FrmExit_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (this.textEdit1.Text.Trim().Equals(DateTime.Now.ToString("yyyyMMdd")))
            {
                this.labelControl2.Visible = false;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                this.labelControl2.Visible = true;
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
