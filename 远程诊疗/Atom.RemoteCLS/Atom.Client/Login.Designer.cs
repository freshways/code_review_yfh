﻿namespace Atom.Client
{
    partial class Login
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtpws = new Atom.Client.Controls.CDisplayBox();
            this.txtuser = new Atom.Client.Controls.CDisplayBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::Atom.Client.Properties.Resources.loginbg;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.txtpws);
            this.panel1.Controls.Add(this.txtuser);
            this.panel1.Controls.Add(this.simpleButton1);
            this.panel1.Controls.Add(this.checkEdit1);
            this.panel1.Location = new System.Drawing.Point(237, 222);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(505, 269);
            this.panel1.TabIndex = 4;
            // 
            // txtpws
            // 
            this.txtpws.AllowMultiline = false;
            this.txtpws.Label = "密码 ";
            this.txtpws.LabelFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtpws.LabelForeColor = System.Drawing.Color.White;
            this.txtpws.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtpws.LabelWidth = 100F;
            this.txtpws.Location = new System.Drawing.Point(53, 107);
            this.txtpws.Name = "txtpws";
            this.txtpws.Size = new System.Drawing.Size(360, 50);
            this.txtpws.TabIndex = 3;
            this.txtpws.TextBackColor = System.Drawing.Color.White;
            this.txtpws.TextFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtpws.TextFontPassWordChar = '*';
            this.txtpws.TextReadOnly = false;
            this.txtpws.Value = null;
            // 
            // txtuser
            // 
            this.txtuser.AllowMultiline = false;
            this.txtuser.Label = "账号 ";
            this.txtuser.LabelFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtuser.LabelForeColor = System.Drawing.Color.White;
            this.txtuser.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtuser.LabelWidth = 100F;
            this.txtuser.Location = new System.Drawing.Point(53, 48);
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(360, 50);
            this.txtuser.TabIndex = 1;
            this.txtuser.TextBackColor = System.Drawing.Color.White;
            this.txtuser.TextFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtuser.TextFontPassWordChar = '\0';
            this.txtuser.TextReadOnly = false;
            this.txtuser.Value = null;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.BorderColor = System.Drawing.Color.White;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseBorderColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(290, 173);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(113, 45);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "登 陆";
            this.simpleButton1.Click += new System.EventHandler(this.myButton1_Click);
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(129, 186);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.checkEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.checkEdit1.Properties.Caption = "记住密码";
            this.checkEdit1.Size = new System.Drawing.Size(119, 23);
            this.checkEdit1.TabIndex = 5;
            // 
            // Login
            // 
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Stretch;
            this.BackgroundImageStore = global::Atom.Client.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(978, 596);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Login_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private Controls.CDisplayBox txtuser;
        private Controls.CDisplayBox txtpws;
    }
}
