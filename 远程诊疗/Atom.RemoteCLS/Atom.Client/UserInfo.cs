﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atom.Client
{
    public class UserInfo
    {
        public static string UserID { get; set; }

        public static string UserPwd { get; set; }

        public static string UserTpye { get; set; }
    }

    public class PeopleInfo
    {
        public string Name { get; set; }

        public string  Sex { get; set; }

        public string  britday { get; set; }

        public string CardNo { get; set; }

        public string Address { get; set; }

        public string ZYH { get; set; }

        public int ZYID { get; set; }

        public string Age { get; set; }
    }
}
