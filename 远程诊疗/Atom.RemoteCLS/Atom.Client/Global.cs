﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO ;
using System.Diagnostics;
using System.Reflection;
using System.Net;
using System.Configuration;
using System.Runtime.InteropServices;

namespace Atom.Client
{
    /// <summary>
    /// 全局访问类
    /// </summary>
    public sealed class Global
    {
        #region 变量
        /// <summary>
        /// 服务器域名 
        /// </summary>
        public static string ServerDomain = ConfigurationManager.AppSettings["ServerDomain"].ToString();//"127.0.0.1";
      
        /// <summary>
        /// 消息服务器端口
        /// </summary>
        public static int ServerMsgPort = Convert.ToInt32(ConfigurationManager.AppSettings["ServerMsgPort"].ToString());//6500;

        /// <summary>
        /// 更新端口
        /// </summary>
        public static int UpLoadPort = Convert.ToInt32(ConfigurationManager.AppSettings["UpDatePort"].ToString());

        //HIS \ LIS 服务器和数据库
        public static string HISDB = ConfigurationManager.AppSettings["HISDB"].ToString();

        /// <summary>
        /// 服务器文件服务主机信息
        /// </summary>
        public static IPEndPoint FileTransmitServerEP=null;
        /// <summary>
        /// 
        /// </summary>
        public static IPEndPoint AVTransmitServerEP = null;

        /// <summary>
        /// 图片服务器主机信息
        /// </summary>
        public static IPEndPoint ImageServerEP = null;
        /// <summary>
        /// 离线文件服务器主机信息
        /// </summary>
        public static IPEndPoint OfflineFileServerEP = null;
        /// <summary>
        /// 会议服务器 
        /// </summary>
        public static string ServerConference = "@";

        /// <summary>
        /// 表情图片列表缓存
        /// </summary>
        public static System.Windows.Forms.ImageList ImageListFace = new ImageList();

        /// <summary>
        /// 消息管理窗
        /// </summary>
        //public static  FormDataManage FormDataManageage = null;

        /// <summary>
        /// 当前登录用户ID
        /// </summary>
        public static string CurrUserID = "";

        /// <summary>
        /// 当前登录用户Name
        /// </summary>
        public static string CurrUserName = "";

        #endregion

        #region 判断系统进程中程序是否已经运行有进行，有返回进程，无返回空值
        /// <summary>
        /// 判断系统进程中程序是否已经运行有进行，有返回进程，无返回空值
        /// </summary>
        /// <returns></returns>
        public static Process RunningInstance()
        {
            Process current = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(current.ProcessName);

            //Loop through the running processes in with the same name
            foreach (Process process in processes)
            {
                //Ignore the current process
                if (process.Id != current.Id)
                {
                    //Make sure that the process is running from the exe file.

                    if (Assembly.GetExecutingAssembly().Location.Replace("/", "\\") == current.MainModule.FileName)
                    {
                        //Return the other process instance.
                        return process;
                    }
                }
            }
            return null;
        }
        #endregion

    }

    public static class ControlsHelper
    {
        #region ini文件操作
        [DllImport("kernel32")]
        public static extern long WritePrivateProfileString(string section, string key, string val, string filePath);//写入ini
        [DllImport("kernel32")]
        public static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath); //读取ini
        /// <summary>
        /// 写入ini方法
        /// </summary>
        /// <param name="section">段</param>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <param name="iniPath">路径</param>
        public static void IniWriteValue(string section, string key, string value, string iniPath)
        {
            WritePrivateProfileString(section, key, value, iniPath);
        }
        /// <summary>
        /// 读取ini方法
        /// </summary>
        /// <param name="section">段</param>
        /// <param name="key">键</param>
        /// <param name="iniPath">路径</param>
        /// <returns></returns>
        public static string IniReadValue(string section, string key, string iniPath)
        {
            StringBuilder sBulider = new StringBuilder(500);
            int i = GetPrivateProfileString(section, key, "", sBulider, 500, iniPath);
            return sBulider.ToString();
        }
        #endregion
    }
}
