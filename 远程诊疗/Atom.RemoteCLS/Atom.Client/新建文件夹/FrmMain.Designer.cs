﻿namespace Atom.Client
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            MySkin.Controls.MyListBoxItem myListBoxItem3 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem4 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxItem myListBoxItem4 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem5 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem6 = new MySkin.Controls.MyListBoxSubItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.picHome = new System.Windows.Forms.PictureBox();
            this.picSetting = new System.Windows.Forms.PictureBox();
            this.picMin = new System.Windows.Forms.PictureBox();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.myPanelMain = new MySkin.Controls.MyPanel();
            this.video1 = new Atom.Client.Controls.video();
            this.messagePanel1 = new Atom.Controls.MessagePanel();
            this.tsButAV = new System.Windows.Forms.Button();
            this.BtnSendMsg = new System.Windows.Forms.Button();
            this.BtnSendBr = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel3 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.myListBox1 = new MySkin.Controls.MyListBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.myPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel3.SuspendLayout();
            this.dockPanel3_Container.SuspendLayout();
            this.dockPanel2.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::Atom.Client.Properties.Resources.bkBar;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.picHome);
            this.panel1.Controls.Add(this.picSetting);
            this.panel1.Controls.Add(this.picMin);
            this.panel1.Controls.Add(this.picClose);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1302, 63);
            this.panel1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("宋体", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(145, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(901, 63);
            this.label2.TabIndex = 9;
            this.label2.Text = "2016-01-21 星期日 17:09";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picHome
            // 
            this.picHome.Dock = System.Windows.Forms.DockStyle.Right;
            this.picHome.Image = global::Atom.Client.Properties.Resources.home;
            this.picHome.Location = new System.Drawing.Point(1046, 0);
            this.picHome.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picHome.Name = "picHome";
            this.picHome.Size = new System.Drawing.Size(61, 63);
            this.picHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picHome.TabIndex = 14;
            this.picHome.TabStop = false;
            this.toolTip1.SetToolTip(this.picHome, "主页");
            this.picHome.Click += new System.EventHandler(this.picHome_Click);
            // 
            // picSetting
            // 
            this.picSetting.Dock = System.Windows.Forms.DockStyle.Right;
            this.picSetting.Image = global::Atom.Client.Properties.Resources.docsetting;
            this.picSetting.Location = new System.Drawing.Point(1107, 0);
            this.picSetting.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picSetting.Name = "picSetting";
            this.picSetting.Size = new System.Drawing.Size(65, 63);
            this.picSetting.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picSetting.TabIndex = 13;
            this.picSetting.TabStop = false;
            this.toolTip1.SetToolTip(this.picSetting, "设置");
            this.picSetting.Click += new System.EventHandler(this.picSetting_Click);
            // 
            // picMin
            // 
            this.picMin.Dock = System.Windows.Forms.DockStyle.Right;
            this.picMin.Image = global::Atom.Client.Properties.Resources.最小化_03;
            this.picMin.Location = new System.Drawing.Point(1172, 0);
            this.picMin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picMin.Name = "picMin";
            this.picMin.Size = new System.Drawing.Size(65, 63);
            this.picMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picMin.TabIndex = 12;
            this.picMin.TabStop = false;
            this.toolTip1.SetToolTip(this.picMin, "最小化");
            this.picMin.Click += new System.EventHandler(this.picMin_Click);
            // 
            // picClose
            // 
            this.picClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.picClose.Image = global::Atom.Client.Properties.Resources.关闭_03;
            this.picClose.Location = new System.Drawing.Point(1237, 0);
            this.picClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(65, 63);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picClose.TabIndex = 10;
            this.picClose.TabStop = false;
            this.toolTip1.SetToolTip(this.picClose, "关闭");
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = global::Atom.Client.Properties.Resources.Offline;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(145, 63);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(69, 24);
            this.toolStripStatusLabel1.Text = "正在运行";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.AutoToolTip = true;
            this.toolStripStatusLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(1184, 24);
            this.toolStripStatusLabel2.Spring = true;
            this.toolStripStatusLabel2.Text = "技术支持©临沂市阳光科技有限公司";
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripDropDownButton1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 840);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1310, 29);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.Image = global::Atom.Client.Properties.Resources.discussion_ico;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.ShowDropDownArrow = false;
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(42, 27);
            this.toolStripDropDownButton1.Text = "0";
            this.toolStripDropDownButton1.Click += new System.EventHandler(this.toolStripDropDownButton1_Click);
            // 
            // myPanelMain
            // 
            this.myPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.myPanelMain.Controls.Add(this.video1);
            this.myPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myPanelMain.Location = new System.Drawing.Point(302, 93);
            this.myPanelMain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.myPanelMain.Name = "myPanelMain";
            this.myPanelMain.Padding = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.myPanelMain.Size = new System.Drawing.Size(679, 747);
            this.myPanelMain.TabIndex = 8;
            // 
            // video1
            // 
            this.video1.BackColor = System.Drawing.Color.FloralWhite;
            this.video1.IsInvite = false;
            this.video1.Location = new System.Drawing.Point(48, 31);
            this.video1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.video1.Name = "video1";
            this.video1.Size = new System.Drawing.Size(581, 594);
            this.video1.TabIndex = 2;
            // 
            // messagePanel1
            // 
            this.messagePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.messagePanel1.Location = new System.Drawing.Point(0, 56);
            this.messagePanel1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.messagePanel1.Name = "messagePanel1";
            this.messagePanel1.Size = new System.Drawing.Size(321, 290);
            this.messagePanel1.TabIndex = 10;
            this.messagePanel1.TitleVisible = false;
            // 
            // tsButAV
            // 
            this.tsButAV.Location = new System.Drawing.Point(153, 387);
            this.tsButAV.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tsButAV.Name = "tsButAV";
            this.tsButAV.Size = new System.Drawing.Size(140, 61);
            this.tsButAV.TabIndex = 0;
            this.tsButAV.Text = "视频";
            this.tsButAV.UseVisualStyleBackColor = true;
            // 
            // BtnSendMsg
            // 
            this.BtnSendMsg.Location = new System.Drawing.Point(5, 387);
            this.BtnSendMsg.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnSendMsg.Name = "BtnSendMsg";
            this.BtnSendMsg.Size = new System.Drawing.Size(140, 61);
            this.BtnSendMsg.TabIndex = 0;
            this.BtnSendMsg.Text = "发送";
            this.BtnSendMsg.UseVisualStyleBackColor = true;
            this.BtnSendMsg.Click += new System.EventHandler(this.BtnSendMsg_Click);
            // 
            // BtnSendBr
            // 
            this.BtnSendBr.Location = new System.Drawing.Point(5, 496);
            this.BtnSendBr.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnSendBr.Name = "BtnSendBr";
            this.BtnSendBr.Size = new System.Drawing.Size(140, 61);
            this.BtnSendBr.TabIndex = 0;
            this.BtnSendBr.Text = "发送病人信息";
            this.BtnSendBr.UseVisualStyleBackColor = true;
            this.BtnSendBr.Visible = false;
            this.BtnSendBr.Click += new System.EventHandler(this.BtnSendBr_Click);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(321, 56);
            this.label9.TabIndex = 5;
            this.label9.Text = "对话信息";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(0, 316);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(294, 56);
            this.label3.TabIndex = 4;
            this.label3.Text = "在线医生";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.ShowCloseButton = false;
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel3,
            this.dockPanel2,
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel3
            // 
            this.dockPanel3.Controls.Add(this.dockPanel3_Container);
            this.dockPanel3.Dock = DevExpress.XtraBars.Docking.DockingStyle.Top;
            this.dockPanel3.FloatVertical = true;
            this.dockPanel3.ID = new System.Guid("3ffd6e54-db03-4cef-80ec-a402c3eeb4fa");
            this.dockPanel3.Location = new System.Drawing.Point(0, 0);
            this.dockPanel3.Name = "dockPanel3";
            this.dockPanel3.OriginalSize = new System.Drawing.Size(200, 93);
            this.dockPanel3.Size = new System.Drawing.Size(1310, 93);
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Controls.Add(this.panel1);
            this.dockPanel3_Container.Location = new System.Drawing.Point(4, 26);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(1302, 63);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // dockPanel2
            // 
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel2.ID = new System.Guid("3bd4463f-a1a5-474e-a2e3-7524d0bb2e1f");
            this.dockPanel2.Location = new System.Drawing.Point(981, 93);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(329, 729);
            this.dockPanel2.Size = new System.Drawing.Size(329, 747);
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.BtnSendBr);
            this.dockPanel2_Container.Controls.Add(this.tsButAV);
            this.dockPanel2_Container.Controls.Add(this.messagePanel1);
            this.dockPanel2_Container.Controls.Add(this.BtnSendMsg);
            this.dockPanel2_Container.Controls.Add(this.label9);
            this.dockPanel2_Container.Location = new System.Drawing.Point(4, 26);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(321, 717);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("3d68f32f-b3f4-480b-be54-2a3cb2da51b5");
            this.dockPanel1.Location = new System.Drawing.Point(0, 93);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(302, 703);
            this.dockPanel1.Size = new System.Drawing.Size(302, 747);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.groupBox1);
            this.dockPanel1_Container.Controls.Add(this.label1);
            this.dockPanel1_Container.Controls.Add(this.pictureBox1);
            this.dockPanel1_Container.Controls.Add(this.label3);
            this.dockPanel1_Container.Controls.Add(this.myListBox1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 26);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(294, 717);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(8, 104);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 176);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "医院简介";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 18);
            this.label4.TabIndex = 0;
            this.label4.Text = "沂水县人民医院";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(53, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 63);
            this.label1.TabIndex = 12;
            this.label1.Text = "用户名";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Atom.Client.Properties.Resources.placeholder_person_ico;
            this.pictureBox1.Location = new System.Drawing.Point(0, 5);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(57, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // myListBox1
            // 
            this.myListBox1.BackColor = System.Drawing.Color.Transparent;
            this.myListBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.myListBox1.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.myListBox1.ForeColor = System.Drawing.Color.Black;
            this.myListBox1.ItemColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.myListBox1.ItemMouseOnColor = System.Drawing.Color.LightSkyBlue;
            myListBoxItem3.GroupID = "";
            myListBoxItem3.IsOpen = true;
            myListBoxSubItem4.DisplayName = "沂水县人民医院";
            myListBoxSubItem4.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem4.HeadImage")));
            myListBoxSubItem4.ID = 20038;
            myListBoxSubItem4.IpAddress = null;
            myListBoxSubItem4.IsTwinkle = false;
            myListBoxSubItem4.NicName = "20038";
            myListBoxSubItem4.PersonalMsg = "离线";
            myListBoxSubItem4.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxItem3.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem4});
            myListBoxItem3.Text = "县医院";
            myListBoxItem4.GroupID = "";
            myListBoxItem4.IsOpen = true;
            myListBoxSubItem5.DisplayName = "杨庄医院";
            myListBoxSubItem5.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem5.HeadImage")));
            myListBoxSubItem5.ID = 10004;
            myListBoxSubItem5.IpAddress = null;
            myListBoxSubItem5.IsTwinkle = false;
            myListBoxSubItem5.NicName = "10004";
            myListBoxSubItem5.PersonalMsg = "离线";
            myListBoxSubItem5.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem6.DisplayName = "张楼子卫生室";
            myListBoxSubItem6.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem6.HeadImage")));
            myListBoxSubItem6.ID = 20048;
            myListBoxSubItem6.IpAddress = null;
            myListBoxSubItem6.IsTwinkle = false;
            myListBoxSubItem6.NicName = "20048";
            myListBoxSubItem6.PersonalMsg = "离线";
            myListBoxSubItem6.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxItem4.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem5,
            myListBoxSubItem6});
            myListBoxItem4.Text = "乡镇医院";
            this.myListBox1.Items.AddRange(new MySkin.Controls.MyListBoxItem[] {
            myListBoxItem3,
            myListBoxItem4});
            this.myListBox1.Location = new System.Drawing.Point(0, 372);
            this.myListBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.myListBox1.Name = "myListBox1";
            this.myListBox1.ScrollArrowBackColor = System.Drawing.Color.Silver;
            this.myListBox1.ScrollBackColor = System.Drawing.Color.Transparent;
            this.myListBox1.ScrollSliderDefaultColor = System.Drawing.Color.LightGray;
            this.myListBox1.ScrollSliderDownColor = System.Drawing.Color.Gainsboro;
            this.myListBox1.Size = new System.Drawing.Size(294, 345);
            this.myListBox1.SubItemColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.myListBox1.SubItemSelectColor = System.Drawing.Color.DeepSkyBlue;
            this.myListBox1.TabIndex = 12;
            this.myListBox1.Text = "myListBox1";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1310, 869);
            this.Controls.Add(this.myPanelMain);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.dockPanel2);
            this.Controls.Add(this.dockPanel3);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmMain";
            this.Text = "FrmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.myPanelMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel3.ResumeLayout(false);
            this.dockPanel3_Container.ResumeLayout(false);
            this.dockPanel2.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label3;
        private Atom.Client.Controls.video video1;
        private System.Windows.Forms.Button BtnSendBr;
        private System.Windows.Forms.Button tsButAV;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox picHome;
        private System.Windows.Forms.PictureBox picSetting;
        private System.Windows.Forms.PictureBox picMin;
        private System.Windows.Forms.PictureBox picClose;
        private MySkin.Controls.MyPanel myPanelMain;
        private System.Windows.Forms.Button BtnSendMsg;
        private Atom.Controls.MessagePanel messagePanel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private MySkin.Controls.MyListBox myListBox1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;

    }
}