﻿namespace Atom.Client
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            MySkin.Controls.MyListBoxItem myListBoxItem1 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem1 = new MySkin.Controls.MyListBoxSubItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem2 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem3 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem4 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxItem myListBoxItem2 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem5 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem6 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem7 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxItem myListBoxItem3 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem8 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem9 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem10 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxItem myListBoxItem4 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem11 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem12 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem13 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxItem myListBoxItem5 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem14 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem15 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxItem myListBoxItem6 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem16 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem17 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem18 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxItem myListBoxItem7 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem19 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem20 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem21 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem22 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxItem myListBoxItem8 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem23 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem24 = new MySkin.Controls.MyListBoxSubItem();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.picHome = new System.Windows.Forms.PictureBox();
            this.picSetting = new System.Windows.Forms.PictureBox();
            this.picMin = new System.Windows.Forms.PictureBox();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.myPanelMain = new MySkin.Controls.MyPanel();
            this.messagePanel1 = new Atom.Controls.MessagePanel();
            this.label9 = new System.Windows.Forms.Label();
            this.video1 = new Atom.Client.Controls.video();
            this.label3 = new System.Windows.Forms.Label();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel3 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.BtnSendBr = new DevExpress.XtraEditors.SimpleButton();
            this.tsButAV = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSendMsg = new DevExpress.XtraEditors.SimpleButton();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.myListBox1 = new MySkin.Controls.MyListBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel3.SuspendLayout();
            this.dockPanel3_Container.SuspendLayout();
            this.dockPanel2.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(50, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 66);
            this.label1.TabIndex = 7;
            this.label1.Text = "用户名";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::Atom.Client.Properties.Resources.bkBar;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.picHome);
            this.panel1.Controls.Add(this.picSetting);
            this.panel1.Controls.Add(this.picMin);
            this.panel1.Controls.Add(this.picClose);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1138, 66);
            this.panel1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("宋体", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(350, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(564, 66);
            this.label2.TabIndex = 9;
            this.label2.Text = "2016-01-21 星期日 17:09";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picHome
            // 
            this.picHome.Dock = System.Windows.Forms.DockStyle.Right;
            this.picHome.Image = global::Atom.Client.Properties.Resources.home;
            this.picHome.Location = new System.Drawing.Point(914, 0);
            this.picHome.Margin = new System.Windows.Forms.Padding(4);
            this.picHome.Name = "picHome";
            this.picHome.Size = new System.Drawing.Size(53, 66);
            this.picHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picHome.TabIndex = 14;
            this.picHome.TabStop = false;
            this.toolTip1.SetToolTip(this.picHome, "主页");
            this.picHome.Click += new System.EventHandler(this.picHome_Click);
            // 
            // picSetting
            // 
            this.picSetting.Dock = System.Windows.Forms.DockStyle.Right;
            this.picSetting.Image = global::Atom.Client.Properties.Resources.docsetting;
            this.picSetting.Location = new System.Drawing.Point(967, 0);
            this.picSetting.Margin = new System.Windows.Forms.Padding(4);
            this.picSetting.Name = "picSetting";
            this.picSetting.Size = new System.Drawing.Size(57, 66);
            this.picSetting.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picSetting.TabIndex = 13;
            this.picSetting.TabStop = false;
            this.toolTip1.SetToolTip(this.picSetting, "设置");
            this.picSetting.Click += new System.EventHandler(this.picSetting_Click);
            // 
            // picMin
            // 
            this.picMin.Dock = System.Windows.Forms.DockStyle.Right;
            this.picMin.Image = global::Atom.Client.Properties.Resources.最小化_03;
            this.picMin.Location = new System.Drawing.Point(1024, 0);
            this.picMin.Margin = new System.Windows.Forms.Padding(4);
            this.picMin.Name = "picMin";
            this.picMin.Size = new System.Drawing.Size(57, 66);
            this.picMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picMin.TabIndex = 12;
            this.picMin.TabStop = false;
            this.toolTip1.SetToolTip(this.picMin, "最小化");
            this.picMin.Click += new System.EventHandler(this.picMin_Click);
            // 
            // picClose
            // 
            this.picClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.picClose.Image = global::Atom.Client.Properties.Resources.关闭_03;
            this.picClose.Location = new System.Drawing.Point(1081, 0);
            this.picClose.Margin = new System.Windows.Forms.Padding(4);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(57, 66);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picClose.TabIndex = 10;
            this.picClose.TabStop = false;
            this.toolTip1.SetToolTip(this.picClose, "关闭");
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = global::Atom.Client.Properties.Resources.Offline;
            this.pictureBox2.Location = new System.Drawing.Point(223, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(127, 66);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::Atom.Client.Properties.Resources.placeholder_person_ico;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 66);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(56, 29);
            this.toolStripStatusLabel1.Text = "正在运行";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.AutoToolTip = true;
            this.toolStripStatusLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(1038, 29);
            this.toolStripStatusLabel2.Spring = true;
            this.toolStripStatusLabel2.Text = "技术支持©临沂市阳光科技有限公司";
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripDropDownButton1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 642);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 12, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1146, 34);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.Image = global::Atom.Client.Properties.Resources.discussion_ico;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.ShowDropDownArrow = false;
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(39, 32);
            this.toolStripDropDownButton1.Text = "0";
            this.toolStripDropDownButton1.Click += new System.EventHandler(this.toolStripDropDownButton1_Click);
            // 
            // myPanelMain
            // 
            this.myPanelMain.BackColor = System.Drawing.Color.Transparent;
            this.myPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myPanelMain.Location = new System.Drawing.Point(302, 93);
            this.myPanelMain.Name = "myPanelMain";
            this.myPanelMain.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.myPanelMain.Size = new System.Drawing.Size(515, 549);
            this.myPanelMain.TabIndex = 8;
            // 
            // messagePanel1
            // 
            this.messagePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.messagePanel1.Location = new System.Drawing.Point(0, 44);
            this.messagePanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.messagePanel1.Name = "messagePanel1";
            this.messagePanel1.Size = new System.Drawing.Size(321, 226);
            this.messagePanel1.TabIndex = 10;
            this.messagePanel1.TitleVisible = false;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(321, 44);
            this.label9.TabIndex = 5;
            this.label9.Text = "对话信息";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // video1
            // 
            this.video1.BackColor = System.Drawing.Color.FloralWhite;
            this.video1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.video1.IsInvite = false;
            this.video1.Location = new System.Drawing.Point(0, 288);
            this.video1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.video1.Name = "video1";
            this.video1.Size = new System.Drawing.Size(294, 234);
            this.video1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(294, 44);
            this.label3.TabIndex = 4;
            this.label3.Text = "在线医生";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.ShowCloseButton = false;
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel3,
            this.dockPanel2,
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel3
            // 
            this.dockPanel3.Controls.Add(this.dockPanel3_Container);
            this.dockPanel3.Dock = DevExpress.XtraBars.Docking.DockingStyle.Top;
            this.dockPanel3.FloatVertical = true;
            this.dockPanel3.ID = new System.Guid("3ffd6e54-db03-4cef-80ec-a402c3eeb4fa");
            this.dockPanel3.Location = new System.Drawing.Point(0, 0);
            this.dockPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dockPanel3.Name = "dockPanel3";
            this.dockPanel3.Options.AllowFloating = false;
            this.dockPanel3.Options.ShowCloseButton = false;
            this.dockPanel3.Options.ShowMaximizeButton = false;
            this.dockPanel3.OriginalSize = new System.Drawing.Size(200, 93);
            this.dockPanel3.Size = new System.Drawing.Size(1146, 93);
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Controls.Add(this.panel1);
            this.dockPanel3_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel3_Container.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(1138, 66);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // dockPanel2
            // 
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel2.ID = new System.Guid("3bd4463f-a1a5-474e-a2e3-7524d0bb2e1f");
            this.dockPanel2.Location = new System.Drawing.Point(817, 93);
            this.dockPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(329, 729);
            this.dockPanel2.Size = new System.Drawing.Size(329, 549);
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.BtnSendBr);
            this.dockPanel2_Container.Controls.Add(this.tsButAV);
            this.dockPanel2_Container.Controls.Add(this.BtnSendMsg);
            this.dockPanel2_Container.Controls.Add(this.messagePanel1);
            this.dockPanel2_Container.Controls.Add(this.label9);
            this.dockPanel2_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel2_Container.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(321, 522);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // BtnSendBr
            // 
            this.BtnSendBr.Image = ((System.Drawing.Image)(resources.GetObject("BtnSendBr.Image")));
            this.BtnSendBr.Location = new System.Drawing.Point(5, 370);
            this.BtnSendBr.Name = "BtnSendBr";
            this.BtnSendBr.Size = new System.Drawing.Size(122, 47);
            this.BtnSendBr.TabIndex = 11;
            this.BtnSendBr.Text = "发送病人信息";
            this.BtnSendBr.Click += new System.EventHandler(this.BtnSendBr_Click);
            // 
            // tsButAV
            // 
            this.tsButAV.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.tsButAV.Appearance.Options.UseFont = true;
            this.tsButAV.Image = ((System.Drawing.Image)(resources.GetObject("tsButAV.Image")));
            this.tsButAV.Location = new System.Drawing.Point(142, 301);
            this.tsButAV.Name = "tsButAV";
            this.tsButAV.Size = new System.Drawing.Size(122, 47);
            this.tsButAV.TabIndex = 11;
            this.tsButAV.Text = "视频";
            // 
            // BtnSendMsg
            // 
            this.BtnSendMsg.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.BtnSendMsg.Appearance.Options.UseFont = true;
            this.BtnSendMsg.Image = ((System.Drawing.Image)(resources.GetObject("BtnSendMsg.Image")));
            this.BtnSendMsg.Location = new System.Drawing.Point(5, 301);
            this.BtnSendMsg.Name = "BtnSendMsg";
            this.BtnSendMsg.Size = new System.Drawing.Size(122, 47);
            this.BtnSendMsg.TabIndex = 11;
            this.BtnSendMsg.Text = "发送";
            this.BtnSendMsg.Click += new System.EventHandler(this.BtnSendMsg_Click);
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("3d68f32f-b3f4-480b-be54-2a3cb2da51b5");
            this.dockPanel1.Location = new System.Drawing.Point(0, 93);
            this.dockPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(302, 703);
            this.dockPanel1.Size = new System.Drawing.Size(302, 549);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.myListBox1);
            this.dockPanel1_Container.Controls.Add(this.video1);
            this.dockPanel1_Container.Controls.Add(this.label3);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(294, 522);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // myListBox1
            // 
            this.myListBox1.BackColor = System.Drawing.Color.Transparent;
            this.myListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myListBox1.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.myListBox1.ForeColor = System.Drawing.Color.Black;
            this.myListBox1.ItemColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.myListBox1.ItemMouseOnColor = System.Drawing.Color.LightSkyBlue;
            myListBoxItem1.GroupID = "";
            myListBoxItem1.IsOpen = true;
            myListBoxSubItem1.DisplayName = "病人服务中心";
            myListBoxSubItem1.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem1.HeadImage")));
            myListBoxSubItem1.ID = 20038;
            myListBoxSubItem1.IpAddress = null;
            myListBoxSubItem1.IsTwinkle = false;
            myListBoxSubItem1.NicName = "20038";
            myListBoxSubItem1.PersonalMsg = "离线";
            myListBoxSubItem1.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem1.Types = "1";
            myListBoxSubItem2.DisplayName = "影像科";
            myListBoxSubItem2.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem2.HeadImage")));
            myListBoxSubItem2.ID = 1100;
            myListBoxSubItem2.IpAddress = null;
            myListBoxSubItem2.IsTwinkle = false;
            myListBoxSubItem2.NicName = "1100";
            myListBoxSubItem2.PersonalMsg = "离线";
            myListBoxSubItem2.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem2.Types = "1";
            myListBoxSubItem3.DisplayName = "急诊";
            myListBoxSubItem3.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem3.HeadImage")));
            myListBoxSubItem3.ID = 1137;
            myListBoxSubItem3.IpAddress = null;
            myListBoxSubItem3.IsTwinkle = false;
            myListBoxSubItem3.NicName = "1137";
            myListBoxSubItem3.PersonalMsg = "离线";
            myListBoxSubItem3.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem3.Types = "1";
            myListBoxSubItem4.DisplayName = "远程会诊中心";
            myListBoxSubItem4.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem4.HeadImage")));
            myListBoxSubItem4.ID = 1138;
            myListBoxSubItem4.IpAddress = null;
            myListBoxSubItem4.IsTwinkle = false;
            myListBoxSubItem4.NicName = "1138";
            myListBoxSubItem4.PersonalMsg = "离线";
            myListBoxSubItem4.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem4.Types = "1";
            myListBoxItem1.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem1,
            myListBoxSubItem2,
            myListBoxSubItem3,
            myListBoxSubItem4});
            myListBoxItem1.Text = "沂水县人民医院";
            myListBoxItem2.GroupID = "";
            myListBoxSubItem5.DisplayName = "全科门诊";
            myListBoxSubItem5.HeadImage = null;
            myListBoxSubItem5.ID = 20026;
            myListBoxSubItem5.IpAddress = null;
            myListBoxSubItem5.IsTwinkle = false;
            myListBoxSubItem5.NicName = "20026";
            myListBoxSubItem5.PersonalMsg = "离线";
            myListBoxSubItem5.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem5.Types = "2";
            myListBoxSubItem6.DisplayName = "影像科";
            myListBoxSubItem6.HeadImage = null;
            myListBoxSubItem6.ID = 20049;
            myListBoxSubItem6.IpAddress = null;
            myListBoxSubItem6.IsTwinkle = false;
            myListBoxSubItem6.NicName = "20049";
            myListBoxSubItem6.PersonalMsg = "离线";
            myListBoxSubItem6.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem6.Types = "2";
            myListBoxSubItem7.DisplayName = "春水社区卫生室";
            myListBoxSubItem7.HeadImage = null;
            myListBoxSubItem7.ID = 20031;
            myListBoxSubItem7.IpAddress = null;
            myListBoxSubItem7.IsTwinkle = false;
            myListBoxSubItem7.NicName = "20031";
            myListBoxSubItem7.PersonalMsg = "离线";
            myListBoxSubItem7.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem7.Types = "3";
            myListBoxItem2.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem5,
            myListBoxSubItem6,
            myListBoxSubItem7});
            myListBoxItem2.Text = "许家湖医院";
            myListBoxItem3.GroupID = "";
            myListBoxSubItem8.DisplayName = "全科门诊";
            myListBoxSubItem8.HeadImage = null;
            myListBoxSubItem8.ID = 21016;
            myListBoxSubItem8.IpAddress = null;
            myListBoxSubItem8.IsTwinkle = false;
            myListBoxSubItem8.NicName = "21016";
            myListBoxSubItem8.PersonalMsg = "离线";
            myListBoxSubItem8.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem8.Types = "2";
            myListBoxSubItem9.DisplayName = "影像科";
            myListBoxSubItem9.HeadImage = null;
            myListBoxSubItem9.ID = 21019;
            myListBoxSubItem9.IpAddress = null;
            myListBoxSubItem9.IsTwinkle = false;
            myListBoxSubItem9.NicName = "21019";
            myListBoxSubItem9.PersonalMsg = "离线";
            myListBoxSubItem9.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem9.Types = "2";
            myListBoxSubItem10.DisplayName = "卫生室";
            myListBoxSubItem10.HeadImage = null;
            myListBoxSubItem10.ID = 21018;
            myListBoxSubItem10.IpAddress = null;
            myListBoxSubItem10.IsTwinkle = false;
            myListBoxSubItem10.NicName = "21018";
            myListBoxSubItem10.PersonalMsg = "离线";
            myListBoxSubItem10.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem10.Types = "3";
            myListBoxItem3.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem8,
            myListBoxSubItem9,
            myListBoxSubItem10});
            myListBoxItem3.Text = "四十里医院";
            myListBoxItem4.GroupID = "";
            myListBoxSubItem11.DisplayName = "全科门诊";
            myListBoxSubItem11.HeadImage = null;
            myListBoxSubItem11.ID = 1236;
            myListBoxSubItem11.IpAddress = null;
            myListBoxSubItem11.IsTwinkle = false;
            myListBoxSubItem11.NicName = "1236";
            myListBoxSubItem11.PersonalMsg = "离线";
            myListBoxSubItem11.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem11.Types = "2";
            myListBoxSubItem12.DisplayName = "影像科";
            myListBoxSubItem12.HeadImage = null;
            myListBoxSubItem12.ID = 1237;
            myListBoxSubItem12.IpAddress = null;
            myListBoxSubItem12.IsTwinkle = false;
            myListBoxSubItem12.NicName = "1237";
            myListBoxSubItem12.PersonalMsg = "离线";
            myListBoxSubItem12.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem12.Types = "2";
            myListBoxSubItem13.DisplayName = "卫生室";
            myListBoxSubItem13.HeadImage = null;
            myListBoxSubItem13.ID = 1240;
            myListBoxSubItem13.IpAddress = null;
            myListBoxSubItem13.IsTwinkle = false;
            myListBoxSubItem13.NicName = "1240";
            myListBoxSubItem13.PersonalMsg = "离线";
            myListBoxSubItem13.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem13.Types = "3";
            myListBoxItem4.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem11,
            myListBoxSubItem12,
            myListBoxSubItem13});
            myListBoxItem4.Text = "高桥医院";
            myListBoxItem5.GroupID = "";
            myListBoxSubItem14.DisplayName = "内科";
            myListBoxSubItem14.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem14.HeadImage")));
            myListBoxSubItem14.ID = 1275;
            myListBoxSubItem14.IpAddress = null;
            myListBoxSubItem14.IsTwinkle = false;
            myListBoxSubItem14.NicName = "1275";
            myListBoxSubItem14.PersonalMsg = "离线";
            myListBoxSubItem14.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem14.Types = "2";
            myListBoxSubItem15.DisplayName = "外科";
            myListBoxSubItem15.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem15.HeadImage")));
            myListBoxSubItem15.ID = 1276;
            myListBoxSubItem15.IpAddress = null;
            myListBoxSubItem15.IsTwinkle = false;
            myListBoxSubItem15.NicName = "1276";
            myListBoxSubItem15.PersonalMsg = "离线";
            myListBoxSubItem15.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem15.Types = "2";
            myListBoxItem5.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem14,
            myListBoxSubItem15});
            myListBoxItem5.Text = "沙沟医院";
            myListBoxItem6.GroupID = "";
            myListBoxSubItem16.DisplayName = "全科门诊";
            myListBoxSubItem16.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem16.HeadImage")));
            myListBoxSubItem16.ID = 1277;
            myListBoxSubItem16.IpAddress = null;
            myListBoxSubItem16.IsTwinkle = false;
            myListBoxSubItem16.NicName = "1277";
            myListBoxSubItem16.PersonalMsg = "离线";
            myListBoxSubItem16.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem16.Types = "2";
            myListBoxSubItem17.DisplayName = "影像科";
            myListBoxSubItem17.HeadImage = null;
            myListBoxSubItem17.ID = 1280;
            myListBoxSubItem17.IpAddress = null;
            myListBoxSubItem17.IsTwinkle = false;
            myListBoxSubItem17.NicName = "1280";
            myListBoxSubItem17.PersonalMsg = "离线";
            myListBoxSubItem17.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem17.Types = "2";
            myListBoxSubItem18.DisplayName = "卫生室";
            myListBoxSubItem18.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem18.HeadImage")));
            myListBoxSubItem18.ID = 1278;
            myListBoxSubItem18.IpAddress = null;
            myListBoxSubItem18.IsTwinkle = false;
            myListBoxSubItem18.NicName = "1278";
            myListBoxSubItem18.PersonalMsg = "离线";
            myListBoxSubItem18.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem18.Types = "3";
            myListBoxItem6.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem16,
            myListBoxSubItem17,
            myListBoxSubItem18});
            myListBoxItem6.Text = "院东头医院";
            myListBoxItem7.GroupID = "";
            myListBoxSubItem19.DisplayName = "全科门诊";
            myListBoxSubItem19.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem19.HeadImage")));
            myListBoxSubItem19.ID = 10002;
            myListBoxSubItem19.IpAddress = null;
            myListBoxSubItem19.IsTwinkle = false;
            myListBoxSubItem19.NicName = "10002";
            myListBoxSubItem19.PersonalMsg = "离线";
            myListBoxSubItem19.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem19.Types = "2";
            myListBoxSubItem20.DisplayName = "影像科";
            myListBoxSubItem20.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem20.HeadImage")));
            myListBoxSubItem20.ID = 10003;
            myListBoxSubItem20.IpAddress = null;
            myListBoxSubItem20.IsTwinkle = false;
            myListBoxSubItem20.NicName = "10003";
            myListBoxSubItem20.PersonalMsg = "离线";
            myListBoxSubItem20.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem20.Types = "2";
            myListBoxSubItem21.DisplayName = "下峪子卫生室";
            myListBoxSubItem21.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem21.HeadImage")));
            myListBoxSubItem21.ID = 20024;
            myListBoxSubItem21.IpAddress = null;
            myListBoxSubItem21.IsTwinkle = false;
            myListBoxSubItem21.NicName = "20024";
            myListBoxSubItem21.PersonalMsg = "离线";
            myListBoxSubItem21.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem21.Types = "3";
            myListBoxSubItem22.DisplayName = "港卜口卫生室";
            myListBoxSubItem22.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem22.HeadImage")));
            myListBoxSubItem22.ID = 20025;
            myListBoxSubItem22.IpAddress = null;
            myListBoxSubItem22.IsTwinkle = false;
            myListBoxSubItem22.NicName = "20025";
            myListBoxSubItem22.PersonalMsg = "离线";
            myListBoxSubItem22.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem22.Types = "3";
            myListBoxItem7.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem19,
            myListBoxSubItem20,
            myListBoxSubItem21,
            myListBoxSubItem22});
            myListBoxItem7.Text = "龙家圈医院";
            myListBoxItem8.GroupID = "";
            myListBoxSubItem23.DisplayName = "全科门诊";
            myListBoxSubItem23.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem23.HeadImage")));
            myListBoxSubItem23.ID = 10004;
            myListBoxSubItem23.IpAddress = null;
            myListBoxSubItem23.IsTwinkle = false;
            myListBoxSubItem23.NicName = "10004";
            myListBoxSubItem23.PersonalMsg = "离线";
            myListBoxSubItem23.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem23.Types = "2";
            myListBoxSubItem24.DisplayName = "南垛庄卫生室";
            myListBoxSubItem24.HeadImage = ((System.Drawing.Image)(resources.GetObject("myListBoxSubItem24.HeadImage")));
            myListBoxSubItem24.ID = 20048;
            myListBoxSubItem24.IpAddress = null;
            myListBoxSubItem24.IsTwinkle = false;
            myListBoxSubItem24.NicName = "20048";
            myListBoxSubItem24.PersonalMsg = "离线";
            myListBoxSubItem24.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem24.Types = "3";
            myListBoxItem8.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem23,
            myListBoxSubItem24});
            myListBoxItem8.Text = "杨庄医院";
            this.myListBox1.Items.AddRange(new MySkin.Controls.MyListBoxItem[] {
            myListBoxItem1,
            myListBoxItem2,
            myListBoxItem3,
            myListBoxItem4,
            myListBoxItem5,
            myListBoxItem6,
            myListBoxItem7,
            myListBoxItem8});
            this.myListBox1.Location = new System.Drawing.Point(0, 44);
            this.myListBox1.Name = "myListBox1";
            this.myListBox1.ScrollArrowBackColor = System.Drawing.Color.Silver;
            this.myListBox1.ScrollBackColor = System.Drawing.Color.Transparent;
            this.myListBox1.ScrollSliderDefaultColor = System.Drawing.Color.LightGray;
            this.myListBox1.ScrollSliderDownColor = System.Drawing.Color.Gainsboro;
            this.myListBox1.Size = new System.Drawing.Size(294, 244);
            this.myListBox1.SubItemColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.myListBox1.SubItemSelectColor = System.Drawing.Color.DeepSkyBlue;
            this.myListBox1.TabIndex = 12;
            this.myListBox1.Text = "myListBox1";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 676);
            this.Controls.Add(this.myPanelMain);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.dockPanel2);
            this.Controls.Add(this.dockPanel3);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "FrmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel3.ResumeLayout(false);
            this.dockPanel3_Container.ResumeLayout(false);
            this.dockPanel2.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label3;
        private Atom.Client.Controls.video video1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox picHome;
        private System.Windows.Forms.PictureBox picSetting;
        private System.Windows.Forms.PictureBox picMin;
        private System.Windows.Forms.PictureBox picClose;
        private MySkin.Controls.MyPanel myPanelMain;
        private Atom.Controls.MessagePanel messagePanel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private MySkin.Controls.MyListBox myListBox1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel3;
        private DevExpress.XtraEditors.SimpleButton BtnSendBr;
        private DevExpress.XtraEditors.SimpleButton tsButAV;
        private DevExpress.XtraEditors.SimpleButton BtnSendMsg;

    }
}