﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Atom.Client.Properties;
using System.Configuration;

namespace Atom.Client
{
    public partial class Login : DevExpress.XtraEditors.XtraForm
    {
        public Login()
        {
            InitializeComponent();
            this.Text +=_Version;
        }

        static readonly string iniSys = "Logined.ini";//ini文件
        static readonly string Path = Application.StartupPath;
        private void myButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtuser.Text))
            {
                MessageBox.Show("请输入用户名");
                return;
            }
            UserInfo.UserID = this.txtuser.Text;
            UserInfo.UserPwd = this.txtpws.Text;
            FrmMain frm = new FrmMain();
            frm.Show();
            this.Hide();
            ControlsHelper.IniWriteValue("Logined", "LoginName", this.txtuser.Text, System.IO.Path.Combine(Path, iniSys));
            ControlsHelper.IniWriteValue("Logined", "LoginPwd", this.txtpws.Text, System.IO.Path.Combine(Path, iniSys));
        }

        private void Login_Load(object sender, EventArgs e)
        {
            OnCheckVerSion();
            if (!_IsLogined)
            {
                MessageBox.Show("请先升级后登陆！");
                return;
            }
            this.txtuser.Text = ControlsHelper.IniReadValue("Logined", "LoginName", System.IO.Path.Combine(Path, iniSys));//Global.LoginName;
            this.txtpws.Text = ControlsHelper.IniReadValue("Logined", "LoginPwd", System.IO.Path.Combine(Path, iniSys)); //Global.LoginPwd;
        }


        #region 版本验证
        public static string GetRemoteConnStr()
        {
            return "Server=192.168.10.121;Database=Atom.Remote;User ID=yggsuser;password=yggsuser";
        }
        private bool _IsLogined = true; //是否允许不升级登陆
        private string _Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        void OnCheckVerSion()
        {
            //应该先ping一下服务器，是否通！如果不通就暂时不验证版本
            string SQL = "select * from Version";
            IMLibrary4.SqlData.DataAccess.ConnectionString = GetRemoteConnStr();
            DataTable data = IMLibrary4.SqlData.DataAccess.GetDataSetBySql(SQL).Tables[0];
            if (data == null || data.Rows.Count <= 0) return;
            Int32 olvs = Convert.ToInt32(_Version.Replace(".", ""));
            Int32 newvs = Convert.ToInt32(data.Rows[0]["限定版本"].ToString().Replace(".", ""));
            if (newvs > olvs)
            {
                if (Convert.ToBoolean(data.Rows[0]["强制升级"]))
                {
                    Update_Click();
                    _IsLogined = false;
                }
                if (MessageBox.Show("发现新版本，是否升级？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Update_Click();
                }
            }
        }

        /// <summary>
        /// 升级功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Update_Click()
        {
            if (System.IO.File.Exists("uplistdo.exe") == false)
            {
                MessageBox.Show("升级工具丢失，请联系软件维护人员处理故障!");
                return;
            }
            System.Diagnostics.Process.Start("uplistdo.exe");
        } 
        #endregion

        Font f = new Font("微软雅黑", 14);
        SolidBrush sb = new SolidBrush(Color.FromArgb(4, 165, 223));
        SolidBrush white = new SolidBrush(Color.White);
        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
            //设置高质量插值法
            e.Graphics.InterpolationMode = InterpolationMode.Bilinear;
            //设置高质量,低速度呈现平滑程度
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            //e.Graphics.FillRectangle(sb, 0, 0, Width, 40);//标题栏底色
            //e.Graphics.DrawImage(Resources.bkBar, 0, -50); //绘制标题栏底图
            e.Graphics.DrawString("沂水县远程门诊服务平台" + _Version, f, white, 30, 10);
        }

    }
}
