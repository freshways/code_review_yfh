﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Atom.Client.Properties;
using IMLibrary4;
using IMLibrary4.Net;
using IMLibrary4.Net.TCP;
using IMLibrary4.Protocol;
using IMLibrary4.Organization;
using IMLibrary4.SqlData;

namespace Atom.Client
{
    public partial class FrmMain : DevExpress.XtraEditors.XtraForm
    {

        #region 变量
        /// <summary>
        /// TCP客户端
        /// </summary>
        private TCPClient tcpClient = null;

        /// <summary>
        /// 登录认证
        /// </summary>
        private Auth MyAuth = null;

        /// <summary>
        /// 组织机构分组
        /// </summary>
        private List<exGroup> Groups = new List<exGroup>();

        /// <summary>
        /// 用户
        /// </summary>
        private List<exUser> Users = new List<exUser>();

        /// <summary>
        /// 群
        /// </summary>
        private List<exRoom> Rooms = new List<exRoom>();

        /// <summary>
        /// 登录超时记数器
        /// </summary>
        private int LoginTimeCount = 0;// 登录超时记数器
        
        #endregion

        public FrmMain()
        {
            InitializeComponent();
            //以下是注册视频通讯事件
            tsButAV.Click += new EventHandler(tsButAV_Click);
            video1.Cancel += new Atom.Client.Controls.video.CancelEventHandler(video1_Cancel);
            video1.GetIPEndPoint += new Atom.Client.Controls.video.GetIPEndPointEventHandler(video1_GetIPEndPoint);
            messagePanel1.CreateMsgAfter += messagePanel1_CreateMsgAfter;
        }

        void messagePanel1_CreateMsgAfter(object sender, IMLibrary4.Protocol.Message Msg)
        {
            BtnSendMsg.PerformClick();
        }

        #region tcpService-通讯服务        

        public IMLibrary4.Protocol.Auth auth = new IMLibrary4.Protocol.Auth();
        private IMLibrary4.Organization.User User = new User();

        #region 视频单击事件
        void tsButAV_Click(object sender, EventArgs e)
        {
            if (myListBox1.SelectSubItem != null)
            {
                User.UserID = myListBox1.SelectSubItem.ID.ToString();
                if (User.UserID == UserInfo.UserID)
                { MessageBox.Show("不能选择自己！"); return; }
            }
            else
            { MessageBox.Show("请选择医生！"); return; }

            ReadyAV(true);//邀请对方并准备视频
            
            //发送邀请
            IMLibrary4.Protocol.AVMsg msg = new IMLibrary4.Protocol.AVMsg();
            msg.type = IMLibrary4.Protocol.type.New;

            //if (SendMsgToUser != null)//触发视频消息事件
            SendMsgToUser(msg, User);
        }

        /// <summary>
        /// 准备视频
        /// </summary>
        /// <param name="IsInvite">是否邀请方</param>
        public void ReadyAV(bool IsInvite)
        {
            tsButAV.Enabled = false;
            video1.Visible = true;
            video1.IsInvite = IsInvite;
            //this.dockPanel4.Size = new Size(dockPanel4.Size.Width,300);
        }

        /// <summary>
        /// 取消视频
        /// </summary>
        /// <param name="IsSelf">是否自己取消</param>
        public void CancelAV(bool IsSelf)
        {
            if (!IsSelf)
                this.video1.CancelAV();//释放视频资源
            video1.Visible = false;
            video1.Refresh();
            tsButAV.Enabled = true;
            //this.dockPanel4.Size = new Size(dockPanel4.Size.Width, 29);
        }
        #endregion
        void video1_GetIPEndPoint(object sender, RtpEventArgs e)
        {
            IMLibrary4.Protocol.AVMsg msg = new IMLibrary4.Protocol.AVMsg();
            msg.type = IMLibrary4.Protocol.type.set;
            msg.LocalIP = e.LocalIP;
            msg.RemoteIP = e.RemoteIP;
            msg.LocalRtpPort = e.LocalRtpPort;
            msg.LocalRtcpPort = e.LocalRtcpPort;
            msg.RemoteRtpPort = e.RemoteRtpPort;
            msg.RemoteRtcpPort = e.RemoteRtcpPort;
            //if (SendMsgToUser != null)//触发消息发送事件
            SendMsgToUser(msg, User);
        }
        void video1_Cancel(object sender)
        {
            CancelAV(true);//自己取消
            
            //发送取消
            IMLibrary4.Protocol.AVMsg msg = new IMLibrary4.Protocol.AVMsg();
            msg.type = IMLibrary4.Protocol.type.cancel;

            //if (SendMsgToUser != null)//触发视频消息事件
            SendMsgToUser(msg, User);
        }

        #region TCP数据达到操作
        private void tcpClient_PacketReceived(object sender, TcpSessionEventArgs e)
        {
            PacketReceivedDelegate outdelegate = new PacketReceivedDelegate(PacketReceived);
            this.BeginInvoke(outdelegate, e.Data);
        }

        private delegate void PacketReceivedDelegate(string data);

        private void PacketReceived(string data)
        {
            Console.WriteLine(data);
            List<object> objs = Factory.CreateInstanceObjects(data);
            if (objs != null)
                foreach (object obj in objs)
                {
                    Element e = obj as Element;

                    if (obj is Auth)//用户登录
                        onLogin(obj as Auth);
                    if (obj is IMLibrary4.Protocol.Message)//聊天消息
                        onMessage(obj as IMLibrary4.Protocol.Message);
                    if (obj is Presence)//用户在线状态发生更改
                        onPresence(obj as Presence);
                    if (obj is Group)//获得分组信息
                        onOrgGroups(obj as Group);
                    if (obj is User)//获得用户信息
                        onOrgUsers(obj as User);

                    if (obj is AVMsg)//视频对话消息 
                        onAVMsg(obj as AVMsg);
                }
        }
        #endregion

        #region 登录回应处理
        private void onLogin(Auth auth)
        {
            if (auth.type == type.error)//密码错误 
            { 
                //LoginPasswordError(); 
                MessageBox.Show("密码错误");
            }
            else if (auth.type == type.result)//登录成功
            {
                string password = MyAuth.Password;

                if (MyAuth.IsSavePassword)//如果是成功登录并且需要保存登录密码 
                {
                    //IMLibrary4.OpeRecordDB.SaveAuth(MyAuth);//保存成功登录用户的信息
                }
                else if (!MyAuth.IsSavePassword)//如果不需要保存登录密码
                {
                    MyAuth.Password = "";
                    //IMLibrary4.OpeRecordDB.SaveAuth(MyAuth);//保存成功登录用户的信息
                }

                MyAuth = auth;//暂存登录成功后的服务器返回的登录信息

                MyAuth.Password = password;

                ///设置UDP文件传输服务器端口
                Global.FileTransmitServerEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), MyAuth.FileServerUDPPort);
                ///设置UDP音视频传输服务器端口
                Global.AVTransmitServerEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), MyAuth.AVServerUDPPort);
                ///设置TCP图片文件传输服务器端口
                Global.ImageServerEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), MyAuth.FileServerTCPPort);
                //设置TCP离线文件传输服务器端口
                Global.OfflineFileServerEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), MyAuth.OfflineFileServerTCPPort);

                OrgVersion orgV = new OrgVersion();//获取服务器组织机构版本信息
                orgV.UsersCount = auth.UsersCount;
                orgV.UsersVersion = auth.UsersVersion;
                orgV.GroupsCount = auth.GroupsCount;
                orgV.GroupsVersion = auth.GroupsVersion;
                orgV.RoomsCount = auth.RoomsCount;
                
                //onOrgVersion(orgV);

                ////登陆成功提示调整到最后，2014-12-29 yufh调整
                //this.LoginTimeCount = 0;//超时器清零
                //this.timerLogin1.Enabled = false;//停止登录超时检测
                //if (UserLoginSuccessful != null) //触发登录成功事件
                //    UserLoginSuccessful(this, auth);

                //DownloadUsers dUsers = new DownloadUsers();
                //dUsers.from = MyAuth.UserID;
                //dUsers.type = type.get;
                //SendMessageToServer(dUsers);//请求下载联系人信息
                //请求联系人在线状态信息
                Presence pre = new Presence();
                pre.from = MyAuth.UserID;
                pre.type = type.get;//必须设置 set,以表示是设置，如果为get，则是获取所有联系人的状态
                SendMessageToServer(pre);

                for (int i = 0; i < myListBox1.Items.Count; i++)
                {
                    for (int j = 0; j < myListBox1.Items[i].SubItems.Count; j++)
                    {
                        if (myListBox1.Items[i].SubItems[j].ID.ToString() == MyAuth.UserID)
                        {
                            myListBox1.Items[i].SubItems[j].PersonalMsg = "在线";
                            UserInfo.UserTpye = myListBox1.Items[i].SubItems[j].Types; //把级别暂存，以后根据登陆判断，暂时这样
                            label1.Text = "[" + MyAuth.UserID + "]\n" + myListBox1.Items[i].Text + "|" + myListBox1.Items[i].SubItems[j].DisplayName;
                        }
                    }
                }
            }
            else if (auth.type == type.Else)//别处登录
            {
                //ElseLogin();
                pictureBox2.Image = Properties.Resources.OnLine;
                MessageBox.Show("您的账号在别处登陆，请尝试重新登陆！","消息提示");
            }
        }
        #endregion

        #region 下载企业组织架构信息
        /// <summary>
        /// 获得群信息
        /// </summary>
        /// <param name="room"></param>
        private void onOrgRooms(Room room)
        {
            exRoom exroom = new exRoom();
            exroom.RoomID = room.RoomID;
            exroom.RoomName = room.RoomName;
            exroom.Notice = room.Notice;
            exroom.UserIDs = room.UserIDs;
            exroom.CreateUserID = room.CreateUserID;
            Rooms.Add(exroom);

            //if (frmOrg != null && !frmOrg.IsDisposed)
            //{
            //    frmOrg.Times = 0;
            //    frmOrg.Value = this.Rooms.Count;
            //}
        }

        /// <summary>
        /// 获得用户信息
        /// </summary>
        /// <param name="user"></param>
        private void onOrgUsers(User user)
        {
            exUser exuser = new exUser();
            exuser.UserID = user.UserID;
            exuser.UserName = user.UserName;
            exuser.GroupID = user.GroupID;
            exuser.OrderID = user.OrderID;
            exuser.ShowType = ShowType.Offline;
            Users.Add(exuser);

            //if (frmOrg != null && !frmOrg.IsDisposed)
            //{
            //    frmOrg.Times = 0;
            //    frmOrg.Value = this.Users.Count;
            //}
        }

        /// <summary>
        /// 获得分组信息
        /// </summary>
        /// <param name="group"></param>
        private void onOrgGroups(Group group)
        {

            exGroup exgroup = new exGroup();
            exgroup.GroupID = group.GroupID;
            exgroup.GroupName = group.GroupName;
            exgroup.SuperiorID = group.SuperiorID;
            exgroup.OrderID = group.OrderID;
            Groups.Add(exgroup);

            //if (frmOrg != null && !frmOrg.IsDisposed)
            //{
            //    frmOrg.Times = 0;
            //    frmOrg.Value = this.Groups.Count;
            //}
        }

        ///// <summary>
        ///// 获得组织机构版本信息
        ///// </summary>
        ///// <param name="org"></param>
        //private void onOrgVersion(OrgVersion org)
        //{
        //    try
        //    {
        //        #region 判断登录的用户本地数据库文件夹是否存在
        //        System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(MyAuth.UserID);
        //        if (!dInfo.Exists)
        //            dInfo.Create();
        //        string FileNamePath = Application.StartupPath + "\\" + MyAuth.UserID + "\\Record.mdb";
        //        if (!System.IO.File.Exists(FileNamePath))
        //            System.IO.File.Copy(Application.StartupPath + "\\Record.db", FileNamePath);

        //        //初始化本地数据库连接字符串
        //        SQLiteDBHelper.connectionString = "data source=" + FileNamePath;
        //        #endregion

        //        OrgVersion localOrg = OpeRecordDB.GetOrgVersion(MyAuth.UserID);
        //        //this.Rooms = OpeRecordDB.GetRooms();//获得本地数据库群组数


        //        #region 如果版本已经改变下载组织机构组信息
        //        if (org.GroupsVersion != localOrg.GroupsVersion)
        //        {
        //            //Groups.Clear();
        //            //treeView_Organization.Nodes.Clear();

        //            //OpeRecordDB.DeleteAllGroup();//删除本地数据库中分组信息

        //            //frmOrg = new FormDownOrganization();
        //            //frmOrg.Shown += delegate(object sender, EventArgs e)
        //            //{
        //            //    DownloadGroups dGroups = new DownloadGroups();
        //            //    dGroups.from = MyAuth.UserID;
        //            //    dGroups.type = type.get;
        //            //    SendMessageToServer(dGroups);//请求下载分组信息
        //            //};
        //            //frmOrg.MaxValue = org.GroupsCount;
        //            //frmOrg.ShowText = "正在下载分组信息...";
        //            //frmOrg.ShowDialog();

        //            //OpeRecordDB.AddGroups(this.Groups);//将下载的分组信息保存于数据库中
        //        }
        //        #endregion

        //        #region 下载组织机构用户信息
        //        if (org.UsersVersion != localOrg.UsersVersion)//(true)
        //        {
        //            //Users.Clear();
        //            //treeView_Organization.Nodes.Clear();
        //            //OpeRecordDB.DeleteAllUser();//删除本地数据库中用户信息

        //            //frmOrg = new FormDownOrganization();
        //            //frmOrg.Shown += delegate(object sender, EventArgs e)
        //            //{
        //            //    DownloadUsers dUsers = new DownloadUsers();
        //            //    dUsers.from = MyAuth.UserID;
        //            //    dUsers.type = type.get;
        //            //    SendMessageToServer(dUsers);//请求下载联系人信息
        //            //};
        //            //frmOrg.MaxValue = org.UsersCount;
        //            //frmOrg.ShowText = "正在下载用户信息...";
        //            //frmOrg.ShowDialog();

        //            //OpeRecordDB.AddUsers(this.Users);
        //        }
        //        #endregion

        //        #region 下载群信息
        //        //if (org.RoomsCount != Rooms.Count)//如果群数大于0
        //        //{
        //        //    Rooms.Clear();
        //        //    treeView_Rooms.Nodes.Clear();

        //        //    OpeRecordDB.DeleteAllRoom();//删除本地数据库中群信息

        //        //    frmOrg = new FormDownOrganization();
        //        //    frmOrg.Shown += delegate(object sender, EventArgs e)
        //        //    {
        //        //        DownloadRooms dRooms = new DownloadRooms();
        //        //        dRooms.from = MyAuth.UserID;
        //        //        dRooms.type = type.get;
        //        //        SendMessageToServer(dRooms);//请求下载群信息
        //        //    };

        //        //    frmOrg.MaxValue = org.RoomsCount;
        //        //    frmOrg.ShowText = "正在下载群资料...";
        //        //    frmOrg.ShowDialog();

        //        //    OpeRecordDB.AddRooms(this.Rooms);
        //        //}
        //        #endregion

        //        ////如果组织机构已经更改或组织机构未加载到树图
        //        //if (this.treeView_Organization.Nodes.Count == 0)
        //        //{
        //        //    //则更新到本地数据库
        //        //    OpeRecordDB.UpdateOrgVersion(org);
        //        //    this.Groups = OpeRecordDB.GetGroups();
        //        //    this.Users = OpeRecordDB.GetUsers();
        //        //    treeViewAddOrg();//加载树
        //        //}

        //        //if (this.treeView_Rooms.Nodes.Count == 0)
        //        //{
        //        //    foreach (Room room in this.Rooms)
        //        //    {
        //        //        foreach (string userID in room.UserIDs.Split(';'))
        //        //        {
        //        //            User user = findUser(userID);
        //        //            if (user != null)
        //        //                room.Users.Add(user.UserID, user);
        //        //        }
        //        //    }

        //        //    //treeViewAddRooms(this.Rooms);
        //        //}

        //        ////请求联系人在线状态信息
        //        //Presence pre = new Presence();
        //        //pre.from = MyAuth.UserID;
        //        //pre.type = type.get;//必须设置 set,以表示是设置，如果为get，则是获取所有联系人的状态
        //        //SendMessageToServer(pre);
        //    }
        //    catch (Exception ex)
        //    {
        //        //IMLibrary4.Global.MsgShow(ex.Source + ex.Message);
        //    }
        //}

        #endregion

        #region 处理用户发送的对话消息
        /// <summary>
        /// 处理用户发送的对话消息 
        /// </summary>
        /// <param name="msg"></param>
        private void onMessage(IMLibrary4.Protocol.Message msg)
        {
            //exUser user = findUser(msg.from);
            //if (user == null) return;
            if (User.UserID == MyAuth.UserID) return;//如果是自己发送给自己的消息，则不处理

            //OpeRecordDB.AddMsg(msg);//将消息添加到数据库

            if (msg.MessageType == MessageType.User)//如果消息发送给用户
            {
                MsgToRichTextBox(msg, false);
                ////OnUserMessage(msg, user);
                //int iniPos = txtRecord.TextLength;//获得当前记录richBox中最后的位置
                //string title = msg.from+msg.DateTime;// System.DateTime.Now.ToString();
                //Color titleColor = Color.Green;

                ////if (!IsSend)//如果是收到消息
                ////    titleColor = Color.Blue;


                //txtRecord.AppendText(title);
                //txtRecord.Select(iniPos, txtRecord.TextLength - iniPos);
                //txtRecord.SelectionFont = new Font("宋体", 10);
                //txtRecord.SelectionColor = titleColor;

                //txtRecord.AppendText("\n  ");
                //txtRecord.AppendText(msg.Content + "\n");
            }
            else if (msg.MessageType == MessageType.Group)//如果消息发送给群
            {
                //OnGroupTalk(msg, user);
            }
            else if (msg.MessageType == MessageType.Notice)//如果发送通知消息给多个用户
            {
                OnNoticeMsg(msg);
            }
        }

        #region 将消息加入 RichTextBox 控件
        /// <summary>
        /// 将消息加入 RichTextBox 控件
        /// </summary>
        /// <param name="msg">消息类</param>
        /// <param name="IsSend">标记是发送消息还是收到消息</param>
        public void MsgToRichTextBox(IMLibrary4.Protocol.Message msg, bool IsSend)//将发送的消息加入历史rich
        {
            string title = "(" + label1.Text + ")";//myUserName + "(" + myUserID + ")";
            if (!IsSend)
                title = "(" + msg.from + ")";//User.UserName + "(" + User.UserID + ")";

            this.messagePanel1.MsgToRichTextBox(msg, IsSend, title);
        }
        #endregion 
        #endregion

        #region 收到通知消息
        /// <summary>
        /// 收到通知消息
        /// </summary>
        private void OnNoticeMsg(IMLibrary4.Protocol.Message msg)
        {
            if (msg.Title == "病人信息通知")
            {
                S病人信息 = msg.Content;
                //txt身份证号.Text = msg.Content;
                string [] ss= S病人信息.Split('&');

                PubPeopleInfo.ZYID = Convert.ToInt32(ss[0]);
                PubPeopleInfo.ZYH = ss[1];
                PubPeopleInfo.Name = ss[2];
                PubPeopleInfo.Sex = ss[3];
                PubPeopleInfo.CardNo = ss[4];
                UserControl_Home.ItSelf.label10.Text = "1";
                new System.Media.SoundPlayer(Application.StartupPath + @"\sound\Beep.wav").Play();
            }
            else if (msg.Title == "wss病人信息通知")
            {
                S身份证号 = msg.Content;
                this.toolStripDropDownButton1.Text = "1";
                new System.Media.SoundPlayer(Application.StartupPath + @"\sound\Beep.wav").Play();
            }
        }
        #endregion

        #region 联系人请求视频
        /// <summary>
        /// 联系人请求视频
        /// </summary>
        /// <param name="msg">视频消息对像</param>
        private void onAVMsg(AVMsg msg)
        {
            var Play = new System.Media.SoundPlayer(Application.StartupPath + @"\sound\Audio.wav");
            //exUser user = findUser(msg.from);
            User.UserID = msg.from;
            if (msg.type == type.New)//如果是邀请视频
            {
                Play.PlayLooping();
                ReadyAV(false);
            }
            else if (msg.type == type.cancel)//如果是取消视频
            {
                CancelAV(false);
            }
            else if (msg.type == type.set)//设置通信参数
            {
                Play.Stop();
                setAVRometEP(msg);
            }
        }
        public void setAVRometEP(IMLibrary4.Protocol.AVMsg msg)
        {
            video1.SetRometEP(msg);
        }
        #endregion

        #region 联系人更改状态

        /// <summary>
        /// 联系人更改状态
        /// </summary>
        /// <param name="pres"></param>
        public void onPresence(IMLibrary4.Protocol.Presence pres)
        {
            //查找联系人列表中是否存在此联系人，如果存在，则更新状态信息
            for (int i = 0; i < myListBox1.Items.Count; i++)
            {
                for (int j = 0; j < myListBox1.Items[i].SubItems.Count; j++)
                {
                    if (myListBox1.Items[i].SubItems[j].ID.ToString() == pres.from)
                    {
                        if (pres.ShowType == ShowType.NONE)
                            myListBox1.Items[i].SubItems[j].PersonalMsg = "在线";
                        else if (pres.ShowType == ShowType.Offline)
                            myListBox1.Items[i].SubItems[j].PersonalMsg = "离线";
                        break;
                    }
                }
            }

            //exUser user = findUser(pres.from);
            //if (user != null)
            //{
            //    user.Status = pres.Status;
            //    user.ShowType = pres.ShowType;

            //    if (user.UserID == MyAuth.UserID)
            //    {
            //        MyAuth.ShowType = user.ShowType;
            //        MyAuth.UserName = user.UserName;
            //    }
            //}
        }
        #endregion

        public void SendMsgToUser(IMLibrary4.Protocol.Element e, IMLibrary4.Organization.User User)
        {
            e.from = MyAuth.UserID;
            e.to = User.UserID;
            SendMessageToServer(e);
        }

        /// <summary>
        /// 发送消息至服务器 
        /// </summary>
        /// <param name="e"></param>
        public void SendMessageToServer(Element e)
        {
            e.from = MyAuth.UserID;//此处告之服务器，消息由自己发送
            if (tcpClient != null && !tcpClient.IsDisposed && tcpClient.IsConnected)
                tcpClient.Write(IMLibrary4.Protocol.Factory.CreateXMLMsg(e));
        } 
        #endregion

        #region 获取病人信息
        //根据MAC地址获取链接字符串
        public static string GetLISConnStr()
        {
            return "Server=192.168.10.57;Database=LIS2701;User ID=yggsuser;password=yggsuser";
        }

        public static string GetHISConnStr()
        {
            return string.Format("Server=192.168.10.131;Database={0};User ID=yggsuser;password=yggsuser", Global.HISDB);
            //return "Server=192.168.10.131;Database=CHIS2706;User ID=yggsuser;password=yggsuser";
        }

        public static string GetEHRConnStr()
        {
            return "Server=192.168.10.121;Database=AtomEHR.YSDB;User ID=yggsuser;password=yggsuser";
        }
  
                
        #endregion

        #region 查找用户
        /// <summary>
        /// 查找用户
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        public exUser findUser(string userID)
        {
            if (Users == null) return null;
            foreach (exUser user in Users)
                if (user.UserID == userID)
                    return user;
            return null;
        }
        #endregion

        #region Loading

        //Timer tm = new Timer();

        private void FrmMain_Load(object sender, EventArgs e)
        {
            this.label1.Text = UserInfo.UserID;
            this.label2.Text = DateTime.Now.ToString("yyyy-MM-dd") + " " + DateTime.Now.ToString("dddd");
            this.video1.Visible = false;
            //this.video1.IsInvite = true;
            auth.UserID = UserInfo.UserID;//"20038"; 
            auth.Password = UserInfo.UserPwd;//"123456";
            auth.ShowType = ShowType.NONE;
            //auth.Resource

            MyAuth = auth;
            
            //tcpClient.PacketReceived += new TCP_Client.PacketReceivedEventHandler(tcpClient_PacketReceived);
            if (tcpClient == null || tcpClient.IsDisposed)
            {
                tcpClient = new TCPClient();
                tcpClient.PacketReceived += new TCP_Client.PacketReceivedEventHandler(tcpClient_PacketReceived);
                //tcpClient.Disonnected += new EventHandler(tcpClient_Disonnected);
            }

            if (!tcpClient.IsConnected)
                tcpClient.Connect(Global.ServerDomain, Global.ServerMsgPort);
            //向服务器发送登陆用户信息
            SendMessageToServer(MyAuth);

            if (myListBox1.SelectSubItem != null)
                User.UserID = myListBox1.SelectSubItem.ID.ToString();//"10004";

            for (int i = 0; i < myListBox1.Items.Count; i++)
            {
                for (int j = 0; j < myListBox1.Items[i].SubItems.Count; j++)
                {
                    if (myListBox1.Items[i].SubItems[j].ID.ToString() == UserInfo.UserID)
                    {
                        UserInfo.UserTpye = myListBox1.Items[i].SubItems[j].Types; //把级别暂存，以后根据登陆判断，暂时这样
                    }
                }
            }
            
            if (UserInfo.UserID == "20038" || UserInfo.UserTpye=="1")
            {
                myPanelMain.Controls.Clear();
                myPanelMain.Controls.Add(UserControl_Home.ItSelf);
                UserControl_Home.ItSelf.Dock = DockStyle.Fill;
                UserControl_Home.ItSelf.OpenBRXX = OpenBrInfo;
            }
            else if (UserInfo.UserID == "10004" || UserInfo.UserTpye == "2") 
            {
                BtnSendBr.Visible = true;
                myPanelMain.Controls.Clear();
                myPanelMain.Controls.Add(UserControl病人列表.ItSelf);
                UserControl病人列表.ItSelf.Dock = DockStyle.Fill;
                UserControl病人列表.ItSelf.SendPPInfo = ChangePeopleInfo;
                UserControl病人列表.ItSelf.OpenBRXX = OpenBrInfo;
            }
            else
            {
                BtnSendBr.Visible = true;
                myPanelMain.Controls.Clear();
                myPanelMain.Controls.Add(UserControl卫生室病人.ItSelf);
                UserControl卫生室病人.ItSelf.Dock = DockStyle.Fill;
                UserControl卫生室病人.ItSelf.SendPPInfo = ChangePeopleInfowss;
            }
        }



        Font f = new Font("微软雅黑", 14);
        SolidBrush sb = new SolidBrush(Color.FromArgb(4, 165, 223));
        SolidBrush white = new SolidBrush(Color.White);
        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
            //设置高质量插值法
            e.Graphics.InterpolationMode = InterpolationMode.Bilinear;
            //设置高质量,低速度呈现平滑程度
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            //e.Graphics.FillRectangle(sb, 0, 0, Width, 40);//标题栏底色
            e.Graphics.DrawImage(Resources.bkBar, 0, -40); //绘制标题栏底图
            //e.Graphics.DrawString("沂水县远程会诊平台", f, white, 30, 10);

            e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(223, 227, 255)), 0, 64, 305, Height - 60); //左侧阴影
            //e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(223, 227, 255)),Width-305, 64,305, Height - 60);//右侧阴影
        }

        #endregion

        #region BarClick
        private void picClose_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            if (video1.Visible)
            {
                MessageBox.Show("请先关闭通话，在进行退出程序！", "消息");
                return;
            }
            System.Environment.Exit(0);
        }

        //最小化
        private void picMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void picSetting_Click(object sender, EventArgs e)
        {
            //设置
        }

        private void picHome_Click(object sender, EventArgs e)
        {
            //主页
            if (UserInfo.UserID == "20038")
            {
                myPanelMain.Controls.Clear();
                myPanelMain.Controls.Add(UserControl_Home.ItSelf);
                UserControl_Home.ItSelf.Dock = DockStyle.Fill;
            }
            else
            {
                BtnSendBr.Visible = true;
                myPanelMain.Controls.Clear();
                myPanelMain.Controls.Add(UserControl病人列表.ItSelf);
                UserControl病人列表.ItSelf.Dock = DockStyle.Fill;
            }
            //UserControl_Home.ItSelf.label10.Text = "1";
        } 
        #endregion

        /// <summary>
        /// 发送消息
        /// </summary>
        private void BtnSendMsg_Click(object sender, EventArgs e)
        {
            if (myListBox1.SelectSubItem != null)
            {
                if (myListBox1.SelectSubItem.ID.ToString() != UserInfo.UserID)
                    User.UserID = myListBox1.SelectSubItem.ID.ToString();//"10004";
                else
                { MessageBox.Show("不能选择自己！"); return; }
            }
            else
            { MessageBox.Show("请选择医生！"); return; }

            if (messagePanel1.txtSend.Text.Length <= 0 || messagePanel1.txtSend.Text == "")
            {
                MessageBox.Show("请勿发送空白消息"); return;
            }
            IMLibrary4.Protocol.Message msg = new IMLibrary4.Protocol.Message();
            msg.from = MyAuth.UserID;
            msg.Content = messagePanel1.txtSend.Text;//this.richTextBox2.Text.TrimEnd();//获得消息文本内容
            msg.Font = messagePanel1.txtSend.Font;//this.richTextBox2.Font;
            msg.Color = messagePanel1.txtSend.ForeColor;//this.richTextBox2.ForeColor;
            msg.ImageInfo = "";
            //msg.Title = "登陆通知";
            //msg.remark = "提醒";
            //msg.Content = "您收到一条通知！来自:" + msg.from;
            msg.MessageType = IMLibrary4.Protocol.MessageType.User;
            msg.to = User.UserID;
            msg.type = type.result;
            try
            {
                SendMessageToServer(msg);
                MsgToRichTextBox(msg, true);
                messagePanel1.txtSend.Text = "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        string S病人信息 = "";
        string S身份证号 = "";
        /// <summary>
        /// 发送病人信息
        /// </summary>
        private void BtnSendBr_Click(object sender, EventArgs e)
        {
            if (myListBox1.SelectSubItem != null)
            {
                if (myListBox1.SelectSubItem.ID.ToString() != UserInfo.UserID)
                    User.UserID = myListBox1.SelectSubItem.ID.ToString();//"10004";
                else
                { MessageBox.Show("不能选择自己！"); return; }
            }
            else
            { MessageBox.Show("请选择医生！"); return; }

            if (string.IsNullOrEmpty(S病人信息) && string.IsNullOrEmpty(S身份证号)) return;

            IMLibrary4.Protocol.Message msg = new IMLibrary4.Protocol.Message();
            msg.from = MyAuth.UserID;
            msg.ImageInfo = "";
            msg.remark = "提醒";
            if (!string.IsNullOrEmpty(S病人信息))
            {
                msg.Title = "病人信息通知";
                msg.Content = S病人信息;
            }
            else if (!string.IsNullOrEmpty(S身份证号))
            {
                msg.Title = "wss病人信息通知";
                msg.Content = S身份证号;
            }
            msg.MessageType = IMLibrary4.Protocol.MessageType.Notice;
            msg.to = User.UserID;
            msg.type = type.result;
            try
            {
                SendMessageToServer(msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        PeopleInfo PubPeopleInfo = new PeopleInfo();
        void ChangePeopleInfo(PeopleInfo ppInfo)
        {
            PubPeopleInfo = ppInfo;
            string s = "";
            s += ppInfo.ZYID.ToString()+"&";
            s += ppInfo.ZYH.ToString() + "&";
            s += ppInfo.Name.ToString() + "&";
            s += ppInfo.Sex.ToString() + "&";
            s += ppInfo.CardNo.ToString() + "&";
            S病人信息 = s;
        }

        void OpenBrInfo()
        {
            myPanelMain.Controls.Clear();
            myPanelMain.Controls.Add(UserControl病人信息.ItSelf);
            UserControl病人信息.ItSelf.Dock = DockStyle.Fill;
            UserControl病人信息.ItSelf.Info = PubPeopleInfo;
        }

        void ChangePeopleInfowss(string ppInfo)
        {
            S身份证号 = ppInfo;
        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {
            if (toolStripDropDownButton1.Text == "0") return;
            myPanelMain.Controls.Clear();
            UserControl卫生室病人 ucwss = new UserControl卫生室病人(S身份证号);
            myPanelMain.Controls.Add(ucwss);
            ucwss.Dock = DockStyle.Fill;

        }
        
    }
}
