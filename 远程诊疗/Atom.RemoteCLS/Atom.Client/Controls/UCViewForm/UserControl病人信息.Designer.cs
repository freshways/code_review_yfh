﻿namespace Atom.Client
{
    partial class UserControl病人信息
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.myTabControl1 = new MySkin.Controls.MyTabControl(this.components);
            this.tabPage病人基本信息 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt姓名 = new Atom.Client.Controls.CDisplayBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt生日 = new Atom.Client.Controls.CDisplayBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txt身份证号 = new Atom.Client.Controls.CDisplayBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txt地址 = new Atom.Client.Controls.CDisplayBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt住院号 = new Atom.Client.Controls.CDisplayBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt性别 = new Atom.Client.Controls.CDisplayBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage健康档案 = new System.Windows.Forms.TabPage();
            this.tabPage就诊记录 = new System.Windows.Forms.TabPage();
            this.dgv就诊信息 = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage化验信息 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridViewMain = new System.Windows.Forms.DataGridView();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage影像信息 = new System.Windows.Forms.TabPage();
            this.tabPage体温单 = new System.Windows.Forms.TabPage();
            this.tabPage电子病历 = new System.Windows.Forms.TabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tabPage转诊 = new System.Windows.Forms.TabPage();
            this.myTabControl1.SuspendLayout();
            this.tabPage病人基本信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage就诊记录.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv就诊信息)).BeginInit();
            this.tabPage化验信息.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
            this.tabPage电子病历.SuspendLayout();
            this.SuspendLayout();
            // 
            // myTabControl1
            // 
            this.myTabControl1.Controls.Add(this.tabPage病人基本信息);
            this.myTabControl1.Controls.Add(this.tabPage健康档案);
            this.myTabControl1.Controls.Add(this.tabPage就诊记录);
            this.myTabControl1.Controls.Add(this.tabPage化验信息);
            this.myTabControl1.Controls.Add(this.tabPage影像信息);
            this.myTabControl1.Controls.Add(this.tabPage体温单);
            this.myTabControl1.Controls.Add(this.tabPage电子病历);
            this.myTabControl1.Controls.Add(this.tabPage转诊);
            this.myTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myTabControl1.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.myTabControl1.ItemSize = new System.Drawing.Size(100, 40);
            this.myTabControl1.Location = new System.Drawing.Point(0, 0);
            this.myTabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.myTabControl1.Name = "myTabControl1";
            this.myTabControl1.Padding = new System.Drawing.Point(0, 0);
            this.myTabControl1.SelectedIndex = 0;
            this.myTabControl1.Size = new System.Drawing.Size(840, 605);
            this.myTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.myTabControl1.TabIndex = 2;
            this.myTabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.myTabControl1_Selected);
            // 
            // tabPage病人基本信息
            // 
            this.tabPage病人基本信息.Controls.Add(this.pictureBox1);
            this.tabPage病人基本信息.Controls.Add(this.label15);
            this.tabPage病人基本信息.Controls.Add(this.txt姓名);
            this.tabPage病人基本信息.Controls.Add(this.label9);
            this.tabPage病人基本信息.Controls.Add(this.label8);
            this.tabPage病人基本信息.Controls.Add(this.label6);
            this.tabPage病人基本信息.Controls.Add(this.txt生日);
            this.tabPage病人基本信息.Controls.Add(this.label13);
            this.tabPage病人基本信息.Controls.Add(this.label12);
            this.tabPage病人基本信息.Controls.Add(this.label18);
            this.tabPage病人基本信息.Controls.Add(this.txt身份证号);
            this.tabPage病人基本信息.Controls.Add(this.label4);
            this.tabPage病人基本信息.Controls.Add(this.label5);
            this.tabPage病人基本信息.Controls.Add(this.label16);
            this.tabPage病人基本信息.Controls.Add(this.txt地址);
            this.tabPage病人基本信息.Controls.Add(this.label11);
            this.tabPage病人基本信息.Controls.Add(this.label14);
            this.tabPage病人基本信息.Controls.Add(this.label10);
            this.tabPage病人基本信息.Controls.Add(this.txt住院号);
            this.tabPage病人基本信息.Controls.Add(this.label17);
            this.tabPage病人基本信息.Controls.Add(this.label7);
            this.tabPage病人基本信息.Controls.Add(this.label1);
            this.tabPage病人基本信息.Controls.Add(this.txt性别);
            this.tabPage病人基本信息.Controls.Add(this.label19);
            this.tabPage病人基本信息.Location = new System.Drawing.Point(4, 44);
            this.tabPage病人基本信息.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage病人基本信息.Name = "tabPage病人基本信息";
            this.tabPage病人基本信息.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage病人基本信息.Size = new System.Drawing.Size(832, 557);
            this.tabPage病人基本信息.TabIndex = 0;
            this.tabPage病人基本信息.Text = "病人基本信息";
            this.tabPage病人基本信息.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Atom.Client.Properties.Resources.defaultPhoto;
            this.pictureBox1.Location = new System.Drawing.Point(174, 126);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(159, 166);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(582, 416);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 26);
            this.label15.TabIndex = 27;
            this.label15.Text = "条";
            this.label15.Visible = false;
            // 
            // txt姓名
            // 
            this.txt姓名.AllowMultiline = false;
            this.txt姓名.Label = "姓名";
            this.txt姓名.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt姓名.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt姓名.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt姓名.LabelWidth = 100F;
            this.txt姓名.Location = new System.Drawing.Point(337, 126);
            this.txt姓名.Margin = new System.Windows.Forms.Padding(2);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(176, 30);
            this.txt姓名.TabIndex = 25;
            this.txt姓名.TextBackColor = System.Drawing.Color.White;
            this.txt姓名.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt姓名.TextFontPassWordChar = '\0';
            this.txt姓名.TextReadOnly = true;
            this.txt姓名.Value = null;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(2, 2);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(828, 26);
            this.label9.TabIndex = 7;
            this.label9.Text = "基本信息";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(582, 366);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 26);
            this.label8.TabIndex = 27;
            this.label8.Text = "条";
            this.label8.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(433, 366);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 26);
            this.label6.TabIndex = 27;
            this.label6.Text = "就诊记录";
            this.label6.Visible = false;
            // 
            // txt生日
            // 
            this.txt生日.AllowMultiline = false;
            this.txt生日.Label = "出生日期";
            this.txt生日.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt生日.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt生日.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt生日.LabelWidth = 100F;
            this.txt生日.Location = new System.Drawing.Point(337, 161);
            this.txt生日.Margin = new System.Windows.Forms.Padding(2);
            this.txt生日.Name = "txt生日";
            this.txt生日.Size = new System.Drawing.Size(284, 30);
            this.txt生日.TabIndex = 24;
            this.txt生日.TextBackColor = System.Drawing.Color.White;
            this.txt生日.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt生日.TextFontPassWordChar = '\0';
            this.txt生日.TextReadOnly = true;
            this.txt生日.Value = null;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(433, 416);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(116, 26);
            this.label13.TabIndex = 27;
            this.label13.Text = "影像信息";
            this.label13.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(332, 416);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 26);
            this.label12.TabIndex = 27;
            this.label12.Text = "条";
            this.label12.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(433, 466);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(116, 26);
            this.label18.TabIndex = 27;
            this.label18.Text = "电子病历";
            this.label18.Visible = false;
            // 
            // txt身份证号
            // 
            this.txt身份证号.AllowMultiline = false;
            this.txt身份证号.Label = "身份证号";
            this.txt身份证号.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt身份证号.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt身份证号.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt身份证号.LabelWidth = 100F;
            this.txt身份证号.Location = new System.Drawing.Point(337, 195);
            this.txt身份证号.Margin = new System.Windows.Forms.Padding(2);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(284, 30);
            this.txt身份证号.TabIndex = 23;
            this.txt身份证号.TextBackColor = System.Drawing.Color.White;
            this.txt身份证号.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt身份证号.TextFontPassWordChar = '\0';
            this.txt身份证号.TextReadOnly = true;
            this.txt身份证号.Value = null;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(298, 366);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 26);
            this.label4.TabIndex = 27;
            this.label4.Text = "10";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(332, 366);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 26);
            this.label5.TabIndex = 27;
            this.label5.Text = "条";
            this.label5.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(202, 466);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 26);
            this.label16.TabIndex = 27;
            this.label16.Text = "体温单";
            this.label16.Visible = false;
            // 
            // txt地址
            // 
            this.txt地址.AllowMultiline = false;
            this.txt地址.Label = "居住地址";
            this.txt地址.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt地址.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt地址.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt地址.LabelWidth = 100F;
            this.txt地址.Location = new System.Drawing.Point(337, 230);
            this.txt地址.Margin = new System.Windows.Forms.Padding(2);
            this.txt地址.Name = "txt地址";
            this.txt地址.Size = new System.Drawing.Size(284, 30);
            this.txt地址.TabIndex = 22;
            this.txt地址.TextBackColor = System.Drawing.Color.White;
            this.txt地址.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt地址.TextFontPassWordChar = '\0';
            this.txt地址.TextReadOnly = true;
            this.txt地址.Value = null;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(308, 416);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 26);
            this.label11.TabIndex = 27;
            this.label11.Text = "0";
            this.label11.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(553, 416);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 26);
            this.label14.TabIndex = 27;
            this.label14.Text = "0";
            this.label14.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(178, 416);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 26);
            this.label10.TabIndex = 27;
            this.label10.Text = "化验信息";
            this.label10.Visible = false;
            // 
            // txt住院号
            // 
            this.txt住院号.AllowMultiline = false;
            this.txt住院号.Label = "住院号";
            this.txt住院号.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt住院号.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt住院号.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt住院号.LabelWidth = 100F;
            this.txt住院号.Location = new System.Drawing.Point(337, 264);
            this.txt住院号.Margin = new System.Windows.Forms.Padding(2);
            this.txt住院号.Name = "txt住院号";
            this.txt住院号.Size = new System.Drawing.Size(283, 28);
            this.txt住院号.TabIndex = 22;
            this.txt住院号.TextBackColor = System.Drawing.Color.White;
            this.txt住院号.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt住院号.TextFontPassWordChar = '\0';
            this.txt住院号.TextReadOnly = true;
            this.txt住院号.Value = null;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(298, 466);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 26);
            this.label17.TabIndex = 27;
            this.label17.Text = "无";
            this.label17.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(553, 366);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 26);
            this.label7.TabIndex = 27;
            this.label7.Text = "0";
            this.label7.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(178, 366);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 26);
            this.label1.TabIndex = 27;
            this.label1.Text = "医嘱信息";
            this.label1.Visible = false;
            // 
            // txt性别
            // 
            this.txt性别.AllowMultiline = false;
            this.txt性别.Label = "性别";
            this.txt性别.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt性别.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt性别.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt性别.LabelWidth = 55F;
            this.txt性别.Location = new System.Drawing.Point(523, 126);
            this.txt性别.Margin = new System.Windows.Forms.Padding(2);
            this.txt性别.Name = "txt性别";
            this.txt性别.Size = new System.Drawing.Size(98, 30);
            this.txt性别.TabIndex = 21;
            this.txt性别.TextBackColor = System.Drawing.Color.White;
            this.txt性别.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt性别.TextFontPassWordChar = '\0';
            this.txt性别.TextReadOnly = true;
            this.txt性别.Value = null;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(549, 466);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(38, 26);
            this.label19.TabIndex = 27;
            this.label19.Text = "无";
            this.label19.Visible = false;
            // 
            // tabPage健康档案
            // 
            this.tabPage健康档案.Location = new System.Drawing.Point(4, 44);
            this.tabPage健康档案.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage健康档案.Name = "tabPage健康档案";
            this.tabPage健康档案.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage健康档案.Size = new System.Drawing.Size(832, 557);
            this.tabPage健康档案.TabIndex = 1;
            this.tabPage健康档案.Text = "健康档案";
            this.tabPage健康档案.UseVisualStyleBackColor = true;
            // 
            // tabPage就诊记录
            // 
            this.tabPage就诊记录.Controls.Add(this.dgv就诊信息);
            this.tabPage就诊记录.Controls.Add(this.label2);
            this.tabPage就诊记录.Location = new System.Drawing.Point(4, 44);
            this.tabPage就诊记录.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage就诊记录.Name = "tabPage就诊记录";
            this.tabPage就诊记录.Size = new System.Drawing.Size(832, 557);
            this.tabPage就诊记录.TabIndex = 2;
            this.tabPage就诊记录.Text = "就诊记录";
            this.tabPage就诊记录.UseVisualStyleBackColor = true;
            // 
            // dgv就诊信息
            // 
            this.dgv就诊信息.AllowUserToAddRows = false;
            this.dgv就诊信息.AllowUserToDeleteRows = false;
            this.dgv就诊信息.AllowUserToResizeRows = false;
            this.dgv就诊信息.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv就诊信息.BackgroundColor = System.Drawing.Color.White;
            this.dgv就诊信息.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv就诊信息.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv就诊信息.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv就诊信息.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.dgv就诊信息.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv就诊信息.Location = new System.Drawing.Point(0, 26);
            this.dgv就诊信息.Margin = new System.Windows.Forms.Padding(2);
            this.dgv就诊信息.Name = "dgv就诊信息";
            this.dgv就诊信息.ReadOnly = true;
            this.dgv就诊信息.RowHeadersVisible = false;
            this.dgv就诊信息.RowTemplate.Height = 27;
            this.dgv就诊信息.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv就诊信息.Size = new System.Drawing.Size(832, 531);
            this.dgv就诊信息.TabIndex = 1;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "ROWID";
            this.Column6.HeaderText = "序号";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Visible = false;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "医院名称";
            this.Column7.HeaderText = "医院(卫生室)";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "病人姓名";
            this.Column8.HeaderText = "姓名";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "类型";
            this.Column9.HeaderText = "就诊类型";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "疾病名称";
            this.Column10.HeaderText = "疾病名称";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "时间";
            this.Column11.HeaderText = "就诊/入院时间";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "查看详细";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(832, 26);
            this.label2.TabIndex = 6;
            this.label2.Text = "就诊记录";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage化验信息
            // 
            this.tabPage化验信息.Controls.Add(this.dataGridViewDetail);
            this.tabPage化验信息.Controls.Add(this.splitter1);
            this.tabPage化验信息.Controls.Add(this.dataGridViewMain);
            this.tabPage化验信息.Controls.Add(this.label3);
            this.tabPage化验信息.Location = new System.Drawing.Point(4, 44);
            this.tabPage化验信息.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage化验信息.Name = "tabPage化验信息";
            this.tabPage化验信息.Size = new System.Drawing.Size(832, 557);
            this.tabPage化验信息.TabIndex = 3;
            this.tabPage化验信息.Text = "化验信息";
            this.tabPage化验信息.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AllowUserToAddRows = false;
            this.dataGridViewDetail.AllowUserToDeleteRows = false;
            this.dataGridViewDetail.AllowUserToResizeRows = false;
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(5);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewDetail.ColumnHeadersHeight = 35;
            this.dataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column25,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.Location = new System.Drawing.Point(425, 26);
            this.dataGridViewDetail.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.Size = new System.Drawing.Size(407, 531);
            this.dataGridViewDetail.TabIndex = 0;
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "项目名称";
            this.Column25.HeaderText = "项目名称";
            this.Column25.Name = "Column25";
            this.Column25.Width = 98;
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "结果";
            this.Column19.HeaderText = "结果";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Width = 70;
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "标记";
            this.Column20.HeaderText = "标记";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Width = 70;
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "参考值";
            this.Column21.HeaderText = "参考值";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Width = 84;
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "单位";
            this.Column22.HeaderText = "单位";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Width = 70;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(421, 26);
            this.splitter1.Margin = new System.Windows.Forms.Padding(2);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 531);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // dataGridViewMain
            // 
            this.dataGridViewMain.AllowUserToAddRows = false;
            this.dataGridViewMain.AllowUserToDeleteRows = false;
            this.dataGridViewMain.AllowUserToResizeRows = false;
            this.dataGridViewMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewMain.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridViewMain.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column24,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column23});
            this.dataGridViewMain.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridViewMain.Location = new System.Drawing.Point(0, 26);
            this.dataGridViewMain.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewMain.Name = "dataGridViewMain";
            this.dataGridViewMain.ReadOnly = true;
            this.dataGridViewMain.RowHeadersVisible = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewMain.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewMain.RowTemplate.Height = 27;
            this.dataGridViewMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMain.Size = new System.Drawing.Size(421, 531);
            this.dataGridViewMain.TabIndex = 0;
            this.dataGridViewMain.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMain_CellClick);
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "fjyid";
            this.Column24.Frozen = true;
            this.Column24.HeaderText = "检验ID";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            this.Column24.Visible = false;
            this.Column24.Width = 69;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "医院名称";
            this.Column13.Frozen = true;
            this.Column13.HeaderText = "医院名称";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 98;
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "化验日期";
            this.Column14.Frozen = true;
            this.Column14.HeaderText = "化验日期";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Width = 98;
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "门诊住院号";
            this.Column15.HeaderText = "门诊/住院号";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 119;
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "姓名";
            this.Column16.HeaderText = "姓名";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Width = 70;
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "身份证号";
            this.Column17.HeaderText = "身份证号";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Width = 98;
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "性别";
            this.Column18.HeaderText = "性别";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 70;
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "年龄";
            this.Column23.HeaderText = "年龄";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.Visible = false;
            this.Column23.Width = 76;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(832, 26);
            this.label3.TabIndex = 7;
            this.label3.Text = "就诊记录";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage影像信息
            // 
            this.tabPage影像信息.Location = new System.Drawing.Point(4, 44);
            this.tabPage影像信息.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage影像信息.Name = "tabPage影像信息";
            this.tabPage影像信息.Size = new System.Drawing.Size(832, 557);
            this.tabPage影像信息.TabIndex = 4;
            this.tabPage影像信息.Text = "影像信息";
            this.tabPage影像信息.UseVisualStyleBackColor = true;
            // 
            // tabPage体温单
            // 
            this.tabPage体温单.Location = new System.Drawing.Point(4, 44);
            this.tabPage体温单.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage体温单.Name = "tabPage体温单";
            this.tabPage体温单.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage体温单.Size = new System.Drawing.Size(832, 557);
            this.tabPage体温单.TabIndex = 5;
            this.tabPage体温单.Text = "体温单";
            this.tabPage体温单.UseVisualStyleBackColor = true;
            // 
            // tabPage电子病历
            // 
            this.tabPage电子病历.Controls.Add(this.webBrowser1);
            this.tabPage电子病历.Location = new System.Drawing.Point(4, 44);
            this.tabPage电子病历.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage电子病历.Name = "tabPage电子病历";
            this.tabPage电子病历.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage电子病历.Size = new System.Drawing.Size(832, 557);
            this.tabPage电子病历.TabIndex = 6;
            this.tabPage电子病历.Text = "电子病历";
            this.tabPage电子病历.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(2, 2);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(828, 553);
            this.webBrowser1.TabIndex = 1;
            // 
            // tabPage转诊
            // 
            this.tabPage转诊.Location = new System.Drawing.Point(4, 44);
            this.tabPage转诊.Name = "tabPage转诊";
            this.tabPage转诊.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage转诊.Size = new System.Drawing.Size(832, 557);
            this.tabPage转诊.TabIndex = 7;
            this.tabPage转诊.Text = "转诊";
            this.tabPage转诊.UseVisualStyleBackColor = true;
            // 
            // UserControl病人信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.myTabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UserControl病人信息";
            this.Size = new System.Drawing.Size(840, 605);
            this.Load += new System.EventHandler(this.UserControl病人信息_Load);
            this.myTabControl1.ResumeLayout(false);
            this.tabPage病人基本信息.ResumeLayout(false);
            this.tabPage病人基本信息.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage就诊记录.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv就诊信息)).EndInit();
            this.tabPage化验信息.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
            this.tabPage电子病历.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MySkin.Controls.MyTabControl myTabControl1;
        private System.Windows.Forms.TabPage tabPage病人基本信息;
        private System.Windows.Forms.TabPage tabPage健康档案;
        private System.Windows.Forms.TabPage tabPage就诊记录;
        private System.Windows.Forms.DataGridView dgv就诊信息;
        private System.Windows.Forms.TabPage tabPage化验信息;
        private System.Windows.Forms.DataGridView dataGridViewDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView dataGridViewMain;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.TabPage tabPage影像信息;
        private System.Windows.Forms.TabPage tabPage体温单;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage电子病历;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private Controls.CDisplayBox txt性别;
        private Controls.CDisplayBox txt地址;
        private Controls.CDisplayBox txt身份证号;
        private Controls.CDisplayBox txt生日;
        private Controls.CDisplayBox txt姓名;
        private Controls.CDisplayBox txt住院号;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage转诊;
    }
}
