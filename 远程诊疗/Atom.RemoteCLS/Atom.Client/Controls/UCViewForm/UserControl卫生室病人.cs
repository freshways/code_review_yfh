﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client
{
    public partial class UserControl卫生室病人 : UserControl
    {
        public UserControl卫生室病人()
        {
            InitializeComponent();
        }

        public UserControl卫生室病人(string CardNo)
        {
            InitializeComponent();
            _s身份证 = CardNo;            
        }

        string _s身份证 = "";
        private void UserControl卫生室病人_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_s身份证))
            {
                BinDingData(_s身份证);
                this.cDisplayBox1.Visible = false;
                this.button2.Visible = false;
            }
        }


        private static UserControl卫生室病人 _ItSelf;
        public static UserControl卫生室病人 ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new UserControl卫生室病人();
                }
                return _ItSelf;
            }
        }

        //病人选择发生变化后调用委托（用于主页面病人刷新和发送）
        public delegate void SendPepoleInfo(string  Cardno);

        public SendPepoleInfo SendPPInfo;

        private void button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.cDisplayBox1.Text))
                BinDingData(this.cDisplayBox1.Text);
            this.label4.Text = "有";
            this.label7.Text = "10";
            this.label11.Text = "2";
        }

        /// <summary>
        /// 基本信息
        /// </summary>
        void BinDingData(string s身份证号)
        {
            System.Drawing.Image image = null;

            //string conn = "Server=192.168.10.131;Database=CHIS2701;User ID=yggsuser;password=yggsuser";
            string conn = "Data Source = 192.168.10.57; Initial Catalog = PubDB; Persist Security Info=True;User ID = yggsuser2016; Password=yggs2016user@163.com";
            string strSQL = "select top 1  name,sex,nation,birthday,address,picture from pubSfzhInfo where cardid='" + s身份证号 + "'";
            try
            {
                System.Data.SqlClient.SqlDataReader reader = WEISHENG.COMM.Db.SqlHelper.ExecuteReader(conn, CommandType.Text,
                            strSQL);
                reader.Read();
                if (reader.HasRows)
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream((byte[])reader["picture"]);
                    image = System.Drawing.Image.FromStream(ms, true);
                    this.txt姓名.Text = reader["name"].ToString();
                    this.txt性别.Text = reader["sex"].ToString();
                    this.txt身份证号.Text = s身份证号;
                    this.txt生日.Text = reader["birthday"].ToString();
                    this.txt地址.Text = reader["address"].ToString();
                    if (SendPPInfo!=null)
                        SendPPInfo(s身份证号);
                }
                else
                {
                    image = Properties.Resources.defaultPhoto;
                }
                this.pictureBox1.Image = image;
            }
            catch
            {

            }
        }

        private void myTabControl1_Selected(object sender, TabControlEventArgs e)
        {
            //记录旧身份证
            //old个人身份证号 = this.txt身份证号.Text;
            switch (myTabControl1.SelectedTab.Name)
            {
                case "tabPage病人基本信息":
                    break;
                case "tabPage健康档案":
                    if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                    {
                        if (tabPage健康档案.Tag == null || this.txt身份证号.Text != tabPage健康档案.Tag.ToString())
                            BindingJKDAN();
                    }
                    break;
                case "tabPage就诊记录":
                    if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                    {
                        if (tabPage就诊记录.Tag == null || this.txt身份证号.Text != tabPage就诊记录.Tag.ToString())
                            BindingZljl(this.txt身份证号.Text);
                    }
                    break;
                case "tabPage化验信息":
                    try
                    {
                        if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                            this.dataGridViewMain.DataSource = BindLisResult(txt身份证号.Text);
                        //this.splitContainerControl1.Panel1.Width = this.gridControlMain.Width;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("显示化验结果时出现异常.异常信息：" + ex.Message);
                    }
                    break;
                case "tabPage转诊":
                    try
                    {
                        if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                        {
                            tabPage转诊.Controls.Clear(); //移除所有控件
                            FJZL.person转诊 person = new FJZL.person转诊();
                            person.S姓名 = txt姓名.Text;
                            person.S身份证号 = txt身份证号.Text;
                            person.S性别 = txt性别.Text;
                            person.S家庭地址 = txt地址.Text;

                            FJZL.XtraForm转出 form = new FJZL.XtraForm转出(person);
                            // this.splitContainerControl1.Panel1.Width = this.gridControlMain.Width;
                            form.TopLevel = false;  //设置为非顶级窗体
                            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;//设置窗体为非边框样式
                            form.Dock = System.Windows.Forms.DockStyle.Fill;//设置样式是否填充整个panel
                            tabPage转诊.Controls.Add(form);//添加窗体
                            form.Show();//窗体运行
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show( ex.Message);
                    }
                    break;    
                default:
                    break;
            }
        }

        /// <summary>
        /// 健康档案
        /// </summary>
        void BindingJKDAN()
        {
            if (txt身份证号.Text != "")
            {
                tabPage健康档案.Controls.Clear(); //移除所有控件
                tabPage健康档案.Tag = txt身份证号.Text;
                Interface个人档案 form = new Interface个人档案(txt身份证号.Text);
                form.SQLConn = FrmMain.GetEHRConnStr();
                form.TopLevel = false;  //设置为非顶级窗体
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;//设置窗体为非边框样式
                form.Dock = System.Windows.Forms.DockStyle.Fill;//设置样式是否填充整个panel
                tabPage健康档案.Controls.Add(form);//添加窗体
                form.Show();//窗体运行
            }
        }

        /// <summary>
        /// 就诊记录
        /// </summary>
        void BindingZljl(string CardNo)
        {
            if (!string.IsNullOrEmpty(CardNo))
            {
                Get诊疗记录(CardNo, 10);
            }
        }

        /// <summary>
        /// 诊疗记录
        /// </summary>
        private DataTable Get诊疗记录(string cardid, int count)
        {
            DataTable dtDetaiInfo = null;
            try
            {
                interfaceZljl.Report.FrmZljl frm = new interfaceZljl.Report.FrmZljl();
                frm.SetReport(cardid, count);
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                dtDetaiInfo = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return dtDetaiInfo;
        }

        /// <summary>
        /// 绑定检验列表
        /// </summary>
        /// <param name="sfzh"></param>
        /// <returns></returns>
        private DataTable BindLisResult(string sfzh)
        {
            DataTable dtList = null;
            try
            {
                string conStr = FrmMain.GetLISConnStr();
                string spname = "Pro_GetLisResult";

                IMLibrary4.SqlData.DataAccess.ConnectionString = conStr;
                dtList = IMLibrary4.SqlData.DataAccess.GetDataSetByProc(spname, sfzh).Tables[0]; //"372827194610054039"
            }
            catch (Exception ex)
            {
                dtList = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            this.dataGridViewDetail.DataSource = null;
            return dtList;
        }

        /// <summary>
        /// 绑定检验明细
        /// </summary>
        /// <param name="yyid"></param>
        /// <param name="fjyid"></param>
        /// <returns></returns>
        private DataTable BindLisResultDetail(string yyid, string fjyid)
        {
            DataTable dtDetail = null;
            try
            {
                string conStr = FrmMain.GetLISConnStr();
                string spname = "Pro_GetLisResultDetail";

                dtDetail = IMLibrary4.SqlData.DataAccess.GetDataSetByProc(spname, yyid, fjyid).Tables[0];
            }
            catch (Exception ex)
            {
                dtDetail = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            return dtDetail;
        }

    }
}
