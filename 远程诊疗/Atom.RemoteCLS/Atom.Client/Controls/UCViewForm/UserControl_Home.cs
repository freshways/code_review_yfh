﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client
{
    public partial class UserControl_Home : UserControl
    {
        public UserControl_Home()
        {
            InitializeComponent();
        }

        private static UserControl_Home _ItSelf;
        public static UserControl_Home ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new UserControl_Home();
                }
                return _ItSelf;
            }
        }
        public delegate void DelegateOpenBRXX();

        public DelegateOpenBRXX OpenBRXX;

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (label10.Text == "0") return;
            OpenBRXX();
        }
    }
}
