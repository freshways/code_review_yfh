﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client
{
    public partial class UserControl病人列表 : UserControl
    {
        public UserControl病人列表()
        {
            InitializeComponent();
        }

        private static UserControl病人列表 _ItSelf;
        public static UserControl病人列表 ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new UserControl病人列表();
                }
                return _ItSelf;
            }
        }

        private void UserControl病人列表_Load(object sender, EventArgs e)
        {
            Init();
            BinDingData("");
        }

        void Init()
        {
            this.txt姓名.Text = "";
            this.txt性别.Text = "";
            this.txt生日.Text = "";
            this.txt身份证号.Text = "";
            this.txt地址.Text = "";
        }

        /// <summary>
        /// 基本信息
        /// </summary>
        void BinDingData(string s住院号码)
        {
//            string sql = @"select  * from (
//                                select top 50 ID,convert(varchar,MZID) 单据号,病人姓名,a.性别,年龄,身份证号
//                                from MF门诊摘要 a   where 1=2
//                                union all 
//                                select top 150 convert(varchar,ZYID),住院号码,病人姓名,性别,''  年龄,身份证号
//                                from ZY病人信息 where 已出院标记=0 order by 住院号码 desc) a  ";
//            if (!string.IsNullOrEmpty(s住院号码))
//                sql += " where 单据号 ='" + s住院号码 + "' ";
//            DataSet ds = IMLibrary4.SqlData.DataAccess.GetDataSetBySql(FrmMain.GetHISConnStr(), sql);
//            if (ds != null && ds.Tables.Count > 0)
//            {
//                dgv病人列表.DataSource = ds.Tables[0];
//            }
            string sqlCmd =
            "SELECT br.ZYID, br.住院号码, br.病人姓名,br.疾病名称,br.[疾病编码], br.入院时间," + "\r\n" +
            "       bq.科室名称 病区名称,br.病床 病床名称,  br.性别, DATEDIFF(year,  br.出生日期, getdate()) 年龄, br.单位编码," + "\r\n" +
            "       br.在院状态, br.主治医生编码, br.[病区] 病区编码, br.科室 科室编码," + "\r\n" +
            "       br.[家庭地址], " + "\r\n" +
            "       br.[分院编码],ys.用户名 主治医生姓名,ks.科室名称,br.医保类型,0 父ZYID,br.身份证号  " + "\r\n" +
            "FROM   ZY病人信息 AS br" + "\r\n" +
            "       left outer join gy科室设置 bq on br.病区 = bq.科室编码" + "\r\n" +
            "       left outer join gy科室设置 ks on br.科室 = ks.科室编码" + "\r\n" +
            "       left outer join pubuser ys on br.主治医生编码=ys.[用户编码] " + "\r\n" +
            " WHERE  (br.已出院标记 = 0)  and  br.在院状态='正常'  and br.分院编码=1 and isnull(br.身份证号,'') <> '' ";
            try
            {
                if (!string.IsNullOrEmpty(s住院号码))
                    sqlCmd += " and br.住院号码='" + s住院号码 + "' ";
                this.gridControl1.DataSource = WEISHENG.COMM.Db.SqlHelper.ExecuteDataset(FrmMain.GetHISConnStr(), CommandType.Text, sqlCmd).Tables[0];
                gridView1.PopulateColumns();
                try
                {
                    gridView1.Columns["ZYID"].Visible = false;
                    gridView1.Columns["单位编码"].Visible = false;
                    //gridView1.Columns["入院时间"].Visible = false;
                    gridView1.Columns["病区编码"].Visible = false;
                    gridView1.Columns["科室编码"].Visible = false;
                    gridView1.Columns["分院编码"].Visible = false;
                    gridView1.Columns["家庭地址"].Visible = false;
                    gridView1.Columns["主治医生编码"].Visible = false;
                    gridView1.Columns["科室名称"].Visible = false;
                    gridView1.Columns["医保类型"].Visible = false;
                    gridView1.Columns["父ZYID"].Visible = false;
                    //gridView1.Columns["疾病名称"].Visible = false;
                    gridView1.Columns["疾病编码"].Visible = false;
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
                for (int i = 0; i < gridView1.Columns.Count; i++)
                {
                    this.gridView1.Columns[i].AppearanceCell.Font = new Font("宋体", 12, FontStyle.Regular);
                    this.gridView1.Columns[i].AppearanceHeader.Font = new Font("宋体", 12, FontStyle.Bold);
                } 
                this.gridView1.OptionsView.RowAutoHeight = false;
                //this.gridView1.RowHeight = 40;
                this.gridView1.OptionsView.ColumnAutoWidth = false;
                this.gridView1.BestFitColumns();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        //病人选择发生变化后调用委托（用于主页面病人刷新和发送）
        public delegate void SendPepoleInfo(PeopleInfo info);

        public SendPepoleInfo SendPPInfo;

        public delegate void DelegateOpenBRXX();

        public DelegateOpenBRXX OpenBRXX;

        private void button1_Click(object sender, EventArgs e)
        {
            if (txt身份证号.Text!="")
                OpenBRXX();
        }

        private void metroTextBoxSearch1_Click(object sender, EventArgs e)
        {
            BinDingData(this.cDisplayBox1.Text);
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
                {
                    return;
                }
            if (gridView1.DataRowCount == 0)
            {
                return;
            }
            else
            {
                DataRow row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
                PeopleInfo pf = new PeopleInfo();
                pf.ZYID = Convert.ToInt32(row["ZYID"].ToString());
                pf.ZYH = row["住院号码"].ToString();
                pf.Name = row["病人姓名"].ToString();
                pf.Sex = row["性别"].ToString();
                pf.CardNo = row["身份证号"].ToString();
                pf.Age = row["年龄"].ToString();
                this.txt姓名.Text = pf.Name;
                this.txt性别.Text = pf.Sex;
                ///this.txt生日.Text = dr.Cells["病人姓名"].Value.ToString();
                ///this.txt地址.Text = row["家庭地址"].ToString();
                this.txt身份证号.Text = pf.CardNo;
                SendPPInfo(pf);
                BinDingData(pf);
            }
        }

        void BinDingData(PeopleInfo _Info)
        {
            System.Drawing.Image image = null;

            string conn = "Server=192.168.10.131;Database=CHIS2701;User ID=yggsuser;password=yggsuser";
            string strSQL = "select top 1  name,sex,nation,birthday,address,picture from pubSfzhInfo where cardid='" + _Info.CardNo + "'";
            try
            {
                System.Data.SqlClient.SqlDataReader reader = WEISHENG.COMM.Db.SqlHelper.ExecuteReader(conn, CommandType.Text,
                            strSQL);
                reader.Read();
                if (reader.HasRows)
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream((byte[])reader["picture"]);
                    image = System.Drawing.Image.FromStream(ms, true);
                    this.txt生日.Text = reader["birthday"].ToString();
                    this.txt地址.Text = reader["address"].ToString();
                }
                else
                {
                    image = Properties.Resources.defaultPhoto;
                }
                this.pictureBox1.Image = image;
            }
            catch
            {

            }
        }
    }
}
