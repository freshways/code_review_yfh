﻿namespace Atom.Client
{
    partial class UserControl病人列表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControl病人列表));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txt性别 = new Atom.Client.Controls.CDisplayBox();
            this.txt地址 = new Atom.Client.Controls.CDisplayBox();
            this.txt身份证号 = new Atom.Client.Controls.CDisplayBox();
            this.txt生日 = new Atom.Client.Controls.CDisplayBox();
            this.txt姓名 = new Atom.Client.Controls.CDisplayBox();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cDisplayBox1 = new Atom.Client.Controls.CDisplayBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Atom.Client.Properties.Resources.defaultPhoto;
            this.pictureBox1.Location = new System.Drawing.Point(77, 22);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(130, 142);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // txt性别
            // 
            this.txt性别.AllowMultiline = false;
            this.txt性别.Label = "性别";
            this.txt性别.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt性别.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt性别.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt性别.LabelWidth = 60F;
            this.txt性别.Location = new System.Drawing.Point(441, 22);
            this.txt性别.Margin = new System.Windows.Forms.Padding(2);
            this.txt性别.Name = "txt性别";
            this.txt性别.Size = new System.Drawing.Size(112, 32);
            this.txt性别.TabIndex = 20;
            this.txt性别.TextBackColor = System.Drawing.Color.White;
            this.txt性别.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt性别.TextFontPassWordChar = '\0';
            this.txt性别.TextReadOnly = true;
            this.txt性别.Value = null;
            // 
            // txt地址
            // 
            this.txt地址.AllowMultiline = false;
            this.txt地址.Label = "居住地址";
            this.txt地址.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt地址.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt地址.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt地址.LabelWidth = 100F;
            this.txt地址.Location = new System.Drawing.Point(230, 133);
            this.txt地址.Margin = new System.Windows.Forms.Padding(2);
            this.txt地址.Name = "txt地址";
            this.txt地址.Size = new System.Drawing.Size(324, 32);
            this.txt地址.TabIndex = 20;
            this.txt地址.TextBackColor = System.Drawing.Color.White;
            this.txt地址.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt地址.TextFontPassWordChar = '\0';
            this.txt地址.TextReadOnly = true;
            this.txt地址.Value = null;
            // 
            // txt身份证号
            // 
            this.txt身份证号.AllowMultiline = false;
            this.txt身份证号.Label = "身份证号";
            this.txt身份证号.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt身份证号.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt身份证号.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt身份证号.LabelWidth = 100F;
            this.txt身份证号.Location = new System.Drawing.Point(230, 96);
            this.txt身份证号.Margin = new System.Windows.Forms.Padding(2);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(324, 32);
            this.txt身份证号.TabIndex = 20;
            this.txt身份证号.TextBackColor = System.Drawing.Color.White;
            this.txt身份证号.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt身份证号.TextFontPassWordChar = '\0';
            this.txt身份证号.TextReadOnly = true;
            this.txt身份证号.Value = null;
            // 
            // txt生日
            // 
            this.txt生日.AllowMultiline = false;
            this.txt生日.Label = "出生日期";
            this.txt生日.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt生日.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt生日.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt生日.LabelWidth = 100F;
            this.txt生日.Location = new System.Drawing.Point(230, 59);
            this.txt生日.Margin = new System.Windows.Forms.Padding(2);
            this.txt生日.Name = "txt生日";
            this.txt生日.Size = new System.Drawing.Size(324, 32);
            this.txt生日.TabIndex = 20;
            this.txt生日.TextBackColor = System.Drawing.Color.White;
            this.txt生日.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt生日.TextFontPassWordChar = '\0';
            this.txt生日.TextReadOnly = true;
            this.txt生日.Value = null;
            // 
            // txt姓名
            // 
            this.txt姓名.AllowMultiline = false;
            this.txt姓名.Label = "姓名";
            this.txt姓名.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt姓名.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt姓名.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt姓名.LabelWidth = 100F;
            this.txt姓名.Location = new System.Drawing.Point(230, 22);
            this.txt姓名.Margin = new System.Windows.Forms.Padding(2);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(200, 32);
            this.txt姓名.TabIndex = 20;
            this.txt姓名.TextBackColor = System.Drawing.Color.White;
            this.txt姓名.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt姓名.TextFontPassWordChar = '\0';
            this.txt姓名.TextReadOnly = true;
            this.txt姓名.Value = null;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gridControl1.Font = new System.Drawing.Font("宋体", 19F);
            this.gridControl1.Location = new System.Drawing.Point(21, 239);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(584, 250);
            this.gridControl1.TabIndex = 22;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // cDisplayBox1
            // 
            this.cDisplayBox1.AllowMultiline = false;
            this.cDisplayBox1.Label = "身份证/住院号/门诊号";
            this.cDisplayBox1.LabelFont = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cDisplayBox1.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.cDisplayBox1.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cDisplayBox1.LabelWidth = 160F;
            this.cDisplayBox1.Location = new System.Drawing.Point(29, 186);
            this.cDisplayBox1.Margin = new System.Windows.Forms.Padding(2);
            this.cDisplayBox1.Name = "cDisplayBox1";
            this.cDisplayBox1.Size = new System.Drawing.Size(324, 32);
            this.cDisplayBox1.TabIndex = 20;
            this.cDisplayBox1.TextBackColor = System.Drawing.Color.White;
            this.cDisplayBox1.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cDisplayBox1.TextFontPassWordChar = '\0';
            this.cDisplayBox1.TextReadOnly = false;
            this.cDisplayBox1.Value = null;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(358, 189);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(100, 26);
            this.simpleButton1.TabIndex = 23;
            this.simpleButton1.Text = "查询";
            this.simpleButton1.Click += new System.EventHandler(this.metroTextBoxSearch1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(464, 189);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(100, 26);
            this.simpleButton2.TabIndex = 23;
            this.simpleButton2.Text = "查看详细";
            this.simpleButton2.Click += new System.EventHandler(this.button1_Click);
            // 
            // UserControl病人列表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt性别);
            this.Controls.Add(this.cDisplayBox1);
            this.Controls.Add(this.txt地址);
            this.Controls.Add(this.txt身份证号);
            this.Controls.Add(this.txt生日);
            this.Controls.Add(this.txt姓名);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UserControl病人列表";
            this.Size = new System.Drawing.Size(637, 502);
            this.Load += new System.EventHandler(this.UserControl病人列表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.CDisplayBox txt姓名;
        private Controls.CDisplayBox txt性别;
        private Controls.CDisplayBox txt生日;
        private Controls.CDisplayBox txt身份证号;
        private Controls.CDisplayBox txt地址;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private Controls.CDisplayBox cDisplayBox1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;

    }
}
