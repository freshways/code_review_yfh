﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client
{
    public partial class UserControl病人信息 : UserControl
    {
        public UserControl病人信息()
        {
            InitializeComponent();
        }

        string Age = "";
        PeopleInfo _Info;
        public PeopleInfo Info
        {
            get { return _Info; }
            set
            {
                _Info = value;
                if (_Info != null && _Info.CardNo != "")
                {
                    this.txt姓名.Text = _Info.Name;
                    this.txt性别.Text = _Info.Sex;
                    //this.txt生日.Text = "";
                    this.txt身份证号.Text = _Info.CardNo;
                    //this.txt地址.Text = "";
                    this.txt住院号.Text = _Info.ZYH;
                    Age = _Info.Age;
                    BinDingData();
                    myTabControl1.SelectedTab = tabPage病人基本信息;
                }
            }
        }

        private static UserControl病人信息 _ItSelf;
        public static UserControl病人信息 ItSelf
        {
            get
            {
                if (_ItSelf == null || _ItSelf.IsDisposed)
                {
                    _ItSelf = new UserControl病人信息();
                }
                return _ItSelf;
            }
        }

        private void UserControl病人信息_Load(object sender, EventArgs e)
        {
            Init();
        }
        

        private void myTabControl1_Selected(object sender, TabControlEventArgs e)
        {
            //记录旧身份证
            //old个人身份证号 = this.txt身份证号.Text;
            switch (myTabControl1.SelectedTab.Name)
            {
                case "tabPage病人基本信息":
                    break;
                case "tabPage健康档案":
                    if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                    {
                        if (tabPage健康档案.Tag == null || this.txt身份证号.Text != tabPage健康档案.Tag.ToString())
                            BindingJKDAN();
                    }
                    break;
                case "tabPage就诊记录":
                    if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                    {
                        if (tabPage就诊记录.Tag == null || this.txt身份证号.Text != tabPage就诊记录.Tag.ToString())
                            BindingZljl(this.txt身份证号.Text);
                    }
                    break;
                case "tabPage化验信息":
                    try
                    {
                        if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                            this.dataGridViewMain.DataSource = BindLisResult(txt身份证号.Text);
                        //this.splitContainerControl1.Panel1.Width = this.gridControlMain.Width;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("显示化验结果时出现异常.异常信息：" + ex.Message);
                    }
                    break;
                case "tabPage影像信息":
                    if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                    {
                        if (tabPage影像信息.Tag == null || this.txt身份证号.Text != tabPage影像信息.Tag.ToString())
                            BindingYX();
                    }
                    break;
                case "tabPage体温单":
                    if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                    {
                        //if (tabPage体温单.Tag == null || this.txt身份证号.Text != tabPage体温单.Tag.ToString())
                            BindingTWD();
                    }
                    break;
                case "tabPage电子病历":
                    if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                    {
                        if (tabPage电子病历.Tag == null || this.txt身份证号.Text != tabPage电子病历.Tag.ToString())
                            Binding病历信息();
                    }
                    break;
                case "tabPage转诊":
                    if (!string.IsNullOrEmpty(this.txt身份证号.Text))
                    {
                        tabPage转诊.Controls.Clear(); //移除所有控件
                        FJZL.person转诊 person = new FJZL.person转诊();
                        person.S姓名 = txt姓名.Text;
                        person.S身份证号 = txt身份证号.Text;
                        person.S性别 = txt性别.Text;
                        person.S家庭地址 = txt地址.Text;
                        person.S年龄 = Age;

                        FJZL.XtraForm转出上级 form = new FJZL.XtraForm转出上级();
                        form.person = person;
                        // this.splitContainerControl1.Panel1.Width = this.gridControlMain.Width;
                        form.TopLevel = false;  //设置为非顶级窗体
                        form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;//设置窗体为非边框样式
                        form.Dock = System.Windows.Forms.DockStyle.Fill;//设置样式是否填充整个panel
                        tabPage转诊.Controls.Add(form);//添加窗体
                        form.Show();//窗体运行
                    }
                    break;
                default:
                    break;
            }
        }

        #region Binding Function
        /// <summary>
        /// 初始化病人信息
        /// </summary>
        void Init()
        {
            this.txt姓名.Text = "";
            this.txt性别.Text = "";
            this.txt生日.Text = "";
            this.txt身份证号.Text = "";
            this.txt地址.Text = "";
            this.txt住院号.Text = "";
        }

        /// <summary>
        /// 医嘱信息
        /// </summary>
        void BinDingData()
        {
            System.Drawing.Image image = null;

            string conn = "Server=192.168.10.131;Database=CHIS2701;User ID=yggsuser;password=yggsuser";
            string strSQL = "select top 1  name,sex,nation,birthday,address,picture from pubSfzhInfo where cardid='" + _Info.CardNo + "'";
            try
            {
                System.Data.SqlClient.SqlDataReader reader = WEISHENG.COMM.Db.SqlHelper.ExecuteReader(conn, CommandType.Text,
                            strSQL);
                reader.Read();
                if (reader.HasRows)
                {
                    System.IO.MemoryStream ms = new System.IO.MemoryStream((byte[])reader["picture"]);
                    image = System.Drawing.Image.FromStream(ms, true);
                    this.txt生日.Text = reader["birthday"].ToString();
                    this.txt地址.Text = reader["address"].ToString();
                }
                else
                {
                    image = Properties.Resources.defaultPhoto;
                }
                this.pictureBox1.Image = image;
            }
            catch
            {

            }
//            string sql = @"select 医嘱类型,组别,项目类型,项目编码,项目名称,剂量数量,剂量单位,医嘱数量,频次名称,用法名称,开嘱时间
//                                from YS住院医嘱
//                                where ZYID =" + _Info.ZYID + " and 医嘱类型='长期医嘱'";
//             string   sql2=@"select 医嘱类型,组别,项目类型,项目编码,项目名称,剂量数量,剂量单位,医嘱数量,频次名称,用法名称,开嘱时间
//                            from YS住院医嘱
//                            where ZYID =" + _Info.ZYID + " and 医嘱类型='临时医嘱'";
//             DataSet ds = IMLibrary4.SqlData.DataAccess.GetDataSetBySql(FrmMain.GetHISConnStr(), sql + sql2);
//            if (ds != null && ds.Tables.Count > 0)
//            {
//                dgv长期医嘱.DataSource = ds.Tables[0];
//                dgv临时医嘱.DataSource = ds.Tables[1];
//            }
        }

        /// <summary>
        /// 健康档案
        /// </summary>
        void BindingJKDAN()
        {
            if (txt身份证号.Text != "")
            {
                tabPage健康档案.Controls.Clear(); //移除所有控件
                tabPage健康档案.Tag = txt身份证号.Text;
                Interface个人档案 form = new Interface个人档案(txt身份证号.Text);
                form.SQLConn = FrmMain.GetEHRConnStr();
                form.TopLevel = false;  //设置为非顶级窗体
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;//设置窗体为非边框样式
                form.Dock = System.Windows.Forms.DockStyle.Fill;//设置样式是否填充整个panel
                tabPage健康档案.Controls.Add(form);//添加窗体
                form.Show();//窗体运行
            }
        }

        /// <summary>
        /// 就诊记录
        /// </summary>
        void BindingZljl(string CardNo)
        {
            if (!string.IsNullOrEmpty(CardNo))
            {
                Get诊疗记录(CardNo, 10);
                //tabPage就诊记录.Tag = CardNo;
                //DataTable dt = Get诊疗记录(CardNo, 10);
                //dgv就诊信息.AutoGenerateColumns = false;
                //dgv就诊信息.DataSource = dt;
            }
        }

        /// <summary>
        /// 诊疗记录
        /// </summary>
        private DataTable Get诊疗记录(string cardid, int count)
        {
            DataTable dtDetaiInfo = null;
            try
            {
                ////string conStr = "Data Source=127.0.0.1;Initial Catalog=OurMsg_New;User ID=sa;Password=sasa";
                //string conStr = "Data Source=192.168.10.131;Initial Catalog=CHIS2701;User ID=yggsuser;Password=yggsuser";
                //string spname = "uSp就医信息检索";

                //IMLibrary4.SqlData.DataAccess.ConnectionString = conStr;
                //dtDetaiInfo = IMLibrary4.SqlData.DataAccess.GetDataSetByProc(spname, cardid, count).Tables[0]; //"372827195212275869"
                interfaceZljl.Report.FrmZljl frm = new interfaceZljl.Report.FrmZljl();
                frm.SetReport(cardid, count);
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                dtDetaiInfo = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return dtDetaiInfo;
        }

        /// <summary>
        /// 绑定检验列表
        /// </summary>
        /// <param name="sfzh"></param>
        /// <returns></returns>
        private DataTable BindLisResult(string sfzh)
        {
            DataTable dtList = null;
            try
            {
                string conStr = FrmMain.GetLISConnStr();
                string spname = "Pro_GetLisResult";

                IMLibrary4.SqlData.DataAccess.ConnectionString = conStr;
                dtList = IMLibrary4.SqlData.DataAccess.GetDataSetByProc(spname, sfzh).Tables[0]; //"372827194610054039"
            }
            catch (Exception ex)
            {
                dtList = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            this.dataGridViewDetail.DataSource = null;
            return dtList;
        }

        /// <summary>
        /// 绑定检验明细
        /// </summary>
        /// <param name="yyid"></param>
        /// <param name="fjyid"></param>
        /// <returns></returns>
        private DataTable BindLisResultDetail(string yyid, string fjyid)
        {
            DataTable dtDetail = null;
            try
            {
                string conStr = FrmMain.GetLISConnStr();
                string spname = "Pro_GetLisResultDetail";

                dtDetail = IMLibrary4.SqlData.DataAccess.GetDataSetByProc(spname, yyid, fjyid).Tables[0];
            }
            catch (Exception ex)
            {
                dtDetail = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            return dtDetail;
        }

        /// <summary>
        /// 影像信息
        /// </summary>
        void BindingYX()
        {
            if (txt身份证号.Text != "")
            {
                tabPage影像信息.Controls.Clear(); //移除所有控件
                tabPage影像信息.Tag = txt身份证号.Text;
                InterfacePacs form = new InterfacePacs(this.txt住院号.Text);

                form.TopLevel = false;  //设置为非顶级窗体
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;//设置窗体为非边框样式
                form.Dock = System.Windows.Forms.DockStyle.Fill;//设置样式是否填充整个panel
                tabPage影像信息.Controls.Add(form);//添加窗体
                form.Show();//窗体运行
            }
        }

        /// <summary>
        /// 体温单
        /// </summary>
        void BindingTWD()
        {
            if (txt身份证号.Text != "")
            {
                tabPage体温单.Controls.Clear(); //移除所有控件
                tabPage体温单.Tag = txt身份证号.Text;

                string 住院号 = Info.ZYID.ToString();//grdv病人信息.GetFocusedRowCellValue("ZYID").ToString();
                string 在院状态 = "在院";//grdv病人信息.GetFocusedRowCellValue("在院状态").ToString();
                WEISHENG.COMM.Db.SqlHelper._sDefaultConn = FrmMain.GetHISConnStr();
                HIS.HSZH.TWD.Form体温单 form = new HIS.HSZH.TWD.Form体温单();
                form.Tag = this;
                form.住院号 = 住院号;
                form.医院名称 = "沙沟镇中心卫生院";
                form.当前用户 = "当前用户";
                form.在院状态 = 在院状态;
                form.WindowState = FormWindowState.Maximized;
                form.ShowDialog();


                //form.TopLevel = false;  //设置为非顶级窗体
                //form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;//设置窗体为非边框样式
                //form.Dock = System.Windows.Forms.DockStyle.Fill;//设置样式是否填充整个panel
                //tabPage体温单.Controls.Add(form);//添加窗体
                //form.Show();//窗体运行
            }
        }

        private string Binding病历信息()
        {

            string text = "";

            string sql = @"select 主题,病历内容,aa.ID  from  emr病历索引 aa left join  ZY病人信息 bb on aa.病案号 = bb.住院号码 
                                where bb.ZYID = '" + _Info.ZYID + "' order by 创建时间 ";

            DataTable dt = IMLibrary4.SqlData.DataAccess.GetDataSetBySql(FrmMain.GetHISConnStr(), sql).Tables[0];

            #region 获取病历的Xml格式的数据
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                text += "<b>" + dt.Rows[i]["主题"].ToString() + "</b><br/>";

                System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                xml.LoadXml(dt.Rows[i]["病历内容"].ToString());
                //text += xml.Requestxml(dt.Rows[i]["病历内容"].ToString());//xml.ConvertXmlToEmrData() + "\n";
                text += xml.SelectSingleNode("/emrtextdoc/body").InnerXml;

            }
            #endregion

            //当数据库中没有记录的情况下
            if (string.IsNullOrWhiteSpace(text))
            {
                text = "没有找到病历记录.";
            }
            webBrowser1.DocumentText = text;
            return text;
        }

        #endregion

        /// <summary>
        /// 查看详细
        /// </summary>
        private void dataGridViewMain_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                DataGridViewRow dr = dataGridViewMain.Rows[e.RowIndex];

                string fjyid = dr.Cells[0].Value.ToString();
                string[] strs = fjyid.Split('_');

                if (strs.Length == 2)
                {
                    DataTable dtdetail = BindLisResultDetail(strs[0], strs[1]);

                    this.dataGridViewDetail.DataSource = dtdetail;
                }
            }
        }

        public void Dispos()
        {
            this.Dispose();
        }

    }
}
