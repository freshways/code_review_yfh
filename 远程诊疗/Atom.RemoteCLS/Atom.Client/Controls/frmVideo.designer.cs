﻿namespace Atom.Client.Controls
{
    partial class frmVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picRemote = new System.Windows.Forms.PictureBox();
            this.picLocal = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picRemote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLocal)).BeginInit();
            this.SuspendLayout();
            // 
            // picRemote
            // 
            this.picRemote.BackColor = System.Drawing.Color.Black;
            this.picRemote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picRemote.Location = new System.Drawing.Point(0, 0);
            this.picRemote.Name = "picRemote";
            this.picRemote.Size = new System.Drawing.Size(584, 453);
            this.picRemote.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRemote.TabIndex = 0;
            this.picRemote.TabStop = false;
            this.picRemote.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // picLocal
            // 
            this.picLocal.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.picLocal.Location = new System.Drawing.Point(252, 201);
            this.picLocal.Name = "picLocal";
            this.picLocal.Size = new System.Drawing.Size(320, 240);
            this.picLocal.TabIndex = 3;
            this.picLocal.TabStop = false;
            // 
            // frmVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 453);
            this.Controls.Add(this.picLocal);
            this.Controls.Add(this.picRemote);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmVideo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmVideo";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmVideo_KeyPress);
            this.Resize += new System.EventHandler(this.frmVideo_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.picRemote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLocal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox picRemote;
        public System.Windows.Forms.PictureBox picLocal;

    }
}