﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client
{
    public partial class InterfacePacs : Form
    {
        public InterfacePacs()
        {
            InitializeComponent();
        }

        string _ZYH = "";
        public InterfacePacs(string ZYH)
        {
            InitializeComponent();
            _ZYH = ZYH;
        }

        private static string _sConnOraPacsDB = @"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.10.107)(PORT=1521)) (CONNECT_DATA=(SERVICE_NAME=SPECTRA)));User Id=hishis;Password=hishis";
        DataTable dtMain;
        private void Binding结果()
        {
            string strSql = @"select  distinct HIS申请单,住院号,病人姓名,性别,年龄,检查项目,报告医生,报告时间,诊断描述,诊断结论,URL  
                                        from eris.ris2his_report where 1=1  ";
            strSql += " and 医院代码='106'  and 住院号='" + _ZYH + "' ";
            try
            {
                dtMain = WEISHENG.COMM.Db.OracleHelper.ExecuteDataset(_sConnOraPacsDB, CommandType.Text, strSql).Tables[0];
                dataGridView1.DataSource = dtMain;                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void InterfacePacs_Load(object sender, EventArgs e)
        {
            if (_ZYH != "")
                Binding结果();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "ColumnURL")
            {
                //MessageBox.Show("You picked " & dataGridView1.Rows[e.RowIndex]. Cells["ColumnURL"].Value)
                try
                {
                    string url = dataGridView1.Rows[e.RowIndex].Cells["ColumnURL"].Value.ToString();
                    if (!string.IsNullOrEmpty(url))
                        System.Diagnostics.Process.Start("iexplore.exe", url);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
