﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IMLibrary4.SqlData;

namespace Atom.Client
{
    public partial class Interface个人档案 : Form
    {
        public Interface个人档案()
        {
            InitializeComponent();
        }

        public Interface个人档案(string _Doc)
        {
            InitializeComponent();
            Doc = _Doc;
        }
        string Doc = "";

        private void Interface个人档案_Load(object sender, EventArgs e)
        {
            DataSet ds = Get个人档案(Doc);

            DoBindingDataSource(ds);
            //设置title文本居中
            //Graphics g = this.CreateGraphics();
            //Double startingPoint = (this.panel1.Parent.Width) - (g.MeasureString(panel1.Text.Trim(), panel1.Font).Width / 2);
            //Double ws = g.MeasureString("*", panel1.Font).Width;
            //String tmp = " ";
            //Double tw = 0;

            //while ((tw + ws) < startingPoint)
            //{
            //    tmp += "*";
            //    tw += ws;
            //}
            //panel1.Text = tmp.Replace("*", " ") + panel1.Text.Trim();
        }

        #region Public function
        /// <summary>
        /// 个人姓名解密
        /// </summary>
        /// <param name="s加密字符串"></param>
        /// <returns></returns>
        public static String DES解密(string s加密字符串)
        {
            String jiemi = "";
            int len = s加密字符串.Length / 16;
            for (int i = 0; i < len; i++)
            {
                jiemi += DESEncrypt.DESDeCode(s加密字符串.Substring(i * 16, 16));
            }
            return jiemi;
            //textBox4.Text = Encrypt.DESDeCode(textBox3.Text);
        }

        private DataTable _常用字典Property;
        public DataTable 常用字典Property
        {
            get
            {
                if (_常用字典Property != null && _常用字典Property.Rows.Count > 0)
                    return _常用字典Property;
                else
                {
                    string sql = "SELECT * FROM [dbo].tb_常用字典  where P_FLAG=1";
                    _常用字典Property = InterFaceJKDAN.Class.SqlHelper.ExecuteDataset(SQLConn, CommandType.Text, sql).Tables[0];
                    return _常用字典Property;
                }
            }
            set
            {
                _常用字典Property = value;
            }
        }

        /// <summary>
        /// 获取字典中的名称
        /// 2015-09-16 21:19:17 yufh 添加
        /// </summary>
        /// <param name="_类型"></param>
        /// <param name="_编码"></param>
        /// <returns></returns>
        public string ReturnDis字典显示(string _类型, string _编码)
        {
            if (常用字典Property != null)
            {
                DataRow[] dr = 常用字典Property.Select("(P_FUN_CODE='" + _类型 + "' or P_FUN_DESC = '" + _类型 + "') and P_CODE='" + _编码 + "' ");

                if (dr.Length > 0)
                {
                    return dr[0]["P_DESC"].ToString();
                }
            }
            return _编码;
        } 
        #endregion

        private void DoBindingDataSource(DataSet _ds)
        {
            DataTable table健康档案 = _ds.Tables["tb_健康档案"];
            DataTable table健康状态 = _ds.Tables["tb_健康档案_健康状态"];
            DataTable table既往病史 = _ds.Tables["tb_健康档案_既往病史"];
            DataTable table家族病史 = _ds.Tables["tb_健康档案_家族病史"];

            if (table健康档案.Rows.Count == 1)
            {
                this.txt档案号.Text = table健康档案.Rows[0]["个人档案编号"].ToString();
                this.txt姓名.Text = DES解密(table健康档案.Rows[0]["姓名"].ToString());
                this.txt性别.Text = table健康档案.Rows[0]["性别"].ToString();
                if (this.txt性别.Text == "男")
                {
                    //this.group妇女.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    this.txt孕产情况.Visible = false;
                    this.txt孕次.Visible = false;
                    this.txt产次.Visible = false;
                }
                else
                {
                    //this.group妇女.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    this.txt孕产情况.Text = table健康档案.Rows[0]["怀孕情况"].ToString();
                    this.txt孕次.Text = table健康档案.Rows[0]["孕次"].ToString();
                    this.txt产次.Text = table健康档案.Rows[0]["产次"].ToString();
                }
                this.txt出生日期.Text = table健康档案.Rows[0]["出生日期"].ToString();
                this.txt档案状态.Text = table健康档案.Rows[0]["档案状态"].ToString();
                this.txt缺项.Text = table健康档案.Rows[0]["缺项"].ToString();
                this.txt完整度.Text = table健康档案.Rows[0]["完整度"].ToString() + "%";

                this.txt证件号码.Text = table健康档案.Rows[0]["证件类型"] + ":" + table健康档案.Rows[0]["身份证号"];
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["身份证号"].ToString()))
                {
                    txt证件号码.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt工作单位.Text = table健康档案.Rows[0]["工作单位"] == null ? "" : table健康档案.Rows[0]["工作单位"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["工作单位"].ToString()))
                {
                    txt工作单位.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt本人电话.Text = table健康档案.Rows[0]["本人电话"] == null ? "" : Convert.ToString(table健康档案.Rows[0]["本人电话"]);
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["本人电话"].ToString()))
                {
                    txt本人电话.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt联系人姓名.Text = table健康档案.Rows[0]["联系人姓名"] == null ? "" : table健康档案.Rows[0]["联系人姓名"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["联系人姓名"].ToString()))
                {
                    txt联系人姓名.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt联系人电话.Text = table健康档案.Rows[0]["联系人电话"] == null ? "" : table健康档案.Rows[0]["联系人电话"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["联系人电话"].ToString()))
                {
                    txt联系人电话.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt常住类型.Text = table健康档案.Rows[0]["常住类型"] == null ? "" : table健康档案.Rows[0]["常住类型"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["常住类型"].ToString()))
                {
                    txt常住类型.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt民族.Text = table健康档案.Rows[0]["民族"] == null ? "" : table健康档案.Rows[0]["民族"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["民族"].ToString()))
                {
                    txt民族.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt与户主关系.Text = table健康档案.Rows[0]["与户主关系"] == null ? "" : table健康档案.Rows[0]["与户主关系"].ToString();

                this.txt职业.Text = table健康档案.Rows[0]["职业"] == null ? "" : table健康档案.Rows[0]["职业"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["职业"].ToString()))
                {
                    txt职业.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt文化程度.Text = table健康档案.Rows[0]["文化程度"] as string == null ? "" : table健康档案.Rows[0]["文化程度"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["文化程度"].ToString()))
                {
                    txt文化程度.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt劳动强度.Text = table健康档案.Rows[0]["劳动强度"] as string == null ? "" : table健康档案.Rows[0]["劳动强度"].ToString();
                this.txt婚姻状况.Text = table健康档案.Rows[0]["婚姻状况"] as string == null ? "" : table健康档案.Rows[0]["婚姻状况"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["婚姻状况"].ToString()))
                {
                    txt婚姻状况.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt医疗费用支付方式.Text = table健康档案.Rows[0]["医疗费支付类型"] == null ? "" : table健康档案.Rows[0]["医疗费支付类型"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["医疗费支付类型"].ToString()))
                {
                    txt医疗费用支付方式.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txt医疗保险号.Text = table健康档案.Rows[0]["医疗保险号"] == null ? "" : table健康档案.Rows[0]["医疗保险号"].ToString();
                this.txt新农合号.Text = table健康档案.Rows[0]["新农合号"] == null ? "" : table健康档案.Rows[0]["新农合号"].ToString();
                this.txt所属片区.Text = table健康档案.Rows[0]["所属片区"] == null ? "" : table健康档案.Rows[0]["所属片区"].ToString();
                this.txt档案类别.Text = table健康档案.Rows[0]["档案类别"] == null ? "" : table健康档案.Rows[0]["档案类别"].ToString();
                this.txt居住地址.Text = table健康档案.Rows[0]["市"].ToString() + table健康档案.Rows[0]["区"].ToString() + table健康档案.Rows[0]["街道"].ToString() + table健康档案.Rows[0]["居委会"].ToString() + table健康档案.Rows[0]["居住地址"].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0]["居住地址"].ToString()))
                {
                    txt居住地址.LabelForeColor = System.Drawing.Color.Red;
                }
                
                this.txt厨房排风设施.Text = table健康档案.Rows[0]["厨房排气设施"] as string == null ? "无" : table健康档案.Rows[0]["厨房排气设施"].ToString();
                this.txt燃料类型.Text = table健康档案.Rows[0]["燃料类型"] as string == null ? "无" : table健康档案.Rows[0]["燃料类型"].ToString();
                this.txt饮水.Text = table健康档案.Rows[0]["饮水"] as string == null ? "无" : table健康档案.Rows[0]["饮水"].ToString();
                this.txt厕所.Text = table健康档案.Rows[0]["厕所"] as string == null ? "无" : table健康档案.Rows[0]["厕所"].ToString();
                this.txt禽畜栏.Text = table健康档案.Rows[0]["禽畜栏"] as string == null ? "无" : table健康档案.Rows[0]["禽畜栏"].ToString();

                this.txt调查时间.Text = table健康档案.Rows[0]["调查时间"].ToString();
                this.txt最近更新时间.Text = table健康档案.Rows[0]["修改时间"].ToString();
                this.txt录入时间.Text = table健康档案.Rows[0]["创建时间"].ToString();
            }

            if (table健康状态.Rows.Count == 1)
            {
                this.txt血型.Text = table健康状态.Rows[0]["血型"] == null ? "" : table健康状态.Rows[0]["血型"].ToString();
                if (string.IsNullOrEmpty(table健康状态.Rows[0]["血型"].ToString()))
                {
                    txt血型.LabelForeColor = System.Drawing.Color.Red;
                }
                this.txtRH.Text = table健康状态.Rows[0]["RH"] == null ? "" : table健康状态.Rows[0]["RH"].ToString();
                if (string.IsNullOrEmpty(table健康状态.Rows[0]["RH"].ToString()))
                {
                    txtRH.LabelForeColor = System.Drawing.Color.Red;
                }

                this.txt化学品.Text = table健康状态.Rows[0]["暴露史"] as string == "2" ? "无" : table健康状态.Rows[0]["暴露史化学品"].ToString();
                this.txt毒物.Text = table健康状态.Rows[0]["暴露史"] as string == "2" ? "无" : table健康状态.Rows[0]["暴露史毒物"].ToString();
                this.txt射线.Text = table健康状态.Rows[0]["暴露史"] as string == "2" ? "无" : table健康状态.Rows[0]["暴露史射线"].ToString();
                this.txt遗传病史.Text = table健康状态.Rows[0]["遗传病史有无"] as string == "2" ? "无" : table健康状态.Rows[0]["遗传病史"].ToString();
                this.txt残疾情况.Text = (table健康状态.Rows[0]["残疾情况"] as string == null ? "无" : (table健康状态.Rows[0]["残疾情况"].ToString())
                    + (table健康状态.Rows[0]["残疾其他"] == "" ? "" : table健康状态.Rows[0]["残疾其他"].ToString()));
                this.txt药物过敏史.Text = (table健康状态.Rows[0]["过敏史有无"] as string == "2" ? "无" : table健康状态.Rows[0]["药物过敏史"].ToString())
                     + (table健康状态.Rows[0]["过敏史其他"] == null ? "" : table健康状态.Rows[0]["过敏史其他"].ToString());
            }

            if (table既往病史.Rows.Count > 0)
            {
                DataRow[] rows疾病 = table既往病史.Select("疾病类型='疾病'");//获取为疾病类型的所有数据行
                if (rows疾病.Length > 0)
                {
                    int count = rows疾病.Length;
                    int length = 0;
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < rows疾病.Length; i++)
                    {
                        DataRow item = rows疾病[i];
                        string 疾病名称 = item["疾病名称"].ToString();
                        if (疾病名称.IndexOf(',') != -1)//存在多个
                        {
                            string[] itemList = 疾病名称.Split(',');
                            //count += itemList.Length - 1;
                            for (int j = 0; j < itemList.Length; j++)
                            {
                                if (string.IsNullOrEmpty(itemList[j])) continue;
                                string _疾病名称 = ReturnDis字典显示("jb_gren", itemList[j]);
                                length++;
                                if (_疾病名称 == "其他")//其他
                                {
                                    if (j == itemList.Length - 1)
                                        builder.Append(_疾病名称 + ":" + item["疾病其他"].ToString().PadRight(10) + "确诊时间：" + item["日期"].ToString());
                                    else
                                        builder.AppendLine(_疾病名称 + ":" + item["疾病其他"].ToString().PadRight(10) + "确诊时间：" + item["日期"].ToString());
                                }
                                else
                                {
                                    if (j == itemList.Length - 1)
                                        builder.Append(_疾病名称.PadRight(10) + "确诊时间：" + item["日期"].ToString());
                                    else
                                        builder.AppendLine(_疾病名称.PadRight(10) + "确诊时间：" + item["日期"].ToString());
                                }
                            }
                        }
                        else//只有一条
                        {
                            string _疾病名称 = ReturnDis字典显示("jb_gren", 疾病名称);
                            length++;
                            if (_疾病名称 == "其他")
                            {
                                if (i == rows疾病.Length - 1)
                                    builder.Append(_疾病名称 + ":" + item["疾病其他"].ToString().PadRight(10) + "确诊时间：" + item["日期"].ToString());
                                else
                                    builder.AppendLine(_疾病名称 + ":" + item["疾病其他"].ToString().PadRight(10) + "确诊时间：" + item["日期"].ToString());
                            }
                            else
                            {
                                if (i == rows疾病.Length - 1)
                                    builder.Append(_疾病名称.PadRight(10) + "确诊时间：" + item["日期"].ToString());
                                else
                                    builder.AppendLine(_疾病名称.PadRight(10) + "确诊时间：" + item["日期"].ToString());
                            }
                        }
                    }
                    this.txt疾病.Text = builder.ToString();
                    //this.txt疾病.Size = new System.Drawing.Size(109, 24 * length);
                    this.txt疾病.Height = 24 * length;
                }
                else
                {
                    this.txt疾病.Text = "无";
                }

                DataRow[] rows手术 = table既往病史.Select("疾病类型='手术'");//获取为疾病类型的所有数据行
                if (rows手术.Length > 0)
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < rows手术.Length; i++)
                    {
                        DataRow item = rows手术[i];
                        string 疾病名称 = item["疾病名称"].ToString();
                        if (i == rows手术.Length - 1)
                            builder.Append(疾病名称.PadRight(10) + "确诊日期：" + item["日期"].ToString());
                        else
                            builder.AppendLine(疾病名称.PadRight(10) + "确诊日期：" + item["日期"].ToString());
                    }
                    this.txt手术.Text = builder.ToString();
                }
                else
                {
                    this.txt手术.Text = "无";
                }

                DataRow[] rows外伤 = table既往病史.Select("疾病类型='外伤'");//获取为疾病类型的所有数据行
                if (rows外伤.Length > 0)
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < rows外伤.Length; i++)
                    {
                        DataRow item = rows外伤[i];
                        string 疾病名称 = item["疾病名称"].ToString();
                        if (i == rows外伤.Length - 1)
                            builder.Append(疾病名称.PadRight(10) + "确诊日期：" + item["日期"].ToString());
                        else
                            builder.AppendLine(疾病名称.PadRight(10) + "确诊日期：" + item["日期"].ToString());
                    }
                    this.txt外伤.Text = builder.ToString();
                }
                else
                {
                    this.txt外伤.Text = "无";
                }

                DataRow[] rows输血 = table既往病史.Select("疾病类型='输血'");//获取为疾病类型的所有数据行
                if (rows输血.Length > 0)
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < rows输血.Length; i++)
                    {
                        DataRow item = rows输血[i];
                        string 疾病名称 = item["疾病名称"].ToString();
                        if (i == rows输血.Length - 1)
                            builder.Append(疾病名称.PadRight(10) + "确诊日期：" + item["日期"].ToString());
                        else
                            builder.AppendLine(疾病名称.PadRight(10) + "确诊日期：" + item["日期"].ToString());
                    }
                    this.txt输血.Text = builder.ToString();
                }
                else
                {
                    this.txt输血.Text = "无";
                }
            }
            else
            {
                this.txt输血.Text = "无";
                this.txt手术.Text = "无";
                this.txt外伤.Text = "无";
                this.txt疾病.Text = "无";
            }


            if (table家族病史.Rows.Count > 0)
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < table家族病史.Rows.Count; i++)
                {
                    string[] 疾病名称 = table家族病史.Rows[i]["家族病史"].ToString().Split(',');
                    string 家族关系 = ReturnDis字典显示("jzscy", table家族病史.Rows[i]["家族关系"].ToString());
                    string _疾病名称 = string.Empty;
                    for (int j = 0; j < 疾病名称.Length; j++)
                    {
                        string a = ReturnDis字典显示("jzsxin", 疾病名称[j]);
                        if (a == "其他")
                        {
                            _疾病名称 += table家族病史.Rows[i]["其他疾病"] + ", ";
                        }
                        else
                        {
                            _疾病名称 += a + ", ";
                        }
                    }
                    if (i == table家族病史.Rows.Count - 1)//最后一个
                        builder.Append("关系：" + 家族关系.PadRight(10) + _疾病名称);
                    else
                        builder.AppendLine("关系：" + 家族关系.PadRight(10) + _疾病名称);
                }
                this.txt家族史.Text = builder.ToString();
                this.txt家族史.Size = new Size(this.txt家族史.Width, this.txt家族史.Size.Height * table家族病史.Rows.Count);

            }
            else
            {
                this.txt家族史.Text = "无";
            }
        }

        string _SQLConn = "Data Source=221.2.93.84,7433;Initial Catalog=AtomEHR.YSDB;User ID=yggsuser;Password=yggsuser";//默认外网
        public string SQLConn
        {
            get
            {
                return _SQLConn;
            }
            set { _SQLConn = value; }
        }

        private DataSet Get个人档案(string docNo)
        {
            string sql0 = @"  declare @docnum varchar(20)
                                    select @docnum = 个人档案编号 from tb_健康档案 where [身份证号]='" + docNo + "' ";
            string sql1 = @"  SELECT  [ID]
                                              ,[个人档案编号]
                                              ,[家庭档案编号]
                                              ,[姓名]
                                              ,[身份证号]
                                              ,[拼音简码]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='yhzgx' AND B.P_CODE =[与户主关系])[与户主关系]
                                              ,[工作单位]
                                              ,[本人电话]
                                              ,[邮箱]
                                              ,[省]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [市])[市]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [区])[区]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [街道])[街道]
                                              ,(SELECT 地区名称 FROM dbo.tb_地区档案 WHERE 地区编码 = [居委会])[居委会]
                                              ,[居住地址]
                                              ,[所属片区]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='jzzk' AND B.P_CODE =[常住类型])[常住类型]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='xb_xingbie' AND B.P_CODE =[性别])[性别]
                                              ,[出生日期]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='mz_minzu' AND B.P_CODE =[民族])[民族]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='whcd' AND B.P_CODE =[文化程度])[文化程度]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='zy_zhiye' AND B.P_CODE =[职业])[职业]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='hyzk' AND B.P_CODE =[婚姻状况])[婚姻状况]
	  	                                        ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='ylfzflx' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [医疗费支付类型] +',')>0
		                                          FOR XML PATH('')) as '医疗费支付类型'
                                              ,[医疗保险号]
                                              ,[新农合号]
                                                ,所属机构 
                                              ,[调查时间]
                                              ,[创建时间]
                                              ,[修改时间]
                                              ,[创建人]
                                              ,[修改人]
                                              ,创建机构
                                              ,[PASSWORD]
                                              ,[D_ZHUXIAO]
                                              ,[联系人姓名]
                                              ,[联系人电话]
                                              ,[医疗费用支付类型其他]
                                              ,[怀孕情况]
                                              ,[孕次]
                                              ,[产次]
                                              ,[缺项]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='dalb' AND B.P_CODE = [档案类别])[档案类别]  
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='rkx_dazt' AND B.P_CODE = [档案状态])[档案状态]  
                                              ,[D_DAZTYY]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='zjlb' AND B.P_CODE = [证件类型])[证件类型]
                                              ,[D_ZJHQT]
	                                          ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cf_pqsb' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ [厨房排气设施] +',')>0
		                                          FOR XML PATH('')) as '厨房排气设施'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='ranliao_lx' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 燃料类型 +',')>0
		                                          FOR XML PATH('')) as '燃料类型'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='yinshui' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 饮水 +',')>0
		                                          FOR XML PATH('')) as '饮水'

		                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='qinxulan' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 禽畜栏 +',')>0
		                                          FOR XML PATH('')) as '禽畜栏'
                                                ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cesuo' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 厕所 +',')>0
		                                          FOR XML PATH('')) as '厕所'
                                              ,[完整度]
                                              ,[D_GRDABH_17]
                                              ,[D_GRDABH_SHOW]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='ldqd' AND B.P_CODE = [劳动强度])[劳动强度]
                                          FROM [tb_健康档案]    where [个人档案编号]=@docnum ";      //子表
            string sql2 = " select * from [tb_健康档案_个人健康特征]   where [个人档案编号]=@docnum ";      //子表
            string sql3 = @" SELECT  TOP 1  [ID]
                                              ,[个人档案编号]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='xuexing' AND B.P_CODE =[血型])[血型]
                                              ,(SELECT B.P_DESC FROM tb_常用字典 B WHERE B.P_FUN_CODE='rhxx' AND B.P_CODE =[RH])[RH]
                                              ,[过敏史有无]
	                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='gms' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 药物过敏史 +',')>0
		                                          FOR XML PATH('')) as '药物过敏史'
                                              ,[过敏史其他]
                                              ,[暴露史]
                                              ,[D_YBLS]
                                              ,[暴露史化学品]
                                              ,[暴露史毒物]
                                              ,[暴露史射线]
                                              ,[遗传病史有无]
                                              ,[遗传病史]
                                              ,[有无残疾]
                                              ,[有无疾病]
                                              ,[有无既往史]
	                                           ,(SELECT P_DESC+',' FROM [tb_常用字典] 
		                                          WHERE P_FUN_CODE='cjjk' and 
		                                          charindex(','+rtrim(p_code )+',' , ','+ 残疾情况 +',')>0
		                                          FOR XML PATH('')) as '残疾情况'
                                              ,[调查时间]
                                              ,[创建人]
                                              ,[修改人]
                                              ,[创建时间]
                                              ,[残疾其他]
                                          FROM[tb_健康档案_健康状态]   where [个人档案编号]=@docnum ORDER BY ID DESC";      //子表
            string sql4 = " select * from [tb_健康档案_既往病史] where [个人档案编号]=@docnum ";//既往史
            string sql5 = " select * from [tb_健康档案_家族病史] where [个人档案编号]=@docnum ";//家族病史
            
            //如果有多个子表请继续添加            
            //SqlCommand cmd = new SqlCommand(sql0+sql1 + sql2 + sql3 + sql4 + sql5);
            //cmd.CommandType = CommandType.Text;
            //SqlConnection conn = new SqlConnection(SQLConn);
            //cmd.Connection = conn;
            //cmd.Parameters.Add("@DocNo1", SqlDbType.VarChar).Value = docNo.Trim();
            //SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            //DataSet ds = null;
            //adapter.Fill(ds);
            DataSet ds = DataAccess.GetDataSetBySql(SQLConn, (sql0 + sql1 + sql2 + sql3 + sql4 + sql5));
            ds.Tables[0].TableName = "tb_健康档案";
            ds.Tables[1].TableName = "tb_健康档案_个人健康特征";   //子表
            ds.Tables[2].TableName = "tb_健康档案_健康状态";   //子表
            ds.Tables[3].TableName = "tb_健康档案_既往病史";   //子表
            ds.Tables[4].TableName = "tb_健康档案_家族病史";   //子表
            return ds;
        }

        private void xPanderPanel1_Click(object sender, EventArgs e)
        {

        }

    }
}
