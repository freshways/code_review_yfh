﻿namespace Atom.Client
{
    partial class Interface个人档案
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt档案号 = new Atom.Client.Controls.CDisplayBox();
            this.txt姓名 = new Atom.Client.Controls.CDisplayBox();
            this.txt性别 = new Atom.Client.Controls.CDisplayBox();
            this.txt出生日期 = new Atom.Client.Controls.CDisplayBox();
            this.txt档案状态 = new Atom.Client.Controls.CDisplayBox();
            this.txt缺项 = new Atom.Client.Controls.CDisplayBox();
            this.txt完整度 = new Atom.Client.Controls.CDisplayBox();
            this.txt证件号码 = new Atom.Client.Controls.CDisplayBox();
            this.txt工作单位 = new Atom.Client.Controls.CDisplayBox();
            this.txt本人电话 = new Atom.Client.Controls.CDisplayBox();
            this.txt联系人姓名 = new Atom.Client.Controls.CDisplayBox();
            this.txt联系人电话 = new Atom.Client.Controls.CDisplayBox();
            this.txt常住类型 = new Atom.Client.Controls.CDisplayBox();
            this.txt民族 = new Atom.Client.Controls.CDisplayBox();
            this.txt与户主关系 = new Atom.Client.Controls.CDisplayBox();
            this.txt血型 = new Atom.Client.Controls.CDisplayBox();
            this.txtRH = new Atom.Client.Controls.CDisplayBox();
            this.txt文化程度 = new Atom.Client.Controls.CDisplayBox();
            this.txt职业 = new Atom.Client.Controls.CDisplayBox();
            this.txt劳动强度 = new Atom.Client.Controls.CDisplayBox();
            this.txt婚姻状况 = new Atom.Client.Controls.CDisplayBox();
            this.txt医疗费用支付方式 = new Atom.Client.Controls.CDisplayBox();
            this.txt医疗保险号 = new Atom.Client.Controls.CDisplayBox();
            this.txt新农合号 = new Atom.Client.Controls.CDisplayBox();
            this.txt所属片区 = new Atom.Client.Controls.CDisplayBox();
            this.txt档案类别 = new Atom.Client.Controls.CDisplayBox();
            this.txt居住地址 = new Atom.Client.Controls.CDisplayBox();
            this.txt药物过敏史 = new Atom.Client.Controls.CDisplayBox();
            this.myPanel既往史 = new MySkin.Controls.MyPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt输血 = new Atom.Client.Controls.CDisplayBox();
            this.txt外伤 = new Atom.Client.Controls.CDisplayBox();
            this.txt疾病 = new Atom.Client.Controls.CDisplayBox();
            this.txt手术 = new Atom.Client.Controls.CDisplayBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt家族史 = new Atom.Client.Controls.CDisplayBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt化学品 = new Atom.Client.Controls.CDisplayBox();
            this.txt毒物 = new Atom.Client.Controls.CDisplayBox();
            this.txt射线 = new Atom.Client.Controls.CDisplayBox();
            this.txt遗传病史 = new Atom.Client.Controls.CDisplayBox();
            this.txt残疾情况 = new Atom.Client.Controls.CDisplayBox();
            this.txt孕产情况 = new Atom.Client.Controls.CDisplayBox();
            this.txt孕次 = new Atom.Client.Controls.CDisplayBox();
            this.txt产次 = new Atom.Client.Controls.CDisplayBox();
            this.myPanel1 = new MySkin.Controls.MyPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt厨房排风设施 = new Atom.Client.Controls.CDisplayBox();
            this.txt燃料类型 = new Atom.Client.Controls.CDisplayBox();
            this.txt饮水 = new Atom.Client.Controls.CDisplayBox();
            this.txt厕所 = new Atom.Client.Controls.CDisplayBox();
            this.txt禽畜栏 = new Atom.Client.Controls.CDisplayBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt调查时间 = new Atom.Client.Controls.CDisplayBox();
            this.txt录入时间 = new Atom.Client.Controls.CDisplayBox();
            this.txt最近更新时间 = new Atom.Client.Controls.CDisplayBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.myPanel既往史.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.myPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.txt档案号);
            this.flowLayoutPanel1.Controls.Add(this.txt姓名);
            this.flowLayoutPanel1.Controls.Add(this.txt性别);
            this.flowLayoutPanel1.Controls.Add(this.txt出生日期);
            this.flowLayoutPanel1.Controls.Add(this.txt档案状态);
            this.flowLayoutPanel1.Controls.Add(this.txt缺项);
            this.flowLayoutPanel1.Controls.Add(this.txt完整度);
            this.flowLayoutPanel1.Controls.Add(this.txt证件号码);
            this.flowLayoutPanel1.Controls.Add(this.txt工作单位);
            this.flowLayoutPanel1.Controls.Add(this.txt本人电话);
            this.flowLayoutPanel1.Controls.Add(this.txt联系人姓名);
            this.flowLayoutPanel1.Controls.Add(this.txt联系人电话);
            this.flowLayoutPanel1.Controls.Add(this.txt常住类型);
            this.flowLayoutPanel1.Controls.Add(this.txt民族);
            this.flowLayoutPanel1.Controls.Add(this.txt与户主关系);
            this.flowLayoutPanel1.Controls.Add(this.txt血型);
            this.flowLayoutPanel1.Controls.Add(this.txtRH);
            this.flowLayoutPanel1.Controls.Add(this.txt文化程度);
            this.flowLayoutPanel1.Controls.Add(this.txt职业);
            this.flowLayoutPanel1.Controls.Add(this.txt劳动强度);
            this.flowLayoutPanel1.Controls.Add(this.txt婚姻状况);
            this.flowLayoutPanel1.Controls.Add(this.txt医疗费用支付方式);
            this.flowLayoutPanel1.Controls.Add(this.txt医疗保险号);
            this.flowLayoutPanel1.Controls.Add(this.txt新农合号);
            this.flowLayoutPanel1.Controls.Add(this.txt所属片区);
            this.flowLayoutPanel1.Controls.Add(this.txt档案类别);
            this.flowLayoutPanel1.Controls.Add(this.txt居住地址);
            this.flowLayoutPanel1.Controls.Add(this.txt药物过敏史);
            this.flowLayoutPanel1.Controls.Add(this.myPanel既往史);
            this.flowLayoutPanel1.Controls.Add(this.txt家族史);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.txt化学品);
            this.flowLayoutPanel1.Controls.Add(this.txt毒物);
            this.flowLayoutPanel1.Controls.Add(this.txt射线);
            this.flowLayoutPanel1.Controls.Add(this.txt遗传病史);
            this.flowLayoutPanel1.Controls.Add(this.txt残疾情况);
            this.flowLayoutPanel1.Controls.Add(this.txt孕产情况);
            this.flowLayoutPanel1.Controls.Add(this.txt孕次);
            this.flowLayoutPanel1.Controls.Add(this.txt产次);
            this.flowLayoutPanel1.Controls.Add(this.myPanel1);
            this.flowLayoutPanel1.Controls.Add(this.txt调查时间);
            this.flowLayoutPanel1.Controls.Add(this.txt录入时间);
            this.flowLayoutPanel1.Controls.Add(this.txt最近更新时间);
            this.flowLayoutPanel1.Controls.Add(this.label4);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 26);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(774, 588);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // txt档案号
            // 
            this.txt档案号.AllowMultiline = false;
            this.txt档案号.Label = "档案号:";
            this.txt档案号.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt档案号.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt档案号.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt档案号.LabelWidth = 100F;
            this.txt档案号.Location = new System.Drawing.Point(1, 5);
            this.txt档案号.Margin = new System.Windows.Forms.Padding(1);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(298, 26);
            this.txt档案号.TabIndex = 2;
            this.txt档案号.TextBackColor = System.Drawing.Color.White;
            this.txt档案号.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt档案号.TextFontPassWordChar = '\0';
            this.txt档案号.TextReadOnly = true;
            this.txt档案号.Value = null;
            // 
            // txt姓名
            // 
            this.txt姓名.AllowMultiline = false;
            this.txt姓名.Label = "姓名:";
            this.txt姓名.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt姓名.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt姓名.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt姓名.LabelWidth = 100F;
            this.txt姓名.Location = new System.Drawing.Point(301, 5);
            this.txt姓名.Margin = new System.Windows.Forms.Padding(1);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(206, 26);
            this.txt姓名.TabIndex = 2;
            this.txt姓名.TextBackColor = System.Drawing.Color.White;
            this.txt姓名.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt姓名.TextFontPassWordChar = '\0';
            this.txt姓名.TextReadOnly = true;
            this.txt姓名.Value = null;
            // 
            // txt性别
            // 
            this.txt性别.AllowMultiline = false;
            this.txt性别.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.SetFlowBreak(this.txt性别, true);
            this.txt性别.Label = "性别:";
            this.txt性别.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt性别.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt性别.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt性别.LabelWidth = 120F;
            this.txt性别.Location = new System.Drawing.Point(509, 5);
            this.txt性别.Margin = new System.Windows.Forms.Padding(1);
            this.txt性别.Name = "txt性别";
            this.txt性别.Size = new System.Drawing.Size(234, 26);
            this.txt性别.TabIndex = 2;
            this.txt性别.TextBackColor = System.Drawing.Color.White;
            this.txt性别.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt性别.TextFontPassWordChar = '\0';
            this.txt性别.TextReadOnly = true;
            this.txt性别.Value = null;
            // 
            // txt出生日期
            // 
            this.txt出生日期.AllowMultiline = false;
            this.txt出生日期.Label = "出生日期:";
            this.txt出生日期.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt出生日期.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt出生日期.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt出生日期.LabelWidth = 100F;
            this.txt出生日期.Location = new System.Drawing.Point(1, 33);
            this.txt出生日期.Margin = new System.Windows.Forms.Padding(1);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Size = new System.Drawing.Size(298, 26);
            this.txt出生日期.TabIndex = 2;
            this.txt出生日期.TextBackColor = System.Drawing.Color.White;
            this.txt出生日期.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt出生日期.TextFontPassWordChar = '\0';
            this.txt出生日期.TextReadOnly = true;
            this.txt出生日期.Value = null;
            // 
            // txt档案状态
            // 
            this.txt档案状态.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt档案状态, true);
            this.txt档案状态.Label = "档案状态:";
            this.txt档案状态.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt档案状态.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt档案状态.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt档案状态.LabelWidth = 100F;
            this.txt档案状态.Location = new System.Drawing.Point(301, 33);
            this.txt档案状态.Margin = new System.Windows.Forms.Padding(1);
            this.txt档案状态.Name = "txt档案状态";
            this.txt档案状态.Size = new System.Drawing.Size(442, 26);
            this.txt档案状态.TabIndex = 2;
            this.txt档案状态.TextBackColor = System.Drawing.Color.White;
            this.txt档案状态.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt档案状态.TextFontPassWordChar = '\0';
            this.txt档案状态.TextReadOnly = true;
            this.txt档案状态.Value = null;
            // 
            // txt缺项
            // 
            this.txt缺项.AllowMultiline = false;
            this.txt缺项.Label = "缺项:";
            this.txt缺项.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt缺项.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt缺项.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt缺项.LabelWidth = 100F;
            this.txt缺项.Location = new System.Drawing.Point(1, 61);
            this.txt缺项.Margin = new System.Windows.Forms.Padding(1, 1, 1, 7);
            this.txt缺项.Name = "txt缺项";
            this.txt缺项.Size = new System.Drawing.Size(298, 26);
            this.txt缺项.TabIndex = 2;
            this.txt缺项.TextBackColor = System.Drawing.Color.White;
            this.txt缺项.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt缺项.TextFontPassWordChar = '\0';
            this.txt缺项.TextReadOnly = true;
            this.txt缺项.Value = null;
            // 
            // txt完整度
            // 
            this.txt完整度.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt完整度, true);
            this.txt完整度.Label = "完整度:";
            this.txt完整度.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt完整度.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt完整度.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt完整度.LabelWidth = 100F;
            this.txt完整度.Location = new System.Drawing.Point(301, 61);
            this.txt完整度.Margin = new System.Windows.Forms.Padding(1);
            this.txt完整度.Name = "txt完整度";
            this.txt完整度.Size = new System.Drawing.Size(442, 26);
            this.txt完整度.TabIndex = 2;
            this.txt完整度.TextBackColor = System.Drawing.Color.White;
            this.txt完整度.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt完整度.TextFontPassWordChar = '\0';
            this.txt完整度.TextReadOnly = true;
            this.txt完整度.Value = null;
            // 
            // txt证件号码
            // 
            this.txt证件号码.AllowMultiline = false;
            this.txt证件号码.Label = "证件号码:";
            this.txt证件号码.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt证件号码.LabelForeColor = System.Drawing.Color.Blue;
            this.txt证件号码.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt证件号码.LabelWidth = 100F;
            this.txt证件号码.Location = new System.Drawing.Point(1, 95);
            this.txt证件号码.Margin = new System.Windows.Forms.Padding(1);
            this.txt证件号码.Name = "txt证件号码";
            this.txt证件号码.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.txt证件号码.Size = new System.Drawing.Size(298, 26);
            this.txt证件号码.TabIndex = 2;
            this.txt证件号码.TextBackColor = System.Drawing.Color.White;
            this.txt证件号码.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt证件号码.TextFontPassWordChar = '\0';
            this.txt证件号码.TextReadOnly = true;
            this.txt证件号码.Value = null;
            // 
            // txt工作单位
            // 
            this.txt工作单位.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt工作单位, true);
            this.txt工作单位.Label = "工作单位:";
            this.txt工作单位.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt工作单位.LabelForeColor = System.Drawing.Color.Blue;
            this.txt工作单位.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt工作单位.LabelWidth = 100F;
            this.txt工作单位.Location = new System.Drawing.Point(301, 95);
            this.txt工作单位.Margin = new System.Windows.Forms.Padding(1);
            this.txt工作单位.Name = "txt工作单位";
            this.txt工作单位.Size = new System.Drawing.Size(442, 26);
            this.txt工作单位.TabIndex = 2;
            this.txt工作单位.TextBackColor = System.Drawing.Color.White;
            this.txt工作单位.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt工作单位.TextFontPassWordChar = '\0';
            this.txt工作单位.TextReadOnly = true;
            this.txt工作单位.Value = null;
            // 
            // txt本人电话
            // 
            this.txt本人电话.AllowMultiline = false;
            this.txt本人电话.Label = "本人电话:";
            this.txt本人电话.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt本人电话.LabelForeColor = System.Drawing.Color.Blue;
            this.txt本人电话.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt本人电话.LabelWidth = 100F;
            this.txt本人电话.Location = new System.Drawing.Point(1, 123);
            this.txt本人电话.Margin = new System.Windows.Forms.Padding(1);
            this.txt本人电话.Name = "txt本人电话";
            this.txt本人电话.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.txt本人电话.Size = new System.Drawing.Size(298, 26);
            this.txt本人电话.TabIndex = 2;
            this.txt本人电话.TextBackColor = System.Drawing.Color.White;
            this.txt本人电话.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt本人电话.TextFontPassWordChar = '\0';
            this.txt本人电话.TextReadOnly = true;
            this.txt本人电话.Value = null;
            // 
            // txt联系人姓名
            // 
            this.txt联系人姓名.AllowMultiline = false;
            this.txt联系人姓名.Label = "联系人:";
            this.txt联系人姓名.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt联系人姓名.LabelForeColor = System.Drawing.Color.Blue;
            this.txt联系人姓名.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt联系人姓名.LabelWidth = 100F;
            this.txt联系人姓名.Location = new System.Drawing.Point(301, 123);
            this.txt联系人姓名.Margin = new System.Windows.Forms.Padding(1);
            this.txt联系人姓名.Name = "txt联系人姓名";
            this.txt联系人姓名.Size = new System.Drawing.Size(206, 26);
            this.txt联系人姓名.TabIndex = 2;
            this.txt联系人姓名.TextBackColor = System.Drawing.Color.White;
            this.txt联系人姓名.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt联系人姓名.TextFontPassWordChar = '\0';
            this.txt联系人姓名.TextReadOnly = true;
            this.txt联系人姓名.Value = null;
            // 
            // txt联系人电话
            // 
            this.txt联系人电话.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt联系人电话, true);
            this.txt联系人电话.Label = "联系人电话:";
            this.txt联系人电话.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt联系人电话.LabelForeColor = System.Drawing.Color.Blue;
            this.txt联系人电话.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt联系人电话.LabelWidth = 120F;
            this.txt联系人电话.Location = new System.Drawing.Point(509, 123);
            this.txt联系人电话.Margin = new System.Windows.Forms.Padding(1);
            this.txt联系人电话.Name = "txt联系人电话";
            this.txt联系人电话.Size = new System.Drawing.Size(234, 26);
            this.txt联系人电话.TabIndex = 2;
            this.txt联系人电话.TextBackColor = System.Drawing.Color.White;
            this.txt联系人电话.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt联系人电话.TextFontPassWordChar = '\0';
            this.txt联系人电话.TextReadOnly = true;
            this.txt联系人电话.Value = null;
            // 
            // txt常住类型
            // 
            this.txt常住类型.AllowMultiline = false;
            this.txt常住类型.Label = "常住类型:";
            this.txt常住类型.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt常住类型.LabelForeColor = System.Drawing.Color.Blue;
            this.txt常住类型.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt常住类型.LabelWidth = 100F;
            this.txt常住类型.Location = new System.Drawing.Point(1, 151);
            this.txt常住类型.Margin = new System.Windows.Forms.Padding(1);
            this.txt常住类型.Name = "txt常住类型";
            this.txt常住类型.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.txt常住类型.Size = new System.Drawing.Size(298, 26);
            this.txt常住类型.TabIndex = 2;
            this.txt常住类型.TextBackColor = System.Drawing.Color.White;
            this.txt常住类型.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt常住类型.TextFontPassWordChar = '\0';
            this.txt常住类型.TextReadOnly = true;
            this.txt常住类型.Value = null;
            // 
            // txt民族
            // 
            this.txt民族.AllowMultiline = false;
            this.txt民族.Label = "民族:";
            this.txt民族.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt民族.LabelForeColor = System.Drawing.Color.Blue;
            this.txt民族.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt民族.LabelWidth = 100F;
            this.txt民族.Location = new System.Drawing.Point(301, 151);
            this.txt民族.Margin = new System.Windows.Forms.Padding(1);
            this.txt民族.Name = "txt民族";
            this.txt民族.Size = new System.Drawing.Size(206, 26);
            this.txt民族.TabIndex = 2;
            this.txt民族.TextBackColor = System.Drawing.Color.White;
            this.txt民族.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt民族.TextFontPassWordChar = '\0';
            this.txt民族.TextReadOnly = true;
            this.txt民族.Value = null;
            // 
            // txt与户主关系
            // 
            this.txt与户主关系.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt与户主关系, true);
            this.txt与户主关系.Label = "与户主关系:";
            this.txt与户主关系.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt与户主关系.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt与户主关系.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt与户主关系.LabelWidth = 120F;
            this.txt与户主关系.Location = new System.Drawing.Point(509, 151);
            this.txt与户主关系.Margin = new System.Windows.Forms.Padding(1);
            this.txt与户主关系.Name = "txt与户主关系";
            this.txt与户主关系.Size = new System.Drawing.Size(234, 26);
            this.txt与户主关系.TabIndex = 2;
            this.txt与户主关系.TextBackColor = System.Drawing.Color.White;
            this.txt与户主关系.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt与户主关系.TextFontPassWordChar = '\0';
            this.txt与户主关系.TextReadOnly = true;
            this.txt与户主关系.Value = null;
            // 
            // txt血型
            // 
            this.txt血型.AllowMultiline = false;
            this.txt血型.Label = "血型:";
            this.txt血型.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt血型.LabelForeColor = System.Drawing.Color.Blue;
            this.txt血型.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt血型.LabelWidth = 100F;
            this.txt血型.Location = new System.Drawing.Point(1, 179);
            this.txt血型.Margin = new System.Windows.Forms.Padding(1);
            this.txt血型.Name = "txt血型";
            this.txt血型.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.txt血型.Size = new System.Drawing.Size(298, 26);
            this.txt血型.TabIndex = 2;
            this.txt血型.TextBackColor = System.Drawing.Color.White;
            this.txt血型.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt血型.TextFontPassWordChar = '\0';
            this.txt血型.TextReadOnly = true;
            this.txt血型.Value = null;
            // 
            // txtRH
            // 
            this.txtRH.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txtRH, true);
            this.txtRH.Label = "RH:";
            this.txtRH.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtRH.LabelForeColor = System.Drawing.Color.Blue;
            this.txtRH.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtRH.LabelWidth = 100F;
            this.txtRH.Location = new System.Drawing.Point(301, 179);
            this.txtRH.Margin = new System.Windows.Forms.Padding(1);
            this.txtRH.Name = "txtRH";
            this.txtRH.Size = new System.Drawing.Size(442, 26);
            this.txtRH.TabIndex = 2;
            this.txtRH.TextBackColor = System.Drawing.Color.White;
            this.txtRH.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtRH.TextFontPassWordChar = '\0';
            this.txtRH.TextReadOnly = true;
            this.txtRH.Value = null;
            // 
            // txt文化程度
            // 
            this.txt文化程度.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt文化程度, true);
            this.txt文化程度.Label = "文化程度:";
            this.txt文化程度.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt文化程度.LabelForeColor = System.Drawing.Color.Blue;
            this.txt文化程度.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt文化程度.LabelWidth = 100F;
            this.txt文化程度.Location = new System.Drawing.Point(2, 208);
            this.txt文化程度.Margin = new System.Windows.Forms.Padding(2);
            this.txt文化程度.Name = "txt文化程度";
            this.txt文化程度.Size = new System.Drawing.Size(742, 26);
            this.txt文化程度.TabIndex = 2;
            this.txt文化程度.TextBackColor = System.Drawing.Color.White;
            this.txt文化程度.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt文化程度.TextFontPassWordChar = '\0';
            this.txt文化程度.TextReadOnly = true;
            this.txt文化程度.Value = null;
            // 
            // txt职业
            // 
            this.txt职业.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt职业, true);
            this.txt职业.Label = "职业:";
            this.txt职业.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt职业.LabelForeColor = System.Drawing.Color.Blue;
            this.txt职业.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt职业.LabelWidth = 100F;
            this.txt职业.Location = new System.Drawing.Point(2, 238);
            this.txt职业.Margin = new System.Windows.Forms.Padding(2);
            this.txt职业.Name = "txt职业";
            this.txt职业.Size = new System.Drawing.Size(742, 26);
            this.txt职业.TabIndex = 2;
            this.txt职业.TextBackColor = System.Drawing.Color.White;
            this.txt职业.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt职业.TextFontPassWordChar = '\0';
            this.txt职业.TextReadOnly = true;
            this.txt职业.Value = null;
            // 
            // txt劳动强度
            // 
            this.txt劳动强度.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt劳动强度, true);
            this.txt劳动强度.Label = "劳动强度:";
            this.txt劳动强度.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt劳动强度.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt劳动强度.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt劳动强度.LabelWidth = 100F;
            this.txt劳动强度.Location = new System.Drawing.Point(2, 268);
            this.txt劳动强度.Margin = new System.Windows.Forms.Padding(2);
            this.txt劳动强度.Name = "txt劳动强度";
            this.txt劳动强度.Size = new System.Drawing.Size(742, 26);
            this.txt劳动强度.TabIndex = 2;
            this.txt劳动强度.TextBackColor = System.Drawing.Color.White;
            this.txt劳动强度.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt劳动强度.TextFontPassWordChar = '\0';
            this.txt劳动强度.TextReadOnly = true;
            this.txt劳动强度.Value = null;
            // 
            // txt婚姻状况
            // 
            this.txt婚姻状况.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt婚姻状况, true);
            this.txt婚姻状况.Label = "婚姻状况:";
            this.txt婚姻状况.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt婚姻状况.LabelForeColor = System.Drawing.Color.Blue;
            this.txt婚姻状况.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt婚姻状况.LabelWidth = 100F;
            this.txt婚姻状况.Location = new System.Drawing.Point(2, 298);
            this.txt婚姻状况.Margin = new System.Windows.Forms.Padding(2, 2, 2, 0);
            this.txt婚姻状况.Name = "txt婚姻状况";
            this.txt婚姻状况.Size = new System.Drawing.Size(742, 26);
            this.txt婚姻状况.TabIndex = 2;
            this.txt婚姻状况.TextBackColor = System.Drawing.Color.White;
            this.txt婚姻状况.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt婚姻状况.TextFontPassWordChar = '\0';
            this.txt婚姻状况.TextReadOnly = true;
            this.txt婚姻状况.Value = null;
            // 
            // txt医疗费用支付方式
            // 
            this.txt医疗费用支付方式.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt医疗费用支付方式, true);
            this.txt医疗费用支付方式.Label = "医疗费用支付方式:";
            this.txt医疗费用支付方式.LabelFont = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt医疗费用支付方式.LabelForeColor = System.Drawing.Color.Blue;
            this.txt医疗费用支付方式.LabelTextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.txt医疗费用支付方式.LabelWidth = 100F;
            this.txt医疗费用支付方式.Location = new System.Drawing.Point(1, 324);
            this.txt医疗费用支付方式.Margin = new System.Windows.Forms.Padding(1, 0, 2, 0);
            this.txt医疗费用支付方式.Name = "txt医疗费用支付方式";
            this.txt医疗费用支付方式.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.txt医疗费用支付方式.Size = new System.Drawing.Size(742, 35);
            this.txt医疗费用支付方式.TabIndex = 2;
            this.txt医疗费用支付方式.TextBackColor = System.Drawing.Color.White;
            this.txt医疗费用支付方式.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt医疗费用支付方式.TextFontPassWordChar = '\0';
            this.txt医疗费用支付方式.TextReadOnly = true;
            this.txt医疗费用支付方式.Value = null;
            // 
            // txt医疗保险号
            // 
            this.txt医疗保险号.AllowMultiline = false;
            this.txt医疗保险号.Label = "医疗保险号:";
            this.txt医疗保险号.LabelFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt医疗保险号.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt医疗保险号.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt医疗保险号.LabelWidth = 100F;
            this.txt医疗保险号.Location = new System.Drawing.Point(2, 361);
            this.txt医疗保险号.Margin = new System.Windows.Forms.Padding(2);
            this.txt医疗保险号.Name = "txt医疗保险号";
            this.txt医疗保险号.Size = new System.Drawing.Size(298, 26);
            this.txt医疗保险号.TabIndex = 2;
            this.txt医疗保险号.TextBackColor = System.Drawing.Color.White;
            this.txt医疗保险号.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt医疗保险号.TextFontPassWordChar = '\0';
            this.txt医疗保险号.TextReadOnly = true;
            this.txt医疗保险号.Value = null;
            // 
            // txt新农合号
            // 
            this.txt新农合号.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt新农合号, true);
            this.txt新农合号.Label = "新农合号:";
            this.txt新农合号.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新农合号.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt新农合号.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt新农合号.LabelWidth = 100F;
            this.txt新农合号.Location = new System.Drawing.Point(304, 359);
            this.txt新农合号.Margin = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.txt新农合号.Name = "txt新农合号";
            this.txt新农合号.Size = new System.Drawing.Size(298, 26);
            this.txt新农合号.TabIndex = 2;
            this.txt新农合号.TextBackColor = System.Drawing.Color.White;
            this.txt新农合号.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt新农合号.TextFontPassWordChar = '\0';
            this.txt新农合号.TextReadOnly = true;
            this.txt新农合号.Value = null;
            // 
            // txt所属片区
            // 
            this.txt所属片区.AllowMultiline = false;
            this.txt所属片区.Label = "所属片区:";
            this.txt所属片区.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt所属片区.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt所属片区.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt所属片区.LabelWidth = 100F;
            this.txt所属片区.Location = new System.Drawing.Point(2, 391);
            this.txt所属片区.Margin = new System.Windows.Forms.Padding(2);
            this.txt所属片区.Name = "txt所属片区";
            this.txt所属片区.Size = new System.Drawing.Size(298, 26);
            this.txt所属片区.TabIndex = 2;
            this.txt所属片区.TextBackColor = System.Drawing.Color.White;
            this.txt所属片区.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt所属片区.TextFontPassWordChar = '\0';
            this.txt所属片区.TextReadOnly = true;
            this.txt所属片区.Value = null;
            // 
            // txt档案类别
            // 
            this.txt档案类别.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt档案类别, true);
            this.txt档案类别.Label = "档案类别:";
            this.txt档案类别.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt档案类别.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt档案类别.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt档案类别.LabelWidth = 100F;
            this.txt档案类别.Location = new System.Drawing.Point(304, 389);
            this.txt档案类别.Margin = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.txt档案类别.Name = "txt档案类别";
            this.txt档案类别.Size = new System.Drawing.Size(298, 26);
            this.txt档案类别.TabIndex = 2;
            this.txt档案类别.TextBackColor = System.Drawing.Color.White;
            this.txt档案类别.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt档案类别.TextFontPassWordChar = '\0';
            this.txt档案类别.TextReadOnly = true;
            this.txt档案类别.Value = null;
            // 
            // txt居住地址
            // 
            this.txt居住地址.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt居住地址, true);
            this.txt居住地址.Label = "居住地址:";
            this.txt居住地址.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt居住地址.LabelForeColor = System.Drawing.Color.Blue;
            this.txt居住地址.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt居住地址.LabelWidth = 100F;
            this.txt居住地址.Location = new System.Drawing.Point(2, 421);
            this.txt居住地址.Margin = new System.Windows.Forms.Padding(2);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Size = new System.Drawing.Size(742, 26);
            this.txt居住地址.TabIndex = 2;
            this.txt居住地址.TextBackColor = System.Drawing.Color.White;
            this.txt居住地址.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt居住地址.TextFontPassWordChar = '\0';
            this.txt居住地址.TextReadOnly = true;
            this.txt居住地址.Value = null;
            // 
            // txt药物过敏史
            // 
            this.txt药物过敏史.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt药物过敏史, true);
            this.txt药物过敏史.Label = "药物过敏史:";
            this.txt药物过敏史.LabelFont = new System.Drawing.Font("微软雅黑", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt药物过敏史.LabelForeColor = System.Drawing.Color.Blue;
            this.txt药物过敏史.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt药物过敏史.LabelWidth = 100F;
            this.txt药物过敏史.Location = new System.Drawing.Point(2, 451);
            this.txt药物过敏史.Margin = new System.Windows.Forms.Padding(2);
            this.txt药物过敏史.Name = "txt药物过敏史";
            this.txt药物过敏史.Size = new System.Drawing.Size(742, 26);
            this.txt药物过敏史.TabIndex = 2;
            this.txt药物过敏史.TextBackColor = System.Drawing.Color.White;
            this.txt药物过敏史.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt药物过敏史.TextFontPassWordChar = '\0';
            this.txt药物过敏史.TextReadOnly = true;
            this.txt药物过敏史.Value = null;
            // 
            // myPanel既往史
            // 
            this.myPanel既往史.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.myPanel既往史.Controls.Add(this.flowLayoutPanel2);
            this.myPanel既往史.Controls.Add(this.label1);
            this.myPanel既往史.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.SetFlowBreak(this.myPanel既往史, true);
            this.myPanel既往史.Location = new System.Drawing.Point(2, 481);
            this.myPanel既往史.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.myPanel既往史.Name = "myPanel既往史";
            this.myPanel既往史.Padding = new System.Windows.Forms.Padding(7, 1, 1, 1);
            this.myPanel既往史.Size = new System.Drawing.Size(742, 130);
            this.myPanel既往史.TabIndex = 5;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.txt输血);
            this.flowLayoutPanel2.Controls.Add(this.txt外伤);
            this.flowLayoutPanel2.Controls.Add(this.txt疾病);
            this.flowLayoutPanel2.Controls.Add(this.txt手术);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(29, 1);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel2.Size = new System.Drawing.Size(710, 126);
            this.flowLayoutPanel2.TabIndex = 5;
            // 
            // txt输血
            // 
            this.txt输血.AllowMultiline = false;
            this.txt输血.Label = "输血:";
            this.txt输血.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt输血.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt输血.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt输血.LabelWidth = 75F;
            this.txt输血.Location = new System.Drawing.Point(4, 4);
            this.txt输血.Margin = new System.Windows.Forms.Padding(2);
            this.txt输血.Name = "txt输血";
            this.txt输血.Size = new System.Drawing.Size(692, 26);
            this.txt输血.TabIndex = 2;
            this.txt输血.TextBackColor = System.Drawing.Color.White;
            this.txt输血.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt输血.TextFontPassWordChar = '\0';
            this.txt输血.TextReadOnly = true;
            this.txt输血.Value = null;
            // 
            // txt外伤
            // 
            this.txt外伤.AllowMultiline = false;
            this.txt外伤.Label = "外伤:";
            this.txt外伤.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt外伤.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt外伤.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt外伤.LabelWidth = 75F;
            this.txt外伤.Location = new System.Drawing.Point(4, 34);
            this.txt外伤.Margin = new System.Windows.Forms.Padding(2);
            this.txt外伤.Name = "txt外伤";
            this.txt外伤.Size = new System.Drawing.Size(692, 26);
            this.txt外伤.TabIndex = 2;
            this.txt外伤.TextBackColor = System.Drawing.Color.White;
            this.txt外伤.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt外伤.TextFontPassWordChar = '\0';
            this.txt外伤.TextReadOnly = true;
            this.txt外伤.Value = null;
            // 
            // txt疾病
            // 
            this.txt疾病.AllowMultiline = false;
            this.txt疾病.Label = "疾病:";
            this.txt疾病.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt疾病.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt疾病.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt疾病.LabelWidth = 75F;
            this.txt疾病.Location = new System.Drawing.Point(4, 64);
            this.txt疾病.Margin = new System.Windows.Forms.Padding(2);
            this.txt疾病.Name = "txt疾病";
            this.txt疾病.Size = new System.Drawing.Size(692, 26);
            this.txt疾病.TabIndex = 2;
            this.txt疾病.TextBackColor = System.Drawing.Color.White;
            this.txt疾病.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt疾病.TextFontPassWordChar = '\0';
            this.txt疾病.TextReadOnly = true;
            this.txt疾病.Value = null;
            // 
            // txt手术
            // 
            this.txt手术.AllowMultiline = false;
            this.txt手术.Label = "手术:";
            this.txt手术.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt手术.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt手术.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt手术.LabelWidth = 75F;
            this.txt手术.Location = new System.Drawing.Point(4, 94);
            this.txt手术.Margin = new System.Windows.Forms.Padding(2);
            this.txt手术.Name = "txt手术";
            this.txt手术.Size = new System.Drawing.Size(692, 26);
            this.txt手术.TabIndex = 2;
            this.txt手术.TextBackColor = System.Drawing.Color.White;
            this.txt手术.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt手术.TextFontPassWordChar = '\0';
            this.txt手术.TextReadOnly = true;
            this.txt手术.Value = null;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(7, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 126);
            this.label1.TabIndex = 4;
            this.label1.Text = "既往史";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt家族史
            // 
            this.txt家族史.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt家族史, true);
            this.txt家族史.Label = "家族史:";
            this.txt家族史.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt家族史.LabelForeColor = System.Drawing.Color.Blue;
            this.txt家族史.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt家族史.LabelWidth = 100F;
            this.txt家族史.Location = new System.Drawing.Point(2, 615);
            this.txt家族史.Margin = new System.Windows.Forms.Padding(2);
            this.txt家族史.Name = "txt家族史";
            this.txt家族史.Size = new System.Drawing.Size(726, 26);
            this.txt家族史.TabIndex = 2;
            this.txt家族史.TextBackColor = System.Drawing.Color.White;
            this.txt家族史.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt家族史.TextFontPassWordChar = '\0';
            this.txt家族史.TextReadOnly = true;
            this.txt家族史.Value = null;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.SetFlowBreak(this.label2, true);
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(2, 643);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(740, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "暴露史";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt化学品
            // 
            this.txt化学品.AllowMultiline = false;
            this.txt化学品.Label = "化学品:";
            this.txt化学品.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt化学品.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt化学品.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt化学品.LabelWidth = 100F;
            this.txt化学品.Location = new System.Drawing.Point(2, 663);
            this.txt化学品.Margin = new System.Windows.Forms.Padding(2);
            this.txt化学品.Name = "txt化学品";
            this.txt化学品.Size = new System.Drawing.Size(245, 26);
            this.txt化学品.TabIndex = 2;
            this.txt化学品.TextBackColor = System.Drawing.Color.White;
            this.txt化学品.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt化学品.TextFontPassWordChar = '\0';
            this.txt化学品.TextReadOnly = true;
            this.txt化学品.Value = null;
            // 
            // txt毒物
            // 
            this.txt毒物.AllowMultiline = false;
            this.txt毒物.Label = "毒物:";
            this.txt毒物.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt毒物.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt毒物.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt毒物.LabelWidth = 100F;
            this.txt毒物.Location = new System.Drawing.Point(251, 663);
            this.txt毒物.Margin = new System.Windows.Forms.Padding(2);
            this.txt毒物.Name = "txt毒物";
            this.txt毒物.Size = new System.Drawing.Size(245, 26);
            this.txt毒物.TabIndex = 2;
            this.txt毒物.TextBackColor = System.Drawing.Color.White;
            this.txt毒物.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt毒物.TextFontPassWordChar = '\0';
            this.txt毒物.TextReadOnly = true;
            this.txt毒物.Value = null;
            // 
            // txt射线
            // 
            this.txt射线.AllowMultiline = false;
            this.txt射线.Label = "射线:";
            this.txt射线.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt射线.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt射线.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt射线.LabelWidth = 100F;
            this.txt射线.Location = new System.Drawing.Point(500, 663);
            this.txt射线.Margin = new System.Windows.Forms.Padding(2);
            this.txt射线.Name = "txt射线";
            this.txt射线.Size = new System.Drawing.Size(245, 26);
            this.txt射线.TabIndex = 2;
            this.txt射线.TextBackColor = System.Drawing.Color.White;
            this.txt射线.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt射线.TextFontPassWordChar = '\0';
            this.txt射线.TextReadOnly = true;
            this.txt射线.Value = null;
            // 
            // txt遗传病史
            // 
            this.txt遗传病史.AllowMultiline = false;
            this.txt遗传病史.Label = "遗传病史:";
            this.txt遗传病史.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt遗传病史.LabelForeColor = System.Drawing.Color.Blue;
            this.txt遗传病史.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt遗传病史.LabelWidth = 100F;
            this.txt遗传病史.Location = new System.Drawing.Point(2, 693);
            this.txt遗传病史.Margin = new System.Windows.Forms.Padding(2);
            this.txt遗传病史.Name = "txt遗传病史";
            this.txt遗传病史.Size = new System.Drawing.Size(742, 26);
            this.txt遗传病史.TabIndex = 2;
            this.txt遗传病史.TextBackColor = System.Drawing.Color.White;
            this.txt遗传病史.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt遗传病史.TextFontPassWordChar = '\0';
            this.txt遗传病史.TextReadOnly = true;
            this.txt遗传病史.Value = null;
            // 
            // txt残疾情况
            // 
            this.txt残疾情况.AllowMultiline = false;
            this.txt残疾情况.Label = "残疾情况:";
            this.txt残疾情况.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt残疾情况.LabelForeColor = System.Drawing.Color.Blue;
            this.txt残疾情况.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt残疾情况.LabelWidth = 100F;
            this.txt残疾情况.Location = new System.Drawing.Point(2, 723);
            this.txt残疾情况.Margin = new System.Windows.Forms.Padding(2);
            this.txt残疾情况.Name = "txt残疾情况";
            this.txt残疾情况.Size = new System.Drawing.Size(742, 26);
            this.txt残疾情况.TabIndex = 2;
            this.txt残疾情况.TextBackColor = System.Drawing.Color.White;
            this.txt残疾情况.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt残疾情况.TextFontPassWordChar = '\0';
            this.txt残疾情况.TextReadOnly = true;
            this.txt残疾情况.Value = null;
            // 
            // txt孕产情况
            // 
            this.txt孕产情况.AllowMultiline = false;
            this.txt孕产情况.Label = "孕产情况:";
            this.txt孕产情况.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt孕产情况.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt孕产情况.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt孕产情况.LabelWidth = 100F;
            this.txt孕产情况.Location = new System.Drawing.Point(2, 753);
            this.txt孕产情况.Margin = new System.Windows.Forms.Padding(2);
            this.txt孕产情况.Name = "txt孕产情况";
            this.txt孕产情况.Size = new System.Drawing.Size(245, 26);
            this.txt孕产情况.TabIndex = 7;
            this.txt孕产情况.TextBackColor = System.Drawing.Color.White;
            this.txt孕产情况.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt孕产情况.TextFontPassWordChar = '\0';
            this.txt孕产情况.TextReadOnly = true;
            this.txt孕产情况.Value = null;
            // 
            // txt孕次
            // 
            this.txt孕次.AllowMultiline = false;
            this.txt孕次.Label = "孕次:";
            this.txt孕次.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt孕次.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt孕次.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt孕次.LabelWidth = 100F;
            this.txt孕次.Location = new System.Drawing.Point(251, 753);
            this.txt孕次.Margin = new System.Windows.Forms.Padding(2);
            this.txt孕次.Name = "txt孕次";
            this.txt孕次.Size = new System.Drawing.Size(245, 26);
            this.txt孕次.TabIndex = 8;
            this.txt孕次.TextBackColor = System.Drawing.Color.White;
            this.txt孕次.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt孕次.TextFontPassWordChar = '\0';
            this.txt孕次.TextReadOnly = true;
            this.txt孕次.Value = null;
            // 
            // txt产次
            // 
            this.txt产次.AllowMultiline = false;
            this.txt产次.Label = "产次:";
            this.txt产次.LabelFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt产次.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt产次.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt产次.LabelWidth = 100F;
            this.txt产次.Location = new System.Drawing.Point(500, 753);
            this.txt产次.Margin = new System.Windows.Forms.Padding(2);
            this.txt产次.Name = "txt产次";
            this.txt产次.Size = new System.Drawing.Size(245, 26);
            this.txt产次.TabIndex = 9;
            this.txt产次.TextBackColor = System.Drawing.Color.White;
            this.txt产次.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt产次.TextFontPassWordChar = '\0';
            this.txt产次.TextReadOnly = true;
            this.txt产次.Value = null;
            // 
            // myPanel1
            // 
            this.myPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.myPanel1.Controls.Add(this.flowLayoutPanel3);
            this.myPanel1.Controls.Add(this.label3);
            this.myPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.SetFlowBreak(this.myPanel1, true);
            this.myPanel1.Location = new System.Drawing.Point(2, 783);
            this.myPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.myPanel1.Name = "myPanel1";
            this.myPanel1.Padding = new System.Windows.Forms.Padding(7, 1, 1, 1);
            this.myPanel1.Size = new System.Drawing.Size(742, 163);
            this.myPanel1.TabIndex = 10;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.txt厨房排风设施);
            this.flowLayoutPanel3.Controls.Add(this.txt燃料类型);
            this.flowLayoutPanel3.Controls.Add(this.txt饮水);
            this.flowLayoutPanel3.Controls.Add(this.txt厕所);
            this.flowLayoutPanel3.Controls.Add(this.txt禽畜栏);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(29, 1);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Padding = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel3.Size = new System.Drawing.Size(710, 159);
            this.flowLayoutPanel3.TabIndex = 5;
            // 
            // txt厨房排风设施
            // 
            this.txt厨房排风设施.AllowMultiline = false;
            this.txt厨房排风设施.Label = "厨房排风设施:";
            this.txt厨房排风设施.LabelFont = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt厨房排风设施.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt厨房排风设施.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt厨房排风设施.LabelWidth = 100F;
            this.txt厨房排风设施.Location = new System.Drawing.Point(4, 4);
            this.txt厨房排风设施.Margin = new System.Windows.Forms.Padding(2);
            this.txt厨房排风设施.Name = "txt厨房排风设施";
            this.txt厨房排风设施.Size = new System.Drawing.Size(692, 26);
            this.txt厨房排风设施.TabIndex = 2;
            this.txt厨房排风设施.TextBackColor = System.Drawing.Color.White;
            this.txt厨房排风设施.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt厨房排风设施.TextFontPassWordChar = '\0';
            this.txt厨房排风设施.TextReadOnly = true;
            this.txt厨房排风设施.Value = null;
            // 
            // txt燃料类型
            // 
            this.txt燃料类型.AllowMultiline = false;
            this.txt燃料类型.Label = "燃料类型:";
            this.txt燃料类型.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt燃料类型.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt燃料类型.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt燃料类型.LabelWidth = 100F;
            this.txt燃料类型.Location = new System.Drawing.Point(4, 34);
            this.txt燃料类型.Margin = new System.Windows.Forms.Padding(2);
            this.txt燃料类型.Name = "txt燃料类型";
            this.txt燃料类型.Size = new System.Drawing.Size(692, 26);
            this.txt燃料类型.TabIndex = 2;
            this.txt燃料类型.TextBackColor = System.Drawing.Color.White;
            this.txt燃料类型.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt燃料类型.TextFontPassWordChar = '\0';
            this.txt燃料类型.TextReadOnly = true;
            this.txt燃料类型.Value = null;
            // 
            // txt饮水
            // 
            this.txt饮水.AllowMultiline = false;
            this.txt饮水.Label = "饮水:";
            this.txt饮水.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt饮水.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt饮水.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt饮水.LabelWidth = 100F;
            this.txt饮水.Location = new System.Drawing.Point(4, 64);
            this.txt饮水.Margin = new System.Windows.Forms.Padding(2);
            this.txt饮水.Name = "txt饮水";
            this.txt饮水.Size = new System.Drawing.Size(692, 26);
            this.txt饮水.TabIndex = 2;
            this.txt饮水.TextBackColor = System.Drawing.Color.White;
            this.txt饮水.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt饮水.TextFontPassWordChar = '\0';
            this.txt饮水.TextReadOnly = true;
            this.txt饮水.Value = null;
            // 
            // txt厕所
            // 
            this.txt厕所.AllowMultiline = false;
            this.txt厕所.Label = "厕所:";
            this.txt厕所.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt厕所.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt厕所.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt厕所.LabelWidth = 100F;
            this.txt厕所.Location = new System.Drawing.Point(4, 94);
            this.txt厕所.Margin = new System.Windows.Forms.Padding(2);
            this.txt厕所.Name = "txt厕所";
            this.txt厕所.Size = new System.Drawing.Size(692, 26);
            this.txt厕所.TabIndex = 2;
            this.txt厕所.TextBackColor = System.Drawing.Color.White;
            this.txt厕所.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt厕所.TextFontPassWordChar = '\0';
            this.txt厕所.TextReadOnly = true;
            this.txt厕所.Value = null;
            // 
            // txt禽畜栏
            // 
            this.txt禽畜栏.AllowMultiline = false;
            this.txt禽畜栏.Label = "禽畜栏:";
            this.txt禽畜栏.LabelFont = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt禽畜栏.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt禽畜栏.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt禽畜栏.LabelWidth = 100F;
            this.txt禽畜栏.Location = new System.Drawing.Point(4, 124);
            this.txt禽畜栏.Margin = new System.Windows.Forms.Padding(2);
            this.txt禽畜栏.Name = "txt禽畜栏";
            this.txt禽畜栏.Size = new System.Drawing.Size(692, 26);
            this.txt禽畜栏.TabIndex = 2;
            this.txt禽畜栏.TextBackColor = System.Drawing.Color.White;
            this.txt禽畜栏.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt禽畜栏.TextFontPassWordChar = '\0';
            this.txt禽畜栏.TextReadOnly = true;
            this.txt禽畜栏.Value = null;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(7, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 159);
            this.label3.TabIndex = 4;
            this.label3.Text = "生活环境";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt调查时间
            // 
            this.txt调查时间.AllowMultiline = false;
            this.txt调查时间.Label = "调查时间:";
            this.txt调查时间.LabelFont = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt调查时间.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt调查时间.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt调查时间.LabelWidth = 100F;
            this.txt调查时间.Location = new System.Drawing.Point(2, 950);
            this.txt调查时间.Margin = new System.Windows.Forms.Padding(2);
            this.txt调查时间.Name = "txt调查时间";
            this.txt调查时间.Size = new System.Drawing.Size(245, 26);
            this.txt调查时间.TabIndex = 11;
            this.txt调查时间.TextBackColor = System.Drawing.SystemColors.Control;
            this.txt调查时间.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt调查时间.TextFontPassWordChar = '\0';
            this.txt调查时间.TextReadOnly = true;
            this.txt调查时间.Value = null;
            // 
            // txt录入时间
            // 
            this.txt录入时间.AllowMultiline = false;
            this.txt录入时间.Label = "录入时间:";
            this.txt录入时间.LabelFont = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt录入时间.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt录入时间.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt录入时间.LabelWidth = 100F;
            this.txt录入时间.Location = new System.Drawing.Point(251, 950);
            this.txt录入时间.Margin = new System.Windows.Forms.Padding(2);
            this.txt录入时间.Name = "txt录入时间";
            this.txt录入时间.Size = new System.Drawing.Size(245, 26);
            this.txt录入时间.TabIndex = 12;
            this.txt录入时间.TextBackColor = System.Drawing.SystemColors.Control;
            this.txt录入时间.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt录入时间.TextFontPassWordChar = '\0';
            this.txt录入时间.TextReadOnly = true;
            this.txt录入时间.Value = null;
            // 
            // txt最近更新时间
            // 
            this.txt最近更新时间.AllowMultiline = false;
            this.flowLayoutPanel1.SetFlowBreak(this.txt最近更新时间, true);
            this.txt最近更新时间.Label = "更新时间:";
            this.txt最近更新时间.LabelFont = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt最近更新时间.LabelForeColor = System.Drawing.SystemColors.ControlText;
            this.txt最近更新时间.LabelTextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txt最近更新时间.LabelWidth = 100F;
            this.txt最近更新时间.Location = new System.Drawing.Point(500, 950);
            this.txt最近更新时间.Margin = new System.Windows.Forms.Padding(2);
            this.txt最近更新时间.Name = "txt最近更新时间";
            this.txt最近更新时间.Size = new System.Drawing.Size(245, 26);
            this.txt最近更新时间.TabIndex = 13;
            this.txt最近更新时间.TextBackColor = System.Drawing.SystemColors.Control;
            this.txt最近更新时间.TextFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt最近更新时间.TextFontPassWordChar = '\0';
            this.txt最近更新时间.TextReadOnly = true;
            this.txt最近更新时间.Value = null;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 978);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 12);
            this.label4.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(774, 26);
            this.label5.TabIndex = 4;
            this.label5.Text = "个人基本信息";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Interface个人档案
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 614);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label5);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Interface个人档案";
            this.Text = "Interface个人档案";
            this.Load += new System.EventHandler(this.Interface个人档案_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.myPanel既往史.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.myPanel1.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Controls.CDisplayBox txt档案号;
        private Controls.CDisplayBox txt姓名;
        private Controls.CDisplayBox txt性别;
        private Controls.CDisplayBox txt出生日期;
        private Controls.CDisplayBox txt档案状态;
        private Controls.CDisplayBox txt缺项;
        private Controls.CDisplayBox txt完整度;
        private Controls.CDisplayBox txt证件号码;
        private Controls.CDisplayBox txt工作单位;
        private Controls.CDisplayBox txt本人电话;
        private Controls.CDisplayBox txt联系人姓名;
        private Controls.CDisplayBox txt联系人电话;
        private Controls.CDisplayBox txt常住类型;
        private Controls.CDisplayBox txt民族;
        private Controls.CDisplayBox txt与户主关系;
        private Controls.CDisplayBox txt血型;
        private Controls.CDisplayBox txtRH;
        private Controls.CDisplayBox txt文化程度;
        private Controls.CDisplayBox txt职业;
        private Controls.CDisplayBox txt劳动强度;
        private Controls.CDisplayBox txt婚姻状况;
        private Controls.CDisplayBox txt医疗费用支付方式;
        private Controls.CDisplayBox txt医疗保险号;
        private Controls.CDisplayBox txt新农合号;
        private Controls.CDisplayBox txt所属片区;
        private Controls.CDisplayBox txt档案类别;
        private Controls.CDisplayBox txt居住地址;
        private Controls.CDisplayBox txt药物过敏史;
        private MySkin.Controls.MyPanel myPanel既往史;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private Controls.CDisplayBox txt输血;
        private Controls.CDisplayBox txt外伤;
        private Controls.CDisplayBox txt疾病;
        private Controls.CDisplayBox txt手术;
        private System.Windows.Forms.Label label1;
        private Controls.CDisplayBox txt家族史;
        private System.Windows.Forms.Label label2;
        private Controls.CDisplayBox txt化学品;
        private Controls.CDisplayBox txt毒物;
        private Controls.CDisplayBox txt射线;
        private Controls.CDisplayBox txt遗传病史;
        private Controls.CDisplayBox txt残疾情况;
        private Controls.CDisplayBox txt孕产情况;
        private Controls.CDisplayBox txt孕次;
        private Controls.CDisplayBox txt产次;
        private MySkin.Controls.MyPanel myPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private Controls.CDisplayBox txt厨房排风设施;
        private Controls.CDisplayBox txt燃料类型;
        private Controls.CDisplayBox txt饮水;
        private Controls.CDisplayBox txt厕所;
        private Controls.CDisplayBox txt禽畜栏;
        private System.Windows.Forms.Label label3;
        private Controls.CDisplayBox txt调查时间;
        private Controls.CDisplayBox txt录入时间;
        private Controls.CDisplayBox txt最近更新时间;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;

    }
}