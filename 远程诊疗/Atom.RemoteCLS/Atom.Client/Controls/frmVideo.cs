﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client.Controls
{
    public partial class frmVideo : Form
    {
        public frmVideo()
        {
            InitializeComponent();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            this.TopMost = false ;
            this.Close();
        }

        private void frmVideo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
            {
                this.TopMost = false;
                this.Close();
            }
        }

        private void frmVideo_Resize(object sender, EventArgs e)
        {
            picLocal.Left = this.Width - picLocal.Width;
            picLocal.Top = this.Height - picLocal.Height;
        }
 
    }
}
