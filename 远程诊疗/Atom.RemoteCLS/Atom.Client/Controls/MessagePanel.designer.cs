﻿namespace Atom.Controls
{
    partial class MessagePanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessagePanel));
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemCopy2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemSelAll2 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSend = new IMLibrary4.MyExtRichTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemPaset = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemDel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemSelAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsButSetFont = new System.Windows.Forms.ToolStripButton();
            this.tsButColor = new System.Windows.Forms.ToolStripButton();
            this.tsButFace = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsButCaptureImageTool = new System.Windows.Forms.ToolStripButton();
            this.tsButAway = new System.Windows.Forms.ToolStripButton();
            this.txtRecord = new IMLibrary4.MyExtRichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemCopy2,
            this.toolStripSeparator1,
            this.MenuItemSelAll2});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(136, 62);
            // 
            // MenuItemCopy2
            // 
            this.MenuItemCopy2.Name = "MenuItemCopy2";
            this.MenuItemCopy2.Size = new System.Drawing.Size(135, 26);
            this.MenuItemCopy2.Text = "复制(&C)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(132, 6);
            // 
            // MenuItemSelAll2
            // 
            this.MenuItemSelAll2.Name = "MenuItemSelAll2";
            this.MenuItemSelAll2.Size = new System.Drawing.Size(135, 26);
            this.MenuItemSelAll2.Text = "全选(&A)";
            // 
            // txtSend
            // 
            this.txtSend.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSend.ContextMenuStrip = this.contextMenuStrip1;
            this.txtSend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtSend.Location = new System.Drawing.Point(0, 230);
            this.txtSend.Margin = new System.Windows.Forms.Padding(4);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(397, 115);
            this.txtSend.TabIndex = 0;
            this.txtSend.Text = "";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemCopy,
            this.MenuItemPaset,
            this.MenuItemCut,
            this.MenuItemDel,
            this.toolStripMenuItem1,
            this.MenuItemSelAll});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(136, 140);
            // 
            // MenuItemCopy
            // 
            this.MenuItemCopy.Name = "MenuItemCopy";
            this.MenuItemCopy.Size = new System.Drawing.Size(135, 26);
            this.MenuItemCopy.Text = "复制(&C)";
            // 
            // MenuItemPaset
            // 
            this.MenuItemPaset.Name = "MenuItemPaset";
            this.MenuItemPaset.Size = new System.Drawing.Size(135, 26);
            this.MenuItemPaset.Text = "粘贴(&P)";
            // 
            // MenuItemCut
            // 
            this.MenuItemCut.Name = "MenuItemCut";
            this.MenuItemCut.Size = new System.Drawing.Size(135, 26);
            this.MenuItemCut.Text = "剪切(&T)";
            // 
            // MenuItemDel
            // 
            this.MenuItemDel.Name = "MenuItemDel";
            this.MenuItemDel.Size = new System.Drawing.Size(135, 26);
            this.MenuItemDel.Text = "删除(&D)";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(132, 6);
            // 
            // MenuItemSelAll
            // 
            this.MenuItemSelAll.Name = "MenuItemSelAll";
            this.MenuItemSelAll.Size = new System.Drawing.Size(135, 26);
            this.MenuItemSelAll.Text = "全选(&A)";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsButSetFont,
            this.tsButColor,
            this.tsButFace,
            this.tsButCaptureImageTool,
            this.tsButAway});
            this.toolStrip2.Location = new System.Drawing.Point(0, 203);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip2.Size = new System.Drawing.Size(397, 27);
            this.toolStrip2.TabIndex = 21;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsButSetFont
            // 
            this.tsButSetFont.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButSetFont.Image = ((System.Drawing.Image)(resources.GetObject("tsButSetFont.Image")));
            this.tsButSetFont.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButSetFont.Name = "tsButSetFont";
            this.tsButSetFont.Size = new System.Drawing.Size(24, 24);
            this.tsButSetFont.Text = "字体设置";
            // 
            // tsButColor
            // 
            this.tsButColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButColor.Image = ((System.Drawing.Image)(resources.GetObject("tsButColor.Image")));
            this.tsButColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButColor.Name = "tsButColor";
            this.tsButColor.Size = new System.Drawing.Size(24, 24);
            this.tsButColor.Text = "字体颜色";
            // 
            // tsButFace
            // 
            this.tsButFace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButFace.Image = ((System.Drawing.Image)(resources.GetObject("tsButFace.Image")));
            this.tsButFace.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButFace.Name = "tsButFace";
            this.tsButFace.ShowDropDownArrow = false;
            this.tsButFace.Size = new System.Drawing.Size(24, 24);
            this.tsButFace.Text = "插入表情";
            this.tsButFace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsButCaptureImageTool
            // 
            this.tsButCaptureImageTool.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButCaptureImageTool.Image = ((System.Drawing.Image)(resources.GetObject("tsButCaptureImageTool.Image")));
            this.tsButCaptureImageTool.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButCaptureImageTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButCaptureImageTool.Name = "tsButCaptureImageTool";
            this.tsButCaptureImageTool.Size = new System.Drawing.Size(24, 24);
            this.tsButCaptureImageTool.Text = "插入截图";
            this.tsButCaptureImageTool.Visible = false;
            // 
            // tsButAway
            // 
            this.tsButAway.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsButAway.Image = ((System.Drawing.Image)(resources.GetObject("tsButAway.Image")));
            this.tsButAway.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButAway.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButAway.Name = "tsButAway";
            this.tsButAway.Size = new System.Drawing.Size(84, 24);
            this.tsButAway.Text = "消息记录";
            // 
            // txtRecord
            // 
            this.txtRecord.BackColor = System.Drawing.SystemColors.Window;
            this.txtRecord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRecord.ContextMenuStrip = this.contextMenuStrip2;
            this.txtRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRecord.Location = new System.Drawing.Point(0, 31);
            this.txtRecord.Margin = new System.Windows.Forms.Padding(4);
            this.txtRecord.Name = "txtRecord";
            this.txtRecord.ReadOnly = true;
            this.txtRecord.Size = new System.Drawing.Size(397, 172);
            this.txtRecord.TabIndex = 22;
            this.txtRecord.Text = "";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(397, 31);
            this.label1.TabIndex = 23;
            this.label1.Text = "对话记录";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MessagePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtRecord);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.txtSend);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MessagePanel";
            this.Size = new System.Drawing.Size(397, 345);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsButSetFont;
        private System.Windows.Forms.ToolStripButton tsButColor;
        private System.Windows.Forms.ToolStripDropDownButton tsButFace;
        private System.Windows.Forms.ToolStripButton tsButCaptureImageTool;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCopy2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSelAll2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCopy;
        private System.Windows.Forms.ToolStripMenuItem MenuItemPaset;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCut;
        private System.Windows.Forms.ToolStripMenuItem MenuItemDel;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSelAll;
        public IMLibrary4.MyExtRichTextBox txtSend;
        private System.Windows.Forms.ToolStripButton tsButAway;
        private IMLibrary4.MyExtRichTextBox txtRecord;
        private System.Windows.Forms.Label label1;
    }
}
