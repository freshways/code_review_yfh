﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Net;
using IMLibrary4;
using IMLibrary4.AV;
using IMLibrary4.Protocol;
using IMLibrary4.Net;
using Atom.Client;

namespace Atom.Client.Controls
{
    public partial class video : UserControl 
    {
        public video()
        {
            InitializeComponent();
            butReceive.Click +=(butReceive_Click);
            butCancel.Click += (butCancel_Click);
            picRemote.MouseDoubleClick += (picRemote_DoubleClick);
        }

        /// <summary>
        /// 通信组对像
        /// </summary>
        private IMLibrary4.AV.AVComponent AVC = null;

        frmVideo frmV = null;

        //IPEndPoint AVTransmitServerEP = new IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), Global.ServerMsgPort);

        #region 事件
        /// <summary>
        /// 获得IPEndPoint事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="local">本地主机信息</param>
        /// <param name="remote">远程主机信息</param>
        public delegate void GetIPEndPointEventHandler(object sender, RtpEventArgs e);
        /// <summary>
        /// 获得IPEndPoint事件
        /// </summary>
        public event GetIPEndPointEventHandler GetIPEndPoint;
        protected virtual void OnGetIPEndPoint(object sender, RtpEventArgs e)
        {
            if (GetIPEndPoint != null)
                GetIPEndPoint(this, e);//触发获取本机主机事件
        } 

        public delegate void CancelEventHandler(object sender);
        /// <summary>
        /// 取消事件
        /// </summary>
        public event CancelEventHandler Cancel; 
        #endregion

        #region 属性
        private bool _IsInvite = false;
        /// <summary>
        /// 是否邀请方（请求方）
        /// </summary>
        public bool IsInvite
        {
            set
            {
                _IsInvite = value;
                butReceive.Visible = !value;
            }
            get { return _IsInvite; }
        }
        #endregion

        #region 初始化本地音视频
        private void IniLocalVideo(System.Net.IPEndPoint ServerEP)
        {
            if (AVC == null)
            {
                AVC = new IMLibrary4.AV.AVComponent(ServerEP);
                AVC.GetIPEndPoint += (sender, e) =>
                {
                    OnGetIPEndPoint(this, e);
                }; 
                AVC.TransmitConnected += (sender, connectedType) =>
                {
                    Console.WriteLine("已经联接：" + connectedType.ToString());
                };
                AVC.iniAV(picLocal, picRemote);
            }
        } 
        #endregion

        #region 设置对方远程主机信息(实现任何类型的NAT穿越,实现P2P通信)
        /// <summary>
        /// 设置对方远程主机信息(实现任何类型的NAT穿越,实现P2P通信)
        /// </summary>
        ///<param name="msg"></param>
        public void SetRometEP(AVMsg msg)
        {
            if (msg.type == type.set)
            {
                IniLocalVideo(Global.AVTransmitServerEP);//设置服务器主机信息并开始服务
                AVC.setRemoteIP(msg.LocalIP, msg.RemoteIP, msg.LocalRtpPort, msg.LocalRtcpPort, msg.RemoteRtpPort, msg.RemoteRtcpPort);//设置AV通信组件远程主机信息
            } 
        }
        #endregion

        #region 取消视频
        /// <summary>
        /// 取消视频
        /// </summary>
        public void CancelAV()
        {
            if (frmV != null && !frmV.IsDisposed)
            {
                frmV.Close();
                frmV.Dispose();
                frmV = null;
            }

            if (AVC != null)
            {
                AVC.Close();
                AVC.Dispose();
                AVC = null;
            }
        }
        #endregion

        #region 事件响应代码

        //开始视频
        void butReceive_Click(object sender, EventArgs e)
        {
            butReceive.Visible = false;
            IniLocalVideo(Global.AVTransmitServerEP);
        }
      

        // 取消视频
        void butCancel_Click(object sender, EventArgs e)
        {
            CancelAV();
            if (Cancel != null)
                Cancel(this);
        }

        //视频全屏
        private void picRemote_DoubleClick(object sender, EventArgs e)
        {
            if (frmV == null || frmV.IsDisposed)
                frmV = new frmVideo();
            else
                return;

            frmV.FormClosed += (s, ev) =>
            {
                if (AVC != null)
                {
                    AVC.picLocal = this.picLocal;
                    AVC.picRemote = this.picRemote;
                }
                frmV.Dispose();
            };

            if (AVC != null)
            {
                AVC.picLocal = frmV.picLocal;
                AVC.picRemote = frmV.picRemote;
            }
            frmV.Show(this);
        }

        #endregion
    }
}
