﻿namespace Atom.Client.Controls
{
    partial class sys_userlist
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            MySkin.Controls.MyListBoxItem myListBoxItem2 = new MySkin.Controls.MyListBoxItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem3 = new MySkin.Controls.MyListBoxSubItem();
            MySkin.Controls.MyListBoxSubItem myListBoxSubItem4 = new MySkin.Controls.MyListBoxSubItem();
            this.myListBox1 = new MySkin.Controls.MyListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // myListBox1
            // 
            this.myListBox1.BackColor = System.Drawing.Color.Transparent;
            this.myListBox1.Font = new System.Drawing.Font("宋体", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.myListBox1.ForeColor = System.Drawing.Color.Black;
            this.myListBox1.ItemColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.myListBox1.ItemMouseOnColor = System.Drawing.Color.LightSkyBlue;
            myListBoxItem2.IsOpen = true;
            myListBoxSubItem3.DisplayName = "内科主任";
            myListBoxSubItem3.HeadImage = global::Atom.Client.Properties.Resources.defaultPhoto;
            myListBoxSubItem3.ID = 10004;
            myListBoxSubItem3.IpAddress = null;
            myListBoxSubItem3.IsTwinkle = false;
            myListBoxSubItem3.NicName = "内科主任";
            myListBoxSubItem3.PersonalMsg = "心情&消息";
            myListBoxSubItem3.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.Online;
            myListBoxSubItem4.DisplayName = "外科主任";
            myListBoxSubItem4.HeadImage = global::Atom.Client.Properties.Resources.defaultPhoto;
            myListBoxSubItem4.ID = 20038;
            myListBoxSubItem4.IpAddress = null;
            myListBoxSubItem4.IsTwinkle = false;
            myListBoxSubItem4.NicName = "昵称";
            myListBoxSubItem4.PersonalMsg = "心情&消息";
            myListBoxSubItem4.Status = MySkin.Controls.MyListBoxSubItem.UserStatus.OffLine;
            myListBoxItem2.SubItems.AddRange(new MySkin.Controls.MyListBoxSubItem[] {
            myListBoxSubItem3,
            myListBoxSubItem4});
            myListBoxItem2.Text = "县医院";
            this.myListBox1.Items.AddRange(new MySkin.Controls.MyListBoxItem[] {
            myListBoxItem2});
            this.myListBox1.Location = new System.Drawing.Point(5, 128);
            this.myListBox1.Name = "myListBox1";
            this.myListBox1.ScrollArrowBackColor = System.Drawing.Color.Silver;
            this.myListBox1.ScrollBackColor = System.Drawing.Color.Transparent;
            this.myListBox1.ScrollSliderDefaultColor = System.Drawing.Color.LightGray;
            this.myListBox1.ScrollSliderDownColor = System.Drawing.Color.Gainsboro;
            this.myListBox1.Size = new System.Drawing.Size(300, 325);
            this.myListBox1.SubItemColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.myListBox1.SubItemSelectColor = System.Drawing.Color.DeepSkyBlue;
            this.myListBox1.TabIndex = 5;
            this.myListBox1.Text = "myListBox1";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(5, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(300, 65);
            this.label3.TabIndex = 6;
            this.label3.Text = "在线医生";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Atom.Client.Properties.Resources.Offline;
            this.pictureBox2.Location = new System.Drawing.Point(185, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(120, 59);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(65, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 59);
            this.label1.TabIndex = 10;
            this.label1.Text = "用户名";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Atom.Client.Properties.Resources.placeholder_person_ico;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(57, 59);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // sys_userlist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.myListBox1);
            this.Controls.Add(this.label3);
            this.Name = "sys_userlist";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Size = new System.Drawing.Size(310, 467);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MySkin.Controls.MyListBox myListBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
