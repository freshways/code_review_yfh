﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client.Controls
{
    public partial class sys_TitleBar : UserControl
    {
        public sys_TitleBar()
        {
            InitializeComponent();
        }

        private void picMin_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Form F = (System.Windows.Forms.Form)this.Parent;
                F.WindowState = FormWindowState.Minimized;
            }
            catch (Exception)
            {

            }
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Form F = (System.Windows.Forms.Form)this.Parent;
                F.Close();
                Application.Exit();
            }
            catch (Exception)
            {
            }

        }


    }
}
