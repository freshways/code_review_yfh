﻿namespace Atom.Client.Controls
{
    partial class sys_TitleBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.picHome = new System.Windows.Forms.PictureBox();
            this.lbTimeing = new System.Windows.Forms.Label();
            this.picSetting = new System.Windows.Forms.PictureBox();
            this.picMin = new System.Windows.Forms.PictureBox();
            this.picClose = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            this.SuspendLayout();
            // 
            // picHome
            // 
            this.picHome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picHome.Image = global::Atom.Client.Properties.Resources.home;
            this.picHome.Location = new System.Drawing.Point(437, 3);
            this.picHome.Name = "picHome";
            this.picHome.Size = new System.Drawing.Size(60, 55);
            this.picHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picHome.TabIndex = 0;
            this.picHome.TabStop = false;
            // 
            // lbTimeing
            // 
            this.lbTimeing.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTimeing.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbTimeing.ForeColor = System.Drawing.Color.White;
            this.lbTimeing.Location = new System.Drawing.Point(85, 3);
            this.lbTimeing.Name = "lbTimeing";
            this.lbTimeing.Size = new System.Drawing.Size(346, 54);
            this.lbTimeing.TabIndex = 1;
            this.lbTimeing.Text = "2016年6月16日 15:02:19";
            this.lbTimeing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picSetting
            // 
            this.picSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picSetting.Image = global::Atom.Client.Properties.Resources.docsetting;
            this.picSetting.Location = new System.Drawing.Point(506, 3);
            this.picSetting.Name = "picSetting";
            this.picSetting.Size = new System.Drawing.Size(60, 55);
            this.picSetting.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picSetting.TabIndex = 0;
            this.picSetting.TabStop = false;
            // 
            // picMin
            // 
            this.picMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picMin.Image = global::Atom.Client.Properties.Resources.最小化_03;
            this.picMin.Location = new System.Drawing.Point(575, 3);
            this.picMin.Name = "picMin";
            this.picMin.Size = new System.Drawing.Size(60, 55);
            this.picMin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMin.TabIndex = 0;
            this.picMin.TabStop = false;
            this.picMin.Click += new System.EventHandler(this.picMin_Click);
            // 
            // picClose
            // 
            this.picClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picClose.Image = global::Atom.Client.Properties.Resources.关闭_03;
            this.picClose.Location = new System.Drawing.Point(644, 3);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(60, 55);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picClose.TabIndex = 0;
            this.picClose.TabStop = false;
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // sys_TitleBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lbTimeing);
            this.Controls.Add(this.picClose);
            this.Controls.Add(this.picMin);
            this.Controls.Add(this.picSetting);
            this.Controls.Add(this.picHome);
            this.MinimumSize = new System.Drawing.Size(700, 60);
            this.Name = "sys_TitleBar";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Size = new System.Drawing.Size(705, 60);
            ((System.ComponentModel.ISupportInitialize)(this.picHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picHome;
        private System.Windows.Forms.Label lbTimeing;
        private System.Windows.Forms.PictureBox picSetting;
        private System.Windows.Forms.PictureBox picMin;
        private System.Windows.Forms.PictureBox picClose;
    }
}
