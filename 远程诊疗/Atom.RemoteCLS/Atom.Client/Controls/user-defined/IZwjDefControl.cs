﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atom.Client.Controls
{
    public interface IZwjDefControl
    {
        string Name
        { get; set; }

        bool Enabled
        { get; set; }

    }
}
