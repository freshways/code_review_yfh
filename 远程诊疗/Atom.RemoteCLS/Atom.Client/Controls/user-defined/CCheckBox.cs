﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client.Controls
{
    public partial class CCheckBox : UserControl,IZwjDefControl
    {
        public CCheckBox()
        {
            InitializeComponent();
        }

        [Browsable(true)]
        [Description("设置勾选框的值")]
        public bool? Value
        {
            get
            {
                return checkBox1.Checked;
            }
            set
            {
                checkBox1.Checked = value ?? false;
            }
        }

        [Browsable(true)]
        [Description("设置标签的值")]
        public string Label
        {
            get
            {
                return label1.Text;
            }
            set
            {
                label1.Text = value;
            }
        }


    }
}
