﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Atom.Client.Controls
{
    public partial class CDisplayBox : UserControl
    {
        public CDisplayBox()
        {
            InitializeComponent();
            tableLayoutPanel1.GetType().GetProperty("DoubleBuffered",
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(tableLayoutPanel1,
                true, null);
        }

        #region textBoxSettings
        [Browsable(true)]
        [Description("设置显示的值")]
        public string Text
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        [Browsable(true)]
        [Description("设置实际的值")]
        public string Value
        {
            get;
            set;
        }

        [Browsable(true)]
        [Description("设置是否允许多行")]
        public bool AllowMultiline
        {
            get
            {
                return textBox1.Multiline;
            }
            set
            {
                textBox1.Multiline = value;
                if (textBox1.Multiline)
                {
                    textBox1.ScrollBars = ScrollBars.Vertical;
                }
            }
        }

        [Browsable(true)]
        [Description("Text背景色")]
        public Color TextBackColor
        {
            get
            {
                return textBox1.BackColor;
            }
            set
            {
                textBox1.BackColor = value;
            }
        }

        [Browsable(true)]
        [Description("设置字体")]
        public Font TextFont
        {
            get
            {
                return textBox1.Font;
            }
            set
            {
                textBox1.Font = value;
            }
        }

        [Browsable(true)]
        [Description("")]
        public char TextFontPassWordChar
        {
            get {
                return this.textBox1.PasswordChar;
            }
            set {
                this.textBox1.PasswordChar = value;
            }
        }

        [Description("")]
        public bool TextReadOnly
        {
            get
            {
                return this.textBox1.ReadOnly;
            }
            set
            {
                this.textBox1.ReadOnly = value;
            }
        }
        #endregion

        #region LabelSettings
        [Browsable(true)]
        [Description("设置标签的值")]
        public string Label
        {
            get
            {
                return label1.Text;
            }
            set
            {
                label1.Text = value;
            }
        }

        [Browsable(true)]
        [Description("设置标签的对齐方式")]
        public ContentAlignment LabelTextAlign
        {
            get
            {
                return label1.TextAlign;
            }
            set
            {
                label1.TextAlign = value;
            }
        }

        [Browsable(true)]
        [Description("设置标签的字体")]
        public Font LabelFont {
            get
            {
                return label1.Font;
            }
            set
            {
                label1.Font = value;
            }
        }

        [Browsable(true)]
        [Description("设置标签的颜色")]
        public Color LabelForeColor
        {
            get
            {
                return label1.ForeColor;
            }
            set
            {
                label1.ForeColor = value;
            }
        }

        [Browsable(true)]
        [Description("设置标签的宽度")]
        public float LabelWidth
        {
            get
            {
                return tableLayoutPanel1.ColumnStyles[0].Width;
            }
            set
            {
                this.tableLayoutPanel1.ColumnStyles[0].Width = value;
            }
        } 
        #endregion

    }
}
