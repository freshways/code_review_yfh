﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Linq.Expressions;

namespace Atom.Client.Controls
{
    public partial class CDropDownBox : UserControl,IZwjDefControl
    {
        public CDropDownBox()
        {
            InitializeComponent();
        }

        [Browsable(true)]
        [Description("设置下拉框的值")]
        public object Value
        {
            get
            {
                return comboBox1.SelectedValue;
            }
            set
            {
                if (value != null && comboBox1.Items.Count <= 0)
                {
                    throw new ArgumentException("设置下拉框的值前请先设置其选项!", "Value");
                }
                comboBox1.SelectedValue = value;
            }
        }

        [Browsable(true)]
        [Description("设置下拉框的选项")]
        public IList Items
        {
            get
            {
               return comboBox1.Items;
            }
            set
            {
                object[] items=new object[value.Count];
                value.CopyTo(items,0);
                comboBox1.Items.AddRange(items);
            }
        }

        [Browsable(true)]
        [Description("设置下拉框样式")]
        public ComboBoxStyle DropDownStyle
        {
            get
            {
                return comboBox1.DropDownStyle;
            }
            set
            {
                comboBox1.DropDownStyle = value;
            }
        }

        [Browsable(true)]
        [Description("设置标签的值")]
        public string Label
        {
            get
            {
                return label1.Text;
            }
            set
            {
                label1.Text = value;
            }
        }

        [Browsable(true)]
        [Description("设置标签的宽度")]
        public float LabelWidth
        {
            get
            {
                return tableLayoutPanel1.ColumnStyles[0].Width;
            }
            set
            {
                this.tableLayoutPanel1.ColumnStyles[0].Width = value;
            }
        }

        [Browsable(true)]
        [Description("设置标签的字体")]
        public Font LabelFont
        {
            get
            {
                return label1.Font;
            }
            set
            {
                label1.Font = value;
            }
        }

        [Browsable(true)]
        [Description("设置标签的颜色")]
        public Color LabelForeColor
        {
            get
            {
                return label1.ForeColor;
            }
            set
            {
                label1.ForeColor = value;
            }
        }

        [Browsable(true)]
        [Description("设置字体")]
        public Font TextFont
        {
            get
            {
                return comboBox1.Font;
            }
            set
            {
                comboBox1.Font = value;
            }
        }

        public void ValueFor<TEntity>(Expression<Func<TEntity, dynamic>> selectField, string fieldValue, IList options, ComboBoxStyle style = ComboBoxStyle.DropDownList) where TEntity : class
        {
            //var fieldInfo = General.GetPropertyInfo(selectField);
            //this.Label = General.GetDisplayName(fieldInfo);
            this.Value = fieldValue;
            this.Items = options;
            this.DropDownStyle = style;

        }

        public void DataBindToItems(IList<object> source,string text,string value)
        {
            //Common.ComboBoxDataBind(comboBox1, source, text, value);
        }
    }
}
