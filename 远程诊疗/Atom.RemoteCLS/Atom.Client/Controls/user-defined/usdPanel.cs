﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Atom.Client.Controls
{
    public partial class usdPanel : System.Windows.Forms.Panel
    {
        public usdPanel()
        {
            InitializeComponent();
            SetStyle(System.Windows.Forms.ControlStyles.UserPaint, true);
            SetStyle(System.Windows.Forms.ControlStyles.AllPaintingInWmPaint, true);   //   禁止擦除背景.
            SetStyle(System.Windows.Forms.ControlStyles.DoubleBuffer, true);   
        }

        public usdPanel(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
