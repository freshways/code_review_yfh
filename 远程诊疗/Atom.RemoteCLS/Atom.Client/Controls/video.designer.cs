﻿namespace Atom.Client.Controls
{
    partial class video
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(video));
            this.picRemote = new System.Windows.Forms.PictureBox();
            this.picLocal = new System.Windows.Forms.PictureBox();
            this.butReceive = new System.Windows.Forms.Button();
            this.butCancel = new System.Windows.Forms.Button();
            this.panelAVUp = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.picRemote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLocal)).BeginInit();
            this.panelAVUp.SuspendLayout();
            this.SuspendLayout();
            // 
            // picRemote
            // 
            this.picRemote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picRemote.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picRemote.BackgroundImage")));
            this.picRemote.Location = new System.Drawing.Point(5, 5);
            this.picRemote.Margin = new System.Windows.Forms.Padding(4);
            this.picRemote.Name = "picRemote";
            this.picRemote.Size = new System.Drawing.Size(318, 240);
            this.picRemote.TabIndex = 5;
            this.picRemote.TabStop = false;
            // 
            // picLocal
            // 
            this.picLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.picLocal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("picLocal.BackgroundImage")));
            this.picLocal.Location = new System.Drawing.Point(226, 175);
            this.picLocal.Margin = new System.Windows.Forms.Padding(4);
            this.picLocal.Name = "picLocal";
            this.picLocal.Size = new System.Drawing.Size(97, 70);
            this.picLocal.TabIndex = 1;
            this.picLocal.TabStop = false;
            // 
            // butReceive
            // 
            this.butReceive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butReceive.Location = new System.Drawing.Point(159, 249);
            this.butReceive.Margin = new System.Windows.Forms.Padding(4);
            this.butReceive.Name = "butReceive";
            this.butReceive.Size = new System.Drawing.Size(83, 30);
            this.butReceive.TabIndex = 4;
            this.butReceive.Text = "接收";
            this.butReceive.UseVisualStyleBackColor = true;
            // 
            // butCancel
            // 
            this.butCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butCancel.Location = new System.Drawing.Point(242, 249);
            this.butCancel.Margin = new System.Windows.Forms.Padding(4);
            this.butCancel.Name = "butCancel";
            this.butCancel.Size = new System.Drawing.Size(83, 30);
            this.butCancel.TabIndex = 3;
            this.butCancel.Text = "取消";
            this.butCancel.UseVisualStyleBackColor = true;
            // 
            // panelAVUp
            // 
            this.panelAVUp.BackColor = System.Drawing.Color.Transparent;
            this.panelAVUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelAVUp.Controls.Add(this.butCancel);
            this.panelAVUp.Controls.Add(this.picLocal);
            this.panelAVUp.Controls.Add(this.butReceive);
            this.panelAVUp.Controls.Add(this.picRemote);
            this.panelAVUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAVUp.Location = new System.Drawing.Point(0, 0);
            this.panelAVUp.Margin = new System.Windows.Forms.Padding(4);
            this.panelAVUp.Name = "panelAVUp";
            this.panelAVUp.Size = new System.Drawing.Size(327, 285);
            this.panelAVUp.TabIndex = 0;
            // 
            // video
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.Controls.Add(this.panelAVUp);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "video";
            this.Size = new System.Drawing.Size(327, 285);
            ((System.ComponentModel.ISupportInitialize)(this.picRemote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLocal)).EndInit();
            this.panelAVUp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picRemote;
        private System.Windows.Forms.PictureBox picLocal;
        private System.Windows.Forms.Button butReceive;
        private System.Windows.Forms.Button butCancel;
        private System.Windows.Forms.Panel panelAVUp;


    }
}
