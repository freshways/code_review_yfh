﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace MySkin.Controls
{
    public partial class MyTabControl : System.Windows.Forms.TabControl
    {
         ///<summary>
        /// 构造函数,设置控件风格
        ///</summary>
        public MyTabControl()
        {
            InitializeComponent();
            SetStyle
                      (System.Windows.Forms.ControlStyles.AllPaintingInWmPaint  //全部在窗口绘制消息中绘图
                      | System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer //使用双缓冲
                      , true);
        }

        public MyTabControl(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        ///<summary>
        /// 设置控件窗口创建参数的扩展风格
        ///</summary>
       protected override System.Windows.Forms.CreateParams CreateParams
        {
           get
            {
               System.Windows.Forms.CreateParams cp = base.CreateParams;
               cp.ExStyle |= 0x02000000;
               return cp;
            }
        }

    }
}
