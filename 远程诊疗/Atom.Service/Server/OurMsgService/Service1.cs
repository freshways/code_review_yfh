﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;

using OurMsgServer;

namespace OurMsgService
{
    public partial class Service1 : ServiceBase
    {

        private Server server = null;


        public Service1()
        {
            InitializeComponent();
            server = new Server();
        }

         
        protected override void OnStart(string[] args)
        {
            try
            { 
                server.Start();
                //IMLibrary4.Operation.Calculate.WirteLog("服务启动成功！");
           
            }
            catch(Exception ex)
            {
                //IMLibrary4.Operation.Calculate.WirteLog("服务启动失败！" + ex.Source + ex.Message);
                //server.Stop();
               //IMLibrary4.Operation.Calculate.WirteLog("服务已停止！");
            }
        }

       

        protected override void OnStop()
        {
            try
            {
                server.Stop();
                //IMLibrary4.Operation.Calculate.WirteLog("服务停止成功！");

            }
            catch (Exception ex)
            {
                //IMLibrary4.Operation.Calculate.WirteLog("服务停止失败！" + ex.Source + ex.Message);
            }
        }
    }
}
