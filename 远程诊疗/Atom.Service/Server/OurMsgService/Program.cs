﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Configuration.Install;


namespace OurMsgService
{
    static class Program
    {
        #region static method Main

        /// <summary>
        /// Application main entry point.
        /// </summary>
        /// <param name="args">Command line argumnets.</param>
        public static void Main(string[] args)
        {
            if (args.Length > 0 && (args[0].ToLower() == "-install" || args[0].ToLower() == "-i"))
            {
                ManagedInstallerClass.InstallHelper(new string[] { "OurMsgService.exe" });

                ServiceController c = new ServiceController("OurMsg2 Service");
                c.Start();
            }
            else if (args.Length > 0 && (args[0].ToLower() == "-uninstall" || args[0].ToLower() == "-u"))
            {
                ManagedInstallerClass.InstallHelper(new string[] { "/u", "OurMsgService.exe" });
            }
            else
            {
                System.ServiceProcess.ServiceBase[] servicesToRun = new System.ServiceProcess.ServiceBase[] { new  Service1 () };
                System.ServiceProcess.ServiceBase.Run(servicesToRun);
            }

        }
        #endregion
    }
}
