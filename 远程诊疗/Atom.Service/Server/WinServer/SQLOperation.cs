﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using IMLibrary4.Protocol;
using IMLibrary4.SqlData;
using IMLibrary4.Organization;
using System.Configuration;


namespace OurMsgServer
{
    /// <summary>
    /// SQLite数据库服务器操作类
    /// </summary>
    public class SQLOperation
    {
        /// <summary>
        /// 
        /// </summary>
        public SQLOperation()
        {
            //SQLiteDBHelper.connectionString = "data source=OurMsgServerDB.s3db";
            DataAccess.ConnectionString = ConfigurationManager.AppSettings["SqlConnectionString"].ToString();

            t.Elapsed += new System.Timers.ElapsedEventHandler(t_Elapsed);
            t.Interval = 1;
        }

        #region 变量
        System.Timers.Timer t = new System.Timers.Timer();
        /// <summary>
        /// 数据库操作栈
        /// </summary>
        Stack<Operation> dbOperStack = new Stack<Operation>();

        bool Operating = false;

        #endregion

        #region 时钟操作
        void t_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (Operating) return;
            Operating = true;
            try
            {
                if (dbOperStack.Count > 0)
                {
                    Operation o = dbOperStack.Pop();//获取一个操作
                    if (o.action == Action.insert )//如果添加消息
                    {
                        if (o.table == Table.msgRecord)
                        {
                            #region 添加消息操作

                            Element msg = o.obj as Element;

                            string sql = "insert into RecordMsg(userID,datetime,Vcard) values(@userID,@datetime,@Vcard)";
                            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]{   
                             new System.Data.SqlClient.SqlParameter("@userID",o.userID ),   
                             new System.Data.SqlClient.SqlParameter("@datetime",System.DateTime.Now.ToString()),   
                             new System.Data.SqlClient.SqlParameter("@Vcard", Factory.CreateXMLMsg(o.obj)),   
                                               };
                            DataAccess.ExecSql(sql, parameters);
                            #endregion
                        }
                        else if(o.table == Table.offlineFile)
                        {
                            #region 添加离线文件信息操作
                            OfflineFileMsg offlinefilemsg = o.obj as OfflineFileMsg;

                            string sql = "insert into OfflineFiles(datetime,MD5) values(@datetime,@MD5)";
                            System.Data.SqlClient.SqlParameter[] parameters = new System.Data.SqlClient.SqlParameter[]{   
                             new System.Data.SqlClient.SqlParameter("@datetime",System.DateTime.Now.ToString()),   
                             new System.Data.SqlClient.SqlParameter("@MD5",offlinefilemsg.MD5 + offlinefilemsg.Extension),   
                                               };
                            DataAccess.ExecSql(sql, parameters);
                            #endregion                                  
                        }
                        else if (o.table == Table.OfflineFiles_users)
                        {
                            #region 添加用户列表，记录接收人
                            try
                            {
                                OfflineFileMsg offlinefilemsg = o.obj as OfflineFileMsg;
                                if (o.userID != "" && o.userID != offlinefilemsg.from)
                                {
                                    string sqluser = "insert into OfflineFiles_users(FromTime,MD5,SendUser,RecUser,Vcard,IFDel) values(@FromTime,@MD5,@SendUser,@RecUser,@Vcard,@IFDel )";
                                    System.Data.SqlClient.SqlParameter[] parametersUser = new System.Data.SqlClient.SqlParameter[]{
                                         new System.Data.SqlClient.SqlParameter("@FromTime",System.DateTime.Now.ToString()),                                     
                                         new System.Data.SqlClient.SqlParameter("@MD5",offlinefilemsg.MD5 + offlinefilemsg.Extension),  
                                         new System.Data.SqlClient.SqlParameter("@SendUser",offlinefilemsg.from), 
                                         new System.Data.SqlClient.SqlParameter("@RecUser",o.userID), 
                                         new System.Data.SqlClient.SqlParameter("@Vcard", Factory.CreateXMLMsg(o.obj)),  
                                         new System.Data.SqlClient.SqlParameter("@IFDel", "1" ), 
                                            };
                                    DataAccess.ExecSql(sqluser, parametersUser);
                                }
                            }
                            catch { }

                            #endregion
                        }
                    }
                    else if (o.action == Action.cmdtext && o.obj is string)
                    {
                        #region 执行SQL文本命令操作
                        DataAccess.ExecSql(o.obj.ToString());
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                
            }

            Operating = false;
        }
        #endregion

        #region 公共方法

        #region 服务
        /// <summary>
        /// 开始数据库单线程操作
        /// </summary>
        public void start()
        {
            t.Enabled = true;
        }

        /// <summary>
        /// 停止数据库单线程操作
        /// </summary>
        public void stop()
        {
            t.Enabled = false;
        }
        #endregion

        #region 添加操作对像
        public void addOperation(Operation o)
        {
            dbOperStack.Push(o);
        }
        #endregion

        #region 添加操作命令
        /// <summary>
        /// 添加命令操作
        /// </summary>
        /// <param name="sql"></param>
        public void addCmdTextOper(string sql)
        {
            Operation o = new Operation();
            o.action = Action.cmdtext;
            o.obj = sql;
            dbOperStack.Push(o); ;//添加删除操作
        }
        #endregion

        #region 获得用户离线消息
        /// <summary>
        /// 获得用户离线消息
        /// </summary>
        /// <param name="userID">用户帐号</param>
        /// <param name="top">消息最后条数</param>
        /// <returns></returns>
        public List<string> GetOfflineMessage(string userID, int top)
        {
            try
            {
                List<string> messageList = new List<string>();
                string sql = string.Format("select top {1} Vcard from RecordMsg where userID='{0}' order by ID desc ", userID, top);

                System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql(sql);
                if (dr != null)
                {
                    while (dr.Read())
                        messageList.Add(dr["Vcard"].ToString());
                    dr.Close();
                }
                dr.Dispose();

                sql = string.Format("delete  from RecordMsg where userID='{0}'", userID);

                addCmdTextOper(sql);//添加删除操作

                return messageList;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Source + ex.Message);
                return null;
            }
        }
        #endregion

        #region 存入离线消息
        /// <summary>
        /// 将文件信息存入离线消息
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="offlineFileMsg"></param>
        public void SavaOfflineFileInfo(string userID, OfflineFileMsg offlineFileMsg)
        {
            string sql = string.Format("select * from OfflineFiles where MD5='{0}'", offlineFileMsg.MD5 + offlineFileMsg.Extension);
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql(sql);
            if (dr != null && dr.HasRows)
            {
                dr.Close();
                dr.Dispose();
                dr = null;
                sql = string.Format("update  OfflineFiles set datetime='{0}'  where MD5='{1}'", System.DateTime.Now.ToString(), offlineFileMsg.MD5 + offlineFileMsg.Extension);
                addCmdTextOper(sql);//添加更新记录操作
            }
            else
            {
                addOperation(new Operation(userID, Action.insert, Table.offlineFile, offlineFileMsg));//插入记录
            }

            #region 记录接收文件的用户
            
            try
            {
                //验证接收文件人是否已经存在数据库
                string sqluser = string.Format("select * from OfflineFiles_users where RecUser='{0}' and MD5='{1}' and SendUser='{2}' ", userID, offlineFileMsg.MD5 + offlineFileMsg.Extension, offlineFileMsg.from);
                System.Data.SqlClient.SqlDataReader druser = DataAccess.GetReaderBySql(sqluser);
                if (druser != null && druser.HasRows)
                {
                    druser.Close();
                    druser.Dispose();
                    druser = null;
                    sql = string.Format("update  OfflineFiles_users set FromTime='{0}'  where MD5='{1}' and RecUser='{2}' ", System.DateTime.Now.ToString(), offlineFileMsg.MD5 + offlineFileMsg.Extension, userID);
                    addCmdTextOper(sql);//添加更新记录操作
                }
                else
                {
                    addOperation(new Operation(userID, Action.insert, Table.OfflineFiles_users, offlineFileMsg));//插入用户记录
                }
            }
            catch  { }

            #endregion
        }
        #endregion

        #region 用户信息管理

        #region 一次性加入多个用户到服务器数据库
        /// <summary>
        /// 用户字典
        /// </summary>
        /// <param name="Users"></param>
        public void AddUsers(Dictionary<string, ServerUser> Users)
        {
            SqlConnection con = new SqlConnection(); //创建连接
            SqlCommand cmd = null;
            con.ConnectionString = DataAccess.ConnectionString;
            try
            {
                con.Open();
                using (SqlTransaction dbTrans = con.BeginTransaction()) //使用事务
                {
                    using (cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "insert into Users(UserID,UserName,Password,GroupID,orderID) values(?,?,?,?,?)";
                        SqlParameter Field1 = cmd.CreateParameter();//添加字段
                        SqlParameter Field2 = cmd.CreateParameter();
                        SqlParameter Field3 = cmd.CreateParameter();
                        SqlParameter Field4 = cmd.CreateParameter();
                        SqlParameter Field5 = cmd.CreateParameter();
                        cmd.Parameters.Add(Field1);
                        cmd.Parameters.Add(Field2);
                        cmd.Parameters.Add(Field3);
                        cmd.Parameters.Add(Field4);
                        cmd.Parameters.Add(Field5);

                        foreach (ServerUser user in Users.Values)
                        {
                            Field1.Value = user.UserID; //字符赋值
                            Field2.Value = user.UserName;
                            Field3.Value = user.Password;
                            Field4.Value = user.GroupID;
                            Field5.Value = user.OrderID;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    dbTrans.Commit();     //提交事务执行
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region 添加新用户到数据库
        /// <summary>
        /// 添加新用户到数据库
        /// </summary>
        /// <param name="user"></param>
        public void AddUser(ServerUser user,UserVcard card)
        {
            string sql = string.Format("insert into Users(UserID,UserName,Password,GroupID,orderID,Power,Vcard,datetime) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", user.UserID, user.UserName, user.Password, user.GroupID, user.OrderID, Factory.CreateXMLMsg(user.Power), Factory.CreateXMLMsg(card),DateTime.Now.ToString());
            addCmdTextOper(sql);
        }
        #endregion

        #region 删除数据库中用户资料
        /// <summary>
        /// 删除数据库中用户资料
        /// </summary>
        public void DelUser(string userID)
        {
            string sql = string.Format("delete from Users where userID='{0}'", userID);
            addCmdTextOper(sql);
        }
        #endregion

        #region 更新用户资料
        /// <summary>
        /// 更新用户资料
        /// </summary>
        /// <param name="user"></param>
        /// <param name="card"></param>
        public void UpdateUser(ServerUser user, UserVcard card)
        {
            string sql = string.Format("update Users set UserName='{1}',GroupID='{2}',orderID='{3}',Power='{4}',Vcard='{5}' where UserID='{0}'", user.UserID, user.UserName, user.GroupID, user.OrderID, Factory.CreateXMLMsg(user.Power), Factory.CreateXMLMsg(card));
            addCmdTextOper(sql);
        }
        #endregion

        #region 获得所有用户集合
        /// <summary>
        /// 获得所有用户集合
        /// </summary>
        public Dictionary<string, ServerUser> GetUsers()
        {
            Dictionary<string, ServerUser> Users = new Dictionary<string, ServerUser>();//创建用户字典
            string sql = "select * from Users order by  datetime";

            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql(sql);
            if (dr != null)
            {
                while (dr.Read())
                {
                    ServerUser user = new ServerUser();
                    {
                        user.UserID = Convert.ToString(dr["UserID"]);
                        user.UserName = Convert.ToString(dr["UserName"]);
                        //user.Password = Convert.ToString(dr["Password"]);
                        user.GroupID = Convert.ToString(dr["GroupID"]);
                        user.OrderID = Convert.ToInt32(dr["OrderID"]);

                        ///获得用户最后一次登录信息
                        UserLoginInfo loginInfo = Factory.CreateInstanceObject(Convert.ToString(dr["LoginInfo"])) as UserLoginInfo;
                        if (loginInfo != null) user.LoginInfo = loginInfo;
                        //获得用户权限
                        UserPower power = getUserPower(user.UserID);
                        if (power != null) user.Power = power;
                    }
                    Users.Add(user.UserID, user);
                }
                dr.Close();
            }
            dr.Dispose();
            return Users;
        }
        #endregion

        #region 用户是否有效密码
        /// <summary>
        /// 用户是否有效密码
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool IsValidPassword(string userID, string Password)
        {
            bool t = false;
            string sql =string.Format( "select Password from Users where  userID='{0}'",userID);
             
            System.Data.SqlClient.SqlDataReader dr =DataAccess.GetReaderBySql(sql );
            if (dr != null && dr.Read())
            {
                if (dr["Password"].ToString().Trim() == Password)
                {
                    t = true;
                    dr.Close();
                }
            }
            dr.Dispose();
            return t;
        }
        #endregion 

        #region 获得用户card 信息
        /// <summary>
        /// 获得用户card信息
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public UserVcard getUserVcard(string userID)
        {
            UserVcard card=null ;
            string sql = string.Format("select Vcard from Users where userID='{0}'", userID);
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql(sql);
            if (dr != null)
            {
                while (dr.Read())
                {
                    card = Factory.CreateInstanceObject(Convert.ToString(dr["Vcard"])) as UserVcard;
                }
                dr.Close();
            }
            dr.Dispose();
            return card;
        }
        #endregion

        #region 更新用户详细信息
        /// <summary>
        /// 更新用户详细信息
        /// </summary>
        ///<param name="userID"></param>
        ///<param name="card"></param>
        /// <returns></returns>
        public bool UpdateUserVcard(string userID, UserVcard card)
        {
            bool t = false;
            string sql = string.Format("update Users set Vcard='{0}'  where  userID='{1}'", Factory.CreateXMLMsg(card), userID);
            addCmdTextOper(sql);
            return t;
        }
        #endregion 

        #region 获得用户权限
        /// <summary>
        /// 获得用户权限
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        private UserPower getUserPower(string userID)
        {
            UserPower power = null;
            string sql = string.Format("select Power from Users where userID='{0}'", userID);
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql(sql);
            if (dr != null)
            {
                while (dr.Read())
                {
                    power = Factory.CreateInstanceObject(Convert.ToString(dr["Power"])) as UserPower;
                }
                dr.Close();
            }
            dr.Dispose();
            return power;
        }
        #endregion

        #region 初始化管理员权限
        /// <summary>
        /// 初始化管理员权限
        /// </summary>
        public void iniAdminPower()
        {
            UserPower power = new UserPower();
            power.isAdmin = true;
            power.isBroadcast = true;
            power.isSendNotice = true;
            power.isSendSMS = true;
            power.isControlClient = true;

            UpdateUserPower("admin",power);

        }
        #endregion

        #region 更新用户权限
        /// <summary>
        /// 更新用户权限
        /// </summary>
        ///<param name="userID"></param>
        ///<param name="card"></param>
        /// <returns></returns>
        public bool UpdateUserPower(string userID, UserPower power)
        {
            bool t = false;
            string sql = string.Format("update Users set power='{0}'  where  userID='{1}'", Factory.CreateXMLMsg(power), userID);
            addCmdTextOper(sql);
            return t;
        }
        #endregion 

        #region 更新用户最后一次登录信息
        /// <summary>
        /// 更新用户最后一次登录信息
        /// </summary>
        ///<param name="userID"></param>
        ///<param name="LoginInfo"></param>
        /// <returns></returns>
        public bool UpdateUserLoginInfo(string userID, UserLoginInfo LoginInfo)
        {
            bool t = false;
            string sql = string.Format("update Users set LoginInfo='{0}'  where  userID='{1}'", Factory.CreateXMLMsg(LoginInfo), userID);
            addCmdTextOper(sql);
            return t;
        }
        #endregion 

        #region 更新用户密码
        /// <summary>
        /// 更新用户密码
        /// </summary>
        /// <param name="userID">用户ID</param>
        /// <param name="newPassword">用户新pndc</param>
        public void updatePWD(string userID,string newPassword)
        {
            string sql = string.Format("update users set password='{0}' where userid='{1}'",newPassword ,userID);
            addCmdTextOper(sql);
        }
        #endregion

        #endregion

        #region 分组信息管理

        /// <summary>
        /// 添加分组
        /// </summary>
        /// <param name="group"></param>
        public void AddGroup(Group group)
        {
            var sql = string.Format("insert into Groups(GroupID,GroupName,SuperiorID,orderID,datetime) values('{0}','{1}','{2}',{3},'{4}')", group.GroupID, group.GroupName, group.SuperiorID, group.OrderID, DateTime.Now.ToString());
            addCmdTextOper(sql);
        }

        /// <summary>
        /// 更新分组
        /// </summary>
        /// <param name="group"></param>
        public void UpdateGroup(Group group)
        {
            var sql = string.Format("update Groups set GroupName='{0}',SuperiorID='{1}',orderID={2} where GroupID='{3}'", group.GroupName, group.SuperiorID, group.OrderID, group.GroupID);
            addCmdTextOper(sql);
        }

        /// <summary>
        /// 删除分组
        /// </summary>
        /// <param name="groupID"></param>
        public void DelGroup(string groupID)
        {
            var sql = string.Format("delete from Groups  where GroupID='{0}'", groupID);
            addCmdTextOper(sql);
        }

        #region 添加分组
        /// <summary>
        ///  添加多个分组信息
        /// </summary>
        /// <param name="Groups"></param>
        public  void AddGroups(Dictionary<string, Group> Groups)
        {
            #region 一次性事务添加
            SqlConnection con = new SqlConnection(); //创建连接
            SqlCommand cmd = null;
            con.ConnectionString = DataAccess.ConnectionString;
            try
            {
                con.Open();
                using (SqlTransaction dbTrans = con.BeginTransaction()) //使用事务
                {
                    using (cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "insert into Groups(GroupID,GroupName,SuperiorID,orderID) values(?,?,?,?)";
                        SqlParameter Field1 = cmd.CreateParameter();   //添加字段
                        SqlParameter Field2 = cmd.CreateParameter();
                        SqlParameter Field3 = cmd.CreateParameter();
                        SqlParameter Field4 = cmd.CreateParameter();
                        cmd.Parameters.Add(Field1);
                        cmd.Parameters.Add(Field2);
                        cmd.Parameters.Add(Field3);
                        cmd.Parameters.Add(Field4);
                        foreach ( Group group in Groups.Values)
                        {
                            Field1.Value = group.GroupID; //字符赋值
                            Field2.Value = group.GroupName;
                            Field3.Value = group.SuperiorID;
                            Field4.Value = group.OrderID;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    dbTrans.Commit();     //提交事务执行
                }
            }
            catch (Exception ex)
            {
                 
            }
            finally
            {
                con.Close();
            }
            #endregion
        }
        #endregion 

        #region 获得所有分组集合
        /// <summary>
        /// 获得所有分组集合
        /// </summary>
        public   Dictionary<string, Group> GetGroups()
        {
            Dictionary<string, Group> Groups = new Dictionary<string, Group>();
            string sql = "select * from Groups order by datetime  ";
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql(sql );
            if (dr != null)
            {
                while (dr.Read())
                {
                    Group group = new Group();
                    {
                        group.GroupID = Convert.ToString(dr["GroupID"]);
                        group.GroupName = Convert.ToString(dr["GroupName"]);
                        group.SuperiorID = Convert.ToString(dr["SuperiorID"]);
                        group.OrderID = Convert.ToInt32(dr["orderID"]);
                    }
                    Groups.Add(group.GroupID ,group);
                }
                dr.Close();
            }
            dr.Dispose();

            return Groups;
        }
        #endregion

        #endregion

        #region 群管理

        #region 添加多个群
        /// <summary>
        ///  添加多个群
        /// </summary>
        /// <param name="Rooms"></param>
        public void AddRooms(Dictionary<string, Room> Rooms)
        {
            #region 一次性事务添加
            SqlConnection con = new SqlConnection(); //创建连接
            SqlCommand cmd = null;
            con.ConnectionString = DataAccess.ConnectionString;
            try
            {
                con.Open();
                using (SqlTransaction dbTrans = con.BeginTransaction()) //使用事务
                {
                    using (cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "insert into Rooms(RoomID,Vcard) values(?,?)";
                        SqlParameter Field1 = cmd.CreateParameter();   //添加字段
                        SqlParameter Field2 = cmd.CreateParameter();
                        cmd.Parameters.Add(Field1);
                        cmd.Parameters.Add(Field2);
                        
                        foreach (Room room in Rooms.Values)
                        {
                            Field1.Value = room.RoomID; //字符赋值
                            Field2.Value =Factory.CreateXMLMsg( room);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    dbTrans.Commit();     //提交事务执行
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                con.Close();
            }
            #endregion
        }
        #endregion 

        #region 获得所有群
        /// <summary>
        /// 获得所有群
        /// </summary>
        public Dictionary<string, Room> GetRooms()
        {
            Dictionary<string, Room> Rooms = new Dictionary<string, Room>();
            string sql = "select * from Rooms";
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql(sql );
            if (dr != null)
            {
                while (dr.Read())
                {
                    Room room =Factory.CreateInstanceObject( Convert.ToString(dr["Vcard"])) as Room  ;
                    if(room!=null)
                    Rooms.Add(room.RoomID ,room);
                }
                dr.Close();
            }
            dr.Dispose();
            return Rooms;
        }
        #endregion

        #region 保存用户新创建的群信息
        /// <summary>
        /// 保存用户新创建的群信息
        /// </summary>
        /// <param name="roomID">群ID</param>
        /// <param name="roomVcardXML">群XML信息</param>
        public void saveCreateNewRoom(string roomID,string  roomVcardXML)
        {
            var sql = string.Format("insert into rooms(roomID,Vcard) values('{0}','{1}')",roomID,roomVcardXML);
            addCmdTextOper(sql);
        }
        #endregion

        #region 保存用户修改的群信息
        /// <summary>
        /// 保存用户修改的群信息
        /// </summary>
        /// <param name="roomID">群ID</param>
        /// <param name="roomVcardXML">群XML信息</param>
        public void saveUpdateRoom(string roomID, string roomVcardXML)
        {
            var sql = string.Format("update rooms set Vcard='{0}' where roomID='{1}'",roomVcardXML ,roomID );
            addCmdTextOper(sql);
        }
        #endregion

        #endregion

        #endregion

    }


    #region 数据操作、动作、枚举等对像

    /// <summary>
    /// 操作表名称
    /// </summary>
    public enum Table
    {
        msgRecord,
        offlineFile,
        OfflineFiles_users,//文件接收列表
    }

    /// <summary>
    /// 动作
    /// </summary>
    public enum Action
    {
        /// <summary>
        /// 添加
        /// </summary>
        insert,
        /// <summary>
        /// 删除
        /// </summary>
        del,
        /// <summary>
        /// 插入
        /// </summary>
        update,
        /// <summary>
        /// 文本命令
        /// </summary>
        cmdtext,
    }

    /// <summary>
    /// 数据库操作
    /// </summary>
    public class Operation
    {
        public Operation()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">动作</param>
        /// <param name="table">表名称</param>
        /// <param name="obj">对像</param>
        public Operation(string userID, Action action, Table table, object objs)
        {
            this.userID = userID;
            this.action = action;
            this.table = table;
            this.obj = objs;
        }
        /// <summary>
        /// 操作用户
        /// </summary>
        public string userID = "";
        /// <summary>
        /// 动作
        /// </summary>
        public Action action = Action.cmdtext;
        /// <summary>
        /// 操作表名称
        /// </summary>
        public Table table = Table.msgRecord;
        /// <summary>
        /// 操作对像
        /// </summary>
        public object obj = null;

    }
    #endregion

}
