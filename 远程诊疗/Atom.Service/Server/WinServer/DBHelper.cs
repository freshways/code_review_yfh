﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Resources;
using System.Reflection;

using IMLibrary4.Organization;
using IMLibrary4.Protocol ;
using IMLibrary4.SqlData;

namespace OurMsgServer
{
    public  class DBHelper
    {
        /// <summary>
        /// 资源管理器
        /// </summary>
        public static  ResourceManager resourceManager = new  ResourceManager("OurMsgServer.Properties.Settings",Assembly.GetExecutingAssembly());

        #region 组织机构管理

        #region 创建新用户
        /// <summary>
        /// 创建新用户
        /// </summary>
        /// <param name="card"></param>
        //public static int CreateUserVcard(UserVcard card)
        //{
        //    string sql = "insert into Users(userID,userName,GroupID,orderID,sex,Password,FaceIndex,CreateRooms,isAdmin,isSendSMS,isEditUserData,isSendNotice,isBroadcast,Vcard)"
        //                 + "values(@userID,@userName,@GroupID,@orderID,@sex,@Password,@FaceIndex,@CreateRooms,@isAdmin,@isSendSMS,@isEditUserData,@isSendNotice,@isBroadcast,@Vcard)";
        //    System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
        //                  { new System.Data.SqlClient.SqlParameter("@userID",card.UserID ),
        //                    new System.Data.SqlClient.SqlParameter("@userName",card.UserName ),
        //                    new System.Data.SqlClient.SqlParameter("@GroupID",card.GroupID ),
        //                    new System.Data.SqlClient.SqlParameter("@orderID",card.OrderID ),
        //                    new System.Data.SqlClient.SqlParameter("@sex", card.Sex),
        //                    new System.Data.SqlClient.SqlParameter("@Password", card.Password),
        //                    new System.Data.SqlClient.SqlParameter("@FaceIndex", card.FaceIndex),
        //                    new System.Data.SqlClient.SqlParameter("@CreateRooms", card.CreateMaxRooms),
        //                    new System.Data.SqlClient.SqlParameter("@isAdmin", card.isAdmin),
        //                    new System.Data.SqlClient.SqlParameter("@isSendSMS", card.isSendSMS),
        //                    new System.Data.SqlClient.SqlParameter("@isEditUserData", card.isEditUserData),
        //                    new System.Data.SqlClient.SqlParameter("@isSendNotice", card.isSendNotice),
        //                    new System.Data.SqlClient.SqlParameter("@isBroadcast", card.isBroadcast),
        //                    new System.Data.SqlClient.SqlParameter("@Vcard",Factory.CreateXMLMsg(card)),
        //                  };
        // return DataAccess.ExecSql(sql, sqlpar);
        //}
        #endregion

        #region 更新用户信息
        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="card"></param>
        //public static int  UpdateUserVcard(UserVcard card)
        //{
        //    string sql = "update Users set userName=@userName,GroupID=@GroupID,orderID=@orderID,sex=@sex,Password=@Password,FaceIndex=@FaceIndex,"
        //    + "CreateRooms=@CreateRooms,isAdmin=@isAdmin,isSendSMS=@isSendSMS,isEditUserData=@isEditUserData,isSendNotice=@isSendNotice,isBroadcast=@isBroadcast,Vcard=@Vcard"
        //               + " where userID=@userID";
        //    System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
        //                  { new System.Data.SqlClient.SqlParameter("@userID",card.UserID ),
        //                    new System.Data.SqlClient.SqlParameter("@userName",card.UserName ),
        //                    new System.Data.SqlClient.SqlParameter("@GroupID",card.GroupID ),
        //                    new System.Data.SqlClient.SqlParameter("@orderID",card.OrderID ),
        //                    new System.Data.SqlClient.SqlParameter("@sex", card.Sex),
        //                    new System.Data.SqlClient.SqlParameter("@Password", card.Password),
        //                    new System.Data.SqlClient.SqlParameter("@FaceIndex", card.FaceIndex),
        //                    new System.Data.SqlClient.SqlParameter("@CreateRooms", card.CreateMaxRooms),
        //                    new System.Data.SqlClient.SqlParameter("@isAdmin", card.isAdmin),
        //                    new System.Data.SqlClient.SqlParameter("@isSendSMS", card.isSendSMS),
        //                    new System.Data.SqlClient.SqlParameter("@isEditUserData", card.isEditUserData),
        //                    new System.Data.SqlClient.SqlParameter("@isSendNotice", card.isSendNotice),
        //                    new System.Data.SqlClient.SqlParameter("@isBroadcast", card.isBroadcast),
        //                    new System.Data.SqlClient.SqlParameter("@Vcard",Factory.CreateXMLMsg(card)),
        //                  };
        //   return  DataAccess.ExecSql(sql, sqlpar);
        //}
        #endregion

        #region 删除用户
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userID"></param>
        public static int  DelUser(string userID)
        {
            string sql = "delete Users where userID=@userID";
            System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[] { new System.Data.SqlClient.SqlParameter("@userID", userID), };
          return   DataAccess.ExecSql(sql, sqlpar);
        }
        #endregion

        #region 创建分组
        /// <summary>
        /// 创建分组
        /// </summary>
        /// <param name="group"></param>
        public static int CreateGroupVcard(EditGroupData group)
        {
            string sql = "insert into Groups(GroupID,GroupName,SuperiorId,orderID)"
                         + "values(@GroupID,@GroupName,@SuperiorId,@orderID)";
            System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
                          { new System.Data.SqlClient.SqlParameter("@GroupID",group.GroupID ),
                            new System.Data.SqlClient.SqlParameter("@GroupName",group.GroupName ),
                            new System.Data.SqlClient.SqlParameter("@SuperiorId",group.SuperiorID  ),
                            new System.Data.SqlClient.SqlParameter("@orderID",group.OrderID ),
                          };
           return  DataAccess.ExecSql(sql, sqlpar);
        }
        #endregion

        #region 更新分组信息
        /// <summary>
        /// 更新分组信息
        /// </summary>
        /// <param name="group"></param>
        public static int UpdateGroupVcard(EditGroupData group)
        {
            string sql = "update Groups set GroupName=@GroupName,SuperiorId=@SuperiorId,orderID=@orderID where GroupID=@GroupID";
            System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
                          { new System.Data.SqlClient.SqlParameter("@GroupID",group.GroupID ),
                            new System.Data.SqlClient.SqlParameter("@GroupName",group.GroupName ),
                            new System.Data.SqlClient.SqlParameter("@SuperiorId",group.SuperiorID  ),
                            new System.Data.SqlClient.SqlParameter("@orderID",group.OrderID ),
                          };
            return DataAccess.ExecSql(sql, sqlpar);
        }
        #endregion

        #region 删除分组
        /// <summary>
        /// 删除分组
        /// </summary>
        /// <param name="groupID"></param>
        public static int  DelGroup(string groupID)
        {
            string sql = "delete Groups where GroupID=@GroupID";
            System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[] { new System.Data.SqlClient.SqlParameter("@GroupID", groupID), };
           return  DataAccess.ExecSql(sql, sqlpar);
        }
        #endregion

        #region 用户是否有效密码
         /// <summary>
        /// 用户是否有效密码
         /// </summary>
         /// <param name="userID"></param>
         /// <param name="password"></param>
         /// <returns></returns>
        public static bool isValidPassword(string userID, string Password)
        {
            bool t = false;
            string sql = "select Password from Users where  userID=@userID";
            System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
                          { new System.Data.SqlClient.SqlParameter("@userID",userID),
  
                          };
            System.Data.SqlClient.SqlDataReader dr =DataAccess.GetReaderBySql(sql, sqlpar);
            if (dr != null && dr.Read())
            {
                if (dr["Password"].ToString().Trim() == Password)
                {
                    t = true;
                    dr.Close();
                }
            }
            dr.Dispose();
            return t;
        }
        #endregion

        #region 更新用户密码
        /// <summary>
        /// 更新用户密码
        /// </summary>
        /// <param name="userID">用户ID</param>
        /// <param name="pssword">用户密码</param>
        public static int UpdatePassword(string userID, string Password)
        {
            string sql = "update Users set Password=@Password  where userID=@userID";
            System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
                          { new System.Data.SqlClient.SqlParameter("@userID",userID),
                            new System.Data.SqlClient.SqlParameter("@Password",Password),
  
                          };
            return DataAccess.ExecSql(sql, sqlpar);
        }
        #endregion

        #endregion

        #region 将所有数据库中用户读入内存

        #region 将所有数据库中用户详细信息读入内存
        /// <summary>
        /// 将所有数据库中用户详细信息读入内存
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, User> GetUsers()
        {
            Dictionary<string, User> users = new Dictionary<string, User>();
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql("select * from users");
            if (dr != null)
            {
                while (dr.Read())
                {
                    User user = getDrUser(dr);
                    users.Add(user.UserID, user);
                }
                dr.Close();
            }
            dr.Dispose();
            return users;
        }
        #endregion

        #region 添加一个用户详细信息到内存字典
        /// <summary>
         /// 添加一个用户详细信息到内存字典
         /// </summary>
         /// <returns>成功返回真，失败返回假</returns>
        public static bool AddUserToUsers(Dictionary<string, User> users, string userID)
        {
            bool t = false;
            System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[] { new System.Data.SqlClient.SqlParameter("@userID", userID), };

            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql("select * from users where userID=@userID",sqlpar);
            
            if (dr != null && dr.Read())
            {
                User user = getDrUser(dr);
                lock (users)
                {
                    users.Add(user.UserID, user);
                }
                t = true;//标记添加成功
                dr.Close();
            }
            dr.Dispose();
            return t;
        }
        #endregion

        #region 将DR中用户详细信息读入内存
        /// <summary>
        /// 将DR中用户详细信息读入内存
         /// </summary>
         /// <param name="dr"></param>
         /// <returns></returns>
        private static User  getDrUser(System.Data.SqlClient.SqlDataReader dr)
        {
            ServerUser user = new ServerUser(); user.Rooms = new Dictionary<string, Room>(); user.Power = new UserPower();
            try
            {
                user.ID = dr["ID"].ToString().Trim();
            } 
            catch { }

            user.UserID = dr["UserID"].ToString().Trim();
            user.UserName = dr["UserName"] is DBNull ? user.UserID : dr["UserName"].ToString().Trim();
            user.Password = dr["Password"] is DBNull ? "E10ADC3949BA59ABBE56E057F20F883E" : dr["Password"].ToString().Trim();//E10ADC3949BA59ABBE56E057F20F883E==123456
            //user.Sex = Convert.ToByte(dr["Sex"] is DBNull ? 0 : dr["Sex"]);
            //user.FaceIndex = Convert.ToInt32(dr["FaceIndex"] is DBNull ? 0 : dr["FaceIndex"]);
            user.GroupID = dr["GroupID"] is DBNull ? "" : dr["GroupID"].ToString().Trim();
            user.OrderID = Convert.ToInt32(dr["OrderID"] is DBNull ? 0 : dr["OrderID"]);
            user.Power.isAdmin = Convert.ToBoolean(dr["isAdmin"] is DBNull ? false : dr["isAdmin"]);
            user.Power.isBroadcast = Convert.ToBoolean(dr["isBroadcast"] is DBNull ? false : dr["isBroadcast"]);
            user.Power.isSendNotice = Convert.ToBoolean(dr["isSendNotice"] is DBNull ? false : dr["isSendNotice"]);
            user.Power.isSendSMS = Convert.ToBoolean(dr["isSendSMS"] is DBNull ? false : dr["isSendSMS"]);
            user.Power.CreateMaxRooms = Convert.ToInt32(dr["CreateRooms"] is DBNull ? 0 : dr["CreateRooms"]);
            user.LoginInfo.LastIP = dr["LastIP"] is DBNull ? "" : dr["LastIP"].ToString().Trim();
            user.LoginInfo.LastDateTime = dr["LastDateTime"] is DBNull ? "" : dr["LastDateTime"].ToString().Trim();
            //string cardstring = dr["Vcard"] is DBNull ? "<x x=\"\"/>" : dr["Vcard"].ToString().Trim();
            //UserVcard card = Factory.CreateInstanceObject(cardstring) as UserVcard;//鸡生蛋还是蛋生鸡?肯定有一个在前:)
            //if (card != null)//扩展资料，可以添加很多
            //{
            //    user.Mail = card.Mail;
            //    user.OfficePhone = card.OfficePhone;
            //    user.Phone = card.Phone;
            //    user.Post = card.Post;
            //    user.Remark = card.Remark;
            //    user.Job = card.Job;
            //    user.Birthday = card.Birthday;
            //}

            return user;
        }
        #endregion

        #region 将所有数据库中用户基本信息读入内存
        /// <summary>
        /// 将所有数据库中用户基本信息读入内存
        /// </summary>
        /// <returns></returns>
        //private  static Dictionary<string, User> GetUsers()
        //{
        //    Dictionary<string, User>   users = new Dictionary<string, User>();
        //    System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql("select * from users");
        //    if (dr != null)
        //    {
        //        while (dr.Read())
        //        {
        //            User user = new User();

        //            user.UserID = dr["UserID"].ToString().Trim();
        //            user.UserName = dr["UserName"] is DBNull ? user.UserID : dr["UserName"].ToString().Trim();
        //            //user.Sex = Convert.ToByte(dr["Sex"] is DBNull ? 0 : dr["Sex"]);
        //            //user.FaceIndex = Convert.ToInt32(dr["FaceIndex"] is DBNull ? 0 : dr["FaceIndex"]);
        //            user.GroupID = dr["GroupID"] is DBNull ? "" : dr["GroupID"].ToString().Trim();
        //            user.OrderID = Convert.ToInt32(dr["OrderID"] is DBNull ? 0 : dr["OrderID"]);

        //            users.Add(user.UserID, user);
        //        }
        //        dr.Close();
        //    } 
        //    dr.Dispose();
        //    return users;
        //}
        #endregion

        #endregion

        #region 将所有数据库中分组基本信息读入内存
        /// <summary>
        /// 将所有数据库中分组基本信息读入内存
        /// </summary>
        /// <returns></returns>
        public  static Dictionary<string, Group> GetGroups()
        {
            Dictionary<string, Group> Groups = new Dictionary<string, Group>(); 
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql("select * from groups");
            if (dr != null)
            {
                while (dr.Read())
                {
                    Group group = new Group();
                    group.GroupID = dr["GroupID"].ToString().Trim();
                    group.GroupName = dr["GroupName"] is DBNull ? group.GroupID : dr["GroupName"].ToString().Trim();
                    group.SuperiorID = dr["SuperiorID"] is DBNull ? "" : dr["SuperiorID"].ToString().Trim();
                    group.OrderID = Convert.ToInt32(dr["OrderID"] is DBNull ? 0 : dr["OrderID"]);
                   
                    Groups.Add(group.GroupID, group);
                }
                dr.Close();
            }
            dr.Dispose();
            return Groups;
        }
        #endregion

        #region 将所有数据库中群组读入内存
        /// <summary>
        /// 将所有数据库中群组读入内存
        /// </summary>
        /// <returns></returns>
        public  static Dictionary<string, Room> GetRooms()
        {
            Dictionary<string, Room>Rooms = new Dictionary<string, Room>(); 
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql("select * from Rooms");
            if (dr != null)
            {
                while (dr.Read())
                {
                    Room room = new Room();
                    room.RoomID = dr["RoomID"].ToString().Trim();
                    room.RoomName = dr["RoomName"] is DBNull ? room.RoomID : dr["RoomName"].ToString().Trim();
                    room.CreateUserID = dr["CreateUserID"] is DBNull ? "" : dr["CreateUserID"].ToString().Trim();
                    room.UserIDs = dr["Users"] is DBNull ? "" : dr["Users"].ToString().Trim();
                    room.Notice = dr["Notice"] is DBNull ? "" : dr["Notice"].ToString().Trim();
                   
                    Rooms.Add(room.RoomID, room);
                }
                dr.Close();
            }
            dr.Dispose();
            return Rooms;
        }
        #endregion

        #region 重设企业组织架构版本信息

        /// <summary>
        /// 重设企业组织架构版本信息
        /// </summary>
        public static IMLibrary4.Protocol.OrgVersion ResetOrgVersion()
        {
            IMLibrary4.Protocol.OrgVersion OrgVersion = new IMLibrary4.Protocol.OrgVersion();

            ResetGroupsXML(OrgVersion);

            ResetUsersXML(OrgVersion);

            return OrgVersion;
        }

         /// <summary>
         /// 获得DB org信息版本
         /// </summary>
         /// <returns></returns>
        public static IMLibrary4.Organization.DBOrgVersion GetDBOrgVersion()
        {
            IMLibrary4.Organization.DBOrgVersion org = new IMLibrary4.Organization.DBOrgVersion();
            System.Data.SqlClient.SqlDataReader dr = DataAccess.GetReaderBySql("select * from OrgVersion");
            if (dr != null && dr.Read())
            {
                org.GroupsVersion =dr["GroupsVersion"] is DBNull ? "": dr["GroupsVersion"].ToString().Trim();
                org.UsersVersion = dr["UsersVersion"] is DBNull ? "" : dr["UsersVersion"].ToString().Trim();
                dr.Close();
            }
            dr.Dispose();
            return org;
        }

         /// <summary>
         /// 重置用户版本信息
         /// </summary>
         /// <param name="OrgVersion"></param>
        public static void ResetUsersXML( IMLibrary4.Protocol.OrgVersion OrgVersion)
        {
            string UsersVersion = "";
            int sendUsersCount = 80;//每次发送用户信息数量（太多发送不到对方，太少服务器负载增加）
            OrgVersion.UsersXML.Clear();

            Dictionary<string, User> Users = GetUsers();//获得所有用户信息
            if (Users != null)
            {
                IMLibrary4.Protocol.DownloadUsers pUsers = new IMLibrary4.Protocol.DownloadUsers();
                pUsers.type = IMLibrary4.Protocol.type.result;

                int i = 0;
                foreach (User user in Users.Values)
                {
                    pUsers.Data.Add(user);
                    i++;
                    if (i == sendUsersCount)
                    {
                        UsersVersion += IMLibrary4.Protocol.Factory.CreateXMLMsg(pUsers);
                        OrgVersion.UsersXML.Add(IMLibrary4.Protocol.Factory.CreateXMLMsg(pUsers));
                        pUsers.Data = new List<object>();//用户信息清零
                        i = 0;
                    }
                }
                if (pUsers.Data.Count > 0)//剩下的用户信息
                {
                    UsersVersion += IMLibrary4.Protocol.Factory.CreateXMLMsg(pUsers);
                    OrgVersion.UsersXML.Add(IMLibrary4.Protocol.Factory.CreateXMLMsg(pUsers));
                }

                OrgVersion.UsersCount = Users.Count;
                OrgVersion.UsersVersion = IMLibrary4.Security.Hasher.GetMD5Hash(Encoding.Default.GetBytes(UsersVersion));
            }
        }

         /// <summary>
        /// 重置分组版本信息
         /// </summary>
         /// <param name="OrgVersion"></param>
        public static void ResetGroupsXML(  IMLibrary4.Protocol.OrgVersion OrgVersion)
        {
            string GroupsVersion = "";
            int sendGroupsCount = 80;//每次发送分组信息数量（太多发送不到对方，太少服务器负载增加）
            OrgVersion.GroupsXML.Clear();

            Dictionary<string, Group> Groups = GetGroups();//获得所有分组信息
            if (Groups != null)
            {
                IMLibrary4.Protocol.DownloadGroups pGroups = new IMLibrary4.Protocol.DownloadGroups();
                pGroups.type = IMLibrary4.Protocol.type.result;

                int i = 0;
                foreach (Group group in Groups.Values)
                {
                    pGroups.Data.Add(group);
                    i++;
                    if (i == sendGroupsCount)
                    {
                        GroupsVersion += IMLibrary4.Protocol.Factory.CreateXMLMsg(pGroups);
                        OrgVersion.GroupsXML.Add(IMLibrary4.Protocol.Factory.CreateXMLMsg(pGroups));
                        pGroups.Data = new List<object>();//分组信息清零
                        i = 0;
                    }
                }
                if (pGroups.Data.Count > 0)//剩下的分组信息
                {
                    GroupsVersion += IMLibrary4.Protocol.Factory.CreateXMLMsg(pGroups);
                    OrgVersion.GroupsXML.Add(IMLibrary4.Protocol.Factory.CreateXMLMsg(pGroups));
                }

                OrgVersion.GroupsCount = Groups.Count;
                OrgVersion.GroupsVersion = IMLibrary4.Security.Hasher.GetMD5Hash(Encoding.Default.GetBytes(GroupsVersion));
            }
        }
        #endregion

        #region 离线消息管理

        #region 添加离线消息到数据库
        /// <summary>
        /// 添加离线消息到数据库
        /// </summary>
        /// <param name="userID">消息接收者</param>
        /// <param name="msg">消息对像</param>
        public static void addMessageToDB(string userID,Element  msg)
        {
            addMessageToDB(userID,msg.from,msg.to,IMLibrary4.Protocol.Factory.CreateXMLMsg(msg));
        }
          

        /// <summary>
        /// 添加离线消息到数据库(对于循环添加的同一消息，性能较好)
        /// </summary>
        /// <param name="userID">消息接收者</param>
        /// <param name="from">消息发送者</param>
        /// <param name="to">消息编号（群或用户号）</param>
        /// <param name="messageType">消息类型</param>
        /// <param name="xml">消息XML字符串</param>
        public static void addMessageToDB(string userID, string from, string to, string xml)
        {
            string sql = "insert into RecordMsg(userID,froms,tos,datetime,MessageXML) values(@userID,@froms,@tos,@datetime,@MessageXML)";
            System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
                          { new System.Data.SqlClient.SqlParameter("@userID", userID),
                            new System.Data.SqlClient.SqlParameter("@froms", from ),
                            new System.Data.SqlClient.SqlParameter("@tos",  to ),
                            new System.Data.SqlClient.SqlParameter("@datetime", DateTime.Now.ToString()),
                            new System.Data.SqlClient.SqlParameter("@MessageXML",xml),
                          };
            DataAccess.ExecSql(sql, sqlpar);
        }
        #endregion

        #region 获得用户离线消息
        /// <summary>
        /// 获得用户离线消息
        /// </summary>
        /// <param name="userID">用户帐号</param>
        /// <param name="top">消息最后条数</param>
        /// <returns></returns>
        public static List<string> GetOfflineMessage(string userID, int top)
        {
            List<string> messageList = new List<string>();
            string sql = string.Format("select top 100 MessageXML from RecordMsg where userID='{0}'",  userID);

            System.Data.SqlClient.SqlDataReader dr =DataAccess.GetReaderBySql(sql);
            if (dr != null)
            {
                while (dr.Read())
                    messageList.Add(dr["MessageXML"] is DBNull ? "" : dr["MessageXML"].ToString());
                dr.Close();
            }
            dr.Dispose();

            sql = string.Format("delete  from RecordMsg where userID='{0}'", userID);

           DataAccess.ExecSql(sql);
            return messageList;
        }
        #endregion

        #endregion

        #region 离线文件信息存入数据库
        public static void savaOfflineFileInfo(OfflineFileMsg fileMsg)
        {
            string sql = string.Format("update offlineFiles set LastActivityDateTime=GETDATE()  where MD5='{0}'", fileMsg.MD5 + fileMsg.Extension);
            if (DataAccess.ExecSql(sql) == 0)
            {
                sql = "insert into offlineFiles(MD5,fileVcard,LastActivityDateTime) values(@MD5,@fileVcard,GETDATE())";
                System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
                          { new System.Data.SqlClient.SqlParameter("@MD5", fileMsg.MD5 + fileMsg.Extension ),
                            new System.Data.SqlClient.SqlParameter("@fileVcard",Factory.CreateXMLMsg ( fileMsg ) ),
                          };
                DataAccess.ExecSql(sql, sqlpar);
            } 
        }
        #endregion

        #region 群管理

        /// <summary>
        /// 创建群
        /// </summary>
        /// <param name="room">群</param>
        public static bool CreateRoom(IMLibrary4.Organization.Room room)
        {
            try
            {
                string sql = "insert into Rooms(RoomID,RoomName,Notice,Users,CreateUserID,CreateDateTime)"
                    + " values(@RoomID,@RoomName,@Notice,@Users,@CreateUserID,@CreateDateTime)";

                System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
                          { new System.Data.SqlClient.SqlParameter("@RoomID", room.RoomID ),
                            new System.Data.SqlClient.SqlParameter("@RoomName", room.RoomName ),
                            new System.Data.SqlClient.SqlParameter("@Notice", room.Notice ),
                            new System.Data.SqlClient.SqlParameter("@Users", room.UserIDs  ),
                            new System.Data.SqlClient.SqlParameter("@CreateUserID", room.CreateUserID ),
                            new System.Data.SqlClient.SqlParameter("@CreateDateTime", DateTime.Now),
                          };
                DataAccess.ExecSql(sql, sqlpar);
                return true;
            }
          catch  {return false;}
        }

        /// <summary>
        ///  更新群
        /// </summary>
        /// <param name="room">群</param>
        public static bool UpdateRoom(IMLibrary4.Organization.Room room)
        {
            try
            {
                string sql = "update  Rooms set RoomName=@RoomName,Notice=@Notice,Users=@Users" 
                               +  "  where (RoomID=@RoomID)";

                System.Data.SqlClient.SqlParameter[] sqlpar = new System.Data.SqlClient.SqlParameter[]
                          { new System.Data.SqlClient.SqlParameter("@RoomID", room.RoomID ),
                            new System.Data.SqlClient.SqlParameter("@RoomName", room.RoomName ),
                            new System.Data.SqlClient.SqlParameter("@Notice", room.Notice ),
                            new System.Data.SqlClient.SqlParameter("@Users", room.UserIDs  ),
                          };
                DataAccess.ExecSql(sql, sqlpar);
                return true;
            }
            catch { return false; }
        }

        #endregion

   
    }
}
