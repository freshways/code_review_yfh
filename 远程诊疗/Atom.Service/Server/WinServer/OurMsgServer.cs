﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net ;
 
using IMLibrary4;
using IMLibrary4.IO;
using IMLibrary4.Net;
using IMLibrary4.Server;
using IMLibrary4.Organization ;
using IMLibrary4.Protocol;
using IMLibrary4.Security;
using IMLibrary4.Operation;
using IMLibrary4.Net.TCP;
using IMLibrary4.Net.UDP;



namespace OurMsgServer
{ 
    public class Server
    {
        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public Server()
        { 

        } 
        #endregion

        #region 变量
        /// <summary>
        /// 
        /// </summary>
        OurMsgServer.Properties.Settings settings = new OurMsgServer.Properties.Settings();

         
        /// <summary>
        /// 消息服务器
        /// </summary>
        private MsgServer msgServer = null;

        private SQLOperation sqliteOper = new SQLOperation();

        #endregion
         
        #region 开始服务
        /// <summary>
        /// 开始服务
        /// </summary>
        public void Start()
        {
            try
            {
                Start(settings.UdpP2PPort, settings.TcpFilePort, settings.OfflineFilePort, settings.TcpMessagePort);
                sqliteOper.start();
                sqliteOper.iniAdminPower();//初始化admin管理权限为最高权限
                //SQLiteDBHelper.CreateDB("aaa.s3db", settings.SqliteCreateDB);//创建数据库
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        /// <summary>
        /// 开始服务
        /// </summary>
        /// <param name="ImageFileServerPort">图片文件传输服务端口(TCP)</param>
        /// <param name="MessageServerPort">消息服务端口(TCP)</param>
        /// <param name="P2PAVServerPort">音视频服务端口(UDP)</param>
        public void Start(int P2PServerPort, int ImageFileServerPort, int OfflineFilePort, int MessageServerPort)
        {
            ///初始化数据库连接字符串
            //DataAccess.ConnectionString = settings.SqlConStr;

            //启动消息服务
            if (msgServer == null)
            {
                msgServer = new MsgServer();
                msgServer.UserLoginBefore += new MsgServer.ServerEventHandler(msgServer_UserLoginBefore);//用户登录前事件，验证密码
                msgServer.UserLoginIsNotExist += new MsgServer.ServerEventHandler(msgServer_UserLoginIsNotExist);//登录用户不存在事件，开发人员可在事件中将数据库中新用户添加到服务器
                msgServer.UserLoginSuccess += new MsgServer.ServerEventHandler(msgServer_UserLoginSuccess);//用户登录成功事件
                msgServer.UserLoginRequestOfflineMsg += new MsgServer.ServerEventHandler(msgServer_UserLoginRequestOfflineMsg);//用户请求发送离线消息事件
                msgServer.RequestSaveOfflineMsg += new MsgServer.ServerEventHandler(msgServer_RequestSaveOfflineMsg);
                msgServer.UserSaveOfflineFileMsg += new MsgServer.ServerEventHandler(msgServer_UserSaveOfflineFileMsg);
                msgServer.RequestChangePassword += new MsgServer.ServerEventHandler(msgServer_RequestChangePassword);
                msgServer.RequestCreateRoom += new MsgServer.ServerEventHandler(msgServer_RequestCreateRoom);
                msgServer.RequestUpdateRoom += new MsgServer.ServerEventHandler(msgServer_RequestUpdateRoom);
                msgServer.RequestCreateGroup += new MsgServer.ServerEventHandler(msgServer_RequestCreateGroup);
                msgServer.RequestUpdateGroup += new MsgServer.ServerEventHandler(msgServer_RequestUpdateGroup);
                msgServer.RequestDelGroup += new MsgServer.ServerEventHandler(msgServer_RequestDelGroup);
                msgServer.RequestCreateUser += new MsgServer.ServerEventHandler(msgServer_RequestCreateUser);
                msgServer.RequestUpdateUser += new MsgServer.ServerEventHandler(msgServer_RequestUpdateUser);
                msgServer.RequestDelUser += new MsgServer.ServerEventHandler(msgServer_RequestDelUser);
                msgServer.RequestUserCard += new MsgServer.ServerEventHandler(msgServer_RequestUserCard);
            }

            /// 加载所有用户信息,即将数据库中所有用户基本信息添加到服务器内存，如果不执行此操作，所有用户无法登录
            msgServer.LoadUsers(sqliteOper.GetUsers());
            ///加载所有分组信息,即将数据库中所有用户基本信息添加到服务器内存，如果不执行此操作，所有用户无法登录
            msgServer.LoadGroups(sqliteOper.GetGroups());
            ///加载所有群信息
            msgServer.LoadRooms(sqliteOper.GetRooms());

            ///启动服务器(包括视频服务器、文件服务器、聊天服务器、图片服务器等)
            msgServer.Start(P2PServerPort, ImageFileServerPort, OfflineFilePort, MessageServerPort);
        }

        void msgServer_RequestUserCard(object sender, ServerEventArgs e)
        {
            e.obj = sqliteOper.getUserVcard(e.RequestUser.UserID);
            e.Allow = true;////标记数据已经从数据库中查询
        }

        void msgServer_RequestDelUser(object sender, ServerEventArgs e)
        {
            sqliteOper.DelUser((e.obj as EditUserData).UserID);
            e.Allow = true;//标记数据已经从数据库中删除，否则服务器不删除内存中的用户
        }

        void msgServer_RequestUpdateUser(object sender, ServerEventArgs e)
        {
            sqliteOper.UpdateUser(e.RequestUser, e.obj as UserVcard);
            e.Allow = true;//标记数据已经更新到数据库，否则服务器不更新内存用户 
        }

        void msgServer_RequestCreateUser(object sender, ServerEventArgs e)
        {
            sqliteOper.AddUser(e.RequestUser, e.obj as UserVcard);
            e.Allow = true;//标记数据已经存入数据库，否则服务器不添加新用户到服务内存
        }



        #endregion

        #region 停止服务
        /// <summary>
        /// 停止服务
        /// </summary>
        public void Stop()
        {
            if (msgServer != null)
            {
                msgServer.Stop();
                //释放事件产生的资源
                msgServer.UserLoginBefore -= new MsgServer.ServerEventHandler(msgServer_UserLoginBefore);//用户登录前事件，验证密码
                msgServer.UserLoginIsNotExist -= new MsgServer.ServerEventHandler(msgServer_UserLoginIsNotExist);//登录用户不存在事件，开发人员可在事件中将数据库中新用户添加到服务器
                msgServer.UserLoginSuccess -= new MsgServer.ServerEventHandler(msgServer_UserLoginSuccess);//用户登录成功事件
                msgServer.UserLoginRequestOfflineMsg -= new MsgServer.ServerEventHandler(msgServer_UserLoginRequestOfflineMsg);//用户请求发送离线消息事件
                msgServer.RequestSaveOfflineMsg -= new MsgServer.ServerEventHandler(msgServer_RequestSaveOfflineMsg);
                msgServer.UserSaveOfflineFileMsg -= new MsgServer.ServerEventHandler(msgServer_UserSaveOfflineFileMsg);
                msgServer.RequestChangePassword -= new MsgServer.ServerEventHandler(msgServer_RequestChangePassword);
                msgServer.RequestCreateRoom -= new MsgServer.ServerEventHandler(msgServer_RequestCreateRoom);
                msgServer.RequestUpdateRoom -= new MsgServer.ServerEventHandler(msgServer_RequestUpdateRoom);
                msgServer.RequestCreateGroup -= new MsgServer.ServerEventHandler(msgServer_RequestCreateGroup);
                msgServer.RequestUpdateGroup -= new MsgServer.ServerEventHandler(msgServer_RequestUpdateGroup);
                msgServer.RequestDelGroup -= new MsgServer.ServerEventHandler(msgServer_RequestDelGroup);
                msgServer.RequestCreateUser -= new MsgServer.ServerEventHandler(msgServer_RequestCreateUser);
                msgServer.RequestUpdateUser -= new MsgServer.ServerEventHandler(msgServer_RequestUpdateUser);
                msgServer.RequestDelUser -= new MsgServer.ServerEventHandler(msgServer_RequestDelUser);
                msgServer.RequestUserCard -= new MsgServer.ServerEventHandler(msgServer_RequestUserCard);
                msgServer = null;
            }
            sqliteOper.stop();
        }
        #endregion 

        #region 用户登录成功事件
        void msgServer_UserLoginSuccess(object sender, ServerEventArgs e)
        {
            sqliteOper.UpdateUserLoginInfo(e.RequestUser.UserID, e.RequestUser.LoginInfo);
        }
        #endregion

        #region 管理员删除分组事件
        void msgServer_RequestDelGroup(object sender, ServerEventArgs e)
        {
            Group group = e.obj as Group;
            sqliteOper.DelGroup(group.GroupID);
            e.Allow = true;
        }
        #endregion

        #region 管理员更新分组事件
        void msgServer_RequestUpdateGroup(object sender, ServerEventArgs e)
        {
            Group group = e.obj as Group;
            sqliteOper.UpdateGroup(group);
            e.Allow = true;
        }
        #endregion

        #region 管理员添加分组事件
        void msgServer_RequestCreateGroup(object sender, ServerEventArgs e)
        {
            Group group = e.obj as Group;
            sqliteOper.AddGroup(group);
            e.Allow = true;
        }
        #endregion

        #region 保存用户更新群的信息到数据库事件
        void msgServer_RequestUpdateRoom(object sender, ServerEventArgs e)
        {
            Room r = e.obj as Room;
            if (r == null) return;
            sqliteOper.saveUpdateRoom(r.RoomID, e.XMLMsg);
        }
        #endregion

        #region 保存用户新创建群的信息到数据库事件
        void msgServer_RequestCreateRoom(object sender, ServerEventArgs e)
        {
            Room r = e.obj as Room;
            if (r == null) return;
            sqliteOper.saveCreateNewRoom(r.RoomID, e.XMLMsg);
            e.Allow = true;//标明已经存入数据库，以便服务器通知用户群创建成功.
        }
        #endregion

        #region 更新用户密码
        void msgServer_RequestChangePassword(object sender, ServerEventArgs e)
        {
            EditPassword cPWD = e.obj as EditPassword;
            if (cPWD == null) return;

            if (e.RequestUser.Power.isAdmin || sqliteOper.IsValidPassword(cPWD.from, cPWD.OldPassword))//如果用户是管理员
            {
                sqliteOper.updatePWD(cPWD.from, cPWD.NewPassword);//更新密码
                e.Allow = true;//标明密码已经更新成功，否则不成功
            }
        }
        #endregion

        #region 服务器请求保存用户离线消息事件
        void msgServer_RequestSaveOfflineMsg(object sender, ServerEventArgs e)
        {
            sqliteOper.addOperation(new Operation(e.RequestUser.UserID,Action.insert, Table.msgRecord, e.obj)); 
        }
        #endregion

        #region 登录用户不存在事件，开发人员可在事件中将数据库中新用户添加到服务器(将事件参数 e.Allow 标识为true),如果不添加，用户将无法登录服务器
        void msgServer_UserLoginIsNotExist(object sender, ServerEventArgs e)
        {
            //Auth auth = e.obj as Auth;
            ////DBHelper.AddUserToUsers(msgServer.Users, auth.UserID) ;
            //e.Allow = true;//标识不存在的用户已经从数据库中添加到服务器
        }
        #endregion

        #region 用户登录前事件,开发人员可将数据库中的用户密码与登录用户的密码正行校验(将事件参数 e.Allow 标识为true)后用户才能登录
        void msgServer_UserLoginBefore(object sender, ServerEventArgs e)
        {
            Auth auth = e.obj as Auth;
            if (sqliteOper.IsValidPassword(auth.UserID, auth.Password))//如果密码正确
                e.Allow = true  ;//标识密码正确，允许登录
        }
        #endregion

        #region 用户请求发送离线消息事件，开发人员可在事件中将数据中保存的用户的离线消息字符串发送给用户（将事件参数 e.Allow 标识为true，e.obj = offlineMsg）
        void msgServer_UserLoginRequestOfflineMsg(object sender, ServerEventArgs e)
        {
            //发送离线消息
            List<string> offlineMsg = sqliteOper.GetOfflineMessage(e.RequestUser.UserID, 100);//获取最后50条离线消息
            if (offlineMsg == null) return;
            e.obj = offlineMsg;//将所有离线消息以 List<string>对像赋值给事件参数obj即可
            e.Allow = true;//标识后服务器认为获得了用户离线消息，才能发送给用户
        }
        #endregion

        #region 用户要求保存离线文件信息及消息,开发人员务必保留或重写此功能，以便用户上线能收到离线文件发送消息
        void msgServer_UserSaveOfflineFileMsg(object sender, ServerEventArgs e)
        {
            //将文件信息存入数据库，以便文件保存服务器7天之后删除。
            sqliteOper.SavaOfflineFileInfo(e.RequestUser.UserID, e.obj as OfflineFileMsg);
        }
        #endregion

    }
}
