﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using IMLibrary4;
using IMLibrary4.IO;
using IMLibrary4.Net;
using IMLibrary4.Server;
using IMLibrary4.Organization;
using IMLibrary4.Protocol;
using IMLibrary4.Security;
using IMLibrary4.Operation;
using IMLibrary4.Net.TCP;
using IMLibrary4.Net.UDP; 

namespace OurMsgServer
{
    /// <summary>
    /// 消息服务器
    /// </summary>
    public class MsgServer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Port"></param>
        public MsgServer()
        {

        }

        #region 变量
        /// <summary>
        /// 组织机构版本号
        /// </summary>
        private IMLibrary4.Protocol.OrgVersion orgVersion = new OrgVersion();

        /// <summary>
        /// TCP服务(消息服务)
        /// </summary>
        private TCPServer tcpMessageServer = null;

        /// <summary>
        /// 所有用户列表
        /// </summary>
        private Dictionary<string, ServerUser> Users = null;

        /// <summary>
        /// 所有分组信息
        /// </summary>
        private Dictionary<string, Group> Groups = null;

        /// <summary>
        /// 群组列表
        /// </summary>
        private Dictionary<string, Room> Rooms = null; 
        /// <summary>
        /// 图片文件传输服务器
        /// </summary>
        private ImageFileServer imageFileServer = null;
        /// <summary>
        /// 文件、音视频、远程协助等P2P应用传输服务器
        /// </summary>
        private UDPP2PServer UdpP2PServer = null;
        /// <summary>
        /// 离线传输文件服务器
        /// </summary>
        private OfflineFileServer offlineFileServer = null;

        private int UDPP2PServerPort = 5556;
        private int TCPImageFileServerPort = 5557;
        private int TCPOfflineFilePort = 5558;
        private int TCPMessageServerPort = 5559;
        #endregion

        #region 事件
      
        public delegate void ServerEventHandler(object sender, ServerEventArgs e);
        /// <summary>
        /// 用户登录之前事件
        /// </summary>
        public event ServerEventHandler UserLoginBefore;
        /// <summary>
        /// 登录用户不存事件
        /// </summary>
        public event ServerEventHandler UserLoginIsNotExist;
        /// <summary>
        /// 用户登录成功事件
        /// </summary>
        public event ServerEventHandler UserLoginSuccess;
        /// <summary>
        /// 登录用户请求获取离线消息事件
        /// </summary>
        public event ServerEventHandler UserLoginRequestOfflineMsg;
        /// <summary>
        /// 服务器请求保存用户离线消息事件
        /// </summary>
        public event ServerEventHandler RequestSaveOfflineMsg ;
        /// <summary>
        /// 在线状态更改事件
        /// </summary>
        public event ServerEventHandler Presence;
        /// <summary>
        /// 请求创建新用户事件
        /// </summary>
        public event ServerEventHandler RequestCreateUser;
        /// <summary>
        /// 请求更新用户资料事件
        /// </summary>
        public event ServerEventHandler RequestUpdateUser;
        /// <summary>
        /// 请求删除用户资料事件
        /// </summary>
        public event ServerEventHandler RequestDelUser;
        /// <summary>
        /// 请求获取用户card事件
        /// </summary>
        public event ServerEventHandler RequestUserCard;


        /// <summary>
        /// 用户要求保存创建群信息到数据库事件
        /// </summary>
        public event ServerEventHandler RequestCreateRoom;
        /// <summary>
        /// 用户要求保存更新群后的信息到数据库事件
        /// </summary>
        public event ServerEventHandler RequestUpdateRoom;
        /// <summary>
        /// 转发图片文件下载消息
        /// </summary>
        public event ServerEventHandler RouteDownLoadImageFile;
        /// <summary>
        /// 转发P2P文件传输消息
        /// </summary>
        public event ServerEventHandler RouteP2PFileTransmit;
        /// <summary>
        /// 转发MPG4编解码的视频对话消息
        /// </summary>
        public event ServerEventHandler RouteAVMsg;
        /// <summary>
        /// 管理员请求创建、更新用户信息
        /// </summary>
        public event ServerEventHandler RequestChangeUserVcard;
        /// <summary>
        /// 管理员请求保存新创建的分组信息
        /// </summary>
        public event ServerEventHandler RequestCreateGroup;
        /// <summary>
        /// 管理员请求保存更新的分组信息
        /// </summary>
        public event ServerEventHandler RequestUpdateGroup;
        /// <summary>
        /// 管理员请求删除分组信息
        /// </summary>
        public event ServerEventHandler RequestDelGroup;
        /// <summary>
        /// 管理员请求修改密码
        /// </summary>
        public event ServerEventHandler RequestChangePassword;
        /// <summary>
        /// 用户下线事件
        /// </summary>
        public event ServerEventHandler UserOffline;
        /// <summary>
        /// 用户要求下次接收离线文件事件
        /// </summary>
        public event ServerEventHandler UserSaveOfflineFileMsg;

        #endregion

        #region 私有方法
        #region TCP发送消息
        /// <summary>
        /// 向在线用户广播消息
        /// </summary>
        /// <param name="e">Element</param>
        private void BroadcastingMessage(object e)
        {
            string xmlmsg = Factory.CreateXMLMsg(e);
            tcpMessageServer.BroadcastingMessage(xmlmsg);
        }

        /// <summary>
        /// 向在线用户广播消息
        /// </summary>
        /// <param name="XMLMsg"></param>
        private void BroadcastingMessage(string XMLMsg)
        {
            tcpMessageServer.BroadcastingMessage(XMLMsg);
        }


        /// <summary>
        /// 发送消息给一个用户
        /// </summary>
        /// <param name="Session">用户</param>
        /// <param name="Message">消息文本</param>
        private void SendMessage(TCPServerSession Session, string XMLMsg)
        {
            tcpMessageServer.SendMessageToSession(Session, XMLMsg);
        }

        /// <summary>
        /// 发送消息给一个用户
        /// </summary>
        /// <param name="Session">TCP客户端</param>
        /// <param name="e">object</param>
        private void SendMessage(TCPServerSession Session, object e)
        {
            tcpMessageServer.SendMessageToSession(Session, Factory.CreateXMLMsg(e));
        }
        #endregion

        #endregion

        #region 公共方法

        #region 开始服务
        /// <summary>
        /// 开始服务
        /// </summary> 
        /// <param name="UDPP2PServerPort">文件、音视频、远程协助等P2P应用传输服务器UDP端口</param>
        /// <param name="TCPImageFileServerPort">截图文件服务TCP中转端口</param>
        /// <param name="TCPOfflineFilePort">离线文件服务TCP端口</param>
        /// <param name="TCPMessageServerPort">消息服务TCP端口</param>
        public void Start(int UDPP2PServerPort, int TCPImageFileServerPort, int TCPOfflineFilePort, int TCPMessageServerPort)
        {
            this.UDPP2PServerPort = UDPP2PServerPort;
            this.TCPImageFileServerPort = TCPImageFileServerPort;
            this.TCPOfflineFilePort = TCPOfflineFilePort;
            this.TCPMessageServerPort = TCPMessageServerPort;

            if (tcpMessageServer == null)
            {
                tcpMessageServer = new TCPServer();
                tcpMessageServer.SessionCreated += new EventHandler<TCP_ServerSessionEventArgs<TCPServerSession>>(tcpMessageServer_SessionCreated);
                tcpMessageServer.Bindings = new IPBindInfo[] { new IPBindInfo("127.0.0.1", BindInfoProtocol.TCP, IPAddress.Any, TCPMessageServerPort) };
            }
            tcpMessageServer.Start();


            //启动图片文件服务
            if (imageFileServer == null) imageFileServer = new ImageFileServer(TCPImageFileServerPort);
            imageFileServer.Start();

            //启动文件、音视频、远程协助等P2P应用传输服务器 
            if (UdpP2PServer == null) UdpP2PServer = new UDPP2PServer(UDPP2PServerPort);
            UdpP2PServer.Start();

            //启动离线文件传输服务
            if (offlineFileServer == null) offlineFileServer = new OfflineFileServer(TCPOfflineFilePort);
            offlineFileServer.FileUploaded += new OfflineFileServer.offlineFileEventHandler(offlineFileServer_FileUploaded);
            offlineFileServer.FileDownloaded += new OfflineFileServer.offlineFileEventHandler(offlineFileServer_FileDownloaded);
            offlineFileServer.Start();
        }
        #endregion

        #region 释放资源
        /// <summary>
        /// 释放资源
        /// </summary>
        private  void Dispose()
        {
            if (tcpMessageServer != null && tcpMessageServer.IsRunning)
            {
                tcpMessageServer.Stop();
                tcpMessageServer.Dispose();
                tcpMessageServer = null;
            }
        }
        #endregion

        #region 停止服务
        /// <summary>
        /// 停止服务
        /// </summary>
        public void Stop()
        {
            if (tcpMessageServer != null && tcpMessageServer.IsRunning)
            {
                tcpMessageServer.SessionCreated -= new EventHandler<TCP_ServerSessionEventArgs<TCPServerSession>>(tcpMessageServer_SessionCreated);
                tcpMessageServer.Stop();
            }

            imageFileServer.Stop();
            UdpP2PServer.Stop();

            if (offlineFileServer != null)
            { 
                offlineFileServer.FileUploaded -= new OfflineFileServer.offlineFileEventHandler(offlineFileServer_FileUploaded);
                offlineFileServer.FileDownloaded -= new OfflineFileServer.offlineFileEventHandler(offlineFileServer_FileDownloaded);
                offlineFileServer.Stop();
            }

            this.Dispose();
            this.Users.Clear();
            this.Users = null;
            this.Rooms.Clear();
            this.Rooms = null;
           
        }
        #endregion

        #region 获得客户端连接套接字集合
        public TCP_ServerSession[] GetTcpServerSession()
        {
            return tcpMessageServer.Sessions.ToArray();
        }
        #endregion

        #region TCP发送消息

        /// <summary>
        /// 向在线用户广播消息
        /// </summary>
        /// <param name="e">Element</param>
        public void BroadcastingMessageToOnlineUser(Element e)
        {
            BroadcastingMessage(e);
        }

        /// <summary>
        /// 向在线用户广播消息
        /// </summary>
        /// <param name="XMLMsg"></param>
        public void BroadcastingMessageToOnlineUser(string XMLMsg)
        {
            BroadcastingMessage(XMLMsg);
        }


        /// <summary>
        /// 发送消息给一个用户
        /// </summary>
        /// <param name="userID">用户编号</param>
        /// <param name="Message">消息文本</param>
        private void SendMessageToUser(string userID, string XMLMsg)
        {
            User user = getUser(userID);
            if (user != null)
                SendMessageToUser(user, XMLMsg);
        }

        /// <summary>
        /// 发送消息给一个用户
        /// </summary>
        /// <param name="userID">用户编号</param>
        /// <param name="e">Element</param>
        private void SendMessageToUser(string userID, Element e)
        {
           User user = getUser(userID);
            if (user != null)
                SendMessageToUser(user, e);
        }


        /// <summary>
        /// 发送消息给一个用户
        /// </summary>
        /// <param name="user">用户</param>
        /// <param name="Message">消息文本</param>
        public void SendMessageToUser(User user, string XMLMsg)
        {
            if (user != null && user.Tag is TCPServerSession)
            {
                SendMessage(user.Tag as TCPServerSession, XMLMsg);
            }
        }

        /// <summary>
        /// 发送消息给一个用户
        /// </summary>
        /// <param name="user">消息接收者</param>
        /// <param name="e">消息对像</param>
        public void SendMessageToUser(User user, Element e)
        {
            if (user.ShowType ==  ShowType.Offline && e.IsSaveAsOfflineMsg)
            {
                if (RequestSaveOfflineMsg != null)
                    RequestSaveOfflineMsg(this, new ServerEventArgs(true, Factory.CreateXMLMsg(e), e, (ServerUser)user));
            }
            if (user != null && user.Tag is TCP_ServerSession)
            {
                SendMessage(user.Tag as TCPServerSession, e);
            }
        }

        /// <summary>
        /// 发送消息到群
        /// </summary>
        /// <param name="fromUserID">消息发送者编号</param>
        /// <param name="roomID">消息接收群编号</param>
        /// <param name="e">消息对像</param>
        public void SendMessageToRoom(string fromUserID, string roomID, Element e)
        {
            Room room = getRoom(roomID);
            if (room == null) return;//如果群不存在

            SendMessageToRoom(fromUserID, room, e);
        }



        /// <summary>
        /// 发送消息到群
        /// </summary>
        /// <param name="fromUserID">消息发送者</param>
        /// <param name="room">消息接收群</param>
        /// <param name="e">消息对像</param>
        public void SendMessageToRoom(string fromUserID, Room room, Element e)
        {
            if (!room.Users.ContainsKey(fromUserID)) return;//如果群中无此用户,则不发送消息
            string XMLMsg = Factory.CreateXMLMsg(e);
            foreach (ServerUser user in GetUsers(room))
            {
                if (user.ShowType ==  ShowType.Offline && e.IsSaveAsOfflineMsg)
                {
                    if (RequestSaveOfflineMsg != null)
                        RequestSaveOfflineMsg(this, new ServerEventArgs(true, XMLMsg, e, user));
                }
                else
                {
                    SendMessageToUser(user, XMLMsg);
                }
            }
        }
        #endregion

        #region 加载所有用户信息
        /// <summary>
        /// 加载所有用户信息
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public bool LoadUsers(Dictionary<string, ServerUser> users)
        {
            if (users != null)
            {
                Users = users;
                ResetUsersVersion(users);
                Console.WriteLine("用户数：" + Users.Count.ToString());
                return true;
            }
            else
                return false;

        }
        #endregion 

        #region 加载分组信息
        /// <summary>
        /// 加载分组信息
        /// </summary>
        /// <param name="groups"></param>
        /// <returns></returns>
        public bool LoadGroups(Dictionary<string, Group> groups)
        {
            if (groups != null)
            {
                Groups = groups;
                ResetGroupsVersion(groups);
                Console.WriteLine("分组数："+Groups.Count.ToString());
                return true;
            }
            else
                return false;

        }
        #endregion

        #region 重置用户版本信息
        /// <summary>
        /// 重置用户版本信息
        /// </summary>
        /// <param name="users"></param>
        private void ResetUsersVersion(Dictionary<string, ServerUser> users)
        {
            string UsersVersion = "";
            int sendUsersCount = 80;//每次发送用户信息数量（太多发送不到对方，太少服务器负载增加）
            orgVersion.UsersXML.Clear();

            if (users != null)
            {
                IMLibrary4.Protocol.DownloadUsers downloadUsers = new IMLibrary4.Protocol.DownloadUsers();
                downloadUsers.type =  type.result;

                int i = 0;
                foreach (User user in users.Values)
                {
                    UsersVersion += user.UserID + user.UserName + user.OrderID + user.GroupID;
                    downloadUsers.Data.Add(user);
                    i++;
                    if (i == sendUsersCount)
                    {
                        orgVersion.UsersXML.Add(Factory.CreateXMLMsg(downloadUsers));
                        downloadUsers.Data = new List<object>();//用户信息清零
                        i = 0;
                        UsersVersion = IMLibrary4.Security.Hasher.GetMD5Hash(Encoding.Default.GetBytes(UsersVersion));
                    }
                }
                if (downloadUsers.Data.Count > 0)//剩下的用户信息
                {
                    orgVersion.UsersXML.Add(Factory.CreateXMLMsg(downloadUsers));
                    UsersVersion = IMLibrary4.Security.Hasher.GetMD5Hash(Encoding.Default.GetBytes(UsersVersion));
                }

                orgVersion.UsersCount = Users.Count;
                orgVersion.UsersVersion = UsersVersion;
            }
        }
        #endregion

        #region 重置分组版本信息
        /// <summary>
        /// 重置分组版本信息
        /// </summary>
        /// <param name="OrgVersion"></param>
        public void ResetGroupsVersion(Dictionary<string, Group> groups)
        {
            string GroupsVersion = "";
            int sendGroupsCount = 80;//每次发送分组信息数量（太多发送不到对方，太少服务器负载增加）
            orgVersion.GroupsXML.Clear();

            if (groups != null)
            {
                IMLibrary4.Protocol.DownloadGroups downloadGroups = new IMLibrary4.Protocol.DownloadGroups();
                downloadGroups.type =  type.result;

                int i = 0;
                foreach (Group group in groups.Values)
                {
                    downloadGroups.Data.Add(group);
                    i++;
                    if (i == sendGroupsCount)
                    {
                        var s = Factory.CreateXMLMsg(downloadGroups);
                        orgVersion.GroupsXML.Add(s);
                        downloadGroups.Data = new List<object>();//分组信息清零
                        i = 0;
                        GroupsVersion += s;
                        GroupsVersion =IMLibrary4.Security.Hasher.GetMD5Hash(Encoding.Default.GetBytes(GroupsVersion));
                    }
                }
                if (downloadGroups.Data.Count > 0)//剩下的分组信息
                {
                    var s = Factory.CreateXMLMsg(downloadGroups);
                    orgVersion.GroupsXML.Add(s);
                    GroupsVersion += s;
                    GroupsVersion = IMLibrary4.Security.Hasher.GetMD5Hash(Encoding.Default.GetBytes(GroupsVersion));
                }

                orgVersion.GroupsCount = Groups.Count;
                orgVersion.GroupsVersion = GroupsVersion;

            }
        }
        #endregion

        #region 加载群信息
        /// <summary>
        /// 加载群信息
        /// </summary>
        /// <param name="rooms"></param>
        public void LoadRooms(Dictionary<string, Room> rooms)
        {
            if (rooms != null)
                Rooms = rooms;
            else
                return;

            foreach (Room room in rooms.Values)
            {
                #region 将内存中的用户添加到内存中的群
                string[] userids = room.UserIDs.Split(';');
                foreach (string userID in userids)
                {
                    ServerUser user = getUser(userID);
                    if (user != null)
                    {
                        if (room.Users == null) room.Users = new Dictionary<string, User>();
                        if (!room.Users.ContainsKey(userID))
                        {
                            if (room.CreateUserID == userID) user.Power.CreatedRoomsCount++;//标记用户已创建群数
                            room.Users.Add(user.UserID, user);
                        }

                        if (user.Rooms == null) user.Rooms = new Dictionary<string, Room>();
                        if (!user.Rooms.ContainsKey(room.RoomID))
                            user.Rooms.Add(room.RoomID, room);
                    }
                }

                room.UserIDs = "";//重新生成群包含的Users
                foreach (ServerUser u in GetUsers(room))
                    room.UserIDs += u.UserID + ";";

                #endregion
            }
        }
        #endregion

        #endregion

        #region TCP创建连接
        private void tcpMessageServer_SessionCreated(object sender, TCP_ServerSessionEventArgs<TCPServerSession> e)
        {
            e.Session.PacketReceived +=(s,e1)=> messageSession_PacketReceived(s,e1);
            e.Session.Disonnected += (s, e1) => Session_Disonnected(s, e1);
        }
        #endregion

        #region TCP连接断开事件
        /// <summary>
        /// TCP连接断开事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Session_Disonnected(object sender, EventArgs e)
        {
            TCPServerSession session = sender as TCPServerSession;
            if (session != null && session.IsAuthenticated)//如果该用户已经成功验证登录
            {
                ServerUser user = session.Tag as ServerUser;
                if (user != null)
                {
                    Presence presence = new Presence();
                    presence.from = user.UserID;
                    presence.ShowType = ShowType.Offline;
                    user.ShowType =  ShowType.Offline;

                    BroadcastingMessageToOnlineUser(presence);//告诉在线者用户已经离线

                    if (UserOffline != null)
                        UserOffline(this, new  ServerEventArgs(true, "", null, user));//触发用户离线事件
                }
            }
        }
        #endregion

        #region TCP连接数据包到达事件
        /// <summary>
        /// TCP连接数据包到达事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void messageSession_PacketReceived(object sender, TcpSessionEventArgs e)
        {
            Console.WriteLine(e.Data);
            if (e.Data.Length < 2) return;

            TCPServerSession Session = sender as TCPServerSession;
            object obj = Factory.CreateInstanceObject(e.Data);

            if (obj != null)//如果收到的消息对像不为空
            {
                if (obj is Auth)//登录请求
                {
                    OnLogin( Session, e.Data, obj, null);
                    return;
                }
                else if (Session.IsAuthenticated)//如果提供其他服务客户端必须是已经进行验证后
                {

                    ServerUser RequestUser = Session.Tag as ServerUser;//获得请求用户
                    if (RequestUser == null) return;//如果用户为空，退出

                    if (obj is Message)//请求路由聊天消息
                    {
                        OnMessage(Session, e.Data, obj, RequestUser);
                        return;
                    }

                    if (obj is Presence)//请求在线状态
                    {
                        if ((obj as Presence).type == type.set)//设置在线状态
                        {
                            OnPresence(Session, e.Data, obj, RequestUser);
                        }
                        else if ((obj as Presence).type == type.get)//获得联系人在线状态
                        {
                            OnRequestUsersPresence(Session, e.Data, obj, RequestUser);
                        }
                        return;
                    }

                    if (obj is DownloadGroups)//请求下载的分组信息
                    {
                        OnRequestOrgGroups(Session, e.Data, obj, RequestUser);
                        return;
                    }
                    else if (obj is DownloadUsers)//请求下载用户信息
                    {
                        OnRequestOrgUsers(Session, e.Data, obj, RequestUser);
                        return;
                    }
                    else if (obj is DownloadRooms)//请求下载群信息
                    {
                        OnRequestOrgRooms(Session, e.Data, obj, RequestUser);
                        return;
                    }


                    if (obj is EditRoom)//更新群信息
                    {
                        OnRequestChangRoom(Session, e.Data, obj, RequestUser);
                        return;
                    }
                    else if (obj is OfflineFileMsg)//用户要求下次接收离线文件事件
                    {
                        OnUserSaveOfflineFileMsg(Session, e.Data, obj, RequestUser);
                        return;
                    }
                    else if (obj is ImageFileMsg)//通知客户端到文件服务器下载已上传完成的文件
                    {
                        OnRouteDownLoadImageFile(Session, e.Data, obj, RequestUser);
                        return;
                    }
                    else if (obj is P2PFileMsg)//转发文件传输消息
                    {
                        OnRouteP2PFileTransmit(Session, e.Data, obj, RequestUser);
                        return;
                    }
                    else if (obj is AVMsg || obj is RemoteDesktopMsg)//转发音视频对话的消息
                    {
                        OnRouteMsg(Session, e.Data, obj, RequestUser);
                        return;
                    }

                    #region 组织架构信息管理
                    if (obj is EditUserData)//如果是管理员变更用户信息资料
                    {
                        OnRequestEditUserData(Session, e.Data, obj, RequestUser);
                    }
                    else if (obj is EditGroupData)//如果是管理员变更分组信息资料
                    {
                        OnRequestChangeGroupVcard(Session, e.Data, obj, RequestUser);
                    }
                    if (obj is EditPassword)//如果是要求修改密码
                    {
                        OnRequestChangePassword(Session, e.Data, obj, RequestUser);
                    }
                    #endregion

                }
            }
            else //收到非法消息
                OnBadCommand(Session);
        }
        #endregion

        #region TCP处理非法入侵
        /// <summary>
        /// 处理非法入侵
        /// </summary>
        /// <param name="session"></param>
        private void OnBadCommand(TCPServerSession session)
        {
            session.BadCmd++;
            if (session.BadCmd > 3)//4不过3，如果收到坏消息大于3条，则视为非法攻击，断掉TCP连接
            {
                throw new Exception("非法消息大于三！");
                session.Disconnect();
                session.Dispose();
            }
        }
        #endregion

        #region 逻辑处理

        #region 登录事件
        void OnLogin(TCPServerSession Session, string XMLMsg, object obj, ServerUser RequestUser)
        {
            Auth auth = obj as Auth;
            ServerUser user = getUser(auth.UserID);

            #region 如果登录用户不存在
            if (user == null)//如果登录用户不存在
                if (UserLoginIsNotExist != null)
                {
                    var e1 = new ServerEventArgs(false, XMLMsg, obj, null);
                    UserLoginIsNotExist(this, e1);//触发登录用户不存在事件，以便开发人员从数据库中将新用户添加到服务器
                    if (e1.Allow)//如果不存在的用户已经从数据库中添加,则继续登录操作
                    {
                        user = getUser(auth.UserID);
                        if (user != null)
                        {
                            //user.type = type.New;//通知所有客户端，有新用户加入组织机构
                            //BroadcastingMessageToOnlineUser(Factory.CreateXMLMsg(user));//广播所有在线用户,通知所有客户端有新用户加入组织机构
                        }
                    }
                }
            #endregion

            #region 如果用户存在未禁用且密码正确
            if (user != null && !user.Power.Disable) //如果用户存在未禁用且密码正确 Hasher.GetMD5Hash(TextEncoder.textToBytes(auth.Password)) 
            {
                if (UserLoginBefore != null)
                {
                    var e2 = new ServerEventArgs(false, XMLMsg, obj, user);
                    UserLoginBefore(this, e2);//触发用户登录前事件

                    if (e2.Allow)//如果密码正确，执行登录操作
                    {

                        #region 重复登录或异地登录
                        if (user.Tag != null && user.Tag is TCPServerSession)
                        {
                            ///通知用户此帐号异地登录
                            auth.type = type.Else;
                            auth.Password = Session.ID;
                            TCPServerSession se = user.Tag as TCPServerSession;
                            if (se != null && !se.IsDisposed && se.IsConnected)
                            {
                                SendMessageToUser(user, auth);
                                se.Disconnect();//断开连接，释放资源
                            }
                        }
                        #endregion

                        #region 不能修改这个代码
                        Session.Tag = user;//这个代码是必须的
                        Session.IsAuthenticated = true;//这个代码是必须的
                        #endregion

                        user.Tag = Session;
                        user.ShowType = auth.ShowType;
                        user.Status = auth.Status;

                        ///发送登录成功消息
                        auth.ID = user.ID;//告之登录用户在数据中的ID值（可以不需要使用此属性，用于特殊需求的用户）
                        auth.UserName = user.UserName;//告之登录用户的姓名
                        auth.type = type.result;//告之登录用户成功登录
                        auth.Password = Session.ID;//告之登录用户在服务器的SESSION
                        auth.LastDateTime = user.LoginInfo.LastDateTime;//告之登录用户上次登录时间
                        auth.LastIP = user.LoginInfo.LastIP;//告之登录用户上次登录IP
                        auth.FileServerTCPPort = this.TCPImageFileServerPort;//告之登录用户TCP图片文件传输服务端口
                        auth.FileServerUDPPort = this.UDPP2PServerPort;//告之登录用户UDP文件传输服务端口
                        auth.AVServerUDPPort = this.UDPP2PServerPort;//告之登录用户UDP音视频传输服务端口
                        auth.OfflineFileServerTCPPort = this.TCPOfflineFilePort;////告之登录用户离线文件传输服务端口
                        auth.isAdmin = user.Power.isAdmin;//告之登录用户是否管理员
                        auth.isBroadcast = user.Power.isBroadcast;//告之登录用户是否可以发送广播消息
                        auth.isSendNotice = user.Power.isSendNotice;//告之登录用户是否可以发送通知消息
                        auth.isSendSMS = user.Power.isSendSMS;//告之登录用户是否可以发送手机短信
                        auth.isControlClient = user.Power.isControlClient;//告之登录用户是否可以控制客户端
                        auth.UsersCount = orgVersion.UsersCount;//告之登录用户当前服务器中组织机构用户总数
                        auth.UsersVersion = orgVersion.UsersVersion;//告之登录用户当前服务器中组织机构用户版本
                        auth.GroupsCount = orgVersion.GroupsCount;//告之登录用户当前服务器中组织机构分组总数
                        auth.GroupsVersion = orgVersion.GroupsVersion;//告之登录用户当前服务器中组织机构分组信息版本
                        auth.RoomsCount = user.Rooms.Count;//告之登录用户当前加入的群总数

                        user.LoginInfo.LastDateTime = DateTime.Now.ToString();//设置最后一次登录时间
                        user.LoginInfo.LastIP = Session.RemoteEndPoint.Address.ToString();//设置最后一次登录IP

                        if (UserLoginSuccess != null)//触发用户登录成功事件
                            UserLoginSuccess(this, new ServerEventArgs(false, "", user, user));

                        //通知用户登录成功
                        SendMessageToUser(user, auth);
                        return;
                    }
                }
            }
            #endregion

            #region 如果用户不存在或密码错误
            {
                ///发送密码错误消息
                auth.type = type.error;
                auth.Password = Session.ID;
                SendMessage(Session, auth);
                Session.Disconnect();//断开连接
                Session.Dispose();
            }
            #endregion

        }
        #endregion

        #region 请求下载用户在线状态事件
        void OnRequestUsersPresence(TCPServerSession Session, string XMLMsg, object obj, ServerUser RequestUser)
        {
            int i = 0;
            Element element = new Element();

            ///将在线用户的ShowType发送给登录用户
            foreach (TCPServerSession se in GetTcpServerSession())
            {
                if (se.IsAuthenticated)//如果是已认证的TCP客户端
                {
                    User userTemp = se.Tag as User;//获得TCP客户端绑定的用户
                    Presence pretemp = new Presence();
                    pretemp.from = userTemp.UserID;
                    pretemp.ShowType = userTemp.ShowType;
                    pretemp.Status = pretemp.Status;

                    element.Data.Add(pretemp);
                }
                i++;

                if (i == 50)//一次发送80个用户
                {
                    SendMessageToUser(RequestUser, element);
                    i = 0;
                    element = new Element();
                }
            }
            if (i > 0)//最一次发送在线用户状态
                SendMessageToUser(RequestUser, element);


            ///广播登录用户上线消息 
            Presence pre = new Presence();
            pre.from = RequestUser.UserID;
            pre.ShowType =RequestUser.ShowType;
            BroadcastingMessageToOnlineUser(pre);


            ///触发请求获取离线消息事件
            if (UserLoginRequestOfflineMsg != null)
            {
                var e = new ServerEventArgs(true, "", null, RequestUser);
                UserLoginRequestOfflineMsg(this, e);
                if (e.Allow && e.obj != null && e.obj is List<string>)
                {
                    var offlineMsg = e.obj as List<string>;
                    foreach (string msgXML in offlineMsg)
                        SendMessageToUser(RequestUser, msgXML);
                } 
            }

          
            
        }
        #endregion

        #region 请求更改群信息
        void OnRequestChangRoom(TCPServerSession Session, string XMLMsg, object obj, ServerUser RequestUser)
        {
            EditRoom changeRoom = obj as EditRoom;

            if (changeRoom.type == type.New && RequestUser.Power.CreatedRoomsCount < RequestUser.Power.CreateMaxRooms)//如果请求创建群
            {
                #region 创建群

                //10次随机产生群号(所谓摇号)，如果10次摇出的群号都已被创建，则退出并表示服务器忙 
                //10都不能摇到空号表示用户运气不好，下次再试！
                for (int i = 0; i < 10; i++)
                {
                    Random ran = new Random();
                    int RandKey = ran.Next(1000000, 2000000000);//开始摇号，群号码从1000000至2000000000随机产生
                    if (!Rooms.ContainsKey(RandKey.ToString()))//如果是空号，以此号创建群
                    {
                        Room room = new Room();
                        room.RoomID = RandKey.ToString();
                        room.RoomName = changeRoom.RoomName;
                        room.Notice = changeRoom.Notice;
                        room.UserIDs = changeRoom.UserIDs;
                        room.CreateUserID =RequestUser.UserID;//创建者为请求者

                        if (RequestCreateRoom != null)//触发用户要求保存创建群信息到数据库事件
                        {
                            var e = new ServerEventArgs(false, Factory.CreateXMLMsg(room), room, RequestUser);
                            RequestCreateRoom(this,e);
                            if (!e.Allow)//如果没有保存到数据库，则创建群也不成功
                                return;
                        }

                        RequestUser.Power.CreatedRoomsCount++;//标记用户创建群数

                        #region 将内存中的用户添加到内存中的群
                        string[] userids = room.UserIDs.Split(';');
                        foreach (string userID in userids)
                        {
                            ServerUser user = getUser(userID);
                            if (user != null)
                            {
                                if (room.Users == null) room.Users = new Dictionary<string, User>();
                                if (!room.Users.ContainsKey(userID))
                                    room.Users.Add(user.UserID, user);

                                if (user.Rooms == null) user.Rooms = new Dictionary<string, Room>();
                                if (!user.Rooms.ContainsKey(room.RoomID))
                                    user.Rooms.Add(room.RoomID, room);
                            }
                        }

                        room.UserIDs = "";//重新生成群包含的Users
                        foreach (User u in GetUsers(room))
                            room.UserIDs += u.UserID + ";";
                        #endregion

                        Rooms.Add(room.RoomID, room);//将创建的群添加到内存

                        changeRoom.RoomID = room.RoomID;
                        changeRoom.CreateUserID = room.CreateUserID;

                        SendMessageToRoom(room.CreateUserID, room, changeRoom);//发送消息到刚创建的群，通知群里的用户已经加入群

                        return;//创建群成功，退出
                    }
                }
                #endregion
            }
            else if (changeRoom.type == type.set)//请求更新群
            {
                #region 更新群
                Room room = getRoom(changeRoom.RoomID);
                if (room != null && room.CreateUserID == RequestUser.UserID)//如果群创建者为请求用户
                {
                    string oldVersion = room.RoomName.Trim() + room.Notice.Trim() + room.UserIDs;
                    changeRoom.CreateUserID = room.CreateUserID;
                    room.Notice = changeRoom.Notice;
                    room.RoomName = changeRoom.RoomName;
                    string[] newUserids = changeRoom.UserIDs.Split(';');

                    #region 查找群中被删除的用户
                    changeRoom.type = type.delete;//标识退出群
                    changeRoom.UserIDs = null;//不要将群新包含的用户通知退出群的用户

                    string delRoomMsg =  Factory.CreateXMLMsg(changeRoom);
                    bool t = false;
                    foreach (ServerUser user in GetUsers(room))
                    {
                        t = false;
                        foreach (string userID in newUserids)
                            if (user.UserID == userID)//如果群里的用户在更新后的用户集合中存在，则表示未删除
                                t = true;

                        if (!t)//如果更新的用户集合中没有当前用户，则表示从群中删除此用户
                        {
                            room.Users.Remove(user.UserID);
                            user.Rooms.Remove(room.RoomID);
                            SendMessageToUser(user, delRoomMsg);//通知用户退出群
                        }
                    }
                    #endregion

                    #region 添加新用户记录群中新用户
                    foreach (string userID in newUserids)
                    {
                        ServerUser user = getUser(userID);//获取新用户
                        if (user != null)
                        {
                            if (!room.Users.ContainsKey(user.UserID))
                            {
                                room.Users.Add(user.UserID, user);//如果群里无此用户，则新增用户
                                if (!user.Rooms.ContainsKey(room.RoomID))
                                    user.Rooms.Add(room.RoomID, room);//如果用户不在此群，则为用户新增群
                            }
                        }
                    }
                    #endregion

                    room.UserIDs = "";//重新生成群包含的Users
                    foreach (User u in GetUsers(room))
                        room.UserIDs += u.UserID + ";";

                    changeRoom.type = type.set;//标识群信息被成功修改
                    changeRoom.UserIDs = room.UserIDs;//设置最新用户列表

                    string newVersion = room.RoomName.Trim() + room.Notice.Trim() + room.UserIDs;

                    if (oldVersion == newVersion) return;//如果没有做任何更改
                    SendMessageToRoom(room.CreateUserID, room, changeRoom);//通知群内原用户，群信息已经修改

                    if (RequestUpdateRoom != null)
                    {
                        RequestUpdateRoom(this,new ServerEventArgs(false,Factory.CreateXMLMsg(room),room,RequestUser));
                    }
                }
                #endregion
            }
        }
        #endregion

        #region 请求保存离线文件下次接收消息
        void OnUserSaveOfflineFileMsg(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
             OfflineFileMsg msg =obj as  OfflineFileMsg;
            msg.from = msg.to;
            msg.to =RequestUser.UserID;

            if (RequestSaveOfflineMsg != null)//触发要求保存离线消息事件
                RequestSaveOfflineMsg(this, new ServerEventArgs(true, "", msg, getUser(msg.to)));
        }
        #endregion
        
        #region 路由音视频协商、远程桌面控制协商等消息
        void OnRouteMsg(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
             Element msg = obj as  Element;
            SendMessageToUser(msg.to,XMLMsg);
        }
        #endregion

        #region 路由客户端文件传输消息
        void OnRouteP2PFileTransmit(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
            P2PFileMsg msg = obj as  P2PFileMsg;
            SendMessageToUser(msg.to, XMLMsg);
        }
        #endregion

        #region 请求下载用户信息事件
        void OnRequestOrgUsers(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
            #region 新算法
            foreach (string str in orgVersion.UsersXML.ToArray())
            {
                SendMessageToUser(RequestUser, str);//发送用户信息
            }
            #endregion
        }
        #endregion

        #region 请求下载群信息事件
        void OnRequestOrgRooms(TCPServerSession Session, string XMLMsg, object obj, ServerUser RequestUser)
        {
            DownloadRooms dRooms =obj as DownloadRooms;

            dRooms.to = RequestUser.UserID; dRooms.from = "";

            int i = 0;
            foreach (Room room in GetRooms(RequestUser))
            {
                dRooms.Data.Add(room);
                i++;
                if (i == 5)//每次发送5个群信息
                {
                    SendMessageToUser(RequestUser, dRooms);//发送群信息
                    dRooms.Data = new List<object>();//用户信息清零
                    i = 0;
                }
            }
            if (dRooms.Data.Count > 0)//发送剩下的群信息
            {
                SendMessageToUser(RequestUser, dRooms);//发送群信息
            }
        }
        #endregion

        #region 请求下载分组信息事件
        void OnRequestOrgGroups(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
            #region 新算法
            foreach (string str in orgVersion.GroupsXML.ToArray())
            {
                SendMessageToUser(RequestUser, str);//发送用户信息
            }
            #endregion
        }
        #endregion

        #region 修改密码事件
        void OnRequestChangePassword(TCPServerSession Session, string XMLMsg, object obj, ServerUser RequestUser)
        {
            EditPassword changePWD = obj as EditPassword;
            if (changePWD.NewPassword.Trim() == "") return;

            if (RequestChangePassword != null)
            {
                var e = new ServerEventArgs(false, "", changePWD, RequestUser);
                RequestChangePassword(this, e);
                if (e.Allow)//如果参数标明密码已经修改
                {
                    SendMessageToUser(RequestUser, XMLMsg);//通知用户更新密码成功 
                }
            }
        }
        #endregion

        #region 请求创建、更新、删除等用户资料事件
        void OnRequestEditUserData(TCPServerSession Session, string XMLMsg, object obj, ServerUser RequestUser)
        {
            EditUserData userData = obj as EditUserData;
            if (userData.UserID.Trim() == "" || userData.UserName.Trim() == "" || userData.GroupID.Trim() == "") return;
            userData.UserID = userData.UserID.Trim();

            ServerUser user = getUser(userData.UserID);

            if (userData.type == type.New && user == null && RequestUser.Power.isAdmin)//如果用户不存在，且请求用户是管理员,则创建新用户
            {
                user = new ServerUser();
                ///基本资料
                user.UserID = userData.UserID;
                user.UserName = userData.UserName;
                user.OrderID = userData.OrderID;
                user.GroupID = userData.GroupID;
                user.Password = "123456";// IMLibrary4.Security.Hasher.GetMD5Hash(IMLibrary4.Operation.TextEncoder.textToBytes("123456"));

                ///权限
                user.Power.CreateMaxRooms = userData.CreateMaxRooms;
                user.Power.Disable = userData.Disable;
                user.Power.isAdmin = userData.isAdmin;
                user.Power.isBroadcast = userData.isBroadcast;
                user.Power.isSendNotice = userData.isSendNotice;
                user.Power.isSendSMS = userData.isSendSMS;
                user.Power.isControlClient = userData.isControlClient;

                UserVcard card = new UserVcard();
                card.Birthday = userData.Birthday;
                card.FaceIndex = userData.FaceIndex;
                card.Job = userData.Job;
                card.Mail = userData.Mail;
                card.OfficePhone = userData.OfficePhone;
                card.Phone = userData.Phone;
                card.Post = userData.Post;
                card.Remark = userData.Remark;
                card.Sex = userData.Sex;

                if (RequestCreateUser != null)
                {
                    var e = new ServerEventArgs(false, "", card, user);
                    RequestCreateUser(this, e);

                    if (e.Allow)//如果用户资料已经存入数据库
                    {
                        var oldCount = Users.Count;
                        Users.Add(user.UserID, user);//添加用户到服务器内存
                        var newCount = Users.Count;
                        if (oldCount != newCount)
                        {
                            ResetUsersVersion(Users);//重设组织机构用户信息版本
                            userData.Version = orgVersion.UsersVersion;
                            BroadcastingMessageToOnlineUser(Factory.CreateXMLMsg(userData));//将创建的新用户信息广播给所有在线用户  
                        }
                    }
                }
            }
            else if (userData.type == type.set)//如果是更新用户 && RequestUser.Power.isAdmin
            {//2014-12-18 10:47:23 yufh 更新，取消admin才能更新用户资料
                if (user == null) return;//如果用户不存在则退出

                string oldVersion = user.UserName + user.GroupID + user.OrderID.ToString();//老信息
                string newVersion = userData.UserName.Trim() + userData.GroupID.Trim() + userData.OrderID.ToString(); //新信息

                if (RequestUpdateUser != null)
                {
                    ///基本资料
                    user.UserName = userData.UserName;
                    user.OrderID = userData.OrderID;
                    user.GroupID = userData.GroupID;

                    ///权限
                    user.Power.CreateMaxRooms = userData.CreateMaxRooms;
                    user.Power.Disable = userData.Disable;
                    user.Power.isAdmin = userData.isAdmin;
                    user.Power.isBroadcast = userData.isBroadcast;
                    user.Power.isSendNotice = userData.isSendNotice;
                    user.Power.isSendSMS = userData.isSendSMS;
                    user.Power.isControlClient = userData.isControlClient;

                    UserVcard card = new UserVcard();
                    card.Birthday = userData.Birthday;
                    card.FaceIndex = userData.FaceIndex;
                    card.Job = userData.Job;
                    card.Mail = userData.Mail;
                    card.OfficePhone = userData.OfficePhone;
                    card.Phone = userData.Phone;
                    card.Post = userData.Post;
                    card.Remark = userData.Remark;
                    card.Sex = userData.Sex;

                    var e = new ServerEventArgs(false, "", card, user);
                    RequestUpdateUser(this, e);

                    if (e.Allow)//如果用户资料已经更新存入数据库
                    {
                        if (oldVersion != newVersion)//如果用户信息改变
                            ResetUsersVersion(Users);//重设组织机构用户信息版本

                        userData.Version = orgVersion.UsersVersion;
                        BroadcastingMessageToOnlineUser(Factory.CreateXMLMsg(userData));//将更新用户信息广播给所有在线用户
                    }
                }
            }
            else if (userData.type == type.get)//如果是获取用户资料
            {
                if (user == null) return;//如果用户不存在则退出
                userData.type = type.result;

                if (RequestUserCard != null)
                {
                    var e = new ServerEventArgs(false, "", null, user);
                    RequestUserCard(this, e);
                    if (e.Allow)
                    {
                        ///基本资料
                        userData.UserName = user.UserName;
                        userData.OrderID = user.OrderID;
                        userData.GroupID = user.GroupID;

                        ///权限
                        userData.CreateMaxRooms = user.Power.CreateMaxRooms;
                        userData.Disable = user.Power.Disable;
                        userData.isAdmin = user.Power.isAdmin;
                        userData.isBroadcast = user.Power.isBroadcast;
                        userData.isSendNotice = user.Power.isSendNotice;
                        userData.isSendSMS = user.Power.isSendSMS;
                        user.Power.isControlClient = userData.isControlClient;

                        UserVcard card = e.obj as UserVcard;
                        if (card != null)
                        {
                            userData.Birthday = card.Birthday;
                            userData.FaceIndex = card.FaceIndex;
                            userData.Job = card.Job;
                            userData.Mail = card.Mail;
                            userData.OfficePhone = card.OfficePhone;
                            userData.Phone = card.Phone;
                            userData.Post = card.Post;
                            userData.Remark = card.Remark;
                            userData.Sex = card.Sex;
                        }

                        SendMessageToUser(RequestUser, Factory.CreateXMLMsg(userData));//将用户资料发送给请求者
                    }
                }
            }
            else if (userData.type == type.delete && RequestUser.Power.isAdmin)//如果是删除用户
            {
                if (user == null) return;//如果用户不存在则退出
                if (RequestUser.UserID == user.UserID) return;//如果管理员想删除自己，退出

                if (RequestDelUser != null)
                {
                    var e = new ServerEventArgs(false, "", userData, RequestUser);
                    RequestDelUser(this, e);

                    if (e.Allow)//如果用户资料已经存入数据库
                    {
                        TCPServerSession se = user.Tag as TCPServerSession;
                        if (se != null && se.IsConnected) { se.Disconnect(); se.Dispose(); }//如果删除的用户在线，则将其踢出
                        var oldCount = Users.Count;
                        Users.Remove(userData.UserID);//添加用户到服务器内存
                        var newCount = Users.Count;
                        if (oldCount != newCount)
                            ResetUsersVersion(Users);//重设组织机构用户信息版本
                        userData.Version = orgVersion.UsersVersion;
                        BroadcastingMessageToOnlineUser(Factory.CreateXMLMsg(userData));//将删除的用户广播给所有在线用户 
                    }
                }
            }
        }
        #endregion

        #region 请求创建、更新、删除等更改分组信息事件
        void OnRequestChangeGroupVcard(TCPServerSession Session, string XMLMsg, object obj, ServerUser RequestUser)
        {
            EditGroupData groupData = obj as EditGroupData;

            if (groupData.GroupID.Trim() == "" || groupData.GroupName.Trim() == "") return;
            groupData.GroupID = groupData.GroupID.Trim();

            Group group = getGroup(groupData.GroupID);
            if (groupData.type == type.New && RequestUser.Power.isAdmin)//如果用户是管理员
            {
                if (group != null) return;//如果分组已经存在，退出

                //将group资料转换为group
                group = new Group();
                group.GroupID = groupData.GroupID;
                group.GroupName = groupData.GroupName;
                group.SuperiorID = groupData.SuperiorID;
                group.OrderID = groupData.OrderID;
                
                var e = new ServerEventArgs(false, XMLMsg, group, RequestUser);
                if (RequestCreateGroup != null)
                {
                    RequestCreateGroup(this, e);
                    if (e.Allow)//如果分组信息已经保存进数据库
                    {
                        var oldCount = Groups.Count;
                        addGroup(group);//添加分组
                        var newCount = Groups.Count;
                        if (oldCount != newCount)//如果确实添加成功
                        {
                            ResetGroupsVersion(Groups);//重设组织机构分组版本
                            groupData.Version = orgVersion.GroupsVersion;//设置最新分组版本
                            BroadcastingMessageToOnlineUser(Factory.CreateXMLMsg(groupData));//将创建的分组信息广播给所有在线用户  
                        }
                    }
                }
            }
            else if (groupData.type == type.set && RequestUser.Power.isAdmin)//如果是更新分组信息
            {
                if (group == null) return;//如果要更新的分组已经不存在，退出

                var oldVersion = group.GroupName.Trim() + group.SuperiorID.Trim() + group.OrderID.ToString();
                var newVersion = groupData.GroupName.Trim() + groupData.SuperiorID.Trim() + groupData.OrderID.ToString();
                if (oldVersion != newVersion)//如果确实做了修改
                {
                    if (RequestUpdateGroup != null)
                    {
                        //将group资料转换为group 
                        group.GroupName = groupData.GroupName;
                        group.SuperiorID = groupData.SuperiorID;
                        group.OrderID = groupData.OrderID;
                        var e = new ServerEventArgs(false, XMLMsg, group, RequestUser);
                        RequestUpdateGroup(this, e);//触发更新事件
                        if (e.Allow)
                        {
                            ResetGroupsVersion(Groups);//重设组织机构分组版本
                            groupData.Version = orgVersion.GroupsVersion;//设置最新分组版本
                            BroadcastingMessageToOnlineUser(Factory.CreateXMLMsg(groupData));//将更新的分组信息广播给所有在线用户
                        }
                    }
                }

            }
            else if (groupData.type == type.delete && RequestUser.Power.isAdmin)//如果是删除分组
            {
                if (group == null) return;//如果要删除的分组已经不存在，退出
                var e = new ServerEventArgs(false, XMLMsg, group, RequestUser);
                if (RequestDelGroup != null)
                {
                    RequestDelGroup(this, e);
                    if (e.Allow)//如果数据库删除成功
                    {
                        var oldCount = Groups.Count;
                        delGroup(group.GroupID);
                        var newCount = Groups.Count;
                        if (oldCount != newCount)//如果确实删除了分组
                        {
                            ResetGroupsVersion(Groups);//重设组织机构分组版本
                            groupData.Version = orgVersion.GroupsVersion;//设置最新分组版本
                            BroadcastingMessageToOnlineUser(Factory.CreateXMLMsg(groupData));//将删除的用户广播给所有在线用户 
                        }
                    }
                }
            }
        }
        #endregion

        #region 路由消息到达事件
        void OnMessage(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
            IMLibrary4.Protocol.Message msg =obj as IMLibrary4.Protocol.Message;

            msg.DateTime = DateTime.Now.ToString();//将消息发送时间设置为服务器的时间 

            if (msg.MessageType ==  MessageType.User)//如果消息发送给用户
            {
                SendMessageToUser(msg.to, msg);
            }
            else if (msg.MessageType ==  MessageType.Group)//如果消息发送给群
            {
                SendMessageToRoom(msg.from, msg.to, msg);
            }
            else if (msg.MessageType ==  MessageType.Notice)//如果发送通知消息给多个用户
            {
                string[] users = msg.to.Split(';');//获得要接收消息的用户数据
                if (users.Length > 0)
                    foreach (string userID in users)
                    {
                        msg.to = userID;
                        SendMessageToUser(userID, msg);
                    }
            }
            else if (msg.MessageType ==  MessageType.broadcasting)//如果发送通知消息给所有用户
                BroadcastingMessageToOnlineUser(msg);
        }
        #endregion

        #region 更改在线状态事件
        void OnPresence(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
            Presence pre = obj as Presence;
            RequestUser.ShowType = pre.ShowType;//更改在线状态

            if (pre.ShowType ==  ShowType.Invisible)//如果用户隐身，则将用户状态设置为离线发送给其他用户
                pre.ShowType =  ShowType.Offline;

            BroadcastingMessageToOnlineUser(pre);
        }
        #endregion

        #region 用户离线事件
        void OnUserOffline(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
            Presence presence = new Presence();
            presence.from =RequestUser.UserID;
            presence.ShowType = ShowType.Offline;
            RequestUser.ShowType =  ShowType.Offline;

            BroadcastingMessageToOnlineUser(presence);//告诉在线者用户已经离线
        }
        #endregion

        #region 客户端离线文件上传完成事件
        /// <summary>
        /// 客户端离线文件上传完成事件
        /// </summary>
        /// <param name="offlineFileMsg"></param>
        private void offlineFileServer_FileUploaded(OfflineFileMsg offlineFileMsg)
        {           
            string[] tousers = offlineFileMsg.to.Split(';');
            foreach (string to in tousers)
            {
                if (to != "" && to != offlineFileMsg.from)
                {
                    if (UserSaveOfflineFileMsg != null)
                        UserSaveOfflineFileMsg(this, new ServerEventArgs(false, "", offlineFileMsg, getUser(to)));

                    offlineFileMsg.to = to;
                    SendMessageToUser(to, offlineFileMsg);//发送离线文件接收消息给用户 
                }
            }
        }

        #endregion

        #region 客户端离线文件下载完成事件
        private void offlineFileServer_FileDownloaded(OfflineFileMsg offlineFileMsg)
        {
            //此处添加离线文件下载完成事件
            //if (offlineFileMsg != null)
            //{ 
                
            //}

        }
        #endregion

        #region 通知客户端下载图片文件事件
        void OnRouteDownLoadImageFile(TCPServerSession Session, string XMLMsg, object obj, User RequestUser)
        {
            ImageFileMsg msg =obj as  ImageFileMsg;

            if (msg.MessageType ==  MessageType.User)//如果消息发送给用户
            {
                SendMessageToUser(msg.to, msg);
            }
            else if (msg.MessageType == MessageType.Group)
            {
                SendMessageToRoom(msg.from, msg.to, msg);
            }
        }
        #endregion

        #endregion

        #region 字典操作

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user"></param>
        void addUser(ServerUser user)
        {
            lock (Users)
            {
                Users.Add(user.UserID, user);
            }
            ResetUsersVersion(Users );
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userID"></param>
        void delUser(string  userID)
        {
            lock (Users)
            {
                Users.Remove(userID);
            }
            ResetUsersVersion(Users);
        }

        /// <summary>
        /// 添加一个分组
        /// </summary>
        /// <param name="group"></param>
        void addGroup(Group group)
        {
            lock (Groups)
            {
                Groups.Add(group.GroupID, group);
            }
            ResetGroupsVersion(Groups);
        }

        /// <summary>
        /// 删除一个分组
        /// </summary>
        /// <param name="groupID"></param>
        void delGroup(string groupID)
        {
            lock (Groups)
            {
                Groups.Remove(groupID);
            }
            ResetGroupsVersion(Groups);

        }

        #region 从内存字典中获取分组
        /// <summary>
        /// 从内存字典中获取分组
        /// </summary>
        /// <param name="groupID">编号</param>
        /// <returns></returns>
        private Group getGroup(string groupID)
        {
            Group group = null;
            if (Groups.ContainsKey(groupID.Trim())) //如果用户存在
                Groups.TryGetValue(groupID.Trim(), out group);
            return group;
        }
        #endregion

        #region 从内存字典中获取用户
        /// <summary>
        /// 从内存字典中获取用户
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        private ServerUser getUser(string userID)
        {
            ServerUser user = null;
            if (Users.ContainsKey(userID.Trim())) //如果用户存在
                Users.TryGetValue(userID.Trim(), out user);
            return user;
        }
        #endregion

        #region 从内存字典中获取群
        /// <summary>
        /// 从内存字典中获取群
        /// </summary>
        /// <param name="roomID">群编号</param>
        /// <returns></returns>
        private Room getRoom(string roomID)
        {
            Room room = null;
            if (Rooms.ContainsKey(roomID.Trim())) //如果用户存在
                Rooms.TryGetValue(roomID.Trim(), out room);
            return room;
        }
        #endregion

        #region 从群中获取用户集合数组
        /// <summary>
        /// 从群中获取用户集合数组
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        private ServerUser[] GetUsers(Room room)
        {
            lock (room.Users)//确保线程安全
            {
                ServerUser[] Users = new ServerUser[room.Users.Count];
                room.Users.Values.CopyTo(Users, 0);
                return Users;
            }
        }
        #endregion

        #region 从用户中获取加入的群集合数组
        /// <summary>
        /// 从用户中获取加入的群集合数组
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private Room[] GetRooms(ServerUser user)
        {
            lock (user.Rooms)//确保线程安全
            {
                Room[] Rooms = new Room[user.Rooms.Count];
                user.Rooms.Values.CopyTo(Rooms, 0);
                return Rooms;
            }
        }
        #endregion

        #endregion
    }

    #region 事件参数
    public class ServerEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Session">TCP客户端</param>
        /// <param name="XMLMsg">XML消息字符串</param>
        /// <param name="obj">消息转换的对像</param>
        /// <param name="RequestUser">请求用户</param>
        public ServerEventArgs(bool Allow, string XMLMsg, object objs, ServerUser RequestUser)
        {
            this.Allow = Allow;
            this.XMLMsg = XMLMsg;
            this.obj = objs;
            this.RequestUser = RequestUser;
        }

         

        /// <summary>
        /// XML消息字符串
        /// </summary>
        public string XMLMsg { get; set; }

        /// <summary>
        /// 消息转换的对像
        /// </summary>
        public object obj { get; set; }

        /// <summary>
        /// 请求用户
        /// </summary>
        public ServerUser RequestUser { get; set; }
        /// <summary>
        /// 标识（用于纪录是否执行事件以后的操作）
        /// </summary>
        public bool Allow { get; set; }
    }
    #endregion
}
