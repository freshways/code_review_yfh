﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using IMLibrary4.Net;
using IMLibrary4.Net.IO;
using IMLibrary4.Net.TCP;
using IMLibrary4.Net.UDP;

namespace OurMsgServer
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        Server server = new Server();
      

        private void Form2_Load(object sender, EventArgs e)
        {
             
        }
         
    
        private void butStrat_Click(object sender, EventArgs e)
        {
            try
            {
                butStrat.Enabled = false;
                butStop.Enabled = true;
                server.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void butStop_Click(object sender, EventArgs e)
        {
            butStrat.Enabled =true ;
            butStop.Enabled =false ;
            server.Stop();
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TsMenu_Exit_Click(object sender, EventArgs e)
        {
            butStop_Click(sender, e);
            Application.Exit();
            this.Dispose();
            notifyIcon1.Dispose();
        }

        /// <summary>
        /// 显示主窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.Show();
            }
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }

       

       
    }
}
