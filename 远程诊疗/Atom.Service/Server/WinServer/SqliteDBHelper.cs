﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using IMLibrary4.SQLiteData;
using IMLibrary4;
using IMLibrary4.Operation;
using IMLibrary4.Protocol;
using IMLibrary4.Organization;


namespace OurMsgServer
{
    /// <summary>
    /// 服务器sqlite数据库操作类
    /// </summary>
    public class SqliteDBHelper
    {
        /// <summary>
        /// 初始化数据库连接字符串
        /// </summary>
        public static void iniConStr()
        {
            ////初始化本地数据库连接字符串
            SQLiteDBHelper.connectionString = "data source=OurMsgServerDB.s3db";
        }

        #region 创建新用户
        /// <summary>
        /// 添加新用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool AddUser(User user)
        {
            try
            {
                string sql = "insert into Users(UserID,UserName,GroupID,orderID) values(@UserID,@UserName,@GroupID,@orderID)";

                System.Data.SQLite.SQLiteParameter[] parameters = new System.Data.SQLite.SQLiteParameter[]{   
                     new System.Data.SQLite.SQLiteParameter("@UserID",user.UserID ),   
                     new System.Data.SQLite.SQLiteParameter("@UserName",user.UserName),   
                     new System.Data.SQLite.SQLiteParameter("@GroupID",user.GroupID),   
                     new System.Data.SQLite.SQLiteParameter("@orderID",user.OrderID),   
                                       };
                SQLiteDBHelper.ExecuteNonQuery(sql, parameters);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 创建新用户
        /// </summary>
        /// <param name="card"></param>
        public static bool updateUserVcard(UserVcard card)
        {
            try
            {
                string sql = "update Users set Vcard=@Vcard where UserID=@UserID";

                System.Data.SQLite.SQLiteParameter[] parameters = new System.Data.SQLite.SQLiteParameter[]{   
                     new System.Data.SQLite.SQLiteParameter("@UserID",card.UserID ),   
                     new System.Data.SQLite.SQLiteParameter("@Vcard",Factory.CreateXMLMsg (card)),   
                     //new System.Data.SQLite.SQLiteParameter("@GroupID",user.GroupID),   
                     //new System.Data.SQLite.SQLiteParameter("@orderID",user.OrderID),   
                                       };
                SQLiteDBHelper.ExecuteNonQuery(sql, parameters);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
         
    }
}
