﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace IMLibrary4.AV
{
    public class MyImage
    {
        public static Bitmap[] splitImage(Image img,int Width = 128,int  Height=96)
        {
            int row = img.Width / Width;
            if (img.Width % Width != 0) row++;
            int col = img.Height / Height;
            if (img.Height % Height != 0) col++;
            Bitmap[] Bitmaps = new Bitmap[row * col];

            int count = 0;
            for (int y = 0; y < img.Height; y += Height)
            {
                for (int x = 0; x < img.Width; x += Width)
                {
                    Bitmap subBitmap = new Bitmap(Width, Height);
                    Graphics gr = Graphics.FromImage(subBitmap);
                    gr.Clear(Color.Transparent);
                    gr.DrawImage(img, new Rectangle(0, 0, Width, Height), x, y, Width, Height, GraphicsUnit.Pixel);
                    gr.Save();
                    Bitmaps[count] = subBitmap;
                    count++;
                }
            }
            return Bitmaps;
        }
    }
}