﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace IMLibrary4.AV
{
    class DecodingSession
    {
        private readonly Coder.Decoder Decoder;

        public DecodingSession()
        {
           Decoder = new Coder.Decoder();
        }

        public Image Decode(byte[] encodedFrame)
        {
            return Decoder.Decode(encodedFrame);
        }
    }
}
