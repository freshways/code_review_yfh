﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using IMLibrary4.Net;
using IMLibrary4.Protocol;
using IMLibrary4.Operation;
using IMLibrary4.WebP;

namespace IMLibrary4.AV
{
    /// <summary>
    /// 远程协助
    /// </summary>
    public class RemoteHelp :UDPP2PTransmit
    {  
        #region 构造函数
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverEP"></param>
        public RemoteHelp(IPEndPoint serverEP)
            : base(serverEP)
        {

            if (!System.IO.File.Exists("ImageCode.dll"))
                WebPFormat.DeCompress();

                Rectangle screenArea = Rectangle.Empty;
                screenArea = Rectangle.Union(screenArea, Screen.PrimaryScreen.Bounds);
                ImageQuality = 20;
                ////if (screenArea.Width == 1440)
                ////    ImageQuality = 30;
                //else if (screenArea.Width > 1440)
                //    ImageQuality = 25;
                //else  if (screenArea.Width < 1440)
                //    ImageQuality = 45;
        }

        #endregion

        #region 变量
        /// <summary>
        /// 对方屏幕宽度
        /// </summary>
        int RemoteDesktopWidth=1024;
        /// <summary>
        /// 对方屏幕高度
        /// </summary>
        int RemoteDesktopHeight = 768;
        /// <summary>
        /// 图片分分割数(横向)
        /// </summary>
        const int splitWidthCount = 16;
        /// <summary>
        /// 图片分分割数(竖向)
        /// </summary>
        const int splitHeightCount = 8;
        /// <summary>
        /// 分割后的图片数量
        /// </summary>
        const int imagesCount = splitWidthCount * splitHeightCount;
        /// <summary>
        /// 收到的新图片
        /// </summary>
        Image[] RecNewImages = new Image[imagesCount];
        /// <summary>
        /// 标识是否初始化组件
        /// </summary>
        bool IsIni; 
        /// <summary>
        /// 图像宽度放大系数X
        /// </summary>
          float uX = 1.60f;
        /// <summary>
        /// 图像高度放大系数X
        /// </summary>
          float uY = 1.60f; 
        /// <summary>
        /// 
        /// </summary>
        System.Security.Cryptography.MD5CryptoServiceProvider oMD5Hasher;
        PictureBox picbox = null;
        /// <summary>
        /// 桌面显示控件
        /// </summary>
        public PictureBox picBox
        {
            set
            {
                picbox = value;
                
                #region 鼠标键盘事件
                if (picbox != null)
                {
                    picbox.MouseMove += (s, e) =>
                    {
                        sendMouseOrder(UdpRemoteDesketOrder.OrderType.MouseMove, e.X, e.Y, e.Delta);
                    };
                    picbox.MouseDown += (s, e) =>
                    {
                        if (e.Button == MouseButtons.Right)
                            sendMouseOrder(UdpRemoteDesketOrder.OrderType.MouseRightDown, e.X, e.Y, e.Delta);
                        else if (e.Button == MouseButtons.Left)
                            sendMouseOrder(UdpRemoteDesketOrder.OrderType.MouseLeftDown, e.X, e.Y, e.Delta);
                    };
                    picbox.MouseUp += (s, e) =>
                    {
                        if (e.Button == MouseButtons.Right)
                            sendMouseOrder(UdpRemoteDesketOrder.OrderType.MouseRightUp, e.X, e.Y, e.Delta);
                        else if (e.Button == MouseButtons.Left)
                            sendMouseOrder(UdpRemoteDesketOrder.OrderType.MouseLeftUp, e.X, e.Y, e.Delta);
                    };
                    picbox.MouseWheel += (s, e) =>
                    {
                        sendMouseOrder(UdpRemoteDesketOrder.OrderType.MouseWheel, e.X, e.Y,e.Delta);
                    };
                    picbox.MouseDoubleClick += (s, e) =>
                    {
                        sendMouseOrder(UdpRemoteDesketOrder.OrderType.MouseDoubleClick, e.X, e.Y, e.Delta);
                    };
                }
                #endregion
            }
            get { return picbox; }
        }

        /// <summary>
        /// 旧图片MD5值
        /// </summary>
        byte[][] imageOldMD5 = new byte[imagesCount][];
        /// <summary>
        /// 是否控制端(控制端不用发送桌面图像给对方)
        /// </summary>
        public bool IsControler = false;
        /// <summary>
        /// 允许控制
        /// </summary>
        public bool AllowControl = false;
        /// <summary>
        /// 标记是否收到过图片侦
        /// </summary>
        bool IsReceiveFirstImage = false; 
        /// <summary>
        /// 发送的包集合
        /// </summary>
        SortedList<int, byte[]> Packets = new SortedList<int, byte[]>();
        /// <summary>
        /// 收到的图片包集合
        /// </summary>
        SortedList<int, UdpImagePacket> UdpImagePackets = new SortedList<int, UdpImagePacket>(); 
        /// <summary>
        /// 收到的包集合
        /// </summary>
        SortedList<int, byte[]>[] RecImagesBuffer = new SortedList<int, byte[]>[imagesCount];
        /// <summary>
        /// 已经收到的64张旧图片
        /// </summary>
        Image[] RecOldImages = new Image[imagesCount];
        /// <summary>
        /// 本次发送的包数
        /// </summary>
        int SendCount = 0; 
        /// <summary>
        /// 是否已经发送请求
        /// </summary>
        bool isRequestSend = false;
        /// <summary>
        /// 是否取消传输
        /// </summary>
        bool IsCancelTransmit = false;
        /// <summary>
        /// 发送一个包的间隔
        /// </summary>
        int sendInterval =1;
        /// <summary>
        /// 时间戳
        /// </summary>
        int TimeStamp = (int)DateTime.Now.Ticks;
        /// <summary>
        /// 请求发送包
        /// </summary>
        UdpFilePacket RequestSendFilePakMsg = new UdpFilePacket();
        /// <summary>
        /// 发送空包打洞
        /// </summary>
        byte[] nullPak = new byte[1];
        /// <summary>
        /// 发送控件
        /// </summary>
        System.Timers.Timer   timerSend  ;
        /// <summary>
        /// 是否正在发送数据
        /// </summary>
        bool isSendImage = false;
        /// <summary>
        /// 
        /// </summary>
        Font font = new Font("黑体", 8);
        /// <summary>
        /// 压缩质量
        /// </summary>
        int ImageQuality = 30;
        #endregion 

        #region 收到数据包事件
        private void ReceiveUdpData(byte[] data)
        {
            if (data.Length < UdpFilePacket.HeaderLength) return;
             
            UdpFilePacket udpFilePacket = new UdpFilePacket(data);//转换为协议对像
            if (udpFilePacket.type == (byte)TransmitType.SendFile)//收到文件发送请求
            { 
                if (isRequestSend) return;
                isRequestSend= true;//只能接收一次发送请求
                if (!IsControler)//如果是被控者
                {
                    if (timerSend == null)
                    {
                        timerSend = new System.Timers.Timer();
                        if (MTU > 1500)
                        {
                            ImageQuality = 5;
                            timerSend.Interval = 50;
                        }
                        else
                            timerSend.Interval =200;

                        timerSend.Elapsed += (s, e) =>
                            {
                                if (isSendImage) return;
                                sendDesktop();
                            };
                        timerSend.Enabled = true;
                    }
                }
            }
            else if (udpFilePacket.type == (byte)TransmitType.getFilePackage)//收到文件数据包 
            {
                if (!IsControler)//如果是受控者
                {
                    lock (Packets)
                        Packets.Remove(udpFilePacket.Index);//删除控制者已经收到的包 
                }
                else//如果是控制者，处理接收到的图片文件数据包
                {
                    //Console.WriteLine("接收到图片文件数据包....");
                    ReceivedFilePacket(udpFilePacket);
                }
            }
            else if (udpFilePacket.type == (byte)TransmitType.CheckPacket)//收到校验包
            {
                if (!IsControler)//如果是受控者
                {
                    lock (Packets)
                        Packets.Remove(udpFilePacket.Index);//删除对方已经收到的校验包
                }
                else//如果是控制者，处理接收到的一侦图片文件数据包
                {
                    ReceivedCheckPacket(udpFilePacket);//处理校验包，并绘制图片
                }
            }
            else if (udpFilePacket.type == (byte)TransmitType.RemoteControl)//收到远程控制命令包
            {
                if (!AllowControl) return;//如果不允许控制，退出
                if (udpFilePacket.PayloadLength >= UdpRemoteDesketOrder.HeaderLength)
                    ExcuteOrder(new UdpRemoteDesketOrder(udpFilePacket.Payload));//执行命令
            }
        }
        #endregion

        #region 事件
        public delegate void ReceiveFirstImageEventHandler(object sender, Image image);
        /// <summary>
        /// 收到第一侦图片事件
        /// </summary>
        public event ReceiveFirstImageEventHandler ReceiveFirstImage;
        #endregion  
          
        #region 发送桌面
        /// <summary>
        /// 发送桌面
        /// </summary> 
        private void sendDesktop()
        {
            Console.WriteLine("收到传输请求....");
             
            //DateTime t1 = DateTime.Now;
            //TimeSpan ts ;

            isSendImage = true;//标识正在发送数据
            SendCount = 0;//重置时间戳

            #region 屏幕切图并分割
            Bitmap screen = null;
            if (AllowControl)
            {
                Rectangle screenArea = Rectangle.Empty;
                screenArea = Rectangle.Union(screenArea, Screen.PrimaryScreen.Bounds);
                screen = new Bitmap(screenArea.Width, screenArea.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Graphics graphics = Graphics.FromImage(screen);
                Size scrSize = new Size(screenArea.Width, screenArea.Height);
                // capture the screen
                graphics.CopyFromScreen(0, 0, 0, 0, scrSize, CopyPixelOperation.SourceCopy);
                // release resources
                graphics.Dispose();
            }
            else 
              screen = CaptureScreen.CaptureDesktopWithCursor();

            if (screen == null)
            {
                isSendImage = false;//标识正在发送数据 
                return;
            }

            Image  myScreen;

            float screenPix = (float)screen.Width / (float)screen.Height;
            Bitmap[] Bitmaps; 

            if (screenPix > 1.5)
            {
                myScreen = screen.GetThumbnailImage(1280, 720, null, IntPtr.Zero);
                Bitmaps = MyImage.splitImage(myScreen, 1280 / splitWidthCount, 720 / splitHeightCount);//分割图片为小图片
            }
            else
            {
                myScreen = screen.GetThumbnailImage(1024, 768, null, IntPtr.Zero);
                Bitmaps = MyImage.splitImage(myScreen, 1024 / splitWidthCount, 768 / splitHeightCount);//分割图片为小图片
            }

            screen.Dispose();


            //ts = DateTime.Now.Subtract(t1);
            //Console.WriteLine("完成分割所需要的时间：" + ts.Seconds.ToString() + ":" + ts.Milliseconds.ToString());
            //t1 = DateTime.Now;

            byte[][] imageNewMD5 = new byte[imagesCount][];// 新图片MD5值
            byte[][] imageBufs = new byte[imagesCount][];// 新图片二进制值

            for (int i = 0; i < Bitmaps.Length; i++)
            {
                //图片转换为字节
                System.IO.MemoryStream Ms = new MemoryStream();
                Bitmaps[i].Save(Ms,System.Drawing.Imaging.ImageFormat.Bmp);
                imageBufs[i] = Ms.ToArray();
                Ms.Close(); 
                if (oMD5Hasher == null)
                    oMD5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
                imageNewMD5[i] = oMD5Hasher.ComputeHash(imageBufs[i]);//获得新图片MD5值
            }
            #endregion

            //ts = DateTime.Now.Subtract(t1);
            //Console.WriteLine("完成MD5所需要的时间：" + ts.Seconds.ToString() + ":" + ts.Milliseconds.ToString());
            //t1 = DateTime.Now;

            #region 在新旧图片中查找图片是否存在,并发送不存在的图片
            for (int i = 0; i < Bitmaps.Length; i++) 
            {
                if (IsCancelTransmit)//如果传输被取消
                {
                    lock (Packets) Packets.Clear();//清空缓冲区并退出发送
                    return;
                }
                
                bool isExsit = false;//图片是否重复
                bool isOld = false;//是否旧图片
                byte ExsitIndex = 255;//存在的图片索引


                if (ArrayEquals(imageOldMD5[i], imageNewMD5[i]))//如果对方有此图片，不用重发
                    continue;

                for (int j = 0; j < Bitmaps.Length; j++)//在旧图片中查找图片是否存在
                {
                    if (ArrayEquals(imageOldMD5[j], imageNewMD5[i]))//如果旧图片已经存在,则不用发送了
                    { 
                        isExsit = true; isOld = true;
                        ExsitIndex = (byte)j;
                        break;//跳出循环
                    }
                }
                if (!isExsit)//如果旧图片中查找不到
                {
                    for (int j = 0; j < i; j++)//在新图片中查找图片是否存在
                    {
                        if (ArrayEquals(imageNewMD5[j], imageNewMD5[i]))//如果新图片中已经存在,则也不用发送了
                        {
                            isExsit = true; isOld = false;
                            ExsitIndex = (byte)j;
                            break;//跳出循环
                        }
                    }
                }
                if (!isExsit)//如果新旧图片中均不存在此图片
                {
                    ////////////////////////////////////////////
                    ////////////////////////////////////////////
                    ///////////////////////////////////////////
                    //////////////////////////////////////////// 
                    imageBufs[i] = WebPFormat.Save(ImageQuality, Bitmaps[i]);//将图片转换为字节数组 
                    /////////////////////////////////////////////
                    /////////////////////////////////////////////
                    ////////////////////////////////////////////
                    /////////////////////////////////////////////

                    //计算分包数
                    int packetCount = imageBufs[i].Length / MTU;
                    int mod = imageBufs[i].Length % MTU;
                    if (mod != 0) packetCount++;
                    int scrOffset = 0;

                    //发送分包的新图片
                    for (int k = 0; k < packetCount; k++)
                    {
                        int size = MTU;
                        if (MTU + scrOffset > imageBufs[i].Length)
                            size = imageBufs[i].Length - scrOffset;

                        UdpImagePacket imagePak = new UdpImagePacket();
                        imagePak.Index = (byte)i;
                        imagePak.SequenceNumber = (byte)k;
                        imagePak.ExsitIndex = (byte)packetCount;//设置包总数
                        imagePak.IsExsit = false;
                        imagePak.Payload = new byte[size];
                        Buffer.BlockCopy(imageBufs[i], scrOffset, imagePak.Payload, 0, imagePak.PayloadLength);
                        scrOffset += imagePak.PayloadLength;


                        UdpFilePacket udpFilePacket = new UdpFilePacket();
                        udpFilePacket.type = (byte)TransmitType.getFilePackage;//告诉对方收到文件包
                        udpFilePacket.Index = SendCount;//包索引
                        udpFilePacket.Payload = imagePak.ToBytes();//将图片包存入UDP包发送
                        lock (Packets)
                            Packets.Add(SendCount, udpFilePacket.ToBytes());//添加包到发送缓冲区,以便重新发送丢包数据

                        SendData(udpFilePacket.ToBytes()); //发送文件数据给对方
                        System.Threading.Thread.Sleep(sendInterval);//休息20毫秒 
                        SendCount++;//发送包数加1 
                    }
                }
                else
                {
                    UdpImagePacket imagePak = new UdpImagePacket();
                    imagePak.Index = (byte)i;
                    imagePak.IsExsit = true;
                    imagePak.ExsitIndex = ExsitIndex;
                    imagePak.ExsitIsOld = isOld;//标记是否存在的是旧图片
                     
                    UdpFilePacket udpFilePacket = new UdpFilePacket();
                    udpFilePacket.type = (byte)TransmitType.getFilePackage;//告诉对方收到文件包
                    udpFilePacket.Index = SendCount;//包索引
                    udpFilePacket.Payload = imagePak.Header;//将图片包存入UDP包发送
                    lock (Packets)
                        Packets.Add(SendCount, udpFilePacket.ToBytes());//添加包到发送缓冲区,以便重新发送丢包数据

                    SendData(udpFilePacket.ToBytes()); //发送文件数据给对方
                    System.Threading.Thread.Sleep(sendInterval);//休息 
                    SendCount++;//发送包数加1 

                    //Console.WriteLine("发送重复包....");
                }
            }
            #endregion

            //ts = DateTime.Now.Subtract(t1);
            //Console.WriteLine("屏幕比较发送等所需要时间："+ ts.Seconds.ToString() +":" +ts.Milliseconds.ToString());
            //t1 = DateTime.Now;

            #region 发送图片
            if (SendCount == 0)//如果一张图片也没发
            {
                SendData(nullPak);//发送一个空包用于打洞
                isSendImage = false;
                //Console.WriteLine("发送一个空包用于打洞！");
            }
            else //如果发送过图片，则要求对方进行校验
            {
                #region 所有图片发完后要求对方进行校验 
                bool isCheckTimer = false;
                bool isSendCheck = false;//是否发送校验包 
                SendCount++;//发送包数加1

                UdpFilePacket CheckPacket = new UdpFilePacket();
                CheckPacket.type = (byte)TransmitType.CheckPacket;
                CheckPacket.Index = SendCount;//本包索引

                System.Timers.Timer timer1 = new System.Timers.Timer();
                if (sendInterval < 1)
                    timer1.Interval = 1;
                else
                    timer1.Interval = sendInterval;
                timer1.Elapsed += (s, e) =>//判断对方是否完成所有包的接收
                {
                    if (isCheckTimer) return;//如果正在校验,退出
                    isCheckTimer = true;//标识正在校验

                    if (IsCancelTransmit)//如果传输被取消
                    {
                        timer1.Enabled = false; timer1.Close(); timer1.Dispose(); timer1 = null;
                        lock (Packets) Packets.Clear();//清空缓冲区并退出发送
                        return;
                    }

                    #region 计算发包间隔

                    int LostCount = Packets.Count;//丢包数  
                    if (LostCount > 1)
                        sendInterval += 4;
                    else if (LostCount == 0)
                    {
                        if (MTU > 1500)//如果是局域网
                        {
                            if (sendInterval > 3)
                            {
                                sendInterval -= 2;
                            }
                            else if (sendInterval > 1 && sendInterval <= 3)
                            {
                                sendInterval -= 1;
                            }
                        }
                        else//如果是广域网
                        {
                            if (sendInterval >= 3)
                            {
                                sendInterval -= 2;
                            }
                        }
                    }

                    //Console.WriteLine("丢包：" + LostCount.ToString() + ",发包间隔：" + sendInterval);

                    #endregion

                    if (LostCount > 0)//如果还有包，则继续接收
                    {
                        Console.WriteLine("丢包：" + LostCount.ToString());
                        byte[][] Datas = new byte[Packets.Count][];
                        lock (Packets)
                            Packets.Values.CopyTo(Datas, 0);
                        foreach (byte[] data in Datas)
                        {
                            SendData(data);//发送包
                            System.Threading.Thread.Sleep(sendInterval);//休息20毫秒
                        }
                        isCheckTimer = false;
                    }
                    else
                    {
                        if (!isSendCheck)//如果没有发送校验包,则发送
                        {
                            isSendCheck = true;//标识已经发送校验包

                            ///发送校验包
                            Packets.Add(SendCount, CheckPacket.Header);//添加校验包到发送缓冲区,以便重新发送丢包数据
                            SendData(CheckPacket.Header);//发送校验包
                            Console.WriteLine("发送校验包！");
                            isCheckTimer = false;//标记此轮执行结束
                        }
                        else//如果对方收到所有包(包括校验包)
                        {
                            timer1.Enabled = false; timer1.Close(); timer1.Dispose(); timer1 = null;
                            isCheckTimer = false;//
                            imageOldMD5 = imageNewMD5;//将新的图片MD5值重置为旧MD5
                            isSendImage = false;//标识没有发送

                            //ts = DateTime.Now.Subtract(t1);
                            //Console.WriteLine("发送完成所需要的时间：" + ts.Seconds.ToString() + ":" + ts.Milliseconds.ToString());
                        }
                    }
                };
                timer1.Enabled = true;
                #endregion
            }
            #endregion
        }
        #endregion 

        #region 比较两字节数组是否相等
        /// <summary>
        /// 比较两字节数组是否相等
        /// </summary>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <returns></returns>
        private bool ArrayEquals(byte[] b1, byte[] b2)
        { 
            if (b1 == null || b2 == null) return false;
            if (b1.Length != b2.Length) return false;
            if (ImageCompare.MemoryCompare(b1, b2) == 0) return true;
            else return false;
        }
        #endregion

        #region 处理对方发送校验数据包
        /// <summary>
        /// 处理对方发送校验数据包
        /// </summary>
        private void ReceivedCheckPacket(UdpFilePacket udpFilePacket)//当对方发送文件数据块过来
        {
            //如果对方要求保存数据
            if (TimeStamp == udpFilePacket.Index) return;//如果收到重复时间戳，返回
            TimeStamp = udpFilePacket.Index;

            DrawDesktop(); //绘制图片

            TimeStamp = (int)DateTime.Now.Ticks;//重置时间戳

            SendData(udpFilePacket.Header);//将包头数据进行返回给对方，确认收到此数据包 
        }
        #endregion

        #region 处理收到的文件数据包
        /// <summary>
        /// 处理对方发送文件数据包
        /// </summary>
        private void ReceivedFilePacket(UdpFilePacket udpFilePacket)//当对方发送文件数据块过来
        {
            if (!UdpImagePackets.ContainsKey(udpFilePacket.Index))//如果包没有收到
                lock (UdpImagePackets)
                    UdpImagePackets.Add(udpFilePacket.Index, new UdpImagePacket(udpFilePacket.Payload));//将数据包缓存起来
            SendData(udpFilePacket.Header);//将包头数据进行返回给对方，确认收到此数据包 
        }
        #endregion

        #region 保存文件数据并进行绘图处理
        /// <summary>
        /// 保存文件数据并进行绘图处理
        /// </summary>
        private void DrawDesktop()
        {
            if (UdpImagePackets.Count > 0)
            {
                //Console.WriteLine("数据拷贝成功....");

                #region 组包算法,获得所有n张图片

                foreach (UdpImagePacket imagePak in UdpImagePackets.Values)//先保存旧图片
                {
                    if (imagePak.IsExsit && imagePak.ExsitIsOld)//如果图片存在并且是旧图片
                    {
                        RecNewImages[imagePak.Index] = RecOldImages[imagePak.ExsitIndex];//先保存旧图片
                        //Console.WriteLine("保存旧图片成功....");
                    }
                    else if (!imagePak.IsExsit)//如果图片不存在，则是新图片，组包生成新图片
                    {
                        RecImagesBuffer[imagePak.Index].Add(imagePak.SequenceNumber, imagePak.Payload);
                        //Console.WriteLine("添加每一个图片小包成功....");
                    }
                }
                for (int i = 0; i < RecImagesBuffer.Length; i++)
                {
                    if (RecImagesBuffer[i].Count > 0)//如果有包，则组包
                    {
                        List<byte> result = new List<byte>();
                        foreach (byte[] data in RecImagesBuffer[i].Values)
                            result.AddRange(data);
                        RecNewImages[i] = WebP.WebPFormat.Load(result.ToArray());//将组包后的字节转换为图片
                        result.Clear(); result = null;
                        RecImagesBuffer[i].Clear();//清除所有数据，以便下次组包
                    }
                }
                //Console.WriteLine("组包每一个图片成功....");
                foreach (UdpImagePacket imagePak in UdpImagePackets.Values)//最后获取重复的新图片
                {
                    if (imagePak.IsExsit && !imagePak.ExsitIsOld)//如果图片存在并且是旧图片
                        RecNewImages[imagePak.Index] = RecNewImages[imagePak.ExsitIndex];//先保存旧图片
                }
                //Console.WriteLine("获取重复的新图片成功....");

                UdpImagePackets.Clear();//清空缓冲区

                #endregion

                #region 触发收到第一侦图片事件
                if (!IsReceiveFirstImage)
                {
                    IsReceiveFirstImage = true;//表示收到第一侦图片
                    if (ReceiveFirstImage != null)//触发获得第一侦图片事件
                        ReceiveFirstImage(this, null);
                }
                #endregion

                #region 绘图算法1
                if (picBox != null)
                {
                    Graphics g;
                    Rectangle r;
                    int index = 0;
                    for (int i = 0; i < splitHeightCount; i++)
                        for (int j = 0; j < splitWidthCount; j++)
                        {
                            if (RecNewImages[index] != null)
                                RecOldImages[index] = RecNewImages[index]; //将收到的老图片重置为新图片 

                            r = new Rectangle(j * RecOldImages[index].Width, i * RecOldImages[index].Height, RecOldImages[index].Width, RecOldImages[index].Height);
                            g = picBox.CreateGraphics();
                            g.DrawImage(RecOldImages[index], r);
                            g.Flush();
                            RecNewImages[index] = null;
                            index++;
                        }
                    g = picBox.CreateGraphics();
                    //yufh 2015-01-05 和租李叶沟通后注释水印代码
                    //DateTime t = DateTime.Now;
                    //if (t.Year > 2014 && t.DayOfWeek == DayOfWeek.Monday)
                    //{
                    //    g.DrawString("[免费软件------严禁商用]", font, Brushes.Red, 0, picbox.Height / 2);
                    //    g.DrawString("租李叶(25348855)版权所有", font, Brushes.Red, 0, picbox.Height / 2 + 20);
                    //}
                    g.Flush();
                }
                #endregion

                #region  绘图算法2
                //if (picBox != null)
                //{
                //    int index = 0;
                //    Bitmap tempBitmap;
                //    float screenPix = (float)RemoteDesktopWidth / (float)RemoteDesktopHeight;
                //    if (screenPix >= 1.6)
                //        tempBitmap = new Bitmap(1280, 720);
                //    else
                //        tempBitmap = new Bitmap(1024, 768);

                //    Graphics gr = Graphics.FromImage(tempBitmap);
                //    gr.Clear(Color.Transparent);

                //    for (int i = 0; i < splitHeightCount; i++)
                //        for (int j = 0; j < splitWidthCount; j++)
                //        {
                //            if (RecNewImages[index] != null)
                //                RecOldImages[index] = RecNewImages[index]; //将收到的老图片重置为新图片  
                //             gr.DrawImage(RecOldImages[index], new Rectangle(j * RecOldImages[index].Width, i * RecOldImages[index].Height, RecOldImages[index].Width, RecOldImages[index].Height),0, 0, RecOldImages[index].Width, RecOldImages[index].Height, GraphicsUnit.Pixel);
                //             RecNewImages[index] = null;
                //            index++;
                //        }
                //    gr.Save();
                //    Graphics g;
                //    Rectangle r;
                //    r = new Rectangle(0, 0, picBox.Width, picBox.Height);
                //    g = picBox.CreateGraphics();
                //    g.DrawImage(tempBitmap, r);
                //    if (DateTime.Now.Year > 2014)
                //    {
                //        g.DrawString("[免费软件------严禁商用]", font, Brushes.Red, 0, picbox.Height / 2);
                //        g.DrawString("租李叶(23348855)版权所有", font, Brushes.Red, 0, picbox.Height / 2 + 20);
                //    } 
                //    g.Flush();
                //}
                #endregion
            }
        }
        #endregion
         
        #region 请求对方开始发送数据包
        /// <summary>
        /// 请求对方开始发送数据包
        /// </summary>
        private void RequestSendDesktop()
        {
            System.Threading.Thread.Sleep(1000);

            if (isRequestSend) return;
            isRequestSend = true;//只能发送一次请求 
            Console.WriteLine("请求对方发送桌面!");

            for (int i = 0; i < RecImagesBuffer.Length; i++)
                if (RecImagesBuffer[i] == null)
                    RecImagesBuffer[i] = new SortedList<int, byte[]>();

            RequestSendFilePakMsg.type = (byte)TransmitType.SendFile;//请求发送文件
            RequestSendFilePakMsg.Index = TimeStamp;//获利当前时间戳 
            SendData(RequestSendFilePakMsg.Header);//请求对方发送文件数据  
            System.Threading.Thread.Sleep(500);
            SendData(RequestSendFilePakMsg.Header);//请求对方发送文件数据  
            System.Threading.Thread.Sleep(500);
            SendData(RequestSendFilePakMsg.Header);//请求对方发送文件数据  

        }
        #endregion

        #region 发送鼠标命令
        /// <summary>
        /// 发送鼠标命令
        /// </summary>
        /// <param name="orderType"></param>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Delta"></param>
        private void sendMouseOrder(UdpRemoteDesketOrder.OrderType orderType, int X, int Y,int Delta)
        {
            UdpRemoteDesketOrder DesketOrder = new UdpRemoteDesketOrder();
            DesketOrder.orderType = orderType;
            DesketOrder.Delta = Delta;

            if (uX != 1.0)
                DesketOrder.X = (int)(X * uX + 1);
            else
                DesketOrder.X = (int)(X * uX);

            if (uY != 1.0)
                DesketOrder.Y = (int)(Y * uY + 1);
            else
                DesketOrder.Y = (int)(Y * uY);
             
            sendControlPacket(DesketOrder);//发送命令
        }
        #endregion

        #region 模拟鼠标执行相应操作
        /// <summary>
        /// 模拟鼠标执行相应操作
        /// </summary>
        /// <param name="mouseEvent">指定的鼠标事件</param>
        private void MouseWork(IMLibrary4.Protocol.UdpRemoteDesketOrder mouseEvent)
        {
            switch (mouseEvent.orderType)
            {
                case UdpRemoteDesketOrder.OrderType.MouseMove:
                    MouseKeyboard.MouseSimulator.X = mouseEvent.X;
                    MouseKeyboard.MouseSimulator.Y = mouseEvent.Y;
                    break;
                case UdpRemoteDesketOrder.OrderType.MouseDoubleClick:
                    MouseKeyboard.MouseSimulator.X = mouseEvent.X;
                    MouseKeyboard.MouseSimulator.Y = mouseEvent.Y;
                    MouseKeyboard.MouseSimulator.DoubleClick(MouseButtons.Left);
                    break;
                case UdpRemoteDesketOrder.OrderType.MouseClick:
                    MouseKeyboard.MouseSimulator.X = mouseEvent.X;
                    MouseKeyboard.MouseSimulator.Y = mouseEvent.Y;
                    MouseKeyboard.MouseSimulator.Click(MouseButtons.Left);
                    break;
                case UdpRemoteDesketOrder.OrderType.MouseLeftDown:
                    MouseKeyboard.MouseSimulator.X = mouseEvent.X;
                    MouseKeyboard.MouseSimulator.Y = mouseEvent.Y;
                    MouseKeyboard.MouseSimulator.MouseDown(MouseButtons.Left);
                    break;
                case UdpRemoteDesketOrder.OrderType.MouseLeftUp:
                    MouseKeyboard.MouseSimulator.X = mouseEvent.X;
                    MouseKeyboard.MouseSimulator.Y = mouseEvent.Y;
                    MouseKeyboard.MouseSimulator.MouseUp(MouseButtons.Left);
                    break;
                case UdpRemoteDesketOrder.OrderType.MouseRightDown:
                    MouseKeyboard.MouseSimulator.X = mouseEvent.X;
                    MouseKeyboard.MouseSimulator.Y = mouseEvent.Y;
                    MouseKeyboard.MouseSimulator.MouseDown(MouseButtons.Right);
                    break;
                case UdpRemoteDesketOrder.OrderType.MouseRightUp:
                    MouseKeyboard.MouseSimulator.X = mouseEvent.X;
                    MouseKeyboard.MouseSimulator.Y = mouseEvent.Y;
                    MouseKeyboard.MouseSimulator.MouseUp(MouseButtons.Right);
                    break;
                case UdpRemoteDesketOrder.OrderType.MouseWheel:
                    MouseKeyboard.MouseSimulator.X = mouseEvent.X;
                    MouseKeyboard.MouseSimulator.Y = mouseEvent.Y;
                    MouseKeyboard.MouseSimulator.MouseWheel(0);
                    break;
            }
        }
        #endregion

        #region 收到控制命令，处理命令
        /// <summary>
        /// 收到控制命令，处理命令
        /// </summary>
        /// <param name="DesketOrder"></param>
        private void ExcuteOrder(UdpRemoteDesketOrder DesketOrder)
        {
            if (DesketOrder.orderType == UdpRemoteDesketOrder.OrderType.KeyDown)
            {
                MouseKeyboard.KeyboardSimulator.KeyDown((Keys)DesketOrder.KeyCode);
            }
            else if (DesketOrder.orderType == UdpRemoteDesketOrder.OrderType.KeyUp)
            {
                MouseKeyboard.KeyboardSimulator.KeyUp((Keys)DesketOrder.KeyCode);
            }
            else
            {
                MouseWork(DesketOrder);
            }
        }
        #endregion

        #region 发送远程控制命令
        /// <summary>
        /// 发送远程控制命令
        /// </summary>
        /// <param name="DesketOrder"></param>
        private void sendControlPacket(UdpRemoteDesketOrder DesketOrder)
        {
            UdpFilePacket pak = new UdpFilePacket();
            pak.type = (byte)TransmitType.RemoteControl;
            pak.Payload = DesketOrder.ToBytes();
            SendData(pak.ToBytes());
        }
        #endregion

        #region 公共方法

        #region UDP服务
        /// <summary>
        /// 开始
        /// </summary>
        public void Start( )
        {
            if (IsIni) return;
            IsIni = true;
    
            #region 收到数据事件
            ReceiveData += (sender, data) =>
            {
                ReceiveUdpData(data);//执行收到数据事件
            };
            #endregion

            #region 网络成功联接事件
            TransmitConnected += (sender, connectedType) =>
            {
                if (connectedType != ConnectedType.None)
                { 
                    if (IsControler)  
                        RequestSendDesktop(); 
                }
            };
            #endregion 

            StartNetConnection();//开始连接对端
        }
        #endregion 

        #region 设置远程主机信息和远程屏幕尺寸
        /// <summary>
        /// 设置远程主机信息和远程屏幕尺寸
        /// </summary>
        /// <param name="remoteLocalIP"></param>
        /// <param name="remoteIP"></param>
        /// <param name="remoteScreenWidth"></param>
        /// <param name="remoteScreenHeigth"></param>
        public void setRemoteIPandScreen(IPEndPoint remoteLocalIP, IPEndPoint remoteIP, int remoteScreenWidth, int remoteScreenHeigth)
        {
            RemoteDesktopWidth = remoteScreenWidth;
            RemoteDesktopHeight = remoteScreenHeigth;

            float screenPix = (float)RemoteDesktopWidth / (float)RemoteDesktopHeight;
            if (screenPix >= 1.6)
            {
                uX = (float)RemoteDesktopWidth / 1280;//设置放大系数
                uY = (float)RemoteDesktopHeight / 720;//设置放大系数
            }
            else
            {
                uX = (float)RemoteDesktopWidth / 1024;//设置放大系数
                uY = (float)RemoteDesktopHeight / 768;//设置放大系数
            }
            setRemoteIP(remoteLocalIP, remoteIP);
        }
        #endregion

        #region 发送键盘键值
        /// <summary>
        /// 发送键盘键值
        /// </summary>
        /// <param name="key"></param>
        public void sendKeyUp(Keys key)
        {
            UdpRemoteDesketOrder DesketOrder = new UdpRemoteDesketOrder();
            DesketOrder.orderType = UdpRemoteDesketOrder.OrderType.KeyUp;
            DesketOrder.KeyCode = (byte)key;
            sendControlPacket(DesketOrder);//发送命令
        }
        /// <summary>
        /// 发送键盘键值
        /// </summary>
        /// <param name="key"></param>
        public void sendKeyDown(Keys key)
        {
            UdpRemoteDesketOrder DesketOrder = new UdpRemoteDesketOrder();
            DesketOrder.orderType = UdpRemoteDesketOrder.OrderType.KeyDown;
            DesketOrder.KeyCode = (byte)key;
            sendControlPacket(DesketOrder);//发送命令
        }
        #endregion

        #region 取消传输
        /// <summary>
        /// 取消传输
        /// </summary>
        public void CancelTransmit()
        {
            IsCancelTransmit = true;//标识文件发送结束  
            if (timerSend != null)//禁止发送数据
            {
                timerSend.Enabled = false;
                timerSend.Close(); timerSend.Dispose(); timerSend = null;
            }
            CloseSock();
        }
        #endregion

        #endregion 
    } 
}
