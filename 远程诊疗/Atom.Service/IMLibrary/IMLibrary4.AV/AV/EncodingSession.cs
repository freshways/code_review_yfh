﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace IMLibrary4.AV
{
    class EncodingSession
    {
        private readonly Coder.Encoder Encoder ;

        public EncodingSession(int width, int height, int fps)
        {
            Encoder = new Coder.Encoder(width, height, fps);
        }

        public byte[] Encode(Image frame)
        {
            return Encoder.Encode((Bitmap)frame);
        }
    }
}
