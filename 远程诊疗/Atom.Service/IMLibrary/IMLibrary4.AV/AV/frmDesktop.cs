﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IMLibrary4.AV
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frmDesktop : Form
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsSelfClose = true;

        private string Title = "对方屏幕";

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="size">对方屏幕尺寸</param>
        /// <param name="title"></param>
        public frmDesktop(Size size,string title)
        {
            InitializeComponent();
            float screenPix = (float)size.Width / (float)size.Height;
            if (screenPix >= 1.6)
                pictureBox1.Size = new Size(1280, 720);
            else
                pictureBox1.Size = new Size(1024, 768);
            Title = title;
            this.Text = Title;
        }

        private void panel1_SizeChanged(object sender, EventArgs e)
        {
            if (panel1.Width > pictureBox1.Width)
                pictureBox1.Left = (panel1.Width - pictureBox1.Width) / 2;
            else
                pictureBox1.Left = 0;
            if (panel1.Height > pictureBox1.Height)
                pictureBox1.Top = (panel1.Height - pictureBox1.Height) / 2;
            else
                pictureBox1.Top = 0;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            this.Text =Title +"["+ e.X.ToString() + ":" + e.Y.ToString()+"]";
        }
    }
}
