﻿////////////////////////////////////////////////////////////////////////////////////////////////////
// File: NoesisWebP.cs
// 
// Copyright 2010 Noesis Innovation Inc. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Runtime.InteropServices;


namespace IMLibrary4.WebP
{
    internal static class API
    {
        [DllImport("ImageCode.dll", EntryPoint = "NoesisWebPGetDimensions", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetDimensions(out int oWidth, out int oHeight, IntPtr iWebPFileImage, int iWebPFileImageSize);

        [DllImport("ImageCode.dll", EntryPoint = "NoesisWebPLoad", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Load(IntPtr iBitmap, IntPtr iWebPFileImage, int iWebPFileImageSize);

        [DllImport("ImageCode.dll", EntryPoint = "NoesisWebPSave", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Save(out IntPtr oWebPFileImage, out int oWebPFileImageSize, IntPtr iBitmap, int iWidth, int iHeight, int iQuality);

        [DllImport("ImageCode.dll", EntryPoint = "NoesisWebPFreeImage", CallingConvention = CallingConvention.Cdecl)]
        public static extern void FreeImage(IntPtr iWebPFileImage);
    }
}
