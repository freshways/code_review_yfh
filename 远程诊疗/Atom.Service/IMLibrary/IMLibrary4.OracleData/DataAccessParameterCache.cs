namespace IMLibrary4.OracleData
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Data.OracleClient;

    /// <summary>
    /// 快速缓冲区参数类
    /// </summary>
    public sealed class DataAccessParameterCache
    {
        private static Hashtable paramCache = Hashtable.Synchronized(new Hashtable());
        private static Hashtable paramDirections = Hashtable.Synchronized(new Hashtable());
        private static Hashtable paramTypes = Hashtable.Synchronized(new Hashtable());

        static DataAccessParameterCache()
        {
            paramTypes.Add("BFile", OracleType.BFile);
            paramTypes.Add("Blob", OracleType.Blob);
            paramTypes.Add("Byte", OracleType.Byte);
            paramTypes.Add("Char", OracleType.Char);
            paramTypes.Add("Clob", OracleType.Clob);
            paramTypes.Add("Cursor", OracleType.Cursor);
            paramTypes.Add("DateTime", OracleType.DateTime);
            paramTypes.Add("Double", OracleType.Double);
            paramTypes.Add("Float", OracleType.Float);
            paramTypes.Add("Int16", OracleType.Int16);
            paramTypes.Add("Int32", OracleType.Int32);
            paramTypes.Add("IntervalDayToSecond", OracleType.IntervalDayToSecond);
            paramTypes.Add("IntervalYearToMonth", OracleType.IntervalYearToMonth);
            paramTypes.Add("LongRaw", OracleType.LongRaw);
            paramTypes.Add("LongVarChar", OracleType.LongVarChar);
            paramTypes.Add("NChar", OracleType.NChar);
            paramTypes.Add("NClob", OracleType.NClob);
            paramTypes.Add("Number", OracleType.Number);
            paramTypes.Add("NVarChar", OracleType.NVarChar);
            paramTypes.Add("Raw", OracleType.Raw);
            paramTypes.Add("RowId", OracleType.RowId);
            paramTypes.Add("SByte", OracleType.SByte);
            paramTypes.Add("Timestamp", OracleType.Timestamp);
            paramTypes.Add("TimestampLocal", OracleType.TimestampLocal);
            paramTypes.Add("TimestampWithTZ", OracleType.TimestampWithTZ);
            paramTypes.Add("UInt16", OracleType.UInt16);
            paramTypes.Add("UInt32", OracleType.UInt32);
            paramTypes.Add("VarChar", OracleType.VarChar);

            
            paramDirections.Add((short) 1, ParameterDirection.Input);
            paramDirections.Add((short) 2, ParameterDirection.InputOutput);
            paramDirections.Add((short) 4, ParameterDirection.ReturnValue);
        }

        private DataAccessParameterCache()
        {
        }

        /// <summary>
        /// 参数设置
        /// </summary>
        /// <param name="connectionString">联接字符串</param>
        /// <param name="commandText">命令文件</param>
        /// <param name="commandParameters">命令参数</param>
        public static void CacheParameterSet(string connectionString, string commandText, params OracleParameter[] commandParameters)
        {
            string text = connectionString + ":" + commandText;
            paramCache[text] = commandParameters;
        }

        private static OracleParameter[] CloneParameters(OracleParameter[] originalParameters)
        {
            OracleParameter[] parameterArray = new OracleParameter[originalParameters.Length];
            int index = 0;
            int length = originalParameters.Length;
            while (index < length)
            {
                parameterArray[index] = (OracleParameter) ((ICloneable) originalParameters[index]).Clone();
                index++;
            }
            return parameterArray;
        }

        private static OracleParameter[] DiscoverSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
        {
            OracleParameter[] parameterArray;
            int num;
            DataTable dataTable = new DataTable("paramDescriptions");
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                OracleCommand selectCommand = new OracleCommand("sp_procedure_params_rowset", connection);
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.AddWithValue("@procedure_name", spName);
                new OracleDataAdapter(selectCommand).Fill(dataTable);
            }
            if (dataTable.Rows.Count <= 0)
            {
                throw new ArgumentException("Stored procedure '" + spName + "' not found", "spName");
            }
            if (includeReturnValueParameter)
            {
                parameterArray = new OracleParameter[dataTable.Rows.Count];
                num = 0;
            }
            else
            {
                parameterArray = new OracleParameter[dataTable.Rows.Count - 1];
                num = 1;
            }
            int index = 0;
            int length = parameterArray.Length;
            while (index < length)
            {
                DataRow row = dataTable.Rows[index + num];
                parameterArray[index] = new OracleParameter();
                parameterArray[index].ParameterName = (string) row["PARAMETER_NAME"];
                parameterArray[index].OracleType = (OracleType)paramTypes[(string)row["TYPE_NAME"]];
                parameterArray[index].Direction = (ParameterDirection) paramDirections[(short) row["PARAMETER_TYPE"]];
                parameterArray[index].Size = (row["CHARACTER_OCTET_LENGTH"] == DBNull.Value) ? 0 : ((int) row["CHARACTER_OCTET_LENGTH"]);
                parameterArray[index].Precision = (row["NUMERIC_PRECISION"] == DBNull.Value) ? ((byte) 0) : ((byte) ((short) row["NUMERIC_PRECISION"]));
                parameterArray[index].Scale = (row["NUMERIC_SCALE"] == DBNull.Value) ? ((byte) 0) : ((byte) ((short) row["NUMERIC_SCALE"]));
                index++;
            }
            return parameterArray;
        }

        /// <summary>
        /// 获取参数设置 
        /// </summary>
        /// <param name="connectionString">联接字符串</param>
        /// <param name="commandText">命令文本</param>
        /// <returns></returns>
        public static OracleParameter[] GetCachedParameterSet(string connectionString, string commandText)
        {
            string text = connectionString + ":" + commandText;
            OracleParameter[] originalParameters = (OracleParameter[]) paramCache[text];
            if (originalParameters == null)
            {
                return null;
            }
            return CloneParameters(originalParameters);
        }

        /// <summary>
        /// 获取存储过程参数
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="spName">存储过程名称</param>
        /// <returns></returns>
        public static OracleParameter[] GetSpParameterSet(string connectionString, string spName)
        {
            return GetSpParameterSet(connectionString, spName, false);
        }

        /// <summary>
        /// 获取存储过程参数
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="spName">存储过程名称</param>
        /// <param name="includeReturnValueParameter">是否包括返回值参数</param>
        /// <returns></returns>
        public static OracleParameter[] GetSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
        {
            string text = connectionString + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");
            OracleParameter[] originalParameters = (OracleParameter[]) paramCache[text];
            if (originalParameters == null)
            {
                object obj2;
                paramCache[text] = obj2 = DiscoverSpParameterSet(connectionString, spName, includeReturnValueParameter);
                originalParameters = (OracleParameter[]) obj2;
            }
            return CloneParameters(originalParameters);
        }
    }
}

