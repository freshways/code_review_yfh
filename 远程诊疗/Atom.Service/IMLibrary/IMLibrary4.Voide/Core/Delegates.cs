﻿// OurSoftware Core Library
// OurSoftware.NET framework
// http://www.OurSoftwarenet.com/framework/
//
// Copyright © OurSoftware.NET, 2007-2011
// contacts@OurSoftwarenet.com
//

namespace OurSoftware
{
    using System;

    /// <summary>
    /// A delegate which is used by events notifying abount sent/received message.
    /// </summary>
    /// 
    /// <param name="sender">Event sender.</param>
    /// <param name="eventArgs">Event arguments containing details about the transferred message.</param>
    ///
    public delegate void MessageTransferHandler( object sender, CommunicationBufferEventArgs eventArgs );
}
