﻿using System;
using System.Collections.Generic;
using System.Text;
using OurSoftware.Video;
using OurSoftware.Video.DirectShow;
using System.Drawing;

namespace IMLibrary4.Voide
{
    public class Capture
    {
        private VideoCaptureDevice VideoSource;

        public event EventHandler<CaptureEventArgs> ImageCapture;

        public Capture(FilterInfo videoDevice, int width, int height)
        {
            VideoSource = new VideoCaptureDevice(videoDevice.MonikerString);
            VideoSource.DesiredFrameSize = new Size(width, height);
            VideoSource.NewFrame += NewFrame;
        }

        public void Start()
        {
            VideoSource.Start();
        }

        public void Stop()
        {
            VideoSource.SignalToStop();
        }

        private void NewFrame(object sender, NewFrameEventArgs e)
        {
            var handler = ImageCapture;
            if (handler != null)
            {
                handler(this, new CaptureEventArgs
                {
                    Image = e.Frame
                });
            }
        }
    }
}
