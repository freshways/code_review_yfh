﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing;
using System.Threading;

namespace IMLibrary4.Voide
{
    public class CaptureEventArgs : EventArgs
    {
        public Bitmap Image { get; set; }
    }
}
