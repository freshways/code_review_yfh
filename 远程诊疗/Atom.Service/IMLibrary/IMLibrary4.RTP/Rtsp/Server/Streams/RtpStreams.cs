﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMLibrary4.Rtsp.Server.Streams
{
    /// <summary>
    /// Adds an abstract RtpSession To SourceStream,   
    /// This could also just be an interface, could have protected set for RtpSession
    /// could also be a class which suscribes to events from the assigned RtpSession for RtpPackets etc
    /// </summary>
    public abstract class RtpSource : SourceStream
    {
        public const string RtpMediaProtocol = "RTP/AVP";

        Sdp.SessionDescription m_Sdp = new Sdp.SessionDescription(1);

        public RtpSource(string name, Uri source) : base(name, source) { }
        
        public bool DisableRtcp { get { return m_DisableQOS; } set { m_DisableQOS = value; } }

        public abstract Rtp.RtpSession RtpSession { get; }

        public virtual Sdp.SessionDescription SessionDescription { get { return m_Sdp; } protected set { m_Sdp = value; } }
    }

    //public abstract class RtpChildStream
    //{
    //}

}
