﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;


#region IMLibrary4
using IMLibrary4.Security;
using IMLibrary4.Organization;
using IMLibrary4.Operation;
using IMLibrary4.Protocol;
using IMLibrary4.IO;
using IMLibrary4.Net;
using IMLibrary4.Net.TCP;
#endregion

namespace IMLibrary4
{
    /// <summary>
    /// Tcp离线文件传输客户端
    /// </summary>
    public class TcpOfflineFileClient : FileTransmitBase 
    {
        #region 构造
        /// <summary>
        /// 构造(上传文件)
        /// </summary>
        /// <param name="serverEP">服务器主机信息</param>
        /// <param name="fullFileName">上传文件（含路径）</param>
        public TcpOfflineFileClient(IPEndPoint serverEP, string fullFileName)
            :base(serverEP,fullFileName)
        {
            mtu = 10240;//1次上传10K
        }

        /// <summary>
        /// 构造(下载文件)
        /// </summary>
        /// <param name="serverEP">服务器主机信息</param>
        /// <param name="tFileInfo">下载文件信息</param>
        public TcpOfflineFileClient(IPEndPoint serverEP, TFileInfo tFileInfo)
            : base(serverEP, tFileInfo)
        {
            mtu = 10240;//1次下载10K
        }
        #endregion

        #region 事件
        /// <summary>
        /// 文件正被其他人传输...
        /// </summary>
        public event fileTransmitEventHandler fileElseTransmiting;
        /// <summary>
        /// 文件正被其他人传输...事件
        /// </summary>
        protected virtual void OnfileElseTransmiting()
        {
            if (fileElseTransmiting != null)
                fileElseTransmiting(this, new fileTransmitEvnetArgs(TFileInfo));
        }
        #endregion

        #region 变量
        /// <summary>
        /// 文件发送者ID
        /// </summary>
        public string from = "";
        /// <summary>
        /// 文件接收者ID
        /// </summary>
        public string to = "";
        #endregion

        #region TCP操作

        #region strat
        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            if (tcpClient == null || tcpClient.IsDisposed)
            {
                tcpClient = new TCPClient();
                tcpClient.PacketReceived += new TCP_Client.PacketReceivedEventHandler(tcpClient_PacketReceived);
                tcpClient.Disonnected += new EventHandler(tcpClient_Disonnected);
            }
            if (!tcpClient.IsConnected)
                tcpClient.Connect(ServerEP.Address.ToString(), ServerEP.Port);

            if (tcpClient.IsConnected)
            {
                //IsConnected = true;//标记已经连接到服务器

                OnFileTransmitConnected();//触发文件传输TCP连接成功事件

                if (IsSend)//如果是上传文件
                {
                    OfflineFileMsg fileMsg = new OfflineFileMsg();
                    fileMsg.Name = TFileInfo.Name;
                    fileMsg.Length = TFileInfo.Length;
                    fileMsg.Extension = TFileInfo.Extension;

                    fileMsg.type = type.New;
                    TCPSendMsg(fileMsg);
                }
                else//如果是下载文件
                {
                    RequestSendFilePak();//如果是下载文件，则开始下载
                }
            }

            #region
            System.Timers.Timer timer = new System.Timers.Timer();
            if (timer == null)
            {
                int count = 0;
                timer = new System.Timers.Timer();
                timer.Interval = 3000;
                timer.Enabled = true;
                timer.Elapsed += delegate(object sender, System.Timers.ElapsedEventArgs e)
                {
                    if (IsConnected)//如果连接到服务器
                    {
                        timer.Enabled = false;
                        timer = null;
                    }
                    else if (count < 3)//如果9秒以后还未连接到服务器
                    {
                        tcpClient.Connect(ServerEP.Address.ToString(), ServerEP.Port);
                        Console.WriteLine("try connecte!" + count.ToString());
                    }
                    else if (count >= 3)//如果3秒后还未连接，则退出连接操作
                    {
                        Console.WriteLine("connecte filad!");
                        timer.Enabled = false;
                        timer = null;
                    }
                    count++;
                };
            }
            #endregion
        }
        #endregion

        #region Disonnected
        private void tcpClient_Disonnected(object sender, EventArgs e)
        {
            Console.WriteLine("断开连接!");
        }
        #endregion

        #region PacketReceived
        private void tcpClient_PacketReceived(object sender, TcpSessionEventArgs e)
        {
            object obj = Factory.CreateInstanceObject(e.Data);
            OfflineFileMsg fileMsg = obj as OfflineFileMsg;
            if (fileMsg!=null )//如果收到的消息对像不为空
            {
                if (fileMsg.type == type.New)//服务器允许上传文件
                { 
                    OnFileTransmitBefore(); }//触发文件传输前事件
                else if (fileMsg.type == type.set)//发送文件到服务器
                { 
                    TCPSendFile(fileMsg); }
                else if (fileMsg.type == type.get)//收到文件数据包
                { 
                    ReceivedFileBlock(fileMsg); }
                else if (fileMsg.type == type.over)//文件传输结束
                {
                    Console.WriteLine("over!");
                    OnFileTransmitted();//触发文件传输完成事件
                }
                else if (fileMsg.type == type.error)//文件传输错误
                {
                    OnFileTransmitError();//触发文件传输错误事件
                }
                else if (fileMsg.type == type.Else)
                {
                    OnfileElseTransmiting();
                }

            }
        }
        #endregion
         
        #endregion

        #region 方法

        #region TCP发送消息到文件代理服务器
        /// <summary>
        /// TCP发送消息到文件代理服务器
        /// </summary>
        /// <param name="e"></param>
        protected void TCPSendMsg(Element e)
        {
            if (ServerEP != null && tcpClient.IsConnected)
            {
                if (e is OfflineFileMsg)
                {
                    OfflineFileMsg finfo = e as OfflineFileMsg;
                    finfo.MD5 = TFileInfo.MD5;
                    finfo.Extension = TFileInfo.Extension;
                    finfo.from = from;
                    finfo.to = to;
                    tcpClient.TcpStream.WriteLine(Factory.CreateXMLMsg(finfo));
                }
            }
        }
        #endregion

        #region TCP发送文件
        /// <summary>
        /// TCP发送文件
        /// </summary>
        /// <param name="fileMsg"></param>
        protected void TCPSendFile(OfflineFileMsg fileMsg)
        {
            long currLength = fileMsg.LastLength;

            if (currLength >= TFileInfo.Length)
            {
                OnFileTransmitted();//触发文件传输结束事件
                return;//如果对方要求发送的数据块起始位置大于文件尺寸则认为是非法请求退出
            }

            if (!isTransmit)
            {
                isTransmit = true;//标记文件传输中
                OnFileTransmitBefore();//触发文件开始传输前事件
            }

            #region 如果当前是需要读写文件
            if (CurrRecLength % maxReadWriteFileBlock == 0)
            {
                CurrRecLength = 0;//断点清零,重新记忆

                if (fileBlock == null)
                    fileBlock = new byte[maxReadWriteFileBlock]; 

                //读文件到内存过程
                if ((TFileInfo.Length - currLength) < maxReadWriteFileBlock)//如果是最后一次读写文件，则将所有文件尾数据全部读入到内存
                    fileBlock = new byte[TFileInfo.Length - currLength];
                   
                ////////////////////////文件操作
                FS = new FileStream(TFileInfo.fullName, FileMode.Open, FileAccess.Read, FileShare.Read);
                FS.Seek(currLength, SeekOrigin.Begin);//上次发送的位置
                FS.Read(fileBlock, 0, fileBlock.Length);
                FS.Close();
                FS.Dispose();
                /////////////////////////// 
            }
            #endregion

             long offSet = CurrRecLength % this.maxReadWriteFileBlock;// 获得要发送的绝对位置
             

            if (offSet + mtu > fileBlock.Length)
                buffer = new byte[fileBlock.Length - offSet];//要发送的缓冲区
            
            Buffer.BlockCopy(fileBlock, (int)offSet, buffer, 0, buffer.Length);//将其保存于Buffer字节数组

            currLength += buffer.Length;
            CurrRecLength += buffer.Length;//断点设置

            fileMsg.type = type.set;//上传标记
            fileMsg.LastLength = currLength;
            fileMsg.fileBlock = buffer;// 
            TCPSendMsg(fileMsg);//发送文件到服务器

            TFileInfo.CurrLength = currLength;
            OnFileTransmitting();//触发收到或发送文件数据事件 

        }
        #endregion

        #region 处理收到的文件数据块
        /// <summary>
        /// 处理对方发送文件数据块
        /// </summary>
        private void ReceivedFileBlock(OfflineFileMsg fileMsg)//当对方发送文件数据块过来
        {
            if (fileMsg.fileBlock != null)
            {
                if (fileBlock == null) fileBlock = new byte[0];

                ///合并已下载的内容
                List<byte> result = new List<byte>();
                result.AddRange(fileBlock);
                result.AddRange(fileMsg.fileBlock);
                fileBlock = result.ToArray();
                ///
            }

            currGetPos += fileMsg.fileBlock.Length;//进行断点续传的断点 
            TFileInfo.CurrLength = currGetPos;//设置当前已传输位置

            OnFileTransmitting();//触发收到或发送文件数据事件 

            if (fileBlock.Length >= 1024000 || currGetPos == TFileInfo.Length)//如果缓冲区大于2M或文件已经传输完成
            {
                OpFile.Write(fileBlock, TFileInfo.fullName);
                fileBlock = new byte[0];
            }
            if (currGetPos != TFileInfo.Length)
                RequestSendFilePak();//请求对方发送文件的下一数据包
            else
                OnFileTransmitted();//如果文件传输完成，触发传输完成事件
        }
        #endregion

        #region  请求发送文件数据包
        /// <summary>
        /// 请求发送文件数据包
        /// </summary>
        private void RequestSendFilePak()
        {
            OfflineFileMsg fileMsg = new OfflineFileMsg();
            fileMsg.type = type.get;//标识下载
            fileMsg.LastLength = currGetPos;
            fileMsg.MD5 = TFileInfo.MD5;//要下载的文件MD5值
            TCPSendMsg(fileMsg);//请求服务器发送文件数据  
        }
        #endregion

        #region 取消文件传输
        /// <summary>
        /// 取消文件传输
        /// </summary>
        public void CancelTransmit()
        { 
            if (tcpClient !=null && !tcpClient.IsDisposed)
            {
                if (tcpClient.IsConnected)
                    tcpClient.Disconnect();
                tcpClient.Dispose();
            }
            if (FS!=null)
            {
                FS.Close(); FS.Dispose(); FS = null;
            } 
        }
        #endregion

        #endregion
    }
}
