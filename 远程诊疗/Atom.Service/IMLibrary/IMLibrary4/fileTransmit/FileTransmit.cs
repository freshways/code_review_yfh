﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using IMLibrary4.Net ;

namespace IMLibrary4.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FileTransmit : UserControl
    {
        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="from">文件发送者ID</param>
        /// <param name="to">文件接收者ID</param>
        /// <param name="offlineFileServerEP">离线文件服务器主机信息</param>
        public FileTransmit(string from,string to,IPEndPoint offlineFileServerEP)
        {
            InitializeComponent();
            this.from = from;
            this.to = to;
            this.OfflineFileServerEP = offlineFileServerEP;
            linkLabelCancel.Click += new EventHandler(linkLabelCancel_Click);
            linkLabelReceive.Click += new EventHandler(linkLabelReceive_Click);
            linkLabelResume.Click += new EventHandler(linkLabelResume_Click);
            linkLabelSaveAs.Click += new EventHandler(linkLabelSaveAs_Click);
            linkLabelOffline.Click += new EventHandler(linkLabelOffline_Click);
            linkLabelNext.Click += new EventHandler(linkLabelNext_Click);
        } 
        #endregion
         
        #region 属性
        /// <summary>
        /// 
        /// </summary>
        private IPEndPoint OfflineFileServerEP;
        /// <summary>
        /// 文件发送者ID
        /// </summary>
        private string from = "";
        /// <summary>
        /// 文件接收者ID
        /// </summary>
        private string to = "";

        /// <summary>
        /// 标识文件传输是离线传输还是P2P实时传输（true为离线文件传输，false为P2P）
        /// </summary>
        private bool isOfflineFileTransmit = false;

        /// <summary>
        /// 传输文件的信息
        /// </summary>
        public TFileInfo TFileInfo = new TFileInfo();

        private TcpOfflineFileClient _tcpOfflineFileClient = null;
        /// <summary>
        /// 离线文件传输组件
        /// </summary>
        public  TcpOfflineFileClient tcpOfflineFileClient
        {
            set
            {
                _tcpOfflineFileClient = value;
                _tcpOfflineFileClient.from = this.from;
                _tcpOfflineFileClient.to = this.to;

                if (value != null)
                {
                    this.labelFileLengthString.Text = value.TFileInfo.LengthStr;
                    this.labelFileName.Text = value.TFileInfo.Name;
                    this.isSend = value.IsSend;

                    this.isOfflineFileTransmit = true;//标识文件传输是离线传输

                    if (isSend)
                        this.icon = FileIcon.GetFileIcon(value.TFileInfo.fullName).ToBitmap();
                    else
                        this.icon = FileIcon.GetFileIcon(value.TFileInfo.Extension).ToBitmap();

                    setEvent(value);
                  
                    if (!isSend)
                    {
                        string CacheFile = "FileCache\\" + value.TFileInfo.MD5;
                        if (File.Exists(CacheFile))//如果缓存文件存在，则触发可断点续传事件
                        {
                            this.linkLabelReceive.Visible = false;
                            this.linkLabelResume.Visible = true;
                        }

                        this.linkLabelNext.Visible = true;
                    }

                }
            }
            get { return _tcpOfflineFileClient; }
        }
         

        private  p2pFileClient  _P2PFileTransmit = null;
        /// <summary>
        /// P2P文件传输组件
        /// </summary>
        public p2pFileClient P2PFileTransmit
        {
            set
            {
                _P2PFileTransmit = value;
                if (value != null)
                {
                    this.TFileInfo = value.TFileInfo;

                    this.labelFileLengthString.Text = value.TFileInfo.LengthStr;
                    this.labelFileName.Text = value.TFileInfo.Name;
                    this.isSend = value.IsSend;

                    if (isSend)
                        this.icon = FileIcon.GetFileIcon(value.TFileInfo.fullName).ToBitmap();
                    else
                        this.icon = FileIcon.GetFileIcon(value.TFileInfo.Extension).ToBitmap();

                    setEvent(value);

                    if (!isSend)
                    {
                        string CacheFile = "FileCache\\" + value.TFileInfo.MD5;
                        if (File.Exists(CacheFile))//如果缓存文件存在，则触发可断点续传事件
                        {
                            this.linkLabelReceive.Visible = false;
                            this.linkLabelResume.Visible = true;
                        }
                    }
                }
            }
            get { return _P2PFileTransmit; }
        }
         

        bool _isSend = false;
        /// <summary>
        /// 发送方
        /// </summary>
        public bool isSend
        {
            set
            {
                _isSend = value;
                if (value)
                {
                    this.labelRequest.Text = "发送文件请求：";
                    this.linkLabelReceive.Visible = false;
                    this.linkLabelSaveAs.Visible = false;
                    this.linkLabelOffline.Visible = true;
                }
                else
                {
                    this.labelRequest.Text = "接收文件请求：";
                }
            }
            get { return _isSend; }
        }

        /// <summary>
        /// 获取图标
        /// </summary>
        public Image icon
        {
            set
            {
                if (value != null)
                    pictureBox1.Image = value;
            }
            get { return pictureBox1.Image; }
        }
        #endregion

        /// <summary>
        /// 设置事件
        /// </summary>
        /// <param name="value"></param>
        private void setEvent(p2pFileClient value)
        {
            value.fileTransmitConnected += (s, e) => showInfo(TransmitState.Connected, e.fileInfo);
            value.fileTransmitting += (s, e) => showInfo(TransmitState.Transmitting, e.fileInfo);
            value.fileTransmitOutTime += (s, e) => showInfo(TransmitState.OutTime, e.fileInfo);
            value.fileTransmitted += (s, e) => showInfo(TransmitState.Over, e.fileInfo);
            value.fileTransmitBefore += (s, e) => showInfo(TransmitState.Before, e.fileInfo);
            value.fileAllowResume += (s, e) =>
            {
                //if (!isOfflineFileTransmit)//如果不是离线文件
                {
                    this.linkLabelReceive.Visible = false;
                    this.linkLabelResume.Visible = true;
                }
            };
            value.fileTransmitBefore += (s, e) =>
            {
                showInfo(TransmitState.Before, e.fileInfo);
            };
            value.GetIPEndPoint += (sender, local, remote) =>
            {
                OnGetIPEndPoint(this, local, remote);
            }; 
        }

        private void setEvent(TcpOfflineFileClient value)
        {
            value.fileTransmitConnected += (s, e) =>showInfo(TransmitState.Connected, e.fileInfo);
            value.fileTransmitting += (s, e) =>  showInfo(TransmitState.Transmitting, e.fileInfo);
            //value.fileTransmitOutTime += (s, e) => showInfo(TransmitState.OutTime, e.fileInfo);
            value.fileTransmitted += (s, e) => showInfo(TransmitState.Over, e.fileInfo);
            value.fileElseTransmiting += (s, e) => showInfo(TransmitState.Else, e.fileInfo);
            value.fileTransmitBefore += (s, e) => showInfo(TransmitState.Before, e.fileInfo);

            value.fileAllowResume += (s, e) =>
            {
                this.linkLabelReceive.Visible = false;
                this.linkLabelResume.Visible = true;
            };
            value.GetIPEndPoint += (sender, local, remote) =>
            {
                OnGetIPEndPoint(this, local, remote);
            };
            value.OfflineFile += (s, e) =>
                {
                    if (OfflineFile != null)
                        OfflineFile(this);
                };
        }
         
        #region 离线文件单击事件
        private void linkLabelOffline_Click(object sender, EventArgs e)
        {
            if (tcpOfflineFileClient == null)
                tcpOfflineFileClient = new TcpOfflineFileClient(OfflineFileServerEP, TFileInfo.fullName);
            tcpOfflineFileClient.from = from;
            tcpOfflineFileClient.to = to;
            tcpOfflineFileClient.Start();//开始传输文件
            linkLabelOffline.Visible = false;
            tcpOfflineFileClient.OnOfflineFile();//触发离线文件传输事件
        }
        #endregion

        #region 离线文件下次接收 单击事件
        void linkLabelNext_Click(object sender, EventArgs e)
        {
            if (offlineFileNextReceive != null)
                offlineFileNextReceive(this);
        }
        #endregion

        #region 文件传输事件
        /// <summary>
        /// 委托事件
        /// </summary>
        /// <param name="sender"></param>
        public delegate void fileTransmitEventHandler(object sender);
        /// <summary>
        /// 取消文件传输
        /// </summary>
        public event fileTransmitEventHandler fileTransmitCancel;
        /// <summary>
        /// 文件传输结束
        /// </summary>
        public event fileTransmitEventHandler fileTransmitted;
        /// <summary>
        /// 离线文件传输事件
        /// </summary>
        public event fileTransmitEventHandler OfflineFile;
        /// <summary>
        /// 离线文件下次接收事件
        /// </summary>
        public event fileTransmitEventHandler offlineFileNextReceive;
        /// <summary>
        /// 文件正被其他人传输...
        /// </summary>
        public event fileTransmitEventHandler fileElseTransmiting;
        /// <summary>
        /// 获得IPEndPoint事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="local">本地主机信息</param>
        /// <param name="remote">远程主机信息</param>
        public delegate void GetIPEndPointEventHandler(object sender, IPEndPoint local, IPEndPoint remote);
        public event GetIPEndPointEventHandler GetIPEndPoint;
        private void OnGetIPEndPoint(object sender, IPEndPoint local, IPEndPoint remote)
        {
            if (GetIPEndPoint != null)
                GetIPEndPoint(this, local, remote);//触发获取本机主机事件
        }

        /// <summary>
        /// 文件传输状态
        /// </summary>
        private enum TransmitState
        {
            Connected,
            Transmitted,
            OutTime,
            Transmitting,
            Over,
            Else,
            Error,
            Before,
        }

        private void showInfo(TransmitState State, TFileInfo fileInfo)
        {
            //try
            {
                delegateSetRecControl d = new delegateSetRecControl(SetRecControl);
                this.Invoke(d, State, fileInfo);
            }
            //catch { }
        }

        delegate void delegateSetRecControl(TransmitState State, TFileInfo fileinfo);
        private void SetRecControl(TransmitState State, TFileInfo fileinfo)
        {
            if (State == TransmitState.Transmitting)
            {
                if (fileinfo.CurrLength > fileinfo.Length) return;
                this.progressBar1.Value = Convert.ToInt32(((decimal)fileinfo.CurrLength / fileinfo.Length) * 100);
                this.labelFileLengthString.Text = IMLibrary4.Operation.Calculate.GetSizeStr(fileinfo.CurrLength) + "/" + fileinfo.LengthStr;
            }
            else if (State == TransmitState.Connected)
            {
                linkLabelOffline.Visible = false;

                if (isSend)
                    this.labelRequest.Text = "正在发送...";
                else
                    this.labelRequest.Text = "正在接收...";

                if (this.P2PFileTransmit != null)
                {
                    if (this.P2PFileTransmit.connectedType == ConnectedType.UDPLocal)
                        labelConType.Text = "[局域网直连]";
                    else if (this.P2PFileTransmit.connectedType == ConnectedType.UDPRemote)
                        labelConType.Text = "[广域网直连]";
                    else if (this.P2PFileTransmit.connectedType == ConnectedType.UDPServer)
                        labelConType.Text = "[UDP中继连接]";
                }
                else
                    labelConType.Text = "[服务器中转]";
            }
            else if (State == TransmitState.Over)
            {
                if (fileTransmitted != null)
                    fileTransmitted(this);//触发文件
            }
            else if (State == TransmitState.Before)
            {
                linkLabelOffline.Visible = false;
            }
            else if (State == TransmitState.Else)
            {
                MessageBox.Show("其他用户正在上传一样的文件，请稍后再试！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (fileElseTransmiting != null)
                    fileElseTransmiting(this);
            }
        } 
        #endregion
         
        #region 接收文件单击事件
        void linkLabelSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = TFileInfo.Name;
            sf.Filter = "*" + TFileInfo.Extension + "|";
            if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!isOfflineFileTransmit)
                {
                    if (sf.FileName.IndexOf(TFileInfo.Extension, 0) < 0)
                        P2PFileTransmit.TFileInfo.fullName = sf.FileName + TFileInfo.Extension;
                    else
                        P2PFileTransmit.TFileInfo.fullName = sf.FileName;

                    P2PFileTransmit.Start(true);//接收文件，并执行断点续传
                }
                else
                {
                    if (sf.FileName.IndexOf(TFileInfo.Extension, 0) < 0)
                        tcpOfflineFileClient.TFileInfo.fullName = sf.FileName + TFileInfo.Extension;
                    else
                        tcpOfflineFileClient.TFileInfo.fullName = sf.FileName;
                    tcpOfflineFileClient.Start();
                }

                linkLabelSaveAs.Visible = false;
                linkLabelReceive.Visible = false;
                linkLabelResume.Visible = false;
                linkLabelNext.Visible = false;

            }

        }

        void linkLabelReceive_Click(object sender, EventArgs e)
        {
            linkLabelSaveAs.Visible = false;
            linkLabelReceive.Visible = false;
            linkLabelNext.Visible = false;
            linkLabelResume.Visible = false;

            if (!isOfflineFileTransmit)
            {
                P2PFileTransmit.TFileInfo.fullName = Application.StartupPath + "\\ReceivedFile\\" +  TFileInfo.Name;
                P2PFileTransmit.Start(false);//接收文件，不执行断点续传 
            }
            else
            {
                tcpOfflineFileClient.TFileInfo.fullName = Application.StartupPath + "\\ReceivedFile\\" + TFileInfo.Name;
                tcpOfflineFileClient.Start();
            }
        }
        #endregion

        #region 取消文件传输
        void linkLabelCancel_Click(object sender, EventArgs e)
        {
            linkLabelCancel.Enabled = false;

                CancelTransmit();
                if (fileTransmitCancel != null)
                    fileTransmitCancel(this);
        }
        #endregion

        #region 续传单击事件
        private void linkLabelResume_Click(object sender, EventArgs e)
        {
            linkLabelSaveAs.Visible = false;
            linkLabelReceive.Visible = false;
            linkLabelResume.Visible = false;

            P2PFileTransmit.TFileInfo.fullName = Application.StartupPath + "\\ReceivedFile\\" + P2PFileTransmit.TFileInfo.Name;
            P2PFileTransmit.Start(true);//接收文件，执行断点续传 
        }
        #endregion

        #region 公共方法
        /// <summary>
        /// 取消文件传输
        /// </summary>
        public void CancelTransmit()
        {
            if (P2PFileTransmit != null)
                P2PFileTransmit.CancelTransmit();//取消文件传输
            if (tcpOfflineFileClient != null)
                tcpOfflineFileClient.CancelTransmit();//取消离线文件传输
        }
        #endregion

      
    }
}
