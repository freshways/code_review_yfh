﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

using IMLibrary4.Net;
using IMLibrary4.Protocol;
using IMLibrary4.Operation;

namespace IMLibrary4
{
    /// <summary>
    /// P2P文件传输
    /// </summary>
    public class p2pFileClient :FileTransmitBase
    { 
        #region 构造函数
        /// <summary>
        /// 发送文件方使用此构造函数
        /// </summary>
        /// <param name="serverEP">代理服务器主机信息</param>
        /// <param name="fullFileName">文件路径</param>
        public p2pFileClient(IPEndPoint serverEP, string fullFileName)
            : base(serverEP, fullFileName)
        {
 
        }

        /// <summary>
        /// 接收文件方使用此构造函数
        /// </summary>
        /// <param name="serverEP">代理服务器主机信息</param>
        /// <param name="tFileInfo">接收文件的信息</param>
        public p2pFileClient(IPEndPoint serverEP, TFileInfo tFileInfo)
            : base(serverEP, tFileInfo)
        {
            CacheFile = System.Windows.Forms.Application.StartupPath + "\\FileCache\\" + TFileInfo.MD5;
        }

      

        #endregion

        #region 变量
        /// <summary>
        /// 发送或收到的包集合
        /// </summary>
        SortedList<int, byte[]> FilePackets = new SortedList<int, byte[]>();
        /// <summary>
        /// 发送的包索引
        /// </summary>
        int SendIndex = 0;
        /// <summary>
        /// 时间戳
        /// </summary>
        int TimeStamp = (int)DateTime.Now.Ticks;
        /// <summary>
        /// 是否完成数据传输
        /// </summary>
        bool isOver = false;
        /// <summary>
        /// 请求发送命令文件包
        /// </summary>
        UdpFilePacket RequestSendFilePakMsg = new UdpFilePacket();
        /// <summary>
        /// 是否已经发送请求
        /// </summary>
        bool isRequestSendFile = false;

        /// <summary>
        /// 是否取消文件传输
        /// </summary>
        bool IsCancelTransmit = false;

        /// <summary>
        /// 发送一个包的间隔
        /// </summary>
        int sendInterval =1000;

        System.Timers.Timer timer;
         
        int sendCount = 0;//发送次数 
        #endregion 

        #region 收到数据包事件
        private void ReceiveUdpData(byte[] data)
        {
            if (data.Length < UdpFilePacket.HeaderLength) return;

            isTransmit = true;//标识正在传输

            UdpFilePacket udpFilePacket = new UdpFilePacket(data);//转换为协议对像
            if (udpFilePacket.type == (byte)TransmitType.SendFile)//收到文件发送请求
            {
                if (TimeStamp == udpFilePacket.Index) return;//如果收到重复的请求包退出
                TimeStamp = udpFilePacket.Index;
                if (isRequestSendFile) return;
                isRequestSendFile = true;//只能接收一次发送请求
                if (IsSend)//如果是文件发送者，
                {
                    currGetPos = BitConverter.ToInt64(udpFilePacket.Payload, 0);//获取发送文件起始位置
                    sendFile(currGetPos, false); //则按指定位置发送文件

                    if (timer == null)
                    {
                        timer = new System.Timers.Timer();
                        timer.Interval = 1000;
                        timer.Elapsed += (s, e) =>
                            {
                                TFileInfo.CurrLength = currGetPos;
                                OnFileTransmitting();//触发文件传输中事件
                            };
                        timer.Enabled = true ;
                    }
                }
            }
            else if (udpFilePacket.type == (byte)TransmitType.getFilePackage)//收到文件数据包 
            {
                if (IsSend)//如果是文件发送者，
                {
                    #region
                    lock (FilePackets)
                    {
                        FilePackets.Remove(udpFilePacket.Index);//删除对方已经收到的包
                       //if(FilePackets.Remove(udpFilePacket.Index))//删除对方已经收到的包 
                        //if (currGetPos + MTU > TFileInfo.Length)
                        //    currGetPos += TFileInfo.Length - currGetPos;
                        //else
                        //    currGetPos += MTU; 
                    }
                    #endregion
                }
                else//如果是文件接收者，处理接收到的文件数据包
                    ReceivedFilePacket(udpFilePacket);
             
            }
            else if (udpFilePacket.type == (byte)TransmitType.CheckPacket)//收到校验包
            {
                if (IsSend)//如果是文件发送者，
                {
                    lock (FilePackets)
                        FilePackets.Remove(udpFilePacket.Index);//删除对方已经收到的包
                }
                else//如果是文件接收者，处理接收到的文件数据包
                {
                    ReceivedCheckPacket(udpFilePacket);//处理校验包，并保存文件
                }
            }
            else if (udpFilePacket.type == (byte)TransmitType.over)//文件传输完成，保存在缓冲区
            {
                if (!isOver)
                {
                    CloseSock();//关闭套接字资源 
                    isOver = true;  
                }
            }
        }
        #endregion

        //int sendNummber = 5;

        #region 发送文件
        /// <summary>
        /// 发送文件
        /// </summary>
        /// <param name="RequestLastPos">当前已经传输完成的文件数据长度</param>
        /// <param name="isSelf">是否对方调用</param>
        private void sendFile(long RequestLastPos, bool isSelf)
        {
            Console.WriteLine("收到文件传输请求...." + RequestLastPos.ToString());

            #region 如果对方要求发送的数据块起始位置大于文件尺寸则认为是非法请求退出
            if (RequestLastPos >= TFileInfo.Length)
            {
                if (timer != null)
                {
                    timer.Enabled = false; timer.Close(); timer.Dispose(); timer = null;
                }
                if (FS != null)//关闭文件流
                { FS.Close(); FS.Dispose(); FS = null; }

                UdpFilePacket overPacket = new UdpFilePacket();
                overPacket.type = (byte)TransmitType.over;//告诉对方文件发送结束
                for (int i = 0; i < 10; i++)
                    SendData(overPacket.Header);

                CloseSock();//关闭套接字资源
                OnFileTransmitted();//触发文件传输结束事件
                return;
            }
            #endregion

            #region 触发文件开始传输事件
            if (!isTransmit)
            {
                isTransmit = true;//标记文件传输中
                OnFileTransmitBefore();//触发文件开始传输前事件
            }
            #endregion

            #region 文件操作
            if (IsReadWriteFile())
            {
                if (RequestLastPos + maxReadWriteFileBlock > TFileInfo.Length)//当缓冲区长度大于文件尺寸时
                    fileBlock = new byte[TFileInfo.Length - RequestLastPos];//要发送的缓冲区
                else
                    fileBlock = new byte[maxReadWriteFileBlock];//设置缓冲区为10M


                CurrRecLength = 0;//重置对方当前收到的数据包为0

                if (FS == null)//打开文件
                    FS = new FileStream(TFileInfo.fullName, FileMode.Open, FileAccess.Read, FileShare.Read);

                ///读文件到缓冲区
                FS.Seek(RequestLastPos, SeekOrigin.Begin);
                FS.Read(fileBlock, 0, fileBlock.Length);

                if (FS != null)//关闭文件流
                { FS.Close(); FS.Dispose(); FS = null; }
                Console.WriteLine("打开文件成功.MTU-" + MTU.ToString());
            }
            #endregion 

            #region 发送文件
            //if (sendInterval > 100)
            int sendNummber = 10;
            sendCount = 0;//发送次数
            while (sendCount < sendNummber && CurrRecLength < fileBlock.Length)//一次发送x个包
            { 
                #region 如果传输被取消
                if (IsCancelTransmit)
                {
                    lock (FilePackets) 
                        FilePackets.Clear();//清空缓冲区并退出发送
                    fileBlock = null;
                    return;
                }
                #endregion 

                if (CurrRecLength + MTU > fileBlock.Length)//当数据包长度大于文件尺寸时
                    buffer = new byte[fileBlock.Length - CurrRecLength];//要发送的缓冲区
                else
                    buffer = new byte[MTU];

                Buffer.BlockCopy(fileBlock, CurrRecLength, buffer, 0, buffer.Length);

                UdpFilePacket udpFilePacket = new UdpFilePacket();
                udpFilePacket.type = (byte)TransmitType.getFilePackage;//告诉对方收到文件包
                udpFilePacket.Index = SendIndex;//包索引
                udpFilePacket.Payload = buffer;

                FilePackets.Add(SendIndex, udpFilePacket.ToBytes());//添加包到发送缓冲区,以便重新发送丢包数据
                //SendData(udpFilePacket.ToBytes()); //发送文件数据给对方
                //System.Threading.Thread.Sleep(0);

                RequestLastPos += buffer.Length; 
                CurrRecLength += buffer.Length;

                currGetPos = RequestLastPos;
                SendIndex++;//发送包索引加1 
                sendCount++;//本次发包数加1 
            }
            #endregion

            #region timer发送检验包

            bool isTimer = false;
            bool isSendCheck = false;//是否发送校验包

            SendIndex++;//发送包索引加1 
            UdpFilePacket CheckPacket = new UdpFilePacket();
            CheckPacket.type = (byte)TransmitType.CheckPacket;
            CheckPacket.Index = SendIndex;//本包索引

            System.Timers.Timer timer1 = new System.Timers.Timer();
            //timer1.Interval = 1000;//1秒检测一次

            if (sendInterval < 1)
                timer1.Interval = 1;
            else
                timer1.Interval = sendInterval;// sendInterval;
            timer1.Elapsed += (s, e) =>//判断对方是否完成所有包的接收
            {
                if (isTimer) return;
                isTimer = true;

                #region 计算发包间隔

                int LostCount = FilePackets.Count;//丢包数  
                //if (LostCount > 1)
                //{
                //    if (sendNummber > 5)
                //        sendNummber -= 5;
                //}
                //else if (LostCount == 0)
                //{
                //    sendNummber += 5;
                //}

                if (LostCount > 1)
                {
                    sendInterval += 20;
                }
                else if (LostCount == 0)
                {
                    if (sendInterval > 300)
                    {
                        sendInterval -= 100;
                    }
                    else if (sendInterval > 20)
                    {
                        sendInterval -= 10;
                    }
                    else if (sendInterval <= 10)
                    {
                        sendInterval -= 2;
                    }
                }

                //Console.WriteLine("丢包：" + LostCount.ToString() + ",发包间隔：" + sendInterval);
                Console.WriteLine("丢包：" + LostCount.ToString() + ",发包间隔：" + sendNummber);
                #endregion

                if (IsCancelTransmit)//如果传输被取消
                {
                    timer1.Enabled = false; timer1.Close(); timer1.Dispose(); timer1 = null;
                    lock (FilePackets) FilePackets.Clear();//清空缓冲区并退出发送
                    return;
                }

                if (FilePackets.Count > 0)//如果还有包，则继续发送
                { 
                    byte[][] Datas = new byte[FilePackets.Count][];
                    lock (FilePackets)
                        FilePackets.Values.CopyTo(Datas, 0);
                    foreach (byte[] data in Datas)
                    {
                        SendData(data);//发送包
                        System.Threading.Thread.Sleep(0);
                    }
                    isTimer = false;
                }
                else
                {
                    if (!isSendCheck)//如果没有发送校验包,则发送
                    {
                        isSendCheck = true;

                        ///发送校验包
                        FilePackets.Add(SendIndex, CheckPacket.Header);//添加校验包到发送缓冲区,以便重新发送丢包数据
                        SendData(CheckPacket.Header);//发送校验包
                        Console.WriteLine("发送校验包！");
                        isTimer = false;//标记此轮执行结束
                    }
                    else//如果对方收到所有包(包括校验包)
                    {
                        timer1.Enabled = false; timer1.Close(); timer1.Dispose(); timer1 = null;
                        isTimer = false;
                        sendFile(RequestLastPos, true);//递归调用，发送下一组文件包
                    }
                }
            };
            timer1.Enabled = true;
            #endregion
        }
        #endregion 

        #region 保存文件数据
        /// <summary>
        /// 保存文件数据
        /// </summary>
        private void SaveFileData()
        {
            if (FilePackets.Count > 0)//如果确实收到文件包
            {
                if (FS == null)//打开文件
                    FS = new FileStream(CacheFile, FileMode.Append, FileAccess.Write, FileShare.Read);

                lock (FilePackets)
                {
                    foreach (byte[] data in FilePackets.Values)
                        FS.Write(data, 0, data.Length);//写文件
                    FilePackets.Clear();//清空文件缓冲区
                    FS.Flush();
                    if (FS != null)//关闭文件流
                    { FS.Close(); FS.Dispose(); FS = null; }
                }
                Console.WriteLine("保存文件长度:" + CurrRecLength.ToString());
            }
            CurrRecLength = 0;
        }
        #endregion

        #region 处理对方发送校验数据包
        /// <summary>
        /// 处理对方发送校验数据包
        /// </summary>
        private void ReceivedCheckPacket(UdpFilePacket udpFilePacket)//当对方发送文件数据块过来
        {
            //如果对方要求保存数据
            if (TimeStamp == udpFilePacket.Index) return;//如果收到重复时间戳，返回
            TimeStamp = udpFilePacket.Index;

            if (CurrRecLength >= (1024000 * 5) || currGetPos == TFileInfo.Length)
                SaveFileData(); //保存文件数据

            TimeStamp = (int)DateTime.Now.Ticks;//重置时间戳

            SendData(udpFilePacket.Header);//将包头数据进行返回给对方，确认收到此数据包 

            #region 如果文件接收完全，触发事件
            if (currGetPos > TFileInfo.Length)
                Console.WriteLine("文件包遭到破坏." + currGetPos.ToString() + ":" + TFileInfo.Length.ToString());

            if (currGetPos == TFileInfo.Length)
            {
                //Console.WriteLine("文件接收完成." + currGetPos.ToString() + ":" + TFileInfo.Length.ToString());
                CloseSock();//关闭套接字资源 
                isOver = true;//标识文件已经传输结束
            }
            #endregion
        }
        #endregion

        #region 处理收到的文件数据包
        /// <summary>
        /// 处理对方发送文件数据包
        /// </summary>
        private void ReceivedFilePacket(UdpFilePacket udpFilePacket)//当对方发送文件数据块过来
        {
            if (!FilePackets.ContainsKey(udpFilePacket.Index))//如果包没有收到
            {
                lock (FilePackets)
                {
                    FilePackets.Add(udpFilePacket.Index, udpFilePacket.Payload);//将数据包缓存起来
                    currGetPos += udpFilePacket.PayloadLength;//当前获得文件尺寸累加
                    CurrRecLength += udpFilePacket.PayloadLength;//当前获得文件尺寸累加
                } 
            } 
            SendData(udpFilePacket.Header);//将包头数据进行返回给对方，确认收到此数据包 
        }
        #endregion

        #region  请求对方发送文件数据包
        /// <summary>
        /// 请求对方发送文件数据包
        /// </summary>
        private void RequestSendFilePak()
        {
            //System.Threading.Thread.Sleep(1500);

            if (isRequestSendFile) return;
            isRequestSendFile = true;//只能发送一次请求
            Console.WriteLine("请求对方发送文件!" + currGetPos.ToString());
             
            RequestSendFilePakMsg.type = (byte)TransmitType.SendFile;//请求发送文件
            RequestSendFilePakMsg.Index = TimeStamp;//获利当前时间戳
            RequestSendFilePakMsg.Payload = BitConverter.GetBytes(currGetPos);//当前要获取数据据的偏移量
            SendData(RequestSendFilePakMsg.ToBytes());//请求对方发送文件数据 

            long  timerCount = 0;
            long  oldCurrGetPos= currGetPos;
            byte[] HeartbeatPacket = new byte[1];//心跳包
            //判断文件是否传输结束并触发事件
            if (timer == null) timer = new System.Timers.Timer(); 
            timer.Interval = 1000;
            timer.Elapsed += (s, e) =>
                {
                    TFileInfo.CurrLength = currGetPos;
                    OnFileTransmitting();//触发文件传输中事件 

                    if (timerCount % 5 == 0 && currGetPos == oldCurrGetPos)//如果每5秒都未收到文件包，则重新请求发送
                    {
                        SendData(RequestSendFilePakMsg.ToBytes());//请求对方发送文件数据
                    }
                    else
                    {
                        SendData(HeartbeatPacket);//心跳包
                    }

                    //if (oldCurrGetPos != currGetPos)
                    //    oldCurrGetPos = currGetPos;
                    //if (timerCount % 10 == 0 && currGetPos == oldCurrGetPos)//如果每10秒都未收到文件包，则重新请求发送
                    //{
                    //    OnfileTransmitDisconnected();//触发网络中断联接事件
                    //}

                    if (isOver)//如果文件传输结束，触发结束事件
                    {
                        timer.Enabled = false; timer.Close(); timer.Dispose(); timer = null;
                        Console.WriteLine("文件接收完成." + currGetPos.ToString() + ":" + TFileInfo.Length.ToString());

                        if (File.Exists(TFileInfo.fullName))
                        {
                            File.Delete(TFileInfo.fullName);
                            System.Threading.Thread.Sleep(500);
                        }
                        File.Move(CacheFile, TFileInfo.fullName);//拷贝文件

                        OnFileTransmitted();//触发结束事件
                    }
                    timerCount++;
                };
            timer.Enabled =true;

        }
        #endregion
  
        #region 公共方法
         
        #region UDP服务
        /// <summary>
        /// 开始
        /// </summary>
        /// <param name="AllowResume">是否断点续传文件</param>
        public void Start(bool AllowResume)
        {
            #region 文件信息准备工作
            System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(System.Windows.Forms.Application.StartupPath + "\\FileCache");
            if (!dInfo.Exists)
                dInfo.Create();
            CacheFile = System.Windows.Forms.Application.StartupPath + "\\FileCache\\" + TFileInfo.MD5;

            if (AllowResume)//如果断点续传
            {
                FileInfo finfo = new FileInfo(CacheFile);
                if (finfo.Exists)//如果缓存文件存在，则触发可断点续传事件
                    currGetPos = finfo.Length;//设置断点续传接收文件位置
            }
            else
            {
                File.Delete(CacheFile);//删除缓存文件
                System.Threading.Thread.Sleep(1000);
            }
            #endregion

            #region 收到数据事件
            ReceiveData += (sender, data) =>
            {
                ReceiveUdpData(data);//执行收到数据事件
            };
            #endregion

            #region 网络成功联接事件
            TransmitConnected += (sender, connectedType) =>
            {
                if (connectedType != ConnectedType.None)
                {
                    OnFileTransmitConnected();

                    maxReadWriteFileBlock = maxReadWriteFileBlock * 10; 
               
                    if (!IsSend)//开始获取文件数据包
                    {
                        RequestSendFilePak();
                    }
                }
            };
            #endregion

            StartNetConnection();//开始连接对端
        }
        #endregion 
         
        #region 取消文件传输
        /// <summary>
        /// 取消文件传输
        /// </summary>
        public void CancelTransmit()
        { 
            IsCancelTransmit = true;//标识文件发送结束
            if (timer != null)
            {
                timer.Enabled = false; timer.Close(); timer.Dispose(); timer = null;
            }
            if (FilePackets.Count > 0 && !IsSend)//如果传输文件存在断点并且是文件接收者,保存断点
            {
                SaveFileData();//保存收到的数据
                CloseSock();//关闭套接字资源
            }

            if (FS != null)//关闭文件流
            { FS.Close(); FS.Dispose(); FS = null; }
        }
        #endregion

        #endregion
    } 
}
