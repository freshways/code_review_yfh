﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Organization
{
    /// <summary>
    /// 数据库中保存的版本信息
    /// </summary>
    public class DBOrgVersion
    {
        /// <summary>
        /// 分组信息旧版本
        /// </summary>
        public string GroupsVersion { set; get; }
        /// <summary>
        /// 用户信息旧版本
        /// </summary>
        public string UsersVersion { set; get; }
    }
}
