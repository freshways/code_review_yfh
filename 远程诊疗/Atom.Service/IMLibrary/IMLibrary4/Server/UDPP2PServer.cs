﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using IMLibrary4.Net;
using IMLibrary4.Protocol;

namespace IMLibrary4.Server
{
    #region TurnUDPServer
    /// <summary>
    /// TurnUDPServer
    /// </summary>
    class TurnUDPServer
    {
        #region 属性
        /// <summary>
        /// 服务ID
        /// </summary>
        public int ID
        {
            private set;
            get;
        }
        /// <summary>
        /// 数据接收者Client IPEndPoint
        /// </summary>
        public IPEndPoint ClientIPEndPoint = null;
        /// <summary>
        /// 最后活动时间
        /// </summary>
        public DateTime LastActiveTime = DateTime.Now;
        /// <summary>
        /// UDP套接字服务
        /// </summary>
        public SockUDP sockUdp = null ;
        /// <summary>
        /// 接收数据的对端
        /// </summary>
        public TurnUDPServer RemoteClient = null;
        /// <summary>
        /// Rtp协议类型
        /// </summary>
        public IPProtocolType protocolType = IPProtocolType.Rtp;
        #endregion

        #region 事件
        /// <summary>
        /// 数据到达事件
        /// </summary>
        /// <param name="sender">套接字对像</param>
        /// <param name="e">事件相关参数</param>
        public delegate void UDPEventHandler(object sender, SockEventArgs e);
        /// <summary>
        /// 数据到达事件
        /// </summary>
        public event UDPEventHandler DataArrival;

        /// <summary>
        /// 套接字错误事件
        /// </summary>
        public event UDPEventHandler Sock_Error;
        #endregion

        #region 公共方法
        /// <summary>
        /// 启动服务
        /// </summary>
        public void Start()
        {
            sockUdp = new SockUDP();
            sockUdp.IsReuseAddress = false;//禁止UDP端口复用
            sockUdp.IsAsync = true;//标明异步通信
            sockUdp.Blocking = false;//非阻止模式
            sockUdp.Listen(0);
            ID = sockUdp.ListenPort;
            sockUdp.DataArrival += (sender, e) =>
            {
                if (ClientIPEndPoint == null)
                {
                    ClientIPEndPoint = e.RemoteIPEndPoint;
                    sockUdp.socket.Connect(ClientIPEndPoint);
                    byte[] data = new byte[1];
                    for (int i = 0; i < 5; i++)
                        sockUdp.socket.Send(data);//向客户端握手
                }
                if (RemoteClient != null)
                {
                    RemoteClient.sendData(e.Data);
                    LastActiveTime = DateTime.Now;
                } 
            };
        }

        /// <summary>
        /// 关闭服务并释放资源
        /// </summary>
        public void Close()
        {
            if (sockUdp != null && sockUdp.Listened)
                sockUdp.CloseSock();
        }

        /// <summary>
        /// 向客户端发送数据
        /// </summary>
        /// <param name="data"></param>
        public void sendData(byte[] data)
        {
            if (ClientIPEndPoint != null)
            {
                sockUdp.socket.Send(data);
                LastActiveTime = DateTime.Now;
            }
        }
        #endregion
    }
    #endregion

    #region UDP P2P Server服务器（类TURN功能服务）
    /// <summary>
    /// UDP P2P Server服务器（类TURN功能服务）
    /// </summary>
    public class UDPP2PServer
    { 
        #region 变量
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Port"></param>
        public UDPP2PServer(int Port)
        {
            port = Port;
        } 
        private SockUDP udpServer = null;

        private int port = 0;

        private Dictionary<int, TurnUDPServer> TurnClients = null;

        private System.Timers.Timer timer = null;
        #endregion

        #region 释放资源
        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            Stop();
        }
        #endregion 

        #region 公共方法
        /// <summary>
        /// 开始服务
        /// </summary>
        public void Start()
        {
            if (udpServer == null)
            {
                udpServer = new SockUDP();
                udpServer.IsAsync = true;//标明异步通信
                udpServer.Blocking = false;//非阻止模式
                udpServer.DataArrival += new SockUDP.UDPEventHandler(udpServer_DataArrival);
                udpServer.Listen(port);
                //udpServer.socket.Blocking = false;//处于非阻塞模式
            }

            if (TurnClients == null)
                TurnClients = new Dictionary<int, TurnUDPServer>();

            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.Interval =30000 ;// 执行一次
                timer.Elapsed += (sender, e) =>
                    {
                        if (TurnClients.Count > 0)
                        {
                            //return;
                            TurnUDPServer[] Cs = new TurnUDPServer[TurnClients.Count];
                            lock (TurnClients)
                            {
                                TurnClients.Values.CopyTo(Cs, 0);
                            }
                            foreach (TurnUDPServer c in Cs)
                            {
                                TimeSpan t = DateTime.Now - c.LastActiveTime;
                                if (Math.Abs(t.Seconds) > 20 || Math.Abs(t.Minutes)>0)
                                {
                                    if (c.RemoteClient != null)
                                        c.RemoteClient.ClientIPEndPoint = null;
                                    c.Close();//释放资源
                                    lock (TurnClients)
                                    {
                                        TurnClients.Remove(c.ID);//删除
                                    }
                                }
                            }
                        }
                    };
                timer.Enabled = true;
            }

        }
         
        /// <summary>
        /// 停止服务
        /// </summary>
        public void Stop()
        {
            if (udpServer != null && udpServer.Listened)
            {
                udpServer.DataArrival -= new SockUDP.UDPEventHandler(udpServer_DataArrival);
                udpServer.CloseSock();
                udpServer = null;
            }

            if (TurnClients != null)
            {
                foreach (TurnUDPServer client in TurnClients.Values)
                {
                    client.Close();
                }
                TurnClients.Clear();
                TurnClients = null;
            }

            if (timer != null)
            {
                timer.Enabled = false;
                timer.Close();
                timer.Dispose();
                timer = null;
            }

        }
        #endregion 

        #region 数据包达到事件
        void udpServer_DataArrival(object sender, SockEventArgs e)
        {
            if (e.Data.Length < UdpTurnPacket.HeaderLength) return;//如果收到非法数据包则退出

            UdpTurnPacket packet = new UdpTurnPacket(e.Data);//转换成协议包

            if (packet.type == (byte)TransmitType.getRemoteEP)//客户端请求获取自己的远程主机信息 
            {
                packet.RemoteIP = e.RemoteIPEndPoint.Address;//设置客户端的远程IP
                packet.Port = e.RemoteIPEndPoint.Port;//设置客户端的远程UDP 端口
                for (int i = 0; i < 3; i++)
                    udpServer.Send(e.RemoteIPEndPoint, packet.Header);//将远程主机信息发送给客户端
            }
            else if (packet.type == (byte)TransmitType.GetTurnServerID)//请求获取Turn服务ID
            {
                TurnUDPServer server = new TurnUDPServer();
                server.Start();
                if (server.ID > 0)
                {
                    packet.LocalTurnID = server.ID;
                    server.protocolType = packet.ProtocolType;
                    lock (TurnClients)
                        TurnClients.Add(server.ID, server);
                    for (int i = 0; i < 5; i++)
                        udpServer.Send(e.RemoteIPEndPoint, packet.Header);//将服务端口ID发送给客户端
                    for (int i = 0; i < 5; i++)
                        server.sockUdp.Send(e.RemoteIPEndPoint, new byte[1]);//向客户端握手
                }
                else
                    server.Close();
            }
            else if (packet.type == (byte)TransmitType.RuterTurnServerID)//客户端要求路由serverID到另一客户端
            {
                udpServer.Send(new IPEndPoint(packet.RemoteIP, packet.Port), packet.Header);//将远程主机信息发送给客户端
            }
            else if (packet.type == (byte)TransmitType.SetToTurnServerID)//请求设置对方Turn服务ID
            {
                TurnUDPServer LocalTurn = getTurnClient(packet.LocalTurnID);
                TurnUDPServer RemoteTurn = getTurnClient(packet.RemoteTurnID);
                if (LocalTurn != null && RemoteTurn != null)
                {
                    if (LocalTurn.RemoteClient == null)
                        LocalTurn.RemoteClient = RemoteTurn;
                    if (RemoteTurn.RemoteClient == null)
                        RemoteTurn.RemoteClient = LocalTurn;
                    for (int i = 0; i < 3; i++)
                        udpServer.Send(e.RemoteIPEndPoint, packet.Header);
                }
            }
        }
        #endregion

        #region 从内存字典中获取用户
        /// <summary>
        /// 从内存字典中获取用户
        /// </summary>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        private TurnUDPServer getTurnClient(int ClientID)
        {
            TurnUDPServer Client = null;
            if (TurnClients.ContainsKey(ClientID)) //如果用户存在
                TurnClients.TryGetValue(ClientID, out Client);
            return Client;
        }
        #endregion
    }
    #endregion
}
