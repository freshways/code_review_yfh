﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

using IMLibrary4;
using IMLibrary4.IO;
using IMLibrary4.Net;
using IMLibrary4.Net.TCP;
using IMLibrary4.Protocol;

namespace IMLibrary4.Server
{
    /// <summary>
    /// 离线文件服务器
    /// </summary>
    public class OfflineFileServer
    {
         /// <summary>
        /// 离线文件服务器
        /// </summary>
        /// <param name="Port">服务端口</param>
        public OfflineFileServer(int Port)
        {
            port = Port;
        }

        #region 属性
        /// <summary>
        /// TCP服务(文件服务)
        /// </summary>
        private TCPServer tcpFileServer = null;

        /// <summary>
        /// 文件上传服务缓冲
        /// </summary>
        private Dictionary<string, ServerFile> UploadServerFiles = null;


        /// <summary>
        /// 文件下载服务缓冲
        /// </summary>
        private Dictionary<string, ServerFile> DownloadServerFiles = null;

        /// <summary>
        /// 文件中转服务时钟
        /// </summary>
        private System.Timers.Timer TimerFileServer = null;

        /// <summary>
        /// 服务端口
        /// </summary>
        private int port = 0;

        /// <summary>
        /// 保存离线文件夹的名称
        /// </summary>
        public string OfflineFolder = "OfflineFiles";
        #endregion

        #region 事件

        /// <summary>
        /// 客户端离线文件上传完成事件
        /// </summary>
        /// <param name="offlineFileMsg"></param>
        public delegate void offlineFileEventHandler(OfflineFileMsg offlineFileMsg);
        /// <summary>
        /// 客户端离线文件上传完成事件
        /// </summary>
        public event offlineFileEventHandler FileUploaded;
       
        /// <summary>
        /// 触发客户端离线文件上传完成事件
        /// </summary>
        /// <param name="offlineFileMsg"></param>
        private void onFileUploaded(OfflineFileMsg offlineFileMsg)
        {
            if (FileUploaded != null)
                FileUploaded(offlineFileMsg);
        }


        /// <summary>
        /// 
        /// </summary>
        public event offlineFileEventHandler FileDownloaded;
       /// <summary>
       /// 触发客户端离线文件上传完成事件
       /// </summary>
       /// <param name="offlineFileMsg"></param>
        private void onFileDownloaded(OfflineFileMsg offlineFileMsg)
        {
            if (FileDownloaded != null)
                FileDownloaded(offlineFileMsg);
        }

        #endregion

        #region 公共方法

        #region 开始服务
        /// <summary>
        /// 开始服务
        /// </summary>
        public void Start()
        {
            #region 创建文件夹
            System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(System.Windows.Forms.Application.StartupPath + "\\"+ OfflineFolder );
            if (!dInfo.Exists)
                dInfo.Create();
            #endregion

            if (UploadServerFiles == null)
                UploadServerFiles = new Dictionary<string, ServerFile>();

            if (DownloadServerFiles == null)
                DownloadServerFiles = new Dictionary<string, ServerFile>();

            if (TimerFileServer == null)
            {
                TimerFileServer = new System.Timers.Timer();
                TimerFileServer.Elapsed += new System.Timers.ElapsedEventHandler(TimerFileServer_Elapsed);
            }

            TimerFileServer.Interval = 2000;//设置文件缓存时间 （默认1分钟）
            TimerFileServer.Enabled = true;//开始服务

            if (tcpFileServer == null)
            {
                tcpFileServer = new TCPServer();
                tcpFileServer.SessionCreated += new EventHandler<TCP_ServerSessionEventArgs<TCPServerSession>>(tcpFileServer_SessionCreated);
                tcpFileServer.Bindings = new IPBindInfo[] { new IPBindInfo("127.0.0.1", BindInfoProtocol.TCP, IPAddress.Any, port) };
            }
            tcpFileServer.Start();
        }
        #endregion

        #region 释放资源
        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            if (tcpFileServer != null && tcpFileServer.IsRunning)
            {
                tcpFileServer.Stop();
                tcpFileServer.Dispose();
                tcpFileServer = null;
            }
            if (TimerFileServer != null)
            {
                TimerFileServer.Enabled = false;//停止文件服务检测
                TimerFileServer.Dispose();
                TimerFileServer = null;
            }
            if (UploadServerFiles != null)
            {
                UploadServerFiles.Clear();
                UploadServerFiles = null;
            }
        }
        #endregion

        #region 停止服务
        /// <summary>
        /// 停止服务
        /// </summary>
        public void Stop()
        {
            if (tcpFileServer != null && tcpFileServer.IsRunning)
            {
                tcpFileServer.SessionCreated -= new EventHandler<TCP_ServerSessionEventArgs<TCPServerSession>>(tcpFileServer_SessionCreated);
                tcpFileServer.Stop();
            }
            if (TimerFileServer != null)
            {
                TimerFileServer.Elapsed -= new System.Timers.ElapsedEventHandler(TimerFileServer_Elapsed);
                TimerFileServer.Enabled = false; TimerFileServer.Close(); TimerFileServer.Dispose(); TimerFileServer = null; }//停止文件服务检测
        }
        #endregion

        #endregion

        #region 从内存字典中获取上传服务缓冲区文件
        /// <summary>
        /// 从内存字典中获取上传服务缓冲区文件
        /// </summary>
        /// <param name="MD5">MD5文件名</param>
        /// <returns></returns>
        private ServerFile getUploadServerFile(string MD5)
        {
            ServerFile file = null;
            if (UploadServerFiles.ContainsKey(MD5)) //如果文件存在
                UploadServerFiles.TryGetValue(MD5, out file);
            return file;
        }
        #endregion

        #region 从内存字典中获取下载服务缓冲区文件
        /// <summary>
        /// 从内存字典中获取下载服务缓冲区文件
        /// </summary>
        /// <param name="MD5">MD5文件名</param>
        /// <returns></returns>
        private ServerFile getDownloadServerFile(string MD5)
        {
            ServerFile file = null;
            if (DownloadServerFiles.ContainsKey(MD5)) //如果文件存在
                DownloadServerFiles.TryGetValue(MD5, out file);
            return file;
        }
        #endregion

        #region 缓冲服务周期
        private void TimerFileServer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            TimerFileServer.Enabled = false;

            string fileFullName = "";
            ServerFile[] upFiles = new ServerFile[UploadServerFiles.Count];
            UploadServerFiles.Values.CopyTo(upFiles, 0);
            foreach (ServerFile serverFile in upFiles)
                if (DateTime.Now > serverFile.LastActivity.AddSeconds(10))
                {
                    //如果文件10秒钟未上传，则清除内存中的文件内容，释放内存并删除文件
                    fileFullName = System.Windows.Forms.Application.StartupPath + "\\" + OfflineFolder + "\\" + serverFile.MD5 + serverFile.Extension;
                    System.IO.File.Delete(fileFullName);//删除不用的文件

                    serverFile.Data = null;
                    lock (UploadServerFiles)
                    {
                        UploadServerFiles.Remove(serverFile.MD5 + serverFile.Extension);//删除并释放内存
                    }
                }

            ServerFile[] downFiles = new ServerFile[DownloadServerFiles.Count];
            DownloadServerFiles.Values.CopyTo(downFiles, 0);
            foreach (ServerFile serverFile in downFiles)
                if (DateTime.Now > serverFile.LastActivity.AddSeconds(10))
                {
                    serverFile.Data = null;
                    if (serverFile.FS != null)
                    {
                        serverFile.FS.Close(); serverFile.FS.Dispose(); serverFile.FS = null;
                    }//释放读文件流资源
                    lock (DownloadServerFiles)
                    {
                        DownloadServerFiles.Remove(serverFile.userID + serverFile.MD5 + serverFile.Extension);//删除并释放内存
                    }
                }
            TimerFileServer.Enabled = true;
        }
        #endregion

        #region 发送消息到文件客户端
        /// <summary>
        /// 发送消息到文件客户端
        /// </summary>
        /// <param name="e"></param>
        private void sendFileMsg(Element e, TCPServerSession se)
        {
            if (se != null && se.IsConnected)
                tcpFileServer.SendMessageToSession(se, Factory.CreateXMLMsg(e));
        }
        #endregion

        #region SessionCreated
        private void tcpFileServer_SessionCreated(object sender, TCP_ServerSessionEventArgs<TCPServerSession> e)
        {
            e.Session.PacketReceived += (s, e1) => fileSession_PacketReceived(s, e1);
            e.Session.IsAuthenticated = true;
        }
        #endregion

        #region PacketReceived
        private void fileSession_PacketReceived(object sender, TcpSessionEventArgs e)
        {
            if (e.Data.Length < 2) return;
            TCPServerSession Session = sender as TCPServerSession;
            if (DateTime.Now > Session.ConnectTime.AddMinutes(30))
            {//如果客户端连接服务器的时间大于30分钟，则视为非法攻击，断开联接
                Session.Disconnect();
                Session.Dispose();
            }

            object obj = Factory.CreateInstanceObject(e.Data);
            if (obj != null)//如果收到的消息对像不为空
            {
                if (obj is OfflineFileMsg)//文件传输
                    onFileTransmit(obj as OfflineFileMsg, Session);
            }
            else //收到非法消息
                OnBadCommand(Session);
        }
        #endregion

        #region TCP处理非法入侵
        /// <summary>
        /// 处理非法入侵
        /// </summary>
        /// <param name="session"></param>
        private void OnBadCommand(TCPServerSession session)
        {
            session.BadCmd++;
            if (session.BadCmd > 3)//4不过3，如果收到坏消息大于3条，则视为非法攻击，断掉TCP连接
            {
                session.Disconnect();
                session.Dispose();
            }
        }
        #endregion

        #region 文件传送
        /// <summary>
        /// 文件传送
        /// </summary>
        /// <param name="fileMsg"></param>
        /// <param name="session"></param>
        private void onFileTransmit(OfflineFileMsg fileMsg, TCPServerSession session)
        {
            if (fileMsg.Length > 1024000 * 1000 || fileMsg.Length < 0) return;//如果所传输的文件大于1000*1M或无大小则为非法请求退出
            string fullFileName = System.Windows.Forms.Application.StartupPath + "\\" + OfflineFolder + "\\" + fileMsg.MD5 + fileMsg.Extension;//标记文件路径

            #region 下载文件
            if (fileMsg.type == type.get)//下载文件请求
            {
                if (File.Exists(fullFileName))//如果服务器磁盘中有此文件
                {
                    FileInfo finfo = new FileInfo(fullFileName);//获得文件信息
                    if (finfo.Length - fileMsg.LastLength <= 0) return;

                    ServerFile downFile = getDownloadServerFile(fileMsg.from + fileMsg.MD5 + fileMsg.Extension);
                    if (downFile == null)//如果缓冲区无此文件，则新建针对此用户的下载缓冲区
                    {
                        downFile = new ServerFile(fileMsg.MD5);//创建内存文件
                        downFile.Extension = fileMsg.Extension;
                        downFile.Length = fileMsg.Length;
                        downFile.currLength = fileMsg.LastLength;
                        downFile.Name = fileMsg.Name;
                        downFile.userID = fileMsg.from;
                        downFile.FS = new FileStream(fullFileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                        DownloadServerFiles.Add(fileMsg.from + fileMsg.MD5 + fileMsg.Extension, downFile);//将内存文件添加到文件下载服务区
                    }

                    int mtu = 10240;//一次最大传输10K数据

                    if (finfo.Length - fileMsg.LastLength > mtu)
                        downFile.Data = new byte[mtu];
                    else
                        downFile.Data = new byte[finfo.Length - fileMsg.LastLength];

                    downFile.FS.Seek(fileMsg.LastLength, SeekOrigin.Begin);
                    downFile.FS.Read(downFile.Data, 0, downFile.Data.Length);

                    downFile.LastActivity = DateTime.Now;//设置激活时间

                    fileMsg.fileBlock = downFile.Data;
                    sendFileMsg(fileMsg, session);//通知客户端下载到文件

                    if (downFile.Data.Length + fileMsg.LastLength == finfo.Length)
                    {
                        if (downFile.FS != null) {
                            downFile.FS.Close(); downFile.FS.Dispose(); downFile.FS = null; }
                        lock (DownloadServerFiles)
                        {
                            DownloadServerFiles.Remove(downFile.userID + downFile.MD5 + downFile.Extension);//删除并释放内存
                        }

                        fileMsg.fileBlock = null;
                        fileMsg.type = type.over;//标识文件已经传输完成，通知客户端停止上传
                        sendFileMsg(fileMsg, session);//通知客户端文件已经上传完成
                        session.Disconnect();//断开客户端的TCP连接

                        onFileDownloaded(fileMsg);//触发离线文件下载完成事件
                    }
                }
                else
                {

                }

                #region 旧算法
                //if (File.Exists(fullFileName))//如果服务器磁盘中有此文件
                //{
                //    FileInfo finfo = new FileInfo(fullFileName);

                //    byte[] fileBlock;

                //    if ((finfo.Length - fileMsg.LastLength) > 1024000)
                //        fileBlock = new byte[1024000];//一次读1M数据到内存
                //    else
                //        fileBlock = new byte[finfo.Length - fileMsg.LastLength];//一次读取尾部数据到内存

                //    FileStream FS = new FileStream(fullFileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                //    FS.Seek(fileMsg.LastLength, SeekOrigin.Begin);
                //    FS.Read(fileBlock, 0, fileBlock.Length);
                //    FS.Close(); FS.Dispose(); FS = null;

                //    int sendCount = fileBlock.Length/ 10240;//发送次数
                //    if (fileBlock.Length % 10240 != 0) sendCount++;

                //    for (int i = 0; i < sendCount; i++)
                //    {
                //        int offSet = i * 10240;// 获得要发送的绝对位置

                //        byte[] buffer = new byte[10240];

                //        if (offSet + 10240 > fileBlock.Length)
                //            buffer = new byte[fileBlock.Length - offSet];//要发送的缓冲区

                //        Buffer.BlockCopy(fileBlock, (int)offSet, buffer, 0, buffer.Length);//将其保存于Buffer字节数组
                //        fileMsg.fileBlock = buffer;// 
                //        sendFileMsg(fileMsg, session);//通知客户端下载到文件
                //    }

                  
                //}
                #endregion

                return;
            }
            #endregion

            ServerFile upFile = getUploadServerFile(fileMsg.MD5 + fileMsg.Extension);
          
            #region 如果是上传离线文件，先在内存中为文件创建空间
            if (fileMsg.type == type.New)//上传文件请求
            {
                if (upFile == null)//如果服务器内存中无此文件,表明文件还没开始上传或早期传过
                {
                    if (File.Exists(fullFileName))//如果服务器磁盘中已经有此文件
                    {
                        FileInfo finfo = new FileInfo(fullFileName);
                        if (finfo.Length == fileMsg.Length)//如果文件已经传输完成
                        {
                            fileMsg.type = type.over;//标识文件已经传输完成，通知客户端停止上传
                            sendFileMsg(fileMsg, session);//通知客户端文件已经上传完成
                            session.Disconnect();//断开客户端的TCP连接

                            onFileUploaded(fileMsg);//触发离线文件上传完成事件
                        }
                        else //如果同一个文件被别人正在传输...
                        {
                            fileMsg.type = type.Else;//标识文件被别人正在传输...，通知客户端稍后再上传
                            sendFileMsg(fileMsg, session);// 通知客户端稍后再上传
                            session.Disconnect();//断开客户端的TCP连接
                        }
                    }
                    else //如果服务器磁盘中无此文件,则允许客户端重新上传
                    {
                        upFile = new ServerFile(fileMsg.MD5);//创建内存文件
                        upFile.Extension = fileMsg.Extension;
                        upFile.Length = fileMsg.Length;
                        upFile.Name = fileMsg.Name;
                        upFile.userID = fileMsg.from;
                        upFile.LastActivity = DateTime.Now;

                        UploadServerFiles.Add(fileMsg.MD5 + fileMsg.Extension, upFile);//将内存文件添加到文件下载服务区

                        fileMsg.type = type.set;//通知客户端上传
                        fileMsg.LastLength = 0;//上传位置从零开始
                        sendFileMsg(fileMsg, session);//通知客户端可以上传文件
                    }
                }
                else//如果服务器内存中有此文件,表明服务器磁盘中有此文件且正在被下载或被上传....
                {
                    fileMsg.type = type.Else;//标识文件被别人正在传输。。。。，通知客户端稍后再上传
                    sendFileMsg(fileMsg, session);// 通知客户端稍后再上传
                    session.Disconnect();//断开客户端的TCP连接
                }
            }
            #endregion
              
            #region 上传文件
            if (fileMsg.type == type.set || upFile != null)//上传文件内容
            {
                if (fileMsg.fileBlock != null && fileMsg.fileBlock.Length > 0)
                {
                    ///合并已上传的内容
                    List<byte> result = new List<byte>();
                    result.AddRange(upFile.Data);
                    result.AddRange(fileMsg.fileBlock);
                    upFile.Data = result.ToArray();
                    ///
                }

                upFile.currLength += fileMsg.fileBlock.Length;//设置最后一次上传文件的末尾长度
                upFile.LastActivity = DateTime.Now;//设置激活时间

                if (upFile.Data.Length > 1024000 * 2 || upFile.currLength == upFile.Length)//如果缓冲区大于5M或文件已经传输完成
                {
                    OpFile.Write(upFile.Data, fullFileName);//写文件
                    upFile.Data = new byte[0];
                }

                fileMsg.LastLength = upFile.currLength;//告诉客户端已上传的文件位置
                fileMsg.fileBlock = null;

                if (upFile.currLength == upFile.Length)//如果文件上传完成
                {
                    lock (UploadServerFiles)
                    {
                        UploadServerFiles.Remove(upFile.MD5 + upFile.Extension);//删除并释放内存
                    }
                    fileMsg.type = type.over;//标识文件已经传输完成，通知客户端停止上传
                    sendFileMsg(fileMsg, session);//通知客户端文件已经上传完成
                    session.Disconnect();//断开客户端的TCP连接

                    onFileUploaded(fileMsg);//触发离线文件上传完成事件
                }
                else
                    sendFileMsg(fileMsg, session);//通知客户端上传文件下一数据包
            }
            #endregion
        }

       
        #endregion
    }
}
