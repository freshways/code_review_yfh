﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace IMLibrary4.Organization
{
    /// <summary>
    /// 服务器用户权限
    /// </summary>
    public class UserPower 
    {
        #region 权限属性
        /// <summary>
        /// 最大创建群数
        /// </summary>
        [DefaultValue(1)]
        public int CreateMaxRooms { set; get; }
        /// <summary>
        /// 已创建群数
        /// </summary>
        public int CreatedRoomsCount = 0;
        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool Disable { set; get; }
        /// <summary>
        /// 是否可以发送短信
        /// </summary>
        public bool isSendSMS { set; get; }
        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool isAdmin { set; get; }
        /// <summary>
        /// 是否可以发送通知
        /// </summary>
        public bool isSendNotice { set; get; }
        /// <summary>
        /// 是否可以广播
        /// </summary>
        public bool isBroadcast { set; get; }

        /// <summary>
        /// 是否控制终端
        /// </summary>
        public bool isControlClient { set; get; }
        #endregion 
    }
}
