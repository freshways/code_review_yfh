﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Organization
{
    /// <summary>
    /// 服务器端用户信息
    /// </summary>
    public class ServerUser : User
    {
        /// <summary>
        /// 用户权限
        /// </summary>
        public UserPower Power = new UserPower();
        /// <summary>
        /// 用户最后一次登录信息
        /// </summary>
        public UserLoginInfo LoginInfo = new UserLoginInfo();
        /// <summary>
        /// 用户加入的群
        /// </summary>
        public Dictionary<string, Room> Rooms = new Dictionary<string, Room>();
        /// <summary>
        /// 群版本号
        /// </summary>
        public string RoomsVersion = "";
        /// <summary>
        /// 用户密码
        /// </summary>
        public string Password = "123456";
        /// <summary>
        /// 用户扩展ID
        /// </summary>
        public string ID = "";


    }
}
