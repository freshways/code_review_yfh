﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace IMLibrary4.Organization
{
    /// <summary>
    /// UserVcard用户资料(开发人员可以无限扩展 用户资料 属性)
    /// </summary>
    public class UserVcard 
    { 
         
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Mail { set; get; }
        /// <summary>
        /// 岗位
        /// </summary>
        public string Job { set; get; }
        /// <summary>
        /// 办公电话
        /// </summary>
        public string OfficePhone { set; get; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Phone { set; get; }
        /// <summary>
        /// 职务
        /// </summary>
        public string Post { set; get; }
        /// <summary>
        /// 生日
        /// </summary>
        public string Birthday { set; get; }
        /// <summary>
        /// 性别
        /// </summary>
        public byte Sex { set; get; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { set; get; }
        /// <summary>
        ///  用户头像索引位置
        /// </summary>
        public int FaceIndex{ set; get; }
        
        ////以下
        ////(开发人员可以无限扩展 用户资料 属性)
        //.....
        //.....
        ////

    }
}
