﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Organization
{
    /// <summary>
    /// 用户最后一次登录信息
    /// </summary>
    public class UserLoginInfo
    {
        /// <summary>
        /// 上次登录时间
        /// </summary>
        public string LastDateTime { set;get;} 
        /// <summary>
        /// 上次登录IP
        /// </summary>
        public string LastIP { set; get; }
        /// <summary>
        /// 在线时长
        /// </summary>
        public int onlineDateLength { set; get; }
        /// <summary>
        /// 最后一次登录计算机名
        /// </summary>
        public string ComputeName { set; get; }
    }
}
