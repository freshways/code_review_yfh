﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMLibrary4.Organization
{
    public class Mail
    {
        /// <summary>
        /// 邮件类
        /// </summary>
        public Mail()
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
        } 

        /// <summary>
        /// 邮件类
        /// </summary>
        /// <param name="Guid">唯一ID</param>
        public Mail(string Guid)
        {
            this.Guid = Guid;
        }

        private string _Guid = "";// 标识唯一的ID
        /// <summary>
        /// Guid。 
        /// </summary>
        public string Guid
        {
            get { return _Guid; }
            set
            {
                _Guid = value;
            }
        }

        /// <summary>
        /// 邮件Tag
        /// </summary>
        public object Tag = null;
    }
}
