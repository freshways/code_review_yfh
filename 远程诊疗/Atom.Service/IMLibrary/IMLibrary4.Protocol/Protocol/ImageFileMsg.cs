﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Protocol
{
    /// <summary>
    /// tcp文件传输消息
    /// </summary>
    public class ImageFileMsg : Element 
    {
        /// <summary>
        /// 构造
        /// </summary>
        public ImageFileMsg()
        {
            this.IsSaveAsOfflineMsg = true;//凡此类消息均保存为离线消息
        }
         
        /// <summary>
        /// 文件发送接收类型（是发送给群还是发送给用户）
        /// </summary>
        public  MessageType MessageType { get; set; }

        /// <summary>
        /// 传输的文件名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 传输文件的大小
        /// </summary>
        public long  Length { get; set; }
  
        /// <summary>
        /// 上次发送的文件位置
        /// </summary>
        public long LastLength { get; set; }

        /// <summary>
        /// 文件MD5值
        /// </summary>
        public string MD5{ get; set; }

        /// <summary>
        /// 文件扩展名
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// 文件包数据
        /// </summary>
        public byte[] fileBlock { get; set; }
    }
}
