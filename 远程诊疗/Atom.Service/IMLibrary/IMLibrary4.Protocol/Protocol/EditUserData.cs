﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Protocol
{
    /// <summary>
    /// 用户资料
    /// </summary>
    public class EditUserData:Organization.User 
    {
        /// <summary>
        /// 协议类型
        /// </summary>
        public IMLibrary4.Protocol.type type { get; set; }

        #region 用户资料(开发人员可以无限扩展 用户资料 属性)
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Mail { set; get; }
        /// <summary>
        /// 岗位
        /// </summary>
        public string Job { set; get; }
        /// <summary>
        /// 办公电话
        /// </summary>
        public string OfficePhone { set; get; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Phone { set; get; }
        /// <summary>
        /// 职务
        /// </summary>
        public string Post { set; get; }
        /// <summary>
        /// 生日
        /// </summary>
        public string Birthday { set; get; }
        /// <summary>
        /// 性别
        /// </summary>
        public byte Sex { set; get; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { set; get; }
        /// <summary>
        ///  用户头像索引位置
        /// </summary>
        public int FaceIndex { set; get; }
        #endregion

        #region 权限属性
        /// <summary>
        /// 最大创建群数
        /// </summary>
        public int CreateMaxRooms { set; get; }
        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool Disable { set; get; }
        /// <summary>
        /// 是否可以发送短信
        /// </summary>
        public bool isSendSMS { set; get; }
        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool isAdmin { set; get; }
        /// <summary>
        /// 是否可以发送通知
        /// </summary>
        public bool isSendNotice { set; get; }
        /// <summary>
        /// 是否可以广播
        /// </summary>
        public bool isBroadcast { set; get; }
        /// <summary>
        /// 是否控制终端
        /// </summary>
        public bool isControlClient { set; get; }
        #endregion

        /// <summary>
        /// 分组版本信息
        /// </summary>
        public string Version { get; set; }
    }
}
