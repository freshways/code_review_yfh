﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMLibrary4.Protocol
{
    public class EditMail : Organization.Mail
    {
        #region 邮件详细信息(开发人员可以无限扩展  邮箱信息属性)

        /// <summary>
        /// Guid
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// 邮件标题
        /// </summary>
        public string Email_Titel { get; set; }

        /// <summary>
        /// 邮件时间
        /// </summary>
        public string Email_Time { get; set; }

        /// <summary>
        /// 邮件内容
        /// </summary>
        public string Email_Content { get; set; }

        /// <summary>
        /// 邮件附件
        /// </summary>
        public string Email_Enclosure { get; set; }

        /// <summary>
        /// 发送人-可多人
        /// </summary>
        public string FromUser { get; set; }

        /// <summary>
        /// 发送人姓名
        /// </summary>
        public string FromUserName { get; set; }

        /// <summary>
        /// 收件人
        /// </summary>
        public string ToUser { get; set; }

        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string ToUserName { get; set; }

        /// <summary>
        /// 抄送人
        /// </summary>
        public string CopyToUser { get; set; }

        /// <summary>
        /// 抄送人姓名
        /// </summary>
        public string CopyToUserName { get; set; }

        /// <summary>
        /// 邮件状态
        /// </summary>
        public string Email_State { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string IFDel { get; set; }

        #endregion
    }

    public class EditMail_User
    {
        #region 邮件收件人列表
        
        /// <summary>
        /// 父级Guid
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// 收件人-单人
        /// </summary>
        public string ToUser { get; set; }

        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string ToUserName { get; set; }

        /// <summary>
        /// 已读未读
        /// </summary>
        public string Email_State { get; set; }

        /// <summary>
        ///  来源（收件/抄送）
        /// </summary>
        public string From_State { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string IFDel { get; set; }

        #endregion
    }
}
