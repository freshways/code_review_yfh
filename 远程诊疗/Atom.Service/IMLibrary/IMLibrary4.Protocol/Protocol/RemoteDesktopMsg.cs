﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace IMLibrary4.Protocol
{
    /// <summary>
    /// 远程桌面控制消息
    /// </summary>
    public class RemoteDesktopMsg:Element
    {
        /// <summary>
        /// 对端远程IP
        /// </summary>
        public IPAddress remoteIP { get; set; }

        /// <summary>
        /// 对端远程端口
        /// </summary>
        public int remotePort { get; set; }

        /// <summary>
        /// 对端局域网IP
        /// </summary>
        public IPAddress LocalIP { get; set; }

        /// <summary>
        /// 对端局域网端口
        /// </summary>
        public int LocalPort { get; set; }
        /// <summary>
        /// 屏幕宽度
        /// </summary>
        public int ScreenWidth { set; get; }
        /// <summary>
        /// 屏幕高度
        /// </summary>
        public int ScreenHeight { set; get; }
    }
}
