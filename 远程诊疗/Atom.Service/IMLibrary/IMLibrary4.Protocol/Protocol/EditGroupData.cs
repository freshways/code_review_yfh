﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Protocol
{
    /// <summary>
    /// 编辑分组资料
    /// </summary>
    public class EditGroupData : Organization.Group
    {
        /// <summary>
        /// 协议类型
        /// </summary>
        public IMLibrary4.Protocol.type type { get; set; }
        /// <summary>
        /// 分组版本信息
        /// </summary>
        public string Version { get; set; }
    }
}
