﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Xml;


namespace IMLibrary4.Protocol
{
    /// <summary>
    /// Element节
    /// </summary>
    public class Element
    {

        /// <summary>
        /// 通信密码
        /// </summary>
        public string Password { set; get; }

        /// <summary>
        /// 是否将此类消息类型保存为离线消息(默认为否)
        /// </summary>
        public bool IsSaveAsOfflineMsg=false;

        /// <summary>
        /// 消息最后激活时间
        /// </summary>
        public DateTime LastActivity;

        /// <summary>
        /// 消息发送帐号
        /// </summary>
        public string from
        {
            set;
            get;
        }


        /// <summary>
        /// 消息接收者或群
        /// </summary>
        public string to
        {
            set;
            get;
        }

        /// <summary>
        /// 协议类型
        /// </summary>
        public type type
        {
            get;
            set;
        }

        /// <summary>
        /// 服务id
        /// </summary>
        public string id
        {
            get;
            set;
        }

        /// <summary>
        /// 版本
        /// </summary>
        public string version
        {
            set;
            get;
        }

        private  List<object> data = new List<object>();
        /// <summary>
        /// 数据
        /// </summary>
        public List<object> Data
        {
            set { data = value; }
            get { return data; }
        }
    }
}
