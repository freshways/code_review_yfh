﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace IMLibrary4.Protocol
{
    /// <summary>
    ///  音视频对话消息
    /// </summary>
    public class AVMsg : Element
    {
       /// <summary>
        /// 对端局域网IP
        /// </summary>
        public IPAddress LocalIP { get; set; }

        /// <summary>
        /// 对端远程IP(公网)
        /// </summary>
        public IPAddress RemoteIP { get; set; }
         
        /// <summary>
        /// 对端局域网Rtp端口
        /// </summary>
        public int LocalRtpPort { get; set; }

        /// <summary>
        /// 对端局域网Rtcp端口
        /// </summary>
        public int LocalRtcpPort { get; set; }

        /// <summary>
        /// 对端远程Rtp端口(公网)
        /// </summary>
        public int RemoteRtpPort { get; set; }

        /// <summary>
        /// 对端远程Rtcp端口(公网)
        /// </summary>
        public int RemoteRtcpPort { get; set; }

        ///// <summary>
        ///// Rtp Turn ServerID
        ///// </summary>
        //public int RtpTurnServerID { get; set; }
        ///// <summary>
        ///// Rtcp Turn ServerID
        ///// </summary>
        //public int RtcpTurnServerID { get; set; }

    }
}
