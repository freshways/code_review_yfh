﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace IMLibrary4.Net
{
    /// <summary>
    /// UDP组件
    /// </summary>
    public partial class SockUDP  
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SockUDP()
        {

        }

        private UdpClient UDP_Server =null ;
        private System.Threading.Thread thdUdp;
        /// <summary>
        /// 是否不关闭接收数据事件
        /// </summary>
        private bool IsNotCloseEvent = true ;

        #region 事件
        /// <summary>
        /// 数据到达事件
        /// </summary>
        /// <param name="sender">套接字对像</param>
        /// <param name="e">事件相关参数</param>
        public delegate void UDPEventHandler(object sender, SockEventArgs e);
        /// <summary>
        /// 数据到达事件
        /// </summary>
        public event UDPEventHandler DataArrival;
       
        /// <summary>
        /// 套接字错误事件
        /// </summary>
        public event UDPEventHandler Sock_Error;
        #endregion

        #region 属性
        /// <summary>
        /// 
        /// </summary>
        public Socket socket;

        /// <summary>
        /// 是否允许端口复用(默认为是)
        /// </summary>
        public bool IsReuseAddress = true;

        /// <summary>
        /// 标识是否侦听
        /// </summary>
        public bool Listened
        {
            get;
            private set;
        }

        /// <summary>
        /// 侦听端口
        /// </summary>
        public int ListenPort
        {
            get;
            private set;
        }

        /// <summary>
        /// 功能描述
        /// </summary>
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// 本机主机信息
        /// </summary>
        public IPEndPoint LocalEndPoint;

        /// <summary>
        /// 是否异步通信
        /// </summary>
        public bool IsAsync
        {
            get;
            set;
        }
        /// <summary>
        /// 是否阻塞模式
        /// </summary>
        public bool Blocking
        {
            get;
            set;
        }
        #endregion

        #region 内部方法

        #region 接收数据
        ///// <summary>
        ///// 异步接收数据
        ///// </summary>
        //private void ReceiveInternal()
        //{
        //    if (!IsNotCloseEvent)
        //    {
        //        return;
        //    }
        //    try
        //    {
        //        UDP_Server.BeginReceive(
        //           new AsyncCallback(ReceiveCallback),
        //           null);
        //    }
        //    catch (SocketException ex)
        //    {
        //        throw ex;
        //    }
        //}

        ///// <summary>
        ///// 异步接收数据
        ///// </summary>
        ///// <param name="result"></param>
        //private void ReceiveCallback(IAsyncResult result)
        //{
        //    if (!IsNotCloseEvent)
        //    {
        //        return;
        //    }
        //    IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
        //    byte[] RData = null;
        //    try
        //    {
        //        RData = UDP_Server.EndReceive(result, ref remoteIP);
        //    }
        //    catch (SocketException ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ReceiveInternal();
        //    }

        //    if (DataArrival != null)
        //        DataArrival(this, new SockEventArgs(RData, remoteIP));
        //}


        /// <summary>
        /// 异步接收数据
        /// </summary>
        /// <param name="ar"></param>
        private void ReadCallback(IAsyncResult ar)
        {
            if (!IsNotCloseEvent)
            {
                return;
            }
            try
            {
                IPEndPoint ipend = new IPEndPoint(System.Net.IPAddress.Any, 0);  
                byte[] RData = UDP_Server.EndReceive(ar, ref ipend);
                if (DataArrival != null && RData != null && RData.Length > 0)
                    DataArrival(this, new SockEventArgs(RData, ipend));
                UDP_Server.BeginReceive(new AsyncCallback(ReadCallback), null);
            }
            catch (Exception e)
            {
                if (Sock_Error != null)
                    Sock_Error(this, new SockEventArgs(e.Source + "," + e.Message));
                Console.WriteLine("ReadCallback:" + e.Source + "," + e.Message);
                //System.Windows.Forms.MessageBox.Show("ReadCallback:" +    e.Source + "," + e.Message);
            }
        }

        /// <summary>
        /// 处理同步接收的数据
        /// </summary> 
        private void GetUDPData()
        {
            while (IsNotCloseEvent)
            {
                try
                {
                    if (UDP_Server.Client.Available > 0)
                    {
                        IPEndPoint ipend = null;
                        byte[] RData = UDP_Server.Receive(ref ipend);
                        if (DataArrival != null && RData.Length > 0)
                            DataArrival(this, new SockEventArgs(RData, ipend));
                    }
                }
                catch (Exception e)
                {
                    if (Sock_Error != null)
                        Sock_Error(this, new SockEventArgs(e.Source + "," + e.Message));
                    Console.WriteLine("GetUDPData:" + e.Source + "," + e.Message);
                }
            }
        }
        #endregion

        #region 异步发送数据
        /// <summary>
        /// 异步发送数据
        /// </summary>
        /// <param name="ar"></param>
        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                UDP_Server.EndSend(ar);
            }
            catch (Exception e)
            {
                if (Sock_Error != null)
                    Sock_Error(this, new SockEventArgs(e.Source + "," + e.Message));
                Console.WriteLine("SendCallback:" + e.Source + "," + e.Message);
                //System.Windows.Forms.MessageBox.Show("SendCallback:" + e.Source + "," + e.Message);
            }
        }

        //protected void SendInternal(byte[] buffer, IPEndPoint remoteIP)
        //{
        //    try
        //    {
        //        UDP_Server.BeginSend(
        //           buffer,
        //           buffer.Length,
        //           remoteIP,
        //           new AsyncCallback(SendCallback),
        //           null);
        //    }
        //    catch (SocketException ex)
        //    {
        //        //throw ex;
        //    }
        //}

        //private void SendCallback(IAsyncResult result)
        //{
        //    try
        //    {
        //        UDP_Server.EndSend(result);
        //    }
        //    catch (SocketException ex)
        //    {
        //        //throw ex;
        //    }
        //}
        #endregion
        #endregion

        #region 外部方法
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="Remote">对方主机信息</param>
        /// <param name="Data">要发送的数据</param>
        public void Send(IPEndPoint Remote , byte[] Data)
        {
            try
            {
                if (IsAsync)
                {
                    UDP_Server.BeginSend(Data, Data.Length, Remote, new AsyncCallback(SendCallback), null);
                    //SendInternal(Data, Remote);
                }
                else
                    UDP_Server.Send(Data, Data.Length, Remote);
            }
            catch (Exception e)
            {
                if (Sock_Error != null)
                    Sock_Error(this, new SockEventArgs(e.Source + "," + e.Message));
                Console.WriteLine("Send:" + e.Source + "," + e.Message);
            }
        }

        /// <summary>
        /// 侦听
        /// </summary>
        /// <param name="Port">侦听端口</param>
        public void Listen(int Port = 0)
        {
            if (Listened)
                return;

            IPEndPoint ipEnd = null;
            try
            {
                UDP_Server = new UdpClient();

                if (IsReuseAddress)
                    UDP_Server.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                #region 套接字低级控制
                //uint IOC_IN = 0x80000000;
                //uint IOC_VENDOR = 0x18000000;
                //uint SIO_UDP_CONNRESET = IOC_IN | IOC_VENDOR | 12;
                //UDP_Server.Client.IOControl((int)SIO_UDP_CONNRESET, new byte[] { Convert.ToByte(false) }, null);

                ////Tell the network stack what we send and receive has an order
                //UDP_Server.Client.DontFragment = true;
                ////Set max ttl for slower networks
                ////RtcpSocket.Ttl = 255;  

                //Windows操作系统的版本号一览
                //操作系统	PlatformID	主版本号	副版本号
                //Windows95  	1	    4            0
                //Windows98	    1	    4	        10
                //WindowsMe     1	    4	        90
                //WindowsNT3.5	2   	3	        0
                //WindowsNT4.0	2   	4	        0
                //Windows2000	2	    5       	0
                //WindowsXP    	2   	5         	1
                //Windows2003	2   	5       	2
                //WindowsVista	2   	6       	0
                //Windows7	    2   	6       	1
                //Windows8	  

                //成员名称	        说明
                //EdgeRestricted    IP 保护级别是“边缘受限的”。 在 Windows Server 2003 和 Windows XP 中，针对套接字的 IP 保护级别的默认值是“边缘受限的”。此值应由设计为在 Internet 上运行的应用程序使用。 此设置不允许使用 Windows Teredo 实现的网络地址转换 (NAT) 遍历。 这些应用程序可能会绕过 IPv4 防火墙，因此，必须加强应用程序的安全性以防范针对开放端口的 Internet 攻击。 
                //Restricted        IP 保护级别是“受限的”。  在 Windows Server 2008 R2 和 Windows Vista 中，针对套接字的 IP 保护级别的默认值是“不受限的”。此值应由未实现 Internet 方案的 Intranet 应用程序使用。 一般情况下，不会针对 Internet 样式的攻击来对这些应用程序进行测试或加强安全性。 此设置将限制仅接收链接本地的通信。
                //Unrestricted      IP 保护级别是“不受限的”。 此值应由设计为在 Internet 上运行的应用程序使用，包括利用 Windows 中内置的 IPv6 NAT 遍历功能（例如，Teredo）的应用程序。 这些应用程序可能会绕过 IPv4 防火墙，因此，必须加强应用程序的安全性以防范针对开放端口的 Internet 攻击。
                //Unspecified       IP 保护级别是“未指定的”。 在 Windows 7 和 Windows Server 2008 R2 中，针对套接字的 IP 保护级别的默认值是“未指定的”。

                ////获取系统信息
                //System.OperatingSystem OperatingSystemInfo = System.Environment.OSVersion;
                ////获取操作系统ID
                //System.PlatformID platformID = OperatingSystemInfo.Platform;
                ////获取主版本号
                //int versionMajor = OperatingSystemInfo.Version.Major;
                ////获取副版本号
                //int versionMinor = OperatingSystemInfo.Version.Minor;

                //May help if behind a router
                //Allow Nat Traversal  msdn.microsoft.com/zh-cn/library/system.net.sockets.ipprotectionlevel.aspx
                //SetIPProtectionLevel

                //if (versionMajor == 5 && versionMinor > 0)
                //    UDP_Server.Client.SetIPProtectionLevel(IPProtectionLevel.EdgeRestricted);//windows 2003和XP设置此值

                //UDP_Server.Client.SetIPProtectionLevel(IPProtectionLevel.Restricted);//如果仅允许局域网通信

                //if (versionMajor == 6 && versionMinor == 0)
                //    UDP_Server.Client.SetIPProtectionLevel(IPProtectionLevel.Unspecified);//Windows Server 2008 R2 和 Windows Vista设置此值

                //if (versionMajor == 6 && versionMinor >= 1)
                //    UDP_Server.Client.SetIPProtectionLevel(IPProtectionLevel.Unrestricted);// Windows 7、Windows 8 和 Windows Server 2008 R2

                //Set type of service
                //UDP_Server.Client.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.TypeOfService, 47);

                //UDP_Server.Client.Blocking = false;

                #endregion

                if (Port < 1 || Port > 65534)
                    ipEnd = new IPEndPoint(System.Net.IPAddress.Any, 0);
                else
                    ipEnd = new IPEndPoint(System.Net.IPAddress.Any, Port);


                UDP_Server.Client.Blocking = Blocking;
                UDP_Server.Client.Bind(ipEnd);
                Listened = true;//侦听

                //Console.WriteLine("Blocking:" + UDP_Server.Client.Blocking.ToString());
                //Console.WriteLine("SendBufferSize:" + UDP_Server.Client.SendBufferSize.ToString());
                //Console.WriteLine("ReceiveBufferSize:" + UDP_Server.Client.ReceiveBufferSize.ToString());
                //Console.WriteLine("Ttl:" + UDP_Server.Client.Ttl.ToString());

                if (IsAsync)//如果采用异步通信
                {
                    // ReceiveInternal();
                    UDP_Server.BeginReceive(new AsyncCallback(ReadCallback), null);
                }
                else//如果是同步通信
                {
                    thdUdp = new Thread(new ThreadStart(GetUDPData));
                    thdUdp.Start();
                }

                LocalEndPoint = (IPEndPoint)UDP_Server.Client.LocalEndPoint;
                ListenPort = LocalEndPoint.Port;

                socket = UDP_Server.Client;

            }
            catch (Exception e)
            {
                if (Sock_Error != null)
                    Sock_Error(this, new SockEventArgs(e.Source + "," + e.Message));
                Console.WriteLine("Listen:" + e.Source + "," + e.Message);
            }
        }

        /// <summary>
        /// 关闭接收数据的事件
        /// </summary>
        public void CloseEvent()
        {
            IsNotCloseEvent = false ;

            if (thdUdp != null)
                thdUdp.Abort();

            Thread.Sleep(0);
        }

        /// <summary>
        /// 关闭套接字
        /// </summary>
        public void CloseSock()
        {
            try
            {
                IsNotCloseEvent = false;// 关闭同步接收数据的事件

                if (thdUdp != null)
                    thdUdp.Abort();
                Thread.Sleep(0);

                if (UDP_Server != null )
                    UDP_Server.Close();

                Listened = false;
               
                Thread.Sleep(0);

            }
            catch (Exception e)
            {
                if (Sock_Error != null)
                    Sock_Error(this, new SockEventArgs(  e.Source + "," + e.Message));
                Console.WriteLine("CloseSock:" + e.Source + "," + e.Message);
            }
        }
        #endregion
    }
}
