﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using IMLibrary4.Net.IO;
using System.IO;
using IMLibrary4.Net;
using IMLibrary4.Net.TCP;
//using IMLibrary4.Protocol;

namespace IMLibrary4.Net
{
    /// <summary>
    /// 
    /// </summary>
    public class TCPServerSession : TCP_ServerSession
    {
        /// <summary>
        /// 
        /// </summary>
        public TCPServerSession() 
        {
             
        }

        /// <summary>
        /// 发送对像数据
        /// </summary>
        /// <param name="e"></param>
        //public void Write(object e)
        //{
        //    this.TcpStream.WriteLine(Factory.CreateXMLMsg(e));
        //}

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="Message">数据</param>
        public void Write(string Message)
        {
            this.TcpStream.WriteLine(Message);
        }
         
    }

 
}
