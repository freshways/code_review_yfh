﻿using System;
using System.Collections.Generic;
using System.Text;
using IMLibrary4.Net.TCP;
//using IMLibrary4.Protocol;

namespace IMLibrary4.Net
{
    /// <summary>
    /// TCP客户端
    /// </summary>
   public  class TCPClient:TCP.TCP_Client 
    {
      /// <summary>
      /// 构造
      /// </summary>
       public TCPClient()
       {
       }

       /// <summary>
       /// 发送数据
       /// </summary>
       /// <param name="e">IMLibrary4.Protocol.Element</param>
       //public void Write(Element e)
       //{
       //    this.Write(Factory.CreateXMLMsg(e));
       //}

       /// <summary>
       /// 发送数据
       /// </summary>
       /// <param name="Message">数据</param>
       public void Write(string Message)
       {
           this.TcpStream.WriteLine(Message);
       }

    }
}
