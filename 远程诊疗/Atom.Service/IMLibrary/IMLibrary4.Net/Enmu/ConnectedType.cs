﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Protocol
{
    #region 文件传输联接类型
    /// <summary>
    /// 文件传输联接类型
    /// </summary>
    public enum ConnectedType
    {
        /// <summary>
        /// 未联接
        /// </summary>
        None,
        /// <summary>
        /// UDP局域网联接
        /// </summary>
        UDPLocal,
        /// <summary>
        /// UDP广域网联接
        /// </summary>
        UDPRemote,
        /// <summary>
        /// UDP服务器中转联接
        /// </summary>
        UDPServer,

    }
    #endregion
}
