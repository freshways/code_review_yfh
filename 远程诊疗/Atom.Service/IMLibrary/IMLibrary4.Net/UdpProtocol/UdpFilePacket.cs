﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Protocol
{
    /// 协议格式：  协议类型  + Index +  Block (BaseData)
    /// 数据长度：    1字节     4字节       动态
    
    /// 
    /// <summary>
    /// UDP File Packet传输协议
    /// </summary>
    public class UdpFilePacket:UdpBasePacket
    { 
        /// <summary>
        /// 基础包长度
        /// </summary>
        public static readonly int HeaderLength = 5;

        /// <summary>
        /// 
        /// </summary>
        public UdpFilePacket()
            : base(HeaderLength)
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public UdpFilePacket(byte[] data)
            : base(data, HeaderLength)
        {
           
        }
              
        #region 协议类型
        /// <summary>
        /// 协议类型
        /// </summary>
        public byte  type
        {
            set
            {
                Buffer.SetByte(BaseData, 0, value);
            }
            get
            {
                return Buffer.GetByte(BaseData, 0);
            }
        }
        #endregion 

        #region 设置或获取包索引
        /// <summary>
        /// 设置或获取包索引
        /// </summary>
        public int Index
        {
            set
            {
                Buffer.BlockCopy(BitConverter.GetBytes(value), 0, BaseData, 1,4);
            }
            get
            {
                return BitConverter.ToInt32(BaseData,1);
            }
        }
        #endregion 
    }
}
