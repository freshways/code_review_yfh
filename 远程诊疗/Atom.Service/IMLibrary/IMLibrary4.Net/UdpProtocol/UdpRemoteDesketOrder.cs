﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMLibrary4.Protocol
{ 
    /// <summary>
    /// 远程桌面控制协议
    /// </summary>
    public class UdpRemoteDesketOrder:UdpBasePacket
    {
        #region 命令类型
        /// <summary>
        /// 命令类型
        /// </summary>
        public enum OrderType
        {
            /// <summary>
            /// 
            /// </summary>
            NONE=0,
            /// <summary>
            /// 
            /// </summary>
            MouseMove=1,
            /// <summary>
            /// 
            /// </summary>
            MouseLeftDown=2,
            /// <summary>
            /// 
            /// </summary>
            MouseLeftUp=3,
            /// <summary>
            /// 
            /// </summary>
            MouseRightDown=4,
            /// <summary>
            /// 
            /// </summary>
            MouseRightUp=5,
            /// <summary>
            /// 
            /// </summary>
            MouseClick=6,
            /// <summary>
            /// 
            /// </summary>
            MouseDoubleClick=7,
            /// <summary>
            /// 
            /// </summary>
            MouseWheel=8,
            /// <summary>
            /// 
            /// </summary>
            KeyDown=10,
            /// <summary>
            /// 
            /// </summary>
            KeyUp=11,

        }
        #endregion

        /// <summary>
        /// 基础包长度
        /// </summary>
        public static readonly int HeaderLength = 16;

        /// <summary>
        /// 
        /// </summary>
        public UdpRemoteDesketOrder()
            : base(HeaderLength) 
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">要生成包的数据</param>
        public UdpRemoteDesketOrder(byte[] data)
            : base(data, HeaderLength) 
        {
            
        }

        /// <summary>
        /// 命令类型
        /// </summary>
        public OrderType orderType
        {
            set
            {
                Buffer.SetByte(BaseData, 0, (byte)value);
            }
            get
            {
                return (OrderType)Buffer.GetByte(BaseData, 0);
            }
        }

        /// <summary>
        /// 键盘键值类型
        /// </summary>
        public byte KeyCode
        {
            set
            {
                Buffer.SetByte(BaseData, 1, value);
            }
            get
            {
                return Buffer.GetByte(BaseData, 1);
            }
        }

        /// <summary>
        ///  鼠标X值
        /// </summary>
        public int X
        {
            set
            {
                Buffer.BlockCopy(BitConverter.GetBytes(value), 0, BaseData, 2, 4);
            }
            get
            {
                return BitConverter.ToInt32(BaseData, 2);
            }
        }
        
        /// <summary>
        /// 鼠标Y值
        /// </summary>
        public int Y
        {
            set
            {
                Buffer.BlockCopy(BitConverter.GetBytes(value), 0, BaseData, 6, 4);
            }
            get
            {
                return BitConverter.ToInt32(BaseData, 6);
            }
        }

         /// <summary>
        /// 鼠标Delta值
        /// </summary>
        public int Delta
        {
            set
            {
                Buffer.BlockCopy(BitConverter.GetBytes(value), 0, BaseData, 10, 4);
            }
            get
            {
                return BitConverter.ToInt32(BaseData, 10);
            }
        }
    }
}
