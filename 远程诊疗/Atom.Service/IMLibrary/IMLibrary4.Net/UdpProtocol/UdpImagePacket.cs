﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMLibrary4.Protocol
{
    /// <summary>
    /// 远程桌面传输协议
    /// </summary>
    public enum RemoteDesketType
    {
        /// <summary>
        /// 获得图片侦
        /// </summary>
        GetImageFrame=1,
        /// <summary>
        /// 重发图片侦
        /// </summary>
        AgainSendImageFrame=2,
        /// <summary>
        /// 获得下一张桌面图片
        /// </summary>
        GetNextDesktopImage = 3,
        /// <summary>
        /// 发送一张桌面图片结束
        /// </summary>
        SendDesktopImageOver=4,
    }

    /// 协议格式：  图片包索引(PakIndex) + 图片索引Index + 是否重复包IsExsit + 重复图片索引ExsitIndex +   是否旧图片ExsitIsOld  +  包内容(Block=BaseData)
    /// 数据长度：     1字节             +   1字节       +   1字节           +         1字节          +             1字节       +       动态
 
    /// <summary>
    /// 远程协助图片包
    /// </summary>
    public class UdpImagePacket :UdpBasePacket 
    {
        /// <summary>
        /// 基础包长度
        /// </summary>
        public static readonly int HeaderLength = 5;

        /// <summary>
        /// 
        /// </summary>
        public UdpImagePacket( )
            : base(HeaderLength)
        {
             
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public UdpImagePacket(byte[] data)
            : base(data, HeaderLength)
        {
 
        }

        #region 图片包索引
        /// <summary>
        /// 图片包索引
        /// </summary>
        public byte SequenceNumber
        {
            set
            {
                Buffer.SetByte(BaseData, 0, value);
            }
            get
            {
                return Buffer.GetByte(BaseData, 0);
            }
        }
        #endregion 

        #region 图片索引
        /// <summary>
        /// 图片索引
        /// </summary>
        public byte Index
        {
            set
            {
                Buffer.SetByte(BaseData, 1, value);
            }
            get
            {
                return Buffer.GetByte(BaseData, 1);
            }
        }
        #endregion

        #region 是否重复包
        /// <summary>
        /// 是否重复包
        /// </summary>
        public bool IsExsit
        {
            set
            {
                if (value)
                    Buffer.SetByte(BaseData, 2, 1);
                else
                    Buffer.SetByte(BaseData, 2, 0);
            }
            get
            {
                if (Buffer.GetByte(BaseData, 2) == 0)
                    return false;
                else
                    return true;
            }
        }
        #endregion

        #region 重复图片索引
        /// <summary>
        /// 重复图片索引
        /// </summary>
        public byte ExsitIndex
        {
            set
            {
                Buffer.SetByte(BaseData, 3, value);
            }
            get
            {
                return Buffer.GetByte(BaseData, 3);
            }
        }
        #endregion

        #region 重复的图片是否旧图片
        /// <summary>
        /// 重复的图片是否旧图片
        /// </summary>
        public bool ExsitIsOld
        {
            set
            {
                if (value)
                    Buffer.SetByte(BaseData, 4, 1);
                else
                    Buffer.SetByte(BaseData, 4, 0);
            }
            get
            {
                if (Buffer.GetByte(BaseData, 4) == 0)
                    return false;
                else
                    return true;
            }
        }
        #endregion
    }

}

