﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMLibrary4.Protocol
{
    #region UDP传输类型
    /// <summary>
    ///  UDP传输类型
    /// </summary>
    public enum TransmitType
    {
        /// <summary>
        /// 获利中继服务ID
        /// </summary>
        GetTurnServerID=10,
        /// <summary>
        /// 设置对端中继服务ID
        /// </summary>
        SetToTurnServerID=20, 
        /// <summary>
        /// 路由我方的中继服务ID给对方
        /// </summary>
        RuterTurnServerID=30,
        /// <summary>
        /// 打洞数据包
        /// </summary>
        Penetrate = 114,
        /// <summary>
        /// 打洞成功包
        /// </summary>
        PenetrateSuccess=115,
        /// <summary>
        /// 心跳包
        /// </summary>
        Heartbeat=116, 
        /// <summary>
        /// 传输完成
        /// </summary>
        over = 118,
        /// <summary>
        /// 获得远程主机信息
        /// </summary>
        getRemoteEP = 126,
        /// <summary>
        /// 发送文件
        /// </summary>
        SendFile=200,
        /// <summary>
        /// 传输文件数据包
        /// </summary>
        getFilePackage = 201,
        /// <summary>
        /// 传输文件缓冲区
        /// </summary>
        getFileBuffer = 202,
        /// <summary>
        /// 校验包
        /// </summary>
        CheckPacket  = 203,
        /// <summary>
        /// 远程桌面控制包
        /// </summary>
        RemoteControl=204,
        /// <summary>
        /// 音频包
        /// </summary>
        Audio = 237,
        /// <summary>
        /// 视频包
        /// </summary>
        Video = 239,


    }
    #endregion

    #region Turn协议类型
    /// <summary>
    /// Turn协议类型
    /// </summary>
    public enum IPProtocolType
    {
        /// <summary>
        /// 
        /// </summary>
        Rtp=0,
        /// <summary>
        /// 
        /// </summary>
        Rtcp=1,
        /// <summary>
        /// 
        /// </summary>
        Tcp=2,
        /// <summary>
        /// 
        /// </summary>
        Udp=3,
        /// <summary>
        /// 
        /// </summary>
        Http=4,
        /// <summary>
        /// 
        /// </summary>
        Else=254
    }
    #endregion


    /// 协议格式：  协议类型(type) + （LocalTurnID）+ （RemoteTurnID） +  IP       +     Port     +     RTP协议类型  +  包内容(Block=BaseData)
    /// 数据长度：     1字节(0)    +     4字节(1)   +    4字节(5)      + 8字节(9)  +   4字节(17)  +      1字节(21)   +     动态分配(22)

    /// <summary>
    /// UdpAvPacket  数据包
    /// </summary>
    public class UdpTurnPacket:UdpBasePacket 
    {
        /// <summary>
        /// 基础包长度
        /// </summary>
        public static readonly int HeaderLength = 22;

        /// <summary>
        /// 
        /// </summary>
        public UdpTurnPacket()
            : base(HeaderLength)
        {

        }

         /// <summary>
         /// 
         /// </summary>
         /// <param name="data"></param>
        public UdpTurnPacket(byte[] data)
            : base(data, HeaderLength)
        {
          
        }
          
        #region 协议类型
        /// <summary>
        /// 协议类型
        /// </summary>
        public byte type
        {
            set
            {
                Buffer.SetByte(BaseData, 0, value);
            }
            get
            {
                return Buffer.GetByte(BaseData, 0);
            }
        }
        #endregion
         
        #region 设置或获取LocalTurnID
        /// <summary>
        /// 设置或获取LocalTurnID
        /// </summary>
        public int LocalTurnID
        {
            set
            {
                Buffer.BlockCopy(BitConverter.GetBytes(value), 0, BaseData, 1, 4);
            }
            get
            {
                return BitConverter.ToInt32(BaseData,1);
            }
        }
        #endregion

        #region 设置或获取RemoteTurnID
        /// <summary>
        /// 设置或获取RemoteTurnID
        /// </summary>
        public int RemoteTurnID
        {
            set
            {
                Buffer.BlockCopy(BitConverter.GetBytes(value), 0, BaseData, 5, 4);
            }
            get
            {
                return BitConverter.ToInt32(BaseData, 5);
            }
        }
        #endregion

        #region 远程主机
        /// <summary>
        /// 远程主机
        /// </summary>
        public System.Net.IPAddress RemoteIP
        {
            set
            {
                byte[] ip = value.GetAddressBytes();
                Buffer.BlockCopy(ip, 0, BaseData,9, ip.Length);
            }
            get
            {
                byte[] ip = new byte[4];
                Buffer.BlockCopy(BaseData, 9, ip, 0, ip.Length);
                try
                {
                    System.Net.IPAddress IP = new System.Net.IPAddress(ip);
                    return IP;
                }
                catch { return null; }
            }
        }
        #endregion

        #region 设置或获取端口号
        /// <summary>
        /// 设置或获取端口号
        /// </summary>
        public int Port
        {
            set
            {
                Buffer.BlockCopy(BitConverter.GetBytes(value), 0, BaseData, 17, 4);
            }
            get
            {
                return BitConverter.ToInt32(BaseData, 17);
            }
        }
        #endregion

        #region 协议类型
        /// <summary>
        /// 协议类型
        /// </summary>
        public IPProtocolType ProtocolType
        {
            set
            {
                protocolType = (byte)value;
            }
            get
            {
                if (protocolType == 0)
                    return IPProtocolType.Rtp;
                else
                    return IPProtocolType.Rtcp;

            }
        }
        #endregion

        #region 协议类型
        /// <summary>
        /// 协议类型
        /// </summary>
        private byte protocolType
        {
            set
            {
                Buffer.SetByte(BaseData, 21, value);
            }
            get
            {
                return Buffer.GetByte(BaseData, 21);
            }
        }
        #endregion 

    }
}
