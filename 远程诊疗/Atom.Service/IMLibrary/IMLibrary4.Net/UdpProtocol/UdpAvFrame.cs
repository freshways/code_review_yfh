﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMLibrary4.Protocol
{
    /// 协议格式：  数据类型(type) +   包内容(Block=BaseData)
    /// 数据长度：     1字节       +       动态

    /// <summary>
    /// 音视频UDP包
    /// </summary>
    public class UdpAvFrame : UdpBasePacket
    {
        /// <summary>
        /// 基础包长度
        /// </summary>
        public static readonly int HeaderLength = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AvType"></param>
        public UdpAvFrame(byte AvType)
            : base(HeaderLength)
        {
            this.type = AvType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public UdpAvFrame(byte[] data)
            : base(data, HeaderLength)
        {
             
        }

        #region 协议类型
        /// <summary>
        /// 协议类型
        /// </summary>
        public byte type
        {
            set
            {
                Buffer.SetByte(BaseData, 0, value);
            }
            get
            {
                return Buffer.GetByte(BaseData, 0);
            }
        }
        #endregion

    }
}
