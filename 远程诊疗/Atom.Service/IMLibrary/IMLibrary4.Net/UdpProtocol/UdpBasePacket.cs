﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace IMLibrary4.Protocol
{

    /// <summary>
    /// UDP协议基础包
    /// </summary>
    public class UdpBasePacket
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="HeaderLength"></param>
        public UdpBasePacket(int HeaderLength)
        {
            BaseLength = HeaderLength;
            BaseData = new byte[HeaderLength];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data">要生成包的数据</param>
        /// <param name="HeaderLength"></param>
        public UdpBasePacket(byte[] data, int HeaderLength)
        {
            BaseLength = HeaderLength;
            BaseData = new byte[HeaderLength];

            Buffer.BlockCopy(data, 0, BaseData, 0, BaseLength);
            Payload = data.Skip(BaseLength).ToArray();
        }

        #region 变量
        /// <summary>
        /// 基础包长度
        /// </summary>
        private int BaseLength = 0; 
        /// <summary>
        /// 数据块
        /// </summary>
        private byte[] Data = new byte[0]; 
        /// <summary>
        /// 消息包头
        /// </summary>
        protected byte[] BaseData = new byte[0];
        #endregion

        #region 获取包头数据
        /// <summary>
        /// 获取包头数据
        /// </summary>
        public byte[] Header
        {
            get
            {
                return BaseData;
            }
        }
        #endregion

        #region 获取有效数据块长度
        /// <summary>
        /// 获取有效数据块长度
        /// </summary>
        public int PayloadLength
        {
            get
            {
                return Data.Length;
            }
        }
        #endregion

        #region 获取或设置有效数据块内容
        /// <summary>
        /// 获取或设置有效数据块内容
        /// </summary>
        public byte[] Payload
        {
            set
            {
                if (value != null)
                    Data = value;
            }
            get
            {
                return Data;
            }
        }
        #endregion

        #region 获取消息字节数组
        /// <summary>
        /// 获得消息字节数组
        /// </summary>
        public byte[] ToBytes()
        {
            if (Data.Length == 0)
                return BaseData; 
            List<byte> result = new List<byte>();
            result.AddRange(BaseData);
            result.AddRange(Data);
            byte[] d = result.ToArray();
            return d; 
        }
        #endregion
    }
}
