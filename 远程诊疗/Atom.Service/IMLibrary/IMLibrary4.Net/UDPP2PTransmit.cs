﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using IMLibrary4;
using IMLibrary4.Protocol;
using System.Net.Sockets;

namespace IMLibrary4.Net
{
    /// <summary>
    /// P2P RTP Rtcp数据传输对像,
    /// 本对像必须配合 IMLibrary4.Server.UDPP2PServer 服务器使用.
    /// 可以穿越所有类型NAT，提供两个点的UDP通信隧道，最终实现UDP 的P2P数据传输.
    /// 对于对称型NAT(Symmetric NAT),提供了采用类TURN方法实现穿越.
    /// 您可以使用此对像P2P传输任何想要传输的数据.
    /// </summary>
    public class UDPP2PTransmit
    {
        #region 变量
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverEP">服务器主机信息</param>
        public UDPP2PTransmit(IPEndPoint serverEP)
        {
            ServerEP = serverEP;
        } 
         
        private void ShutdownThread()
        {
            if (sockUDP != null && sockUDP.Listened)
            {
                sockUDP.CloseSock();
                sockUDP = null;
            }
     
        }

        /// <summary>
        /// 释放网络资源
        /// </summary>
        public void CloseSock()
        {
            System.Threading.Thread st = new System.Threading.Thread(new System.Threading.ThreadStart(ShutdownThread));
            st.Start();

        }

        /// <summary>
        /// 服务器主机
        /// </summary>
        private IPEndPoint ServerEP = null;
        /// <summary>
        /// 对方局域网IP
        /// </summary>
        private IPAddress ToLocalIP = null;
        /// <summary>
        /// 对方局域网端口
        /// </summary>
        private int ToLocalPort = 0;
        /// <summary>
        /// 对方广域网远程IP
        /// </summary>
        private IPAddress ToRemoteIP = null;
        /// <summary>
        /// 对方广域网远程端口
        /// </summary>
        private int ToRemotePort = 0 ;

        //最终接收数据的终端
        private IPEndPoint ToIPEndPoint = null;
        /// <summary>
        /// 本地局域网IP
        /// </summary>
        private IPAddress MyLocalIP = null;
        /// <summary>
        /// 本地局域网端口
        /// </summary>
        private int MyLocalPort = 0;
        /// <summary>
        /// 本地广域网远程IP
        /// </summary>
        private IPAddress MyRemoteIP = null;
        /// <summary>
        /// 本地广域网远程端口
        /// </summary>
        private int MyRemotePort = 0 ; 
        /// <summary>
        /// 本地TRUN服务编号
        /// </summary>
        private int MyTurnServerID = 0;
        /// <summary>
        /// 对方TRUN服务编号
        /// </summary>
        private int ToTurnServerID = 0; 
        /// <summary>
        ///  UDP套接字
        /// </summary>
        private SockUDP sockUDP = null;
        /// <summary>
        /// 标识打洞是否成功
        /// </summary>
        private bool  PenetrateSuccess; 
        /// <summary>
        /// 联接类型
        /// </summary>
        public  ConnectedType connectedType  = ConnectedType.None;
   
        /// <summary>
        /// 获取是否连接
        /// </summary>
        public bool IsConnected
        {
            private set;
            get;
        }

        private int _MTU = 512;
        /// <summary>
        /// 获取MTU值
        /// </summary>
        public int MTU
        {
            set { _MTU = value; }
            get { return _MTU; }
        }

        #endregion

        #region 事件
        /// <summary>
        /// 收到侦数据事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        public delegate void RecDataEventHandler(object sender,byte[] data);
        /// <summary>
        /// 收到侦数据事件
        /// </summary>
        public event RecDataEventHandler ReceiveData;
        /// <summary>
        /// 获得本地网络和广域网络主机信息事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="local">本地主机信息</param>
        /// <param name="remote">远程主机信息</param>
        public delegate void GetIPEndPointEventHandler(object sender, IPEndPoint local, IPEndPoint remote);
        public event GetIPEndPointEventHandler GetIPEndPoint;
        protected virtual void OnGetIPEndPoint(object sender, IPEndPoint local, IPEndPoint remote)
        { 
            if (GetIPEndPoint != null)
                GetIPEndPoint(this, local, remote);//触发获取本机主机事件
        }

        public delegate void TransmitEventHandler(object sender, ConnectedType connectedType);
        /// <summary>
        /// 成功联接到服务器事件
        /// </summary>
        public event TransmitEventHandler TransmitConnected;
        /// <summary>
        /// 连接超时事件
        /// </summary>
        public event TransmitEventHandler ConnectOutTime;
        /// <summary>
        /// 申请中转服务器忙事件
        /// </summary>
        public event TransmitEventHandler TURNServerBusying;
        /// <summary>
        /// 连接P2P服务器失败事件
        /// </summary>
        public event TransmitEventHandler ConnectUDPP2PServerFailed;


        /// <summary>
        ///触发成功联接到服务器事件
        /// </summary>
        protected virtual void OnTransmitConnected()
        {
            System.Diagnostics.Debug.WriteLine("已经连接:" + connectedType.ToString());
            if (IsConnected) return;
            IsConnected = true;

            if (TransmitConnected != null)
                TransmitConnected(this, connectedType);
        }
        /// <summary>
        /// 触发连接P2P服务器失败事件
        /// </summary>
        protected virtual void OnConnectUDPP2PServerFailed()
        {
            if (ConnectUDPP2PServerFailed != null)
                ConnectUDPP2PServerFailed(this, connectedType);
        }
        /// <summary>
        /// 触发申请中转服务器忙事件
        /// </summary>
        protected virtual void OnConnectTURNServerBusying()
        {
            if (TURNServerBusying != null)
                TURNServerBusying(this, connectedType);
        }
        #endregion 
 
        #region 公共方法

        #region UDP服务
        /// <summary>
        /// 开始网络联接服务
        /// </summary>
        public void StartNetConnection()
        {
            if (sockUDP != null) return;//如果已经初始化套接字,则退出

            #region sockRtpUDP
            if (sockUDP == null)
            {
                sockUDP = new SockUDP();
                sockUDP.IsAsync = true;
                sockUDP.Blocking = false;//处于非阻塞
                sockUDP.Listen(0);//随机侦听
                //sockUDP.socket.Ttl = 255;
                //sockUDP.socket.SetSocketOption(System.Net.Sockets.SocketOptionLevel.IP, System.Net.Sockets.SocketOptionName.TypeOfService,47);
                sockUDP.DataArrival += (sender, e) => DataArrival(sender,e); 
            }
            #endregion
             
            #region 获得本机局域网IPV4地址和侦听端口
            System.Net.IPAddress[] ips = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());
            for (int i = ips.Length - 1; i >= 0; i--)
            {
                if (ips[i].GetAddressBytes().Length == 4)
                {
                    //获得本机局域网地址
                    MyLocalIP = ips[i];
                    MyLocalPort = sockUDP.ListenPort;
                    break;
                }
            }
            #endregion
             
            #region 主机信息获取 时间计数器
            UdpTurnPacket p = new UdpTurnPacket();//请求获取广域网远程主机信息包
            p.type = (byte)TransmitType.getRemoteEP;//获得公网主机信息
            sockUDP.Send(ServerEP, p.Header);//发送请求到服务器

            int TimeCount = 0;
            System.Timers.Timer timer1 = new System.Timers.Timer();
            timer1.Interval = 500;
            timer1.Enabled = true;
            timer1.Elapsed += (s, e) =>
            {
                if (MyRemotePort == 0 && TimeCount < 10)//如果还未获得本机远程主机信息
                {
                    sockUDP.Send(ServerEP, p.Header);//每隔500毫秒发送一次请求
                } 

                if (MyRemoteIP != null && MyRemotePort > 0)
                {
                    //终止发送，并触发获得主机事件
                    timer1.Enabled = false;
                    timer1.Close();
                    timer1.Dispose(); 
                    timer1 = null;
                    return;
                }

                if (TimeCount > 10)//无论5秒后是否已经获得本地广域网主机信息
                {
                    //终止发送，并触发获得主机事件
                    timer1.Enabled = false;
                    timer1.Close();
                    timer1.Dispose();
                    timer1 = null;
                    if (MyRemoteIP == null)//如果连接服务器超时 
                    {
                        OnConnectUDPP2PServerFailed();//触发联接P2P服务器失败
                        return;
                    }
                }
                TimeCount++;
            };
            #endregion
        } 
        #endregion

        #region 设置远程主机信息(实现任何类型的NAT穿越)
        /// <summary>
        /// 设置远程主机信息(实现任何类型的NAT穿越)
        /// </summary>
        /// <param name="remoteLocalIP"></param>
        /// <param name="remoteIP"></param>
        public  void setRemoteIP(IPEndPoint remoteLocalIP, IPEndPoint remoteIP)
        {
            this.ToLocalIP = remoteLocalIP.Address ;
            this.ToRemoteIP = remoteIP.Address ;
            this.ToLocalPort = remoteLocalIP.Port  ;
            this.ToRemotePort = remoteIP.Port;

            UdpTurnPacket penetratePacket = new UdpTurnPacket();//打洞数据包
            penetratePacket.type = (byte)TransmitType.Penetrate;
            //penetratePacket.type = (byte)TransmitType.over; 

            UdpTurnPacket getTurnServerIDPacket = new UdpTurnPacket();//申请TURN服务包，实现对称NAT穿越
            getTurnServerIDPacket.type = (byte)TransmitType.GetTurnServerID;
            getTurnServerIDPacket.ProtocolType = IPProtocolType.Udp;

            #region 网络互联打洞 时间计数器(以下代码实现NAT穿越)
            int TimeCount = 0;
            System.Timers.Timer timer1 = new System.Timers.Timer();
            timer1.Interval =500;
            timer1.Enabled = true;
            timer1.Elapsed += (sender, e) =>
            {
                TimeCount++;

                #region 如果联接，触发事件
                if (connectedType != ConnectedType.None)
                {
                    //终止发送，并触发获得主机事件
                    if (timer1 != null)
                    {
                        timer1.Enabled = false; timer1.Close(); timer1.Dispose(); timer1 = null;
                    }
                    OnTransmitConnected();//触发连接建立事件
                }
                #endregion

                if (PenetrateSuccess) return;

                #region 局域网打洞过程
                if (connectedType == ConnectedType.None && TimeCount <=4)//如果2秒后还未与对方建立联接
                {
                    if (sockUDP != null && sockUDP.Listened)
                    {
                        if (TimeCount ==1)
                        {
                            penetratePacket.Payload = new byte[32000];
                            sockUDP.Send(new IPEndPoint(ToLocalIP, ToLocalPort), penetratePacket.ToBytes());//发送一次局域网超大包打洞请求
                        }
                        else if (TimeCount== 2)
                        {
                            penetratePacket.Payload = new byte[24000];
                            sockUDP.Send(new IPEndPoint(ToLocalIP, ToLocalPort), penetratePacket.ToBytes());//发送一次局域网大包打洞请求
                        }
                        else if (TimeCount== 3)
                        {
                            penetratePacket.Payload = new byte[12000];
                            sockUDP.Send(new IPEndPoint(ToLocalIP, ToLocalPort), penetratePacket.ToBytes());//发送一次局域网大包打洞请求
                        }
                        else if (TimeCount== 4)
                        {
                            penetratePacket.Payload = new byte[1400];
                            sockUDP.Send(new IPEndPoint(ToLocalIP, ToLocalPort), penetratePacket.ToBytes());//发送一次局域网中包打洞请求
                        } 
                    }
                }
                #endregion

                #region 广域网打洞过程
                if (connectedType == ConnectedType.None && TimeCount > 4 && TimeCount <=16)//如果8秒后还未与对方建立联接
                {
                    if (sockUDP != null && sockUDP.Listened)
                    {
                        if (!PenetrateSuccess)
                        {
                            if (TimeCount <= 10)//如果2秒后5秒前未成功
                            {
                                penetratePacket.Payload = new byte[1024];
                                sockUDP.Send(new IPEndPoint(ToRemoteIP, ToRemotePort), penetratePacket.ToBytes());//发送一次广域网打洞请求
                            }
                            else//如果5秒后8秒前未成功
                            {
                                penetratePacket.Payload = new byte[512];
                                sockUDP.Send(new IPEndPoint(ToRemoteIP, ToRemotePort), penetratePacket.ToBytes());//发送一次广域网打洞请求
                            }
                        }
                        else
                        {
                            penetratePacket.Port = MTU;
                            sockUDP.Send(new IPEndPoint(ToRemoteIP, ToRemotePort), penetratePacket.Header);//发送一次广域网打洞请求
                        }
                    }
                }
                #endregion

                #region 申请TURN服务过程
                if (TimeCount==20)//如果10秒后NAT穿越不成功，那么申请TURN服务，实现NAT穿越(申请时间为12秒，每3秒发一次请求，发3次请求)
                {
                    MTU = 512;
                    sockUDP.Send(ServerEP, getTurnServerIDPacket.Header);
                }
                else if (TimeCount == 26  && MyTurnServerID==0)//如果13秒后还未获利服务编号
                { 
                    sockUDP.Send(ServerEP, getTurnServerIDPacket.Header);
                }
                else if (TimeCount == 32 && MyTurnServerID == 0)//如果16秒后还未获利服务编号
                { 
                    sockUDP.Send(ServerEP, getTurnServerIDPacket.Header);
                }

                if (TimeCount == 40)//如果20秒后NAT穿越不成功
                {
                    //终止发送，并触发获得主机事件
                    if (timer1 != null)
                    {
                        timer1.Enabled = false; timer1.Close(); timer1.Dispose(); timer1 = null;
                    }
                    OnConnectTURNServerBusying();//触发申请中转服务器忙事件
                } 
                #endregion 
            };
            #endregion
        }
        #endregion 
 
        #region 设置对端Turn ServerID
        /// <summary>
        /// 设置对端Turn ServerID
        /// </summary>
        /// <param name="toTurnServerID"></param>
        private void setRemoteTurnServerID(int toTurnServerID)
        {
            int TimeCount = 0;
            System.Timers.Timer timer1 = new System.Timers.Timer();
            timer1.Interval = 500;
            timer1.Enabled = true;
            timer1.Elapsed += (s, e) =>
            {
                TimeCount++;
                if (MyTurnServerID > 0)
                {
                    if (timer1 != null)
                    {
                        timer1.Enabled = false; timer1.Close(); timer1.Dispose(); timer1 = null;
                    }
                    UdpTurnPacket RtpPacket = new UdpTurnPacket();
                    RtpPacket.type = (byte)TransmitType.SetToTurnServerID;
                    RtpPacket.LocalTurnID = MyTurnServerID;
                    RtpPacket.RemoteTurnID = toTurnServerID;
                    for (int i = 0; i < 3; i++)
                        sockUDP.Send(ServerEP, RtpPacket.Header);//向服务器注册对端 RTP TurnServerID
                }
            };
        }
        #endregion
         
        #region 发送数据
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int SendData(byte[] data)
        {
            //try
            //{
            //    if (ToIPEndPoint == null)
            //    {
            //        ToIPEndPoint = new IPEndPoint(ToRemoteIP, ToRemotePort);
            //        sockUDP.socket.Connect(ToIPEndPoint);
            //        IsConnected = true;
            //    }

            //    if (IsConnected)
            //    {
            //        sockUDP.socket.Send(data); 
            //sockUDP.Send(ToIPEndPoint,data);
            //    }
            //}
            //catch
            //{
            //}
            SocketError error = SocketError.SocketError;
            int sent = 0;
            try
            {
                if (ToIPEndPoint == null)
                {
                    ToIPEndPoint = new IPEndPoint(ToRemoteIP, ToRemotePort);
                    sockUDP.socket.Connect(ToIPEndPoint);
                    IsConnected = true;
                }

                if (IsConnected)
                {
                    
                    //Send data
                    sent = sockUDP.socket.Send(data, sent, data.Length - sent, SocketFlags.None, out error);
                    if (sent < data.Length || error == SocketError.MessageSize)
                    {
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("Did not send entire datagram, SocketError = \"" + error + "\", Resending rest of the data (" + (data.Length - sent) + " Bytes) with SocketFlags.Truncated.");
#endif

                        //Send the rest using Truncated
                        sent += sockUDP.socket.Send(data, sent, data.Length - sent, SocketFlags.Truncated, out error);
                    }

                    //If the send was not successful throw an error with the errorCode
                    if (error != SocketError.Success) throw new SocketException((int)error);
                }
            }
            catch (Exception ex)
            {
                string output = "Exception occured in RtpSession.SendData: " + ex.Message + ". SocketError = " + error + '"';
                if (ex is SocketException) output += ", ErrorCode = " + (ex as SocketException).ErrorCode + '"';
                System.Diagnostics.Debug.WriteLine(output);
            }
            return sent;
        }
        #endregion

        #endregion

        #region 私有方法

        #region 数据包到达事件
        /// <summary>
        /// 数据包到达事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataArrival(object sender, SockEventArgs e)
        {
            if (connectedType != ConnectedType.None && ReceiveData != null)
            {
                ReceiveData(this, e.Data);
                return;
            }

            if (e.Data.Length < UdpTurnPacket.HeaderLength)
                return;

            UdpTurnPacket packet = new UdpTurnPacket(e.Data);//获得数据包
            switch (packet.type)
            {
                case (byte)TransmitType.GetTurnServerID://收到 TurnServerID 
                    {
                        #region
                        if (MyTurnServerID > 0) return;//如果已经获得一次服务编号,则退出

                        MyTurnServerID = packet.LocalTurnID;//设置我方TurnServerID(服务器中继端口)

                        byte[] dataACK = new byte[1];
                        for (int i = 0; i < 10; i++)
                            sockUDP.Send(new IPEndPoint(ServerEP.Address, MyTurnServerID), dataACK);//向TURN SERVER(服务器中继端口)发送一个握手包

                        packet.type = (byte)TransmitType.RuterTurnServerID;//路由我方serverID 
                        packet.RemoteIP = ToRemoteIP;//对方IP
                        packet.Port = ToRemotePort;//对方Rtp端口
                        packet.RemoteTurnID = MyTurnServerID;//我方TURN服务ID
                        for (int i = 0; i < 5; i++)
                            sockUDP.Send(ServerEP, packet.Header);

                        if (ToTurnServerID > 0)//如果已经收到对方serverID
                        {
                            ToRemoteIP = ServerEP.Address;
                            ToRemotePort = MyTurnServerID;//我方serverID

                            setRemoteTurnServerID(ToTurnServerID);
                        }
                        #endregion
                    }
                    break;
                case (byte)TransmitType.RuterTurnServerID://收到对方TurnServerID
                    {
                        #region
                        if (ToTurnServerID == 0)//如果没有收到过对方 TurnServerID
                        {
                            ToTurnServerID = packet.RemoteTurnID;//记录对方TurnServerID

                            if (MyTurnServerID == 0) return;//如果我方还未从服务器获取TurnServerID

                            ToRemoteIP = ServerEP.Address;
                            ToRemotePort = MyTurnServerID;//我方serverID

                            setRemoteTurnServerID(ToTurnServerID);
                        }
                        #endregion
                    }
                    break;
                case (byte)TransmitType.SetToTurnServerID://设置远程TurnServerID成功
                    {
                        #region
                        if (this.connectedType == ConnectedType.None)
                        {
                            this.connectedType = ConnectedType.UDPServer;
                            OnTransmitConnected();
                        }
                        #endregion
                    }
                    break;
                case (byte)TransmitType.Penetrate://收到另一客户端请求打洞 
                    {
                        //return;//如果想测试服务器中继能力，请返回，即不执行P2P打洞
                        #region
                        ToRemoteIP = e.RemoteIPEndPoint.Address;
                        ToRemotePort = e.RemoteIPEndPoint.Port;
                        PenetrateSuccess = true;//标识打洞成功

                        if (packet.PayloadLength > 0)
                        {
                            if (MTU < packet.PayloadLength)
                                MTU = packet.PayloadLength;//设置MTU值
                            packet.Port = MTU;
                        }
                        else if (MTU < packet.Port)
                        {
                             MTU = packet.Port;//设置MTU值
                        }

                        for (int i = 0; i < 10; i++)
                        {
                            sockUDP.Send(e.RemoteIPEndPoint, packet.Header);
                            System.Threading.Thread.Sleep(0);
                        }

                        System.Threading.Thread.Sleep(500);


                        if (e.RemoteIPEndPoint.Address.ToString() == ToLocalIP.ToString())
                            connectedType = ConnectedType.UDPLocal;
                        else
                            connectedType = ConnectedType.UDPRemote; 
                        #endregion
                    }
                    break;
                case (byte)TransmitType.getRemoteEP://收到自己的远程主机信息 
                    {
                        #region
                        //设置自己的远程主机信息
                        if (MyRemoteIP == null && MyRemotePort == 0)
                        {
                            MyRemoteIP = packet.RemoteIP;
                            MyRemotePort = packet.Port;
                            OnGetIPEndPoint(this, new IPEndPoint(MyLocalIP, MyLocalPort), new IPEndPoint(MyRemoteIP, MyRemotePort));
                        }
                        #endregion
                    }
                    break;
            }
        }
        #endregion


        #endregion
    }
}
