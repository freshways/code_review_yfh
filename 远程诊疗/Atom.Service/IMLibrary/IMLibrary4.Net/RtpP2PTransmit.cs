﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using IMLibrary4;
using IMLibrary4.Net.UDP ;
using IMLibrary4.Protocol;
using IMLibrary4.Rtp;

namespace IMLibrary4.Net
{
    /// <summary>
    /// P2P RTP Rtcp数据传输对像,
    /// 本对像必须配合 IMLibrary4.Server.UDPP2PServer 服务器使用.
    /// 可以穿越所有类型NAT，提供两个点的UDP通信隧道，最终实现UDP 的P2P数据传输.
    /// 对于对称型NAT(Symmetric NAT),提供了采用类TURN方法实现穿越.
    /// 您可以使用此对像P2P传输任何想要传输的数据.
    /// </summary>
    public class RtpP2PTransmit
    {
        #region 变量
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverEP">服务器主机信息</param>
        public RtpP2PTransmit(IPEndPoint serverEP)
        {
            ServerEP = serverEP;
        } 

        public void Dispose()
        {
            System.Threading.Thread st = new System.Threading.Thread(new System.Threading.ThreadStart(ShutdownThread));
            st.Start();
            if (rtpSession!=null && rtpSession.Connected)
                rtpSession.Disconnect();
            rtpSession = null;
        }

        private void ShutdownThread()
        {
            if (sockRtpUDP != null && sockRtpUDP.Listened)
            {
                sockRtpUDP.CloseSock();
                sockRtpUDP = null;
            }
            if (sockRtcpUDP != null && sockRtcpUDP.Listened)
            {
                sockRtcpUDP.CloseSock();
                sockRtcpUDP = null;
            }
        }

        /// <summary>
        /// 服务器主机
        /// </summary>
        private IPEndPoint ServerEP = null;

        /// <summary>
        /// 对方本地IP
        /// </summary>
        private IPAddress ToLocalIP = null;
        /// <summary>
        /// 对方远程IP
        /// </summary>
        private IPAddress ToRemoteIP = null;
        /// <summary>
        /// 
        /// </summary>
        private int ToLocalRtpPort = 0, ToLocalRtcpPort = 0;
        /// <summary>
        /// 
        /// </summary>
        private int ToRemoteRtpPort = 0, ToRemoteRtcpPort = 0;

        /// <summary>
        /// 自己本地IP
        /// </summary>
        private IPAddress MyLocalIP = null;
        /// <summary>
        /// 自己远程IP
        /// </summary>
        private IPAddress MyRemoteIP = null;
        /// <summary>
        /// 
        /// </summary>
        private int MyLocalRtpPort = 0, MyLocalRtcpPort = 0;
        /// <summary>
        /// 
        /// </summary>
        private int MyRemoteRtpPort = 0, MyRemoteRtcpPort = 0;

        private int MyRtpTurnServerID = 0;

        private int MyRtcpTurnServerID = 0;

        private int ToRtpTurnServerID = 0;

        private int ToRtcpTurnServerID = 0;

        /// <summary>
        ///  RtpUDP套接字
        /// </summary>
        private SockUDP sockRtpUDP = null;
        /// <summary>
        ///  RtcpUDP套接字
        /// </summary>
        private SockUDP sockRtcpUDP = null;
         
        /// <summary>
        /// 联接类型
        /// </summary>
        private ConnectedType connectedTypeRtp = ConnectedType.None;

        private ConnectedType connectedTypeRtcp = ConnectedType.None;

        /// <summary>
        /// RtpSession 
        /// </summary>
        private Rtp.RtpSession rtpSession;

        /// <summary>
        /// 是否连接
        /// </summary>
        public bool IsConnected
        {
            private set;
            get;
        }
        #endregion

        #region 事件
        /// <summary>
        /// 收到侦数据事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        public delegate void RecDataEventHandler(object sender,byte[] data);
        /// <summary>
        /// 收到侦数据事件
        /// </summary>
        public event RecDataEventHandler RecFrameData;
        /// <summary>
        /// 收到Rtcp app 数据包事件
        /// </summary>
        public event RtcpPacketChangedEventHandler RecRtcpAppPacket;
        /// <summary>
        /// 收到Rtcp报告包事件
        /// </summary>
        public event RtcpPacketChangedEventHandler RecRtcpReportPacket;

        public delegate void RtcpPacketChangedEventHandler(object sender, Rtcp.RtcpPacket Packet);
        public delegate void RtpFrameChangedEventHandler(object sender, Rtp.RtpFrame Frame);

        //public event RtpFrameChangedEventHandler RtpFrameChanged;


        /// <summary>
        /// 获得IPEndPoint事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void GetIPEndPointEventHandler(object sender, RtpEventArgs e);
        public event GetIPEndPointEventHandler GetIPEndPoint;
        protected virtual void OnGetIPEndPoint(object sender, RtpEventArgs e)
        { 
            if (GetIPEndPoint != null)
            {
                GetIPEndPoint(this, e);//触发获取本机主机事件
            }
        } 

        public delegate void TransmitEventHandler(object sender, ConnectedType connectedType);
        /// <summary>
        /// 成功联接到服务器事件
        /// </summary>
        public event TransmitEventHandler TransmitConnected;
        /// <summary>
        /// 连接超时事件
        /// </summary>
        public event TransmitEventHandler ConnectOutTime;

        /// <summary>
        ///成功联接到服务器事件
        /// </summary>
        protected virtual void OnTransmitConnected()
        {
            System.Diagnostics.Debug.WriteLine("已经连接:" + this.connectedTypeRtp.ToString() + ":" + this.connectedTypeRtcp.ToString());
            if (IsConnected) return;
            IsConnected = true; 
            if (TransmitConnected != null)
            {
                TransmitConnected(this, this.connectedTypeRtp);

                if (connectedTypeRtp != ConnectedType.None)
                {

                    #region 测试TURN 隧道打洞情况
                    //if (connectedTypeRtp == ConnectedType.UDPServer)
                    //{
                    //    UdpTurnPacket packet = new UdpTurnPacket();//获得数据包
                    //    packet.type = (byte)TransmitType.SetToTurnServerID;
                    //    packet.ProtocolType = IPProtocolType.Rtp;
                    //    sockRtpUDP.Send(new IPEndPoint(ToRemoteIP, ToRemoteRtpPort), packet.BaseData);
                    //    packet.ProtocolType = IPProtocolType.Rtcp;
                    //    sockRtcpUDP.Send(new IPEndPoint(ToRemoteIP, ToRemoteRtcpPort), packet.BaseData);
                    //}
                    //return;
                    #endregion


                    #region rtpSession == nul
                    if (rtpSession == null)
                    {
                        rtpSession = Rtp.RtpSession.Receiever(ToRemoteIP);


                        rtpSession.RtpFrameChanged += (sender, frame) =>
                            {
                                //if (!frame.IsMissingPackets && RtpFrameChanged != null)//如果收到合法侦
                                //    RtpFrameChanged(this, frame);
                                if (!frame.IsMissingPackets && RecFrameData != null)//如果收到合法侦
                                    RecFrameData(this, frame.Assemble());
                            };

                        rtpSession.RtpPacketReceieved += (sender, p) =>
                            {
                                Console.WriteLine("rec RtpPacketReceieved");
                            };

                        //rtpSession.RtcpPacketReceieved += (s, p) =>
                        //    {
                        //        //if (p.PacketType == Rtcp.RtcpPacket.RtcpPacketType.ApplicationSpecific)//收到应用包
                        //        //{
                        //        //    if (RecRtcpAppData != null)
                        //        //        RecRtcpAppData(this, p.Payload);
                        //        //}
                        //        //else 
                        //        if (p.PacketType == Rtcp.RtcpPacket.RtcpPacketType.SendersReport)//收到Report包
                        //        {
                        //            if (RecRtcpReportPacket != null)
                        //                RecRtcpReportPacket(this, p);
                        //            //rtpSession.SendReceiversReports();
                        //            //Console.WriteLine(" p.Channel:" + p.Channel.ToString());
                        //        }
                        //    };

                        //Create a Session Description
                        Sdp.SessionDescription SessionDescription = new Sdp.SessionDescription(1);

                        //Add a MediaDescription to our Sdp on any port  for RTP/AVP Transport using the RtpJpegPayloadType
                        if (connectedTypeRtp != ConnectedType.UDPServer)
                            SessionDescription.Add(new Sdp.MediaDescription(Sdp.MediaType.video, ToRemoteRtpPort, Rtsp.Server.Streams.RtpSource.RtpMediaProtocol, Rtp.JpegFrame.RtpJpegPayloadType));
                        else
                            SessionDescription.Add(new Sdp.MediaDescription(Sdp.MediaType.video, MyRemoteRtpPort, Rtsp.Server.Streams.RtpSource.RtpMediaProtocol, Rtp.JpegFrame.RtpJpegPayloadType));

                        Rtp.RtpSession.TransportContext Context = new Rtp.RtpSession.TransportContext(0, 1,(uint)DateTime.UtcNow.Ticks, SessionDescription.MediaDescriptions[0]);

                        //Context.InitializeSockets(IPAddress.Parse("0.0.0.0"), ToRemoteIP, MyLocalRtpPort, MyLocalRtcpPort, ToRemoteRtpPort, ToRemoteRtcpPort, true);


                        sockRtcpUDP.CloseEvent();//关闭原事件
                        sockRtpUDP.CloseEvent();//关闭原事件

                        System.Threading.Thread.Sleep(1000);

                        Context.InitializeSockets(sockRtpUDP.socket,sockRtcpUDP.socket,ToRemoteIP,ToRemoteRtpPort,ToRemoteRtcpPort);

                       
                         //try to add the transportChannel
                        rtpSession.AddTransportContext(Context);
                         
                        //Connect the sender
                        rtpSession.Connect();
                        //Send an initial report
                        rtpSession.SendSendersReports();

                        System.Timers.Timer timer1 = new System.Timers.Timer();
                        timer1.Interval = 5000;
                        timer1.Elapsed += (s, e) =>
                            {
                                if (rtpSession != null && rtpSession.Connected)
                                {
                                    rtpSession.SendSendersReports();
                                }
                                else
                                {
                                    timer1.Close(); timer1.Dispose();
                                }
                            };
                        timer1.Enabled = true;

                        //sockRtpUDP.CloseSock(); sockRtcpUDP.CloseSock();
                    }
                    #endregion 
                }
            }
    }
        #endregion 
 
        #region 公共方法

        #region UDP服务
        /// <summary>
        /// 开始
        /// </summary>
        public void Start()
        {
            if (sockRtpUDP != null || sockRtcpUDP != null) return;//如果已经初始化套接字,则退出

            #region sockRtpUDP
            if (sockRtpUDP == null)
            {
                sockRtpUDP = new SockUDP();
                sockRtpUDP.Listen(0);//随机侦听
                sockRtpUDP.DataArrival += (sender, e) =>
                    {
                        if (e.Data.Length < UdpTurnPacket.HeaderLength) return;
                        UdpTurnPacket packet = new UdpTurnPacket(e.Data);//获得数据包
                        if (packet.type == (byte)TransmitType.GetTurnServerID)//收到 TurnServerID   
                        {

                            MyRtpTurnServerID = packet.LocalTurnID;//设置我方TurnServerID(服务器中继端口)
                            sockRtpUDP.Send(new IPEndPoint(ServerEP.Address, MyRtpTurnServerID), new byte[4]);//向TURN SERVER(服务器中继端口)发送一个握手包
                            System.Threading.Thread.Sleep(20);
                            sockRtpUDP.Send(new IPEndPoint(ServerEP.Address, MyRtpTurnServerID), new byte[4]);//向TURN SERVER(服务器中继端口)发送一个握手包
                            System.Threading.Thread.Sleep(20);
                            sockRtpUDP.Send(new IPEndPoint(ServerEP.Address, MyRtpTurnServerID), new byte[4]);//向TURN SERVER(服务器中继端口)发送一个握手包

                            packet.type = (byte)TransmitType.RuterTurnServerID;//路由我方serverID
                            packet.ProtocolType = IPProtocolType.Rtp;
                            packet.RemoteIP = ToRemoteIP;//对方IP
                            packet.Port = ToRemoteRtpPort;//对方Rtp端口
                            packet.RemoteTurnID = MyRtpTurnServerID;//我方TURN服务ID
                            sockRtpUDP.Send(ServerEP, packet.Header);

                            if (ToRtpTurnServerID > 0)//如果已经收到对方serverID
                            {
                                ToRemoteIP = ServerEP.Address;
                                ToRemoteRtpPort = MyRtpTurnServerID;//我方serverID
                                if (ToRtpTurnServerID > 0 && ToRtcpTurnServerID > 0)
                                    setRemoteTurnServerID(ToRtpTurnServerID, ToRtcpTurnServerID);
                            }

                        }
                        else if (packet.type == (byte)TransmitType.RuterTurnServerID)//收到对方TurnServerID
                        {
                            if (ToRtpTurnServerID == 0)//如果没有收到过对方RtcpTurnServerID
                            {
                                ToRtpTurnServerID = packet.RemoteTurnID;//记录对方TurnServerID

                                if (MyRtpTurnServerID == 0) return;//如果我方还未从服务器获取TurnServerID

                                ToRemoteIP = ServerEP.Address;
                                ToRemoteRtpPort = MyRtpTurnServerID;//我方serverID
                                if (ToRtpTurnServerID > 0 && ToRtcpTurnServerID > 0)
                                    setRemoteTurnServerID(ToRtpTurnServerID, ToRtcpTurnServerID);
                            }
                        }
                        else if (packet.type == (byte)TransmitType.SetToTurnServerID)//设置远程TurnServerID成功
                        {
                            //this.connectedTypeRtp = ConnectedType.UDPServer;
                            //if (connectedTypeRtp != ConnectedType.None && connectedTypeRtcp != ConnectedType.None)
                            //    OnTransmitConnected();
                        }
                        else if (packet.type == (byte)TransmitType.Penetrate)//收到另一客户端请求打洞 
                        {
                            //return;
                            ToRemoteIP = e.RemoteIPEndPoint.Address;
                            ToRemoteRtpPort = e.RemoteIPEndPoint.Port; 

                            for (int i = 0; i < 5; i++)
                            {
                                sockRtpUDP.Send(e.RemoteIPEndPoint, packet.Header);//告之对方收到打洞包并向对方打洞 
                                System.Threading.Thread.Sleep(30);
                            }

                            if (e.RemoteIPEndPoint.Address.ToString() == ToLocalIP.ToString())
                                connectedTypeRtp = ConnectedType.UDPLocal;
                            else
                                connectedTypeRtp = ConnectedType.UDPRemote;
                        }
                        else if (packet.type == (byte)TransmitType.getRemoteEP)//收到自己的远程主机信息 
                        {
                            //设置自己的远程主机信息
                            MyRemoteIP = packet.RemoteIP;
                            MyRemoteRtpPort = packet.Port;
                        }
                    };
            }
            #endregion

            #region sockRtcpUDP
            if (sockRtcpUDP == null)
            {
                sockRtcpUDP = new SockUDP();
                sockRtcpUDP.Listen(0);//随机侦听
                sockRtcpUDP.DataArrival += (sender, e) =>
                {
                    if (e.Data.Length < UdpTurnPacket.HeaderLength) return;
                    UdpTurnPacket packet = new UdpTurnPacket(e.Data);//获得一侦数据的一个包

                    if (packet.type == (byte)TransmitType.GetTurnServerID)//收到 TurnServerID   
                    { 
                        MyRtcpTurnServerID = packet.LocalTurnID;//设置自己TurnServerID为服务器中继端口
                        sockRtcpUDP.Send(new IPEndPoint(ServerEP.Address, MyRtcpTurnServerID), new byte[4]);//向服务器中继端口(TURN SERVER)发送一个握手包
                        System.Threading.Thread.Sleep(20);
                        sockRtcpUDP.Send(new IPEndPoint(ServerEP.Address, MyRtcpTurnServerID), new byte[4]);//向服务器中继端口(TURN SERVER)发送一个握手包
                        System.Threading.Thread.Sleep(20);
                        sockRtcpUDP.Send(new IPEndPoint(ServerEP.Address, MyRtcpTurnServerID), new byte[4]);//向服务器中继端口(TURN SERVER)发送一个握手包

                        packet.ProtocolType = IPProtocolType.Rtcp;
                        packet.type = (byte)TransmitType.RuterTurnServerID;//路由我方serverID
                        packet.RemoteIP = ToRemoteIP;//对方IP
                        packet.Port = ToRemoteRtcpPort;//对方Rtcp端口
                        packet.RemoteTurnID = MyRtcpTurnServerID;//我方TURN服务ID

                        sockRtcpUDP.Send(ServerEP, packet.Header);
                        System.Threading.Thread.Sleep(100);
                        sockRtcpUDP.Send(ServerEP, packet.Header);
                     

                        if (ToRtcpTurnServerID > 0)//如果已经收到对方serverID(最多收到2次包)
                        {
                            ToRemoteIP = ServerEP.Address;
                            ToRemoteRtcpPort = MyRtcpTurnServerID;//我方serverID
                            if (ToRtpTurnServerID > 0 && ToRtcpTurnServerID > 0)
                                setRemoteTurnServerID(ToRtpTurnServerID, ToRtcpTurnServerID);
                        }
                    }
                    else if (packet.type == (byte)TransmitType.RuterTurnServerID)//收到对方TurnServerID
                    {
                        if (ToRtcpTurnServerID == 0)//如果没有收到过对方RtcpTurnServerID
                        {
                            ToRtcpTurnServerID = packet.RemoteTurnID;//记录对方TurnServerID

                            if (MyRtcpTurnServerID == 0) return;//如果我方还未从服务器获取TurnServerID

                            ToRemoteIP = ServerEP.Address;
                            ToRemoteRtcpPort = MyRtcpTurnServerID;//我方serverID
                            if (ToRtpTurnServerID > 0 && ToRtcpTurnServerID > 0)
                                setRemoteTurnServerID(ToRtpTurnServerID, ToRtcpTurnServerID);
                        }
                    }
                    else if (packet.type == (byte)TransmitType.SetToTurnServerID)//设置远程TurnServerID成功
                    {
                        //this.connectedTypeRtcp = ConnectedType.UDPServer;
                        //if (connectedTypeRtp != ConnectedType.None && connectedTypeRtcp != ConnectedType.None)
                        //    OnTransmitConnected();
                    }
                    else if (packet.type == (byte)TransmitType.Penetrate)//收到另一客户端请求打洞 
                    {
                        ToRemoteIP = e.RemoteIPEndPoint.Address;
                        ToRemoteRtcpPort = e.RemoteIPEndPoint.Port;

                        for (int i = 0; i < 5; i++)
                        {
                            sockRtcpUDP.Send(e.RemoteIPEndPoint, packet.Header);//告之对方收到打洞包并向对方打洞 
                            System.Threading.Thread.Sleep(30);
                        }

                        if (e.RemoteIPEndPoint.Address.ToString() == ToLocalIP.ToString())
                            connectedTypeRtcp = ConnectedType.UDPLocal;
                        else
                            connectedTypeRtcp = ConnectedType.UDPRemote;
                    }
                    else if (packet.type == (byte)TransmitType.getRemoteEP)//收到自己的远程主机信息 
                    {
                        //设置自己的远程主机信息
                        MyRemoteIP = packet.RemoteIP;
                        MyRemoteRtcpPort = packet.Port;
                    }
                };
            }
            #endregion

            #region 获得本机局域网IPV4地址和侦听端口
            System.Net.IPAddress[] ips = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());
            for (int i = ips.Length - 1; i >= 0; i--)
            {
                if (ips[i].GetAddressBytes().Length == 4)
                {
                    //获得本机局域网地址
                    MyLocalIP = ips[i];
                    MyLocalRtpPort = sockRtpUDP.ListenPort;
                    MyLocalRtcpPort = sockRtcpUDP.ListenPort;
                    break;
                }
            }
            #endregion

            UdpTurnPacket p = new UdpTurnPacket();
            p.type = (byte)TransmitType.getRemoteEP;//获得公网主机信息
            sockRtpUDP.Send(ServerEP, p.Header);//获得公网Rtp主机信息
            sockRtcpUDP.Send(ServerEP, p.Header);//获得公网Rtcp主机信息

            #region 主机信息获取 时间计数器
            int TimeCount = 0;
            System.Timers.Timer timer1 = new System.Timers.Timer();
            timer1.Interval = 500;
            timer1.Enabled = true;
            timer1.Elapsed += (s, e) =>
            {
                if (MyRemoteRtpPort == 0 && TimeCount < 10)//如果还未获得本机远程主机信息
                {
                    sockRtpUDP.Send(ServerEP, p.Header);//每隔500毫秒发送一次请求
                }
                if (MyRemoteRtcpPort == 0 && TimeCount < 10)//如果还未获得本机远程主机信息
                {
                    sockRtcpUDP.Send(ServerEP, p.Header);//每隔500毫秒发送一次请求
                }
                if (MyRemoteIP != null && MyRemoteRtpPort > 0 && MyRemoteRtcpPort > 0)
                {
                    //终止发送，并触发获得主机事件
                    timer1.Enabled = false;
                    timer1.Close();
                    timer1.Dispose(); 
                    timer1 = null;
                    OnGetIPEndPoint(this, new RtpEventArgs(MyLocalIP, MyRemoteIP, MyLocalRtpPort, MyLocalRtcpPort, MyRemoteRtpPort, MyRemoteRtcpPort));
                    return;
                }

                if (TimeCount > 10)//如果三秒内已经获得主机信息或三秒钟后未获得主机信息
                {
                    //终止发送，并触发获得主机事件
                    timer1.Enabled = false;
                    timer1.Close();
                    timer1.Dispose();
                    timer1 = null;
                    if (MyRemoteIP == null)//如果连接服务器超时 
                    {
                        if (ConnectOutTime != null)
                            ConnectOutTime(this, ConnectedType.None);
                        return;
                    }
                }
                TimeCount++;
            };
            #endregion
        }
        #endregion

        #region 设置远程主机信息(实现任何类型的NAT穿越)
        /// <summary>
        /// 设置远程主机信息(实现任何类型的NAT穿越)
        /// </summary>
        /// <param name="toLocalIP"></param>
        /// <param name="toRemoteIP"></param>
        /// <param name="toLocalRtpPort"></param>
        /// <param name="toLocalRtcpPort"></param>
        /// <param name="toRemoteRtpPort"></param>
        /// <param name="toRemoteRtcpPort"></param>
        public void setRemoteIP(IPAddress toLocalIP, IPAddress toRemoteIP, int toLocalRtpPort, int toLocalRtcpPort, int toRemoteRtpPort, int toRemoteRtcpPort)
        {
            this.ToLocalIP = toLocalIP;
            this.ToRemoteIP = toRemoteIP;
            this.ToLocalRtpPort = toLocalRtpPort;
            this.ToLocalRtcpPort = toLocalRtcpPort;
            this.ToRemoteRtpPort = toRemoteRtpPort;
            this.ToRemoteRtcpPort = toRemoteRtcpPort; 

            #region 网络互联打洞 时间计数器(以下代码实现NAT穿越)
            int TimeCount = 0;
            System.Timers.Timer timer1 = new System.Timers.Timer();
            timer1.Interval = 500;
            timer1.Enabled = true;

            timer1.Elapsed += (sender, e) =>
            {
                TimeCount++;
                UdpTurnPacket packet = new UdpTurnPacket();
                packet.type = (byte)TransmitType.Penetrate;//打洞数据包
                //packet.type = (byte)TransmitType.over;//打洞数据包
                //packet.Payload = new byte[1400];
                byte[] data = packet.ToBytes();
                if (connectedTypeRtp == ConnectedType.None && TimeCount <=4)//如果2秒后还未与对方建立RTP联接
                {
                    if (sockRtpUDP != null && sockRtpUDP.Listened)
                    {
                        sockRtpUDP.Send(new IPEndPoint(ToLocalIP, ToLocalRtpPort), data);//发送一次Rtp局域网正常打洞请求
                        System.Threading.Thread.Sleep(100);
                        sockRtpUDP.Send(new IPEndPoint(ToLocalIP, ToLocalRtpPort), data);//发送一次Rtp局域网正常打洞请求
                    }
                }
                else if (connectedTypeRtcp == ConnectedType.None && TimeCount <= 4)//如果2秒后还未与对方建立RTP联接
                {
                    if (sockRtcpUDP != null && sockRtcpUDP.Listened)
                    {
                        sockRtcpUDP.Send(new IPEndPoint(ToLocalIP, ToLocalRtcpPort), data);//发送一次Rtcp局域网正常打洞请求
                        System.Threading.Thread.Sleep(100);
                        sockRtcpUDP.Send(new IPEndPoint(ToLocalIP, ToLocalRtcpPort), data);//发送一次Rtcp局域网正常打洞请求
                    }
                }
                else if (connectedTypeRtp == ConnectedType.None && TimeCount > 4 && TimeCount <= 16)//如果8秒后还未与对方建立RTCP联接
                {
                    if (sockRtpUDP != null && sockRtpUDP.Listened)
                    {
                        sockRtpUDP.Send(new IPEndPoint(ToRemoteIP, ToRemoteRtpPort), data);//发送一次广域网打洞请求 
                        System.Threading.Thread.Sleep(100);
                        sockRtpUDP.Send(new IPEndPoint(ToRemoteIP, ToRemoteRtpPort), data);//发送一次广域网打洞请求
                    }
                }
                else if (connectedTypeRtcp == ConnectedType.None && TimeCount > 4 && TimeCount <= 16)//如果8秒后还未与对方建立RTCP联接
                {
                    if (sockRtcpUDP != null && sockRtcpUDP.Listened)
                    {
                        sockRtcpUDP.Send(new IPEndPoint(ToRemoteIP, ToRemoteRtcpPort), data);//发送一次广域网打洞请求 
                        System.Threading.Thread.Sleep(100);
                        sockRtcpUDP.Send(new IPEndPoint(ToRemoteIP, ToRemoteRtcpPort), data);//发送一次广域网打洞请求 
                    }
                }
                else if (connectedTypeRtp != ConnectedType.None && connectedTypeRtcp != ConnectedType.None)
                {
                    //终止发送，并触发获得主机事件
                    timer1.Enabled = false;
                    timer1.Dispose();
                    timer1 = null;
                    OnTransmitConnected();//触发连接建立事件
                }
                else if (TimeCount >20)//如果如果10秒后NAT穿越不成功，那么申请TURN服务，实现NAT穿越
                {
                    //终止发送，并触发获得主机事件
                    timer1.Enabled = false;
                    timer1.Close();
                    timer1.Dispose();
                    timer1 = null;

                    UdpTurnPacket p = new UdpTurnPacket();
                    p.type = (byte)TransmitType.GetTurnServerID;//申请TURN服务，实现对称NAT穿越

                    p.ProtocolType = IPProtocolType.Rtcp;
                    sockRtcpUDP.Send(ServerEP, p.Header);

                    p.ProtocolType = IPProtocolType.Rtp;
                    sockRtpUDP.Send(ServerEP, p.Header);
                } 
            };
            #endregion
        }
        #endregion 
 
        #region 设置对端Turn ServerID
        /// <summary>
        /// 设置对端Turn ServerID
        /// </summary>
        /// <param name="toRtpTurnServerID"></param>
        /// <param name="toRtcpTurnServerID"></param>
        private  void setRemoteTurnServerID(int toRtpTurnServerID, int toRtcpTurnServerID)
        {
            int TimeCount = 0;
            System.Timers.Timer timer1 = new System.Timers.Timer();
            timer1.Interval = 500;
            timer1.Enabled = true;
            timer1.Elapsed += (s, e) =>
            {
                TimeCount++;
                if (MyRtpTurnServerID > 0 && MyRtcpTurnServerID >0)
                {
                    timer1.Enabled = false;
                    timer1.Close();
                    timer1.Dispose();
                    timer1 = null;

                    UdpTurnPacket RtpPacket = new UdpTurnPacket();
                    RtpPacket.type = (byte)TransmitType.SetToTurnServerID;
                    RtpPacket.LocalTurnID = MyRtpTurnServerID;
                    RtpPacket.RemoteTurnID = toRtpTurnServerID;
                    sockRtpUDP.Send(ServerEP, RtpPacket.Header);//向服务器注册对端 RTP TurnServerID
                    System.Threading.Thread.Sleep(50);
                    sockRtpUDP.Send(ServerEP, RtpPacket.Header);//向服务器注册对端 RTP TurnServerID
                    System.Threading.Thread.Sleep(50);
                    sockRtpUDP.Send(ServerEP, RtpPacket.Header);//向服务器注册对端 RTP TurnServerID

                    UdpTurnPacket RtcpPacket = new UdpTurnPacket();
                    RtcpPacket.type = (byte)TransmitType.SetToTurnServerID;
                    RtcpPacket.LocalTurnID = MyRtcpTurnServerID;
                    RtcpPacket.RemoteTurnID = toRtcpTurnServerID;
                    sockRtcpUDP.Send(ServerEP, RtcpPacket.Header);//向服务器注册对端 RTCP TurnServerID
                    System.Threading.Thread.Sleep(50);
                    sockRtcpUDP.Send(ServerEP, RtcpPacket.Header);//向服务器注册对端 RTCP TurnServerID
                    System.Threading.Thread.Sleep(50);
                    sockRtcpUDP.Send(ServerEP, RtcpPacket.Header);//向服务器注册对端 RTCP TurnServerID


                    this.connectedTypeRtp = ConnectedType.UDPServer;
                    this.connectedTypeRtcp = ConnectedType.UDPServer;
                    OnTransmitConnected();
                }
                else if (TimeCount > 10)
                {
                    timer1.Enabled = false;
                    timer1.Close();
                    timer1.Dispose();
                    timer1 = null;
                    //OnTransmitConnected();
                }
            };
           
        }
        #endregion
         
        #region 发送一侦侦数据
        public void sendFrame(byte[] data)
        {
            try
            {
                if (rtpSession != null && rtpSession.Connected)
                    rtpSession.SendRtpFrame(new Rtp.RtpFrame(data));
            }
            catch { }
        }
        #endregion

        #region 发送一侦图像数据
        public void sendJpegFrame(Rtp.JpegFrame p)
        {
            try
            {
                if (rtpSession != null && rtpSession.Connected)
                    rtpSession.SendRtpFrame(p);
            }
            catch { }
        }
        #endregion

        #region 发送一个RTCP APP包 
        /// <summary>
        /// 发送一个RTCP APP包
        /// </summary>
        /// <param name="rtcpPacket"></param>
        public void sendRtcpPack(Rtcp.RtcpPacket  rtcpPacket)
        {
            try
            {
                if (rtpSession != null && rtpSession.Connected)
                    rtpSession.SendRtcpPacket(rtcpPacket);
            }
            catch { }
        }
        #endregion

        #endregion
    }

    #region RTP通信事件参数
    /// <summary>
    /// RTP通信参数
    /// </summary>
    public class RtpEventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="localIP"></param>
        /// <param name="remoteIP"></param>
        /// <param name="localRtpPort"></param>
        /// <param name="localRtcpPort"></param>
        /// <param name="remoteRtpPort"></param>
        /// <param name="remoteRtcpPort"></param>
        public RtpEventArgs(IPAddress localIP, IPAddress remoteIP, int localRtpPort, int localRtcpPort, int remoteRtpPort, int remoteRtcpPort)
        {
            LocalIP = localIP;
            RemoteIP = remoteIP;
            LocalRtpPort = localRtpPort;
            LocalRtcpPort = localRtcpPort;
            RemoteRtpPort = remoteRtpPort;
            RemoteRtcpPort = remoteRtcpPort;
        }

        /// <summary>
        /// 对端局域网IP
        /// </summary>
        public IPAddress LocalIP { get; set; }

        /// <summary>
        /// 对端远程IP(公网)
        /// </summary>
        public IPAddress RemoteIP { get; set; }

        /// <summary>
        /// 对端局域网Rtp端口
        /// </summary>
        public int LocalRtpPort { get; set; }

        /// <summary>
        /// 对端局域网Rtcp端口
        /// </summary>
        public int LocalRtcpPort { get; set; }

        /// <summary>
        /// 对端远程Rtp端口(公网)
        /// </summary>
        public int RemoteRtpPort { get; set; }

        /// <summary>
        /// 对端远程Rtcp端口(公网)
        /// </summary>
        public int RemoteRtcpPort { get; set; }
    }
    #endregion
}
