using System;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace IMLibrary4.Audio
{
	/// <summary>
	/// WaveOut 的摘要说明。
	/// </summary>
	/// 
	public class SoundPlayer
	{
		[DllImport("Winmm.dll")]
		static extern bool  sndPlaySound(string wfname, int fuSound);
		public SoundPlayer()
		{
		}

		public void Play(string file)
		{

			bool b=sndPlaySound(file,1);
		}

        [DllImport("winmm.dll")]
         static extern int sndPlaySoundA(byte[] lpszSoundName, int uFlags);
        public void play(System.IO.Stream stream)
        {
            byte[] buf = new byte[stream.Length];
            int SND_MEMORY = 0x4;
            stream.Read(buf, 0, buf.Length);
            stream.Close();
            sndPlaySoundA(buf, SND_MEMORY);
        }
	}

	public class WaveOut
	{
		bool m_running;
		int m_outdex;
		IntPtr m_out;
		waveProc m_outcb;
		WAVEFORMATEX m_fmt;
		WAVEHDR[] m_hs;
		GCHandle[] gchs;
		int m_pos=0;
		int m_buffersize;
		public event WaveErrorEventHandler WaveOutError;
		public WaveOut(int index,WAVEFORMATEX format,int buffersize,int buffer_count)//波形输出
		{
			this.m_fmt=format;
			this.m_outdex=index;
			m_buffersize=buffersize;
			m_outcb=new waveProc(this.waveOutProc);
            //Debug.Assert(buffer_count>0 && buffer_count<65535);
			this.m_hs=new WAVEHDR[buffer_count];
			this.gchs=new GCHandle[buffer_count];
			for(int i=0;i<buffer_count;i++)
			{
				gchs[i]=GCHandle.Alloc(new byte[buffersize],GCHandleType.Pinned);
				WAVEHDR hdr=new WAVEHDR();
				hdr.dwBufferLength=buffersize;
				hdr.lpData=this.gchs[i].AddrOfPinnedObject();
				hdr.dwUser=i;
				this.m_hs[i]=hdr;
			}
			CheckError(waveOutOpen(ref m_out,m_outdex,ref m_fmt,m_outcb,0,0x00030000));
		}
		~WaveOut()//波形输出
		 {
			 for(int i=0;i<this.m_hs.Length;i++)
			 {
				 //	Marshal.FreeCoTaskMem(this.m_hs[i].lpData);
				 this.gchs[i].Free();
			 }
			if(this.m_out!=IntPtr.Zero)
			{
				try
				{
					CheckError(waveOutClose(this.m_out));
				}
				catch
				{
				}
			}
		 }
		public WAVEFORMATEX WAVEFORMATEX
		{
			get{return this.m_fmt;}
		}
		public void Start()
		{
			if(this.m_running)throw(new AVException("正在播放"));
			m_running=true;
		}
		public void Reset()
		{
			this.m_pos=0;
			CheckError(waveOutReset(this.m_out));
			m_running=false;
		}
		public void Stop()
		{
			m_running=false;
		//	this.m_pos=0;
		}
		private void waveOutProc(IntPtr hwi,int uMsg,int dwInstance,ref WAVEHDR hdr,int dwParam2)
		{
			switch(uMsg)
			{
				case WOM_OPEN:
					break;
				case WOM_DONE:
					try
					{
						this.CheckError(waveOutUnprepareHeader(m_out,ref hdr,Marshal.SizeOf(typeof(WAVEHDR))));
						m_pos=hdr.dwUser;
						hdr.dwBytesRecorded=0;
					}
					catch(AVException e)
					{
						m_running=false;
						if(this.WaveOutError!=null)this.WaveOutError(this,e);
					}
					break;
				case WOM_CLOSE:
					break;
			}
		}

		public void Write(byte[] data)
		{
            try
            {
                if (!this.m_running) return;
                for (int i = this.m_pos; i < this.m_hs.Length + this.m_pos; i++)
                {
                    int newpos = i % this.m_hs.Length;
                    if (this.m_hs[newpos].dwBytesRecorded == 0)
                    {
                        int cnt = data.Length > this.m_buffersize ? this.m_buffersize : data.Length;
                        this.m_hs[newpos].dwBytesRecorded = cnt;
                        Marshal.Copy(data, 0, this.m_hs[newpos].lpData, cnt);
                        CheckError(waveOutPrepareHeader(m_out, ref this.m_hs[newpos], Marshal.SizeOf(typeof(WAVEHDR))));
                        CheckError((waveOutWrite(m_out, ref this.m_hs[newpos], System.Runtime.InteropServices.Marshal.SizeOf(typeof(WAVEHDR)))));
                        break;
                    }
                }
            }
            catch { }
		}

		public void CheckError(int result)
		{
			if(result!=0)
			{
				this.m_pos=0;
				StringBuilder sb=new StringBuilder(128);
				waveOutGetErrorTextA(result,sb,128);
				throw(new AVException(sb.ToString(),result));
			}
		}




		#region
		private const int MM_WOM_OPEN         =0x3BB;           /* waveform output */
		private const int MM_WOM_CLOSE        =0x3BC;
		private const int MM_WOM_DONE         =0x3BD;
		private const int MM_MOM_OPEN         =0x3C7;           /* MIDI output */
		private const int MM_MOM_CLOSE        =0x3C8;
		private const int MM_MOM_DONE         =0x3C9;

		private const int WOM_OPEN        =MM_WOM_OPEN;
		private const int WOM_CLOSE       =MM_WOM_CLOSE;
		private const int WOM_DONE        =MM_WOM_DONE;

		private const int WAVE_INVALIDFORMAT     =0x00000000;       /* invalid format */


		[DllImport("winmm.dll")]
		private extern static int waveInGetDevCapsA(int uDeviceID,ref WAVEINCAPSA pwic,int cbwic);
		[DllImport("winmm.dll")]
		private extern static int waveInGetDevCapsW(int uDeviceID,ref WAVEINCAPSW pwic,int cbwic);


		[DllImport("winmm.dll")]
		private extern static int waveInOpen(ref IntPtr phwi, int uDeviceID,
			ref WAVEFORMATEX pwfx,waveProc dwCallback, int dwInstance, int fdwOpen);

		[DllImport("winmm.dll")]
		private extern static int waveOutGetDevCapsA(int uDeviceID,ref WAVEOUTCAPSA pwoc,int cbwoc);
		[DllImport("winmm.dll")]
		private extern static int waveOutGetDevCapsW(int uDeviceID,ref WAVEOUTCAPSW pwoc,int cbwoc);
		[DllImport("winmm.dll")]
		private extern static int waveOutGetVolume(IntPtr hwo,ref int pdwVolume);
		[DllImport("winmm.dll")]
		private extern static int waveOutSetVolume(IntPtr hwo,int dwVolume);

		[DllImport("winmm.dll")]
		private extern static int waveOutOpen(ref IntPtr phwo,int uDeviceID,
			ref WAVEFORMATEX pwfx, waveProc dwCallback,int dwInstance, int fdwOpen);
		[DllImport("winmm.dll")]
		private extern static int waveOutClose(IntPtr hwo);
		[DllImport("winmm.dll")]
		private extern static int waveOutPrepareHeader(IntPtr hwo,ref WAVEHDR pwh,int cbwh);
		[DllImport("winmm.dll")]
		private extern static int waveOutUnprepareHeader(IntPtr hwo,ref WAVEHDR pwh,int cbwh);
		[DllImport("winmm.dll")]
		private extern static int waveOutWrite(IntPtr hwo,ref WAVEHDR pwh,int cbwh);
		[DllImport("winmm.dll")]
		private extern static int waveOutPause(IntPtr hwo);
		[DllImport("winmm.dll")]
		private extern static int waveOutRestart(IntPtr hwo);
		[DllImport("winmm.dll")]
		private extern static int waveOutReset(IntPtr hwo);
		[DllImport("winmm.dll")]
		private extern static int waveOutGetErrorTextA(int mmrError,StringBuilder pszText,int cchText);
		[DllImport("winmm.dll")]
		private extern static int  waveOutGetErrorTextW(int mmrError,StringBuilder pszText,int cchText);
		[DllImport("winmm.dll")]
		private extern static int waveOutGetNumDevs(); 
		#endregion
	}

    #region wave
    [StructLayout(LayoutKind.Sequential)]
    public struct WAVEHDR
    {
        public IntPtr lpData;                 /* pointer to locked data buffer */
        public int dwBufferLength;         /* length of data buffer */
        public int dwBytesRecorded;        /* used for input only */
        public int dwUser;                 /* for client's use */
        public int dwFlags;                /* assorted flags (see defines) */
        public int dwLoops;                /* loop control counter */
        public int lpNext;     /* reserved for driver */
        public int reserved;               /* reserved for driver */
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct WAVEFORMATEX
    {
        public short wFormatTag;
        public short nChannels;
        public int nSamplesPerSec;
        public int nAvgBytesPerSec;
        public short nBlockAlign;
        public short wBitsPerSample;
        public short cbSize;
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct PCMWAVEFORMAT
    {
        public WAVEFORMAT wf;
        public short wBitsPerSample;
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct WAVEOUTCAPSA
    {
        public short wMid;                  /* manufacturer ID */
        public short wPid;                  /* product ID */
        public int vDriverVersion;      /* version of the driver */
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string szPname;   /* product name (NULL terminated string) */
        public int dwFormats;             /* formats supported */
        public short wChannels;             /* number of sources supported */
        public short wReserved1;            /* packing */
        public int dwSupport;             /* functionality supported by driver */
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct WAVEOUTCAPSW
    {
        public short wMid;                  /* manufacturer ID */
        public short wPid;                  /* product ID */
        public int vDriverVersion;      /* version of the driver */
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string szPname;   /* product name (NULL terminated string) */
        public int dwFormats;             /* formats supported */
        public short wChannels;             /* number of sources supported */
        public short wReserved1;            /* packing */
        public int dwSupport;             /* functionality supported by driver */
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct WAVEINCAPSW
    {
        public short wMid;                    /* manufacturer ID */
        public short wPid;                    /* product ID */
        public uint vDriverVersion;        /* version of the driver */
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string szPname;    /* product name (NULL terminated string) */
        public int dwFormats;               /* formats supported */
        public short wChannels;               /* number of channels supported */
        public short wReserved1;              /* structure packing */
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct WAVEINCAPSA
    {
        public short wMid;                    /* manufacturer ID */
        public short wPid;                    /* product ID */
        public int vDriverVersion;        /* version of the driver */
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string szPname;    /* product name (NULL terminated string) */
        public int dwFormats;               /* formats supported */
        public short wChannels;               /* number of channels supported */
        public short wReserved1;              /* structure packing */
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct WAVEFORMAT
    {
        public short wFormatTag;        /* format type */
        public short nChannels;         /* number of channels (i.e. mono, stereo, etc.) */
        public int nSamplesPerSec;    /* sample rate */
        public int nAvgBytesPerSec;   /* for buffer estimation */
        public short nBlockAlign;       /* block size of data */
    }
    public delegate void waveProc(IntPtr hwi, int uMsg, int dwInstance, ref WAVEHDR hdr, int dwParam2);

    #endregion

    #region mixer

    [StructLayout(LayoutKind.Sequential)]
    public struct MIXERCAPS
    {
        public short wMid;
        public short wPid;
        public int vDriverVersion;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string szPname;
        public int fdwSupport;
        public int cDestinations;
    }

    [StructLayout(LayoutKind.Explicit, Size = 148)]
    public struct MIXERCONTROL
    {
        [FieldOffset(0)]
        public int cbStruct;
        [FieldOffset(4)]
        public int dwControlID;
        [FieldOffset(8)]
        public int dwControlType;
        [FieldOffset(12)]
        public int fdwControl;
        [FieldOffset(16)]
        public int cMultipleItems;
        [FieldOffset(20)]
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
        string szShortName;
        [FieldOffset(36)]
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        string szName;
        [FieldOffset(100)]
        public Volume Bounds;
        [FieldOffset(124)]
        public int Metrics;
    }

    public struct Volume
    {
        public int dwMinimum;
        public int dwMaximum;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MIXERCONTROLDETAILS
    {
        public int cbStruct;
        public int dwControlID;
        public int cChannels;
        public int cMultipleItems;
        public int cbDetails;
        public int paDetails;
    }

    [StructLayout(LayoutKind.Sequential)]
    public class MIXERCONTROLDETAILS_BOOLEAN
    {
        public bool fValue;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MIXERCONTROLDETAILS_LISTTEXT
    {
        public int dwParam1;
        public int dwParam2;
        public string szName;
    }

    [StructLayout(LayoutKind.Sequential)]
    public class MIXERCONTROLDETAILS_SIGNED
    {
        public int lValue;
    }

    [StructLayout(LayoutKind.Sequential)]
    public class MIXERCONTROLDETAILS_UNSIGNED
    {
        public int dwValue;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MIXERLINE
    {
        public int cbStruct;
        public uint dwDestination;
        public uint dwSource;
        public uint dwLineID;
        public int fdwLine;
        public int dwUser;
        public int dwComponentType;
        public int cChannels;
        public int cConnections;
        public int cControls;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
        public string szShortName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string szName;
        public Target tTarget;

    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Target
    {
        public int dwType;
        public int dwDeviceID;
        public short wMid;
        public short wPid;
        public int vDriverVersion;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string szPname;

    }


    [StructLayout(LayoutKind.Sequential)]
    public struct MIXERLINECONTROLS
    {
        public int cbStruct;
        public uint dwLineID;
        //union 
        //{ 
        //	DWORD dwControlID; 
        //	DWORD dwControlType; 
        //}; 
        public int dwControlType;
        public int cControls;
        public int cbmxctrl;
        public IntPtr pamxctrl;
    }
    #endregion

    #region AV异常参数类
    /// <summary>
    /// AV异常参数类
    /// </summary>
    public class AVException : System.Exception
    {
        int m_num = 0;
        public AVException() : base() { }
        public AVException(int er) : base() { this.m_num = er; }
        public AVException(string message) : base(message) { }
        public AVException(string message, int er) : base(message) { this.m_num = er; }
        //public AVException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public int ErrorNumber
        {
            get { return m_num; }
        }
    }
    #endregion


}
