// VP8.NET.cpp : Defines the exported functions for the DLL application.
//

#pragma once

#include "stdafx.h"

using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Imaging;
using namespace System::Runtime::InteropServices;

#define VPX_CODEC_DISABLE_COMPAT 1

#include <vpx/vpx_encoder.h>
#include <vpx/vp8cx.h>
#define vp8cx (vpx_codec_vp8_cx())

#include <vpx/vpx_decoder.h>
#include <vpx/vp8dx.h>
#define vp8dx (vpx_codec_vp8_dx())

namespace Coder
{
    public ref class Converter
    {
    public:
        static void ConvertRGB24ToI420(unsigned char *rgbData, vpx_image_t *yuvImage)
        {
            int rgb24Stride = yuvImage->d_w * 3;

            int rgb1 = 0;
            int yi1 = 0;
            int uvi = 0;
            for (unsigned int ypos = 0; ypos < yuvImage->d_h; ypos += 2)
            {
                for (unsigned int xpos = 0; xpos < yuvImage->d_w; xpos += 2)
                {
                    int rgb2 = rgb1 + 3;
                    int rgb3 = rgb24Stride + rgb1;
                    int rgb4 = rgb24Stride + rgb2;
					
					int ro, go, bo;
					if (BitConverter::IsLittleEndian)
					{
						bo = 0;
						go = 1;
						ro = 2;
					}
					else
					{
						ro = 0;
						go = 1;
						bo = 2;
					}

                    int r1 = rgbData[rgb1+ro];
                    int g1 = rgbData[rgb1+go];
                    int b1 = rgbData[rgb1+bo];
                    int r2 = rgbData[rgb2+ro];
                    int g2 = rgbData[rgb2+go];
                    int b2 = rgbData[rgb2+bo];
                    int r3 = rgbData[rgb3+ro];
                    int g3 = rgbData[rgb3+go];
                    int b3 = rgbData[rgb3+bo];
                    int r4 = rgbData[rgb4+ro];
                    int g4 = rgbData[rgb4+go];
                    int b4 = rgbData[rgb4+bo];

                    int yi2 = yi1 + 1;
                    int yi3 = yuvImage->d_w + yi1;
                    int yi4 = yuvImage->d_w + yi2;

                    yuvImage->planes[VPX_PLANE_Y][yi1] = (unsigned char) getY(r1, g1, b1);
                    yuvImage->planes[VPX_PLANE_Y][yi2] = (unsigned char) getY(r2, g2, b2);
                    yuvImage->planes[VPX_PLANE_Y][yi3] = (unsigned char) getY(r3, g3, b3);
                    yuvImage->planes[VPX_PLANE_Y][yi4] = (unsigned char) getY(r4, g4, b4);

                    int u1 = getU(r1, g1, b1);
                    int u2 = getU(r2, g2, b2);
                    int u3 = getU(r3, g3, b3);
                    int u4 = getU(r4, g4, b4);

                    int v1 = getV(r1, g1, b1);
                    int v2 = getV(r2, g2, b2);
                    int v3 = getV(r3, g3, b3);
                    int v4 = getV(r4, g4, b4);
                    
                    yuvImage->planes[VPX_PLANE_U][uvi] = (unsigned char) ((u1 + u2 + u3 + u4) / 4);
                    yuvImage->planes[VPX_PLANE_V][uvi] = (unsigned char) ((v1 + v2 + v3 + v4) / 4);

                    rgb1 += 6;
                    yi1 += 2;
                    uvi++;
                }

                rgb1 += rgb24Stride;
                yi1 += yuvImage->d_w;
            }
        }
        
        static void ConvertI420ToRGB24(vpx_image_t *yuvImage, unsigned char *rgbData)
        {
            int yPadding = yuvImage->stride[VPX_PLANE_Y] - yuvImage->d_w;
            int uPadding = yuvImage->stride[VPX_PLANE_U] - (yuvImage->d_w / 2);
            int vPadding = yuvImage->stride[VPX_PLANE_V] - (yuvImage->d_w / 2);

            int yi = 0;
            int ui = 0;
            int vi = 0;
            int rgbi = 0;
            for (unsigned int ypos = 0; ypos < yuvImage->d_h; ypos++)
            {
                for (unsigned int xpos = 0; xpos < yuvImage->d_w; xpos++)
                {
                    int y = yuvImage->planes[VPX_PLANE_Y][yi] & 0xFF;
                    int u = yuvImage->planes[VPX_PLANE_U][ui] & 0xFF;
                    int v = yuvImage->planes[VPX_PLANE_V][vi] & 0xFF;
                
                    int r = getR(y, u, v);
                    int g = getG(y, u, v);
                    int b = getB(y, u, v);
					
					if (BitConverter::IsLittleEndian)
					{
						rgbData[rgbi++] = (unsigned char) b;
						rgbData[rgbi++] = (unsigned char) g;
						rgbData[rgbi++] = (unsigned char) r;
					}
					else
					{
						rgbData[rgbi++] = (unsigned char) r;
						rgbData[rgbi++] = (unsigned char) g;
						rgbData[rgbi++] = (unsigned char) b;
					}

                    yi++;
                    if (xpos % 2 == 1)
                    {
                        ui++;
                        vi++;
                    }
                }
            
                yi += yPadding;
            
                if (ypos % 2 == 0)
                {
                    ui -= (yuvImage->d_w / 2);
                    vi -= (yuvImage->d_w / 2);
                }
                else
                {
                    ui += uPadding;
                    vi += vPadding;
                }
            }
        }

    private:
        static int getR(int y, int u, int v)
        {
            return clamp(y + (int) (1.402f * (v - 128)));
        }
    
        static int getG(int y, int u, int v)
        {
            return clamp(y - (int) (0.344f * (u - 128) + 0.714f * (v - 128)));
        }
    
        static int getB(int y, int u, int v)
        {
            return clamp(y + (int) (1.772f * (u - 128)));
        }
        
        static int getY(int r, int g, int b)
        {
            return (int) (0.299f * r + 0.587f * g + 0.114f * b);
        }
    
        static int getU(int r, int g, int b)
        {
            return (int) (-0.169f * r - 0.331f * g + 0.499f * b + 128);
        }
    
        static int getV(int r, int g, int b)
        {
            return (int) (0.499f * r - 0.418f * g - 0.0813f * b + 128);
        }
    
        static int clamp(int value)
        {
            return Math::Min(Math::Max(value, 0), 255);
        }
    };

    public ref class Encoder
    {
    private:
        vpx_codec_ctx_t      *codec;
        vpx_image_t          *img;
        int                   frame_cnt;
		vpx_enc_frame_flags_t flag;
		bool				  destroyed;

    public:
		bool DebugMode;

        Encoder(int width, int height, int fps)
        {
            // allocate image
            img = vpx_img_alloc(NULL, VPX_IMG_FMT_I420, width, height, 0);

            // define configuration options
            frame_cnt = 0;
            vpx_codec_enc_cfg_t cfg;
            vpx_codec_err_t res = vpx_codec_enc_config_default(vp8cx, &cfg, 0);
            if (res)
            {
                System::Console::WriteLine("Could not get encoder config. {0}", gcnew String(vpx_codec_err_to_string(res)));
                throw 0;
            }
            cfg.g_timebase.num = 1;
            cfg.g_timebase.den = fps;
			cfg.rc_target_bitrate = width * height * 256 / 320 / 240;
			cfg.rc_end_usage = VPX_CBR;
            cfg.g_w = width;
            cfg.g_h = height;
			cfg.kf_mode = VPX_KF_AUTO;
			cfg.kf_min_dist = cfg.kf_max_dist = fps; // keyframe 1x / second
			cfg.g_error_resilient = 1;
			cfg.g_lag_in_frames = 0;
			cfg.g_pass = VPX_RC_ONE_PASS;
			cfg.rc_min_quantizer = 0;
			cfg.rc_max_quantizer = 63;
			cfg.g_profile = 0;

            // initialize encoder
            codec = new vpx_codec_ctx_t();
            res = vpx_codec_enc_init(codec, vp8cx, &cfg, 0);
            if (res)
            {
                System::Console::WriteLine("Could not initialize encoder. {0}", gcnew String(vpx_codec_err_to_string(res)));
                throw 0;
            }
        }

        !Encoder()
        {
			if (!destroyed)
			{
				destroyed = true;
				vpx_img_free(img);
				vpx_codec_destroy(codec);
				delete codec;
			}
        }

        ~Encoder()
        {
            this->!Encoder();
        }

        array<Byte>^ Encode(Bitmap^ frame)
        {
            // lock system memory
            BitmapData^ frameData = frame->LockBits(System::Drawing::Rectangle(Point::Empty, frame->Size), ImageLockMode::ReadOnly, frame->PixelFormat);
            try
            {
				// convert RGB to I420
				Converter::ConvertRGB24ToI420((unsigned char*)frameData->Scan0.ToPointer(), img);
            }
            finally
            {
                frame->UnlockBits(frameData);
            }

            // encode
            vpx_codec_err_t res = vpx_codec_encode(codec, img, frame_cnt, 1, flag, VPX_DL_REALTIME);
            if (res)
            {
				if (DebugMode)
				{
					System::Console::WriteLine("Could not encode frame. {0}", gcnew String(vpx_codec_err_to_string(res)));
				}
				return nullptr;
            }
			flag = 0;

            frame_cnt++;
            
            // get frame
            vpx_codec_iter_t iter = NULL;
            const vpx_codec_cx_pkt_t *pkt;
            while (pkt = vpx_codec_get_cx_data(codec, &iter))
            {
                if (pkt->kind == VPX_CODEC_CX_FRAME_PKT)
                {
                    // copy unmanaged buffer to managed buffer
                    array<Byte>^ result = gcnew array<Byte>((int)pkt->data.frame.sz);
                    Marshal::Copy((IntPtr)pkt->data.frame.buf, result, 0, (int)pkt->data.frame.sz);
                    return result;
                }
            }
			
			if (DebugMode)
			{
				System::Console::WriteLine("Could not encode frame.");
			}
			return nullptr;
        }

		void ForceKeyframe()
		{
			flag = VPX_EFLAG_FORCE_KF;
		}
    };

    public ref class Decoder
    {
    private:
		bool             destroyed;
        vpx_codec_ctx_t *codec;

    public:
		bool DebugMode;
		
        Decoder()
        {
            // initialize decoder
            codec = new vpx_codec_ctx_t();
            vpx_codec_err_t res = vpx_codec_dec_init(codec, vp8dx, NULL, 0);
            if (res)
            {
                System::Console::WriteLine("Could not initialize decoder. {0}", gcnew String(vpx_codec_err_to_string(res)));
                throw 0;
            }
        }

        !Decoder()
        {
			if (!destroyed)
			{
				destroyed = true;
				vpx_codec_destroy(codec);
				delete codec;
			}
        }

        ~Decoder()
        {
            this->!Decoder();
        }

        Bitmap^ Decode(array<Byte>^ encodedFrame)
        {
            // copy managed buffer to unmanaged buffer
            unsigned char *encFrame = new unsigned char[encodedFrame->Length];
            Marshal::Copy(encodedFrame, 0, (IntPtr)encFrame, encodedFrame->Length);

            // decode
            vpx_codec_err_t res = vpx_codec_decode(codec, encFrame, encodedFrame->Length, NULL, 0);
			delete encFrame;
            if (res)
            {
				if (DebugMode)
				{
					System::Console::WriteLine("Could not decode frame. {0}", gcnew String(vpx_codec_err_to_string(res)));
				}
				return nullptr;
            }

            // get frame
            vpx_image_t *img;
            vpx_codec_iter_t iter = NULL;
            while (img = vpx_codec_get_frame(codec, &iter))
            {
				Bitmap^ frame = gcnew Bitmap(img->d_w, img->d_h, PixelFormat::Format24bppRgb);
				BitmapData^ frameData = frame->LockBits(System::Drawing::Rectangle(Point::Empty, frame->Size), ImageLockMode::ReadOnly, frame->PixelFormat);
				try
				{
					// convert I420 to RGB
					Converter::ConvertI420ToRGB24(img, (unsigned char*)frameData->Scan0.ToPointer());
				}
				finally
				{
					frame->UnlockBits(frameData);
				}
                return frame;
            }
			
			if (DebugMode)
			{
				System::Console::WriteLine("Could not decode frame.");
			}
			return nullptr;
        }
    };
}
