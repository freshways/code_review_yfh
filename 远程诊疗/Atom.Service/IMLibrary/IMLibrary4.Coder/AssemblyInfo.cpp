﻿#include "stdafx.h"

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security::Permissions;

// 有关程序集的常规信息通过以下
// 特性集控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("IMLibrary4.Coder")];
[assembly: AssemblyDescription("即时通讯基础组件,禁止商业用途。")];
[assembly: AssemblyConfiguration("")];
[assembly: AssemblyCompany("ourmsg.net")];
[assembly: AssemblyProduct("IMLibrary4.Coder")];
[assembly: AssemblyCopyright("Copyright © 租李叶(25348855),禁止商业用途.")];
[assembly: AssemblyTrademark("IMLibrary4")];
[assembly: AssemblyCulture("")];

//[assembly:AssemblyKeyFileAttribute("IMLibrary4.Coder.snk")];
//[assembly:AssemblyDelaySignAttribute(true)];