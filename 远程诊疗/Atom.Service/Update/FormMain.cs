using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Update
{
	/// <summary>
	/// Form1 的摘要说明。
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelState;
        private Timer timer1;
        private IContainer components;

		public FormMain()
		{
			//
			// Windows 窗体设计器支持所必需的
			//
			InitializeComponent();

			//
			// TODO: 在 InitializeComponent 调用后添加任何构造函数代码
			//
		}

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows 窗体设计器生成的代码
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelState = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelState);
            this.groupBox1.Location = new System.Drawing.Point(10, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(279, 61);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "升级状态";
            this.groupBox1.UseWaitCursor = true;
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Location = new System.Drawing.Point(16, 29);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(71, 12);
            this.labelState.TabIndex = 0;
            this.labelState.Text = "正在运行...";
            this.labelState.UseWaitCursor = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.ClientSize = new System.Drawing.Size(299, 80);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormMain";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OurMsg智能更新程序";
            this.TopMost = true;
            this.UseWaitCursor = true;
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{
             
		}

		private void KillEMProcess()//关闭所有正在运行的EM进程
		{
			System.Diagnostics.Process[] pTemp = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process pTempProcess in pTemp)
            {
                if ((pTempProcess.ProcessName.ToLower() == ("OurMsg").ToLower()) || (pTempProcess.ProcessName.ToLower()) == ("OurMsg.exe").ToLower())
                    pTempProcess.Kill();
            }
		}

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.timer1.Enabled = false;
            this.labelState.Text = "正在终止OurMsg的运行...";//
            this.labelState.Refresh();

            KillEMProcess();//关闭所有正在运行的EMsg进程

            Application.DoEvents();

            this.labelState.Text = "正在更新...";//
            this.labelState.Refresh();
            System.Threading.Thread.Sleep(10000);

            DeCompress("", "BSE.Windows.Forms.dll", true);//
            DeCompress("", "CaptureImageTool.dll", true);//             
            DeCompress("", "IMLibrary4.dll", true);// 
            
            //begin 2014-12-17 12:08:50 编辑
            DeCompress("", "IMLibrary4.Audio.dll", true);// 
            DeCompress("", "IMLibrary4.AV.dll", true);// 
            DeCompress("", "IMLibrary4.Coder.dll", true);// 
            DeCompress("", "IMLibrary4.File.dll", true);//
            DeCompress("", "IMLibrary4.ImageC.dll", true);//
            DeCompress("", "IMLibrary4.Net.dll", true);//
            DeCompress("", "IMLibrary4.RTP.dll", true);//
            DeCompress("", "IMLibrary4.Voide.dll", true);//
            //end
            DeCompress("", "IMLibrary4.SQLiteData.dll", true);// 
            DeCompress("", "Login.db", false);//  
            DeCompress("", "OurMsg.exe", true);// 
            DeCompress("", "OurMsg.exe.config", true);// 
            DeCompress("", "Record.db", false );//             

            DeCompress("", "SQLite.Interop.dll", true);//  
            DeCompress("", "System.Data.SQLite.dll", true);// 
            DeCompress("", "updatelog.txt", true);// 

            #region 更新文件夹下内容
            
            try
            {
                System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(Application.StartupPath + "\\face");
                if (!dInfo.Exists)
                    dInfo.Create();

                for (int i = 0; i < 98; i++)
                    DeCompress("face", i.ToString() + ".gif", false);

                dInfo = new System.IO.DirectoryInfo(Application.StartupPath + "\\sound");
                if (!dInfo.Exists)
                    dInfo.Create();

                DeCompress("sound", "call.wav", false);
                DeCompress("sound", "folder.wav", false);
                DeCompress("sound", "Global.wav", false);
                DeCompress("sound", "InputAlert.wav", false);
                DeCompress("sound", "msg.wav", false);
                DeCompress("sound", "ring.wav", false);
                DeCompress("sound", "security.wav", false);
                DeCompress("sound", "system.wav", false);
                //dInfo = new System.IO.DirectoryInfo(Application.StartupPath + "\\mpg4vki");
                //if (!dInfo.Exists)
                //    dInfo.Create();

                //DeCompress("mpg4vki", "install.bat", true );
                //DeCompress("mpg4vki", "MPG4C32.dll", false);
                //DeCompress("mpg4vki", "mpg4ds32.ax", false);
                //DeCompress("mpg4vki", "MPG4VKI.inf", false);
              

            }
            catch { }

            #endregion

            this.labelState.Text = "正在启动OurMsg...";
            this.labelState.Refresh();
            System.Diagnostics.Process.Start(Application.StartupPath + "\\OurMsg.exe");//运行刚更新的应用程序

            Application.Exit();
        }

        /// <summary>
        /// 更新现有资源文件
        /// </summary>
        /// <param name="resourceName">资源文件名</param>
        /// <param name="isDelUpdate">如果资源文件存在，是否需要删除并更新</param>
        private void DeCompress(string path, string resourceName, bool isDelUpdate)//将资源文件写入当前目录
        {
            try
            {
                string TempFile = Application.StartupPath + "\\" + path + "\\" + resourceName;
                if (path == "")
                    TempFile = Application.StartupPath + "\\" + resourceName;

                if (System.IO.File.Exists(TempFile) && isDelUpdate)//如果资源文件存在，且需要删除源文件并更新
                    System.IO.File.Delete(TempFile);//则删除文件

                if (System.IO.File.Exists(TempFile) && !isDelUpdate)
                    return;//退出不删除不更新

                Application.DoEvents();


                byte[] b = new byte[1];

                string rName = "Update.files." + path + "." + resourceName;

                if (!System.IO.File.Exists(TempFile))
                {
                    if (path == "")
                        rName = "Update.files." + resourceName;

                    System.IO.Stream tobjStream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(rName);
                    System.IO.Stream f = new System.IO.FileStream(TempFile, System.IO.FileMode.CreateNew);
                    long fLength = tobjStream.Length;
                    b = new byte[fLength];
                    tobjStream.Read(b, 0, b.Length);
                    f.Write(b, 0, b.Length);
                    f.Flush();
                    f.Close();
                    tobjStream.Close();
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }
	}
}
