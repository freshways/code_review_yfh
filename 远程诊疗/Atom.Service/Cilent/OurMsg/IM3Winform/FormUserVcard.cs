﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OurMsg;
using CCWin;

namespace IMLibrary4 
{
    public partial class FormUserVcard : BaseForm
    {
        #region 闪烁处理
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;////用双缓冲绘制窗口的所有子控件
                return cp;
            }
        }
        #endregion

        public FormUserVcard()
        {
            InitializeComponent();
            comboBoxSex.SelectedIndex = 0;
            //this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrmChat_Paint);
        }

        #region 事件

        /// <summary>
        /// 创建用户事件代理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="userVcard"></param>
        public delegate void changeEventHandler(object sender, IMLibrary4.Protocol.EditUserData    userVcard);

        /// <summary>
        /// 创建用户事件
        /// </summary>
        public event changeEventHandler Create;

        /// <summary>
        /// 刷新用户资料事件
        /// </summary>
        public event changeEventHandler RefreshVcard;

    
        /// <summary>
        /// 更新用户信息事件
        /// </summary>
        public event changeEventHandler UpdateVcard;


        /// <summary>
        /// 修改密码委托
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void changePasswordEventHandler(object sender, IMLibrary4.Protocol.EditPassword e);

        /// <summary>
        /// 密码修改事件
        /// </summary>
        public event changePasswordEventHandler ChanagePassword;

        /// <summary>
        /// 修改主窗体用户名
        /// </summary>

        public event changeEventHandler SetMainUser;

        #endregion

        #region 属性
        /// <summary>
        /// 当前用户
        /// </summary>
        public string myUserID = "";

        /// <summary>
        /// 更新群组信息超时记数器
        /// </summary>
        private int outTime = 0;

        private bool _isAdmin = false;
        public bool isAdmin
        {
            set
            {
                _isAdmin = value;
                butRefresh.Visible = !value;
                checkBox1.Enabled = value;
            }
            get { return _isAdmin; }
        }

        private bool _IsCreate = false;
        /// <summary>
        /// 是否创建，否为更新
        /// </summary>
        public bool IsCreate
        {
            set
            {
                _IsCreate = value;
                if (value)
                {
                    this.butOK.Visible = false;
                    this.butCreate.Visible = true;
                }
                else
                {
                    this.butOK.Visible = true;
                    this.butCreate.Visible = false;
                    this.textBoxUserID.ReadOnly = true;

                    #region 查看用户信息
                    
                    if (!isAdmin)//如果不是管理员，则只能查看用户信息
                    {
                        tabControl1.TabPages.Remove(tabPage2);
                        //如果不是管理员直接冻结确定按钮
                        this.butOK.Enabled = false;
                        this.butSelectGroupID.Visible = false; //分组按钮隐藏
                        //详细信息
                        this.textBoxUserName.ReadOnly = true; //姓名
                        this.textBoxGroupID.ReadOnly = true; //分组
                        this.numericUpDownOrder.Enabled = false; //排序
                        this.textBoxMail.ReadOnly = true; //邮箱
                        this.textBoxJob.ReadOnly = true; //岗位
                        this.textBoxOfficePhone.ReadOnly = true; //电话
                        this.textBoxPhone.ReadOnly = true; //手机
                        this.textBoxPost.ReadOnly = true; //职务
                        this.textBoxRemark.ReadOnly = true; //说明
                        this.dateTimePickerBirthday.Enabled = false;//生日
                        this.comboBoxSex.Enabled = false; //性别
                        //权限页签
                        this.numericUpDownCreateRooms.Enabled = false; //允许创建群组
                        this.checkBoxDisable.Enabled = false;
                        this.checkBoxIsAdmin.Enabled = false;
                        this.checkBoxIsSendNotice.Enabled = false;
                        this.checkBoxIsSendSMS.Enabled = false;
                    }
                    #endregion

                    #region 修改本人信息

                    if (myUserID == UserData.UserID)//如果是本人员，则可以修改信息
                    {
                        //如果不是管理员，并且是自己，即可更改个人信息 2014-12-18 08:58:41yufh修改
                        //1.打开更新按钮
                        butOK.Enabled = true;
                        //2.详细信息设置可编辑
                        this.textBoxUserName.ReadOnly = false; //姓名
                        this.textBoxMail.ReadOnly = false; //邮箱
                        this.textBoxJob.ReadOnly = false; //岗位
                        this.textBoxOfficePhone.ReadOnly = false; //电话
                        this.textBoxPhone.ReadOnly = false; //手机
                        this.textBoxPost.ReadOnly = false; //职务
                        this.textBoxRemark.ReadOnly = false; //说明
                        this.dateTimePickerBirthday.Enabled = true;//生日
                        this.comboBoxSex.Enabled = true; //性别
                        //3.打开密码修改
                        checkBox1.Enabled = true;
                    }

                    #endregion
                }
            }
            get { return _IsCreate; }
        }

        /// <summary>
        /// 创建或更新群组信息是否成功
        /// </summary>
        public bool isUpdateSuccess = false;

        private IMLibrary4.Protocol.EditUserData _UserData;
        public IMLibrary4.Protocol.EditUserData UserData
        {
            set
            {
                _UserData = value;
                this.textBoxUserID.Text = _UserData.UserID;
                this.textBoxUserName.Text = _UserData.UserName;
                this.textBoxGroupID.Text = _UserData.GroupID;
                this.numericUpDownOrder.Value = _UserData.OrderID;
                this.textBoxMail.Text = _UserData.Mail;
                this.textBoxJob.Text = _UserData.Job;
                this.textBoxOfficePhone.Text = _UserData.OfficePhone;
                this.textBoxPhone.Text = _UserData.Phone;
                this.textBoxPost.Text = _UserData.Post;
                this.textBoxRemark.Text = _UserData.Remark;
                if (_UserData.Birthday!=null)
                this.dateTimePickerBirthday.Value =Convert.ToDateTime(_UserData.Birthday);
                this.comboBoxSex.SelectedIndex = _UserData.Sex;
                this.numericUpDownCreateRooms.Value = _UserData.CreateMaxRooms;
                this.checkBoxDisable.Checked = _UserData.Disable;
                this.checkBoxIsAdmin.Checked = _UserData.isAdmin;
                this.checkBoxIsSendNotice.Checked = _UserData.isSendNotice;
                this.checkBoxIsSendSMS.Checked = _UserData.isSendSMS;
                this.checkBoxIsControlClient.Checked = _UserData.isControlClient;
            }
            get
            {
                if (_UserData == null) _UserData = new Protocol.EditUserData();
                _UserData.UserID = this.textBoxUserID.Text.Trim();
                _UserData.UserName = this.textBoxUserName.Text.Trim();
                _UserData.GroupID = this.textBoxGroupID.Text.Trim();
                _UserData.OrderID = (int)this.numericUpDownOrder.Value;
                _UserData.Job = this.textBoxJob.Text.Trim();
                _UserData.OfficePhone = this.textBoxOfficePhone.Text.Trim();
                _UserData.Mail = this.textBoxMail.Text.Trim();
                _UserData.Phone = this.textBoxPhone.Text.Trim();
                _UserData.Post = this.textBoxPost.Text.Trim();
                _UserData.Remark = this.textBoxRemark.Text.Trim();
                _UserData.Birthday = this.dateTimePickerBirthday.Value.ToShortDateString();
                _UserData.Sex = (byte)this.comboBoxSex.SelectedIndex;
                _UserData.CreateMaxRooms = (int)this.numericUpDownCreateRooms.Value;
                _UserData.Disable = this.checkBoxDisable.Checked;
                _UserData.isAdmin = this.checkBoxIsAdmin.Checked;
                _UserData.isSendNotice = this.checkBoxIsSendNotice.Checked;
                _UserData.isSendSMS = this.checkBoxIsSendSMS.Checked;
                _UserData.isControlClient = this.checkBoxIsControlClient.Checked;
                return _UserData;
            }
        }
        #endregion

        #region 窗体事件

        //渐变层
        private void FrmChat_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            SolidBrush sb = new SolidBrush(Color.FromArgb(100, 255, 255, 255));
            //g.FillRectangle(sb, new Rectangle(new Point(1, 91), new Size(Width - 2, Height - 91)));
        }
        #endregion  

        private void FormUserVcard_Load(object sender, EventArgs e)
        {

        }

        private void butOK_Click(object sender, EventArgs e)
        {
            if (!isAdmin)
            {
                //this.Close();//如果不是管理员，关闭窗口
                //return;
            }
            if (textBoxUserID.Text.Trim() == "")
            {
                IMLibrary4.Global.MsgShow("请填写用户帐号！"); return;
            }
            if (textBoxUserName.Text.Trim() == "")
            {
                IMLibrary4.Global.MsgShow("请填写用户姓名！"); return;
            }
            if (textBoxGroupID.Text.Trim() == "")
            {
                IMLibrary4.Global.MsgShow("请选择用户所在分组！"); return;
            }

            this.butOK.Enabled = false;
            this.isUpdateSuccess = false;
            timer1.Enabled = true;

            if (UpdateVcard != null)
                UpdateVcard(this,UserData);
        }

        private void butCreate_Click(object sender, EventArgs e)
        {
            if (textBoxUserID.Text.Trim() == "")
            {
                IMLibrary4.Global.MsgShow("请填写用户帐号！"); return;
            }
            if (textBoxUserName.Text.Trim() == "")
            {
                IMLibrary4.Global.MsgShow("请填写用户姓名！"); return;
            }
            if (textBoxGroupID  .Text.Trim() == "")
            {
                IMLibrary4.Global.MsgShow("请选择用户所在分组！"); return;
            }
            IsCreate = true;

            this.butCreate.Enabled = false;
            this.isUpdateSuccess = false;
            timer1.Enabled = true;

            if (Create  != null)
                Create(this, UserData);
        }


        private void butCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void butSelectGroupID_Click(object sender, EventArgs e)
        {
            formSelectGroup frmSelGroup = new formSelectGroup();
            frmSelGroup.LoadData();
            frmSelGroup.selectGroup(this.textBoxGroupID.Text);
            frmSelGroup.ShowDialog();
            if (frmSelGroup != null) 
                this.textBoxGroupID.Text = frmSelGroup.Group.GroupID;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            outTime++;
            if (isUpdateSuccess)
            {
                timer1.Enabled = false;
                if (!IsCreate)
                {
                    IMLibrary4.Global.MsgShow("更新新用户信息成功！");
                    SetMainUser(this,UserData);//向父级声明更新后的名称
                    this.Close();
                }
                else
                {
                    IMLibrary4.Global.MsgShow("创建新用户成功！");
                    string groupID = this.textBoxGroupID.Text;
                    UserData = new Protocol.EditUserData();
                    this.textBoxGroupID.Text = groupID;
                    this.butCreate.Enabled = true;
                }
            }
            else if (outTime > 10)
            {
                timer1.Enabled = false;
                IMLibrary4.Global.MsgShow("操作超时！");

                this.butCreate.Enabled = true;
                this.butOK.Enabled = true;

                outTime = 0;
            }
        }

        private void butRefresh_Click(object sender, EventArgs e)
        {
            butRefresh.Enabled = false;


            int count = 0;
            Timer t = new Timer();
            t.Interval = 1000;
            t.Enabled = true;
            t.Tick += delegate(object sender1, EventArgs e1)
            {
                count++;
                butRefresh.Text = "更新(" + (5 - count).ToString() + ")";
                if (count == 5)
                {
                    t.Enabled = false;
                    butRefresh.Enabled = true;
                    butRefresh.Text = "更新";
                    t.Dispose();
                    sender1 = null;
                }
            };

            if (RefreshVcard != null)
                RefreshVcard(this, UserData);

           
        }

        private void butChanagePassword_Click(object sender, EventArgs e)
        {
            if (textBoxOldPassword.Text.Trim() == "" && !isAdmin )
            {
                IMLibrary4.Global.MsgShow("请填写旧密码！"); return;
            }
            if (textBoxNewPassword. Text.Trim() == "")
            {
                IMLibrary4.Global.MsgShow("请填写新密码！"); return;
            }
            if (textBoxConfirmPassword. Text.Trim() == "")
            {
                IMLibrary4.Global.MsgShow("请填写确认新密码！"); return;
            }
            if (textBoxNewPassword.Text != textBoxConfirmPassword.Text )
            {
                IMLibrary4.Global.MsgShow("新密码两次输入不正确，请重新输入！"); return;
            }

            IMLibrary4.Protocol.EditPassword cw = new Protocol.EditPassword();
            cw.from = textBoxUserID.Text;//要更改密码的用户ID
            cw.type = Protocol.type.set;
            cw.NewPassword =textBoxNewPassword.Text;
            cw.OldPassword =textBoxOldPassword.Text;
            butChanagePassword.Enabled = false;

            int count = 0;
            Timer t = new Timer();
            t.Interval = 1000;
            t.Enabled = true;
            t.Tick += delegate(object sender1, EventArgs e1)
            {
                count++;
                butChanagePassword.Text = "提交(" + (5-count).ToString() +")";
                if (count == 5)
                {
                    t.Enabled = false;
                    butChanagePassword.Enabled = true;
                    butChanagePassword.Text = "提交";
                    t.Dispose();
                    sender1 = null;
                }
            };

            if (ChanagePassword != null) ChanagePassword(this, cw);//触发密码更新事件

        }

        

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (!isAdmin)
                this.textBoxOldPassword.ReadOnly = !checkBox1.Checked;
            this.textBoxConfirmPassword.ReadOnly = !checkBox1.Checked;
            this.textBoxNewPassword.ReadOnly = !checkBox1.Checked;
            this.butChanagePassword.Enabled = checkBox1.Checked;

        }
    }
}
