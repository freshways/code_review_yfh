﻿namespace IMLibrary4 
{
    partial class FormGroupVcard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.butSelectGroupID = new System.Windows.Forms.Button();
            this.numericUpDownOrder = new System.Windows.Forms.NumericUpDown();
            this.textBoxSuperiorId = new System.Windows.Forms.TextBox();
            this.textBoxGroupName = new System.Windows.Forms.TextBox();
            this.textBoxGroupID = new System.Windows.Forms.TextBox();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.butCreate = new System.Windows.Forms.Button();
            this.butRefresh = new System.Windows.Forms.Button();
            this.butCancel = new System.Windows.Forms.Button();
            this.butOK = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrder)).BeginInit();
            this.skinPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(7, 29);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(407, 271);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Window;
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.butSelectGroupID);
            this.tabPage1.Controls.Add(this.numericUpDownOrder);
            this.tabPage1.Controls.Add(this.textBoxSuperiorId);
            this.tabPage1.Controls.Add(this.textBoxGroupName);
            this.tabPage1.Controls.Add(this.textBoxGroupID);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(399, 245);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本资料";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 26;
            this.label6.Text = "排   序:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 25;
            this.label5.Text = "上级分组:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 24;
            this.label2.Text = "分组名称:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 23;
            this.label1.Text = "分组编号:";
            // 
            // butSelectGroupID
            // 
            this.butSelectGroupID.Location = new System.Drawing.Point(332, 110);
            this.butSelectGroupID.Name = "butSelectGroupID";
            this.butSelectGroupID.Size = new System.Drawing.Size(40, 23);
            this.butSelectGroupID.TabIndex = 8;
            this.butSelectGroupID.Text = "...";
            this.butSelectGroupID.UseVisualStyleBackColor = true;
            this.butSelectGroupID.Click += new System.EventHandler(this.butSelectGroupID_Click);
            // 
            // numericUpDownOrder
            // 
            this.numericUpDownOrder.Location = new System.Drawing.Point(90, 152);
            this.numericUpDownOrder.Name = "numericUpDownOrder";
            this.numericUpDownOrder.Size = new System.Drawing.Size(281, 21);
            this.numericUpDownOrder.TabIndex = 21;
            // 
            // textBoxSuperiorId
            // 
            this.textBoxSuperiorId.Location = new System.Drawing.Point(90, 111);
            this.textBoxSuperiorId.MaxLength = 10;
            this.textBoxSuperiorId.Name = "textBoxSuperiorId";
            this.textBoxSuperiorId.ReadOnly = true;
            this.textBoxSuperiorId.Size = new System.Drawing.Size(281, 21);
            this.textBoxSuperiorId.TabIndex = 20;
            // 
            // textBoxGroupName
            // 
            this.textBoxGroupName.Location = new System.Drawing.Point(90, 72);
            this.textBoxGroupName.MaxLength = 20;
            this.textBoxGroupName.Name = "textBoxGroupName";
            this.textBoxGroupName.Size = new System.Drawing.Size(281, 21);
            this.textBoxGroupName.TabIndex = 16;
            // 
            // textBoxGroupID
            // 
            this.textBoxGroupID.Location = new System.Drawing.Point(90, 33);
            this.textBoxGroupID.MaxLength = 20;
            this.textBoxGroupID.Name = "textBoxGroupID";
            this.textBoxGroupID.Size = new System.Drawing.Size(281, 21);
            this.textBoxGroupID.TabIndex = 14;
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.butCreate);
            this.skinPanel1.Controls.Add(this.butRefresh);
            this.skinPanel1.Controls.Add(this.butCancel);
            this.skinPanel1.Controls.Add(this.butOK);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(4, 28);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(414, 306);
            this.skinPanel1.TabIndex = 9;
            // 
            // butCreate
            // 
            this.butCreate.BackgroundImage = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.butCreate.Location = new System.Drawing.Point(336, 276);
            this.butCreate.Name = "butCreate";
            this.butCreate.Size = new System.Drawing.Size(73, 26);
            this.butCreate.TabIndex = 4;
            this.butCreate.Text = "创建";
            this.butCreate.UseVisualStyleBackColor = true;
            this.butCreate.Visible = false;
            this.butCreate.Click += new System.EventHandler(this.butCreate_Click);
            // 
            // butRefresh
            // 
            this.butRefresh.BackgroundImage = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.butRefresh.Location = new System.Drawing.Point(3, 276);
            this.butRefresh.Name = "butRefresh";
            this.butRefresh.Size = new System.Drawing.Size(73, 26);
            this.butRefresh.TabIndex = 7;
            this.butRefresh.Text = "更新";
            this.butRefresh.UseVisualStyleBackColor = true;
            this.butRefresh.Visible = false;
            this.butRefresh.Click += new System.EventHandler(this.butRefresh_Click);
            // 
            // butCancel
            // 
            this.butCancel.BackgroundImage = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.butCancel.Location = new System.Drawing.Point(254, 276);
            this.butCancel.Name = "butCancel";
            this.butCancel.Size = new System.Drawing.Size(73, 26);
            this.butCancel.TabIndex = 5;
            this.butCancel.Text = "取消";
            this.butCancel.UseVisualStyleBackColor = true;
            this.butCancel.Click += new System.EventHandler(this.butCancel_Click);
            // 
            // butOK
            // 
            this.butOK.Location = new System.Drawing.Point(335, 277);
            this.butOK.Name = "butOK";
            this.butOK.Size = new System.Drawing.Size(73, 26);
            this.butOK.TabIndex = 6;
            this.butOK.Text = "确定";
            this.butOK.UseVisualStyleBackColor = true;
            this.butOK.Click += new System.EventHandler(this.butOK_Click);
            // 
            // FormGroupVcard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 338);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.skinPanel1);
            this.MinimizeBox = true;
            this.MinimumSize = new System.Drawing.Size(422, 338);
            this.Name = "FormGroupVcard";
            this.Text = "分组资料";
            this.Load += new System.EventHandler(this.FormUserVcard_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownOrder)).EndInit();
            this.skinPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button butOK;
        private System.Windows.Forms.Button butCancel;
        private System.Windows.Forms.Button butCreate;
        private System.Windows.Forms.Button butRefresh;
        private System.Windows.Forms.Timer timer1;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butSelectGroupID;
        private System.Windows.Forms.NumericUpDown numericUpDownOrder;
        private System.Windows.Forms.TextBox textBoxSuperiorId;
        private System.Windows.Forms.TextBox textBoxGroupName;
        private System.Windows.Forms.TextBox textBoxGroupID;
        private System.Windows.Forms.TabControl tabControl1;
    }
}