﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CCWin;
using System.Runtime.InteropServices;

namespace OurMsg
{
    public partial class BaseForm : CCSkinMain
    {
        
        public BaseForm()
        {
            InitializeComponent();
            this.SizeChanged += new EventHandler(BaseForm_SizeChanged);
            this.ControlRemoved += new ControlEventHandler(BaseForm_ControlRemoved);

        }
        private void BaseForm_SizeChanged(object sender, EventArgs e)
        {
            //this.Refresh();
        }

        private void BaseForm_ControlRemoved(object sender, ControlEventArgs e)
        {
            try
            {
                this.Refresh();
            }
            catch { }
        }

        #region UseCustomIcon
        private bool useCustomIcon = false;
        /// <summary>
        /// 是否使用自己的图标。
        /// </summary>
        public bool UseCustomIcon
        {
            get { return useCustomIcon; }
            set { useCustomIcon = value; }
        } 
        #endregion

        #region UseCustomBackImage
        private bool useCustomBackImage = false;
        /// <summary>
        /// 是否使用自己的背景图片
        /// </summary>
        public bool UseCustomBackImage
        {
            get { return useCustomBackImage; }
            set { useCustomBackImage = value; }
        } 
        #endregion            


         
    }
}
