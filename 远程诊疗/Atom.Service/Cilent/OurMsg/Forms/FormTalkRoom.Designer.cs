﻿namespace OurMsg
{
    partial class FormTalkRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTalkRoom));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.MessagePanel1 = new OurMsg.Controls.MessagePanel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panelGroupCount = new BSE.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new BSE.Windows.Forms.Panel();
            this.txtNotice = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsButSendFile = new System.Windows.Forms.ToolStripButton();
            this.tsButAV = new System.Windows.Forms.ToolStripButton();
            this.tsButSetGroupData = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.butClose = new CCWin.SkinControl.SkinButton();
            this.butSend = new CCWin.SkinControl.SkinButton();
            this.panelGroupCount.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            this.imageList1.Images.SetKeyName(13, "");
            this.imageList1.Images.SetKeyName(14, "");
            this.imageList1.Images.SetKeyName(15, "");
            this.imageList1.Images.SetKeyName(16, "");
            this.imageList1.Images.SetKeyName(17, "");
            this.imageList1.Images.SetKeyName(18, "");
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FloralWhite;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(13, 256);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(573, 142);
            this.flowLayoutPanel1.TabIndex = 3;
            this.flowLayoutPanel1.Visible = false;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // MessagePanel1
            // 
            this.MessagePanel1.AllowDrop = true;
            this.MessagePanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MessagePanel1.BackColor = System.Drawing.Color.Transparent;
            this.MessagePanel1.Location = new System.Drawing.Point(7, 76);
            this.MessagePanel1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MessagePanel1.Name = "MessagePanel1";
            this.MessagePanel1.Size = new System.Drawing.Size(583, 494);
            this.MessagePanel1.TabIndex = 0;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.SystemColors.Window;
            this.linkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.linkLabel1.Location = new System.Drawing.Point(11, 591);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(157, 15);
            this.linkLabel1.TabIndex = 2;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Tag = "www.ourmsg.net";
            this.linkLabel1.Text = "点击此处查看每日要闻";
            this.linkLabel1.Visible = false;
            // 
            // panelGroupCount
            // 
            this.panelGroupCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelGroupCount.AssociatedSplitter = null;
            this.panelGroupCount.BackColor = System.Drawing.Color.Transparent;
            this.panelGroupCount.CaptionFont = new System.Drawing.Font("微软雅黑", 9F);
            this.panelGroupCount.CaptionHeight = 22;
            this.panelGroupCount.Controls.Add(this.listView1);
            this.panelGroupCount.CustomColors.BorderColor = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.CaptionGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.CaptionGradientMiddle = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.CaptionSelectedGradientBegin = System.Drawing.SystemColors.Window;
            this.panelGroupCount.CustomColors.CaptionSelectedGradientEnd = System.Drawing.SystemColors.Window;
            this.panelGroupCount.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.CollapsedCaptionText = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.ContentGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.ContentGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.panelGroupCount.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.Image = null;
            this.panelGroupCount.Location = new System.Drawing.Point(595, 290);
            this.panelGroupCount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelGroupCount.MinimumSize = new System.Drawing.Size(29, 28);
            this.panelGroupCount.Name = "panelGroupCount";
            this.panelGroupCount.PanelStyle = BSE.Windows.Forms.PanelStyle.Office2007;
            this.panelGroupCount.ShowExpandIcon = true;
            this.panelGroupCount.Size = new System.Drawing.Size(255, 326);
            this.panelGroupCount.TabIndex = 45;
            this.panelGroupCount.Text = "成员(0/0)";
            this.panelGroupCount.ToolTipTextCloseIcon = null;
            this.panelGroupCount.ToolTipTextExpandIconPanelCollapsed = null;
            this.panelGroupCount.ToolTipTextExpandIconPanelExpanded = null;
            // 
            // listView1
            // 
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listView1.Location = new System.Drawing.Point(1, 23);
            this.listView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(253, 302);
            this.listView1.SmallImageList = this.imageList1;
            this.listView1.TabIndex = 4;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 140;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AssociatedSplitter = null;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.CaptionFont = new System.Drawing.Font("微软雅黑", 9F);
            this.panel1.CaptionHeight = 22;
            this.panel1.Controls.Add(this.txtNotice);
            this.panel1.CustomColors.BorderColor = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.CaptionGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.CaptionGradientMiddle = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.CaptionSelectedGradientBegin = System.Drawing.SystemColors.Window;
            this.panel1.CustomColors.CaptionSelectedGradientEnd = System.Drawing.SystemColors.Window;
            this.panel1.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.CollapsedCaptionText = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.ContentGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.ContentGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Image = ((System.Drawing.Image)(resources.GetObject("panel1.Image")));
            this.panel1.Location = new System.Drawing.Point(595, 76);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.MinimumSize = new System.Drawing.Size(29, 28);
            this.panel1.Name = "panel1";
            this.panel1.PanelStyle = BSE.Windows.Forms.PanelStyle.Office2007;
            this.panel1.ShowExpandIcon = true;
            this.panel1.Size = new System.Drawing.Size(255, 215);
            this.panel1.TabIndex = 43;
            this.panel1.Text = "动态";
            this.panel1.ToolTipTextCloseIcon = null;
            this.panel1.ToolTipTextExpandIconPanelCollapsed = null;
            this.panel1.ToolTipTextExpandIconPanelExpanded = null;
            // 
            // txtNotice
            // 
            this.txtNotice.BackColor = System.Drawing.SystemColors.Window;
            this.txtNotice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNotice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNotice.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtNotice.Location = new System.Drawing.Point(1, 23);
            this.txtNotice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNotice.Multiline = true;
            this.txtNotice.Name = "txtNotice";
            this.txtNotice.ReadOnly = true;
            this.txtNotice.Size = new System.Drawing.Size(253, 191);
            this.txtNotice.TabIndex = 4;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsButSendFile,
            this.tsButAV,
            this.tsButSetGroupData});
            this.toolStrip1.Location = new System.Drawing.Point(9, 40);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(59, 31);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsButSendFile
            // 
            this.tsButSendFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButSendFile.Image = ((System.Drawing.Image)(resources.GetObject("tsButSendFile.Image")));
            this.tsButSendFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButSendFile.Name = "tsButSendFile";
            this.tsButSendFile.Size = new System.Drawing.Size(28, 28);
            this.tsButSendFile.Text = "发送文件 ";
            // 
            // tsButAV
            // 
            this.tsButAV.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButAV.Image = ((System.Drawing.Image)(resources.GetObject("tsButAV.Image")));
            this.tsButAV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButAV.Name = "tsButAV";
            this.tsButAV.Size = new System.Drawing.Size(28, 28);
            this.tsButAV.Text = "视频对话";
            this.tsButAV.Visible = false;
            // 
            // tsButSetGroupData
            // 
            this.tsButSetGroupData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButSetGroupData.Image = ((System.Drawing.Image)(resources.GetObject("tsButSetGroupData.Image")));
            this.tsButSetGroupData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButSetGroupData.Name = "tsButSetGroupData";
            this.tsButSetGroupData.Size = new System.Drawing.Size(28, 28);
            this.tsButSetGroupData.Click += new System.EventHandler(this.tsButSetGroupData_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.linkLabel2);
            this.panel2.Location = new System.Drawing.Point(595, 34);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(255, 41);
            this.panel2.TabIndex = 0;
            this.panel2.Visible = false;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkLabel2.LinkColor = System.Drawing.Color.DarkRed;
            this.linkLabel2.Location = new System.Drawing.Point(4, 6);
            this.linkLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(60, 24);
            this.linkLabel2.TabIndex = 0;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "广告";
            // 
            // butClose
            // 
            this.butClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butClose.BackColor = System.Drawing.Color.Transparent;
            this.butClose.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.butClose.DownBack = global::OurMsg.Properties.Resources.AddAccountBtn_Down;
            this.butClose.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.butClose.Location = new System.Drawing.Point(368, 579);
            this.butClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.butClose.MouseBack = global::OurMsg.Properties.Resources.AddAccountBtn_mouseover;
            this.butClose.Name = "butClose";
            this.butClose.NormlBack = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.butClose.Size = new System.Drawing.Size(109, 36);
            this.butClose.TabIndex = 46;
            this.butClose.Text = "关闭(&C)";
            this.butClose.UseVisualStyleBackColor = false;
            // 
            // butSend
            // 
            this.butSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butSend.BackColor = System.Drawing.Color.Transparent;
            this.butSend.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.butSend.DownBack = global::OurMsg.Properties.Resources.AddAccountBtn_Down;
            this.butSend.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.butSend.Location = new System.Drawing.Point(481, 579);
            this.butSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.butSend.MouseBack = global::OurMsg.Properties.Resources.AddAccountBtn_mouseover;
            this.butSend.Name = "butSend";
            this.butSend.NormlBack = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.butSend.Size = new System.Drawing.Size(109, 36);
            this.butSend.TabIndex = 46;
            this.butSend.Text = "发送(&S)";
            this.butSend.UseVisualStyleBackColor = false;
            // 
            // FormTalkRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.BorderPalace = ((System.Drawing.Image)(resources.GetObject("$this.BorderPalace")));
            this.ClientSize = new System.Drawing.Size(853, 622);
            this.Controls.Add(this.butSend);
            this.Controls.Add(this.butClose);
            this.Controls.Add(this.panelGroupCount);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.MessagePanel1);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.toolStrip1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.MinimumSize = new System.Drawing.Size(853, 622);
            this.Name = "FormTalkRoom";
            this.Text = "与某某组对话";
            this.Load += new System.EventHandler(this.FormGroupChat_Load);
            this.panelGroupCount.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsButSendFile;
        private System.Windows.Forms.ToolStripButton tsButAV;
        private System.Windows.Forms.ToolStripButton tsButSetGroupData;
        private BSE.Windows.Forms.Panel panelGroupCount;
        private BSE.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private Controls.MessagePanel MessagePanel1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.TextBox txtNotice;
        public System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private CCWin.SkinControl.SkinButton butClose;
        private CCWin.SkinControl.SkinButton butSend;
    }
}