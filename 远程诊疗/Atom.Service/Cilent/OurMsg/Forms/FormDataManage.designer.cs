namespace OurMsg
{
    partial class FormDataManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDataManage));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemSelAll = new System.Windows.Forms.ToolStripMenuItem();
            this.splitter1 = new BSE.Windows.Forms.Splitter();
            this.panel1 = new BSE.Windows.Forms.Panel();
            this.txtRecord = new IMLibrary4.MyExtRichTextBox();
            this.panelGroupCount = new BSE.Windows.Forms.Panel();
            this.treeView_Organization = new System.Windows.Forms.TreeView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ButFirst = new System.Windows.Forms.ToolStripButton();
            this.ButUp = new System.Windows.Forms.ToolStripButton();
            this.TextBoxPage = new System.Windows.Forms.ToolStripTextBox();
            this.ButDown = new System.Windows.Forms.ToolStripButton();
            this.ButLast = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ButDelRecord = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelGroupCount.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            this.imageList1.Images.SetKeyName(13, "");
            this.imageList1.Images.SetKeyName(14, "");
            this.imageList1.Images.SetKeyName(15, "");
            this.imageList1.Images.SetKeyName(16, "");
            this.imageList1.Images.SetKeyName(17, "");
            this.imageList1.Images.SetKeyName(18, "");
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemCopy,
            this.toolStripSeparator2,
            this.MenuItemSelAll});
            this.contextMenuStrip.Name = "contextMenuStrip1";
            this.contextMenuStrip.Size = new System.Drawing.Size(115, 54);
            this.contextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip_ItemClicked);
            // 
            // MenuItemCopy
            // 
            this.MenuItemCopy.Name = "MenuItemCopy";
            this.MenuItemCopy.Size = new System.Drawing.Size(114, 22);
            this.MenuItemCopy.Text = "复制(&C)";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(111, 6);
            // 
            // MenuItemSelAll
            // 
            this.MenuItemSelAll.Name = "MenuItemSelAll";
            this.MenuItemSelAll.Size = new System.Drawing.Size(114, 22);
            this.MenuItemSelAll.Text = "全选(&A)";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Transparent;
            this.splitter1.Location = new System.Drawing.Point(297, 53);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 443);
            this.splitter1.TabIndex = 48;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.AssociatedSplitter = null;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.CaptionFont = new System.Drawing.Font("微软雅黑", 9F);
            this.panel1.CaptionHeight = 22;
            this.panel1.Controls.Add(this.txtRecord);
            this.panel1.CustomColors.BorderColor = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.CaptionGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.CaptionGradientMiddle = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.CaptionSelectedGradientBegin = System.Drawing.SystemColors.Window;
            this.panel1.CustomColors.CaptionSelectedGradientEnd = System.Drawing.SystemColors.Window;
            this.panel1.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.CollapsedCaptionText = System.Drawing.SystemColors.ControlText;
            this.panel1.CustomColors.ContentGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.ContentGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panel1.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Image = null;
            this.panel1.Location = new System.Drawing.Point(302, 53);
            this.panel1.MinimumSize = new System.Drawing.Size(22, 22);
            this.panel1.Name = "panel1";
            this.panel1.PanelStyle = BSE.Windows.Forms.PanelStyle.Office2007;
            this.panel1.ShowExpandIcon = true;
            this.panel1.Size = new System.Drawing.Size(494, 443);
            this.panel1.TabIndex = 47;
            this.panel1.Text = "消息记录";
            this.panel1.ToolTipTextCloseIcon = null;
            this.panel1.ToolTipTextExpandIconPanelCollapsed = null;
            this.panel1.ToolTipTextExpandIconPanelExpanded = null;
            // 
            // txtRecord
            // 
            this.txtRecord.BackColor = System.Drawing.SystemColors.Window;
            this.txtRecord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRecord.ContextMenuStrip = this.contextMenuStrip;
            this.txtRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRecord.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtRecord.Location = new System.Drawing.Point(1, 23);
            this.txtRecord.Name = "txtRecord";
            this.txtRecord.ReadOnly = true;
            this.txtRecord.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtRecord.Size = new System.Drawing.Size(492, 419);
            this.txtRecord.TabIndex = 46;
            this.txtRecord.Text = "";
            // 
            // panelGroupCount
            // 
            this.panelGroupCount.AssociatedSplitter = null;
            this.panelGroupCount.BackColor = System.Drawing.Color.Transparent;
            this.panelGroupCount.CaptionFont = new System.Drawing.Font("微软雅黑", 9F);
            this.panelGroupCount.CaptionHeight = 22;
            this.panelGroupCount.Controls.Add(this.treeView_Organization);
            this.panelGroupCount.CustomColors.BorderColor = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.CaptionGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.CaptionGradientMiddle = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.CaptionSelectedGradientBegin = System.Drawing.SystemColors.Window;
            this.panelGroupCount.CustomColors.CaptionSelectedGradientEnd = System.Drawing.SystemColors.Window;
            this.panelGroupCount.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.CollapsedCaptionText = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.CustomColors.ContentGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.ContentGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panelGroupCount.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.panelGroupCount.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelGroupCount.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panelGroupCount.Image = null;
            this.panelGroupCount.Location = new System.Drawing.Point(4, 53);
            this.panelGroupCount.MinimumSize = new System.Drawing.Size(22, 22);
            this.panelGroupCount.Name = "panelGroupCount";
            this.panelGroupCount.PanelStyle = BSE.Windows.Forms.PanelStyle.Office2007;
            this.panelGroupCount.ShowExpandIcon = true;
            this.panelGroupCount.Size = new System.Drawing.Size(293, 443);
            this.panelGroupCount.TabIndex = 46;
            this.panelGroupCount.Text = "分组信息";
            this.panelGroupCount.ToolTipTextCloseIcon = null;
            this.panelGroupCount.ToolTipTextExpandIconPanelCollapsed = null;
            this.panelGroupCount.ToolTipTextExpandIconPanelExpanded = null;
            // 
            // treeView_Organization
            // 
            this.treeView_Organization.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView_Organization.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView_Organization.ImageIndex = 0;
            this.treeView_Organization.ImageList = this.imageList1;
            this.treeView_Organization.Location = new System.Drawing.Point(1, 23);
            this.treeView_Organization.Name = "treeView_Organization";
            this.treeView_Organization.SelectedImageIndex = 0;
            this.treeView_Organization.Size = new System.Drawing.Size(291, 419);
            this.treeView_Organization.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ButFirst,
            this.ButUp,
            this.TextBoxPage,
            this.ButDown,
            this.ButLast,
            this.toolStripSeparator1,
            this.ButDelRecord});
            this.toolStrip1.Location = new System.Drawing.Point(4, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(792, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // ButFirst
            // 
            this.ButFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButFirst.Image = ((System.Drawing.Image)(resources.GetObject("ButFirst.Image")));
            this.ButFirst.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButFirst.Name = "ButFirst";
            this.ButFirst.Size = new System.Drawing.Size(23, 22);
            this.ButFirst.Text = "第一页";
            this.ButFirst.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ButFirst.ToolTipText = "第一页";
            // 
            // ButUp
            // 
            this.ButUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButUp.Image = ((System.Drawing.Image)(resources.GetObject("ButUp.Image")));
            this.ButUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButUp.Name = "ButUp";
            this.ButUp.Size = new System.Drawing.Size(23, 22);
            this.ButUp.Text = "上一页";
            this.ButUp.ToolTipText = "上一页";
            // 
            // TextBoxPage
            // 
            this.TextBoxPage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxPage.Name = "TextBoxPage";
            this.TextBoxPage.Size = new System.Drawing.Size(80, 25);
            this.TextBoxPage.Text = "0/0";
            this.TextBoxPage.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ButDown
            // 
            this.ButDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButDown.Image = ((System.Drawing.Image)(resources.GetObject("ButDown.Image")));
            this.ButDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButDown.Name = "ButDown";
            this.ButDown.Size = new System.Drawing.Size(23, 22);
            this.ButDown.Text = "下一页";
            this.ButDown.ToolTipText = "下一页";
            // 
            // ButLast
            // 
            this.ButLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButLast.Image = ((System.Drawing.Image)(resources.GetObject("ButLast.Image")));
            this.ButLast.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButLast.Name = "ButLast";
            this.ButLast.Size = new System.Drawing.Size(23, 22);
            this.ButLast.Text = "最后一页";
            this.ButLast.ToolTipText = "最后一页";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ButDelRecord
            // 
            this.ButDelRecord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButDelRecord.Image = ((System.Drawing.Image)(resources.GetObject("ButDelRecord.Image")));
            this.ButDelRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButDelRecord.Name = "ButDelRecord";
            this.ButDelRecord.Size = new System.Drawing.Size(23, 22);
            this.ButDelRecord.Text = "删除记录";
            // 
            // FormDataManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 500);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelGroupCount);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "FormDataManage";
            this.Text = "信息管理器";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormDataManage_Load);
            this.contextMenuStrip.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelGroupCount.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ButFirst;
        private System.Windows.Forms.ToolStripButton ButUp;
        private System.Windows.Forms.ToolStripButton ButDown;
        private System.Windows.Forms.ToolStripButton ButLast;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ButDelRecord;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCopy;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSelAll;
        private System.Windows.Forms.ToolStripTextBox TextBoxPage;
        public System.Windows.Forms.ImageList imageList1;
        private BSE.Windows.Forms.Panel panelGroupCount;
        private BSE.Windows.Forms.Panel panel1;
        public IMLibrary4.MyExtRichTextBox txtRecord;
        private System.Windows.Forms.TreeView treeView_Organization;
        private BSE.Windows.Forms.Splitter splitter1;

    }
}