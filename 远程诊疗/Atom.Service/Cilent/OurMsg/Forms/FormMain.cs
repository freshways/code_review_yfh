﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading ;

namespace OurMsg
{
    public partial class FormMain : BaseForm
    {
        #region  重写WndProc(用于捕获Windows消息)
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            ////   一旦程序收到WM_SHUTDOWN消息， 
            ////   将systemShutdown布尔值设置为true。 
            if (m.Msg == WM_SHUTDOWN)
                systemShutdown = true;
            //if (m.Msg == 0x0014) // 禁掉清除背景消息
            //    return; 
            base.WndProc(ref   m);
        }
        #endregion

        #region 窗口初始化
        public FormMain()
        {
            InitializeComponent();
            #region 控制面板事件
            this.controlPanel1.UserLoginOutTime += new OurMsg.Controls.ControlPanel.ControlPanelEventHandler(controlPanel1_UserLoginOutTime);
            this.controlPanel1.UserLoginPasswordErrored += new OurMsg.Controls.ControlPanel.ControlPanelEventHandler(this.controlPanel1_UserLoginPasswordErrored);
            this.controlPanel1.UserLoginSuccessful += new OurMsg.Controls.ControlPanel.LoginEventHandler(controlPanel1_UserLoginSuccessful);
            this.controlPanel1.UserOffline += new OurMsg.Controls.ControlPanel.ControlPanelEventHandler(controlPanel1_UserOffline);
            this.controlPanel1.UserElseLogin += new OurMsg.Controls.ControlPanel.ControlPanelEventHandler(controlPanel1_UserElseLogin);
            this.controlPanel1.LoadMail += new OurMsg.Controls.ControlPanel.ControlPanelEventHandler(controlPanel1_LoadMail);
            this.controlPanel1.SetMainUser += new OurMsg.Controls.ControlPanel.SetUserName(controlPanel1_SetMainUser);
            #endregion

            this.Load += new EventHandler(FormMain_Load);
            this.FormClosing += new FormClosingEventHandler(FormMain_FormClosing);
            this.tButManageMsg.Click += new EventHandler(tButManageMsg_Click);
            this.tsmMsgMis.Click += new EventHandler(tButManageMsg_Click);
        }

        private void tButManageMsg_Click(object sender, EventArgs e)
        {
            if (Global.FormDataManageage == null || Global.FormDataManageage.IsDisposed)
            {
                Global.FormDataManageage = new FormDataManage();
                Global.FormDataManageage.myUserID = auth.UserID;
                Global.FormDataManageage.FormClosed += delegate(object sender1, FormClosedEventArgs e1)
                {
                    Global.FormDataManageage.Dispose(); Global.FormDataManageage = null;
                };
            }
            Global.FormDataManageage.Show();
        }
        #endregion
        
        #region 变量
        /// <summary>
        /// 关闭电脑时的Windows   Message的值
        /// </summary>
        private static int WM_SHUTDOWN = 0x11;

        /// <summary>
        /// 布尔值判断是否是关闭电脑 
        /// </summary>
        private static bool systemShutdown = false;

        /// <summary>
        /// 登录参数
        /// </summary>
        public IMLibrary4.Protocol.Auth auth = new IMLibrary4.Protocol.Auth();

        /// <summary>
        /// 标识当前用户是否成功登录过系统
        /// </summary>
        private bool IsLogined = false;//标识当前用户是否成功登录过系统

        private FormLogin frmLogin = null;//登录窗口
        #endregion

        #region 窗口相关事件

        #region 加载窗口
        private void FormMain_Load(object sender, EventArgs e)
        {
            setFormPoinstion();
            FormMain_Resize(null, null);

            if (Global.RunningInstance() == null)//如果程序线程已经运行了
                ReadyLogin(false);// 准备登录,系统中没有OurMsg程序在运行
            else
                ReadyLogin(true);// 准备登录，系统中至少有一个OurMsg程序在运行
        }
        #endregion

        #region 设置窗口位置
        /// <summary>
        /// 设置窗口位置
        /// </summary>
        private void setFormPoinstion()
        {
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            this.Top = (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2;
        }
        #endregion

        #region 准备登录
        /// <summary>
        /// 准备登录
        /// </summary>
        private void ReadyLogin(bool IsRepeat)
        {
            try
            {
                this.Hide();
                if (frmLogin == null || frmLogin.IsDisposed)
                    frmLogin = new FormLogin();

                frmLogin.IsRepeat = IsRepeat;

                if (IsRepeat && this.auth != null)//如果是重复登录，则更改登录参数
                    frmLogin.SetLoginParameter(this.auth);

                frmLogin.Text += Global.Version.ToString();//版本号
                frmLogin.ShowDialog();
                if (frmLogin.isExit)//如果退出应用程序
                {
                    this.ExitApp();//退出应用程序
                }
                else//如果不是退出应用程序，则登录
                {
                    if (!this.userLoginPanel1.IsDisposed)//如果登录进度显示组件未释放
                        this.userLoginPanel1.Start();//则显示登录进度
                    this.Show();//显示主窗口
                    this.auth = frmLogin.auth;//将当前登录参数设置为最新的
                    this.controlPanel1.Login(auth, false);//登录
                }
                frmLogin = null;
                //向主窗体添加版本号
                this.Text += Global.Version.ToString();
            }
            catch (Exception ex)
            {
                IMLibrary4.Global.MsgShow(ex.Message + ex.Source);
            }
        }

        #endregion

        #region 窗口关闭事件
        /// <summary>
        /// 窗口关闭事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!systemShutdown)//如果不是关闭windows
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
                this.Hide();
            }
            else//如果关闭windows，则退出程序
            {
                this.notifyIcon1.Dispose();//删除小图标
                this.controlPanel1.Close(true);//退出程序
                Application.Exit();
            }
        }
        #endregion

        #region 应用退出程序函数 ExitApp()
        /// <summary>
        /// 应用退出程序函数
        /// </summary>
        private void ExitApp()
        {
            systemShutdown = true;//强行指定为windows关闭
            this.Close();
            System.Diagnostics.Process.GetCurrentProcess().Kill();//强制杀进程结束程序
            try
            { Application.Exit(); }
            catch { }
        }
        #endregion

        #endregion

        #region 在线状态控件选择后事件
        private void ShowType1_ShowTypeChanged(object sender, IMLibrary4.Controls.ShowTypeControl.ShowTypeEventArgs e)
        {
            this.controlPanel1.setMyPresence(e.ShowType);
        }

        private void ShowType1_ShowTypeExitApp(object sender, IMLibrary4.Controls.ShowTypeControl.ShowTypeEventArgs e)
        {
            this.ExitApp();
        }
        #endregion

        #region 窗口尺寸更改事件
        private void FormMain_Resize(object sender, EventArgs e)
        {
            if (!this.userLoginPanel1.IsDisposed)
            {
                this.userLoginPanel1.Width = this.Width;
                this.userLoginPanel1.Height = this.Height;
            }
        }
        #endregion

        #region 取消登录事件
        private void userLoginPanel1_CancelLogin(object sender, EventArgs e)
        {
            ReadyLogin(true);// 准备登录
        }
        #endregion

        #region 用户别处登录
        private void controlPanel1_UserElseLogin(object sender)
        {
            this.ShowType1.State = IMLibrary4.Protocol.ShowType.Offline;
            showNotifyIconMessage("用户在别处登录，系统已强迫您下线！", "OurMsg掉线", 3000);
        }
        #endregion

        #region 登录密码错误处理
        private void controlPanel1_UserLoginPasswordErrored(object sender)
        {
            if (!this.userLoginPanel1.IsDisposed)
                this.userLoginPanel1.Stop();

            IMLibrary4.Global.MsgShow("密码错误！");
            ReadyLogin(true);// 准备登录
        }
        #endregion

        #region 登录超时处理
        private void controlPanel1_UserLoginOutTime(object sender)
        {
            if (!this.userLoginPanel1.IsDisposed)
                this.userLoginPanel1.Stop();

            if (!IsLogined)//如果没有成功登录过系统
            {
                IMLibrary4.Global.MsgShow("连接服务器超时。\n请重试！");
                ReadyLogin(true);// 准备登录
            }
            else
            {
                this.showNotifyIconMessage("连接服务器超时。\n请重试！", "OurMsg 提示", 5000);
            }
        }
        #endregion

        #region 登录成功处理

        private void controlPanel1_UserLoginSuccessful(object sender, IMLibrary4.Protocol.Auth auth)
        {
            this.showNotifyIconMessage("上次登录时间：" + (auth.LastDateTime == null ? "" : auth.LastDateTime), "OurMsg 登录成功", 5000);//上次登录IP：" + (auth.LastIP == null ? "" : auth.LastIP) + " \n
            this.notifyIcon1.Text = "OurMsg:" + auth.UserName + "(" + auth.UserID + ")";
            this.labUserName.Text = auth.UserName + "(" + auth.UserID + ")";
            ShowType1.State = auth.ShowType;

            Global.CurrUserID = auth.UserID;
            Global.CurrUserName = auth.UserName;            

            IsLogined = true;//标识当前用户已经登录过系统

            timer1.Enabled = true;//开始检测MIS系统任务
            mailCount = IMLibrary4.OpeRecordDB.GetMailCount(auth.UserID).ToString();
            timer1_Tick(null, null);

            Console.WriteLine(IMLibrary4.Protocol.Factory.CreateXMLMsg(auth));

            #region 删除登录进度效果显示面板
            this.Controls.Remove(this.userLoginPanel1);
            this.userLoginPanel1.Dispose();
            #endregion
        }
        /// <summary>
        /// 超时记数器
        /// </summary>
        private int outTime = 0;
        private string mailCount = "";
        private void timer1_Tick(object sender, EventArgs e)
        {
            outTime++;
            // OAmessage();
            if (mailCount != "")
            {
                timer1.Enabled = false;

                //在此获取邮件数量当收到邮件提醒时，进行刷新
                tsBtnMail.Text = mailCount;// IMLibrary4.OpeRecordDB.GetMailCount(auth.UserID).ToString();
            }
            else if (outTime > 10)
            {
                outTime = 0;
                timer1.Enabled = false;
            }
        }

        #endregion

        #region 窗体刷新

        /// <summary>
        /// 收到邮件通知时加载邮件数量
        /// </summary>
        /// <param name="sender"></param>
        private void controlPanel1_LoadMail(object sender)
        {
            tsBtnMail.Text = Convert.ToString(sender);

            FormMailNewList mail = new FormMailNewList();
            if (Application.OpenForms[mail.Name] != null)
            {
                mail = (FormMailNewList)Application.OpenForms[mail.Name];
                mail.LoadMail();
                mail.Activate();
            }
            //mailCount = sender.ToString();

            //timer1.Enabled = true;//开始检测mail
            
            //timer1_Tick(null, null);
        }

        /// <summary>
        /// 更新用户名称
        /// </summary>
        /// <param name="UserName"></param>
        private void controlPanel1_SetMainUser(string UserName)
        {
            labUserName.Text = UserName;
            this.notifyIcon1.Text = "OurMsg:" + UserName;
        }
        #endregion

        #region 用户离线
        public void controlPanel1_UserOffline(object sender)
        {
            this.ShowType1.State = IMLibrary4.Protocol.ShowType.Offline;
            //showNotifyIconMessage("与服务器失去连接！","OurMsg掉线",3000);
        }
        #endregion

        #region 显示提示信息
        /// <summary>
        /// 显示提示信息
        /// </summary>
        /// <param name="tip">要显示的内容</param>
        /// <param name="title">标题</param>
        /// <param name="timeLen">显示时间，以毫秒为单位</param>
        private void showNotifyIconMessage(string tip, string title, int timeLen)
        {
            this.notifyIcon1.BalloonTipText = tip;
            this.notifyIcon1.BalloonTipTitle = title;
            this.notifyIcon1.ShowBalloonTip(timeLen);
        }
        #endregion

        #region 图标双击
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (frmLogin != null && !frmLogin.IsDisposed)
            {
                frmLogin.Activate();
                return;
            }
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }
        #endregion

        #region 图标关联菜单事件
        private void contextMenuNotify_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name)
            {
                case "MenuItemExit":
                    this.ExitApp();//退出程序
                    break;
                case "MenuItemOpenMain":
                    {
                        this.Show();
                        this.WindowState = FormWindowState.Normal;
                    }
                    break;
            }
        }
        #endregion

        #region 开始菜单

        /// <summary>
        /// 个人信息设置-点击头像
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selfFaceImage1_ImageClick(object sender, EventArgs e)
        {
            controlPanel1.ShowUserVcard(auth.UserID);
        }

        /// <summary>
        /// 个人信息设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmMyInfo_Click(object sender, EventArgs e)
        {
            controlPanel1.ShowUserVcard(auth.UserID);
        }        

        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmClose_Click(object sender, EventArgs e)
        {
            this.ExitApp();//退出程序
        }

        /// <summary>
        /// 注销
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmLogOut_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定要退出,更换其它账号?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                this.controlPanel1.LogOut();
                //isRestart = true;
                Application.ExitThread();
                System.Diagnostics.Process.Start(System.Reflection.Assembly.GetExecutingAssembly().Location);
                //for (int i = 0; i < Application.OpenForms.Count;i++ )
                //{
                //    if (Application.OpenForms[i].Name != this.Name && Application.OpenForms[i].Name != "SkinFormTwo")
                //        Application.OpenForms[i].Close();
                //}
                //IsLogOut = true;
                //ReadyLogin(false);
            }
        }
        #endregion

        #region 邮件
        /// <summary>
        /// 打开邮箱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsBtnMail_Click(object sender, EventArgs e)
        {
            controlPanel1.ShowMainForm("收件箱");
        }

        #endregion        

        #region 离线文件列表
        
        /// <summary>
        /// 打开离线文件列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsBtnEnclosure_Click(object sender, EventArgs e)
        {
            controlPanel1.ShowEnclosure();
        }
        #endregion

    }   
}
