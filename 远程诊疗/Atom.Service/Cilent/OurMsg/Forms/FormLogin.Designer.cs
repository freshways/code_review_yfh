﻿namespace OurMsg
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.butLogin = new CCWin.SkinControl.SkinButton();
            this.butCancel = new CCWin.SkinControl.SkinButton();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.checkBoxAutoLogin = new CCWin.SkinControl.SkinCheckBox();
            this.checkBoxSavePassword = new CCWin.SkinControl.SkinCheckBox();
            this.skinPictureBox1 = new CCWin.SkinControl.SkinPictureBox();
            this.ShowType1 = new IMLibrary4.Controls.ShowTypeControl();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.cmbBoxUserID = new System.Windows.Forms.ComboBox();
            this.webUpdate1 = new OurMsg.Controls.webUpdate();
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // butLogin
            // 
            this.butLogin.BackColor = System.Drawing.Color.Transparent;
            this.butLogin.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.butLogin.DownBack = global::OurMsg.Properties.Resources.button_login_down;
            this.butLogin.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.butLogin.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.butLogin.ForeColor = System.Drawing.Color.White;
            this.butLogin.Location = new System.Drawing.Point(91, 400);
            this.butLogin.Margin = new System.Windows.Forms.Padding(4);
            this.butLogin.MouseBack = global::OurMsg.Properties.Resources.button_login_hover;
            this.butLogin.Name = "butLogin";
            this.butLogin.NormlBack = global::OurMsg.Properties.Resources.button_login_normal;
            this.butLogin.Size = new System.Drawing.Size(247, 35);
            this.butLogin.TabIndex = 50;
            this.butLogin.Text = "登 陆";
            this.butLogin.UseVisualStyleBackColor = false;
            this.butLogin.Click += new System.EventHandler(this.butLogin_Click);
            // 
            // butCancel
            // 
            this.butCancel.BackColor = System.Drawing.Color.Transparent;
            this.butCancel.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.butCancel.DownBack = global::OurMsg.Properties.Resources.button_login_down;
            this.butCancel.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.butCancel.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.butCancel.ForeColor = System.Drawing.Color.White;
            this.butCancel.Location = new System.Drawing.Point(91, 400);
            this.butCancel.Margin = new System.Windows.Forms.Padding(4);
            this.butCancel.MouseBack = global::OurMsg.Properties.Resources.button_login_hover;
            this.butCancel.Name = "butCancel";
            this.butCancel.NormlBack = global::OurMsg.Properties.Resources.button_login_normal;
            this.butCancel.Size = new System.Drawing.Size(247, 35);
            this.butCancel.TabIndex = 50;
            this.butCancel.Text = "取 消";
            this.butCancel.UseVisualStyleBackColor = false;
            this.butCancel.Visible = false;
            this.butCancel.Click += new System.EventHandler(this.butCancel_Click);
            // 
            // skinLabel3
            // 
            this.skinLabel3.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.None;
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel3.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.skinLabel3.Location = new System.Drawing.Point(83, 285);
            this.skinLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(52, 27);
            this.skinLabel3.TabIndex = 49;
            this.skinLabel3.Text = "状态";
            // 
            // skinLabel2
            // 
            this.skinLabel2.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.None;
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.BorderSize = 2;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel2.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.skinLabel2.Location = new System.Drawing.Point(83, 216);
            this.skinLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(104, 27);
            this.skinLabel2.TabIndex = 49;
            this.skinLabel2.Text = "密码|Pass";
            // 
            // skinLabel1
            // 
            this.skinLabel1.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.None;
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.BorderSize = 2;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.skinLabel1.Location = new System.Drawing.Point(83, 149);
            this.skinLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(105, 27);
            this.skinLabel1.TabIndex = 49;
            this.skinLabel1.Text = "账号|User";
            // 
            // checkBoxAutoLogin
            // 
            this.checkBoxAutoLogin.AutoSize = true;
            this.checkBoxAutoLogin.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxAutoLogin.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.checkBoxAutoLogin.DownBack = null;
            this.checkBoxAutoLogin.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxAutoLogin.Location = new System.Drawing.Point(228, 339);
            this.checkBoxAutoLogin.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxAutoLogin.MouseBack = null;
            this.checkBoxAutoLogin.Name = "checkBoxAutoLogin";
            this.checkBoxAutoLogin.NormlBack = null;
            this.checkBoxAutoLogin.SelectedDownBack = null;
            this.checkBoxAutoLogin.SelectedMouseBack = null;
            this.checkBoxAutoLogin.SelectedNormlBack = null;
            this.checkBoxAutoLogin.Size = new System.Drawing.Size(91, 24);
            this.checkBoxAutoLogin.TabIndex = 48;
            this.checkBoxAutoLogin.Text = "自动登录";
            this.checkBoxAutoLogin.UseVisualStyleBackColor = false;
            this.checkBoxAutoLogin.CheckedChanged += new System.EventHandler(this.checkBoxAutoLogin_CheckedChanged);
            // 
            // checkBoxSavePassword
            // 
            this.checkBoxSavePassword.AutoSize = true;
            this.checkBoxSavePassword.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxSavePassword.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.checkBoxSavePassword.DownBack = null;
            this.checkBoxSavePassword.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxSavePassword.Location = new System.Drawing.Point(100, 339);
            this.checkBoxSavePassword.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxSavePassword.MouseBack = null;
            this.checkBoxSavePassword.Name = "checkBoxSavePassword";
            this.checkBoxSavePassword.NormlBack = null;
            this.checkBoxSavePassword.SelectedDownBack = null;
            this.checkBoxSavePassword.SelectedMouseBack = null;
            this.checkBoxSavePassword.SelectedNormlBack = null;
            this.checkBoxSavePassword.Size = new System.Drawing.Size(91, 24);
            this.checkBoxSavePassword.TabIndex = 47;
            this.checkBoxSavePassword.Text = "记住密码";
            this.checkBoxSavePassword.UseVisualStyleBackColor = false;
            this.checkBoxSavePassword.CheckedChanged += new System.EventHandler(this.checkBoxSavePassword_CheckedChanged);
            // 
            // skinPictureBox1
            // 
            this.skinPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinPictureBox1.BackgroundImage = global::OurMsg.Properties.Resources.cloud;
            this.skinPictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.skinPictureBox1.Location = new System.Drawing.Point(0, 24);
            this.skinPictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.skinPictureBox1.Name = "skinPictureBox1";
            this.skinPictureBox1.Size = new System.Drawing.Size(275, 56);
            this.skinPictureBox1.TabIndex = 46;
            this.skinPictureBox1.TabStop = false;
            // 
            // ShowType1
            // 
            this.ShowType1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ShowType1.BackColor = System.Drawing.Color.Transparent;
            this.ShowType1.IsShowOffline = false;
            this.ShowType1.Location = new System.Drawing.Point(144, 284);
            this.ShowType1.Margin = new System.Windows.Forms.Padding(5);
            this.ShowType1.Name = "ShowType1";
            this.ShowType1.Size = new System.Drawing.Size(139, 28);
            this.ShowType1.State = IMLibrary4.Protocol.ShowType.NONE;
            this.ShowType1.TabIndex = 33;
            this.ShowType1.ShowTypeChanged += new IMLibrary4.Controls.ShowTypeControl.ShowTypeChangedHandler(this.ShowType1_ShowTypeChanged);
            this.ShowType1.ShowTypeExitApp += new IMLibrary4.Controls.ShowTypeControl.ShowTypeChangedHandler(this.ShowType1_ShowTypeExitApp);
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.Font = new System.Drawing.Font("宋体", 12F);
            this.txtPassword.Location = new System.Drawing.Point(85, 249);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(261, 23);
            this.txtPassword.TabIndex = 4;
            // 
            // cmbBoxUserID
            // 
            this.cmbBoxUserID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbBoxUserID.Font = new System.Drawing.Font("宋体", 12F);
            this.cmbBoxUserID.FormattingEnabled = true;
            this.cmbBoxUserID.Location = new System.Drawing.Point(83, 184);
            this.cmbBoxUserID.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBoxUserID.Name = "cmbBoxUserID";
            this.cmbBoxUserID.Size = new System.Drawing.Size(261, 28);
            this.cmbBoxUserID.TabIndex = 2;
            this.cmbBoxUserID.SelectedIndexChanged += new System.EventHandler(this.cmbBoxUserID_SelectedIndexChanged);
            // 
            // webUpdate1
            // 
            this.webUpdate1.BackColor = System.Drawing.Color.Transparent;
            this.webUpdate1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.webUpdate1.Location = new System.Drawing.Point(4, 635);
            this.webUpdate1.Margin = new System.Windows.Forms.Padding(5);
            this.webUpdate1.Name = "webUpdate1";
            this.webUpdate1.Size = new System.Drawing.Size(420, 12);
            this.webUpdate1.TabIndex = 34;
            this.webUpdate1.UrlFileLoad = "";
            this.webUpdate1.UrlVersion = "";
            this.webUpdate1.Visible = false;
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.ClientSize = new System.Drawing.Size(428, 651);
            this.CloseBoxSize = new System.Drawing.Size(30, 25);
            this.CloseDownBack = global::OurMsg.Properties.Resources.sysbtn_close_down;
            this.CloseMouseBack = global::OurMsg.Properties.Resources.sysbtn_close_hover;
            this.CloseNormlBack = global::OurMsg.Properties.Resources.sysbtn_close_normal;
            this.Controls.Add(this.butLogin);
            this.Controls.Add(this.butCancel);
            this.Controls.Add(this.skinLabel3);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.checkBoxAutoLogin);
            this.Controls.Add(this.checkBoxSavePassword);
            this.Controls.Add(this.skinPictureBox1);
            this.Controls.Add(this.webUpdate1);
            this.Controls.Add(this.ShowType1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.cmbBoxUserID);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(428, 651);
            this.MiniDownBack = global::OurMsg.Properties.Resources.sysbtn_min_down;
            this.MinimizeBox = true;
            this.MiniMouseBack = global::OurMsg.Properties.Resources.sysbtn_min_hover;
            this.MinimumSize = new System.Drawing.Size(428, 651);
            this.MiniNormlBack = global::OurMsg.Properties.Resources.sysbtn_min_normal;
            this.MiniSize = new System.Drawing.Size(30, 25);
            this.Name = "FormLogin";
            this.Radius = 3;
            this.SkinOpacity = 10D;
            this.Special = false;
            this.Text = "远程会诊平台";
            this.TitleOffset = new System.Drawing.Point(10, 0);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormLogin_FormClosing);
            this.Load += new System.EventHandler(this.FormLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.skinPictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbBoxUserID;
        private System.Windows.Forms.TextBox txtPassword;
        private IMLibrary4.Controls.ShowTypeControl ShowType1;
        private Controls.webUpdate webUpdate1;
        private CCWin.SkinControl.SkinPictureBox skinPictureBox1;
        private CCWin.SkinControl.SkinCheckBox checkBoxSavePassword;
        private CCWin.SkinControl.SkinCheckBox checkBoxAutoLogin;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinButton butCancel;
        private CCWin.SkinControl.SkinButton butLogin;
        private CCWin.SkinControl.SkinLabel skinLabel2;
    }
}