﻿namespace OurMsg
{
    partial class FormTalkUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTalkUser));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.MessagePanel1 = new OurMsg.Controls.MessagePanel();
            this.panelSendMsgBut = new System.Windows.Forms.Panel();
            this.butSend = new CCWin.SkinControl.SkinButton();
            this.butClose = new CCWin.SkinControl.SkinButton();
            this.panelDynamicInfo = new BSE.Windows.Forms.Panel();
            this.xPanderPanelList1 = new BSE.Windows.Forms.XPanderPanelList();
            this.xPanderPanel1 = new BSE.Windows.Forms.XPanderPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.xPanderPanel2 = new BSE.Windows.Forms.XPanderPanel();
            this.video1 = new OurMsg.Controls.video();
            this.xPanderPanel3 = new BSE.Windows.Forms.XPanderPanel();
            this.desketControl1 = new OurMsg.Controls.DesktopControl();
            this.panelTool = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsButSendFile = new System.Windows.Forms.ToolStripButton();
            this.tsButAV = new System.Windows.Forms.ToolStripButton();
            this.tsButRemote = new System.Windows.Forms.ToolStripButton();
            this.tsButRemoteTo = new System.Windows.Forms.ToolStripButton();
            this.ButUserData = new System.Windows.Forms.ToolStripButton();
            this.panelSendMsgBut.SuspendLayout();
            this.panelDynamicInfo.SuspendLayout();
            this.xPanderPanelList1.SuspendLayout();
            this.xPanderPanel1.SuspendLayout();
            this.xPanderPanel2.SuspendLayout();
            this.xPanderPanel3.SuspendLayout();
            this.panelTool.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MessagePanel1
            // 
            this.MessagePanel1.AllowDrop = true;
            this.MessagePanel1.BackColor = System.Drawing.Color.Transparent;
            this.MessagePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessagePanel1.Location = new System.Drawing.Point(4, 58);
            this.MessagePanel1.Margin = new System.Windows.Forms.Padding(5);
            this.MessagePanel1.Name = "MessagePanel1";
            this.MessagePanel1.Size = new System.Drawing.Size(562, 535);
            this.MessagePanel1.TabIndex = 37;
            // 
            // panelSendMsgBut
            // 
            this.panelSendMsgBut.BackColor = System.Drawing.Color.Transparent;
            this.panelSendMsgBut.Controls.Add(this.butSend);
            this.panelSendMsgBut.Controls.Add(this.butClose);
            this.panelSendMsgBut.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSendMsgBut.Location = new System.Drawing.Point(4, 593);
            this.panelSendMsgBut.Margin = new System.Windows.Forms.Padding(4);
            this.panelSendMsgBut.Name = "panelSendMsgBut";
            this.panelSendMsgBut.Size = new System.Drawing.Size(562, 41);
            this.panelSendMsgBut.TabIndex = 36;
            // 
            // butSend
            // 
            this.butSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butSend.BackColor = System.Drawing.Color.Transparent;
            this.butSend.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.butSend.DownBack = global::OurMsg.Properties.Resources.AddAccountBtn_Down;
            this.butSend.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.butSend.Location = new System.Drawing.Point(457, 4);
            this.butSend.Margin = new System.Windows.Forms.Padding(4);
            this.butSend.MouseBack = global::OurMsg.Properties.Resources.AddAccountBtn_mouseover;
            this.butSend.Name = "butSend";
            this.butSend.NormlBack = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.butSend.Size = new System.Drawing.Size(100, 35);
            this.butSend.TabIndex = 2;
            this.butSend.Text = "发送(&S)";
            this.butSend.UseVisualStyleBackColor = false;
            // 
            // butClose
            // 
            this.butClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.butClose.BackColor = System.Drawing.Color.Transparent;
            this.butClose.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.butClose.DownBack = global::OurMsg.Properties.Resources.AddAccountBtn_Down;
            this.butClose.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.butClose.Location = new System.Drawing.Point(351, 4);
            this.butClose.Margin = new System.Windows.Forms.Padding(4);
            this.butClose.MouseBack = global::OurMsg.Properties.Resources.AddAccountBtn_mouseover;
            this.butClose.Name = "butClose";
            this.butClose.NormlBack = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.butClose.Size = new System.Drawing.Size(100, 35);
            this.butClose.TabIndex = 2;
            this.butClose.Text = "关闭(&C)";
            this.butClose.UseVisualStyleBackColor = false;
            // 
            // panelDynamicInfo
            // 
            this.panelDynamicInfo.AssociatedSplitter = null;
            this.panelDynamicInfo.BackColor = System.Drawing.Color.Transparent;
            this.panelDynamicInfo.CaptionFont = new System.Drawing.Font("微软雅黑", 9F);
            this.panelDynamicInfo.CaptionHeight = 22;
            this.panelDynamicInfo.Controls.Add(this.xPanderPanelList1);
            this.panelDynamicInfo.CustomColors.BorderColor = System.Drawing.SystemColors.ControlText;
            this.panelDynamicInfo.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.panelDynamicInfo.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.panelDynamicInfo.CustomColors.CaptionGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panelDynamicInfo.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panelDynamicInfo.CustomColors.CaptionGradientMiddle = System.Drawing.SystemColors.ButtonFace;
            this.panelDynamicInfo.CustomColors.CaptionSelectedGradientBegin = System.Drawing.SystemColors.Window;
            this.panelDynamicInfo.CustomColors.CaptionSelectedGradientEnd = System.Drawing.SystemColors.Window;
            this.panelDynamicInfo.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.panelDynamicInfo.CustomColors.CollapsedCaptionText = System.Drawing.SystemColors.ControlText;
            this.panelDynamicInfo.CustomColors.ContentGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panelDynamicInfo.CustomColors.ContentGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panelDynamicInfo.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.panelDynamicInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelDynamicInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panelDynamicInfo.Image = ((System.Drawing.Image)(resources.GetObject("panelDynamicInfo.Image")));
            this.panelDynamicInfo.Location = new System.Drawing.Point(566, 58);
            this.panelDynamicInfo.Margin = new System.Windows.Forms.Padding(4);
            this.panelDynamicInfo.MinimumSize = new System.Drawing.Size(22, 22);
            this.panelDynamicInfo.Name = "panelDynamicInfo";
            this.panelDynamicInfo.PanelStyle = BSE.Windows.Forms.PanelStyle.Office2007;
            this.panelDynamicInfo.ShowExpandIcon = true;
            this.panelDynamicInfo.Size = new System.Drawing.Size(199, 576);
            this.panelDynamicInfo.TabIndex = 33;
            this.panelDynamicInfo.Text = "动态信息";
            this.panelDynamicInfo.ToolTipTextCloseIcon = null;
            this.panelDynamicInfo.ToolTipTextExpandIconPanelCollapsed = null;
            this.panelDynamicInfo.ToolTipTextExpandIconPanelExpanded = null;
            // 
            // xPanderPanelList1
            // 
            this.xPanderPanelList1.CaptionStyle = BSE.Windows.Forms.CaptionStyle.Normal;
            this.xPanderPanelList1.Controls.Add(this.xPanderPanel1);
            this.xPanderPanelList1.Controls.Add(this.xPanderPanel2);
            this.xPanderPanelList1.Controls.Add(this.xPanderPanel3);
            this.xPanderPanelList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xPanderPanelList1.GradientBackground = System.Drawing.Color.Empty;
            this.xPanderPanelList1.Location = new System.Drawing.Point(0, 23);
            this.xPanderPanelList1.Margin = new System.Windows.Forms.Padding(4);
            this.xPanderPanelList1.Name = "xPanderPanelList1";
            this.xPanderPanelList1.PanelColors = null;
            this.xPanderPanelList1.Size = new System.Drawing.Size(199, 552);
            this.xPanderPanelList1.TabIndex = 3;
            this.xPanderPanelList1.Text = "xPanderPanelList1";
            // 
            // xPanderPanel1
            // 
            this.xPanderPanel1.CaptionFont = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Bold);
            this.xPanderPanel1.Controls.Add(this.flowLayoutPanel1);
            this.xPanderPanel1.CustomColors.BackColor = System.Drawing.SystemColors.Control;
            this.xPanderPanel1.CustomColors.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.xPanderPanel1.CustomColors.CaptionCheckedGradientBegin = System.Drawing.Color.Empty;
            this.xPanderPanel1.CustomColors.CaptionCheckedGradientEnd = System.Drawing.Color.Empty;
            this.xPanderPanel1.CustomColors.CaptionCheckedGradientMiddle = System.Drawing.Color.Empty;
            this.xPanderPanel1.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel1.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel1.CustomColors.CaptionGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.xPanderPanel1.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.xPanderPanel1.CustomColors.CaptionGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.xPanderPanel1.CustomColors.CaptionPressedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel1.CustomColors.CaptionPressedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel1.CustomColors.CaptionPressedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel1.CustomColors.CaptionSelectedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel1.CustomColors.CaptionSelectedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel1.CustomColors.CaptionSelectedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel1.CustomColors.CaptionSelectedText = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel1.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel1.CustomColors.FlatCaptionGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.xPanderPanel1.CustomColors.FlatCaptionGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.xPanderPanel1.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.xPanderPanel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel1.Image = null;
            this.xPanderPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.xPanderPanel1.Name = "xPanderPanel1";
            this.xPanderPanel1.Size = new System.Drawing.Size(199, 25);
            this.xPanderPanel1.TabIndex = 0;
            this.xPanderPanel1.Text = "文件传输";
            this.xPanderPanel1.ToolTipTextCloseIcon = null;
            this.xPanderPanel1.ToolTipTextExpandIconPanelCollapsed = null;
            this.xPanderPanel1.ToolTipTextExpandIconPanelExpanded = null;
            this.xPanderPanel1.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FloralWhite;
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1, 25);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(197, 0);
            this.flowLayoutPanel1.TabIndex = 3;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // xPanderPanel2
            // 
            this.xPanderPanel2.CaptionFont = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Bold);
            this.xPanderPanel2.Controls.Add(this.video1);
            this.xPanderPanel2.CustomColors.BackColor = System.Drawing.SystemColors.Control;
            this.xPanderPanel2.CustomColors.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.xPanderPanel2.CustomColors.CaptionCheckedGradientBegin = System.Drawing.Color.Empty;
            this.xPanderPanel2.CustomColors.CaptionCheckedGradientEnd = System.Drawing.Color.Empty;
            this.xPanderPanel2.CustomColors.CaptionCheckedGradientMiddle = System.Drawing.Color.Empty;
            this.xPanderPanel2.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel2.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel2.CustomColors.CaptionGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.xPanderPanel2.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.xPanderPanel2.CustomColors.CaptionGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.xPanderPanel2.CustomColors.CaptionPressedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel2.CustomColors.CaptionPressedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel2.CustomColors.CaptionPressedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel2.CustomColors.CaptionSelectedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel2.CustomColors.CaptionSelectedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel2.CustomColors.CaptionSelectedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel2.CustomColors.CaptionSelectedText = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel2.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel2.CustomColors.FlatCaptionGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.xPanderPanel2.CustomColors.FlatCaptionGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.xPanderPanel2.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.xPanderPanel2.Expand = true;
            this.xPanderPanel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel2.Image = null;
            this.xPanderPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.xPanderPanel2.Name = "xPanderPanel2";
            this.xPanderPanel2.Size = new System.Drawing.Size(199, 502);
            this.xPanderPanel2.TabIndex = 1;
            this.xPanderPanel2.Text = "视频对话";
            this.xPanderPanel2.ToolTipTextCloseIcon = null;
            this.xPanderPanel2.ToolTipTextExpandIconPanelCollapsed = null;
            this.xPanderPanel2.ToolTipTextExpandIconPanelExpanded = null;
            this.xPanderPanel2.Visible = false;
            // 
            // video1
            // 
            this.video1.BackColor = System.Drawing.Color.White;
            this.video1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.video1.IsInvite = false;
            this.video1.Location = new System.Drawing.Point(1, 25);
            this.video1.Margin = new System.Windows.Forms.Padding(5);
            this.video1.Name = "video1";
            this.video1.Size = new System.Drawing.Size(197, 477);
            this.video1.TabIndex = 1;
            // 
            // xPanderPanel3
            // 
            this.xPanderPanel3.CaptionFont = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Bold);
            this.xPanderPanel3.Controls.Add(this.desketControl1);
            this.xPanderPanel3.CustomColors.BackColor = System.Drawing.SystemColors.Control;
            this.xPanderPanel3.CustomColors.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(184)))), ((int)(((byte)(184)))));
            this.xPanderPanel3.CustomColors.CaptionCheckedGradientBegin = System.Drawing.Color.Empty;
            this.xPanderPanel3.CustomColors.CaptionCheckedGradientEnd = System.Drawing.Color.Empty;
            this.xPanderPanel3.CustomColors.CaptionCheckedGradientMiddle = System.Drawing.Color.Empty;
            this.xPanderPanel3.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel3.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel3.CustomColors.CaptionGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.xPanderPanel3.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.xPanderPanel3.CustomColors.CaptionGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.xPanderPanel3.CustomColors.CaptionPressedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel3.CustomColors.CaptionPressedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel3.CustomColors.CaptionPressedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.xPanderPanel3.CustomColors.CaptionSelectedGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel3.CustomColors.CaptionSelectedGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel3.CustomColors.CaptionSelectedGradientMiddle = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.xPanderPanel3.CustomColors.CaptionSelectedText = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel3.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel3.CustomColors.FlatCaptionGradientBegin = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.xPanderPanel3.CustomColors.FlatCaptionGradientEnd = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.xPanderPanel3.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.xPanderPanel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xPanderPanel3.Image = null;
            this.xPanderPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.xPanderPanel3.Name = "xPanderPanel3";
            this.xPanderPanel3.Size = new System.Drawing.Size(199, 25);
            this.xPanderPanel3.TabIndex = 2;
            this.xPanderPanel3.Text = "远程协助";
            this.xPanderPanel3.ToolTipTextCloseIcon = null;
            this.xPanderPanel3.ToolTipTextExpandIconPanelCollapsed = null;
            this.xPanderPanel3.ToolTipTextExpandIconPanelExpanded = null;
            this.xPanderPanel3.Visible = false;
            // 
            // desketControl1
            // 
            this.desketControl1.BackColor = System.Drawing.Color.FloralWhite;
            this.desketControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.desketControl1.IsAllowControl = false;
            this.desketControl1.IsControler = false;
            this.desketControl1.Location = new System.Drawing.Point(1, 25);
            this.desketControl1.Margin = new System.Windows.Forms.Padding(5);
            this.desketControl1.Name = "desketControl1";
            this.desketControl1.Size = new System.Drawing.Size(197, 0);
            this.desketControl1.TabIndex = 0;
            // 
            // panelTool
            // 
            this.panelTool.BackColor = System.Drawing.Color.Transparent;
            this.panelTool.Controls.Add(this.toolStrip1);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTool.Location = new System.Drawing.Point(4, 28);
            this.panelTool.Margin = new System.Windows.Forms.Padding(4);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(761, 30);
            this.panelTool.TabIndex = 32;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsButSendFile,
            this.tsButAV,
            this.tsButRemote,
            this.tsButRemoteTo,
            this.ButUserData});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(761, 31);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsButSendFile
            // 
            this.tsButSendFile.AutoSize = false;
            this.tsButSendFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButSendFile.Image = ((System.Drawing.Image)(resources.GetObject("tsButSendFile.Image")));
            this.tsButSendFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButSendFile.Name = "tsButSendFile";
            this.tsButSendFile.Size = new System.Drawing.Size(32, 28);
            this.tsButSendFile.Text = "发送文件 ";
            // 
            // tsButAV
            // 
            this.tsButAV.AutoSize = false;
            this.tsButAV.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButAV.Image = ((System.Drawing.Image)(resources.GetObject("tsButAV.Image")));
            this.tsButAV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButAV.Name = "tsButAV";
            this.tsButAV.Size = new System.Drawing.Size(32, 28);
            this.tsButAV.Text = "视频对话";
            // 
            // tsButRemote
            // 
            this.tsButRemote.AutoSize = false;
            this.tsButRemote.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButRemote.Image = global::OurMsg.Properties.Resources.romSet;
            this.tsButRemote.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButRemote.Name = "tsButRemote";
            this.tsButRemote.Size = new System.Drawing.Size(32, 28);
            this.tsButRemote.Text = "邀请对方远程协助";
            // 
            // tsButRemoteTo
            // 
            this.tsButRemoteTo.AutoSize = false;
            this.tsButRemoteTo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButRemoteTo.Image = ((System.Drawing.Image)(resources.GetObject("tsButRemoteTo.Image")));
            this.tsButRemoteTo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButRemoteTo.Name = "tsButRemoteTo";
            this.tsButRemoteTo.Size = new System.Drawing.Size(32, 28);
            this.tsButRemoteTo.Text = "请求控制对方电脑";
            this.tsButRemoteTo.Visible = false;
            // 
            // ButUserData
            // 
            this.ButUserData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButUserData.Image = ((System.Drawing.Image)(resources.GetObject("ButUserData.Image")));
            this.ButUserData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButUserData.Name = "ButUserData";
            this.ButUserData.Size = new System.Drawing.Size(28, 28);
            this.ButUserData.Visible = false;
            // 
            // FormTalkUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.ClientSize = new System.Drawing.Size(769, 638);
            this.Controls.Add(this.MessagePanel1);
            this.Controls.Add(this.panelSendMsgBut);
            this.Controls.Add(this.panelDynamicInfo);
            this.Controls.Add(this.panelTool);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = true;
            this.MinimizeBox = true;
            this.MinimumSize = new System.Drawing.Size(760, 575);
            this.Name = "FormTalkUser";
            this.Text = "与某某(10000000)对话";
            this.Load += new System.EventHandler(this.FormTalkUser_Load);
            this.panelSendMsgBut.ResumeLayout(false);
            this.panelDynamicInfo.ResumeLayout(false);
            this.xPanderPanelList1.ResumeLayout(false);
            this.xPanderPanel1.ResumeLayout(false);
            this.xPanderPanel2.ResumeLayout(false);
            this.xPanderPanel3.ResumeLayout(false);
            this.panelTool.ResumeLayout(false);
            this.panelTool.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private BSE.Windows.Forms.Panel panelDynamicInfo;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsButSendFile;
        private System.Windows.Forms.ToolStripButton tsButAV;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panelSendMsgBut;
        private Controls.MessagePanel MessagePanel1;
        private System.Windows.Forms.ToolStripButton ButUserData;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private BSE.Windows.Forms.XPanderPanelList xPanderPanelList1;
        private BSE.Windows.Forms.XPanderPanel xPanderPanel1;
        private BSE.Windows.Forms.XPanderPanel xPanderPanel2;
        private Controls.video video1;
        private BSE.Windows.Forms.XPanderPanel xPanderPanel3;
        private Controls.DesktopControl desketControl1;
        private System.Windows.Forms.ToolStripButton tsButRemote;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panelTool;
        private CCWin.SkinControl.SkinButton butClose;
        private CCWin.SkinControl.SkinButton butSend;
        private System.Windows.Forms.ToolStripButton tsButRemoteTo;

    }
}