﻿namespace OurMsg
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuNotify = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.fsfsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.联机ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.接听电话ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.外出就餐ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.暂时离开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.工作中ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vcxvToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemOpenMain = new System.Windows.Forms.ToolStripMenuItem();
            this.vcxvToolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tButManageMsg = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.labUserName = new System.Windows.Forms.Label();
            this.ShowType1 = new IMLibrary4.Controls.ShowTypeControl();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmMyInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMsgMis = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmLogOut = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmClose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnMail = new System.Windows.Forms.ToolStripButton();
            this.tsBtnEnclosure = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.selfFaceImage1 = new OurMsg.Controls.SelfFaceImage();
            this.userLoginPanel1 = new OurMsg.Controls.UserLoginPanel();
            this.controlPanel1 = new OurMsg.Controls.ControlPanel();
            this.contextMenuNotify.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.ContextMenuStrip = this.contextMenuNotify;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "OurMsg";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuNotify
            // 
            this.contextMenuNotify.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuNotify.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fsfsToolStripMenuItem,
            this.vcxvToolStripMenuItem,
            this.MenuItemOpenMain,
            this.vcxvToolStripMenuItem3,
            this.MenuItemExit});
            this.contextMenuNotify.Name = "cMenuNotify";
            this.contextMenuNotify.Size = new System.Drawing.Size(160, 94);
            this.contextMenuNotify.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuNotify_ItemClicked);
            // 
            // fsfsToolStripMenuItem
            // 
            this.fsfsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.联机ToolStripMenuItem,
            this.接听电话ToolStripMenuItem,
            this.外出就餐ToolStripMenuItem,
            this.暂时离开ToolStripMenuItem,
            this.工作中ToolStripMenuItem});
            this.fsfsToolStripMenuItem.Name = "fsfsToolStripMenuItem";
            this.fsfsToolStripMenuItem.Size = new System.Drawing.Size(159, 26);
            this.fsfsToolStripMenuItem.Text = "我的状态";
            this.fsfsToolStripMenuItem.Visible = false;
            // 
            // 联机ToolStripMenuItem
            // 
            this.联机ToolStripMenuItem.Checked = true;
            this.联机ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.联机ToolStripMenuItem.Name = "联机ToolStripMenuItem";
            this.联机ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.联机ToolStripMenuItem.Text = "联机";
            // 
            // 接听电话ToolStripMenuItem
            // 
            this.接听电话ToolStripMenuItem.Name = "接听电话ToolStripMenuItem";
            this.接听电话ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.接听电话ToolStripMenuItem.Text = "接听电话";
            // 
            // 外出就餐ToolStripMenuItem
            // 
            this.外出就餐ToolStripMenuItem.Name = "外出就餐ToolStripMenuItem";
            this.外出就餐ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.外出就餐ToolStripMenuItem.Text = "外出就餐";
            // 
            // 暂时离开ToolStripMenuItem
            // 
            this.暂时离开ToolStripMenuItem.Name = "暂时离开ToolStripMenuItem";
            this.暂时离开ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.暂时离开ToolStripMenuItem.Text = "暂时离开";
            // 
            // 工作中ToolStripMenuItem
            // 
            this.工作中ToolStripMenuItem.Name = "工作中ToolStripMenuItem";
            this.工作中ToolStripMenuItem.Size = new System.Drawing.Size(144, 26);
            this.工作中ToolStripMenuItem.Text = "工作中";
            // 
            // vcxvToolStripMenuItem
            // 
            this.vcxvToolStripMenuItem.Name = "vcxvToolStripMenuItem";
            this.vcxvToolStripMenuItem.Size = new System.Drawing.Size(156, 6);
            this.vcxvToolStripMenuItem.Visible = false;
            // 
            // MenuItemOpenMain
            // 
            this.MenuItemOpenMain.Name = "MenuItemOpenMain";
            this.MenuItemOpenMain.Size = new System.Drawing.Size(159, 26);
            this.MenuItemOpenMain.Text = "打开主面板";
            // 
            // vcxvToolStripMenuItem3
            // 
            this.vcxvToolStripMenuItem3.Name = "vcxvToolStripMenuItem3";
            this.vcxvToolStripMenuItem3.Size = new System.Drawing.Size(156, 6);
            // 
            // MenuItemExit
            // 
            this.MenuItemExit.BackColor = System.Drawing.Color.Transparent;
            this.MenuItemExit.Name = "MenuItemExit";
            this.MenuItemExit.Size = new System.Drawing.Size(159, 26);
            this.MenuItemExit.Text = "退出";
            // 
            // timer1
            // 
            this.timer1.Interval = 80000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tButManageMsg,
            this.toolStripButton1});
            this.toolStrip2.Location = new System.Drawing.Point(4, 28);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(32, 588);
            this.toolStrip2.TabIndex = 26;
            this.toolStrip2.Text = "toolStrip2";
            this.toolStrip2.Visible = false;
            // 
            // tButManageMsg
            // 
            this.tButManageMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tButManageMsg.Image = ((System.Drawing.Image)(resources.GetObject("tButManageMsg.Image")));
            this.tButManageMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tButManageMsg.Name = "tButManageMsg";
            this.tButManageMsg.Size = new System.Drawing.Size(22, 24);
            this.tButManageMsg.Text = "toolStripButton4";
            this.tButManageMsg.ToolTipText = "信息管理器";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(22, 24);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // labUserName
            // 
            this.labUserName.AutoSize = true;
            this.labUserName.BackColor = System.Drawing.Color.Transparent;
            this.labUserName.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labUserName.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labUserName.Location = new System.Drawing.Point(100, 71);
            this.labUserName.Name = "labUserName";
            this.labUserName.Size = new System.Drawing.Size(132, 15);
            this.labUserName.TabIndex = 23;
            this.labUserName.Text = "某某某(10000000)";
            this.labUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ShowType1
            // 
            this.ShowType1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ShowType1.BackColor = System.Drawing.Color.Transparent;
            this.ShowType1.IsShowOffline = true;
            this.ShowType1.Location = new System.Drawing.Point(98, 14);
            this.ShowType1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ShowType1.Name = "ShowType1";
            this.ShowType1.Size = new System.Drawing.Size(151, 26);
            this.ShowType1.State = IMLibrary4.Protocol.ShowType.NONE;
            this.ShowType1.TabIndex = 24;
            this.ShowType1.ShowTypeChanged += new IMLibrary4.Controls.ShowTypeControl.ShowTypeChangedHandler(this.ShowType1_ShowTypeChanged);
            this.ShowType1.ShowTypeExitApp += new IMLibrary4.Controls.ShowTypeControl.ShowTypeChangedHandler(this.ShowType1_ShowTypeExitApp);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.BackColor = System.Drawing.Color.Transparent;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(4, 637);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip1.Size = new System.Drawing.Size(357, 30);
            this.statusStrip1.TabIndex = 24;
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmMyInfo,
            this.tsmMsgMis,
            this.toolStripMenuItem1,
            this.tsmLogOut,
            this.tsmClose});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.ShowDropDownArrow = false;
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(41, 28);
            this.toolStripDropDownButton1.Text = " ";
            // 
            // tsmMyInfo
            // 
            this.tsmMyInfo.Name = "tsmMyInfo";
            this.tsmMyInfo.Size = new System.Drawing.Size(174, 26);
            this.tsmMyInfo.Text = "个人信息设置";
            this.tsmMyInfo.Click += new System.EventHandler(this.tsmMyInfo_Click);
            // 
            // tsmMsgMis
            // 
            this.tsmMsgMis.Name = "tsmMsgMis";
            this.tsmMsgMis.Size = new System.Drawing.Size(174, 26);
            this.tsmMsgMis.Text = "消息管理器";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(171, 6);
            // 
            // tsmLogOut
            // 
            this.tsmLogOut.Name = "tsmLogOut";
            this.tsmLogOut.Size = new System.Drawing.Size(174, 26);
            this.tsmLogOut.Text = "注销";
            this.tsmLogOut.ToolTipText = "切换账号";
            this.tsmLogOut.Click += new System.EventHandler(this.tsmLogOut_Click);
            // 
            // tsmClose
            // 
            this.tsmClose.BackColor = System.Drawing.SystemColors.Control;
            this.tsmClose.Name = "tsmClose";
            this.tsmClose.Size = new System.Drawing.Size(174, 26);
            this.tsmClose.Text = "退出";
            this.tsmClose.Click += new System.EventHandler(this.tsmClose_Click);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 25);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnMail,
            this.tsBtnEnclosure});
            this.toolStrip1.Location = new System.Drawing.Point(4, 119);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(357, 29);
            this.toolStrip1.TabIndex = 28;
            // 
            // tsBtnMail
            // 
            this.tsBtnMail.AutoSize = false;
            this.tsBtnMail.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tsBtnMail.Image = global::OurMsg.Properties.Resources.mail_1;
            this.tsBtnMail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnMail.Name = "tsBtnMail";
            this.tsBtnMail.Size = new System.Drawing.Size(40, 28);
            this.tsBtnMail.Text = "0";
            this.tsBtnMail.ToolTipText = "邮箱";
            this.tsBtnMail.Click += new System.EventHandler(this.tsBtnMail_Click);
            // 
            // tsBtnEnclosure
            // 
            this.tsBtnEnclosure.AutoSize = false;
            this.tsBtnEnclosure.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tsBtnEnclosure.ForeColor = System.Drawing.SystemColors.MenuText;
            this.tsBtnEnclosure.Image = global::OurMsg.Properties.Resources.mail_attachment;
            this.tsBtnEnclosure.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnEnclosure.Name = "tsBtnEnclosure";
            this.tsBtnEnclosure.Size = new System.Drawing.Size(40, 28);
            this.tsBtnEnclosure.Text = "0";
            this.tsBtnEnclosure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tsBtnEnclosure.ToolTipText = "离线文件管理";
            this.tsBtnEnclosure.Click += new System.EventHandler(this.tsBtnEnclosure_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.selfFaceImage1);
            this.panel1.Controls.Add(this.ShowType1);
            this.panel1.Controls.Add(this.labUserName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(4, 28);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(357, 91);
            this.panel1.TabIndex = 30;
            // 
            // selfFaceImage1
            // 
            this.selfFaceImage1.BackColor = System.Drawing.Color.Transparent;
            this.selfFaceImage1.Location = new System.Drawing.Point(0, 7);
            this.selfFaceImage1.Margin = new System.Windows.Forms.Padding(4);
            this.selfFaceImage1.Name = "selfFaceImage1";
            this.selfFaceImage1.Size = new System.Drawing.Size(77, 77);
            this.selfFaceImage1.TabIndex = 25;
            this.selfFaceImage1.ImageClick += new OurMsg.Controls.SelfFaceImage.EventHandler(this.selfFaceImage1_ImageClick);
            // 
            // userLoginPanel1
            // 
            this.userLoginPanel1.BackColor = System.Drawing.Color.Transparent;
            this.userLoginPanel1.Location = new System.Drawing.Point(0, 0);
            this.userLoginPanel1.Name = "userLoginPanel1";
            this.userLoginPanel1.Size = new System.Drawing.Size(11, 11);
            this.userLoginPanel1.TabIndex = 28;
            this.userLoginPanel1.CancelLogin += new OurMsg.Controls.UserLoginPanel.CancleEventHandler(this.userLoginPanel1_CancelLogin);
            // 
            // controlPanel1
            // 
            this.controlPanel1.BackColor = System.Drawing.Color.Transparent;
            this.controlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlPanel1.ForeColor = System.Drawing.Color.Transparent;
            this.controlPanel1.Location = new System.Drawing.Point(4, 148);
            this.controlPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.controlPanel1.Name = "controlPanel1";
            this.controlPanel1.Size = new System.Drawing.Size(357, 489);
            this.controlPanel1.TabIndex = 29;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.BackRectangle = new System.Drawing.Rectangle(5, 5, 10, 10);
            this.BorderPalace = ((System.Drawing.Image)(resources.GetObject("$this.BorderPalace")));
            this.ClientSize = new System.Drawing.Size(365, 671);
            this.CloseBoxSize = new System.Drawing.Size(33, 24);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.userLoginPanel1);
            this.Controls.Add(this.controlPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaxSize = new System.Drawing.Size(33, 24);
            this.MinimizeBox = true;
            this.MinimumSize = new System.Drawing.Size(330, 600);
            this.MiniSize = new System.Drawing.Size(33, 24);
            this.Name = "FormMain";
            this.Text = "远程会诊平台";
            this.Resize += new System.EventHandler(this.FormMain_Resize);
            this.contextMenuNotify.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labUserName;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tButManageMsg;
        private IMLibrary4.Controls.ShowTypeControl ShowType1;
        private OurMsg.Controls.SelfFaceImage selfFaceImage1;
        private OurMsg.Controls.ControlPanel controlPanel1;
        private Controls.UserLoginPanel userLoginPanel1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ContextMenuStrip contextMenuNotify;
        private System.Windows.Forms.ToolStripMenuItem fsfsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 联机ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 接听电话ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 外出就餐ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 暂时离开ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 工作中ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator vcxvToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuItemOpenMain;
        private System.Windows.Forms.ToolStripSeparator vcxvToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem MenuItemExit;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton tsBtnEnclosure;
        private System.Windows.Forms.ToolStripButton tsBtnMail;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem tsmMyInfo;
        private System.Windows.Forms.ToolStripMenuItem tsmMsgMis;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmLogOut;
        private System.Windows.Forms.ToolStripMenuItem tsmClose;
        private System.Windows.Forms.Panel panel1;
    }
}

