﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using IMLibrary4;
using IMLibrary4.Protocol;
using IMLibrary4.Net ;
using IMLibrary4.Controls;

namespace OurMsg
{
    public partial class FormTalkUser : BaseForm
    {
        #region 构造函数
        internal FormTalkUser(IMLibrary4.Organization.User User)
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(FormTalkUser_FormClosing);
            tsButSendFile.Click += new EventHandler(tsButSendFile_Click);

            tsButRemote.Click += new EventHandler(tsButRemote_Click);
            tsButRemoteTo.Click += new EventHandler(tsButRemoteTo_Click);
            desketControl1.Cancel += new OurMsg.Controls.DesktopControl.CancelEventHandler(desketControl1_Cancel);
            desketControl1.GetIPEndPoint += new  Controls.DesktopControl.GetIPEndPointEventHandler(desketControl1_GetIPEndPoint);

            tsButAV.Click += new EventHandler(tsButAV_Click);
            video1.Cancel += new OurMsg.Controls.video.CancelEventHandler(video1_Cancel);
            video1.GetIPEndPoint += new OurMsg.Controls.video.GetIPEndPointEventHandler(video1_GetIPEndPoint);
            flowLayoutPanel1.ControlAdded += new ControlEventHandler(flowLayoutPanel1_ControlAdded);
            flowLayoutPanel1.ControlRemoved += new ControlEventHandler(flowLayoutPanel1_ControlRemoved);
            this.butClose.Click +=new EventHandler(butClose_Click);
            this.butSend.Click +=new EventHandler(butSend_Click);

            this.MessagePanel1.CreateMsgAfter += new OurMsg.Controls.MessagePanel.CreateMsgEventHandler(MessagePanel1_CreateMsgAfter);
            this.MessagePanel1.DragDrop += new DragEventHandler(MessagePanel1_DragDrop);
            this.MessagePanel1.DragEnter += new DragEventHandler(MessagePanel1_DragEnter);
            this.MessagePanel1.SendFile += delegate(object sender, string filename) { sendFile(filename); };
            this.MessagePanel1.MsgAway += new OurMsg.Controls.MessagePanel.MsgAwayEventHandler(MessagePanel1_OpenAway);
            this.MessagePanel1.SendNullMsg += delegate(string msg) { toolTip1.Show(msg, this, 0, this.Height-20); };
            this.Tag = User.UserID;
            this.User = User;

            RefreshPanelDynamicInfoWidth();
        }
        #endregion
         
        #region 变量

        /// <summary>
        /// 对方信息
        /// </summary>
        public IMLibrary4.Organization.User User = null;

        string _myUserID = "";
        /// <summary>
        /// 用户ID
        /// </summary>
        public string myUserID
        {
            set
            {
                _myUserID = value;
                MessagePanel1.myUserID = value;
            }
            get { return _myUserID; }
        }
        /// <summary>
        /// 用户名
        /// </summary>
        public string myUserName { get; set; }

        /// <summary>
        /// 显示控制对方电脑按钮
        /// </summary>
        bool _IsRemoteControl = false;
        public bool IsRemoteControl
        {
            set 
            {
                _IsRemoteControl = value;
                tsButRemoteTo.Visible = value;
            }
            get { return _IsRemoteControl; }
        }

        /// <summary>
        /// 跳过用户直接允许控制
        /// </summary>
        bool _IsAllowControl = false;
        public bool IsAllowControl
        {
            set
            {
                _IsAllowControl = value;
                desketControl1.IsAllowControl = value;//是否直接允许控制
            }
            get { return _IsRemoteControl; }
        }

        #endregion

        #region 事件
        /// <summary>
        /// 发送事件代理
        /// </summary>
        /// <param name="Msg">要发送的消息</param>
        /// <param name="userID">接收消息的用户帐号</param>
        public delegate void SendMsgEventHandler(IMLibrary4.Protocol.Element  e, IMLibrary4.Organization.User User);

        /// <summary>
        /// 发送消息事件
        /// </summary>
        public event SendMsgEventHandler SendMsgToUser;

        #endregion 

        #region 视频
        //音视频通信端口获取事件
        void video1_GetIPEndPoint(object sender,  RtpEventArgs e)
        {
            IMLibrary4.Protocol.AVMsg msg = new IMLibrary4.Protocol.AVMsg();
            msg.type = IMLibrary4.Protocol.type.set;
            msg.LocalIP =e.LocalIP;
            msg.RemoteIP = e.RemoteIP;
            msg.LocalRtpPort = e.LocalRtpPort;
            msg.LocalRtcpPort = e.LocalRtcpPort;
            msg.RemoteRtpPort =e.RemoteRtpPort;
            msg.RemoteRtcpPort = e.RemoteRtcpPort;
            if (SendMsgToUser != null)//触发消息发送事件
                SendMsgToUser(msg, User);
        } 

        void video1_Cancel(object sender)
        {
            CancelAV(true);//自己取消

            //发送取消
            IMLibrary4.Protocol.AVMsg msg = new IMLibrary4.Protocol.AVMsg();
            msg.type = IMLibrary4.Protocol.type.cancel;

            if (SendMsgToUser != null)//触发视频消息事件
                SendMsgToUser(msg, User);
        }


        #region 视频单击事件
        void tsButAV_Click(object sender, EventArgs e)
        {
            ReadyAV(true);//邀请对方并准备视频

            //发送邀请
            IMLibrary4.Protocol.AVMsg msg = new IMLibrary4.Protocol.AVMsg();
            msg.type = IMLibrary4.Protocol.type.New;

            if (SendMsgToUser != null)//触发视频消息事件
                SendMsgToUser(msg, User);
        }
        #endregion

        /// <summary>
        /// 准备视频
        /// </summary>
        /// <param name="IsInvite">是否邀请方</param>
        public void ReadyAV(bool IsInvite)
        {
            video1.IsInvite = IsInvite;
            tsButAV.Enabled = false;

            foreach (BSE.Windows.Forms.XPanderPanel xpanel in xPanderPanelList1.XPanderPanels)
                xpanel.Expand = false;
          
            xPanderPanel2.Visible = true;
            xPanderPanel2.Expand = true;
            xPanderPanelList1.Refresh();

            RefreshPanelDynamicInfoWidth();
        }

        private bool xPanderPanel1VisibleState = false;
        /// <summary>
        /// 重设面板宽度
        /// </summary>
        private void RefreshPanelDynamicInfoWidth()
        {
            panelDynamicInfo.Width = 152;

            if (xPanderPanel1VisibleState == true)
            {
                panelDynamicInfo.Width = 245;
                //this.Width += 95;
            }
            if (xPanderPanel2.Visible == true)
            {
                panelDynamicInfo.Width = 345;
                //this.Width += 195;
            }
            panelDynamicInfo.Refresh();
        }

        /// <summary>
        /// 取消视频
        /// </summary>
        /// <param name="IsSelf">是否自己取消</param>
        public void CancelAV(bool IsSelf)
        {
            if (!IsSelf)
                this.video1.CancelAV();//释放视频资源

            tsButAV.Enabled = true;
            xPanderPanel2.Visible = false;
            RefreshPanelDynamicInfoWidth();
            xPanderPanelList1.Refresh();
        }

        #region 设置视频对话远程主机信息
        /// <summary>
        /// 设置视频对话远程主机信息
        /// </summary>
        public void setAVRometEP(IMLibrary4.Protocol.AVMsg msg)
        {
            video1.SetRometEP(msg);
        }
        #endregion

        #endregion

        #region 远程协助

        void desketControl1_GetIPEndPoint(object sender,IPEndPoint local, IPEndPoint remote, int ScreenWidth, int ScreenHeigth)
        {
            IMLibrary4.Protocol.RemoteDesktopMsg msg = new IMLibrary4.Protocol.RemoteDesktopMsg();
            msg.type = IMLibrary4.Protocol.type.set;
            msg.LocalIP = local.Address;
            msg.LocalPort = local.Port;
            msg.remoteIP = remote.Address;
            msg.remotePort = remote.Port; 
            msg.ScreenWidth = ScreenWidth;//发送屏幕宽度
            msg.ScreenHeight =ScreenHeigth;//发送屏幕高度
            if (SendMsgToUser != null)//触发消息发送事件
                SendMsgToUser(msg, User);
        }

        void desketControl1_Cancel(object sender)
        {
            CancelRemoteDesktop(true);//我方取消

            //发送取消
            IMLibrary4.Protocol.RemoteDesktopMsg msg = new IMLibrary4.Protocol.RemoteDesktopMsg();
            msg.type = IMLibrary4.Protocol.type.cancel;

            if (SendMsgToUser != null)//触发视频消息事件
                SendMsgToUser(msg, User);
        }

        /// <summary>
        /// 准备远程桌面通信
        /// </summary>
        /// <param name="IsControler">是否受控方</param>
        public void ReadyRemoteDesktop(bool IsControler)
        {
            desketControl1.IsControler = IsControler;            
            tsButRemote.Enabled = false;
            tsButRemoteTo.Enabled = false;

            foreach (BSE.Windows.Forms.XPanderPanel xpanel in xPanderPanelList1.XPanderPanels)
                xpanel.Expand = false;
            xPanderPanel3.Visible = true;
            xPanderPanel3.Expand = true;
            xPanderPanelList1.Refresh();
        }

        /// <summary>
        /// 设置远程协助对方远程主机信息
        /// </summary>
        public void setRemoteDesktopEP(IMLibrary4.Protocol.RemoteDesktopMsg msg)
        {
            desketControl1.SetRometEP(msg,User.UserName +"("+ User.UserID+ ")的桌面");
        }

        /// <summary>
        /// 邀请远程协助
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tsButRemote_Click(object sender, EventArgs e)
        {
            #region 控件设置
            ReadyRemoteDesktop(false);
            #endregion

            //发送邀请
            IMLibrary4.Protocol.RemoteDesktopMsg msg = new IMLibrary4.Protocol.RemoteDesktopMsg();
            msg.type = IMLibrary4.Protocol.type.New;

            if (SendMsgToUser != null)//触发视频消息事件
                SendMsgToUser(msg, User);

        }

        /// <summary>
        /// 请求控制对方电脑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tsButRemoteTo_Click(object sender, EventArgs e)
        {
            #region 控件设置
            ReadyRemoteDesktop(true);
            #endregion

            //发送邀请
            IMLibrary4.Protocol.RemoteDesktopMsg msg = new IMLibrary4.Protocol.RemoteDesktopMsg();
            msg.type = IMLibrary4.Protocol.type.get;

            if (SendMsgToUser != null)//触发远程消息事件
                SendMsgToUser(msg, User);

        }

        /// <summary>
        /// 取消远程桌面
        /// </summary>
        /// <param name="IsSelf">是否自己取消</param>
        public void CancelRemoteDesktop(bool IsSelf)
        {
            if (!IsSelf)//如果是对方取消
                this.desketControl1.Close();//释放视频资源
            try
            {
                tsButRemote.Enabled = true;
                tsButRemoteTo.Enabled = true;
                xPanderPanel3.Visible = false;
                RefreshPanelDynamicInfoWidth();
                xPanderPanelList1.Refresh();
            }
            catch { }
        }
        #endregion

        #region 文件传输面板控件事件

        void flowLayoutPanel1_ControlRemoved(object sender, ControlEventArgs e)
        {
            if (flowLayoutPanel1.Controls.Count == 0)
            {
                xPanderPanel1.Visible = false;
                xPanderPanel1VisibleState = false;
                RefreshPanelDynamicInfoWidth();
                xPanderPanelList1.Refresh();

            }
        }

        void flowLayoutPanel1_ControlAdded(object sender, ControlEventArgs e)
        {
            foreach (BSE.Windows.Forms.XPanderPanel xpanel in xPanderPanelList1.XPanderPanels)
                xpanel.Expand = false;

            xPanderPanel1.Visible = true;
            xPanderPanel1VisibleState = true;
            xPanderPanel1.Expand = true;
            RefreshPanelDynamicInfoWidth();
            xPanderPanelList1.Refresh();
        }

        void MessagePanel1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
                e.Effect = DragDropEffects.All;
            else if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
        }

        void MessagePanel1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] MyFiles;
                int i;
                // 将文件赋给一个数组。
                MyFiles = (string[])(e.Data.GetData(DataFormats.FileDrop));
                // 循环处理数组并将文件添加到列表中。
                for (i = 0; i <= MyFiles.Length - 1; i++)
                {
                    System.IO.FileInfo f = new System.IO.FileInfo(MyFiles[i]);
                    if (f.Exists)
                        sendFile(f.FullName);
                }
            }
        }
        #endregion

        #region 文件传输方法

        #region 发送文件单击事件
        void tsButSendFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sendFile(of.FileName);
            }
        }

        private void sendFile(string filename)
        {
            FileTransmit ft = new  FileTransmit(myUserID, User.UserID,Global.OfflineFileServerEP);
            ft.P2PFileTransmit = new p2pFileClient(Global.AVTransmitServerEP, filename);
            foreach (System.Windows.Forms.Control c in flowLayoutPanel1.Controls)
            {
                 FileTransmit f = c as  FileTransmit;
                if (f != null && ft.TFileInfo.MD5 == f.TFileInfo.MD5)
                {
                    ft.Dispose(); ft = null;
                    return;
                }
            }

            setEvent(ft);

            flowLayoutPanel1.Controls.Add(ft);

            IMLibrary4.Protocol.P2PFileMsg fileMsg = new IMLibrary4.Protocol.P2PFileMsg();//文件传输协商协议
            fileMsg.type = IMLibrary4.Protocol.type.New;//标记发送新文件
            fileMsg.Name = ft.TFileInfo.Name;
            fileMsg.MD5 = ft.TFileInfo.MD5;
            fileMsg.Length = ft.TFileInfo.Length;
            fileMsg.Extension = ft.TFileInfo.Extension;

            if (SendMsgToUser != null)//触发消息发送事件
                SendMsgToUser(fileMsg, User);
        } 
        #endregion

        #region 设置文件传输事件
        /// <summary>
        /// 设置文件传输事件
        /// </summary>
        /// <param name="ft"></param>
        private void setEvent(FileTransmit ft)
        {
            ///文件取消事件
            ft.fileTransmitCancel += (sender) =>
            {
                P2PFileMsg pfile = new P2PFileMsg();//文件传输协商协议
                pfile.type = IMLibrary4.Protocol.type.cancel;//标记取消文件传输
                pfile.MD5 = ft.TFileInfo.MD5;

                if (SendMsgToUser != null)//触发消息发送事件
                    SendMsgToUser(pfile, User);

                MessagePanel1.addRemarkTextToRecord(" 您取消了文件“" + ft.TFileInfo.Name + "”的传输！");

                flowLayoutPanel1.Controls.Remove(ft);
                ft.CancelTransmit();
                ft.Dispose();
                ft = null;
            };
            //离线文件事件
            ft.OfflineFile += (sender) =>
            {
                P2PFileMsg pfile = new P2PFileMsg();//文件传输协商协议
                pfile.type = IMLibrary4.Protocol.type.cancel;//标记取消文件传输
                pfile.MD5 = ft.TFileInfo.MD5;

                if (SendMsgToUser != null)//触发消息发送事件
                    SendMsgToUser(pfile, User);
            };
            ///获得主机信息事件
            ft.GetIPEndPoint += (sender, local, remote) =>
            {
                P2PFileMsg pfile = new P2PFileMsg();//文件传输协商协议
                pfile.type = IMLibrary4.Protocol.type.set;//标记要求对方设置当前用户的远程主机信息
                pfile.MD5 = ft.TFileInfo.MD5;
                pfile.LocalIP = local.Address;
                pfile.LocalPort = local.Port;
                pfile.remoteIP = remote.Address;
                pfile.remotePort = remote.Port;

                if (SendMsgToUser != null)//触发消息发送事件
                    SendMsgToUser(pfile, User);
            };
            ///文件传输结束事件
            ft.fileTransmitted += (sender) =>
            {
                if (ft.isSend)
                    MessagePanel1.addRemarkTextToRecord(" 文件“" + ft.TFileInfo.Name + "”已经传输结束！");
                else
                    MessagePanel1.addRemarkTextToRecord(" 文件“<file:\\\\" + ft.TFileInfo.fullName + ">”已经传输结束！");

                flowLayoutPanel1.Controls.Remove(ft);
                ft.CancelTransmit();
                ft.Dispose();
                ft = null;
            };
            ///下一次接收离线文件事件
            ft.offlineFileNextReceive += (sender) =>
                {
                    //将离线文件消息保存于服务器下，已便下次接收文件
                    OfflineFileMsg fileMsg = new OfflineFileMsg();
                    fileMsg.MD5 = ft.TFileInfo.MD5;
                    fileMsg.Extension = ft.TFileInfo.Extension;
                    fileMsg.Length = ft.TFileInfo.Length;
                    fileMsg.Name = ft.TFileInfo.Name;

                    flowLayoutPanel1.Controls.Remove(ft);
                    ft.CancelTransmit();
                    ft.Dispose();
                    ft = null;
                    if (SendMsgToUser != null)
                        SendMsgToUser(fileMsg, User);
                };
        }
        #endregion

        #region 发送消息单击事件
        private void butSend_Click(object sender, EventArgs e)
        {
            butSend.Enabled = false;
            this.MessagePanel1.SendMsg();            
            butSend.Enabled = true;            
        }
        #endregion

        #region 当消息创建事件
        private void MessagePanel1_CreateMsgAfter(object sender, IMLibrary4.Protocol.Message msg)
        {
            if (User.UserID == myUserID)
            {
                IMLibrary4.Global.MsgShow("不能给自己发送消息");
                return;
            }

            msg.MessageType = MessageType.User;

            if (SendMsgToUser != null)
                SendMsgToUser(msg, User);
            this.MsgToRichTextBox(msg, true);

            #region 发送截图
            List<IMLibrary4.MyPicture> pictures = MessagePanel1.GetSendPicture();
            if (pictures != null )//如果文件是截图
                foreach (IMLibrary4.MyPicture pic in pictures)
                    if (pic.MD5.Length == 32)
                    {
                        string fileName = myUserID + "\\sendImage\\" + pic.MD5 + ".gif";
                        TcpImageFileClient tcpFile = new TcpImageFileClient(Global.ImageServerEP, fileName);//发送图片文件到服务器
                        tcpFile.fileTransmitted += delegate(object sender1, fileTransmitEvnetArgs e)//文件发送结束事件
                        {
                            if (SendMsgToUser != null)
                            {
                                IMLibrary4.Protocol.ImageFileMsg fileMsg = new IMLibrary4.Protocol.ImageFileMsg();
                                fileMsg.MessageType = MessageType.User;
                                fileMsg.Name = "";
                                fileMsg.MD5 = e.fileInfo.MD5;
                                fileMsg.Extension = e.fileInfo.Extension;
                                fileMsg.Length = e.fileInfo.Length;
                                SendMsgToUser(fileMsg, User);
                            }
                            (sender1 as TcpImageFileClient).Dispose();
                            sender1 = null;
                        };
                    }
            #endregion

            this.MessagePanel1.ClearMsgTextBox();
        }
        #endregion

        #region 消息记录

        private void MessagePanel1_OpenAway(object sender)
        {
            if (Global.FormDataManageage == null || Global.FormDataManageage.IsDisposed)
            {
                Global.FormDataManageage = new FormDataManage();
                Global.FormDataManageage.myUserID = myUserID;
                Global.FormDataManageage.SelectUserID = User.UserID;
                Global.FormDataManageage.MessageType = MessageType.User;
                Global.FormDataManageage.FormClosed += delegate(object sender1, FormClosedEventArgs e1)
                {
                    Global.FormDataManageage.Dispose(); Global.FormDataManageage = null;
                };
            }
            Global.FormDataManageage.Show();            
        }
        #endregion

        #region 窗口事件

        #region 窗口加载事件
        private void FormTalkUser_Load(object sender, EventArgs e)
        {
            this.MessagePanel1.FaceSet();
        }
        #endregion

        #region 关闭窗口按钮单击事件
        private void butClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 窗口关闭事件
        void FormTalkUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!tsButAV.Enabled || flowLayoutPanel1.Controls.Count > 0 || !tsButRemote.Enabled)
            {
                if (MessageBox.Show("如果关闭窗口，将会终止文件传输、视频对话、远程协助等功能。是否关闭窗口？", "提示", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                {

                    IMLibrary4.Protocol.AVMsg avMsg = new IMLibrary4.Protocol.AVMsg();
                    avMsg.type = IMLibrary4.Protocol.type.cancel;
                    if (SendMsgToUser != null)//触发消息发送事件
                        SendMsgToUser(avMsg, User);

                    IMLibrary4.Protocol.RemoteDesktopMsg remoteDesktopMsg = new IMLibrary4.Protocol.RemoteDesktopMsg();
                    remoteDesktopMsg.type = IMLibrary4.Protocol.type.cancel;
                    if (SendMsgToUser != null)//触发消息发送事件
                        SendMsgToUser(remoteDesktopMsg, User);

                    IMLibrary4.Protocol.P2PFileMsg pfile = new IMLibrary4.Protocol.P2PFileMsg();//文件传输协商协议
                    pfile.type = IMLibrary4.Protocol.type.cancel;//标记取消文件传输
                    pfile.MD5 = "";
                    if (SendMsgToUser != null)//触发消息发送事件
                        SendMsgToUser(pfile, User);

                    
                    foreach (Control c in flowLayoutPanel1.Controls)
                    {
                        FileTransmit ft = c as FileTransmit;
                        if (ft != null)
                        {
                            ft.CancelTransmit();
                            //flowLayoutPanel1.Controls.Remove(ft);
                            //ft.Dispose();
                            //ft = null;
                        }
                    }

                    e.Cancel = false;
                }
                else
                    e.Cancel = true;
            }
        }
        #endregion

        #endregion 

        #region 获取需要接收的图片文件集合
        public List<IMLibrary4.MyPicture> GetNeedRecPicture()
        {
            return MessagePanel1.needRecPictures;
        }
        #endregion

        #region 将消息加入 RichTextBox 控件
        /// <summary>
        /// 将消息加入 RichTextBox 控件
        /// </summary>
        /// <param name="msg">消息类</param>
        /// <param name="IsSend">标记是发送消息还是收到消息</param>
        public void MsgToRichTextBox(IMLibrary4.Protocol.Message msg, bool IsSend)//将发送的消息加入历史rich
        {
            string title =myUserName  +"(" + myUserID + ")";
            if (!IsSend)
                title = User.UserName + "(" + User.UserID + ")";

            this.MessagePanel1.MsgToRichTextBox(msg, IsSend, title);
        }
        #endregion 

        #region 准备接收文件传输
        /// <summary>
        /// 准备接收P2P文件传输
        /// </summary>
        /// <param name="pfile"></param>
        public void ReceiveP2PFile(IMLibrary4.Protocol.P2PFileMsg  pfile)
        {

            foreach (System.Windows.Forms.Control c in flowLayoutPanel1.Controls)
            {
               FileTransmit f = c as FileTransmit;
                if (f != null && pfile.MD5 == f.TFileInfo.MD5)
                    return;
            }

            TFileInfo tFileInfo = new TFileInfo();
            tFileInfo.Name = pfile.Name;
            tFileInfo.Length = pfile.Length;
            tFileInfo.Extension = pfile.Extension;
            tFileInfo.MD5 = pfile.MD5;

             FileTransmit ft = new FileTransmit(User.UserID,myUserID ,Global.OfflineFileServerEP);
            ft.P2PFileTransmit = new p2pFileClient(Global.AVTransmitServerEP, tFileInfo);
            setEvent(ft);
            flowLayoutPanel1.Controls.Add(ft);
        }

        /// <summary>
        /// 准备接收离线文件传输
        /// </summary>
        /// <param name="pfile"></param>
        public void ReceiveOfflineFile(IMLibrary4.Protocol.OfflineFileMsg  ofile)
        {
            foreach (System.Windows.Forms.Control c in flowLayoutPanel1.Controls)
            {
                FileTransmit f = c as  FileTransmit;
                if (f != null && ofile.MD5 == f.TFileInfo.MD5)
                    return;
            }

            TFileInfo tFileInfo = new TFileInfo();
            tFileInfo.Name = ofile.Name;
            tFileInfo.Length = ofile.Length;
            tFileInfo.Extension = ofile.Extension;
            tFileInfo.MD5 = ofile.MD5;

             FileTransmit ft = new  FileTransmit(User.UserID, myUserID,Global.OfflineFileServerEP);
            ft.TFileInfo = tFileInfo;
            ft.tcpOfflineFileClient  = new TcpOfflineFileClient(Global.OfflineFileServerEP, tFileInfo);
            setEvent(ft);
            flowLayoutPanel1.Controls.Add(ft);
        } 
        #endregion

        #region 取消文件传输
        /// <summary>
        /// 取消文件传输
        /// </summary>
        /// <param name="pfile"></param>
        public void CancelFile(IMLibrary4.Protocol.P2PFileMsg  pfile)
        {
            if (pfile.MD5 == "")
            {
                flowLayoutPanel1.Controls.Clear();
                MessagePanel1.addRemarkTextToRecord(" 对方取消了所有文件的传输！");
                return;
            }

            foreach (Control c in flowLayoutPanel1.Controls)
            {
                FileTransmit ft = c as FileTransmit;
                if (ft != null && ft.P2PFileTransmit!=null && (ft.P2PFileTransmit.TFileInfo.MD5 == pfile.MD5))
                {
                    ft.CancelTransmit();
                    MessagePanel1.addRemarkTextToRecord(" 对方取消了文件“" + ft.P2PFileTransmit.TFileInfo.Name + "”的传输！");
                    flowLayoutPanel1.Controls.Remove(ft);
                    ft.CancelTransmit();
                    ft.Dispose();
                    ft = null;
                }
            }
        }
        #endregion

        #region 设置文件传输远程主机信息
        /// <summary>
        /// 设置文件传输远程主机信息
        /// </summary>
        public void setFileRometEP(IMLibrary4.Protocol.P2PFileMsg  pfile)
        {
            foreach (Control c in flowLayoutPanel1.Controls)
            {
             FileTransmit ft = c as  FileTransmit;
                if (ft != null && ft.TFileInfo.MD5 == pfile.MD5)
                {
                    ft.P2PFileTransmit.Start(true);//发送方参数须设置为真
                    ft.P2PFileTransmit.setRemoteIP(new IPEndPoint(pfile.LocalIP ,pfile.LocalPort ),
                                                   new IPEndPoint(pfile.remoteIP ,pfile.remotePort));//设置文件传输远程主机信息
                     
                } 
            }
        }
        #endregion  


        #endregion
    }
}
