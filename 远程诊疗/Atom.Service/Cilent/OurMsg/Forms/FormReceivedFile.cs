﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.Windows.Forms;
using System.Threading;
using IMLibrary4;
using IMLibrary4.Protocol;
using IMLibrary4.Organization;

namespace OurMsg
{
    public partial class FormReceivedFile : BaseForm
    {
        public FormReceivedFile(string Userid)
        {
            InitializeComponent();
            UserID = Userid;
        }

        #region 委托
        
        public delegate void OfflineFileEventHandler(object sender,String User);

        //public event OfflineFileEventHandler LoadOffline;

        #endregion

        #region 变量

        private String UserID = "";

        /// <summary>
        /// 用户
        /// </summary>
        private List<exUser> Users = new List<exUser>();

        #endregion


        /// <summary>
        /// load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormReceivedFile_Load(object sender, EventArgs e)
        {
            InitListView();
            this.Users = OpeRecordDB.GetUsers();
        }

        #region Init
        
        void InitListView()
        {
            //listView1
            // Set the view to show details.
            listView1.View = View.Details;
            // Allow the user to edit item text.
            listView1.LabelEdit = false;
            // Allow the user to rearrange columns.
            listView1.AllowColumnReorder = true;
            // Display check boxes. 是否显示复选框
            listView1.CheckBoxes = false;
            // Select the item and subitems when selection is made. 是否选中整行
            listView1.FullRowSelect = true;
            // Display grid lines. 是否显示网格
            listView1.GridLines = false;
            // Sort the items in the list in ascending order. 升序还是降序
            listView1.Sorting = SortOrder.Ascending;

            //listView2
            // Set the view to show details.
            listView2.View = View.Details;
            // Allow the user to edit item text.
            listView2.LabelEdit = false;
            // Allow the user to rearrange columns.
            listView2.AllowColumnReorder = true;
            // Display check boxes. 是否显示复选框
            listView2.CheckBoxes = false;
            // Select the item and subitems when selection is made. 是否选中整行
            listView2.FullRowSelect = true;
            // Display grid lines. 是否显示网格
            listView2.GridLines = false;
            // Sort the items in the list in ascending order. 升序还是降序
            listView2.Sorting = SortOrder.Ascending;
        }

        #endregion

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsBtnQuery_Click(object sender, EventArgs e)
        {
            if (UserID != "")
                GetOfflineFile(UserID);
        }

        private void GetOfflineFile(string userId)
        {
            String Sql = "select * from OfflineFiles_users where RecUser='" + userId + "' ";
            string SqlSend = "select * from OfflineFiles_users where SendUser='" + userId + "' ";

            if (toolStripComboBox1.Text == "三天内" || toolStripComboBox1.Text == "")
            {
                Sql += " and FromTime>='" + System.DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd 00:00:00") + "'";
                SqlSend += " and FromTime>='" + System.DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd 00:00:00") + "'";
            }
            if (toolStripComboBox1.Text == "七天内")
            {
                Sql += " and FromTime>='" + System.DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd 00:00:00") + "'";
                SqlSend += " and FromTime>='" + System.DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd 00:00:00") + "'";
            }

            DataTable dt = OpeRecordDB.GetDataSetBySql(Sql);
            DataTable dtSend = OpeRecordDB.GetDataSetBySql(SqlSend);


            #region 收到的文件
            
            if (dt != null && dt.Rows.Count > 0)
            {
                listView1.Items.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    exUser user = findUser(dt.Rows[i]["SendUser"].ToString());
                    ListViewItem li = new ListViewItem();
                    li.ImageIndex = 0;
                    li.Tag = Factory.CreateInstanceObject(dt.Rows[i]["Vcard"].ToString());
                    if (li.Tag is OfflineFileMsg)
                    {
                        OfflineFileMsg offline = li.Tag as OfflineFileMsg;
                        li.SubItems.Add(dt.Rows[i]["FromTime"].ToString());
                        li.SubItems.Add(user.UserName + "(" + offline.from + ")");
                        li.SubItems.Add(offline.Name.ToString());
                        li.SubItems.Add(offline.Length.ToString());
                        this.listView1.Items.Add(li);
                    }
                }
            }
            #endregion

            #region 发出的文件

            if (dtSend != null && dtSend.Rows.Count > 0)
            {
                listView2.Items.Clear();
                for (int i = 0; i < dtSend.Rows.Count; i++)
                {
                    exUser user = findUser(dtSend.Rows[i]["RecUser"].ToString());
                    ListViewItem li = new ListViewItem();
                    li.ImageIndex = 0;
                    li.Tag = Factory.CreateInstanceObject(dtSend.Rows[i]["Vcard"].ToString());
                    if (li.Tag is OfflineFileMsg)
                    {
                        OfflineFileMsg offline = li.Tag as OfflineFileMsg;
                        li.SubItems.Add(dtSend.Rows[i]["FromTime"].ToString());
                        li.SubItems.Add(user.UserName + "(" + offline.from + ")");
                        li.SubItems.Add(offline.Name.ToString());
                        li.SubItems.Add(offline.Length.ToString());
                        this.listView2.Items.Add(li);
                    }
                }
            }
            #endregion
        }

        #region 查找用户
        /// <summary>
        /// 查找用户
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        public exUser findUser(string userID)
        {
            if (Users == null) return null;
            foreach (exUser user in Users)
                if (user.UserID == userID)
                    return user;
            return null;
        }
        #endregion

        #region 文件下载
        
        /// <summary>
        /// 文件服务
        /// </summary>
        private TcpOfflineFileClient tcpOfflineFileClient = null;
        private OfflineFileMsg offlinefile = null;
        private int Length = 0;
        private int MaxLength = 0;
        private void tsMenuSaveAs_Click(object sender, EventArgs e)
        {
            if (toolStripStatusLabel1.Text == "正在下载...")
            {
                if (MessageBox.Show("还有正在下载的文件,是否要下载新文件?\n(后台静默下载)", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;
            }
            if (this.listView1.Focused == true)
            {
                if (this.listView1.SelectedItems != null && listView1.SelectedItems.Count > 0)
                    offlinefile = listView1.SelectedItems[0].Tag as OfflineFileMsg;
            }
            else if (this.listView2.Focused == true)
            {
                if (this.listView2.SelectedItems != null && listView2.SelectedItems.Count > 0)
                    offlinefile = listView2.SelectedItems[0].Tag as OfflineFileMsg;
            }
            #region 初始化文件
            
            if (offlinefile != null)
            {
                SaveFileDialog sf = new SaveFileDialog();
                sf.FileName = offlinefile.Name;
                sf.Filter = "*" + offlinefile.Extension + "|";
                if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    TFileInfo tFileInfo = new TFileInfo();
                    tFileInfo.Name = offlinefile.Name; //文件名
                    tFileInfo.Length = offlinefile.Length; //大小
                    tFileInfo.Extension = offlinefile.Extension; // 后缀
                    tFileInfo.MD5 = offlinefile.MD5; //md5

                    tcpOfflineFileClient = new TcpOfflineFileClient(Global.OfflineFileServerEP, tFileInfo);

                    tcpOfflineFileClient.TFileInfo.fullName = sf.FileName;
                    tcpOfflineFileClient.to = Global.CurrUserID;

                    //初始进度条
                    toolStripProgressBar1.Value = 0;
                    toolStripProgressBar1.Visible = true;
                    toolStripStatusLabel1.Text = "正在下载...";
                    toolStripStatusLabel1.Visible = true;
                    MaxLength = Convert.ToInt32(offlinefile.Length);
                    toolStripProgressBar1.Maximum = MaxLength;
                    Thread th = new Thread(new ThreadStart(DownFile));
                    th.Start();
                    timer1.Enabled = true;
                    timer1_Tick(null, null);
                    //while (true)
                    //{
                    //    toolStripProgressBar1.Value = Convert.ToInt32(((decimal)tcpOfflineFileClient.TFileInfo.CurrLength / offlinefile.Length) * 100);
                    //    if (toolStripProgressBar1.Value == 100)
                    //    {
                    //        toolStripStatusLabel1.Text = "下载完成!";
                    //        return;
                    //    }
                    //}
                    //                    
                }
            }

            #endregion
        }
        /// <summary>
        /// 开始下载文件
        /// </summary>
        private void DownFile()
        {
            tcpOfflineFileClient.Start();//开始下载文件
            while (true)
            {
                Length = Convert.ToInt32(tcpOfflineFileClient.TFileInfo.CurrLength);
                if (Length == MaxLength)
                {
                    return;
                }
            }
        }

        /// <summary>
        /// 加载进度
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripProgressBar1.Value = Length;
            if (Length == MaxLength)
            {
                timer1.Enabled = false;
                toolStripStatusLabel1.Text = "下载完成!";
                return;
            }
        }

        #endregion

        private void tsBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }
}
