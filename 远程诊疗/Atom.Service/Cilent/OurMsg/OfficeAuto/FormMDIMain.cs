﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OurMsg
{
    public partial class FormMDIMain : BaseForm
    {
        #region 变量

        private string _FormText="";

        /// <summary>
        /// 窗体Text
        /// </summary>
        public string FormText
        {
            set 
            { 
                _FormText = value; 
            }
            get { return _FormText; }
        }

        private object _Path = null;

        /// <summary>
        /// 地址
        /// </summary>
        public object Path
        {
            set 
            {
                _Path = value;
            }
            get { return _Path; }
        }

        /// <summary>
        /// 默认路径
        /// </summary>
        string LocalPath = "/Main/MyDesk.aspx"; //默认到桌面

        /// <summary>
        /// WebOAIP
        /// </summary>
        string WebOAIP = "192.168.10.118";
        /// <summary>
        /// Pro
        /// </summary>
        string WebOAPro = "8087";
        #endregion

        #region 委托/事件

        /// <summary>
        /// 声明委托
        /// </summary>
        public delegate void MailListEventHandler(object sender);

        /// <summary>
        /// 读取邮件
        /// </summary>
        public event MailListEventHandler ReadMail;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="FromUser"></param>
        public delegate void SendToUserMsgHandler(IMLibrary4.Protocol.Message msg, String FromUser);

        public event SendToUserMsgHandler SendeToUserMsg;

        #endregion

        #region Init/初始
        
        public FormMDIMain()
        {            
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            this.treeView1.NodeMouseClick += treeView1_NodeMouseClick;
            this.treeView2.NodeMouseClick += treeView1_NodeMouseClick;
            this.treeView3.NodeMouseClick += treeView1_NodeMouseClick;
            this.treeView4.NodeMouseClick += treeView1_NodeMouseClick;

            //自动变更ip
            WebOAIP = Global.MailServer;
            WebOAPro = Global.MailPort;
        }        

        private void FormMDIMain_Load(object sender, EventArgs e)
        {
            this.treeView1.ExpandAll();
            this.treeView2.ExpandAll();
            this.treeView3.ExpandAll();
            this.treeView4.ExpandAll();
            ShowForms("我的桌面", Path);//窗体加载时打开web桌面
            if(FormText!="")
                ShowForms(FormText,Path);
        }

        #endregion

        void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            //throw new NotImplementedException();
            ShowForms(e.Node.Text,e.Node.Tag);
        }

        #region 窗体加载方法

        private Form form1; //定义一个窗体判断是否打开

        /// <summary>
        /// 用于窗体打开时调用
        /// </summary>
        /// <param name="_FormText"></param>
        public void ShowForms()
        {
            if (FormText != null && FormText != "")
                ShowForms(FormText,Path);
        }

        /// <summary>
        /// 打开对应窗口
        /// </summary>
        /// <param name="_FormText"></param>
        /// <param name="Path">跳转地址</param>
        private void ShowForms(String _FormText,object _path)
        {            
            if (_path != null)
                LocalPath = _path.ToString();
            #region 邮件窗体
            
            FormMailNewList frm = new FormMailNewList();//邮件
            //加载事件
            frm.ReadMail += delegate(object sd)
            {
                //刷新
                ReadMail(sd);
            };
            frm.SendeToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
            {
                //发送通知
                SendeToUserMsg(msg, FromUser);
            };

            #endregion

            FormWeb formWeb = new FormWeb(); //web窗体
            formWeb.Url = "http://" + WebOAIP + ":" + WebOAPro;
            switch (_FormText)
            {
                #region 邮箱

                case "收件箱":
                    frm.Text = "收件箱";
                    if (!ShowChildrenForm(frm.Text))
                    {
                        frm.FormState = OurMsg.IMMail.Mail_Enum.Form_State.InBox;
                        frm.MdiParent = this;
                        frm.WindowState = FormWindowState.Maximized;
                        frm.Show();
                    }
                    break;
                case "草稿箱":
                    frm.Text = "草稿箱";
                    if (!ShowChildrenForm(frm.Text))
                    {
                        frm.FormState = OurMsg.IMMail.Mail_Enum.Form_State.DraftBox;
                        frm.MdiParent = this;
                        frm.WindowState = FormWindowState.Maximized;
                        frm.Show();
                    }
                    break;
                case "发件箱":
                    frm.Text = "发件箱";
                    if (!ShowChildrenForm(frm.Text))
                    {
                        frm.FormState = OurMsg.IMMail.Mail_Enum.Form_State.OutBox;
                        frm.MdiParent = this;
                        frm.WindowState = FormWindowState.Maximized;
                        frm.Show();
                    }
                    break;
                case "新邮件":
                    OurMsg.IMMail.FormMail FormMail = new OurMsg.IMMail.FormMail(null, OurMsg.IMMail.Mail_Enum.Form_State.NewMail, "新增邮件");
                    FormMail.SendToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
                    {
                        //发送通知
                        SendeToUserMsg(msg, FromUser);
                    };
                    if (Application.OpenForms[FormMail.Name] != null)
                    {
                        form1 = Application.OpenForms[FormMail.Name];
                        form1.Activate();
                    }
                    else
                    {
                        FormMail.ShowDialog();
                    }
                    break;

                #endregion

                #region Web窗体

                case "我的桌面":
                    formWeb.Text = "我的桌面";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "公告通知":
                    formWeb.Text = "公告通知";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "新建工作":
                    formWeb.Text = "新建工作";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "我的工作":
                    formWeb.Text = "我的工作";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "代办工作":
                    formWeb.Text = "代办工作";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "已办工作":
                    formWeb.Text = "已办工作";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "我的计划":
                    formWeb.Text = "我的计划";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "协同计划":
                    formWeb.Text = "协同计划";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "个人文件":
                    formWeb.Text = "个人文件";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;
                case "单位文件":
                    formWeb.Text = "单位文件";
                    if (!ShowChildrenForm(formWeb.Text))
                    {
                        formWeb.Url += "/loginData.aspx?name=demo&password=demo&path=" + LocalPath;
                        formWeb.MdiParent = this;
                        formWeb.WindowState = FormWindowState.Maximized;
                        formWeb.Show();
                    }
                    break;

                #endregion
            }
        }
        
        /// <summary>
        /// 校验窗体是否打开
        /// </summary>
        /// <param name="p_ChildrenFormName">formName</param>
        /// <returns></returns>
        private bool ShowChildrenForm(string p_ChildrenFormText)
        {
            int i;     //依次检测当前窗体的子窗体     
            for (i = 0; i < this.MdiChildren.Length; i++)
            {
                if (this.MdiChildren[i].Text == p_ChildrenFormText) //判断当前子窗体的Text属性值是否与传入的字符串值相同
                {
                    this.MdiChildren[i].Activate();  //如果值相同则表示此子窗体为想要调用的子窗体，激活此子窗体并返回true值
                    if (MdiChildren[i].WindowState == FormWindowState.Minimized)       //如果当前窗体已经最小化
                    {
                        MdiChildren[i].WindowState = FormWindowState.Normal;              //还原窗体
                    }  
                    return true;
                }
            }
            return false; //如果没有相同的值则表示要调用的子窗体还没有被打开，返回false值    

        }

        #endregion

        #region MDIButtonClick

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        #endregion

    }
}
