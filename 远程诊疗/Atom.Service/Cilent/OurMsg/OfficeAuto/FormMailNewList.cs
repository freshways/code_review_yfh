﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OurMsg.IMMail;
using IMLibrary4;

namespace OurMsg
{
    public partial class FormMailNewList : Form
    {
        public FormMailNewList()
        {
            InitializeComponent();
        }
        #region 委托/事件

        /// <summary>
        /// 声明委托
        /// </summary>
        public delegate void MailListEventHandler(object sender);

        /// <summary>
        /// 读取邮件
        /// </summary>
        public event MailListEventHandler ReadMail;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="FromUser"></param>
        public delegate void SendToUserMsgHandler(IMLibrary4.Protocol.Message msg, String FromUser);

        public event SendToUserMsgHandler SendeToUserMsg;

        #endregion

        #region 变量

        private Form form1;
        private Mail_Enum.Form_State _FormState;
        public Mail_Enum.Form_State FormState
        {
            set { _FormState = value; }
            get { return _FormState; }
        }
        private StringBuilder sb = new StringBuilder();
        private string InboxButn = "toolStripButton_Add|toolStripButton_View|toolStripButton_State|toolStripButton_Select|toolStripButton4|toolStripButton3";

        #endregion

        private void FormMailNewList_Load(object sender, EventArgs e)
        {
            HiddenButton();
            //FormState = Mail_Enum.Form_State.InBox;
            ShowButton(InboxButn);
            BindMailList();
        }

        private void toolStripButton_Select_Click(object sender, EventArgs e)
        {
            BindMailList();
        }

        #region 菜单事件

        /// <summary>
        /// 新邮件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton_Add_Click(object sender, EventArgs e)
        {
            FormMail frm = new FormMail(null, Mail_Enum.Form_State.NewMail, "新增邮件");
            //发送邮件通知代理事件
            frm.SendToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
            {
                SendeToUserMsg(msg, FromUser);
            };
            if (Application.OpenForms[frm.Name] != null)
            {
                form1 = Application.OpenForms[frm.Name];
                form1.Activate();
            }
            else
                frm.Show();
        }

        /// <summary>
        /// 查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton_View_Click(object sender, EventArgs e)
        {
            if (dataGridView1.DataSource == null || dataGridView1.Rows.Count == 0)
                return;
            string guid = dataGridView1.SelectedRows[0].Cells["Guid"].Value.ToString();
            FormMail frm = new FormMail(null, FormState, "查看邮件");
            //发送邮件通知代理事件
            frm.SendToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
            {
                SendeToUserMsg(msg, FromUser);
            };

            frm.OldGuid = guid;

            //判断是未读的邮件进行标记
            if (dataGridView1.SelectedRows[0].Cells["Email_State"].Value.ToString() == "未读")
            {
                MyService.ExecSQLEnum exSql = new MyService.ExecSQLEnum();
                exSql.type = MyService.exType.update;
                exSql.from = "Email_User";
                exSql.column = "Email_State='已读',ReadTime=getdate() ";
                exSql.where = "where Guid='" + guid + "' and ToUser='" + Global.CurrUserID + "' ";
                try
                {
                    int rowint = OpeRecordDB.ExecSql(exSql);
                    dataGridView1.SelectedRows[0].Cells["Email_State"].Value = "已读";
                    //委托已读事件
                    ReadMail(IMLibrary4.OpeRecordDB.GetMailCount(Global.CurrUserID).ToString());
                    //回执-通知发件人
                    if (dataGridView1.SelectedRows[0].Cells["State"].Value.ToString() == "1")
                    {
                        SendMessage(Convert.ToString(dataGridView1.SelectedRows[0].Cells["FromUserID"].Value),
                            Convert.ToString(dataGridView1.SelectedRows[0].Cells["Email_Title"].Value));
                    }
                }
                catch { }
            }
            //显示邮件
            if (Application.OpenForms[frm.Name] != null)
            {
                form1 = Application.OpenForms[frm.Name];
                form1.Activate();
            }
            else
                frm.Show();
            //MessageBox.Show(guid);
        }

        /// <summary>
        /// 标记已读
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton_State_Click(object sender, EventArgs e)
        {
            string guid = dataGridView1.SelectedRows[0].Cells["Guid"].Value.ToString();
            if (dataGridView1.SelectedRows[0].Cells["Email_State"].Value.ToString() == "未读")
            {
                MyService.ExecSQLEnum exSql = new MyService.ExecSQLEnum();
                exSql.type = MyService.exType.update;
                exSql.from = "Email_User";
                exSql.column = "Email_State='已读',ReadTime=getdate() ";
                exSql.where = "where Guid='" + guid + "' and ToUser='" + Global.CurrUserID + "' ";
                try
                {
                    int rowint = OpeRecordDB.ExecSql(exSql);
                    dataGridView1.SelectedRows[0].Cells["Email_State"].Value = "已读";
                    //委托已读事件
                    ReadMail(IMLibrary4.OpeRecordDB.GetMailCount(Global.CurrUserID).ToString());
                    //回执-通知发件人
                    if (dataGridView1.SelectedRows[0].Cells["State"].Value.ToString() == "1")
                    {                        
                        SendMessage(Convert.ToString(dataGridView1.SelectedRows[0].Cells["FromUserID"].Value),
                            Convert.ToString(dataGridView1.SelectedRows[0].Cells["Email_Title"].Value));
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            try
            {
                string guid = dataGridView1.SelectedRows[0].Cells["Guid"].Value.ToString();

                MyService.ExecSQLEnum exSql = new MyService.ExecSQLEnum();

                if (FormState == Mail_Enum.Form_State.InBox)
                {
                    exSql.type = MyService.exType.update;
                    exSql.from = "Email_User";
                    exSql.column = "IFDel='0'";
                    exSql.where = "where Guid='" + guid + "' and ToUser='" + Global.CurrUserID + "'";
                }
                if (FormState == Mail_Enum.Form_State.OutBox)
                {
                    exSql.type = MyService.exType.update;
                    exSql.from = "Email";
                    exSql.column = "IFDel='0'";
                    exSql.where = "where Guid='" + guid + "'";
                }
                if (FormState == Mail_Enum.Form_State.DraftBox) //草稿箱
                {
                    exSql.type = MyService.exType.delete;
                    exSql.from = "Email";
                    exSql.where = "where Guid='" + guid + "'";
                }

                int rowint = OpeRecordDB.ExecSql(exSql);
                if (rowint > 0)
                {
                    MessageBox.Show("删除成功", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
                }
            }
            catch { }
        }

        #endregion

        #region 方法

        public void LoadMail()
        {
            BindMailList();
        }

        /// <summary>
        /// 绑定邮件列表
        /// </summary>
        private void BindMailList()
        {
            try
            {
                sb.Remove(0, sb.Length);
                //Guid, Email_Title, Email_Time, Email_Content, Email_Enclosure, FromUser, ToUser, Email_State from Email

                if (FormState == Mail_Enum.Form_State.InBox) //收件箱
                {
                    sb.Append("select Email.Guid, Email_Title, Email_Time, FromUserName as FromUser,FromUser as FromUserID, Email_User.Email_State,Email_Enclosure,Email.Email_State as State ");
                    sb.Append(" from Email,Email_User ");
                    sb.Append(" where Email.Guid = Email_User.Guid ");
                    sb.Append(" and Email_User.ToUser='").Append(Global.CurrUserID).Append("' ");
                    sb.Append(" and Email_User.IFDel<>'0' ");

                    dataGridView1.Columns["FromUser"].HeaderText = "发件人";
                }
                if (FormState == Mail_Enum.Form_State.OutBox) //发件箱
                {
                    sb.Append("select Guid, Email_Title, Email_Time, ToUserName as FromUser, '已发送' as Email_State,Email_Enclosure from Email ");
                    sb.Append(" where FromUser='").Append(Global.CurrUserID).Append("' ");
                    sb.Append(" and IFDel not in('0','-1') ");

                    dataGridView1.Columns["FromUser"].HeaderText = "收件人";
                }
                if (FormState == Mail_Enum.Form_State.DraftBox) //草稿箱
                {
                    sb.Append("select Guid, Email_Title, Email_Time, ToUserName as FromUser, '草稿' Email_State,Email_Enclosure from Email ");
                    sb.Append(" where IFDel='-1' ");

                    dataGridView1.Columns["FromUser"].HeaderText = "收件人";
                }
                sb.Append(" order by Email_Time desc");

                DataTable dt = OpeRecordDB.GetDataSetBySql(sb.ToString());
                if (dt == null)
                    return;
                dataGridView1.DataSource = dt;
                //skinLabel2.Text = "共计(" + dt.Rows.Count.ToString() + "条)";

            }
            catch (Exception ex)
            {
                MessageBox.Show("加载邮件失败！\n" + ex.Message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// 隐藏所有TollButton
        /// </summary>
        private void HiddenButton()
        {
            for (int i = 0; i < skinToolStrip1.Items.Count; i++)
            {
                skinToolStrip1.Items[i].Visible = false;
            }
        }

        /// <summary>
        /// 显示菜单
        /// </summary>
        /// <param name="buttonString"></param>
        private void ShowButton(string buttonString)
        {
            try
            {
                for (int i = 0; i < skinToolStrip1.Items.Count; i++)
                {
                    if (buttonString.Contains(skinToolStrip1.Items[i].Name) == true)
                        skinToolStrip1.Items[i].Visible = true;
                }
            }
            catch { }
        }

        /// <summary>
        /// 通知收件人
        /// </summary>
        /// <param name="UserID">收件人</param>
        /// <param name="title">邮件标题</param>
        private void SendMessage(string UserID,string Title)
        {
            try
            {
                //通知用户
                IMLibrary4.Protocol.Message msg = new IMLibrary4.Protocol.Message();
                msg.Content = "对方已经打开邮件！来自：" + Global.CurrUserName + "<" + Global.CurrUserID + ">\n邮件标题：" + Title;//获得消息文本内容
                msg.ImageInfo = "";
                msg.from = Global.CurrUserID;
                msg.to = UserID;
                msg.MessageType = IMLibrary4.Protocol.MessageType.Notice;
                msg.Title = "邮件阅读通知";
                msg.remark = "邮件阅读提醒";
                if (UserID != "")
                    SendeToUserMsg(msg, Global.CurrUserID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "\n" + ex.Message);
            }
        }

        #endregion

        #region 绘制附件图标

        private void skinDataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                //2015-01-21 绘制图标时变更已读/未读颜色
                //定义字体样式，未读邮件行字体加粗
                System.Drawing.Font fontaBold = new System.Drawing.Font("", dataGridView1.Font.Size, System.Drawing.FontStyle.Bold);
                //定义字体样式，已读邮件行字体恢复
                System.Drawing.Font fontaRegular = new System.Drawing.Font("", dataGridView1.Font.Size, System.Drawing.FontStyle.Regular);

                if (dataGridView1.Rows[e.RowIndex].Cells["Email_State"].Value.ToString() == "未读")
                {
                    e.Graphics.DrawImage(Properties.Resources.mail_1, e.RowBounds.Left + this.dataGridView1.RowHeadersWidth - 31,
                        e.RowBounds.Top + 4, 16, 16);//绘制图标 
                    dataGridView1.Rows[e.RowIndex].DefaultCellStyle.Font = fontaBold;
                }
                if (dataGridView1.Rows[e.RowIndex].Cells["Email_State"].Value.ToString() == "已读")
                {
                    e.Graphics.DrawImage(Properties.Resources.mail_2, e.RowBounds.Left + this.dataGridView1.RowHeadersWidth - 31,
                        e.RowBounds.Top + 4, 16, 16);//绘制图标 
                    dataGridView1.Rows[e.RowIndex].DefaultCellStyle.Font = fontaRegular;
                }
                if (dataGridView1.Rows[e.RowIndex].Cells["Email_Enclosure"].Value.ToString() != "")
                    e.Graphics.DrawImage(Properties.Resources.mail_attachment, e.RowBounds.Left + this.dataGridView1.RowHeadersWidth - 15,
                        e.RowBounds.Top + 4, 16, 16);//绘制图标 
            }
            catch { }
        }

        #endregion

        /// <summary>
        /// 双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            toolStripButton_View_Click(sender, e);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }
}
