﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OurMsg
{
    public partial class FormWeb : Form
    {
        public FormWeb()
        {
            InitializeComponent();
        }

        public String Url = "";
        private void FormWeb_Load(object sender, EventArgs e)
        {
            if (Url != "")
                this.webBrowser1.Navigate(Url);
        }

    }
}
