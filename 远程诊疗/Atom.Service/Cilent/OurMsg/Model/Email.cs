﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OurMsg.Model
{
    public class Email
    {        
        public string Guid { get; set; }

        public string Email_Titel { get; set; }

        public string Email_Time { get; set; }

        public string Email_Content { get; set; }

        public string Email_Enclosure { get; set; }

        public string FromUser { get; set; }

        public string FromUserName { get; set; }

        public string ToUser { get; set; }

        public string ToUserName { get; set; }

        public string CopyToUser { get; set; }

        public string CopyToUserName { get; set; }

        public string Email_State { get; set; }

        public string IFDel { get; set; }
    }

    public class Email_User
    {
        public string Guid { get; set; }

        public string ToUser { get; set; }

        public string ToUserName { get; set; }

        /// <summary>
        /// 已读未读
        /// </summary>
        public string Email_State { get; set; }

        /// <summary>
        ///  来源（收件/抄送）
        /// </summary>
        public string From_State { get; set; }

        public string IFDel { get; set; }

    }
}
