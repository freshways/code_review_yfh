﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OurMsg.IMMail
{
    public class Mail_Enum
    {
        /// <summary>
        /// 窗体状态
        /// </summary>
        public enum Form_State
        {
            InBox, //收件箱
            OutBox, //发件箱
            DraftBox, //草稿箱
            NewMail //新邮件
        }

        /// <summary>
        /// 
        /// </summary>
        public enum Mail_State
        {
            x123, //默认发送
            x153, //收件人删除
            x423, //发件人删除
            x453, //双方删除
            x003 //草稿
        }
    }
}
