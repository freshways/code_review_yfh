﻿namespace OurMsg.IMMail
{
    partial class FormMailList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMailList));
            this.skinPanel2 = new CCWin.SkinControl.SkinPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Guid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FromUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email_Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email_Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email_State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email_Enclosure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinToolStrip1 = new CCWin.SkinControl.SkinToolStrip();
            this.toolStripButton_Add = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_View = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_State = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Select = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.skButton_DraftBox = new CCWin.SkinControl.SkinButton();
            this.skButton_OutBox = new CCWin.SkinControl.SkinButton();
            this.skButton_InBox = new CCWin.SkinControl.SkinButton();
            this.skinPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.skinToolStrip1.SuspendLayout();
            this.skinPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // skinPanel2
            // 
            this.skinPanel2.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel2.Controls.Add(this.dataGridView1);
            this.skinPanel2.Controls.Add(this.skinLabel1);
            this.skinPanel2.Controls.Add(this.skinLabel2);
            this.skinPanel2.Controls.Add(this.skinToolStrip1);
            this.skinPanel2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinPanel2.DownBack = null;
            this.skinPanel2.Location = new System.Drawing.Point(168, 28);
            this.skinPanel2.MouseBack = null;
            this.skinPanel2.Name = "skinPanel2";
            this.skinPanel2.NormlBack = null;
            this.skinPanel2.Size = new System.Drawing.Size(699, 488);
            this.skinPanel2.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeight = 26;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Guid,
            this.Column1,
            this.Column2,
            this.FromUser,
            this.Email_Title,
            this.Email_Time,
            this.Email_State,
            this.Email_Enclosure});
            this.dataGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dataGridView1.Location = new System.Drawing.Point(0, 38);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(699, 450);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.skinDataGridView1_RowPostPaint);
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            // 
            // Guid
            // 
            this.Guid.DataPropertyName = "Guid";
            this.Guid.HeaderText = "Guid";
            this.Guid.Name = "Guid";
            this.Guid.ReadOnly = true;
            this.Guid.Visible = false;
            // 
            // Column1
            // 
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 45;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.Visible = false;
            this.Column2.Width = 30;
            // 
            // FromUser
            // 
            this.FromUser.DataPropertyName = "FromUser";
            this.FromUser.HeaderText = "发件人";
            this.FromUser.Name = "FromUser";
            this.FromUser.ReadOnly = true;
            this.FromUser.Width = 110;
            // 
            // Email_Title
            // 
            this.Email_Title.DataPropertyName = "Email_Title";
            this.Email_Title.HeaderText = "主题";
            this.Email_Title.Name = "Email_Title";
            this.Email_Title.ReadOnly = true;
            this.Email_Title.Width = 310;
            // 
            // Email_Time
            // 
            this.Email_Time.DataPropertyName = "Email_Time";
            this.Email_Time.HeaderText = "时间";
            this.Email_Time.Name = "Email_Time";
            this.Email_Time.ReadOnly = true;
            this.Email_Time.Width = 150;
            // 
            // Email_State
            // 
            this.Email_State.DataPropertyName = "Email_State";
            this.Email_State.HeaderText = "状态";
            this.Email_State.Name = "Email_State";
            this.Email_State.ReadOnly = true;
            this.Email_State.Width = 60;
            // 
            // Email_Enclosure
            // 
            this.Email_Enclosure.DataPropertyName = "Email_Enclosure";
            this.Email_Enclosure.HeaderText = "附件";
            this.Email_Enclosure.Name = "Email_Enclosure";
            this.Email_Enclosure.ReadOnly = true;
            this.Email_Enclosure.Visible = false;
            // 
            // skinLabel1
            // 
            this.skinLabel1.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.None;
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(327, 14);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(44, 17);
            this.skinLabel1.TabIndex = 3;
            this.skinLabel1.Text = "收件箱";
            // 
            // skinLabel2
            // 
            this.skinLabel2.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.None;
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(392, 14);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(28, 17);
            this.skinLabel2.TabIndex = 4;
            this.skinLabel2.Text = "共()";
            // 
            // skinToolStrip1
            // 
            this.skinToolStrip1.Arrow = System.Drawing.Color.Black;
            this.skinToolStrip1.Back = System.Drawing.Color.White;
            this.skinToolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.skinToolStrip1.BackRadius = 4;
            this.skinToolStrip1.BackRectangle = new System.Drawing.Rectangle(10, 10, 10, 10);
            this.skinToolStrip1.Base = System.Drawing.Color.Transparent;
            this.skinToolStrip1.BaseFore = System.Drawing.Color.Black;
            this.skinToolStrip1.BaseForeAnamorphosis = false;
            this.skinToolStrip1.BaseForeAnamorphosisBorder = 4;
            this.skinToolStrip1.BaseForeAnamorphosisColor = System.Drawing.Color.White;
            this.skinToolStrip1.BaseForeOffset = new System.Drawing.Point(0, 0);
            this.skinToolStrip1.BaseHoverFore = System.Drawing.Color.White;
            this.skinToolStrip1.BaseItemAnamorphosis = true;
            this.skinToolStrip1.BaseItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.BaseItemBorderShow = true;
            this.skinToolStrip1.BaseItemDown = ((System.Drawing.Image)(resources.GetObject("skinToolStrip1.BaseItemDown")));
            this.skinToolStrip1.BaseItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.BaseItemMouse = ((System.Drawing.Image)(resources.GetObject("skinToolStrip1.BaseItemMouse")));
            this.skinToolStrip1.BaseItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.BaseItemRadius = 4;
            this.skinToolStrip1.BaseItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip1.BaseItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.skinToolStrip1.Fore = System.Drawing.Color.Black;
            this.skinToolStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 4, 2);
            this.skinToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.skinToolStrip1.HoverFore = System.Drawing.Color.White;
            this.skinToolStrip1.ItemAnamorphosis = true;
            this.skinToolStrip1.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemBorderShow = true;
            this.skinToolStrip1.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.skinToolStrip1.ItemRadius = 4;
            this.skinToolStrip1.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Add,
            this.toolStripButton_View,
            this.toolStripButton_State,
            this.toolStripButton_Select,
            this.toolStripButton4});
            this.skinToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.skinToolStrip1.Name = "skinToolStrip1";
            this.skinToolStrip1.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinToolStrip1.Size = new System.Drawing.Size(699, 38);
            this.skinToolStrip1.SkinAllColor = true;
            this.skinToolStrip1.TabIndex = 1;
            this.skinToolStrip1.Text = "skinToolStrip1";
            this.skinToolStrip1.TitleAnamorphosis = true;
            this.skinToolStrip1.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.skinToolStrip1.TitleRadius = 4;
            this.skinToolStrip1.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // toolStripButton_Add
            // 
            this.toolStripButton_Add.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Add.Image")));
            this.toolStripButton_Add.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolStripButton_Add.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Add.Name = "toolStripButton_Add";
            this.toolStripButton_Add.Size = new System.Drawing.Size(47, 35);
            this.toolStripButton_Add.Text = "新邮件";
            this.toolStripButton_Add.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton_Add.Click += new System.EventHandler(this.toolStripButton_Add_Click);
            // 
            // toolStripButton_View
            // 
            this.toolStripButton_View.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_View.Image")));
            this.toolStripButton_View.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_View.Name = "toolStripButton_View";
            this.toolStripButton_View.Size = new System.Drawing.Size(35, 35);
            this.toolStripButton_View.Text = "查看";
            this.toolStripButton_View.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton_View.Click += new System.EventHandler(this.toolStripButton_View_Click);
            // 
            // toolStripButton_State
            // 
            this.toolStripButton_State.Image = global::OurMsg.Properties.Resources.mail_2;
            this.toolStripButton_State.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_State.Name = "toolStripButton_State";
            this.toolStripButton_State.Size = new System.Drawing.Size(59, 35);
            this.toolStripButton_State.Text = "标记已读";
            this.toolStripButton_State.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton_State.Click += new System.EventHandler(this.toolStripButton_State_Click);
            // 
            // toolStripButton_Select
            // 
            this.toolStripButton_Select.Image = global::OurMsg.Properties.Resources.mail_1;
            this.toolStripButton_Select.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Select.Name = "toolStripButton_Select";
            this.toolStripButton_Select.Size = new System.Drawing.Size(35, 35);
            this.toolStripButton_Select.Text = "查询";
            this.toolStripButton_Select.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton_Select.Click += new System.EventHandler(this.toolStripButton_Select_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::OurMsg.Properties.Resources.mail_del;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(35, 35);
            this.toolStripButton4.Text = "删除";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.skButton_DraftBox);
            this.skinPanel1.Controls.Add(this.skButton_OutBox);
            this.skinPanel1.Controls.Add(this.skButton_InBox);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(4, 28);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(164, 488);
            this.skinPanel1.TabIndex = 2;
            // 
            // skButton_DraftBox
            // 
            this.skButton_DraftBox.BackColor = System.Drawing.Color.Transparent;
            this.skButton_DraftBox.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skButton_DraftBox.DownBack = global::OurMsg.Properties.Resources.AddAccountBtn_Down;
            this.skButton_DraftBox.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.skButton_DraftBox.Location = new System.Drawing.Point(29, 180);
            this.skButton_DraftBox.MouseBack = global::OurMsg.Properties.Resources.AddAccountBtn_mouseover;
            this.skButton_DraftBox.Name = "skButton_DraftBox";
            this.skButton_DraftBox.NormlBack = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.skButton_DraftBox.Size = new System.Drawing.Size(107, 35);
            this.skButton_DraftBox.TabIndex = 0;
            this.skButton_DraftBox.Text = "草稿箱";
            this.skButton_DraftBox.UseVisualStyleBackColor = false;
            this.skButton_DraftBox.Click += new System.EventHandler(this.skButton_DraftBox_Click);
            // 
            // skButton_OutBox
            // 
            this.skButton_OutBox.BackColor = System.Drawing.Color.Transparent;
            this.skButton_OutBox.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skButton_OutBox.DownBack = global::OurMsg.Properties.Resources.AddAccountBtn_Down;
            this.skButton_OutBox.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.skButton_OutBox.Location = new System.Drawing.Point(29, 123);
            this.skButton_OutBox.MouseBack = global::OurMsg.Properties.Resources.AddAccountBtn_mouseover;
            this.skButton_OutBox.Name = "skButton_OutBox";
            this.skButton_OutBox.NormlBack = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.skButton_OutBox.Size = new System.Drawing.Size(107, 35);
            this.skButton_OutBox.TabIndex = 0;
            this.skButton_OutBox.Text = "发件箱";
            this.skButton_OutBox.UseVisualStyleBackColor = false;
            this.skButton_OutBox.Click += new System.EventHandler(this.skButton_OutBox_Click);
            // 
            // skButton_InBox
            // 
            this.skButton_InBox.BackColor = System.Drawing.Color.Transparent;
            this.skButton_InBox.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skButton_InBox.DownBack = global::OurMsg.Properties.Resources.AddAccountBtn_Down;
            this.skButton_InBox.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.skButton_InBox.Location = new System.Drawing.Point(29, 67);
            this.skButton_InBox.MouseBack = global::OurMsg.Properties.Resources.AddAccountBtn_mouseover;
            this.skButton_InBox.Name = "skButton_InBox";
            this.skButton_InBox.NormlBack = global::OurMsg.Properties.Resources.AddAccountBtn;
            this.skButton_InBox.Size = new System.Drawing.Size(107, 35);
            this.skButton_InBox.TabIndex = 0;
            this.skButton_InBox.Text = "收件箱";
            this.skButton_InBox.UseVisualStyleBackColor = false;
            this.skButton_InBox.Click += new System.EventHandler(this.skButton_InBox_Click);
            // 
            // FormMailList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(159)))), ((int)(((byte)(215)))));
            this.BorderPalace = global::OurMsg.Properties.Resources.all_inside02_bkg;
            this.ClientSize = new System.Drawing.Size(871, 520);
            this.Controls.Add(this.skinPanel2);
            this.Controls.Add(this.skinPanel1);
            this.EffectBack = System.Drawing.SystemColors.Window;
            this.EffectWidth = 5;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = true;
            this.MinimumSize = new System.Drawing.Size(860, 520);
            this.Name = "FormMailList";
            this.Text = "邮件列表";
            this.Load += new System.EventHandler(this.FormMailList_Load);
            this.skinPanel2.ResumeLayout(false);
            this.skinPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.skinToolStrip1.ResumeLayout(false);
            this.skinToolStrip1.PerformLayout();
            this.skinPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinToolStrip skinToolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Add;
        private System.Windows.Forms.ToolStripButton toolStripButton_Select;
        private System.Windows.Forms.ToolStripButton toolStripButton_State;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton_View;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinButton skButton_OutBox;
        private CCWin.SkinControl.SkinButton skButton_InBox;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinButton skButton_DraftBox;
        private CCWin.SkinControl.SkinPanel skinPanel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Guid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn FromUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email_Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email_Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email_State;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email_Enclosure;
    }
}