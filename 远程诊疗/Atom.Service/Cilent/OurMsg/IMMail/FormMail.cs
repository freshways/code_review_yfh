﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using IMLibrary4;
using IMLibrary4.Organization;

namespace OurMsg.IMMail
{
    public partial class FormMail : BaseForm
    {
        #region 变量
        
        private Mail_Enum.Form_State FrmState;
        //窗体button
        private string NewMailButn = "toolStripButton1|toolStripButton2|toolStripButton3";
        private string InBox = "toolStripButton4|toolStripButton5|toolStripButton3";
        private string OutBox = "toolStripButton5|toolStripButton3";
        private string DraftBox = "toolStripButton1|toolStripButton2|toolStripButton3";
        //收件人or抄送人
        private string ToUserAndCopyTo = "ToUser";

        //旧的Guid
        public String OldGuid = "";

        //发件人信息
        private String[] InitUser = new string[] { Global.CurrUserID, Global.CurrUserName + "<" + Global.CurrUserID + ">" };

        //附件上传和下载地址
        private string Uploadurl = @"http://" + Global.ServerDomain + ":" + Global.UpLoadPort + "/" + Global.MailPath + "/" + Global.MailIndex;
        private string DownLoadurl = @"http://" + Global.ServerDomain + ":" + Global.UpLoadPort + "/" + Global.MailPath + "/" + Global.MailLoadPath + "/";


        /// <summary>
        /// 组织机构分组
        /// </summary>
        List<exGroup> Groups = null;

        /// <summary>
        /// 选择的用户集合
        /// </summary>
        public List<exUser> Users = null;

        #endregion

        #region 委托/事件

        public delegate void SendToUserEventHandler(IMLibrary4.Protocol.Message msg,String FromUser);

        public event SendToUserEventHandler SendToUserMsg;

        #endregion

        #region Init

        public FormMail(String[] ToUser,Mail_Enum.Form_State frmSt,string Text)
        {
            InitializeComponent();
            if (ToUser != null)
            {
                Text_ToUser.Tag = ToUser[0] + ";";
                Text_ToUser.Text = ToUser[1];
            }
            FrmState = frmSt;
            this.Text = Text;
        }

        private void FormMail_Load(object sender, EventArgs e)
        {
            //隐藏button  
            HiddenButton();
            //初始化新邮件
            if (FrmState == Mail_Enum.Form_State.NewMail)
            {
                ShowButton(NewMailButn);
                //给发件人赋值
                labFromUser.Tag = InitUser[0];
                labFromUser.Text = InitUser[1];
                checkBox1.Enabled = true;
            }

            #region 初始化button
            //根据不同的入口显示button
            if (FrmState == Mail_Enum.Form_State.InBox)
                ShowButton(InBox);
            if (FrmState == Mail_Enum.Form_State.OutBox)
                ShowButton(OutBox);
            if (FrmState == Mail_Enum.Form_State.DraftBox)
                ShowButton(DraftBox);

            #endregion

            #region 初始化事件

            //处理收件人
            this.Text_ToUser.Enter += new EventHandler(Text_ToUser_Enter);
            this.Text_CopyTo.Enter += new EventHandler(Text_CopyTo_Enter);
            //清空收件/抄送人
            this.btnClearToUser.Click += new EventHandler(btnClearToUser_Click);
            this.btnClearCopyUser.Click += new EventHandler(btnClearCopyUser_Click);

            #endregion

            #region 初始化附件窗口

            //初始化listview
            InitListView(listView1);

            //清空附件字段
            label_fujian.Text = "";           

            #endregion

            //加载联系人
            Groups = OpeRecordDB.GetGroups();
            Users = OpeRecordDB.GetUsers();
            LoadLocalOrg();
            Users = new List<exUser>();//重新添加用户

            //绑定邮件
            if (OldGuid != "")
            {
                BindEmail();
            }
            //添加listview 排序
            this.listView2.ListViewItemSorter = new Common.ListViewColumnSorter();
            this.listView2.ColumnClick += new ColumnClickEventHandler(Common.ListViewHelper.ListView_ColumnClick);
        }

        #region 加载本地数据库中的组织机构过程
        /// <summary>
        /// 加载本地数据库中的组织机构
        /// </summary>
        private void LoadLocalOrg()
        {
            if (this.treeView1.Nodes.Count > 0) return;

            TreeNode node = this.addGroupToTreeView(this.Groups);
            this.addUserToTreeView(this.Users, this.Groups);
            if (node == null) return;

            foreach (TreeNode nodeTemp in node.Nodes)
            {
                treeView1.Nodes.Add(nodeTemp);
                //nodeTemp.Expand();
            }
        }
        #endregion

        #region 在treeView中添加分组groups集合并返回 TreeNode
        /// <summary>
        /// 在treeView中添加分组groups集合并返回 TreeNode
        /// </summary>
        /// <param name="treeView1"></param>
        /// <param name="groups"></param>
        /// <returns></returns>
        private TreeNode addGroupToTreeView(List<exGroup> groups)
        {
            if (groups == null) return null;
            TreeNode nodeTemp = new TreeNode();

            ///添加根分组节点
            foreach (exGroup group in groups)
                if (findGroup(group.SuperiorID, groups) == null)
                {
                    TreeNode node = new TreeNode();
                    node.Text = group.GroupName;
                    node.ToolTipText = group.GroupName;
                    node.ImageIndex = 1;
                    node.SelectedImageIndex = 2;
                    node.Tag = group;
                    group.TreeNode = node;
                    nodeTemp.Nodes.Add(node);
                }

            bool t = false;
            exGroup parentGroup;
            int rCount = 0;
            int sqrCount = (groups.Count * groups.Count);//最大循环次数

            while (!t && rCount <= sqrCount)//如果查询还未结束且循环次数没有超过部门数n平方，则继续
            {
                t = true;
                foreach (exGroup group in groups)
                {
                    parentGroup = findGroup(group.SuperiorID, groups);//找到上级部门节点
                    if (parentGroup != null && group.TreeNode == null) //如果要添加的部门节点不是根部门节点且此节点还未添加 
                    {
                        if (parentGroup.TreeNode != null)// 当前的上级部门已经添加时，添加部门  
                        {
                            TreeNode node = new TreeNode();
                            node.Text = group.GroupName;
                            node.ToolTipText = group.GroupName;
                            node.ImageIndex = 1;
                            node.SelectedImageIndex = 2;
                            node.Tag = group;
                            group.TreeNode = node;
                            (parentGroup.TreeNode as TreeNode).Nodes.Add(node);

                            group.SuperiorGroup = parentGroup;//设置上级组
                        }
                        else//如果当前部门节点的上级部门不是根部门节点并且上级部门的上级部门还未添加，则添加不成功，循环后再添加
                        {
                            t = false;
                        }
                    }
                    rCount++;//查询次数增1，如果大于部门n平方还未结束，则强行结束
                }
            }
            return nodeTemp;
        }
        #endregion

        #region 在treeView中的groups中添加用户
        /// <summary>
        /// 在treeView中的groups中添加用户
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="users"></param>
        private void addUserToTreeView(List<exUser> users, List<exGroup> groups)
        {
            if (users == null || groups == null) return;

            foreach (exUser user in users)
            {
                exGroup group = findGroup(user.GroupID, groups);
                if (group != null && group.TreeNode != null)
                {
                    TreeNode node = new TreeNode();
                    node.Text = user.UserName;
                    node.ToolTipText = user.UserName;
                    node.ImageIndex = 0;
                    node.SelectedImageIndex = 0;
                    node.Tag = user;
                    user.TreeNode = node;
                    TreeNode groupNode = group.TreeNode as TreeNode;
                    if (groupNode != null)
                        groupNode.Nodes.Add(node);

                    //user.Group = group;
                    //user.Group.UserCount++;
                }

            }
        }
        #endregion

        #region 查找组
        /// <summary>
        /// 查找组
        /// </summary>
        /// <param name="GroupID"></param>
        /// <returns></returns>
        private exGroup findGroup(string GroupID, List<exGroup> groups)
        {
            if (groups == null) return null;

            foreach (exGroup group in groups)
                if (group.GroupID == GroupID)
                    return group;
            return null;
        }
        #endregion

        #endregion

        #region 事件方法

        #region 邮件附件操作

        /// <summary>
        /// 初始化附件列表listview
        /// </summary>
        /// <param name="listView"></param>
        private void InitListView(ListView listView)
        {
            listView1.SmallImageList = new ImageList();
            listView1.LargeImageList = new ImageList();

            listView1.View = View.LargeIcon;
            listView1.AllowDrop = true;
            this.listView1.LargeImageList = this.imageList1;

            //事件加载
            this.listView1.DragDrop += new DragEventHandler(listView1_DragDrop);
            this.listView1.DragEnter += new DragEventHandler(listView1_DragEnter);

            //初始化列
            listView1.Items.Clear();
            listView1.Columns.Clear();
        }
        
        /// <summary>
        /// 拖放完成后的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                String[] files = e.Data.GetData(DataFormats.FileDrop, false) as String[];

                foreach (string srcfile in files)
                {
                    UploadFile(srcfile);                    
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 拖动到控件边缘时的操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }

        }        

        #endregion

        #region 收件人/抄送人操作
        
        /// <summary>
        /// 收件人获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Text_ToUser_Enter(object sender, EventArgs e)
        {
            ToUserAndCopyTo = "ToUser";
        }

        /// <summary>
        /// 抄送人获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Text_CopyTo_Enter(object sender, EventArgs e)
        {
            ToUserAndCopyTo = "CopyTo";
        }

        /// <summary>
        /// 清空收件人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearToUser_Click(object sender, EventArgs e)
        {
            Text_ToUser.Tag = "";
            Text_ToUser.Text = "";
        }

        /// <summary>
        /// 清空抄送人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearCopyUser_Click(object sender, EventArgs e)
        {
            Text_CopyTo.Tag = "";
            Text_CopyTo.Text = "";
        }

        #endregion

        /// <summary>
        /// 绑定邮件
        /// </summary>
        private void BindEmail()
        {
            try
            {
                string sql = "select Guid ,Email_Title ,Email_Time ,Email_Content ,Email_Enclosure ,FromUser ,FromUserName ,ToUser ,ToUserName ,CopyToUser ,CopyToUserName ,Email_State from Email where Guid='" + OldGuid + "'";
                string sql1 = "select ToUserName,Email_State,ReadTime from Email_User where Guid='" + OldGuid + "' order by Email_State ";
                DataTable dt = OpeRecordDB.GetDataSetBySql(sql);
                DataTable dtUser = OpeRecordDB.GetDataSetBySql(sql1);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //OldGuid = this.Tag.ToString();
                    Text_MailTitel.Text = dt.Rows[0]["Email_Title"].ToString();
                    label_fujian.Text = dt.Rows[0]["Email_Enclosure"].ToString();
                    labFromUser.Tag = dt.Rows[0]["FromUser"].ToString();
                    labFromUser.Text = dt.Rows[0]["FromUserName"].ToString();
                    Text_ToUser.Tag = dt.Rows[0]["ToUser"].ToString();
                    Text_ToUser.Text = dt.Rows[0]["ToUserName"].ToString();
                    Text_CopyTo.Tag = dt.Rows[0]["CopyToUser"].ToString();
                    Text_CopyTo.Text = dt.Rows[0]["CopyToUserName"].ToString();
                    Rtx_Content.Rtf = dt.Rows[0]["Email_Content"].ToString();
                    checkBox1.Checked = dt.Rows[0]["Email_State"].ToString() == "1" ? true : false;
                    checkBox1.Enabled = false;
                    //显示附件
                    ListFolder(label_fujian.Text);
                }

                #region 收到的文件

                if (dtUser != null && dtUser.Rows.Count > 0)
                {
                    listView2.Items.Clear();
                    for (int i = 0; i < dtUser.Rows.Count; i++)
                    {
                        ListViewItem li = new ListViewItem();
                        //li.ImageIndex = 0;
                        li.Text = dtUser.Rows[i]["ToUserName"].ToString();
                        li.SubItems.Add(dtUser.Rows[i]["Email_State"].ToString());
                        li.SubItems.Add(dtUser.Rows[i]["ReadTime"].ToString());
                        this.listView2.Items.Add(li);
                    }
                }
                #endregion
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        #endregion        

        #region 上传下载附件

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUploadFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fl = new OpenFileDialog();
                fl.Filter = "所有文件(*.*)|*.*";
                fl.Multiselect = false;

                DialogResult dl = fl.ShowDialog();
                if (dl == DialogResult.OK)
                {
                    UploadFile(fl.FileName);
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 上传附件
        /// </summary>
        /// <param name="File"></param>
        private void UploadFile(String File)
        {
            try
            {
                string serverpath = Uploadurl;
                string fileName = File.Substring(File.LastIndexOf("\\") + 1);
                string fileNameExt = File.Substring(File.LastIndexOf(".") + 1);//后缀名
                string newFileName = DateTime.Now.ToString("yyyyMMddhhmmss") + fileName;
                //string Md5FileName = "";
                string state = "";
                
                label7.Text = "上传中...";
                label7.Visible = true;
                //开始上传附件
                progressBar1.Visible = true;
                int uploadcount = UploadFiled.Upload_Request(serverpath, File, Encrypt.Md5To32(newFileName) + "." + fileNameExt, this.progressBar1, out state);
                if (uploadcount > 0)
                {
                    progressBar1.Maximum = 100;
                    label_fujian.Text += newFileName + "|";
                    //label_fujian.Visible = true;
                    label7.Text = "上传成功";
                    //添加到listview显示
                    ListFolder(newFileName);

                    int count = 0;
                    System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
                    t.Interval = 1000;
                    t.Enabled = true;
                    t.Tick += delegate(object sender1, EventArgs e1)
                    {
                        count++;
                        if (count == 3)
                        {
                            t.Enabled = false;
                            label7.Text = "状态";
                            label7.Visible = false;
                            progressBar1.Maximum = 0;
                            progressBar1.Visible = false;
                            t.Dispose();
                            sender1 = null;
                        }
                    };
                    
                }
                else
                    MessageBox.Show("上传失败!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("上传失败!\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        /// <summary>
        /// 下载全部附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {                
                if (label_fujian.Text == "") return;
                //System.Diagnostics.Process.Start("http://localhost:1180/UploadFile/Documents/2.doc");
                FolderBrowserDialog fbl = new FolderBrowserDialog();
                DialogResult dl = fbl.ShowDialog();
                if (dl == DialogResult.OK)
                {
                    saveUrl = fbl.SelectedPath;

                    Thread th = new Thread(new ThreadStart(DonwLoadFile));
                    th.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("附件下载失败!\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        /// <summary>
        /// 下载选中附件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                SaveFileDialog fbl = new SaveFileDialog();
                fbl.Filter = "*.*|所有文件";
                fbl.FileName = listView1.SelectedItems[0].Text;
                DialogResult dl = fbl.ShowDialog();
                if (dl == DialogResult.OK)
                {
                    String LocalFilePath = fbl.FileName;
                    saveUrl = LocalFilePath.Substring(0, LocalFilePath.LastIndexOf("\\"));
                    string fileNameExt = LocalFilePath.Substring(LocalFilePath.LastIndexOf("\\") + 1);
                    DonwLoadFile(listView1.SelectedItems[0].Text, fileNameExt);
                    //Thread th = new Thread(new ThreadStart(DonwLoadFile));
                    //th.Start();
                }
            }
        }
        string saveUrl = Application.StartupPath + @"\FileCache"; //另存路径
        private void DonwLoadFile()
        {
            string loadstate = "";
            String[] fileList = label_fujian.Text.Split('|');

            foreach (string fileName in fileList)
            {
                if (fileName != "")
                {
                    string fileNameExt = fileName.Substring(fileName.LastIndexOf(".") + 1);//后缀名
                    bool state = UploadFiled.Download(DownLoadurl + Encrypt.Md5To32(fileName) + "." + fileNameExt, saveUrl, fileName);
                    if (!state)
                        loadstate += fileName + "|";
                }
            }
            if (loadstate == "")
                MessageBox.Show("附件下载成功!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("附件下载失败!\n" + loadstate, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private bool DonwLoadFile(String Filename,string NewFileName)
        {
            bool state = false;
            string _newFileNmae = Filename;
            if (NewFileName != "")
                _newFileNmae = NewFileName;
            if (Filename != "")
            {
                string fileNameExt = Filename.Substring(Filename.LastIndexOf(".") + 1);//后缀名
                state = UploadFiled.Download(DownLoadurl + Encrypt.Md5To32(Filename) + "." + fileNameExt, saveUrl, _newFileNmae);
            }
            return state;
        }

        /// <summary>
        /// 显示附件列表
        /// </summary>
        /// <param name="directory">附件字符串</param>
        private void ListFolder(string directory)
        {
            String[] fileList = directory.Split('|');

            foreach (string fileName in fileList)
            {
                //fileName.Substring(fileName.LastIndexOf(".") + 1);//后缀名
                if (fileName != "")
                {
                    ListViewItem itemName = new ListViewItem(fileName);
                    itemName.Tag = fileName;
                    itemName.ImageIndex = 3;
                    listView1.Items.Add(itemName);
                }
            }
        }

        #endregion

        #region 菜单
        
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (Text_MailTitel.Text == "")
            {
                MessageBox.Show("请填写标题！");
                return;
            }
            if (Text_ToUser.Text == "")
            {
                MessageBox.Show("请选择收件人！");
                return;
            }

            if(SaveMail("1"))
                MessageBox.Show("发送成功!", "Messag", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
        
        /// <summary>
        /// 保存草稿
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if(SaveMail("-1"))
                MessageBox.Show("保存成功!", "Messag", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        /// <summary>
        /// 回复
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            //显示发送按钮
            this.toolStripButton1.Visible = true;
            this.toolStripButton2.Visible = true;
            this.toolStripButton4.Enabled = false;
            this.toolStripButton5.Enabled = false;
            //把件人替换到收件人
            //1.取出发件人
            string oldFromUser = Convert.ToString(labFromUser.Tag);
            string oldFromUserName = labFromUser.Text;
            //2.替换发件人
            labFromUser.Tag = InitUser[0].ToString();
            labFromUser.Text = InitUser[1].ToString();
            //3.把取出的发件人复制给收件人
            Text_ToUser.Tag = oldFromUser + ";";
            Text_ToUser.Text = oldFromUserName + ";";
            
            //内容拼接
            Text_MailTitel.Text = "回复:" + Text_MailTitel.Text;
            Rtx_Content.Text = "\n\n------------------ 原始邮件 ------------------\n" + Rtx_Content.Text;
            Text_MailTitel.Focus();
            //清空附件
            label_fujian.Text = "";
            listView1.Items.Clear();
            listView1.Columns.Clear();
            //已读回执
            checkBox1.Enabled = true;
            checkBox1.Checked = false;
        }

        /// <summary>
        /// 转发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            //显示发送按钮
            this.toolStripButton1.Visible = true;
            this.toolStripButton2.Visible = true;
            this.toolStripButton4.Enabled = false;
            this.toolStripButton5.Enabled = false;
            //置空收件人
            Text_ToUser.Tag = "";
            Text_ToUser.Text = "";
            //置空抄送人
            Text_CopyTo.Tag = "";
            Text_CopyTo.Text = "";
            //替换发件人
            labFromUser.Tag = InitUser[0].ToString();
            labFromUser.Text = InitUser[1].ToString();
            //内容拼接
            Text_MailTitel.Text = "转发:" + Text_MailTitel.Text;
            Rtx_Content.Text = "\n\n------------------ 原始邮件 ------------------\n" + Rtx_Content.Text;
            Text_MailTitel.Focus();
            //已读回执
            checkBox1.Enabled = true;
            checkBox1.Checked = false;
        }

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            //查看邮件时才会出现
        }

        #endregion

        #region function

        /// <summary>
        /// 保存mail
        /// </summary>
        /// <param name="mailState">邮件状态</param>
        /// （-1 草稿）
        private bool SaveMail(string mailState)
        {
            //Email eml = new Email();
            MyService.EditMail eml = new MyService.EditMail();
            eml.Guid = System.Guid.NewGuid().ToString();
            eml.Email_Titel = Text_MailTitel.Text.ToString();
            eml.Email_Time = DateTime.Now.ToString();
            eml.Email_Content = this.Rtx_Content.Rtf.ToString();
            eml.Email_Enclosure = label_fujian.Text;
            eml.FromUser = Convert.ToString(labFromUser.Tag);
            eml.FromUserName = labFromUser.Text;
            eml.ToUser = Convert.ToString(Text_ToUser.Tag);
            eml.ToUserName = Text_ToUser.Text.ToString();
            eml.CopyToUser = Convert.ToString(Text_CopyTo.Tag);
            eml.CopyToUserName = Text_CopyTo.Text.ToString();
            eml.Email_State = checkBox1.Checked ? "1" : "";
            eml.IFDel = mailState;
            try
            {
                int Rowsint = OpeRecordDB.AddMail(eml);
                if (OldGuid != "" && FrmState == Mail_Enum.Form_State.DraftBox)
                {
                    MyService.ExecSQLEnum exSql = new MyService.ExecSQLEnum();
                    exSql.type = MyService.exType.delete;
                    exSql.from = "Email";
                    exSql.where = "where Guid ='" + OldGuid + "'";
                    OpeRecordDB.ExecSql(exSql);
                }
                #region 插入收件人和抄送人/通知

                if (Rowsint > 0 && mailState != "-1")
                {
                    //2015-01-04 yufh调整到webservice
                    
                    //通知收件人
                    SendMessage();
                }
                else if (Rowsint == 0)
                {
                    MessageBox.Show("保存失败,发生了未知的错误!","Message",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    return false;
                }
                #endregion                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "\n" + ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 通知收件人
        /// </summary>
        private void SendMessage()
        {
            try
            {
                //通知用户
                IMLibrary4.Protocol.Message msg = new IMLibrary4.Protocol.Message();
                msg.Content = "您收到一封邮件！来自：" + labFromUser.Text.ToString() + "\n邮件标题：" + Text_MailTitel.Text;//获得消息文本内容
                //msg.Font = {Name = "宋体" Size=10};
                //msg.Color = "{Name=ff000000, ARGB=(255, 0, 0, 0)}";
                msg.ImageInfo = "";
                msg.from = Convert.ToString(labFromUser.Tag);
                msg.to = Convert.ToString(Text_ToUser.Tag) + Convert.ToString(Text_CopyTo.Tag);
                msg.MessageType = IMLibrary4.Protocol.MessageType.Notice;
                msg.Title = "邮件通知";
                msg.remark = "邮件提醒";
                if (Text_ToUser.Text != "")
                    SendToUserMsg(msg, labFromUser.Tag.ToString());
                //Client.SendMessageToServer(msg, labFromUser.Tag.ToString());//触发发送通知消息事件
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "\n" + ex.Message);
            }
        }

        /// <summary>
        /// 隐藏所有TollButton
        /// </summary>
        private void HiddenButton()
        {
            for (int i = 0; i < toolStrip1.Items.Count; i++)
            {
                toolStrip1.Items[i].Visible = false;
            }
        }

        /// <summary>
        /// 显示菜单
        /// </summary>
        /// <param name="buttonString"></param>
        private void ShowButton(string buttonString)
        {
            try
            {
                for (int i = 0; i < toolStrip1.Items.Count; i++)
                {
                    if (buttonString.Contains(toolStrip1.Items[i].Name) == true)
                        toolStrip1.Items[i].Visible = true;
                }
            }
            catch { }
        }

        #endregion

        /// <summary>
        /// 双击选择发送用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (treeView1.SelectedNode.Tag is exUser)
                {
                    exUser user = treeView1.SelectedNode.Tag as exUser;

                    if (ToUserAndCopyTo == "ToUser")
                    {
                        //判断是否已经包含该用户
                        if (Convert.ToString(Text_ToUser.Tag).Contains(user.UserID) == true)
                            return;
                        Text_ToUser.Tag += user.UserID + ";";
                        Text_ToUser.Text += user.UserName + "<" + user.UserID + ">;";
                    }
                    if (ToUserAndCopyTo == "CopyTo")
                    {
                        //判断是否已经包含该用户
                        if (Convert.ToString(Text_CopyTo.Tag).Contains(user.UserID) == true)
                            return;
                        Text_CopyTo.Tag += user.UserID + ";";
                        Text_CopyTo.Text += user.UserName + "<" + user.UserID + ">;";
                    }
                }
                else
                {
                    if (treeView1.SelectedNode.Nodes.Count > 0)
                    {
                        for (int i = 0; i < treeView1.SelectedNode.Nodes.Count; i++)
                        {
                            if (treeView1.SelectedNode.Nodes[i].Tag is exUser)
                            {
                                exUser user = treeView1.SelectedNode.Nodes[i].Tag as exUser;

                                if (ToUserAndCopyTo == "ToUser")
                                {
                                    //判断是否已经包含该用户
                                    if (Convert.ToString(Text_ToUser.Tag).Contains(user.UserID) == false)
                                    {
                                        Text_ToUser.Tag += user.UserID + ";";
                                        Text_ToUser.Text += user.UserName + "<" + user.UserID + ">;";
                                    }
                                }
                                if (ToUserAndCopyTo == "CopyTo")
                                {
                                    //判断是否已经包含该用户
                                    if (Convert.ToString(Text_CopyTo.Tag).Contains(user.UserID) == false)
                                    {
                                        Text_CopyTo.Tag += user.UserID + ";";
                                        Text_CopyTo.Text += user.UserName + "<" + user.UserID + ">;";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch {  }            
        }

        /// <summary>
        /// 右键下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TSMenuDown_Click(object sender, EventArgs e)
        {
            button1_Click(sender, e);
        }

        /// <summary>
        /// 右键另存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TSMenuOpDown_Click(object sender, EventArgs e)
        {
            button3_Click(sender, e);
        }  

        /// <summary>
        /// 双击打开
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems[0].Text != "")
            {
                if (File.Exists(saveUrl + @"\" + listView1.SelectedItems[0].Text))
                    System.Diagnostics.Process.Start(saveUrl + @"\" + listView1.SelectedItems[0].Text);
                else
                {
                    if (DonwLoadFile(listView1.SelectedItems[0].Text,""))
                        System.Diagnostics.Process.Start(saveUrl + @"\" +listView1.SelectedItems[0].Text);
                }
            }
        }

       
      
        
    }
}
