﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Configuration;

namespace OurMsg.IMMail
{
    public class UploadFiled
    {
        #region HttpWebRequest上传

        /// <summary> 
        /// 将本地文件上传到指定的服务器(HttpWebRequest方法) 
        /// </summary> 
        /// <param name="address">文件上传到的服务器</param> 
        /// <param name="fileNamePath">要上传的本地文件（全路径）</param> 
        /// <param name="saveName">文件上传后的名称</param> 
        /// <param name="progressBar">上传进度条</param> 
        /// <returns>成功返回1，失败返回0</returns> 
        public static int Upload_Request(string address, string fileNamePath, string saveName, ProgressBar progressBar ,out string text_state)
        {
            string State = "";
            int returnValue = 0; 
            // 要上传的文件 
            FileStream fs = new FileStream(fileNamePath, FileMode.Open, FileAccess.Read);
            BinaryReader r = new BinaryReader(fs);     
            //时间戳 
            string strBoundary = "----------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + strBoundary + "\r\n");     
            //请求头部信息 
            StringBuilder sb = new StringBuilder();
            sb.Append("--");
            sb.Append(strBoundary);
            sb.Append("\r\n");
            sb.Append("Content-Disposition: form-data; name=\"");
            sb.Append("file");
            sb.Append("\"; filename=\"");
            sb.Append(saveName);
            sb.Append("\";");
            sb.Append("\r\n");
            sb.Append("Content-Type: ");
            sb.Append("application/octet-stream");
            sb.Append("\r\n");
            sb.Append("\r\n");
            string strPostHeader = sb.ToString();
            byte[] postHeaderBytes = Encoding.UTF8.GetBytes(strPostHeader);     
            // 根据uri创建HttpWebRequest对象 
            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(new Uri(address));
            httpReq.Method = "POST";     
            //对发送的数据不使用缓存 
            httpReq.AllowWriteStreamBuffering = false;     
            //设置获得响应的超时时间（300秒） 
            httpReq.Timeout = 300000;
            httpReq.ContentType = "multipart/form-data; boundary=" + strBoundary;
            long length = fs.Length + postHeaderBytes.Length + boundaryBytes.Length;
            long fileLength = fs.Length;
            httpReq.ContentLength = length;
            try
            {
                progressBar.Maximum = int.MaxValue;
                progressBar.Minimum = 0;
                progressBar.Value = 0;
                //每次上传4k
                int bufferLength = 4096;
                byte[] buffer = new byte[bufferLength]; 
                //已上传的字节数 
                long offset = 0;         
                //开始上传时间 
                DateTime startTime = DateTime.Now;
                int size = r.Read(buffer, 0, bufferLength);
                Stream postStream = httpReq.GetRequestStream();         
                //发送请求头部消息 
                postStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);
                while (size > 0)
                {
                    postStream.Write(buffer, 0, size);
                    offset += size;
                    progressBar.Value = (int)(offset * (int.MaxValue / length));
                    TimeSpan span = DateTime.Now - startTime;
                    double second = span.TotalSeconds;
                    State += "已用时：" + second.ToString("F2") + "秒";
                    if (second > 0.001)
                    {
                        State += "\r平均速度：" + (offset / 1024 / second).ToString("0.00") + "KB/秒";
                    }
                    else
                    {
                        State += "\r正在连接…";
                    }
                    State += "\r已上传：" + (offset * 100.0 / length).ToString("F2") + "%";
                    State += "\r" + (offset / 1048576.0).ToString("F2") + "M/" + (fileLength / 1048576.0).ToString("F2") + "M";
                    Application.DoEvents();
                    size = r.Read(buffer, 0, bufferLength);
                }
                //添加尾部的时间戳 
                postStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                postStream.Close();         
                //获取服务器端的响应 
                WebResponse webRespon = httpReq.GetResponse();
                Stream s = webRespon.GetResponseStream();
                //读取服务器端返回的消息
                StreamReader sr = new StreamReader(s);
                String sReturnString = sr.ReadLine();
                s.Close();
                sr.Close();
                if (sReturnString == "Success")
                {
                    returnValue = 1;
                }
                else if (sReturnString == "Error")
                {
                    returnValue = 0;
                }
            }
            catch
            {
                returnValue = 0;
            }
            finally
            {
                fs.Close();
                r.Close();
            }
            text_state = State;
            return returnValue;
        }

        #endregion

        #region 未测试通过-废弃

        /// <summary>
        /// WebClient上传文件至服务器
        /// </summWebClient上传文件至服务器ary>
        /// <param name="localFilePath"></param>
        /// <param name="serverFolder">服务器文件夹路径</param>
        /// <param name="reName">是否需要修改文件名，这里默认是日期格式</param>
        /// <param name="NewFileName">返回新文件名</param>
        /// <returns></returns>
        public static bool UploadFile(string localFilePath, string serverFolder, bool reName,out String _NewFileName)
        {
            string fileNameExt, newFileName, uriString;
            if (reName)
            {
                fileNameExt = localFilePath.Substring(localFilePath.LastIndexOf(".") + 1);
                newFileName = DateTime.Now.ToString("yyMMddhhmmss") + localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1);                
            }
            else
            {
                newFileName = localFilePath.Substring(localFilePath.LastIndexOf("\\") + 1);
            }
            //把新服务器的新文件名返回给客户端
            _NewFileName = newFileName;

            if (!serverFolder.EndsWith("/") && !serverFolder.EndsWith("\\"))
            {
                serverFolder = serverFolder + "/";
            }

            uriString = serverFolder + newFileName;   //服务器保存路径
            /// 创建WebClient实例
            WebClient myWebClient = new WebClient();
            myWebClient.Credentials = CredentialCache.DefaultCredentials;

            // 要上传的文件
            FileStream fs = new FileStream(localFilePath, FileMode.Open, FileAccess.Read);
            BinaryReader r = new BinaryReader(fs);
            try
            {
                //使用UploadFile方法可以用下面的格式
                //myWebClient.UploadFile(uriString,"PUT",localFilePath);
                byte[] postArray = r.ReadBytes((int)fs.Length);
                Stream postStream = myWebClient.OpenWrite(uriString, "PUT");
                if (postStream.CanWrite)
                {
                    postStream.Write(postArray, 0, postArray.Length);
                }
                else
                {
                    MessageBox.Show("文件目前不可写！");
                }
                postStream.Close();
                fs.Close();
            }
            catch
            {
                //MessageBox.Show("文件上传失败，请稍候重试~");
                return false;
            }

            return true;
        }

        #endregion

        #region Dowload

        /// <summary>
        /// 下载服务器文件至客户端
        /// </summary>
        /// <param name="uri">被下载的文件地址</param>
        /// <param name="savePath">另存放的目录</param>
        public static bool Download(string uri, string savePath,string FileName)
        {
            string _fileName;  //被下载的文件名
            if (uri.IndexOf("\\") > -1)
            {
                _fileName = uri.Substring(uri.LastIndexOf("\\") + 1);
            }
            else
            {
                _fileName = uri.Substring(uri.LastIndexOf("/") + 1);
            }


            if (!savePath.EndsWith("/") && !savePath.EndsWith("\\"))
            {
                savePath = savePath + "/";
            }
            if (FileName!="")
                savePath += FileName;   //另存为的绝对路径＋文件名
            else
                savePath += _fileName;   //另存为的绝对路径＋文件名

            WebClient client = new WebClient();
            try
            {
                client.DownloadFile(uri, savePath);
            }
            catch
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}
