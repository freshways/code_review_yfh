﻿namespace OurMsg.IMMail
{
    partial class FormMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMail));
            CCWin.SkinControl.Animation animation1 = new CCWin.SkinControl.Animation();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TSMenuDown = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMenuOpDown = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.Rtx_Content = new RicherTextBox.RicherTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listView2 = new System.Windows.Forms.ListView();
            this.UserName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MailState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ReadTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button2 = new System.Windows.Forms.Button();
            this.btnUploadFile = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.skinTabControl1 = new CCWin.SkinControl.SkinTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnClearCopyUser = new CCWin.SkinControl.SkinButton();
            this.btnClearToUser = new CCWin.SkinControl.SkinButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label_fujian = new System.Windows.Forms.Label();
            this.labFromUser = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Text_CopyTo = new System.Windows.Forms.TextBox();
            this.Text_ToUser = new System.Windows.Forms.TextBox();
            this.Text_MailTitel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.skinTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "mail_attachment.png");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMenuDown,
            this.TSMenuOpDown});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 48);
            // 
            // TSMenuDown
            // 
            this.TSMenuDown.Name = "TSMenuDown";
            this.TSMenuDown.Size = new System.Drawing.Size(112, 22);
            this.TSMenuDown.Text = "下载";
            this.TSMenuDown.Click += new System.EventHandler(this.TSMenuDown_Click);
            // 
            // TSMenuOpDown
            // 
            this.TSMenuOpDown.Name = "TSMenuOpDown";
            this.TSMenuOpDown.Size = new System.Drawing.Size(112, 22);
            this.TSMenuOpDown.Text = "另存为";
            this.TSMenuOpDown.Click += new System.EventHandler(this.TSMenuOpDown_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.ItemSize = new System.Drawing.Size(48, 19);
            this.tabControl1.Location = new System.Drawing.Point(97, 236);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(648, 185);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.Rtx_Content);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(640, 158);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "内容";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // Rtx_Content
            // 
            this.Rtx_Content.AlignCenterVisible = true;
            this.Rtx_Content.AlignLeftVisible = true;
            this.Rtx_Content.AlignRightVisible = true;
            this.Rtx_Content.BoldVisible = true;
            this.Rtx_Content.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Rtx_Content.BulletsVisible = true;
            this.Rtx_Content.ChooseFontVisible = true;
            this.Rtx_Content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Rtx_Content.FontColorVisible = true;
            this.Rtx_Content.FontFamilyVisible = false;
            this.Rtx_Content.FontSizeVisible = true;
            this.Rtx_Content.GroupAlignmentVisible = true;
            this.Rtx_Content.GroupBoldUnderlineItalicVisible = true;
            this.Rtx_Content.GroupFontColorVisible = true;
            this.Rtx_Content.GroupFontNameAndSizeVisible = false;
            this.Rtx_Content.GroupIndentationAndBulletsVisible = true;
            this.Rtx_Content.GroupInsertVisible = true;
            this.Rtx_Content.GroupSaveAndLoadVisible = true;
            this.Rtx_Content.GroupZoomVisible = false;
            this.Rtx_Content.INDENT = 10;
            this.Rtx_Content.IndentVisible = true;
            this.Rtx_Content.InsertPictureVisible = true;
            this.Rtx_Content.ItalicVisible = true;
            this.Rtx_Content.LoadVisible = true;
            this.Rtx_Content.Location = new System.Drawing.Point(0, 0);
            this.Rtx_Content.Name = "Rtx_Content";
            this.Rtx_Content.OutdentVisible = true;
            this.Rtx_Content.Rtf = "{\\rtf1\\ansi\\ansicpg936\\deff0\\deflang1033\\deflangfe2052{\\fonttbl{\\f0\\fnil\\fcharset" +
    "204 Microsoft Sans Serif;}}\r\n\\viewkind4\\uc1\\pard\\lang2052\\f0\\fs18\\par\r\n}\r\n";
            this.Rtx_Content.SaveVisible = true;
            this.Rtx_Content.SeparatorAlignVisible = true;
            this.Rtx_Content.SeparatorBoldUnderlineItalicVisible = true;
            this.Rtx_Content.SeparatorFontColorVisible = true;
            this.Rtx_Content.SeparatorFontVisible = true;
            this.Rtx_Content.SeparatorIndentAndBulletsVisible = true;
            this.Rtx_Content.SeparatorInsertVisible = true;
            this.Rtx_Content.SeparatorSaveLoadVisible = true;
            this.Rtx_Content.Size = new System.Drawing.Size(640, 158);
            this.Rtx_Content.TabIndex = 6;
            this.Rtx_Content.ToolStripVisible = true;
            this.Rtx_Content.UnderlineVisible = true;
            this.Rtx_Content.WordWrapVisible = true;
            this.Rtx_Content.ZoomFactorTextVisible = false;
            this.Rtx_Content.ZoomInVisible = false;
            this.Rtx_Content.ZoomOutVisible = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.listView2);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(640, 158);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "收件人";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.UserName,
            this.MailState,
            this.ReadTime});
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.FullRowSelect = true;
            this.listView2.Location = new System.Drawing.Point(0, 0);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(640, 158);
            this.listView2.TabIndex = 14;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // UserName
            // 
            this.UserName.Text = "用户";
            this.UserName.Width = 120;
            // 
            // MailState
            // 
            this.MailState.Text = "阅读状态";
            // 
            // ReadTime
            // 
            this.ReadTime.Text = "时间";
            this.ReadTime.Width = 160;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(609, 518);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(69, 20);
            this.button2.TabIndex = 13;
            this.button2.Text = "删除附件";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // btnUploadFile
            // 
            this.btnUploadFile.Location = new System.Drawing.Point(97, 516);
            this.btnUploadFile.Name = "btnUploadFile";
            this.btnUploadFile.Size = new System.Drawing.Size(69, 25);
            this.btnUploadFile.TabIndex = 13;
            this.btnUploadFile.Text = "上传附件";
            this.btnUploadFile.UseVisualStyleBackColor = true;
            this.btnUploadFile.Click += new System.EventHandler(this.btnUploadFile_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(228, 516);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(69, 25);
            this.button3.TabIndex = 13;
            this.button3.Text = "下载选中附件";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(308, 516);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(69, 25);
            this.button1.TabIndex = 13;
            this.button1.Text = "全部下载附件";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listView1
            // 
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.Location = new System.Drawing.Point(97, 424);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(646, 87);
            this.listView1.TabIndex = 12;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDoubleClick);
            // 
            // skinTabControl1
            // 
            animation1.AnimateOnlyDifferences = false;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 2F;
            animation1.TransparencyCoeff = 0F;
            this.skinTabControl1.Animation = animation1;
            this.skinTabControl1.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.skinTabControl1.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.skinTabControl1.Controls.Add(this.tabPage1);
            this.skinTabControl1.ItemSize = new System.Drawing.Size(70, 36);
            this.skinTabControl1.Location = new System.Drawing.Point(773, 83);
            this.skinTabControl1.Name = "skinTabControl1";
            this.skinTabControl1.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowDown")));
            this.skinTabControl1.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowHover")));
            this.skinTabControl1.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseHover")));
            this.skinTabControl1.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseNormal")));
            this.skinTabControl1.PageDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageDown")));
            this.skinTabControl1.PageHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageHover")));
            this.skinTabControl1.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.skinTabControl1.PageNorml = null;
            this.skinTabControl1.SelectedIndex = 0;
            this.skinTabControl1.Size = new System.Drawing.Size(211, 434);
            this.skinTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.skinTabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.treeView1);
            this.tabPage1.Location = new System.Drawing.Point(0, 36);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(211, 398);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "通讯录";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.ImageIndex = 1;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(3, 3);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(205, 392);
            this.treeView1.TabIndex = 10;
            this.treeView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseDoubleClick);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(402, 522);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 12);
            this.progressBar1.TabIndex = 9;
            this.progressBar1.Visible = false;
            // 
            // btnClearCopyUser
            // 
            this.btnClearCopyUser.BackColor = System.Drawing.Color.Transparent;
            this.btnClearCopyUser.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnClearCopyUser.DownBack = global::OurMsg.Properties.Resources.sysbtn_close_hover;
            this.btnClearCopyUser.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.btnClearCopyUser.Location = new System.Drawing.Point(749, 173);
            this.btnClearCopyUser.MouseBack = global::OurMsg.Properties.Resources.sysbtn_close_down;
            this.btnClearCopyUser.Name = "btnClearCopyUser";
            this.btnClearCopyUser.NormlBack = global::OurMsg.Properties.Resources.sysbtn_close_hover;
            this.btnClearCopyUser.Size = new System.Drawing.Size(15, 15);
            this.btnClearCopyUser.TabIndex = 7;
            this.btnClearCopyUser.UseVisualStyleBackColor = false;
            // 
            // btnClearToUser
            // 
            this.btnClearToUser.BackColor = System.Drawing.Color.Transparent;
            this.btnClearToUser.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnClearToUser.DownBack = global::OurMsg.Properties.Resources.sysbtn_close_hover;
            this.btnClearToUser.DrawType = CCWin.SkinControl.DrawStyle.Img;
            this.btnClearToUser.Location = new System.Drawing.Point(749, 113);
            this.btnClearToUser.MouseBack = global::OurMsg.Properties.Resources.sysbtn_close_down;
            this.btnClearToUser.Name = "btnClearToUser";
            this.btnClearToUser.NormlBack = global::OurMsg.Properties.Resources.sysbtn_close_hover;
            this.btnClearToUser.Radius = 4;
            this.btnClearToUser.Size = new System.Drawing.Size(15, 15);
            this.btnClearToUser.TabIndex = 7;
            this.btnClearToUser.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(528, 522);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "状态";
            this.label7.Visible = false;
            // 
            // label_fujian
            // 
            this.label_fujian.AutoSize = true;
            this.label_fujian.BackColor = System.Drawing.Color.Transparent;
            this.label_fujian.Location = new System.Drawing.Point(38, 522);
            this.label_fujian.Name = "label_fujian";
            this.label_fujian.Size = new System.Drawing.Size(29, 12);
            this.label_fujian.TabIndex = 5;
            this.label_fujian.Text = "附件";
            this.label_fujian.Visible = false;
            // 
            // labFromUser
            // 
            this.labFromUser.AutoSize = true;
            this.labFromUser.BackColor = System.Drawing.Color.Transparent;
            this.labFromUser.Location = new System.Drawing.Point(95, 212);
            this.labFromUser.Name = "labFromUser";
            this.labFromUser.Size = new System.Drawing.Size(41, 12);
            this.labFromUser.TabIndex = 5;
            this.labFromUser.Text = "发件人";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("宋体", 10F);
            this.label5.Location = new System.Drawing.Point(31, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "发件人:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("宋体", 10F);
            this.label8.Location = new System.Drawing.Point(37, 436);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 3;
            this.label8.Text = "附 件:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("宋体", 10F);
            this.label4.Location = new System.Drawing.Point(31, 259);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "内  容:";
            // 
            // Text_CopyTo
            // 
            this.Text_CopyTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text_CopyTo.BackColor = System.Drawing.SystemColors.Window;
            this.Text_CopyTo.Location = new System.Drawing.Point(97, 169);
            this.Text_CopyTo.Multiline = true;
            this.Text_CopyTo.Name = "Text_CopyTo";
            this.Text_CopyTo.ReadOnly = true;
            this.Text_CopyTo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Text_CopyTo.Size = new System.Drawing.Size(646, 33);
            this.Text_CopyTo.TabIndex = 3;
            // 
            // Text_ToUser
            // 
            this.Text_ToUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text_ToUser.BackColor = System.Drawing.SystemColors.Window;
            this.Text_ToUser.Location = new System.Drawing.Point(97, 106);
            this.Text_ToUser.Multiline = true;
            this.Text_ToUser.Name = "Text_ToUser";
            this.Text_ToUser.ReadOnly = true;
            this.Text_ToUser.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Text_ToUser.Size = new System.Drawing.Size(646, 57);
            this.Text_ToUser.TabIndex = 2;
            // 
            // Text_MailTitel
            // 
            this.Text_MailTitel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text_MailTitel.Location = new System.Drawing.Point(97, 78);
            this.Text_MailTitel.Name = "Text_MailTitel";
            this.Text_MailTitel.Size = new System.Drawing.Size(646, 21);
            this.Text_MailTitel.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("宋体", 10F);
            this.label3.Location = new System.Drawing.Point(31, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "抄送人:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("宋体", 10F);
            this.label2.Location = new System.Drawing.Point(31, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "收件人:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("宋体", 10F);
            this.label1.Location = new System.Drawing.Point(31, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "标  题:";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton3,
            this.toolStripButton6});
            this.toolStrip1.Location = new System.Drawing.Point(4, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(983, 40);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton1.Text = "发送";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::OurMsg.Properties.Resources.mail_Drafts;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(48, 37);
            this.toolStripButton2.Text = "存草稿";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton4.Text = "回复";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::OurMsg.Properties.Resources.mail_transmit;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton5.Text = "转发";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::OurMsg.Properties.Resources.Exit_easyicon_net_48;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton3.Text = "关闭";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = global::OurMsg.Properties.Resources.mail_del;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton6.Text = "删除";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Visible = false;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.Transparent;
            this.checkBox1.Font = new System.Drawing.Font("宋体", 10F);
            this.checkBox1.Location = new System.Drawing.Point(584, 212);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(82, 18);
            this.checkBox1.TabIndex = 15;
            this.checkBox1.Text = "已读回执";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // FormMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 549);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnUploadFile);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.skinTabControl1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnClearCopyUser);
            this.Controls.Add(this.btnClearToUser);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label_fujian);
            this.Controls.Add(this.labFromUser);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Text_CopyTo);
            this.Controls.Add(this.Text_ToUser);
            this.Controls.Add(this.Text_MailTitel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMail";
            this.Text = "新增邮件";
            this.Load += new System.EventHandler(this.FormMail_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.skinTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Text_MailTitel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Text_ToUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Text_CopyTo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labFromUser;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private RicherTextBox.RicherTextBox Rtx_Content;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private CCWin.SkinControl.SkinButton btnClearToUser;
        private CCWin.SkinControl.SkinButton btnClearCopyUser;
        private System.Windows.Forms.Label label_fujian;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TreeView treeView1;
        public System.Windows.Forms.ImageList imageList1;
        private CCWin.SkinControl.SkinTabControl skinTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnUploadFile;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem TSMenuDown;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader UserName;
        private System.Windows.Forms.ColumnHeader MailState;
        private System.Windows.Forms.ColumnHeader ReadTime;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ToolStripMenuItem TSMenuOpDown;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}