﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IMLibrary4;
using OurMsg.Controls;

namespace OurMsg.IMMail
{
    public partial class FormMailList : BaseForm
    {
        public FormMailList()
        {
            InitializeComponent();
        }

        #region 委托/事件

        /// <summary>
        /// 声明委托
        /// </summary>
        public delegate void MailListEventHandler(object sender);        

        /// <summary>
        /// 读取邮件
        /// </summary>
        public event MailListEventHandler ReadMail;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="FromUser"></param>
        public delegate void SendToUserMsgHandler(IMLibrary4.Protocol.Message msg, String FromUser);

        public event SendToUserMsgHandler SendeToUserMsg;

        #endregion


        #region 变量

        private Form form1;
        private Mail_Enum.Form_State FormState;
        private StringBuilder sb = new StringBuilder();
        private string InboxButn = "toolStripButton_Add|toolStripButton_View|toolStripButton_State|toolStripButton_Select|toolStripButton4";

        #endregion

        private void FormMailList_Load(object sender, EventArgs e)
        {
            HiddenButton();
            FormState = Mail_Enum.Form_State.InBox;
            ShowButton(InboxButn);
            BindMailList();
        }

        private void toolStripButton_Select_Click(object sender, EventArgs e)
        {
            BindMailList();
        }

        #region 菜单事件

        /// <summary>
        /// 新邮件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton_Add_Click(object sender, EventArgs e)
        {
            FormMail frm = new FormMail(null, Mail_Enum.Form_State.NewMail, "新增邮件");
            //发送邮件通知代理事件
            frm.SendToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
            {
                SendeToUserMsg(msg, FromUser);
            };
            if (Application.OpenForms[frm.Name] != null)
            {
                form1 = Application.OpenForms[frm.Name];
                form1.Activate();
            }
            else
                frm.Show();
            //frm.Show();
        }

        /// <summary>
        /// 查看
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton_View_Click(object sender, EventArgs e)
        {
            if (dataGridView1.DataSource == null || dataGridView1.Rows.Count == 0)
                return;
            string guid = dataGridView1.SelectedRows[0].Cells["Guid"].Value.ToString();
            FormMail frm = new FormMail(null, FormState, "查看邮件");
            //发送邮件通知代理事件
            frm.SendToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
            {
                SendeToUserMsg(msg, FromUser);
            };

            frm.OldGuid = guid;
            
            //判断是未读的邮件进行标记
            if (dataGridView1.SelectedRows[0].Cells["Email_State"].Value.ToString() == "未读")
            {
                //sb.Remove(0, sb.Length);
                ////Guid, Email_Title, Email_Time, Email_Content, Email_Enclosure, FromUser, ToUser, Email_State from Email
                //sb.Append("update Email_User set Email_State='已读' ");
                //sb.Append(" where Guid='").Append(guid).Append("' ");
                //sb.Append(" and ToUser='").Append(Global.CurrUserID).Append("' ");
                MyService.ExecSQLEnum exSql = new MyService.ExecSQLEnum();
                exSql.type = MyService.exType.update;
                exSql.from = "Email_User";
                exSql.column = "Email_State='已读'";
                exSql.where = "where Guid='" + guid + "' and ToUser='" + Global.CurrUserID + "' ";
                try
                {
                    int rowint = OpeRecordDB.ExecSql(exSql);
                    dataGridView1.SelectedRows[0].Cells["Email_State"].Value = "已读";
                    //已读之后文字变普通
                    System.Drawing.Font fonta = new System.Drawing.Font("", dataGridView1.Font.Size, System.Drawing.FontStyle.Regular);
                    dataGridView1.SelectedRows[0].DefaultCellStyle.Font = fonta;
                    //委托已读事件
                    ReadMail(IMLibrary4.OpeRecordDB.GetMailCount(Global.CurrUserID).ToString());
                }
                catch { }                
            }
            //显示邮件
            if (Application.OpenForms[frm.Name] != null)
            {
                form1 = Application.OpenForms[frm.Name];
                form1.Activate();
            }
            else
                frm.Show();
            //MessageBox.Show(guid);
        }

        /// <summary>
        /// 标记已读
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton_State_Click(object sender, EventArgs e)
        {
            string guid = dataGridView1.SelectedRows[0].Cells["Guid"].Value.ToString();
            if (dataGridView1.SelectedRows[0].Cells["Email_State"].Value.ToString() == "未读")
            {
                MyService.ExecSQLEnum exSql = new MyService.ExecSQLEnum();
                exSql.type = MyService.exType.update;
                exSql.from = "Email_User";
                exSql.column = "Email_State='已读'";
                exSql.where = "where Guid='" + guid + "' and ToUser='" + Global.CurrUserID + "' ";
                try
                {
                    int rowint = OpeRecordDB.ExecSql(exSql);
                    dataGridView1.SelectedRows[0].Cells["Email_State"].Value = "已读";
                    //已读之后文字变普通
                    System.Drawing.Font fonta = new System.Drawing.Font("", dataGridView1.Font.Size, System.Drawing.FontStyle.Regular);
                    dataGridView1.SelectedRows[0].DefaultCellStyle.Font = fonta;
                    //委托已读事件
                    ReadMail(IMLibrary4.OpeRecordDB.GetMailCount(Global.CurrUserID).ToString());
                }
                catch { }
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            try
            {
                string guid = dataGridView1.SelectedRows[0].Cells["Guid"].Value.ToString();

                MyService.ExecSQLEnum exSql = new MyService.ExecSQLEnum();                

                if (FormState == Mail_Enum.Form_State.InBox)
                {
                    exSql.type = MyService.exType.update;
                    exSql.from = "Email_User";
                    exSql.column = "IFDel='0'";
                    exSql.where = "where Guid='" + guid + "' and ToUser='" + Global.CurrUserID + "'";
                }
                if (FormState == Mail_Enum.Form_State.OutBox)
                {
                    exSql.type = MyService.exType.update;
                    exSql.from = "Email";
                    exSql.column = "IFDel='0'";
                    exSql.where = "where Guid='" + guid + "'";
                }
                if (FormState == Mail_Enum.Form_State.DraftBox) //草稿箱
                {
                    exSql.type = MyService.exType.delete;
                    exSql.from = "Email";
                    exSql.where = "where Guid='" + guid + "'";
                }

                int rowint = OpeRecordDB.ExecSql(exSql);
                if (rowint > 0)
                {
                    MessageBox.Show("删除成功", "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
                }
            }
            catch { }
        }

        #endregion

        #region 方法
        
        /// <summary>
        /// 绑定邮件列表
        /// </summary>
        private void BindMailList()
        {
            try
            {
                sb.Remove(0, sb.Length);
                //Guid, Email_Title, Email_Time, Email_Content, Email_Enclosure, FromUser, ToUser, Email_State from Email
                
                if (FormState == Mail_Enum.Form_State.InBox) //收件箱
                {
                    sb.Append("select Email.Guid, Email_Title, Email_Time, FromUserName as FromUser, Email_User.Email_State,Email_Enclosure ");
                    sb.Append(" from Email,Email_User ");
                    sb.Append(" where Email.Guid = Email_User.Guid ");
                    sb.Append(" and Email_User.ToUser='").Append(Global.CurrUserID).Append("' ");
                    sb.Append(" and Email_User.IFDel<>'0' ");

                    dataGridView1.Columns["FromUser"].HeaderText = "发件人";
                }
                if (FormState == Mail_Enum.Form_State.OutBox) //发件箱
                {
                    sb.Append("select Guid, Email_Title, Email_Time, ToUserName as FromUser, '已发送' as Email_State,Email_Enclosure from Email ");
                    sb.Append(" where FromUser='").Append(Global.CurrUserID).Append("' ");
                    sb.Append(" and IFDel not in('0','-1') ");

                    dataGridView1.Columns["FromUser"].HeaderText = "收件人";
                }
                if (FormState == Mail_Enum.Form_State.DraftBox) //草稿箱
                {
                    sb.Append("select Guid, Email_Title, Email_Time, ToUserName as FromUser, '草稿' Email_State,Email_Enclosure from Email ");
                    sb.Append(" where IFDel='-1' ");

                    dataGridView1.Columns["FromUser"].HeaderText = "收件人";
                }
                sb.Append(" order by Email_Time desc");
                
                DataTable dt = OpeRecordDB.GetDataSetBySql(sb.ToString());
                if (dt == null)
                    return;
                dataGridView1.DataSource = dt;
                skinLabel2.Text = "共计(" + dt.Rows.Count.ToString() + "条)";
                
                #region 未读邮件变色
                //ColImg 
                if (FormState == Mail_Enum.Form_State.InBox)
                {
                    try
                    {
                        //定义字体样式，未读邮件行字体加粗
                        System.Drawing.Font fonta = new System.Drawing.Font("", dataGridView1.Font.Size, System.Drawing.FontStyle.Bold);
                        for (int i = 0; i < dataGridView1.Rows.Count; i++)
                        {
                            if (dataGridView1.Rows[i].Cells["Email_State"].Value.ToString() == "未读")
                            {
                                dataGridView1.Rows[i].DefaultCellStyle.Font = fonta;
                                //dataGridView1.Rows[i].Cells["ColImg"].Value = Properties.Resources.mail_attachment;
                            }
                        }
                    }
                    catch { }
                }
                #endregion
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("加载邮件失败！\n" + ex.Message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// 隐藏所有TollButton
        /// </summary>
        private void HiddenButton()
        {
            for (int i = 0; i < skinToolStrip1.Items.Count; i++)
            {
                skinToolStrip1.Items[i].Visible = false;
            }
        }

        /// <summary>
        /// 显示菜单
        /// </summary>
        /// <param name="buttonString"></param>
        private void ShowButton(string buttonString)
        {
            try
            {
                for (int i = 0; i < skinToolStrip1.Items.Count; i++)
                {
                    if (buttonString.Contains(skinToolStrip1.Items[i].Name) == true)
                        skinToolStrip1.Items[i].Visible = true;
                }
            }
            catch { }
        }

        #endregion

        #region 左侧菜单
        
        /// <summary>
        /// 收件箱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void skButton_InBox_Click(object sender, EventArgs e)
        {
            FormState = Mail_Enum.Form_State.InBox;
            skinLabel1.Text = skButton_InBox.Text;
            BindMailList();
        }

        /// <summary>
        /// 发件箱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void skButton_OutBox_Click(object sender, EventArgs e)
        {
            FormState = Mail_Enum.Form_State.OutBox;
            skinLabel1.Text = skButton_OutBox.Text;
            BindMailList();
        }

        /// <summary>
        /// 草稿箱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void skButton_DraftBox_Click(object sender, EventArgs e)
        {
            FormState = Mail_Enum.Form_State.DraftBox;
            skinLabel1.Text = skButton_DraftBox.Text;
            BindMailList();
        }

        #endregion

        #region 绘制附件图标
        
        private void skinDataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            try
            {
                if (dataGridView1.Rows[e.RowIndex].Cells["Email_State"].Value.ToString() == "未读")
                    e.Graphics.DrawImage(Properties.Resources.mail_1, e.RowBounds.Left + this.dataGridView1.RowHeadersWidth - 31,
                        e.RowBounds.Top + 4, 16, 16);//绘制图标 
                if (dataGridView1.Rows[e.RowIndex].Cells["Email_State"].Value.ToString() == "已读")
                    e.Graphics.DrawImage(Properties.Resources.mail_2, e.RowBounds.Left + this.dataGridView1.RowHeadersWidth - 31,
                        e.RowBounds.Top + 4, 16, 16);//绘制图标 
                if (dataGridView1.Rows[e.RowIndex].Cells["Email_Enclosure"].Value.ToString() != "")
                    e.Graphics.DrawImage(Properties.Resources.mail_attachment, e.RowBounds.Left + this.dataGridView1.RowHeadersWidth - 15,
                        e.RowBounds.Top + 4, 16, 16);//绘制图标 
            }
            catch { }
        }

        #endregion

        /// <summary>
        /// 双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            toolStripButton_View_Click(sender, e);
        }

        
    }

}
