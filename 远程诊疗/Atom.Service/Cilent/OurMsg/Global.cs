﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO ;
using System.Diagnostics;
using System.Reflection;
using System.Net;
using System.Configuration;

namespace OurMsg
{
    /// <summary>
    /// 全局访问类
    /// </summary>
    public sealed class Global
    {
        #region 变量
        /// <summary>
        /// 服务器域名 
        /// </summary>
        public static string ServerDomain = ConfigurationManager.AppSettings["ServerDomain"].ToString();//"127.0.0.1";// "192.168.10.110";//
      
        /// <summary>
        /// 消息服务器端口
        /// </summary>
        public static int ServerMsgPort = Convert.ToInt32(ConfigurationManager.AppSettings["ServerMsgPort"].ToString());//6500;// 

        /// <summary>
        /// 更新端口
        /// </summary>
        public static int UpLoadPort = Convert.ToInt32(ConfigurationManager.AppSettings["UpDatePort"].ToString());

        /// <summary>
        /// 服务器文件服务主机信息
        /// </summary>
        public static IPEndPoint FileTransmitServerEP=null;
        /// <summary>
        /// 
        /// </summary>
        public static IPEndPoint AVTransmitServerEP = null;

        /// <summary>
        /// 图片服务器主机信息
        /// </summary>
        public static IPEndPoint ImageServerEP = null;
        /// <summary>
        /// 离线文件服务器主机信息
        /// </summary>
        public static IPEndPoint OfflineFileServerEP = null;
        /// <summary>
        /// 会议服务器 
        /// </summary>
        public static string ServerConference = "@";

        /// <summary>
        /// 表情图片列表缓存
        /// </summary>
        public static System.Windows.Forms.ImageList ImageListFace = new ImageList();

        /// <summary>
        /// 消息管理窗
        /// </summary>
        public static  FormDataManage FormDataManageage = null;

        /// <summary>
        /// 当前登录用户ID
        /// </summary>
        public static string CurrUserID = "";

        /// <summary>
        /// 当前登录用户Name
        /// </summary>
        public static string CurrUserName = "";

        #region 扩展/邮箱配置/版本号/yufh 2015-01-07 添加

        /// <summary>
        /// 当前程序版本号
        /// </summary>
        public static string Version
        {
            get
            {
                try
                {
                    return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }
                catch (Exception ex) 
                { 
                    //MessageBox.Show("获取版本号异常\n" + ex.Source + ex.Message);  
                }
                return "";
            }
        }//= System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        /// <summary>
        /// webOA服务器IP
        /// </summary>
        public static string MailServer = ConfigurationManager.AppSettings["MailServer"].ToString();
        /// <summary>
        /// 端口
        /// </summary>
        public static string MailPort = ConfigurationManager.AppSettings["MailPort"].ToString();
        /// <summary>
        /// 附件上传目录
        /// </summary>
        public static string MailPath = ConfigurationManager.AppSettings["MailPath"].ToString();
        /// <summary>
        /// 附件上传接收处理页
        /// </summary>
        public static string MailIndex = ConfigurationManager.AppSettings["MailIndex"].ToString();
        /// <summary>
        /// 附件下载目录
        /// </summary>
        public static string MailLoadPath = ConfigurationManager.AppSettings["MailLoadPath"].ToString();

        #endregion

        #endregion

        #region 判断系统进程中程序是否已经运行有进行，有返回进程，无返回空值
        /// <summary>
        /// 判断系统进程中程序是否已经运行有进行，有返回进程，无返回空值
        /// </summary>
        /// <returns></returns>
        public static Process RunningInstance()
        {
            Process current = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(current.ProcessName);

            //Loop through the running processes in with the same name
            foreach (Process process in processes)
            {
                //Ignore the current process
                if (process.Id != current.Id)
                {
                    //Make sure that the process is running from the exe file.

                    if (Assembly.GetExecutingAssembly().Location.Replace("/", "\\") == current.MainModule.FileName)
                    {
                        //Return the other process instance.
                        return process;
                    }
                }
            }
            return null;
        }
        #endregion

    }
}
