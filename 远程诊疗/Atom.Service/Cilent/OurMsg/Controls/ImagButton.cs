﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.CompilerServices;

namespace OurMsg.Controls
{
    public partial class ImagButton : UserControl
    {
        public ImagButton()
        {
            InitializeComponent();
        }

        private void ImageButton_Load(object sender, EventArgs e)
        {
            this.pImage.Image = this.backgroundimage;

            this.lblTemp.Parent = this.pImage;
            this.lblTemp.Width = this.pImage.Width;
            this.lblTemp.Left = (int)Math.Round((double)((((double)this.pImage.Width) / 2.0) - (((double)this.lblTemp.Width) / 2.0)));
            this.lblTemp.Top = (int)Math.Round((double)((((double)this.pImage.Height) / 2.0) - (((double)this.lblTemp.Height) / 2.0)));
        }

        private void ImageButton_Resize(object sender, EventArgs e)
        {
            this.lblTemp.Left = (int)Math.Round((double)((((double)this.pImage.Width) / 2.0) - (((double)this.lblTemp.Width) / 2.0)));
            this.lblTemp.Top = (int)Math.Round((double)((((double)this.pImage.Height) / 2.0) - (((double)this.lblTemp.Height) / 2.0)));
        }

        #region 属性

        private Image backgroundimage;
        private Image mouseoverimage;
        private Image mouseclickimage;

        [Category("重要属性"), Description("按钮默认显示的背景图片")]
        public override Image BackgroundImage
        {
            get
            {
                return this.pImage.Image;
            }
            set
            {
                this.pImage.Image = value;
                this.backgroundimage = value;
            }
        }

        [Category("重要属性"), Description("按钮中要显示的文本")]
        public string ButtonText
        {
            get
            {
                return this.lblTemp.Text;
            }
            set
            {
                this.lblTemp.Text = value;
            }
        }

        [Category("重要属性"), Description("单击按钮时候显示的图片")]
        public Image MouseClickImage
        {
            get
            {
                return this.mouseclickimage;
            }
            set
            {
                this.mouseclickimage = value;
            }
        }

        [Category("重要属性"), Description("鼠标移动到按钮上时显示的图片")]
        public Image MouseOverImage
        {
            get
            {
                return this.mouseoverimage;
            }
            set
            {
                this.mouseoverimage = value;
            }
        }

        #endregion

        #region 事件

        public delegate void ClickEventHandler(object sender, EventArgs e);
        public event ClickEventHandler Click;

        private void OnClick(object sender, EventArgs e)
        {
            ClickEventHandler clickEvent = this.Click;
            if (clickEvent != null)
            {
                clickEvent(this, e);
            }
        }

        #region 鼠标事件

        private void Mouse_Enter(object sender, EventArgs e)
        {
            //this.Cursor = Cursors.Hand;
            this.pImage.Image = this.mouseoverimage;
        }

        private void Mouse_Leave(object sender, EventArgs e)
        {
            //this.Cursor = Cursors.Arrow;
            this.pImage.Image = this.backgroundimage;
        }

        private void Mouse_Down(object sender, MouseEventArgs e)
        {
            //this.Cursor = Cursors.Hand;
            this.pImage.Image = this.mouseclickimage;
        }

        private void Mouse_Hover(object sender, EventArgs e)
        {
            this.pImage.Image = this.mouseoverimage;
        }

        private void Mouse_Up(object sender, MouseEventArgs e)
        {
            this.pImage.Image = this.backgroundimage;
        }

        #endregion

        private void lblTemp_TextChanged(object sender, EventArgs e)
        {
            this.lblTemp.Parent = this.pImage;
            this.lblTemp.Width = this.pImage.Width;
            this.lblTemp.Left = (int)Math.Round((double)((((double)this.pImage.Width) / 2.0) - (((double)this.lblTemp.Width) / 2.0)));
            this.lblTemp.Top = (int)Math.Round((double)((((double)this.pImage.Height) / 2.0) - (((double)this.lblTemp.Height) / 2.0)));
        }

        private void pbImage_BackgroundImageChanged(object sender, EventArgs e)
        {
            this.pImage.Refresh();
        }

        #endregion

        private void pImage_Paint(object sender, PaintEventArgs e)
        {
            //Graphics g = e.Graphics;
            //StringFormat sf = new StringFormat();
            //sf.Alignment = StringAlignment.Center;
            //sf.LineAlignment = StringAlignment.Center;
            //if (BackgroundImage != null)
            //{
            //    Image img = this.BackgroundImage;
            //    g.DrawImage(img, e.ClipRectangle, 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
            //}
            //g.DrawString(Text, Font, new SolidBrush(Color.Black), e.ClipRectangle, sf);
        }
    }
}
