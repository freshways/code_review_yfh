﻿using System;
using System.Data;
using System.Windows.Forms;

namespace OurMsg.Controls
{
    public static class DataGridViewAssert
    {
        public static void InsertCheckBoxColumn(DataGridView dgv, int index)
        {
            DataTable dtPlanProducts = dgv.DataSource as DataTable; //获取数据源
            if (dtPlanProducts == null)
                throw new NoNullAllowedException(dgv.Name + "控件未绑定数据源");

            int originalColumnsCount = dtPlanProducts.Columns.Count;    //获取原始数据源列的个数
            if (index < 0 || index > originalColumnsCount)
                throw new IndexOutOfRangeException();

            dgv.CellClick += new DataGridViewCellEventHandler(dgv_CellClick);   //注册单元格单击事件

            //添加CheckBox列
            dtPlanProducts.Columns.Add(null, typeof(bool));
            dgv.Columns[originalColumnsCount].DisplayIndex = index;

            //添加CheckBox表头
            DatagridViewCheckBoxHeaderCell cbHeaderProducts = new DatagridViewCheckBoxHeaderCell();
            dgv.Columns[originalColumnsCount].HeaderCell = cbHeaderProducts;
            dgv.Columns[originalColumnsCount].Resizable = DataGridViewTriState.False;
            dgv.Columns[originalColumnsCount].Width = 20;
            dgv.Columns[originalColumnsCount].HeaderText = string.Empty;
        }

        static void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0) return;
            DataGridView dgv = sender as DataGridView;

            if (dgv.Columns[e.ColumnIndex].HeaderCell.GetType() == typeof(DatagridViewCheckBoxHeaderCell))
            {
                if (e.RowIndex < 0) //单击表头
                {
                    DatagridViewCheckBoxHeaderCell cell = dgv.Columns[e.ColumnIndex].HeaderCell as DatagridViewCheckBoxHeaderCell;
                    cell.Checked = !cell.Checked;
                    foreach (DataGridViewRow row in dgv.Rows)
                    {
                        if (row.IsNewRow) continue; //不处理新列
                        if (row.Cells[e.ColumnIndex] == dgv.CurrentCell)
                            dgv.EndEdit();

                        row.Cells[e.ColumnIndex].Value = cell.Checked;  //表头状态赋予到记录
                    }
                }
                else if (dgv.Rows[e.RowIndex].IsNewRow) { } //不处理新列的单击事件
                else   //单击记录
                {
                    DataGridViewCell currentCell = dgv[e.ColumnIndex, e.RowIndex];
                    object currentValue = currentCell.Value;

                    //未初始化的记录，按false处理
                    if (currentValue == null || currentCell.Value is System.DBNull || Convert.ToBoolean(currentValue) == false)
                    {
                        currentCell.Value = true;   //选中

                        //判断是否全部选中
                        bool enumState = true;
                        foreach (DataGridViewRow row in dgv.Rows)
                        {
                            if (row.IsNewRow) continue;
                            if (row.Cells[e.ColumnIndex].Value is System.DBNull || Convert.ToBoolean(row.Cells[e.ColumnIndex].Value.ToString()) == false)
                            {
                                enumState = false;
                                break;
                            }
                        }

                        //如果全选，选中表头
                        if (enumState == true)
                            (dgv.Columns[e.ColumnIndex].HeaderCell as DatagridViewCheckBoxHeaderCell).Checked = true;
                    }
                    else
                    {
                        currentCell.Value = false;  //取消选中

                        //取消选中表头
                        DatagridViewCheckBoxHeaderCell cell = dgv.Columns[e.ColumnIndex].HeaderCell as DatagridViewCheckBoxHeaderCell;
                        if (cell.Checked)
                            cell.Checked = false;
                    }

                    //刷新显示当前单元格
                    dgv.EndEdit();
                }
            }
        }
    }
}
