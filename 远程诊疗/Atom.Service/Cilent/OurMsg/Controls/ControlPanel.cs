﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

#region IMLibrary 
using IMLibrary4;
using IMLibrary4.Security;
using IMLibrary4.Organization;
using IMLibrary4.Operation;
using IMLibrary4.Protocol;

using IMLibrary4.Net;
using IMLibrary4.Net.TCP;

using IMLibrary4.SQLiteData;
#endregion

namespace OurMsg.Controls
{
    public partial class ControlPanel : UserControl
    {
        #region 组件初始化
        public ControlPanel()
        {
            this.UpdateStyles();
            InitializeComponent();
            this.treeView_Organization.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(treeView_Organization_NodeMouseDoubleClick);
            this.treeView_Organization.AfterCheck += new TreeViewEventHandler(treeView_Organization_AfterCheck);
            this.treeView_Organization.NodeMouseClick += new TreeNodeMouseClickEventHandler(treeView_Organization_NodeMouseClick);
            this.treeView_Rooms.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(treeView_Rooms_NodeMouseDoubleClick);
        }
        #endregion
 
        #region 事件
        /// <summary>
        /// 事件代理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void ControlPanelEventHandler(object sender);


        public delegate void LoginEventHandler(object sender,Auth auth);


        public delegate void SetUserName(string userNmae);

        /// <summary>
        /// 登录密码错误事件
        /// </summary>
        public event ControlPanelEventHandler UserLoginPasswordErrored;

        /// <summary>
        /// 登录成功
        /// </summary>
        public event LoginEventHandler UserLoginSuccessful;

        /// <summary>
        /// 登录超进
        /// </summary>
        public event ControlPanelEventHandler UserLoginOutTime;

        /// <summary>
        /// 用户掉线
        /// </summary>
        public event ControlPanelEventHandler UserOffline;
        
        /// <summary>
        /// 用户在别处登录
        /// </summary>
        public event ControlPanelEventHandler UserElseLogin;

        /// <summary>
        /// 刷新邮件事件-yufh添加
        /// </summary>
        public event ControlPanelEventHandler LoadMail;

        /// <summary>
        /// 用户修改资料时更新主窗口-yufh添加
        /// </summary>
        public event SetUserName SetMainUser;

       #endregion

        #region 变量
        /// <summary>
        /// TCP客户端
        /// </summary>
        private TCPClient tcpClient = null;
         
        /// <summary>
        /// 登录认证
        /// </summary>
        private  Auth MyAuth = null;

        /// <summary>
        /// 组织机构分组
        /// </summary>
        private List<exGroup> Groups = new List<exGroup>();

        /// <summary>
        /// 用户
        /// </summary>
        private List<exUser> Users = new List<exUser>();

        /// <summary>
        /// 群
        /// </summary>
        private List<exRoom> Rooms = new List<exRoom>();
         
        /// <summary>
        /// 登录超时记数器
        /// </summary>
        private int LoginTimeCount = 0;// 登录超时记数器

        /// <summary>
        /// 发送通知消息窗口
        /// </summary>
        private FormSendNotice frmNotice =null ;

        /// <summary>
        /// 创建组窗口
        /// </summary>
        private FormCreateRoom frmCreateGroup = null;

        /// <summary>
        /// 下载组织机构窗口
        /// </summary>
        private FormDownOrganization frmOrg = null;

        /// <summary>
        /// 用户资料窗口
        /// </summary>
        private Dictionary <string,FormUserVcard> frmUserVcards =  new Dictionary<string,FormUserVcard>();

        /// <summary>
        /// 分组资料窗口
        /// </summary>
        private Dictionary<string, FormGroupVcard>frmGroupVcards = new  Dictionary<string,FormGroupVcard>();


        /// <summary>
        /// 创建用户窗口
        /// </summary>
        private FormUserVcard frmUserVcard =null;

        /// <summary>
        /// 创建分组窗口
        /// </summary>
        private FormGroupVcard frmGroupVcard = null;
        #endregion

        #region 组织机构树操作事件处理
        private void treeView_Organization_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag is exUser)
            {
                exUser user = e.Node.Tag as exUser;
                if (user.UserID == MyAuth.UserID) return;//不能给自己发送消息
                FormTalkUser fs = GetUserMsgForm(user);// 获得用户消息对话框
                fs.Show();
                fs.Activate();
            }
        }

        private void treeView_Organization_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            //如果是选择用户则突出显示
            if (e.Node.Tag is exUser)
                treeView_Organization.FullRowSelect = true;
            else
                treeView_Organization.FullRowSelect = false;

            //如果是鼠标右键，选中节点
            if (e.Button == MouseButtons.Right)
            {
                treeView_Organization.SelectedNode = treeView_Organization.GetNodeAt(e.X, e.Y);
            }
            else
            {
                if (e.Node.TreeView.CheckBoxes==true)
                    return;
                //左键点中节点，如果不是展开状态，则展开节点，否则折叠节点
                if (!e.Node.IsExpanded && e.Node.Nodes.Count != 0 )//&& !e.Node.TreeView.CheckBoxes
                {
                    //e.Node.TreeView.CollapseAll();
                    e.Node.Expand();
                }
                else
                {
                    //e.Node.TreeView.CollapseAll();
                    e.Node.Collapse();
                }                
            }
        }

        private void treeView_Organization_AfterCheck(object sender, TreeViewEventArgs e)
        {
            foreach (TreeNode node in e.Node.Nodes)
                node.Checked = e.Node.Checked;
        }

        #region 群树操作事件处理
        private void treeView_Rooms_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag is exRoom)
            {
                exRoom room = e.Node.Tag as exRoom;
                FormTalkRoom fs = GetRoomMsgForm(room);
                fs.Show();
                fs.Activate();
            }
        }

        #endregion

        #region 加载本地数据库中的组织机构过程

        /// <summary>
        /// 加载本地数据库中的组织机构
        /// </summary>
        private void treeViewAddOrg()
        {
            if (this.treeView_Organization.Nodes.Count > 0) return;

            TreeNode node = this.addGroupToTreeView(this.Groups);
            this.addUserToTreeView(this.Users, this.Groups);
            if (node == null) return;

            foreach (exUser user in this.Users)
                user.UserName = user.UserName;

            foreach (TreeNode nodeTemp in node.Nodes)
            {
                treeView_Organization.Nodes.Add(nodeTemp);
            }
        }
        #endregion

        #region 添加群到treeView
        /// <summary>
        /// 添加群到treeView
        /// </summary>
        private void treeViewAddRooms(List<exRoom> Rooms)
        {
            treeView_Rooms.ImageList = this.imageList1;
            foreach (exRoom room in Rooms)
            {
                TreeNode node = new TreeNode();
                node.Text = room.RoomName + "(" + room.RoomID + ")";
                node.ToolTipText = room.RoomName;
                node.Tag = room;
                node.ImageIndex = 17;
                node.SelectedImageIndex = 17;
                room.TreeNode  = node;
                treeView_Rooms.Nodes.Add(node);
            }
        }
        #endregion

        #region 在treeView中添加分组groups集合并返回 TreeNode
        /// <summary>
        /// 在treeView中添加分组groups集合并返回 TreeNode
        /// </summary>
        /// <param name="treeView1"></param>
        /// <param name="groups"></param>
        /// <returns></returns>
        private TreeNode addGroupToTreeView(List<exGroup> groups)
        {
            if (groups == null) return null;
            TreeNode nodeTemp = new TreeNode();

            ///添加根分组节点
            foreach (exGroup group in groups)
                if (findGroup(group.SuperiorID) == null)
                {
                    TreeNode node = new TreeNode();
                    node.Text = group.GroupName;
                    node.ToolTipText = group.GroupName;
                    node.ImageIndex = 14;
                    node.SelectedImageIndex = 15;
                    node.Tag = group;
                    group.TreeNode = node;
                    nodeTemp.Nodes.Add(node);
                    group.GroupName = group.GroupName;
                }

            bool t = false;
           exGroup parentGroup;
            int rCount = 0;
            int sqrCount = (groups.Count * groups.Count);//最大循环次数

            while (!t && rCount <= sqrCount)//如果查询还未结束且循环次数没有超过部门数n平方，则继续
            {
                t = true;
                foreach (exGroup group in groups)
                {
                    parentGroup = findGroup(group.SuperiorID);//找到上级部门节点
                    if (parentGroup != null && group.TreeNode == null) //如果要添加的部门节点不是根部门节点且此节点还未添加 
                    {
                        if (parentGroup.TreeNode != null)// 当前的上级部门已经添加时，添加部门  
                        {
                            TreeNode node = new TreeNode();
                            node.Text = group.GroupName;
                            node.ToolTipText = group.GroupName;
                            node.ImageIndex = 14;
                            node.SelectedImageIndex = 15;
                            node.Tag = group;
                            group.TreeNode = node;
                            (parentGroup.TreeNode as TreeNode).Nodes.Add(node);
                            group.GroupName = group.GroupName;

                            group.SuperiorGroup = parentGroup;//设置上级组
                        }
                        else//如果当前部门节点的上级部门不是根部门节点并且上级部门的上级部门还未添加，则添加不成功，循环后再添加
                        {
                            t = false;
                        }
                    }
                    rCount++;//查询次数增1，如果大于部门n平方还未结束，则强行结束
                }
            }
            return nodeTemp;
        }
        #endregion

        #region 在treeView中的groups中添加用户
        /// <summary>
        /// 在treeView中的groups中添加用户
        /// </summary>
        /// <param name="groups"></param>
        /// <param name="users"></param>
        public void addUserToTreeView(List<exUser> users, List<exGroup> groups)
        {
            if (users == null || groups == null) return;

            foreach (exUser user in users)
                addUserTreeNode(user);
        }

        /// <summary>
        /// 创建用户树节点
        /// </summary>
        /// <param name="user"></param>
        private void addUserTreeNode(exUser user)
        {
            exGroup group = findGroup(user.GroupID);
            if (group != null && group.TreeNode != null)
            {
                TreeNode node = new TreeNode();
                node.Text = user.UserName;
                node.ToolTipText = user.UserName;
                node.ImageIndex = 0;
                node.SelectedImageIndex = 0;
                node.Tag = user;
                user.TreeNode = node;
                TreeNode groupNode = group.TreeNode as TreeNode;
                if (groupNode != null)
                    groupNode.Nodes.Add(node);

                user.Group = group;
                user.Group.UserCount++;
            }
        }
        #endregion

        #endregion

        #region TCP数据达到操作
        private void tcpClient_PacketReceived(object sender, TcpSessionEventArgs e)
        {
            PacketReceivedDelegate outdelegate = new PacketReceivedDelegate(PacketReceived);
            this.BeginInvoke(outdelegate, e.Data);
        }

        private delegate void PacketReceivedDelegate(string data);

        private void PacketReceived(string data)
        {
            Console.WriteLine(data);
            List<object> objs = Factory.CreateInstanceObjects(data);
            if (objs != null)
                foreach (object obj in objs)
                {
                    #region 组织机构管理
                    if (obj is EditUserData)//创建新用户
                    {
                        onEditUserData(obj as EditUserData);
                        return;
                    }
                    else if (obj is EditGroupData)
                    {
                        onEditGroupData(obj as EditGroupData);
                        return;
                    }
                    else if (obj is EditPassword)
                    {
                        onChangePassword(obj as EditPassword);
                        return;
                    }
                    #endregion

                    Element e = obj as Element;

                    if (obj is Auth)//用户登录
                        onLogin(obj as Auth);
                     if (obj is IMLibrary4.Protocol.Message)//聊天消息
                        onMessage(obj as IMLibrary4.Protocol.Message);
                     if (obj is Presence)//用户在线状态发生更改
                        onPresence(obj as Presence);
                     if (obj is Group)//获得分组信息
                        onOrgGroups(obj as Group);
                     if (obj is User)//获得用户信息
                        onOrgUsers(obj as User);
                     if (obj is Room)//获得群信息
                        onOrgRooms(obj as Room);
                     if (obj is EditRoom)//群信息更新
                        onEditRoom(obj as EditRoom);
                    if (obj is ImageFileMsg)//接收图片文件
                        onTCPImageFile(obj as ImageFileMsg);
                    if (obj is P2PFileMsg)//文件传输消息
                        onPFile(obj as P2PFileMsg);
                    if (obj is OfflineFileMsg)//离线文件传输消息
                        onOfflineFile(obj as OfflineFileMsg);

                    if (obj is RemoteDesktopMsg)//远程协助对话消息
                        onRemoteDesktopMsg(obj as RemoteDesktopMsg);
                     if (obj is AVMsg)//视频对话消息
                        onAVMsg(obj as AVMsg);
                }
        }
        #endregion
         
        #region 服务器回应密码更改
        /// <summary>
        /// 服务器回应密码更改
        /// </summary>
        /// <param name="cPWD"></param>
        public void onChangePassword(EditPassword cPWD)
        {
            if (cPWD.from == MyAuth.UserID)//如果是修改自己的密码
            {
                MyAuth.Password  = cPWD.NewPassword;
                IMLibrary4.OpeRecordDB.SaveAuth(MyAuth);//保存成功登录用户的信息
                IMLibrary4.Global.MsgShow("密码已经修改，请记住新密码！！");
            }
            else//如果是管理员修改别人的密码
            {
                exUser user = findUser(cPWD.from);
                if (user != null)
                    IMLibrary4.Global.MsgShow(user.UserName + "(" + user.UserID + ") 的密码修改成功！！");
            }
        }
        #endregion 

        #region 管理组织机构操作回应

        #region 创建、删除、更新分组信息操作
        private void onEditGroupData(EditGroupData groupData)
        {
            if (frmGroupVcard != null && !frmGroupVcard.IsDisposed && groupData.type == type.New)
                frmGroupVcard.isUpdateSuccess = true;

            exGroup group = findGroup(groupData.GroupID);//查找本地用户是否存在操作用户

            if (groupData.type == type.New && group == null)
            {
                group = new exGroup();
                group.GroupID = groupData.GroupID;
                group.SuperiorID = groupData.SuperiorID;
                Groups.Add(group);
                //将用户节点添加到树
                {
                    TreeNode node = new TreeNode();
                    node.Text = group.GroupName;
                    node.ImageIndex = 14;
                    node.SelectedImageIndex = 15;
                    node.Tag = group;
                    group.TreeNode = node;
                    ///添加根分组节点
                    if (findGroup(group.SuperiorID) == null)
                        treeView_Organization.Nodes.Add(node);
                    else
                        (findGroup(group.SuperiorID).TreeNode as TreeNode).Nodes.Add(node);
                }
                group.GroupName = groupData.GroupName;

                OpeRecordDB.UpdateGroupVcard(groupData);//添加分组信息
            }
            else if (groupData.type == type.delete && group != null)
            {
                TreeNode node = group.TreeNode as TreeNode;
                if (node != null)
                    treeView_Organization.Nodes.Remove(node);
                OpeRecordDB.DeleteGroup(groupData.GroupID);//本地数据库中删除
            }
            else if (group != null)
            {
                FormGroupVcard frm = getGroupVcardForm(groupData.GroupID);
                if (frm != null && !frm.IsDisposed)
                    if (groupData.type != type.set)
                        frm.GroupVcard = groupData;
                    else
                        frm.isUpdateSuccess = true;
                group.GroupName = groupData.GroupName;

                OpeRecordDB.UpdateGroupVcard(groupData);//更新分组信息
            }


            OrgVersion localOrg = OpeRecordDB.GetOrgVersion(MyAuth.UserID);//获得本地组织机构版本
            localOrg.GroupsVersion = groupData.Version;
            OpeRecordDB.UpdateOrgVersion(localOrg);
        }
        #endregion

        #region 创建、删除、更新用户信息操作
        private void onEditUserData(EditUserData userData)
        {
            if (frmUserVcard != null && !frmUserVcard.IsDisposed && userData.type == type.New)
                frmUserVcard.isUpdateSuccess = true;

            exUser user = findUser(userData.UserID);//查找本地用户是否存在操作用户

            if (userData.type == type.New && user == null)
            {
                user = new exUser();
                user.UserID = userData.UserID;
                user.GroupID = userData.GroupID;
                Users.Add(user);
                addUserTreeNode(user);//将用户节点添加到树
                user.UserName = userData.UserName;
                OpeRecordDB.UpdateUserVcard(userData);//添加到本地数据库
            }
            else if (userData.type == type.delete && user != null)
            {
                TreeNode node = user.TreeNode as TreeNode;
                if (node != null && node.Parent != null)
                {
                    if (user.ShowType != ShowType.Offline)
                        user.Group.OnLineUserCount -= 1;

                    user.Group.UserCount -= 1;

                    node.Parent.Nodes.Remove(node);//删除用户树节点
                    Form frm = user.Tag as Form;
                    if (frm != null && !frm.IsDisposed) { frm.Close(); frm.Dispose(); }//关闭用户对话窗口
                    Users.Remove(user);//删除用户
                    OpeRecordDB.DeleteUser(userData.UserID);//本地数据库中删除
                }
            }
            else if (user != null)
            {
                FormUserVcard frm = getUserVcardForm(userData.UserID);
                if (frm != null && !frm.IsDisposed)
                    if (userData.type != type.set)
                        frm.UserData = userData;
                    else
                        frm.isUpdateSuccess = true;
                user.UserName = userData.UserName;
                OpeRecordDB.UpdateUserVcard(userData);//更新本地数据库
            }

            OrgVersion localOrg = OpeRecordDB.GetOrgVersion(MyAuth.UserID);//获得本地组织机构版本
            localOrg.UsersVersion = userData.Version;//更新用户信息版本
            OpeRecordDB.UpdateOrgVersion(localOrg);
        }
        #endregion

        #endregion

        #region 联系人请求远程协助
        /// <summary>
        /// 联系人请求远程协助
        /// </summary>
        /// <param name="msg">视频消息对像</param>
        private void onRemoteDesktopMsg(RemoteDesktopMsg msg)
        {
            exUser user = findUser(msg.from);
            if (user != null)
            {
                FormTalkUser fs = user.Tag as FormTalkUser;

                if (msg.type == type.New)//如果是邀请远程协助
                {
                    fs = GetUserMsgForm(user);
                    fs.ReadyRemoteDesktop(true);
                    fs.Show();
                    fs.Activate();
                }
                else if (msg.type == type.get)//如果是控制对方电脑 yufh 2015-01-06 添加
                {
                    fs = GetUserMsgForm(user);
                    fs.IsAllowControl = true;//如果是对方请求控制本机则直接设置允许控制
                    fs.ReadyRemoteDesktop(false);                    
                    fs.Show();
                    fs.Activate();
                }
                else if (msg.type == type.cancel)//如果是取消视频
                {
                    if (fs != null && !fs.IsDisposed)
                        fs.CancelRemoteDesktop(false);//对方取消
                }
                else if (msg.type == type.set)//设置通信参数
                {
                    if (fs != null && !fs.IsDisposed)
                        fs.setRemoteDesktopEP(msg);
                }
            }
        }
        #endregion

        #region 联系人请求视频
        /// <summary>
        /// 联系人请求视频
        /// </summary>
        /// <param name="msg">视频消息对像</param>
        private void onAVMsg(AVMsg msg)
        {
            exUser user = findUser(msg.from);
            if (user != null)
            {
                FormTalkUser fs = user.Tag as FormTalkUser;

                if (msg.type == type.New)//如果是邀请视频
                {
                    fs = GetUserMsgForm(user);
                    fs.Show();
                    fs.ReadyAV(false);
                    fs.Activate();
                }
                else if (msg.type == type.cancel)//如果是取消视频
                {
                    if (fs != null || !fs.IsDisposed)
                        fs.CancelAV(false);//对方取消
                }
                else if (msg.type == type.set)//设置通信参数
                {
                    if (fs != null || !fs.IsDisposed)
                    {
                        fs.setAVRometEP(msg);
                    }
                }
            }
        } 
        #endregion

        #region 联系人文件传输请求
        /// <summary>
        /// 离线文件传输请求
        /// </summary>
        /// <param name="msg">消息对像</param>
        private void onOfflineFile(OfflineFileMsg msg)
        {
            exUser user = findUser(msg.from);
            if (user != null && user.UserID != MyAuth.UserID)
            {
                FormTalkUser fs = user.Tag as FormTalkUser;
                fs = GetUserMsgForm(user);
                fs.Show();
                fs.ReceiveOfflineFile(msg);
                fs.Activate();

            }
        }

        /// <summary>
        /// p2p文件传输请求
        /// </summary>
        /// <param name="pfile">消息对像</param>
        private void onPFile(P2PFileMsg  pfile)
        {
            exUser user = findUser(pfile.from );
            if (user != null && user.UserID != MyAuth.UserID)
            {
                FormTalkUser fs = user.Tag as FormTalkUser;
                 
                if (pfile.type == type.New)//如果是接收新文件
                {
                    fs = GetUserMsgForm(user);
                    fs.ReceiveP2PFile(pfile);
                    fs.Show();
                    fs.Activate();
                }
                else  if (pfile.type == type.cancel)//如果是取消文件传输
                {
                    if (fs != null && !fs.IsDisposed)
                    {
                        fs.CancelFile(pfile);
                    }
                }
                else if (pfile.type == type.set)//如果是设置远程主机信息
                {
                    if (fs != null && !fs.IsDisposed)
                    {
                        fs.setFileRometEP(pfile);
                    }
                }
            }
        }
        #endregion

        #region 用户登录

        #region 下载企业组织架构信息
        /// <summary>
        /// 获得群信息
        /// </summary>
        /// <param name="room"></param>
        private void onOrgRooms(Room room)
        {
            exRoom exroom = new exRoom();
            exroom.RoomID = room.RoomID;
            exroom.RoomName = room.RoomName;
            exroom.Notice = room.Notice;
            exroom.UserIDs = room.UserIDs;
            exroom.CreateUserID = room.CreateUserID;
            Rooms.Add(exroom);

            if (frmOrg != null && !frmOrg.IsDisposed)
            {
                frmOrg.Times = 0;
                frmOrg.Value = this.Rooms.Count;
            }
        }

        /// <summary>
        /// 获得用户信息
        /// </summary>
        /// <param name="user"></param>
        private void onOrgUsers(User user)
        {
            exUser exuser = new exUser();
            exuser.UserID = user.UserID;
            exuser.UserName = user.UserName;
            exuser.GroupID = user.GroupID;
            exuser.OrderID = user.OrderID;
            exuser.ShowType = ShowType.Offline;
            Users.Add(exuser);

            if (frmOrg != null && !frmOrg.IsDisposed)
            {
                frmOrg.Times = 0;
                frmOrg.Value = this.Users.Count;
            }
        }

        /// <summary>
        /// 获得分组信息
        /// </summary>
        /// <param name="group"></param>
        private void onOrgGroups(Group group)
        {

            exGroup exgroup = new exGroup();
            exgroup.GroupID = group.GroupID;
            exgroup.GroupName = group.GroupName;
            exgroup.SuperiorID = group.SuperiorID;
            exgroup.OrderID = group.OrderID;
            Groups.Add(exgroup);

            if (frmOrg != null && !frmOrg.IsDisposed)
            {
                frmOrg.Times = 0;
                frmOrg.Value = this.Groups.Count;
            }
        }

        /// <summary>
        /// 获得组织机构版本信息
        /// </summary>
        /// <param name="org"></param>
        private void onOrgVersion(OrgVersion org)
        {
            try
            {
                #region 判断登录的用户本地数据库文件夹是否存在
                System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(MyAuth.UserID);
                if (!dInfo.Exists)
                    dInfo.Create();
                string FileNamePath =Application.StartupPath +"\\"+ MyAuth.UserID + "\\Record.mdb";
                if (!System.IO.File.Exists(FileNamePath))
                    System.IO.File.Copy(Application.StartupPath + "\\Record.db", FileNamePath);

                ////初始化本地数据库连接字符串
                SQLiteDBHelper.connectionString = "data source=" + FileNamePath;
                #endregion

                OrgVersion localOrg = OpeRecordDB.GetOrgVersion(MyAuth.UserID);
                this.Rooms = OpeRecordDB.GetRooms();//获得本地数据库群组数


                #region 如果版本已经改变下载组织机构组信息
                if (org.GroupsVersion != localOrg.GroupsVersion)
                {
                    Groups.Clear();
                    treeView_Organization.Nodes.Clear();

                    OpeRecordDB.DeleteAllGroup();//删除本地数据库中分组信息

                    frmOrg = new FormDownOrganization();
                    frmOrg.Shown += delegate(object sender, EventArgs e)
                    {
                        DownloadGroups dGroups = new DownloadGroups();
                        dGroups.from = MyAuth.UserID;
                        dGroups.type = type.get;
                        SendMessageToServer(dGroups);//请求下载分组信息
                    };
                    frmOrg.MaxValue = org.GroupsCount;
                    frmOrg.ShowText = "正在下载分组信息...";
                    frmOrg.ShowDialog();

                    OpeRecordDB.AddGroups(this.Groups);//将下载的分组信息保存于数据库中
                }
                #endregion

                #region 下载组织机构用户信息
                if (org.UsersVersion != localOrg.UsersVersion)//(true)
                {
                    Users.Clear();
                    treeView_Organization.Nodes.Clear();
                    OpeRecordDB.DeleteAllUser();//删除本地数据库中用户信息

                    frmOrg = new FormDownOrganization();
                    frmOrg.Shown += delegate(object sender, EventArgs e)
                    {
                        DownloadUsers  dUsers = new  DownloadUsers();
                        dUsers.from = MyAuth.UserID;
                        dUsers.type = type.get;
                        SendMessageToServer(dUsers);//请求下载联系人信息
                    };
                    frmOrg.MaxValue = org.UsersCount;
                    frmOrg.ShowText = "正在下载用户信息...";
                    frmOrg.ShowDialog();

                    OpeRecordDB.AddUsers(this.Users);
                }
                #endregion

                #region 下载群信息
                if (org.RoomsCount !=  Rooms.Count )//如果群数大于0
                {
                    Rooms.Clear();
                    treeView_Rooms.Nodes.Clear();

                    OpeRecordDB.DeleteAllRoom();//删除本地数据库中群信息

                    frmOrg = new FormDownOrganization();
                    frmOrg.Shown += delegate(object sender, EventArgs e)
                    {
                        DownloadRooms dRooms = new DownloadRooms();
                        dRooms.from = MyAuth.UserID;
                        dRooms.type = type.get;
                        SendMessageToServer(dRooms);//请求下载群信息
                    };

                    frmOrg.MaxValue = org.RoomsCount;
                    frmOrg.ShowText = "正在下载群资料...";
                    frmOrg.ShowDialog();

                    OpeRecordDB.AddRooms(this.Rooms);
                }
                #endregion

                //如果组织机构已经更改或组织机构未加载到树图
                if (this.treeView_Organization.Nodes.Count == 0)
                {
                    //则更新到本地数据库
                    OpeRecordDB.UpdateOrgVersion(org);
                    this.Groups = OpeRecordDB.GetGroups();
                    this.Users = OpeRecordDB.GetUsers();
                    treeViewAddOrg();//加载树
                }

                if (this.treeView_Rooms.Nodes.Count == 0)
                {
                    foreach (Room room in this.Rooms)
                    {
                        foreach (string userID in room.UserIDs.Split(';'))
                        {
                            User user = findUser(userID);
                            if (user != null)
                                room.Users.Add(user.UserID, user);
                        }
                    }

                    treeViewAddRooms(this.Rooms);
                }
                
                ////请求联系人在线状态信息
                //Presence pre = new Presence();
                //pre.from = MyAuth.UserID;
                //pre.type = type.get;//必须设置 set,以表示是设置，如果为get，则是获取所有联系人的状态
                //SendMessageToServer(pre);
            }
            catch (Exception ex)
            {
                IMLibrary4.Global.MsgShow(ex.Source + ex.Message);
            }
        }
  
        #endregion

        #region 开始登录
        /// <summary>
        /// 开始登录 
        /// </summary>
        /// <param name="auth">登录用户参数</param>
        /// <param name="IsOutTime">是否超时时钟重复的登录</param>
        public void Login(IMLibrary4.Protocol.Auth auth, bool IsOutTime)
        {

            MyAuth = auth;//暂存自己的登录信息于内存

            if (tcpClient == null || tcpClient.IsDisposed)
            {
                tcpClient = new TCPClient();
                tcpClient.PacketReceived += new TCP_Client.PacketReceivedEventHandler(tcpClient_PacketReceived);
                tcpClient.Disonnected += new EventHandler(tcpClient_Disonnected);                
            }

            if (!tcpClient.IsConnected)
                tcpClient.Connect(Global.ServerDomain, Global.ServerMsgPort);

            if (tcpClient.IsConnected)
            {
                Console.WriteLine("tcpClient Connected OK");
                //MessageBox.Show("OK");  //    
            }
            if (!this.timerLogin1.Enabled)
                this.timerLogin1.Enabled = true;           
            SendMessageToServer(auth);//向服务器登录
        }

        #endregion

        #region 登录心跳时钟
        private void timerLogin1_Tick(object sender, EventArgs e)
        {
            LoginTimeCount++;

            ///如果30秒后未登录成功，则触发登录超时事件
            if (LoginTimeCount > 30 && !tcpClient.IsConnected)
            {
                this.timerLogin1.Enabled = false;

                LoginTimeCount = 0;

                if (UserLoginOutTime != null)
                    UserLoginOutTime(this);
            }

            ///每个8秒重新发送一次登录请求
            if (LoginTimeCount > 1 && LoginTimeCount % 10 == 0 && !tcpClient.IsConnected)
                Login(MyAuth, true);//登录。。。              


            //如果用户已经上线，则停止检测检测
            if (tcpClient.IsConnected)
            {
                this.timerLogin1.Enabled = false;
                LoginTimeCount = 0;
            }
        }
        #endregion

        #region 登录回应处理
        private void onLogin(Auth auth)
        {            
            if (auth.type == type.error)//密码错误 
                LoginPasswordError();
            else if (auth.type == type.result)//登录成功
            {
                string password = MyAuth.Password;

                if (MyAuth.IsSavePassword)//如果是成功登录并且需要保存登录密码 
                {
                    IMLibrary4.OpeRecordDB.SaveAuth(MyAuth);//保存成功登录用户的信息
                }
                else if (!MyAuth.IsSavePassword)//如果不需要保存登录密码
                {
                    MyAuth.Password = "";
                    IMLibrary4.OpeRecordDB.SaveAuth(MyAuth);//保存成功登录用户的信息
                }

                MyAuth = auth;//暂存登录成功后的服务器返回的登录信息

                MyAuth.Password = password;

                ///设置UDP文件传输服务器端口
                Global.FileTransmitServerEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), MyAuth.FileServerUDPPort);
                ///设置UDP音视频传输服务器端口
                Global.AVTransmitServerEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), MyAuth.AVServerUDPPort);
                ///设置TCP图片文件传输服务器端口
                Global.ImageServerEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), MyAuth.FileServerTCPPort);
                //设置TCP离线文件传输服务器端口
                Global.OfflineFileServerEP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(Global.ServerDomain), MyAuth.OfflineFileServerTCPPort);
                
                OrgVersion orgV = new OrgVersion();//获取服务器组织机构版本信息
                orgV.UsersCount = auth.UsersCount;
                orgV.UsersVersion = auth.UsersVersion;
                orgV.GroupsCount = auth.GroupsCount;
                orgV.GroupsVersion = auth.GroupsVersion;
                orgV.RoomsCount = auth.RoomsCount;

                onOrgVersion(orgV);

                //登陆成功提示调整到最后，2014-12-29 yufh调整
                this.LoginTimeCount = 0;//超时器清零
                this.timerLogin1.Enabled = false;//停止登录超时检测
                if (UserLoginSuccessful != null) //触发登录成功事件
                    UserLoginSuccessful(this, auth);

                //请求联系人在线状态信息
                Presence pre = new Presence();
                pre.from = MyAuth.UserID;
                pre.type = type.get;//必须设置 set,以表示是设置，如果为get，则是获取所有联系人的状态
                SendMessageToServer(pre);
            }
            else if (auth.type == type.Else)//别处登录
                ElseLogin();
        }
        #endregion

        #region 用户别处登录
        /// <summary>
        /// 用户别处登录
        /// </summary>
        private void ElseLogin()
        { 
            SetAllUsersOffline();//将所有用户设置为离线

            MyAuth.ShowType = ShowType.Offline;

            if (UserElseLogin != null)
                UserElseLogin(this);
        }
        #endregion

        #region 服务器返回用户密码错误
        /// <summary>
        /// 用户登录密码错误
        /// </summary>
        private void LoginPasswordError()
        {
            this.LoginTimeCount = 0;//超时器清零
            this.timerLogin1.Enabled = false;//停止登录超时检测

            if (UserLoginPasswordErrored != null)
                UserLoginPasswordErrored(this);
        }
        #endregion

        #endregion

        #region 用户离线
        private void tcpClient_Disonnected(object sender, EventArgs e)
        {
            DisonnectedDelegate outdelegate = new DisonnectedDelegate(Disonnected);
            this.BeginInvoke(outdelegate);
        }
        private delegate void DisonnectedDelegate();
        private void Disonnected()
        {
            if (UserOffline != null)
                UserOffline(this);
            MyAuth.ShowType = ShowType.Offline;
            SetAllUsersOffline();
        }

        /// <summary>
        /// 注销
        /// </summary>
        public void LogOut()
        {
            if (MyAuth.IsAutoLogin)
                MyAuth.IsAutoLogin = false;//去掉自动登陆
            IMLibrary4.OpeRecordDB.SaveAuth(MyAuth);//保存成功登录用户的信息
            //tcpClient.Dispose();//释放连接
        }

        #endregion

        #region 状态更改

        #region 自己更改状态
        /// <summary>
        /// 设置登录用户在线状态
        /// </summary>
        /// <param name="showType"></param>
        public void setMyPresence(ShowType showType)
        {
            if (showType == MyAuth.ShowType)//如果状态未做改变，则无需发送到服务器
                return;

            if (showType == ShowType.Offline
                && (tcpClient != null && !tcpClient.IsDisposed && tcpClient.IsConnected)) //如果要下线
            {
                this.tcpClient.Disconnect();
                this.tcpClient.Dispose();
                this.tcpClient = null;
                return;
            }

            if (showType != ShowType.Offline 
                && (tcpClient == null || tcpClient.IsDisposed || !tcpClient.IsConnected))//如果已经下线，则上线
            { 
                MyAuth.ShowType = showType;
                Login(MyAuth, true);//登录。。。 
                return;
            }

            Presence pre = new Presence();
            pre.from = MyAuth.UserID;
            pre.ShowType = showType;
            pre.type = type.set;//必须设置 set,以表示是设置，如果为get，则是获取所有联系人的状态
            SendMessageToServer(pre );
        }
        #endregion

        #region 联系人更改状态

        /// <summary>
        /// 联系人更改状态
        /// </summary>
        /// <param name="pres"></param>
        public void onPresence(IMLibrary4.Protocol.Presence pres)
        {
            //查找联系人列表中是否存在此联系人，如果存在，则更新状态信息
            exUser user = findUser(pres.from );
            if (user != null)
            {
                user.Status = pres.Status;
                user.ShowType = pres.ShowType;

                if (user.UserID == MyAuth.UserID)
                {
                    MyAuth.ShowType = user.ShowType;
                    MyAuth.UserName = user.UserName;
                }
            }
        }
        #endregion

        #endregion

        #region 群信息更新
        private void onEditRoom(EditRoom roomData)
        {
            #region 创建临时群
            exRoom room = new exRoom();
            room.RoomID = roomData.RoomID;
            room.RoomName = roomData.RoomName;
            room.Notice = roomData.Notice;
            room.CreateUserID = roomData.CreateUserID;
            room.UserIDs = roomData.UserIDs;
            if(room.UserIDs !=null)
            foreach (string userID in  room.UserIDs.Split(';'))
            {
                User user = findUser(userID);
                if (user != null)
                    room.Users.Add(user.UserID, user);
            }
            #endregion

            exRoom exroom = findRoom(roomData.RoomID);//查找群是否已经存在

            if (roomData.CreateUserID == MyAuth.UserID)//如果是自己创建的群
            {
                if (roomData.type == type.New)//如果创建群成功,更新群成功
                {
                    if (this.frmCreateGroup != null && !this.frmCreateGroup.IsDisposed)//如果创建群窗口已经加载
                    {
                        this.frmCreateGroup.Room = room;
                        this.frmCreateGroup.isUpdateSuccess = true;
                    }
                    if (exroom == null)//如果群不存在，则添加群
                    {
                        this.Rooms.Add(room);
                        List<exRoom> rooms = new List<exRoom>();
                        rooms.Add(room);
                        treeViewAddRooms(rooms);
                    }
                }
                else if (roomData.type == type.set)//如果是自己更新群信息
                {
                    if (exroom != null)//如果群存在，则更新群信息即可
                    {
                        exroom.Notice = room.Notice;
                        exroom.UserIDs = room.UserIDs;
                        exroom.RoomName = room.RoomName;
                        exroom.Users = room.Users;
                        FormTalkRoom frmGroup = exroom.Tag as FormTalkRoom;
                        if (frmGroup != null)
                            frmGroup.Room = exroom;

                        OurMsg.FormCreateRoom frm = (exroom.FormData as FormCreateRoom);
                        if (frm != null && !frm.IsDisposed)
                        {
                            frm.Room = room;
                            frm.isUpdateSuccess = true;
                        }
                    }
                }
            }
            else//如果群信息发生改变
            {
                if (roomData.type == type.New || roomData.type == type.set)//如果加入群 
                {
                    if (exroom == null)//如果群不存在，则添加群
                    {
                        this.Rooms.Add(room);
                        List<exRoom> rooms = new List<exRoom>();
                        rooms.Add(room);
                        treeViewAddRooms(rooms);
                    }
                    else//如果群存在，则更新信息
                    {
                        exroom.Notice = room.Notice;
                        exroom.UserIDs = room.UserIDs;
                        exroom.RoomName = room.RoomName;
                        exroom.Users = room.Users;

                        FormTalkRoom frmGroup = exroom.Tag as FormTalkRoom;
                        if (frmGroup != null)
                            frmGroup.Room = exroom;
                    }
                }
                else if (roomData.type == type.delete)//如果退出群
                {
                    if (exroom != null)
                    {
                        Form frm = exroom.Tag as Form;
                        if (frm != null && !frm.IsDisposed)
                            frm.Close();

                        frm = exroom.FormData as Form;
                        if (frm != null && !frm.IsDisposed)
                            frm.Close();
                         
                        TreeNode node = (exroom.TreeNode as TreeNode);
                        if (node != null) node.Remove();
                         
                        this.Rooms.Remove(exroom);
                    }
                }
            }
        }

        #region 创建或添加群成功后操作
        /// <summary>
        /// 创建或添加群成功后操作
        /// </summary>
        /// <param name="Room"></param>
        private void CreateOrAddGroup(exRoom Room)
        {
            exRoom group = findRoom(Room.RoomID);//查找群组 

            if (group == null)
            {
                group = Room;//获取群组信息

                this.Rooms.Add(group);//内存中添加群组

                TreeNode node = new TreeNode();
                node.Text = group.RoomName + "(" + group.RoomID + ")";
                node.Tag = group;
                group.TreeNode = node;
                treeView_Rooms.Nodes.Add(node);
            }
             OpeRecordDB.UpdateRooms(group);//数据库中保存群组资料
        }
        #endregion

        #endregion

        #region 收到对话消息

        #region 处理接收图片文件
        /// <summary>
        /// 处理接收图片文件 
        /// </summary>
        /// <param name="msg"></param>
        private void onTCPImageFile(ImageFileMsg msg)
        {
            exUser user = findUser(msg.from);
            if (user == null) return;
            if (user.UserID == MyAuth.UserID) return;//如果是自己发送给自己的消息，则不处理

            System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(MyAuth.UserID + "\\ArrivalImage");
            if (!dInfo.Exists)
                dInfo.Create();

            string fileName = MyAuth.UserID + "\\ArrivalImage\\" + msg.MD5 + msg.Extension;

            if (System.IO.File.Exists(fileName)) return;//如果本地已经有此文件已经存在，则退出(不接收文件)

            TFileInfo fileinfo = new TFileInfo();
            fileinfo.MD5 = msg.MD5;
            fileinfo.Length = msg.Length;
            fileinfo.Extension = msg.Extension;
            fileinfo.fullName = fileName;

            if (msg.MessageType == MessageType.User)//如果图片发送给用户
            {
                TcpImageFileClient tcpfile = new TcpImageFileClient(Global.ImageServerEP, fileinfo);
                tcpfile.fileTransmitted += delegate(object sender, fileTransmitEvnetArgs e)
                {
                    FormTalkUser fs = user.Tag as FormTalkUser;
                    if (fs != null)
                    {
                        List<MyPicture> needRecPics = fs.GetNeedRecPicture();
                        foreach (MyPicture pic in needRecPics)
                            if (pic.MD5 == e.fileInfo.MD5)
                            {
                                needRecPics.Remove(pic);
                                pic.Image = System.Drawing.Image.FromFile(e.fileInfo.fullName);
                                fs.Invalidate();
                            }
                    }
                    (sender as TcpImageFileClient).Dispose();
                    sender = null;
                };
            }
            else if (msg.MessageType == MessageType.Group)//如果图片发送给群
            {

                exRoom room = findRoom(msg.to);//获得消息接收群
                if (room == null) return;
                FormTalkRoom fs = room.Tag as FormTalkRoom;

                TcpImageFileClient tcpfile = new TcpImageFileClient(Global.ImageServerEP, fileinfo);
                tcpfile.fileTransmitted += delegate(object sender, fileTransmitEvnetArgs e)
                {
                    if (fs != null)
                    {
                        List<MyPicture> needRecPics = fs.GetNeedRecPicture();
                        foreach (MyPicture pic in needRecPics)
                            if (pic.MD5 == e.fileInfo.MD5)
                            {
                                needRecPics.Remove(pic);
                                pic.Image = System.Drawing.Image.FromFile(e.fileInfo.fullName);
                                fs.Invalidate();
                            }
                    }
                    (sender as TcpImageFileClient).Dispose();
                    sender = null;
                };
            } 
        }

       
        #endregion

        #region 处理用户发送的对话消息
        /// <summary>
        /// 处理用户发送的对话消息 
        /// </summary>
        /// <param name="msg"></param>
        private void onMessage(IMLibrary4.Protocol.Message msg)
        {
            exUser user = findUser(msg.from);
            if (user == null) return;
            if (user.UserID == MyAuth.UserID) return;//如果是自己发送给自己的消息，则不处理

            OpeRecordDB.AddMsg(msg);//将消息添加到数据库

            if (msg.MessageType == MessageType.User)//如果消息发送给用户
            {
                OnUserMessage(msg, user);
            }
            else if (msg.MessageType == MessageType.Group)//如果消息发送给群
            {
                OnGroupTalk(msg,user);
            }
            else if (msg.MessageType == MessageType.Notice)//如果发送通知消息给多个用户
            {
                OnNoticeMsg(msg, user);
            }
            else if (msg.MessageType == MessageType.broadcasting)//如果发送通知消息给所有用户
            {

            }
        }
        #endregion

        #region 收到通知消息
        /// <summary>
        /// 收到通知消息
        /// </summary>
        private void OnNoticeMsg(IMLibrary4.Protocol.Message msg,exUser user)
        {
            if (msg.Title == "邮件通知")
            { 
                //FormMain frMain = (FormMain)this.o
                string mailCount = IMLibrary4.OpeRecordDB.GetMailCount(MyAuth.UserID).ToString();
                LoadMail(mailCount);
            }
            FormNotice fs = new FormNotice();
            fs.OpenMail += delegate()
                {
                    ShowMainForm("收件箱");
                };
            fs.MsgToRichTextBox(msg, user);
            fs.Show();
        }
        #endregion

        #region 收到用户对话消息
        /// <summary>
        /// 收到用户对话消息
        /// </summary>
        private void OnUserMessage(IMLibrary4.Protocol.Message msg,exUser user)
        {
            FormTalkUser fs = GetUserMsgForm(user);
            fs.MsgToRichTextBox(msg, false);
            fs.Show();
            fs.Activate();
        }
        #endregion

        #region 收到群组对话消息
        /// <summary>
        /// 收到群组对话消息
        /// </summary>
        private void OnGroupTalk(IMLibrary4.Protocol.Message Msg, exUser user)
        { 
            exRoom room = findRoom(Msg.to);//获得消息接收群
            if (room == null) return;

            FormTalkRoom fs = GetRoomMsgForm(room);
            fs.MsgToRichTextBox(Msg, false, user.UserName + "(" + user.UserID + ")");
            fs.Show();
        }
        #endregion

        #endregion

        #region 公共方法
        /// <summary>
        /// 发送消息至服务器 
        /// </summary>
        /// <param name="e"></param>
        public void SendMessageToServer(Element  e)
        {
            e.from = MyAuth.UserID;//此处告之服务器，消息由自己发送
            string xmlmsg = Factory.CreateXMLMsg(e);
            if (tcpClient!=null && !tcpClient.IsDisposed && tcpClient.IsConnected)
                tcpClient.Write(xmlmsg);
        }

        /// <summary>
        /// 发送消息至服务器
        /// </summary>
        /// <param name="e">消息对像</param>
        public  void SendMessageToServer(object  e)
        {
            if (tcpClient != null && !tcpClient.IsDisposed && tcpClient.IsConnected)
                tcpClient.Write(IMLibrary4.Protocol.Factory.CreateXMLMsg( e));
        } 
        
        #region 关闭服务
        /// <summary>
        /// 关闭服务
        /// </summary>
        /// <param name="IsCloseCon">是否关闭与服务器的连接(退出程序时用)</param>
        public void Close(bool IsCloseCon)
        {
             
        }
        #endregion

        /// <summary>
        /// 创建新用户
        /// </summary>
        /// <param name="userVcard"></param>
        public void CreateUser(EditUserData card)
        {
            card.type = type.New;
            SendMessageToServer(card);
        }

        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="userVcard"></param>
        public void UpdateUser(EditUserData card)
        {            
            card.type = type.set;
            SendMessageToServer(card);
        }


        /// <summary>
        /// 创建新分组
        /// </summary>
        /// <param name="userVcard"></param>
        public void CreateGroup(EditGroupData   card)
        {
            card.type = type.New;
            SendMessageToServer(card);
        }

        /// <summary>
        /// 更新分组
        /// </summary>
        /// <param name="userVcard"></param>
        public void UpdateGroup(EditGroupData  card)
        {
            card.type = type.set;
            SendMessageToServer(card);
        }

        /// <summary>
        /// 显示用户资料窗口
        /// </summary>
        /// <param name="userID"></param>
        public void ShowUserVcard(string   userID)
        {
            exUser user = findUser(userID);
            if (user != null)
                ShowUserVcard(user);
        }

        /// <summary>
        /// 显示用户资料窗口
        /// </summary>
        /// <param name="user"></param>
        public void ShowUserVcard(exUser user)
        {
            FormUserVcard frm = getUserVcardForm(user.UserID);
            if (frm == null)
            {
                frm = new FormUserVcard();
                frmUserVcards.Add(user.UserID, frm);

                if (MyAuth.isAdmin)//如果是管理员，则添加更新事件
                    frm.UpdateVcard += delegate(object sender1, EditUserData userVcard)//更新事件
                    {
                        UpdateUser(userVcard);//更新用户资料
                    };
                else//如果不是管理员，则添加刷新事件
                {
                    frm.RefreshVcard += delegate(object sender1, EditUserData userVcard)//刷新事件
                    {
                        //向服务器请求获得用户最新资料
                        userVcard.type = type.get;
                        SendMessageToServer(userVcard);
                    };
                    //2014-12-18 10:35:45 yufh给用户添加更新事件，满足用户自己修改资料
                    frm.UpdateVcard += delegate(object sender1, EditUserData userVcard)//更新事件
                    {
                        UpdateUser(userVcard);//更新用户资料
                    };
                }

                frm.FormClosed += delegate(object sender1, FormClosedEventArgs e1)//窗口关闭事件
                {
                    frm.Dispose();
                    frmUserVcards.Remove(user.UserID);
                };

                frm.ChanagePassword += delegate(object sender2, EditPassword e2)//密码修改事件
                {
                    if (!frm.isAdmin && e2.OldPassword != MyAuth.Password )
                    {
                        IMLibrary4.Global.MsgShow("旧密码不正确，请重新输入");
                        return;
                    }
                    SendMessageToServer((object)e2);
                };
                //2014-12-19 yufh添加用户更新资料时更新主窗体用户名
                frm.SetMainUser += delegate(object sender, EditUserData userVcard)
                {
                    if (userVcard.UserID == MyAuth.UserID)
                    {
                        SetMainUser(userVcard.UserName + "(" + userVcard.UserID + ")");//更新主窗体用户名称
                    }
                };

                EditUserData userData = OpeRecordDB.GetUserVcard(user.UserID);
                if (userData == null || userData.UserName != user.UserName || userData.GroupID != user.GroupID)
                {
                    userData = new EditUserData();
                    userData.UserID = user.UserID;
                    userData.UserName = user.UserName;
                    userData.GroupID = user.GroupID;
                    userData.OrderID = user.OrderID;

                    //向服务器请求获得用户最新资料
                    userData.type = type.get;
                    SendMessageToServer(userData);
                }
                frm.UserData = userData;
            }

            if (MyAuth.isAdmin)//如果是管理员
                frm.Text = "修改 " + user.UserName + "(" + user.UserID + ")的资料";
            else
                frm.Text = "查看/修改 " + user.UserName + "(" + user.UserID + ")的资料";

            frm.myUserID = MyAuth.UserID;
            frm.isAdmin = MyAuth.isAdmin;
            frm.IsCreate = false;            
            frm.Show();
            frm.Activate ();
        }
        #endregion

        #region 将所有用户设置为离线
        /// <summary>
        /// 将所有用户设置为离线
        /// </summary>
        private void SetAllUsersOffline()
        {
            foreach (exUser user in this.Users)
                user.ShowType = ShowType.Offline;
        }

        #endregion
        
        #region 获得消息对话框

        #region 获得用户消息对话框
        /// <summary>
        /// 获得用户消息对话框
        /// </summary>
        /// <param name="user"></param>
        private FormTalkUser GetUserMsgForm(exUser user)
        {
            FormTalkUser fs = null;
            try
            {
                fs = user.Tag as FormTalkUser;
                if (fs == null || fs.IsDisposed)
                {
                    fs = new FormTalkUser(user );//发送消息对话框根据需要替换
                    user.Tag = fs;
                    fs.myUserID = MyAuth.UserID;
                    fs.myUserName = MyAuth.UserName;
                    if (MyAuth.isControlClient)//如果有权限，则显示控制对方电脑按钮
                        fs.IsRemoteControl = true;
                    fs.Text = "与 " + user.UserName + "(" + user.UserID + ") 对话";

                    ///消息发送事件代理
                    fs.SendMsgToUser += delegate(IMLibrary4.Protocol.Element e, IMLibrary4.Organization.User User)
                    {
                        e.from = MyAuth.UserID;
                        e.to = User.UserID; 
                        SendMessageToServer(e);

                        if (e is IMLibrary4.Protocol.Message)
                            OpeRecordDB.AddMsg(e as IMLibrary4.Protocol.Message);//将消息添加到数据库
                    };
                    fs.FormClosed += delegate(object sender, FormClosedEventArgs e)
                                    { fs.Dispose(); fs = null; user.Tag = null; };
                
                }
            }
            catch (Exception ex)
            {
                IMLibrary4.Global.MsgShow(ex.Message);
            }
            return fs;
        }
       
        #endregion

        #region 获得群对话框
        /// <summary>
        /// 获得群对话框
        /// </summary>
        /// <param name="room"></param>
        private FormTalkRoom GetRoomMsgForm(exRoom room)
        {
            FormTalkRoom fs = null;
            try
            {
                fs = room.Tag as FormTalkRoom;
                if (fs == null || fs.IsDisposed)
                {
                    fs = new FormTalkRoom();//发送消息对话框根据需要替换
                    room.Tag = fs;

                    ///群中创建用户对话窗口事件发生时
                    fs.CreateFormTalkUser += delegate(object sender, exUser user)
                    {
                        FormTalkUser fTalkUser = GetUserMsgForm(user);// 获得用户消息对话框
                        fTalkUser.Show();
                        fTalkUser.Activate();
                    };

                    //更新群事件发生时
                    fs.UpdateRoom += delegate(object sender, exRoom updateRoom)
                    {
                        EditRoom changeRoom = new EditRoom();
                        changeRoom.type = type.set;
                        changeRoom.RoomID = updateRoom.RoomID;
                        changeRoom.RoomName = updateRoom.RoomName;
                        changeRoom.UserIDs = updateRoom.UserIDs;
                        changeRoom.Notice = updateRoom.Notice;
                        SendMessageToServer(changeRoom);
                        //向服务器请求更新群组资料 
                    };


                    ///当在群里发送消息事件发生时
                    fs.SendMsgToGroup += delegate(IMLibrary4.Protocol.Element e, exRoom Room)
                    {
                        SendMessageToServer(e);//发送消息到服务器
                        if (e is IMLibrary4.Protocol.Message)
                            OpeRecordDB.AddMsg(e as IMLibrary4.Protocol.Message);//将消息添加到数据库
                    }; 

                    fs.FormClosed += delegate(object sender, FormClosedEventArgs e)
                    { fs.Dispose(); fs = null; room.Tag = null; };

                    fs.Room = room;
                    fs.myUserID = MyAuth.UserID ;
                    fs.myUserName = MyAuth.UserName;
                }
            }
            catch (Exception ex)
            {
                IMLibrary4.Global.MsgShow(ex.Message);
            }
            return fs;
        }
 
        #region 创建群事件
        private void tButCreateGroup_Click(object sender, EventArgs e)
        {
            if (frmCreateGroup == null || frmCreateGroup.IsDisposed)
            {
                frmCreateGroup = new FormCreateRoom(MyAuth.UserID,MyAuth.UserName, true);
                frmCreateGroup.CreateRoom += delegate(object senders, exRoom room)
                {
                    EditRoom changeRoom = new EditRoom();
                    changeRoom.type = type.New;//标明是新建群
                    changeRoom.RoomID = room.RoomID;
                    changeRoom.RoomName = room.RoomName;
                    changeRoom.UserIDs = room.UserIDs;
                    changeRoom.Notice = room.Notice;
                    SendMessageToServer(changeRoom);//发送消息到服务器，创建群组
                };

            }
            frmCreateGroup.Show();
            frmCreateGroup.Activate();
        } 
        #endregion 
         
        #endregion

        #region 发送通知消息菜单单击

        private void tbutSendNoticeMsg_Click(object sender, EventArgs e)
        {
            if (!MyAuth.isSendNotice)
            {
                IMLibrary4.Global.MsgShow("对不起，目前您没有发送通知消息的权限！");
                return;
            }

            this.treeView_Organization.CheckBoxes = true;//显示复选框
            this.treeView_Organization.ShowPlusMinus = true;//显示+-号

            if (frmNotice == null || frmNotice.IsDisposed)
            {
                frmNotice = new FormSendNotice();

                // 消息窗口关闭后事件
                frmNotice.FormClosed += delegate(object senders, FormClosedEventArgs es)
                {
                    this.treeView_Organization.CheckBoxes = false;
                    this.treeView_Organization.ShowPlusMinus = false;//显示+-号
                };

                //当发送通知消息事件产生时
                frmNotice.SendNotice += delegate(IMLibrary4.Protocol.Message msg)
                {
                    if (msg == null) return;
                    msg.from = MyAuth.UserID;
                    int selectUserCount = 0;
                    foreach (exUser user in this.Users)
                    {
                        TreeNode treeNode = user.TreeNode as TreeNode;
                        if (treeNode!=null && treeNode.Checked)
                            msg.to += user.UserID + ";";
                        selectUserCount++;
                    }
                    if (selectUserCount > 0)//如果选择接收联系人数大于0
                    {
                        SendMessageToServer(msg);//发送消息
                        OpeRecordDB.AddMsg(msg);//将消息添加到数据库
                        //MessageBox.Show("消息发送成功！");
                    }
                };
            }
            frmNotice.Show();
        }

     
        #endregion 

        #endregion

        #region 工具栏事件
        private void tButBaseUsers_ButtonClick(object sender, EventArgs e)
        {
            treeView_Rooms.Visible = false;
            treeView_Organization.Visible = true;

            ToolStripButton btn = sender  as ToolStripButton;
            if (btn != null)
                btn.CheckState = CheckState.Checked;
        }

        private void tButBaseGroups_ButtonClick(object sender, EventArgs e)
        {
            treeView_Rooms.Visible = true;
            treeView_Organization.Visible =false ;

            ToolStripButton btn = sender as ToolStripButton;
            if (btn != null)
                btn.CheckState = CheckState.Checked;
        }
        #endregion

        #region 查找

        #region 查找组
        /// <summary>
        /// 查找组
        /// </summary>
        /// <param name="GroupID"></param>
        /// <returns></returns>
        public exGroup findGroup(string GroupID)
        {
            if (Groups == null) return null;

            foreach (exGroup group in Groups)
                if (group.GroupID == GroupID)
                    return group;
            return null;
        }
        #endregion

        #region 查找用户
        /// <summary>
        /// 查找用户
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        public exUser findUser(string userID)
        {
            if (Users == null) return null;
            foreach (exUser user in Users)
                if (user.UserID == userID)
                    return user;
            return null;
        }
        #endregion

        #region 查找群
        /// <summary>
        /// 查找群
        /// </summary>
        /// <param name="GroupID"></param>
        /// <returns></returns>
        public exRoom findRoom(string RoomID)
        {
            if (Rooms == null) return null;

            foreach (exRoom room in Rooms)
                if (room.RoomID == RoomID)
                    return room;
            return null;
        }
        #endregion

        #region 查找以userid的群
        /// <summary>
        /// userid的群
        /// </summary>
        /// <param name="GroupID"></param>
        /// <returns></returns>
        public exRoom findMyRoom(string userID)
        {
            if (Rooms == null) return null;

            foreach (exRoom room in Rooms)
                if (room.CreateUserID == userID)
                    return room;
            return null;
        }
        #endregion

        #endregion

        #region 管理组织机构信息

        #region 创建用户菜单事件
        private void TsmCreateUser_Click(object sender, EventArgs e)
        {
            if (!MyAuth.isAdmin)
            {
                IMLibrary4.Global.MsgShow("对不起，目前您没有此项管理权限！");
                return;
            }
            if (frmUserVcard == null || frmUserVcard.IsDisposed)
            {
                frmUserVcard = new FormUserVcard();
                frmUserVcard.Create += delegate(object sender1, EditUserData userVcard)
                { CreateUser(userVcard); };
            }
            frmUserVcard.IsCreate = true;//操作为创建 
            frmUserVcard.Show();
        }

        private void tmsCreateUserVcard_Click(object sender, EventArgs e)
        {
            TsmCreateUser_Click(sender, e);
        }
        #endregion

        #region 删除用户菜单事件
        private void TsmDelUser_Click(object sender, EventArgs e)
        {
            TreeNode node = treeView_Organization.SelectedNode;
            if (node != null && node.Tag is exUser)
            {
                exUser user = node.Tag as exUser;
                if (MessageBox.Show("确定要删除用户 " + user.UserName + "(" + user.UserID + ")吗？", "提示", 
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information ) == DialogResult.Yes)
                {
                    EditUserData card = new EditUserData();
                    card.UserID = user.UserID;
                    card.UserName = user.UserName;
                    card.GroupID = user.GroupID;
                    card.type = type.delete;
                    SendMessageToServer(card);//通知服务器删除用户
                }
            }
        }
        #endregion

        #region 菜单弹出事件-显示控制
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            //隐藏控件上所有菜单
            for (int i = 0; i < contextMenuStrip1.Items.Count; i++)
            {
                contextMenuStrip1.Items[i].Visible = false;
            }

            if (treeView_Organization.SelectedNode != null)
            {
                if (treeView_Organization.SelectedNode.Tag is exGroup)
                {
                    if (MyAuth.isAdmin)
                    {
                        TsmCreateGroup.Visible =true ;
                        TsmCreateUser.Visible = true;
                        TsmDelGroup.Visible = true;
                        TsmShow.Visible = true;
                        TsmShowGroupVcard.Text = "修改分组资料";
                    }
                    TsmLoadUser.Visible = true;
                    TsmShowGroupVcard.Visible = true;
                    TsmSendRoomMessage.Visible = true;//现实群发消息
                }
                if (treeView_Organization.SelectedNode.Tag is exUser)
                {
                    if (MyAuth.isAdmin)
                    {
                        TsmCreateGroup.Visible = true;
                        TsmCreateUser.Visible = true;
                        TsmDelUser.Visible = true;
                        TsmShow.Visible = true;
                        TsmShowUserVcard.Text = "修改用户资料";
                    }
                    TsmSendEmail.Visible = true; 
                    TsmSendMessage.Visible = true;
                    TsmShowUserVcard.Visible = true;
                }
            }
            else
                TsmLoadUser.Visible = true;
        }
        #endregion

        #region 获得用户资料窗口
        /// <summary>
        /// 获得用户资料窗口
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        private FormUserVcard getUserVcardForm(string userID)
        {
            FormUserVcard frm = null;
            if (frmUserVcards.ContainsKey(userID))
                frmUserVcards.TryGetValue(userID, out frm);
            return frm;
        }
        #endregion
         
        #region 获得分组资料窗口
        /// <summary>
        /// 获得分组资料窗口
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        private FormGroupVcard getGroupVcardForm(string groupID)
        {
            FormGroupVcard frm = null;
            if (frmGroupVcards.ContainsKey(groupID))
                frmGroupVcards.TryGetValue(groupID, out frm);
            return frm;
        }
        #endregion

        #region 显示用户资料菜单事件
        private void TsmShowUserVcard_Click(object sender, EventArgs e)
        {
            TreeNode node = treeView_Organization.SelectedNode;
            if (node != null)
            {
                exUser user = node.Tag as exUser;
                ShowUserVcard(user);
            }
        }

       
        #endregion

        #region 创建分组
        private void TsmCreateGroup_Click(object sender, EventArgs e)
        {
            if (!MyAuth.isAdmin)
            {
                IMLibrary4.Global.MsgShow("对不起，目前您没有此项管理权限！");
                return;
            }
            if (frmGroupVcard == null || frmGroupVcard.IsDisposed)
            {
                frmGroupVcard = new  FormGroupVcard();
                frmGroupVcard.Create += delegate(object sender1, EditGroupData groupVcard)
                {  CreateGroup(groupVcard); };
            }
            frmGroupVcard.IsCreate = true;//操作为创建 
            frmGroupVcard.Show();
            
        }

        private void tmsCreateGroupVcard_Click(object sender, EventArgs e)
        { 
            TsmCreateGroup_Click(sender, e);
        }

        #endregion

        #region 显示分组资料
        private void TsmShowGroupVcard_Click(object sender, EventArgs e)
        {
            TreeNode node = treeView_Organization.SelectedNode;
            if (node != null)
            {
                exGroup group = node.Tag as exGroup;
                FormGroupVcard  frm = getGroupVcardForm(group.GroupID);
                if (frm == null)
                {
                    frm = new FormGroupVcard();
                    frmGroupVcards.Add(group.GroupID, frm);
                    if (MyAuth.isAdmin)//如果是管理员，则添加更新事件
                        frm.UpdateVcard += delegate(object sender1, EditGroupData card)
                    {
                        UpdateGroup(card);//更新分组资料
                    };
                    else
                        frm.RefreshVcard += delegate(object sender1, EditGroupData card)
                        {
                            //向服务器请求获得分组最新资料
                            //card.type = type.get;
                            //SendMessageToServer(card);
                        };
                    frm.FormClosed += delegate(object sender1, FormClosedEventArgs e1)
                    {
                        frm.Dispose();
                        frmGroupVcards.Remove(group.GroupID);
                    };
                    EditGroupData vcard = new EditGroupData();
                    vcard.GroupID = group.GroupID;
                    vcard.GroupName = group.GroupName;
                    vcard.SuperiorID = group.SuperiorID;
                    vcard.OrderID = group.OrderID;
                    frm.GroupVcard = vcard;
                }
                if (MyAuth.isAdmin)//如果是管理员
                    frm.Text = "修改 " + group.GroupName + "(" + group.GroupID + ")的资料";
                else
                    frm.Text = "查看 " + group.GroupName + "(" + group.GroupID + ")的资料";

                frm.isAdmin = MyAuth.isAdmin;
                frm.IsCreate = false;
                frm.Show();
            }

        }
        #endregion 

        #region 删除分组菜单
        private void TsmDelGroup_Click(object sender, EventArgs e)
        {
            TreeNode node = treeView_Organization.SelectedNode;
            if (node != null && node.Tag is exGroup)
            {
                if (node.Nodes.Count > 0)
                {
                    IMLibrary4.Global.MsgShow("请先删除此分组内所有用户和子分组！");
                    return;
                }
                exGroup group = node.Tag as exGroup;
                if (MessageBox.Show("确定要删除分组 " + group.GroupName + "(" + group.GroupID  + ")吗？", "提示",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    EditGroupData card = new EditGroupData();
                    card.GroupID = group.GroupID;
                    card.GroupName = group.GroupName;
                    card.type = type.delete;
                    SendMessageToServer(card);//通知服务器删除用户
                }
            }
        }
        #endregion

        #region 发送即时消息
        
        private void TsmSendMessage_Click(object sender, EventArgs e)
        {
            if (treeView_Organization.SelectedNode.Tag is exUser)
            {
                exUser user = treeView_Organization.SelectedNode.Tag as exUser;
                if (user.UserID == MyAuth.UserID) return;//不能给自己发送消息
                FormTalkUser fs = GetUserMsgForm(user);// 获得用户消息对话框
                fs.Show();
                fs.Activate();
            }
            //MessageBox.Show("1");
        }
        #endregion        

        #region 群发消息
        private void TsmSendRoomMessage_Click(object sender, EventArgs e)
        {
            if (treeView_Organization.SelectedNode.Nodes.Count == 0) return;
            if (treeView_Organization.SelectedNode.Tag is exGroup)
            {
                exGroup Group = treeView_Organization.SelectedNode.Tag as exGroup;

                //查找群
                exRoom room = findMyRoom(MyAuth.UserID);
                
                EditRoom changeRoom = new EditRoom();
                if (room == null)
                {
                    changeRoom.type = type.New;//标明是新建群
                    changeRoom.RoomID = "";                    
                }
                else
                {
                    changeRoom.type = type.set;//标明是新建群
                    changeRoom.RoomID = room.RoomID;
                }
                changeRoom.RoomName = "[" + MyAuth.UserID + "]" + MyAuth.UserName + "发起的会话";//群组名称
                changeRoom.CreateUserID = MyAuth.UserID;//群组创建用户编号
                //循环取用户
                foreach (TreeNode node in this.treeView_Organization.SelectedNode.Nodes)
                {
                    if (node.Tag is exUser)
                    {
                        exUser user = node.Tag as exUser;
                        if (user.UserID!=MyAuth.UserID) //防止点击其他不包含自己的分组
                            changeRoom.UserIDs += user.UserID + ";";
                    }
                }
                changeRoom.UserIDs += MyAuth.UserID + ";";//添加自己到更新人员
                changeRoom.Notice = "";//群组通知
                SendMessageToServer(changeRoom);//发送消息到服务器，创建群组

                //创建或更新后重新查询
                room = findMyRoom(MyAuth.UserID);
                if (room != null)
                {//打开
                    FormTalkRoom fs = GetRoomMsgForm(room);
                    //fs.Text = "[" + MyAuth.UserID + "]" + MyAuth.UserName + "发起的会话";
                    fs.Show();
                    fs.Activate();
                }
            }
        }

        #endregion

        #region 刷新所有用户状态

        /// <summary>
        /// 刷新所有用户状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TsmLoadUser_Click(object sender, EventArgs e)
        {
            //请求联系人在线状态信息
            Presence pre = new Presence();
            pre.from = MyAuth.UserID;
            pre.type = type.get;//必须设置 set,以表示是设置，如果为get，则是获取所有联系人的状态
            SendMessageToServer(pre);
        }

        #endregion

        #region 发送邮件
        private void TsmSendEmail_Click(object sender, EventArgs e)
        {
            if (treeView_Organization.SelectedNode.Tag is exUser)
            {
                exUser user = treeView_Organization.SelectedNode.Tag as exUser;
                if (user.UserID == MyAuth.UserID) return;//不能给自己发送
                //获得发送邮件页面
                string[] ToUser = { user.UserID, user.UserName + "<" + user.UserID + ">;" };
                OurMsg.IMMail.FormMail frm = new IMMail.FormMail(ToUser, IMMail.Mail_Enum.Form_State.NewMail, "新增邮件");
                //发送邮件通知代理事件
                frm.SendToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
                {
                    //发送通知
                    SendMessageToServer(msg);
                };
                frm.Show();
                frm.Activate();
            }

        }

        #endregion

        #region 打开新窗口
        
        private Form frmshow; //定义一个窗体判断是否被打开
        /// <summary>
        /// 打开发送邮件
        /// </summary>
        public void ShowMail()
        {
            //string[] User = { MyAuth.UserID, MyAuth.UserName + "<" + MyAuth.UserID + ">" };
            IMMail.FormMailList frm = new IMMail.FormMailList();
            frm.ReadMail += delegate(object sender)
            {
                //刷新
                LoadMail(sender);
            };
            frm.SendeToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
            {
                //发送通知
                SendMessageToServer(msg);
            };
            if (Application.OpenForms[frm.Name] != null)
            {
                frmshow = Application.OpenForms[frm.Name];
                frmshow.Activate();
            }
            else
                frm.Show();
        }

        /// <summary>
        /// 打开离线文件列表
        /// </summary>
        public void ShowEnclosure()
        {
            FormReceivedFile frm = new FormReceivedFile(MyAuth.UserID);
            if (Application.OpenForms[frm.Name] != null)
            {
                frmshow = Application.OpenForms[frm.Name];
                frmshow.Activate();
            }
            else
                frm.Show();
        }

        /// <summary>
        /// 打开主窗口
        /// </summary>
        public void ShowMainForm(string _ShowFormText)
        {
            FormMDIMain frm = new FormMDIMain();
            frm.FormText = _ShowFormText;
            //加载事件
            frm.ReadMail += delegate(object sender)
            {
                //邮箱刷新
                LoadMail(sender);
            };
            frm.SendeToUserMsg += delegate(IMLibrary4.Protocol.Message msg, String FromUser)
            {
                //发送邮件通知
                SendMessageToServer(msg);
            };
            if (Application.OpenForms[frm.Name] != null)
            {
                frm = (FormMDIMain)Application.OpenForms[frm.Name];
                frm.Activate();
                if (frm.WindowState == FormWindowState.Minimized)//如果当前窗体已经最小化
                {
                    frm.WindowState = FormWindowState.Maximized; //还原窗体
                }
                frm.ShowForms();
            }
            else
                frm.Show();
        }

        #endregion

        private void TsmSendRoomEmail_Click(object sender, EventArgs e)
        {

        }

        #endregion

    }
}
