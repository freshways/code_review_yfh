﻿namespace OurMsg.Controls
{
    partial class MessagePanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessagePanel));
            this.panel3 = new BSE.Windows.Forms.Panel();
            this.txtRecord = new IMLibrary4.MyExtRichTextBox();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemCopy2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemSelAll2 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSend = new IMLibrary4.MyExtRichTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemPaset = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemDel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemSelAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsButSetFont = new System.Windows.Forms.ToolStripButton();
            this.tsButColor = new System.Windows.Forms.ToolStripButton();
            this.tsButFace = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsButCaptureImageTool = new System.Windows.Forms.ToolStripButton();
            this.tsButAway = new System.Windows.Forms.ToolStripButton();
            this.panel3.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.AssociatedSplitter = null;
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.CaptionFont = new System.Drawing.Font("微软雅黑", 9F);
            this.panel3.CaptionHeight = 22;
            this.panel3.Controls.Add(this.txtRecord);
            this.panel3.CustomColors.BorderColor = System.Drawing.SystemColors.ControlText;
            this.panel3.CustomColors.CaptionCloseIcon = System.Drawing.SystemColors.ControlText;
            this.panel3.CustomColors.CaptionExpandIcon = System.Drawing.SystemColors.ControlText;
            this.panel3.CustomColors.CaptionGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panel3.CustomColors.CaptionGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panel3.CustomColors.CaptionGradientMiddle = System.Drawing.SystemColors.ButtonFace;
            this.panel3.CustomColors.CaptionSelectedGradientBegin = System.Drawing.SystemColors.Window;
            this.panel3.CustomColors.CaptionSelectedGradientEnd = System.Drawing.SystemColors.Window;
            this.panel3.CustomColors.CaptionText = System.Drawing.SystemColors.ControlText;
            this.panel3.CustomColors.CollapsedCaptionText = System.Drawing.SystemColors.ControlText;
            this.panel3.CustomColors.ContentGradientBegin = System.Drawing.SystemColors.ButtonFace;
            this.panel3.CustomColors.ContentGradientEnd = System.Drawing.SystemColors.ButtonFace;
            this.panel3.CustomColors.InnerBorderColor = System.Drawing.SystemColors.Window;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel3.Image = null;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.MinimumSize = new System.Drawing.Size(22, 22);
            this.panel3.Name = "panel3";
            this.panel3.PanelStyle = BSE.Windows.Forms.PanelStyle.Office2007;
            this.panel3.ShowExpandIcon = true;
            this.panel3.Size = new System.Drawing.Size(397, 195);
            this.panel3.TabIndex = 29;
            this.panel3.Text = "对话记录";
            this.panel3.ToolTipTextCloseIcon = null;
            this.panel3.ToolTipTextExpandIconPanelCollapsed = null;
            this.panel3.ToolTipTextExpandIconPanelExpanded = null;
            // 
            // txtRecord
            // 
            this.txtRecord.BackColor = System.Drawing.SystemColors.Window;
            this.txtRecord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRecord.ContextMenuStrip = this.contextMenuStrip2;
            this.txtRecord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRecord.Location = new System.Drawing.Point(1, 23);
            this.txtRecord.Margin = new System.Windows.Forms.Padding(4);
            this.txtRecord.Name = "txtRecord";
            this.txtRecord.ReadOnly = true;
            this.txtRecord.Size = new System.Drawing.Size(395, 171);
            this.txtRecord.TabIndex = 0;
            this.txtRecord.Text = "";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemCopy2,
            this.toolStripSeparator1,
            this.MenuItemSelAll2});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(130, 58);
            // 
            // MenuItemCopy2
            // 
            this.MenuItemCopy2.Name = "MenuItemCopy2";
            this.MenuItemCopy2.Size = new System.Drawing.Size(129, 24);
            this.MenuItemCopy2.Text = "复制(&C)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(126, 6);
            // 
            // MenuItemSelAll2
            // 
            this.MenuItemSelAll2.Name = "MenuItemSelAll2";
            this.MenuItemSelAll2.Size = new System.Drawing.Size(129, 24);
            this.MenuItemSelAll2.Text = "全选(&A)";
            // 
            // txtSend
            // 
            this.txtSend.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSend.ContextMenuStrip = this.contextMenuStrip1;
            this.txtSend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtSend.Location = new System.Drawing.Point(0, 227);
            this.txtSend.Margin = new System.Windows.Forms.Padding(4);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(397, 118);
            this.txtSend.TabIndex = 0;
            this.txtSend.Text = "";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemCopy,
            this.MenuItemPaset,
            this.MenuItemCut,
            this.MenuItemDel,
            this.toolStripMenuItem1,
            this.MenuItemSelAll});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(130, 130);
            // 
            // MenuItemCopy
            // 
            this.MenuItemCopy.Name = "MenuItemCopy";
            this.MenuItemCopy.Size = new System.Drawing.Size(129, 24);
            this.MenuItemCopy.Text = "复制(&C)";
            // 
            // MenuItemPaset
            // 
            this.MenuItemPaset.Name = "MenuItemPaset";
            this.MenuItemPaset.Size = new System.Drawing.Size(129, 24);
            this.MenuItemPaset.Text = "粘贴(&P)";
            // 
            // MenuItemCut
            // 
            this.MenuItemCut.Name = "MenuItemCut";
            this.MenuItemCut.Size = new System.Drawing.Size(129, 24);
            this.MenuItemCut.Text = "剪切(&T)";
            // 
            // MenuItemDel
            // 
            this.MenuItemDel.Name = "MenuItemDel";
            this.MenuItemDel.Size = new System.Drawing.Size(129, 24);
            this.MenuItemDel.Text = "删除(&D)";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(126, 6);
            // 
            // MenuItemSelAll
            // 
            this.MenuItemSelAll.Name = "MenuItemSelAll";
            this.MenuItemSelAll.Size = new System.Drawing.Size(129, 24);
            this.MenuItemSelAll.Text = "全选(&A)";
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsButSetFont,
            this.tsButColor,
            this.tsButFace,
            this.tsButCaptureImageTool,
            this.tsButAway});
            this.toolStrip2.Location = new System.Drawing.Point(0, 195);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(397, 32);
            this.toolStrip2.TabIndex = 21;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsButSetFont
            // 
            this.tsButSetFont.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButSetFont.Image = ((System.Drawing.Image)(resources.GetObject("tsButSetFont.Image")));
            this.tsButSetFont.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButSetFont.Name = "tsButSetFont";
            this.tsButSetFont.Size = new System.Drawing.Size(24, 29);
            this.tsButSetFont.Text = "字体设置";
            // 
            // tsButColor
            // 
            this.tsButColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButColor.Image = ((System.Drawing.Image)(resources.GetObject("tsButColor.Image")));
            this.tsButColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButColor.Name = "tsButColor";
            this.tsButColor.Size = new System.Drawing.Size(24, 27);
            this.tsButColor.Text = "字体颜色";
            // 
            // tsButFace
            // 
            this.tsButFace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButFace.Image = ((System.Drawing.Image)(resources.GetObject("tsButFace.Image")));
            this.tsButFace.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButFace.Name = "tsButFace";
            this.tsButFace.ShowDropDownArrow = false;
            this.tsButFace.Size = new System.Drawing.Size(24, 27);
            this.tsButFace.Text = "插入表情";
            this.tsButFace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsButCaptureImageTool
            // 
            this.tsButCaptureImageTool.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsButCaptureImageTool.Image = ((System.Drawing.Image)(resources.GetObject("tsButCaptureImageTool.Image")));
            this.tsButCaptureImageTool.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButCaptureImageTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButCaptureImageTool.Name = "tsButCaptureImageTool";
            this.tsButCaptureImageTool.Size = new System.Drawing.Size(24, 27);
            this.tsButCaptureImageTool.Text = "插入截图";
            // 
            // tsButAway
            // 
            this.tsButAway.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsButAway.Image = global::OurMsg.Properties.Resources.away;
            this.tsButAway.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsButAway.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsButAway.Name = "tsButAway";
            this.tsButAway.Size = new System.Drawing.Size(84, 27);
            this.tsButAway.Text = "消息记录";
            // 
            // MessagePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.txtSend);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MessagePanel";
            this.Size = new System.Drawing.Size(397, 345);
            this.panel3.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private BSE.Windows.Forms.Panel panel3;
        private IMLibrary4.MyExtRichTextBox txtRecord;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsButSetFont;
        private System.Windows.Forms.ToolStripButton tsButColor;
        private System.Windows.Forms.ToolStripDropDownButton tsButFace;
        private System.Windows.Forms.ToolStripButton tsButCaptureImageTool;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCopy2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSelAll2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCopy;
        private System.Windows.Forms.ToolStripMenuItem MenuItemPaset;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCut;
        private System.Windows.Forms.ToolStripMenuItem MenuItemDel;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSelAll;
        public IMLibrary4.MyExtRichTextBox txtSend;
        private System.Windows.Forms.ToolStripButton tsButAway;
    }
}
