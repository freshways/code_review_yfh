﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IMLibrary4.AV ;
using IMLibrary4.Protocol;
using IMLibrary4.Net;
using System.Net;

namespace OurMsg.Controls
{
    public partial class DesktopControl : UserControl
    {

        #region 变量和属性
        public DesktopControl()
        {
            InitializeComponent();
            butReceive.Click += (butReceive_Click);
            butCancel.Click += (butCancel_Click);
        }

        /// <summary>
        /// 远程桌面控制对像
        /// </summary>
        public RemoteHelp  remoteDesktopControl = null;

        PictureBox remotePictureBox = new PictureBox();

        /// <summary>
        /// 是否允许控制
        /// </summary>
        private bool isAllowControl = false;
        public bool IsAllowControl
        {
            set 
            {
                isAllowControl = value;
                butCancel.Visible = false;
            }
            get { return isAllowControl; } 
        }

        private bool isControler = false;

        /// <summary>
        /// 是否控制端(控制端不用发送桌面图像给对方)
        /// </summary>
        public bool IsControler
        {
            set
            {
                isControler = value;
                butReceive.Visible = value;
                checkBox1.Visible = !value;
                checkBox1.Checked = false;
                if (value)
                    this.label1.Text = "对方邀请您远程协助...";
                else
                    this.label1.Text = "请求对方远程协助...";
            }
            get { return isControler; }
        }

        frmDesktop fDesktop = null;

        /// <summary>
        /// 对方屏幕宽度
        /// </summary>
        private int ToScreenWidth { set; get; }
        /// <summary>
        /// 对方屏幕高度
        /// </summary>
        private  int ToScreenHeight { set; get; }
        /// <summary>
        /// 对方桌面窗口标题
        /// </summary>
        private string DesktopTilte = "对方桌面";
        #endregion

        #region 事件
        /// <summary>
        /// 获得IPEndPoint事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="local">本地主机信息</param>
        /// <param name="remote">远程主机信息</param>
        public delegate void GetIPEndPointEventHandler(object sender, IPEndPoint local, IPEndPoint remote, int ScreenWidth, int ScreenHeigth);
        public event GetIPEndPointEventHandler GetIPEndPoint;
        private void OnGetIPEndPoint(object sender, IPEndPoint local, IPEndPoint remote)
        {
            if (GetIPEndPoint != null)
            {
                Rectangle screenArea = Rectangle.Empty;
                screenArea = Rectangle.Union(screenArea, Screen.PrimaryScreen.Bounds);
                GetIPEndPoint(this, local, remote,screenArea.Width,screenArea.Height);//触发获取本机主机事件
            }
        }
     

        public delegate void CancelEventHandler(object sender);
        /// <summary>
        /// 取消事件
        /// </summary>
        public event CancelEventHandler Cancel;
        #endregion

        #region 初始化本地组件
        private void IniLocalControls(System.Net.IPEndPoint ServerEP)
        {
            if (remoteDesktopControl == null)
            {
                remoteDesktopControl = new RemoteHelp(ServerEP);
                remoteDesktopControl.IsControler = IsControler;

                remoteDesktopControl.GetIPEndPoint += (sender, local, remote) =>
                {
                    OnGetIPEndPoint(this, local, remote);
                };
                remoteDesktopControl.TransmitConnected += (sender, connectedType) =>
                {
                    showConnectedInfoDelegate show = new showConnectedInfoDelegate(showConnectedInfo);
                    this.Invoke(show, connectedType);
                };
                remoteDesktopControl.ReceiveFirstImage += (s, e) =>
                    {
                        delegateShowRemoteDesktopForm d = new delegateShowRemoteDesktopForm(ShowRemoteDesktopForm);
                        this.Invoke(d);
                    };
                remoteDesktopControl.Start();
            }
        }

       
        #endregion

        #region 显示远程桌面
        delegate void delegateShowRemoteDesktopForm();
        private void ShowRemoteDesktopForm()
        {
            if (isControler && fDesktop == null)//如果是控制者
            {
                fDesktop = new frmDesktop(new Size(ToScreenWidth, ToScreenHeight), DesktopTilte);

                fDesktop.FormClosed += (s1, e1) =>
                {
                    if (fDesktop.IsSelfClose)
                        butCancel_Click(null, null);
                };

                fDesktop.KeyDown += (sender, e) =>
                    {
                        remoteDesktopControl.sendKeyDown(e.KeyCode);
                    };
                fDesktop.KeyUp += (sender, e) =>
                {
                    remoteDesktopControl.sendKeyUp(e.KeyCode);
                };
                remoteDesktopControl.picBox = fDesktop.pictureBox1;
                fDesktop.Show();
            }
        }
        #endregion

        #region 连接产生事件
        delegate void showConnectedInfoDelegate(ConnectedType connectedType);
        private void showConnectedInfo(ConnectedType connectedType)
        {
            switch (connectedType)
            {
                case ConnectedType.None:
                    this.label1.Text = "未能与对方建立连接。";
                    break;
                case ConnectedType.UDPLocal :
                    this.label1.Text = "已与对方建立局域网直接。";
                    break;
                case ConnectedType.UDPRemote :
                    this.label1.Text = "已与对方建立广域网直接。";
                    break;
                case ConnectedType.UDPServer :
                    this.label1.Text = "已与对方建立服务器中转连接。";
                    break;
            }
            if (!isControler && isAllowControl)//建立连接之后如果不是控制方并且控制方是有权限直接允许控制
            {
                checkBox1.Checked = true;
            }
        }
        #endregion

        #region 设置对方远程主机信息(实现任何类型的NAT穿越,实现P2P通信)
        /// <summary>
        /// 设置对方远程主机信息(实现任何类型的NAT穿越,实现P2P通信)
        /// </summary>
        ///<param name="msg"></param>
        public void SetRometEP(RemoteDesktopMsg msg, string desktopTilte)
        {
            this.DesktopTilte = desktopTilte;

            if (msg.type == type.set)
            {
                IniLocalControls(Global.AVTransmitServerEP);//设置服务器主机信息并开始服务,设置对方桌面宽度,设置对方桌面高度
                remoteDesktopControl.setRemoteIPandScreen(new IPEndPoint(msg.LocalIP, msg.LocalPort), new IPEndPoint(msg.remoteIP, msg.remotePort),
                                                       msg.ScreenWidth ,  msg.ScreenHeight );//设置AV通信组件远程主机信息
                ToScreenWidth = msg.ScreenWidth;
                ToScreenHeight = msg.ScreenHeight;
            }
            if(msg.type == type.get)
                checkBox1.Checked = true;
        }
        #endregion

        #region 取消
        /// <summary>
        /// 取消
        /// </summary>
        public void Close()
        {
            if (remoteDesktopControl != null)
            {
                remoteDesktopControl.CancelTransmit();
                remoteDesktopControl = null;
            }
            if (fDesktop != null && !fDesktop.IsDisposed)//如果是控制者
            {
                fDesktop.IsSelfClose = false;
                fDesktop.Close();
                fDesktop.Dispose();
                fDesktop = null;
            }
        }
        #endregion

        #region 事件响应代码

        //开始控制
        void butReceive_Click(object sender, EventArgs e)
        {
            butReceive.Visible = false;
            IniLocalControls(Global.AVTransmitServerEP);
        }


        // 取消控制
        void butCancel_Click(object sender, EventArgs e)
        {
            Close();
            if (Cancel != null)
                Cancel(this);
        }

        #endregion

        #region 设置是否允许对方控制本地桌面
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (null != remoteDesktopControl)
                    remoteDesktopControl.AllowControl = checkBox1.Checked;
                else
                    checkBox1.Checked = false;
            }
            catch { checkBox1.Checked = false; }            
        }
        #endregion
    }
}
