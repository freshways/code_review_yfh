﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace OurMsg.Controls
{
    public partial class SelfFaceImage : UserControl
    {
        public SelfFaceImage()
        {
            InitializeComponent();
            this.BorderStyle = System.Windows.Forms.BorderStyle.None;
        }

        /// <summary>
        /// 事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void  EventHandler(object sender,EventArgs e);
        /// <summary>
        /// 头像单击事件
        /// </summary>
        public event EventHandler ImageClick;
        
        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.BorderStyle = System.Windows.Forms.BorderStyle.None;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (ImageClick != null)
                ImageClick(this, new EventArgs());
        }
    }
}
