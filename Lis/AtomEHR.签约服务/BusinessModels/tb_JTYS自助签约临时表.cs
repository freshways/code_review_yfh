﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.Models
{


/*==========================================
 *   程序说明: tb_JTYS自助签约临时表的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2019-03-27 02:27:25
 *   最后修改: 2019-03-27 02:27:25
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_JTYS自助签约临时表,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYS自助签约临时表", "ID", true)]
    public sealed class tb_JTYS自助签约临时表
    {
        public static string __TableName ="tb_JTYS自助签约临时表";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 档案号 = "档案号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 联系电话 = "联系电话"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,5,false,true,false,false,false)]
        public static string 签约对象类型 = "签约对象类型"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 健康状况 = "健康状况"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,200,false,true,false,false,false)]
        public static string 健康状况名称 = "健康状况名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,360,false,true,false,false,false)]
        public static string 签约服务包 = "签约服务包"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,300,false,true,false,false,false)]
        public static string 服务包名称 = "服务包名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 团队ID = "团队ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 签约医生 = "签约医生"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 签约日期 = "签约日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 审批状态 = "审批状态"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,400,false,true,false,false,false)]
        public static string 审批备注 = "审批备注"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 生效日期 = "生效日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 到期日期 = "到期日期"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,10,false,true,false,false,false)]
        public static string 审批人 = "审批人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 审批时间 = "审批时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 签约来源 = "签约来源"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 签约人 = "签约人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,-1,false,true,false,false,false)]
        public static string 手签照 = "手签照"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 档案机构 = "档案机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 录入人机构 = "录入人机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,40,false,true,false,false,false)]
        public static string 唯一标志 = "唯一标志"; 

    }
}