﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.签约服务.Models
{


/*==========================================
 *   程序说明: tb_JTYS全局参数的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2017/12/28 08:19:36
 *   最后修改: 2017/12/28 08:19:36
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_JTYS全局参数,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYS全局参数", "ID", true)]
    public sealed class tb_JTYS全局参数
    {
        public static string __TableName ="tb_JTYS全局参数";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string code = "code"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string parValue = "parValue"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string paramDesc = "paramDesc"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,200,false,true,false,false,false)]
        public static string remark = "remark"; 

    }
}