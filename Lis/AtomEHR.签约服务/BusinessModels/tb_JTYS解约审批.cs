﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.签约服务.Models
{


/*==========================================
 *   程序说明: tb_JTYS解约审批的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018/09/25 03:43:45
 *   最后修改: 2018/09/25 03:43:45
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_JTYS解约审批,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYS解约审批", "ID", true)]
    public sealed class tb_JTYS解约审批
    {
        public static string __TableName ="tb_JTYS解约审批";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.BigInt,8,false,true,false,false,false)]
        public static string QYID = "QYID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 申请人 = "申请人"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,40,false,true,false,false,false)]
        public static string 申请人姓名 = "申请人姓名"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,400,false,true,false,false,false)]
        public static string 解约原因 = "解约原因"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 申请时间 = "申请时间"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,20,false,true,false,false,false)]
        public static string 审批状态 = "审批状态"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,400,false,true,false,false,false)]
        public static string 审批备注 = "审批备注"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 审批人 = "审批人"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,40,false,true,false,false,false)]
        public static string 审批人姓名 = "审批人姓名"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 审批时间 = "审批时间"; 

    }
}