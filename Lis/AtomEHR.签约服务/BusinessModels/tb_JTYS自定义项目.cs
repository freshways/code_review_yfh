﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.签约服务.Models
{


/*==========================================
 *   程序说明: tb_JTYS自定义项目的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018/01/03 09:51:08
 *   最后修改: 2018/01/03 09:51:08
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_JTYS自定义项目,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYS自定义项目", "项目ID", true)]
    public sealed class tb_JTYS自定义项目
    {
        public static string __TableName ="tb_JTYS自定义项目";

        public static string __KeyName = "项目ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,true,false,false)]
        public static string 项目ID = "项目ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,100,false,true,false,false,false)]
        public static string 项目名称 = "项目名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 拼音简码 = "拼音简码"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 单位 = "单位"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,9,false,true,false,false,false)]
        public static string 单价 = "单价"; 

        [ORM_FieldAttribute(SqlDbType.Bit,1,false,true,false,false,false)]
        public static string 检验项目 = "检验项目"; 

        [ORM_FieldAttribute(SqlDbType.Bit,1,false,true,false,false,false)]
        public static string 检查项目 = "检查项目"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 项目分组 = "项目分组"; 

        [ORM_FieldAttribute(SqlDbType.Bit,1,false,true,false,false,false)]
        public static string 是否有效 = "是否有效"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

    }
}