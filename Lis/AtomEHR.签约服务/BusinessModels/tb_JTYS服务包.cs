﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.签约服务.Models
{


/*==========================================
 *   程序说明: tb_JTYS服务包的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018/04/24 04:30:15
 *   最后修改: 2018/04/24 04:30:15
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_JTYS服务包,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYS服务包", "ID", true)]
    public sealed class tb_JTYS服务包
    {
        public static string __TableName ="tb_JTYS服务包";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string ServiceID = "ServiceID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 名称 = "名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 适宜对象 = "适宜对象"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,600,false,true,false,false,false)]
        public static string 服务内容 = "服务内容"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,600,false,true,false,false,false)]
        public static string 卫生院职责 = "卫生院职责"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,600,false,true,false,false,false)]
        public static string 卫生室职责 = "卫生室职责"; 

        [ORM_FieldAttribute(SqlDbType.Decimal,9,false,true,false,false,false)]
        public static string 年收费标准 = "年收费标准"; 

        [ORM_FieldAttribute(SqlDbType.Bit,1,false,true,false,false,false)]
        public static string 是否有效 = "是否有效"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string 允许一年多次 = "允许一年多次"; 

    }
}