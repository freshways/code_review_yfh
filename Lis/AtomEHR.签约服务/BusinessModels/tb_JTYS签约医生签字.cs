﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;


/*==========================================
 *   程序说明: tb_JTYS签约医生签字的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018-11-22 08:52:49
 *   最后修改: 2018-11-22 08:52:49
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Models
{
    ///<summary>
    /// ORM模型, 数据表:tb_JTYS签约医生签字,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYS签约医生签字", "ID", true)]
    public sealed class tb_JTYS签约医生签字
    {
        public static string __TableName = "tb_JTYS签约医生签字";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int, 4, false, false, true, false, false)]
        public static string ID = "ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, 40, false, true, false, false, false)]
        public static string 费用ID = "费用ID";

        [ORM_FieldAttribute(SqlDbType.VarChar, -1, false, true, false, false, false)]
        public static string 签字 = "签字";

    }
}