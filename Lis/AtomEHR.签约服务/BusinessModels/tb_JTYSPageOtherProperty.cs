﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.签约服务.Models
{


/*==========================================
 *   程序说明: tb_JTYSPageOtherProperty的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2018/07/16 07:28:55
 *   最后修改: 2018/07/16 07:28:55
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_JTYSPageOtherProperty,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYSPageOtherProperty", "ID", true)]
    public sealed class tb_JTYSPageOtherProperty
    {
        public static string __TableName ="tb_JTYSPageOtherProperty";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,false,true,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string 所属机构 = "所属机构";

        [ORM_FieldAttribute(SqlDbType.VarChar, 4, false, true, false, false, false)]
        public static string SPShowJMJE = "SPShowJMJE"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,5,false,true,false,false,false)]
        public static string PaperSize = "PaperSize"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,4,false,true,false,false,false)]
        public static string showpage1 = "showpage1"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string CreateTime = "CreateTime"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string Createuser = "Createuser"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string updatetime = "updatetime"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,30,false,true,false,false,false)]
        public static string updateuser = "updateuser"; 

    }
}