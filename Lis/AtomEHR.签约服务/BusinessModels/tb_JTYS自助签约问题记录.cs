﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.签约服务.Models
{


/*==========================================
 *   程序说明: tb_JTYS自助签约问题记录的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2019-03-12 02:52:34
 *   最后修改: 2019-03-12 02:52:34
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_JTYS自助签约问题记录,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYS自助签约问题记录", "ID", true)]
    public sealed class tb_JTYS自助签约问题记录
    {
        public static string __TableName ="tb_JTYS自助签约问题记录";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 档案号 = "档案号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 所属医院 = "所属医院"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 档案所属机构 = "档案所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 档案机构名称 = "档案机构名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,18,false,true,false,false,false)]
        public static string 居住地址编码 = "居住地址编码"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,200,false,true,false,false,false)]
        public static string 详细地址 = "详细地址"; 

        [ORM_FieldAttribute(SqlDbType.NVarChar,400,false,true,false,false,false)]
        public static string 提醒内容 = "提醒内容"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 处理人 = "处理人"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 处理时间 = "处理时间"; 

        [ORM_FieldAttribute(SqlDbType.Bit,1,false,true,false,false,false)]
        public static string 是否处理 = "是否处理"; 

    }
}