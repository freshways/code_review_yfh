﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AtomEHR.ORM;

namespace AtomEHR.签约服务.Models
{


/*==========================================
 *   程序说明: tb_JTYS团队卫生室管理关系的ORM模型
 *   作者姓名: ATOM
 *   创建日期: 2019-03-12 02:50:38
 *   最后修改: 2019-03-12 02:50:38
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

    ///<summary>
    /// ORM模型, 数据表:tb_JTYS团队卫生室管理关系,由[代码生成器]自动生成
    /// </summary>
    [ORM_ObjectClassAttribute("tb_JTYS团队卫生室管理关系", "ID", true)]
    public sealed class tb_JTYS团队卫生室管理关系
    {
        public static string __TableName ="tb_JTYS团队卫生室管理关系";

        public static string __KeyName = "ID";

        [ORM_FieldAttribute(SqlDbType.Int,4,false,true,false,false,false)]
        public static string ID = "ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 所属机构 = "所属机构"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 机构名称 = "机构名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 团队ID = "团队ID"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,50,false,true,false,false,false)]
        public static string 团队名称 = "团队名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 卫生室编号 = "卫生室编号"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,150,false,true,false,false,false)]
        public static string 卫生室名称 = "卫生室名称"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 创建人 = "创建人"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 创建时间 = "创建时间"; 

        [ORM_FieldAttribute(SqlDbType.VarChar,20,false,true,false,false,false)]
        public static string 修改人 = "修改人"; 

        [ORM_FieldAttribute(SqlDbType.DateTime,8,false,true,false,false,false)]
        public static string 修改时间 = "修改时间"; 

        [ORM_FieldAttribute(SqlDbType.Bit,1,false,true,false,false,false)]
        public static string 是否有效 = "是否有效"; 

    }
}