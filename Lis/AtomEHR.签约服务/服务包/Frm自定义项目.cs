﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.Common;

namespace AtomEHR.签约服务.服务包
{
    public partial class Frm自定义项目 : AtomEHR.Library.frmBaseBusinessForm
    {
        public Frm自定义项目()
        {
            InitializeComponent();
        }

        private void Frm自定义项目_Load(object sender, EventArgs e)
        {
            this.pagerControl1.Height = 34;
			this.InitializeForm();
        }
		
		protected override void InitializeForm()
        {
            _BLL = new bllJTYS服务包();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            //frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //util.BindingHelper.LookupEditBindDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, lookUpEdit性别, "P_CODE", "P_DESC");

            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");     
            
            if(Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;
            }
        }
        
        protected override bool DoSearchSummary()
        {
            string _strWhere = "";//" and " + tb_JTYS医生信息.是否有效 + "=1";

            string str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            if (!string.IsNullOrWhiteSpace(str机构))
            {
                _strWhere = " and " + tb_JTYS自定义项目.所属机构 + "='" + str机构 + "' ";
            }

            if (!string.IsNullOrWhiteSpace(txtName.Text))
            {
                _strWhere = " and " + tb_JTYS自定义项目.项目名称 + " like '%" + txtName.Text.Trim() + "%' ";
            }

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS自定义项目", "*", _strWhere, tb_JTYS自定义项目.项目ID, "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   
            gvSummary.BestFitColumns();

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Frm自定义项目Add frm = new Frm自定义项目Add(UpdateType.Add, "");
            frm.ShowDialog();
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }

            string itemid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS自定义项目.项目ID).ToString();
            Frm自定义项目Add frm = new Frm自定义项目Add(UpdateType.Modify, itemid);
            frm.ShowDialog();
        }

        private void btn删除_Click(object sender, EventArgs e)
        {

        }
    }
}
