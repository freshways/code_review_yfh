﻿namespace AtomEHR.签约服务.服务包
{
    partial class Frm服务包项目
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcLeftItems = new DevExpress.XtraGrid.GridControl();
            this.gvLeftItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtLeftItem = new DevExpress.XtraEditors.TextEdit();
            this.sbtnLeftQuery = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcRightItem = new DevExpress.XtraGrid.GridControl();
            this.gvRightItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtRightItem = new DevExpress.XtraEditors.TextEdit();
            this.sbtnRightQuery = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddHisItem = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnRemoveItem = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcLeftBottom = new DevExpress.XtraGrid.GridControl();
            this.gvLeftBottom = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustomItemName = new DevExpress.XtraEditors.TextEdit();
            this.sbtnLeftBottomQuery = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddCustomItem = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcLeftItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLeftItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeftItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRightItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRightItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRightItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcLeftBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLeftBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomItemName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gcLeftItems);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Location = new System.Drawing.Point(14, 14);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(352, 240);
            this.panelControl1.TabIndex = 0;
            this.panelControl1.Text = "医疗项目";
            // 
            // gcLeftItems
            // 
            this.gcLeftItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLeftItems.Location = new System.Drawing.Point(2, 58);
            this.gcLeftItems.MainView = this.gvLeftItem;
            this.gcLeftItems.Name = "gcLeftItems";
            this.gcLeftItems.Size = new System.Drawing.Size(348, 180);
            this.gcLeftItems.TabIndex = 3;
            this.gcLeftItems.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLeftItem});
            // 
            // gvLeftItem
            // 
            this.gvLeftItem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn12,
            this.gridColumn1});
            this.gvLeftItem.GridControl = this.gcLeftItems;
            this.gvLeftItem.Name = "gvLeftItem";
            this.gvLeftItem.OptionsBehavior.Editable = false;
            this.gvLeftItem.OptionsView.ColumnAutoWidth = false;
            this.gvLeftItem.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "项目ID";
            this.gridColumn11.FieldName = "项目ID";
            this.gridColumn11.Name = "gridColumn11";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "项目名称";
            this.gridColumn2.FieldName = "项目名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "单价";
            this.gridColumn3.FieldName = "单价";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 60;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "单位";
            this.gridColumn4.FieldName = "单位";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "拼音简码";
            this.gridColumn12.FieldName = "拼音简码";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            this.gridColumn12.Width = 60;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "项目编码";
            this.gridColumn1.FieldName = "项目编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 4;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Controls.Add(this.txtLeftItem);
            this.panelControl3.Controls.Add(this.sbtnLeftQuery);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(2, 22);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(348, 36);
            this.panelControl3.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "项目：";
            // 
            // txtLeftItem
            // 
            this.txtLeftItem.Location = new System.Drawing.Point(57, 5);
            this.txtLeftItem.Name = "txtLeftItem";
            this.txtLeftItem.Size = new System.Drawing.Size(196, 20);
            this.txtLeftItem.TabIndex = 1;
            this.txtLeftItem.EditValueChanged += new System.EventHandler(this.txtLeftItem_EditValueChanged);
            // 
            // sbtnLeftQuery
            // 
            this.sbtnLeftQuery.Location = new System.Drawing.Point(259, 4);
            this.sbtnLeftQuery.Name = "sbtnLeftQuery";
            this.sbtnLeftQuery.Size = new System.Drawing.Size(87, 27);
            this.sbtnLeftQuery.TabIndex = 2;
            this.sbtnLeftQuery.Text = "查询";
            this.sbtnLeftQuery.Click += new System.EventHandler(this.sbtnLeftQuery_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gcRightItem);
            this.panelControl2.Controls.Add(this.panelControl5);
            this.panelControl2.Location = new System.Drawing.Point(427, 14);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(381, 442);
            this.panelControl2.TabIndex = 1;
            this.panelControl2.Text = "已选择项目";
            // 
            // gcRightItem
            // 
            this.gcRightItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcRightItem.Location = new System.Drawing.Point(2, 58);
            this.gcRightItem.MainView = this.gvRightItem;
            this.gcRightItem.Name = "gcRightItem";
            this.gcRightItem.Size = new System.Drawing.Size(377, 382);
            this.gcRightItem.TabIndex = 6;
            this.gcRightItem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRightItem});
            // 
            // gvRightItem
            // 
            this.gvRightItem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn21,
            this.gridColumn13,
            this.gridColumn6,
            this.gridColumn9,
            this.gridColumn8,
            this.gridColumn10,
            this.gridColumn7,
            this.gridColumn5});
            this.gvRightItem.GridControl = this.gcRightItem;
            this.gvRightItem.Name = "gvRightItem";
            this.gvRightItem.OptionsBehavior.Editable = false;
            this.gvRightItem.OptionsView.ColumnAutoWidth = false;
            this.gvRightItem.OptionsView.ShowGroupPanel = false;
            this.gvRightItem.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvRightItem_CellValueChanged);
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "ID";
            this.gridColumn14.FieldName = "ID";
            this.gridColumn14.Name = "gridColumn14";
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "项目类别";
            this.gridColumn21.FieldName = "项目类别";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "项目ID";
            this.gridColumn13.FieldName = "项目ID";
            this.gridColumn13.Name = "gridColumn13";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "项目名称";
            this.gridColumn6.FieldName = "项目名称";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "数量";
            this.gridColumn9.FieldName = "数量";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 50;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "单价";
            this.gridColumn8.FieldName = "单价";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 60;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "总价";
            this.gridColumn10.FieldName = "总价";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "单位";
            this.gridColumn7.FieldName = "单位";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "项目编码";
            this.gridColumn5.FieldName = "项目编码";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.labelControl2);
            this.panelControl5.Controls.Add(this.txtRightItem);
            this.panelControl5.Controls.Add(this.sbtnRightQuery);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(2, 22);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(377, 36);
            this.panelControl5.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 8);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "项目：";
            // 
            // txtRightItem
            // 
            this.txtRightItem.Location = new System.Drawing.Point(47, 5);
            this.txtRightItem.Name = "txtRightItem";
            this.txtRightItem.Size = new System.Drawing.Size(226, 20);
            this.txtRightItem.TabIndex = 4;
            // 
            // sbtnRightQuery
            // 
            this.sbtnRightQuery.Location = new System.Drawing.Point(280, 4);
            this.sbtnRightQuery.Name = "sbtnRightQuery";
            this.sbtnRightQuery.Size = new System.Drawing.Size(87, 27);
            this.sbtnRightQuery.TabIndex = 5;
            this.sbtnRightQuery.Text = "查询";
            this.sbtnRightQuery.Click += new System.EventHandler(this.sbtnRightQuery_Click);
            // 
            // sbtnAddHisItem
            // 
            this.sbtnAddHisItem.Location = new System.Drawing.Point(372, 142);
            this.sbtnAddHisItem.Name = "sbtnAddHisItem";
            this.sbtnAddHisItem.Size = new System.Drawing.Size(49, 27);
            this.sbtnAddHisItem.TabIndex = 2;
            this.sbtnAddHisItem.Text = "=>";
            this.sbtnAddHisItem.ToolTip = "将选中医疗项目添加到右侧列表";
            this.sbtnAddHisItem.Click += new System.EventHandler(this.sbtnAddHisItem_Click);
            // 
            // sbtnRemoveItem
            // 
            this.sbtnRemoveItem.Location = new System.Drawing.Point(372, 241);
            this.sbtnRemoveItem.Name = "sbtnRemoveItem";
            this.sbtnRemoveItem.Size = new System.Drawing.Size(49, 27);
            this.sbtnRemoveItem.TabIndex = 2;
            this.sbtnRemoveItem.Text = "<=";
            this.sbtnRemoveItem.ToolTip = "移除右侧列表中的选中项目";
            this.sbtnRemoveItem.Click += new System.EventHandler(this.sbtnRemoveItem_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gcLeftBottom);
            this.groupControl1.Controls.Add(this.panelControl4);
            this.groupControl1.Location = new System.Drawing.Point(16, 259);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(350, 236);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "自定义项目";
            // 
            // gcLeftBottom
            // 
            this.gcLeftBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLeftBottom.Location = new System.Drawing.Point(2, 58);
            this.gcLeftBottom.MainView = this.gvLeftBottom;
            this.gcLeftBottom.Name = "gcLeftBottom";
            this.gcLeftBottom.Size = new System.Drawing.Size(346, 176);
            this.gcLeftBottom.TabIndex = 6;
            this.gcLeftBottom.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLeftBottom});
            // 
            // gvLeftBottom
            // 
            this.gvLeftBottom.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20});
            this.gvLeftBottom.GridControl = this.gcLeftBottom;
            this.gvLeftBottom.Name = "gvLeftBottom";
            this.gvLeftBottom.OptionsBehavior.Editable = false;
            this.gvLeftBottom.OptionsView.ColumnAutoWidth = false;
            this.gvLeftBottom.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "项目ID";
            this.gridColumn15.FieldName = "项目ID";
            this.gridColumn15.Name = "gridColumn15";
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "项目名称";
            this.gridColumn16.FieldName = "项目名称";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            this.gridColumn16.Width = 100;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "单价";
            this.gridColumn17.FieldName = "单价";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            this.gridColumn17.Width = 60;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "单位";
            this.gridColumn18.FieldName = "单位";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 2;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "拼音简码";
            this.gridColumn19.FieldName = "拼音简码";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 3;
            this.gridColumn19.Width = 60;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "项目编码";
            this.gridColumn20.FieldName = "项目编码";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 4;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.labelControl3);
            this.panelControl4.Controls.Add(this.txtCustomItemName);
            this.panelControl4.Controls.Add(this.sbtnLeftBottomQuery);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 22);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(346, 36);
            this.panelControl4.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 8);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 14);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "项目：";
            // 
            // txtCustomItemName
            // 
            this.txtCustomItemName.Location = new System.Drawing.Point(55, 5);
            this.txtCustomItemName.Name = "txtCustomItemName";
            this.txtCustomItemName.Size = new System.Drawing.Size(196, 20);
            this.txtCustomItemName.TabIndex = 1;
            this.txtCustomItemName.EditValueChanged += new System.EventHandler(this.txtCustomItemName_EditValueChanged);
            // 
            // sbtnLeftBottomQuery
            // 
            this.sbtnLeftBottomQuery.Location = new System.Drawing.Point(257, 4);
            this.sbtnLeftBottomQuery.Name = "sbtnLeftBottomQuery";
            this.sbtnLeftBottomQuery.Size = new System.Drawing.Size(87, 27);
            this.sbtnLeftBottomQuery.TabIndex = 2;
            this.sbtnLeftBottomQuery.Text = "查询";
            this.sbtnLeftBottomQuery.Click += new System.EventHandler(this.sbtnLeftBottomQuery_Click);
            // 
            // sbtnAddCustomItem
            // 
            this.sbtnAddCustomItem.Location = new System.Drawing.Point(372, 338);
            this.sbtnAddCustomItem.Name = "sbtnAddCustomItem";
            this.sbtnAddCustomItem.Size = new System.Drawing.Size(49, 27);
            this.sbtnAddCustomItem.TabIndex = 2;
            this.sbtnAddCustomItem.Text = "=>";
            this.sbtnAddCustomItem.ToolTip = "将选中自定义项目添加到右侧列表";
            this.sbtnAddCustomItem.Click += new System.EventHandler(this.sbtnAddCustomItem_Click);
            // 
            // sbtnOK
            // 
            this.sbtnOK.Location = new System.Drawing.Point(696, 464);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(112, 31);
            this.sbtnOK.TabIndex = 4;
            this.sbtnOK.Text = "确定";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // Frm服务包项目
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 505);
            this.Controls.Add(this.sbtnOK);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.sbtnRemoveItem);
            this.Controls.Add(this.sbtnAddCustomItem);
            this.Controls.Add(this.sbtnAddHisItem);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm服务包项目";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "服务包项目设置";
            this.Load += new System.EventHandler(this.Frm服务包项目_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcLeftItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLeftItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeftItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcRightItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRightItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRightItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcLeftBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLeftBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomItemName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl panelControl1;
        private DevExpress.XtraEditors.GroupControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sbtnAddHisItem;
        private DevExpress.XtraEditors.SimpleButton sbtnRemoveItem;
        private DevExpress.XtraEditors.SimpleButton sbtnLeftQuery;
        private DevExpress.XtraEditors.TextEdit txtLeftItem;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton sbtnRightQuery;
        private DevExpress.XtraEditors.TextEdit txtRightItem;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.GridControl gcLeftItems;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLeftItem;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.GridControl gcRightItem;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRightItem;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtCustomItemName;
        private DevExpress.XtraEditors.SimpleButton sbtnLeftBottomQuery;
        private DevExpress.XtraGrid.GridControl gcLeftBottom;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLeftBottom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton sbtnAddCustomItem;
        private DevExpress.XtraEditors.SimpleButton sbtnOK;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
    }
}