﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.Common;

namespace AtomEHR.签约服务.服务包
{
    //vw_JTYS优惠方式
    public partial class Frm优惠情况 : AtomEHR.Library.frmBaseBusinessForm
    {
        public Frm优惠情况()
        {
            InitializeComponent();
        }
        protected override void InitializeForm()
        {
            _BLL = new bllJTYS优惠方式();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            BindingSummarySearchPanel(btnQuery, btnAdd, gcFindGroup);

            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                util.ControlsHelper.SetComboxData(ssjg, cbo机构);
                cbo机构.Enabled = false;
            }
        }

        protected override bool DoSearchSummary()
        {
            string _strWhere = "";

            string str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            if (!string.IsNullOrWhiteSpace(str机构))
            {
                _strWhere = " and 所属机构='" + str机构 + "' ";
            }

            //if (!string.IsNullOrWhiteSpace(cbo机构.Text))
            //{
            //    _strWhere = " and " + tb_JTYS优惠方式.对象类型 + " like '%" + cbo机构.Text.Trim() + "%' ";
            //}

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS优惠方式", "*", _strWhere, tb_JTYS优惠方式.ID, "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   
            gvSummary.BestFitColumns();

            return dt != null && dt.Rows.Count > 0;
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Frm优惠Edit frm = new Frm优惠Edit(UpdateType.Add, "");
           // Frm优惠Add frm = new Frm优惠Add(UpdateType.Add, "");
            frm.ShowDialog();
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            try
            {
                int[] selectdIndex = gvSummary.GetSelectedRows();
                if (selectdIndex.Length == 0)
                {
                    Msg.ShowInformation("请选择需要修改的行");
                    return;
                }
                else if (selectdIndex.Length > 1)
                {
                    Msg.ShowInformation("每次只能修改一条数据");
                    return;
                }

                string id = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS优惠方式.ID).ToString();
                string serobj = gvSummary.GetRowCellValue(selectdIndex[0], "签约对象名称").ToString();
                string spname = gvSummary.GetRowCellValue(selectdIndex[0], "服务包名称").ToString();

                if (!Msg.AskQuestion("确定要禁用【" + serobj + "】与【" + spname + "】间的优惠关系吗？")) return;

                int count = ((bllJTYS优惠方式)_BLL).Update是否禁用(id, false);

                if (count > 0)
                {
                    DoSearchSummary();
                }
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void Frm优惠情况_Load(object sender, EventArgs e)
        {
            this.pagerControl1.Height = 45;
            this.InitializeForm();
        }





    }
}
