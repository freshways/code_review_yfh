﻿using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.服务包
{
    public partial class Frm自定义项目Add : frmBase
    {
        bllJTYS自定义项目 bll = new bllJTYS自定义项目();
        string m_项目ID;
        UpdateType m_UpdateType;
        //DataSet m_ds项目;
        public Frm自定义项目Add(UpdateType updatetype, string itemid)
        {
            InitializeComponent();

            m_UpdateType = updatetype;
            m_项目ID = itemid;

            initForm();
        }

        void initForm()
        {
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            lookUp机构.Properties.ValueMember = "机构编号";
            lookUp机构.Properties.DisplayMember = "机构名称";
            lookUp机构.Properties.DataSource = dt所属机构;
            lookUp机构.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称"));
        }

        private void sbtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {
            if (lookUp机构.EditValue == null)
            {
                MessageBox.Show("请选择所属机构！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                MessageBox.Show("请填写项目名称！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrWhiteSpace(txtUnit.Text))
            {
                MessageBox.Show("请填写单位！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrWhiteSpace(numPrice.Text))
            {
                MessageBox.Show("请填写单价！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            if (m_UpdateType == UpdateType.Add)
            {
                bll.GetBusinessByKey("-1", true);
                bll.NewBusiness();
                //保存概要信息
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.所属机构] = lookUp机构.EditValue.ToString();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.项目名称] = txtName.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.拼音简码] = DESEncrypt.GetChineseSpell(this.txtName.Text.Trim()); ;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.单位] = txtUnit.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.单价] = numPrice.Value;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.检验项目] = chk检验项目.Checked;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.检查项目] = chk检查项目.Checked;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.项目分组] = txt项目类别.Text.Trim();
                bll.Save(bll.CurrentBusiness);
            }
            else if(m_UpdateType == UpdateType.Modify)
            {
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.拼音简码] = DESEncrypt.GetChineseSpell(this.txtName.Text.Trim()); ;
                //bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.修改人] = Loginer.CurrentUser.用户编码;
                //bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS自定义项目.修改时间] = bll.ServiceDateTime;
                DataSet dsTemplate = bll.CreateSaveData(bll.CurrentBusiness, m_UpdateType); //创建用于保存的临时数据
                bll.Save(dsTemplate);//调用业务逻辑保存数据方法
            }
            //bll.Save(bll.CurrentBusiness);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;

        }

        private void Frm自定义项目Add_Load(object sender, EventArgs e)
        {
            if (m_UpdateType == UpdateType.Add)
            {
                // TODO: 需要修改机构编号的地方
                // TODO: It is even possible to add comments at the end.
                //lookUp机构.EditValue = "371321C21008";
                string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                lookUp机构.EditValue = ssjg;
                if (ssjg.Length > 6)
                {
                    lookUp机构.Enabled = false;
                }
            }
            else if(m_UpdateType == UpdateType.Modify)
            {
                lookUp机构.Enabled = false;
                bll.GetBusinessByKey(m_项目ID, true);

                //txtName.Text = m_ds项目.Tables[0].Rows[0][tb_JTYS服务包.名称].ToString();
                //txtUnit.Text = m_ds项目.Tables[0].Rows[0][tb_JTYS服务包.适宜对象].ToString();
                //numPrice.Value = Convert.ToDecimal(m_ds项目.Tables[0].Rows[0][tb_JTYS服务包.年收费标准]);

                DataBinder.BindingTextEdit(txtName, bll.CurrentBusiness.Tables[0], tb_JTYS自定义项目.项目名称);
                DataBinder.BindingTextEdit(lookUp机构, bll.CurrentBusiness.Tables[0], tb_JTYS自定义项目.所属机构);
                DataBinder.BindingTextEdit(txtUnit, bll.CurrentBusiness.Tables[0], tb_JTYS自定义项目.单位);
                DataBinder.BindingNumericUpDown(numPrice, bll.CurrentBusiness.Tables[0], tb_JTYS自定义项目.单价);
                DataBinder.BindingCheckEdit(chk检验项目, bll.CurrentBusiness.Tables[0], tb_JTYS自定义项目.检验项目);
                DataBinder.BindingCheckEdit(chk检查项目, bll.CurrentBusiness.Tables[0], tb_JTYS自定义项目.检查项目);
                DataBinder.BindingTextEdit(txt项目类别, bll.CurrentBusiness.Tables[0], tb_JTYS自定义项目.项目分组);
            }
        }

        private void chk检查项目_CheckedChanged(object sender, EventArgs e)
        {
            if(chk检查项目.Checked)
            {
                chk检验项目.Checked = false;
            }
        }

        private void chk检验项目_CheckedChanged(object sender, EventArgs e)
        {
            if (chk检验项目.Checked)
            {
                chk检查项目.Checked = false;
            }
        }
    }
}
