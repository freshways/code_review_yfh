﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.服务包
{
    public partial class FrmItemCountSetting : frmBase
    {
        public int count = 1;
        public FrmItemCountSetting()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            count = Convert.ToInt32(txtCount.EditValue);
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void sbtnSubOne_Click(object sender, EventArgs e)
        {
            int tempcount = Convert.ToInt32(txtCount.EditValue);
            if(tempcount ==1)
            {
                //do nothing
            }
            else
            {
                txtCount.EditValue = tempcount - 1;
            }
        }

        private void sbtnAddOne_Click(object sender, EventArgs e)
        {
            int tempcount = Convert.ToInt32(txtCount.EditValue);
            txtCount.EditValue = tempcount + 1;
        }
    }
}
