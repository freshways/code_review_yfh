﻿namespace AtomEHR.签约服务.服务包
{
    partial class Frm自定义项目
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm自定义项目));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.cbo机构 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tpSummary.Size = new System.Drawing.Size(1105, 655);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 33);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.pnlSummary.Size = new System.Drawing.Size(1111, 661);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tcBusiness.Size = new System.Drawing.Size(1111, 661);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tpDetail.Size = new System.Drawing.Size(1219, 987);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.gcNavigator.Padding = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.gcNavigator.Size = new System.Drawing.Size(1111, 33);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(879, 5);
            this.controlNavigatorSummary.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.controlNavigatorSummary.Size = new System.Drawing.Size(230, 23);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(622, 5);
            this.lblAboutInfo.Size = new System.Drawing.Size(257, 23);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtName);
            this.layoutControl1.Controls.Add(this.cbo机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(447, 23, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(1101, 82);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(436, 34);
            this.txtName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(172, 24);
            this.txtName.StyleController = this.layoutControl1;
            this.txtName.TabIndex = 138;
            // 
            // cbo机构
            // 
            this.cbo机构.Location = new System.Drawing.Point(75, 34);
            this.cbo机构.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbo机构.Name = "cbo机构";
            this.cbo机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo机构.Properties.NullText = "[EditValue is null]";
            this.cbo机构.Properties.PopupSizeable = true;
            this.cbo机构.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo机构.Size = new System.Drawing.Size(281, 24);
            this.cbo机构.StyleController = this.layoutControl1;
            this.cbo机构.TabIndex = 137;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 1;
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1101, 82);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "查询条件";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem1,
            this.emptySpaceItem1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup2";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1097, 78);
            this.layoutControlGroup3.Text = "查询条件";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem16.Control = this.cbo机构;
            this.layoutControlItem16.CustomizationFormText = "所属机构：";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(350, 42);
            this.layoutControlItem16.Text = "所属机构:";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtName;
            this.layoutControlItem1.CustomizationFormText = "服务包名称：";
            this.layoutControlItem1.Location = new System.Drawing.Point(350, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(252, 42);
            this.layoutControlItem1.Text = "项目名称：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(75, 18);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(602, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(483, 42);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 125);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(1105, 480);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 5;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 30;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn2,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn13,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn4,
            this.gridColumn11});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "ServiceID";
            this.gridColumn12.FieldName = "ServiceID";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "项目名称";
            this.gridColumn1.FieldName = "项目名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "单位";
            this.gridColumn3.FieldName = "单位";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "单价";
            this.gridColumn2.FieldName = "单价";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "检验项目";
            this.gridColumn5.FieldName = "检验项目";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "检查项目";
            this.gridColumn6.FieldName = "检查项目";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "项目类别";
            this.gridColumn7.FieldName = "项目分组";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "是否有效";
            this.gridColumn13.FieldName = "是否有效";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 6;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "创建人";
            this.gridColumn9.FieldName = "创建人姓名";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "创建时间";
            this.gridColumn10.FieldName = "创建时间";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 8;
            this.gridColumn10.Width = 120;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "修改人";
            this.gridColumn4.FieldName = "修改人姓名";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 9;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "修改时间";
            this.gridColumn11.FieldName = "修改时间";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 10;
            this.gridColumn11.Width = 120;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 605);
            this.pagerControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 10780);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 15;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(1105, 50);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 129;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.layoutControl1);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(1105, 86);
            this.gcFindGroup.TabIndex = 130;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btnEmpty);
            this.flowLayoutPanel1.Controls.Add(this.btnAdd);
            this.flowLayoutPanel1.Controls.Add(this.sbtnEdit);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 86);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(114, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1105, 39);
            this.flowLayoutPanel1.TabIndex = 131;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(117, 4);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnQuery.MinimumSize = new System.Drawing.Size(86, 28);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(86, 28);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(209, 4);
            this.btnEmpty.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEmpty.MinimumSize = new System.Drawing.Size(86, 28);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(86, 28);
            this.btnEmpty.TabIndex = 6;
            this.btnEmpty.Text = "重置";
            this.btnEmpty.Visible = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(301, 4);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAdd.MinimumSize = new System.Drawing.Size(86, 28);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(86, 28);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "添加";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.Image = ((System.Drawing.Image)(resources.GetObject("sbtnEdit.Image")));
            this.sbtnEdit.Location = new System.Drawing.Point(393, 4);
            this.sbtnEdit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sbtnEdit.MinimumSize = new System.Drawing.Size(86, 28);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(86, 28);
            this.sbtnEdit.TabIndex = 7;
            this.sbtnEdit.Text = "修改";
            this.sbtnEdit.Click += new System.EventHandler(this.sbtnEdit_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(485, 4);
            this.btn删除.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn删除.MinimumSize = new System.Drawing.Size(86, 28);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(86, 28);
            this.btn删除.TabIndex = 3;
            this.btn删除.Text = "禁用";
            this.btn删除.Visible = false;
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(577, 4);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(97, 30);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "详细信息";
            this.simpleButton1.Visible = false;
            // 
            // Frm自定义项目
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1111, 694);
            this.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.Name = "Frm自定义项目";
            this.Text = "自定义项目管理";
            this.Load += new System.EventHandler(this.Frm自定义项目_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private TActionProject.PagerControl pagerControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.ComboBoxEdit cbo机构;
        private DevExpress.XtraEditors.SimpleButton sbtnEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
    }
}