﻿namespace AtomEHR.签约服务.服务包
{
    partial class Frm服务包医疗项目
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcLeftItems = new DevExpress.XtraGrid.GridControl();
            this.gvLeftItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sbtnLeftQuery = new DevExpress.XtraEditors.SimpleButton();
            this.txtLeftItem = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcRightItem = new DevExpress.XtraGrid.GridControl();
            this.gvRightItem = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sbtnRightQuery = new DevExpress.XtraEditors.SimpleButton();
            this.txtRightItem = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.sbtnAddItem = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnRemoveItem = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcLeftItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLeftItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeftItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRightItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRightItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRightItem.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gcLeftItems);
            this.panelControl1.Controls.Add(this.sbtnLeftQuery);
            this.panelControl1.Controls.Add(this.txtLeftItem);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Location = new System.Drawing.Point(14, 14);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(352, 481);
            this.panelControl1.TabIndex = 0;
            this.panelControl1.Text = "医疗项目";
            // 
            // gcLeftItems
            // 
            this.gcLeftItems.Location = new System.Drawing.Point(9, 64);
            this.gcLeftItems.MainView = this.gvLeftItem;
            this.gcLeftItems.Name = "gcLeftItems";
            this.gcLeftItems.Size = new System.Drawing.Size(338, 410);
            this.gcLeftItems.TabIndex = 3;
            this.gcLeftItems.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLeftItem});
            // 
            // gvLeftItem
            // 
            this.gvLeftItem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn12,
            this.gridColumn1});
            this.gvLeftItem.GridControl = this.gcLeftItems;
            this.gvLeftItem.Name = "gvLeftItem";
            this.gvLeftItem.OptionsView.ColumnAutoWidth = false;
            this.gvLeftItem.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "项目ID";
            this.gridColumn11.FieldName = "项目ID";
            this.gridColumn11.Name = "gridColumn11";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "项目编码";
            this.gridColumn1.FieldName = "项目编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 4;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "项目名称";
            this.gridColumn2.FieldName = "项目名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "单价";
            this.gridColumn3.FieldName = "单价";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 60;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "单位";
            this.gridColumn4.FieldName = "单位";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "拼音简码";
            this.gridColumn12.FieldName = "拼音简码";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            this.gridColumn12.Width = 60;
            // 
            // sbtnLeftQuery
            // 
            this.sbtnLeftQuery.Location = new System.Drawing.Point(260, 31);
            this.sbtnLeftQuery.Name = "sbtnLeftQuery";
            this.sbtnLeftQuery.Size = new System.Drawing.Size(87, 27);
            this.sbtnLeftQuery.TabIndex = 2;
            this.sbtnLeftQuery.Text = "查询";
            this.sbtnLeftQuery.Click += new System.EventHandler(this.sbtnLeftQuery_Click);
            // 
            // txtLeftItem
            // 
            this.txtLeftItem.Location = new System.Drawing.Point(58, 32);
            this.txtLeftItem.Name = "txtLeftItem";
            this.txtLeftItem.Size = new System.Drawing.Size(196, 20);
            this.txtLeftItem.TabIndex = 1;
            this.txtLeftItem.EditValueChanged += new System.EventHandler(this.txtLeftItem_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(16, 35);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "项目：";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gcRightItem);
            this.panelControl2.Controls.Add(this.sbtnRightQuery);
            this.panelControl2.Controls.Add(this.txtRightItem);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Location = new System.Drawing.Point(427, 14);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(381, 481);
            this.panelControl2.TabIndex = 1;
            this.panelControl2.Text = "已选择项目";
            // 
            // gcRightItem
            // 
            this.gcRightItem.Location = new System.Drawing.Point(6, 64);
            this.gcRightItem.MainView = this.gvRightItem;
            this.gcRightItem.Name = "gcRightItem";
            this.gcRightItem.Size = new System.Drawing.Size(370, 410);
            this.gcRightItem.TabIndex = 6;
            this.gcRightItem.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRightItem});
            // 
            // gvRightItem
            // 
            this.gvRightItem.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn13,
            this.gridColumn6,
            this.gridColumn9,
            this.gridColumn8,
            this.gridColumn10,
            this.gridColumn7,
            this.gridColumn5});
            this.gvRightItem.GridControl = this.gcRightItem;
            this.gvRightItem.Name = "gvRightItem";
            this.gvRightItem.OptionsView.ColumnAutoWidth = false;
            this.gvRightItem.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "ID";
            this.gridColumn14.FieldName = "ID";
            this.gridColumn14.Name = "gridColumn14";
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "项目ID";
            this.gridColumn13.FieldName = "项目ID";
            this.gridColumn13.Name = "gridColumn13";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "项目编码";
            this.gridColumn5.FieldName = "项目编码";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "项目名称";
            this.gridColumn6.FieldName = "项目名称";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "数量";
            this.gridColumn9.FieldName = "数量";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 50;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "单位";
            this.gridColumn7.FieldName = "单位";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "单价";
            this.gridColumn8.FieldName = "单价";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            this.gridColumn8.Width = 60;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "总价";
            this.gridColumn10.FieldName = "总价";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 5;
            // 
            // sbtnRightQuery
            // 
            this.sbtnRightQuery.Location = new System.Drawing.Point(288, 31);
            this.sbtnRightQuery.Name = "sbtnRightQuery";
            this.sbtnRightQuery.Size = new System.Drawing.Size(87, 27);
            this.sbtnRightQuery.TabIndex = 5;
            this.sbtnRightQuery.Text = "查询";
            this.sbtnRightQuery.Click += new System.EventHandler(this.sbtnRightQuery_Click);
            // 
            // txtRightItem
            // 
            this.txtRightItem.Location = new System.Drawing.Point(55, 32);
            this.txtRightItem.Name = "txtRightItem";
            this.txtRightItem.Size = new System.Drawing.Size(226, 20);
            this.txtRightItem.TabIndex = 4;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 35);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "项目：";
            // 
            // sbtnAddItem
            // 
            this.sbtnAddItem.Location = new System.Drawing.Point(372, 170);
            this.sbtnAddItem.Name = "sbtnAddItem";
            this.sbtnAddItem.Size = new System.Drawing.Size(49, 27);
            this.sbtnAddItem.TabIndex = 2;
            this.sbtnAddItem.Text = "=>";
            this.sbtnAddItem.Click += new System.EventHandler(this.sbtnAddItem_Click);
            // 
            // sbtnRemoveItem
            // 
            this.sbtnRemoveItem.Location = new System.Drawing.Point(372, 259);
            this.sbtnRemoveItem.Name = "sbtnRemoveItem";
            this.sbtnRemoveItem.Size = new System.Drawing.Size(49, 27);
            this.sbtnRemoveItem.TabIndex = 2;
            this.sbtnRemoveItem.Text = "<=";
            this.sbtnRemoveItem.Click += new System.EventHandler(this.sbtnRemoveItem_Click);
            // 
            // Frm服务包医疗项目
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 505);
            this.Controls.Add(this.sbtnRemoveItem);
            this.Controls.Add(this.sbtnAddItem);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm服务包医疗项目";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "服务包医疗项目设置";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcLeftItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLeftItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLeftItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRightItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRightItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRightItem.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl panelControl1;
        private DevExpress.XtraEditors.GroupControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sbtnAddItem;
        private DevExpress.XtraEditors.SimpleButton sbtnRemoveItem;
        private DevExpress.XtraEditors.SimpleButton sbtnLeftQuery;
        private DevExpress.XtraEditors.TextEdit txtLeftItem;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton sbtnRightQuery;
        private DevExpress.XtraEditors.TextEdit txtRightItem;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.GridControl gcLeftItems;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLeftItem;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.GridControl gcRightItem;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRightItem;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
    }
}