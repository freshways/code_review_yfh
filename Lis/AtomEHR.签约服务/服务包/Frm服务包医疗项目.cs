﻿using AtomEHR.Library;
using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.服务包
{
    public partial class Frm服务包医疗项目 : frmBase
    {
        string m_packageID;
        DataTable dtItems;
        bllJTYS服务包医疗项目 bll项目 = new bllJTYS服务包医疗项目();
        public Frm服务包医疗项目(DataTable dt, string packageID)
        {
            InitializeComponent();

            m_packageID = packageID;
            dtItems = dt;
            this.gcRightItem.DataSource = dtItems;
        }

        private void sbtnAddItem_Click(object sender, EventArgs e)
        {
            int[] indexs = gvLeftItem.GetSelectedRows();
            if (indexs.Length == 0)
            {
                Msg.ShowInformation("请选择需要添加的项目");
                return;
            }
            else if (indexs.Length > 1)
            {
                Msg.ShowInformation("一次只能添加一个项目");
                return;
            }

            string str项目ID = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目ID).ToString();
            DataRow[] drs = dtItems.Select(tb_JTYS服务包医疗项目.项目ID + "='" + str项目ID + "'");
            if (drs.Length > 0)
            {
                Msg.ShowInformation("项目已经存在列表中，请确认");
                return;
            }

            FrmItemCountSetting frm = new FrmItemCountSetting();
            frm.ShowDialog();
            int count = frm.count;
            frm.Dispose();

            string str项目编码 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目编码).ToString();
            string str项目名称 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目名称).ToString();
            string str拼音简码 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.拼音简码).ToString();
            string str单价 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.单价).ToString();
            string str单位 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.单位).ToString();

            DataRow newdr = dtItems.Rows.Add();
            newdr[tb_JTYS服务包医疗项目.ServiceID] = m_packageID;
            newdr[tb_JTYS服务包医疗项目.项目ID] = str项目ID;
            newdr[tb_JTYS服务包医疗项目.项目编码] = str项目编码;
            newdr[tb_JTYS服务包医疗项目.项目名称] = str项目名称;
            newdr[tb_JTYS服务包医疗项目.拼音简码] = str拼音简码;
            newdr[tb_JTYS服务包医疗项目.单价] = str单价;
            newdr[tb_JTYS服务包医疗项目.单位] = str单位;
            newdr[tb_JTYS服务包医疗项目.总价] = count * Convert.ToDecimal(str单价);
            newdr[tb_JTYS服务包医疗项目.创建人] = Loginer.CurrentUser.用户编码;
            newdr[tb_JTYS服务包医疗项目.创建时间] = bll项目.ServiceDateTime;
            newdr[tb_JTYS服务包医疗项目.修改人] = Loginer.CurrentUser.用户编码;
            newdr[tb_JTYS服务包医疗项目.修改时间] = newdr[tb_JTYS服务包医疗项目.创建时间];
        }

        private void sbtnRemoveItem_Click(object sender, EventArgs e)
        {
            int[] indexs = gvRightItem.GetSelectedRows();
            if (indexs.Length==0 )
            {
                Msg.ShowInformation("请选择需要删除的项目");
                return;
            }
            else if (indexs.Length > 1)
            {
                Msg.ShowInformation("一次只能删除一个项目");
                return;
            }

            //string chargeid = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目ID).ToString();
            gvLeftItem.DeleteSelectedRows();
        }

        private void sbtnLeftQuery_Click(object sender, EventArgs e)
        {
            QueryLeft();
        }

        private void txtLeftItem_EditValueChanged(object sender, EventArgs e)
        {
            QueryLeft();
        }

        void QueryLeft()
        {
            DataTable dt = bll项目.GetHISItemList(txtLeftItem.Text.Trim());
            this.gcLeftItems.DataSource = dt;
        }

        private void sbtnRightQuery_Click(object sender, EventArgs e)
        {

        }

    }
}
