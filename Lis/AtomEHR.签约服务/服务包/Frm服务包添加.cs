﻿using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.服务包
{
    public partial class Frm服务包添加 : frmBase
    {
        bllJTYS服务包 bllServicePackage = new bllJTYS服务包();
        bllJTYS服务包医疗项目 bllServicePackageItems = new bllJTYS服务包医疗项目();
        
        string m_packageID;
        UpdateType m_UpdateType;
        DataSet m_dsServicePackage;
        
        //明细
        DataTable dtServiceItems;

        bool m_onlyShow=false;

        public Frm服务包添加(UpdateType updatetype, string packageid, bool onlyshow=false)
        {
            InitializeComponent();

            m_UpdateType = updatetype;
            m_packageID = packageid;

            m_onlyShow = onlyshow;

            initForm();
        }

        void initForm()
        {

            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            
            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            lookUp机构.Properties.ValueMember = "机构编号";
            lookUp机构.Properties.DisplayMember = "机构名称";
            lookUp机构.Properties.DataSource = dt所属机构;
            lookUp机构.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称"));
            //lookUp机构.EditValue = "371321C21008";
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == tb_JTYS服务包医疗项目.数量)
            {
                decimal PriceValue = Convert.ToDecimal(gridView1.GetRowCellValue(e.RowHandle, tb_JTYS服务包医疗项目.单价));
                decimal NoValue = Convert.ToDecimal(gridView1.GetRowCellValue(e.RowHandle, tb_JTYS服务包医疗项目.数量));
                decimal newTotalValue = PriceValue * NoValue;
                gridView1.SetRowCellValue(e.RowHandle, tb_JTYS服务包医疗项目.总价, newTotalValue);
            }
        }

        private void sbtnItemEdit_Click(object sender, EventArgs e)
        {
            Frm服务包项目 frm = new Frm服务包项目(dtServiceItems, m_packageID);
            frm.ShowDialog();
            gridView1.BestFitColumns();
        }

        private void sbtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {
            if (lookUp机构.EditValue == null)
            {
                MessageBox.Show("请选择服务包所属机构！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                MessageBox.Show("请填写服务包名称！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrWhiteSpace(txtServiceObject.Text))
            {
                MessageBox.Show("请填写适宜对象！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrWhiteSpace(numPrice.Text))
            {
                MessageBox.Show("请填写年收费标准！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrWhiteSpace(memoServiceContent.Text))
            {
                MessageBox.Show("请填写服务内容！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            
            

            //string ServiceID = bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.ServiceID].ToString();

            //保存明细
            string opdatetime = bllServicePackageItems.ServiceDateTime;
            for (int index = 0; index < dtServiceItems.Rows.Count; index++ )
            {
                if(dtServiceItems.Rows[index].RowState == DataRowState.Added)
                {
                    //dtServiceItems.Rows[index][tb_JTYS服务包医疗项目.ServiceID] = m_packageID;
                    dtServiceItems.Rows[index][tb_JTYS服务包医疗项目.创建人] = Loginer.CurrentUser.用户编码;
                    dtServiceItems.Rows[index][tb_JTYS服务包医疗项目.创建时间] = opdatetime;
                    dtServiceItems.Rows[index][tb_JTYS服务包医疗项目.修改人] = Loginer.CurrentUser.用户编码;
                    dtServiceItems.Rows[index][tb_JTYS服务包医疗项目.修改时间] = opdatetime;
                }
                else if (dtServiceItems.Rows[index].RowState == DataRowState.Modified)
                {
                    dtServiceItems.Rows[index][tb_JTYS服务包医疗项目.修改人] = Loginer.CurrentUser.用户编码;
                    dtServiceItems.Rows[index][tb_JTYS服务包医疗项目.修改时间] = opdatetime;
                }
            }
            SaveResult itemsaveresult = bllServicePackageItems.Save(dtServiceItems.DataSet);

            if (itemsaveresult.Success)
            {
                if (m_UpdateType == UpdateType.Add)
                {
                    bllServicePackage.GetBusinessByKey("-1", true);
                    bllServicePackage.NewBusiness();
                    //保存概要信息
                    bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.ServiceID] = m_packageID;
                    bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.所属机构] = lookUp机构.EditValue.ToString();
                    bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.名称] = txtName.Text.Trim();
                    bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.适宜对象] = txtServiceObject.Text.Trim();
                    bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.年收费标准] = numPrice.Value;
                    bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.服务内容] = memoServiceContent.Text;
                    bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.卫生院职责] = memoHospContent.Text;
                    bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.卫生室职责] = memoClinicContent.Text;
                    if (rgOnceMore.EditValue == null)
                    {
                        bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.允许一年多次] = 0;
                    }
                    else
                    {
                        bllServicePackage.CurrentBusiness.Tables[0].Rows[0][tb_JTYS服务包.允许一年多次] = Convert.ToInt32(rgOnceMore.EditValue);
                    }

                    bllServicePackage.Save(bllServicePackage.CurrentBusiness);
                }
                else
                {
                    m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.名称] = txtName.Text.Trim();
                    m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.适宜对象] = txtServiceObject.Text.Trim();
                    m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.年收费标准] = numPrice.Value;
                    m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.服务内容] = memoServiceContent.Text;
                    m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.卫生院职责] = memoHospContent.Text;
                    m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.卫生室职责] = memoClinicContent.Text;
                    if (rgOnceMore.EditValue==null)
                    {
                        m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.允许一年多次] = 0;
                    }
                    else
                    {
                        m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.允许一年多次] = Convert.ToInt32(rgOnceMore.EditValue);
                    }
                    
                    bllServicePackage.Save(m_dsServicePackage);
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                Msg.ShowError("医疗项目信息保存失败");
            }
        }

        private void Frm服务包添加_Load(object sender, EventArgs e)
        {
            if (m_UpdateType == UpdateType.Add)
            {
                m_packageID = Guid.NewGuid().ToString().Replace("-", "");

                //TODO:  需要修改机构编号的地方
                //lookUp机构.EditValue = "371321C21008";
                string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                lookUp机构.EditValue = ssjg;
                if(ssjg.Length >6)
                {
                    lookUp机构.Enabled = false;
                }
            }
            else if(m_UpdateType == UpdateType.Modify)
            {
                lookUp机构.Enabled = false;
                
                m_dsServicePackage = bllServicePackage.GetBusinessByServiceID(m_packageID);

                lookUp机构.EditValue = m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.所属机构].ToString();
                txtName.Text = m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.名称].ToString();
                txtServiceObject.Text = m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.适宜对象].ToString();
                numPrice.Value = Convert.ToDecimal(m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.年收费标准]);
                memoServiceContent.Text = m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.服务内容].ToString();
                memoHospContent.Text = m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.卫生院职责].ToString();
                memoClinicContent.Text = m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.卫生室职责].ToString();
                rgOnceMore.EditValue = Convert.ToInt32(m_dsServicePackage.Tables[0].Rows[0][tb_JTYS服务包.允许一年多次].ToString());
            }
            dtServiceItems = bllServicePackageItems.GetValidateBusinessByPackageID(m_packageID).Tables[0];

            DataView dvItems = dtServiceItems.DefaultView;
            dvItems.RowFilter = tb_JTYS服务包医疗项目.是否有效+"=1";
            this.gridControlServiceItems.DataSource = dvItems;
            gridView1.BestFitColumns();

            if(m_onlyShow)
            {
                sbtnServiceItemEdit.Visible = false;
                sbtnSave.Visible = false;
                gridView1.OptionsBehavior.Editable = false;
            }
        }
    }
}
