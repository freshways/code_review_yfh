﻿namespace AtomEHR.签约服务.服务包
{
    partial class FrmItemCountSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.txtCount = new DevExpress.XtraEditors.TextEdit();
            this.sbtnSubOne = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddOne = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtCount.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "数量：";
            // 
            // sbtnOK
            // 
            this.sbtnOK.Location = new System.Drawing.Point(141, 98);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(75, 23);
            this.sbtnOK.TabIndex = 3;
            this.sbtnOK.Text = "确定";
            this.sbtnOK.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtCount
            // 
            this.txtCount.EditValue = "1";
            this.txtCount.Location = new System.Drawing.Point(103, 37);
            this.txtCount.Name = "txtCount";
            this.txtCount.Properties.Mask.BeepOnError = true;
            this.txtCount.Properties.Mask.EditMask = "d";
            this.txtCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCount.Size = new System.Drawing.Size(73, 20);
            this.txtCount.TabIndex = 4;
            // 
            // sbtnSubOne
            // 
            this.sbtnSubOne.Location = new System.Drawing.Point(63, 36);
            this.sbtnSubOne.Name = "sbtnSubOne";
            this.sbtnSubOne.Size = new System.Drawing.Size(34, 23);
            this.sbtnSubOne.TabIndex = 5;
            this.sbtnSubOne.Text = "<";
            this.sbtnSubOne.Click += new System.EventHandler(this.sbtnSubOne_Click);
            // 
            // sbtnAddOne
            // 
            this.sbtnAddOne.Location = new System.Drawing.Point(182, 36);
            this.sbtnAddOne.Name = "sbtnAddOne";
            this.sbtnAddOne.Size = new System.Drawing.Size(34, 23);
            this.sbtnAddOne.TabIndex = 6;
            this.sbtnAddOne.Text = ">";
            this.sbtnAddOne.Click += new System.EventHandler(this.sbtnAddOne_Click);
            // 
            // FrmItemCountSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 146);
            this.Controls.Add(this.sbtnAddOne);
            this.Controls.Add(this.sbtnSubOne);
            this.Controls.Add(this.txtCount);
            this.Controls.Add(this.sbtnOK);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmItemCountSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "数量设置";
            ((System.ComponentModel.ISupportInitialize)(this.txtCount.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton sbtnOK;
        private DevExpress.XtraEditors.TextEdit txtCount;
        private DevExpress.XtraEditors.SimpleButton sbtnSubOne;
        private DevExpress.XtraEditors.SimpleButton sbtnAddOne;
    }
}