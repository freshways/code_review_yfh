﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.Common;
using WEISHENG.COMM.PluginsAttribute;
using AtomEHR.Bridge;
using AtomEHR.Business.Security;
using AtomEHR.Interfaces;
using AtomEHR.Core;

namespace AtomEHR.签约服务.服务包
{
    [ClassInfoMarkAttribute(GUID = "98947E48-3B5A-44C1-B8DF-528D0A175293", 父ID = "eb62e99a-c72e-4183-abef-0025f4e90b5e", 键ID = "98947E48-3B5A-44C1-B8DF-528D0A175293", 菜单类型 = "WinForm", 功能名称 = "服务包管理", 程序集名称 = "AtomEHR.签约服务", 程序集调用类地址 = "服务包.Frm服务包管理", 全屏打开 = true)]
    public partial class Frm服务包管理 : AtomEHR.Library.frmBaseBusinessForm
    {
        public Frm服务包管理()
        {
            InitializeComponent();
        }

        private ILoginAuthorization _CurrentAuthorization = null;//当前授权模式

        private void Frm服务包管理_Load(object sender, EventArgs e)
        {
			this.InitializeForm();

            if (string.IsNullOrEmpty(Loginer.CurrentUser.Account))
            {
                try
                {
                    SystemConfig.ReadSettings(); //读取用户自定义设置

                    if (!BridgeFactory.InitializeBridge())
                    {
                        Msg.Warning("初始化失败!");
                    }

                    //第二步： 获得此身份证号对应的用户名密码，初始化登陆参数。
                    string userID = "rxy";
                    string password = CEncoder.Encode("888888");
                    string dataSetID = "YSDB"; //txtDataset.EditValue.ToString();//帐套编号
                    string dataSetDB = "AtomEHR.YSDB";

                    LoginUser loginUser = new LoginUser(userID, password, dataSetID, dataSetDB, "HIS1.0", "");
                    _CurrentAuthorization = new LoginSystemAuth();
                    //第三步： 调用登陆验证方法。
                    if (_CurrentAuthorization.Login(loginUser)) //调用登录策略
                    {
                        SystemAuthentication.Current = _CurrentAuthorization; //授权成功, 保存当前授权模式

                    }
                }
                catch (Exception ex)
                {
                    Msg.Warning("登录失败!失败原因:\n" + ex.Message);
                }
            }
        }
		
		protected override void InitializeForm()
        {
            _BLL = new bllJTYS服务包();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            //base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //util.BindingHelper.LookupEditBindDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, lookUpEdit性别, "P_CODE", "P_DESC");

            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";

            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;
            }
        }
        
        protected override bool DoSearchSummary()
        {
            string _strWhere = "";//" and " + tb_JTYS医生信息.是否有效 + "=1";

            string str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            if (!string.IsNullOrWhiteSpace(str机构))
            {
                _strWhere = " and " + tb_JTYS服务包.所属机构 + "='" + str机构 + "' ";
            }

            if (!string.IsNullOrWhiteSpace(txtName.Text))
            {
                _strWhere = " and " + tb_JTYS服务包.名称 + " like '%" + txtName.Text.Trim() + "%' ";
            }

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS服务包", "*", _strWhere, tb_JTYS服务包.ID, "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Frm服务包添加 frm = new Frm服务包添加(UpdateType.Add, "");
            frm.ShowDialog();
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }

            string serviceID = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS服务包.ServiceID).ToString();
            Frm服务包添加 frm = new Frm服务包添加(UpdateType.Modify, serviceID);
            frm.ShowDialog();
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }
            try
            {
                string serviceID = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS服务包.ServiceID).ToString();
                string serName = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS服务包.名称).ToString();

                if (!Msg.AskQuestion("确定要禁用服务包【" + serName + "】吗？")) return;

                int count = ((bllJTYS服务包)_BLL).Update是否有效(serviceID, false);

                if(count>0)
                {
                    DoSearchSummary();
                }
            }
            catch(Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }

            string serviceID = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS服务包.ServiceID).ToString();
            Frm服务包添加 frm = new Frm服务包添加(UpdateType.Modify, serviceID, true);
            frm.ShowDialog();
        }
    }
}
