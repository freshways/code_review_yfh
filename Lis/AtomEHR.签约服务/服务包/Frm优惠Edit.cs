﻿using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.服务包
{
    public partial class Frm优惠Edit : frmBase
    {
        bllJTYS优惠方式 bll = new bllJTYS优惠方式();
        string m_项目ID;
        UpdateType m_UpdateType;

        public Frm优惠Edit(UpdateType updatetype, string itemid)
        {
            InitializeComponent();

            m_UpdateType = updatetype;
            m_项目ID = itemid;

            initForm();
        }
        bllJTYS签约对象类型 bll签约对象 = new bllJTYS签约对象类型();
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        void initForm()
        {
            //签约对象设置数据源
            DataTable dt签约对象 = bll签约对象.GetAllValidateData();
            //util.ControlsHelper.BindComboxData(dt签约对象, cboServiceObject, tb_JTYS签约对象类型.ID, tb_JTYS签约对象类型.类型名称);
            lueServiceObject.Properties.ValueMember = tb_JTYS签约对象类型.ID;
            lueServiceObject.Properties.DisplayMember = tb_JTYS签约对象类型.类型名称;
            lueServiceObject.Properties.DataSource = dt签约对象;

            //服务包设置数据源
            DataTable dt服务包 = bll服务包.GetAllValidateDataByRgid();
            lue服务包.Properties.ValueMember = tb_JTYS服务包.ServiceID;
            lue服务包.Properties.DisplayMember =tb_JTYS服务包.名称;
            lue服务包.Properties.DataSource = dt服务包;
            lue服务包.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS服务包.名称));
            lue服务包.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS服务包.年收费标准));
        }

        private void rgp优惠方式_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rgp优惠方式.EditValue != null && rgp优惠方式.EditValue.ToString() != "折扣优惠")
            {
                num折扣额度.Enabled = false;
                num折扣额度.Value = 0m;

                num定额优惠.Enabled = true;
            }
            else if (rgp优惠方式.EditValue != null && rgp优惠方式.EditValue.ToString() != "定额优惠")
            {
                num定额优惠.Enabled = false;
                num定额优惠.Value = 0m;

                num折扣额度.Enabled = true;
            }
            else
            {
                num折扣额度.Enabled = false;
                num折扣额度.Value = 0m;
                num定额优惠.Enabled = false;
                num定额优惠.Value = 0m;
            }
        }

        private void sbtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {  
            Frm优惠情况 frm优惠情况 = new Frm优惠情况();

            
            if (lueServiceObject.EditValue==null && string.IsNullOrWhiteSpace(lueServiceObject.EditValue.ToString()))
            {
                MessageBox.Show("请选择签约对象类型！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (lue服务包.EditValue==null && string.IsNullOrWhiteSpace(lue服务包.EditValue.ToString()))
            {
                MessageBox.Show("请选择服务包名称！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (rgp优惠方式.EditValue==null || string.IsNullOrWhiteSpace(rgp优惠方式.EditValue.ToString()))
            {
                MessageBox.Show("请选择优惠方式！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                //string s= "折扣优惠";
                if (rgp优惠方式.Text == "折扣优惠")
                {
                    if (string.IsNullOrWhiteSpace(num折扣额度.Text) || num折扣额度.Value==0)
                    {
                        Msg.ShowInformation("请填写折扣额度！");
                        return;
                    }

                    if (num折扣额度.Value >= 100 || num折扣额度.Value < 0)
                    {
                        Msg.ShowInformation("折扣只能在0--100之间。");
                        return;
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(num定额优惠.Text) || num定额优惠.Value == 0)
                    {
                        Msg.ShowInformation("请填写定额优惠额度！");
                        return;
                    }

                    DataTable dttemp = lue服务包.Properties.DataSource as DataTable;

                    string str = lue服务包.EditValue.ToString();
                    DataRow[] drs = dttemp.Select(tb_JTYS服务包.ServiceID + "='"+str+"'");
                    if(drs.Length >0)
                    {
                        decimal rawprice = Convert.ToDecimal(drs[0][tb_JTYS服务包.年收费标准].ToString());
                        if(num定额优惠.Value > rawprice || num定额优惠.Value < 0)
                        {
                            Msg.ShowInformation("折扣金额必须大于0，且小于年收费标准金额。");
                            return;
                        }
                    }
                }
            }

            if (m_UpdateType == UpdateType.Add)
            {
                bll.GetBusinessByKey("-1", true);
                bll.NewBusiness();
                //保存概要信息
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS优惠方式.对象类型] = lueServiceObject.EditValue.ToString();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS优惠方式.服务包ID] = lue服务包.EditValue.ToString();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS优惠方式.优惠方式] = rgp优惠方式.EditValue.ToString();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS优惠方式.优惠额度] = num定额优惠.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS优惠方式.优惠百分比] = num折扣额度.Text.Trim();
               
                bll.Save(bll.CurrentBusiness);
            }
            //else if (m_UpdateType == UpdateType.Modify)
            //{
            //    bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS优惠方式.ID] = DESEncrypt.GetChineseSpell(this.lueServiceObject.Text.Trim()); 
            //    DataSet dsTemplate = bll.CreateSaveData(bll.CurrentBusiness, m_UpdateType); //创建用于保存的临时数据
            //    bll.Save(dsTemplate);
            //}
            ////bll.Save(bll.CurrentBusiness);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
  