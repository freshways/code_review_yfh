﻿using AtomEHR.Library;
using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.服务包
{
    public partial class Frm服务包项目 : frmBase
    {
        string m_hospid;
        string m_乡镇机构编码;
        string m_packageID;
        DataTable dtItems;
        DataView m_dvItem;
        bllJTYS服务包医疗项目 bll医疗项目 = new bllJTYS服务包医疗项目();
        bllJTYS自定义项目 bll自定义项目 = new bllJTYS自定义项目();
        bllJTYS全局参数 bll全局 = new bllJTYS全局参数();
        public Frm服务包项目(DataTable dt, string packageID)
        {
            InitializeComponent();

            //TODO:
            dtItems = dt;
            m_packageID = packageID;
            m_dvItem = dtItems.DefaultView;
            m_dvItem.RowFilter = tb_JTYS服务包医疗项目.是否有效 + "=1";
            this.gcRightItem.DataSource = m_dvItem;

            try
            {
                m_乡镇机构编码 = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                m_hospid = bll全局.GetHospidByRgid(m_乡镇机构编码);
            }
            catch
            {
                sbtnLeftQuery.Enabled = false;
                sbtnLeftBottomQuery.Enabled = false;
                sbtnAddHisItem.Enabled = false;
                sbtnAddCustomItem.Enabled = false;
                sbtnRemoveItem.Enabled = false;
                sbtnRightQuery.Enabled = false;
            }
        }

        private void sbtnAddHisItem_Click(object sender, EventArgs e)
        {
            int[] indexs = gvLeftItem.GetSelectedRows();
            if (indexs.Length == 0)
            {
                Msg.ShowInformation("请选择需要添加的项目");
                return;
            }
            else if (indexs.Length > 1)
            {
                Msg.ShowInformation("一次只能添加一个项目");
                return;
            }

            string str项目ID = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目ID).ToString();
            string str项目编码 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目编码).ToString();
            string str项目名称 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目名称).ToString();
            string str拼音简码 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.拼音简码).ToString();
            string str单价 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.单价).ToString();
            string str单位 = gvLeftItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.单位).ToString();

            AddItemToRightList("HIS", str项目ID, str项目编码, str项目名称, Convert.ToDecimal(str单价), str单位);
        }

        private void sbtnAddCustomItem_Click(object sender, EventArgs e)
        {
            int[] indexs = gvLeftBottom.GetSelectedRows();
            if (indexs.Length == 0)
            {
                Msg.ShowInformation("请选择需要添加的项目");
                return;
            }
            else if (indexs.Length > 1)
            {
                Msg.ShowInformation("一次只能添加一个项目");
                return;
            }

            string str项目ID = gvLeftBottom.GetRowCellValue(indexs[0], tb_JTYS自定义项目.项目ID).ToString();
            string str项目名称 = gvLeftBottom.GetRowCellValue(indexs[0], tb_JTYS自定义项目.项目名称).ToString();
            string str拼音简码 = gvLeftBottom.GetRowCellValue(indexs[0], tb_JTYS自定义项目.拼音简码).ToString();
            string str单价 = gvLeftBottom.GetRowCellValue(indexs[0], tb_JTYS自定义项目.单价).ToString();
            string str单位 = gvLeftBottom.GetRowCellValue(indexs[0], tb_JTYS自定义项目.单位).ToString();

            AddItemToRightList("自定义", str项目ID, "", str项目名称, Convert.ToDecimal(str单价), str单位);
        }

        void AddItemToRightList(string str类别, string str项目ID, string str项目编码, string str项目名称, decimal d单价, string str单位)
        {
            FrmItemCountSetting frm = new FrmItemCountSetting();
            frm.ShowDialog();
            int count = frm.count;
            frm.Dispose();

            DataRow[] drs = dtItems.Select(tb_JTYS服务包医疗项目.项目类别 + "='" + str类别 + "' and " + tb_JTYS服务包医疗项目.项目ID + "='" + str项目ID + "'");

            DataRow newdr;
            if(drs.Length > 0)
            {
                if(Convert.ToBoolean(drs[0][tb_JTYS服务包医疗项目.是否有效]))
                {
                    Msg.ShowInformation("右侧列表中已经存在此项目，若要修改数量，请在右侧列表中直接修改。");
                    return;
                }
                else
                {
                    newdr = drs[0];
                }

            }
            else
            {
                newdr = dtItems.Rows.Add();
            }

            //DataRow newdr = dtItems.Rows.Add();
            newdr[tb_JTYS服务包医疗项目.ServiceID] = m_packageID;
            newdr[tb_JTYS服务包医疗项目.项目类别] = str类别;
            newdr[tb_JTYS服务包医疗项目.项目ID] = str项目ID;
            newdr[tb_JTYS服务包医疗项目.项目编码] = str项目编码;
            newdr[tb_JTYS服务包医疗项目.项目名称] = str项目名称;
            newdr[tb_JTYS服务包医疗项目.拼音简码] = "";
            newdr[tb_JTYS服务包医疗项目.单价] = d单价;
            newdr[tb_JTYS服务包医疗项目.单位] = str单位;
            newdr[tb_JTYS服务包医疗项目.数量] = count;
            newdr[tb_JTYS服务包医疗项目.总价] = count * Convert.ToDecimal(d单价);
            newdr[tb_JTYS服务包医疗项目.是否有效] = true;
            //newdr[tb_JTYS服务包医疗项目.创建人] = Loginer.CurrentUser.用户编码;
            //newdr[tb_JTYS服务包医疗项目.创建时间] = bll医疗项目.ServiceDateTime;
            //newdr[tb_JTYS服务包医疗项目.修改人] = Loginer.CurrentUser.用户编码;
            //newdr[tb_JTYS服务包医疗项目.修改时间] = newdr[tb_JTYS服务包医疗项目.创建时间];
        }

        private void sbtnRemoveItem_Click(object sender, EventArgs e)
        {
            int[] indexs = gvRightItem.GetSelectedRows();
            if (indexs.Length==0 )
            {
                Msg.ShowInformation("请选择需要删除的项目");
                return;
            }
            else if (indexs.Length > 1)
            {
                Msg.ShowInformation("一次只能删除一个项目");
                return;
            }

            string str项目ID = gvRightItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目ID).ToString();
            string str类别 = gvRightItem.GetRowCellValue(indexs[0], tb_JTYS服务包医疗项目.项目类别).ToString();

            DataRow[] drs = dtItems.Select(tb_JTYS服务包医疗项目.项目类别+"='"+str类别+"' and "+tb_JTYS服务包医疗项目.项目ID+"='"+str项目ID+"' ");
            for(int ind = 0; ind < drs.Length; ind++)
            {
                if (drs[ind].RowState == DataRowState.Modified || drs[ind].RowState == DataRowState.Unchanged)
                {
                    drs[ind][tb_JTYS服务包医疗项目.是否有效] = false;
                }
                else
                {
                    drs[ind].Delete();
                }
            }
        }

        #region 查询HIS项目
        private void sbtnLeftQuery_Click(object sender, EventArgs e)
        {
            QueryLeft();
        }

        private void txtLeftItem_EditValueChanged(object sender, EventArgs e)
        {
            QueryLeft();
        }

        void QueryLeft()
        {
            DataTable dt = bll医疗项目.GetHISItemList(m_hospid, txtLeftItem.Text.Trim());
            this.gcLeftItems.DataSource = dt;
        }
        #endregion

        private void sbtnRightQuery_Click(object sender, EventArgs e)
        {

        }

        private void sbtnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void sbtnLeftBottomQuery_Click(object sender, EventArgs e)
        {
            QueryCustomItems();
        }

        private void txtCustomItemName_EditValueChanged(object sender, EventArgs e)
        {
            QueryCustomItems();
        }
        void QueryCustomItems()
        {
            DataSet ds = bll自定义项目.GetValidateBusinessByName(m_乡镇机构编码, txtCustomItemName.Text.Trim());
            this.gcLeftBottom.DataSource = ds.Tables[0];
            gvLeftBottom.BestFitColumns();
        }

        private void gvRightItem_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == tb_JTYS服务包医疗项目.数量)
            {
                decimal PriceValue = Convert.ToDecimal(gvRightItem.GetRowCellValue(e.RowHandle, tb_JTYS服务包医疗项目.单价));
                decimal NoValue = Convert.ToDecimal(gvRightItem.GetRowCellValue(e.RowHandle, tb_JTYS服务包医疗项目.数量));
                decimal newTotalValue = PriceValue * NoValue;
                gvRightItem.SetRowCellValue(e.RowHandle, tb_JTYS服务包医疗项目.总价, newTotalValue);
            }
        }

        private void Frm服务包项目_Load(object sender, EventArgs e)
        {

        }
    }
}
