﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.Common;

namespace AtomEHR.签约服务.磁卡管理
{
    public partial class Frm磁卡管理 : AtomEHR.Library.frmBaseBusinessForm
    {
        bllJTYS档案VS磁卡 bllDahVsMargCard = new bllJTYS档案VS磁卡();
        public Frm磁卡管理()
        {
            InitializeComponent();
        }

        private void Frm磁卡管理_Load(object sender, EventArgs e)
        {
			this.InitializeForm();
        }
		
		protected override void InitializeForm()
        {
            _BLL = new bllJTYS服务包();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnReset, gcFindGroup);

            //util.BindingHelper.LookupEditBindDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, lookUpEdit性别, "P_CODE", "P_DESC");

            #region 机构信息
            try
            {
                AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                tllue所属机构.Properties.ValueMember = "机构编号";
                tllue所属机构.Properties.DisplayMember = "机构名称";

                tllue所属机构.Properties.TreeList.KeyFieldName = "机构编号";
                tllue所属机构.Properties.TreeList.ParentFieldName = "上级机构";
                tllue所属机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                tllue所属机构.Properties.DataSource = dt所属机构;
                //tllue所属机构.Properties.TreeList.ExpandAll();
                //tllue所属机构.Properties.TreeList.CollapseAll();

                tllue所属机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                tllue所属机构.Text = Loginer.CurrentUser.所属机构;
            }

            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                tllue所属机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                tllue所属机构.Properties.AutoExpandAllNodes = false;
            }
            #endregion


        }
        
        protected override bool DoSearchSummary()
        {
            string _strWhere = "";//" and " + tb_JTYS医生信息.是否有效 + "=1";

            string str所属机构 = tllue所属机构.EditValue.ToString();
            if (ck含下属机构.Checked)
            {
                if (str所属机构.Length == 12)
                {
                    _strWhere += " AND [档案所属机构] IN (select 机构编号 from tb_机构信息 where 上级机构='" + str所属机构 + "' or 机构编号='" + str所属机构 + "' ) ";
                }
                else
                {
                    _strWhere += " and 档案所属机构 like '" + str所属机构 + "%'";
                }

            }
            else
            {
                _strWhere += " and 档案所属机构='" + str所属机构 + "'";
            }

            if (string.IsNullOrWhiteSpace(txt姓名.Text))
            { }
            else
            {
                _strWhere += " and (姓名 like'" + DESEncrypt.DES加密(txt姓名.Text) + "%' or 姓名 like '" + this.txt姓名.Text.Trim() + "%')";
            }

            if (string.IsNullOrWhiteSpace(txt档案号.Text))
            { }
            else
            {
                _strWhere += " and 档案号 = '" + txt档案号.Text.Trim() + "'";
            }

            if (string.IsNullOrWhiteSpace(txt身份证号.Text))
            { }
            else
            {
                _strWhere += " and 身份证号 = '" + txt身份证号.Text.Trim() + "'";
            }

            if (string.IsNullOrWhiteSpace(txtMargCard.Text))
            { }
            else
            {
                _strWhere += " and 磁卡号 = '" + txtMargCard.Text.Trim() + "'";
            }

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS磁卡信息", "*", _strWhere, tb_JTYS档案VS磁卡.ID, "DESC").Tables[0];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][AtomEHR.Models.tb_健康档案.姓名] = DESEncrypt.DES解密(dt.Rows[i][AtomEHR.Models.tb_健康档案.姓名].ToString());
                dt.Rows[i][AtomEHR.Models.tb_健康档案.居住地址] = dt.Rows[i][AtomEHR.Models.tb_健康档案.区].ToString() + dt.Rows[i][AtomEHR.Models.tb_健康档案.街道] + dt.Rows[i][AtomEHR.Models.tb_健康档案.居委会] + dt.Rows[i][AtomEHR.Models.tb_健康档案.居住地址];
            }

            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            gvSummary.BestFitColumns();

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Frm签约档案查询 frm = new Frm签约档案查询();
            //System.Windows.Forms.DialogResult result = frm.ShowDialog();

            //if(result == System.Windows.Forms.DialogResult.OK)
            //{
            //    frm签约 frmQY = new frm签约(frm.SelectedDabh, frm.SelectedName, frm.SelectedCardNo, frm.SelectedTelNo, frm.selectedAddress);
            //    frmQY.ShowDialog();
            //}
            //else
            //{
            //    frm.Dispose();
            //}
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            tllue所属机构.EditValue = Loginer.CurrentUser.用户编码;
            ck含下属机构.Checked = true;

            for(int index = 0; index < layoutControl1.Controls.Count; index++)
            {
                Type type = layoutControl1.Controls[index].GetType();
                if(type.FullName=="DevExpress.XtraEditors.TextEdit")
                {
                    DevExpress.XtraEditors.TextEdit con = layoutControl1.Controls[index] as DevExpress.XtraEditors.TextEdit;
                    con.Text = "";
                }
            }
        }

        private void btnChangeMargCard_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请未选择任何行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能选择一行");
                return;
            }

            string createuser = gvSummary.GetRowCellValue(selectdIndex[0], gcol创建人ID).ToString();

            if (!CheckAccess(createuser))
            {
                Msg.ShowInformation("您没有修改此人磁卡号的权限，请联系与此人的签约医生修改。");
                return;
            }


            string id = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS档案VS磁卡.ID).ToString();
            FrmEdit磁卡号 frm = new FrmEdit磁卡号(id);
            frm.ShowDialog();
            frm.Dispose();
        }

        private void sbtnValidateMargCard_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请未选择任何行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能选择一行");
                return;
            }

            string createuser = gvSummary.GetRowCellValue(selectdIndex[0], gcol创建人ID).ToString();

            if(!CheckAccess(createuser))
            {
                Msg.ShowInformation("您没有修改此人磁卡号的权限，请联系与此人的签约医生修改。");
                return;
            }

            string id = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS档案VS磁卡.ID).ToString();
            string name = gvSummary.GetRowCellValue(selectdIndex[0], gcol姓名).ToString();
            string dah = gvSummary.GetRowCellValue(selectdIndex[0], gcol档案号).ToString();
            string address = gvSummary.GetRowCellValue(selectdIndex[0], gcol居住地址).ToString();

            string strNotity = "注销之前请确认信息：\n档案号："+dah+"\n姓　名："+name+"\n地址："+address+"\n确定要注销此人的磁卡号吗？";
            if(MessageBox.Show(strNotity, "确认", MessageBoxButtons.YesNo,MessageBoxIcon.Information) != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            bllDahVsMargCard.GetBusinessByKey(id, true);
            bllDahVsMargCard.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.状态] = "2";
            bllDahVsMargCard.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.修改人] = Loginer.CurrentUser.用户编码;
            bllDahVsMargCard.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.修改时间] = bllDahVsMargCard.ServiceDateTime;

            bllDahVsMargCard.Save(bllDahVsMargCard.CurrentBusiness);

            DoSearchSummary();
        }

        bool CheckAccess(string createUser)
        {
            if(Loginer.CurrentUser.IsAdmin() || Loginer.CurrentUser.用户编码 == createUser)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
