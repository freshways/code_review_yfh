﻿namespace AtomEHR.签约服务.磁卡管理
{
    partial class Frm磁卡管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm磁卡管理));
            this.pagerControl1 = new TActionProject.PagerControl();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ck含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.txtMargCard = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.tllue所属机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.btnChangeMargCard = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnValidateMargCard = new DevExpress.XtraEditors.SimpleButton();
            this.gc个人健康档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcol所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol磁卡号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol居住地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol修改人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol修改时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol创建人ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMargCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gc个人健康档案);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Size = new System.Drawing.Size(966, 508);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(972, 514);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(972, 514);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(972, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(794, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(597, 2);
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 468);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 14265);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 15;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(966, 40);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 129;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.layoutControl1);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(966, 93);
            this.gcFindGroup.TabIndex = 130;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.ck含下属机构);
            this.layoutControl1.Controls.Add(this.txtMargCard);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.tllue所属机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(271, 167, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(962, 89);
            this.layoutControl1.TabIndex = 35;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ck含下属机构
            // 
            this.ck含下属机构.EditValue = true;
            this.ck含下属机构.Location = new System.Drawing.Point(245, 32);
            this.ck含下属机构.Name = "ck含下属机构";
            this.ck含下属机构.Properties.Caption = "含下属机构";
            this.ck含下属机构.Size = new System.Drawing.Size(107, 19);
            this.ck含下属机构.StyleController = this.layoutControl1;
            this.ck含下属机构.TabIndex = 124;
            // 
            // txtMargCard
            // 
            this.txtMargCard.Location = new System.Drawing.Point(87, 57);
            this.txtMargCard.Name = "txtMargCard";
            this.txtMargCard.Size = new System.Drawing.Size(265, 20);
            this.txtMargCard.StyleController = this.layoutControl1;
            this.txtMargCard.TabIndex = 27;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(431, 32);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(195, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 8;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(431, 57);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(195, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(705, 32);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(245, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 6;
            // 
            // tllue所属机构
            // 
            this.tllue所属机构.Location = new System.Drawing.Point(87, 32);
            this.tllue所属机构.Name = "tllue所属机构";
            this.tllue所属机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tllue所属机构.Properties.NullText = "";
            this.tllue所属机构.Properties.PopupSizeable = false;
            this.tllue所属机构.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tllue所属机构.Size = new System.Drawing.Size(154, 20);
            this.tllue所属机构.StyleController = this.layoutControl1;
            this.tllue所属机构.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "签约服务查询";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem17,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(962, 89);
            this.layoutControlGroup1.Text = "查询条件";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tllue所属机构;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(233, 25);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txt档案号;
            this.layoutControlItem3.CustomizationFormText = "姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(618, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(94, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(324, 25);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "健康档案号：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(344, 25);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(274, 24);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ck含下属机构;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(233, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(111, 25);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt姓名;
            this.layoutControlItem5.CustomizationFormText = "健康档案号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(344, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(274, 25);
            this.layoutControlItem5.Text = "姓名：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txtMargCard;
            this.layoutControlItem17.CustomizationFormText = "磁卡号：";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(344, 24);
            this.layoutControlItem17.Text = "磁卡号：";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(72, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(618, 25);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(324, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btnReset);
            this.flowLayoutPanel1.Controls.Add(this.btnChangeMargCard);
            this.flowLayoutPanel1.Controls.Add(this.sbtnValidateMargCard);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 93);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(966, 30);
            this.flowLayoutPanel1.TabIndex = 131;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(103, 3);
            this.btnQuery.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 22);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.Image")));
            this.btnReset.Location = new System.Drawing.Point(184, 3);
            this.btnReset.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 22);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "重置";
            this.btnReset.ToolTip = "情况查询条件";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnChangeMargCard
            // 
            this.btnChangeMargCard.Image = ((System.Drawing.Image)(resources.GetObject("btnChangeMargCard.Image")));
            this.btnChangeMargCard.Location = new System.Drawing.Point(265, 3);
            this.btnChangeMargCard.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnChangeMargCard.Name = "btnChangeMargCard";
            this.btnChangeMargCard.Size = new System.Drawing.Size(99, 22);
            this.btnChangeMargCard.TabIndex = 2;
            this.btnChangeMargCard.Text = "修改磁卡号";
            this.btnChangeMargCard.Click += new System.EventHandler(this.btnChangeMargCard_Click);
            // 
            // sbtnValidateMargCard
            // 
            this.sbtnValidateMargCard.Image = ((System.Drawing.Image)(resources.GetObject("sbtnValidateMargCard.Image")));
            this.sbtnValidateMargCard.Location = new System.Drawing.Point(370, 3);
            this.sbtnValidateMargCard.Name = "sbtnValidateMargCard";
            this.sbtnValidateMargCard.Size = new System.Drawing.Size(100, 23);
            this.sbtnValidateMargCard.TabIndex = 7;
            this.sbtnValidateMargCard.Text = "注销磁卡号";
            this.sbtnValidateMargCard.Click += new System.EventHandler(this.sbtnValidateMargCard_Click);
            // 
            // gc个人健康档案
            // 
            this.gc个人健康档案.AllowBandedGridColumnSort = false;
            this.gc个人健康档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc个人健康档案.IsBestFitColumns = true;
            this.gc个人健康档案.Location = new System.Drawing.Point(0, 123);
            this.gc个人健康档案.MainView = this.gvSummary;
            this.gc个人健康档案.Name = "gc个人健康档案";
            this.gc个人健康档案.ShowContextMenu = true;
            this.gc个人健康档案.Size = new System.Drawing.Size(966, 345);
            this.gc个人健康档案.StrWhere = "";
            this.gc个人健康档案.TabIndex = 132;
            this.gc个人健康档案.UseCheckBox = true;
            this.gc个人健康档案.View = "";
            this.gc个人健康档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 30;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcol所属机构,
            this.gcol档案号,
            this.gcol磁卡号,
            this.gcol姓名,
            this.gcol性别,
            this.gcol身份证号,
            this.gcol居住地址,
            this.gcol创建人,
            this.gcol创建时间,
            this.gcol修改人,
            this.gcol修改时间,
            this.gcol创建人ID});
            this.gvSummary.GridControl = this.gc个人健康档案;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSummary.OptionsView.EnableAppearanceOddRow = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // gcol所属机构
            // 
            this.gcol所属机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol所属机构.AppearanceHeader.Options.UseFont = true;
            this.gcol所属机构.Caption = "所属机构";
            this.gcol所属机构.FieldName = "所属机构名称";
            this.gcol所属机构.Name = "gcol所属机构";
            this.gcol所属机构.OptionsColumn.ReadOnly = true;
            this.gcol所属机构.Visible = true;
            this.gcol所属机构.VisibleIndex = 0;
            // 
            // gcol档案号
            // 
            this.gcol档案号.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gcol档案号.AppearanceCell.Options.UseBackColor = true;
            this.gcol档案号.AppearanceCell.Options.UseTextOptions = true;
            this.gcol档案号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol档案号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol档案号.AppearanceHeader.Options.UseFont = true;
            this.gcol档案号.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol档案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol档案号.Caption = "档案号";
            this.gcol档案号.FieldName = "档案号";
            this.gcol档案号.Name = "gcol档案号";
            this.gcol档案号.OptionsColumn.ReadOnly = true;
            this.gcol档案号.Visible = true;
            this.gcol档案号.VisibleIndex = 1;
            this.gcol档案号.Width = 127;
            // 
            // gcol磁卡号
            // 
            this.gcol磁卡号.Caption = "磁卡号";
            this.gcol磁卡号.FieldName = "磁卡号";
            this.gcol磁卡号.Name = "gcol磁卡号";
            this.gcol磁卡号.Visible = true;
            this.gcol磁卡号.VisibleIndex = 2;
            // 
            // gcol姓名
            // 
            this.gcol姓名.AppearanceCell.Options.UseTextOptions = true;
            this.gcol姓名.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol姓名.AppearanceHeader.Options.UseFont = true;
            this.gcol姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol姓名.Caption = "姓名";
            this.gcol姓名.FieldName = "姓名";
            this.gcol姓名.Name = "gcol姓名";
            this.gcol姓名.OptionsColumn.ReadOnly = true;
            this.gcol姓名.Visible = true;
            this.gcol姓名.VisibleIndex = 3;
            // 
            // gcol性别
            // 
            this.gcol性别.AppearanceCell.Options.UseTextOptions = true;
            this.gcol性别.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol性别.AppearanceHeader.Options.UseFont = true;
            this.gcol性别.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol性别.Caption = "性别";
            this.gcol性别.FieldName = "性别名称";
            this.gcol性别.Name = "gcol性别";
            this.gcol性别.OptionsColumn.ReadOnly = true;
            this.gcol性别.Visible = true;
            this.gcol性别.VisibleIndex = 4;
            // 
            // gcol身份证号
            // 
            this.gcol身份证号.AppearanceCell.Options.UseTextOptions = true;
            this.gcol身份证号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol身份证号.AppearanceHeader.Options.UseFont = true;
            this.gcol身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol身份证号.Caption = "身份证号";
            this.gcol身份证号.FieldName = "身份证号";
            this.gcol身份证号.Name = "gcol身份证号";
            this.gcol身份证号.OptionsColumn.ReadOnly = true;
            this.gcol身份证号.Visible = true;
            this.gcol身份证号.VisibleIndex = 5;
            // 
            // gcol居住地址
            // 
            this.gcol居住地址.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol居住地址.AppearanceHeader.Options.UseFont = true;
            this.gcol居住地址.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol居住地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol居住地址.Caption = "居住地址";
            this.gcol居住地址.FieldName = "居住地址";
            this.gcol居住地址.Name = "gcol居住地址";
            this.gcol居住地址.OptionsColumn.ReadOnly = true;
            this.gcol居住地址.Visible = true;
            this.gcol居住地址.VisibleIndex = 6;
            this.gcol居住地址.Width = 163;
            // 
            // gcol创建人
            // 
            this.gcol创建人.AppearanceCell.Options.UseTextOptions = true;
            this.gcol创建人.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol创建人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol创建人.AppearanceHeader.Options.UseFont = true;
            this.gcol创建人.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol创建人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol创建人.Caption = "创建人";
            this.gcol创建人.FieldName = "创建人姓名";
            this.gcol创建人.Name = "gcol创建人";
            this.gcol创建人.OptionsColumn.ReadOnly = true;
            this.gcol创建人.Visible = true;
            this.gcol创建人.VisibleIndex = 7;
            // 
            // gcol创建时间
            // 
            this.gcol创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.gcol创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol创建时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol创建时间.AppearanceHeader.Options.UseFont = true;
            this.gcol创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol创建时间.Caption = "创建时间";
            this.gcol创建时间.FieldName = "创建时间";
            this.gcol创建时间.Name = "gcol创建时间";
            this.gcol创建时间.OptionsColumn.ReadOnly = true;
            this.gcol创建时间.Visible = true;
            this.gcol创建时间.VisibleIndex = 8;
            // 
            // gcol修改人
            // 
            this.gcol修改人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol修改人.AppearanceHeader.Options.UseFont = true;
            this.gcol修改人.Caption = "修改人";
            this.gcol修改人.FieldName = "修改人姓名";
            this.gcol修改人.Name = "gcol修改人";
            this.gcol修改人.Visible = true;
            this.gcol修改人.VisibleIndex = 9;
            // 
            // gcol修改时间
            // 
            this.gcol修改时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol修改时间.AppearanceHeader.Options.UseFont = true;
            this.gcol修改时间.Caption = "修改时间";
            this.gcol修改时间.FieldName = "修改时间";
            this.gcol修改时间.Name = "gcol修改时间";
            this.gcol修改时间.Visible = true;
            this.gcol修改时间.VisibleIndex = 10;
            // 
            // gcol创建人ID
            // 
            this.gcol创建人ID.Caption = "创建人ID";
            this.gcol创建人ID.FieldName = "创建人";
            this.gcol创建人ID.Name = "gcol创建人ID";
            // 
            // Frm磁卡管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 540);
            this.Name = "Frm磁卡管理";
            this.Text = "磁卡管理";
            this.Load += new System.EventHandler(this.Frm磁卡管理_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMargCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TActionProject.PagerControl pagerControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.SimpleButton btnChangeMargCard;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraEditors.SimpleButton btnReset;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtMargCard;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.CheckEdit ck含下属机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private Library.UserControls.DataGridControl gc个人健康档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn gcol档案号;
        private DevExpress.XtraGrid.Columns.GridColumn gcol姓名;
        private DevExpress.XtraGrid.Columns.GridColumn gcol性别;
        private DevExpress.XtraGrid.Columns.GridColumn gcol身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn gcol居住地址;
        private DevExpress.XtraGrid.Columns.GridColumn gcol创建人;
        private DevExpress.XtraGrid.Columns.GridColumn gcol创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn gcol所属机构;
        private DevExpress.XtraEditors.SimpleButton sbtnValidateMargCard;
        private DevExpress.XtraEditors.TreeListLookUpEdit tllue所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn gcol磁卡号;
        private DevExpress.XtraGrid.Columns.GridColumn gcol修改人;
        private DevExpress.XtraGrid.Columns.GridColumn gcol修改时间;
        private DevExpress.XtraGrid.Columns.GridColumn gcol创建人ID;
    }
}