﻿using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.磁卡管理
{
    public partial class FrmEdit磁卡号 : frmBase
    {
        string m_id;
        public FrmEdit磁卡号(string id)
        {
            InitializeComponent();
            m_id = id;
        }

        private void sbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnOK_Click(object sender, EventArgs e)
        {
            if(txtMargCard.Text.Length !=11)
            {
                if(MessageBox.Show("请确认磁卡号是否正确，是否执行保存操作？\n点击“确定”，执行保存操作；\n点击“取消”，中断保存操作。","确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)== System.Windows.Forms.DialogResult.OK)
                {
                }
                else
                {
                    return;
                }
            }

            DataSet ds = bllDahVsMargCard.GetBusinessByKey(m_id, true);
            bllDahVsMargCard.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.磁卡号] = txtMargCard.Text.Trim();
            bllDahVsMargCard.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.修改人] = Loginer.CurrentUser.用户编码;
            bllDahVsMargCard.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.修改时间] = bllDahVsMargCard.ServiceDateTime;
            bllDahVsMargCard.Save(bllDahVsMargCard.CurrentBusiness);
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        bllJTYS档案VS磁卡 bllDahVsMargCard = new bllJTYS档案VS磁卡();
        AtomEHR.Business.bll健康档案 bll档案 = new AtomEHR.Business.bll健康档案();
        private void FrmEdit磁卡号_Load(object sender, EventArgs e)
        {
            DataSet ds = bllDahVsMargCard.GetBusinessByKeyFromView(m_id);
            txtDAH.Text = ds.Tables[0].Rows[0][tb_JTYS档案VS磁卡.档案号].ToString();
            txtMargCard.Text = ds.Tables[0].Rows[0][tb_JTYS档案VS磁卡.磁卡号].ToString();
            txtSFZH.Text = ds.Tables[0].Rows[0]["身份证号"].ToString();
            txtName.Text = DESEncrypt.DES解密(ds.Tables[0].Rows[0]["姓名"].ToString());
        }
    }
}
