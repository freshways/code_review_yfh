﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Interfaces;
using AtomEHR.签约服务.签约服务管理;
using AtomEHR.签约服务.服务包;
using AtomEHR.签约服务.团队管理;

namespace AtomEHR.签约服务
{
    public partial class frm签约服务主窗体 : AtomEHR.Library.frmModuleBase
    {
        public frm签约服务主窗体()
        {
            InitializeComponent();

            _ModuleID = ModuleID.签约服务; //设置模块编号
            _ModuleName = ModuleNames.签约服务;//设置模块名称
            menuMain签约服务.Text = ModuleNames.签约服务; //与AssemblyModuleEntry.ModuleName定义相同

            this.MainMenuStrip = this.menuStrip1;

            //this.SetMenuTag();
            foreach (ToolStripItem tl in menuStrip1.Items)
            {
                SetMenuTag(tl);
            }
        }

        /// <summary>
        /// 重写该方法，返回模块主菜单对象.
        /// </summary>
        /// <returns></returns>
        public override MenuStrip GetModuleMenu()
        {
            return this.menuStrip1;
        }

        /// <summary>
        /// 设置菜单标记。初始化窗体的可用权限
        /// 请参考MenuItemTag类定义
        /// </summary>
        private void SetMenuTag(ToolStripItem tl)
        {
            tl.Tag = new MenuItemTag(MenuType.ItemOwner, (int)ModuleID.签约服务, AuthorityCategory.NONE);
            if (tl is ToolStripMenuItem)
                foreach (ToolStripItem tls in ((ToolStripMenuItem)tl).DropDownItems)
                {
                    SetMenuTag(tls);
                }

        }

        /// <summary>
        /// 设置模块主界面的权限
        /// </summary>        
        public override void SetSecurity(object securityInfo)
        {
            base.SetSecurity(securityInfo);

            if (securityInfo is ToolStrip)
            {
                //设置按钮权限
                //btnEdit.Enabled = MenuItemEdit.Enabled;
                //btnBusEdit.Enabled = MenuItemBusEdit.Enabled;
            }
        }

        private void 履约管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm履约管理), menuItem);
        }


        private void 签约管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm签约管理), menuItem);
        }

        private void 服务包ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm服务包管理), menuItem);
        }

        private void 团队管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm团队管理), menuItem);
        }

        private void 乡医管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            //MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm乡医管理), menuItem);
        }

        private void 医生管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm医生管理), menuItem);
        }

        private void 健康状况类型ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(基础设置.Frm健康状况管理), menuItem);
        }

        private void 签约对象类型ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(基础设置.frm签约对象管理), menuItem);
        }

        private void 签约审批管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm签约审批), menuItem);
        }

        private void 磁卡信息管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
        }

        private void 自定义项目维护ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm自定义项目), menuItem);
        }

        private void 按签约医生统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(统计.Frm按签约人统计), menuItem);
        }

        private void 按签约团队统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(统计.Frm按签约团队统计), menuItem);
        }

        private void 团队角色管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm团队角色), menuItem);
        }

        private void 协议书打印设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(AtomEHR.签约服务.签约服务管理.Report.Frm协议书设置), menuItem);
        }

        private void 优惠设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm优惠情况), menuItem);
        }

        private void 解约审批ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
                MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm解约审批), menuItem);
        }

        private void 解约查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(Frm解约审批查询), menuItem);
        }

        private void 自助签约审批ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
        }

        private void 自助签约问题提醒ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
        }

        private void 团队地区设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
        }

        private void 团队卫生室设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
        }

        private void 自助签约审批设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
        }

        private void 团队管理区域规则ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
        }

        private void 微信自助服务包ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
        }

        private void 按签约人群统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(AtomEHR.签约服务.统计.Frm签约人群统计), menuItem);
        }

        private void 按健康状况统计ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(AtomEHR.签约服务.统计.Frm健康状况统计), menuItem);
        }

        private void 家医转诊ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            MdiTools.OpenChildForm(this.MdiParent as IMdiForm, typeof(AtomEHR.签约服务.转诊.Frm转诊查询), menuItem);
        }
    }
}
