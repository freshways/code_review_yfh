﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AtomEHR.Core;
using AtomEHR.Interfaces;
using WEISHENG.COMM.PluginsAttribute;

// 有关程序集的常规信息通过以下
// 特性集控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyModuleEntry(ModuleID.签约服务, ModuleNames.签约服务, "AtomEHR.签约服务.frm签约服务主窗体")]
[assembly: AssemblyTitle("AtomEHR.签约服务")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("AtomEHR.签约服务")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 使此程序集中的类型
// 对 COM 组件不可见。  如果需要从 COM 访问此程序集中的类型，
// 则将该类型上的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("eb62e99a-c72e-4183-abef-0025f4e90b5e")]

// 程序集的版本信息由下面四个值组成: 
//
//      主版本
//      次版本 
//      生成号
//      修订号
//
// 可以指定所有这些值，也可以使用“生成号”和“修订号”的默认值，
// 方法是按如下所示使用“*”: 
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: PluginAssemblyInfo(插件名称 = "公卫家庭医生签约", 功能说明 = "公卫签约模块", 插件版本 = "1.0.0.0", 维护团队 = "阳光科技", 联系方式 = "", 插件文件名 = "AtomEHR.签约服务.dll", 插件授权 = "", 插件更新日期 = "2020-08-06", 插件是否有效 = true, 插件网址 = "", 插件GUID = "eb62e99a-c72e-4183-abef-0025f4e90b5e", 插件运行目录 = "", Tag = null)]
