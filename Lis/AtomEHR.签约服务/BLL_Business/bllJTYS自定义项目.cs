﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;

/*==========================================
 *   程序说明: JTYS自定义项目的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2017-12-23 05:51:49
 *   最后修改: 2017-12-23 05:51:49
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Business
{
    /// <summary>
    /// bllJTYS自定义项目
    /// </summary>
    public class bllJTYS自定义项目 : bllBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         public bllJTYS自定义项目()
         {
             _KeyFieldName = tb_JTYS自定义项目.__KeyName; //主键字段
             _SummaryTableName = tb_JTYS自定义项目.__TableName;//表名
         }

          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
          {
              DataSet ds = new dalJTYS自定义项目(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

          public DataSet GetValidateBusinessByName(string name)
          {
              AtomEHR.Business.bll机构信息 bll = new AtomEHR.Business.bll机构信息();
              string sjjg = bll.Get上级机构(Loginer.CurrentUser.所属机构);
              string bjjg = Loginer.CurrentUser.所属机构;
              DataSet ds = new dalJTYS自定义项目(Loginer.CurrentUser).GetValidateBusinessByName(name, sjjg, bjjg);
              return ds;
          }

          public DataSet GetValidateBusinessByName(string rgid, string name)
          {
              AtomEHR.Business.bll机构信息 bll = new AtomEHR.Business.bll机构信息();
              string sjjg = bll.Get上级机构(rgid);
              string bjjg = rgid;
              DataSet ds = new dalJTYS自定义项目(Loginer.CurrentUser).GetValidateBusinessByName(name, sjjg, bjjg);
              return ds;
          }

          /// <summary>
          ///删除单据
          /// </summary>
          public override bool Delete(string keyValue)
          {
              return new dalJTYS自定义项目(Loginer.CurrentUser).Delete(keyValue);
          }

          /// <summary>
          ///检查单号是否存在
          /// </summary>
          public bool CheckNoExists(string keyValue)
          {
              return new dalJTYS自定义项目(Loginer.CurrentUser).CheckNoExists(keyValue);
          }

          /// <summary>
          ///保存数据
          /// </summary>
          public override SaveResult Save(DataSet saveData)
          {
              return new dalJTYS自定义项目(Loginer.CurrentUser).Update(saveData); //交给数据层处理
          }

          /// <summary>
          ///审核单据
          /// </summary>
          //public override void ApprovalBusiness(DataRow summaryRow)
          //{
          //     summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
          //     summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.用户编码;
          //     summaryRow[BusinessCommonFields.FlagApp] = "Y";
          //     string key = ConvertEx.ToString(summaryRow[tb_JTYS自定义项目.__KeyName]);
          //     new dalJTYS自定义项目(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.用户编码, DateTime.Now);
          //}

          /// <summary>
          ///新增一张业务单据
          /// </summary>
          public override void NewBusiness()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_JTYS自定义项目.__TableName];
              DataRow row = summaryTable.Rows.Add();     
              //根据需要自行取消注释
              //row[tb_JTYS自定义项目.__KeyName] = "*自动生成*";
              row[tb_JTYS自定义项目.是否有效] = true;
              row[tb_JTYS自定义项目.创建人] = Loginer.CurrentUser.用户编码;
              row[tb_JTYS自定义项目.创建时间] = ServiceDateTime;
              row[tb_JTYS自定义项目.修改人] = Loginer.CurrentUser.用户编码;
              row[tb_JTYS自定义项目.修改时间] = row[tb_JTYS自定义项目.创建时间];
           }

          /// <summary>
          ///创建用于保存的临时数据
          /// </summary>
          public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
          {
              this.UpdateSummaryRowState(this.DataBindRow, currentType);

              //创建用于保存的临时数据,里面包含主表数据
              DataSet save = this.DoCreateTempData(currentBusiness, tb_JTYS自定义项目.__TableName);
              DataTable summary = save.Tables[0];
              //根据需要自行取消注释
              summary.Rows[0][AtomEHR.Models.BusinessCommonFields.s修改人] = Loginer.CurrentUser.用户编码;
              summary.Rows[0][AtomEHR.Models.BusinessCommonFields.s修改时间] = ServiceDateTime;

              //DataTable detail = currentBusiness.Tables[tb_JTYS自定义项目s.__TableName].Copy();
              //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
              //save.Tables.Add(detail); //加入明细数据 

              return save; 
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              DataTable dt = new dalJTYS自定义项目(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          #region Business Log

          /// <summary>
          /// 写入日志
          /// </summary>
          public override void WriteLog()
          {
              string key = this.DataBinder.Rows[0][tb_JTYS自定义项目.__KeyName].ToString();//取单据号码
              DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
              DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
              this.WriteLog(dsOriginal, dsTemplate);//保存日志      
          }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataTable original, DataTable changes) { }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataSet original, DataSet changes)
          {  //单独处理,即使错误,不向外抛出异常
              //if (_SaveVersionLog == false) return; //应用策略
              try
              {
                  string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID

                  IBridge_EditLogHistory SystemLog = bllBusinessLog.CreateEditLogHistoryBridge();//初始化日志方法

                  SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_JTYS自定义项目.__TableName, tb_JTYS自定义项目.__KeyName, true); //主表
                  //明细表的修改日志,系统不支持自动生成,请手工调整代码
                  //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_JTYS自定义项目.__TableName, tb_JTYS自定义项目.__KeyName, false);
              }
              catch
              {
                  Msg.Warning("写入日志发生错误！");
              }
          }

          #endregion
     }
}
