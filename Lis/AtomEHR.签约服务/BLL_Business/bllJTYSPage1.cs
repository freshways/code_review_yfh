﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.签约服务.Models;
//using AtomEHR.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;
using AtomEHR.签约服务.Server.DataAccess;

/*==========================================
 *   程序说明: JTYSPage1的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2018/07/10 06:25:08
 *   最后修改: 2018/07/10 06:25:08
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Business
{
    /// <summary>
    /// bllJTYSPage1
    /// </summary>
    public class bllJTYSPage1 : bllBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         public bllJTYSPage1()
         {
             _KeyFieldName = tb_JTYSPage1.__KeyName; //主键字段
             _SummaryTableName = tb_JTYSPage1.__TableName;//表名
         }

          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
          {
              DataSet ds = new dalJTYSPage1(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

          /// <summary>
          ///删除单据
          /// </summary>
          public override bool Delete(string keyValue)
          {
              return new dalJTYSPage1(Loginer.CurrentUser).Delete(keyValue);
          }

          /// <summary>
          ///检查单号是否存在
          /// </summary>
          public bool CheckNoExists(string keyValue)
          {
              return new dalJTYSPage1(Loginer.CurrentUser).CheckNoExists(keyValue);
          }

          /// <summary>
          ///保存数据
          /// </summary>
          public override SaveResult Save(DataSet saveData)
          {
              return new dalJTYSPage1(Loginer.CurrentUser).Update(saveData); //交给数据层处理
          }

          /// <summary>
          ///审核单据
          /// </summary>
          //public override void ApprovalBusiness(DataRow summaryRow)
          //{
          //     summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
          //     summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.用户编码;
          //     summaryRow[BusinessCommonFields.FlagApp] = "Y";
          //     string key = ConvertEx.ToString(summaryRow[tb_JTYSPage1.__KeyName]);
          //     new dalJTYSPage1(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.用户编码, DateTime.Now);
          //}

          /// <summary>
          ///新增一张业务单据
          /// </summary>
          public override void NewBusiness()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_JTYSPage1.__TableName];
              DataRow row = summaryTable.Rows.Add();     
              //根据需要自行取消注释
              //row[tb_JTYSPage1.__KeyName] = "*自动生成*";
              //row[tb_JTYSPage1.是否有效] = true;
              row[tb_JTYSPage1.createuser] = Loginer.CurrentUser.用户编码;
              row[tb_JTYSPage1.createtime] = ServiceDateTime;
              row[tb_JTYSPage1.updateuser] = Loginer.CurrentUser.用户编码;
              row[tb_JTYSPage1.updatetime] = row[tb_JTYSPage1.createtime];
           }

          /// <summary>
          ///创建用于保存的临时数据
          /// </summary>
          public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
          {
              this.UpdateSummaryRowState(this.DataBindRow, currentType);

              //创建用于保存的临时数据,里面包含主表数据
              DataSet save = this.DoCreateTempData(currentBusiness, tb_JTYSPage1.__TableName);
              DataTable summary = save.Tables[0];
              //根据需要自行取消注释
              //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.用户编码;
              //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

              //DataTable detail = currentBusiness.Tables[tb_JTYSPage1s.__TableName].Copy();
              //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
              //save.Tables.Add(detail); //加入明细数据 

              return save; 
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              DataTable dt = new dalJTYSPage1(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          public DataSet GetBusinessByRGID(string rgid, bool resetCurrent)
          {
              DataSet ds = new dalJTYSPage1(Loginer.CurrentUser).GetBusinessByRGID(rgid);
              //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 0)
              //{
              //    DataRow dr = ds.Tables[0].Rows.Add();
              //    dr[tb_JTYSPage1.所属机构] = rgid;
              //}
              if (resetCurrent)
              {
                  _CurrentBusiness = ds; //保存当前业务数据的对象引用
              }
              return ds;
          }

          #region Business Log

          /// <summary>
          /// 写入日志
          /// </summary>
          public override void WriteLog()
          {
              string key = this.DataBinder.Rows[0][tb_JTYSPage1.__KeyName].ToString();//取单据号码
              DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
              DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
              this.WriteLog(dsOriginal, dsTemplate);//保存日志      
          }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataTable original, DataTable changes) { }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataSet original, DataSet changes)
          {  //单独处理,即使错误,不向外抛出异常
              //if (_SaveVersionLog == false) return; //应用策略
              try
              {
                  string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID

                  IBridge_EditLogHistory SystemLog = bllBusinessLog.CreateEditLogHistoryBridge();//初始化日志方法

                  SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_JTYSPage1.__TableName, tb_JTYSPage1.__KeyName, true); //主表
                  //明细表的修改日志,系统不支持自动生成,请手工调整代码
                  //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_JTYSPage1.__TableName, tb_JTYSPage1.__KeyName, false);
              }
              catch
              {
                  Msg.Warning("写入日志发生错误！");
              }
          }

          #endregion
     }
}
