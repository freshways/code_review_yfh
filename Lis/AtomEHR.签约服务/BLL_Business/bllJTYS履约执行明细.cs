﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;

/*==========================================
 *   程序说明: JTYS履约执行明细的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2017-12-27 10:41:33
 *   最后修改: 2017-12-27 10:41:33
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Business
{
    /// <summary>
    /// bllJTYS履约执行明细
    /// </summary>
    public class bllJTYS履约执行明细 : bllBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         public bllJTYS履约执行明细()
         {
             _KeyFieldName = tb_JTYS履约执行明细.__KeyName; //主键字段
             _SummaryTableName = tb_JTYS履约执行明细.__TableName;//表名
         }

          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
          {
              DataSet ds = new dalJTYS履约执行明细(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

          /// <summary>
          ///删除单据
          /// </summary>
          public override bool Delete(string keyValue)
          {
              return new dalJTYS履约执行明细(Loginer.CurrentUser).Delete(keyValue);
          }

          /// <summary>
          ///检查单号是否存在
          /// </summary>
          public bool CheckNoExists(string keyValue)
          {
              return new dalJTYS履约执行明细(Loginer.CurrentUser).CheckNoExists(keyValue);
          }

          /// <summary>
          ///保存数据
          /// </summary>
          public override SaveResult Save(DataSet saveData)
          {
              return new dalJTYS履约执行明细(Loginer.CurrentUser).Update(saveData); //交给数据层处理
          }

          /// <summary>
          ///审核单据
          /// </summary>
          //public override void ApprovalBusiness(DataRow summaryRow)
          //{
          //     summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
          //     summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.用户编码;
          //     summaryRow[BusinessCommonFields.FlagApp] = "Y";
          //     string key = ConvertEx.ToString(summaryRow[tb_JTYS履约执行明细.__KeyName]);
          //     new dalJTYS履约执行明细(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.用户编码, DateTime.Now);
          //}

          /// <summary>
          ///新增一张业务单据
          /// </summary>
          public override void NewBusiness()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_JTYS履约执行明细.__TableName];
              DataRow row = summaryTable.Rows.Add();     
              //根据需要自行取消注释
              //row[tb_JTYS履约执行明细.__KeyName] = "*自动生成*";
              //row[tb_JTYS履约执行明细.是否有效] = true;
              row[tb_JTYS履约执行明细.创建人] = Loginer.CurrentUser.用户编码;
              row[tb_JTYS履约执行明细.创建时间] = ServiceDateTime;
              row[tb_JTYS履约执行明细.修改人] = Loginer.CurrentUser.用户编码;
              row[tb_JTYS履约执行明细.修改时间] = row[tb_JTYS履约执行明细.创建时间];
           }

          public DataRow NewBusinessWithReturn()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_JTYS履约执行明细.__TableName];
              DataRow row = summaryTable.Rows.Add();
              //根据需要自行取消注释
              //row[tb_JTYS履约执行明细.__KeyName] = "*自动生成*";
              //row[tb_JTYS履约执行明细.是否有效] = true;
              row[tb_JTYS履约执行明细.创建人] = Loginer.CurrentUser.用户编码;
              row[tb_JTYS履约执行明细.创建时间] = ServiceDateTime;
              row[tb_JTYS履约执行明细.修改人] = Loginer.CurrentUser.用户编码;
              row[tb_JTYS履约执行明细.修改时间] = row[tb_JTYS履约执行明细.创建时间];

              return row;
          }

          /// <summary>
          ///创建用于保存的临时数据
          /// </summary>
          public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
          {
              this.UpdateSummaryRowState(this.DataBindRow, currentType);

              //创建用于保存的临时数据,里面包含主表数据
              DataSet save = this.DoCreateTempData(currentBusiness, tb_JTYS履约执行明细.__TableName);
              DataTable summary = save.Tables[0];
              //根据需要自行取消注释
              //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.用户编码;
              //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

              //DataTable detail = currentBusiness.Tables[tb_JTYS履约执行明细s.__TableName].Copy();
              //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
              //save.Tables.Add(detail); //加入明细数据 

              return save; 
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              DataTable dt = new dalJTYS履约执行明细(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          #region Business Log

          /// <summary>
          /// 写入日志
          /// </summary>
          public override void WriteLog()
          {
              string key = this.DataBinder.Rows[0][tb_JTYS履约执行明细.__KeyName].ToString();//取单据号码
              DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
              DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
              this.WriteLog(dsOriginal, dsTemplate);//保存日志      
          }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataTable original, DataTable changes) { }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataSet original, DataSet changes)
          {  //单独处理,即使错误,不向外抛出异常
              //if (_SaveVersionLog == false) return; //应用策略
              try
              {
                  string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID

                  IBridge_EditLogHistory SystemLog = bllBusinessLog.CreateEditLogHistoryBridge();//初始化日志方法

                  SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_JTYS履约执行明细.__TableName, tb_JTYS履约执行明细.__KeyName, true); //主表
                  //明细表的修改日志,系统不支持自动生成,请手工调整代码
                  //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_JTYS履约执行明细.__TableName, tb_JTYS履约执行明细.__KeyName, false);
              }
              catch
              {
                  Msg.Warning("写入日志发生错误！");
              }
          }

          #endregion

          public bool CheckExistBy签约ID(string qyid)
          {
              bool ret = new dalJTYS履约执行明细(Loginer.CurrentUser).CheckExistBy签约ID(qyid);
              return ret;
          }

        /// <summary>
        /// 根据签约ID,获取已签约的所有项目、及项目的执行数量。参数中的服务包ID数组也是通过签约ID获取的。
        /// </summary>
        /// <param name="qyid"></param>
        /// <param name="serviceIDs"></param>
        /// <returns></returns>
          public DataSet Get服务包执行情况(string qyid, string[] serviceIDs)
          {
              DataSet ds = new dalJTYS履约执行明细(Loginer.CurrentUser).Get服务包执行情况(qyid, serviceIDs);
              return ds;
          }
           

          public DataSet Get执行明细ByQYID(string qyid)
          {
              DataSet ds = new dalJTYS履约执行明细(Loginer.CurrentUser).Get执行明细ByQYID(qyid);
              return ds;
          }

          public DataSet Get执行明细ByQYIDofDate(string qyid,string date)
          {
              DataSet ds = new dalJTYS履约执行明细(Loginer.CurrentUser).Get执行明细ByQYIDofDate(qyid, date);
              return ds;
          }

        

        /// <summary>
        /// 根据签约ID，创建时间获取执行记录中的检验、检查明细，为生成条码备用
        /// </summary>
        /// <param name="qyid"></param>
        /// <param name="createtime"></param>
        /// <returns></returns>
          public DataSet Get检验检查项目(string qyid, string createtime)
          {
              DataSet ds = new dalJTYS履约执行明细(Loginer.CurrentUser).Get检验检查项目(qyid, createtime);
              ds.Tables[0].TableName = tb_JTYS履约执行明细.__TableName;
              //ds.Tables[1].TableName = tb_JTYS履约执行明细.__TableName;
              return ds;
          }

          public DataSet Get检验检查项目WithBarcode(string qyid, string barcode)
          {
              DataSet ds = new dalJTYS履约执行明细(Loginer.CurrentUser).Get检验检查项目WithBarcode(qyid, barcode);
              return ds;
          }

          public string GetSerialCode()
          {
              string ret = new dalJTYS履约执行明细(Loginer.CurrentUser).GetSerialCode();
              return ret;
          }

          public DataTable Get化验结果ByBarcode(string barcode)
          {
              DataSet ds = new dalJTYS履约执行明细(Loginer.CurrentUser).Get化验结果ByBarcode(barcode);
              return ds.Tables[0];
          }

          public void UpdateLYZT(string qyid)
          {
              new dalJTYS履约执行明细(Loginer.CurrentUser).UpdateLYZT(qyid);
          }
    }
}
