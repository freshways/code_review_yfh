﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using AtomEHR.Bridge;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Server.DataAccess;
using AtomEHR.Interfaces.Interfaces_Bridge;

/*==========================================
 *   程序说明: JTYS团队概要的业务逻辑层
 *   作者姓名: ATOM
 *   创建日期: 2017-12-13 02:23:17
 *   最后修改: 2017-12-13 02:23:17
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Business
{
    /// <summary>
    /// bllJTYS团队概要
    /// </summary>
    public class bllJTYS团队概要 : bllBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         public bllJTYS团队概要()
         {
             _KeyFieldName = tb_JTYS团队概要.__KeyName; //主键字段
             _SummaryTableName = tb_JTYS团队概要.__TableName;//表名
         }

          /// <summary>
          ///根据单据号码取业务数据
          /// </summary>
          public override DataSet GetBusinessByKey(string keyValue, bool resetCurrent)
          {
              DataSet ds = new dalJTYS团队概要(Loginer.CurrentUser).GetBusinessByKey(keyValue); 
              this.SetNumericDefaultValue(ds); //设置预设值
              if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return ds;
           }

          public DataSet GetBusinessByTeamID(string keyValue)
          {
              DataSet ds = new dalJTYS团队概要(Loginer.CurrentUser).GetBusinessByTeamID(keyValue);
              return ds;
          }

          /// <summary>
          ///删除单据
          /// </summary>
          public override bool Delete(string keyValue)
          {
              return new dalJTYS团队概要(Loginer.CurrentUser).Delete(keyValue);
          }

          /// <summary>
          ///检查单号是否存在
          /// </summary>
          public bool CheckNoExists(string keyValue)
          {
              return new dalJTYS团队概要(Loginer.CurrentUser).CheckNoExists(keyValue);
          }

          /// <summary>
          ///保存数据
          /// </summary>
          public override SaveResult Save(DataSet saveData)
          {
              return new dalJTYS团队概要(Loginer.CurrentUser).Update(saveData); //交给数据层处理
          }

          /// <summary>
          ///审核单据
          /// </summary>
          //public override void ApprovalBusiness(DataRow summaryRow)
          //{
          //     summaryRow[BusinessCommonFields.AppDate] = DateTime.Now;
          //     summaryRow[BusinessCommonFields.AppUser] = Loginer.CurrentUser.Account;
          //     summaryRow[BusinessCommonFields.FlagApp] = "Y";
          //     string key = ConvertEx.ToString(summaryRow[tb_JTYS团队概要.__KeyName]);
          //     new dalJTYS团队概要(Loginer.CurrentUser).ApprovalBusiness(key, "Y", Loginer.CurrentUser.Account, DateTime.Now);
          //}

          /// <summary>
          ///新增一张业务单据
          /// </summary>
          public override void NewBusiness()
          {
              DataTable summaryTable = _CurrentBusiness.Tables[tb_JTYS团队概要.__TableName];
              DataRow row = summaryTable.Rows.Add();     
              //根据需要自行取消注释
              //row[tb_JTYS团队概要.__KeyName] = "*自动生成*";
              //row[tb_JTYS团队概要.团队ID] = Guid.NewGuid();
              row[tb_JTYS团队概要.是否有效] = true;
              row[tb_JTYS团队概要.创建人] = Loginer.CurrentUser.用户编码;
              row[tb_JTYS团队概要.创建时间] = ServiceDateTime;
              row[tb_JTYS团队概要.修改人] = Loginer.CurrentUser.用户编码;
              row[tb_JTYS团队概要.修改时间] = row[tb_JTYS团队成员.创建时间];
           }

          /// <summary>
          ///创建用于保存的临时数据
          /// </summary>
          public override DataSet CreateSaveData(DataSet currentBusiness, UpdateType currentType)
          {
              this.UpdateSummaryRowState(this.DataBindRow, currentType);

              //创建用于保存的临时数据,里面包含主表数据
              DataSet save = this.DoCreateTempData(currentBusiness, tb_JTYS团队概要.__TableName);
              DataTable summary = save.Tables[0];
              //根据需要自行取消注释
              //summary.Rows[0][BusinessCommonFields.s修改人] = Loginer.CurrentUser.Account;
              //summary.Rows[0][BusinessCommonFields.s修改时间] = DateTime.Now;

              //DataTable detail = currentBusiness.Tables[tb_JTYS团队概要s.__TableName].Copy();
              //this.UpdateDetailCommonValue(detail); //更新明细表的公共字段数据
              //save.Tables.Add(detail); //加入明细数据 

              return save; 
          }

          /// <summary>
          ///查询数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              DataTable dt = new dalJTYS团队概要(Loginer.CurrentUser).GetSummaryByParam(docNoFrom, docNoTo, docDateFrom, docDateTo);
              //this.SetNumericDefaultValue(ds); //设置预设值
              //if (resetCurrent) _CurrentBusiness = ds; //保存当前业务数据的对象引用
              return dt;
          }

          /// <summary>
          ///获取报表数据
          /// </summary>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          public DataTable GetAllValidateData()
          {
              AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
              string strWhere = " and " + tb_JTYS团队概要.是否有效 + "=1 ";
              if (Loginer.CurrentUser.IsAdmin())
              {
              }
              else
              {
                  strWhere += " and (" + tb_JTYS团队概要.所属机构 + "='" + Loginer.CurrentUser.所属机构 + "' or " + tb_JTYS团队概要.所属机构 + "='" + bll机构.Get上级机构(Loginer.CurrentUser.所属机构) + "') ";
              }
              return Get全部数据("vw_JTYS团队概要", "*", strWhere, tb_JTYS团队概要.ID, "ASC");
          }

          public DataSet GetTeamCapNameAndTel(string teamid)
          {
              DataSet ds = new dalJTYS团队概要(Loginer.CurrentUser).GetTeamCapNameAndTel(teamid);
              return ds;
          }

        //获取最大人数的团队信息
          public DataSet GetTeamCapNameAndTelByXZ(string xz)
          {
              DataSet ds = new dalJTYS团队概要(Loginer.CurrentUser).GetTeamCapNameAndTelByXZ(xz);
              return ds;
          }

          public DataSet GetTeamInfoByteamid(string teamid)
          {
              DataSet ds = new dalJTYS团队概要(Loginer.CurrentUser).GetTeamInfoByteamid(teamid);
              return ds;
          }
          public int Update是否禁用(string teamid, bool bvalidate)
          {
              return new dalJTYS团队概要(Loginer.CurrentUser).Update是否禁用(teamid, bvalidate);
          }

          public DataTable GetTeamInfoByRgid(string rgid)
          {
              DataTable dt = new dalJTYS团队概要(Loginer.CurrentUser).GetTeamInfoByRgid(rgid);
              return dt;
          }
          public DataTable GetTeamInfoByRgid2(string rgid)
          {
              DataTable dt = new dalJTYS团队概要(Loginer.CurrentUser).GetTeamInfoByRgid2(rgid);
              return dt;
          }
          public DataTable GetTeamInfoByRgid3(string rgid)
          {
              DataTable dt = new dalJTYS团队概要(Loginer.CurrentUser).GetTeamInfoByRgid3(rgid);
              return dt;
          }

          #region Business Log

          /// <summary>
          /// 写入日志
          /// </summary>
          public override void WriteLog()
          {
              string key = this.DataBinder.Rows[0][tb_JTYS团队概要.__KeyName].ToString();//取单据号码
              DataSet dsOriginal = this.GetBusinessByKey(key, false); //取保存前的原始数据, 用于保存日志时匹配数据.
              DataSet dsTemplate = this.CreateSaveData(this.CurrentBusiness, UpdateType.Modify); //创建用于保存的临时数据
              this.WriteLog(dsOriginal, dsTemplate);//保存日志      
          }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataTable original, DataTable changes) { }

          /// <summary>
          /// 写入日志
          /// </summary>
          /// <param name="original">原始数据</param>
          /// <param name="changes">修改后的数据</param>
          public override void WriteLog(DataSet original, DataSet changes)
          {  //单独处理,即使错误,不向外抛出异常
              //if (_SaveVersionLog == false) return; //应用策略
              try
              {
                  string logID = Guid.NewGuid().ToString().Replace("-", ""); //本次日志ID

                  IBridge_EditLogHistory SystemLog = bllBusinessLog.CreateEditLogHistoryBridge();//初始化日志方法

                  SystemLog.WriteLog(logID, original.Tables[0], changes.Tables[0], tb_JTYS团队概要.__TableName, tb_JTYS团队概要.__KeyName, true); //主表
                  //明细表的修改日志,系统不支持自动生成,请手工调整代码
                  //SystemLog.WriteLog(logID, original.Tables[1], changes.Tables[1], tb_JTYS团队概要.__TableName, tb_JTYS团队概要.__KeyName, false);
              }
              catch
              {
                  Msg.Warning("写入日志发生错误！");
              }
          }

          #endregion
     }
}
