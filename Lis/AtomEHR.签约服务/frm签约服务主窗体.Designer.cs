﻿namespace AtomEHR.签约服务
{
    partial class frm签约服务主窗体
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm签约服务主窗体));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuMain签约服务 = new System.Windows.Forms.ToolStripMenuItem();
            this.签约管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.履约管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.解约审批ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.解约查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.自助签约审批ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.自助签约问题提醒ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.磁卡管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.磁卡信息管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.基础信息设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.服务包ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.优惠设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.自定义项目维护ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.团队管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.医生管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.团队角色管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.健康状况类型ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.签约对象类型ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.协议书打印设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.微信自助签约ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.团队地区设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.团队卫生室设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.自助签约审批设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.团队管理区域规则ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.微信自助服务包ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.签约统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.按签约医生统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.按签约团队统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.按签约人群统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.按健康状况统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.家医转诊ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).BeginInit();
            this.pnlContainer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilReports
            // 
            this.ilReports.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilReports.ImageStream")));
            this.ilReports.Images.SetKeyName(0, "16_ArrayWhite.bmp");
            // 
            // pnlContainer
            // 
            this.pnlContainer.Controls.Add(this.menuStrip1);
            this.pnlContainer.Size = new System.Drawing.Size(975, 467);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuMain签约服务,
            this.磁卡管理ToolStripMenuItem,
            this.基础信息设置ToolStripMenuItem,
            this.签约统计ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(2, 2);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(971, 25);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // menuMain签约服务
            // 
            this.menuMain签约服务.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.签约管理ToolStripMenuItem,
            this.履约管理ToolStripMenuItem,
            this.解约审批ToolStripMenuItem,
            this.解约查询ToolStripMenuItem,
            this.自助签约审批ToolStripMenuItem,
            this.自助签约问题提醒ToolStripMenuItem,
            this.家医转诊ToolStripMenuItem});
            this.menuMain签约服务.Image = global::AtomEHR.签约服务.Properties.Resources.folder;
            this.menuMain签约服务.Name = "menuMain签约服务";
            this.menuMain签约服务.Size = new System.Drawing.Size(84, 21);
            this.menuMain签约服务.Text = "签约服务";
            // 
            // 签约管理ToolStripMenuItem
            // 
            this.签约管理ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.签约管理ToolStripMenuItem.Name = "签约管理ToolStripMenuItem";
            this.签约管理ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.签约管理ToolStripMenuItem.Text = "签约管理";
            this.签约管理ToolStripMenuItem.Click += new System.EventHandler(this.签约管理ToolStripMenuItem_Click);
            // 
            // 履约管理ToolStripMenuItem
            // 
            this.履约管理ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.履约管理ToolStripMenuItem.Name = "履约管理ToolStripMenuItem";
            this.履约管理ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.履约管理ToolStripMenuItem.Text = "履约管理";
            this.履约管理ToolStripMenuItem.Click += new System.EventHandler(this.履约管理ToolStripMenuItem_Click);
            // 
            // 解约审批ToolStripMenuItem
            // 
            this.解约审批ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.解约审批ToolStripMenuItem.Name = "解约审批ToolStripMenuItem";
            this.解约审批ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.解约审批ToolStripMenuItem.Text = "解约审批";
            this.解约审批ToolStripMenuItem.Click += new System.EventHandler(this.解约审批ToolStripMenuItem_Click);
            // 
            // 解约查询ToolStripMenuItem
            // 
            this.解约查询ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.解约查询ToolStripMenuItem.Name = "解约查询ToolStripMenuItem";
            this.解约查询ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.解约查询ToolStripMenuItem.Text = "解约查询";
            this.解约查询ToolStripMenuItem.Click += new System.EventHandler(this.解约查询ToolStripMenuItem_Click);
            // 
            // 自助签约审批ToolStripMenuItem
            // 
            this.自助签约审批ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.自助签约审批ToolStripMenuItem.Name = "自助签约审批ToolStripMenuItem";
            this.自助签约审批ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.自助签约审批ToolStripMenuItem.Text = "自助签约审批";
            this.自助签约审批ToolStripMenuItem.Click += new System.EventHandler(this.自助签约审批ToolStripMenuItem_Click);
            // 
            // 自助签约问题提醒ToolStripMenuItem
            // 
            this.自助签约问题提醒ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.自助签约问题提醒ToolStripMenuItem.Name = "自助签约问题提醒ToolStripMenuItem";
            this.自助签约问题提醒ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.自助签约问题提醒ToolStripMenuItem.Text = "自助签约问题提醒";
            this.自助签约问题提醒ToolStripMenuItem.Click += new System.EventHandler(this.自助签约问题提醒ToolStripMenuItem_Click);
            // 
            // 磁卡管理ToolStripMenuItem
            // 
            this.磁卡管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.磁卡信息管理ToolStripMenuItem});
            this.磁卡管理ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.folder;
            this.磁卡管理ToolStripMenuItem.Name = "磁卡管理ToolStripMenuItem";
            this.磁卡管理ToolStripMenuItem.Size = new System.Drawing.Size(84, 21);
            this.磁卡管理ToolStripMenuItem.Text = "磁卡管理";
            // 
            // 磁卡信息管理ToolStripMenuItem
            // 
            this.磁卡信息管理ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.磁卡信息管理ToolStripMenuItem.Name = "磁卡信息管理ToolStripMenuItem";
            this.磁卡信息管理ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.磁卡信息管理ToolStripMenuItem.Text = "磁卡信息管理";
            this.磁卡信息管理ToolStripMenuItem.Click += new System.EventHandler(this.磁卡信息管理ToolStripMenuItem_Click);
            // 
            // 基础信息设置ToolStripMenuItem
            // 
            this.基础信息设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.服务包ToolStripMenuItem,
            this.优惠设置ToolStripMenuItem,
            this.自定义项目维护ToolStripMenuItem,
            this.团队管理ToolStripMenuItem,
            this.医生管理ToolStripMenuItem,
            this.团队角色管理ToolStripMenuItem,
            this.健康状况类型ToolStripMenuItem,
            this.签约对象类型ToolStripMenuItem,
            this.协议书打印设置ToolStripMenuItem,
            this.微信自助签约ToolStripMenuItem});
            this.基础信息设置ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.folder;
            this.基础信息设置ToolStripMenuItem.Name = "基础信息设置ToolStripMenuItem";
            this.基础信息设置ToolStripMenuItem.Size = new System.Drawing.Size(108, 21);
            this.基础信息设置ToolStripMenuItem.Text = "签约基础信息";
            // 
            // 服务包ToolStripMenuItem
            // 
            this.服务包ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.服务包ToolStripMenuItem.Name = "服务包ToolStripMenuItem";
            this.服务包ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.服务包ToolStripMenuItem.Text = "服务包管理";
            this.服务包ToolStripMenuItem.Click += new System.EventHandler(this.服务包ToolStripMenuItem_Click);
            // 
            // 优惠设置ToolStripMenuItem
            // 
            this.优惠设置ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.优惠设置ToolStripMenuItem.Name = "优惠设置ToolStripMenuItem";
            this.优惠设置ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.优惠设置ToolStripMenuItem.Text = "优惠设置";
            this.优惠设置ToolStripMenuItem.Click += new System.EventHandler(this.优惠设置ToolStripMenuItem_Click);
            // 
            // 自定义项目维护ToolStripMenuItem
            // 
            this.自定义项目维护ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.自定义项目维护ToolStripMenuItem.Name = "自定义项目维护ToolStripMenuItem";
            this.自定义项目维护ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.自定义项目维护ToolStripMenuItem.Text = "自定义项目维护";
            this.自定义项目维护ToolStripMenuItem.Click += new System.EventHandler(this.自定义项目维护ToolStripMenuItem_Click);
            // 
            // 团队管理ToolStripMenuItem
            // 
            this.团队管理ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.团队管理ToolStripMenuItem.Name = "团队管理ToolStripMenuItem";
            this.团队管理ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.团队管理ToolStripMenuItem.Text = "团队管理";
            this.团队管理ToolStripMenuItem.Click += new System.EventHandler(this.团队管理ToolStripMenuItem_Click);
            // 
            // 医生管理ToolStripMenuItem
            // 
            this.医生管理ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.医生管理ToolStripMenuItem.Name = "医生管理ToolStripMenuItem";
            this.医生管理ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.医生管理ToolStripMenuItem.Text = "医生管理";
            this.医生管理ToolStripMenuItem.Click += new System.EventHandler(this.医生管理ToolStripMenuItem_Click);
            // 
            // 团队角色管理ToolStripMenuItem
            // 
            this.团队角色管理ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.团队角色管理ToolStripMenuItem.Name = "团队角色管理ToolStripMenuItem";
            this.团队角色管理ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.团队角色管理ToolStripMenuItem.Text = "团队角色管理";
            this.团队角色管理ToolStripMenuItem.Click += new System.EventHandler(this.团队角色管理ToolStripMenuItem_Click);
            // 
            // 健康状况类型ToolStripMenuItem
            // 
            this.健康状况类型ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.健康状况类型ToolStripMenuItem.Name = "健康状况类型ToolStripMenuItem";
            this.健康状况类型ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.健康状况类型ToolStripMenuItem.Text = "健康状况类型";
            this.健康状况类型ToolStripMenuItem.Click += new System.EventHandler(this.健康状况类型ToolStripMenuItem_Click);
            // 
            // 签约对象类型ToolStripMenuItem
            // 
            this.签约对象类型ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.签约对象类型ToolStripMenuItem.Name = "签约对象类型ToolStripMenuItem";
            this.签约对象类型ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.签约对象类型ToolStripMenuItem.Text = "签约对象类型";
            this.签约对象类型ToolStripMenuItem.Click += new System.EventHandler(this.签约对象类型ToolStripMenuItem_Click);
            // 
            // 协议书打印设置ToolStripMenuItem
            // 
            this.协议书打印设置ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.协议书打印设置ToolStripMenuItem.Name = "协议书打印设置ToolStripMenuItem";
            this.协议书打印设置ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.协议书打印设置ToolStripMenuItem.Text = "协议书打印设置";
            this.协议书打印设置ToolStripMenuItem.Click += new System.EventHandler(this.协议书打印设置ToolStripMenuItem_Click);
            // 
            // 微信自助签约ToolStripMenuItem
            // 
            this.微信自助签约ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.团队地区设置ToolStripMenuItem,
            this.团队卫生室设置ToolStripMenuItem,
            this.自助签约审批设置ToolStripMenuItem,
            this.团队管理区域规则ToolStripMenuItem,
            this.微信自助服务包ToolStripMenuItem});
            this.微信自助签约ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.folder;
            this.微信自助签约ToolStripMenuItem.Name = "微信自助签约ToolStripMenuItem";
            this.微信自助签约ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.微信自助签约ToolStripMenuItem.Text = "微信自助签约";
            // 
            // 团队地区设置ToolStripMenuItem
            // 
            this.团队地区设置ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.团队地区设置ToolStripMenuItem.Name = "团队地区设置ToolStripMenuItem";
            this.团队地区设置ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.团队地区设置ToolStripMenuItem.Text = "团队地区设置";
            this.团队地区设置ToolStripMenuItem.Click += new System.EventHandler(this.团队地区设置ToolStripMenuItem_Click);
            // 
            // 团队卫生室设置ToolStripMenuItem
            // 
            this.团队卫生室设置ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.团队卫生室设置ToolStripMenuItem.Name = "团队卫生室设置ToolStripMenuItem";
            this.团队卫生室设置ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.团队卫生室设置ToolStripMenuItem.Text = "团队卫生室设置";
            this.团队卫生室设置ToolStripMenuItem.Click += new System.EventHandler(this.团队卫生室设置ToolStripMenuItem_Click);
            // 
            // 自助签约审批设置ToolStripMenuItem
            // 
            this.自助签约审批设置ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.自助签约审批设置ToolStripMenuItem.Name = "自助签约审批设置ToolStripMenuItem";
            this.自助签约审批设置ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.自助签约审批设置ToolStripMenuItem.Text = "自助签约审批设置";
            this.自助签约审批设置ToolStripMenuItem.Click += new System.EventHandler(this.自助签约审批设置ToolStripMenuItem_Click);
            // 
            // 团队管理区域规则ToolStripMenuItem
            // 
            this.团队管理区域规则ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.团队管理区域规则ToolStripMenuItem.Name = "团队管理区域规则ToolStripMenuItem";
            this.团队管理区域规则ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.团队管理区域规则ToolStripMenuItem.Text = "团队管理区域规则";
            this.团队管理区域规则ToolStripMenuItem.Click += new System.EventHandler(this.团队管理区域规则ToolStripMenuItem_Click);
            // 
            // 微信自助服务包ToolStripMenuItem
            // 
            this.微信自助服务包ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.微信自助服务包ToolStripMenuItem.Name = "微信自助服务包ToolStripMenuItem";
            this.微信自助服务包ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.微信自助服务包ToolStripMenuItem.Text = "微信自助服务包";
            this.微信自助服务包ToolStripMenuItem.Click += new System.EventHandler(this.微信自助服务包ToolStripMenuItem_Click);
            // 
            // 签约统计ToolStripMenuItem
            // 
            this.签约统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.按签约医生统计ToolStripMenuItem,
            this.按签约团队统计ToolStripMenuItem,
            this.按签约人群统计ToolStripMenuItem,
            this.按健康状况统计ToolStripMenuItem});
            this.签约统计ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.folder;
            this.签约统计ToolStripMenuItem.Name = "签约统计ToolStripMenuItem";
            this.签约统计ToolStripMenuItem.Size = new System.Drawing.Size(84, 21);
            this.签约统计ToolStripMenuItem.Text = "签约统计";
            // 
            // 按签约医生统计ToolStripMenuItem
            // 
            this.按签约医生统计ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.按签约医生统计ToolStripMenuItem.Name = "按签约医生统计ToolStripMenuItem";
            this.按签约医生统计ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.按签约医生统计ToolStripMenuItem.Text = "按签约医生统计";
            this.按签约医生统计ToolStripMenuItem.Click += new System.EventHandler(this.按签约医生统计ToolStripMenuItem_Click);
            // 
            // 按签约团队统计ToolStripMenuItem
            // 
            this.按签约团队统计ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.按签约团队统计ToolStripMenuItem.Name = "按签约团队统计ToolStripMenuItem";
            this.按签约团队统计ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.按签约团队统计ToolStripMenuItem.Text = "按签约团队统计";
            this.按签约团队统计ToolStripMenuItem.Click += new System.EventHandler(this.按签约团队统计ToolStripMenuItem_Click);
            // 
            // 按签约人群统计ToolStripMenuItem
            // 
            this.按签约人群统计ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.按签约人群统计ToolStripMenuItem.Name = "按签约人群统计ToolStripMenuItem";
            this.按签约人群统计ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.按签约人群统计ToolStripMenuItem.Text = "按签约人群统计";
            this.按签约人群统计ToolStripMenuItem.Click += new System.EventHandler(this.按签约人群统计ToolStripMenuItem_Click);
            // 
            // 按健康状况统计ToolStripMenuItem
            // 
            this.按健康状况统计ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.按健康状况统计ToolStripMenuItem.Name = "按健康状况统计ToolStripMenuItem";
            this.按健康状况统计ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.按健康状况统计ToolStripMenuItem.Text = "按健康状况统计";
            this.按健康状况统计ToolStripMenuItem.Click += new System.EventHandler(this.按健康状况统计ToolStripMenuItem_Click);
            // 
            // 家医转诊ToolStripMenuItem
            // 
            this.家医转诊ToolStripMenuItem.Image = global::AtomEHR.签约服务.Properties.Resources.document;
            this.家医转诊ToolStripMenuItem.Name = "家医转诊ToolStripMenuItem";
            this.家医转诊ToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.家医转诊ToolStripMenuItem.Text = "家医转诊";
            this.家医转诊ToolStripMenuItem.Click += new System.EventHandler(this.家医转诊ToolStripMenuItem_Click);
            // 
            // frm签约服务主窗体
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.ClientSize = new System.Drawing.Size(975, 467);
            this.Name = "frm签约服务主窗体";
            this.Text = "签约服务";
            ((System.ComponentModel.ISupportInitialize)(this.pnlContainer)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            this.pnlContainer.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuMain签约服务;
        private System.Windows.Forms.ToolStripMenuItem 签约管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 履约管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 基础信息设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 服务包ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 团队管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 医生管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 健康状况类型ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 签约对象类型ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 磁卡管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 磁卡信息管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 自定义项目维护ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 签约统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 按签约医生统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 按签约团队统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 团队角色管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 协议书打印设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 优惠设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 解约审批ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 解约查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 微信自助签约ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 团队地区设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 团队卫生室设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 自助签约审批设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 团队管理区域规则ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 自助签约审批ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 自助签约问题提醒ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 微信自助服务包ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 按签约人群统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 按健康状况统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 家医转诊ToolStripMenuItem;
    }
}
