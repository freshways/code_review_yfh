﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class frm解约 : frmBase
    {
        public frm解约()
        {
            InitializeComponent();
        }

        string m_qyid;
        public frm解约(string qyid)
        {
            InitializeComponent();
            m_qyid = qyid;
            InitForm();
        }

        //bllJTYS签约对象类型 bll签约对象 = new bllJTYS签约对象类型();
        //bllJTYS健康状况 bll健康状况 = new bllJTYS健康状况();
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        //bllJTYS团队概要 bll团队 = new bllJTYS团队概要();
        //bllJTYS档案VS磁卡 bll档案磁卡 = new bllJTYS档案VS磁卡();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        AtomEHR.Business.bll健康档案 bll档案 = new AtomEHR.Business.bll健康档案();
        void InitForm()
        {
            //判断此人是否为贫困人口
            //查询是否为贫困人口
            bool isPK = false;
            if (isPK)
            {
                emptySpaceItem贫困人口.Text = "此人可能属于贫困人口";
            }
            else
            {
                emptySpaceItem贫困人口.Text = " ";
            }
        }

        private void frm解约_Load(object sender, EventArgs e)
        {
            //this.ActiveControl = txtMagCard;
            DataSet ds = bll签约.GetBusinessByIdFromView(m_qyid);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                txtDabh.Text = dt.Rows[0][tb_JTYS签约信息.档案号].ToString();
                //txtName.Text = dt.Rows[0][AtomEHR.Models.tb_健康档案.姓名].ToString();
                txtName.Text = DESEncrypt.DES解密(dt.Rows[0][AtomEHR.Models.tb_健康档案.姓名].ToString());
                txtTelNo.Text = dt.Rows[0][tb_JTYS签约信息.联系电话].ToString();
                txtAddress.Text = bll档案.GetAddressByDAH(txtDabh.Text);

                txtMagCard.Text = dt.Rows[0][tb_JTYS档案VS磁卡.磁卡号].ToString();
                txtCardID.Text = dt.Rows[0][AtomEHR.Models.tb_健康档案.身份证号].ToString();
                txtServiceObject.Text = dt.Rows[0]["签约对象类型名称"].ToString();
                txtHealthCondition.Text = dt.Rows[0]["健康状况名称"].ToString();
                
                //服务团队
                txtServiceTeam.Text = dt.Rows[0]["团队名称"].ToString();
                txt签约日期.Text = dt.Rows[0][tb_JTYS签约信息.签约日期].ToString();
                txt签约人.Text = dt.Rows[0]["签约医生姓名"].ToString();
                txt状态.Text = dt.Rows[0]["签约状态"].ToString();
                txt创建人.Text = dt.Rows[0]["创建人姓名"].ToString();
                txt创建时间.Text = dt.Rows[0][tb_JTYS签约信息.创建时间].ToString();
                txt修改人.Text = dt.Rows[0]["修改人姓名"].ToString();
                txt修改时间.Text = dt.Rows[0][tb_JTYS签约信息.修改时间].ToString();

                string serviceIDs = dt.Rows[0][tb_JTYS签约信息.签约服务包].ToString();
                string[] arrServices = serviceIDs.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                if(arrServices.Length > 0)
                {
                    DataTable dtServicePackage = bll服务包.GetServicePackagesByIDList(arrServices);
                    gcServicePackage.DataSource = dtServicePackage;
                }
            }
            txt解约人.Text = Loginer.CurrentUser.AccountName;
        }

        private void sbtn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        bllJTYS解约审批 bll解约审批 = new bllJTYS解约审批();
        private void sbtn解约_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txt解约原因.Text))
            {
                Msg.ShowInformation("请填写申请解约原因。");
                return;
            }

            //DataSet ds = bll签约.GetBusinessByKey(m_qyid, true);
            //if(ds !=null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            //{
            //    if(ds.Tables[0].Rows[0][tb_JTYS签约信息.状态].ToString()=="5")//已解约
            //    {
            //        Msg.ShowInformation("此次签约信息已经解约。请重新查询确认。");
            //    }
            //    else
            //    {
            //        bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.状态] = "5";
            //        bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.解约人] = Loginer.CurrentUser.用户编码;
            //        bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.解约原因] = txt解约原因.Text.Trim();
            //        bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.修改人] = Loginer.CurrentUser.用户编码;
            //        bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.修改时间] = bll签约.ServiceDateTime;
            //        bll签约.Save(bll签约.CurrentBusiness);
            //        this.DialogResult = DialogResult.OK;
            //    }
            //}
            //else
            //{
            //    Msg.ShowInformation("进行解约时没有查询到所要解约的签约信息。");
            //}

            
            DataSet ds = bll签约.GetBusinessByKey(m_qyid, true);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][tb_JTYS签约信息.状态].ToString() == "5")//已解约
                {
                    Msg.ShowInformation("此次签约信息已经解约。请重新查询确认。");
                }
                else
                {
                    bool ret = bll解约审批.Check已解约或已申请解约(m_qyid);
                    if (ret)
                    {
                        Msg.ShowInformation("已存在此签约的解约申请或已解约。");
                        return;
                    }

                    bll解约审批.Create待审批记录(m_qyid, txt解约原因.Text.Trim());

                    this.DialogResult = DialogResult.OK;
                }
            }
            

            
        }
    }
}

