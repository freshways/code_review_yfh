﻿namespace AtomEHR.签约服务.签约服务管理
{
    partial class Frm签约档案查询
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm签约档案查询));
            this.gc个人健康档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv个人健康档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col个人档案号码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col居住地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col本人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col联系人姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col联系人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col档案状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.tabSearchCondition = new DevExpress.XtraTab.XtraTabControl();
            this.tab_基础 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dte调查时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte调查时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间1 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生时间2 = new DevExpress.XtraEditors.DateEdit();
            this.dte出生时间1 = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.treeListLookUpEdit机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tab_重点 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk听力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk言语残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肢体残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk智力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk视力残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.chk残疾All = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2型糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肿瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk慢阻肺 = new DevExpress.XtraEditors.CheckEdit();
            this.chk重症精神病 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio重点人群 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tab_高危 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk临界高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk血脂边缘升高 = new DevExpress.XtraEditors.CheckEdit();
            this.chk空腹血糖升高 = new DevExpress.XtraEditors.CheckEdit();
            this.chk糖耐量异常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肥胖 = new DevExpress.XtraEditors.CheckEdit();
            this.chk重度吸烟 = new DevExpress.XtraEditors.CheckEdit();
            this.chk超重且中心型肥胖 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabSearchCondition)).BeginInit();
            this.tabSearchCondition.SuspendLayout();
            this.tab_基础.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.tab_重点.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk听力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk言语残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肢体残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk智力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk视力残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾All.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2型糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肿瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk慢阻肺.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重症精神病.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio重点人群.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            this.tab_高危.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk临界高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血脂边缘升高.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk空腹血糖升高.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk糖耐量异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肥胖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重度吸烟.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk超重且中心型肥胖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // gc个人健康档案
            // 
            this.gc个人健康档案.AllowBandedGridColumnSort = false;
            this.gc个人健康档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc个人健康档案.IsBestFitColumns = true;
            this.gc个人健康档案.Location = new System.Drawing.Point(0, 153);
            this.gc个人健康档案.MainView = this.gv个人健康档案;
            this.gc个人健康档案.Name = "gc个人健康档案";
            this.gc个人健康档案.ShowContextMenu = true;
            this.gc个人健康档案.Size = new System.Drawing.Size(945, 269);
            this.gc个人健康档案.StrWhere = "";
            this.gc个人健康档案.TabIndex = 5;
            this.gc个人健康档案.UseCheckBox = true;
            this.gc个人健康档案.View = "";
            this.gc个人健康档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv个人健康档案});
            // 
            // gv个人健康档案
            // 
            this.gv个人健康档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv个人健康档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv个人健康档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Empty.Options.UseFont = true;
            this.gv个人健康档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv个人健康档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.OddRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Preview.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Row.Options.UseFont = true;
            this.gv个人健康档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv个人健康档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.VertLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv个人健康档案.ColumnPanelRowHeight = 30;
            this.gv个人健康档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col个人档案号码,
            this.col姓名,
            this.col性别,
            this.col出生日期,
            this.col身份证号,
            this.col居住地址,
            this.col本人电话,
            this.col联系人姓名,
            this.col联系人电话,
            this.col所属机构,
            this.col创建人,
            this.col创建时间,
            this.col档案状态,
            this.gridColumn14});
            this.gv个人健康档案.GridControl = this.gc个人健康档案;
            this.gv个人健康档案.GroupPanelText = "DragColumn";
            this.gv个人健康档案.Name = "gv个人健康档案";
            this.gv个人健康档案.OptionsView.ColumnAutoWidth = false;
            this.gv个人健康档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv个人健康档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv个人健康档案.OptionsView.ShowGroupPanel = false;
            // 
            // col个人档案号码
            // 
            this.col个人档案号码.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col个人档案号码.AppearanceCell.Options.UseBackColor = true;
            this.col个人档案号码.AppearanceCell.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col个人档案号码.AppearanceHeader.Options.UseFont = true;
            this.col个人档案号码.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.Caption = "档案号";
            this.col个人档案号码.FieldName = "个人档案编号";
            this.col个人档案号码.Name = "col个人档案号码";
            this.col个人档案号码.OptionsColumn.ReadOnly = true;
            this.col个人档案号码.Visible = true;
            this.col个人档案号码.VisibleIndex = 0;
            this.col个人档案号码.Width = 127;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceCell.Options.UseTextOptions = true;
            this.col姓名.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col姓名.AppearanceHeader.Options.UseFont = true;
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 1;
            // 
            // col性别
            // 
            this.col性别.AppearanceCell.Options.UseTextOptions = true;
            this.col性别.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col性别.AppearanceHeader.Options.UseFont = true;
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 2;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceCell.Options.UseTextOptions = true;
            this.col出生日期.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col出生日期.AppearanceHeader.Options.UseFont = true;
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col出生日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.ReadOnly = true;
            this.col出生日期.Visible = true;
            this.col出生日期.VisibleIndex = 3;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceCell.Options.UseTextOptions = true;
            this.col身份证号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col身份证号.AppearanceHeader.Options.UseFont = true;
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 4;
            // 
            // col居住地址
            // 
            this.col居住地址.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col居住地址.AppearanceHeader.Options.UseFont = true;
            this.col居住地址.AppearanceHeader.Options.UseTextOptions = true;
            this.col居住地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居住地址.Caption = "居住地址";
            this.col居住地址.FieldName = "居住地址";
            this.col居住地址.Name = "col居住地址";
            this.col居住地址.OptionsColumn.ReadOnly = true;
            this.col居住地址.Visible = true;
            this.col居住地址.VisibleIndex = 5;
            this.col居住地址.Width = 163;
            // 
            // col本人电话
            // 
            this.col本人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col本人电话.AppearanceHeader.Options.UseFont = true;
            this.col本人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col本人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本人电话.Caption = "本人电话";
            this.col本人电话.FieldName = "本人电话";
            this.col本人电话.Name = "col本人电话";
            this.col本人电话.OptionsColumn.ReadOnly = true;
            this.col本人电话.Visible = true;
            this.col本人电话.VisibleIndex = 6;
            // 
            // col联系人姓名
            // 
            this.col联系人姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col联系人姓名.AppearanceHeader.Options.UseFont = true;
            this.col联系人姓名.Caption = "联系人姓名";
            this.col联系人姓名.FieldName = "联系人姓名";
            this.col联系人姓名.Name = "col联系人姓名";
            this.col联系人姓名.OptionsColumn.ReadOnly = true;
            this.col联系人姓名.Visible = true;
            this.col联系人姓名.VisibleIndex = 7;
            // 
            // col联系人电话
            // 
            this.col联系人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col联系人电话.AppearanceHeader.Options.UseFont = true;
            this.col联系人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col联系人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col联系人电话.Caption = "联系人电话";
            this.col联系人电话.FieldName = "联系人电话";
            this.col联系人电话.Name = "col联系人电话";
            this.col联系人电话.OptionsColumn.ReadOnly = true;
            this.col联系人电话.Visible = true;
            this.col联系人电话.VisibleIndex = 8;
            // 
            // col所属机构
            // 
            this.col所属机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col所属机构.AppearanceHeader.Options.UseFont = true;
            this.col所属机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col所属机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.Caption = "当前所属机构";
            this.col所属机构.FieldName = "所属机构名称";
            this.col所属机构.Name = "col所属机构";
            this.col所属机构.OptionsColumn.ReadOnly = true;
            this.col所属机构.Visible = true;
            this.col所属机构.VisibleIndex = 9;
            this.col所属机构.Width = 93;
            // 
            // col创建人
            // 
            this.col创建人.AppearanceCell.Options.UseTextOptions = true;
            this.col创建人.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建人.AppearanceHeader.Options.UseFont = true;
            this.col创建人.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建人.Caption = "录入人";
            this.col创建人.FieldName = "创建人";
            this.col创建人.Name = "col创建人";
            this.col创建人.OptionsColumn.ReadOnly = true;
            this.col创建人.Visible = true;
            this.col创建人.VisibleIndex = 10;
            // 
            // col创建时间
            // 
            this.col创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.col创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建时间.AppearanceHeader.Options.UseFont = true;
            this.col创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.Caption = "录入时间";
            this.col创建时间.DisplayFormat.FormatString = "d";
            this.col创建时间.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col创建时间.FieldName = "创建时间";
            this.col创建时间.Name = "col创建时间";
            this.col创建时间.OptionsColumn.ReadOnly = true;
            this.col创建时间.Visible = true;
            this.col创建时间.VisibleIndex = 11;
            // 
            // col档案状态
            // 
            this.col档案状态.AppearanceCell.Options.UseTextOptions = true;
            this.col档案状态.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案状态.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col档案状态.AppearanceHeader.Options.UseFont = true;
            this.col档案状态.AppearanceHeader.Options.UseTextOptions = true;
            this.col档案状态.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案状态.Caption = "档案状态";
            this.col档案状态.FieldName = "档案状态";
            this.col档案状态.Name = "col档案状态";
            this.col档案状态.OptionsColumn.ReadOnly = true;
            this.col档案状态.Visible = true;
            this.col档案状态.VisibleIndex = 12;
            // 
            // gridColumn14
            // 
            this.gridColumn14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridColumn14.AppearanceHeader.Options.UseFont = true;
            this.gridColumn14.Caption = "档案位置";
            this.gridColumn14.FieldName = "档案位置";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 13;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 422);
            this.pagerControl1.Margin = new System.Windows.Forms.Padding(22, 118, 22, 118);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 13893);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 10;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(945, 34);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 6;
            // 
            // tabSearchCondition
            // 
            this.tabSearchCondition.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabSearchCondition.Location = new System.Drawing.Point(0, 0);
            this.tabSearchCondition.Name = "tabSearchCondition";
            this.tabSearchCondition.SelectedTabPage = this.tab_基础;
            this.tabSearchCondition.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.tabSearchCondition.Size = new System.Drawing.Size(945, 125);
            this.tabSearchCondition.TabIndex = 12;
            this.tabSearchCondition.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tab_基础,
            this.tab_重点,
            this.tab_高危});
            // 
            // tab_基础
            // 
            this.tab_基础.Controls.Add(this.panelControl1);
            this.tab_基础.Name = "tab_基础";
            this.tab_基础.Size = new System.Drawing.Size(939, 96);
            this.tab_基础.Text = "基础查询";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(939, 96);
            this.panelControl1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.dte调查时间1);
            this.layoutControl1.Controls.Add(this.dte调查时间2);
            this.layoutControl1.Controls.Add(this.dte录入时间2);
            this.layoutControl1.Controls.Add(this.dte录入时间1);
            this.layoutControl1.Controls.Add(this.dte出生时间2);
            this.layoutControl1.Controls.Add(this.dte出生时间1);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.checkEdit21);
            this.layoutControl1.Controls.Add(this.treeListLookUpEdit机构);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.cbo性别);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(935, 92);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dte调查时间1
            // 
            this.dte调查时间1.EditValue = null;
            this.dte调查时间1.Location = new System.Drawing.Point(374, 52);
            this.dte调查时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte调查时间1.Name = "dte调查时间1";
            this.dte调查时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte调查时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte调查时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte调查时间1.Size = new System.Drawing.Size(132, 20);
            this.dte调查时间1.StyleController = this.layoutControl1;
            this.dte调查时间1.TabIndex = 6;
            // 
            // dte调查时间2
            // 
            this.dte调查时间2.EditValue = null;
            this.dte调查时间2.Location = new System.Drawing.Point(525, 52);
            this.dte调查时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte调查时间2.Name = "dte调查时间2";
            this.dte调查时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte调查时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte调查时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte调查时间2.Size = new System.Drawing.Size(131, 20);
            this.dte调查时间2.StyleController = this.layoutControl1;
            this.dte调查时间2.TabIndex = 8;
            // 
            // dte录入时间2
            // 
            this.dte录入时间2.EditValue = null;
            this.dte录入时间2.Location = new System.Drawing.Point(193, 52);
            this.dte录入时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte录入时间2.Name = "dte录入时间2";
            this.dte录入时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间2.Size = new System.Drawing.Size(112, 20);
            this.dte录入时间2.StyleController = this.layoutControl1;
            this.dte录入时间2.TabIndex = 5;
            // 
            // dte录入时间1
            // 
            this.dte录入时间1.EditValue = null;
            this.dte录入时间1.Location = new System.Drawing.Point(69, 52);
            this.dte录入时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte录入时间1.Name = "dte录入时间1";
            this.dte录入时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte录入时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte录入时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte录入时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间1.Size = new System.Drawing.Size(105, 20);
            this.dte录入时间1.StyleController = this.layoutControl1;
            this.dte录入时间1.TabIndex = 3;
            // 
            // dte出生时间2
            // 
            this.dte出生时间2.EditValue = null;
            this.dte出生时间2.Location = new System.Drawing.Point(193, 28);
            this.dte出生时间2.Margin = new System.Windows.Forms.Padding(0);
            this.dte出生时间2.Name = "dte出生时间2";
            this.dte出生时间2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间2.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生时间2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生时间2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生时间2.Size = new System.Drawing.Size(112, 20);
            this.dte出生时间2.StyleController = this.layoutControl1;
            this.dte出生时间2.TabIndex = 2;
            // 
            // dte出生时间1
            // 
            this.dte出生时间1.EditValue = null;
            this.dte出生时间1.Location = new System.Drawing.Point(69, 28);
            this.dte出生时间1.Margin = new System.Windows.Forms.Padding(0);
            this.dte出生时间1.Name = "dte出生时间1";
            this.dte出生时间1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生时间1.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生时间1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte出生时间1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生时间1.Size = new System.Drawing.Size(105, 20);
            this.dte出生时间1.StyleController = this.layoutControl1;
            this.dte出生时间1.TabIndex = 0;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(510, 28);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit村.Size = new System.Drawing.Size(146, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 37;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(374, 28);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit镇.Size = new System.Drawing.Size(132, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 36;
            this.comboBoxEdit镇.Click += new System.EventHandler(this.comboBoxEdit镇_EditValueChanged);
            // 
            // checkEdit21
            // 
            this.checkEdit21.EditValue = true;
            this.checkEdit21.Location = new System.Drawing.Point(211, 4);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "含下属机构";
            this.checkEdit21.Size = new System.Drawing.Size(94, 19);
            this.checkEdit21.StyleController = this.layoutControl1;
            this.checkEdit21.TabIndex = 35;
            // 
            // treeListLookUpEdit机构
            // 
            this.treeListLookUpEdit机构.Location = new System.Drawing.Point(69, 4);
            this.treeListLookUpEdit机构.Name = "treeListLookUpEdit机构";
            this.treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            this.treeListLookUpEdit机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEdit机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEdit机构.Size = new System.Drawing.Size(138, 20);
            this.treeListLookUpEdit机构.StyleController = this.layoutControl1;
            this.treeListLookUpEdit机构.TabIndex = 34;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 99);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(725, 28);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(206, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 8;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(725, 4);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(206, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 7;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(374, 4);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(132, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // cbo性别
            // 
            this.cbo性别.Location = new System.Drawing.Point(575, 4);
            this.cbo性别.Name = "cbo性别";
            this.cbo性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo性别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo性别.Size = new System.Drawing.Size(81, 20);
            this.cbo性别.StyleController = this.layoutControl1;
            this.cbo性别.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem32,
            this.layoutControlItem1,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem6,
            this.layoutControlItem39,
            this.layoutControlItem5,
            this.layoutControlItem10,
            this.layoutControlItem40,
            this.emptySpaceItem2,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(935, 92);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.Location = new System.Drawing.Point(305, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(201, 24);
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem3.Control = this.cbo性别;
            this.layoutControlItem3.CustomizationFormText = "性别：";
            this.layoutControlItem3.Location = new System.Drawing.Point(506, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "性别：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem4.Control = this.txt档案号;
            this.layoutControlItem4.CustomizationFormText = "档案号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(656, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(275, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "档案号：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.treeListLookUpEdit机构;
            this.layoutControlItem32.CustomizationFormText = "所属机构：";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "所属机构：";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkEdit21;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(207, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(90, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(98, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.comboBoxEdit镇;
            this.layoutControlItem33.CustomizationFormText = "居住地址：";
            this.layoutControlItem33.Location = new System.Drawing.Point(305, 24);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(201, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "居住地址：";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.comboBoxEdit村;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
            this.layoutControlItem34.Location = new System.Drawing.Point(506, 24);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem34.Text = "layoutControlItem34";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.dte出生时间1;
            this.layoutControlItem37.CustomizationFormText = "出生日期：";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(174, 24);
            this.layoutControlItem37.Text = "出生日期：";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.dte出生时间2;
            this.layoutControlItem38.CustomizationFormText = "~";
            this.layoutControlItem38.Location = new System.Drawing.Point(174, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(131, 24);
            this.layoutControlItem38.Text = "~";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dte录入时间1;
            this.layoutControlItem6.CustomizationFormText = "录入时间：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(174, 24);
            this.layoutControlItem6.Text = "录入时间：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.dte录入时间2;
            this.layoutControlItem39.CustomizationFormText = "~";
            this.layoutControlItem39.Location = new System.Drawing.Point(174, 48);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(131, 24);
            this.layoutControlItem39.Text = "~";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem5.Control = this.txt身份证号;
            this.layoutControlItem5.CustomizationFormText = "身份证号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(656, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(119, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(275, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "身份证号：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.dte调查时间1;
            this.layoutControlItem10.CustomizationFormText = "调查时间：";
            this.layoutControlItem10.Location = new System.Drawing.Point(305, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(201, 24);
            this.layoutControlItem10.Text = "调查时间：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.dte调查时间2;
            this.layoutControlItem40.CustomizationFormText = "~";
            this.layoutControlItem40.Location = new System.Drawing.Point(506, 48);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem40.Text = "~";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(656, 48);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(275, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(931, 16);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tab_重点
            // 
            this.tab_重点.Controls.Add(this.panelControl2);
            this.tab_重点.Name = "tab_重点";
            this.tab_重点.Size = new System.Drawing.Size(939, 96);
            this.tab_重点.Text = "重点人群查询";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.layoutControl2);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(939, 96);
            this.panelControl2.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl2.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl2.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 2);
            this.layoutControl2.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsView.DrawItemBorders = true;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(935, 92);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.chk听力残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk言语残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk肢体残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk智力残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk视力残疾);
            this.flowLayoutPanel7.Controls.Add(this.chk残疾All);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(59, 62);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Padding = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel7.Size = new System.Drawing.Size(872, 25);
            this.flowLayoutPanel7.TabIndex = 6;
            // 
            // chk听力残疾
            // 
            this.chk听力残疾.Location = new System.Drawing.Point(1, 1);
            this.chk听力残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk听力残疾.Name = "chk听力残疾";
            this.chk听力残疾.Properties.Caption = "听力残疾";
            this.chk听力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk听力残疾.TabIndex = 0;
            // 
            // chk言语残疾
            // 
            this.chk言语残疾.Location = new System.Drawing.Point(76, 1);
            this.chk言语残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk言语残疾.Name = "chk言语残疾";
            this.chk言语残疾.Properties.Caption = "言语残疾";
            this.chk言语残疾.Size = new System.Drawing.Size(86, 19);
            this.chk言语残疾.TabIndex = 2;
            // 
            // chk肢体残疾
            // 
            this.chk肢体残疾.Location = new System.Drawing.Point(162, 1);
            this.chk肢体残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk肢体残疾.Name = "chk肢体残疾";
            this.chk肢体残疾.Properties.Caption = "肢体残疾";
            this.chk肢体残疾.Size = new System.Drawing.Size(75, 19);
            this.chk肢体残疾.TabIndex = 2;
            // 
            // chk智力残疾
            // 
            this.chk智力残疾.Location = new System.Drawing.Point(237, 1);
            this.chk智力残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk智力残疾.Name = "chk智力残疾";
            this.chk智力残疾.Properties.Caption = "智力残疾";
            this.chk智力残疾.Size = new System.Drawing.Size(75, 19);
            this.chk智力残疾.TabIndex = 3;
            // 
            // chk视力残疾
            // 
            this.chk视力残疾.Location = new System.Drawing.Point(312, 1);
            this.chk视力残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk视力残疾.Name = "chk视力残疾";
            this.chk视力残疾.Properties.Caption = "视力残疾";
            this.chk视力残疾.Size = new System.Drawing.Size(76, 19);
            this.chk视力残疾.TabIndex = 7;
            // 
            // chk残疾All
            // 
            this.chk残疾All.Location = new System.Drawing.Point(388, 1);
            this.chk残疾All.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾All.Name = "chk残疾All";
            this.chk残疾All.Properties.Caption = "全部";
            this.chk残疾All.Size = new System.Drawing.Size(75, 19);
            this.chk残疾All.TabIndex = 7;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.chk高血压);
            this.flowLayoutPanel6.Controls.Add(this.chk2型糖尿病);
            this.flowLayoutPanel6.Controls.Add(this.chk冠心病);
            this.flowLayoutPanel6.Controls.Add(this.chk脑卒中);
            this.flowLayoutPanel6.Controls.Add(this.chk肿瘤);
            this.flowLayoutPanel6.Controls.Add(this.chk慢阻肺);
            this.flowLayoutPanel6.Controls.Add(this.chk重症精神病);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(59, 34);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Padding = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel6.Size = new System.Drawing.Size(872, 24);
            this.flowLayoutPanel6.TabIndex = 5;
            // 
            // chk高血压
            // 
            this.chk高血压.Location = new System.Drawing.Point(1, 1);
            this.chk高血压.Margin = new System.Windows.Forms.Padding(0);
            this.chk高血压.Name = "chk高血压";
            this.chk高血压.Properties.Caption = "高血压";
            this.chk高血压.Size = new System.Drawing.Size(75, 19);
            this.chk高血压.TabIndex = 0;
            // 
            // chk2型糖尿病
            // 
            this.chk2型糖尿病.Location = new System.Drawing.Point(76, 1);
            this.chk2型糖尿病.Margin = new System.Windows.Forms.Padding(0);
            this.chk2型糖尿病.Name = "chk2型糖尿病";
            this.chk2型糖尿病.Properties.Caption = "2型糖尿病";
            this.chk2型糖尿病.Size = new System.Drawing.Size(86, 19);
            this.chk2型糖尿病.TabIndex = 1;
            // 
            // chk冠心病
            // 
            this.chk冠心病.Location = new System.Drawing.Point(162, 1);
            this.chk冠心病.Margin = new System.Windows.Forms.Padding(0);
            this.chk冠心病.Name = "chk冠心病";
            this.chk冠心病.Properties.Caption = "冠心病";
            this.chk冠心病.Size = new System.Drawing.Size(75, 19);
            this.chk冠心病.TabIndex = 2;
            // 
            // chk脑卒中
            // 
            this.chk脑卒中.Location = new System.Drawing.Point(237, 1);
            this.chk脑卒中.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑卒中.Name = "chk脑卒中";
            this.chk脑卒中.Properties.Caption = "脑卒中";
            this.chk脑卒中.Size = new System.Drawing.Size(75, 19);
            this.chk脑卒中.TabIndex = 3;
            // 
            // chk肿瘤
            // 
            this.chk肿瘤.Location = new System.Drawing.Point(312, 1);
            this.chk肿瘤.Margin = new System.Windows.Forms.Padding(0);
            this.chk肿瘤.Name = "chk肿瘤";
            this.chk肿瘤.Properties.Caption = "肿瘤";
            this.chk肿瘤.Size = new System.Drawing.Size(76, 19);
            this.chk肿瘤.TabIndex = 4;
            // 
            // chk慢阻肺
            // 
            this.chk慢阻肺.Location = new System.Drawing.Point(388, 1);
            this.chk慢阻肺.Margin = new System.Windows.Forms.Padding(0);
            this.chk慢阻肺.Name = "chk慢阻肺";
            this.chk慢阻肺.Properties.Caption = "慢阻肺";
            this.chk慢阻肺.Size = new System.Drawing.Size(63, 19);
            this.chk慢阻肺.TabIndex = 5;
            // 
            // chk重症精神病
            // 
            this.chk重症精神病.Location = new System.Drawing.Point(451, 1);
            this.chk重症精神病.Margin = new System.Windows.Forms.Padding(0);
            this.chk重症精神病.Name = "chk重症精神病";
            this.chk重症精神病.Properties.Caption = "重症精神病";
            this.chk重症精神病.Size = new System.Drawing.Size(88, 19);
            this.chk重症精神病.TabIndex = 6;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.radio重点人群);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(59, 4);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(872, 26);
            this.flowLayoutPanel5.TabIndex = 4;
            // 
            // radio重点人群
            // 
            this.radio重点人群.Location = new System.Drawing.Point(0, 0);
            this.radio重点人群.Margin = new System.Windows.Forms.Padding(0);
            this.radio重点人群.Name = "radio重点人群";
            this.radio重点人群.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("", "不限年龄"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "0~3岁幼童"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "0~6岁幼童"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("15", "育龄妇女"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("23", "孕产妇"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("65", "65岁及以上老年人")});
            this.radio重点人群.Size = new System.Drawing.Size(718, 24);
            this.radio重点人群.TabIndex = 0;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(935, 92);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.flowLayoutPanel5;
            this.layoutControlItem14.CustomizationFormText = "普通：";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(159, 30);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(931, 30);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "普通：";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.flowLayoutPanel6;
            this.layoutControlItem15.CustomizationFormText = "患者：";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(159, 28);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(931, 28);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "患者：";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.flowLayoutPanel7;
            this.layoutControlItem16.CustomizationFormText = "残疾：";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 29);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(143, 28);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(931, 30);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "残疾：";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // tab_高危
            // 
            this.tab_高危.Controls.Add(this.panelControl3);
            this.tab_高危.Name = "tab_高危";
            this.tab_高危.Size = new System.Drawing.Size(939, 96);
            this.tab_高危.Text = "高危人群查询";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.layoutControl3);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(939, 96);
            this.panelControl3.TabIndex = 0;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 2);
            this.layoutControl3.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsView.DrawItemBorders = true;
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(935, 92);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.chk临界高血压);
            this.flowLayoutPanel8.Controls.Add(this.chk血脂边缘升高);
            this.flowLayoutPanel8.Controls.Add(this.chk空腹血糖升高);
            this.flowLayoutPanel8.Controls.Add(this.chk糖耐量异常);
            this.flowLayoutPanel8.Controls.Add(this.chk肥胖);
            this.flowLayoutPanel8.Controls.Add(this.chk重度吸烟);
            this.flowLayoutPanel8.Controls.Add(this.chk超重且中心型肥胖);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(67, 12);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(856, 26);
            this.flowLayoutPanel8.TabIndex = 4;
            // 
            // chk临界高血压
            // 
            this.chk临界高血压.Location = new System.Drawing.Point(3, 3);
            this.chk临界高血压.Name = "chk临界高血压";
            this.chk临界高血压.Properties.Caption = "A: 临界高血压";
            this.chk临界高血压.Size = new System.Drawing.Size(107, 19);
            this.chk临界高血压.TabIndex = 0;
            // 
            // chk血脂边缘升高
            // 
            this.chk血脂边缘升高.Location = new System.Drawing.Point(116, 3);
            this.chk血脂边缘升高.Name = "chk血脂边缘升高";
            this.chk血脂边缘升高.Properties.Caption = "B: 血脂边缘升高";
            this.chk血脂边缘升高.Size = new System.Drawing.Size(120, 19);
            this.chk血脂边缘升高.TabIndex = 1;
            // 
            // chk空腹血糖升高
            // 
            this.chk空腹血糖升高.Location = new System.Drawing.Point(242, 3);
            this.chk空腹血糖升高.Name = "chk空腹血糖升高";
            this.chk空腹血糖升高.Properties.Caption = "C: 空腹血糖升高";
            this.chk空腹血糖升高.Size = new System.Drawing.Size(124, 19);
            this.chk空腹血糖升高.TabIndex = 2;
            // 
            // chk糖耐量异常
            // 
            this.chk糖耐量异常.Location = new System.Drawing.Point(372, 3);
            this.chk糖耐量异常.Name = "chk糖耐量异常";
            this.chk糖耐量异常.Properties.Caption = "D: 糖耐量异常";
            this.chk糖耐量异常.Size = new System.Drawing.Size(109, 19);
            this.chk糖耐量异常.TabIndex = 3;
            // 
            // chk肥胖
            // 
            this.chk肥胖.Location = new System.Drawing.Point(487, 3);
            this.chk肥胖.Name = "chk肥胖";
            this.chk肥胖.Properties.Caption = "E: 肥胖";
            this.chk肥胖.Size = new System.Drawing.Size(75, 19);
            this.chk肥胖.TabIndex = 4;
            // 
            // chk重度吸烟
            // 
            this.chk重度吸烟.Location = new System.Drawing.Point(568, 3);
            this.chk重度吸烟.Name = "chk重度吸烟";
            this.chk重度吸烟.Properties.Caption = "F: 重度吸烟";
            this.chk重度吸烟.Size = new System.Drawing.Size(93, 19);
            this.chk重度吸烟.TabIndex = 5;
            // 
            // chk超重且中心型肥胖
            // 
            this.chk超重且中心型肥胖.Location = new System.Drawing.Point(667, 3);
            this.chk超重且中心型肥胖.Name = "chk超重且中心型肥胖";
            this.chk超重且中心型肥胖.Properties.Caption = "G: 超重且中心型肥胖";
            this.chk超重且中心型肥胖.Size = new System.Drawing.Size(147, 19);
            this.chk超重且中心型肥胖.TabIndex = 6;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17,
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(935, 92);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.flowLayoutPanel8;
            this.layoutControlItem17.CustomizationFormText = "高危：";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(145, 30);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(915, 30);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "高危：";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 30);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(915, 42);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btnEmpty);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 125);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(945, 28);
            this.flowLayoutPanel1.TabIndex = 13;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(100, 0);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(100, 0, 3, 0);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(71, 28);
            this.btnQuery.TabIndex = 5;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(177, 0);
            this.btnEmpty.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(80, 28);
            this.btnEmpty.TabIndex = 9;
            this.btnEmpty.Text = "清空";
            this.btnEmpty.Visible = false;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.sbtnCancel);
            this.panelControl4.Controls.Add(this.sbtnOK);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl4.Location = new System.Drawing.Point(0, 456);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(945, 38);
            this.panelControl4.TabIndex = 14;
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.sbtnCancel.Appearance.Options.UseFont = true;
            this.sbtnCancel.Location = new System.Drawing.Point(803, 10);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(75, 23);
            this.sbtnCancel.TabIndex = 0;
            this.sbtnCancel.Text = "取消";
            this.sbtnCancel.Click += new System.EventHandler(this.sbtnCancel_Click);
            // 
            // sbtnOK
            // 
            this.sbtnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.sbtnOK.Appearance.Options.UseFont = true;
            this.sbtnOK.Location = new System.Drawing.Point(676, 10);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(75, 23);
            this.sbtnOK.TabIndex = 0;
            this.sbtnOK.Text = "确定";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // Frm签约档案查询
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 494);
            this.Controls.Add(this.gc个人健康档案);
            this.Controls.Add(this.pagerControl1);
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.tabSearchCondition);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Frm签约档案查询";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "签约--健康档案查询";
            this.Load += new System.EventHandler(this.Frm签约档案查询_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabSearchCondition)).EndInit();
            this.tabSearchCondition.ResumeLayout(false);
            this.tab_基础.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生时间1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.tab_重点.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            this.flowLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk听力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk言语残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肢体残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk智力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk视力残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾All.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2型糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肿瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk慢阻肺.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重症精神病.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio重点人群.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            this.tab_高危.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk临界高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血脂边缘升高.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk空腹血糖升高.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk糖耐量异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肥胖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重度吸烟.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk超重且中心型肥胖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Library.UserControls.DataGridControl gc个人健康档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv个人健康档案;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案号码;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col居住地址;
        private DevExpress.XtraGrid.Columns.GridColumn col本人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col联系人姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col联系人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn col创建人;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn col档案状态;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraTab.XtraTabControl tabSearchCondition;
        private DevExpress.XtraTab.XtraTabPage tab_基础;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit dte调查时间1;
        private DevExpress.XtraEditors.DateEdit dte调查时间2;
        private DevExpress.XtraEditors.DateEdit dte录入时间2;
        private DevExpress.XtraEditors.DateEdit dte录入时间1;
        private DevExpress.XtraEditors.DateEdit dte出生时间2;
        private DevExpress.XtraEditors.DateEdit dte出生时间1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEdit机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo性别;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraTab.XtraTabPage tab_重点;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraEditors.CheckEdit chk听力残疾;
        private DevExpress.XtraEditors.CheckEdit chk言语残疾;
        private DevExpress.XtraEditors.CheckEdit chk肢体残疾;
        private DevExpress.XtraEditors.CheckEdit chk智力残疾;
        private DevExpress.XtraEditors.CheckEdit chk视力残疾;
        private DevExpress.XtraEditors.CheckEdit chk残疾All;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit chk高血压;
        private DevExpress.XtraEditors.CheckEdit chk2型糖尿病;
        private DevExpress.XtraEditors.CheckEdit chk冠心病;
        private DevExpress.XtraEditors.CheckEdit chk脑卒中;
        private DevExpress.XtraEditors.CheckEdit chk肿瘤;
        private DevExpress.XtraEditors.CheckEdit chk慢阻肺;
        private DevExpress.XtraEditors.CheckEdit chk重症精神病;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.RadioGroup radio重点人群;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraTab.XtraTabPage tab_高危;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private DevExpress.XtraEditors.CheckEdit chk临界高血压;
        private DevExpress.XtraEditors.CheckEdit chk血脂边缘升高;
        private DevExpress.XtraEditors.CheckEdit chk空腹血糖升高;
        private DevExpress.XtraEditors.CheckEdit chk糖耐量异常;
        private DevExpress.XtraEditors.CheckEdit chk肥胖;
        private DevExpress.XtraEditors.CheckEdit chk重度吸烟;
        private DevExpress.XtraEditors.CheckEdit chk超重且中心型肥胖;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton sbtnCancel;
        private DevExpress.XtraEditors.SimpleButton sbtnOK;
    }
}