﻿namespace AtomEHR.签约服务.签约服务管理
{
    partial class Frm解约审批处理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbo审批意见 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.med审批备注 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.sbtn确定 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn取消 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.cbo审批意见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.med审批备注.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // cbo审批意见
            // 
            this.cbo审批意见.Location = new System.Drawing.Point(100, 31);
            this.cbo审批意见.Name = "cbo审批意见";
            this.cbo审批意见.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo审批意见.Properties.Items.AddRange(new object[] {
            "",
            "允许解约",
            "不允许解约"});
            this.cbo审批意见.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo审批意见.Size = new System.Drawing.Size(323, 20);
            this.cbo审批意见.TabIndex = 0;
            // 
            // med审批备注
            // 
            this.med审批备注.Location = new System.Drawing.Point(100, 70);
            this.med审批备注.Name = "med审批备注";
            this.med审批备注.Size = new System.Drawing.Size(323, 204);
            this.med审批备注.TabIndex = 1;
            this.med审批备注.UseOptimizedRendering = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(34, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "审批意见：";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(34, 73);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "审批备注：";
            // 
            // sbtn确定
            // 
            this.sbtn确定.Location = new System.Drawing.Point(123, 328);
            this.sbtn确定.Name = "sbtn确定";
            this.sbtn确定.Size = new System.Drawing.Size(101, 23);
            this.sbtn确定.TabIndex = 3;
            this.sbtn确定.Text = "确定";
            this.sbtn确定.Click += new System.EventHandler(this.sbtn确定_Click);
            // 
            // sbtn取消
            // 
            this.sbtn取消.Location = new System.Drawing.Point(270, 328);
            this.sbtn取消.Name = "sbtn取消";
            this.sbtn取消.Size = new System.Drawing.Size(101, 23);
            this.sbtn取消.TabIndex = 3;
            this.sbtn取消.Text = "取消";
            this.sbtn取消.Click += new System.EventHandler(this.sbtn取消_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(100, 280);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(141, 14);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "填写内容限定在200字以内";
            // 
            // Frm解约审批处理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 387);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.sbtn取消);
            this.Controls.Add(this.sbtn确定);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.med审批备注);
            this.Controls.Add(this.cbo审批意见);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm解约审批处理";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "解约审批处理";
            ((System.ComponentModel.ISupportInitialize)(this.cbo审批意见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.med审批备注.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit cbo审批意见;
        private DevExpress.XtraEditors.MemoEdit med审批备注;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton sbtn确定;
        private DevExpress.XtraEditors.SimpleButton sbtn取消;
        private DevExpress.XtraEditors.LabelControl labelControl3;
    }
}