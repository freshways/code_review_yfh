﻿using AtomEHR.Common;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using AtomEHR.Business.Security;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class Frm签约档案查询 : frmBase
    {
        AtomEHR.Business.bll健康档案 _BLL = new AtomEHR.Business.bll健康档案();// 业务逻辑层实例
        public Frm签约档案查询()
        {
            InitializeComponent();
        }

        private string _strWhere;
        BackgroundWorker back = new BackgroundWorker();

        #region Handler Events
        private void Frm签约档案查询_Load(object sender, EventArgs e)
        {
            InitView();//初始化页面操作

            //tpDetail.Hide();
            this.gc个人健康档案.UseCheckBox = true;
            this.pagerControl1.OnPageChanged += pagerControl1_OnPageChanged;
            back.DoWork += back_DoWork;
            back.RunWorkerCompleted += back_RunWorkerCompleted;
            //back.WorkerReportsProgress = true;
            this.pagerControl1.Height = 35;
        }
        void back_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataSet ds = e.Result as DataSet;
            this.gc个人健康档案.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv个人健康档案.BestFitColumns();
            this.btnQuery.Enabled = true;
        }
        void back_DoWork(object sender, DoWorkEventArgs e)
        {

            _strWhere = string.Empty;
            this.pagerControl1.InitControl();
            string pgrid = this.treeListLookUpEdit机构.EditValue.ToString();
            if (this.checkEdit21.Checked)//包含下属机构
            {
                if (pgrid.Length == 12)
                {
                    #region  所属机构关联（未用）
                    //_strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'D'+substring([所属机构],9,7) like '" + pgrid + "%')";
                    //_strWhere += " and ( [所属机构] in (select 机构编号 from dbo.fn_GetRGID('" + pgrid + "') ))";
                    //_strWhere += " and ([所属机构]=  '" + pgrid + "' or [所属机构] in (select 机构编号 from tb_机构信息 where 上级机构='" + pgrid + "' ) )";
                    //_strWhere += "and ([所属机构]='" + pgrid + "' or [所属机构] in (select 机构编号 from tb_机构信息 where 所属机构 in (select 机构编号 from tb_机构信息 where 上级机构='" + pgrid + "')))";//多级查询
                    #endregion
                    _strWhere += " AND [所属机构] IN (select 机构编号 from tb_机构信息 where 上级机构='" + pgrid + "' or 机构编号='" + pgrid + "' ) ";
                }
                else
                {
                    _strWhere += " and [所属机构] like '" + pgrid + "%'";
                }
            }
            else
            {
                _strWhere += " and [所属机构] ='" + pgrid + "'";
            }
            if (!string.IsNullOrEmpty(this.txt姓名.Text.Trim()))
            {
                _strWhere += " and (姓名 LIKE '" + DESEncrypt.DES加密(this.txt姓名.Text.Trim()) + "%'  or 姓名 LIKE '" + this.txt姓名.Text.Trim() + "%') ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo性别)))
            {
                _strWhere += " and 性别编码 ='" + util.ControlsHelper.GetComboxKey(cbo性别) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.txt档案号.Text.Trim()))
            {
                _strWhere += " and 个人档案编号 LIKE '%" + this.txt档案号.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.txt身份证号.Text.Trim()))
            {
                _strWhere += " and 身份证号 LIKE '%" + this.txt身份证号.Text.Trim() + "%'  ";
            }
            if (!string.IsNullOrEmpty(this.dte出生时间1.Text.Trim()))
            {
                _strWhere += " and 出生日期 >= '" + this.dte出生时间1.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte出生时间2.Text.Trim()))
            {
                _strWhere += " and 出生日期 <= '" + this.dte出生时间2.Text.Trim() + "'  ";
            }
            if (!string.IsNullOrEmpty(this.dte录入时间1.Text.Trim()))
            {
                _strWhere += " and 创建时间 >= '" + this.dte录入时间1.Text.Trim() + " 00:00:00'  ";
            }
            if (!string.IsNullOrEmpty(this.dte录入时间2.Text.Trim()))
            {
                _strWhere += " and 创建时间 <= '" + this.dte录入时间2.Text.Trim() + " 23:59:59'  ";
            }
            if (!string.IsNullOrEmpty(this.dte调查时间1.Text.Trim()))
            {
                _strWhere += " and 调查时间 >= '" + this.dte调查时间1.Text.Trim() + " 00:00:00'  ";
            }
            if (!string.IsNullOrEmpty(this.dte调查时间2.Text.Trim()))
            {
                _strWhere += " and 调查时间 <= '" + this.dte调查时间2.Text.Trim() + " 23:59:59'  ";
            }
            
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit镇)))
            {
                _strWhere += " and 街道编码 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit村)))
            {
                _strWhere += " and 居委会编码 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'  ";
            }


            //重点人群
            if (this.radio重点人群.EditValue != null && this.radio重点人群.EditValue.ToString() != "")
            {
                string age = this.radio重点人群.EditValue.ToString();
                if (age == "3")//儿童0-三岁
                {
                    string date = DateTime.Now.AddYears(-4).ToString("yyyy-MM-dd");
                    _strWhere += "  and  出生日期 >= '" + date + "' ";
                }
                else if (age == "6")//儿童0-6岁
                {
                    string date = DateTime.Now.AddYears(-7).ToString("yyyy-MM-dd");
                    //_strWhere += "  and datediff(year, 出生日期 ,GETDATE())<6 ";
                    _strWhere += "  and  出生日期 >= '" + date + "' ";
                }
                else if (age == "15")//育龄妇女
                {
                    string date1 = DateTime.Now.AddYears(-15).ToString("yyyy-MM-dd");
                    string date2 = DateTime.Now.AddYears(-49).ToString("yyyy-MM-dd");
                    //_strWhere += "  and datediff(year, 出生日期 ,GETDATE())>15 and 性别  = '女' ";
                    _strWhere += "  and 出生日期 <= '" + date1 + "'  and 出生日期 >= '" + date2 + "' and 性别  = '女' ";
                }
                else if (age == "23")//孕产妇
                {
                    _strWhere += "  and 孕产妇基本信息表 is not null  and 孕产妇基本信息表<>''  ";
                    //_strWhere += "  and 个人档案编号 in (select 个人档案编号 from tb_孕妇基本信息 group by 个人档案编号)  and  (怀孕情况 = '已孕未生产' or 怀孕情况 = '已生产随访期内')";
                }
                else if (age == "65")//65岁及以上老年人 
                {
                    string date = DateTime.Now.AddYears(-65).ToString("yyyy-MM-dd");
                    //_strWhere += "  and datediff(year, 出生日期 ,GETDATE())<6 ";
                    _strWhere += "  and  substring(出生日期,0,5) <= '" + date + "' ";
                }
            }
            if (chk高血压.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB高血压管理卡 where isnull(tb_MXB高血压管理卡.终止管理,'')<>'1') ";
            }
            if (chk2型糖尿病.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB糖尿病管理卡 where isnull(tb_MXB糖尿病管理卡.终止管理,'')<>'1') ";
            }
            if (chk冠心病.Checked)
            {
                _strWhere += " and  个人档案编号 in  ( select 个人档案编号 from tb_MXB冠心病管理卡 where isnull(tb_MXB冠心病管理卡.终止管理,'')<>'1') ";
            }
            if (chk脑卒中.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB脑卒中管理卡 where isnull(tb_MXB脑卒中管理卡.终止管理,'')<>'1')  ";
            }
            if (chk肿瘤.Checked)
            {
                _strWhere += " and 是否肿瘤 =  '1' ";
            }
            if (chk慢阻肺.Checked)
            {
                _strWhere += " and 是否慢阻肺 =  '1' ";
            }
            if (chk重症精神病.Checked)
            {
                _strWhere += " and isnull(精神疾病信息补充表,'') <> '' ";
                //_strWhere += " and 精神疾病信息补充表 <>  '' and 精神疾病信息补充表 is not null ";
            }
            if (chk残疾All.Checked)
            {
                _strWhere += " and (isnull(听力言语残疾随访表,'') <> '' or  isnull(肢体残疾随访表,'') <> '' or  isnull(智力残疾随访表,'') <> '' or  isnull(视力残疾随访表,'') <>  ''  or isnull(精神疾病信息补充表,'')<>'' ) ";
            }
            if (chk听力残疾.Checked)
            {
                _strWhere += " and isnull(听力言语残疾随访表,'') <>  '' ";
            }
            if (chk言语残疾.Checked)
            {
                _strWhere += " and  isnull(听力言语残疾随访表,'') <>  '' and  残疾情况='3' ";
            }
            if (chk肢体残疾.Checked)
            {
                _strWhere += " and isnull(肢体残疾随访表,'') <>  '' ";
            }
            if (chk智力残疾.Checked)
            {
                _strWhere += " and isnull(智力残疾随访表,'') <>  ''  ";
            }
            if (chk视力残疾.Checked)
            {
                _strWhere += " and isnull(视力残疾随访表,'') <>  ''  ";
            }

            //高危人群

            if (chk临界高血压.Checked)
            {
                _strWhere += " and 是否高血压 =  '2' ";
            }
            if (chk血脂边缘升高.Checked)
            {
                _strWhere += " and 是否血脂 =  '1' ";
            }
            if (chk空腹血糖升高.Checked)
            {
                _strWhere += " and 是否空腹血糖 =  '1' ";
            }
            //餐后2小时血糖，超过正常的7.8mmol/L，但仍未达到11.1mmol/L的糖尿病诊断标准（或空腹血糖升高，未达到糖尿病的诊断标准，即空腹血糖在6.2~7.0之间）称糖耐量异常（或空腹葡萄糖受损）
            if (chk糖耐量异常.Checked)
            {
                _strWhere += " and( 是否餐后血糖 =  '1' or  是否空腹血糖 =  '1'  and 是否糖尿病 <> '1' )";
            }
            if (chk肥胖.Checked)
            {
                _strWhere += " and 是否肥胖 =  '1' ";
            }
            if (chk重度吸烟.Checked)
            {
                _strWhere += " and 是否重度吸烟 =  '1' ";
            }
            if (chk超重且中心型肥胖.Checked)
            {
                _strWhere += " and 是否超重肥胖 =  '1' ";
            }

            this.gc个人健康档案.View = "View_个人信息表";
            this.gc个人健康档案.StrWhere = _strWhere;


            DataSet ds = null;
            //if (this.cbo排序方式.Text.Trim() != "查询排序方式")
            //{
            //    if (this.cbo排序方式.Text.Trim() == "档案创建时间")
            //        ds = this.pagerControl1.GetQueryResult("View_个人信息表", "*", _strWhere, "创建时间", "DESC");
            //    if (this.cbo排序方式.Text.Trim() == "个人档案编号")
            //        ds = this.pagerControl1.GetQueryResult("View_个人信息表", "*", _strWhere, "个人档案编号", "DESC");
            //    if (this.cbo排序方式.Text.Trim() == "家庭档案编号")
            //        ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "家庭档案编号", "DESC");
            //}
            //else
            //{
                ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "创建时间", "DESC");
            //}
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.姓名] = DESEncrypt.DES解密(ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.姓名].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.档案状态] = _BLL.ReturnDis字典显示("rkx_dazt", ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.档案状态].ToString());
                //ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.所属机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.所属机构].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.创建机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.创建机构].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.创建人].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.居住地址] = ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.区].ToString() + ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.街道] + ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.居委会] + ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.居住地址];
            }
            e.Result = ds;
            //this.gc个人健康档案.DataSource = ds.Tables[0];
            //this.pagerControl1.DrawControl();
            //BindData();
        }
        void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// 绑定数据
        /// </summary>
        private void BindData()
        {
            this.gc个人健康档案.View = "View_个人信息表";
            this.gc个人健康档案.StrWhere = _strWhere;

            DataSet ds = this.pagerControl1.GetQueryResultNew("View_个人信息表", "*", _strWhere, "创建时间", "DESC");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.姓名] = DESEncrypt.DES解密(ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.姓名].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.档案状态] = _BLL.ReturnDis字典显示("rkx_dazt", ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.档案状态].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.所属机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.所属机构].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.创建机构] = _BLL.Return机构名称(ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.创建机构].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.创建人] = _BLL.Return用户名称(ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.创建人].ToString());
                ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.居住地址] = ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.市].ToString() + ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.区] + ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.街道] + ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.居委会] + ds.Tables[0].Rows[i][AtomEHR.Models.tb_健康档案.居住地址];
            }
            this.gc个人健康档案.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv个人健康档案.BestFitColumns();//列自适应宽度         
        }
        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(AtomEHR.Business.DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
        private bool funCheck()
        {
            if (!string.IsNullOrEmpty(this.dte出生时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte出生时间2.Text.Trim()) && this.dte出生时间1.DateTime > this.dte出生时间2.DateTime)
            {
                Msg.ShowError("出生日期 结束时间不能小于开始时间，请重新选择！");
                this.dte出生时间2.Text = "";
                this.dte出生时间2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.dte调查时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte调查时间2.Text.Trim()) && this.dte调查时间1.DateTime > this.dte调查时间2.DateTime)
            {
                Msg.ShowError("调查时间 结束时间不能小于开始时间，请重新选择！");
                this.dte调查时间2.Text = "";
                this.dte调查时间2.Focus();
                return false;
            }
            if (!string.IsNullOrEmpty(this.dte录入时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte录入时间2.Text.Trim()) && this.dte录入时间1.DateTime > this.dte录入时间2.DateTime)
            {
                Msg.ShowError("录入时间 结束时间不能小于开始时间，请重新选择！");
                this.dte录入时间2.Text = "";
                this.dte录入时间2.Focus();
                return false;
            }
            //if (!string.IsNullOrEmpty(this.dte最近更新时间1.Text.Trim()) && !string.IsNullOrEmpty(this.dte最近更新时间2.Text.Trim()) && this.dte最近更新时间1.DateTime > this.dte最近更新时间2.DateTime)
            //{
            //    Msg.ShowError("最近更新时间 结束时间不能小于开始时间，请重新选择！");
            //    this.dte最近更新时间2.Text = "";
            //    this.dte最近更新时间2.Focus();
            //    return false;
            //}
            return true;
        }

        #endregion

        #region Private Methods
        private void InitView()
        {
            util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t性别, cbo性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案状态, cbo档案状态, "P_CODE", "P_DESC");
            //cbo档案状态.SelectedIndex = 1;
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo合格档案, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t职业, cbo职业, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t常住类型, cbo常住类型, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t民族, cbo民族, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t婚姻状况, cbo婚姻状况, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t文化程度, cbo文化程度, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t血型, cbo血型, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.tRH, cboRH, "P_CODE", "P_DESC");
            //为“镇”绑定信息
            DataView dv镇 = new DataView(AtomEHR.Business.DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码='" + Loginer.CurrentUser.单位代码 + "' ";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");

            //为“所属机构"绑定信息
            try
            {
                AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();

                //TODO:  需要修改机构编号的地方
                //DataTable dt所属机构 = bll机构.Get机构树("371321C21008");

                string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                DataTable dt所属机构 = bll机构.Get机构树(ssjg);

                treeListLookUpEdit机构.Properties.ValueMember = "机构编号";
                treeListLookUpEdit机构.Properties.DisplayMember = "机构名称";

                treeListLookUpEdit机构.Properties.TreeList.KeyFieldName = "机构编号";
                treeListLookUpEdit机构.Properties.TreeList.ParentFieldName = "上级机构";
                treeListLookUpEdit机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                treeListLookUpEdit机构.Properties.DataSource = dt所属机构;
                //treeListLookUpEdit机
                //treeListLookUpEdit机构.Properties.TreeList.CollapseAll();

                treeListLookUpEdit机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                treeListLookUpEdit机构.Text = Loginer.CurrentUser.所属机构;
            }
            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                treeListLookUpEdit机构.Properties.AutoExpandAllNodes = false;
            }
        }

        #endregion

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                try
                {
                    this.btnQuery.Enabled = false;
                    back.RunWorkerAsync();
                }
                catch (Exception ex)
                {
                    Msg.ShowInformation(ex.Message);
                }
            }
        }

        private void sbtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        public string SelectedDabh = string.Empty;
        public string SelectedName = string.Empty;
        public string SelectedTelNo = string.Empty;
        public string SelectedCardNo = string.Empty;
        public string selectedAddress = string.Empty;
        public string selectedSsjg = string.Empty;
        private void sbtnOK_Click(object sender, EventArgs e)
        {
            int[] selectedIndexs = gv个人健康档案.GetSelectedRows();
            if(selectedIndexs.Length == 0 )
            {
                Msg.ShowInformation("请勾选需要签约的健康档案");
                return;
            }
            if(selectedIndexs.Length > 1)
            {
                Msg.ShowInformation("勾选数量只能为1");
                return;
            }

            object dabh = gv个人健康档案.GetRowCellValue(selectedIndexs[0], AtomEHR.Models.tb_健康档案.个人档案编号);

            ////验证此人是否已经签约
            //bool checkresult = bll签约.CheckValidateExists(dabh.ToString());
            //if(checkresult)
            //{
            //    Msg.ShowInformation("此人已经存在签约信息，不允许再次签约。");
            //    return;
            //}
            object ssjg = gv个人健康档案.GetRowCellValue(selectedIndexs[0], AtomEHR.Models.tb_健康档案.所属机构);
            object name = gv个人健康档案.GetRowCellValue(selectedIndexs[0], AtomEHR.Models.tb_健康档案.姓名);
            object telNo = gv个人健康档案.GetRowCellValue(selectedIndexs[0], AtomEHR.Models.tb_健康档案.本人电话);
            object CardNo = gv个人健康档案.GetRowCellValue(selectedIndexs[0], AtomEHR.Models.tb_健康档案.身份证号);
            object address = gv个人健康档案.GetRowCellValue(selectedIndexs[0], AtomEHR.Models.tb_健康档案.居住地址);
            
            SelectedDabh = (dabh == null) ? "" : dabh.ToString();
            SelectedName = (name == null) ? "" : name.ToString();
            SelectedTelNo = (telNo == null) ? "" : telNo.ToString();
            SelectedCardNo = (CardNo == null) ? "" : CardNo.ToString();
            selectedAddress = (address == null) ? "" : address.ToString();
            selectedSsjg = (ssjg == null) ? "" : ssjg.ToString();

            if(isAllowSignMore())
            {
            }
            else
            {
                //验证效期内是否有有签约记录
                bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
                int signedCount = bll签约.GetValidateSignedCountByDah(SelectedDabh);
                if(signedCount>0)
                {
                    Msg.ShowInformation("此人已签约过，有效期内不允许再次签约。如需变更签约内容，请联系家庭医生负责人进行咨询。");
                    return;
                }
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private bool isAllowSignMore()
        {
            bllJTYS全局参数Local bllglocal = new bllJTYS全局参数Local();
            string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);

            string value = bllglocal.GetValueByCode(ssjg, "signMore");

            bool ret = false;
            if(value=="1" || string.IsNullOrWhiteSpace(value))
            {
                ret = true;
            }
            else
            {
                ret = false;
            }

            return ret;
        }
    }
}
