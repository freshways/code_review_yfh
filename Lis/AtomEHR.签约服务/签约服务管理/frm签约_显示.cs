﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class frm签约_显示 : frmBase
    {
        public frm签约_显示()
        {
            InitializeComponent();
        }

        string m_qyid;
        public frm签约_显示(string qyid)
        {
            InitializeComponent();
            m_qyid = qyid;
            InitForm();
        }

        //bllJTYS签约对象类型 bll签约对象 = new bllJTYS签约对象类型();
        //bllJTYS健康状况 bll健康状况 = new bllJTYS健康状况();
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        //bllJTYS团队概要 bll团队 = new bllJTYS团队概要();
        //bllJTYS档案VS磁卡 bll档案磁卡 = new bllJTYS档案VS磁卡();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        AtomEHR.Business.bll健康档案 bll档案 = new AtomEHR.Business.bll健康档案();
        bllJTYS团队成员 bll成员 = new bllJTYS团队成员();
        bllJTYS签约医生签字 bll医生签字 = new bllJTYS签约医生签字();

        void InitForm()
        {
            //判断此人是否为贫困人口
            //查询是否为贫困人口
            bool isPK = false;
            if (isPK)
            {
                emptySpaceItem贫困人口.Text = "此人可能属于贫困人口";
            }
            else
            {
                emptySpaceItem贫困人口.Text = " ";
            }
        }

        private void frm签约_显示_Load(object sender, EventArgs e)
        {
            //this.ActiveControl = txtMagCard;
            DataSet ds = bll签约.GetBusinessByIdFromView(m_qyid);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                txtDabh.Text = dt.Rows[0][tb_JTYS签约信息.档案号].ToString();
                txtName.Text = dt.Rows[0][AtomEHR.Models.tb_健康档案.姓名].ToString();
                txtTelNo.Text = dt.Rows[0][tb_JTYS签约信息.联系电话].ToString();
                txtAddress.Text = bll档案.GetAddressByDAH(txtDabh.Text);

                txtMagCard.Text = dt.Rows[0][tb_JTYS档案VS磁卡.磁卡号].ToString();
                txtCardID.Text = dt.Rows[0][AtomEHR.Models.tb_健康档案.身份证号].ToString();
                txtServiceObject.Text = dt.Rows[0]["签约对象类型名称"].ToString();
                txtHealthCondition.Text = dt.Rows[0]["健康状况名称"].ToString();
                
                //服务团队
                txtServiceTeam.Text = dt.Rows[0]["团队名称"].ToString();
                txt签约日期.Text = dt.Rows[0][tb_JTYS签约信息.签约日期].ToString();
                //txt签约人.Text = dt.Rows[0]["签约医生姓名"].ToString();
                txt状态.Text = dt.Rows[0]["签约状态"].ToString();
                txt创建人.Text = dt.Rows[0]["创建人姓名"].ToString();
                txt创建时间.Text = dt.Rows[0][tb_JTYS签约信息.创建时间].ToString();
                txt修改人.Text = dt.Rows[0]["修改人姓名"].ToString();
                txt修改时间.Text = dt.Rows[0][tb_JTYS签约信息.修改时间].ToString();

                string serviceIDs = dt.Rows[0][tb_JTYS签约信息.签约服务包].ToString();
                string[] arrServices = serviceIDs.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                if(arrServices.Length > 0)
                {
                    DataTable dtServicePackage = bll服务包.GetServicePackagesByIDList(arrServices);
                    gcServicePackage.DataSource = dtServicePackage;
                }

                //获取签约人信息
                if (Loginer.CurrentUser.所属机构.StartsWith("371321"))
                {
                    txt签约人.Text = dt.Rows[0]["签约医生姓名"].ToString();
                }
                else
                {
                    try
                    {
                        string QYRID = dt.Rows[0][tb_JTYS签约信息.签约人].ToString();
                        if (string.IsNullOrWhiteSpace(QYRID))
                        { }
                        else
                        {
                            DataTable dt成员 = bll成员.GetDataByKey(QYRID);
                            txt签约人.Text = dt成员.Rows[0][tb_JTYS团队成员.成员姓名].ToString();
                        }
                    }
                    catch
                    {

                    }
                }

                string SQ = dt.Rows[0][tb_JTYS签约信息.手签照].ToString();
                if (!string.IsNullOrEmpty(SQ) && SQ.Length > 64)
                {
                    Byte[] bitmapData = new Byte[SQ.Length];
                    bitmapData = Convert.FromBase64String(SQ);
                    System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);
                    this.pice签名.Image = Image.FromStream(streamBitmap);
                }

                //新增20181122
                DataSet ds医生签字 = bll医生签字.GetBusinessBy费用ID(dt.Rows[0][tb_JTYS签约信息.费用ID].ToString(), false);
                if (ds医生签字 != null && ds医生签字.Tables.Count > 0 && ds医生签字.Tables[0].Rows.Count > 0)
                {
                    string yssqz = ds医生签字.Tables[0].Rows[0][tb_JTYS签约医生签字.签字].ToString();
                    if (!string.IsNullOrWhiteSpace(yssqz) && yssqz.Length > 64)
                    {
                        Byte[] bitmapData = new Byte[yssqz.Length];
                        bitmapData = Convert.FromBase64String(yssqz);
                        System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);
                        this.pictureEdit1.Image = Image.FromStream(streamBitmap);
                    }
                }
            }
        }
    }
}

