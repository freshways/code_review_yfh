﻿namespace AtomEHR.签约服务.签约服务管理
{
    partial class frm签约
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.sbtnGetFinger = new DevExpress.XtraEditors.SimpleButton();
            this.peFinger = new DevExpress.XtraEditors.PictureEdit();
            this.rgp签约医生 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cboeHealthCondition = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.lookUpEditServiceTeam = new DevExpress.XtraEditors.LookUpEdit();
            this.txtMagCard = new DevExpress.XtraEditors.TextEdit();
            this.txt签约人 = new DevExpress.XtraEditors.TextEdit();
            this.gcServicePackage = new DevExpress.XtraGrid.GridControl();
            this.gvServicePackage = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服务包名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服务内容 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.col年收费标准 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol优惠方式 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEdit签约 = new DevExpress.XtraEditors.DateEdit();
            this.txtTelNo = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtCardID = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtDabh = new DevExpress.XtraEditors.TextEdit();
            this.lueServiceObject = new DevExpress.XtraEditors.LookUpEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem贫困人口 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn取消 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnYSFinger = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peFinger.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgp签约医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboeHealthCondition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditServiceTeam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMagCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcServicePackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvServicePackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签约.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签约.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDabh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceObject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem贫困人口)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup1.CustomizationFormText = "签约服务项目执行";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(757, 516);
            this.layoutControlGroup1.Text = "签约服务项目执行";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "签约服务项目执行";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.Size = new System.Drawing.Size(757, 516);
            this.layoutControlGroup2.Text = "签约服务项目执行";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.sbtnYSFinger);
            this.layoutControl2.Controls.Add(this.pictureEdit1);
            this.layoutControl2.Controls.Add(this.sbtnGetFinger);
            this.layoutControl2.Controls.Add(this.peFinger);
            this.layoutControl2.Controls.Add(this.rgp签约医生);
            this.layoutControl2.Controls.Add(this.labelControl1);
            this.layoutControl2.Controls.Add(this.cboeHealthCondition);
            this.layoutControl2.Controls.Add(this.lookUpEditServiceTeam);
            this.layoutControl2.Controls.Add(this.txtMagCard);
            this.layoutControl2.Controls.Add(this.txt签约人);
            this.layoutControl2.Controls.Add(this.gcServicePackage);
            this.layoutControl2.Controls.Add(this.dateEdit签约);
            this.layoutControl2.Controls.Add(this.txtTelNo);
            this.layoutControl2.Controls.Add(this.txtAddress);
            this.layoutControl2.Controls.Add(this.txtCardID);
            this.layoutControl2.Controls.Add(this.txtName);
            this.layoutControl2.Controls.Add(this.txtDabh);
            this.layoutControl2.Controls.Add(this.lueServiceObject);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(606, 301, 250, 350);
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(853, 616);
            this.layoutControl2.TabIndex = 127;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(451, 468);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ReadOnly = true;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(111, 136);
            this.pictureEdit1.StyleController = this.layoutControl2;
            this.pictureEdit1.TabIndex = 18;
            // 
            // sbtnGetFinger
            // 
            this.sbtnGetFinger.Location = new System.Drawing.Point(800, 468);
            this.sbtnGetFinger.Name = "sbtnGetFinger";
            this.sbtnGetFinger.Size = new System.Drawing.Size(41, 136);
            this.sbtnGetFinger.StyleController = this.layoutControl2;
            this.sbtnGetFinger.TabIndex = 17;
            this.sbtnGetFinger.Text = "录\r\n入\r\n指\r\n纹";
            this.sbtnGetFinger.Click += new System.EventHandler(this.sbtnGetFinger_Click);
            // 
            // peFinger
            // 
            this.peFinger.Location = new System.Drawing.Point(683, 468);
            this.peFinger.Name = "peFinger";
            this.peFinger.Properties.ReadOnly = true;
            this.peFinger.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.peFinger.Size = new System.Drawing.Size(113, 136);
            this.peFinger.StyleController = this.layoutControl2;
            this.peFinger.TabIndex = 16;
            // 
            // rgp签约医生
            // 
            this.rgp签约医生.Location = new System.Drawing.Point(99, 468);
            this.rgp签约医生.Name = "rgp签约医生";
            this.rgp签约医生.Size = new System.Drawing.Size(273, 88);
            this.rgp签约医生.StyleController = this.layoutControl2;
            this.rgp签约医生.TabIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(721, 444);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(120, 14);
            this.labelControl1.StyleController = this.layoutControl2;
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "请选择自己所属的团队";
            // 
            // cboeHealthCondition
            // 
            this.cboeHealthCondition.Location = new System.Drawing.Point(99, 111);
            this.cboeHealthCondition.Name = "cboeHealthCondition";
            this.cboeHealthCondition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboeHealthCondition.Properties.DropDownRows = 12;
            this.cboeHealthCondition.Size = new System.Drawing.Size(742, 20);
            this.cboeHealthCondition.StyleController = this.layoutControl2;
            this.cboeHealthCondition.TabIndex = 13;
            // 
            // lookUpEditServiceTeam
            // 
            this.lookUpEditServiceTeam.Location = new System.Drawing.Point(99, 444);
            this.lookUpEditServiceTeam.Name = "lookUpEditServiceTeam";
            this.lookUpEditServiceTeam.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditServiceTeam.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("团队名称", "团队名称"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("组长姓名", "团队组长")});
            this.lookUpEditServiceTeam.Size = new System.Drawing.Size(618, 20);
            this.lookUpEditServiceTeam.StyleController = this.layoutControl2;
            this.lookUpEditServiceTeam.TabIndex = 12;
            this.lookUpEditServiceTeam.EditValueChanged += new System.EventHandler(this.lookUpEditServiceTeam_EditValueChanged);
            // 
            // txtMagCard
            // 
            this.txtMagCard.EditValue = "";
            this.txtMagCard.Location = new System.Drawing.Point(99, 63);
            this.txtMagCard.Name = "txtMagCard";
            this.txtMagCard.Size = new System.Drawing.Size(207, 20);
            this.txtMagCard.StyleController = this.layoutControl2;
            this.txtMagCard.TabIndex = 11;
            // 
            // txt签约人
            // 
            this.txt签约人.Location = new System.Drawing.Point(99, 584);
            this.txt签约人.Name = "txt签约人";
            this.txt签约人.Properties.ReadOnly = true;
            this.txt签约人.Size = new System.Drawing.Size(273, 20);
            this.txt签约人.StyleController = this.layoutControl2;
            this.txt签约人.TabIndex = 10;
            // 
            // gcServicePackage
            // 
            this.gcServicePackage.Location = new System.Drawing.Point(99, 135);
            this.gcServicePackage.MainView = this.gvServicePackage;
            this.gcServicePackage.Name = "gcServicePackage";
            this.gcServicePackage.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.gcServicePackage.Size = new System.Drawing.Size(742, 305);
            this.gcServicePackage.TabIndex = 7;
            this.gcServicePackage.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvServicePackage});
            // 
            // gvServicePackage
            // 
            this.gvServicePackage.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.col服务包名称,
            this.gridColumn1,
            this.col服务内容,
            this.col年收费标准,
            this.gridColumn3,
            this.gcol优惠方式,
            this.gridColumn5});
            this.gvServicePackage.GridControl = this.gcServicePackage;
            this.gvServicePackage.Name = "gvServicePackage";
            this.gvServicePackage.OptionsSelection.MultiSelect = true;
            this.gvServicePackage.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gvServicePackage.OptionsView.ColumnAutoWidth = false;
            this.gvServicePackage.OptionsView.RowAutoHeight = true;
            this.gvServicePackage.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ServiceID";
            this.gridColumn2.FieldName = "ServiceID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // col服务包名称
            // 
            this.col服务包名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col服务包名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务包名称.Caption = "服务包名称";
            this.col服务包名称.FieldName = "名称";
            this.col服务包名称.Name = "col服务包名称";
            this.col服务包名称.OptionsColumn.AllowEdit = false;
            this.col服务包名称.OptionsColumn.ReadOnly = true;
            this.col服务包名称.Visible = true;
            this.col服务包名称.VisibleIndex = 1;
            this.col服务包名称.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "适宜对象";
            this.gridColumn1.FieldName = "适宜对象";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 96;
            // 
            // col服务内容
            // 
            this.col服务内容.AppearanceHeader.Options.UseTextOptions = true;
            this.col服务内容.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务内容.Caption = "服务内容";
            this.col服务内容.ColumnEdit = this.repositoryItemMemoEdit1;
            this.col服务内容.FieldName = "服务内容";
            this.col服务内容.Name = "col服务内容";
            this.col服务内容.OptionsColumn.AllowEdit = false;
            this.col服务内容.OptionsColumn.ReadOnly = true;
            this.col服务内容.Visible = true;
            this.col服务内容.VisibleIndex = 3;
            this.col服务内容.Width = 283;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // col年收费标准
            // 
            this.col年收费标准.AppearanceHeader.Options.UseTextOptions = true;
            this.col年收费标准.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col年收费标准.Caption = "年收费标准(元)";
            this.col年收费标准.FieldName = "年收费标准2";
            this.col年收费标准.Name = "col年收费标准";
            this.col年收费标准.OptionsColumn.AllowEdit = false;
            this.col年收费标准.OptionsColumn.ReadOnly = true;
            this.col年收费标准.Visible = true;
            this.col年收费标准.VisibleIndex = 4;
            this.col年收费标准.Width = 95;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "一年内运行多次签约";
            this.gridColumn3.FieldName = "允许一年多次";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            this.gridColumn3.Width = 100;
            // 
            // gcol优惠方式
            // 
            this.gcol优惠方式.Caption = "gridColumn4";
            this.gcol优惠方式.FieldName = "优惠方式";
            this.gcol优惠方式.Name = "gcol优惠方式";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "gridColumn5";
            this.gridColumn5.FieldName = "年收费标准";
            this.gridColumn5.Name = "gridColumn5";
            // 
            // dateEdit签约
            // 
            this.dateEdit签约.EditValue = null;
            this.dateEdit签约.Location = new System.Drawing.Point(99, 560);
            this.dateEdit签约.Name = "dateEdit签约";
            this.dateEdit签约.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit签约.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit签约.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit签约.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit签约.Size = new System.Drawing.Size(273, 20);
            this.dateEdit签约.StyleController = this.layoutControl2;
            this.dateEdit签约.TabIndex = 9;
            // 
            // txtTelNo
            // 
            this.txtTelNo.EditValue = "";
            this.txtTelNo.Location = new System.Drawing.Point(667, 39);
            this.txtTelNo.Name = "txtTelNo";
            this.txtTelNo.Size = new System.Drawing.Size(174, 20);
            this.txtTelNo.StyleController = this.layoutControl2;
            this.txtTelNo.TabIndex = 4;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(99, 87);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(476, 20);
            this.txtAddress.StyleController = this.layoutControl2;
            this.txtAddress.TabIndex = 3;
            // 
            // txtCardID
            // 
            this.txtCardID.Location = new System.Drawing.Point(397, 63);
            this.txtCardID.Name = "txtCardID";
            this.txtCardID.Properties.ReadOnly = true;
            this.txtCardID.Size = new System.Drawing.Size(179, 20);
            this.txtCardID.StyleController = this.layoutControl2;
            this.txtCardID.TabIndex = 2;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(397, 39);
            this.txtName.Name = "txtName";
            this.txtName.Properties.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(179, 20);
            this.txtName.StyleController = this.layoutControl2;
            this.txtName.TabIndex = 1;
            // 
            // txtDabh
            // 
            this.txtDabh.Location = new System.Drawing.Point(99, 39);
            this.txtDabh.Name = "txtDabh";
            this.txtDabh.Properties.ReadOnly = true;
            this.txtDabh.Size = new System.Drawing.Size(207, 20);
            this.txtDabh.StyleController = this.layoutControl2;
            this.txtDabh.TabIndex = 0;
            // 
            // lueServiceObject
            // 
            this.lueServiceObject.Location = new System.Drawing.Point(667, 63);
            this.lueServiceObject.Name = "lueServiceObject";
            this.lueServiceObject.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueServiceObject.Properties.NullText = "";
            this.lueServiceObject.Properties.PopupSizeable = false;
            this.lueServiceObject.Size = new System.Drawing.Size(174, 20);
            this.lueServiceObject.StyleController = this.layoutControl2;
            this.lueServiceObject.TabIndex = 5;
            this.lueServiceObject.EditValueChanged += new System.EventHandler(this.lueServiceObject_EditValueChanged);
            // 
            // Root
            // 
            this.Root.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.Root.AppearanceGroup.Options.UseFont = true;
            this.Root.AppearanceGroup.Options.UseTextOptions = true;
            this.Root.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem7,
            this.layoutControlItem11,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem13,
            this.emptySpaceItem贫困人口,
            this.layoutControlItem5,
            this.layoutControlItem14,
            this.layoutControlItem1,
            this.layoutControlItem8,
            this.layoutControlItem12,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(853, 616);
            this.Root.Text = "家庭医生签约";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtName;
            this.layoutControlItem3.CustomizationFormText = "姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(298, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem3.Text = "姓名：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtDabh;
            this.layoutControlItem2.CustomizationFormText = "个人档案编号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(298, 24);
            this.layoutControlItem2.Text = "个人档案编号：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.dateEdit签约;
            this.layoutControlItem9.CustomizationFormText = "执行日期";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 521);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem9.Text = "签约日期：";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.gcServicePackage;
            this.layoutControlItem7.CustomizationFormText = "服务包";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(833, 309);
            this.layoutControlItem7.Text = "服务包：";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt签约人;
            this.layoutControlItem10.CustomizationFormText = "签约人";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 545);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(364, 24);
            this.layoutControlItem10.Text = "录入人：";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.lookUpEditServiceTeam;
            this.layoutControlItem11.CustomizationFormText = "签约服务团队：";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 405);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(709, 24);
            this.layoutControlItem11.Text = "服务团队：";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtCardID;
            this.layoutControlItem4.CustomizationFormText = "身份证号";
            this.layoutControlItem4.Location = new System.Drawing.Point(298, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtTelNo;
            this.layoutControlItem6.CustomizationFormText = "联系电话";
            this.layoutControlItem6.Location = new System.Drawing.Point(568, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItem6.Text = "联系电话：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.lueServiceObject;
            this.layoutControlItem13.CustomizationFormText = "签约对象类型";
            this.layoutControlItem13.Location = new System.Drawing.Point(568, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItem13.Text = "签约对象类型：";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(84, 14);
            // 
            // emptySpaceItem贫困人口
            // 
            this.emptySpaceItem贫困人口.AllowHotTrack = false;
            this.emptySpaceItem贫困人口.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem贫困人口.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem贫困人口.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem贫困人口.Location = new System.Drawing.Point(567, 48);
            this.emptySpaceItem贫困人口.Name = "emptySpaceItem贫困人口";
            this.emptySpaceItem贫困人口.Size = new System.Drawing.Size(266, 24);
            this.emptySpaceItem贫困人口.Text = " ";
            this.emptySpaceItem贫困人口.TextSize = new System.Drawing.Size(84, 0);
            this.emptySpaceItem贫困人口.TextVisible = true;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtAddress;
            this.layoutControlItem5.CustomizationFormText = "居住地址";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(567, 24);
            this.layoutControlItem5.Text = "居住地址：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtMagCard;
            this.layoutControlItem14.CustomizationFormText = "磁条卡号：";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(298, 24);
            this.layoutControlItem14.Text = "磁条卡号：";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.cboeHealthCondition;
            this.layoutControlItem1.CustomizationFormText = "健康状况：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(833, 24);
            this.layoutControlItem1.Text = "健康状况：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.labelControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(709, 405);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(124, 24);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.rgp签约医生;
            this.layoutControlItem12.CustomizationFormText = "签约人：";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 429);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(364, 92);
            this.layoutControlItem12.Text = "签约人：";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem15.Control = this.peFinger;
            this.layoutControlItem15.CustomizationFormText = "签字 (PC端只提供指纹)：";
            this.layoutControlItem15.Location = new System.Drawing.Point(596, 429);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(192, 140);
            this.layoutControlItem15.Text = "签约对象签字 (PC端只提供指纹)：";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.sbtnGetFinger;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(788, 429);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 140);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(30, 140);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(45, 140);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem17.Control = this.pictureEdit1;
            this.layoutControlItem17.CustomizationFormText = "负责人/家庭医生签字(PC端只支持指纹)：";
            this.layoutControlItem17.Location = new System.Drawing.Point(364, 429);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(190, 140);
            this.layoutControlItem17.Text = "负责人/家庭医生签字(PC端只支持指纹)：";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btn保存);
            this.panelControl1.Controls.Add(this.btn取消);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 616);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(853, 45);
            this.panelControl1.TabIndex = 129;
            // 
            // btn保存
            // 
            this.btn保存.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn保存.Location = new System.Drawing.Point(581, 10);
            this.btn保存.Margin = new System.Windows.Forms.Padding(23, 3, 47, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(82, 26);
            this.btn保存.TabIndex = 11;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn取消
            // 
            this.btn取消.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn取消.Location = new System.Drawing.Point(713, 10);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(87, 27);
            this.btn取消.TabIndex = 12;
            this.btn取消.Text = "取消";
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // sbtnYSFinger
            // 
            this.sbtnYSFinger.Location = new System.Drawing.Point(566, 468);
            this.sbtnYSFinger.Name = "sbtnYSFinger";
            this.sbtnYSFinger.Size = new System.Drawing.Size(38, 136);
            this.sbtnYSFinger.StyleController = this.layoutControl2;
            this.sbtnYSFinger.TabIndex = 19;
            this.sbtnYSFinger.Text = "录\r\n入\r\n指\r\n纹";
            this.sbtnYSFinger.Click += new System.EventHandler(this.sbtnYSFinger_Click);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.sbtnYSFinger;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(554, 429);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 140);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(30, 140);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(42, 140);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // frm签约
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 661);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.Name = "frm签约";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "家庭医生签约服务";
            this.Load += new System.EventHandler(this.frm签约_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peFinger.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgp签约医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboeHealthCondition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditServiceTeam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMagCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcServicePackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvServicePackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签约.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签约.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDabh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceObject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem贫困人口)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditServiceTeam;
        private DevExpress.XtraEditors.TextEdit txtMagCard;
        private DevExpress.XtraEditors.TextEdit txt签约人;
        private DevExpress.XtraGrid.GridControl gcServicePackage;
        private DevExpress.XtraGrid.Views.Grid.GridView gvServicePackage;
        private DevExpress.XtraGrid.Columns.GridColumn col服务包名称;
        private DevExpress.XtraGrid.Columns.GridColumn col服务内容;
        private DevExpress.XtraGrid.Columns.GridColumn col年收费标准;
        private DevExpress.XtraEditors.DateEdit dateEdit签约;
        private DevExpress.XtraEditors.TextEdit txtTelNo;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.TextEdit txtCardID;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.TextEdit txtDabh;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem贫困人口;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn取消;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cboeHealthCondition;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.LookUpEdit lueServiceObject;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.RadioGroup rgp签约医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraGrid.Columns.GridColumn gcol优惠方式;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.PictureEdit peFinger;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.SimpleButton sbtnGetFinger;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.SimpleButton sbtnYSFinger;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
    }
}