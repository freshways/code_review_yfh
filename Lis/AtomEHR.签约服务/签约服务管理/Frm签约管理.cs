﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.Common;
using WEISHENG.COMM.PluginsAttribute;
using AtomEHR.Bridge;
using AtomEHR.Interfaces;
using AtomEHR.Business.Security;
using AtomEHR.Core;

namespace AtomEHR.签约服务.签约服务管理
{
    [ClassInfoMarkAttribute(GUID = "99213697-80E9-4B21-8921-8282AD8538F9", 父ID = "eb62e99a-c72e-4183-abef-0025f4e90b5e", 键ID = "99213697-80E9-4B21-8921-8282AD8538F9", 菜单类型 = "WinForm", 功能名称 = "签约管理", 程序集名称 = "AtomEHR.签约服务", 程序集调用类地址 = "签约服务管理.Frm签约管理", 全屏打开 = true)]
    public partial class Frm签约管理 : AtomEHR.Library.frmBaseBusinessForm
    {
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        bllJTYS签约对象类型 bll签约对象 = new bllJTYS签约对象类型();
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        bllJTYS审批状态 bll状态 = new bllJTYS审批状态();
        bllJTYS履约执行明细 bll履约记录 = new bllJTYS履约执行明细();
        bllJTYS团队概要 bll团队 = new bllJTYS团队概要();
        bllJTYS团队成员 bll成员 = new bllJTYS团队成员();
        
        public Frm签约管理()
        {
            InitializeComponent();
        }

        private ILoginAuthorization _CurrentAuthorization = null;//当前授权模式
        private void Frm签约管理_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Loginer.CurrentUser.Account))
            {
                try
                {
                    SystemConfig.ReadSettings(); //读取用户自定义设置

                    if (!BridgeFactory.InitializeBridge())
                    {
                        Msg.Warning("初始化失败!");
                    }

                    //第二步： 获得此身份证号对应的用户名密码，初始化登陆参数。
                    string userID = "rxy";
                    string password = CEncoder.Encode("888888");
                    string dataSetID = "YSDB"; //txtDataset.EditValue.ToString();//帐套编号
                    string dataSetDB = "AtomEHR.YSDB";

                    LoginUser loginUser = new LoginUser(userID, password, dataSetID, dataSetDB, "HIS1.0", "");
                    _CurrentAuthorization = new LoginSystemAuth();
                    //第三步： 调用登陆验证方法。
                    if (_CurrentAuthorization.Login(loginUser)) //调用登录策略
                    {
                        SystemAuthentication.Current = _CurrentAuthorization; //授权成功, 保存当前授权模式

                    }
                }
                catch (Exception ex)
                {
                    Msg.Warning("登录失败!失败原因:\n" + ex.Message);
                }
            }

            InitView();
            this.InitializeForm();

            if (Loginer.CurrentUser.所属机构.StartsWith("371321"))
            {
                sbtnPrint.Visible = false;
                layoutControlItem9.Text = "签约医生：";
                this.gridColumn10.Caption = "签约医生";
                this.col签约医生.Caption = "签约医生";
            }
            else
            {
                this.gridColumn10.Caption = "录入人";
                this.col签约医生.Caption = "录入人";
            }
        }

        void InitView()
        {
            #region 绑定机构信息
            try
            {
                AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                tllue机构.Properties.ValueMember = "机构编号";
                tllue机构.Properties.DisplayMember = "机构名称";

                tllue机构.Properties.TreeList.KeyFieldName = "机构编号";
                tllue机构.Properties.TreeList.ParentFieldName = "上级机构";
                tllue机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                tllue机构.Properties.DataSource = dt所属机构;
                tllue机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                tllue机构.Text = Loginer.CurrentUser.所属机构;
            }
            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                tllue机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                tllue机构.Properties.AutoExpandAllNodes = false;
            }
            #endregion

            //绑定签约对象
            //lue签约对象.Properties.ValueMember = tb_JTYS签约对象类型.ID;
            //lue签约对象.Properties.DisplayMember = tb_JTYS签约对象类型.类型名称;
            //lue签约对象.Properties.Columns.Add( new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS签约对象类型.ID));
            //lue签约对象.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS签约对象类型.类型名称));
            DataTable dtServiceObject = bll签约对象.GetAllValidateData();
            util.ControlsHelper.BindComboxData(dtServiceObject, lue签约对象, tb_JTYS签约对象类型.ID, tb_JTYS签约对象类型.类型名称);
            //lue签约对象.Properties.DataSource = dtServiceObject;

            //绑定签约服务包
            //lue服务包类型.Properties.ValueMember = tb_JTYS服务包.ServiceID;
            //lue服务包类型.Properties.DisplayMember = tb_JTYS服务包.名称;
            //lue服务包类型.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS服务包.名称));
            DataTable dt服务包 = bll服务包.GetAllValidateDataWithCodeAndName();
            util.ControlsHelper.BindComboxData(dt服务包, lue服务包类型, tb_JTYS服务包.ServiceID, tb_JTYS服务包.名称);

            //绑定签约状态
            //lue状态.Properties.ValueMember = tb_JTYS审批状态.ApprovalCode;
            //lue状态.Properties.DisplayMember = tb_JTYS审批状态.ApprovalName;
            //lue状态.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS审批状态.ApprovalCode));
            //lue状态.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS审批状态.ApprovalName));
            DataTable dt状态 = bll状态.GetAllValidateData();
            util.ControlsHelper.BindComboxData(dt状态, lue状态, tb_JTYS审批状态.ApprovalCode, tb_JTYS审批状态.ApprovalName);
            util.ControlsHelper.SetComboxData("1", lue状态);
            //lue状态.Properties.DataSource = dt状态;

            DataView dv镇 = new DataView(AtomEHR.Business.DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码='" + Loginer.CurrentUser.单位代码 + "' ";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");
        }

        private void comboBoxEdit镇_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(AtomEHR.Business.DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
		
		protected override void InitializeForm()
        {
            _BLL = new bllJTYS服务包();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            //base.InitButtonsBase();

            //frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //util.BindingHelper.LookupEditBindDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, lookUpEdit性别, "P_CODE", "P_DESC");

            //DataTable dt所属机构 = _bll机构信息.Get机构("371328");
            //util.ControlsHelper.BindComboxData(dt所属机构, tllue机构, "机构编号", "机构名称");
        }
        string _strWhere = "";
        protected override bool DoSearchSummary()
        {
            //" and " + tb_JTYS医生信息.是否有效 + "=1";

            //if (Loginer.CurrentUser.IsAdmin() || Loginer.CurrentUser.所属机构 == "371321C21008")
            //{
            //}
            //else
            //{
            //    _strWhere += " and " + tb_JTYS签约信息.签约医生+ "='" + Loginer.CurrentUser.用户编码 + "'";
            //}

            _strWhere = "";

            if(chkMe.Checked)
            {
                _strWhere += " and 签约医生='" + Loginer.CurrentUser.用户编码 + "'";
            }
            else
            {
                if (tllue机构.Text != "" && tllue机构.EditValue != null)
                {
                    string pgrid = tllue机构.EditValue.ToString();
                    if (ck含下属机构.Checked)
                    {
                        if (pgrid.Length == 12)
                        {
                            _strWhere += " and (档案机构='" + pgrid + "' or 签约时上级='" + pgrid + "')";
                        }
                        else
                        {
                            _strWhere += " AND [档案机构] LIKE '" + pgrid + "%'";
                        }
                    }
                    else
                    {
                        _strWhere += " AND 档案机构='" + pgrid + "'";
                    }
                }

                if (string.IsNullOrWhiteSpace(txt签约医生.Text))
                { }
                else if (txt签约医生.Text.Contains("'") || txt签约医生.Text.Contains("--") || txt签约医生.Text.Contains(";"))
                { }
                else
                {
                    _strWhere += " and 签约医生姓名='" + txt签约医生.Text.Trim() + "'";
                }
            }

            if (txt姓名.Text.Contains("'") || txt姓名.Text.Contains("--") || txt姓名.Text.Contains(";"))
            { }
            else if (txt姓名.Text != "")
            {
                _strWhere += " AND (姓名 like'" + DESEncrypt.DES加密(txt姓名.Text) + "%' or 姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            }

            if(!string.IsNullOrWhiteSpace(de签约日期B.Text))
            {
                _strWhere += " and 签约日期>='"+de签约日期B.Text.Trim()+"' ";
            }

            if(!string.IsNullOrWhiteSpace(de签约日期E.Text))
            {
                _strWhere += " and 签约日期<='" + de签约日期E.Text.Trim() + "' ";
            }

            if(!string.IsNullOrWhiteSpace(de生效日期B.Text))
            {
                _strWhere += " and 生效日期>='" + de生效日期B.Text.Trim() + "' ";
            }

            if (!string.IsNullOrWhiteSpace(de生效日期E.Text))
            {
                _strWhere += " and 生效日期<='" + de生效日期E.Text.Trim() + "' ";
            }

            if (!string.IsNullOrWhiteSpace(de到期日期B.Text))
            {
                _strWhere += " and 到期日期>='" + de到期日期B.Text.Trim() + "' ";
            }

            if (!string.IsNullOrWhiteSpace(de到期日期E.Text))
            {
                _strWhere += " and 到期日期<='" + de到期日期E.Text.Trim() + "' ";
            }

            //if (!string.IsNullOrWhiteSpace(de审批日期B.Text))
            //{
            //    _strWhere += " and 审批日期>='" + de审批日期B.Text.Trim() + "' ";
            //}

            //if (!string.IsNullOrWhiteSpace(de审批日期E.Text))
            //{
            //    _strWhere += " and 审批日期<='" + de审批日期E.Text.Trim() + "' ";
            //}
            if (!string.IsNullOrWhiteSpace(de审批日期B.Text) && !string.IsNullOrWhiteSpace(de审批日期E.Text))
            {
                _strWhere += " and  exists (select 1 from dbo.tb_健康档案 JKDA where JKDA.[个人档案编号]=档案号 and 出生日期 >='" + de审批日期B.Text + "' and 出生日期 <='" + de审批日期E.Text + "') ";
            }
            else if (!string.IsNullOrWhiteSpace(de审批日期B.Text))
            {
                _strWhere += " and  exists (select 1 from dbo.tb_健康档案 JKDA where JKDA.[个人档案编号]=档案号 and 出生日期 >='" + de审批日期B.Text + "') ";
            }
            else if (!string.IsNullOrWhiteSpace(de审批日期E.Text))
            {
                _strWhere += " and  exists (select 1 from dbo.tb_健康档案 JKDA where JKDA.[个人档案编号]=档案号 and 出生日期 <='" + de审批日期E.Text + "') ";
            }
            else
            {
            }

            if(!string.IsNullOrWhiteSpace(txt身份证号.Text))
            {
                _strWhere += " and 身份证号='"+txt身份证号.Text.Trim()+"' ";
            }

            if(!string.IsNullOrWhiteSpace(txt磁卡号.Text))
            {
                _strWhere += " and 磁卡号='" + txt磁卡号.Text.Trim() + "'";
            }

            if(!string.IsNullOrWhiteSpace(txt档案号.Text))
            {
                _strWhere += " and 档案号='" + txt档案号.Text.Trim() + "'";
            }

            string strSO = util.ControlsHelper.GetComboxKey(lue签约对象);
            if(!string.IsNullOrWhiteSpace(strSO))
            {
                _strWhere += " and 签约对象类型='" + strSO + "' ";
            }

            string strPT = util.ControlsHelper.GetComboxKey(lue服务包类型);
            if (!string.IsNullOrWhiteSpace(strPT))
            {
                _strWhere += " and 签约服务包 like '%," + strPT + ",%' ";
            }

            string strZT = util.ControlsHelper.GetComboxKey(lue状态);
            if (!string.IsNullOrWhiteSpace(strZT))
            {
                _strWhere += " and 状态 = '" + strZT + "' ";
            }

            string stLYZT = cbo履约状态.Text;
            if(string.IsNullOrWhiteSpace(stLYZT))
            { }
            else
            {
                _strWhere += " and 履约状态 = '" + stLYZT + "' ";
            }

            if(string.IsNullOrWhiteSpace(cboe来源.Text))
            {
            }
            else if (cboe来源.Text == "手机APP")
            {
                _strWhere += " and 签约来源 = 'APP' ";
            }
            else if (cboe来源.Text == "自助签约")
            {
                _strWhere += " and 签约来源 = '自助签约' ";
            }
            else
            {
                _strWhere += " and ((签约来源 <> '自助签约' and 签约来源 <>'APP') OR 签约来源 IS NULL)";
            }

            if(string.IsNullOrWhiteSpace(cboe打印.Text))
            {}
            else if (cboe打印.Text == "已打印")
            {
                _strWhere += " and [已打印协议]=1 ";
            }
            else
            {
                _strWhere += " and (已打印协议 =0 OR 已打印协议 IS NULL)";
            }

            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit镇)))
            {
                _strWhere += " and 街道 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'  ";
            }
            if (!string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(comboBoxEdit村)))
            {
                _strWhere += " and 居委会 = '" + util.ControlsHelper.GetComboxKey(comboBoxEdit村) + "'  ";
            }
            if (!string.IsNullOrEmpty(this.textEdit地址.Text.Trim()))
            {
                _strWhere += " and 居住地址 LIKE '%" + this.textEdit地址.Text.Trim() + "%'  ";
            }

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS签约信息", "*", _strWhere, tb_JTYS签约信息.ID, "DESC").Tables[0];

            //for (int index = 0; index < dt.Rows.Count; index++ )
            //{
            //    dt.Rows[index]["姓名"] = DESEncrypt.DES解密(dt.Rows[index]["姓名"].ToString());
            //}

            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.

            gvSummary.BestFitColumns();

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            _strWhere = "";
            pagerControl1.InitControl();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Frm签约档案查询 frm = new Frm签约档案查询();
            System.Windows.Forms.DialogResult result = frm.ShowDialog();

            if(result == System.Windows.Forms.DialogResult.OK)
            {
                frm签约 frmQY = new frm签约(frm.SelectedDabh, frm.SelectedName, frm.SelectedCardNo, frm.SelectedTelNo, frm.selectedAddress, frm.selectedSsjg);
                frmQY.ShowDialog();
            }
            else
            {
                frm.Dispose();
            }
        }

        private void sbtnInfo_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请未选择任何行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能选择一行");
                return;
            }

            string qyid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.ID).ToString();

            frm签约_显示 frm = new frm签约_显示(qyid);
            frm.ShowDialog();
            frm.Dispose();
        }

        private void btn解约_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请未选择任何行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能选择一行");
                return;
            }

            string qyys = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.签约医生).ToString();
            if (Loginer.CurrentUser.用户编码 != qyys && Loginer.CurrentUser.IsAdmin()==false)
            {
                Msg.ShowInformation("不允许对其他人签约进行解约处理");
                return;
            }

            string qyid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.ID).ToString();

            //检验是否为“已签约”状态，如果是的话，才允许进行解约操作
            string state = bll签约.Get状态Code(qyid);
            if (state == "1")
            {
                //检验是否已进行过履约，若有履约记录，则不允许解约
                bool retly = bll履约记录.CheckExistBy签约ID(qyid);
                if (retly)
                {
                    Msg.ShowInformation("已经存在履约记录，不允许进行解约。");
                    return;
                }

                frm解约 frm = new frm解约(qyid);
                frm.ShowDialog();
            }
            else
            {
                Msg.ShowInformation("此记录当前不是“已签约”状态，不允许进行解约操作。");
            }
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请未选择任何行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能选择一行");
                return;
            }

            string qyys = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.签约医生).ToString();

            string ssxz = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);//所属乡镇


            if (Loginer.CurrentUser.用户编码 != qyys && Loginer.CurrentUser.IsAdmin() == false)
            {
                if (ssxz == Loginer.CurrentUser.所属机构)
                { }
                else
                {
                    Msg.ShowInformation("不允许修改其他人签约的记录");
                    return;
                }
            }

            string qyid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.ID).ToString();

            string state = bll签约.Get状态Code(qyid);
            if (state == "1")
            {
                frm签约修改 frm = new frm签约修改(qyid);
                frm.ShowDialog();
            }
            else
            {
                Msg.ShowInformation("此记录当前不是“已签约”状态，不允许进行修改操作。");
            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            de签约日期B.Text = "";
            de签约日期E.Text = "";
            de生效日期B.Text = "";
            de生效日期E.Text = "";
            de到期日期B.Text = "";
            de到期日期E.Text = "";
            de审批日期B.Text = "";
            de审批日期E.Text = "";
            tllue机构.EditValue = Loginer.CurrentUser.所属机构;
            ck含下属机构.Checked = true;
            txt签约医生.Text = "";
            txt姓名.Text = "";
            txt身份证号.Text = "";
            txt磁卡号.Text = "";
            txt档案号.Text = "";
            util.ControlsHelper.SetComboxData("", lue服务包类型);
            util.ControlsHelper.SetComboxData("", lue签约对象);
            util.ControlsHelper.SetComboxData("", lue状态);
        }

        private void chkMe_CheckedChanged(object sender, EventArgs e)
        {
            if(chkMe.Checked)
            {
                txt签约医生.Text = "";
                tllue机构.Enabled = false;
                ck含下属机构.Enabled = false;
            }
            else
            {
                tllue机构.Enabled = true;
                ck含下属机构.Enabled = true;
            }
        }

        private void sbtnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XLS file(*.xls)|*.xls";//"PDF file(*.pdf)|*.pdf";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                DataTable dt = gcSummary.DataSource as DataTable;
                AtomEHR.Business.bll机构信息 bll = new AtomEHR.Business.bll机构信息();
//                string fileds = @"[ID]
//      ,[档案号]
//      ,[联系电话]
//      ,[健康状况名称]
//      ,[服务包名称]
//      ,[签约日期]
//      ,convert(varchar(20), [生效日期],120) [生效日期]
//      ,convert(varchar(20), [到期日期],120) [到期日期]
//      ,[创建时间]
//      ,[解约人]
//      ,[解约原因]
//      ,[修改人]
//      ,[修改时间]
//      ,[履约状态]
//      ,[磁卡号]
//      ,[姓名]
//      ,[性别名称]
//      ,[身份证号]
//      ,[签约对象类型名称]
//      ,[签约医生姓名]
//      ,[签约状态]
//      ,[团队名称]
//      ,[创建人姓名]
//      ,[修改人姓名]
//      ,[所属机构名称] 档案所属机构名称
//      ,[居住地址]";
                DataTable newtable = new AtomEHR.Business.BLL_Base.bllBase().Get全部数据("vw_JTYS签约信息", "*", _strWhere, "ID", "DESC");
                for (int i = 0; i < newtable.Rows.Count; i++)
                {
                    newtable.Rows[i]["姓名"] = Common.DESEncrypt.DES解密(newtable.Rows[i]["姓名"].ToString());
                    //newtable.Rows[i]["居住地址"] = bll.Return地区名称(bll.Return地区名称(newtable.Rows[i]["居委会"].ToString()) + newtable.Rows[i]["居住地址"].ToString());
                    //newtable.Rows[i]["所属机构"] = bll.Return机构名称(newtable.Rows[i]["所属机构"].ToString());
                    //newtable.Rows[i]["创建人"] = bll.Return用户名称(newtable.Rows[i]["创建人"].ToString());
                }
                gcSummary.DataSource = newtable;
                gcSummary.ExportToXls(dlg.FileName);
                gcSummary.DataSource = dt;
                //view.ExportToXls(dlg.FileName);
                //ExportToExcel(table, dlg.FileName);
            }
        }


        bllJTYS签约费用明细 bll费用明细 = new bllJTYS签约费用明细();
        private void sbtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                //Font font = new System.Drawing.Font("Wingdings 2", 10);
                //if (font.Name != "Wingdings 2")
                //{
                //    Msg.ShowInformation("电脑系统字体缺失，请先安装字体，字体在电脑重新启动后生效。在您点击“确定”后，为您显示字体安装窗口。");
                //    System.Diagnostics.Process.Start("Wingdings 2.ttf");
                //    return;
                //}

                int[] selectdIndex = gvSummary.GetSelectedRows();
                if (selectdIndex.Length == 0)
                {
                    Msg.ShowInformation("请未选择任何行");
                    return;
                }
                else if (selectdIndex.Length > 1)
                {
                    Msg.ShowInformation("每次只能选择一行");
                    return;
                }

                //string qyys = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.签约医生).ToString();
                //if (Loginer.CurrentUser.用户编码 != qyys && Loginer.CurrentUser.IsAdmin() == false)
                //{
                //    Msg.ShowInformation("不允许修改其他人签约的记录");
                //    return;
                //}

                string qyid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.ID).ToString();
                //gvSummary.FocusedRowHandle   与下边效果相同
                if (gvSummary.GetRowCellValue(selectdIndex[0], grid签约来源).ToString() == "自助签约")
                {
                    string 费用ID = gvSummary.GetRowCellValue(selectdIndex[0], grid费用ID).ToString();
                    bool chk费用 = bll费用明细.GetYesOrNo(费用ID);
                    if (!chk费用)
                    {
                        Msg.ShowInformation("选中的签约记录为自助签约，需先进行费用确认，然后才可打印协议书或进行履约。");
                        return;
                    }
                }

                string state = bll签约.Get状态Code(qyid);
                if (state == "1")
                {
                    #region 桃墟的旧版打印
                    ////获取档案号
                    //string dah = gvSummary.GetRowCellValue(selectdIndex[0], col健康档案号).ToString();

                    ////根据qyid获取姓名、电话、地址、身份证号、手签、签约日期、服务包
                    //DataTable dtInfo = bll签约.GetInfoForPrint(qyid);
                    //string xm = dtInfo.Rows[0]["姓名"].ToString();
                    //string tel = dtInfo.Rows[0]["联系电话"].ToString();
                    //if(string.IsNullOrWhiteSpace(tel))
                    //{
                    //    tel = dtInfo.Rows[0]["本人电话"].ToString();
                    //}

                    //string sfzh = dtInfo.Rows[0]["身份证号"].ToString();

                    //string qyrq = dtInfo.Rows[0]["签约日期"].ToString();
                    //qyrq = Convert.ToDateTime(qyrq).ToString(" yyyy年 MM月 dd日 ");

                    //string sqz = dtInfo.Rows[0]["手签照"].ToString();

                    //string fuwb = dtInfo.Rows[0]["签约服务包"].ToString();
                    //string addr = dtInfo.Rows[0]["地址"].ToString();

                    ////DataSet dsTeamInfo = bll团队.GetBusinessByTeamID();
                    ////根据签约ID 获取签约服务团队、签约医生
                    //DataTable dtTeamID = bll签约.GetTeamidByQyid(qyid);
                    //string teamid = dtTeamID.Rows[0][tb_JTYS签约信息.团队ID].ToString();
                    //string sjqyr = dtTeamID.Rows[0][tb_JTYS签约信息.签约人].ToString();//实际签约人

                    ////获取队长姓名、电话，成员姓名电话，分两个datatable
                    //DataSet dsTeamCapAndMemInfo = bll团队.GetTeamCapNameAndTel(teamid);

                    ////组长(团队长)的姓名电话
                    //string teamCapName = dsTeamCapAndMemInfo.Tables[0].Rows[0]["DocName"].ToString();
                    //string teamCapTel = dsTeamCapAndMemInfo.Tables[0].Rows[0]["TelNo"].ToString();

                    //string zzlb = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长类别"].ToString();
                    //string zzbh = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长编号"].ToString();

                    //DataTable dtMem = dsTeamCapAndMemInfo.Tables[1];

                    ////签约医生姓名电话
                    //string sjqyrName = "";
                    //string sjqyrTel = "";

                    //if (!string.IsNullOrWhiteSpace(sjqyr))
                    //{
                    //    DataRow[] drQYYS = dtMem.Select("ID=" + sjqyr + "");
                    //    if (drQYYS.Length > 0)
                    //    {
                    //        sjqyrName = drQYYS[0]["DocName"].ToString();
                    //        sjqyrTel = drQYYS[0]["TelNo"].ToString();
                    //    }
                    //}

                    ////其他团队成员，剔除签约人、队长，只取前5个
                    //DataRow[] drQTYS;
                    //if(string.IsNullOrWhiteSpace(sjqyr))
                    //{
                    //    drQTYS = dtMem.Select("成员类别<>'" + zzlb + "' or 成员ID<>'" + zzbh + "'");
                    //}
                    //else
                    //{
                    //    drQTYS = dtMem.Select("ID<>" + sjqyr + " and (成员类别<>'" + zzlb + "' or 成员ID<>'" + zzbh + "')");
                    //}

                    //string ys0 = "";
                    //string ys0Tel = "";
                    //if(drQTYS.Length >0)
                    //{
                    //    ys0 = drQTYS[0]["DocName"].ToString();
                    //    ys0Tel = drQTYS[0]["TelNo"].ToString();
                    //}

                    //string ys1 = "";
                    //string ys1Tel = "";
                    //if(drQTYS.Length >1)
                    //{
                    //    ys1 = drQTYS[1]["DocName"].ToString();
                    //    ys1Tel = drQTYS[1]["TelNo"].ToString();
                    //}

                    //string ys2 = "";
                    //string ys2Tel = "";
                    //if (drQTYS.Length > 2)
                    //{
                    //    ys2 = drQTYS[2]["DocName"].ToString();
                    //    ys2Tel = drQTYS[2]["TelNo"].ToString();
                    //}

                    //string ys3 = "";
                    //string ys3Tel = "";
                    //if (drQTYS.Length > 3)
                    //{
                    //    ys3 = drQTYS[3]["DocName"].ToString();
                    //    ys3Tel = drQTYS[3]["TelNo"].ToString();
                    //}

                    //string ys4 = "";
                    //string ys4Tel = "";
                    //if (drQTYS.Length > 4)
                    //{
                    //    ys4 = drQTYS[4]["DocName"].ToString();
                    //    ys4Tel = drQTYS[4]["TelNo"].ToString();
                    //}

                    //string ys5 = "";
                    //string ys5Tel = "";
                    //if (drQTYS.Length > 5)
                    //{
                    //    ys5 = drQTYS[5]["DocName"].ToString();
                    //    ys5Tel = drQTYS[5]["TelNo"].ToString();
                    //}

                    //Report.XtraReport桃墟 rept = new Report.XtraReport桃墟(xm, tel, sfzh, addr, qyrq, sqz, fuwb,
                    //    teamCapName,teamCapTel,
                    //    sjqyrName, sjqyrTel,
                    //    ys0, ys0Tel,
                    //    ys1, ys1Tel,
                    //    ys2, ys2Tel,
                    //    ys3, ys3Tel,
                    //    ys4, ys4Tel,
                    //    ys5, ys5Tel
                    //    );
                    //DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(rept);
                    //tool.ShowPreviewDialog();

                    //bool isPrinted = rept.GetPrintedState();
                    //if(isPrinted)
                    //{
                    //    bll签约.UpdatePrintedState(qyid, true);
                    //}
                    #endregion
                    Print协议书(qyid);
                }
                else
                {
                    Msg.ShowInformation("此记录当前不是“已签约”状态，不再支持打印协议。");
                }
            }
            catch (Exception ex)
            {
                Msg.ShowInformation("出现异常：" + ex.Message);
            }
        }

        private void Print协议书(string qyid)
        {
            //获取签约信息
            //DataSet dtQyInfo = bll签约.GetBusinessByIdFromView(qyid);
            DataTable dtInfo = bll签约.GetInfoForPrint(qyid);

            string fyid = dtInfo.Rows[0][tb_JTYS签约信息.费用ID].ToString();

            //if (!string.IsNullOrWhiteSpace(fyid))
            //{
            //    //获取签约明细及签约价格
            //    DataTable dtFYMX = bll费用明细.GetDataBy费用ID(fyid);
            //}

            //获取page1/2.3.4
            //string ssxzjg = GetSSSXZJG();
            string ssxzjg = GetSSSXZJG(dtInfo.Rows[0][tb_JTYS签约信息.录入人机构].ToString());

            //获取录入人的上级机构
            //

            DataTable dtpage1 = GetPage1(ssxzjg);
            DataTable dtpage2 = GetPage2(ssxzjg);


            string fuwb = dtInfo.Rows[0]["签约服务包"].ToString();
            DataSet dsSP3 = GetSPPage(ssxzjg, fyid, 3);
            if (dsSP3 != null)
            {
                for (int tabi = 0; tabi < dsSP3.Tables.Count; tabi++)
                {
                    if (!dsSP3.Tables[tabi].Columns.Contains("selected"))
                    {
                        dsSP3.Tables[tabi].Columns.Add("selected");
                    }
                    for (int inner = 0; inner < dsSP3.Tables[tabi].Rows.Count; inner++)
                    {
                        if (fuwb.Contains(dsSP3.Tables[tabi].Rows[inner]["SPID"].ToString()))
                        {
                            dsSP3.Tables[tabi].Rows[inner]["selected"] = "√";
                        }
                    }
                }
            }
            DataSet dsSP4 = GetSPPage(ssxzjg, fyid, 4);
            if (dsSP4 != null)
            {
                for (int tabi = 0; tabi < dsSP4.Tables.Count; tabi++)
                {
                    if (!dsSP4.Tables[tabi].Columns.Contains("selected"))
                    {
                        dsSP4.Tables[tabi].Columns.Add("selected");
                    }
                    for (int inner = 0; inner < dsSP4.Tables[tabi].Rows.Count; inner++)
                    {
                        if (fuwb.Contains(dsSP4.Tables[tabi].Rows[inner]["SPID"].ToString()))
                        {
                            dsSP4.Tables[tabi].Rows[inner]["selected"] = "√";
                        }
                    }
                }
            }

            DataTable dtRQList = GetRQList(ssxzjg);

            string teamid = dtInfo.Rows[0][tb_JTYS签约信息.团队ID].ToString();
            //获取队长姓名、电话，成员姓名电话，分两个datatable

            DataSet dsTeamCapAndMemInfo = bll团队.GetTeamInfoByteamid(teamid);
            string zzlb = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长类别"].ToString();
            string zzbh = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长编号"].ToString();
            string capID = "";
            DataRow[] drsCap = dsTeamCapAndMemInfo.Tables[1].Select("成员类别='" + zzlb + "' and 成员ID='" + zzbh + "'");
            if (drsCap.Length > 0)
            {
                capID = drsCap[0]["ID"].ToString();
            }
            DataTable dtMem = dsTeamCapAndMemInfo.Tables[1];


            DataTable dtOtherProperty = GetPageOtherProperty(ssxzjg);
            DataTable dtpage3 = GetPage3(ssxzjg);
            DataTable dtpage4 = Getpage4(ssxzjg);

            string name = dtInfo.Rows[0]["姓名"].ToString();
            string tel = dtInfo.Rows[0][tb_JTYS签约信息.联系电话].ToString();
            string sfzh = dtInfo.Rows[0]["身份证号"].ToString();
            string dah = dtInfo.Rows[0][tb_JTYS签约信息.档案号].ToString();
            string addr = dtInfo.Rows[0]["地址"].ToString();
            string qyrq = dtInfo.Rows[0]["签约日期"].ToString();
            qyrq = Convert.ToDateTime(qyrq).ToString(" yyyy年 MM月 dd日 ");

            string sqz = dtInfo.Rows[0]["手签照"].ToString();
            string dxlx = dtInfo.Rows[0]["签约对象类型"].ToString();
            string qyrid = dtInfo.Rows[0]["签约人"].ToString();

            //string fyid = dtInfo.Rows[0]["费用ID"].ToString();
            string yssqz = bll签约.Get医生签名(dtInfo.Rows[0]["费用ID"].ToString());
            //调用打印功能
            Report.ReportComm rep = new Report.ReportComm(dtpage1, dtpage2, dsSP3, dsSP4, dtRQList, dtMem, dtOtherProperty, dtpage3, dtpage4, capID,
                name, tel, sfzh, dah, addr, qyrq, sqz, dxlx, qyrid, yssqz);
            rep.ShowPreviewDialog();

            //获取是否打印标志
            //更新打印标志
            bool isPrinted = rep.GetPrintState();
            if (isPrinted)
            {
                bll签约.UpdatePrintedState(qyid, true);
            }
        }

        #region 获取所属乡镇机构编码
        string m_ssxzjg = string.Empty;
        string m_ssxzjgNew = string.Empty;
        private string GetSSSXZJG(string cjrjg)
        {
            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            string temp = bll机构.Get所在乡镇编码(cjrjg);
            m_ssxzjg = m_ssxzjgNew;
            m_ssxzjgNew = temp;
            //if (string.IsNullOrWhiteSpace(m_ssxzjgNew))
            //{
            //    m_ssxzjg = m_ssxzjgNew;
            //    m_ssxzjgNew = _bll机构信息.Get所在乡镇编码(cjrjg);
            //}
            //else if (!m_ssxzjgNew.Equals(m_ssxzjg))
            //{
            //    m_ssxzjg = m_ssxzjgNew;
            //    m_ssxzjgNew = _bll机构信息.Get所在乡镇编码(cjrjg);
            //}
            //else
            //{ }
            return m_ssxzjgNew;
        }

        /// <summary>
        /// 比较这次查询的机构是否与上次的一致,不一致时返回true
        /// </summary>
        /// <returns></returns>
        private bool CompareJG()
        {
            return !m_ssxzjgNew.Equals(m_ssxzjg);
        }
        #endregion

        #region 获取page1
        DataTable m_page1 = null;
        bllJTYSPage1 bllpage1 = new bllJTYSPage1();
        private DataTable GetPage1(string rgid)
        {
            if (m_page1 == null || CompareJG())
            {
                DataSet ds = bllpage1.GetBusinessByRGID(rgid, false);
                if(ds!=null && ds.Tables.Count >0)
                {
                    m_page1 = ds.Tables[0];
                }
            }
            return m_page1;
        }
        #endregion

        #region 获取page2
        DataTable m_page2 = null;
        bllJTYSPage2 bllpage2 = new bllJTYSPage2();
        private DataTable GetPage2(string ssjg)
        {
            if (m_page2 == null || CompareJG())
            {
                DataSet ds = bllpage2.GetBusinessByRGID(ssjg, false);
                m_page2 = ds.Tables[0];
            }
            return m_page2;
        }
        #endregion

        #region 获取sp3 4
        bllJTYSPageSP bllsp = new bllJTYSPageSP();
        private DataSet GetSPPage(string rgid, string fyid, int pageno)
        {
            DataSet ds = bllsp.GetBusinessGroupByFYID(rgid, fyid, pageno);
            return ds;
        }

        #endregion

        #region 获取RQLIST
        DataTable m_RQList = null;
        bllJTYS签约对象类型 bll对象类型 = new bllJTYS签约对象类型();
        private DataTable GetRQList(string ssxzjg)
        {
            if(m_RQList==null)
            {
                m_RQList = bll对象类型.GetAllValidateData();
            }
            return m_RQList;
        }
        #endregion

        #region pageotherproperty
        DataTable m_pageOther = null;
        bllJTYSPageOtherProperty bllOtherProperty = new bllJTYSPageOtherProperty();
        private DataTable GetPageOtherProperty(string ssjg)
        {
            if (m_pageOther == null || CompareJG())
            {
                DataSet ds = bllOtherProperty.GetBusinessByRGID(ssjg, false);
                m_pageOther = ds.Tables[0];
            }
            return m_pageOther;
        }

        #endregion

        #region page3
        DataTable m_page3 = null;
        bllJTYSPage3 bllpage3 = new bllJTYSPage3();
        private DataTable GetPage3(string ssjg)
        {
            if (m_page3 == null || CompareJG())
            {
                DataSet ds = bllpage3.GetBusinessByRGID(ssjg, false);
                m_page3 = ds.Tables[0];
            }
            return m_page3;
        }
        #endregion

        #region page4
        DataTable m_page4 = null;
        bllJTYSPage4 bllpage4 = new bllJTYSPage4();
        private DataTable Getpage4(string ssjg)
        {
            if (m_page4 == null || CompareJG())
            {
                DataSet ds = bllpage4.GetBusinessByRGID(ssjg, false);
                m_page4 = ds.Tables[0];
            }
            return m_page4;
        }
        #endregion

        private void tllue机构_EditValueChanged(object sender, EventArgs e)
        {
            //局级管理员的权限
            if (Loginer.CurrentUser.所属机构.Length == 6)
            {
                string rgid = tllue机构.EditValue.ToString();
                DataTable dt服务包 = bll服务包.GetAllValidateDataWithCodeAndNameByRGID(rgid);
                util.ControlsHelper.BindComboxData(dt服务包, lue服务包类型, tb_JTYS服务包.ServiceID, tb_JTYS服务包.名称);
                util.ControlsHelper.SetComboxData("", lue服务包类型);
            }
        }

        private void sbtnConfirm_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请未选择任何行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能选择一行");
                return;
            }

            string qyid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.ID).ToString();
            int count = bll费用明细.Create费用明细(qyid);
            Msg.ShowInformation("选中签约记录的费用信息确认成功");
        }

        private void sbtnPrint2_Click(object sender, EventArgs e)
        {
            try
            {
                int[] selectdIndex = gvSummary.GetSelectedRows();
                if (selectdIndex.Length == 0)
                {
                    Msg.ShowInformation("请未选择任何行");
                    return;
                }
                else if (selectdIndex.Length > 1)
                {
                    Msg.ShowInformation("每次只能选择一行");
                    return;
                }

                string qyid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.ID).ToString();
                //gvSummary.FocusedRowHandle   与下边效果相同
                if (gvSummary.GetRowCellValue(selectdIndex[0], grid签约来源).ToString() == "自助签约")
                {
                    string 费用ID = gvSummary.GetRowCellValue(selectdIndex[0], grid费用ID).ToString();
                    bool chk费用 = bll费用明细.GetYesOrNo(费用ID);
                    if (!chk费用)
                    {
                        Msg.ShowInformation("选中的签约记录为自助签约，需先进行费用确认，然后才可打印协议书或进行履约。");
                        return;
                    }
                }

                string state = bll签约.Get状态Code(qyid);
                if (state == "1")
                {
                    Print签约服务卡(qyid);
                }
                else
                {
                    Msg.ShowInformation("此记录当前不是“已签约”状态，不再支持打印协议。");
                }
            }
            catch (Exception ex)
            {
                Msg.ShowInformation("出现异常：" + ex.Message);
            }
        }


        private void Print签约服务卡(string qyid)
        {
            //获取签约信息
            //DataSet dtQyInfo = bll签约.GetBusinessByIdFromView(qyid);
            DataTable dtInfo = bll签约.GetInfoForPrint(qyid);
            DataTable dtInfosub = bll签约.GetInfoForPrint2(qyid);

            string fyid = dtInfo.Rows[0][tb_JTYS签约信息.费用ID].ToString();

            //if (!string.IsNullOrWhiteSpace(fyid))
            //{
            //    //获取签约明细及签约价格
            //    DataTable dtFYMX = bll费用明细.GetDataBy费用ID(fyid);
            //}

            //获取page1/2.3.4
            //string ssxzjg = GetSSSXZJG();
            string ssxzjg = GetSSSXZJG(dtInfo.Rows[0][tb_JTYS签约信息.录入人机构].ToString());

            //获取录入人的上级机构
            //

            //DataTable dtpage1 = GetPage1(ssxzjg);
            DataTable dtpage2 = GetPage2(ssxzjg);

            //string fuwb = dtInfo.Rows[0]["签约服务包"].ToString();
            //DataSet dsSP3 = GetSPPage(ssxzjg, fyid, 3);
            //if (dsSP3 != null)
            //{
            //    for (int tabi = 0; tabi < dsSP3.Tables.Count; tabi++)
            //    {
            //        if (!dsSP3.Tables[tabi].Columns.Contains("selected"))
            //        {
            //            dsSP3.Tables[tabi].Columns.Add("selected");
            //        }
            //        for (int inner = 0; inner < dsSP3.Tables[tabi].Rows.Count; inner++)
            //        {
            //            if (fuwb.Contains(dsSP3.Tables[tabi].Rows[inner]["SPID"].ToString()))
            //            {
            //                dsSP3.Tables[tabi].Rows[inner]["selected"] = "√";
            //            }
            //        }
            //    }
            //}
            //DataSet dsSP4 = GetSPPage(ssxzjg, fyid, 4);
            //if (dsSP4 != null)
            //{
            //    for (int tabi = 0; tabi < dsSP4.Tables.Count; tabi++)
            //    {
            //        if (!dsSP4.Tables[tabi].Columns.Contains("selected"))
            //        {
            //            dsSP4.Tables[tabi].Columns.Add("selected");
            //        }
            //        for (int inner = 0; inner < dsSP4.Tables[tabi].Rows.Count; inner++)
            //        {
            //            if (fuwb.Contains(dsSP4.Tables[tabi].Rows[inner]["SPID"].ToString()))
            //            {
            //                dsSP4.Tables[tabi].Rows[inner]["selected"] = "√";
            //            }
            //        }
            //    }
            //}

            DataTable dtRQList = GetRQList(ssxzjg);

            string teamid = dtInfo.Rows[0][tb_JTYS签约信息.团队ID].ToString();
            //获取队长姓名、电话，成员姓名电话，分两个datatable

            DataSet dsTeamCapAndMemInfo = bll团队.GetTeamInfoByteamid(teamid);
            string zzlb = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长类别"].ToString();
            string zzbh = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长编号"].ToString();
            string capID = "";
            DataRow[] drsCap = dsTeamCapAndMemInfo.Tables[1].Select("成员类别='" + zzlb + "' and 成员ID='" + zzbh + "'");
            if (drsCap.Length > 0)
            {
                capID = drsCap[0]["ID"].ToString();
            }
            DataTable dtMem = dsTeamCapAndMemInfo.Tables[1];


            DataTable dtOtherProperty = GetPageOtherProperty(ssxzjg);
            DataTable dtpage3 = GetPage3(ssxzjg);
            DataTable dtpage4 = Getpage4(ssxzjg);

            string name = dtInfo.Rows[0]["姓名"].ToString();
            string tel = dtInfo.Rows[0][tb_JTYS签约信息.联系电话].ToString();
            string sfzh = dtInfo.Rows[0]["身份证号"].ToString();
            string dah = dtInfo.Rows[0][tb_JTYS签约信息.档案号].ToString();
            string addr = dtInfo.Rows[0]["地址"].ToString();
            string qyrq = dtInfo.Rows[0]["签约日期"].ToString();
            qyrq = Convert.ToDateTime(qyrq).ToString(" yyyy年 MM月 dd日 ");

            string sqz = dtInfo.Rows[0]["手签照"].ToString();
            string dxlx = dtInfo.Rows[0]["签约对象类型"].ToString();
            string qyrid = dtInfo.Rows[0]["签约人"].ToString();

            //string fyid = dtInfo.Rows[0]["费用ID"].ToString();
            string yssqz = bll签约.Get医生签名(dtInfo.Rows[0]["费用ID"].ToString());
            //调用打印功能
            Report.ReportComm rep = new Report.ReportComm(dtpage2, dtRQList, dtMem, dtOtherProperty, dtpage3, dtpage4, dtInfosub, capID,
                name, tel, sfzh, dah, addr, qyrq, sqz, dxlx, qyrid, yssqz);
            rep.ShowPreviewDialog2();

            //获取是否打印标志
            //更新打印标志
            bool isPrinted = rep.GetPrintState();
            if (isPrinted)
            {
                bll签约.UpdatePrintedState(qyid, true);
            }
        }
    }
}
