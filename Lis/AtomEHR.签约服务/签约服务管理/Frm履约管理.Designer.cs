﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.签约服务.签约服务管理
{
    partial class Frm履约管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm履约管理));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chkMe = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtMargCard = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo服务包类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.date生效日期E = new DevExpress.XtraEditors.DateEdit();
            this.date生效日期B = new DevExpress.XtraEditors.DateEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.ck含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.txt签约医生 = new DevExpress.XtraEditors.TextEdit();
            this.tllue所属机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.BtN导出 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn履约记录单 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.gc项目执行明细 = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiPrintBarcode = new System.Windows.Forms.ToolStripMenuItem();
            this.输入履约执行结果ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.查询履约结果ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gv项目执行明细 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服务时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服务医生 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col执行数量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col门诊号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gc签约项目 = new DevExpress.XtraGrid.GridControl();
            this.gv签约项目 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服务包名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col项目名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col签约次数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col已服务次数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col项目单位 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gc签约信息 = new AtomEHR.Library.UserControls.DataGridControl();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.查看执行情况ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gv签约信息 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col健康档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col签约包 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col生效日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col到期日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col签约医生 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid签约来源 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid费用ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMargCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo服务包类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期E.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期E.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期B.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc项目执行明细)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv项目执行明细)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc签约项目)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv签约项目)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc签约信息)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv签约信息)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Controls.Add(this.panelControl1);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.layoutControl1);
            this.tpSummary.Size = new System.Drawing.Size(1019, 448);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(1025, 454);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(1025, 454);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(1025, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(847, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(650, 2);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chkMe);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.txtMargCard);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.textEdit地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.cbo服务包类型);
            this.layoutControl1.Controls.Add(this.date生效日期E);
            this.layoutControl1.Controls.Add(this.date生效日期B);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.ck含下属机构);
            this.layoutControl1.Controls.Add(this.txt签约医生);
            this.layoutControl1.Controls.Add(this.tllue所属机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(271, 167, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1019, 114);
            this.layoutControl1.TabIndex = 33;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // chkMe
            // 
            this.chkMe.Location = new System.Drawing.Point(546, 80);
            this.chkMe.Name = "chkMe";
            this.chkMe.Properties.Caption = "录入人是我";
            this.chkMe.Size = new System.Drawing.Size(225, 19);
            this.chkMe.StyleController = this.layoutControl1;
            this.chkMe.TabIndex = 19;
            this.chkMe.CheckedChanged += new System.EventHandler(this.chkMe_CheckedChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(775, 80);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(194, 14);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "只显示状态为“已签约”的签约记录";
            // 
            // txtMargCard
            // 
            this.txtMargCard.Location = new System.Drawing.Point(840, 56);
            this.txtMargCard.Name = "txtMargCard";
            this.txtMargCard.Size = new System.Drawing.Size(167, 20);
            this.txtMargCard.StyleController = this.layoutControl1;
            this.txtMargCard.TabIndex = 17;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(217, 80);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Size = new System.Drawing.Size(141, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 16;
            // 
            // textEdit地址
            // 
            this.textEdit地址.Location = new System.Drawing.Point(362, 80);
            this.textEdit地址.Name = "textEdit地址";
            this.textEdit地址.Size = new System.Drawing.Size(180, 20);
            this.textEdit地址.StyleController = this.layoutControl1;
            this.textEdit地址.TabIndex = 15;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(87, 80);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Size = new System.Drawing.Size(126, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 13;
            this.comboBoxEdit镇.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit镇_SelectedIndexChanged);
            // 
            // cbo服务包类型
            // 
            this.cbo服务包类型.Location = new System.Drawing.Point(417, 56);
            this.cbo服务包类型.Name = "cbo服务包类型";
            this.cbo服务包类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo服务包类型.Size = new System.Drawing.Size(125, 20);
            this.cbo服务包类型.StyleController = this.layoutControl1;
            this.cbo服务包类型.TabIndex = 11;
            // 
            // date生效日期E
            // 
            this.date生效日期E.EditValue = null;
            this.date生效日期E.Location = new System.Drawing.Point(232, 56);
            this.date生效日期E.Name = "date生效日期E";
            this.date生效日期E.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date生效日期E.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date生效日期E.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.date生效日期E.Size = new System.Drawing.Size(126, 20);
            this.date生效日期E.StyleController = this.layoutControl1;
            this.date生效日期E.TabIndex = 10;
            // 
            // date生效日期B
            // 
            this.date生效日期B.EditValue = null;
            this.date生效日期B.Location = new System.Drawing.Point(87, 56);
            this.date生效日期B.Name = "date生效日期B";
            this.date生效日期B.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date生效日期B.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date生效日期B.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.date生效日期B.Size = new System.Drawing.Size(126, 20);
            this.date生效日期B.StyleController = this.layoutControl1;
            this.date生效日期B.TabIndex = 9;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(621, 31);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(150, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 8;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(840, 31);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(167, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(417, 31);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(125, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 6;
            // 
            // ck含下属机构
            // 
            this.ck含下属机构.EditValue = true;
            this.ck含下属机构.Location = new System.Drawing.Point(217, 31);
            this.ck含下属机构.Name = "ck含下属机构";
            this.ck含下属机构.Properties.Caption = "含下属机构";
            this.ck含下属机构.Size = new System.Drawing.Size(141, 19);
            this.ck含下属机构.StyleController = this.layoutControl1;
            this.ck含下属机构.TabIndex = 5;
            // 
            // txt签约医生
            // 
            this.txt签约医生.Location = new System.Drawing.Point(621, 56);
            this.txt签约医生.Name = "txt签约医生";
            this.txt签约医生.Size = new System.Drawing.Size(150, 20);
            this.txt签约医生.StyleController = this.layoutControl1;
            this.txt签约医生.TabIndex = 12;
            // 
            // tllue所属机构
            // 
            this.tllue所属机构.Location = new System.Drawing.Point(87, 31);
            this.tllue所属机构.Name = "tllue所属机构";
            this.tllue所属机构.Properties.AutoExpandAllNodes = false;
            this.tllue所属机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tllue所属机构.Properties.NullText = "";
            this.tllue所属机构.Properties.PopupSizeable = false;
            this.tllue所属机构.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tllue所属机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.tllue所属机构.Size = new System.Drawing.Size(126, 20);
            this.tllue所属机构.StyleController = this.layoutControl1;
            this.tllue所属机构.TabIndex = 4;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "签约服务查询";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem12,
            this.layoutControlItem11,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem13,
            this.emptySpaceItem1,
            this.layoutControlItem14,
            this.layoutControlItem15});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1019, 114);
            this.layoutControlGroup1.Text = "查询条件";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.tllue所属机构;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(205, 25);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(70, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ck含下属机构;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(205, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(90, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(145, 25);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.cbo服务包类型;
            this.layoutControlItem8.CustomizationFormText = "服务包类型：";
            this.layoutControlItem8.Location = new System.Drawing.Point(350, 25);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem8.Text = "服务包：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.comboBoxEdit镇;
            this.layoutControlItem10.CustomizationFormText = "居住地址：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(205, 26);
            this.layoutControlItem10.Text = "居住地址：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt签约医生;
            this.layoutControlItem9.CustomizationFormText = "签约医生：";
            this.layoutControlItem9.Location = new System.Drawing.Point(534, 25);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem9.Text = "签约医生：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit地址;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(350, 49);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(184, 26);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.comboBoxEdit村;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(205, 49);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(145, 26);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt姓名;
            this.layoutControlItem3.CustomizationFormText = "姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(350, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(94, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(184, 25);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "姓名：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt档案号;
            this.layoutControlItem5.CustomizationFormText = "健康档案号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(534, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(229, 25);
            this.layoutControlItem5.Text = "健康档案号：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.date生效日期B;
            this.layoutControlItem6.CustomizationFormText = "生效日期：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(205, 24);
            this.layoutControlItem6.Text = "生效日期：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.date生效日期E;
            this.layoutControlItem7.CustomizationFormText = "~";
            this.layoutControlItem7.Location = new System.Drawing.Point(205, 25);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(145, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "~";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(763, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(236, 25);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtMargCard;
            this.layoutControlItem13.CustomizationFormText = "磁条卡号：";
            this.layoutControlItem13.Location = new System.Drawing.Point(763, 25);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem13.Text = "磁条卡号：";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(961, 49);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(38, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.labelControl1;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(763, 49);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(198, 26);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.chkMe;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(534, 49);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(229, 26);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn查询);
            this.flowLayoutPanel1.Controls.Add(this.BtN导出);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Controls.Add(this.btn履约记录单);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 114);
            this.flowLayoutPanel1.MaximumSize = new System.Drawing.Size(0, 30);
            this.flowLayoutPanel1.MinimumSize = new System.Drawing.Size(0, 30);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 30);
            this.flowLayoutPanel1.TabIndex = 34;
            // 
            // btn查询
            // 
            this.btn查询.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn查询.ImageOptions.Image")));
            this.btn查询.Location = new System.Drawing.Point(103, 3);
            this.btn查询.MaximumSize = new System.Drawing.Size(70, 25);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(70, 25);
            this.btn查询.TabIndex = 0;
            this.btn查询.Text = "查询";
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // BtN导出
            // 
            this.BtN导出.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtN导出.ImageOptions.Image")));
            this.BtN导出.Location = new System.Drawing.Point(196, 3);
            this.BtN导出.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.BtN导出.MaximumSize = new System.Drawing.Size(70, 25);
            this.BtN导出.Name = "BtN导出";
            this.BtN导出.Size = new System.Drawing.Size(70, 25);
            this.BtN导出.TabIndex = 1;
            this.BtN导出.Text = "导出";
            this.BtN导出.Click += new System.EventHandler(this.BtN导出_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(289, 3);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(131, 23);
            this.simpleButton1.TabIndex = 2;
            this.simpleButton1.Text = "自助签约费用确认";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btn履约记录单
            // 
            this.btn履约记录单.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn履约记录单.ImageOptions.Image")));
            this.btn履约记录单.Location = new System.Drawing.Point(443, 3);
            this.btn履约记录单.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.btn履约记录单.Name = "btn履约记录单";
            this.btn履约记录单.Size = new System.Drawing.Size(99, 23);
            this.btn履约记录单.TabIndex = 2;
            this.btn履约记录单.Text = "履约记录单";
            this.btn履约记录单.Click += new System.EventHandler(this.btn履约记录单_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl3);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl1.Location = new System.Drawing.Point(560, 144);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(459, 304);
            this.panelControl1.TabIndex = 35;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gc项目执行明细);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(2, 153);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(455, 149);
            this.groupControl3.TabIndex = 31;
            this.groupControl3.Text = "服务执行明细";
            // 
            // gc项目执行明细
            // 
            this.gc项目执行明细.ContextMenuStrip = this.contextMenuStrip1;
            this.gc项目执行明细.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc项目执行明细.Location = new System.Drawing.Point(2, 21);
            this.gc项目执行明细.MainView = this.gv项目执行明细;
            this.gc项目执行明细.Name = "gc项目执行明细";
            this.gc项目执行明细.Size = new System.Drawing.Size(451, 126);
            this.gc项目执行明细.TabIndex = 0;
            this.gc项目执行明细.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv项目执行明细});
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiPrintBarcode,
            this.输入履约执行结果ToolStripMenuItem,
            this.查询履约结果ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(191, 76);
            // 
            // tsmiPrintBarcode
            // 
            this.tsmiPrintBarcode.Name = "tsmiPrintBarcode";
            this.tsmiPrintBarcode.Size = new System.Drawing.Size(190, 24);
            this.tsmiPrintBarcode.Text = "打印选中条码";
            this.tsmiPrintBarcode.Click += new System.EventHandler(this.tsmiPrintBarcode_Click);
            // 
            // 输入履约执行结果ToolStripMenuItem
            // 
            this.输入履约执行结果ToolStripMenuItem.Name = "输入履约执行结果ToolStripMenuItem";
            this.输入履约执行结果ToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.输入履约执行结果ToolStripMenuItem.Text = "输入履约结果";
            this.输入履约执行结果ToolStripMenuItem.ToolTipText = "例如记录测量的血压值";
            this.输入履约执行结果ToolStripMenuItem.Click += new System.EventHandler(this.输入履约执行结果ToolStripMenuItem_Click);
            // 
            // 查询履约结果ToolStripMenuItem
            // 
            this.查询履约结果ToolStripMenuItem.Name = "查询履约结果ToolStripMenuItem";
            this.查询履约结果ToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.查询履约结果ToolStripMenuItem.Text = "查询履约结果明细";
            this.查询履约结果ToolStripMenuItem.Click += new System.EventHandler(this.查询履约结果ToolStripMenuItem_Click);
            // 
            // gv项目执行明细
            // 
            this.gv项目执行明细.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn15,
            this.gridColumn11,
            this.gridColumn10,
            this.col服务时间,
            this.gridColumn13,
            this.gridColumn14,
            this.col服务医生,
            this.col执行数量,
            this.gridColumn9,
            this.col门诊号,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn16});
            this.gv项目执行明细.GridControl = this.gc项目执行明细;
            this.gv项目执行明细.Name = "gv项目执行明细";
            this.gv项目执行明细.OptionsView.ColumnAutoWidth = false;
            this.gv项目执行明细.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "ID";
            this.gridColumn12.FieldName = "ID";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "签约ID";
            this.gridColumn15.FieldName = "签约ID";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "ServiceID";
            this.gridColumn11.FieldName = "ServiceID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "项目复合ID";
            this.gridColumn10.FieldName = "项目复合ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            // 
            // col服务时间
            // 
            this.col服务时间.AppearanceCell.Options.UseTextOptions = true;
            this.col服务时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col服务时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务时间.Caption = "服务时间";
            this.col服务时间.FieldName = "服务时间";
            this.col服务时间.Name = "col服务时间";
            this.col服务时间.OptionsColumn.AllowEdit = false;
            this.col服务时间.OptionsColumn.ReadOnly = true;
            this.col服务时间.Visible = true;
            this.col服务时间.VisibleIndex = 0;
            this.col服务时间.Width = 87;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "服务包名称";
            this.gridColumn13.FieldName = "服务包名称";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 3;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "项目名称";
            this.gridColumn14.FieldName = "项目名称";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 5;
            // 
            // col服务医生
            // 
            this.col服务医生.AppearanceCell.Options.UseTextOptions = true;
            this.col服务医生.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务医生.AppearanceHeader.Options.UseTextOptions = true;
            this.col服务医生.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务医生.Caption = "服务医生";
            this.col服务医生.FieldName = "服务医生姓名";
            this.col服务医生.Name = "col服务医生";
            this.col服务医生.OptionsColumn.AllowEdit = false;
            this.col服务医生.OptionsColumn.ReadOnly = true;
            this.col服务医生.Visible = true;
            this.col服务医生.VisibleIndex = 1;
            // 
            // col执行数量
            // 
            this.col执行数量.AppearanceCell.Options.UseTextOptions = true;
            this.col执行数量.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col执行数量.AppearanceHeader.Options.UseTextOptions = true;
            this.col执行数量.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col执行数量.Caption = "执行数量";
            this.col执行数量.FieldName = "执行数量";
            this.col执行数量.Name = "col执行数量";
            this.col执行数量.OptionsColumn.AllowEdit = false;
            this.col执行数量.OptionsColumn.ReadOnly = true;
            this.col执行数量.Visible = true;
            this.col执行数量.VisibleIndex = 2;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "项目单位";
            this.gridColumn9.FieldName = "项目单位";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 4;
            // 
            // col门诊号
            // 
            this.col门诊号.AppearanceCell.Options.UseTextOptions = true;
            this.col门诊号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col门诊号.AppearanceHeader.Options.UseTextOptions = true;
            this.col门诊号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col门诊号.Caption = "门诊号";
            this.col门诊号.Name = "col门诊号";
            this.col门诊号.OptionsColumn.AllowEdit = false;
            this.col门诊号.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "执行科室";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "开单科室";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "执行条码";
            this.gridColumn16.FieldName = "执行条码";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            this.gridColumn16.Width = 180;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gc签约项目);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl2.Location = new System.Drawing.Point(2, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(455, 151);
            this.groupControl2.TabIndex = 30;
            this.groupControl2.Text = "签约项目（双击此列表进行履约）";
            // 
            // gc签约项目
            // 
            this.gc签约项目.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc签约项目.Location = new System.Drawing.Point(2, 21);
            this.gc签约项目.MainView = this.gv签约项目;
            this.gc签约项目.Name = "gc签约项目";
            this.gc签约项目.Size = new System.Drawing.Size(451, 128);
            this.gc签约项目.TabIndex = 0;
            this.gc签约项目.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv签约项目});
            this.gc签约项目.DoubleClick += new System.EventHandler(this.gc签约项目情况_DoubleClick);
            // 
            // gv签约项目
            // 
            this.gv签约项目.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn4,
            this.col服务包名称,
            this.col项目名称,
            this.col签约次数,
            this.col已服务次数,
            this.col项目单位});
            this.gv签约项目.GridControl = this.gc签约项目;
            this.gv签约项目.Name = "gv签约项目";
            this.gv签约项目.OptionsBehavior.Editable = false;
            this.gv签约项目.OptionsView.ColumnAutoWidth = false;
            this.gv签约项目.OptionsView.ShowGroupPanel = false;
            this.gv签约项目.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv签约项目_CustomDrawCell);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "签约ID";
            this.gridColumn5.FieldName = "签约ID";
            this.gridColumn5.Name = "gridColumn5";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "ServiceID";
            this.gridColumn6.FieldName = "ServiceID";
            this.gridColumn6.Name = "gridColumn6";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "项目复合ID";
            this.gridColumn4.FieldName = "项目复合ID";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // col服务包名称
            // 
            this.col服务包名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col服务包名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务包名称.Caption = "服务包名称";
            this.col服务包名称.FieldName = "服务包名称";
            this.col服务包名称.Name = "col服务包名称";
            this.col服务包名称.OptionsColumn.ReadOnly = true;
            this.col服务包名称.Visible = true;
            this.col服务包名称.VisibleIndex = 0;
            this.col服务包名称.Width = 100;
            // 
            // col项目名称
            // 
            this.col项目名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col项目名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col项目名称.Caption = "项目名称";
            this.col项目名称.FieldName = "项目名称";
            this.col项目名称.Name = "col项目名称";
            this.col项目名称.OptionsColumn.ReadOnly = true;
            this.col项目名称.Visible = true;
            this.col项目名称.VisibleIndex = 1;
            this.col项目名称.Width = 160;
            // 
            // col签约次数
            // 
            this.col签约次数.AppearanceHeader.Options.UseTextOptions = true;
            this.col签约次数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col签约次数.Caption = "签约次数";
            this.col签约次数.FieldName = "数量";
            this.col签约次数.Name = "col签约次数";
            this.col签约次数.OptionsColumn.ReadOnly = true;
            this.col签约次数.Visible = true;
            this.col签约次数.VisibleIndex = 2;
            this.col签约次数.Width = 60;
            // 
            // col已服务次数
            // 
            this.col已服务次数.AppearanceHeader.Options.UseTextOptions = true;
            this.col已服务次数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col已服务次数.Caption = "已服务次数";
            this.col已服务次数.FieldName = "执行总量";
            this.col已服务次数.Name = "col已服务次数";
            this.col已服务次数.OptionsColumn.ReadOnly = true;
            this.col已服务次数.Visible = true;
            this.col已服务次数.VisibleIndex = 3;
            // 
            // col项目单位
            // 
            this.col项目单位.AppearanceHeader.Options.UseTextOptions = true;
            this.col项目单位.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col项目单位.Caption = "项目单位";
            this.col项目单位.FieldName = "单位";
            this.col项目单位.Name = "col项目单位";
            this.col项目单位.OptionsColumn.ReadOnly = true;
            this.col项目单位.Visible = true;
            this.col项目单位.VisibleIndex = 4;
            this.col项目单位.Width = 60;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gc签约信息);
            this.groupControl1.Controls.Add(this.pagerControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 144);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(560, 304);
            this.groupControl1.TabIndex = 36;
            this.groupControl1.Text = "签约信息（双击查看履约情况）";
            // 
            // gc签约信息
            // 
            this.gc签约信息.AllowBandedGridColumnSort = false;
            this.gc签约信息.ContextMenuStrip = this.contextMenuStrip2;
            this.gc签约信息.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc签约信息.IsBestFitColumns = true;
            this.gc签约信息.Location = new System.Drawing.Point(2, 21);
            this.gc签约信息.MainView = this.gv签约信息;
            this.gc签约信息.Name = "gc签约信息";
            this.gc签约信息.ShowContextMenu = true;
            this.gc签约信息.Size = new System.Drawing.Size(556, 247);
            this.gc签约信息.StrWhere = "";
            this.gc签约信息.TabIndex = 0;
            this.gc签约信息.UseCheckBox = false;
            this.gc签约信息.View = "";
            this.gc签约信息.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv签约信息});
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查看执行情况ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(191, 28);
            // 
            // 查看执行情况ToolStripMenuItem
            // 
            this.查看执行情况ToolStripMenuItem.Name = "查看执行情况ToolStripMenuItem";
            this.查看执行情况ToolStripMenuItem.Size = new System.Drawing.Size(190, 24);
            this.查看执行情况ToolStripMenuItem.Text = "查询履约结果明细";
            this.查看执行情况ToolStripMenuItem.Click += new System.EventHandler(this.查看执行情况ToolStripMenuItem_Click);
            // 
            // gv签约信息
            // 
            this.gv签约信息.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn2,
            this.gridColumn1,
            this.col健康档案号,
            this.col姓名,
            this.col性别,
            this.col身份证号,
            this.gridColumn18,
            this.gridColumn17,
            this.col签约包,
            this.col生效日期,
            this.col到期日期,
            this.col状态,
            this.col签约医生,
            this.grid签约来源,
            this.grid费用ID});
            this.gv签约信息.GridControl = this.gc签约信息;
            this.gv签约信息.Name = "gv签约信息";
            this.gv签约信息.OptionsBehavior.Editable = false;
            this.gv签约信息.OptionsView.ColumnAutoWidth = false;
            this.gv签约信息.OptionsView.ShowGroupPanel = false;
            this.gv签约信息.DoubleClick += new System.EventHandler(this.gv签约服务_DoubleClick);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "签约ID";
            this.gridColumn3.FieldName = "ID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "所属机构名称";
            this.gridColumn2.FieldName = "所属机构名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "磁卡号";
            this.gridColumn1.FieldName = "磁卡号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // col健康档案号
            // 
            this.col健康档案号.AppearanceHeader.Options.UseTextOptions = true;
            this.col健康档案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col健康档案号.Caption = "健康档案号";
            this.col健康档案号.FieldName = "档案号";
            this.col健康档案号.Name = "col健康档案号";
            this.col健康档案号.OptionsColumn.ReadOnly = true;
            this.col健康档案号.Visible = true;
            this.col健康档案号.VisibleIndex = 2;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 3;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别名称";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 4;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 5;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "签约对象类型";
            this.gridColumn18.FieldName = "签约对象类型名称";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 6;
            this.gridColumn18.Width = 100;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "健康状况";
            this.gridColumn17.FieldName = "健康状况名称";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            // 
            // col签约包
            // 
            this.col签约包.AppearanceHeader.Options.UseTextOptions = true;
            this.col签约包.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col签约包.Caption = "签约包";
            this.col签约包.FieldName = "服务包名称";
            this.col签约包.Name = "col签约包";
            this.col签约包.OptionsColumn.ReadOnly = true;
            this.col签约包.Visible = true;
            this.col签约包.VisibleIndex = 8;
            // 
            // col生效日期
            // 
            this.col生效日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col生效日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col生效日期.Caption = "生效日期";
            this.col生效日期.FieldName = "生效日期";
            this.col生效日期.Name = "col生效日期";
            this.col生效日期.OptionsColumn.ReadOnly = true;
            this.col生效日期.Visible = true;
            this.col生效日期.VisibleIndex = 9;
            // 
            // col到期日期
            // 
            this.col到期日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col到期日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col到期日期.Caption = "到期日期";
            this.col到期日期.FieldName = "到期日期";
            this.col到期日期.Name = "col到期日期";
            this.col到期日期.OptionsColumn.ReadOnly = true;
            this.col到期日期.Visible = true;
            this.col到期日期.VisibleIndex = 10;
            // 
            // col状态
            // 
            this.col状态.AppearanceHeader.Options.UseTextOptions = true;
            this.col状态.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col状态.Caption = "状态";
            this.col状态.FieldName = "签约状态";
            this.col状态.Name = "col状态";
            this.col状态.OptionsColumn.ReadOnly = true;
            this.col状态.Visible = true;
            this.col状态.VisibleIndex = 11;
            // 
            // col签约医生
            // 
            this.col签约医生.AppearanceHeader.Options.UseTextOptions = true;
            this.col签约医生.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col签约医生.Caption = "签约医生";
            this.col签约医生.FieldName = "签约医生姓名";
            this.col签约医生.Name = "col签约医生";
            this.col签约医生.OptionsColumn.ReadOnly = true;
            this.col签约医生.Visible = true;
            this.col签约医生.VisibleIndex = 12;
            // 
            // grid签约来源
            // 
            this.grid签约来源.Caption = "签约来源";
            this.grid签约来源.FieldName = "签约来源";
            this.grid签约来源.Name = "grid签约来源";
            this.grid签约来源.Visible = true;
            this.grid签约来源.VisibleIndex = 13;
            // 
            // grid费用ID
            // 
            this.grid费用ID.Caption = "费用ID";
            this.grid费用ID.FieldName = "费用ID";
            this.grid费用ID.Name = "grid费用ID";
            this.grid费用ID.Visible = true;
            this.grid费用ID.VisibleIndex = 14;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(2, 268);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 34);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 10;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(556, 34);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 130;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // Frm履约管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 480);
            this.Name = "Frm履约管理";
            this.Text = "履约管理";
            this.Load += new System.EventHandler(this.Frm履约管理_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.gcNavigator, 0);
            this.Controls.SetChildIndex(this.pnlSummary, 0);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkMe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMargCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo服务包类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期E.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期E.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期B.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc项目执行明细)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gv项目执行明细)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc签约项目)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv签约项目)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc签约信息)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gv签约信息)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.TextEdit textEdit地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraEditors.ComboBoxEdit cbo服务包类型;
        private DevExpress.XtraEditors.DateEdit date生效日期E;
        private DevExpress.XtraEditors.DateEdit date生效日期B;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.CheckEdit ck含下属机构;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn查询;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gc签约项目;
        private DevExpress.XtraGrid.Views.Grid.GridView gv签约项目;
        private DevExpress.XtraGrid.Columns.GridColumn col服务包名称;
        private DevExpress.XtraGrid.Columns.GridColumn col项目名称;
        private DevExpress.XtraGrid.Columns.GridColumn col签约次数;
        private DevExpress.XtraGrid.Columns.GridColumn col已服务次数;
        private DevExpress.XtraGrid.Columns.GridColumn col项目单位;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl gc项目执行明细;
        private DevExpress.XtraGrid.Views.Grid.GridView gv项目执行明细;
        private DevExpress.XtraGrid.Columns.GridColumn col服务时间;
        private DevExpress.XtraGrid.Columns.GridColumn col服务医生;
        private DevExpress.XtraGrid.Columns.GridColumn col执行数量;
        private DevExpress.XtraGrid.Columns.GridColumn col门诊号;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DataGridControl gc签约信息;
        private DevExpress.XtraGrid.Views.Grid.GridView gv签约信息;
        private DevExpress.XtraGrid.Columns.GridColumn col健康档案号;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col签约包;
        private DevExpress.XtraGrid.Columns.GridColumn col生效日期;
        private DevExpress.XtraGrid.Columns.GridColumn col到期日期;
        private DevExpress.XtraGrid.Columns.GridColumn col状态;
        private DevExpress.XtraGrid.Columns.GridColumn col签约医生;
        private DevExpress.XtraEditors.TextEdit txtMargCard;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.TextEdit txt签约医生;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TreeListLookUpEdit tllue所属机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.CheckEdit chkMe;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiPrintBarcode;
        private System.Windows.Forms.ToolStripMenuItem 输入履约执行结果ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 查询履约结果ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem 查看执行情况ToolStripMenuItem;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.SimpleButton BtN导出;
        private DevExpress.XtraGrid.Columns.GridColumn grid签约来源;
        private DevExpress.XtraGrid.Columns.GridColumn grid费用ID;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btn履约记录单;
    }
}