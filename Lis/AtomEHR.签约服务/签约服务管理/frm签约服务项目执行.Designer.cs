﻿namespace AtomEHR.签约服务.签约服务管理
{
    partial class frm签约服务项目执行
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm签约服务项目执行));
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt签约日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt签约医生 = new DevExpress.XtraEditors.TextEdit();
            this.txt服务医生 = new DevExpress.XtraEditors.TextEdit();
            this.de执行日期 = new DevExpress.XtraEditors.DateEdit();
            this.gc签约服务项目明细 = new DevExpress.XtraGrid.GridControl();
            this.gv签约服务项目明细 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcol签约ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolServiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol项目复合ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol服务包名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol项目名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol签约次数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gco总价 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol已服务次数 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol执行数量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol项目单位 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol检验项目 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol检查项目 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol项目分组 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol备注 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol执行科室 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtPackageNames = new DevExpress.XtraEditors.TextEdit();
            this.txtTelNo = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtSFZH = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtDabh = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labcNotice = new DevExpress.XtraEditors.LabelControl();
            this.sbtnDone = new DevExpress.XtraEditors.SimpleButton();
            this.btn取消 = new DevExpress.XtraEditors.SimpleButton();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.txt执行科室 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt开单科室 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服务医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de执行日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de执行日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc签约服务项目明细)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv签约服务项目明细)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPackageNames.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSFZH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDabh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt执行科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开单科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup1.CustomizationFormText = "签约服务项目执行";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(757, 516);
            this.layoutControlGroup1.Text = "签约服务项目执行";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "签约服务项目执行";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.Size = new System.Drawing.Size(757, 516);
            this.layoutControlGroup2.Text = "签约服务项目执行";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt签约日期);
            this.layoutControl1.Controls.Add(this.txt签约医生);
            this.layoutControl1.Controls.Add(this.txt服务医生);
            this.layoutControl1.Controls.Add(this.de执行日期);
            this.layoutControl1.Controls.Add(this.gc签约服务项目明细);
            this.layoutControl1.Controls.Add(this.txtPackageNames);
            this.layoutControl1.Controls.Add(this.txtTelNo);
            this.layoutControl1.Controls.Add(this.txtAddress);
            this.layoutControl1.Controls.Add(this.txtSFZH);
            this.layoutControl1.Controls.Add(this.txtName);
            this.layoutControl1.Controls.Add(this.txtDabh);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.txt执行科室);
            this.layoutControl1.Controls.Add(this.txt开单科室);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(259, 198, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(889, 493);
            this.layoutControl1.TabIndex = 126;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt签约日期
            // 
            this.txt签约日期.Location = new System.Drawing.Point(391, 85);
            this.txt签约日期.Name = "txt签约日期";
            this.txt签约日期.Properties.ReadOnly = true;
            this.txt签约日期.Size = new System.Drawing.Size(150, 20);
            this.txt签约日期.StyleController = this.layoutControl1;
            this.txt签约日期.TabIndex = 132;
            // 
            // txt签约医生
            // 
            this.txt签约医生.Location = new System.Drawing.Point(88, 85);
            this.txt签约医生.Name = "txt签约医生";
            this.txt签约医生.Properties.ReadOnly = true;
            this.txt签约医生.Size = new System.Drawing.Size(224, 20);
            this.txt签约医生.StyleController = this.layoutControl1;
            this.txt签约医生.TabIndex = 131;
            // 
            // txt服务医生
            // 
            this.txt服务医生.Enabled = false;
            this.txt服务医生.Location = new System.Drawing.Point(777, 419);
            this.txt服务医生.Name = "txt服务医生";
            this.txt服务医生.Size = new System.Drawing.Size(99, 20);
            this.txt服务医生.StyleController = this.layoutControl1;
            this.txt服务医生.TabIndex = 130;
            // 
            // de执行日期
            // 
            this.de执行日期.EditValue = null;
            this.de执行日期.Location = new System.Drawing.Point(78, 419);
            this.de执行日期.Name = "de执行日期";
            this.de执行日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de执行日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de执行日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de执行日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de执行日期.Size = new System.Drawing.Size(177, 20);
            this.de执行日期.StyleController = this.layoutControl1;
            this.de执行日期.TabIndex = 128;
            // 
            // gc签约服务项目明细
            // 
            this.gc签约服务项目明细.EmbeddedNavigator.Appearance.Options.UseTextOptions = true;
            this.gc签约服务项目明细.EmbeddedNavigator.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gc签约服务项目明细.Location = new System.Drawing.Point(13, 109);
            this.gc签约服务项目明细.MainView = this.gv签约服务项目明细;
            this.gc签约服务项目明细.Name = "gc签约服务项目明细";
            this.gc签约服务项目明细.Size = new System.Drawing.Size(863, 306);
            this.gc签约服务项目明细.TabIndex = 128;
            this.gc签约服务项目明细.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv签约服务项目明细});
            // 
            // gv签约服务项目明细
            // 
            this.gv签约服务项目明细.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcol签约ID,
            this.gcolServiceID,
            this.gcol项目复合ID,
            this.gcol服务包名称,
            this.gcol项目名称,
            this.gcol签约次数,
            this.gco总价,
            this.gcol已服务次数,
            this.gcol执行数量,
            this.gcol项目单位,
            this.gcol检验项目,
            this.gcol检查项目,
            this.gcol项目分组,
            this.gcol备注,
            this.gcol执行科室});
            this.gv签约服务项目明细.GridControl = this.gc签约服务项目明细;
            this.gv签约服务项目明细.Name = "gv签约服务项目明细";
            this.gv签约服务项目明细.OptionsBehavior.Editable = false;
            this.gv签约服务项目明细.OptionsSelection.MultiSelect = true;
            this.gv签约服务项目明细.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gv签约服务项目明细.OptionsView.ColumnAutoWidth = false;
            this.gv签约服务项目明细.OptionsView.ShowGroupPanel = false;
            this.gv签约服务项目明细.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gv签约服务项目明细_CustomDrawCell);
            this.gv签约服务项目明细.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gv签约服务项目明细_SelectionChanged);
            // 
            // gcol签约ID
            // 
            this.gcol签约ID.Caption = "签约ID";
            this.gcol签约ID.FieldName = "签约ID";
            this.gcol签约ID.Name = "gcol签约ID";
            this.gcol签约ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gcolServiceID
            // 
            this.gcolServiceID.Caption = "ServiceID";
            this.gcolServiceID.FieldName = "ServiceID";
            this.gcolServiceID.Name = "gcolServiceID";
            this.gcolServiceID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gcol项目复合ID
            // 
            this.gcol项目复合ID.Caption = "项目复合ID";
            this.gcol项目复合ID.FieldName = "项目复合ID";
            this.gcol项目复合ID.Name = "gcol项目复合ID";
            this.gcol项目复合ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gcol服务包名称
            // 
            this.gcol服务包名称.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol服务包名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol服务包名称.Caption = "服务包名称";
            this.gcol服务包名称.FieldName = "服务包名称";
            this.gcol服务包名称.Name = "gcol服务包名称";
            this.gcol服务包名称.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcol服务包名称.Visible = true;
            this.gcol服务包名称.VisibleIndex = 1;
            this.gcol服务包名称.Width = 94;
            // 
            // gcol项目名称
            // 
            this.gcol项目名称.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol项目名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol项目名称.Caption = "项目名称";
            this.gcol项目名称.FieldName = "项目名称";
            this.gcol项目名称.Name = "gcol项目名称";
            this.gcol项目名称.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcol项目名称.Visible = true;
            this.gcol项目名称.VisibleIndex = 2;
            this.gcol项目名称.Width = 132;
            // 
            // gcol签约次数
            // 
            this.gcol签约次数.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol签约次数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol签约次数.Caption = "签约次数";
            this.gcol签约次数.FieldName = "数量";
            this.gcol签约次数.Name = "gcol签约次数";
            this.gcol签约次数.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcol签约次数.Visible = true;
            this.gcol签约次数.VisibleIndex = 3;
            this.gcol签约次数.Width = 59;
            // 
            // gco总价
            // 
            this.gco总价.Caption = "总价";
            this.gco总价.FieldName = "总价";
            this.gco总价.Name = "gco总价";
            this.gco总价.Visible = true;
            this.gco总价.VisibleIndex = 4;
            // 
            // gcol已服务次数
            // 
            this.gcol已服务次数.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol已服务次数.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol已服务次数.Caption = "已服务次数";
            this.gcol已服务次数.FieldName = "执行总量";
            this.gcol已服务次数.Name = "gcol已服务次数";
            this.gcol已服务次数.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcol已服务次数.Visible = true;
            this.gcol已服务次数.VisibleIndex = 5;
            this.gcol已服务次数.Width = 71;
            // 
            // gcol执行数量
            // 
            this.gcol执行数量.AppearanceCell.Options.UseTextOptions = true;
            this.gcol执行数量.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gcol执行数量.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol执行数量.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol执行数量.Caption = "执行数量";
            this.gcol执行数量.FieldName = "执行数量";
            this.gcol执行数量.Name = "gcol执行数量";
            this.gcol执行数量.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcol执行数量.Visible = true;
            this.gcol执行数量.VisibleIndex = 6;
            this.gcol执行数量.Width = 59;
            // 
            // gcol项目单位
            // 
            this.gcol项目单位.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol项目单位.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol项目单位.Caption = "项目单位";
            this.gcol项目单位.FieldName = "单位";
            this.gcol项目单位.Name = "gcol项目单位";
            this.gcol项目单位.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcol项目单位.Visible = true;
            this.gcol项目单位.VisibleIndex = 7;
            this.gcol项目单位.Width = 62;
            // 
            // gcol检验项目
            // 
            this.gcol检验项目.Caption = "检验项目";
            this.gcol检验项目.FieldName = "检验项目";
            this.gcol检验项目.Name = "gcol检验项目";
            this.gcol检验项目.OptionsColumn.ReadOnly = true;
            this.gcol检验项目.Visible = true;
            this.gcol检验项目.VisibleIndex = 8;
            this.gcol检验项目.Width = 60;
            // 
            // gcol检查项目
            // 
            this.gcol检查项目.Caption = "检查项目";
            this.gcol检查项目.FieldName = "检查项目";
            this.gcol检查项目.Name = "gcol检查项目";
            this.gcol检查项目.OptionsColumn.ReadOnly = true;
            this.gcol检查项目.Visible = true;
            this.gcol检查项目.VisibleIndex = 9;
            this.gcol检查项目.Width = 60;
            // 
            // gcol项目分组
            // 
            this.gcol项目分组.Caption = "项目分组";
            this.gcol项目分组.FieldName = "项目分组";
            this.gcol项目分组.Name = "gcol项目分组";
            this.gcol项目分组.OptionsColumn.ReadOnly = true;
            this.gcol项目分组.Visible = true;
            this.gcol项目分组.VisibleIndex = 10;
            this.gcol项目分组.Width = 60;
            // 
            // gcol备注
            // 
            this.gcol备注.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol备注.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol备注.Caption = "备注";
            this.gcol备注.Name = "gcol备注";
            this.gcol备注.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcol备注.Visible = true;
            this.gcol备注.VisibleIndex = 11;
            this.gcol备注.Width = 81;
            // 
            // gcol执行科室
            // 
            this.gcol执行科室.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol执行科室.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol执行科室.Caption = "执行科室";
            this.gcol执行科室.Name = "gcol执行科室";
            this.gcol执行科室.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gcol执行科室.Visible = true;
            this.gcol执行科室.VisibleIndex = 12;
            this.gcol执行科室.Width = 81;
            // 
            // txtPackageNames
            // 
            this.txtPackageNames.Location = new System.Drawing.Point(620, 61);
            this.txtPackageNames.Name = "txtPackageNames";
            this.txtPackageNames.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.txtPackageNames.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPackageNames.Properties.Appearance.Options.UseFont = true;
            this.txtPackageNames.Properties.Appearance.Options.UseForeColor = true;
            this.txtPackageNames.Properties.ReadOnly = true;
            this.txtPackageNames.Size = new System.Drawing.Size(256, 20);
            this.txtPackageNames.StyleController = this.layoutControl1;
            this.txtPackageNames.TabIndex = 128;
            // 
            // txtTelNo
            // 
            this.txtTelNo.EditValue = "";
            this.txtTelNo.Location = new System.Drawing.Point(391, 61);
            this.txtTelNo.Name = "txtTelNo";
            this.txtTelNo.Properties.ReadOnly = true;
            this.txtTelNo.Size = new System.Drawing.Size(150, 20);
            this.txtTelNo.StyleController = this.layoutControl1;
            this.txtTelNo.TabIndex = 128;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(88, 61);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.Appearance.Options.UseTextOptions = true;
            this.txtAddress.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtAddress.Properties.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(224, 20);
            this.txtAddress.StyleController = this.layoutControl1;
            this.txtAddress.TabIndex = 128;
            // 
            // txtSFZH
            // 
            this.txtSFZH.Location = new System.Drawing.Point(620, 37);
            this.txtSFZH.Name = "txtSFZH";
            this.txtSFZH.Properties.ReadOnly = true;
            this.txtSFZH.Size = new System.Drawing.Size(256, 20);
            this.txtSFZH.StyleController = this.layoutControl1;
            this.txtSFZH.TabIndex = 128;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(391, 37);
            this.txtName.Name = "txtName";
            this.txtName.Properties.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(150, 20);
            this.txtName.StyleController = this.layoutControl1;
            this.txtName.TabIndex = 128;
            // 
            // txtDabh
            // 
            this.txtDabh.Location = new System.Drawing.Point(88, 37);
            this.txtDabh.Name = "txtDabh";
            this.txtDabh.Properties.ReadOnly = true;
            this.txtDabh.Size = new System.Drawing.Size(224, 20);
            this.txtDabh.StyleController = this.layoutControl1;
            this.txtDabh.TabIndex = 128;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labcNotice);
            this.panelControl1.Controls.Add(this.sbtnDone);
            this.panelControl1.Controls.Add(this.btn取消);
            this.panelControl1.Controls.Add(this.btn保存);
            this.panelControl1.Location = new System.Drawing.Point(13, 443);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(863, 37);
            this.panelControl1.TabIndex = 128;
            // 
            // labcNotice
            // 
            this.labcNotice.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labcNotice.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labcNotice.Appearance.Options.UseFont = true;
            this.labcNotice.Appearance.Options.UseForeColor = true;
            this.labcNotice.Location = new System.Drawing.Point(225, 12);
            this.labcNotice.Name = "labcNotice";
            this.labcNotice.Size = new System.Drawing.Size(168, 14);
            this.labcNotice.TabIndex = 3;
            this.labcNotice.Text = "已被标记为“履约完毕”状态。";
            this.labcNotice.Visible = false;
            // 
            // sbtnDone
            // 
            this.sbtnDone.Location = new System.Drawing.Point(24, 6);
            this.sbtnDone.Name = "sbtnDone";
            this.sbtnDone.Size = new System.Drawing.Size(169, 26);
            this.sbtnDone.TabIndex = 2;
            this.sbtnDone.Text = "手工标记履约完毕";
            this.sbtnDone.Click += new System.EventHandler(this.sbtnDone_Click);
            // 
            // btn取消
            // 
            this.btn取消.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn取消.Location = new System.Drawing.Point(734, 6);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(87, 27);
            this.btn取消.TabIndex = 1;
            this.btn取消.Text = "取消";
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // btn保存
            // 
            this.btn保存.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn保存.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.ImageOptions.Image")));
            this.btn保存.Location = new System.Drawing.Point(589, 6);
            this.btn保存.Margin = new System.Windows.Forms.Padding(23, 3, 47, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(82, 26);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // txt执行科室
            // 
            this.txt执行科室.Location = new System.Drawing.Point(552, 419);
            this.txt执行科室.Name = "txt执行科室";
            this.txt执行科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt执行科室.Properties.Items.AddRange(new object[] {
            "化验室",
            "放射科"});
            this.txt执行科室.Size = new System.Drawing.Size(146, 20);
            this.txt执行科室.StyleController = this.layoutControl1;
            this.txt执行科室.TabIndex = 129;
            // 
            // txt开单科室
            // 
            this.txt开单科室.Location = new System.Drawing.Point(324, 419);
            this.txt开单科室.Name = "txt开单科室";
            this.txt开单科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt开单科室.Properties.Items.AddRange(new object[] {
            "内科",
            "外科",
            "妇科",
            "儿科",
            "中医科"});
            this.txt开单科室.Size = new System.Drawing.Size(159, 20);
            this.txt开单科室.StyleController = this.layoutControl1;
            this.txt开单科室.TabIndex = 128;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup3.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup3.CustomizationFormText = "签约服务项目执行";
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem7,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.emptySpaceItem1});
            this.layoutControlGroup3.Name = "Root";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 10, 10);
            this.layoutControlGroup3.Size = new System.Drawing.Size(889, 493);
            this.layoutControlGroup3.Text = "签约服务项目执行";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.panelControl1;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 406);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 41);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(104, 41);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(867, 41);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtDabh;
            this.layoutControlItem1.CustomizationFormText = "健康档案号：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem1.Text = "健康档案号：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtName;
            this.layoutControlItem2.CustomizationFormText = "姓名：";
            this.layoutControlItem2.Location = new System.Drawing.Point(303, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem2.Text = "姓名：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtSFZH;
            this.layoutControlItem3.CustomizationFormText = "身份证号：";
            this.layoutControlItem3.Location = new System.Drawing.Point(532, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(335, 24);
            this.layoutControlItem3.Text = "身份证号：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtAddress;
            this.layoutControlItem4.CustomizationFormText = "居住地址：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem4.Text = "居住地址：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtTelNo;
            this.layoutControlItem5.CustomizationFormText = "联系电话：";
            this.layoutControlItem5.Location = new System.Drawing.Point(303, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem5.Text = "联系电话：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.Control = this.txtPackageNames;
            this.layoutControlItem6.CustomizationFormText = "服务包：";
            this.layoutControlItem6.Location = new System.Drawing.Point(532, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(335, 24);
            this.layoutControlItem6.Text = "服务包：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.de执行日期;
            this.layoutControlItem8.CustomizationFormText = "执行日期：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 382);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(246, 24);
            this.layoutControlItem8.Text = "执行日期：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txt开单科室;
            this.layoutControlItem9.CustomizationFormText = "开单科室：";
            this.layoutControlItem9.Location = new System.Drawing.Point(246, 382);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem9.Text = "开单科室：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt执行科室;
            this.layoutControlItem10.CustomizationFormText = "执行科室：";
            this.layoutControlItem10.Location = new System.Drawing.Point(474, 382);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem10.Text = "执行科室：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gc签约服务项目明细;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(867, 310);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt服务医生;
            this.layoutControlItem11.Location = new System.Drawing.Point(689, 382);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(178, 24);
            this.layoutControlItem11.Text = "服务医生：";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt签约医生;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem12.Text = "签约医生：";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txt签约日期;
            this.layoutControlItem14.Location = new System.Drawing.Point(303, 48);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem14.Text = "签约日期：";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(72, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(532, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(335, 24);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm签约服务项目执行
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 493);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.Name = "frm签约服务项目执行";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "签约服务项目执行";
            this.Load += new System.EventHandler(this.frm签约服务项目执行_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt签约日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt服务医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de执行日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de执行日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc签约服务项目明细)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv签约服务项目明细)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPackageNames.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSFZH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDabh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt执行科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开单科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn取消;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit txtDabh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtSFZH;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit txtTelNo;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.DateEdit de执行日期;
        private DevExpress.XtraGrid.GridControl gc签约服务项目明细;
        private DevExpress.XtraGrid.Views.Grid.GridView gv签约服务项目明细;
        private DevExpress.XtraGrid.Columns.GridColumn gcol服务包名称;
        private DevExpress.XtraGrid.Columns.GridColumn gcol项目名称;
        private DevExpress.XtraGrid.Columns.GridColumn gcol签约次数;
        private DevExpress.XtraGrid.Columns.GridColumn gcol已服务次数;
        private DevExpress.XtraGrid.Columns.GridColumn gcol执行数量;
        private DevExpress.XtraGrid.Columns.GridColumn gcol项目单位;
        private DevExpress.XtraGrid.Columns.GridColumn gcol备注;
        private DevExpress.XtraGrid.Columns.GridColumn gcol执行科室;
        private DevExpress.XtraEditors.TextEdit txtPackageNames;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraGrid.Columns.GridColumn gcol签约ID;
        private DevExpress.XtraGrid.Columns.GridColumn gcolServiceID;
        private DevExpress.XtraGrid.Columns.GridColumn gcol项目复合ID;
        private DevExpress.XtraGrid.Columns.GridColumn gcol检验项目;
        private DevExpress.XtraGrid.Columns.GridColumn gcol检查项目;
        private DevExpress.XtraGrid.Columns.GridColumn gcol项目分组;
        private DevExpress.XtraEditors.SimpleButton sbtnDone;
        private DevExpress.XtraEditors.LabelControl labcNotice;
        private DevExpress.XtraGrid.Columns.GridColumn gco总价;
        private DevExpress.XtraEditors.ComboBoxEdit txt执行科室;
        private DevExpress.XtraEditors.ComboBoxEdit txt开单科室;
        private DevExpress.XtraEditors.TextEdit txt服务医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit txt签约日期;
        private DevExpress.XtraEditors.TextEdit txt签约医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}