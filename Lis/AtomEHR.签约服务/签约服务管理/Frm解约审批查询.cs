﻿using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableXtraReport;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class Frm解约审批查询 : AtomEHR.Library.frmBaseBusinessForm
    {

        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        bllJTYS全局参数 bl全局 = new bllJTYS全局参数();
        bllJTYS履约执行明细 bll执行明细 = new bllJTYS履约执行明细();
        public Frm解约审批查询()
        {
            InitializeComponent();
        }

        private void Frm解约审批查询_Load(object sender, EventArgs e)
        {
            //size = layoutControl1.Height;
            this.pagerControl1.Height = 35;
            base.InitButtonsBase();
            //comboBoxEdit是否合格.EditValue = null;
            //为性别绑定信息
            //DataBinder.BindingLookupEditDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_DESC", "P_CODE");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, comboBoxEdit性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, com档案类型, "P_CODE", "P_DESC");
            //comboBoxEdit性别.EditValue = null;
            //frmGridCustomize.RegisterGrid(gv签约服务);

            //为“镇”绑定信息
            DataView dv镇 = new DataView(AtomEHR.Business.DataDictCache.Cache.t地区信息);
            dv镇.RowFilter = "上级编码='" + Loginer.CurrentUser.单位代码 + "' ";
            //DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");

            //为“所属机构"绑定信息
            try
            {
                AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                tllue所属机构.Properties.ValueMember = "机构编号";
                tllue所属机构.Properties.DisplayMember = "机构名称";

                tllue所属机构.Properties.TreeList.KeyFieldName = "机构编号";
                tllue所属机构.Properties.TreeList.ParentFieldName = "上级机构";
                tllue所属机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                tllue所属机构.Properties.DataSource = dt所属机构;
                //tllue所属机构.Properties.TreeList.ExpandAll();
                //tllue所属机构.Properties.TreeList.CollapseAll();

                tllue所属机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                tllue所属机构.Text = Loginer.CurrentUser.所属机构;
            }

            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                tllue所属机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                tllue所属机构.Properties.AutoExpandAllNodes = false;
            }

            DataTable dt服务包 = bll服务包.GetAllValidateDataWithCodeAndName();
            util.ControlsHelper.BindComboxData(dt服务包, cbo服务包类型, tb_JTYS服务包.ServiceID, tb_JTYS服务包.名称);
        }

        private void comboBoxEdit镇_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(AtomEHR.Business.DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        string strWhere = "";
        private void btn查询_Click(object sender, EventArgs e)
        {
            try
            {
                //strWhere = " and (状态='2' or  状态='5')";

                string str所属机构 = tllue所属机构.EditValue.ToString();
                this.pagerControl1.InitControl();
                this.pagerControl1.PageSize = 10;

                if (ck含下属机构.Checked)
                {
                    if (str所属机构.Length == 12)
                    {
                        strWhere += " AND [档案机构] IN (select 机构编号 from tb_机构信息 where 上级机构='" + str所属机构 + "' or 机构编号='" + str所属机构 + "' ) ";
                    }
                    else
                    {
                        strWhere += " and 档案机构 like '" + str所属机构 + "%'";
                    }
                }
                else
                {
                    strWhere += " and 档案机构='" + str所属机构 + "'";
                }
                

                if (string.IsNullOrWhiteSpace(txt姓名.Text))
                { }
                else
                {
                    strWhere += " and (姓名 like'" + DESEncrypt.DES加密(txt姓名.Text) + "%' or 姓名 like '" + this.txt姓名.Text.Trim() + "%')";
                }


                //if (!(string.IsNullOrWhiteSpace(date生效日期B.Text) || string.IsNullOrWhiteSpace(date生效日期E.Text)))
                //{
                //    strWhere += " and 生效日期 >= '" + date生效日期B.Text + "' AND 生效日期 <=  '" + date生效日期B.Text + "' ";
                //}
                //else 
                if (!(string.IsNullOrWhiteSpace(date生效日期B.Text)))
                {
                    strWhere += " and 生效日期 >= '" + date生效日期B.Text + "'";
                }
                else if (!(string.IsNullOrWhiteSpace(date生效日期E.Text)))
                {
                    strWhere += " and 生效日期 <= '" + date生效日期E.Text + "'";
                }

                string str服务包 = util.ControlsHelper.GetComboxKey(cbo服务包类型);
                if (string.IsNullOrWhiteSpace(str服务包))
                { }
                else
                {
                    strWhere += " and 签约服务包 like '%" + str服务包 + "%' ";
                }

                if (string.IsNullOrWhiteSpace(txt档案号.Text))
                { }
                else
                {
                    strWhere += " and 档案号 = '" + txt档案号.Text + "'";
                }

                if (string.IsNullOrWhiteSpace(txt身份证号.Text))
                { }
                else
                {
                    strWhere += " and 身份证号 = '" + txt身份证号.Text + "'";
                }

                if (string.IsNullOrWhiteSpace(txtMargCard.Text))
                { }
                else
                {
                    strWhere += " and 磁卡号 = '" + txtMargCard.Text + "'";
                }

                string str镇 = util.ControlsHelper.GetComboxKey(comboBoxEdit镇);
                if (string.IsNullOrWhiteSpace(str镇))
                { }
                else
                {
                    strWhere += " and 街道='" + str镇 + "' ";
                }

                string str村 = util.ControlsHelper.GetComboxKey(comboBoxEdit村);
                if (string.IsNullOrWhiteSpace(str村))
                { }
                else
                {
                    strWhere += " and 居委会='" + str村 + "' ";
                }

                if (string.IsNullOrWhiteSpace(textEdit地址.Text))
                { }
                else
                {
                    strWhere += " and 居住地址 like '%'+'" + textEdit地址.Text + "'+'%' ";
                }

                BindDataList();
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void BindDataList()
        {
            //this.pagerControl1.InitControl();
            if (string.IsNullOrWhiteSpace(strWhere))
            {
                return;
            }
            DataSet ds = this.pagerControl1.GetQueryResult("vw_JTYS解约审批", strWhere);
            this.gc签约信息.View = "vw_JTYS解约审批";
            this.gc签约信息.StrWhere = strWhere;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["姓名"] = DESEncrypt.DES解密(ds.Tables[0].Rows[i]["姓名"].ToString());
                //ds.Tables[0].Rows[i]["性别"] = _bll地区.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
            }
            this.gc签约信息.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv签约信息.BestFitColumns();//列自适应宽度
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }
    }
}
