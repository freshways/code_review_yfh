﻿using AtomEHR.Library;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class Frm执行结果 : frmBase
    {
        string m_id;
        bllJTYS履约执行明细 bll执行明细 = new bllJTYS履约执行明细();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        public Frm执行结果(string id)
        {
            InitializeComponent();
            m_id = id;
        }

        private void sbtn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {
            if(txtExcResult.Text.Trim().Length > 300)
            {
                Msg.ShowInformation("执行结果记录长度不要超过300个字。");
                return;
            }

            DataSet dsInfo = bll执行明细.GetBusinessByKey(m_id, false);
            dsInfo.Tables[0].Rows[0][tb_JTYS履约执行明细.执行结果] = txtExcResult.Text.Trim();
            dsInfo.Tables[0].Rows[0][tb_JTYS履约执行明细.修改人] = Loginer.CurrentUser.用户编码;
            dsInfo.Tables[0].Rows[0][tb_JTYS履约执行明细.修改时间] = bll执行明细.ServiceDateTime;

            bll执行明细.Save(dsInfo);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        
        private void Frm执行结果_Load(object sender, EventArgs e)
        {
            DataSet dsInfo = bll执行明细.GetBusinessByKey(m_id, false);
            if(dsInfo == null || dsInfo.Tables.Count ==0 || dsInfo.Tables[0].Rows.Count==0)
            {
                return;
            }

            txtSPname.Text = dsInfo.Tables[0].Rows[0][tb_JTYS履约执行明细.服务包名称].ToString();
            txtItem.Text = dsInfo.Tables[0].Rows[0][tb_JTYS履约执行明细.项目名称].ToString();
            txtExcResult.Text = dsInfo.Tables[0].Rows[0][tb_JTYS履约执行明细.执行结果].ToString();


            string qyid = dsInfo.Tables[0].Rows[0][tb_JTYS履约执行明细.签约ID].ToString();

            DataSet dsQYinfo = bll签约.GetBusinessByIdFromView(qyid);

            if(dsQYinfo == null || dsQYinfo.Tables.Count ==0 || dsQYinfo.Tables[0].Rows.Count==0)
            {
                return;
            }

            txtCardID.Text = dsQYinfo.Tables[0].Rows[0][AtomEHR.Models.tb_健康档案.身份证号].ToString();
            txtDabh.Text = dsQYinfo.Tables[0].Rows[0][tb_JTYS签约信息.档案号].ToString();
            txtName.Text = DESEncrypt.DES解密(dsQYinfo.Tables[0].Rows[0][AtomEHR.Models.tb_健康档案.姓名].ToString());
            txtMagCard.Text = dsQYinfo.Tables[0].Rows[0][tb_JTYS档案VS磁卡.磁卡号].ToString();
            txtTelNo.Text = dsQYinfo.Tables[0].Rows[0][tb_JTYS签约信息.联系电话].ToString();

            this.ActiveControl = this.txtExcResult;
            this.txtExcResult.Select(this.txtExcResult.Text.Length, 0);
        }
    }
}
