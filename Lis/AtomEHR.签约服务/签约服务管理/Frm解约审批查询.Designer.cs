﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.签约服务.签约服务管理
{
    partial class Frm解约审批查询
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm解约审批查询));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtMargCard = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo服务包类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.date生效日期E = new DevExpress.XtraEditors.DateEdit();
            this.date生效日期B = new DevExpress.XtraEditors.DateEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.ck含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.tllue所属机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gc签约信息 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv签约信息 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolJYSQID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col健康档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col签约包 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col生效日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col到期日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col履约状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol申请时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol解约原因 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol申请人姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol审批状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol审批备注 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol审批人姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol审批时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMargCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo服务包类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期E.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期E.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期B.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc签约信息)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv签约信息)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.groupControl1);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.layoutControl1);
            this.tpSummary.Size = new System.Drawing.Size(1019, 448);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(1025, 454);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(1025, 454);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(1025, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(847, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(650, 2);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtMargCard);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.textEdit地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.cbo服务包类型);
            this.layoutControl1.Controls.Add(this.date生效日期E);
            this.layoutControl1.Controls.Add(this.date生效日期B);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.ck含下属机构);
            this.layoutControl1.Controls.Add(this.tllue所属机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(271, 167, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1019, 114);
            this.layoutControl1.TabIndex = 33;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtMargCard
            // 
            this.txtMargCard.Location = new System.Drawing.Point(631, 57);
            this.txtMargCard.Name = "txtMargCard";
            this.txtMargCard.Size = new System.Drawing.Size(150, 20);
            this.txtMargCard.StyleController = this.layoutControl1;
            this.txtMargCard.TabIndex = 17;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(231, 81);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Size = new System.Drawing.Size(133, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 16;
            // 
            // textEdit地址
            // 
            this.textEdit地址.Location = new System.Drawing.Point(368, 81);
            this.textEdit地址.Name = "textEdit地址";
            this.textEdit地址.Size = new System.Drawing.Size(184, 20);
            this.textEdit地址.StyleController = this.layoutControl1;
            this.textEdit地址.TabIndex = 15;
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(107, 81);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Size = new System.Drawing.Size(120, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 13;
            this.comboBoxEdit镇.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit镇_SelectedIndexChanged);
            // 
            // cbo服务包类型
            // 
            this.cbo服务包类型.Location = new System.Drawing.Point(423, 57);
            this.cbo服务包类型.Name = "cbo服务包类型";
            this.cbo服务包类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo服务包类型.Size = new System.Drawing.Size(129, 20);
            this.cbo服务包类型.StyleController = this.layoutControl1;
            this.cbo服务包类型.TabIndex = 11;
            // 
            // date生效日期E
            // 
            this.date生效日期E.EditValue = null;
            this.date生效日期E.Location = new System.Drawing.Point(246, 57);
            this.date生效日期E.Name = "date生效日期E";
            this.date生效日期E.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date生效日期E.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date生效日期E.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.date生效日期E.Size = new System.Drawing.Size(118, 20);
            this.date生效日期E.StyleController = this.layoutControl1;
            this.date生效日期E.TabIndex = 10;
            // 
            // date生效日期B
            // 
            this.date生效日期B.EditValue = null;
            this.date生效日期B.Location = new System.Drawing.Point(107, 57);
            this.date生效日期B.Name = "date生效日期B";
            this.date生效日期B.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date生效日期B.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date生效日期B.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.date生效日期B.Size = new System.Drawing.Size(120, 20);
            this.date生效日期B.StyleController = this.layoutControl1;
            this.date生效日期B.TabIndex = 9;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(631, 32);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(150, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 8;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(850, 32);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(157, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(423, 32);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(129, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 6;
            // 
            // ck含下属机构
            // 
            this.ck含下属机构.EditValue = true;
            this.ck含下属机构.Location = new System.Drawing.Point(231, 32);
            this.ck含下属机构.Name = "ck含下属机构";
            this.ck含下属机构.Properties.Caption = "含下属机构";
            this.ck含下属机构.Size = new System.Drawing.Size(133, 19);
            this.ck含下属机构.StyleController = this.layoutControl1;
            this.ck含下属机构.TabIndex = 5;
            // 
            // tllue所属机构
            // 
            this.tllue所属机构.Location = new System.Drawing.Point(107, 32);
            this.tllue所属机构.Name = "tllue所属机构";
            this.tllue所属机构.Properties.AutoExpandAllNodes = false;
            this.tllue所属机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tllue所属机构.Properties.NullText = "";
            this.tllue所属机构.Properties.PopupSizeable = false;
            this.tllue所属机构.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tllue所属机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.tllue所属机构.Size = new System.Drawing.Size(120, 20);
            this.tllue所属机构.StyleController = this.layoutControl1;
            this.tllue所属机构.TabIndex = 4;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "签约服务查询";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem11,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem13,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1019, 114);
            this.layoutControlGroup1.Text = "查询条件";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.tllue所属机构;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(219, 25);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ck含下属机构;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(219, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(90, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(137, 25);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.cbo服务包类型;
            this.layoutControlItem8.CustomizationFormText = "服务包类型：";
            this.layoutControlItem8.Location = new System.Drawing.Point(356, 25);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(188, 24);
            this.layoutControlItem8.Text = "服务包：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.comboBoxEdit镇;
            this.layoutControlItem10.CustomizationFormText = "居住地址：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(219, 25);
            this.layoutControlItem10.Text = "居住地址：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit地址;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(356, 49);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(188, 25);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.comboBoxEdit村;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(219, 49);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(137, 25);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt姓名;
            this.layoutControlItem3.CustomizationFormText = "姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(356, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(94, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(188, 25);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "姓名：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt档案号;
            this.layoutControlItem5.CustomizationFormText = "健康档案号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(544, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(229, 25);
            this.layoutControlItem5.Text = "健康档案号：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.date生效日期B;
            this.layoutControlItem6.CustomizationFormText = "生效日期：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem6.Text = "解约申请日期：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.date生效日期E;
            this.layoutControlItem7.CustomizationFormText = "~";
            this.layoutControlItem7.Location = new System.Drawing.Point(219, 25);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(137, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "~";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(10, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(773, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(226, 25);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtMargCard;
            this.layoutControlItem13.CustomizationFormText = "磁条卡号：";
            this.layoutControlItem13.Location = new System.Drawing.Point(544, 25);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem13.Text = "磁条卡号：";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(70, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(544, 49);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(455, 25);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(773, 25);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(226, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn查询);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 114);
            this.flowLayoutPanel1.MaximumSize = new System.Drawing.Size(0, 30);
            this.flowLayoutPanel1.MinimumSize = new System.Drawing.Size(0, 30);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1019, 30);
            this.flowLayoutPanel1.TabIndex = 34;
            // 
            // btn查询
            // 
            this.btn查询.Image = ((System.Drawing.Image)(resources.GetObject("btn查询.Image")));
            this.btn查询.Location = new System.Drawing.Point(103, 3);
            this.btn查询.MaximumSize = new System.Drawing.Size(70, 25);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(70, 25);
            this.btn查询.TabIndex = 0;
            this.btn查询.Text = "查询";
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gc签约信息);
            this.groupControl1.Controls.Add(this.pagerControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 144);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1019, 304);
            this.groupControl1.TabIndex = 36;
            this.groupControl1.Text = "签约信息（双击查看履约情况）";
            // 
            // gc签约信息
            // 
            this.gc签约信息.AllowBandedGridColumnSort = false;
            this.gc签约信息.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc签约信息.IsBestFitColumns = true;
            this.gc签约信息.Location = new System.Drawing.Point(2, 22);
            this.gc签约信息.MainView = this.gv签约信息;
            this.gc签约信息.Name = "gc签约信息";
            this.gc签约信息.ShowContextMenu = true;
            this.gc签约信息.Size = new System.Drawing.Size(1015, 246);
            this.gc签约信息.StrWhere = "";
            this.gc签约信息.TabIndex = 0;
            this.gc签约信息.UseCheckBox = false;
            this.gc签约信息.View = "";
            this.gc签约信息.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv签约信息});
            // 
            // gv签约信息
            // 
            this.gv签约信息.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolJYSQID,
            this.gridColumn3,
            this.gridColumn2,
            this.gridColumn1,
            this.col健康档案号,
            this.col姓名,
            this.col性别,
            this.col身份证号,
            this.gridColumn18,
            this.gridColumn17,
            this.col签约包,
            this.col生效日期,
            this.col到期日期,
            this.col履约状态,
            this.gcol申请时间,
            this.gcol解约原因,
            this.gcol申请人姓名,
            this.gcol审批状态,
            this.gcol审批备注,
            this.gcol审批人姓名,
            this.gcol审批时间});
            this.gv签约信息.GridControl = this.gc签约信息;
            this.gv签约信息.Name = "gv签约信息";
            this.gv签约信息.OptionsBehavior.Editable = false;
            this.gv签约信息.OptionsView.ColumnAutoWidth = false;
            this.gv签约信息.OptionsView.ShowGroupPanel = false;
            // 
            // gcolJYSQID
            // 
            this.gcolJYSQID.Caption = "JYSQID";
            this.gcolJYSQID.FieldName = "ID";
            this.gcolJYSQID.Name = "gcolJYSQID";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "签约ID";
            this.gridColumn3.FieldName = "QYID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "所属机构名称";
            this.gridColumn2.FieldName = "所属机构名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "磁卡号";
            this.gridColumn1.FieldName = "磁卡号";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // col健康档案号
            // 
            this.col健康档案号.AppearanceHeader.Options.UseTextOptions = true;
            this.col健康档案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col健康档案号.Caption = "健康档案号";
            this.col健康档案号.FieldName = "档案号";
            this.col健康档案号.Name = "col健康档案号";
            this.col健康档案号.OptionsColumn.ReadOnly = true;
            this.col健康档案号.Visible = true;
            this.col健康档案号.VisibleIndex = 1;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 2;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别名称";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 3;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 4;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "签约对象类型";
            this.gridColumn18.FieldName = "签约对象类型名称";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            this.gridColumn18.Width = 100;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "健康状况";
            this.gridColumn17.FieldName = "健康状况名称";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 6;
            // 
            // col签约包
            // 
            this.col签约包.AppearanceHeader.Options.UseTextOptions = true;
            this.col签约包.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col签约包.Caption = "签约包";
            this.col签约包.FieldName = "服务包名称";
            this.col签约包.Name = "col签约包";
            this.col签约包.OptionsColumn.ReadOnly = true;
            this.col签约包.Visible = true;
            this.col签约包.VisibleIndex = 7;
            // 
            // col生效日期
            // 
            this.col生效日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col生效日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col生效日期.Caption = "生效日期";
            this.col生效日期.FieldName = "生效日期";
            this.col生效日期.Name = "col生效日期";
            this.col生效日期.OptionsColumn.ReadOnly = true;
            this.col生效日期.Visible = true;
            this.col生效日期.VisibleIndex = 8;
            // 
            // col到期日期
            // 
            this.col到期日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col到期日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col到期日期.Caption = "到期日期";
            this.col到期日期.FieldName = "到期日期";
            this.col到期日期.Name = "col到期日期";
            this.col到期日期.OptionsColumn.ReadOnly = true;
            this.col到期日期.Visible = true;
            this.col到期日期.VisibleIndex = 9;
            // 
            // col履约状态
            // 
            this.col履约状态.AppearanceHeader.Options.UseTextOptions = true;
            this.col履约状态.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col履约状态.Caption = "履约状态";
            this.col履约状态.FieldName = "履约状态";
            this.col履约状态.Name = "col履约状态";
            this.col履约状态.OptionsColumn.ReadOnly = true;
            this.col履约状态.Visible = true;
            this.col履约状态.VisibleIndex = 10;
            // 
            // gcol申请时间
            // 
            this.gcol申请时间.Caption = "申请时间";
            this.gcol申请时间.FieldName = "申请时间";
            this.gcol申请时间.Name = "gcol申请时间";
            this.gcol申请时间.Visible = true;
            this.gcol申请时间.VisibleIndex = 11;
            // 
            // gcol解约原因
            // 
            this.gcol解约原因.Caption = "申请解约原因";
            this.gcol解约原因.FieldName = "解约原因";
            this.gcol解约原因.Name = "gcol解约原因";
            this.gcol解约原因.Visible = true;
            this.gcol解约原因.VisibleIndex = 12;
            this.gcol解约原因.Width = 150;
            // 
            // gcol申请人姓名
            // 
            this.gcol申请人姓名.Caption = "解约申请人";
            this.gcol申请人姓名.FieldName = "申请人姓名";
            this.gcol申请人姓名.Name = "gcol申请人姓名";
            this.gcol申请人姓名.Visible = true;
            this.gcol申请人姓名.VisibleIndex = 13;
            // 
            // gcol审批状态
            // 
            this.gcol审批状态.Caption = "审批状态";
            this.gcol审批状态.FieldName = "审批状态";
            this.gcol审批状态.Name = "gcol审批状态";
            this.gcol审批状态.Visible = true;
            this.gcol审批状态.VisibleIndex = 14;
            // 
            // gcol审批备注
            // 
            this.gcol审批备注.Caption = "审批备注";
            this.gcol审批备注.FieldName = "审批备注";
            this.gcol审批备注.Name = "gcol审批备注";
            this.gcol审批备注.Visible = true;
            this.gcol审批备注.VisibleIndex = 15;
            // 
            // gcol审批人姓名
            // 
            this.gcol审批人姓名.Caption = "审批人姓名";
            this.gcol审批人姓名.FieldName = "审批人姓名";
            this.gcol审批人姓名.Name = "gcol审批人姓名";
            this.gcol审批人姓名.Visible = true;
            this.gcol审批人姓名.VisibleIndex = 16;
            // 
            // gcol审批时间
            // 
            this.gcol审批时间.Caption = "审批时间";
            this.gcol审批时间.FieldName = "审批时间";
            this.gcol审批时间.Name = "gcol审批时间";
            this.gcol审批时间.Visible = true;
            this.gcol审批时间.VisibleIndex = 17;
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(2, 268);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 34);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 10;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(1015, 34);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 130;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // Frm解约审批查询
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 480);
            this.Name = "Frm解约审批查询";
            this.Text = "解约审批查询";
            this.Load += new System.EventHandler(this.Frm解约审批查询_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMargCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo服务包类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期E.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期E.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期B.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date生效日期B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc签约信息)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv签约信息)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraEditors.TextEdit textEdit地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraEditors.ComboBoxEdit cbo服务包类型;
        private DevExpress.XtraEditors.DateEdit date生效日期E;
        private DevExpress.XtraEditors.DateEdit date生效日期B;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.CheckEdit ck含下属机构;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn查询;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DataGridControl gc签约信息;
        private DevExpress.XtraGrid.Views.Grid.GridView gv签约信息;
        private DevExpress.XtraGrid.Columns.GridColumn col健康档案号;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col签约包;
        private DevExpress.XtraGrid.Columns.GridColumn col生效日期;
        private DevExpress.XtraGrid.Columns.GridColumn col到期日期;
        private DevExpress.XtraGrid.Columns.GridColumn col履约状态;
        private DevExpress.XtraEditors.TextEdit txtMargCard;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TreeListLookUpEdit tllue所属机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gcol解约原因;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gcolJYSQID;
        private DevExpress.XtraGrid.Columns.GridColumn gcol申请时间;
        private DevExpress.XtraGrid.Columns.GridColumn gcol申请人姓名;
        private DevExpress.XtraGrid.Columns.GridColumn gcol审批状态;
        private DevExpress.XtraGrid.Columns.GridColumn gcol审批备注;
        private DevExpress.XtraGrid.Columns.GridColumn gcol审批人姓名;
        private DevExpress.XtraGrid.Columns.GridColumn gcol审批时间;
    }
}