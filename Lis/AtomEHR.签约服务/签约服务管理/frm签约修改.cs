﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class frm签约修改 : frmBase
    {
        string m_qyid;
        public frm签约修改(string qyid)
        {
            InitializeComponent();
            m_qyid = qyid;

            InitForm();

            string nowdate = bll签约.ServiceDateTime;

            //Begin WXF 2019-01-02 | 11:33
            //@@UpdateValue 
            
            //根据旧寨需求，将日期的修改限制去掉
            //if (nowdate.StartsWith("2018") || nowdate.StartsWith("2019-01") || nowdate.StartsWith("2019-02"))
            //{ //End
                dateEdit签约.Properties.ReadOnly = false;
                dateEdit签约.Properties.Buttons[0].Enabled = true;
            //}
            //else
            //{
            //    dateEditBegin.Properties.ReadOnly = true;
            //    dateEditBegin.Properties.Buttons[0].Enabled = false;

            //    dateEdit签约.Properties.ReadOnly = true;
            //    dateEdit签约.Properties.Buttons[0].Enabled = false;
            //}
        }

        bllJTYS签约对象类型 bll签约对象 = new bllJTYS签约对象类型();
        bllJTYS健康状况 bll健康状况 = new bllJTYS健康状况();
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        bllJTYS团队概要 bll团队 = new bllJTYS团队概要();
        AtomEHR.Business.bll健康档案 bll档案 = new AtomEHR.Business.bll健康档案();
        bllJTYS团队成员 bll成员 = new bllJTYS团队成员();
        bllJTYS签约医生签字 bll医生签字 = new bllJTYS签约医生签字();

        void InitForm()
        {
            //签约对象设置数据源
            DataTable dt签约对象 = bll签约对象.GetAllValidateData();
            //util.ControlsHelper.BindComboxData(dt签约对象, cboServiceObject, tb_JTYS签约对象类型.ID, tb_JTYS签约对象类型.类型名称);
            lueServiceObject.Properties.ValueMember = tb_JTYS签约对象类型.ID;
            lueServiceObject.Properties.DisplayMember = tb_JTYS签约对象类型.类型名称;
            lueServiceObject.Properties.DataSource = dt签约对象;


            //健康状况设置数据源
            DataTable dt健康状况 = bll健康状况.GetAllValidateData();
            cboeHealthCondition.Properties.ValueMember = tb_JTYS健康状况.ID;
            cboeHealthCondition.Properties.DisplayMember = tb_JTYS健康状况.名称;
            cboeHealthCondition.Properties.DataSource = dt健康状况;

            //服务包设置数据源
            DataTable dt服务包 = bll服务包.GetAllValidateData();
            gcServicePackage.DataSource = dt服务包;

            //服务团队数据源
            DataTable dt团队 = bll团队.GetAllValidateData();
            lookUpEditServiceTeam.Properties.ValueMember = tb_JTYS团队概要.团队ID;
            lookUpEditServiceTeam.Properties.DisplayMember = tb_JTYS团队概要.团队名称;
            lookUpEditServiceTeam.Properties.DataSource = dt团队;

            lookUpEditServiceTeam.Enabled = false;

            this.dateEdit签约.DateTime = DateTime.Now;

            //判断此人是否为贫困人口
            //查询是否为贫困人口
            bool isPK = false;
            if (isPK)
            {
                emptySpaceItem贫困人口.Text = "此人可能属于贫困人口";
            }
            else
            {
                emptySpaceItem贫困人口.Text = " ";
            }
        }

        private void frm解约修改_Load(object sender, EventArgs e)
        {
            this.ActiveControl = txtMagCard;

            DataSet ds = bll签约.GetBusinessByIdFromView(m_qyid);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                txtDabh.Text = dt.Rows[0][tb_JTYS签约信息.档案号].ToString();
                txtName.Text = dt.Rows[0][AtomEHR.Models.tb_健康档案.姓名].ToString();
                txtTelNo.Text = dt.Rows[0][tb_JTYS签约信息.联系电话].ToString();
                txtAddress.Text = bll档案.GetAddressByDAH(txtDabh.Text);

                txtMagCard.Text = dt.Rows[0][tb_JTYS档案VS磁卡.磁卡号].ToString();
                txtCardID.Text = dt.Rows[0][AtomEHR.Models.tb_健康档案.身份证号].ToString();
                lueServiceObject.EditValue = Convert.ToInt32(dt.Rows[0][tb_JTYS签约信息.签约对象类型].ToString());

                string healthcondition = dt.Rows[0][tb_JTYS签约信息.健康状况].ToString();
                //cboeHealthCondition.EditValue = healthcondition.Substring(1, healthcondition.Length - 2);
                if (healthcondition.Length > 2)
                {
                    cboeHealthCondition.SetEditValue(healthcondition.Substring(1, healthcondition.Length - 2));
                }
                else
                {
                    cboeHealthCondition.SetEditValue(healthcondition);
                }

                //服务团队

                //txtServiceTeam.Text = dt.Rows[0]["团队名称"].ToString();
                dateEdit签约.Text = dt.Rows[0][tb_JTYS签约信息.签约日期].ToString();
                lookUpEditServiceTeam.EditValue = dt.Rows[0][tb_JTYS签约信息.团队ID].ToString();
                //txt签约人.Text = dt.Rows[0]["签约医生姓名"].ToString();
                txt状态.Text = dt.Rows[0]["签约状态"].ToString();
                txt创建人.Text = dt.Rows[0]["创建人姓名"].ToString();
                txt创建时间.Text = dt.Rows[0][tb_JTYS签约信息.创建时间].ToString();
                txt修改人.Text = dt.Rows[0]["修改人姓名"].ToString();
                txt修改时间.Text = dt.Rows[0][tb_JTYS签约信息.修改时间].ToString();

                dateEditBegin.DateTime = Convert.ToDateTime(dt.Rows[0][tb_JTYS签约信息.生效日期].ToString());
                dateEditEnd.DateTime = Convert.ToDateTime(dt.Rows[0][tb_JTYS签约信息.到期日期].ToString());

                string serviceIDs = dt.Rows[0][tb_JTYS签约信息.签约服务包].ToString();
                string[] arrServices = serviceIDs.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                if (arrServices.Length > 0)
                {
                    DataTable dtServicePackage = bll服务包.GetServicePackagesByIDList(arrServices);
                    gcServicePackage.DataSource = dtServicePackage;
                }

                //获取签约人信息
                if (Loginer.CurrentUser.所属机构.StartsWith("371321"))
                {
                    txt签约人.Text = dt.Rows[0]["签约医生姓名"].ToString();
                }
                else
                {
                    try
                    {
                        string QYRID = dt.Rows[0][tb_JTYS签约信息.签约人].ToString();
                        if (string.IsNullOrWhiteSpace(QYRID))
                        { }
                        else
                        {
                            DataTable dt成员 = bll成员.GetDataByKey(QYRID);
                            txt签约人.Text = dt成员.Rows[0][tb_JTYS团队成员.成员姓名].ToString();
                        }
                    }
                    catch
                    {

                    }
                }

                //新增20181122
                if (!string.IsNullOrWhiteSpace(dt.Rows[0][tb_JTYS签约信息.费用ID].ToString()))
                {
                    DataSet ds医生签字 = bll医生签字.GetBusinessBy费用ID(dt.Rows[0][tb_JTYS签约信息.费用ID].ToString(), false);
                    if (ds医生签字 != null && ds医生签字.Tables.Count > 0 && ds医生签字.Tables[0].Rows.Count > 0)
                    {
                        string yssqz = ds医生签字.Tables[0].Rows[0][tb_JTYS签约医生签字.签字].ToString();
                        if (!string.IsNullOrWhiteSpace(yssqz) && yssqz.Length > 64)
                        {
                            Byte[] bitmapData = new Byte[yssqz.Length];
                            bitmapData = Convert.FromBase64String(yssqz);
                            System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);
                            this.pictureEdit1.Image = Image.FromStream(streamBitmap);

                            isAllowNewDocFinger = false;
                        }
                        else
                        {
                            isAllowNewDocFinger = true;
                        }
                    }
                    else
                    {
                        isAllowNewDocFinger = true;
                    }
                }
                else
                {
                    isAllowNewDocFinger = true;
                }


                string SQ = dt.Rows[0][tb_JTYS签约信息.手签照].ToString();
                if (!string.IsNullOrEmpty(SQ) && SQ.Length > 64)
                {
                    Byte[] bitmapData = new Byte[SQ.Length];
                    bitmapData = Convert.FromBase64String(SQ);
                    System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);
                    this.pice指纹.Image = Image.FromStream(streamBitmap);

                    isAllowNewFinger = false;
                }
                else
                {
                    isAllowNewFinger = true;
                }
            }
        }

        bool isAllowNewFinger = false;//是否允许补录指纹

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        bllJTYS档案VS磁卡 bll档案磁卡 = new bllJTYS档案VS磁卡();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        private void btn保存_Click(object sender, EventArgs e)
        {
            //if(string.IsNullOrWhiteSpace(txtMagCard.Text))
            //{
            //    Msg.ShowInformation("请录入磁卡号。");
            //    return;
            //}

            if (lueServiceObject.EditValue == null || string.IsNullOrWhiteSpace(lueServiceObject.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择签约对象类型。");
                return;
            }

            if (cboeHealthCondition.EditValue == null || string.IsNullOrWhiteSpace(cboeHealthCondition.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择健康状况。");
                return;
            }

            if (lookUpEditServiceTeam.EditValue == null || string.IsNullOrWhiteSpace(lookUpEditServiceTeam.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择服务团队。");
                return;
            }

            //int[] selectedIndex = gvServicePackage.GetSelectedRows();
            //if(selectedIndex.Length ==0)
            //{
            //    Msg.ShowInformation("请至少选择一个服务包。");
            //    return;
            //}
            //else if(selectedIndex.Length > 1)
            //{
            //    Msg.ShowInformation("只允许选择一个服务包。");
            //    return;
            //}
            //decimal total = 0;
            //StringBuilder packagebuilder = new StringBuilder();
            //StringBuilder serviceIDBuilder = new StringBuilder(",");
            //for(int index = 0; index < selectedIndex.Length; index++)
            //{
            //    total += Convert.ToDecimal(gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.年收费标准));

            //    serviceIDBuilder.Append(gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.ServiceID) + ",");
            //    if(index < selectedIndex.Length-1)
            //    {
            //        packagebuilder.Append(gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.名称)+"，");
            //    }
            //    else
            //    {
            //        packagebuilder.Append(gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.名称));
            //    }
            //}

            ////进行服务包重复性检查
            //bool checkre = CheckUniqueServicePackage(txtDabh.Text.Trim());
            //if(!checkre)//false 有重复签约的服务包内容
            //{
            //    return;
            //}

            //DialogResult result = MessageBox.Show("您选择了如下服务包：\n\n" + packagebuilder.ToString() + "\n总共需收费："+total.ToString()+"元。\n\n点击“确定”按钮，进行签约保存；\n点击“取消”按钮，可以重新选择。", "服务包确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            //if(result == System.Windows.Forms.DialogResult.Cancel)
            //{
            //    return;
            //}

            //decimal price = Convert.ToDecimal(gvServicePackage.GetRowCellValue(selectedIndex[0], tb_JTYS服务包.年收费标准));
            //if(price == 0 && txtMagCard.Text.Length > 0)
            //{
            //    Msg.ShowInformation("基础服务包不需要发卡，已为您。");
            //}

            ////校验次档案号是否已发卡，或磁卡已发出
            //DataSet ds = bll档案磁卡.GetDataTableForCheckBy档案号磁卡号(txtDabh.Text.Trim(), txtMagCard.Text.Trim());
            //if(ds==null || ds.Tables.Count !=3)
            //{
            //    Msg.ShowInformation("对档案号、磁卡号进行检验时出现未知的异常。");
            //    return;
            //}
            //else if(ds.Tables[0].Rows.Count> 0)//已发卡，当卡号与本次填写的卡号不一致
            //{
            //    Msg.ShowInformation("档案号【" + txtDabh.Text.Trim() + "】已发卡，已发磁卡卡号为：" + ds.Tables[0].Rows[0][tb_JTYS档案VS磁卡.磁卡号]);
            //    return;
            //}
            //else if (ds.Tables[1].Rows.Count > 0)//磁卡已发出，但发卡记录中的档案号与本次填写的档案号不一致
            //{
            //    Msg.ShowInformation("卡号为【" + txtMagCard.Text.Trim() + "】的磁卡已发出，对应的健康档案号为：" + ds.Tables[1].Rows[0][tb_JTYS档案VS磁卡.档案号]);
            //    return;
            //}
            //else if (ds.Tables[2].Rows.Count > 0)//表示已发磁卡，并且磁卡与本次填写的卡号一致
            //{
            //    //不做任何处理
            //}
            //else//表示未发卡，磁卡也未使用
            //{
            //    //保存磁卡档案号对应关系
            //    bll档案磁卡.GetBusinessByKey("-1", true);
            //    bll档案磁卡.NewBusiness();
            //    bll档案磁卡.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.档案号] = txtDabh.Text.Trim();
            //    bll档案磁卡.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.磁卡号] = txtMagCard.Text.Trim();
            //    //bll档案磁卡.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.状态] = "1";
            //    bll档案磁卡.Save(bll档案磁卡.CurrentBusiness);
            //}




            //保存签约信息
            bll签约.GetBusinessByKey(m_qyid, true);


            //新加的20181122

            if (isAllowNewDocFinger && isNewDocFinger)
            {
                string strfyid = bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.费用ID].ToString();
                if (string.IsNullOrWhiteSpace(strfyid))
                {
                    strfyid = System.Guid.NewGuid().ToString().Replace("-", "");
                    bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.费用ID] = strfyid;
                }


                //指纹
                bll医生签字.GetBusinessBy费用ID("-1", true);
                if (bll医生签字.CurrentBusiness.Tables[0].Rows.Count > 0)
                {
                    using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
                    {
                        this.pictureEdit1.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] arr1 = new byte[ms1.Length];
                        ms1.Position = 0;
                        ms1.Read(arr1, 0, (int)ms1.Length);
                        ms1.Close();
                        bll医生签字.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约医生签字.签字] = Convert.ToBase64String(arr1);
                    }
                }
                else
                {
                    bll医生签字.NewBusiness();
                    using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
                    {
                        this.pictureEdit1.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] arr1 = new byte[ms1.Length];
                        ms1.Position = 0;
                        ms1.Read(arr1, 0, (int)ms1.Length);
                        ms1.Close();
                        bll医生签字.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约医生签字.签字] = Convert.ToBase64String(arr1);
                    }
                    bll医生签字.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约医生签字.费用ID] = strfyid;
                }

                bll医生签字.Save(bll医生签字.CurrentBusiness);
            }


            //bll签约.NewBusiness();
            //bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.档案号] = txtDabh.Text.Trim();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.联系电话] = txtTelNo.Text.Trim();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约对象类型] = lueServiceObject.EditValue.ToString();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.健康状况] = "," + cboeHealthCondition.EditValue.ToString() + ",";
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.健康状况名称] = cboeHealthCondition.Text;
            //bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约服务包] = serviceIDBuilder.ToString();
            //bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.服务包名称] = packagebuilder.ToString().Replace("，",",");
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.团队ID] = lookUpEditServiceTeam.EditValue.ToString();
            //bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约日期] = dateEdit签约.DateTime.ToString("yyyy-MM-dd");
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约日期] = dateEdit签约.DateTime.ToString("yyyy-MM-dd");
            //bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约医生] = Loginer.CurrentUser.用户编码;

            //由于暂不需要审批功能，故先添加这两个字段
            if (dateEdit签约.Properties.ReadOnly)
            {
                //do nothing
                //bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.生效日期] = dateEdit签约.DateTime.ToString("yyyy-MM-dd");
                //bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.到期日期] = dateEdit签约.DateTime.AddYears(1).AddDays(-1).ToString("yyyy-MM-dd");
            }
            else
            {
                bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.生效日期] = dateEditBegin.DateTime.ToString("yyyy-MM-dd");
                bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.到期日期] = dateEditEnd.DateTime.ToString("yyyy-MM-dd");
            }
            //bll档案磁卡.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.状态] = "1";
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.修改人] = Loginer.CurrentUser.用户编码;
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.修改时间] = bll签约.ServiceDateTime;

            if (isAllowNewFinger && isNewFinger)
            {
                //指纹
                using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
                {
                    this.pice指纹.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] arr1 = new byte[ms1.Length];
                    ms1.Position = 0;
                    ms1.Read(arr1, 0, (int)ms1.Length);
                    ms1.Close();
                    bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.手签照] = Convert.ToBase64String(arr1);
                }
            }

            bll签约.Save(bll签约.CurrentBusiness);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }


        /// <summary>
        /// 检查已签约服务包的唯一性,true检验没有重复，false当前签约的服务包与已签约的服务包有重复
        /// </summary>
        /// <returns></returns>
        public bool CheckUniqueServicePackage(string dah)
        {
            bool ret = false;

            DataTable dtServiceID = bll签约.GetValidateSignedServiceidByDah(dah);
            List<string> list = new List<string>();
            for (int index = 0; index < dtServiceID.Rows.Count; index++)
            {
                string[] arrSID = dtServiceID.Rows[index][tb_JTYS签约信息.签约服务包].ToString().Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int inner = 0; inner < arrSID.Length; inner++)
                {
                    list.Add(arrSID[inner].Trim());
                }
            }

            string snameTemp = "";
            int[] selectedIndex = gvServicePackage.GetSelectedRows();
            if (selectedIndex.Length > 0)
            {
                for (int sindex = 0; sindex < selectedIndex.Length; sindex++)
                {
                    string sid = gvServicePackage.GetRowCellValue(selectedIndex[sindex], tb_JTYS服务包.ServiceID).ToString();
                    if (list.Contains(sid))
                    {
                        snameTemp += gvServicePackage.GetRowCellValue(selectedIndex[sindex], tb_JTYS服务包.名称).ToString() + ", ";
                    }
                }
            }

            if (snameTemp.Length > 0)
            {
                Msg.ShowInformation("此人已签约过下列服务包，请不要重复签约：\n" + snameTemp.Substring(0, snameTemp.Length - 2));
                ret = false;
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        //FingerPrintHelper.FPForm frm = new FingerPrintHelper.FPForm();
        bool isNewFinger = false;
        private void sbtnFinger_Click(object sender, EventArgs e)
        {
            if (isAllowNewFinger)
            {
                //frm.bmp = null;
                //DialogResult result = frm.ShowDialog();
                //if (result == DialogResult.OK)
                //{
                //    isNewFinger = true;
                //    pice指纹.Image = frm.bmp;
                //}
            }
            else
            {
                Msg.ShowInformation("不允许补录指纹，如果左侧“签名或指纹”区域空白，请联系技术人员。");
            }
        }

        bool isNewDocFinger = false;
        bool isAllowNewDocFinger = false;
        private void sbtnDocFinger_Click(object sender, EventArgs e)
        {
            if (isAllowNewDocFinger)
            {
                //frm.bmp = null;
                //DialogResult result = frm.ShowDialog();
                //if (result == DialogResult.OK)
                //{
                //    isNewDocFinger = true;
                //    pictureEdit1.Image = frm.bmp;
                //}
            }
            else
            {
                Msg.ShowInformation("不允许补录指纹，如果左侧“签名或指纹”区域空白，请联系技术人员。");
            }
        }

        private void dateEdit签约_EditValueChanged(object sender, EventArgs e)
        {
            this.dateEditBegin.DateTime = this.dateEdit签约.DateTime;
            //this.dateEditEnd.DateTime = this.dateEdit签约.DateTime.AddYears(1).AddDays(-1);
        }

        private void dateEditBegin_EditValueChanged(object sender, EventArgs e)
        {
            this.dateEditEnd.DateTime = this.dateEditBegin.DateTime.AddYears(1).AddDays(-1);
        }



    }
}

