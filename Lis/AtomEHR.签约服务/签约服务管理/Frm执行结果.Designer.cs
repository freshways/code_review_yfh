﻿namespace AtomEHR.签约服务.签约服务管理
{
    partial class Frm执行结果
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.txtExcResult = new DevExpress.XtraEditors.MemoEdit();
            this.txtItem = new DevExpress.XtraEditors.TextEdit();
            this.txtSPname = new DevExpress.XtraEditors.TextEdit();
            this.txtMagCard = new DevExpress.XtraEditors.TextEdit();
            this.txtTelNo = new DevExpress.XtraEditors.TextEdit();
            this.txtCardID = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtDabh = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbtn取消 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSave = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtExcResult.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSPname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMagCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDabh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.txtExcResult);
            this.layoutControl2.Controls.Add(this.txtItem);
            this.layoutControl2.Controls.Add(this.txtSPname);
            this.layoutControl2.Controls.Add(this.txtMagCard);
            this.layoutControl2.Controls.Add(this.txtTelNo);
            this.layoutControl2.Controls.Add(this.txtCardID);
            this.layoutControl2.Controls.Add(this.txtName);
            this.layoutControl2.Controls.Add(this.txtDabh);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(606, 301, 250, 350);
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(581, 310);
            this.layoutControl2.TabIndex = 128;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // txtExcResult
            // 
            this.txtExcResult.Location = new System.Drawing.Point(75, 121);
            this.txtExcResult.Name = "txtExcResult";
            this.txtExcResult.Size = new System.Drawing.Size(494, 177);
            this.txtExcResult.StyleController = this.layoutControl2;
            this.txtExcResult.TabIndex = 14;
            this.txtExcResult.UseOptimizedRendering = true;
            // 
            // txtItem
            // 
            this.txtItem.Location = new System.Drawing.Point(312, 97);
            this.txtItem.Name = "txtItem";
            this.txtItem.Properties.ReadOnly = true;
            this.txtItem.Size = new System.Drawing.Size(257, 20);
            this.txtItem.StyleController = this.layoutControl2;
            this.txtItem.TabIndex = 13;
            // 
            // txtSPname
            // 
            this.txtSPname.Location = new System.Drawing.Point(75, 97);
            this.txtSPname.Name = "txtSPname";
            this.txtSPname.Properties.ReadOnly = true;
            this.txtSPname.Size = new System.Drawing.Size(170, 20);
            this.txtSPname.StyleController = this.layoutControl2;
            this.txtSPname.TabIndex = 12;
            // 
            // txtMagCard
            // 
            this.txtMagCard.EditValue = "";
            this.txtMagCard.Location = new System.Drawing.Point(75, 63);
            this.txtMagCard.Name = "txtMagCard";
            this.txtMagCard.Properties.ReadOnly = true;
            this.txtMagCard.Size = new System.Drawing.Size(133, 20);
            this.txtMagCard.StyleController = this.layoutControl2;
            this.txtMagCard.TabIndex = 11;
            // 
            // txtTelNo
            // 
            this.txtTelNo.EditValue = "";
            this.txtTelNo.Location = new System.Drawing.Point(462, 39);
            this.txtTelNo.Name = "txtTelNo";
            this.txtTelNo.Properties.ReadOnly = true;
            this.txtTelNo.Size = new System.Drawing.Size(107, 20);
            this.txtTelNo.StyleController = this.layoutControl2;
            this.txtTelNo.TabIndex = 4;
            // 
            // txtCardID
            // 
            this.txtCardID.Location = new System.Drawing.Point(275, 63);
            this.txtCardID.Name = "txtCardID";
            this.txtCardID.Properties.ReadOnly = true;
            this.txtCardID.Size = new System.Drawing.Size(294, 20);
            this.txtCardID.StyleController = this.layoutControl2;
            this.txtCardID.TabIndex = 2;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(275, 39);
            this.txtName.Name = "txtName";
            this.txtName.Properties.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(120, 20);
            this.txtName.StyleController = this.layoutControl2;
            this.txtName.TabIndex = 1;
            // 
            // txtDabh
            // 
            this.txtDabh.Location = new System.Drawing.Point(75, 39);
            this.txtDabh.Name = "txtDabh";
            this.txtDabh.Properties.ReadOnly = true;
            this.txtDabh.Size = new System.Drawing.Size(133, 20);
            this.txtDabh.StyleController = this.layoutControl2;
            this.txtDabh.TabIndex = 0;
            // 
            // Root
            // 
            this.Root.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.Root.AppearanceGroup.Options.UseFont = true;
            this.Root.AppearanceGroup.Options.UseTextOptions = true;
            this.Root.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem14,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem5,
            this.layoutControlItem7});
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(581, 310);
            this.Root.Text = "执行结果";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtName;
            this.layoutControlItem3.CustomizationFormText = "姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(187, 24);
            this.layoutControlItem3.Text = "姓名：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtDabh;
            this.layoutControlItem2.CustomizationFormText = "个人档案编号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.Text = "档案编号：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtCardID;
            this.layoutControlItem4.CustomizationFormText = "身份证号";
            this.layoutControlItem4.Location = new System.Drawing.Point(200, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(361, 24);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtTelNo;
            this.layoutControlItem6.CustomizationFormText = "联系电话";
            this.layoutControlItem6.Location = new System.Drawing.Point(387, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(174, 24);
            this.layoutControlItem6.Text = "联系电话：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtMagCard;
            this.layoutControlItem14.CustomizationFormText = "磁条卡号：";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem14.Text = "磁条卡号：";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtSPname;
            this.layoutControlItem1.CustomizationFormText = "服务包：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(237, 24);
            this.layoutControlItem1.Text = "服务包：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(561, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtItem;
            this.layoutControlItem5.CustomizationFormText = "项目名称：";
            this.layoutControlItem5.Location = new System.Drawing.Point(237, 58);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem5.Text = "项目名称：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtExcResult;
            this.layoutControlItem7.CustomizationFormText = "执行结果：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 82);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(561, 181);
            this.layoutControlItem7.Text = "执行结果：";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(60, 14);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.sbtn取消);
            this.panelControl1.Controls.Add(this.sbtnSave);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 310);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(581, 48);
            this.panelControl1.TabIndex = 130;
            // 
            // sbtn取消
            // 
            this.sbtn取消.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtn取消.Location = new System.Drawing.Point(481, 13);
            this.sbtn取消.Name = "sbtn取消";
            this.sbtn取消.Size = new System.Drawing.Size(75, 23);
            this.sbtn取消.TabIndex = 5;
            this.sbtn取消.Text = "取消";
            this.sbtn取消.Click += new System.EventHandler(this.sbtn取消_Click);
            // 
            // sbtnSave
            // 
            this.sbtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnSave.Location = new System.Drawing.Point(365, 13);
            this.sbtnSave.Name = "sbtnSave";
            this.sbtnSave.Size = new System.Drawing.Size(75, 23);
            this.sbtnSave.TabIndex = 5;
            this.sbtnSave.Text = "保存";
            this.sbtnSave.Click += new System.EventHandler(this.sbtnSave_Click);
            // 
            // Frm执行结果
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 358);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm执行结果";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "记录履约执行结果";
            this.Load += new System.EventHandler(this.Frm执行结果_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtExcResult.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSPname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMagCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDabh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit txtSPname;
        private DevExpress.XtraEditors.TextEdit txtMagCard;
        private DevExpress.XtraEditors.TextEdit txtTelNo;
        private DevExpress.XtraEditors.TextEdit txtCardID;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.TextEdit txtDabh;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.MemoEdit txtExcResult;
        private DevExpress.XtraEditors.TextEdit txtItem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton sbtn取消;
        private DevExpress.XtraEditors.SimpleButton sbtnSave;
    }
}