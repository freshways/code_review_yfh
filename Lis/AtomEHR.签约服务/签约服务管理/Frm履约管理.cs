﻿using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableXtraReport;
using AtomEHR.签约服务.签约服务管理.Report;
using DevExpress.XtraReports.UI;
using WEISHENG.COMM.PluginsAttribute;
using AtomEHR.Bridge;
using AtomEHR.Interfaces;
using AtomEHR.Business.Security;
using AtomEHR.Core;

namespace AtomEHR.签约服务.签约服务管理
{
    [ClassInfoMarkAttribute(GUID = "22F18E9C-D69F-4FEB-9BA6-E18376941950", 父ID = "eb62e99a-c72e-4183-abef-0025f4e90b5e", 键ID = "22F18E9C-D69F-4FEB-9BA6-E18376941950", 菜单类型 = "WinForm", 功能名称 = "履约管理", 程序集名称 = "AtomEHR.签约服务", 程序集调用类地址 = "签约服务管理.Frm履约管理", 全屏打开 = true)]
    public partial class Frm履约管理 : AtomEHR.Library.frmBaseBusinessForm
    {

        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        bllJTYS全局参数 bl全局 = new bllJTYS全局参数();
        bllJTYS履约执行明细 bll执行明细 = new bllJTYS履约执行明细();
        bllJTYS签约费用明细 bll费用明细 = new bllJTYS签约费用明细();
        public Frm履约管理()
        {
            InitializeComponent();
        }

        private ILoginAuthorization _CurrentAuthorization = null;//当前授权模式
        private void Frm履约管理_Load(object sender, EventArgs e)
        {
            //size = layoutControl1.Height;
            this.pagerControl1.Height = 35;
            //base.InitButtonsBase();
            //comboBoxEdit是否合格.EditValue = null;
            //为性别绑定信息
            //DataBinder.BindingLookupEditDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_DESC", "P_CODE");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, comboBoxEdit性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, com档案类型, "P_CODE", "P_DESC");
            //comboBoxEdit性别.EditValue = null;
            //frmGridCustomize.RegisterGrid(gv签约服务);

            if (string.IsNullOrEmpty(Loginer.CurrentUser.Account))
            {
                try
                {
                    SystemConfig.ReadSettings(); //读取用户自定义设置

                    if (!BridgeFactory.InitializeBridge())
                    {
                        Msg.Warning("初始化失败!");
                    }

                    //第二步： 获得此身份证号对应的用户名密码，初始化登陆参数。
                    string userID = "rxy";
                    string password = CEncoder.Encode("888888");
                    string dataSetID = "YSDB"; //txtDataset.EditValue.ToString();//帐套编号
                    string dataSetDB = "AtomEHR.YSDB";

                    LoginUser loginUser = new LoginUser(userID, password, dataSetID, dataSetDB, "HIS1.0", "");
                    _CurrentAuthorization = new LoginSystemAuth();
                    //第三步： 调用登陆验证方法。
                    if (_CurrentAuthorization.Login(loginUser)) //调用登录策略
                    {
                        SystemAuthentication.Current = _CurrentAuthorization; //授权成功, 保存当前授权模式
                        
                    }
                }
                catch (Exception ex)
                {
                    Msg.Warning("登录失败!失败原因:\n" + ex.Message);
                }
            }
            //为“镇”绑定信息
            //DataView dv镇 = new DataView(AtomEHR.Business.DataDictCache.Cache.t地区信息);
            //dv镇.RowFilter = "上级编码='" + Loginer.CurrentUser.单位代码 + "' ";
            ////DataBinder.BindingLookupEditDataSource(lookUpEdit镇, dv市.ToTable(), "地区名称", "地区编码");
            //util.ControlsHelper.BindComboxData(dv镇.ToTable(), comboBoxEdit镇, "地区编码", "地区名称");

            //为“所属机构"绑定信息
            try
            {
                AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
                DataTable dt所属机构 = bll机构.Get机构树();
                tllue所属机构.Properties.ValueMember = "机构编号";
                tllue所属机构.Properties.DisplayMember = "机构名称";

                tllue所属机构.Properties.TreeList.KeyFieldName = "机构编号";
                tllue所属机构.Properties.TreeList.ParentFieldName = "上级机构";
                tllue所属机构.Properties.TreeList.RootValue = Loginer.CurrentUser.所属机构;

                tllue所属机构.Properties.DataSource = dt所属机构;
                //tllue所属机构.Properties.TreeList.ExpandAll();
                //tllue所属机构.Properties.TreeList.CollapseAll();

                tllue所属机构.EditValue = Loginer.CurrentUser.所属机构;
            }
            catch
            {
                tllue所属机构.Text = Loginer.CurrentUser.所属机构;
            }

            if (Loginer.CurrentUser.所属机构.Length >= 12)
            {
                tllue所属机构.Properties.AutoExpandAllNodes = true;
            }
            else
            {
                tllue所属机构.Properties.AutoExpandAllNodes = false;
            }

            DataTable dt服务包 = bll服务包.GetAllValidateDataWithCodeAndName();
            util.ControlsHelper.BindComboxData(dt服务包, cbo服务包类型, tb_JTYS服务包.ServiceID, tb_JTYS服务包.名称);
        }

        private void comboBoxEdit镇_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv村 = new DataView(AtomEHR.Business.DataDictCache.Cache.t地区信息);
                dv村.RowFilter = "上级编码='" + util.ControlsHelper.GetComboxKey(comboBoxEdit镇) + "'";
                //DataBinder.BindingLookupEditDataSource(lookUpEdit村, dv县.ToTable(), "地区名称", "地区编码");
                util.ControlsHelper.BindComboxData(dv村.ToTable(), comboBoxEdit村, "地区编码", "地区名称");
                util.ControlsHelper.SetComboxNullData("", comboBoxEdit村, "请选择居/村委会");
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        string strWhere = "";
        private void btn查询_Click(object sender, EventArgs e)
        {
            try
            {
                strWhere = " and 状态='1' ";

                string str所属机构 = tllue所属机构.EditValue.ToString();
                this.pagerControl1.InitControl();
                this.pagerControl1.PageSize = 10;

                if (chkMe.Checked)
                {
                    strWhere += " and 签约医生='" + Loginer.CurrentUser.用户编码 + "'";
                }
                else
                {
                    if (ck含下属机构.Checked)
                    {
                        if (str所属机构.Length == 12)
                        {
                            strWhere += " AND [档案机构] IN (select 机构编号 from tb_机构信息 where 上级机构='" + str所属机构 + "' or 机构编号='" + str所属机构 + "' ) ";
                        }
                        else
                        {
                            strWhere += " and 档案机构 like '" + str所属机构 + "%'";
                        }
                    }
                    else
                    {
                        strWhere += " and 档案机构='" + str所属机构 + "'";
                    }

                    if (string.IsNullOrWhiteSpace(txt签约医生.Text))
                    { }
                    else if (txt签约医生.Text.Contains("'") || txt签约医生.Text.Contains("--") || txt签约医生.Text.Contains(";"))
                    { }
                    else
                    {
                        strWhere += " and 签约医生姓名='" + txt签约医生.Text.Trim() + "'";
                    }
                }

                if (string.IsNullOrWhiteSpace(txt姓名.Text))
                { }
                else
                {
                    strWhere += " and (姓名 like'" + DESEncrypt.DES加密(txt姓名.Text) + "%' or 姓名 like '" + this.txt姓名.Text.Trim() + "%')";
                }


                //if (!(string.IsNullOrWhiteSpace(date生效日期B.Text) || string.IsNullOrWhiteSpace(date生效日期E.Text)))
                //{
                //    strWhere += " and 生效日期 >= '" + date生效日期B.Text + "' AND 生效日期 <=  '" + date生效日期B.Text + "' ";
                //}
                //else 
                if (!(string.IsNullOrWhiteSpace(date生效日期B.Text)))
                {
                    strWhere += " and 生效日期 >= '" + date生效日期B.Text + "'";
                }
                else if (!(string.IsNullOrWhiteSpace(date生效日期E.Text)))
                {
                    strWhere += " and 生效日期 <= '" + date生效日期E.Text + "'";
                }

                string str服务包 = util.ControlsHelper.GetComboxKey(cbo服务包类型);
                if (string.IsNullOrWhiteSpace(str服务包))
                { }
                else
                {
                    strWhere += " and 签约服务包 like '%" + str服务包 + "%' ";
                }

                if (string.IsNullOrWhiteSpace(txt档案号.Text))
                { }
                else
                {
                    strWhere += " and 档案号 = '" + txt档案号.Text + "'";
                }

                if (string.IsNullOrWhiteSpace(txt身份证号.Text))
                { }
                else
                {
                    strWhere += " and 身份证号 = '" + txt身份证号.Text + "'";
                }

                if (string.IsNullOrWhiteSpace(txtMargCard.Text))
                { }
                else
                {
                    strWhere += " and 磁卡号 = '" + txtMargCard.Text + "'";
                }

                string str镇 = util.ControlsHelper.GetComboxKey(comboBoxEdit镇);
                if (string.IsNullOrWhiteSpace(str镇))
                { }
                else
                {
                    strWhere += " and 街道='" + str镇 + "' ";
                }

                string str村 = util.ControlsHelper.GetComboxKey(comboBoxEdit村);
                if (string.IsNullOrWhiteSpace(str村))
                { }
                else
                {
                    strWhere += " and 居委会='" + str村 + "' ";
                }

                if (string.IsNullOrWhiteSpace(textEdit地址.Text))
                { }
                else
                {
                    strWhere += " and 居住地址 like '%'+'" + textEdit地址.Text + "'+'%' ";
                }

                BindDataList();
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void BindDataList()
        {
            //this.pagerControl1.InitControl();
            if (string.IsNullOrWhiteSpace(strWhere))
            {
                return;
            }
            DataSet ds = this.pagerControl1.GetQueryResult("vw_JTYS签约信息", strWhere);
            this.gc签约信息.View = "vw_JTYS签约信息";
            this.gc签约信息.StrWhere = strWhere;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ds.Tables[0].Rows[i]["姓名"] = ds.Tables[0].Rows[i]["姓名"].ToString();
                ds.Tables[0].Rows[i]["性别"] = _bll地区.ReturnDis字典显示("xb_xingbie", ds.Tables[0].Rows[i]["性别"].ToString());
            }
            this.gc签约信息.DataSource = ds.Tables[0];
            this.pagerControl1.DrawControl();
            this.gv签约信息.BestFitColumns();//列自适应宽度
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            BindDataList();
        }

        private void gv签约服务_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.Utils.DXMouseEventArgs args = e as DevExpress.Utils.DXMouseEventArgs;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hInfo = gv签约信息.CalcHitInfo(new Point(args.X, args.Y));
            //if (args.Button == MouseButtons.Left && args.Clicks == 2)
            //{
            //判断光标是否在行范围内 
            if (hInfo.InRow)
            {


                //string baseServicePackageID = "";

                ////判断是否自动加入初级包的服务内容
                //DataSet dsParam = bl全局.GetBusinessByCode("autoAddBasePackage", "basePackageID");
                //if(dsParam !=null && dsParam.Tables.Count >=2)
                //{
                //    if (dsParam.Tables[0].Rows.Count > 0 && dsParam.Tables[1].Rows.Count > 0 && dsParam.Tables[0].Rows[0][tb_JTYS全局参数.parValue].ToString() == "1")
                //    {
                //        baseServicePackageID = dsParam.Tables[1].Rows[0][tb_JTYS全局参数.parValue].ToString();
                //    }
                //}

                //if(!string.IsNullOrWhiteSpace(baseServicePackageID))
                //{ }
                //
                //同时还需要判断是否签约过初级包

                //获取服务包的服务项目列表,根据服务包ID获取具体的项目明细、及执行次数

                if (gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, grid签约来源).ToString() == "自助签约")
                {
                    string 费用ID = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, grid费用ID).ToString();
                    bool chk费用 = bll费用明细.GetYesOrNo(费用ID);
                    if (!chk费用)
                    {
                        Msg.ShowInformation("选中的签约记录为自助签约，需先进行费用确认，然后才可打印协议书或进行履约。");
                        return;
                    }
                }

                show签约项目And执行明细();
            }
            //}
        }

        private void show签约项目And执行明细()
        {
            string qyid = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, tb_JTYS签约信息.ID).ToString();
            string serviceids = bll签约.GetServiceIdByQYID(qyid);
            string[] arrServiceID = serviceids.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            if (arrServiceID.Length == 0)
            {
                Msg.ShowInformation("此记录不存在有效的服务包，请确认此条签约记录是否正确。");
                return;
            }

            DataSet ds = bll执行明细.Get服务包执行情况(qyid, arrServiceID);
            gc签约项目.DataSource = ds.Tables[0];
            //gv签约项目情况.BestFitColumns();

            //查询执行明细
            gc项目执行明细.DataSource = bll执行明细.Get执行明细ByQYID(qyid).Tables[0];
        }
        

        private void gc签约项目情况_DoubleClick(object sender, EventArgs e)
        {
            //签约项目列表中没有任何行时，不执行履约操作
            if (gv签约项目.DataRowCount == 0)
            {
                return;
            }

            //获取签约信息列表中选中行的签约id，判断签约ID与签约项目列表中的签约ID是否一致。
            //即判断 “签约信息”列表里选中签约信息，与“签约项目”列表中的项目，它们的服务对象是否为同一个人。
            if (gv签约信息.FocusedRowHandle < 0)
            {
                Msg.ShowInformation("未选择任何的签约信息。");
                return;
            }


            string qyid = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, tb_JTYS签约信息.ID).ToString();
            //Right Top 
            string rtQyid = gv签约项目.GetRowCellValue(0, "签约ID").ToString();
            if (qyid != rtQyid)
            {
                Msg.ShowInformation("当前选中的签约信息与签约项目列表的内容，不属于同一个人，请确认。");
                return;
            }


            if (gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, grid签约来源).ToString() == "自助签约")
            {
                string 费用ID = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, grid费用ID).ToString();
                bool chk费用 = bll费用明细.GetYesOrNo(费用ID);
                if (!chk费用)
                {
                    Msg.ShowInformation("选中的签约记录为自助签约，需先进行费用确认，然后才可打印协议书或进行履约。");
                    return;
                }
            }
            string qyys = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, "签约医生姓名").ToString();
            frm签约服务项目执行 frm = new frm签约服务项目执行(qyid, qyys);
            DialogResult ret = frm.ShowDialog();
            if (ret == DialogResult.OK)
            {
                show签约项目And执行明细();
            }
            else
            {
                // do nothing
            }
        }

        private void gv签约项目_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            //if (gv签约项目.GetRow(e.RowHandle) != null)
            //{
            //    //获取所在行指定列的值
            //    string totalcount = gv签约项目.GetRowCellValue(e.RowHandle, "数量").ToString();
            //    string nowcount = gv签约项目.GetRowCellValue(e.RowHandle, "执行总量").ToString();
            //    //设置此行的背景颜色
            //    if (Convert.ToDecimal(totalcount) == Convert.ToDecimal(nowcount))
            //    {
            //        //e.Appearance.ForeColor = Color.Gray;
            //        //e.Appearance.BackColor = Color.Gray;
            //    }
            //}
        }

        private void chkMe_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMe.Checked)
            {
                txt签约医生.Text = "";
            }
        }

        private void tsmiPrintBarcode_Click(object sender, EventArgs e)
        {
            if (gv项目执行明细.DataRowCount == 0)
            {
                return;
            }

            //获取签约信息列表中选中行的签约id，判断签约ID与签约项目列表中的签约ID是否一致。
            //即判断 “签约信息”列表里选中签约信息，与“签约项目”列表中的项目，它们的服务对象是否为同一个人。
            if (gv签约信息.FocusedRowHandle < 0)
            {
                Msg.ShowInformation("未选择任何的签约信息。");
                return;
            }

            string qyid = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, tb_JTYS签约信息.ID).ToString();
            //Right bottom
            string rbQyid = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "签约ID").ToString();
            if (qyid != rbQyid)
            {
                Msg.ShowInformation("当前选中的签约信息与项目履约执行明细列表中的内容，不属于同一条签约记录，请确认。");
                return;
            }

            string strbarcode = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "执行条码").ToString();

            if (string.IsNullOrWhiteSpace(strbarcode))
            {
                Msg.ShowInformation("没有条码号可供打印。");
                return;
            }


            PrintBarcode(qyid, strbarcode);
        }

        private void PrintBarcode(string qyid, string strbarcode)
        {
            //获取条码信息，获取个人基本信息
            DataSet dsBaseInfo = bll签约.GetBaseInfoByQYID(qyid);
            string name = DESEncrypt.DES解密(dsBaseInfo.Tables[0].Rows[0]["姓名"].ToString());
            string sex = CardIDHelper.GetSex(dsBaseInfo.Tables[0].Rows[0]["身份证号"].ToString());
            if (sex == "1")
            {
                sex = "男";
            }
            else if (sex == "2")
            {
                sex = "女";
            }

            string age = CardIDHelper.GetAge(dsBaseInfo.Tables[0].Rows[0]["身份证号"].ToString(), Convert.ToDateTime(bll签约.ServiceDateTime));

            DataSet dsItems = bll执行明细.Get检验检查项目WithBarcode(qyid, strbarcode);

            if (dsItems == null || dsItems.Tables.Count == 0)
            {
                Msg.ShowInformation("未查询到条码明细信息");
                return;
            }
            string items = "";
            for (int index = 0; index < dsItems.Tables[0].Rows.Count; index++)
            {
                string temp = dsItems.Tables[0].Rows[index]["项目名称"].ToString();
                if (dsItems.Tables[0].Rows.Count > 0 && temp.Contains("（"))
                {
                    items += temp.Substring(0, temp.IndexOf("（"));
                }
                else
                {
                    items += temp;
                }

                if (index < dsItems.Tables[0].Rows.Count - 1)
                {
                    items += ",";
                }
            }

            TableXReport rep = GetSubReport(name + "/" + sex + "/" + age + " 家医履约", items, strbarcode);
            DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(rep);
            tool.ShowPreviewDialog();
        }


        private TableXReport GetSubReport(string baseinfo, string itemnames, string strbarcode)
        {
            TableXReport xtr = new TableXReport();

            Font ft = new System.Drawing.Font("宋体", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
            xtr.SetReportTitle(baseinfo, true, ft, TextAlignment.TopLeft, Size.Empty);

            if (itemnames.Length <= 19)
            {
                xtr.SetReportTitle(itemnames, true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                xtr.SetReportTitle(itemnames.Substring(0, 19), true, ft7, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(itemnames.Substring(19), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            //xtr.SetReportTitle("密度脂蛋白、低密度脂蛋白）,肾功能（尿素氮、肌肝）", true, ft7, TextAlignment.TopLeft, Size.Empty);
            //指定条码格式
            DevExpress.XtraPrinting.BarCode.Code128Generator barcode = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            barcode.CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetC;

            xtr.SetReportBarCode(strbarcode, new Point(5, 140), new Size(450, 125), barcode, true);
            xtr.TextAlignment = TextAlignment.BottomCenter;
            //SetReportMain只能最后设置一次
            //xtr.SetReportMain(100, 12, 2, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 100, 10, 100), System.Drawing.Printing.PaperKind.A4, Size.Empty, 0, 0, "");           
            xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 6, 6), System.Drawing.Printing.PaperKind.Custom, new Size(500, 300), 0, 0, "");

            //预览条形码页面(该页面有打印功能)
            //DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(xtr);
            //tool.ShowPreviewDialog();

            xtr.CreateDocument();//如果不加这一行，report可能不能显示

            return xtr;
        }

        private void 输入履约执行结果ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gv项目执行明细.DataRowCount == 0)
            {
                Msg.ShowInformation("未选择任何的项目执行明细信息。");
                return;
            }

            //获取签约信息列表中选中行的签约id，判断签约ID与签约项目列表中的签约ID是否一致。
            //即判断 “签约信息”列表里选中签约信息，与“签约项目”列表中的项目，它们的服务对象是否为同一个人。
            if (gv签约信息.FocusedRowHandle < 0)
            {
                Msg.ShowInformation("未选择任何的签约信息。");
                return;
            }

            string qyid = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, tb_JTYS签约信息.ID).ToString();
            //Right bottom
            string rbQyid = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "签约ID").ToString();
            if (qyid != rbQyid)
            {
                Msg.ShowInformation("当前选中的签约信息与项目履约执行明细列表中的内容，不属于同一条签约记录，请确认。");
                return;
            }

            //string strbarcode = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "执行条码").ToString();

            //if (!string.IsNullOrWhiteSpace(strbarcode))
            //{
            //    Msg.ShowInformation("该项目是。");
            //    return;
            //}

            string strID = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "ID").ToString();
            Frm执行结果 frm = new Frm执行结果(strID);
            frm.ShowDialog();
        }

        private void 查询履约结果ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gv项目执行明细.DataRowCount == 0)
            {
                Msg.ShowInformation("未选择任何的项目执行明细信息。");
                return;
            }

            //获取签约信息列表中选中行的签约id，判断签约ID与签约项目列表中的签约ID是否一致。
            //即判断 “签约信息”列表里选中签约信息，与“签约项目”列表中的项目，它们的服务对象是否为同一个人。
            if (gv签约信息.FocusedRowHandle < 0)
            {
                Msg.ShowInformation("未选择任何的签约信息。");
                return;
            }

            string qyid = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, tb_JTYS签约信息.ID).ToString();
            //Right bottom
            string rbQyid = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "签约ID").ToString();
            if (qyid != rbQyid)
            {
                Msg.ShowInformation("当前选中的签约信息与项目履约执行明细列表中的内容，不属于同一条签约记录，请确认。");
                return;
            }

            //string strbarcode = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "执行条码").ToString();

            //if (!string.IsNullOrWhiteSpace(strbarcode))
            //{
            //    Msg.ShowInformation("该项目是。");
            //    return;
            //}

            string strQYID = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "签约ID").ToString();

            Frm执行结果汇总 frm = new Frm执行结果汇总(strQYID);
            frm.ShowDialog();
        }

        private void 查看执行情况ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gv签约信息.DataRowCount == 0)
            {
                Msg.ShowInformation("未选择任何的签约信息。");
                return;
            }

            //获取签约信息列表中选中行的签约id，判断签约ID与签约项目列表中的签约ID是否一致。
            //即判断 “签约信息”列表里选中签约信息，与“签约项目”列表中的项目，它们的服务对象是否为同一个人。
            if (gv签约信息.FocusedRowHandle < 0)
            {
                Msg.ShowInformation("未选择任何的签约信息。");
                return;
            }

            string strQYID = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, tb_JTYS签约信息.ID).ToString();

            Frm执行结果汇总 frm = new Frm执行结果汇总(strQYID);
            frm.ShowDialog();
        }

        private void BtN导出_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XLS file(*.xls)|*.xls";//"PDF file(*.pdf)|*.pdf";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    //string qyid = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, tb_JTYS签约信息.ID).ToString();

                    //string serviceids = bll签约.GetServiceIdByQYID(qyid);
                    //string[] arrServiceID = serviceids.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                    //if (arrServiceID.Length == 0)
                    //{
                    //    Msg.ShowInformation("此记录不存在有效的服务包，请确认此条签约记录是否正确。");
                    //    return;
                    //}

                    //DataSet ds = bll执行明细.Get服务包执行情况(qyid, arrServiceID);
                    //gc签约项目.DataSource = ds.Tables[0];
                    //Business.bll机构信息 bll = new Business.bll机构信息();
                    //DataTable newtable = new AtomEHR.Business.BLL_Base.bllBase().Get全部数据(gc个人健康档案.View, "*", _strWhere, "ID", "DESC");
                    //for (int i = 0; i < newtable.Rows.Count; i++)
                    //{
                    //    newtable.Rows[i]["姓名"] = Common.DESEncrypt.DES解密(newtable.Rows[i]["姓名"].ToString());
                    //    newtable.Rows[i]["居住地址"] = bll.Return地区名称(newtable.Rows[i]["区"].ToString()) + bll.Return地区名称(newtable.Rows[i]["街道"].ToString()) + bll.Return地区名称(newtable.Rows[i]["居委会"].ToString()) + newtable.Rows[i]["居住地址"].ToString();
                    //    newtable.Rows[i]["所属机构"] = bll.Return机构名称(newtable.Rows[i]["所属机构"].ToString());
                    //    newtable.Rows[i]["创建人"] = bll.Return用户名称(newtable.Rows[i]["创建人"].ToString());
                    //}
                    //gv签约项目情况.BestFitColumns();

                    //查询执行明细
                    //gc项目执行明细.DataSource = bll执行明细.Get执行明细ByQYID(qyid).Tables[0];

                    DataTable dt = gc签约信息.DataSource as DataTable;
                    //DataTable dt = gc签约项目.DataSource as DataTable;


                    gc签约信息.ExportToXls(dlg.FileName);
                    gc签约信息.DataSource = dt;
                }
                catch (Exception ex)
                {
                    Msg.ShowException(ex);
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gv签约信息.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请未选择任何行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能选择一行");
                return;
            }

            string qyid = gv签约信息.GetRowCellValue(selectdIndex[0], tb_JTYS签约信息.ID).ToString();
            int count = bll费用明细.Create费用明细(qyid);
            Msg.ShowInformation("选中签约记录的费用信息确认成功");
        }

        private void btn履约记录单_Click(object sender, EventArgs e)
        {
            if (gv项目执行明细.DataRowCount == 0)
            {
                Msg.ShowInformation("未选择任何的项目执行明细信息。");
                return;
            }

            //获取签约信息列表中选中行的签约id，判断签约ID与签约项目列表中的签约ID是否一致。
            //即判断 “签约信息”列表里选中签约信息，与“签约项目”列表中的项目，它们的服务对象是否为同一个人。
            if (gv签约信息.FocusedRowHandle < 0)
            {
                Msg.ShowInformation("未选择任何的签约信息。");
                return;
            }

            string qyid = gv签约信息.GetRowCellValue(gv签约信息.FocusedRowHandle, tb_JTYS签约信息.ID).ToString();
            //Right bottom
            string rbQyid = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "签约ID").ToString();
            if (qyid != rbQyid)
            {
                Msg.ShowInformation("当前选中的签约信息与项目履约执行明细列表中的内容，不属于同一条签约记录，请确认。");
                return;
            }


            string strQYID = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "签约ID").ToString();
            string strDate = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, "服务时间").ToString();
            bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
            DataTable dtInfo = bll签约.GetInfoForPrint(qyid);
            string ssxzjg = dtInfo.Rows[0][tb_JTYS签约信息.录入人机构].ToString();
            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            string temp = bll机构.Get所在乡镇编码(ssxzjg);
            bllJTYSPage2 bllpage2 = new bllJTYSPage2();
            DataTable dttile = bllpage2.GetBusinessByRGID(temp, false).Tables[0];
            if (dttile != null && dttile.Rows.Count > 0)
            {
                string strTile = dttile.Rows[0]["hosp"].ToString();

                XtraReport履约记录单 xtr = new XtraReport履约记录单(strQYID, strDate, strTile);
                ReportPrintTool rpt = new ReportPrintTool(xtr);
                rpt.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("未设置标题！");
            }
        }
    }
}

