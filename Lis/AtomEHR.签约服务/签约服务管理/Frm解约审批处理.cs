﻿using AtomEHR.Common;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class Frm解约审批处理 : frmBase
    {
        public Frm解约审批处理()
        {
            InitializeComponent();
        }

        private void sbtn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        public string m_str审批意见;
        public string m_str审批备注;
        private void sbtn确定_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.cbo审批意见.Text) || (this.cbo审批意见.Text == "不允许解约" && string.IsNullOrWhiteSpace(this.med审批备注.Text)))
            {
                Msg.ShowInformation("请检查是否填写了审批意见。若不允许解约，请填写审批备注。");
                return;
            }

            m_str审批意见 = this.cbo审批意见.Text;
            int length = this.med审批备注.Text.Length > 200 ? 200 : this.med审批备注.Text.Length;
            m_str审批备注 = this.med审批备注.Text.Substring(0, length);
            
            this.DialogResult = DialogResult.OK;
        }
    }
}
