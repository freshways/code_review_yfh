﻿using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TableXtraReport;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class frm签约服务项目执行 : frmBase
    {
        string m_qyid;
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        bllJTYS全局参数 bl全局 = new bllJTYS全局参数();
        bllJTYS履约执行明细 bll执行明细 = new bllJTYS履约执行明细();
        AtomEHR.Business.bll健康档案 bll档案 = new AtomEHR.Business.bll健康档案();
        public frm签约服务项目执行(string qyid,string qyys)
        {
            InitializeComponent();
            m_qyid = qyid;
            this.txt签约医生.Text = qyys;
        }

        private void frm签约服务项目执行_Load(object sender, EventArgs e)
        {
            AtomEHR.Library.UserControls.DataGridControl.SetRowNumberIndicator(this.gv签约服务项目明细);

            this.txt开单科室.Text = HIS.COMM.zdInfo.ModelUserInfo.科室名称;
            this.txt服务医生.Text = HIS.COMM.zdInfo.ModelUserInfo.用户名;
            //加载签约信息
            DataSet dsQY = bll签约.GetBusinessByIdFromView(m_qyid);
            DataTable dtQY = dsQY.Tables[0];
            txtDabh.Text = dtQY.Rows[0][tb_JTYS签约信息.档案号].ToString();
            txtName.Text = dtQY.Rows[0][AtomEHR.Models.tb_健康档案.姓名].ToString();
            txtSFZH.Text = dtQY.Rows[0][AtomEHR.Models.tb_健康档案.身份证号].ToString();
            txtTelNo.Text = dtQY.Rows[0][tb_JTYS签约信息.联系电话].ToString();
            
            txtAddress.Text = bll档案.GetAddressByDAH(txtDabh.Text);

            txtPackageNames.Text = dtQY.Rows[0][tb_JTYS签约信息.服务包名称].ToString();
            this.txt签约日期.Text = dtQY.Rows[0][tb_JTYS签约信息.签约日期].ToString();

            //加载服务包项目明细
            string serviceids = bll签约.GetServiceIdByQYID(m_qyid);
            string[] arrServiceID = serviceids.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            if (arrServiceID.Length == 0)
            {
                Msg.ShowInformation("此记录不存在有效的服务包，请确认此条签约记录是否正确。");
                //return;
            }
            else
            {
                DataSet ds项目 = bll执行明细.Get服务包执行情况(m_qyid, arrServiceID);
                ds项目.Tables[0].Columns.Add("执行数量");
                gc签约服务项目明细.DataSource = ds项目.Tables[0];
            }

            de执行日期.DateTime = Convert.ToDateTime(bll签约.ServiceDateTime);

            if(dtQY.Rows[0][tb_JTYS签约信息.履约状态].ToString()=="履约完毕")
            {
                Set履约完毕显示();
            }
        }

        private void gv签约服务项目明细_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if(e.Action == CollectionChangeAction.Remove)
            {
                gv签约服务项目明细.SetRowCellValue(e.ControllerRow, gcol执行数量, "");
            }
            else if(e.Action == CollectionChangeAction.Add)
            {
                decimal totalcount = Convert.ToDecimal(gv签约服务项目明细.GetRowCellValue(e.ControllerRow, gcol签约次数));
                decimal usedcount = Convert.ToDecimal(gv签约服务项目明细.GetRowCellValue(e.ControllerRow, gcol已服务次数));
                if(totalcount <= usedcount)
                {
                    gv签约服务项目明细.UnselectRow(e.ControllerRow);
                }
                else
                {
                    gv签约服务项目明细.SetRowCellValue(e.ControllerRow, gcol执行数量, "1");
                }
            }
            else if (e.Action == CollectionChangeAction.Refresh)
            {
                int rowcount = gv签约服务项目明细.DataRowCount;
                int[] selectedIndexs = gv签约服务项目明细.GetSelectedRows();

                for (int inner = 0; inner < rowcount; inner++)
                {
                    if (selectedIndexs.Length > 0 && selectedIndexs.Contains(inner))
                    {
                        decimal totalcount = Convert.ToDecimal(gv签约服务项目明细.GetRowCellValue(inner, gcol签约次数));
                        decimal usedcount = Convert.ToDecimal(gv签约服务项目明细.GetRowCellValue(inner, gcol已服务次数));
                        if (totalcount <= usedcount)
                        {
                            gv签约服务项目明细.UnselectRow(inner);
                        }
                        else
                        {
                            gv签约服务项目明细.SetRowCellValue(inner, gcol执行数量, "1");
                        }
                    }
                    else
                    {
                        gv签约服务项目明细.SetRowCellValue(inner, gcol执行数量, "");
                    }
                }
            }
        }

        private void gv签约服务项目明细_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (gv签约服务项目明细.GetRow(e.RowHandle) != null)
            {
                decimal totalcount = Convert.ToDecimal(gv签约服务项目明细.GetRowCellValue(e.RowHandle, gcol签约次数));
                decimal usedcount = Convert.ToDecimal(gv签约服务项目明细.GetRowCellValue(e.RowHandle, gcol已服务次数));
                if (totalcount <= usedcount)
                {
                    //e.Appearance.BackColor = Color.Yellow;
                    e.Appearance.ForeColor = Color.LightGray;
                }

            }
        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Set履约完毕显示()
        {
            sbtnDone.Enabled = false;
            btn保存.Enabled = false;
            labcNotice.Visible = true;
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            int[] selectRowIndexs = gv签约服务项目明细.GetSelectedRows();
            if (selectRowIndexs.Length == 0)
            {
                Msg.ShowInformation("未选择任何行");
                return;
            }
            if (string.IsNullOrEmpty(txt开单科室.Text))
            {
                Msg.ShowInformation("未选择【开单科室】");
                return;
            }
            //if (string.IsNullOrEmpty(txt执行科室.Text))
            //{
            //    Msg.ShowInformation("未选择【执行科室】");
            //    return;
            //}

            //验证是否履约完毕状态
            string lyzt = bll签约.Get履约状态(m_qyid);
            if(lyzt=="履约完毕")
            {
                Msg.ShowInformation("已是履约完毕状态，无法继续操作。");
                Set履约完毕显示();
                return;
            }

            DataSet dsNew = bll执行明细.GetBusinessByKey("-1", true);
            string createtime = bll执行明细.ServiceDateTime;
            for (int inner = 0; inner < selectRowIndexs.Length; inner++)
            {
                //DataRow drnew = bll执行明细.NewBusinessWithReturn();
                DataRow drnew = bll执行明细.CurrentBusiness.Tables[0].Rows.Add();
                drnew[tb_JTYS履约执行明细.签约ID] = m_qyid;
                drnew[tb_JTYS履约执行明细.ServiceID] = gv签约服务项目明细.GetRowCellValue(selectRowIndexs[inner], gcolServiceID).ToString();
                drnew[tb_JTYS履约执行明细.服务包名称] = gv签约服务项目明细.GetRowCellValue(selectRowIndexs[inner], gcol服务包名称).ToString();
                drnew[tb_JTYS履约执行明细.项目复合ID] = gv签约服务项目明细.GetRowCellValue(selectRowIndexs[inner], gcol项目复合ID).ToString();
                drnew[tb_JTYS履约执行明细.项目编码] = "";
                drnew[tb_JTYS履约执行明细.项目名称] = gv签约服务项目明细.GetRowCellValue(selectRowIndexs[inner], gcol项目名称).ToString();
                drnew[tb_JTYS履约执行明细.执行数量] = 1;
                drnew[tb_JTYS履约执行明细.项目单位] =gv签约服务项目明细.GetRowCellValue(selectRowIndexs[inner], gcol项目单位).ToString();
                drnew[tb_JTYS履约执行明细.服务时间] =de执行日期.DateTime.ToString("yyyy-MM-dd");
                if (!string.IsNullOrEmpty(txt服务医生.Text.Trim()))
                {
                    drnew[tb_JTYS履约执行明细.服务医生] = txt服务医生.Text.Trim();
                }
                else
                {
                    drnew[tb_JTYS履约执行明细.服务医生] = Loginer.CurrentUser.用户编码;
                }
                drnew[tb_JTYS履约执行明细.开单科室] = txt开单科室.Text.Trim();
                drnew[tb_JTYS履约执行明细.执行科室] = txt执行科室.Text.Trim();

                drnew[tb_JTYS履约执行明细.创建人] = Loginer.CurrentUser.用户编码;
                drnew[tb_JTYS履约执行明细.创建时间] = createtime;
                drnew[tb_JTYS履约执行明细.修改人] = Loginer.CurrentUser.用户编码;
                drnew[tb_JTYS履约执行明细.修改时间] = createtime;
            }
            SaveResult sr = bll执行明细.Save(bll执行明细.CurrentBusiness);
            if (sr.Success)
            {

                try
                {
                    bll执行明细.UpdateLYZT(m_qyid);
                }
                catch(Exception ex)
                {
                    Msg.ShowInformation("更新履约状态可能失败，异常信息："+ex.Message);
                }

                //检查是否有检验项目或检查项目，如果有，提示是否打印条码
                DataSet ds条码 = bll执行明细.Get检验检查项目(m_qyid, createtime);
                //if(ds条码==null || ds条码.Tables.Count < 2)
                //if (ds条码 == null || ds条码.Tables.Count != 2 || (ds条码.Tables[0].Rows.Count == 0 && ds条码.Tables[1].Rows.Count == 0))
                if (ds条码 == null || ds条码.Tables.Count == 0 || (ds条码.Tables[0].Rows.Count == 0 ))
                {
                    //do nothing
                }
                else 
                {
                    string barcodestart = "5" + Convert.ToDateTime(createtime).ToString("yyMMdd") + txtDabh.Text.Trim().Substring(6);
                    List<string> JYbarcodeList = new List<string>();
                    List<string> JCbarcodeList = new List<string>();
                    if(ds条码.Tables[0].Rows.Count > 0 )
                    {
                        #region 获取有几种类型的化验，若项目分组是空的，先排除
                        List<string> grouplist = new List<string>();
                        for (int index = 0; index < ds条码.Tables[0].Rows.Count;index++ )
                        {
                            string temp = ds条码.Tables[0].Rows[index]["项目分组"].ToString();
                            if(grouplist.Contains(temp) || string.IsNullOrWhiteSpace(temp))
                            {
                            }
                            else
                            {
                                grouplist.Add(temp);
                            }
                        }
                        #endregion

                        #region 相同分组的使用同一个条码
                        //每一组使用相同的条码号
                        for (int index = 0; index < grouplist.Count; index++)
                        {
                            string serialcode = bll执行明细.GetSerialCode();
                            string strbarcode = barcodestart + serialcode;

                            JYbarcodeList.Add(strbarcode);

                            DataRow[] drs = ds条码.Tables[0].Select("项目分组='" + grouplist[index] + "'");
                            for(int inner = 0; inner < drs.Length; inner++)
                            {
                                drs[inner][tb_JTYS履约执行明细.执行条码] = strbarcode;
                            }
                        }
                        #endregion

                        #region 分组为空的，每项单独一个条码
                        //获取项目分组是空的
                        DataRow[] drsSpace = ds条码.Tables[0].Select("项目分组='' or 项目分组 is null");
                        for (int index = 0; index < drsSpace.Length; index++)
                        {
                            string serialcode = bll执行明细.GetSerialCode();
                            string strbarcode = barcodestart + serialcode;

                            JYbarcodeList.Add(strbarcode);

                            drsSpace[index][tb_JTYS履约执行明细.执行条码] = strbarcode;
                        }
                        #endregion
                    }

                    #region 生成检查条码,暂时注释掉
                    //if (ds条码.Tables[1].Rows.Count > 0 && MessageBox.Show("是否打印检查条码？", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    //{
                    //    //执行生成检查条码的操作
                    //    for (int index = 0; index < ds条码.Tables[1].Rows.Count; index++)
                    //    {
                    //        string serialcode = bll执行明细.GetSerialCode();
                    //        string strbarcode = barcodestart + serialcode;

                    //        JCbarcodeList.Add(strbarcode);

                    //        ds条码.Tables[1].Rows[index][tb_JTYS履约执行明细.执行条码] = strbarcode;
                    //    }
                    //}
                    #endregion

                    ds条码.Tables[0].Columns.Remove("项目分组");
                    //ds条码.Tables[1].Columns.Remove("项目分组");
                    SaveResult srrInner = bll执行明细.Save(ds条码);

                    if (srrInner.Success && MessageBox.Show("是否打印化验条码？", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == System.Windows.Forms.DialogResult.Yes)
                    {
                        string sex = "";
                        string age = "";
                        string name = txtName.Text.Trim();
                        string sfzh = txtSFZH.Text.Trim();
                        bool checkRet = CardIDHelper.Check(sfzh);


                        sex = CardIDHelper.GetSex(sfzh);
                        if (sex == "1")
                        {
                            sex = "男";
                        }
                        else if (sex == "2")
                        {
                            sex = "女";
                        }

                        age = CardIDHelper.GetAge(sfzh, Convert.ToDateTime(createtime));

                        DataTable dtBarcode = new DataTable();
                        dtBarcode.Columns.Add("baseinfo");
                        dtBarcode.Columns.Add("items");
                        dtBarcode.Columns.Add("barcode");

                        //print barcode 
                        for (int inner = 0; inner < JYbarcodeList.Count; inner++)
                        {
                            DataRow[] jydrs = ds条码.Tables[0].Select(tb_JTYS履约执行明细.执行条码+"='"+JYbarcodeList[inner]+"'");
                            string itemnames = "";
                            
                            for(int iii=0; iii < jydrs.Length; iii++)
                            {
                                string temp = jydrs[iii][tb_JTYS履约执行明细.项目名称].ToString();
                                if(jydrs.Length > 1 && temp.Contains("（"))
                                {
                                    temp = temp.Substring(0, temp.IndexOf("（"));
                                }

                                itemnames += temp;
                                if(iii < jydrs.Length-1)
                                {
                                    itemnames += ",";
                                }
                            }

                            DataRow drbarcode = dtBarcode.Rows.Add();
                            drbarcode["baseinfo"] = name + "/" + sex + "/" + age + " 家医履约";
                            drbarcode["items"] = itemnames;
                            drbarcode["barcode"] = JYbarcodeList[inner];
                        }

                        if(JYbarcodeList.Count > 0)
                        {
                            //TableXtraReport.TableXReport rep  = GetSubReport(dtBarcode.Rows[0]["baseinfo"].ToString(),
                            //                                                       dtBarcode.Rows[0]["items"].ToString(),
                            //                                                       dtBarcode.Rows[0]["barcode"].ToString());
                            TableXtraReport.TableXReport rep = new TableXReport();
                            for(int index =0; index < dtBarcode.Rows.Count; index++)
                            {
                                TableXtraReport.TableXReport subrep = GetSubReport(dtBarcode.Rows[index]["baseinfo"].ToString(),
                                                                                   dtBarcode.Rows[index]["items"].ToString(),
                                                                                   dtBarcode.Rows[index]["barcode"].ToString());
                                rep.Pages.AddRange(subrep.Pages);
                                //rep.Pages.AddRange(subrep.Pages);
                            }

                            //预览条形码页面(该页面有打印功能)
                            DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(rep);
                            tool.ShowPreviewDialog();
                        }
                    }
                }
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }



        private TableXReport GetSubReport(string baseinfo, string itemnames, string strbarcode)
        {
            TableXReport xtr = new TableXReport();

            Font ft = new System.Drawing.Font("宋体", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
            xtr.SetReportTitle(baseinfo, true, ft, TextAlignment.TopLeft, Size.Empty);

            if (itemnames.Length <= 19)
            {
                xtr.SetReportTitle(itemnames, true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                xtr.SetReportTitle(itemnames.Substring(0,19), true, ft7, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(itemnames.Substring(19), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            //xtr.SetReportTitle("密度脂蛋白、低密度脂蛋白）,肾功能（尿素氮、肌肝）", true, ft7, TextAlignment.TopLeft, Size.Empty);
            //指定条码格式
            DevExpress.XtraPrinting.BarCode.Code128Generator barcode = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            barcode.CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetC;

            xtr.SetReportBarCode(strbarcode, new Point(5, 140), new Size(450, 125), barcode, true);
            xtr.TextAlignment = TextAlignment.BottomCenter;
            //SetReportMain只能最后设置一次
            //xtr.SetReportMain(100, 12, 2, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 100, 10, 100), System.Drawing.Printing.PaperKind.A4, Size.Empty, 0, 0, "");           
            xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 6, 6), System.Drawing.Printing.PaperKind.Custom, new Size(500, 300), 0, 0, "");

            //预览条形码页面(该页面有打印功能)
            //DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(xtr);
            //tool.ShowPreviewDialog();

            xtr.CreateDocument();//如果不加这一行，report可能不能显示

            return xtr;
        }

        private void sbtnDone_Click(object sender, EventArgs e)
        {
            if(Msg.AskQuestion("确定要标记为“履约完毕”吗？"))
            {
                bll签约.Set履约完毕ByYQID(m_qyid);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }
    }
}
