﻿using AtomEHR.Library;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class Frm执行结果汇总 : Form
    {
        bllJTYS履约执行明细 bll执行明细 = new bllJTYS履约执行明细();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();

        string m_qyid;
        public Frm执行结果汇总(string qyid)
        {
            InitializeComponent();
            m_qyid = qyid;
        }

        private void Frm执行结果汇总_Load(object sender, EventArgs e)
        {
            DataSet dsInfo = bll签约.GetBusinessByIdFromView(m_qyid);

            txtCardID.Text = dsInfo.Tables[0].Rows[0][AtomEHR.Models.tb_健康档案.身份证号].ToString();
            txtDabh.Text = dsInfo.Tables[0].Rows[0][tb_JTYS签约信息.档案号].ToString();
            txtMagCard.Text = dsInfo.Tables[0].Rows[0][tb_JTYS档案VS磁卡.磁卡号].ToString();
            txtName.Text = DESEncrypt.DES解密(dsInfo.Tables[0].Rows[0][AtomEHR.Models.tb_健康档案.姓名].ToString());
            txtSPname.Text = dsInfo.Tables[0].Rows[0][tb_JTYS签约信息.服务包名称].ToString();
            txtTelNo.Text = dsInfo.Tables[0].Rows[0][tb_JTYS签约信息.联系电话].ToString();

            gc项目执行明细.DataSource = bll执行明细.Get执行明细ByQYID(m_qyid).Tables[0];
        }

        private void gv项目执行明细_Click(object sender, EventArgs e)
        {
            DevExpress.Utils.DXMouseEventArgs args = e as DevExpress.Utils.DXMouseEventArgs;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hInfo = gv项目执行明细.CalcHitInfo(new Point(args.X, args.Y));
            //if (args.Button == MouseButtons.Left && args.Clicks == 2)
            //{
            //判断光标是否在行范围内 
            if (hInfo.InRow)
            {
                string barcode = gv项目执行明细.GetRowCellValue(gv项目执行明细.FocusedRowHandle, tb_JTYS履约执行明细.执行条码).ToString();
                if(!string.IsNullOrWhiteSpace(barcode))
                {
                    DataTable dt化验结果 = bll执行明细.Get化验结果ByBarcode(barcode);
                    gridControl1.DataSource = dt化验结果;
                    gridView1.BestFitColumns();
                }
            }
        }
    }
}
