﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class frm签约 : frmBase
    {
        public frm签约()
        {
            InitializeComponent();
        }
        private string m_dassjg = string.Empty;
        public frm签约(string dah, string name, string cardID, string telno, string address, string dassjg)
        {
            InitializeComponent();

            this.txtDabh.Text = dah;
            this.txtName.Text = name;
            this.txtTelNo.Text = telno;
            this.txtCardID.Text = cardID;
            this.txtAddress.Text = address;

            m_dassjg = dassjg;

            this.txt签约人.Text = Loginer.CurrentUser.AccountName;

            InitForm();

            if (Loginer.CurrentUser.所属机构.StartsWith("371321"))
            {
                layoutControlItem10.Text = "签约人：";
                layoutControlItem12.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            else
            {
                layoutControlItem10.Text = "录入账号：";
            }
        }

        bllJTYS签约对象类型 bll签约对象 = new bllJTYS签约对象类型();
        bllJTYS健康状况 bll健康状况 = new bllJTYS健康状况();
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        bllJTYS团队概要 bll团队 = new bllJTYS团队概要();
        bllJTYS团队成员 bll成员 = new bllJTYS团队成员();
        bllJTYS签约医生签字 bll医生签字 = new bllJTYS签约医生签字();

        DataTable m_dtYouHui = null;
        void GetYouHui()
        {
            bllJTYS优惠方式 bllYH = new bllJTYS优惠方式();
            string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
            m_dtYouHui = bllYH.getAllValidateYH(ssjg);
        }
        void InitForm()
        {
            //获取服务包与签约对象类型之间的优惠关系
            GetYouHui();
            
            //签约对象设置数据源
            DataTable dt签约对象 = bll签约对象.GetAllValidateData();
            //util.ControlsHelper.BindComboxData(dt签约对象, cboServiceObject, tb_JTYS签约对象类型.ID, tb_JTYS签约对象类型.类型名称);
            lueServiceObject.Properties.ValueMember = tb_JTYS签约对象类型.ID;
            lueServiceObject.Properties.DisplayMember = tb_JTYS签约对象类型.类型名称;
            lueServiceObject.Properties.DataSource = dt签约对象;


            //健康状况设置数据源
            DataTable dt健康状况 = bll健康状况.GetAllValidateData();
            cboeHealthCondition.Properties.ValueMember = tb_JTYS健康状况.ID;
            cboeHealthCondition.Properties.DisplayMember = tb_JTYS健康状况.名称;
            cboeHealthCondition.Properties.DataSource = dt健康状况;

            //服务包设置数据源
            DataTable dt服务包 = bll服务包.GetAllValidateData();
            if(dt服务包!=null)
            {
                dt服务包.Columns.Add("年收费标准2");
                dt服务包.Columns.Add("优惠方式");
                for(int index = 0; index< dt服务包.Rows.Count;index++)
                {
                    dt服务包.Rows[index]["年收费标准2"] = dt服务包.Rows[index]["年收费标准"];
                }
            }
            
            gcServicePackage.DataSource = dt服务包;

            //服务团队数据源
            DataTable dt团队 = bll团队.GetAllValidateData();
            lookUpEditServiceTeam.Properties.ValueMember = tb_JTYS团队概要.团队ID;
            lookUpEditServiceTeam.Properties.DisplayMember = tb_JTYS团队概要.团队名称;
            lookUpEditServiceTeam.Properties.DataSource = dt团队;

            this.dateEdit签约.DateTime = Convert.ToDateTime(bll服务包.ServiceDateTime);

            //判断此人是否为贫困人口
            //查询是否为贫困人口
            bool isPK = false;
            if (isPK)
            {
                emptySpaceItem贫困人口.Text = "此人可能属于贫困人口";
            }
            else
            {
                emptySpaceItem贫困人口.Text = " ";
            }
        }

        private void frm签约_Load(object sender, EventArgs e)
        {
            this.ActiveControl = txtMagCard;
        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        bllJTYS档案VS磁卡 bll档案磁卡 = new bllJTYS档案VS磁卡();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        bllJTYS全局参数 bll全局 = new bllJTYS全局参数();
        private void btn保存_Click(object sender, EventArgs e)
        {
            string isNeedMagCard = bll全局.GetValueByCode("NeedMagCard");
            if(isNeedMagCard=="1" || string.IsNullOrWhiteSpace(isNeedMagCard))
            {
                if (string.IsNullOrWhiteSpace(txtMagCard.Text))
                {
                    Msg.ShowInformation("请录入磁卡号。");
                    return;
                }
            }

            if (lueServiceObject.EditValue == null || string.IsNullOrWhiteSpace(lueServiceObject.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择签约对象类型。");
                return;
            }

            if (cboeHealthCondition.EditValue == null || string.IsNullOrWhiteSpace(cboeHealthCondition.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择健康状况。");
                return;
            }

            if (lookUpEditServiceTeam.EditValue == null || string.IsNullOrWhiteSpace(lookUpEditServiceTeam.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择服务团队。");
                return;
            }

            int[] selectedIndex = gvServicePackage.GetSelectedRows();
            if(selectedIndex.Length ==0)
            {
                Msg.ShowInformation("请至少选择一个服务包。");
                return;
            }

            if (Loginer.CurrentUser.所属机构.StartsWith("371321"))
            {
            }
            else
            {
                if (rgp签约医生.EditValue == null || string.IsNullOrWhiteSpace(rgp签约医生.EditValue.ToString()))
                {
                    Msg.ShowInformation("请选择签约人");
                }
            }

            if (!b手签更新 || !b医生手签)
            {
                bool askret = Msg.AskQuestion("确定不需要录入指纹吗？点“是”，继续保存签约记录，点“否”取消保存。");
                if (!askret)
                {
                    return;
                }
            }

            //else if(selectedIndex.Length > 1)
            //{
            //    Msg.ShowInformation("只允许选择一个服务包。");
            //    return;
            //}
            decimal total = 0;
            StringBuilder packagebuilder = new StringBuilder();
            StringBuilder serviceIDBuilder = new StringBuilder(",");
            for(int index = 0; index < selectedIndex.Length; index++)
            {
                //total += Convert.ToDecimal(gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.年收费标准));
                total += Convert.ToDecimal(gvServicePackage.GetRowCellValue(selectedIndex[index], col年收费标准));

                serviceIDBuilder.Append(gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.ServiceID) + ",");
                if(index < selectedIndex.Length-1)
                {
                    packagebuilder.Append(gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.名称)+"，");
                }
                else
                {
                    packagebuilder.Append(gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.名称));
                }
            }

            //进行服务包重复性检查
            bool checkre = CheckUniqueServicePackage(txtDabh.Text.Trim());
            if(!checkre)//false 有重复签约的服务包内容
            {
                return;
            }

            DialogResult result = MessageBox.Show("您选择了如下服务包：\n\n" + packagebuilder.ToString() + "\n总共需收费："+total.ToString()+"元。\n\n点击“确定”按钮，进行签约保存；\n点击“取消”按钮，可以重新选择。", "服务包确认", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if(result == System.Windows.Forms.DialogResult.Cancel)
            {
                return;
            }

            

            if (isNeedMagCard == "1" || string.IsNullOrWhiteSpace(isNeedMagCard))
            {
                //校验次档案号是否已发卡，或磁卡已发出
                DataSet ds = bll档案磁卡.GetDataTableForCheckBy档案号磁卡号(txtDabh.Text.Trim(), txtMagCard.Text.Trim());
                if (ds == null || ds.Tables.Count != 3)
                {
                    Msg.ShowInformation("对档案号、磁卡号进行检验时出现未知的异常。");
                    return;
                }
                else if (ds.Tables[0].Rows.Count > 0)//已发卡，当卡号与本次填写的卡号不一致
                {
                    Msg.ShowInformation("档案号【" + txtDabh.Text.Trim() + "】已发卡，已发磁卡卡号为：" + ds.Tables[0].Rows[0][tb_JTYS档案VS磁卡.磁卡号]);
                    return;
                }
                else if (ds.Tables[1].Rows.Count > 0)//磁卡已发出，但发卡记录中的档案号与本次填写的档案号不一致
                {
                    Msg.ShowInformation("卡号为【" + txtMagCard.Text.Trim() + "】的磁卡已发出，对应的健康档案号为：" + ds.Tables[1].Rows[0][tb_JTYS档案VS磁卡.档案号]);
                    return;
                }
                else if (ds.Tables[2].Rows.Count > 0)//表示已发磁卡，并且磁卡与本次填写的卡号一致
                {
                    //不做任何处理
                }
                else//表示未发卡，磁卡也未使用
                {
                    //保存磁卡档案号对应关系
                    bll档案磁卡.GetBusinessByKey("-1", true);
                    bll档案磁卡.NewBusiness();
                    bll档案磁卡.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.档案号] = txtDabh.Text.Trim();
                    bll档案磁卡.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.磁卡号] = txtMagCard.Text.Trim();
                    //bll档案磁卡.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.状态] = "1";
                    bll档案磁卡.Save(bll档案磁卡.CurrentBusiness);
                }
            }
            
            string str费用ID = Guid.NewGuid().ToString().Replace("-", "");

            bllJTYS签约费用明细 bll明细 = new bllJTYS签约费用明细();
            bll明细.GetBusinessByKey("-1", true);
            for (int index = 0; index < selectedIndex.Length; index++)
            {
                DataRow drtemp = bll明细.NewBusinessNew();
                drtemp[tb_JTYS签约费用明细.费用ID] = str费用ID;
                drtemp[tb_JTYS签约费用明细.SPID] = gvServicePackage.GetRowCellValue(selectedIndex[index], tb_JTYS服务包.ServiceID);

                string str优惠方式 = gvServicePackage.GetRowCellValue(selectedIndex[index], gcol优惠方式).ToString();

                if (str优惠方式 == null || string.IsNullOrWhiteSpace(str优惠方式))
                {
                    drtemp[tb_JTYS签约费用明细.优惠方式] = DBNull.Value;
                }
                else
                {
                    drtemp[tb_JTYS签约费用明细.优惠方式] = Convert.ToInt32(str优惠方式);
                }
                
                drtemp[tb_JTYS签约费用明细.PRICE] = Convert.ToDecimal(gvServicePackage.GetRowCellValue(selectedIndex[index], col年收费标准));
            }
            if (bll明细.CurrentBusiness.Tables.Count >0)
            {
                bll明细.Save(bll明细.CurrentBusiness);
            }

            //新加的20181122
            if (b医生手签)
            {
                bll医生签字.GetBusinessByKey("-1", true);
                bll医生签字.NewBusiness();

                //指纹
                using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
                {
                    this.pictureEdit1.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] arr1 = new byte[ms1.Length];
                    ms1.Position = 0;
                    ms1.Read(arr1, 0, (int)ms1.Length);
                    ms1.Close();
                    bll医生签字.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约医生签字.签字] = Convert.ToBase64String(arr1);
                }
                bll医生签字.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约医生签字.费用ID] = str费用ID;
                bll医生签字.Save(bll医生签字.CurrentBusiness);
            }

            //保存签约信息
            bll签约.GetBusinessByKey("-1", true);
            bll签约.NewBusiness();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.收费金额] = total;
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.档案号] = txtDabh.Text.Trim();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.联系电话] = txtTelNo.Text.Trim();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约对象类型] = lueServiceObject.EditValue.ToString();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.健康状况] = ","+cboeHealthCondition.EditValue.ToString()+",";
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.健康状况名称] = cboeHealthCondition.Text;
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约服务包] = serviceIDBuilder.ToString();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.服务包名称] = packagebuilder.ToString().Replace("，",",");
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.团队ID] = lookUpEditServiceTeam.EditValue.ToString();
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约日期] = dateEdit签约.DateTime.ToString("yyyy-MM-dd");
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约医生] = Loginer.CurrentUser.用户编码;

            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.履约状态] = "未履约";

            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.签约人] = rgp签约医生.EditValue.ToString();

            
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.费用ID] = str费用ID;

            //由于暂不需要审批功能，故先添加这两个字段
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.生效日期] = dateEdit签约.DateTime.ToString("yyyy-MM-dd");
            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.到期日期] = dateEdit签约.DateTime.AddYears(1).AddDays(-1).ToString("yyyy-MM-dd");

            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.录入人机构] = Loginer.CurrentUser.所属机构;

            bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.档案机构] = m_dassjg;

            if (b手签更新)
            {
                //指纹
                using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
                {
                    this.peFinger.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] arr1 = new byte[ms1.Length];
                    ms1.Position = 0;
                    ms1.Read(arr1, 0, (int)ms1.Length);
                    ms1.Close();
                    bll签约.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约信息.手签照] = Convert.ToBase64String(arr1);
                }
            }

            


            //bll档案磁卡.CurrentBusiness.Tables[0].Rows[0][tb_JTYS档案VS磁卡.状态] = "1";
            bll签约.Save(bll签约.CurrentBusiness);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }


        /// <summary>
        /// 检查已签约服务包的唯一性,true检验没有重复，false当前签约的服务包与已签约的服务包有重复
        /// </summary>
        /// <returns></returns>
        public bool CheckUniqueServicePackage(string dah)
        {
            bool ret = false;

            DataTable dtServiceID = bll签约.GetValidateSignedServiceidByDah(dah);
            List<string> list = new List<string>();
            for (int index = 0; index < dtServiceID.Rows.Count; index++)
            {
                string[] arrSID = dtServiceID.Rows[index][tb_JTYS签约信息.签约服务包].ToString().Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int inner = 0; inner < arrSID.Length; inner++)
                {
                    list.Add(arrSID[inner].Trim());
                }
            }

            string snameTemp = "";
            int[] selectedIndex = gvServicePackage.GetSelectedRows();
            if(selectedIndex.Length > 0)
            {
                for(int sindex = 0; sindex < selectedIndex.Length; sindex++)
                {
                    string sMoreOnce = gvServicePackage.GetRowCellValue(selectedIndex[sindex], tb_JTYS服务包.允许一年多次).ToString();
                    if(sMoreOnce=="1")//一年多次，不再验证
                    {
                        continue;
                    }

                    string sid = gvServicePackage.GetRowCellValue(selectedIndex[sindex], tb_JTYS服务包.ServiceID).ToString();
                    if (list.Contains(sid))
                    {
                        snameTemp += gvServicePackage.GetRowCellValue(selectedIndex[sindex], tb_JTYS服务包.名称).ToString()+", ";
                    }
                }
            }

            if(snameTemp.Length > 0)
            {
                Msg.ShowInformation("此人已签约过下列服务包，请不要重复签约：\n"+ snameTemp.Substring(0, snameTemp.Length-2));
                ret = false;
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        private void lookUpEditServiceTeam_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                rgp签约医生.Properties.Items.Clear();
                string tdid = lookUpEditServiceTeam.EditValue.ToString();
                DataTable dtmembers = bll成员.GetSignListByTeamID(tdid);
                for(int index = 0; index< dtmembers.Rows.Count; index++)
                {
                    DevExpress.XtraEditors.Controls.RadioGroupItem item = new DevExpress.XtraEditors.Controls.RadioGroupItem();
                    //item.Value = dtmembers.Rows[index][tb_JTYS团队成员.成员类别].ToString()
                    //    + "||" + dtmembers.Rows[index][tb_JTYS团队成员.成员ID].ToString();
                    item.Value = dtmembers.Rows[index][tb_JTYS团队成员.ID].ToString();
                    item.Description = dtmembers.Rows[index][tb_JTYS团队成员.成员姓名].ToString();

                    rgp签约医生.Properties.Items.Add(item);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lueServiceObject_EditValueChanged(object sender, EventArgs e)
        {
            if (m_dtYouHui == null || m_dtYouHui.Rows.Count == 0)
            {
                return;
            }

            string str = lueServiceObject.EditValue.ToString();

            DataRow[] drs = m_dtYouHui.Select("对象类型="+str);
            if (drs.Length > 0)
            {
                string strspid = drs[0]["服务包ID"].ToString();
                DataTable dtSP = gcServicePackage.DataSource as DataTable;
                if (dtSP != null && dtSP.Rows.Count > 0)
                {
                    DataRow[] drSP = dtSP.Select("ServiceID='" + strspid + "'");
                    if (drSP.Length > 0)
                    {
                        if (drs[0][tb_JTYS优惠方式.优惠方式].ToString() == "定额优惠")
                        {
                            decimal dec = Convert.ToDecimal(drSP[0]["年收费标准"].ToString()) - Convert.ToDecimal(drs[0][tb_JTYS优惠方式.优惠额度].ToString());
                            drSP[0]["年收费标准2"] = dec < 0 ? 0.00m : dec;
                        }
                        else if (drs[0][tb_JTYS优惠方式.优惠方式].ToString() == "折扣优惠")
                        {
                            drSP[0]["年收费标准2"] = Convert.ToDecimal(drSP[0]["年收费标准"].ToString()) - Convert.ToDecimal(drs[0][tb_JTYS优惠方式.优惠额度].ToString());
                        }

                        drSP[0]["优惠方式"] = drs[0][tb_JTYS优惠方式.ID].ToString();

                        for (int index = 0; index < dtSP.Rows.Count; index++)
                        {
                            if (dtSP.Rows[index][tb_JTYS服务包.ServiceID].ToString() != strspid)
                            {
                                dtSP.Rows[index]["年收费标准2"] = dtSP.Rows[index]["年收费标准"];
                                dtSP.Rows[index]["优惠方式"] = DBNull.Value;
                            }
                        }
                    }
                }
            }
            else
            {
                DataTable dtSP = gcServicePackage.DataSource as DataTable;
                for (int index = 0; index < dtSP.Rows.Count; index++)
                {
                    dtSP.Rows[index]["年收费标准2"] = dtSP.Rows[index]["年收费标准"];
                    dtSP.Rows[index]["优惠方式"] = DBNull.Value;
                }
            }
        }

        //FingerPrintHelper.FPForm frm = new FingerPrintHelper.FPForm();
        bool b手签更新 = false;
        private void sbtnGetFinger_Click(object sender, EventArgs e)
        {
            //frm.bmp = null;
            //DialogResult result = frm.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    b手签更新 = true;
            //    peFinger.Image = frm.bmp;
            //}
        }

        bool b医生手签 = false;
        private void sbtnYSFinger_Click(object sender, EventArgs e)
        {
            //frm.bmp = null;
            //DialogResult result = frm.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    b医生手签 = true;
            //    pictureEdit1.Image = frm.bmp;
            //}
        }
    }
}

