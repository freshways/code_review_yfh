﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using DevExpress.XtraRichEdit;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class XtraReportP2 : DevExpress.XtraReports.UI.XtraReport
    {
        string m_renqun = "";
        string m_strContent = "";
        public XtraReportP2(DataTable dtSetting, DataTable dtAllMember, string capID, string qyrID, string rqxinxi,
                            string name, string sfzh, string dah, string tel, string addr, string sqz, string qyrq, string ysqz)
        {
            InitializeComponent();

            m_renqun = rqxinxi;
            
            xrlYFName.Text = name;
            xrlYFsfzh.Text = sfzh;
            xrlYFdah.Text = dah;
            xrlYFaddr.Text = addr;
            xrlYFTel.Text = tel;

            if (!string.IsNullOrWhiteSpace(qyrq))
            {
                xrLabel59.Text = qyrq;
            }
            #region 手签
            if (!string.IsNullOrEmpty(sqz) && sqz.Length > 12)
            {
                Byte[] bitmapData = new Byte[sqz.Length];
                try
                {
                    bitmapData = Convert.FromBase64String(sqz);
                    System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);
                    xrP乙方手签.Image = Image.FromStream(streamBitmap);
                }
                catch
                {
                    xrP乙方手签.Image = null;
                    return;
                }
            }
            else
            {
                xrP乙方手签.Image = null;
            }
            #endregion

            #region 医生手签
            if (!string.IsNullOrEmpty(ysqz) && ysqz.Length > 12)
            {
                Byte[] bitmapData = new Byte[ysqz.Length];
                try
                {
                    bitmapData = Convert.FromBase64String(ysqz);
                    System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);
                    xrPYSSQ.Image = Image.FromStream(streamBitmap);
                }
                catch
                {
                    xrPYSSQ.Image = null;
                    return;
                }
            }
            else
            {
                xrPYSSQ.Image = null;
            }
            #endregion

            if (dtSetting!=null && dtSetting.Rows.Count>0)
            {
                m_strContent = dtSetting.Rows[0]["contents"].ToString();
                
                xrlTitle.Text = dtSetting.Rows[0]["title"].ToString();
                xrlHosp.Text = dtSetting.Rows[0]["hosp"].ToString();

                xrlSignerTitle.Text = dtSetting.Rows[0]["signerTitle"].ToString();

                string isShowCap = dtSetting.Rows[0]["showCap"].ToString();
                string isShowObj = dtSetting.Rows[0]["showSerObj"].ToString();
                string showdahsfz = dtSetting.Rows[0]["notype"].ToString();
                string showbh = dtSetting.Rows[0]["showno"].ToString();
                

                #region 控制协议书编号
                if(showbh!="1")
                {
                    xrLabel2.Visible = false;
                    xrLabel3.Visible = false;
                }
                #endregion

                #region 身份证号、档案号的显示控制
                if (showdahsfz=="1")//只显示身份证号
                {
                    xrLabel13.Visible = false;
                    xrlYFsfzh.Visible = false;
                    xrLabel6.Text = xrLabel13.Text;
                    xrlYFdah.Text = xrlYFsfzh.Text;
                }
                else if(showdahsfz=="2")//只显示档案号
                {
                    xrLabel13.Visible = false;
                    xrlYFsfzh.Visible = false;
                }
                else //if(showdahsfz=="3")//同时显示档案号、身份证号
                { }//do nothing
                #endregion

                #region b不显示签约对象时的处理
                if (isShowObj!="1")
                {
                    xrLabel1.Visible = false;
                    xrRichText1.Visible = false;

                    float moveupsize = xrRichText1.LocationF.Y + xrRichText1.SizeF.Height-xrLabel1.LocationF.Y;
                    xrLabel19.LocationF = new PointF(xrLabel19.LocationF.X, xrLabel19.LocationF.Y - moveupsize);
                    xrlteamcap.LocationF = new PointF(xrlteamcap.LocationF.X, xrlteamcap.LocationF.Y - moveupsize);
                    xrlTeamCapName.LocationF = new PointF(xrlTeamCapName.LocationF.X, xrlTeamCapName.LocationF.Y - moveupsize);
                    xrlteamcaptel.LocationF = new PointF(xrlteamcaptel.LocationF.X, xrlteamcaptel.LocationF.Y - moveupsize);
                    xrlTeamCapTelText.LocationF = new PointF(xrlTeamCapTelText.LocationF.X, xrlTeamCapTelText.LocationF.Y - moveupsize);
                    xrlMemHead.LocationF = new PointF(xrlMemHead.LocationF.X, xrlMemHead.LocationF.Y - moveupsize);
                    xrTable1.LocationF = new PointF(xrTable1.LocationF.X, xrTable1.LocationF.Y - moveupsize);
                    xrRichText2.LocationF = new PointF(xrRichText2.LocationF.X, xrRichText2.LocationF.Y - moveupsize);
                    xrPanel1.LocationF = new PointF(xrPanel1.LocationF.X, xrPanel1.LocationF.Y - moveupsize);


                }
                #endregion

                #region 不显示团队组长时的处理
                if (isShowCap!="1")
                {
                    xrlteamcap.Visible = false;
                    xrlTeamCapName.Visible = false;
                    xrlteamcaptel.Visible = false;
                    xrlTeamCapTelText.Visible = false;

                    xrlMemHead.LocationF = new PointF(xrlMemHead.LocationF.X, xrlMemHead.LocationF.Y - xrlteamcap.SizeF.Height);
                    xrTable1.LocationF = new PointF(xrTable1.LocationF.X, xrTable1.LocationF.Y - xrlteamcap.SizeF.Height);
                    this.xrRichText2.LocationF = new PointF(this.xrRichText2.LocationF.X, this.xrRichText2.LocationF.Y - xrlteamcap.SizeF.Height);
                    this.xrPanel1.LocationF = new PointF(this.xrPanel1.LocationF.X, this.xrPanel1.LocationF.Y - xrlteamcap.SizeF.Height);
                }
                #endregion

                #region 判断是否显示团队内的角色
                bool isShowRole = true;

                if (dtAllMember != null)
                {
                    for (int index = 0; index < dtAllMember.Rows.Count; index++)
                    {
                        if (string.IsNullOrWhiteSpace(dtAllMember.Rows[index]["DocRole"].ToString()))
                        {
                            isShowRole = false;
                            break;
                        }
                    }
                }
                #endregion

                if (isShowRole == false)
                {
                    xrtcDoc0.WidthF = 30f;
                    xrtcDoc1.WidthF = 30f;
                
                    xrtcName0.WidthF -= 100f;
                    xrtcName1.WidthF -= 100f;

                    xrtcTelNo0.WidthF += 30f;
                    xrtcTelNo1.WidthF += 30f;
                }

                if(dtAllMember!=null && dtAllMember.Rows.Count >0)
                {
                    DataTable dtMember = null;
                    if (isShowCap == "1")
                    {
                        DataRow[] drsCap = dtAllMember.Select("ID=" + capID);
                        if (drsCap.Length > 0)
                        {
                            xrlTeamCapName.Text = drsCap[0]["DocName"].ToString();
                            xrlTeamCapTelText.Text = drsCap[0]["TelNo"].ToString();
                        }

                        //DataRow[] drsMem = dtAllMember.Select("ID<>" + capID);
                        //dtMember = drsMem.Length==0? dtAllMember.Clone(): drsMem.CopyToDataTable();
                        
                        dtMember = dtAllMember.Clone();
                        for(int inner = 0; inner < dtAllMember.Rows.Count; inner++)
                        {
                            string idtemp = dtAllMember.Rows[inner]["ID"].ToString();
                            if(idtemp!=capID)
                            {
                                dtMember.ImportRow(dtAllMember.Rows[inner]);
                            }
                        }
                    }
                    else
                    {
                        dtMember = dtAllMember;
                    }

                    Font font = xrtcName0.Font;

                    #region 显示团队成员首行
                    if (dtMember.Rows.Count > 0)
                    {
                        if (isShowRole)
                        {
                            xrtcDoc0.Text = dtMember.Rows[0]["DocRole"].ToString();
                        }
                        else
                        {
                            xrtcDoc0.WidthF = 30f;
                        }
                        if(dtMember.Rows[0]["ID"].ToString()==qyrID)
                        {
                            xrtcName0.Font = new Font(font.FontFamily, font.Size, FontStyle.Bold);
                        }

                        xrtcName0.Text = dtMember.Rows[0]["DocName"].ToString();
                        xrtcTelNo0.Text = dtMember.Rows[0]["TelNo"].ToString();
                    }

                    if (dtMember.Rows.Count > 1)
                    {
                        if (isShowRole)
                        {
                            xrtcDoc1.Text = dtMember.Rows[1]["DocRole"].ToString();
                        }
                        else
                        {
                            xrtcDoc1.WidthF = 30f;
                        }
                        if (dtMember.Rows[1]["ID"].ToString() == qyrID)
                        {
                            xrtcName1.Font = new Font(font.FontFamily, font.Size, FontStyle.Bold);
                        }
                        xrtcName1.Text = dtMember.Rows[1]["DocName"].ToString();
                        xrtcTelNo1.Text = dtMember.Rows[1]["TelNo"].ToString();
                    }
                    #endregion

                    int yushu = dtMember.Rows.Count % 2;
                    int memRowCout = dtMember.Rows.Count / 2 + yushu;//向上取整  Math.Ceiling

                    for(int index=1; index< memRowCout;index++)
                    {
                        XRTableRow row = new XRTableRow();
                        for (int inner = 0; inner < xrTableRow1.Cells.Count; inner++ )
                        {
                            DataRow drMem = null;
                            if (index == (memRowCout - 1) && yushu == 1)
                            {
                                drMem = (inner >= 4) ? null : dtMember.Rows[index * 2];
                            }
                            else
                            {
                                drMem = (inner < 4) ? dtMember.Rows[index * 2] : dtMember.Rows[index * 2 + 1];
                            }

                            XRTableCell existCell = xrTableRow1.Cells[inner] as XRTableCell;
                            XRTableCell cell = new XRTableCell();
                            cell.Borders = existCell.Borders;
                            cell.Dpi = existCell.Dpi;


                            //cell.Font = existCell.Font;

                            if (existCell.Name.StartsWith("xrtcName") && drMem!=null && drMem["ID"].ToString() == qyrID)
                            {
                                cell.Font = new Font(font.FontFamily, font.Size, FontStyle.Bold);
                                cell.CanGrow = false;
                            }
                            else if(existCell.Name.StartsWith("xrtcName"))
                            {
                                cell.Font = font;
                                cell.CanGrow = false;
                            }
                            else
                            {
                                cell.Font = existCell.Font;
                            }

                            cell.Name = "xrtc" + index.ToString() + inner.ToString();
                            cell.StylePriority.UseBorders = existCell.StylePriority.UseBorders;
                            cell.StylePriority.UseFont = existCell.StylePriority.UseFont;
                            cell.Weight = existCell.Weight;
                            
                            string cellname = GetText(inner, drMem);
                            if (existCell.WidthF == 30F && cellname == "其他")
                            {
                            }
                            else
                            {
                                cell.Text = cellname;
                            }
                            

                            row.Cells.Add(cell);
                        }
                        row.Dpi = this.xrTableRow1.Dpi;
                        row.Name = "xrTableRow"+(index+1).ToString();
                        row.Weight = this.xrTableRow1.Weight;
                        xrTable1.Rows.Add(row);
                        //this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {            row});
                    }
                    this.xrRichText2.LocationF = new PointF(this.xrRichText2.LocationF.X, this.xrRichText2.LocationF.Y + (memRowCout - 1) * this.xrTable1.SizeF.Height);
                    this.xrPanel1.LocationF = new PointF(this.xrPanel1.LocationF.X, this.xrPanel1.LocationF.Y + (memRowCout - 1) * this.xrTable1.SizeF.Height);
                    this.xrTable1.SizeF = new System.Drawing.SizeF(this.xrTable1.SizeF.Width, this.xrTable1.SizeF.Height * memRowCout);
                }

            }
            

        }

        private string GetText(int index, DataRow drMem)
        {
            string ret = "";
            switch( index%4)
            {
                case 0:
                    ret = drMem == null ? "其他" : drMem["DocRole"].ToString();
                    break;
                case 1:
                    ret = drMem == null ? "" : drMem["DocName"].ToString();
                    break;
                case 2:
                    ret = "联系电话:";
                    break;
                case 3:
                    ret = drMem == null ? "" : drMem["TelNo"].ToString();
                    break;
                default:
                    ret = "";
                    break;
            }
            return ret;
        }

        private void xrRichText1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRRichText richText = sender as XRRichText;
            RichEditDocumentServer docServer = new RichEditDocumentServer();
            
            //docServer.LoadDocument(filePath);
            docServer.Text = m_renqun;
            docServer.Document.DefaultParagraphProperties.LineSpacingType = DevExpress.XtraRichEdit.API.Native.ParagraphLineSpacing.Multiple;
            docServer.Document.DefaultParagraphProperties.LineSpacingMultiplier = 1.3f;

            richText.Text = docServer.RtfText;
        }

        private void xrRichText2_AfterPrint(object sender, EventArgs e)
        {
           
        }

        private void xrRichText2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRRichText richText = sender as XRRichText;
            RichEditDocumentServer docServer = new RichEditDocumentServer();

            //docServer.LoadDocument(filePath);
            docServer.Text = m_strContent;
            docServer.Document.DefaultParagraphProperties.LineSpacingType = DevExpress.XtraRichEdit.API.Native.ParagraphLineSpacing.Single;
            docServer.Document.DefaultParagraphProperties.LineSpacingMultiplier = 1.3f;

            richText.Text = docServer.RtfText;
        
        }

        //private void SetMemberText(DataRow dr, XRTableCell )

    }
}
