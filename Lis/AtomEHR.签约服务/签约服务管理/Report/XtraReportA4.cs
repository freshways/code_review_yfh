﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class XtraReportA4 : DevExpress.XtraReports.UI.XtraReport
    {
        bool m_isPrinted = false;
        public XtraReportA4()
        {
            InitializeComponent();
        }

        private void XtraReportA4_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            m_isPrinted = true;
        }

        public bool GetPrintState()
        {
            return m_isPrinted;
        }

    }
}
