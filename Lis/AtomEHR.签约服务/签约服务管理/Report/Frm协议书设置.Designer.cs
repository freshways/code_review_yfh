﻿namespace AtomEHR.签约服务.签约服务管理.Report
{
    partial class Frm协议书设置
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnPreview = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.sbtnSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtP1RG = new DevExpress.XtraEditors.TextEdit();
            this.txtP1T = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtSignerTitle = new DevExpress.XtraEditors.TextEdit();
            this.sbtnP2Preview = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnP2Save = new DevExpress.XtraEditors.SimpleButton();
            this.txtP2Contents = new DevExpress.XtraEditors.MemoEdit();
            this.rgpShowCap = new DevExpress.XtraEditors.RadioGroup();
            this.rgpShowRQ = new DevExpress.XtraEditors.RadioGroup();
            this.rgpP2Showds = new DevExpress.XtraEditors.RadioGroup();
            this.txtP2jiafang = new DevExpress.XtraEditors.TextEdit();
            this.rgpP2ShowBH = new DevExpress.XtraEditors.RadioGroup();
            this.txtP2Title = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnP3Preview = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnP3Save = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtp3beizhu = new DevExpress.XtraEditors.MemoEdit();
            this.txtp3title = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnP4Preview = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnP4Save = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtP4beizhu = new DevExpress.XtraEditors.MemoEdit();
            this.txtP4title = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolSPID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnSPEdit = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnSPRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnGlobalPreview = new DevExpress.XtraEditors.SimpleButton();
            this.otherSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.homePage = new DevExpress.XtraEditors.RadioGroup();
            this.paperSize = new DevExpress.XtraEditors.RadioGroup();
            this.showMoney = new DevExpress.XtraEditors.RadioGroup();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lue所属机构 = new DevExpress.XtraEditors.LookUpEdit();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtP1RG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP1T.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP2Contents.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpShowCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpShowRQ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpP2Showds.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP2jiafang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpP2ShowBH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP2Title.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtp3beizhu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtp3title.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtP4beizhu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP4title.Properties)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.homePage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showMoney.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lue所属机构.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.xtraTabControl1);
            this.tpSummary.Controls.Add(this.panelControl2);
            this.tpSummary.Size = new System.Drawing.Size(820, 450);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(826, 456);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(826, 456);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(826, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(648, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(451, 2);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 44);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(820, 406);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.pictureEdit1);
            this.xtraTabPage1.Controls.Add(this.panelControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(814, 377);
            this.xtraTabPage1.Text = "P1设置";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::AtomEHR.签约服务.Properties.Resources.P1;
            this.pictureEdit1.Location = new System.Drawing.Point(413, 0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(401, 377);
            this.pictureEdit1.TabIndex = 1;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.sbtnPreview);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.sbtnSave);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtP1RG);
            this.panelControl1.Controls.Add(this.txtP1T);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(413, 377);
            this.panelControl1.TabIndex = 4;
            // 
            // sbtnPreview
            // 
            this.sbtnPreview.Location = new System.Drawing.Point(314, 129);
            this.sbtnPreview.Name = "sbtnPreview";
            this.sbtnPreview.Size = new System.Drawing.Size(75, 23);
            this.sbtnPreview.TabIndex = 4;
            this.sbtnPreview.Text = "预览";
            this.sbtnPreview.Click += new System.EventHandler(this.sbtnPreview_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(24, 77);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(80, 19);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "单位名称：";
            // 
            // sbtnSave
            // 
            this.sbtnSave.Location = new System.Drawing.Point(209, 129);
            this.sbtnSave.Name = "sbtnSave";
            this.sbtnSave.Size = new System.Drawing.Size(75, 23);
            this.sbtnSave.TabIndex = 3;
            this.sbtnSave.Text = "保存";
            this.sbtnSave.Click += new System.EventHandler(this.sbtnSave_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(56, 39);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 19);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "标题：";
            // 
            // txtP1RG
            // 
            this.txtP1RG.Location = new System.Drawing.Point(110, 74);
            this.txtP1RG.Name = "txtP1RG";
            this.txtP1RG.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtP1RG.Properties.Appearance.Options.UseFont = true;
            this.txtP1RG.Size = new System.Drawing.Size(279, 26);
            this.txtP1RG.TabIndex = 2;
            // 
            // txtP1T
            // 
            this.txtP1T.Location = new System.Drawing.Point(110, 36);
            this.txtP1T.Name = "txtP1T";
            this.txtP1T.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.txtP1T.Properties.Appearance.Options.UseFont = true;
            this.txtP1T.Size = new System.Drawing.Size(279, 26);
            this.txtP1T.TabIndex = 2;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.pictureEdit2);
            this.xtraTabPage2.Controls.Add(this.panelControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(814, 377);
            this.xtraTabPage2.Text = "P2设置";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit2.EditValue = global::AtomEHR.签约服务.Properties.Resources.P2;
            this.pictureEdit2.Location = new System.Drawing.Point(419, 0);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit2.Size = new System.Drawing.Size(395, 377);
            this.pictureEdit2.TabIndex = 1;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.layoutControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(419, 377);
            this.panelControl3.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtSignerTitle);
            this.layoutControl1.Controls.Add(this.sbtnP2Preview);
            this.layoutControl1.Controls.Add(this.sbtnP2Save);
            this.layoutControl1.Controls.Add(this.txtP2Contents);
            this.layoutControl1.Controls.Add(this.rgpShowCap);
            this.layoutControl1.Controls.Add(this.rgpShowRQ);
            this.layoutControl1.Controls.Add(this.rgpP2Showds);
            this.layoutControl1.Controls.Add(this.txtP2jiafang);
            this.layoutControl1.Controls.Add(this.rgpP2ShowBH);
            this.layoutControl1.Controls.Add(this.txtP2Title);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(415, 373);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtSignerTitle
            // 
            this.txtSignerTitle.Location = new System.Drawing.Point(152, 303);
            this.txtSignerTitle.Name = "txtSignerTitle";
            this.txtSignerTitle.Size = new System.Drawing.Size(251, 20);
            this.txtSignerTitle.StyleController = this.layoutControl1;
            this.txtSignerTitle.TabIndex = 12;
            // 
            // sbtnP2Preview
            // 
            this.sbtnP2Preview.Location = new System.Drawing.Point(292, 339);
            this.sbtnP2Preview.Name = "sbtnP2Preview";
            this.sbtnP2Preview.Size = new System.Drawing.Size(78, 22);
            this.sbtnP2Preview.StyleController = this.layoutControl1;
            this.sbtnP2Preview.TabIndex = 11;
            this.sbtnP2Preview.Text = "预览";
            this.sbtnP2Preview.Click += new System.EventHandler(this.sbtnP2Preview_Click);
            // 
            // sbtnP2Save
            // 
            this.sbtnP2Save.Location = new System.Drawing.Point(165, 339);
            this.sbtnP2Save.Name = "sbtnP2Save";
            this.sbtnP2Save.Size = new System.Drawing.Size(77, 22);
            this.sbtnP2Save.StyleController = this.layoutControl1;
            this.sbtnP2Save.TabIndex = 10;
            this.sbtnP2Save.Text = "保存";
            this.sbtnP2Save.Click += new System.EventHandler(this.sbtnP2Save_Click);
            // 
            // txtP2Contents
            // 
            this.txtP2Contents.Location = new System.Drawing.Point(152, 214);
            this.txtP2Contents.Name = "txtP2Contents";
            this.txtP2Contents.Size = new System.Drawing.Size(251, 85);
            this.txtP2Contents.StyleController = this.layoutControl1;
            this.txtP2Contents.TabIndex = 9;
            // 
            // rgpShowCap
            // 
            this.rgpShowCap.Location = new System.Drawing.Point(152, 185);
            this.rgpShowCap.Name = "rgpShowCap";
            this.rgpShowCap.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "显示"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "不显示")});
            this.rgpShowCap.Size = new System.Drawing.Size(251, 25);
            this.rgpShowCap.StyleController = this.layoutControl1;
            this.rgpShowCap.TabIndex = 8;
            // 
            // rgpShowRQ
            // 
            this.rgpShowRQ.Location = new System.Drawing.Point(152, 156);
            this.rgpShowRQ.Name = "rgpShowRQ";
            this.rgpShowRQ.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "显示"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "不显示")});
            this.rgpShowRQ.Size = new System.Drawing.Size(251, 25);
            this.rgpShowRQ.StyleController = this.layoutControl1;
            this.rgpShowRQ.TabIndex = 7;
            // 
            // rgpP2Showds
            // 
            this.rgpP2Showds.Location = new System.Drawing.Point(152, 89);
            this.rgpP2Showds.Name = "rgpP2Showds";
            this.rgpP2Showds.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "只显示身份证号"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "只显示档案号"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "身份证号、档案号都显示")});
            this.rgpP2Showds.Size = new System.Drawing.Size(251, 63);
            this.rgpP2Showds.StyleController = this.layoutControl1;
            this.rgpP2Showds.TabIndex = 5;
            // 
            // txtP2jiafang
            // 
            this.txtP2jiafang.Location = new System.Drawing.Point(152, 65);
            this.txtP2jiafang.Name = "txtP2jiafang";
            this.txtP2jiafang.Size = new System.Drawing.Size(251, 20);
            this.txtP2jiafang.StyleController = this.layoutControl1;
            this.txtP2jiafang.TabIndex = 4;
            // 
            // rgpP2ShowBH
            // 
            this.rgpP2ShowBH.Location = new System.Drawing.Point(152, 36);
            this.rgpP2ShowBH.Name = "rgpP2ShowBH";
            this.rgpP2ShowBH.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "显示"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "不显示")});
            this.rgpP2ShowBH.Size = new System.Drawing.Size(251, 25);
            this.rgpP2ShowBH.StyleController = this.layoutControl1;
            this.rgpP2ShowBH.TabIndex = 2;
            // 
            // txtP2Title
            // 
            this.txtP2Title.Location = new System.Drawing.Point(152, 12);
            this.txtP2Title.Name = "txtP2Title";
            this.txtP2Title.Size = new System.Drawing.Size(251, 20);
            this.txtP2Title.StyleController = this.layoutControl1;
            this.txtP2Title.TabIndex = 1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem1,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem10});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(415, 373);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtP2Title;
            this.layoutControlItem1.CustomizationFormText = "(1)标题：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(395, 24);
            this.layoutControlItem1.Text = "(1)标题：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(137, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.rgpP2ShowBH;
            this.layoutControlItem2.CustomizationFormText = "(2)是否显示协议编号：";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 29);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(195, 29);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(395, 29);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "(2)是否显示协议编号：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(137, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtP2jiafang;
            this.layoutControlItem3.CustomizationFormText = "(3)甲方名称：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(395, 24);
            this.layoutControlItem3.Text = "(3)甲方名称：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(137, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.rgpP2Showds;
            this.layoutControlItem4.CustomizationFormText = "(4)档案号身份证号显示：";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 77);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 67);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(195, 67);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(395, 67);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "(4)档案号身份证号显示：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(137, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.rgpShowRQ;
            this.layoutControlItem6.CustomizationFormText = "(5)是否显示人群分类：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 29);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(195, 29);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(395, 29);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "(5)是否显示人群分类：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(137, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.rgpShowCap;
            this.layoutControlItem5.CustomizationFormText = "(6)是否显示团队长：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 173);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(395, 29);
            this.layoutControlItem5.Text = "(6)是否显示团队长：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(137, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtP2Contents;
            this.layoutControlItem7.CustomizationFormText = "(7)协议内容：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 202);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(395, 89);
            this.layoutControlItem7.Text = "(7)协议内容：";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(137, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.sbtnP2Save;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(153, 327);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(81, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 466);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(153, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.sbtnP2Preview;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(280, 327);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(234, 466);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(46, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(362, 466);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(33, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 454);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(395, 12);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtSignerTitle;
            this.layoutControlItem10.CustomizationFormText = "(8)签约人开头：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 291);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(395, 24);
            this.layoutControlItem10.Text = "(8)签约人开头：";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(137, 14);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.pictureEdit3);
            this.xtraTabPage3.Controls.Add(this.panelControl4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(814, 377);
            this.xtraTabPage3.Text = "P3设置";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit3.EditValue = global::AtomEHR.签约服务.Properties.Resources.P3;
            this.pictureEdit3.Location = new System.Drawing.Point(386, 0);
            this.pictureEdit3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit3.Size = new System.Drawing.Size(428, 377);
            this.pictureEdit3.TabIndex = 3;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.sbtnP3Preview);
            this.panelControl4.Controls.Add(this.sbtnP3Save);
            this.panelControl4.Controls.Add(this.labelControl5);
            this.panelControl4.Controls.Add(this.labelControl4);
            this.panelControl4.Controls.Add(this.txtp3beizhu);
            this.panelControl4.Controls.Add(this.txtp3title);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(386, 377);
            this.panelControl4.TabIndex = 2;
            // 
            // sbtnP3Preview
            // 
            this.sbtnP3Preview.Location = new System.Drawing.Point(287, 282);
            this.sbtnP3Preview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sbtnP3Preview.Name = "sbtnP3Preview";
            this.sbtnP3Preview.Size = new System.Drawing.Size(74, 23);
            this.sbtnP3Preview.TabIndex = 5;
            this.sbtnP3Preview.Text = "预览";
            this.sbtnP3Preview.Click += new System.EventHandler(this.sbtnP3Preview_Click);
            // 
            // sbtnP3Save
            // 
            this.sbtnP3Save.Location = new System.Drawing.Point(194, 282);
            this.sbtnP3Save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sbtnP3Save.Name = "sbtnP3Save";
            this.sbtnP3Save.Size = new System.Drawing.Size(71, 23);
            this.sbtnP3Save.TabIndex = 4;
            this.sbtnP3Save.Text = "保存";
            this.sbtnP3Save.Click += new System.EventHandler(this.sbtnP3Save_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(8, 65);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 14);
            this.labelControl5.TabIndex = 3;
            this.labelControl5.Text = "备注：";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(9, 34);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 14);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "标题：";
            // 
            // txtp3beizhu
            // 
            this.txtp3beizhu.Location = new System.Drawing.Point(52, 63);
            this.txtp3beizhu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtp3beizhu.Name = "txtp3beizhu";
            this.txtp3beizhu.Size = new System.Drawing.Size(319, 203);
            this.txtp3beizhu.TabIndex = 1;
            // 
            // txtp3title
            // 
            this.txtp3title.Location = new System.Drawing.Point(53, 32);
            this.txtp3title.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtp3title.Name = "txtp3title";
            this.txtp3title.Size = new System.Drawing.Size(318, 20);
            this.txtp3title.TabIndex = 0;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.pictureBox2);
            this.xtraTabPage4.Controls.Add(this.panelControl5);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(814, 377);
            this.xtraTabPage4.Text = "P4设置";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::AtomEHR.签约服务.Properties.Resources.P3;
            this.pictureBox2.Location = new System.Drawing.Point(403, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(411, 377);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.sbtnP4Preview);
            this.panelControl5.Controls.Add(this.sbtnP4Save);
            this.panelControl5.Controls.Add(this.labelControl7);
            this.panelControl5.Controls.Add(this.labelControl6);
            this.panelControl5.Controls.Add(this.txtP4beizhu);
            this.panelControl5.Controls.Add(this.txtP4title);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(403, 377);
            this.panelControl5.TabIndex = 2;
            // 
            // sbtnP4Preview
            // 
            this.sbtnP4Preview.Location = new System.Drawing.Point(283, 281);
            this.sbtnP4Preview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sbtnP4Preview.Name = "sbtnP4Preview";
            this.sbtnP4Preview.Size = new System.Drawing.Size(88, 23);
            this.sbtnP4Preview.TabIndex = 5;
            this.sbtnP4Preview.Text = "预览";
            this.sbtnP4Preview.Click += new System.EventHandler(this.sbtnP4Preview_Click);
            // 
            // sbtnP4Save
            // 
            this.sbtnP4Save.Location = new System.Drawing.Point(181, 281);
            this.sbtnP4Save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sbtnP4Save.Name = "sbtnP4Save";
            this.sbtnP4Save.Size = new System.Drawing.Size(73, 23);
            this.sbtnP4Save.TabIndex = 4;
            this.sbtnP4Save.Text = "保存";
            this.sbtnP4Save.Click += new System.EventHandler(this.sbtnP4Save_Click);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(10, 74);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 14);
            this.labelControl7.TabIndex = 3;
            this.labelControl7.Text = "备注：";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(10, 31);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(36, 14);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "标题：";
            // 
            // txtP4beizhu
            // 
            this.txtP4beizhu.Location = new System.Drawing.Point(54, 72);
            this.txtP4beizhu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtP4beizhu.Name = "txtP4beizhu";
            this.txtP4beizhu.Size = new System.Drawing.Size(333, 195);
            this.txtP4beizhu.TabIndex = 1;
            // 
            // txtP4title
            // 
            this.txtP4title.Location = new System.Drawing.Point(54, 29);
            this.txtP4title.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtP4title.Name = "txtP4title";
            this.txtP4title.Size = new System.Drawing.Size(333, 20);
            this.txtP4title.TabIndex = 0;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.panelControl7);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(814, 377);
            this.xtraTabPage5.Text = "服务包分页显示设置";
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.gridControl1);
            this.panelControl7.Controls.Add(this.panelControl8);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Padding = new System.Windows.Forms.Padding(10);
            this.panelControl7.Size = new System.Drawing.Size(814, 377);
            this.panelControl7.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(12, 62);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(790, 303);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolSPID,
            this.gcolName,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gcolSPID
            // 
            this.gcolSPID.Caption = "服务包ID";
            this.gcolSPID.FieldName = "ServiceID";
            this.gcolSPID.Name = "gcolSPID";
            // 
            // gcolName
            // 
            this.gcolName.Caption = "服务包名称";
            this.gcolName.FieldName = "名称";
            this.gcolName.Name = "gcolName";
            this.gcolName.Visible = true;
            this.gcolName.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "一级打印名称";
            this.gridColumn3.FieldName = "name1";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "二级打印名称";
            this.gridColumn4.FieldName = "name2";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "三级打印名称";
            this.gridColumn5.FieldName = "name3";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "所在页面";
            this.gridColumn6.FieldName = "pageNo";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "页内排序";
            this.gridColumn7.FieldName = "orderby";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "优惠前价格";
            this.gridColumn8.FieldName = "totalPrice";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.sbtnSPEdit);
            this.panelControl8.Controls.Add(this.sbtnSPRefresh);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl8.Location = new System.Drawing.Point(12, 12);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(790, 50);
            this.panelControl8.TabIndex = 1;
            // 
            // sbtnSPEdit
            // 
            this.sbtnSPEdit.Location = new System.Drawing.Point(124, 15);
            this.sbtnSPEdit.Name = "sbtnSPEdit";
            this.sbtnSPEdit.Size = new System.Drawing.Size(75, 23);
            this.sbtnSPEdit.TabIndex = 0;
            this.sbtnSPEdit.Text = "编辑";
            this.sbtnSPEdit.Click += new System.EventHandler(this.sbtnSPEdit_Click);
            // 
            // sbtnSPRefresh
            // 
            this.sbtnSPRefresh.Location = new System.Drawing.Point(17, 15);
            this.sbtnSPRefresh.Name = "sbtnSPRefresh";
            this.sbtnSPRefresh.Size = new System.Drawing.Size(75, 23);
            this.sbtnSPRefresh.TabIndex = 0;
            this.sbtnSPRefresh.Text = "刷新";
            this.sbtnSPRefresh.Click += new System.EventHandler(this.sbtnSPRefresh_Click);
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.panelControl6);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(814, 377);
            this.xtraTabPage6.Text = "其他属性设置";
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.sbtnGlobalPreview);
            this.panelControl6.Controls.Add(this.otherSave);
            this.panelControl6.Controls.Add(this.labelControl10);
            this.panelControl6.Controls.Add(this.labelControl9);
            this.panelControl6.Controls.Add(this.labelControl8);
            this.panelControl6.Controls.Add(this.homePage);
            this.panelControl6.Controls.Add(this.paperSize);
            this.panelControl6.Controls.Add(this.showMoney);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(0, 0);
            this.panelControl6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(814, 377);
            this.panelControl6.TabIndex = 1;
            // 
            // sbtnGlobalPreview
            // 
            this.sbtnGlobalPreview.Location = new System.Drawing.Point(566, 27);
            this.sbtnGlobalPreview.Name = "sbtnGlobalPreview";
            this.sbtnGlobalPreview.Size = new System.Drawing.Size(133, 68);
            this.sbtnGlobalPreview.TabIndex = 7;
            this.sbtnGlobalPreview.Text = "整体效果预览";
            this.sbtnGlobalPreview.Click += new System.EventHandler(this.sbtnGlobalPreview_Click);
            // 
            // otherSave
            // 
            this.otherSave.Location = new System.Drawing.Point(348, 190);
            this.otherSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.otherSave.Name = "otherSave";
            this.otherSave.Size = new System.Drawing.Size(88, 28);
            this.otherSave.TabIndex = 6;
            this.otherSave.Text = "保存";
            this.otherSave.Click += new System.EventHandler(this.otherSave_Click);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(65, 128);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(84, 14);
            this.labelControl10.TabIndex = 5;
            this.labelControl10.Text = "是否显示首页：";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(91, 74);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 14);
            this.labelControl9.TabIndex = 4;
            this.labelControl9.Text = "纸张大小：";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(39, 27);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(108, 14);
            this.labelControl8.TabIndex = 3;
            this.labelControl8.Text = "是否显示减免金额：";
            // 
            // homePage
            // 
            this.homePage.Location = new System.Drawing.Point(171, 124);
            this.homePage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.homePage.Name = "homePage";
            this.homePage.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "否")});
            this.homePage.Size = new System.Drawing.Size(264, 23);
            this.homePage.TabIndex = 2;
            // 
            // paperSize
            // 
            this.paperSize.Location = new System.Drawing.Point(171, 70);
            this.paperSize.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.paperSize.Name = "paperSize";
            this.paperSize.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("A3", "A3"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("A4", "A4")});
            this.paperSize.Size = new System.Drawing.Size(264, 25);
            this.paperSize.TabIndex = 1;
            // 
            // showMoney
            // 
            this.showMoney.Location = new System.Drawing.Point(172, 23);
            this.showMoney.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.showMoney.Name = "showMoney";
            this.showMoney.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "显示"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("0", "不显示")});
            this.showMoney.Size = new System.Drawing.Size(264, 23);
            this.showMoney.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.lue所属机构);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(820, 44);
            this.panelControl2.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(9, 16);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 14);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "所属机构：";
            // 
            // lue所属机构
            // 
            this.lue所属机构.Location = new System.Drawing.Point(72, 13);
            this.lue所属机构.Name = "lue所属机构";
            this.lue所属机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue所属机构.Properties.DropDownRows = 16;
            this.lue所属机构.Properties.NullText = "";
            this.lue所属机构.Properties.PopupSizeable = false;
            this.lue所属机构.Size = new System.Drawing.Size(165, 20);
            this.lue所属机构.TabIndex = 1;
            this.lue所属机构.EditValueChanged += new System.EventHandler(this.lue所属机构_EditValueChanged);
            // 
            // Frm协议书设置
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 482);
            this.Name = "Frm协议书设置";
            this.Text = "协议书设置";
            this.Load += new System.EventHandler(this.Frm协议书设置_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtP1RG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP1T.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSignerTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP2Contents.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpShowCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpShowRQ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpP2Showds.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP2jiafang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgpP2ShowBH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP2Title.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtp3beizhu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtp3title.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtP4beizhu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtP4title.Properties)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.homePage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showMoney.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lue所属机构.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtP1RG;
        private DevExpress.XtraEditors.TextEdit txtP1T;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton sbtnSave;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton sbtnPreview;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lue所属机构;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.RadioGroup rgpP2ShowBH;
        private DevExpress.XtraEditors.TextEdit txtP2Title;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.RadioGroup rgpP2Showds;
        private DevExpress.XtraEditors.TextEdit txtP2jiafang;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.RadioGroup rgpShowRQ;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.RadioGroup rgpShowCap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton sbtnP2Preview;
        private DevExpress.XtraEditors.SimpleButton sbtnP2Save;
        private DevExpress.XtraEditors.MemoEdit txtP2Contents;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.SimpleButton sbtnP3Preview;
        private DevExpress.XtraEditors.SimpleButton sbtnP3Save;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.MemoEdit txtp3beizhu;
        private DevExpress.XtraEditors.TextEdit txtp3title;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton sbtnP4Preview;
        private DevExpress.XtraEditors.SimpleButton sbtnP4Save;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.MemoEdit txtP4beizhu;
        private DevExpress.XtraEditors.TextEdit txtP4title;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton otherSave;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.RadioGroup homePage;
        private DevExpress.XtraEditors.RadioGroup paperSize;
        private DevExpress.XtraEditors.RadioGroup showMoney;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSPID;
        private DevExpress.XtraGrid.Columns.GridColumn gcolName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.SimpleButton sbtnSPRefresh;
        private DevExpress.XtraEditors.SimpleButton sbtnSPEdit;
        private DevExpress.XtraEditors.SimpleButton sbtnGlobalPreview;
        private DevExpress.XtraEditors.TextEdit txtSignerTitle;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;


    }
}