﻿using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class Frm协议书设置 : AtomEHR.Library.frmBaseBusinessForm
    {
        bllJTYSPage2 bllpage2 = new bllJTYSPage2();
        public Frm协议书设置()
        {
            InitializeComponent();
        }

        #region page1 设置预览
        private void sbtnSave_Click(object sender, EventArgs e)
        {
            if(lue所属机构.EditValue==null || string.IsNullOrWhiteSpace(lue所属机构.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择所属机构。");
                return;
            }
            if(string.IsNullOrWhiteSpace(txtP1T.Text) || string.IsNullOrWhiteSpace(txtP1RG.Text))
            {
                Msg.ShowInformation("标题、单位名称不允许是空的。");
                return;
            }

            //if (bllpage1.CurrentBusiness.Tables[0].Rows.Count > 0
            //    && bllpage1.CurrentBusiness.Tables[0].Rows[0].RowState != DataRowState.Modified)
            //{
            //    return;
            //}
            //else 
                if (bllpage1.CurrentBusiness.Tables[0].Rows.Count > 0)
            {
                bllpage1.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage1.updateuser] = Loginer.CurrentUser.用户编码;
                bllpage1.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage1.updatetime] = bllpage1.ServiceDateTime;
            }
            else
            {
                bllpage1.NewBusiness();
                bllpage1.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage1.所属机构] = lue所属机构.EditValue.ToString();
            }

            bllpage1.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage1.title] = txtP1T.Text.Trim();
            bllpage1.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage1.hosp] = txtP1RG.Text.Trim();

            DataRowState rowState = bllpage1.CurrentBusiness.Tables[0].Rows[0].RowState;

            SaveResult result = bllpage1.Save(bllpage1.CurrentBusiness);
            if (result.Success)
            {
                Msg.ShowInformation("保存成功");

                if(rowState==DataRowState.Added)
                {
                    bllpage1.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
                }
            }
        }

        private void sbtnPreview_Click(object sender, EventArgs e)
        {
            DataSet ds = bllpage1.GetBusinessByRGID(lue所属机构.EditValue.ToString(), false);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                XtraReportP1 rep = new XtraReportP1(ds.Tables[0]);
                DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(rep);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("请先设置标题、单位名称。");
            }
        }
        #endregion

        private void Frm协议书设置_Load(object sender, EventArgs e)
        {
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            lue所属机构.Properties.ValueMember = "机构编号";
            lue所属机构.Properties.DisplayMember = "机构名称";
            lue所属机构.Properties.DataSource = dt所属机构;
            lue所属机构.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称"));


            string ssxz = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
            if (string.IsNullOrWhiteSpace(ssxz) || ssxz.Length <=6)
            {

            }
            else
            {
                lue所属机构.Enabled = false;
                lue所属机构.EditValue = ssxz;
            }
        }

        bllJTYSPage1 bllpage1 = new bllJTYSPage1();
        bllJTYSPage3 bllpage3 = new bllJTYSPage3();
        bllJTYSPage4 bllpage4 = new bllJTYSPage4();
        bllJTYSPageOtherProperty bllOtherProperty = new bllJTYSPageOtherProperty();

        private void lue所属机构_EditValueChanged(object sender, EventArgs e)
        {
            #region 加载page1设置
            bllpage1.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
            if (bllpage1.CurrentBusiness.Tables.Count > 0 && bllpage1.CurrentBusiness.Tables[0].Rows.Count == 0)
            {
                this.txtP1T.Text ="";
                this.txtP1RG.Text = "";
            }
            else if (bllpage1.CurrentBusiness.Tables.Count > 0 && bllpage1.CurrentBusiness.Tables[0].Rows.Count > 0)
            {
                this.txtP1T.Text = bllpage1.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage1.title].ToString();
                this.txtP1RG.Text = bllpage1.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage1.hosp].ToString();
            }

            //this.txtP1T.DataBindings.Add("Text", bllpage1.CurrentBusiness, tb_JTYSPage1.title);
            //this.txtP1RG.DataBindings.Add("Text", bllpage1.CurrentBusiness, tb_JTYSPage1.hosp);
            #endregion

            #region 加载page2设置
            bllpage2.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
            if (bllpage2.CurrentBusiness.Tables.Count > 0 && bllpage2.CurrentBusiness.Tables[0].Rows.Count == 0)
            {
                this.txtP2Title.Text = "";
                this.rgpP2ShowBH.EditValue ="";
                this.txtP2jiafang.Text = "";
                this.rgpP2Showds.EditValue = "";
                this.rgpShowRQ.EditValue = "";
                this.rgpShowCap.EditValue = "";
                this.txtP2Contents.Text = "";
                this.txtSignerTitle.Text = "";
            }
            else if (bllpage2.CurrentBusiness.Tables.Count > 0 && bllpage2.CurrentBusiness.Tables[0].Rows.Count > 0)
            {
                this.txtP2Title.Text = bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.title].ToString();
                this.rgpP2ShowBH.EditValue = bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.showno].ToString();
                this.txtP2jiafang.Text = bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.hosp].ToString();
                this.rgpP2Showds.EditValue = bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.notype].ToString();
                this.rgpShowRQ.EditValue = bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.showSerObj].ToString();
                this.rgpShowCap.EditValue = bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.showCap].ToString();
                this.txtP2Contents.Text = bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.contents].ToString();
                this.txtSignerTitle.Text = bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.signerTitle].ToString();
            }
            #endregion
			
			 #region 加载page3设置
            bllpage3.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
            if (bllpage3.CurrentBusiness.Tables.Count > 0 && bllpage3.CurrentBusiness.Tables[0].Rows.Count == 0)
            {
                this.txtp3title.Text = "";
                this.txtp3beizhu.Text = "";
            }
            else if (bllpage3.CurrentBusiness.Tables.Count > 0 && bllpage3.CurrentBusiness.Tables[0].Rows.Count > 0)
            {
                this.txtp3title.Text = bllpage3.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage3.title].ToString();
                this.txtp3beizhu.Text = bllpage3.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage3.tail].ToString();
            }
            #endregion

            #region 加载page4设置
            bllpage4.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
            if (bllpage4.CurrentBusiness.Tables.Count > 0 && bllpage4.CurrentBusiness.Tables[0].Rows.Count == 0)
            {
                this.txtP4title.Text = "";
                this.txtP4beizhu.Text = "";
            }
            else if (bllpage4.CurrentBusiness.Tables.Count > 0 && bllpage4.CurrentBusiness.Tables[0].Rows.Count > 0)
            {
                this.txtP4title.Text = bllpage4.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage4.title].ToString();
                this.txtP4beizhu.Text = bllpage4.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage4.tail].ToString();
            }
            #endregion

            #region  JTYSPageOtherProperty
            bllOtherProperty.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
            if (bllOtherProperty.CurrentBusiness.Tables.Count > 0 && bllOtherProperty.CurrentBusiness.Tables[0].Rows.Count == 0)
            {
                this.showMoney.EditValue = "";
                this.paperSize.EditValue = "";
                this.homePage.EditValue = "";
            }
            else if (bllOtherProperty.CurrentBusiness.Tables.Count > 0 && bllOtherProperty.CurrentBusiness.Tables[0].Rows.Count > 0)
            {
                this.showMoney.EditValue = bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.SPShowJMJE].ToString();
                this.paperSize.EditValue = bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.PaperSize].ToString();
                this.homePage.EditValue = bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.showpage1].ToString();
            }
            #endregion

            #region pageSP
            if (lue所属机构.EditValue != null && !string.IsNullOrWhiteSpace(lue所属机构.EditValue.ToString()))
            {
                BindSPPrintInfo(lue所属机构.EditValue.ToString());
            }
            #endregion
        }
        
        #region p2设置预览
        private void sbtnP2Save_Click(object sender, EventArgs e)
        {
            if (lue所属机构.EditValue == null || string.IsNullOrWhiteSpace(lue所属机构.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择所属机构。");
                return;
            }
            if (string.IsNullOrWhiteSpace(txtP1T.Text) || string.IsNullOrWhiteSpace(txtP1RG.Text))
            {
                Msg.ShowInformation("标题、单位名称不允许是空的。");
                return;
            }

            //if (bllpage2.CurrentBusiness.Tables[0].Rows.Count > 0
            //    && bllpage2.CurrentBusiness.Tables[0].Rows[0].RowState != DataRowState.Modified)
            //{
            //    return;
            //}
            //else 
                if (bllpage2.CurrentBusiness.Tables[0].Rows.Count > 0
                //&& bllpage2.CurrentBusiness.Tables[0].Rows[0].RowState == DataRowState.Modified
                    )
            {
                bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.updateuser] = Loginer.CurrentUser.用户编码;
                bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.updatetime] = bllpage2.ServiceDateTime;
            }
            else
            {
                bllpage2.NewBusiness();
                bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.所属机构] = lue所属机构.EditValue.ToString();
            }

            bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.title] = this.txtP2Title.Text;
            bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.showno] = this.rgpP2ShowBH.EditValue.ToString();
            bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.hosp] = this.txtP2jiafang.Text;
            bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.notype] = this.rgpP2Showds.EditValue.ToString();
            bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.showSerObj] = this.rgpShowRQ.EditValue.ToString();
            bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.showCap] = this.rgpShowCap.EditValue.ToString();
            bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.contents] = this.txtP2Contents.Text;
            bllpage2.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage2.signerTitle] = this.txtSignerTitle.Text.Trim();

            DataRowState rowState = bllpage2.CurrentBusiness.Tables[0].Rows[0].RowState;
           
            SaveResult result = bllpage2.Save(bllpage2.CurrentBusiness);
            if (result.Success)
            {
                Msg.ShowInformation("保存成功");

                if(rowState== DataRowState.Added)
                {
                    bllpage2.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
                }
            }
        }

        bllJTYS团队概要 bll团队 = new bllJTYS团队概要();
        bllJTYS签约对象类型 bll对象类型 = new bllJTYS签约对象类型();

        private void sbtnP2Preview_Click(object sender, EventArgs e)
        {
            DataSet ds = bllpage2.GetBusinessByRGID(lue所属机构.EditValue.ToString(), false);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                DataSet dsTeamCapAndMemInfo = bll团队.GetTeamCapNameAndTelByXZ(lue所属机构.EditValue.ToString());
                string zzlb = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长类别"].ToString();
                string zzbh = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长编号"].ToString();
                string capID = "";
                DataRow[] drs = dsTeamCapAndMemInfo.Tables[1].Select("成员类别='" + zzlb + "' and 成员ID='" + zzbh + "'");
                if(drs.Length >0)
                {
                    capID=drs[0]["ID"].ToString();
                }

                DataTable dtSerObj = bll对象类型.GetAllValidateData();
                string strSerObj = "";
                for(int index = 0; index< dtSerObj.Rows.Count; index++)
                {
                    strSerObj += dtSerObj.Rows[index][tb_JTYS签约对象类型.类型名称] + "☐    ";
                }

                XtraReportP2 rep = new XtraReportP2(ds.Tables[0], dsTeamCapAndMemInfo.Tables[1], capID, "", strSerObj,"", "","","","","","","");
                DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(rep);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("请先设置标题、单位名称。");
            }
        }
        #endregion
		
        #region page3 设置预览
        private void sbtnP3Save_Click(object sender, EventArgs e)
        {
            if (lue所属机构.EditValue == null || string.IsNullOrWhiteSpace(lue所属机构.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择所属机构。");
                return;
            }
            //if (string.IsNullOrWhiteSpace(txtp3title.Text) || string.IsNullOrWhiteSpace(txtp3beizhu.Text))
            //{
            //    Msg.ShowInformation("标题、备注不允许是空的。");
            //    return;
            //}

            //if (bllpage3.CurrentBusiness.Tables[0].Rows.Count > 0
            //    && bllpage3.CurrentBusiness.Tables[0].Rows[0].RowState != DataRowState.Modified)
            //{
            //    return;
            //}
            //else 
                if (bllpage3.CurrentBusiness.Tables[0].Rows.Count > 0
                //&& bllpage3.CurrentBusiness.Tables[0].Rows[0].RowState == DataRowState.Modified
                    )
            {
                bllpage3.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage3.updateuser] = Loginer.CurrentUser.用户编码;
                bllpage3.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage3.updatetime] = bllpage1.ServiceDateTime;
            }
            else
            {
                bllpage3.NewBusiness();
                bllpage3.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage3.所属机构] = lue所属机构.EditValue.ToString();
            }

            bllpage3.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage3.title] = txtp3title.Text.Trim();
            bllpage3.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage3.tail] = txtp3beizhu.Text.Trim();

            DataRowState rowState = bllpage3.CurrentBusiness.Tables[0].Rows[0].RowState;

            SaveResult result = bllpage3.Save(bllpage3.CurrentBusiness);
            if (result.Success)
            {
                Msg.ShowInformation("保存成功");

                if(rowState == DataRowState.Added)
                {
                    bllpage3.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
                }
            }
        }

        private void sbtnP3Preview_Click(object sender, EventArgs e)
        {
            DataSet ds = bllpage3.GetBusinessByRGID(lue所属机构.EditValue.ToString(), false);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                XtraReportP3 rep = new XtraReportP3(null, true, ds.Tables[0].Rows[0][tb_JTYSPage3.title].ToString(), ds.Tables[0].Rows[0][tb_JTYSPage3.tail].ToString());
                DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(rep);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("请先设置标题、备注。");
            }
        }
        #endregion

        #region page4设置预览
        private void sbtnP4Save_Click(object sender, EventArgs e)
        {
            if (lue所属机构.EditValue == null || string.IsNullOrWhiteSpace(lue所属机构.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择所属机构。");
                return;
            }
            //if (string.IsNullOrWhiteSpace(txtP4title.Text) || string.IsNullOrWhiteSpace(txtP4beizhu.Text))
            //{
            //    Msg.ShowInformation("标题、备注不允许是空的。");
            //    return;
            //}

            //if (bllpage4.CurrentBusiness.Tables[0].Rows.Count > 0
            //    && bllpage4.CurrentBusiness.Tables[0].Rows[0].RowState != DataRowState.Modified)
            //{
            //    return;
            //}
            //else 
                if (bllpage4.CurrentBusiness.Tables[0].Rows.Count > 0
                //&& bllpage4.CurrentBusiness.Tables[0].Rows[0].RowState == DataRowState.Modified
                    )
            {
                bllpage4.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage4.updateuser] = Loginer.CurrentUser.用户编码;
                bllpage4.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage4.updatetime] = bllpage1.ServiceDateTime;
            }
            else
            {
                bllpage4.NewBusiness();
                bllpage4.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage4.所属机构] = lue所属机构.EditValue.ToString();
            }

            bllpage4.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage4.title] = txtp3title.Text.Trim();
            bllpage4.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPage4.tail] = txtp3beizhu.Text.Trim();

            DataRowState rowState = bllpage4.CurrentBusiness.Tables[0].Rows[0].RowState;

            SaveResult result = bllpage4.Save(bllpage4.CurrentBusiness);
            if (result.Success)
            {
                Msg.ShowInformation("保存成功");
                if(rowState == DataRowState.Added)
                {
                    bllpage4.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
                }
            }
        }
        private void sbtnP4Preview_Click(object sender, EventArgs e)
        {
            DataSet ds = bllpage4.GetBusinessByRGID(lue所属机构.EditValue.ToString(), false);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                XtraReportP3 rep = new XtraReportP3(null, true, ds.Tables[0].Rows[0][tb_JTYSPage4.title].ToString(), ds.Tables[0].Rows[0][tb_JTYSPage4.tail].ToString());
                DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(rep);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("请先设置标题、备注。");
            }
        }
        #endregion

        #region 其他信息设置
        private void otherSave_Click(object sender, EventArgs e)
        {
            if (lue所属机构.EditValue == null || string.IsNullOrWhiteSpace(lue所属机构.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择所属机构。");
                return;
            }
            if (string.IsNullOrWhiteSpace(showMoney.Text) || string.IsNullOrWhiteSpace(paperSize.Text)|| string.IsNullOrWhiteSpace(homePage.Text))
            {
                Msg.ShowInformation("请确认全部选择。");
                return;
            }


            //if (bllOtherProperty.CurrentBusiness.Tables[0].Rows.Count > 0
            //    && bllOtherProperty.CurrentBusiness.Tables[0].Rows[0].RowState != DataRowState.Modified)
            //{
            //    return;
            //}
            //else 
                if (bllOtherProperty.CurrentBusiness.Tables[0].Rows.Count > 0)
            {
                bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.updateuser] = Loginer.CurrentUser.用户编码;
                bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.updatetime] = bllOtherProperty.ServiceDateTime;
            }
            else
            {
                bllOtherProperty.NewBusiness();
                bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.所属机构] = lue所属机构.EditValue.ToString();
            }

            bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.SPShowJMJE] = showMoney.Text.Trim();
            bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.PaperSize] = paperSize.Text.Trim();
            bllOtherProperty.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageOtherProperty.showpage1] = homePage.Text.Trim();

            DataRowState rowState = bllOtherProperty.CurrentBusiness.Tables[0].Rows[0].RowState;

            SaveResult result = bllOtherProperty.Save(bllOtherProperty.CurrentBusiness);
            if (result.Success)
            {
                Msg.ShowInformation("保存成功");

                if (rowState == DataRowState.Added)
                {
                    bllOtherProperty.GetBusinessByRGID(lue所属机构.EditValue.ToString(), true);
                }
                //bllOtherProperty.CurrentBusiness.Tables[0].AcceptChanges();
            }

        }
        #endregion


        #region pagesp

        void BindSPPrintInfo(string rgid)
        {
            DataTable dtsp = bllsp.GetAllValidateSP(rgid);
            this.gridControl1.DataSource = dtsp;
            this.gridView1.BestFitColumns();
        }

        bllJTYSPageSP bllsp = new bllJTYSPageSP();

        private void sbtnSPRefresh_Click(object sender, EventArgs e)
        {
            if (lue所属机构.EditValue != null && !string.IsNullOrWhiteSpace(lue所属机构.EditValue.ToString()))
            {
                BindSPPrintInfo(lue所属机构.EditValue.ToString());
            }
        }

        private void sbtnSPEdit_Click(object sender, EventArgs e)
        {
            int[] selectedindexs = gridView1.GetSelectedRows();
            if(selectedindexs.Length==0)
            {
                Msg.ShowInformation("请选择一个服务包");
                return;
            }
            else if (selectedindexs.Length >1)
            {
                Msg.ShowInformation("请只选择一个服务包");
                return;
            }

            string spid = gridView1.GetRowCellValue(selectedindexs[0], gcolSPID).ToString();
            string spname = gridView1.GetRowCellValue(selectedindexs[0],gcolName).ToString();

            FrmSPEdit frm = new FrmSPEdit(spid, spname);
            frm.ShowDialog();
        }
        #endregion

        private void sbtnGlobalPreview_Click(object sender, EventArgs e)
        {
            try
            {
                string rgid = lue所属机构.EditValue.ToString();
                DataSet dspage1 = bllpage1.GetBusinessByRGID(rgid, false);
                DataSet dspage2 = bllpage2.GetBusinessByRGID(rgid, false);
                

                DataSet dsSPpage3Temp = bllsp.GetBusinessGroupByName1(rgid, 3);
                DataSet dsSppage3 = new DataSet();
                if(dsSPpage3Temp.Tables.Count ==2)
                {
                    for(int index = 0; index< dsSPpage3Temp.Tables[0].Rows.Count; index++)
                    {
                        DataRow[] drs = dsSPpage3Temp.Tables[1].Select("name1='" + dsSPpage3Temp.Tables[0].Rows[index]["name1"].ToString()+ "'");
                        if(drs.Length >0)
                        {
                            DataTable dtTemp = drs.CopyToDataTable();
                            dsSppage3.Tables.Add(dtTemp);
                        }
                    }
                }

                DataSet dsSPpage4Temp = bllsp.GetBusinessGroupByName1(rgid, 4);
                DataSet dsSppage4 = new DataSet();
                if (dsSPpage4Temp.Tables.Count == 2)
                {
                    for (int index = 0; index < dsSPpage4Temp.Tables[0].Rows.Count; index++)
                    {
                        DataRow[] drs = dsSPpage4Temp.Tables[1].Select("name1='" + dsSPpage4Temp.Tables[0].Rows[index]["name1"].ToString() + "'");
                        if (drs.Length > 0)
                        {
                            DataTable dtTemp = drs.CopyToDataTable();
                            dsSppage4.Tables.Add(dtTemp);
                        }
                    }
                }

                DataSet dsTeamCapAndMemInfo = bll团队.GetTeamCapNameAndTelByXZ(rgid);
                string zzlb = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长类别"].ToString();
                string zzbh = dsTeamCapAndMemInfo.Tables[0].Rows[0]["组长编号"].ToString();
                string capID = "";
                DataRow[] drsCap = dsTeamCapAndMemInfo.Tables[1].Select("成员类别='" + zzlb + "' and 成员ID='" + zzbh + "'");
                if (drsCap.Length > 0)
                {
                    capID = drsCap[0]["ID"].ToString();
                }

                DataSet dspage3 = bllpage3.GetBusinessByRGID(rgid, false);
                DataSet dspage4 = bllpage4.GetBusinessByRGID(rgid, false);
                DataSet dspageother = bllOtherProperty.GetBusinessByRGID(rgid, false);
                DataTable dsRQList = bll对象类型.GetAllValidateData();

                ReportComm rep = new ReportComm(dspage1.Tables[0], dspage2.Tables[0], dsSppage3, dsSppage4, dsRQList, dsTeamCapAndMemInfo.Tables[1], 
                    dspageother.Tables[0], dspage3.Tables[0], dspage4.Tables[0], capID, "","","","","","","","","","");
                rep.ShowPreviewDialog();
            }
            catch(Exception ex)
            {
                Msg.ShowInformation("预览按过程中出现异常，异常信息："+ex.Message);
            }

        }

    }
}
