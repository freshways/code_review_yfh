﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using DevExpress.XtraRichEdit;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class XtraReportP3 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReportP3(DataSet dsSP, bool isShowyouhui, string title, string tail)
        {
            InitializeComponent();

            if(isShowyouhui)
            { }
            else
            {
                xrTableCell17.WidthF += xrTableCell20.WidthF;
                //xrTableCell25.WidthF -= xrTableCell20.WidthF;
                xrLabel8.WidthF -= xrTableCell20.WidthF;
                xrTable4.WidthF -= xrTableCell20.WidthF;
                //xrTableCell20.WidthF = 0;
                xrTableCell26.WidthF = xrTableCell26.WidthF;
                xrTableRow8.Cells.Remove(xrTableCell20);

            }

            xrLabel1.Text = title;
            m_strRemark = tail;

            if(dsSP!=null)
            {
                for(int index=dsSP.Tables.Count-1; index>=0; index--)
                {
                    AddDetailReportBand(dsSP.Tables[index], index, isShowyouhui);
                }
            }
        }

        void AddDetailReportBand(DataTable dtSource, int index, bool isShowyouhui)
        {
            DevExpress.XtraReports.UI.DetailReportBand deRepSP;
            DevExpress.XtraReports.UI.DetailBand detSP;
            DevExpress.XtraReports.UI.XRTable xrTableSP;
            DevExpress.XtraReports.UI.XRTableRow xrtrSP;
            DevExpress.XtraReports.UI.XRTableCell xrtcSP1;
            DevExpress.XtraReports.UI.XRTableCell xrtcSP5;
            DevExpress.XtraReports.UI.XRTableCell xrtcSP6;

            DevExpress.XtraReports.UI.XRTableCell xrtcSP4;
            DevExpress.XtraReports.UI.XRTableCell xrtcSP7;
            DevExpress.XtraReports.UI.XRTableCell xrtcSP8;
            DevExpress.XtraReports.UI.XRTableCell xrtcSP9;
            DevExpress.XtraReports.UI.XRTableCell xrtcSP2;
            DevExpress.XtraReports.UI.XRTableCell xrtcSP3;

            xrTableSP = new DevExpress.XtraReports.UI.XRTable();
            xrtrSP = new DevExpress.XtraReports.UI.XRTableRow();
            xrtcSP1 = new DevExpress.XtraReports.UI.XRTableCell();
            xrtcSP2 = new DevExpress.XtraReports.UI.XRTableCell();
            xrtcSP3 = new DevExpress.XtraReports.UI.XRTableCell();
            xrtcSP4 = new DevExpress.XtraReports.UI.XRTableCell();
            xrtcSP5 = new DevExpress.XtraReports.UI.XRTableCell();
            xrtcSP6 = new DevExpress.XtraReports.UI.XRTableCell();
            xrtcSP7 = new DevExpress.XtraReports.UI.XRTableCell();
            xrtcSP8 = new DevExpress.XtraReports.UI.XRTableCell();
            xrtcSP9 = new DevExpress.XtraReports.UI.XRTableCell();
            deRepSP = new DevExpress.XtraReports.UI.DetailReportBand();
            detSP = new DevExpress.XtraReports.UI.DetailBand();

            bool isShowSecondName = true;
            if(dtSource!=null && dtSource.Rows.Count >0)
            {
                if(string.IsNullOrWhiteSpace(dtSource.Rows[0]["name2"].ToString()))
                {
                    isShowSecondName = false;
                }
            }
            // xrTableSP
            // 
            xrTableSP.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            xrTableSP.Dpi = 254F;
            xrTableSP.Font = new System.Drawing.Font("宋体", 8F);
            xrTableSP.LocationFloat = new DevExpress.Utils.PointFloat(94.70833F, 0F);
            xrTableSP.Name = "xrTableSP" + index.ToString();
            xrTableSP.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            xrtrSP});
            xrTableSP.SizeF = new System.Drawing.SizeF(1893.079F, 63.5F);
            xrTableSP.StylePriority.UseBorders = false;
            xrTableSP.StylePriority.UseFont = false;
            xrTableSP.StylePriority.UseTextAlignment = false;
            xrTableSP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrtrSP
            // 
            XRTableCell[] cells;
            if(isShowSecondName)
            {
                if(isShowyouhui)
                {
                    cells = new DevExpress.XtraReports.UI.XRTableCell[] {
            xrtcSP1,
            xrtcSP2,
            xrtcSP3,
            xrtcSP4,
            xrtcSP5,
            xrtcSP6,
            xrtcSP7,
            xrtcSP8,
            xrtcSP9};
                }
                else
                {
                    cells = new DevExpress.XtraReports.UI.XRTableCell[] {
            xrtcSP1,
            xrtcSP2,
            xrtcSP3,
            xrtcSP4,
            xrtcSP5,
            xrtcSP6,
            xrtcSP8,
            xrtcSP9};
                }
            }
            else
            {
                if(isShowyouhui)
                {
                    cells = new DevExpress.XtraReports.UI.XRTableCell[] {
            xrtcSP1,
            xrtcSP3,
            xrtcSP4,
            xrtcSP5,
            xrtcSP6,
            xrtcSP7,
            xrtcSP8,
            xrtcSP9};
                }
                else
                {
                    cells = new DevExpress.XtraReports.UI.XRTableCell[] {
            xrtcSP1,
            xrtcSP3,
            xrtcSP4,
            xrtcSP5,
            xrtcSP6,
            xrtcSP8,
            xrtcSP9};
                }
            }
            xrtrSP.Cells.AddRange(cells);
            xrtrSP.Dpi = 254F;
            xrtrSP.Name = "xrtrSP" + index.ToString();
            xrtrSP.Weight = 1D;
            // 
            // xrtcSP1
            // 
            xrtcSP1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            xrtcSP1.CanGrow = false;
            xrtcSP1.Multiline = true;
            xrtcSP1.Dpi = 254F;
            xrtcSP1.Name = "xrtcSP1" + index.ToString();
            xrtcSP1.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.MergeByValue;
            xrtcSP1.StylePriority.UseBorders = false;
            xrtcSP1.StylePriority.UseTextAlignment = false;
            xrtcSP1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            xrtcSP1.Weight = 0.11755977156783501D;
            // 
            // xrtcSP2
            // 
            xrtcSP2.CanGrow = false;
            xrtcSP2.Multiline = true;
            xrtcSP2.Dpi = 254F;
            xrtcSP2.Name = "xrtcSP2" + index.ToString();
            xrtcSP2.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.MergeByValue;
            xrtcSP2.Weight = 0.13433135649068664D;

            if (isShowSecondName)
            {
            }
            else
            {
                xrtcSP1.Weight += xrtcSP2.Weight;
                //xrtcSP2.Weight = 0.00000000000000001D;
                xrtcSP2.Visible = false;
            }
            // 
            // xrtcSP3
            // 
            xrtcSP3.Dpi = 254F;
            xrtcSP3.Name = "xrtcSP3" + index.ToString();
            xrtcSP3.Weight = 0.1512615217977954D;
            // 
            // xrtcSP4
            // 
            xrtcSP4.Dpi = 254F;
            xrtcSP4.Name = "xrtcSP4" + index.ToString();
            xrtcSP4.Weight = 0.33724532268771718D;
            // 
            // xrtcSP5
            // 
            xrtcSP5.Dpi = 254F;
            xrtcSP5.Name = "xrtcSP5" + index.ToString();
            xrtcSP5.StylePriority.UseTextAlignment = false;
            xrtcSP5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            xrtcSP5.Weight = 1.4438447508576968D;

            xrtcSP5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, xrtcSP5.Dpi);
            xrtcSP5.StylePriority.UsePadding = false;
            // 
            // xrtcSP6
            // 
            xrtcSP6.Dpi = 254F;
            xrtcSP6.Name = "xrtcSP6" + index.ToString();
            xrtcSP6.Weight = 0.1985223130098745D;
            // 
            // xrtcSP7
            // 
            xrtcSP7.Dpi = 254F;
            xrtcSP7.Name = "xrtcSP7" + index.ToString();
            xrtcSP7.Weight = 0.20129751104341892D;
            // 
            // xrtcSP8
            // 
            xrtcSP8.Dpi = 254F;
            xrtcSP8.Name = "xrtcSP8" + index.ToString();
            xrtcSP8.Weight = 0.19715077615866028D;

            if(isShowyouhui)
            {
            }
            else
            {
                xrtcSP5.Weight += xrtcSP7.Weight;
            }
            // 
            // xrtcSP9
            // 
            xrtcSP9.Dpi = 254F;
            xrtcSP9.Name = "xrtcSP9" + index.ToString();
            xrtcSP9.Weight = 0.21878667638631535D;
            xrtcSP9.Font = new System.Drawing.Font("宋体", 12F);
            // 
            // deRepSP
            // 
            deRepSP.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            detSP});
            deRepSP.Dpi = 254F;
            deRepSP.Level = 0;
            deRepSP.Name = "deRepSP" + index.ToString();
            // 
            // detSP
            // 
            detSP.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            xrTableSP});
            detSP.Dpi = 254F;
            detSP.HeightF = 63.5F;
            detSP.Name = "detSP" + index.ToString();
            // 
            // XtraReport1
            // 
            Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            deRepSP
            });

            xrtcSP1.DataBindings.Add("Text", null, "name1");
            if (isShowSecondName)
            {
                xrtcSP2.DataBindings.Add("Text", null, "name2");
            }
            xrtcSP3.DataBindings.Add("Text", null, "name3");
            xrtcSP4.DataBindings.Add("Text", null, "Serobj");
            xrtcSP5.DataBindings.Add("Text",null, "contents");
            xrtcSP6.DataBindings.Add("Text", null, "total");
            if (isShowyouhui)
            {
                xrtcSP7.DataBindings.Add("Text", null, "youhui");
            }
            xrtcSP8.DataBindings.Add("Text", null, "actual");
            xrtcSP9.DataBindings.Add("Text", null, "selected");

            deRepSP.DataSource = dtSource;
        }

        string m_strRemark = string.Empty;
        private void xrRichText1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRRichText richText = sender as XRRichText;
            RichEditDocumentServer docServer = new RichEditDocumentServer();

            //docServer.LoadDocument(filePath);
            docServer.Text = m_strRemark;
            docServer.Document.DefaultParagraphProperties.LineSpacingType = DevExpress.XtraRichEdit.API.Native.ParagraphLineSpacing.Single;
            docServer.Document.DefaultParagraphProperties.LineSpacingMultiplier = 1.3f;

            richText.Text = docServer.RtfText;
        }
    }
}
