﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using AtomEHR.签约服务.Business;
using AtomEHR.Common;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class XtraReport履约记录单 : DevExpress.XtraReports.UI.XtraReport
    {
        bllJTYS履约执行明细 bll执行明细 = new bllJTYS履约执行明细();
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        string m_qyid,m_date;

        public XtraReport履约记录单(string qyid, string strDate,string tile)
        {
            InitializeComponent();
            m_qyid = qyid;
            m_date = strDate;
            this.xrLabel标题.Text = tile;
            Binding();
        }

        void Binding()
        {
            DataSet dsInfo = bll签约.GetBusinessByIdFromView(m_qyid);

            txt姓名.Text = dsInfo.Tables[0].Rows[0][AtomEHR.Models.tb_健康档案.姓名].ToString();
            txt性别.Text = dsInfo.Tables[0].Rows[0]["性别名称"].ToString();
            int age = 0;
            DateTime birthday = default(DateTime);
            if (DateTime.TryParse(dsInfo.Tables[0].Rows[0]["出生日期"].ToString(), out birthday))
            {
                DateTime ServerTime = DateTime.Parse(new AtomEHR.Business.bllCom().GetDateTimeFromDBServer());
                age = ServerTime.Year - birthday.Year;
            }
            txt年龄.Text = age.ToString();
            txt身份证号.Text = dsInfo.Tables[0].Rows[0][AtomEHR.Models.tb_健康档案.身份证号].ToString();
            txt住址.Text = dsInfo.Tables[0].Rows[0][AtomEHR.Models.tb_健康档案.居住地址].ToString();
            txt电话.Text = dsInfo.Tables[0].Rows[0]["联系电话"].ToString();
            txt履约时间.Text = m_date;
            txt团队.Text = dsInfo.Tables[0].Rows[0]["团队名称"].ToString();
            txt卫生室.Text = dsInfo.Tables[0].Rows[0]["所属机构名称"].ToString();
            txt签约对象.Text = dsInfo.Tables[0].Rows[0]["签约对象类型名称"].ToString();

            DataTable dtInfo = bll执行明细.Get执行明细ByQYIDofDate(m_qyid, m_date).Tables[0];
            if (dtInfo != null && dtInfo.Rows.Count >= 1)
            {
                txt履约科室.Text = dtInfo.Rows[0]["开单科室"].ToString();
                txt履约医生.Text = dtInfo.Rows[0]["服务医生姓名"].ToString();
                decimal zj = dtInfo.AsEnumerable().Where(a=>a.Field<object>("总价")!=null).Sum(x => x.Field<decimal>("总价"));
                txt合计.Text = "￥" + zj.ToString();
                txt大写.Text = RMBConverter.toRMB(zj);
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (DataRow dr in dtInfo.Rows)
                {
                    sb.Append(dr["服务包名称"].ToString() + ":" + dr["项目名称"].ToString() + "  " + dr["执行数量"].ToString() + "*" + dr["单价"].ToString() +"\n\r");
                }
                txt履约项目.Text = sb.ToString();
            }

        }

    }
}
