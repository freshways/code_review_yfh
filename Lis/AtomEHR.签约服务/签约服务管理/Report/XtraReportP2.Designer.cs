﻿namespace AtomEHR.签约服务.签约服务管理.Report
{
    partial class XtraReportP2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraReportP2));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrlSignerTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrP乙方手签 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrtcDoc0 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtcName0 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtcTel0 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtcTelNo0 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtcDoc1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtcName1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtcTel1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrtcTelNo1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRichText2 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrlteamcap = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlTeamCapTelText = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlteamcaptel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlTeamCapName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlMemHead = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlYFName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlYFsfzh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlYFTel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlHosp = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlYFdah = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlYFaddr = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPYSSQ = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1,
            this.xrRichText1,
            this.xrLabel1,
            this.xrTable1,
            this.xrlTitle,
            this.xrLabel2,
            this.xrLabel3,
            this.xrRichText2,
            this.xrlteamcap,
            this.xrlTeamCapTelText,
            this.xrlteamcaptel,
            this.xrlTeamCapName,
            this.xrlMemHead,
            this.xrLabel19,
            this.xrLabel4,
            this.xrLabel6,
            this.xrlYFName,
            this.xrlYFsfzh,
            this.xrLabel13,
            this.xrlYFTel,
            this.xrLabel11,
            this.xrlHosp,
            this.xrLabel5,
            this.xrlYFdah,
            this.xrLabel9,
            this.xrlYFaddr});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 2970F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPYSSQ,
            this.xrlSignerTitle,
            this.xrLabel47,
            this.xrLabel52,
            this.xrP乙方手签,
            this.xrLabel59});
            this.xrPanel1.Dpi = 254F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 1792F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1858.175F, 349.2385F);
            // 
            // xrlSignerTitle
            // 
            this.xrlSignerTitle.Dpi = 254F;
            this.xrlSignerTitle.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlSignerTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 150.6006F);
            this.xrlSignerTitle.Name = "xrlSignerTitle";
            this.xrlSignerTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlSignerTitle.SizeF = new System.Drawing.SizeF(504.8248F, 79.58667F);
            this.xrlSignerTitle.StylePriority.UseFont = false;
            this.xrlSignerTitle.StylePriority.UseTextAlignment = false;
            this.xrlSignerTitle.Text = "主要负责人（签字）:";
            this.xrlSignerTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Dpi = 254F;
            this.xrLabel47.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25.82352F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(306.9166F, 79.58667F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "甲方（盖章）:";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Dpi = 254F;
            this.xrLabel52.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(1034.936F, 25.82352F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(306.9166F, 79.58667F);
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "乙方（签字）:";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrP乙方手签
            // 
            this.xrP乙方手签.Dpi = 254F;
            this.xrP乙方手签.LocationFloat = new DevExpress.Utils.PointFloat(1353.609F, 0F);
            this.xrP乙方手签.Name = "xrP乙方手签";
            this.xrP乙方手签.SizeF = new System.Drawing.SizeF(443.3271F, 269.6519F);
            this.xrP乙方手签.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Dpi = 254F;
            this.xrLabel59.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(1166.417F, 269.652F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(444.5F, 79.58667F);
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "    年    月    日";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrRichText1
            // 
            this.xrRichText1.Dpi = 254F;
            this.xrRichText1.Font = new System.Drawing.Font("宋体", 11F);
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(329.0752F, 648.3943F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(1642.402F, 184.207F);
            this.xrRichText1.StylePriority.UseFont = false;
            this.xrRichText1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrRichText1_BeforePrint);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 621.936F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(209.55F, 79.58664F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "人群分类:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("宋体", 11F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(235.9421F, 970.0261F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1730F, 65F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrtcDoc0,
            this.xrtcName0,
            this.xrtcTel0,
            this.xrtcTelNo0,
            this.xrtcDoc1,
            this.xrtcName1,
            this.xrtcTel1,
            this.xrtcTelNo1});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrtcDoc0
            // 
            this.xrtcDoc0.Dpi = 254F;
            this.xrtcDoc0.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.xrtcDoc0.Name = "xrtcDoc0";
            this.xrtcDoc0.StylePriority.UseFont = false;
            this.xrtcDoc0.Text = " ";
            this.xrtcDoc0.Weight = 0.32784192829967429D;
            // 
            // xrtcName0
            // 
            this.xrtcName0.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrtcName0.CanGrow = false;
            this.xrtcName0.Dpi = 254F;
            this.xrtcName0.Font = new System.Drawing.Font("宋体", 11F);
            this.xrtcName0.Name = "xrtcName0";
            this.xrtcName0.StylePriority.UseBorders = false;
            this.xrtcName0.StylePriority.UseFont = false;
            this.xrtcName0.Weight = 0.34509677407199835D;
            // 
            // xrtcTel0
            // 
            this.xrtcTel0.Dpi = 254F;
            this.xrtcTel0.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.xrtcTel0.Name = "xrtcTel0";
            this.xrtcTel0.StylePriority.UseFont = false;
            this.xrtcTel0.Text = "联系电话:";
            this.xrtcTel0.Weight = 0.3450967716544755D;
            // 
            // xrtcTelNo0
            // 
            this.xrtcTelNo0.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrtcTelNo0.Dpi = 254F;
            this.xrtcTelNo0.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtcTelNo0.Name = "xrtcTelNo0";
            this.xrtcTelNo0.StylePriority.UseBorders = false;
            this.xrtcTelNo0.StylePriority.UseFont = false;
            this.xrtcTelNo0.Weight = 0.4831354932713976D;
            // 
            // xrtcDoc1
            // 
            this.xrtcDoc1.Dpi = 254F;
            this.xrtcDoc1.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.xrtcDoc1.Name = "xrtcDoc1";
            this.xrtcDoc1.StylePriority.UseFont = false;
            this.xrtcDoc1.Weight = 0.31058709048685085D;
            // 
            // xrtcName1
            // 
            this.xrtcName1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrtcName1.CanGrow = false;
            this.xrtcName1.Dpi = 254F;
            this.xrtcName1.Font = new System.Drawing.Font("宋体", 11F);
            this.xrtcName1.Name = "xrtcName1";
            this.xrtcName1.StylePriority.UseBorders = false;
            this.xrtcName1.StylePriority.UseFont = false;
            this.xrtcName1.Weight = 0.3450967569243828D;
            // 
            // xrtcTel1
            // 
            this.xrtcTel1.Dpi = 254F;
            this.xrtcTel1.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.xrtcTel1.Name = "xrtcTel1";
            this.xrtcTel1.StylePriority.UseFont = false;
            this.xrtcTel1.Text = "联系电话:";
            this.xrtcTel1.Weight = 0.34509676204924339D;
            // 
            // xrtcTelNo1
            // 
            this.xrtcTelNo1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrtcTelNo1.Dpi = 254F;
            this.xrtcTelNo1.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.xrtcTelNo1.Name = "xrtcTelNo1";
            this.xrtcTelNo1.StylePriority.UseBorders = false;
            this.xrtcTelNo1.StylePriority.UseFont = false;
            this.xrtcTelNo1.Weight = 0.4831354300750163D;
            // 
            // xrlTitle
            // 
            this.xrlTitle.Dpi = 254F;
            this.xrlTitle.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold);
            this.xrlTitle.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 210.6609F);
            this.xrlTitle.Name = "xrlTitle";
            this.xrlTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrlTitle.SizeF = new System.Drawing.SizeF(1834.362F, 130.3866F);
            this.xrlTitle.StylePriority.UseFont = false;
            this.xrlTitle.StylePriority.UseTextAlignment = false;
            this.xrlTitle.Text = "蒙阴县家庭医生签约服务协议书";
            this.xrlTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(1445.887F, 125.5708F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(235.4799F, 53.34005F);
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "协议编号：";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel3.BorderWidth = 1F;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1681.367F, 125.5708F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(272.5212F, 53.34003F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseBorderWidth = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrRichText2
            // 
            this.xrRichText2.Dpi = 254F;
            this.xrRichText2.Font = new System.Drawing.Font("宋体", 11F);
            this.xrRichText2.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 1054.094F);
            this.xrRichText2.Name = "xrRichText2";
            this.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString");
            this.xrRichText2.SizeF = new System.Drawing.SizeF(1858.175F, 737.1466F);
            this.xrRichText2.StylePriority.UseFont = false;
            this.xrRichText2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrRichText2_BeforePrint);
            this.xrRichText2.AfterPrint += new System.EventHandler(this.xrRichText2_AfterPrint);
            // 
            // xrlteamcap
            // 
            this.xrlteamcap.Dpi = 254F;
            this.xrlteamcap.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.xrlteamcap.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 895.0261F);
            this.xrlteamcap.Name = "xrlteamcap";
            this.xrlteamcap.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrlteamcap.SizeF = new System.Drawing.SizeF(116.4169F, 70F);
            this.xrlteamcap.StylePriority.UseFont = false;
            this.xrlteamcap.StylePriority.UseTextAlignment = false;
            this.xrlteamcap.Text = "队长:";
            this.xrlteamcap.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlTeamCapTelText
            // 
            this.xrlTeamCapTelText.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrlTeamCapTelText.Dpi = 254F;
            this.xrlTeamCapTelText.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlTeamCapTelText.LocationFloat = new DevExpress.Utils.PointFloat(756.1129F, 895.0256F);
            this.xrlTeamCapTelText.Name = "xrlTeamCapTelText";
            this.xrlTeamCapTelText.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlTeamCapTelText.SizeF = new System.Drawing.SizeF(318.0293F, 70.00043F);
            this.xrlTeamCapTelText.StylePriority.UseBorders = false;
            this.xrlTeamCapTelText.StylePriority.UseFont = false;
            this.xrlTeamCapTelText.StylePriority.UseTextAlignment = false;
            this.xrlTeamCapTelText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlteamcaptel
            // 
            this.xrlteamcaptel.Dpi = 254F;
            this.xrlteamcaptel.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.xrlteamcaptel.LocationFloat = new DevExpress.Utils.PointFloat(546.5627F, 895.0258F);
            this.xrlteamcaptel.Name = "xrlteamcaptel";
            this.xrlteamcaptel.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlteamcaptel.SizeF = new System.Drawing.SizeF(209.5502F, 70.00031F);
            this.xrlteamcaptel.StylePriority.UseFont = false;
            this.xrlteamcaptel.StylePriority.UseTextAlignment = false;
            this.xrlteamcaptel.Text = "联系电话:";
            this.xrlteamcaptel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlTeamCapName
            // 
            this.xrlTeamCapName.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrlTeamCapName.Dpi = 254F;
            this.xrlTeamCapName.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlTeamCapName.LocationFloat = new DevExpress.Utils.PointFloat(235.9421F, 895.0256F);
            this.xrlTeamCapName.Name = "xrlTeamCapName";
            this.xrlTeamCapName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlTeamCapName.SizeF = new System.Drawing.SizeF(310.6207F, 70.00037F);
            this.xrlTeamCapName.StylePriority.UseBorders = false;
            this.xrlTeamCapName.StylePriority.UseFont = false;
            this.xrlTeamCapName.StylePriority.UseTextAlignment = false;
            this.xrlTeamCapName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrlMemHead
            // 
            this.xrlMemHead.Dpi = 254F;
            this.xrlMemHead.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.xrlMemHead.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 965.0261F);
            this.xrlMemHead.Name = "xrlMemHead";
            this.xrlMemHead.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrlMemHead.SizeF = new System.Drawing.SizeF(116.4169F, 69.99994F);
            this.xrlMemHead.StylePriority.UseFont = false;
            this.xrlMemHead.StylePriority.UseTextAlignment = false;
            this.xrlMemHead.Text = "成员:";
            this.xrlMemHead.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 832.6013F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(1280.583F, 62.42444F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "甲乙双方共同确定下列人员为乙方的家庭医生签约服务团队。";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 383.1758F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(148.1667F, 79.58665F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "甲方：";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(621.1752F, 542.3495F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(270.4042F, 79.58658F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "健康档案号：";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlYFName
            // 
            this.xrlYFName.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrlYFName.Dpi = 254F;
            this.xrlYFName.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlYFName.LocationFloat = new DevExpress.Utils.PointFloat(267.6919F, 462.7625F);
            this.xrlYFName.Name = "xrlYFName";
            this.xrlYFName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlYFName.SizeF = new System.Drawing.SizeF(353.4834F, 79.58665F);
            this.xrlYFName.StylePriority.UseBorders = false;
            this.xrlYFName.StylePriority.UseFont = false;
            this.xrlYFName.StylePriority.UseTextAlignment = false;
            this.xrlYFName.Text = "测试人员";
            this.xrlYFName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrlYFsfzh
            // 
            this.xrlYFsfzh.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrlYFsfzh.Dpi = 254F;
            this.xrlYFsfzh.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlYFsfzh.LocationFloat = new DevExpress.Utils.PointFloat(891.5795F, 462.7626F);
            this.xrlYFsfzh.Name = "xrlYFsfzh";
            this.xrlYFsfzh.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlYFsfzh.SizeF = new System.Drawing.SizeF(433.3875F, 79.58664F);
            this.xrlYFsfzh.StylePriority.UseBorders = false;
            this.xrlYFsfzh.StylePriority.UseFont = false;
            this.xrlYFsfzh.StylePriority.UseTextAlignment = false;
            this.xrlYFsfzh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(621.1754F, 462.7626F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(270.4041F, 79.58664F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "身份证号:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlYFTel
            // 
            this.xrlYFTel.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrlYFTel.Dpi = 254F;
            this.xrlYFTel.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlYFTel.LocationFloat = new DevExpress.Utils.PointFloat(329.0752F, 542.3494F);
            this.xrlYFTel.Name = "xrlYFTel";
            this.xrlYFTel.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlYFTel.SizeF = new System.Drawing.SizeF(292.1F, 79.58667F);
            this.xrlYFTel.StylePriority.UseBorders = false;
            this.xrlYFTel.StylePriority.UseFont = false;
            this.xrlYFTel.StylePriority.UseTextAlignment = false;
            this.xrlYFTel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 542.3494F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(209.55F, 79.58664F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "联系电话:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlHosp
            // 
            this.xrlHosp.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlHosp.Dpi = 254F;
            this.xrlHosp.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlHosp.LocationFloat = new DevExpress.Utils.PointFloat(267.6919F, 383.1759F);
            this.xrlHosp.Name = "xrlHosp";
            this.xrlHosp.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrlHosp.SizeF = new System.Drawing.SizeF(903.8168F, 79.58661F);
            this.xrlHosp.StylePriority.UseBorders = false;
            this.xrlHosp.StylePriority.UseFont = false;
            this.xrlHosp.StylePriority.UseTextAlignment = false;
            this.xrlHosp.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(119.5252F, 462.7626F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(148.1666F, 79.58665F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "乙方：";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlYFdah
            // 
            this.xrlYFdah.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrlYFdah.Dpi = 254F;
            this.xrlYFdah.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlYFdah.LocationFloat = new DevExpress.Utils.PointFloat(891.5791F, 542.3494F);
            this.xrlYFdah.Name = "xrlYFdah";
            this.xrlYFdah.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlYFdah.SizeF = new System.Drawing.SizeF(433.3877F, 79.58667F);
            this.xrlYFdah.StylePriority.UseBorders = false;
            this.xrlYFdah.StylePriority.UseFont = false;
            this.xrlYFdah.StylePriority.UseTextAlignment = false;
            this.xrlYFdah.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(1324.967F, 542.3494F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(148.1667F, 79.58664F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "住址：";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlYFaddr
            // 
            this.xrlYFaddr.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrlYFaddr.Dpi = 254F;
            this.xrlYFaddr.Font = new System.Drawing.Font("宋体", 11F);
            this.xrlYFaddr.LocationFloat = new DevExpress.Utils.PointFloat(1473.134F, 542.3495F);
            this.xrlYFaddr.Name = "xrlYFaddr";
            this.xrlYFaddr.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrlYFaddr.SizeF = new System.Drawing.SizeF(492.808F, 79.58661F);
            this.xrlYFaddr.StylePriority.UseBorders = false;
            this.xrlYFaddr.StylePriority.UseFont = false;
            this.xrlYFaddr.StylePriority.UseTextAlignment = false;
            this.xrlYFaddr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPYSSQ
            // 
            this.xrPYSSQ.Dpi = 254F;
            this.xrPYSSQ.LocationFloat = new DevExpress.Utils.PointFloat(506.4169F, 0F);
            this.xrPYSSQ.Name = "xrPYSSQ";
            this.xrPYSSQ.SizeF = new System.Drawing.SizeF(443.3271F, 269.6519F);
            this.xrPYSSQ.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            // 
            // XtraReportP2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrlTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrlTeamCapTelText;
        private DevExpress.XtraReports.UI.XRLabel xrlteamcaptel;
        private DevExpress.XtraReports.UI.XRLabel xrlTeamCapName;
        private DevExpress.XtraReports.UI.XRLabel xrlMemHead;
        private DevExpress.XtraReports.UI.XRLabel xrlteamcap;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrlYFName;
        private DevExpress.XtraReports.UI.XRLabel xrlYFsfzh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrlYFTel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrlYFaddr;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrlHosp;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel xrlSignerTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrlYFdah;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRPictureBox xrP乙方手签;
        private DevExpress.XtraReports.UI.XRRichText xrRichText2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrtcName0;
        private DevExpress.XtraReports.UI.XRTableCell xrtcTel0;
        private DevExpress.XtraReports.UI.XRTableCell xrtcTelNo0;
        private DevExpress.XtraReports.UI.XRTableCell xrtcDoc1;
        private DevExpress.XtraReports.UI.XRTableCell xrtcName1;
        private DevExpress.XtraReports.UI.XRTableCell xrtcTel1;
        private DevExpress.XtraReports.UI.XRTableCell xrtcTelNo1;
        private DevExpress.XtraReports.UI.XRTableCell xrtcDoc0;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRRichText xrRichText1;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPYSSQ;
    }
}
