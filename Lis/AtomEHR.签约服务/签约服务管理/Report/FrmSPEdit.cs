﻿using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class FrmSPEdit : frmBase
    {
        string m_spid = string.Empty;
        public FrmSPEdit(string spid, string spname)
        {
            InitializeComponent();

            m_spid = spid;
            this.txtspname.Text = spname;
        }

        private void sbtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {
           
            if (string.IsNullOrWhiteSpace(meName1.Text) //|| string.IsNullOrWhiteSpace(meName2.Text)
                || string.IsNullOrWhiteSpace(meName3.Text) || string.IsNullOrWhiteSpace(cboePageNo.Text)
                || string.IsNullOrWhiteSpace(txtTotalPrice.Text) || string.IsNullOrWhiteSpace(txtOrder.Text))
            {
                Msg.ShowInformation("请确认所有信息都已录入。");
                return;
            }

            if (bllsp.CurrentBusiness.Tables[0].Rows.Count > 0
                && bllsp.CurrentBusiness.Tables[0].Rows[0].RowState == DataRowState.Modified)
            {
                bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.updateuser] = Loginer.CurrentUser.用户编码;
                bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.updatetime] = bllsp.ServiceDateTime;
            }
            else
            {
                bllsp.NewBusiness();
                bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.SPID] = m_spid;
            }

            bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.name1] = this.meName1.Text;
            bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.name2] = this.meName2.Text;
            bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.name3] = this.meName3.Text;
            bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.pageNo] = this.cboePageNo.Text;
            bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.orderby] = this.txtOrder.Text;
            bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.totalPrice] = this.txtTotalPrice.Text;

            SaveResult result = bllsp.Save(bllsp.CurrentBusiness);
            if (result.Success)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                Msg.ShowInformation("保存失败");
            }
        }

        bllJTYSPageSP bllsp = new bllJTYSPageSP();
        private void FrmSPEdit_Load(object sender, EventArgs e)
        {
            bllsp.GetBusinessBySPID(m_spid, true);
            if (bllsp.CurrentBusiness.Tables.Count > 0 && bllsp.CurrentBusiness.Tables[0].Rows.Count == 0)
            {
                
            }
            else if (bllsp.CurrentBusiness.Tables.Count > 0 && bllsp.CurrentBusiness.Tables[0].Rows.Count > 0)
            {
                this.meName1.Text = bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.name1].ToString();
                this.meName2.Text = bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.name2].ToString();
                this.meName3.Text = bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.name3].ToString();
                this.cboePageNo.Text = bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.pageNo].ToString();
                this.txtOrder.Text = bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.orderby].ToString();
                this.txtTotalPrice.Text = bllsp.CurrentBusiness.Tables[0].Rows[0][tb_JTYSPageSP.totalPrice].ToString();
            }
        }
    }
}
