﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class XtraReportA3 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReportA3(XtraReport leftRep, XtraReport rightRep)
        {
            InitializeComponent();


            //this.xrSubreport1.Report = new XtraReportP1(dt1);
            this.xrSubreport1.ReportSource = leftRep;
            //this.xrSubreport2.Report = new XtraReportP1(dt2);
            this.xrSubreport2.ReportSource = rightRep;
        }

        bool m_PrintState=false;
        private void XtraReportA3_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            m_PrintState = true;
        }
        public bool GetPrintState()
        {
            return m_PrintState;
        }

    }
}
