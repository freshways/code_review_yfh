﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    class ReportComm
    {
        XtraReportA4 repa4 = null;
        XtraReportA3 repa3 = null;
        private bool isNullRep = false;
        public ReportComm(DataTable dtPage1, DataTable dtPage2, DataSet dsSPPage3, DataSet dsSPPage4, 
                          DataTable dtRQlist, DataTable dtMember, DataTable dtOtherProperty, DataTable dtPage3, DataTable dtPage4,//string paperSize, bool ShowPage1, bool isShowJMJE,
                          string capID, string name, string tel, string sfzh, 
                          string dah, string addr, string qyrq, string shouqzhao,
                          string strSerObj,string qyrid, string yssqz
            )
        {
            if(dtOtherProperty==null|| dtOtherProperty.Rows.Count==0 ||
                ((dtOtherProperty.Rows[0][tb_JTYSPageOtherProperty.showpage1].ToString() == "1") && (dtPage1 == null || dtPage1.Rows.Count == 0))
                
                //ShowPage1 &&( dtPage1==null || dtPage1.Rows.Count==0)
                || dtPage2==null || dtPage2.Rows.Count==0
                || dsSPPage3==null //|| dsSPPage3.Tables.Count ==0
                //|| dsSPPage4==null || dsSPPage4.Tables.Count ==0
                //|| dtQYinfo==null || dtQYinfo.Rows.Count ==0
                || dtRQlist == null || dtRQlist.Rows.Count==0
                //|| dtOtherProperty==null|| dtOtherProperty.Rows.Count==0 
                )
            {
                repa4 = new XtraReportA4();
                isNullRep = true;
            }
            else
            {
                string paperSize = dtOtherProperty.Rows[0][tb_JTYSPageOtherProperty.PaperSize].ToString();
                string ShowPage1 = dtOtherProperty.Rows[0][tb_JTYSPageOtherProperty.showpage1].ToString();
                string isShowJMJE = dtOtherProperty.Rows[0][tb_JTYSPageOtherProperty.SPShowJMJE].ToString();

                XtraReport rep1 = null;
                if (ShowPage1=="1")
                {
                    //创建PAGE1
                    rep1 = new XtraReportP1(dtPage1);
                }

                //string[] arrSerObjs = strSerObj.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                string strqydx = "";
                
                for(int index = 0; index< dtRQlist.Rows.Count; index++)
                {
                    if ((strSerObj.Contains(",") && strSerObj.Contains(","+dtRQlist.Rows[index][tb_JTYS签约对象类型.ID].ToString()+","))
                        || strSerObj == dtRQlist.Rows[index][tb_JTYS签约对象类型.ID].ToString())
                    {
                        strqydx += dtRQlist.Rows[index][tb_JTYS签约对象类型.类型名称].ToString() + "■    ";
                    }
                    else
                    {
                        strqydx += dtRQlist.Rows[index][tb_JTYS签约对象类型.类型名称].ToString() + "□    ";
                    }
                }
                XtraReport rep2 = new XtraReportP2(dtPage2, dtMember, capID, qyrid, strqydx, name, sfzh, dah, tel, addr, shouqzhao, qyrq, yssqz);

                bool bIsShowJMJE = isShowJMJE=="1" ?true:false;
                XtraReport rep3 = new XtraReportP3(dsSPPage3, bIsShowJMJE, 
                    dtPage3.Rows[0][tb_JTYSPage3.title].ToString(),
                    dtPage3.Rows[0][tb_JTYSPage3.tail].ToString());

                XtraReport rep4 = null;
                if(dsSPPage4!=null && dsSPPage4.Tables.Count>0)
                {
                    rep4 = new XtraReportP3(dsSPPage4, bIsShowJMJE,
                        dtPage4.Rows[0][tb_JTYSPage4.title].ToString(),
                    dtPage4.Rows[0][tb_JTYSPage4.tail].ToString());
                }

                if(paperSize=="A3")
                {
                    if (ShowPage1 == "1")
                    {
                        XtraReportA3 tempa3 = new XtraReportA3(rep2, rep3);
                        tempa3.CreateDocument();
                        repa3 = new XtraReportA3(rep4, rep1);
                        repa3.CreateDocument();

                        repa3.Pages.AddRange(tempa3.Pages);
                    }
                    else
                    {
                        repa3 = new XtraReportA3(rep2, rep3);
                        repa3.CreateDocument();
                    }
                }
                else if (paperSize=="A4")
                {
                    repa4 = new XtraReportA4();

                    if (ShowPage1 == "1")
                    {
                        rep1.CreateDocument();
                        repa4.Pages.AddRange(rep1.Pages);
                    }
                    
                    rep2.CreateDocument();
                    rep3.CreateDocument();
                    
                    repa4.Pages.AddRange(rep2.Pages);
                    repa4.Pages.AddRange(rep3.Pages);

                    if (rep4 != null)
                    {
                        rep4.CreateDocument();
                        repa4.Pages.AddRange(rep4.Pages);
                    }
                }
                else
                {
                    repa3 = null;
                    repa4 = new XtraReportA4();
                }

            }
        }

        public void ShowPreviewDialog()
        {
            if(repa4!=null)
            {
                ReportPrintTool tool = new ReportPrintTool(repa4);
                tool.ShowPreviewDialog();
            }
            else if(repa3!=null)
            {
                ReportPrintTool tool = new ReportPrintTool(repa3);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("请先联系管理员配置打印格式。");
            }
        }

        public ReportComm(DataTable dtPage2,
                          DataTable dtRQlist, DataTable dtMember, DataTable dtOtherProperty, DataTable dtPage3, DataTable dtPage4,DataTable dtInfosub,//string paperSize, bool ShowPage1, bool isShowJMJE,
                          string capID, string name, string tel, string sfzh,
                          string dah, string addr, string qyrq, string shouqzhao,
                          string strSerObj, string qyrid, string yssqz
            )
        {
            if (dtOtherProperty == null || dtOtherProperty.Rows.Count == 0
                || dtPage2 == null || dtPage2.Rows.Count == 0
                || dtRQlist == null || dtRQlist.Rows.Count == 0
                )
            {
                repa4 = new XtraReportA4();
                isNullRep = true;
            }
            else
            {
                string paperSize = dtOtherProperty.Rows[0][tb_JTYSPageOtherProperty.PaperSize].ToString();
                string ShowPage1 = dtOtherProperty.Rows[0][tb_JTYSPageOtherProperty.showpage1].ToString();
                string isShowJMJE = dtOtherProperty.Rows[0][tb_JTYSPageOtherProperty.SPShowJMJE].ToString();
                

                //string[] arrSerObjs = strSerObj.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
                string strqydx = "";

                for (int index = 0; index < dtRQlist.Rows.Count; index++)
                {
                    if ((strSerObj.Contains(",") && strSerObj.Contains("," + dtRQlist.Rows[index][tb_JTYS签约对象类型.ID].ToString() + ","))
                        || strSerObj == dtRQlist.Rows[index][tb_JTYS签约对象类型.ID].ToString())
                    {
                        strqydx += dtRQlist.Rows[index][tb_JTYS签约对象类型.类型名称].ToString() + "■    ";
                    }
                    else
                    {
                        strqydx += dtRQlist.Rows[index][tb_JTYS签约对象类型.类型名称].ToString() + "□    ";
                    }
                }
                XtraReportP签约服务卡 rep2 = new XtraReportP签约服务卡(dtPage2, dtMember, dtInfosub, capID, qyrid, strqydx, name, sfzh, dah, tel, addr, shouqzhao, qyrq, yssqz);

                bool bIsShowJMJE = isShowJMJE == "1" ? true : false;
                //XtraReport rep3 = new XtraReportP3(dsSPPage3, bIsShowJMJE,
                //    dtPage3.Rows[0][tb_JTYSPage3.title].ToString(),
                //    dtPage3.Rows[0][tb_JTYSPage3.tail].ToString());

                //XtraReport rep4 = null;
                //if (dsSPPage4 != null && dsSPPage4.Tables.Count > 0)
                //{
                //    rep4 = new XtraReportP3(dsSPPage4, bIsShowJMJE,
                //        dtPage4.Rows[0][tb_JTYSPage4.title].ToString(),
                //    dtPage4.Rows[0][tb_JTYSPage4.tail].ToString());
                //}

                if (paperSize == "A4")
                {
                    repa4 = new XtraReportA4();
                    
                    rep2.CreateDocument();
                    //rep3.CreateDocument();

                    repa4.Pages.AddRange(rep2.Pages);
                    //repa4.Pages.AddRange(rep3.Pages);

                    //if (rep4 != null)
                    //{
                    //    rep4.CreateDocument();
                    //    repa4.Pages.AddRange(rep4.Pages);
                    //}
                }
                else
                {
                    repa3 = null;
                    repa4 = new XtraReportA4();
                }

            }
        }

        public void ShowPreviewDialog2()
        {
            if (repa4 != null)
            {
                ReportPrintTool tool = new ReportPrintTool(repa4);
                tool.ShowPreviewDialog();
            }
            else if(repa3 != null)
            {
                ReportPrintTool tool = new ReportPrintTool(repa3);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("请先联系管理员配置打印格式。");
            }
        }

        public bool GetPrintState()
        {
            bool isPrint = false;
            if (isNullRep)
            {
                isPrint= false;
            }
            else if (repa3 != null)
            {
                isPrint = repa3.GetPrintState();
            }
            else if (repa4 != null)
            {
                isPrint = repa4.GetPrintState();
            }
            else
            {
                isPrint = false;
            }
            return isPrint;
        }
    }
}
