﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class XtraReportP1 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReportP1(DataTable dtSetting)
        {
            InitializeComponent();

            if (dtSetting != null && dtSetting.Rows.Count > 0)
            {
                xrlTitle.Text = dtSetting.Rows[0]["title"].ToString();
                xrlHosp.Text = dtSetting.Rows[0]["hosp"].ToString();
            }
        }

    }
}
