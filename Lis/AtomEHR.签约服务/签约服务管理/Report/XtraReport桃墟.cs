﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace AtomEHR.签约服务.签约服务管理.Report
{
    public partial class XtraReport桃墟 : DevExpress.XtraReports.UI.XtraReport
    {
        bool printed = false;
        public XtraReport桃墟(string name, string tel, string sfzh, string addr, string qyrq, string shouqzhao, string fuwubao
                                ,string teamcapName, string teamcapTel
                                ,string qyrName, string qyrTel
                                ,string other0, string other0Tel
                                ,string other1, string other1Tel
                                ,string other2, string other2Tel
                                ,string other3, string other3Tel
                                ,string other4, string other4Tel
                                , string other5, string other5Tel
            )
        {
            InitializeComponent();

            xrl乙方姓名.Text = name;
            xrl联系电话.Text = tel;
            xrl住址.Text = addr;
            xrl签约日期.Text = qyrq;
            xrl身份证号.Text = sfzh;

            xrlTeamCapName.Text = teamcapName;
            xrlTeamCapTel.Text = teamcapTel;

            if(string.IsNullOrWhiteSpace(qyrName))
            {
                xrlTeamMemOne.Text = other0;
                xrlTeamMemOneTel.Text = other0Tel;

                xrlTeamMemTwo.Text = other1;
                xrlTeamMemTwoTel.Text = other1Tel;

                xrlTeamMemThr.Text = other2;
                xrlTeamMemThrTel.Text = other2Tel;

                xrlTeamMemFour.Text = other3;
                xrlTeamMemFourTel.Text = other3Tel;

                xrlTeamMemFive.Text = other4;
                xrlTeamMemFiveTel.Text = other4Tel;

                xrlTeamMemSix.Text = other5;
                xrlTeamMemSixTel.Text = other5Tel;
            }
            else
            {
                this.xrlTeamMemOne.Font = new System.Drawing.Font(this.xrlTeamMemOne.Font.FontFamily, this.xrlTeamMemOne.Font.Size, FontStyle.Bold);
                xrlTeamMemOne.Text = qyrName;
                xrlTeamMemOneTel.Text = qyrTel;


                xrlTeamMemTwo.Text = other0;
                xrlTeamMemTwoTel.Text = other0Tel;

                xrlTeamMemThr.Text = other1;
                xrlTeamMemThrTel.Text = other1Tel;

                xrlTeamMemFour.Text = other2;
                xrlTeamMemFourTel.Text = other2Tel;

                xrlTeamMemFive.Text = other3;
                xrlTeamMemFiveTel.Text = other3Tel;

                xrlTeamMemSix.Text = other4;
                xrlTeamMemSixTel.Text = other4Tel;
            }

            if (!string.IsNullOrEmpty(shouqzhao) && shouqzhao.Length > 12)
            {
                Byte[] bitmapData = new Byte[shouqzhao.Length];
                try
                {
                    bitmapData = Convert.FromBase64String(shouqzhao);
                    System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);
                    xrP乙方手签.Image = Image.FromStream(streamBitmap);
                }
                catch
                {
                    xrP乙方手签.Image = null;
                    return;
                }
            }
            else
            {
                xrP乙方手签.Image = null;
            }

            //ServiceID名称
//31c7323d9e4948baacf2b02e061ce4f2初级包  二型收费包
//c32b04887f59494da414eed3c8b7a33d初级包  一型免费包
//bfb3d8536788426696b0e27a36d0105c中级包  一型(0-6岁儿童)
//7ab8c3f4bfc94246ac7bc59f14c3a402中级包  二型(孕产妇)
//0f3276e3bf024f3caea39f89ea54ffda中级包  三型(肺结核患者)
//936935421a4444cd86969215f8e31275中级包  四型(高血压病人)
//7613c176c6e540159dbe4fef09f1c593中级包  五型(糖尿病病人)
//e6b0a4d0c5ee4a6ba0389c8e0cd02ac5中级包  六型(高血压、糖尿病高危人群)
//21d740b478634986a4b7c147db4d8c53中级包  七型(65岁及以上老人)
//e4b3e6e908d2408b9b6c30f84b871fdc高级包  一型(脑卒中及后遗症病人)
//17f2f41f9ee2432687f78abb17b61a3e高级包  二型(冠心病患者)
//5be5841d4a8a404ba485084a1ad78c23高级包  三型(慢阻肺病人)
//5efb5f5c9b3445d0a7b9b69a436a0e36高级包  四型(恶性肿瘤患者)
//23184f4f6d6e43f984dde3f14159014c高级包  五型(颈肩腰腿疼患者)
//42d2c65931dd4409a9160e70272415fa高级包  六型(有需求的精准扶贫患者)
//53cc9af7fa19497bb7eeb840012aad80高级包  七型(有需求残疾人患者)

            if(fuwubao.Contains("31c7323d9e4948baacf2b02e061ce4f2"))
            {
                xrTC初级包二.Text = "√";
            }
            if (fuwubao.Contains("c32b04887f59494da414eed3c8b7a33d"))
            {
                xrTC初级包一.Text = "√";
            }
            if (fuwubao.Contains("bfb3d8536788426696b0e27a36d0105c"))
            {
                xrTC中级包一.Text = "√";
            }
            if (fuwubao.Contains("7ab8c3f4bfc94246ac7bc59f14c3a402"))
            {
                xrTC中级包二.Text = "√";
            }
            if (fuwubao.Contains("0f3276e3bf024f3caea39f89ea54ffda"))
            {
                xrTC中级包三.Text = "√";
            }
            if (fuwubao.Contains("936935421a4444cd86969215f8e31275"))
            {
                xrTC中级包四.Text = "√";
            }
            if (fuwubao.Contains("7613c176c6e540159dbe4fef09f1c593"))
            {
                xrTC中级包五.Text = "√";
            }
            if (fuwubao.Contains("e6b0a4d0c5ee4a6ba0389c8e0cd02ac5"))
            {
                xrTC中级包六.Text = "√";
            }
            if (fuwubao.Contains("21d740b478634986a4b7c147db4d8c53"))
            {
                xrTC中级包七.Text = "√";
            }
            if (fuwubao.Contains("e4b3e6e908d2408b9b6c30f84b871fdc"))
            {
                xrTC高级包一.Text = "√";
            }
            if (fuwubao.Contains("17f2f41f9ee2432687f78abb17b61a3e"))
            {
                xrTC高级包二.Text = "√";
            }
            if (fuwubao.Contains("5be5841d4a8a404ba485084a1ad78c23"))
            {
                xrTC高级包三.Text = "√";
            }
            if (fuwubao.Contains("5efb5f5c9b3445d0a7b9b69a436a0e36"))
            {
                xrTC高级包四.Text = "√";
            }
            if (fuwubao.Contains("23184f4f6d6e43f984dde3f14159014c"))
            {
                xrTC高级包五.Text = "√";
            }
            if (fuwubao.Contains("42d2c65931dd4409a9160e70272415fa"))
            {
                xrTC高级包六.Text = "√";
            }
            if (fuwubao.Contains("53cc9af7fa19497bb7eeb840012aad80"))
            {
                xrTC高级包七.Text = "√";
            }

        }



        public bool GetPrintedState()
        {
            return printed;
        }

        private void XtraReport桃墟_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            printed = true;
        }

    }
}
