﻿namespace AtomEHR.签约服务.签约服务管理
{
    partial class frm签约_显示
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.pice签名 = new DevExpress.XtraEditors.PictureEdit();
            this.txt状态 = new DevExpress.XtraEditors.TextEdit();
            this.txt修改时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt修改人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建人 = new DevExpress.XtraEditors.TextEdit();
            this.txtMagCard = new DevExpress.XtraEditors.TextEdit();
            this.txt签约人 = new DevExpress.XtraEditors.TextEdit();
            this.gcServicePackage = new DevExpress.XtraGrid.GridControl();
            this.gvServicePackage = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服务包名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服务内容 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.col年收费标准 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.txtTelNo = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress = new DevExpress.XtraEditors.TextEdit();
            this.txtCardID = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtDabh = new DevExpress.XtraEditors.TextEdit();
            this.txtServiceObject = new DevExpress.XtraEditors.TextEdit();
            this.txtHealthCondition = new DevExpress.XtraEditors.TextEdit();
            this.txt签约日期 = new DevExpress.XtraEditors.TextEdit();
            this.txtServiceTeam = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem贫困人口 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pice签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt修改时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMagCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcServicePackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvServicePackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDabh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceObject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHealthCondition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceTeam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem贫困人口)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup1.CustomizationFormText = "签约服务项目执行";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(757, 516);
            this.layoutControlGroup1.Text = "签约服务项目执行";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "签约服务项目执行";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.Size = new System.Drawing.Size(757, 516);
            this.layoutControlGroup2.Text = "签约服务项目执行";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.pictureEdit1);
            this.layoutControl2.Controls.Add(this.pice签名);
            this.layoutControl2.Controls.Add(this.txt状态);
            this.layoutControl2.Controls.Add(this.txt修改时间);
            this.layoutControl2.Controls.Add(this.txt修改人);
            this.layoutControl2.Controls.Add(this.txt创建时间);
            this.layoutControl2.Controls.Add(this.txt创建人);
            this.layoutControl2.Controls.Add(this.txtMagCard);
            this.layoutControl2.Controls.Add(this.txt签约人);
            this.layoutControl2.Controls.Add(this.gcServicePackage);
            this.layoutControl2.Controls.Add(this.txtTelNo);
            this.layoutControl2.Controls.Add(this.txtAddress);
            this.layoutControl2.Controls.Add(this.txtCardID);
            this.layoutControl2.Controls.Add(this.txtName);
            this.layoutControl2.Controls.Add(this.txtDabh);
            this.layoutControl2.Controls.Add(this.txtServiceObject);
            this.layoutControl2.Controls.Add(this.txtHealthCondition);
            this.layoutControl2.Controls.Add(this.txt签约日期);
            this.layoutControl2.Controls.Add(this.txtServiceTeam);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(606, 301, 250, 350);
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(853, 661);
            this.layoutControl2.TabIndex = 127;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // pice签名
            // 
            this.pice签名.Location = new System.Drawing.Point(719, 521);
            this.pice签名.Name = "pice签名";
            this.pice签名.Properties.ReadOnly = true;
            this.pice签名.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pice签名.Size = new System.Drawing.Size(122, 116);
            this.pice签名.StyleController = this.layoutControl2;
            this.pice签名.TabIndex = 19;
            // 
            // txt状态
            // 
            this.txt状态.Location = new System.Drawing.Point(99, 617);
            this.txt状态.Name = "txt状态";
            this.txt状态.Properties.ReadOnly = true;
            this.txt状态.Size = new System.Drawing.Size(360, 20);
            this.txt状态.StyleController = this.layoutControl2;
            this.txt状态.TabIndex = 18;
            // 
            // txt修改时间
            // 
            this.txt修改时间.Location = new System.Drawing.Point(323, 593);
            this.txt修改时间.Name = "txt修改时间";
            this.txt修改时间.Properties.ReadOnly = true;
            this.txt修改时间.Size = new System.Drawing.Size(136, 20);
            this.txt修改时间.StyleController = this.layoutControl2;
            this.txt修改时间.TabIndex = 17;
            // 
            // txt修改人
            // 
            this.txt修改人.Location = new System.Drawing.Point(323, 569);
            this.txt修改人.Name = "txt修改人";
            this.txt修改人.Properties.ReadOnly = true;
            this.txt修改人.Size = new System.Drawing.Size(136, 20);
            this.txt修改人.StyleController = this.layoutControl2;
            this.txt修改人.TabIndex = 16;
            // 
            // txt创建时间
            // 
            this.txt创建时间.Location = new System.Drawing.Point(99, 593);
            this.txt创建时间.Name = "txt创建时间";
            this.txt创建时间.Properties.ReadOnly = true;
            this.txt创建时间.Size = new System.Drawing.Size(133, 20);
            this.txt创建时间.StyleController = this.layoutControl2;
            this.txt创建时间.TabIndex = 15;
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(99, 569);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.ReadOnly = true;
            this.txt创建人.Size = new System.Drawing.Size(133, 20);
            this.txt创建人.StyleController = this.layoutControl2;
            this.txt创建人.TabIndex = 14;
            // 
            // txtMagCard
            // 
            this.txtMagCard.EditValue = "";
            this.txtMagCard.Location = new System.Drawing.Point(99, 63);
            this.txtMagCard.Name = "txtMagCard";
            this.txtMagCard.Properties.ReadOnly = true;
            this.txtMagCard.Size = new System.Drawing.Size(203, 20);
            this.txtMagCard.StyleController = this.layoutControl2;
            this.txtMagCard.TabIndex = 11;
            // 
            // txt签约人
            // 
            this.txt签约人.Location = new System.Drawing.Point(99, 545);
            this.txt签约人.Name = "txt签约人";
            this.txt签约人.Properties.ReadOnly = true;
            this.txt签约人.Size = new System.Drawing.Size(133, 20);
            this.txt签约人.StyleController = this.layoutControl2;
            this.txt签约人.TabIndex = 10;
            // 
            // gcServicePackage
            // 
            this.gcServicePackage.Location = new System.Drawing.Point(99, 135);
            this.gcServicePackage.MainView = this.gvServicePackage;
            this.gcServicePackage.Name = "gcServicePackage";
            this.gcServicePackage.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.repositoryItemMemoEdit2,
            this.repositoryItemMemoEdit3});
            this.gcServicePackage.Size = new System.Drawing.Size(742, 382);
            this.gcServicePackage.TabIndex = 7;
            this.gcServicePackage.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvServicePackage});
            // 
            // gvServicePackage
            // 
            this.gvServicePackage.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.col服务包名称,
            this.gridColumn1,
            this.col服务内容,
            this.col年收费标准,
            this.gridColumn4,
            this.gridColumn3});
            this.gvServicePackage.GridControl = this.gcServicePackage;
            this.gvServicePackage.Name = "gvServicePackage";
            this.gvServicePackage.OptionsView.ColumnAutoWidth = false;
            this.gvServicePackage.OptionsView.RowAutoHeight = true;
            this.gvServicePackage.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ServiceID";
            this.gridColumn2.FieldName = "ServiceID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // col服务包名称
            // 
            this.col服务包名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col服务包名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务包名称.Caption = "服务包名称";
            this.col服务包名称.FieldName = "名称";
            this.col服务包名称.Name = "col服务包名称";
            this.col服务包名称.OptionsColumn.AllowEdit = false;
            this.col服务包名称.OptionsColumn.ReadOnly = true;
            this.col服务包名称.Visible = true;
            this.col服务包名称.VisibleIndex = 0;
            this.col服务包名称.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "适宜对象";
            this.gridColumn1.FieldName = "适宜对象";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 96;
            // 
            // col服务内容
            // 
            this.col服务内容.AppearanceHeader.Options.UseTextOptions = true;
            this.col服务内容.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服务内容.Caption = "服务内容";
            this.col服务内容.ColumnEdit = this.repositoryItemMemoEdit1;
            this.col服务内容.FieldName = "服务内容";
            this.col服务内容.Name = "col服务内容";
            this.col服务内容.OptionsColumn.AllowEdit = false;
            this.col服务内容.OptionsColumn.ReadOnly = true;
            this.col服务内容.Visible = true;
            this.col服务内容.VisibleIndex = 2;
            this.col服务内容.Width = 237;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // col年收费标准
            // 
            this.col年收费标准.AppearanceHeader.Options.UseTextOptions = true;
            this.col年收费标准.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col年收费标准.Caption = "年收费标准(元)";
            this.col年收费标准.FieldName = "年收费标准";
            this.col年收费标准.Name = "col年收费标准";
            this.col年收费标准.OptionsColumn.AllowEdit = false;
            this.col年收费标准.OptionsColumn.ReadOnly = true;
            this.col年收费标准.Width = 95;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "卫生院职责";
            this.gridColumn4.ColumnEdit = this.repositoryItemMemoEdit2;
            this.gridColumn4.FieldName = "卫生院职责";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 200;
            // 
            // repositoryItemMemoEdit2
            // 
            this.repositoryItemMemoEdit2.Name = "repositoryItemMemoEdit2";
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "卫生室职责";
            this.gridColumn3.ColumnEdit = this.repositoryItemMemoEdit3;
            this.gridColumn3.FieldName = "卫生室职责";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 200;
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            // 
            // txtTelNo
            // 
            this.txtTelNo.EditValue = "";
            this.txtTelNo.Location = new System.Drawing.Point(670, 39);
            this.txtTelNo.Name = "txtTelNo";
            this.txtTelNo.Properties.ReadOnly = true;
            this.txtTelNo.Size = new System.Drawing.Size(171, 20);
            this.txtTelNo.StyleController = this.layoutControl2;
            this.txtTelNo.TabIndex = 4;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(99, 87);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Properties.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(480, 20);
            this.txtAddress.StyleController = this.layoutControl2;
            this.txtAddress.TabIndex = 3;
            // 
            // txtCardID
            // 
            this.txtCardID.Location = new System.Drawing.Point(393, 63);
            this.txtCardID.Name = "txtCardID";
            this.txtCardID.Properties.ReadOnly = true;
            this.txtCardID.Size = new System.Drawing.Size(186, 20);
            this.txtCardID.StyleController = this.layoutControl2;
            this.txtCardID.TabIndex = 2;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(393, 39);
            this.txtName.Name = "txtName";
            this.txtName.Properties.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(186, 20);
            this.txtName.StyleController = this.layoutControl2;
            this.txtName.TabIndex = 1;
            // 
            // txtDabh
            // 
            this.txtDabh.Location = new System.Drawing.Point(99, 39);
            this.txtDabh.Name = "txtDabh";
            this.txtDabh.Properties.ReadOnly = true;
            this.txtDabh.Size = new System.Drawing.Size(203, 20);
            this.txtDabh.StyleController = this.layoutControl2;
            this.txtDabh.TabIndex = 0;
            // 
            // txtServiceObject
            // 
            this.txtServiceObject.Location = new System.Drawing.Point(670, 63);
            this.txtServiceObject.Name = "txtServiceObject";
            this.txtServiceObject.Properties.ReadOnly = true;
            this.txtServiceObject.Size = new System.Drawing.Size(171, 20);
            this.txtServiceObject.StyleController = this.layoutControl2;
            this.txtServiceObject.TabIndex = 5;
            // 
            // txtHealthCondition
            // 
            this.txtHealthCondition.Location = new System.Drawing.Point(99, 111);
            this.txtHealthCondition.Name = "txtHealthCondition";
            this.txtHealthCondition.Properties.ReadOnly = true;
            this.txtHealthCondition.Size = new System.Drawing.Size(742, 20);
            this.txtHealthCondition.StyleController = this.layoutControl2;
            this.txtHealthCondition.TabIndex = 13;
            // 
            // txt签约日期
            // 
            this.txt签约日期.Location = new System.Drawing.Point(323, 545);
            this.txt签约日期.Name = "txt签约日期";
            this.txt签约日期.Properties.Mask.EditMask = "d";
            this.txt签约日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txt签约日期.Properties.ReadOnly = true;
            this.txt签约日期.Size = new System.Drawing.Size(136, 20);
            this.txt签约日期.StyleController = this.layoutControl2;
            this.txt签约日期.TabIndex = 9;
            // 
            // txtServiceTeam
            // 
            this.txtServiceTeam.Location = new System.Drawing.Point(99, 521);
            this.txtServiceTeam.Name = "txtServiceTeam";
            this.txtServiceTeam.Properties.NullText = "[EditValue is null]";
            this.txtServiceTeam.Properties.ReadOnly = true;
            this.txtServiceTeam.Size = new System.Drawing.Size(360, 20);
            this.txtServiceTeam.StyleController = this.layoutControl2;
            this.txtServiceTeam.TabIndex = 12;
            // 
            // Root
            // 
            this.Root.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.Root.AppearanceGroup.Options.UseFont = true;
            this.Root.AppearanceGroup.Options.UseTextOptions = true;
            this.Root.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem7,
            this.layoutControlItem11,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem13,
            this.layoutControlItem5,
            this.layoutControlItem14,
            this.layoutControlItem1,
            this.emptySpaceItem贫困人口,
            this.layoutControlItem18,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem15,
            this.emptySpaceItem1,
            this.layoutControlItem19,
            this.layoutControlItem16,
            this.layoutControlItem12,
            this.layoutControlItem17});
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(853, 661);
            this.Root.Text = "家庭医生签约";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtName;
            this.layoutControlItem3.CustomizationFormText = "姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(294, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem3.Text = "姓名：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtDabh;
            this.layoutControlItem2.CustomizationFormText = "个人档案编号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem2.Text = "个人档案编号：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.gcServicePackage;
            this.layoutControlItem7.CustomizationFormText = "服务包";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(833, 386);
            this.layoutControlItem7.Text = "服务包：";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtServiceTeam;
            this.layoutControlItem11.CustomizationFormText = "签约服务团队：";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 482);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem11.Text = "服务团队：";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtCardID;
            this.layoutControlItem4.CustomizationFormText = "身份证号";
            this.layoutControlItem4.Location = new System.Drawing.Point(294, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txtTelNo;
            this.layoutControlItem6.CustomizationFormText = "联系电话";
            this.layoutControlItem6.Location = new System.Drawing.Point(571, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem6.Text = "联系电话：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtServiceObject;
            this.layoutControlItem13.CustomizationFormText = "签约对象类型";
            this.layoutControlItem13.Location = new System.Drawing.Point(571, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem13.Text = "签约对象类型：";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtAddress;
            this.layoutControlItem5.CustomizationFormText = "居住地址";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(571, 24);
            this.layoutControlItem5.Text = "居住地址：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtMagCard;
            this.layoutControlItem14.CustomizationFormText = "磁条卡号：";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem14.Text = "磁条卡号：";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtHealthCondition;
            this.layoutControlItem1.CustomizationFormText = "健康状况：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(833, 24);
            this.layoutControlItem1.Text = "健康状况：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(84, 14);
            // 
            // emptySpaceItem贫困人口
            // 
            this.emptySpaceItem贫困人口.AllowHotTrack = false;
            this.emptySpaceItem贫困人口.CustomizationFormText = "emptySpaceItem贫困人口";
            this.emptySpaceItem贫困人口.Location = new System.Drawing.Point(576, 48);
            this.emptySpaceItem贫困人口.Name = "emptySpaceItem贫困人口";
            this.emptySpaceItem贫困人口.Size = new System.Drawing.Size(264, 24);
            this.emptySpaceItem贫困人口.Text = "emptySpaceItem贫困人口";
            this.emptySpaceItem贫困人口.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem18.Control = this.pice签名;
            this.layoutControlItem18.CustomizationFormText = "签名或指纹：";
            this.layoutControlItem18.Location = new System.Drawing.Point(642, 482);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(191, 120);
            this.layoutControlItem18.Text = "签约对象签名或指纹：";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(60, 14);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt签约人;
            this.layoutControlItem10.CustomizationFormText = "签约人";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 506);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem10.Text = "签约人：";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt状态;
            this.layoutControlItem17.CustomizationFormText = "签约状态：";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 578);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(451, 24);
            this.layoutControlItem17.Text = "签约状态：";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txt签约日期;
            this.layoutControlItem9.CustomizationFormText = "执行日期";
            this.layoutControlItem9.Location = new System.Drawing.Point(224, 506);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(227, 24);
            this.layoutControlItem9.Text = "签约日期：";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txt创建人;
            this.layoutControlItem8.CustomizationFormText = "创建人：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 530);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem8.Text = "创建人：";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txt修改人;
            this.layoutControlItem15.CustomizationFormText = "修改人：";
            this.layoutControlItem15.Location = new System.Drawing.Point(224, 530);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(227, 24);
            this.layoutControlItem15.Text = "修改人：";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt创建时间;
            this.layoutControlItem12.CustomizationFormText = "创建时间：";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 554);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem12.Text = "创建时间：";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(84, 14);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txt修改时间;
            this.layoutControlItem16.CustomizationFormText = "修改时间：";
            this.layoutControlItem16.Location = new System.Drawing.Point(224, 554);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(227, 24);
            this.layoutControlItem16.Text = "修改时间：";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(84, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 559);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(840, 11);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Location = new System.Drawing.Point(528, 521);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Size = new System.Drawing.Size(122, 116);
            this.pictureEdit1.StyleController = this.layoutControl2;
            this.pictureEdit1.TabIndex = 20;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem19.Control = this.pictureEdit1;
            this.layoutControlItem19.CustomizationFormText = "负责人/家庭医生签字/指纹：";
            this.layoutControlItem19.Location = new System.Drawing.Point(451, 482);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(191, 120);
            this.layoutControlItem19.Text = "负责人/家庭医生签字/指纹：";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // frm签约_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 661);
            this.Controls.Add(this.layoutControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.Name = "frm签约_显示";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "家庭医生签约服务";
            this.Load += new System.EventHandler(this.frm签约_显示_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pice签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt修改时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMagCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcServicePackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvServicePackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCardID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDabh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceObject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHealthCondition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceTeam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem贫困人口)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit txtMagCard;
        private DevExpress.XtraEditors.TextEdit txt签约人;
        private DevExpress.XtraGrid.GridControl gcServicePackage;
        private DevExpress.XtraGrid.Views.Grid.GridView gvServicePackage;
        private DevExpress.XtraGrid.Columns.GridColumn col服务包名称;
        private DevExpress.XtraGrid.Columns.GridColumn col服务内容;
        private DevExpress.XtraGrid.Columns.GridColumn col年收费标准;
        private DevExpress.XtraEditors.TextEdit txtTelNo;
        private DevExpress.XtraEditors.TextEdit txtAddress;
        private DevExpress.XtraEditors.TextEdit txtCardID;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.TextEdit txtDabh;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.TextEdit txt修改时间;
        private DevExpress.XtraEditors.TextEdit txt修改人;
        private DevExpress.XtraEditors.TextEdit txt创建时间;
        private DevExpress.XtraEditors.TextEdit txt创建人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.TextEdit txt状态;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.TextEdit txtServiceObject;
        private DevExpress.XtraEditors.TextEdit txtHealthCondition;
        private DevExpress.XtraEditors.TextEdit txt签约日期;
        private DevExpress.XtraEditors.TextEdit txtServiceTeam;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem贫困人口;
        private DevExpress.XtraEditors.PictureEdit pice签名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
    }
}