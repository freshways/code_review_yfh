﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.Common;

namespace AtomEHR.签约服务.签约服务管理
{
    public partial class Frm签约审批 : AtomEHR.Library.frmBaseBusinessForm
    {
        public Frm签约审批()
        {
            InitializeComponent();
        }

        private void Frm签约审批_Load(object sender, EventArgs e)
        {
			this.InitializeForm();
        }
		
		protected override void InitializeForm()
        {
            _BLL = new bllJTYS服务包();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //util.BindingHelper.LookupEditBindDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, lookUpEdit性别, "P_CODE", "P_DESC");

            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;
            }
        }
        
        protected override bool DoSearchSummary()
        {
            string _strWhere = "";//" and " + tb_JTYS医生信息.是否有效 + "=1";

            

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS服务包", "*", _strWhere, tb_JTYS服务包.ID, "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Frm签约档案查询 frm = new Frm签约档案查询();
            //System.Windows.Forms.DialogResult result = frm.ShowDialog();

            //if(result == System.Windows.Forms.DialogResult.OK)
            //{
            //    frm签约 frmQY = new frm签约(frm.SelectedDabh, frm.SelectedName, frm.SelectedCardNo, frm.SelectedTelNo, frm.selectedAddress);
            //    frmQY.ShowDialog();
            //}
            //else
            //{
            //    frm.Dispose();
            //}
        }
    }
}
