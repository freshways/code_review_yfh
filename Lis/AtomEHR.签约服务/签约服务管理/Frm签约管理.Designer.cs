﻿namespace AtomEHR.签约服务.签约服务管理
{
    partial class Frm签约管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm签约管理));
            this.pagerControl1 = new TActionProject.PagerControl();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEdit镇 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit地址 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit村 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cboe打印 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cboe来源 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo履约状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chkMe = new DevExpress.XtraEditors.CheckEdit();
            this.ck含下属机构 = new DevExpress.XtraEditors.CheckEdit();
            this.txt磁卡号 = new DevExpress.XtraEditors.TextEdit();
            this.de审批日期E = new DevExpress.XtraEditors.DateEdit();
            this.de审批日期B = new DevExpress.XtraEditors.DateEdit();
            this.de到期日期E = new DevExpress.XtraEditors.DateEdit();
            this.de到期日期B = new DevExpress.XtraEditors.DateEdit();
            this.de生效日期E = new DevExpress.XtraEditors.DateEdit();
            this.de生效日期B = new DevExpress.XtraEditors.DateEdit();
            this.de签约日期E = new DevExpress.XtraEditors.DateEdit();
            this.de签约日期B = new DevExpress.XtraEditors.DateEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt签约医生 = new DevExpress.XtraEditors.TextEdit();
            this.tllue机构 = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.lue签约对象 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lue服务包类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lue状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btn解约 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnInfo = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnExport = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnConfirm = new DevExpress.XtraEditors.SimpleButton();
            this.gcSummary = new DevExpress.XtraGrid.GridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col健康档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col签约包 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col生效日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col到期日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col签约医生 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid签约来源 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid费用ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sbtnPrint2 = new DevExpress.XtraEditors.SimpleButton();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboe打印.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboe来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo履约状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt磁卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期E.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期E.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期B.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de到期日期E.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de到期日期E.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de到期日期B.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de到期日期B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de生效日期E.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de生效日期E.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de生效日期B.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de生效日期B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de签约日期E.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de签约日期E.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de签约日期B.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de签约日期B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue签约对象.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue服务包类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Size = new System.Drawing.Size(966, 508);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(972, 514);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(972, 514);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(972, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(794, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(597, 2);
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 461);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 1246541);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 15;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(966, 47);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 129;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.layoutControl1);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(966, 167);
            this.gcFindGroup.TabIndex = 130;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.comboBoxEdit镇);
            this.layoutControl1.Controls.Add(this.textEdit地址);
            this.layoutControl1.Controls.Add(this.comboBoxEdit村);
            this.layoutControl1.Controls.Add(this.cboe打印);
            this.layoutControl1.Controls.Add(this.cboe来源);
            this.layoutControl1.Controls.Add(this.cbo履约状态);
            this.layoutControl1.Controls.Add(this.chkMe);
            this.layoutControl1.Controls.Add(this.ck含下属机构);
            this.layoutControl1.Controls.Add(this.txt磁卡号);
            this.layoutControl1.Controls.Add(this.de审批日期E);
            this.layoutControl1.Controls.Add(this.de审批日期B);
            this.layoutControl1.Controls.Add(this.de到期日期E);
            this.layoutControl1.Controls.Add(this.de到期日期B);
            this.layoutControl1.Controls.Add(this.de生效日期E);
            this.layoutControl1.Controls.Add(this.de生效日期B);
            this.layoutControl1.Controls.Add(this.de签约日期E);
            this.layoutControl1.Controls.Add(this.de签约日期B);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.txt签约医生);
            this.layoutControl1.Controls.Add(this.tllue机构);
            this.layoutControl1.Controls.Add(this.lue签约对象);
            this.layoutControl1.Controls.Add(this.lue服务包类型);
            this.layoutControl1.Controls.Add(this.lue状态);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(271, 167, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(962, 163);
            this.layoutControl1.TabIndex = 35;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // comboBoxEdit镇
            // 
            this.comboBoxEdit镇.Location = new System.Drawing.Point(473, 128);
            this.comboBoxEdit镇.Name = "comboBoxEdit镇";
            this.comboBoxEdit镇.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit镇.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit镇.Size = new System.Drawing.Size(119, 20);
            this.comboBoxEdit镇.StyleController = this.layoutControl1;
            this.comboBoxEdit镇.TabIndex = 124;
            this.comboBoxEdit镇.EditValueChanged += new System.EventHandler(this.comboBoxEdit镇_EditValueChanged);
            // 
            // textEdit地址
            // 
            this.textEdit地址.Location = new System.Drawing.Point(768, 128);
            this.textEdit地址.Name = "textEdit地址";
            this.textEdit地址.Size = new System.Drawing.Size(182, 20);
            this.textEdit地址.StyleController = this.layoutControl1;
            this.textEdit地址.TabIndex = 125;
            // 
            // comboBoxEdit村
            // 
            this.comboBoxEdit村.Location = new System.Drawing.Point(596, 128);
            this.comboBoxEdit村.Name = "comboBoxEdit村";
            this.comboBoxEdit村.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit村.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEdit村.Size = new System.Drawing.Size(168, 20);
            this.comboBoxEdit村.StyleController = this.layoutControl1;
            this.comboBoxEdit村.TabIndex = 124;
            // 
            // cboe打印
            // 
            this.cboe打印.Location = new System.Drawing.Point(301, 128);
            this.cboe打印.Name = "cboe打印";
            this.cboe打印.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboe打印.Properties.Items.AddRange(new object[] {
            "",
            "已打印",
            "未打印"});
            this.cboe打印.Size = new System.Drawing.Size(93, 20);
            this.cboe打印.StyleController = this.layoutControl1;
            this.cboe打印.TabIndex = 32;
            this.cboe打印.ToolTip = "签约协议书是否打印过";
            this.cboe打印.ToolTipTitle = "签约协议书是否打印过";
            // 
            // cboe来源
            // 
            this.cboe来源.Location = new System.Drawing.Point(87, 128);
            this.cboe来源.Name = "cboe来源";
            this.cboe来源.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboe来源.Properties.Items.AddRange(new object[] {
            "",
            "手机APP",
            "电脑客户端",
            "自助签约"});
            this.cboe来源.Size = new System.Drawing.Size(135, 20);
            this.cboe来源.StyleController = this.layoutControl1;
            this.cboe来源.TabIndex = 31;
            this.cboe来源.ToolTip = "从什么地方录入的签约信息";
            this.cboe来源.ToolTipTitle = "从什么地方录入的签约信息";
            // 
            // cbo履约状态
            // 
            this.cbo履约状态.Location = new System.Drawing.Point(671, 80);
            this.cbo履约状态.Name = "cbo履约状态";
            this.cbo履约状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo履约状态.Properties.Items.AddRange(new object[] {
            "",
            "未履约",
            "履约中",
            "履约完毕"});
            this.cbo履约状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo履约状态.Size = new System.Drawing.Size(93, 20);
            this.cbo履约状态.StyleController = this.layoutControl1;
            this.cbo履约状态.TabIndex = 30;
            // 
            // chkMe
            // 
            this.chkMe.Location = new System.Drawing.Point(596, 56);
            this.chkMe.Name = "chkMe";
            this.chkMe.Properties.Caption = "录入人是我";
            this.chkMe.Size = new System.Drawing.Size(168, 19);
            this.chkMe.StyleController = this.layoutControl1;
            this.chkMe.TabIndex = 29;
            this.chkMe.CheckedChanged += new System.EventHandler(this.chkMe_CheckedChanged);
            // 
            // ck含下属机构
            // 
            this.ck含下属机构.EditValue = true;
            this.ck含下属机构.Location = new System.Drawing.Point(596, 31);
            this.ck含下属机构.Name = "ck含下属机构";
            this.ck含下属机构.Properties.Caption = "含下属机构";
            this.ck含下属机构.Size = new System.Drawing.Size(168, 19);
            this.ck含下属机构.StyleController = this.layoutControl1;
            this.ck含下属机构.TabIndex = 28;
            // 
            // txt磁卡号
            // 
            this.txt磁卡号.Location = new System.Drawing.Point(473, 104);
            this.txt磁卡号.Name = "txt磁卡号";
            this.txt磁卡号.Size = new System.Drawing.Size(119, 20);
            this.txt磁卡号.StyleController = this.layoutControl1;
            this.txt磁卡号.TabIndex = 27;
            // 
            // de审批日期E
            // 
            this.de审批日期E.EditValue = null;
            this.de审批日期E.Location = new System.Drawing.Point(243, 104);
            this.de审批日期E.Name = "de审批日期E";
            this.de审批日期E.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de审批日期E.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de审批日期E.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de审批日期E.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de审批日期E.Size = new System.Drawing.Size(151, 20);
            this.de审批日期E.StyleController = this.layoutControl1;
            this.de审批日期E.TabIndex = 26;
            // 
            // de审批日期B
            // 
            this.de审批日期B.EditValue = null;
            this.de审批日期B.Location = new System.Drawing.Point(87, 104);
            this.de审批日期B.Name = "de审批日期B";
            this.de审批日期B.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de审批日期B.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de审批日期B.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de审批日期B.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de审批日期B.Size = new System.Drawing.Size(135, 20);
            this.de审批日期B.StyleController = this.layoutControl1;
            this.de审批日期B.TabIndex = 25;
            // 
            // de到期日期E
            // 
            this.de到期日期E.EditValue = null;
            this.de到期日期E.Location = new System.Drawing.Point(243, 80);
            this.de到期日期E.Name = "de到期日期E";
            this.de到期日期E.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de到期日期E.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de到期日期E.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de到期日期E.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de到期日期E.Size = new System.Drawing.Size(151, 20);
            this.de到期日期E.StyleController = this.layoutControl1;
            this.de到期日期E.TabIndex = 24;
            // 
            // de到期日期B
            // 
            this.de到期日期B.EditValue = null;
            this.de到期日期B.Location = new System.Drawing.Point(87, 80);
            this.de到期日期B.Name = "de到期日期B";
            this.de到期日期B.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de到期日期B.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de到期日期B.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de到期日期B.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de到期日期B.Size = new System.Drawing.Size(135, 20);
            this.de到期日期B.StyleController = this.layoutControl1;
            this.de到期日期B.TabIndex = 23;
            // 
            // de生效日期E
            // 
            this.de生效日期E.EditValue = null;
            this.de生效日期E.Location = new System.Drawing.Point(243, 56);
            this.de生效日期E.Name = "de生效日期E";
            this.de生效日期E.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de生效日期E.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de生效日期E.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de生效日期E.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de生效日期E.Size = new System.Drawing.Size(151, 20);
            this.de生效日期E.StyleController = this.layoutControl1;
            this.de生效日期E.TabIndex = 22;
            // 
            // de生效日期B
            // 
            this.de生效日期B.EditValue = null;
            this.de生效日期B.Location = new System.Drawing.Point(87, 56);
            this.de生效日期B.Name = "de生效日期B";
            this.de生效日期B.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de生效日期B.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de生效日期B.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de生效日期B.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de生效日期B.Size = new System.Drawing.Size(135, 20);
            this.de生效日期B.StyleController = this.layoutControl1;
            this.de生效日期B.TabIndex = 21;
            // 
            // de签约日期E
            // 
            this.de签约日期E.EditValue = null;
            this.de签约日期E.Location = new System.Drawing.Point(243, 31);
            this.de签约日期E.Name = "de签约日期E";
            this.de签约日期E.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de签约日期E.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de签约日期E.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de签约日期E.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de签约日期E.Size = new System.Drawing.Size(151, 20);
            this.de签约日期E.StyleController = this.layoutControl1;
            this.de签约日期E.TabIndex = 20;
            // 
            // de签约日期B
            // 
            this.de签约日期B.EditValue = null;
            this.de签约日期B.Location = new System.Drawing.Point(87, 31);
            this.de签约日期B.Name = "de签约日期B";
            this.de签约日期B.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de签约日期B.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de签约日期B.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de签约日期B.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de签约日期B.Size = new System.Drawing.Size(135, 20);
            this.de签约日期B.StyleController = this.layoutControl1;
            this.de签约日期B.TabIndex = 19;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(637, 104);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(127, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 8;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(473, 80);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(119, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(843, 56);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(107, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 6;
            // 
            // txt签约医生
            // 
            this.txt签约医生.Location = new System.Drawing.Point(473, 56);
            this.txt签约医生.Name = "txt签约医生";
            this.txt签约医生.Size = new System.Drawing.Size(119, 20);
            this.txt签约医生.StyleController = this.layoutControl1;
            this.txt签约医生.TabIndex = 12;
            // 
            // tllue机构
            // 
            this.tllue机构.Location = new System.Drawing.Point(473, 31);
            this.tllue机构.Name = "tllue机构";
            this.tllue机构.Properties.AutoExpandAllNodes = false;
            this.tllue机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tllue机构.Properties.NullText = "";
            this.tllue机构.Properties.PopupSizeable = false;
            this.tllue机构.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.tllue机构.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.tllue机构.Size = new System.Drawing.Size(119, 20);
            this.tllue机构.StyleController = this.layoutControl1;
            this.tllue机构.TabIndex = 4;
            this.tllue机构.EditValueChanged += new System.EventHandler(this.tllue机构_EditValueChanged);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // lue签约对象
            // 
            this.lue签约对象.Location = new System.Drawing.Point(843, 31);
            this.lue签约对象.Name = "lue签约对象";
            this.lue签约对象.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue签约对象.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lue签约对象.Size = new System.Drawing.Size(107, 20);
            this.lue签约对象.StyleController = this.layoutControl1;
            this.lue签约对象.TabIndex = 18;
            // 
            // lue服务包类型
            // 
            this.lue服务包类型.Location = new System.Drawing.Point(843, 80);
            this.lue服务包类型.Name = "lue服务包类型";
            this.lue服务包类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue服务包类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lue服务包类型.Size = new System.Drawing.Size(107, 20);
            this.lue服务包类型.StyleController = this.layoutControl1;
            this.lue服务包类型.TabIndex = 11;
            // 
            // lue状态
            // 
            this.lue状态.Location = new System.Drawing.Point(843, 104);
            this.lue状态.Name = "lue状态";
            this.lue状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lue状态.Size = new System.Drawing.Size(107, 20);
            this.lue状态.StyleController = this.layoutControl1;
            this.lue状态.TabIndex = 17;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "签约服务查询";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem13,
            this.layoutControlItem4,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem5,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem25,
            this.layoutControlItem24});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(962, 163);
            this.layoutControlGroup1.Text = "签约服务查询";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tllue机构;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(386, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(198, 25);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.de签约日期B;
            this.layoutControlItem6.CustomizationFormText = "签约日期：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(214, 25);
            this.layoutControlItem6.Text = "签约日期：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.de签约日期E;
            this.layoutControlItem7.CustomizationFormText = "至";
            this.layoutControlItem7.Location = new System.Drawing.Point(214, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(172, 25);
            this.layoutControlItem7.Text = "至";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.de生效日期B;
            this.layoutControlItem10.CustomizationFormText = "生效日期：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(214, 24);
            this.layoutControlItem10.Text = "生效日期：";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.de生效日期E;
            this.layoutControlItem11.CustomizationFormText = "至";
            this.layoutControlItem11.Location = new System.Drawing.Point(214, 25);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItem11.Text = "至";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.de到期日期B;
            this.layoutControlItem12.CustomizationFormText = "到期日期：";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(214, 24);
            this.layoutControlItem12.Text = "到期日期：";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.de到期日期E;
            this.layoutControlItem14.CustomizationFormText = "至";
            this.layoutControlItem14.Location = new System.Drawing.Point(214, 49);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItem14.Text = "至";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.de审批日期B;
            this.layoutControlItem15.CustomizationFormText = "审核日期：";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 73);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(214, 24);
            this.layoutControlItem15.Text = "出生日期：";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.de审批日期E;
            this.layoutControlItem16.CustomizationFormText = "至";
            this.layoutControlItem16.Location = new System.Drawing.Point(214, 73);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItem16.Text = "至";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lue签约对象;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(756, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(186, 25);
            this.layoutControlItem2.Text = "签约对象：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txt档案号;
            this.layoutControlItem3.CustomizationFormText = "姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(756, 25);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(94, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "健康档案号：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.lue状态;
            this.layoutControlItem13.CustomizationFormText = "状态：";
            this.layoutControlItem13.Location = new System.Drawing.Point(756, 73);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem13.Text = "签约状态：";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(386, 49);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txt签约医生;
            this.layoutControlItem9.CustomizationFormText = "签约医生：";
            this.layoutControlItem9.Location = new System.Drawing.Point(386, 25);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem9.Text = "录入人：";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lue服务包类型;
            this.layoutControlItem8.CustomizationFormText = "服务包类型：";
            this.layoutControlItem8.Location = new System.Drawing.Point(756, 49);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem8.Text = "服务包类型：";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt磁卡号;
            this.layoutControlItem17.CustomizationFormText = "磁卡号：";
            this.layoutControlItem17.Location = new System.Drawing.Point(386, 73);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem17.Text = "磁卡号：";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.ck含下属机构;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(584, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(172, 25);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.chkMe;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(584, 25);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt姓名;
            this.layoutControlItem5.CustomizationFormText = "健康档案号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(584, 73);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItem5.Text = "姓名：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(36, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.cbo履约状态;
            this.layoutControlItem20.CustomizationFormText = "履约状态：";
            this.layoutControlItem20.Location = new System.Drawing.Point(584, 49);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItem20.Text = "履约状态：";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.cboe来源;
            this.layoutControlItem21.CustomizationFormText = "来源：";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 97);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(214, 27);
            this.layoutControlItem21.Text = "来源：";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.cboe打印;
            this.layoutControlItem22.CustomizationFormText = "是否已打印：";
            this.layoutControlItem22.Location = new System.Drawing.Point(214, 97);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(172, 27);
            this.layoutControlItem22.Text = "是否已打印：";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.comboBoxEdit村;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(584, 97);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(172, 27);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.comboBoxEdit镇;
            this.layoutControlItem25.CustomizationFormText = "地址：";
            this.layoutControlItem25.Location = new System.Drawing.Point(386, 97);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(198, 27);
            this.layoutControlItem25.Text = "地址：";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.textEdit地址;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(756, 97);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(186, 27);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btnEmpty);
            this.flowLayoutPanel1.Controls.Add(this.btnAdd);
            this.flowLayoutPanel1.Controls.Add(this.btn解约);
            this.flowLayoutPanel1.Controls.Add(this.sbtnEdit);
            this.flowLayoutPanel1.Controls.Add(this.sbtnInfo);
            this.flowLayoutPanel1.Controls.Add(this.sbtnExport);
            this.flowLayoutPanel1.Controls.Add(this.sbtnPrint);
            this.flowLayoutPanel1.Controls.Add(this.sbtnConfirm);
            this.flowLayoutPanel1.Controls.Add(this.sbtnPrint2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 167);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(966, 31);
            this.flowLayoutPanel1.TabIndex = 131;
            // 
            // btnQuery
            // 
            this.btnQuery.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.ImageOptions.Image")));
            this.btnQuery.Location = new System.Drawing.Point(53, 3);
            this.btnQuery.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 22);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.ImageOptions.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(134, 3);
            this.btnEmpty.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(75, 22);
            this.btnEmpty.TabIndex = 6;
            this.btnEmpty.Text = "重置";
            this.btnEmpty.ToolTip = "情况查询条件";
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.Location = new System.Drawing.Point(215, 3);
            this.btnAdd.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 22);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "签约";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btn解约
            // 
            this.btn解约.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn解约.ImageOptions.Image")));
            this.btn解约.Location = new System.Drawing.Point(296, 3);
            this.btn解约.MinimumSize = new System.Drawing.Size(75, 22);
            this.btn解约.Name = "btn解约";
            this.btn解约.Size = new System.Drawing.Size(75, 22);
            this.btn解约.TabIndex = 3;
            this.btn解约.Text = "解约";
            this.btn解约.Click += new System.EventHandler(this.btn解约_Click);
            // 
            // sbtnEdit
            // 
            this.sbtnEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnEdit.ImageOptions.Image")));
            this.sbtnEdit.Location = new System.Drawing.Point(377, 3);
            this.sbtnEdit.Name = "sbtnEdit";
            this.sbtnEdit.Size = new System.Drawing.Size(75, 23);
            this.sbtnEdit.TabIndex = 7;
            this.sbtnEdit.Text = "修改";
            this.sbtnEdit.ToolTip = "可修改除服务包之外的信息";
            this.sbtnEdit.Click += new System.EventHandler(this.sbtnEdit_Click);
            // 
            // sbtnInfo
            // 
            this.sbtnInfo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnInfo.ImageOptions.Image")));
            this.sbtnInfo.Location = new System.Drawing.Point(458, 3);
            this.sbtnInfo.Name = "sbtnInfo";
            this.sbtnInfo.Size = new System.Drawing.Size(85, 23);
            this.sbtnInfo.TabIndex = 4;
            this.sbtnInfo.Text = "详细信息";
            this.sbtnInfo.Click += new System.EventHandler(this.sbtnInfo_Click);
            // 
            // sbtnExport
            // 
            this.sbtnExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnExport.ImageOptions.Image")));
            this.sbtnExport.Location = new System.Drawing.Point(549, 3);
            this.sbtnExport.Name = "sbtnExport";
            this.sbtnExport.Size = new System.Drawing.Size(75, 23);
            this.sbtnExport.TabIndex = 8;
            this.sbtnExport.Text = "导出";
            this.sbtnExport.Click += new System.EventHandler(this.sbtnExport_Click);
            // 
            // sbtnPrint
            // 
            this.sbtnPrint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnPrint.ImageOptions.Image")));
            this.sbtnPrint.Location = new System.Drawing.Point(630, 3);
            this.sbtnPrint.Name = "sbtnPrint";
            this.sbtnPrint.Size = new System.Drawing.Size(83, 23);
            this.sbtnPrint.TabIndex = 8;
            this.sbtnPrint.Text = "打印协议";
            this.sbtnPrint.Click += new System.EventHandler(this.sbtnPrint_Click);
            // 
            // sbtnConfirm
            // 
            this.sbtnConfirm.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("sbtnConfirm.ImageOptions.Image")));
            this.sbtnConfirm.Location = new System.Drawing.Point(721, 3);
            this.sbtnConfirm.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.sbtnConfirm.Name = "sbtnConfirm";
            this.sbtnConfirm.Size = new System.Drawing.Size(131, 23);
            this.sbtnConfirm.TabIndex = 9;
            this.sbtnConfirm.Text = "自助签约费用确认";
            this.sbtnConfirm.Click += new System.EventHandler(this.sbtnConfirm_Click);
            // 
            // gcSummary
            // 
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.Location = new System.Drawing.Point(0, 198);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.Size = new System.Drawing.Size(966, 263);
            this.gcSummary.TabIndex = 132;
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn9,
            this.col健康档案号,
            this.gridColumn8,
            this.col姓名,
            this.col性别,
            this.col身份证号,
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn2,
            this.col签约包,
            this.col生效日期,
            this.col到期日期,
            this.col状态,
            this.gridColumn10,
            this.col签约医生,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn11,
            this.grid签约来源,
            this.grid费用ID});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "档案所属机构";
            this.gridColumn9.FieldName = "所属机构名称";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            this.gridColumn9.Width = 103;
            // 
            // col健康档案号
            // 
            this.col健康档案号.AppearanceHeader.Options.UseTextOptions = true;
            this.col健康档案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col健康档案号.Caption = "档案号";
            this.col健康档案号.FieldName = "档案号";
            this.col健康档案号.Name = "col健康档案号";
            this.col健康档案号.OptionsColumn.ReadOnly = true;
            this.col健康档案号.Visible = true;
            this.col健康档案号.VisibleIndex = 1;
            this.col健康档案号.Width = 127;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "磁条卡号";
            this.gridColumn8.FieldName = "磁卡号";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            this.gridColumn8.Width = 100;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 3;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别名称";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 4;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 5;
            this.col身份证号.Width = 140;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "医疗机构";
            this.gridColumn1.FieldName = "医疗机构";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 6;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "签约对象类型";
            this.gridColumn3.FieldName = "签约对象类型名称";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 7;
            this.gridColumn3.Width = 100;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "健康状况";
            this.gridColumn2.FieldName = "健康状况名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 8;
            this.gridColumn2.Width = 100;
            // 
            // col签约包
            // 
            this.col签约包.AppearanceHeader.Options.UseTextOptions = true;
            this.col签约包.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col签约包.Caption = "签约服务包";
            this.col签约包.FieldName = "服务包名称";
            this.col签约包.Name = "col签约包";
            this.col签约包.OptionsColumn.ReadOnly = true;
            this.col签约包.Visible = true;
            this.col签约包.VisibleIndex = 9;
            this.col签约包.Width = 240;
            // 
            // col生效日期
            // 
            this.col生效日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col生效日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col生效日期.Caption = "生效日期";
            this.col生效日期.FieldName = "生效日期";
            this.col生效日期.Name = "col生效日期";
            this.col生效日期.OptionsColumn.ReadOnly = true;
            this.col生效日期.Visible = true;
            this.col生效日期.VisibleIndex = 10;
            // 
            // col到期日期
            // 
            this.col到期日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col到期日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col到期日期.Caption = "到期日期";
            this.col到期日期.FieldName = "到期日期";
            this.col到期日期.Name = "col到期日期";
            this.col到期日期.OptionsColumn.ReadOnly = true;
            this.col到期日期.Visible = true;
            this.col到期日期.VisibleIndex = 11;
            // 
            // col状态
            // 
            this.col状态.AppearanceHeader.Options.UseTextOptions = true;
            this.col状态.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col状态.Caption = "状态";
            this.col状态.FieldName = "签约状态";
            this.col状态.Name = "col状态";
            this.col状态.OptionsColumn.ReadOnly = true;
            this.col状态.Visible = true;
            this.col状态.VisibleIndex = 12;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "签约医生";
            this.gridColumn10.FieldName = "签约医生";
            this.gridColumn10.Name = "gridColumn10";
            // 
            // col签约医生
            // 
            this.col签约医生.AppearanceHeader.Options.UseTextOptions = true;
            this.col签约医生.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col签约医生.Caption = "录入人";
            this.col签约医生.FieldName = "签约医生姓名";
            this.col签约医生.Name = "col签约医生";
            this.col签约医生.OptionsColumn.ReadOnly = true;
            this.col签约医生.Visible = true;
            this.col签约医生.VisibleIndex = 13;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "审核日期";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "审核者";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "解约原因";
            this.gridColumn7.FieldName = "解约原因";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "履约状态";
            this.gridColumn11.FieldName = "履约状态";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 14;
            // 
            // grid签约来源
            // 
            this.grid签约来源.Caption = "签约来源";
            this.grid签约来源.FieldName = "签约来源";
            this.grid签约来源.Name = "grid签约来源";
            this.grid签约来源.OptionsColumn.AllowEdit = false;
            this.grid签约来源.Visible = true;
            this.grid签约来源.VisibleIndex = 15;
            // 
            // grid费用ID
            // 
            this.grid费用ID.Caption = "费用ID";
            this.grid费用ID.FieldName = "费用ID";
            this.grid费用ID.Name = "grid费用ID";
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.CustomizationFormText = "居住地址：";
            this.layoutControlItem33.Location = new System.Drawing.Point(294, 48);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(174, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "居住地址：";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // sbtnPrint2
            // 
            this.sbtnPrint2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.sbtnPrint2.Location = new System.Drawing.Point(858, 3);
            this.sbtnPrint2.Name = "sbtnPrint2";
            this.sbtnPrint2.Size = new System.Drawing.Size(94, 23);
            this.sbtnPrint2.TabIndex = 10;
            this.sbtnPrint2.Text = "打印服务卡";
            this.sbtnPrint2.Click += new System.EventHandler(this.sbtnPrint2_Click);
            // 
            // Frm签约管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 540);
            this.Name = "Frm签约管理";
            this.Text = "签约管理";
            this.Load += new System.EventHandler(this.Frm签约管理_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit镇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit村.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboe打印.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboe来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo履约状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ck含下属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt磁卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期E.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期E.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期B.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de到期日期E.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de到期日期E.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de到期日期B.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de到期日期B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de生效日期E.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de生效日期E.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de生效日期B.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de生效日期B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de签约日期E.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de签约日期E.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de签约日期B.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de签约日期B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue签约对象.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue服务包类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TActionProject.PagerControl pagerControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btn解约;
        private DevExpress.XtraEditors.SimpleButton sbtnInfo;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn col健康档案号;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn col签约包;
        private DevExpress.XtraGrid.Columns.GridColumn col生效日期;
        private DevExpress.XtraGrid.Columns.GridColumn col到期日期;
        private DevExpress.XtraGrid.Columns.GridColumn col状态;
        private DevExpress.XtraGrid.Columns.GridColumn col签约医生;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.DateEdit de签约日期E;
        private DevExpress.XtraEditors.DateEdit de签约日期B;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.DateEdit de生效日期E;
        private DevExpress.XtraEditors.DateEdit de生效日期B;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.DateEdit de到期日期E;
        private DevExpress.XtraEditors.DateEdit de到期日期B;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.DateEdit de审批日期E;
        private DevExpress.XtraEditors.DateEdit de审批日期B;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.TextEdit txt签约医生;
        private DevExpress.XtraEditors.TextEdit txt磁卡号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.CheckEdit ck含下属机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.TreeListLookUpEdit tllue机构;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.SimpleButton sbtnEdit;
        private DevExpress.XtraEditors.ComboBoxEdit lue签约对象;
        private DevExpress.XtraEditors.ComboBoxEdit lue服务包类型;
        private DevExpress.XtraEditors.ComboBoxEdit lue状态;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.CheckEdit chkMe;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.ComboBoxEdit cbo履约状态;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.SimpleButton sbtnExport;
        private DevExpress.XtraEditors.SimpleButton sbtnPrint;
        private DevExpress.XtraEditors.ComboBoxEdit cboe打印;
        private DevExpress.XtraEditors.ComboBoxEdit cboe来源;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.TextEdit textEdit地址;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit村;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit镇;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraGrid.Columns.GridColumn grid签约来源;
        private DevExpress.XtraGrid.Columns.GridColumn grid费用ID;
        private DevExpress.XtraEditors.SimpleButton sbtnConfirm;
        private DevExpress.XtraEditors.SimpleButton sbtnPrint2;
    }
}