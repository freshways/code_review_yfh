﻿using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.转诊
{
    public partial class Frm转诊查询 : AtomEHR.Library.frmBaseBusinessForm
    {
        public Frm转诊查询()
        {
            InitializeComponent();
            _BLL = new bllJTYS转诊();
        }

        private void Frm转诊查询_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            pagerControl1.Height = 34;

            
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        protected override void InitializeForm()
        {
            _BLL = new bllJTYS医生信息();// 业务逻辑层实例

            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.
            base.InitButtonsBase();
            frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //DataTable dt所属机构 = bll机构.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            util.ControlsHelper.BindComboxData(dt所属机构, cbo转出机构, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo转出机构);
                cbo转出机构.Enabled = false;
            }

            util.ControlsHelper.BindComboxData(dt所属机构, cbo接收机构, "机构编号", "机构名称");
        }

        protected override bool DoSearchSummary()
        {
            string _strWhere = "";//" and " + tb_JTYS医生信息.是否有效 + "=1";

            string str转出机构 = util.ControlsHelper.GetComboxKey(cbo转出机构);
            string str接收机构 = util.ControlsHelper.GetComboxKey(cbo接收机构);
            if (!string.IsNullOrWhiteSpace(str转出机构))
            {
                _strWhere = " and " + tb_JTYS转诊.转出单位 + "='" + str转出机构 + "' ";
            }

            if (!string.IsNullOrWhiteSpace(str接收机构))
            {
                _strWhere = " and " + tb_JTYS转诊.接收单位 + "='" + str接收机构 + "' ";
            }


            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS转诊", "*", _strWhere, tb_JTYS转诊.ID, "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            gvSummary.BestFitColumns();

            return dt != null && dt.Rows.Count > 0;
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {

        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            Frm转诊记录单 frm = new Frm转诊记录单();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                XtraMessageBox.Show("添加成功");
            }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            try
            {
                string id = this.gvSummary.GetFocusedRowCellValue("ID").ToString();
                bool bret = _BLL.Delete(id);
                if(bret)
                {
                    DoSearchSummary();
                }
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        private void sbtnOut_Click(object sender, EventArgs e)
        {
            Frm转诊记录单 frm = new Frm转诊记录单();
            if(frm.ShowDialog()== System.Windows.Forms.DialogResult.OK)
            {
                XtraMessageBox.Show("添加成功");
            }

        }
    }
}
