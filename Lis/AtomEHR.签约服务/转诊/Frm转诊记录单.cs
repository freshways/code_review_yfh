﻿using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.转诊
{
    public partial class Frm转诊记录单 : XtraForm
    {
        public Frm转诊记录单()
        {
            InitializeComponent();
        }

        AtomEHR.Business.bll健康档案 bll档案 = new AtomEHR.Business.bll健康档案();
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DataSet ds = bll档案.GetRowInfoByKey(txt查询档案号.Text);
            if(ds==null || ds.Tables.Count==0 || ds.Tables[0].Rows.Count==0)
            {
                XtraMessageBox.Show("请先通过健康档案号查询出要转诊的患者。");
                return;
            }

            txtName.Text = ds.Tables[0].Rows[0]["姓名"].ToString();
            txt档案号.Text = ds.Tables[0].Rows[0]["个人档案编号"].ToString();
        }

        AtomEHR.Business.bll机构信息 _bll机构信息 = new AtomEHR.Business.bll机构信息();
        private void Frm转诊记录单_Load(object sender, EventArgs e)
        {
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            util.ControlsHelper.BindComboxData(dt所属机构, cbo接受单位, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                //util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo接受单位);
                //cbo接受单位.Enabled = false;
            }

            util.ControlsHelper.BindComboxData(dt所属机构, cbo转出单位, "机构编号", "机构名称");
        }

        bllJTYS转诊 bll = new bllJTYS转诊();
        private void sbtn保存_Click(object sender, EventArgs e)
        {
            string str转出单位 = util.ControlsHelper.GetComboxKey(cbo转出单位);
            string str接收单位 = util.ControlsHelper.GetComboxKey(cbo接受单位);

            if(string.IsNullOrWhiteSpace(str转出单位) || string.IsNullOrWhiteSpace(str接收单位) || string.IsNullOrWhiteSpace(txt档案号.Text)
                || string.IsNullOrWhiteSpace(txt转移原因.Text) || txt转移原因.Text=="因家庭医生履约需要，")
            {
                XtraMessageBox.Show("请确认转诊信息。");
                return;
            }

            DataSet ds = bll.GetBusinessByKey("-1", true);

            bll.NewBusiness();

            bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS转诊.接收单位] = str接收单位;
            bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS转诊.转出单位] = str转出单位;
            bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS转诊.转移患者] = txt档案号.Text;
            bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS转诊.转诊原因] = txt转移原因.Text;
            //bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS转诊.创建人] = Loginer.CurrentUser.用户编码;
            //bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS转诊.创建时间] = bll.ServiceDateTime;
            //bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS转诊.是否有效] = true;
            bll.Save(bll.CurrentBusiness);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void sbtn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
