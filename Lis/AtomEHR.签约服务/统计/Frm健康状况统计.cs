﻿using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.统计
{
    public partial class Frm健康状况统计 : AtomEHR.Library.frmBaseDataForm
    {
        public Frm健康状况统计()
        {
            InitializeComponent();
        }

        private void Frm健康状况统计_Load(object sender, EventArgs e)
        {
            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            //DataTable dt所属机构 = bll机构.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;
            }

            dte开始1.DateTime = new DateTime(2018, 1, 1);
            dte结束2.DateTime = new DateTime(2018, 12, 31);
        }



        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "电子表格|*.xlsx";
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                gvSummary.ExportToXlsx(saveFileDialog1.FileName);
            }
        }

        bllJTYS签约信息 bll签约信息 = new bllJTYS签约信息();
        private void btnQuery_Click(object sender, EventArgs e)
        {
            string str机构 = util.ControlsHelper.GetComboxKey(cbo机构);

            string dateBegin = dte开始1.Text;
            string dateEnd = dte结束2.Text;

            DataTable dt = bll签约信息.GetSummaryBy健康状况(str机构, dateBegin, dateEnd);
            this.gcSummary.DataSource = dt;
        }
    }
}
