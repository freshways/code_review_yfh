﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using AtomEHR.Interfaces;
using DevExpress.XtraEditors.Repository;

namespace AtomEHR.签约服务.统计
{
    public partial class Frm按签约团队统计 : AtomEHR.Library.frmBaseDataForm
    {
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        public Frm按签约团队统计()
        {
            InitializeComponent();
        }

        private void Frm按签约团队统计_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            this.gvSummary.CustomDrawFooterCell += gvSummary_CustomDrawFooterCell;
            this.gvSummary.CustomRowCellEdit += gvSummary_CustomRowCellEdit;
        }

        protected override void InitializeForm()
        {
            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            //DataTable dt所属机构 = bll机构.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;
            }

            string m_ServerDate = _bll.ServiceDateTime;
            try
            {
                this.dte开始1.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-01");
                this.dte结束2.Text = Convert.ToDateTime(m_ServerDate).ToString("yyyy-MM-dd");
            }
            catch
            {
                this.dte开始1.Text = DateTime.Now.ToString("yyyy-MM-01");
                this.dte结束2.Text = DateTime.Now.ToString("yyyy-MM-dd");
            }
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            //DataTable dt服务包 = bll服务包.GetAllValidateDataWithCodeAndName();
            //util.ControlsHelper.BindComboxData(dt服务包, cbo服务包, tb_JTYS服务包.ServiceID, tb_JTYS服务包.名称);
        }

        bllJTYS签约信息 _bll = new bllJTYS签约信息();
        private void btnQuery_Click(object sender, EventArgs e)
        {
            string queryType = rgp统计方式.EditValue.ToString();
            if(string.IsNullOrWhiteSpace(dte开始1.Text) || string.IsNullOrWhiteSpace(dte结束2.Text))
            {
                Msg.ShowInformation("请选择签约日期的起止日期。");
                return;
            }

            if(dte开始1.DateTime > dte结束2.DateTime)
            {
                Msg.ShowInformation("签约日期的开始日期需小于截止日期。");
                return;
            }

            //string str服务包 = util.ControlsHelper.GetComboxKey(cbo服务包);
            string str服务包 = "";

            string str机构 = util.ControlsHelper.GetComboxKey(cbo机构);


            DataSet ds = _bll.Get签约数量With团队New(str机构, queryType, dte开始1.DateTime.ToString("yyyy-MM-dd"), dte结束2.DateTime.ToString("yyyy-MM-dd"), str服务包);

            this.gcSummary.DataSource = ds.Tables[0];
            //if (ds != null && ds.Tables.Count > 0)
            //{
            //    gcSummary.DataSource = ds.Tables[0];
            //    this.gcSummary.BringToFront();
            //    this.gvSummary.BestFitColumns();
            //}
            this.gvSummary.BestFitColumns();
        }
        private void btnEmpty_Click(object sender, EventArgs e)
        {
            //base.ClearContainerEditorText(btnEmpty.Parent);
        }


        private int iAll = 1;
        //private int iHege = 0;
        private void gvSummary_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            if (e.Column == gridColumn3)
            {
                try
                {
                    int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
                    iAll = (temp == 0) ? 1 : temp;
                }
                catch
                { }
            }

            //if (e.Column == gridColumn3)
            //{
            //    e.Info.DisplayText = (iHege * 100.0 / iAll).ToString("N2") + "%";
            //}
            //else if (e.Column == gridColumn6)
            //{
            //    iAll = 1;
            //    try
            //    {
            //        int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
            //        iAll = (temp == 0) ? 1 : temp;
            //    }
            //    catch
            //    { }
            //}
            //else if (e.Column == gridColumn7)
            //{
            //    iHege = 1;
            //    try
            //    {
            //        int temp = Convert.ToInt32(e.Column.SummaryText.Replace(",", ""));
            //        iHege = temp;
            //    }
            //    catch
            //    { }
            //}
            //else
            //{ }
        }
        private void gvSummary_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            //DataRowView row = gvSummary.GetRow(e.RowHandle) as DataRowView;
            //if (row == null) return;
            //string zongshu = row["总数"].ToString();
            //string str已随访 = row["已随访"].ToString();
            //string str未随访 = row["未随访"].ToString();
        }
        
    }
}
