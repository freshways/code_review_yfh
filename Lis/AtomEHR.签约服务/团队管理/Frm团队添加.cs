﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;

namespace AtomEHR.签约服务.团队管理
{
    public partial class Frm团队添加 : frmBase
    {
        bllJTYS团队概要 bll = new bllJTYS团队概要();
        bllJTYS团队成员 bllMember = new bllJTYS团队成员();

        DataTable dtMembers = new DataTable();
        DataView dvMembers;

        UpdateType m_updateType = UpdateType.None;
        string m_teamid;
        DataSet m_ds = null;
        public Frm团队添加(UpdateType updatetype, string teamid)
        {
            InitializeComponent();

            m_updateType = updatetype;
            m_teamid = teamid;
        }

        private void Frm团队添加_Load(object sender, EventArgs e)
        {
            initForm();

            if(m_updateType == UpdateType.None)
            {
                //dtMembers = bllMember.GetValidateMemberListByTeamID(m_teamid);
                //m_ds = bll.GetBusinessByTeamID(m_teamid);
                lookUpEdit机构.Enabled = false;
                sbtnMemberEdit.Visible = false;
                sbtnMemberEdit.Enabled = false;
                sbtnSave.Visible = false;
                gridView成员.OptionsBehavior.Editable = false;
                textEdit名称.Properties.ReadOnly = false;
                lookUpEdit组长.Properties.ReadOnly = false;
                lookUpEdit组长.Enabled = false;
            }
        }


        bllJTYS团队角色 bllDocRole = new bllJTYS团队角色();
        DataTable m_dtDocRole = null;
        string m_ssxzjg = null;
        private DataTable GetDocRole()
        {
            if (m_ssxzjg == null)
            {
                m_ssxzjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
            }

            if (m_dtDocRole == null)
            {
                m_dtDocRole = bllDocRole.GetRolesByRGID(m_ssxzjg);
            }
            return m_dtDocRole;
        }


        void initForm()
        {
            //dtMembers.Columns.Add(tb_JTYS团队成员.成员类别);
            //dtMembers.Columns.Add(tb_JTYS团队成员.成员ID);
            //dtMembers.Columns.Add(tb_JTYS团队成员.成员姓名);
            GetDocRole();
            this.repositoryItemLookUpEdit1.DataSource = m_dtDocRole;
            

            if (m_updateType == UpdateType.Add)
            {
                dtMembers = bllMember.GetBusinessByKey("-1", false).Tables[0];
            }
            else
            {
                dtMembers = bllMember.GetValidateMemberListByTeamID(m_teamid);
                m_ds = bll.GetBusinessByTeamID(m_teamid);
            }


            dvMembers = dtMembers.DefaultView;
            dvMembers.RowFilter = tb_JTYS团队成员.是否有效 + "=1";

            this.gridControl成员.DataSource = dvMembers;

            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);


            lookUpEdit机构.Properties.ValueMember = "机构编号";
            lookUpEdit机构.Properties.DisplayMember = "机构名称";
            lookUpEdit机构.Properties.DataSource = dt所属机构;
            lookUpEdit机构.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称"));

            //TODO:  需要修改机构编号的地方
            //lookUpEdit机构.EditValue = "371321C21008";
            string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
            lookUpEdit机构.EditValue = ssjg;
            if (ssjg.Length > 6)
            {
                lookUpEdit机构.Enabled = false;
            }

            lookUpEdit组长.Properties.ValueMember = tb_JTYS团队成员.成员ID;
            lookUpEdit组长.Properties.DisplayMember = tb_JTYS团队成员.成员姓名;
            lookUpEdit组长.Properties.DataSource = dvMembers;
            lookUpEdit组长.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS团队成员.成员类别));
            lookUpEdit组长.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS团队成员.成员ID));
            lookUpEdit组长.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS团队成员.成员姓名));

            if (m_updateType != UpdateType.Add && (m_ds == null || m_ds.Tables.Count > 0 && m_ds.Tables[0].Rows.Count == 0))//未查询到有效的团队信息时
            {
                sbtnSave.Enabled = false;
            }
            else if (m_updateType != UpdateType.Add)
            {
                textEdit名称.Text = m_ds.Tables[0].Rows[0][tb_JTYS团队概要.团队名称].ToString();
                lookUpEdit机构.EditValue = m_ds.Tables[0].Rows[0][tb_JTYS团队概要.所属机构].ToString();
                lookUpEdit组长.EditValue = m_ds.Tables[0].Rows[0][tb_JTYS团队概要.组长编号].ToString();
            }
            else
            { }
        }


        private void sbtnMemberEdit_Click(object sender, EventArgs e)
        {
            Frm成员编辑 frm = new Frm成员编辑(dtMembers, m_teamid);
            //DialogResult result = frm.ShowDialog();
            frm.ShowDialog();
            //if(result == DialogResult.OK)
            //{
            //    gridControl成员.DataSource = dtMembers;
            //}
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {
            if(lookUpEdit机构.EditValue == null)
            {
                MessageBox.Show("请选择团队所属机构！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if(string.IsNullOrWhiteSpace(textEdit名称.Text))
            {
                MessageBox.Show("请填写团队名称！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (dtMembers.Rows.Count == 0)
            {
                MessageBox.Show("请添加团队成员！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (lookUpEdit组长.EditValue == null)
            {
                MessageBox.Show("请设置团队负责人！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if(lookUpEdit组长.GetSelectedDataRow() == null)
            {
                MessageBox.Show("请选择团队组长！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //获取团队负责人信息
            DataRow dr = ((System.Data.DataRowView)lookUpEdit组长.GetSelectedDataRow()).Row;
            string type = dr[tb_JTYS团队成员.成员类别].ToString();
            string id = dr[tb_JTYS团队成员.成员ID].ToString();
            string name = dr[tb_JTYS团队成员.成员姓名].ToString();


            if (m_updateType == UpdateType.Add)
            {
                bll.GetBusinessByKey("-1", true);
                bll.NewBusiness();

                //string teamid = bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队概要.团队ID].ToString();

                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队概要.团队ID] = m_teamid;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队概要.团队名称] = textEdit名称.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队概要.组长类别] = type;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队概要.组长编号] = id;
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队概要.所属机构] = lookUpEdit机构.EditValue.ToString();
                bll.Save(bll.CurrentBusiness);
            }
            else
            {
                m_ds.Tables[0].Rows[0][tb_JTYS团队概要.团队名称] = textEdit名称.Text.Trim();
                m_ds.Tables[0].Rows[0][tb_JTYS团队概要.组长类别] = type;
                m_ds.Tables[0].Rows[0][tb_JTYS团队概要.组长编号] = id;
                m_ds.Tables[0].Rows[0][tb_JTYS团队概要.所属机构] = lookUpEdit机构.EditValue.ToString();
                m_ds.Tables[0].Rows[0][tb_JTYS团队概要.修改人] = Loginer.CurrentUser.用户编码;
                m_ds.Tables[0].Rows[0][tb_JTYS团队概要.修改时间] = bll.ServiceDateTime ;
                bll.Save(m_ds);
            }

            

            //bllMember.GetBusinessByKey("-1", true);
            //for (int index = 0; index < dtMembers.Rows.Count; index++)
            //{
            //    bllMember.NewBusiness();
            //    bllMember.CurrentBusiness.Tables[0].Rows[index][tb_JTYS团队成员.团队ID] = teamid;
            //    bllMember.CurrentBusiness.Tables[0].Rows[index][tb_JTYS团队成员.成员类别] = dtMembers.Rows[index][tb_JTYS团队成员.成员类别];
            //    bllMember.CurrentBusiness.Tables[0].Rows[index][tb_JTYS团队成员.成员ID] = dtMembers.Rows[index][tb_JTYS团队成员.成员ID];
            //    bllMember.CurrentBusiness.Tables[0].Rows[index][tb_JTYS团队成员.成员姓名] = dtMembers.Rows[index][tb_JTYS团队成员.成员姓名];

            //    if(dtMembers.Rows[index].RowState == DataRowState.Added)
            //    {
            //        bll.CurrentBusiness.Tables[0].Rows[index].SetAdded();
            //    }
            //    else if (dtMembers.Rows[index].RowState == DataRowState.Modified)
            //    {
            //        bll.CurrentBusiness.Tables[0].Rows[index].SetModified();
            //    }
            //    else if (dtMembers.Rows[index].RowState == DataRowState.Unchanged)
            //    {
            //        bll.CurrentBusiness.Tables[0].Rows[index].AcceptChanges();
            //    }
            //}
            //DataTable deletedRows = dtMembers.GetChanges(DataRowState.Deleted);

            //for (int index = 0; index< dtMembers.Rows.Count;index++ )
            //{
            //    if(dtMembers.Rows[index].RowState == DataRowState.Added)
            //    {
            //        dtMembers.Rows[index][tb_JTYS团队成员.团队ID] = teamid;
            //        dtMembers.Rows[index][tb_JTYS团队成员.创建人] = Loginer.CurrentUser.用户编码;
            //        dtMembers.Rows[index][tb_JTYS团队成员.创建时间] = bllMember.ServiceDateTime;
            //        dtMembers.Rows[index][tb_JTYS团队成员.是否有效] = true;
            //        dtMembers.Rows[index][tb_JTYS团队成员.修改人] = Loginer.CurrentUser.用户编码;
            //        dtMembers.Rows[index][tb_JTYS团队成员.修改时间] = dtMembers.Rows[index][tb_JTYS团队成员.创建时间];
            //    }
            //    else if (dtMembers.Rows[index].RowState == DataRowState.Modified)
            //    {
            //        dtMembers.Rows[index][tb_JTYS团队成员.修改人] = Loginer.CurrentUser.用户编码;
            //        dtMembers.Rows[index][tb_JTYS团队成员.修改时间] = bllMember.ServiceDateTime;
            //    }
            //}
            bllMember.Save(dtMembers.DataSet);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        
    }
}
