﻿using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.团队管理
{
    public partial class frm医生添加 : frmBase
    {
        bllJTYS医生信息 bll = new bllJTYS医生信息();
        DataSet m_dsDocInfo = null;
        UpdateType m_updatetype;
        string m_docid = string.Empty;
        public frm医生添加(UpdateType updatetype, string docid)
        {
            InitializeComponent();

            m_updatetype = updatetype;
            m_docid = docid;

            util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t性别, comboBoxEdit性别, "P_CODE", "P_DESC");

            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            lookUpEdit机构.Properties.ValueMember = "机构编号";
            lookUpEdit机构.Properties.DisplayMember = "机构名称";
            lookUpEdit机构.Properties.DataSource = dt所属机构;
            lookUpEdit机构.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称"));

            if(m_updatetype == UpdateType.Modify)
            {
                lookUpEdit机构.Enabled = false;
                m_dsDocInfo = bll.GetBusinessByKey(m_docid, false);
                if(m_dsDocInfo == null || m_dsDocInfo.Tables.Count ==0 || m_dsDocInfo.Tables[0].Rows.Count ==0)
                {
                    btn保存.Enabled = false;
                }
                else
                {
                    util.ControlsHelper.SetComboxData(m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.DocSex].ToString(), comboBoxEdit性别);
                    textEdit姓名.Text = m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.DocName].ToString();
                    textEdit身份证号.Text = m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.CardID].ToString();
                    textEdit联系电话.Text = m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.TelNo].ToString();
                    lookUpEdit机构.EditValue = m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.所属机构].ToString();
                }
            }
            else
            {
                //TODO:  需要修改机构编号的地方
                //lookUpEdit机构.EditValue = "371321C21008";
                string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                lookUpEdit机构.EditValue = ssjg;
                if (ssjg.Length > 6)
                {
                    lookUpEdit机构.Enabled = false;
                }
            }
        }

        

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            //验证
            string sfzh = textEdit身份证号.Text.Trim();
            if (sfzh.Length > 0 && !CardIDHelper.Check18(sfzh))
            {
                MessageBox.Show("身份证号有误，请确认！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (lookUpEdit机构.EditValue==null)
            {
                MessageBox.Show("未选择所属机构，请确认！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (util.ControlsHelper.GetComboxKey(comboBoxEdit性别) != "1" && util.ControlsHelper.GetComboxKey(comboBoxEdit性别)!="2")
            {
                MessageBox.Show("请选择正确的性别！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (m_updatetype == UpdateType.Add)
            {
                bll.GetBusinessByKey("-1", true);
                bll.NewBusiness();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS医生信息.DocName] = textEdit姓名.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS医生信息.DocSex] = util.ControlsHelper.GetComboxKey(comboBoxEdit性别);
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS医生信息.CardID] = textEdit身份证号.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS医生信息.TelNo] = textEdit联系电话.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS医生信息.所属机构] = lookUpEdit机构.EditValue.ToString();
                bll.Save(bll.CurrentBusiness);
            }
            else if(m_updatetype == UpdateType.Modify)
            {
                m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.DocName] = textEdit姓名.Text.Trim();
                m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.DocSex] = util.ControlsHelper.GetComboxKey(comboBoxEdit性别);
                m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.CardID] = textEdit身份证号.Text.Trim();
                m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.TelNo] = textEdit联系电话.Text.Trim();
                m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.所属机构] = lookUpEdit机构.EditValue.ToString();
                m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.修改人] = Loginer.CurrentUser.用户编码;
                m_dsDocInfo.Tables[0].Rows[0][tb_JTYS医生信息.修改时间] = bll.ServiceDateTime;
                bll.Save(m_dsDocInfo);
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
