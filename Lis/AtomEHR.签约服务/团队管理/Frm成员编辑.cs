﻿using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.团队管理
{
    public partial class Frm成员编辑 : frmBase
    {
        bllJTYS医生信息 bllDoc = new bllJTYS医生信息();

        DataTable dtMembers ;
        string m_teamid;
        public Frm成员编辑()
        {
            InitializeComponent();

            InitForm();
        }

        public Frm成员编辑(DataTable dt, string teamid)
        {
            InitializeComponent();

            dtMembers = dt;
            
            m_teamid = teamid;

            InitForm();
        }

        void InitForm()
        {
            //DataTable dt所属机构 = _bll机构信息.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            cboGWJG.Properties.ValueMember = "机构编号";
            cboGWJG.Properties.DisplayMember = "机构名称";
            cboGWJG.Properties.DataSource = dt所属机构;
            cboGWJG.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称"));

            //TODO:  需要修改机构编号的地方
            //cboGWJG.EditValue = "371321C21008";
            string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
            if (ssjg.Length > 6)
            {
                cboGWJG.EditValue = ssjg;
                cboGWJG.Enabled = false;
            }


            cboDocJG.Properties.ValueMember = "机构编号";
            cboDocJG.Properties.DisplayMember = "机构名称";
            cboDocJG.Properties.DataSource = dt所属机构;
            cboDocJG.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称"));

            //TODO:  需要修改机构编号的地方
            //cboDocJG.EditValue = "371321C21008";
            
            if (ssjg.Length > 6)
            {
                cboDocJG.EditValue = ssjg;
                cboDocJG.Enabled = false;
            }
            //dtMembers.Columns.Add("成员类别");
            //dtMembers.Columns.Add("成员ID");
            //dtMembers.Columns.Add("成员姓名");

            GetDocRole();
            this.repositoryItemLookUpEdit1.DataSource = m_dtDocRole;

            gridControlMember.DataSource = dtMembers.DefaultView;
        }

        private void sbtnGWuserQ_Click(object sender, EventArgs e)
        {
            if(cboGWJG.EditValue==null)
            {
                MessageBox.Show("请选择公卫人员的所属机构！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string rgid = cboGWJG.EditValue.ToString();
            DataTable dtgw = _bllUser.GetGWUserListByRGID(rgid, textEditGWname.Text.Trim());
            gridControlGWUser.DataSource = dtgw;
        }

        private void sbtnDocQuery_Click(object sender, EventArgs e)
        {
            if (cboDocJG.EditValue == null)
            {
                MessageBox.Show("请选择医生的所属机构！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string rgid = cboDocJG.EditValue.ToString();
            DataTable dtdoc = bllDoc.GetDocListByRGID(rgid, textEditDocName.Text.Trim());
            gridControlDoc.DataSource = dtdoc;
        }

        private void sbtnRemoveItem_Click(object sender, EventArgs e)
        {
            int[] selectRows = gridViewMember.GetSelectedRows();
            if (selectRows.Length == 0)
            {
                Msg.ShowInformation("请选择一条记录。");
                return;
            }
            if (selectRows.Length > 1)
            {
                Msg.ShowInformation("只能选择一条记录。");
                return;
            }

            string memberty = gridViewMember.GetRowCellValue(selectRows[0], "成员类别").ToString();
            string memberid = gridViewMember.GetRowCellValue(selectRows[0], "成员ID").ToString();

            DataRow[] drs = dtMembers.Select("成员类别='" + memberty + "' and 成员ID='"+memberid+"'");
            if(drs.Length >0)
            {
                //dtMembers.Rows.Remove(drs[0]);

                for(int index =0; index < drs.Length; index++)
                {
                    if(drs[index].RowState == DataRowState.Added)
                    {
                        dtMembers.Rows.Remove(drs[0]);
                    }
                    else if (drs[index].RowState == DataRowState.Modified || drs[index].RowState == DataRowState.Unchanged)
                    {
                        drs[index][tb_JTYS团队成员.是否有效] = false;
                        drs[index][tb_JTYS团队成员.修改人] = Loginer.CurrentUser.用户编码;
                        drs[index][tb_JTYS团队成员.修改时间] = bllDoc.ServiceDateTime;
                    }
                }
            }
        }

        private void sbtnAddGW_Click(object sender, EventArgs e)
        {
            int[] selectRows = gridViewGWUser.GetSelectedRows();
            if (selectRows.Length == 0)
            {
                Msg.ShowInformation("请选择一条记录。");
                return;
            }
            if (selectRows.Length > 1)
            {
                Msg.ShowInformation("只能选择一条记录。");
                return;
            }

            string membercode = gridViewGWUser.GetRowCellValue(selectRows[0], "用户编码").ToString();
            string membername = gridViewGWUser.GetRowCellValue(selectRows[0], "UserName").ToString();

            AddMember("公卫人员", membercode, membername);

            //dtMembers.Columns.Add("成员类别");
            //dtMembers.Columns.Add("成员ID");
            //dtMembers.Columns.Add("成员姓名");
        }

        private void sbtnAddDoc_Click(object sender, EventArgs e)
        {
            int[] selectRows = gridViewDoc.GetSelectedRows();
            if (selectRows.Length == 0)
            {
                Msg.ShowInformation("请选择一条记录。");
                return;
            }
            if (selectRows.Length > 1)
            {
                Msg.ShowInformation("只能选择一条记录。");
                return;
            }

            string membercode = gridViewDoc.GetRowCellValue(selectRows[0], "DocID").ToString();
            string membername = gridViewDoc.GetRowCellValue(selectRows[0], "DocName").ToString();

            AddMember("医生", membercode, membername);
        }

        bllJTYS全局参数Local bllglocal = new bllJTYS全局参数Local();
        string m_useDocRole = null;
        string m_ssxzjg = null;
        private string GetuseDocRole()
        {
            if(m_useDocRole==null)
            {
                if (m_ssxzjg == null)
                {
                    m_ssxzjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                }
                m_useDocRole = bllglocal.GetValueByCode(m_ssxzjg, "useDocRole");
            }

            return m_useDocRole;
        }

        bllJTYS团队角色 bllDocRole = new bllJTYS团队角色();
        DataTable m_dtDocRole = null;
        private DataTable GetDocRole()
        {
            if (m_ssxzjg == null)
            {
                m_ssxzjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
            }

            if(m_dtDocRole==null)
            {
                m_dtDocRole = bllDocRole.GetRolesByRGID(m_ssxzjg);
            }
            return m_dtDocRole;
        }

        private void AddMember(string type, string code, string name)
        {
            bool bret = false;
            string docRole = null;
            if (Loginer.CurrentUser.所属机构.StartsWith("371321"))
            {
                bret = true;
            }
            else
            {
                if (GetuseDocRole() == "1")
                {
                    DataTable dttemp = GetDocRole();
                    Frm选择签约权限 frm = new Frm选择签约权限(dttemp);
                    frm.ShowDialog();
                    bret = frm.isAccess;
                    docRole = frm.docRole;
                }
                else
                {
                    //DataTable dttemp = GetDocRole();
                    Frm选择签约权限 frm = new Frm选择签约权限(null);
                    frm.ShowDialog();
                    bret = frm.isAccess;
                    docRole = null;
                }
            }

            DataRow[] drs = dtMembers.Select(tb_JTYS团队成员.成员类别+"='" + type + "' and "+tb_JTYS团队成员.成员ID+"='" + code + "'");
            if (drs.Length == 0)
            {
                DataRow dr = dtMembers.Rows.Add();
                dr[tb_JTYS团队成员.成员类别] = type;
                dr[tb_JTYS团队成员.成员ID] = code;
                dr[tb_JTYS团队成员.成员姓名] = name;
                dr[tb_JTYS团队成员.是否有效] = true;
                dr[tb_JTYS团队成员.团队ID] = m_teamid;
                dr[tb_JTYS团队成员.创建人] = Loginer.CurrentUser.用户编码;
                dr[tb_JTYS团队成员.创建时间] = bllDoc.ServiceDateTime;
                dr[tb_JTYS团队成员.修改人] = Loginer.CurrentUser.用户编码;
                dr[tb_JTYS团队成员.修改时间] = dr[tb_JTYS团队成员.创建时间];
                dr[tb_JTYS团队成员.签约权限] = bret;
                if(docRole==null)
                {
                    dr[tb_JTYS团队成员.DocRole] = DBNull.Value;
                }
                else
                {
                    dr[tb_JTYS团队成员.DocRole] = Convert.ToInt32(docRole);
                }
            }
            else if (drs[0][tb_JTYS团队成员.是否有效].ToString() == "0" || drs[0][tb_JTYS团队成员.是否有效].ToString().ToLower()=="false")
            {
                drs[0][tb_JTYS团队成员.成员姓名] = name;
                drs[0][tb_JTYS团队成员.是否有效] = true;
                drs[0][tb_JTYS团队成员.DocRole] = Convert.ToInt32(docRole);
            }
            else
            {

            }
        }

        private void sbtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void textEditDocName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)//Enter
            {
                sbtnDocQuery_Click(null, null);
                this.gridControlDoc.Focus();
            }
            
        }

        private void textEditGWname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)//Enter
            {
                sbtnGWuserQ_Click(null, null);
            }
        }

        private void gridControlDoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                sbtnAddDoc_Click(null, null);
                this.textEditDocName.Focus();
            }
        }
    }
}
