﻿using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.团队管理
{
    public partial class Frm医生管理 : AtomEHR.Library.frmBaseBusinessForm
    {
        public Frm医生管理()
        {
            InitializeComponent();
        }

        private void Frm医生管理New_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
        }

        protected override void InitializeForm()
        {
            _BLL = new bllJTYS医生信息();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //util.BindingHelper.LookupEditBindDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_CODE", "P_DESC");
            util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t性别, lookUpEdit性别, "P_CODE", "P_DESC");

            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();

            //DataTable dt所属机构 = bll机构.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            util.ControlsHelper.BindComboxData(dt所属机构, lookUpEdit机构, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, lookUpEdit机构);
                lookUpEdit机构.Enabled = false;
            }
        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            frm医生添加 frm = new frm医生添加(Common.UpdateType.Add, "");
            frm.ShowDialog();
        }
        
        protected override bool DoSearchSummary()
        {
            string _strWhere = "";//" and " + tb_JTYS医生信息.是否有效 + "=1";

            string str机构 = util.ControlsHelper.GetComboxKey(lookUpEdit机构);
            if (!string.IsNullOrWhiteSpace(str机构))
            {
                _strWhere = " and " + tb_JTYS医生信息.所属机构 + "='" + str机构 + "' ";
            }

            if(!string.IsNullOrWhiteSpace(textEdit姓名.Text))
            {
                _strWhere = " and " + tb_JTYS医生信息.DocName + "='" + textEdit姓名.Text.Trim() + "' ";
            }
            if(!string.IsNullOrWhiteSpace(textEdit身份证号.Text))
            {
                _strWhere = " and " + tb_JTYS医生信息.CardID + "='" + textEdit身份证号.Text.Trim() + "' ";
            }
            string str性别 = util.ControlsHelper.GetComboxKey(lookUpEdit性别);
            if (!string.IsNullOrWhiteSpace(str性别) && (str性别 == "1" || str性别=="2"))
            {
                _strWhere = " and " + tb_JTYS医生信息.DocSex + "='" + str性别 + "' ";
            }

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS医生信息", "*", _strWhere, tb_JTYS医生信息.DocID, "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }

            string docid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS医生信息.DocID).ToString();
            frm医生添加 frm = new frm医生添加(Common.UpdateType.Modify, docid);
            frm.ShowDialog();
        }
           
        private void btn禁用_Click(object sender, EventArgs e)
        {
            try
            {
                int[] selectdIndex = gvSummary.GetSelectedRows();
                if (selectdIndex.Length == 0)
                {
                    Msg.ShowInformation("请选择需要修改的行");
                    return;
                }
                else if (selectdIndex.Length > 1)
                {
                    Msg.ShowInformation("每次只能修改一条数据");
                    return;
                }

                string docid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS医生信息.DocID).ToString();
                string docname = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS医生信息.DocName).ToString();

                if (!Msg.AskQuestion("确定要禁用【" + docname + "】吗？")) return;

                int count = ((bllJTYS医生信息)_BLL).Update是否禁用(docid, false);

                if (count > 0)
                {
                    DoSearchSummary();
                }
            }
            catch(Exception ex)
            {
                Msg.ShowException(ex);
            }
        }
    }
}
