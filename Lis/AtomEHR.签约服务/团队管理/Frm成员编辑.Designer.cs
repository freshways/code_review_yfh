﻿namespace AtomEHR.签约服务.团队管理
{
    partial class Frm成员编辑
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sbtnRemoveItem = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddGW = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlMember = new DevExpress.XtraGrid.GridControl();
            this.gridViewMember = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlDoc = new DevExpress.XtraGrid.GridControl();
            this.gridViewDoc = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEditDocName = new DevExpress.XtraEditors.TextEdit();
            this.sbtnDocQuery = new DevExpress.XtraEditors.SimpleButton();
            this.cboDocJG = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlGWUser = new DevExpress.XtraGrid.GridControl();
            this.gridViewGWUser = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditGWname = new DevExpress.XtraEditors.TextEdit();
            this.sbtnGWuserQ = new DevExpress.XtraEditors.SimpleButton();
            this.cboGWJG = new DevExpress.XtraEditors.LookUpEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAddDoc = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMember)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMember)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDocName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDocJG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGWUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGWUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGWname.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboGWJG.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbtnRemoveItem
            // 
            this.sbtnRemoveItem.Location = new System.Drawing.Point(463, 245);
            this.sbtnRemoveItem.Name = "sbtnRemoveItem";
            this.sbtnRemoveItem.Size = new System.Drawing.Size(57, 31);
            this.sbtnRemoveItem.TabIndex = 5;
            this.sbtnRemoveItem.Text = "<=";
            this.sbtnRemoveItem.ToolTip = "从成员名单中移除成员";
            this.sbtnRemoveItem.Click += new System.EventHandler(this.sbtnRemoveItem_Click);
            // 
            // sbtnAddGW
            // 
            this.sbtnAddGW.Location = new System.Drawing.Point(463, 130);
            this.sbtnAddGW.Name = "sbtnAddGW";
            this.sbtnAddGW.Size = new System.Drawing.Size(57, 31);
            this.sbtnAddGW.TabIndex = 6;
            this.sbtnAddGW.Text = "=>";
            this.sbtnAddGW.ToolTip = "将公卫人员添加到成员名单中";
            this.sbtnAddGW.Click += new System.EventHandler(this.sbtnAddGW_Click);
            // 
            // gridControlMember
            // 
            this.gridControlMember.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMember.Location = new System.Drawing.Point(2, 22);
            this.gridControlMember.MainView = this.gridViewMember;
            this.gridControlMember.Name = "gridControlMember";
            this.gridControlMember.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemLookUpEdit1});
            this.gridControlMember.Size = new System.Drawing.Size(345, 438);
            this.gridControlMember.TabIndex = 6;
            this.gridControlMember.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMember});
            // 
            // gridViewMember
            // 
            this.gridViewMember.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn4,
            this.gridColumn8});
            this.gridViewMember.GridControl = this.gridControlMember;
            this.gridViewMember.Name = "gridViewMember";
            this.gridViewMember.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "人员类型";
            this.gridColumn5.FieldName = "成员类别";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 60;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "编号";
            this.gridColumn6.FieldName = "成员ID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 56;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "姓名";
            this.gridColumn7.FieldName = "成员姓名";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 62;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "是否有签约权限";
            this.gridColumn4.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn4.FieldName = "签约权限";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 80;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "角色";
            this.gridColumn8.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.gridColumn8.FieldName = "DocRole";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            this.gridColumn8.Width = 69;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.DisplayMember = "角色名称";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.NullText = "";
            this.repositoryItemLookUpEdit1.ValueMember = "ID";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Location = new System.Drawing.Point(16, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(441, 496);
            this.panelControl1.TabIndex = 3;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControlDoc);
            this.groupControl2.Controls.Add(this.panelControl4);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(2, 261);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(437, 233);
            this.groupControl2.TabIndex = 5;
            this.groupControl2.Text = "医生";
            // 
            // gridControlDoc
            // 
            this.gridControlDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDoc.Location = new System.Drawing.Point(2, 63);
            this.gridControlDoc.MainView = this.gridViewDoc;
            this.gridControlDoc.Name = "gridControlDoc";
            this.gridControlDoc.Size = new System.Drawing.Size(433, 168);
            this.gridControlDoc.TabIndex = 7;
            this.gridControlDoc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDoc});
            this.gridControlDoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gridControlDoc_KeyPress);
            // 
            // gridViewDoc
            // 
            this.gridViewDoc.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11});
            this.gridViewDoc.GridControl = this.gridControlDoc;
            this.gridViewDoc.Name = "gridViewDoc";
            this.gridViewDoc.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "编号";
            this.gridColumn10.FieldName = "DocID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "医生姓名";
            this.gridColumn11.FieldName = "DocName";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.labelControl5);
            this.panelControl4.Controls.Add(this.labelControl3);
            this.panelControl4.Controls.Add(this.textEditDocName);
            this.panelControl4.Controls.Add(this.sbtnDocQuery);
            this.panelControl4.Controls.Add(this.cboDocJG);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl4.Location = new System.Drawing.Point(2, 22);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(433, 41);
            this.panelControl4.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(22, 13);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 14);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "机构：";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(208, 13);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 14);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "姓名：";
            // 
            // textEditDocName
            // 
            this.textEditDocName.Location = new System.Drawing.Point(250, 10);
            this.textEditDocName.Name = "textEditDocName";
            this.textEditDocName.Size = new System.Drawing.Size(94, 20);
            this.textEditDocName.TabIndex = 1;
            this.textEditDocName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEditDocName_KeyPress);
            // 
            // sbtnDocQuery
            // 
            this.sbtnDocQuery.Location = new System.Drawing.Point(350, 5);
            this.sbtnDocQuery.Name = "sbtnDocQuery";
            this.sbtnDocQuery.Size = new System.Drawing.Size(68, 31);
            this.sbtnDocQuery.TabIndex = 2;
            this.sbtnDocQuery.Text = "查询";
            this.sbtnDocQuery.Click += new System.EventHandler(this.sbtnDocQuery_Click);
            // 
            // cboDocJG
            // 
            this.cboDocJG.Location = new System.Drawing.Point(64, 10);
            this.cboDocJG.Name = "cboDocJG";
            this.cboDocJG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDocJG.Properties.NullText = "";
            this.cboDocJG.Properties.PopupSizeable = false;
            this.cboDocJG.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboDocJG.Size = new System.Drawing.Size(130, 20);
            this.cboDocJG.TabIndex = 4;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControlGWUser);
            this.groupControl1.Controls.Add(this.panelControl3);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(2, 2);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(437, 259);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "公卫人员、乡医";
            // 
            // gridControlGWUser
            // 
            this.gridControlGWUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlGWUser.Location = new System.Drawing.Point(2, 63);
            this.gridControlGWUser.MainView = this.gridViewGWUser;
            this.gridControlGWUser.Name = "gridControlGWUser";
            this.gridControlGWUser.Size = new System.Drawing.Size(433, 194);
            this.gridControlGWUser.TabIndex = 3;
            this.gridControlGWUser.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewGWUser});
            // 
            // gridViewGWUser
            // 
            this.gridViewGWUser.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridViewGWUser.GridControl = this.gridControlGWUser;
            this.gridViewGWUser.Name = "gridViewGWUser";
            this.gridViewGWUser.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "用户编码";
            this.gridColumn1.FieldName = "用户编码";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "姓名";
            this.gridColumn2.FieldName = "UserName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "所属机构名称";
            this.gridColumn3.FieldName = "所属机构名称";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.labelControl4);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Controls.Add(this.textEditGWname);
            this.panelControl3.Controls.Add(this.sbtnGWuserQ);
            this.panelControl3.Controls.Add(this.cboGWJG);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl3.Location = new System.Drawing.Point(2, 22);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(433, 41);
            this.panelControl3.TabIndex = 5;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(22, 11);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(36, 14);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "机构：";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(208, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "姓名：";
            // 
            // textEditGWname
            // 
            this.textEditGWname.Location = new System.Drawing.Point(250, 9);
            this.textEditGWname.Name = "textEditGWname";
            this.textEditGWname.Size = new System.Drawing.Size(94, 20);
            this.textEditGWname.TabIndex = 1;
            this.textEditGWname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEditGWname_KeyPress);
            // 
            // sbtnGWuserQ
            // 
            this.sbtnGWuserQ.Location = new System.Drawing.Point(350, 4);
            this.sbtnGWuserQ.Name = "sbtnGWuserQ";
            this.sbtnGWuserQ.Size = new System.Drawing.Size(68, 31);
            this.sbtnGWuserQ.TabIndex = 2;
            this.sbtnGWuserQ.Text = "查询";
            this.sbtnGWuserQ.Click += new System.EventHandler(this.sbtnGWuserQ_Click);
            // 
            // cboGWJG
            // 
            this.cboGWJG.Location = new System.Drawing.Point(64, 8);
            this.cboGWJG.Name = "cboGWJG";
            this.cboGWJG.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboGWJG.Properties.NullText = "";
            this.cboGWJG.Properties.PopupSizeable = false;
            this.cboGWJG.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cboGWJG.Size = new System.Drawing.Size(130, 20);
            this.cboGWJG.TabIndex = 3;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gridControlMember);
            this.groupControl3.Controls.Add(this.panelControl2);
            this.groupControl3.Location = new System.Drawing.Point(525, 12);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(349, 496);
            this.groupControl3.TabIndex = 7;
            this.groupControl3.Text = "成员名单";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.sbtnOK);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(2, 460);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(345, 34);
            this.panelControl2.TabIndex = 8;
            // 
            // sbtnOK
            // 
            this.sbtnOK.Location = new System.Drawing.Point(241, 8);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(75, 23);
            this.sbtnOK.TabIndex = 0;
            this.sbtnOK.Text = "确定";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // sbtnAddDoc
            // 
            this.sbtnAddDoc.Location = new System.Drawing.Point(463, 362);
            this.sbtnAddDoc.Name = "sbtnAddDoc";
            this.sbtnAddDoc.Size = new System.Drawing.Size(57, 31);
            this.sbtnAddDoc.TabIndex = 6;
            this.sbtnAddDoc.Text = "=>";
            this.sbtnAddDoc.ToolTip = "将医生添加到成员名单中";
            this.sbtnAddDoc.Click += new System.EventHandler(this.sbtnAddDoc_Click);
            // 
            // Frm成员编辑
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 521);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.sbtnRemoveItem);
            this.Controls.Add(this.sbtnAddDoc);
            this.Controls.Add(this.sbtnAddGW);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm成员编辑";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "成员编辑";
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMember)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMember)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDocName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDocJG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlGWUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGWUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGWname.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboGWJG.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton sbtnRemoveItem;
        private DevExpress.XtraEditors.SimpleButton sbtnAddGW;
        private DevExpress.XtraGrid.GridControl gridControlMember;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMember;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlGWUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewGWUser;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton sbtnGWuserQ;
        private DevExpress.XtraEditors.TextEdit textEditGWname;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textEditDocName;
        private DevExpress.XtraEditors.SimpleButton sbtnDocQuery;
        private DevExpress.XtraGrid.GridControl gridControlDoc;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDoc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton sbtnAddDoc;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit cboDocJG;
        private DevExpress.XtraEditors.LookUpEdit cboGWJG;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton sbtnOK;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
    }
}