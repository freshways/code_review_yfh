﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;

namespace AtomEHR.签约服务.团队管理
{
    public partial class Frm选择签约权限 : frmBase
    {
        public Frm选择签约权限(DataTable dtDocRole)
        {
            InitializeComponent();

            if (dtDocRole != null)
            {
                lueRole.Properties.ValueMember = tb_JTYS团队角色.ID;
                lueRole.Properties.DisplayMember = tb_JTYS团队角色.角色名称;
                lueRole.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo(tb_JTYS团队角色.角色名称));

                lueRole.Properties.DataSource = dtDocRole;
            }
            else
            {
                labelControl3.Visible = false;
                lueRole.Visible = false;
                panelControl1.Location = new Point(12, 21);
                this.Height -= 32;
            }
        }

        public bool isAccess = false;

        public string docRole = null;
        private void sbtnOK_Click(object sender, EventArgs e)
        {
            if(lueRole.Visible)
            {
                if(lueRole.EditValue == null || string.IsNullOrWhiteSpace(lueRole.EditValue.ToString()))
                {
                    Msg.ShowInformation("请选择该医生在团队中的角色。");
                    return;
                }

                docRole = lueRole.EditValue.ToString();
            }

            object obj = radioGroup1.EditValue;
            if(obj!=null && obj.ToString().ToLower()=="true")
            {
                isAccess = true;
            }
            else
            {
                isAccess = false;
            }

            this.DialogResult = DialogResult.OK;
        }
    }
}
