﻿namespace AtomEHR.签约服务.团队管理
{
    partial class Frm团队管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm团队管理));
            this.pagerControl1 = new TActionProject.PagerControl();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit名称 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit组长 = new DevExpress.XtraEditors.TextEdit();
            this.cbo机构 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.but禁用 = new DevExpress.XtraEditors.SimpleButton();
            this.gcSummary = new AtomEHR.Library.UserControls.DataGridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcol团队ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol团队名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol组长类别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol组长姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol成员 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol是否有效 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol修改人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol修改时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit组长.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Size = new System.Drawing.Size(794, 394);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(800, 400);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(800, 400);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(800, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(622, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(425, 2);
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 339);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 105818);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 15;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(794, 55);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 129;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.layoutControl1);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(794, 62);
            this.gcFindGroup.TabIndex = 133;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit名称);
            this.layoutControl1.Controls.Add(this.textEdit组长);
            this.layoutControl1.Controls.Add(this.cbo机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(241, 163, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(790, 64);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit名称
            // 
            this.textEdit名称.Location = new System.Drawing.Point(300, 32);
            this.textEdit名称.Name = "textEdit名称";
            this.textEdit名称.Size = new System.Drawing.Size(141, 20);
            this.textEdit名称.StyleController = this.layoutControl1;
            this.textEdit名称.TabIndex = 14;
            // 
            // textEdit组长
            // 
            this.textEdit组长.Location = new System.Drawing.Point(508, 32);
            this.textEdit组长.Name = "textEdit组长";
            this.textEdit组长.Size = new System.Drawing.Size(126, 20);
            this.textEdit组长.StyleController = this.layoutControl1;
            this.textEdit组长.TabIndex = 13;
            // 
            // cbo机构
            // 
            this.cbo机构.Location = new System.Drawing.Point(75, 32);
            this.cbo机构.Name = "cbo机构";
            this.cbo机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo机构.Properties.NullText = "[EditValue is null]";
            this.cbo机构.Properties.PopupSizeable = true;
            this.cbo机构.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo机构.Size = new System.Drawing.Size(158, 20);
            this.cbo机构.StyleController = this.layoutControl1;
            this.cbo机构.TabIndex = 12;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "医生查询";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(790, 64);
            this.layoutControlGroup3.Text = "团队查询";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(626, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(144, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.cbo机构;
            this.layoutControlItem7.CustomizationFormText = "所属机构：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem7.Text = "所属机构：";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit名称;
            this.layoutControlItem2.CustomizationFormText = "团队名称：";
            this.layoutControlItem2.Location = new System.Drawing.Point(225, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(208, 24);
            this.layoutControlItem2.Text = "团队名称：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit组长;
            this.layoutControlItem1.CustomizationFormText = "成员姓名：";
            this.layoutControlItem1.Location = new System.Drawing.Point(433, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(193, 24);
            this.layoutControlItem1.Text = "组长姓名：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 14);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btnEmpty);
            this.flowLayoutPanel1.Controls.Add(this.btnAdd);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Controls.Add(this.but禁用);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 62);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(794, 30);
            this.flowLayoutPanel1.TabIndex = 134;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(103, 3);
            this.btnQuery.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 22);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(184, 3);
            this.btnEmpty.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(75, 22);
            this.btnEmpty.TabIndex = 5;
            this.btnEmpty.Text = "重置";
            this.btnEmpty.Visible = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(265, 3);
            this.btnAdd.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 22);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "添加";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(346, 3);
            this.btn修改.MinimumSize = new System.Drawing.Size(75, 22);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 22);
            this.btn修改.TabIndex = 3;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(427, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(85, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "详细信息";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // but禁用
            // 
            this.but禁用.Image = ((System.Drawing.Image)(resources.GetObject("but禁用.Image")));
            this.but禁用.Location = new System.Drawing.Point(518, 3);
            this.but禁用.MinimumSize = new System.Drawing.Size(75, 22);
            this.but禁用.Name = "but禁用";
            this.but禁用.Size = new System.Drawing.Size(75, 22);
            this.but禁用.TabIndex = 0;
            this.but禁用.Text = "禁用";
            this.but禁用.Click += new System.EventHandler(this.but禁用_Click);
            // 
            // gcSummary
            // 
            this.gcSummary.AllowBandedGridColumnSort = false;
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.IsBestFitColumns = true;
            this.gcSummary.Location = new System.Drawing.Point(0, 92);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.ShowContextMenu = false;
            this.gcSummary.Size = new System.Drawing.Size(794, 247);
            this.gcSummary.StrWhere = "";
            this.gcSummary.TabIndex = 135;
            this.gcSummary.UseCheckBox = false;
            this.gcSummary.View = "";
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 30;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcol团队ID,
            this.gcol所属机构,
            this.gcol团队名称,
            this.gcol组长类别,
            this.gcol组长姓名,
            this.gcol成员,
            this.gcol是否有效,
            this.gcol创建人,
            this.gcol创建时间,
            this.gcol修改人,
            this.gcol修改时间});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // gcol团队ID
            // 
            this.gcol团队ID.Caption = "团队ID";
            this.gcol团队ID.FieldName = "团队ID";
            this.gcol团队ID.Name = "gcol团队ID";
            // 
            // gcol所属机构
            // 
            this.gcol所属机构.Caption = "所属机构";
            this.gcol所属机构.FieldName = "所属机构名称";
            this.gcol所属机构.Name = "gcol所属机构";
            this.gcol所属机构.OptionsColumn.ReadOnly = true;
            this.gcol所属机构.Visible = true;
            this.gcol所属机构.VisibleIndex = 0;
            this.gcol所属机构.Width = 120;
            // 
            // gcol团队名称
            // 
            this.gcol团队名称.Caption = "团队名称";
            this.gcol团队名称.FieldName = "团队名称";
            this.gcol团队名称.Name = "gcol团队名称";
            this.gcol团队名称.OptionsColumn.ReadOnly = true;
            this.gcol团队名称.Visible = true;
            this.gcol团队名称.VisibleIndex = 1;
            this.gcol团队名称.Width = 100;
            // 
            // gcol组长类别
            // 
            this.gcol组长类别.Caption = "组长类别";
            this.gcol组长类别.FieldName = "组长类别";
            this.gcol组长类别.Name = "gcol组长类别";
            this.gcol组长类别.OptionsColumn.ReadOnly = true;
            this.gcol组长类别.Visible = true;
            this.gcol组长类别.VisibleIndex = 2;
            // 
            // gcol组长姓名
            // 
            this.gcol组长姓名.Caption = "组长姓名";
            this.gcol组长姓名.FieldName = "组长姓名";
            this.gcol组长姓名.Name = "gcol组长姓名";
            this.gcol组长姓名.OptionsColumn.ReadOnly = true;
            this.gcol组长姓名.Visible = true;
            this.gcol组长姓名.VisibleIndex = 3;
            // 
            // gcol成员
            // 
            this.gcol成员.Caption = "成员";
            this.gcol成员.FieldName = "成员";
            this.gcol成员.Name = "gcol成员";
            this.gcol成员.OptionsColumn.ReadOnly = true;
            this.gcol成员.Visible = true;
            this.gcol成员.VisibleIndex = 4;
            // 
            // gcol是否有效
            // 
            this.gcol是否有效.Caption = "是否有效";
            this.gcol是否有效.FieldName = "是否有效";
            this.gcol是否有效.Name = "gcol是否有效";
            this.gcol是否有效.OptionsColumn.ReadOnly = true;
            this.gcol是否有效.Visible = true;
            this.gcol是否有效.VisibleIndex = 5;
            // 
            // gcol创建人
            // 
            this.gcol创建人.Caption = "创建人";
            this.gcol创建人.FieldName = "创建人姓名";
            this.gcol创建人.Name = "gcol创建人";
            this.gcol创建人.OptionsColumn.ReadOnly = true;
            this.gcol创建人.Visible = true;
            this.gcol创建人.VisibleIndex = 6;
            // 
            // gcol创建时间
            // 
            this.gcol创建时间.Caption = "创建时间";
            this.gcol创建时间.FieldName = "创建时间";
            this.gcol创建时间.Name = "gcol创建时间";
            this.gcol创建时间.OptionsColumn.ReadOnly = true;
            this.gcol创建时间.Visible = true;
            this.gcol创建时间.VisibleIndex = 7;
            this.gcol创建时间.Width = 120;
            // 
            // gcol修改人
            // 
            this.gcol修改人.Caption = "修改人";
            this.gcol修改人.FieldName = "修改人姓名";
            this.gcol修改人.Name = "gcol修改人";
            this.gcol修改人.OptionsColumn.ReadOnly = true;
            this.gcol修改人.Visible = true;
            this.gcol修改人.VisibleIndex = 8;
            // 
            // gcol修改时间
            // 
            this.gcol修改时间.Caption = "修改时间";
            this.gcol修改时间.FieldName = "修改时间";
            this.gcol修改时间.Name = "gcol修改时间";
            this.gcol修改时间.OptionsColumn.ReadOnly = true;
            this.gcol修改时间.Visible = true;
            this.gcol修改时间.VisibleIndex = 9;
            this.gcol修改时间.Width = 120;
            // 
            // Frm团队管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 426);
            this.Name = "Frm团队管理";
            this.Text = "团队管理";
            this.Load += new System.EventHandler(this.Frm团队管理NewNew_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit组长.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TActionProject.PagerControl pagerControl1;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit名称;
        private DevExpress.XtraEditors.TextEdit textEdit组长;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private Library.UserControls.DataGridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn gcol所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn gcol团队名称;
        private DevExpress.XtraGrid.Columns.GridColumn gcol组长姓名;
        private DevExpress.XtraGrid.Columns.GridColumn gcol创建人;
        private DevExpress.XtraGrid.Columns.GridColumn gcol创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn gcol是否有效;
        private DevExpress.XtraGrid.Columns.GridColumn gcol修改人;
        private DevExpress.XtraGrid.Columns.GridColumn gcol修改时间;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraEditors.ComboBoxEdit cbo机构;
        private DevExpress.XtraGrid.Columns.GridColumn gcol组长类别;
        private DevExpress.XtraGrid.Columns.GridColumn gcol成员;
        private DevExpress.XtraGrid.Columns.GridColumn gcol团队ID;
        private DevExpress.XtraEditors.SimpleButton but禁用;
    }
}