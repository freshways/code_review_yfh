﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;
using AtomEHR.Common;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.团队管理
{
    public partial class Frm团队角色Edit : frmBase
    {
        bllJTYS团队角色 bll = new bllJTYS团队角色();

        DataSet m_dsRoleInfo = null;
        UpdateType m_updatetype;
        string m_roleid = string.Empty;


        public Frm团队角色Edit(UpdateType updatetype, string roleid)
        {
            InitializeComponent();
            m_updatetype = updatetype;
            m_roleid = roleid;

            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

            lookUpEdit机构.Properties.ValueMember = "机构编号";
            lookUpEdit机构.Properties.DisplayMember = "机构名称";
            lookUpEdit机构.Properties.DataSource = dt所属机构;
            lookUpEdit机构.Properties.Columns.Add(new DevExpress.XtraEditors.Controls.LookUpColumnInfo("机构名称"));

            if (m_updatetype == UpdateType.Modify)
            {
                lookUpEdit机构.Enabled = false;
                m_dsRoleInfo = bll.GetBusinessByKey(m_roleid, false);
                if (m_dsRoleInfo == null || m_dsRoleInfo.Tables.Count == 0 || m_dsRoleInfo.Tables[0].Rows.Count == 0)
                {
                    btn保存.Enabled = false;
                }
                else
                {
                    textEdit名称.Text = m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.角色名称].ToString();
                    txtOrderBy.Text = m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.排序].ToString();
                    lookUpEdit机构.EditValue = m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.所属机构].ToString();
                }
            }
            else
            {
                //TODO:  需要修改机构编号的地方
                //lookUpEdit机构.EditValue = "371321C21008";
                string ssjg = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                lookUpEdit机构.EditValue = ssjg;
                if (ssjg.Length > 6)
                {
                    lookUpEdit机构.Enabled = false;
                }
            }

        }

        private void btn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnSub_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtOrderBy.Text=="0")
                {}
                else
                {
                    txtOrderBy.Text = (Convert.ToInt32(txtOrderBy.Text)-1).ToString();
                }
            }
            catch
            {
                txtOrderBy.Text = "0";
            }
        }

        private void sbtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtOrderBy.Text = (Convert.ToInt32(txtOrderBy.Text) + 1).ToString();
            }
            catch
            {
                txtOrderBy.Text = "0";
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                int num = Convert.ToInt32(txtOrderBy.Text);
                if(num <0)
                {
                    Msg.ShowInformation("排序序号输入错误。");
                    return;
                }
            }
            catch
            {
                Msg.ShowInformation("排序序号输入错误。");
                return;
            }

            if (textEdit名称.Text.Trim().Length == 0)
            {
                MessageBox.Show("名称不允许为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (lookUpEdit机构.EditValue == null)
            {
                MessageBox.Show("未选择所属机构，请确认！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (m_updatetype == UpdateType.Add)
            {
                bll.GetBusinessByKey("-1", true);
                bll.NewBusiness();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队角色.角色名称] = textEdit名称.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队角色.排序] = txtOrderBy.Text.Trim();
                bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS团队角色.所属机构] = lookUpEdit机构.EditValue.ToString();
                bll.Save(bll.CurrentBusiness);
            }
            else if (m_updatetype == UpdateType.Modify)
            {
                m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.角色名称] = textEdit名称.Text.Trim();
                m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.排序] = txtOrderBy.Text.Trim();
                m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.所属机构] = lookUpEdit机构.EditValue.ToString();
                m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.所属机构] = lookUpEdit机构.EditValue.ToString();
                m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.修改人] = Loginer.CurrentUser.用户编码;
                m_dsRoleInfo.Tables[0].Rows[0][tb_JTYS团队角色.修改时间] = bll.ServiceDateTime;
                bll.Save(m_dsRoleInfo);
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
