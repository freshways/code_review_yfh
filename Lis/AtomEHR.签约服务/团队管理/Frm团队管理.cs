﻿using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.团队管理
{
    public partial class Frm团队管理 : AtomEHR.Library.frmBaseBusinessForm
    {
        public Frm团队管理()
        {
            InitializeComponent();
        }

        private void Frm团队管理NewNew_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
            pagerControl1.Height = 34;
        }

        protected override void InitializeForm()
        {
            _BLL = new bllJTYS医生信息();// 业务逻辑层实例
            
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);

            //util.BindingHelper.LookupEditBindDataSource(lookUpEdit性别, DataDictCache.Cache.t性别, "P_CODE", "P_DESC");
            //util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, lookUpEdit性别, "P_CODE", "P_DESC");

            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            //DataTable dt所属机构 = bll机构.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;
            }
        }
        
        protected override bool DoSearchSummary()
        {
            string _strWhere = "";//" and " + tb_JTYS医生信息.是否有效 + "=1";

            string str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            if (!string.IsNullOrWhiteSpace(str机构))
            {
                _strWhere = " and " + tb_JTYS团队概要.所属机构 + "='" + str机构 + "' ";
            }

            if (!string.IsNullOrWhiteSpace(textEdit名称.Text))
            {
                _strWhere = " and " + tb_JTYS团队概要.团队名称 + " like '%" + textEdit名称.Text.Trim() + "%' ";
            }
            if (!string.IsNullOrWhiteSpace(textEdit组长.Text))
            {
                _strWhere = " and " + tb_JTYS团队概要.组长编号 + " like '%" + textEdit组长.Text.Trim() + "%' ";
            }

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS团队概要", "*", _strWhere, tb_JTYS团队概要.ID, "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            gvSummary.BestFitColumns();

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Frm团队添加 frm = new Frm团队添加(UpdateType.Add, Guid.NewGuid().ToString());
            frm.ShowDialog();
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if(selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if(selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }

            string teamid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS团队概要.团队ID).ToString();
            Frm团队添加 frm = new Frm团队添加(UpdateType.Modify, teamid);
            frm.ShowDialog();
        }

        private void but禁用_Click(object sender, EventArgs e)
        {
            try
            {
                int[] selectdIndex = gvSummary.GetSelectedRows();
                if (selectdIndex.Length == 0)
                {
                    Msg.ShowInformation("请选择需要修改的行");
                    return;
                }
                else if (selectdIndex.Length > 1)
                {
                    Msg.ShowInformation("每次只能修改一条数据");
                    return;
                }

                string teamid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS团队概要.团队ID).ToString();
                string docname = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS团队概要.团队名称).ToString();

                if (!Msg.AskQuestion("确定要禁用【" + docname + "】吗？")) return;
                bllJTYS团队概要 _bll2 = new bllJTYS团队概要();
                int count = _bll2.Update是否禁用(teamid, false);

                if (count > 0)
                {
                    DoSearchSummary();
                }
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int[] selectdIndex = gvSummary.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }

            string teamid = gvSummary.GetRowCellValue(selectdIndex[0], tb_JTYS团队概要.团队ID).ToString();
            Frm团队添加 frm = new Frm团队添加(UpdateType.None, teamid);
            frm.ShowDialog();
        }
    }
}
