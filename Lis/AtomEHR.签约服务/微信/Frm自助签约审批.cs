﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Models;
using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.Common;
using DevExpress.XtraEditors;

namespace AtomEHR.签约服务.微信
{
    public partial class Frm自助签约审批 : AtomEHR.Library.frmBaseBusinessForm
    {
        bllJTYS签约信息 bll签约 = new bllJTYS签约信息();
        bllJTYS签约对象类型 bll签约对象 = new bllJTYS签约对象类型();
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        AtomEHR.Business.bllMyUserVs医生签字 bllUserVs签字 = new AtomEHR.Business.bllMyUserVs医生签字();
        bllJTYS自助签约临时表 bll自助签约 = new bllJTYS自助签约临时表();

        string signImage = "";
        
        public Frm自助签约审批()
        {
            InitializeComponent();
        }

        private void Frm自助签约审批_Load(object sender, EventArgs e)
        {
            InitView();
			this.InitializeForm();
        }

        /// <summary>
        /// 检查当前账号是否有审批权限
        /// </summary>

        //团队成员在团队中的id
        private string m_cyid = "";

        private string m_teamid;
        private string m_teamname;
        void InitView()
        {
            #region 绑定机构信息
            try
            {
                AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();

                string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
                DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);

                //DataTable dt所属机构 = bll机构.Get机构树();
                tllue机构.Properties.ValueMember = "机构编号";
                tllue机构.Properties.DisplayMember = "机构名称";

                tllue机构.Properties.DataSource = dt所属机构;
                tllue机构.EditValue = Loginer.CurrentUser.所属机构;

                //判断是否有审批权限
                if (Loginer.CurrentUser.所属机构.Length > 6)
                {
                    string str乡镇机构 = _bll机构信息.Get所在乡镇编码(Loginer.CurrentUser.所属机构);
                    bllJTYS团队Vs公卫人员 bll审批权限 = new bllJTYS团队Vs公卫人员();
                    string teamid = "";
                    string teamName="";
                    string cyid = "";
                    bool isPower = bll审批权限.CheckAuthority(str乡镇机构, Loginer.CurrentUser.用户编码, out teamid, out teamName, out cyid);
                    if(isPower)
                    {
                        labNotice.Visible = true;
                        labNotice.Text = "您具有【"+teamName+"】的审批权限";

                        m_cyid = cyid;
                        m_teamid = teamid;
                        m_teamname = teamName;

                        DataTable tempdt = bllUserVs签字.GetImage(Loginer.CurrentUser.用户编码);
                        var temp = tempdt.Rows.Count > 0 ? tempdt.Rows[0]["s手签"] : null;
                        byte[] arrImage =  temp == DBNull.Value ? (byte[])null : (byte[])temp;
                        Bitmap bmap = (Bitmap)ZipTools.DecompressionObject(arrImage);

                        byte[] signImgebytes;
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                        {
                            bmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                            signImgebytes = ms.GetBuffer();  //byte[]   bytes=   ms.ToArray(); 这两句都可以，至于区别么，下面有解释
                            ms.Close();
                        }

                        if (signImgebytes == null)
                        {
                        }
                        else
                        {
                            signImage = Convert.ToBase64String(signImgebytes);
                        }
                        if(signImage.Length < 20)
                        {
                            sbtnAllow.Enabled = false;
                            sbtnRefuse.Enabled = false;
                            labNotice.Text += "   由于未设置签字对照，故暂时不能进行审批。请使用医院大账号从“用户管理”进行设置。";
                        }
                    }
                    else
                    {
                        sbtnAllow.Visible = false;
                        sbtnRefuse.Visible = false;
                        labNotice.Visible = true;
                    }
                }
                else
                {//局级账号不能进行审批，但可查询全部申请。
                    sbtnAllow.Visible = false;
                    sbtnRefuse.Visible = false;
                    labNotice.Visible = true;
                }
            }
            catch
            {
                //tllue机构.Text = Loginer.CurrentUser.所属机构;
            }

            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                //util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                tllue机构.Enabled = false;
            }

            #endregion

           
            DataTable dtServiceObject = bll签约对象.GetAllValidateData();
            util.ControlsHelper.BindComboxData(dtServiceObject, lue签约对象, tb_JTYS签约对象类型.ID, tb_JTYS签约对象类型.类型名称);

            //绑定签约服务包
            DataTable dt服务包 = bll服务包.GetAllValidateDataWithCodeAndName();
            util.ControlsHelper.BindComboxData(dt服务包, lue服务包类型, tb_JTYS服务包.ServiceID, tb_JTYS服务包.名称);

        }

		protected override void InitializeForm()
        {
            _BLL = new bllJTYS服务包();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();
            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);
        }
        string _strWhere = "";
        protected override bool DoSearchSummary()
        {
            _strWhere = "";


            if (tllue机构.Text != "" && tllue机构.EditValue != null)
            {
                string pgrid = tllue机构.EditValue.ToString();
                if (string.IsNullOrWhiteSpace(pgrid))
                {
                }
                else
                {
                    _strWhere += " AND 录入人机构='" + pgrid + "'";
                }
            }

            if (txt姓名.Text.Contains("'") || txt姓名.Text.Contains("--") || txt姓名.Text.Contains(";"))
            { }
            else if (txt姓名.Text != "")
            {
                _strWhere += " AND (姓名 like'" + DESEncrypt.DES加密(txt姓名.Text) + "%' or 姓名 like'" + this.txt姓名.Text.Trim() + "%') ";
            }

            if(!string.IsNullOrWhiteSpace(de申请日期B.Text))
            {
                _strWhere += " and 创建时间>='" + de申请日期B.Text.Trim() + "' ";
            }

            if(!string.IsNullOrWhiteSpace(de申请日期E.Text))
            {
                _strWhere += " and 创建时间<='" + de申请日期E.Text.Trim() + "' ";
            }

            if(!string.IsNullOrWhiteSpace(de审批日期B.Text))
            {
                _strWhere += " and 审批时间>='" + de审批日期B.Text.Trim() + "' ";
            }

            if (!string.IsNullOrWhiteSpace(de审批日期E.Text))
            {
                _strWhere += " and 审批时间<='" + de审批日期E.Text.Trim() + "' ";
            }

            //if (!string.IsNullOrWhiteSpace(de审批日期B.Text))
            //{
            //    _strWhere += " and 审批日期>='" + de审批日期B.Text.Trim() + "' ";
            //}

            //if (!string.IsNullOrWhiteSpace(de审批日期E.Text))
            //{
            //    _strWhere += " and 审批日期<='" + de审批日期E.Text.Trim() + "' ";
            //}

            if(!string.IsNullOrWhiteSpace(txt身份证号.Text))
            {
                _strWhere += " and 身份证号='"+txt身份证号.Text.Trim()+"' ";
            }


            if(!string.IsNullOrWhiteSpace(txt档案号.Text))
            {
                _strWhere += " and 档案号='" + txt档案号.Text.Trim() + "'";
            }

            string strSO = util.ControlsHelper.GetComboxKey(lue签约对象);
            if(!string.IsNullOrWhiteSpace(strSO))
            {
                _strWhere += " and 签约对象类型='" + strSO + "' ";
            }

            string strPT = util.ControlsHelper.GetComboxKey(lue服务包类型);
            if (!string.IsNullOrWhiteSpace(strPT))
            {
                _strWhere += " and 签约服务包 like '%," + strPT + ",%' ";
            }

            string strZT = lue状态.Text;
            if (!string.IsNullOrWhiteSpace(strZT))
            {
                _strWhere += " and 审批状态 = '" + strZT + "' ";
            }
            

            DataTable dt = this.pagerControl1.GetQueryResultNew("vw_JTYS自助签约临时表", "*", _strWhere, tb_JTYS签约信息.ID, "DESC").Tables[0];

            for (int index = 0; index < dt.Rows.Count; index++ )
            {
                dt.Rows[index]["姓名"] = DESEncrypt.DES解密(dt.Rows[index]["姓名"].ToString());
            }

            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.

            gvSummary.BestFitColumns();

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            _strWhere = "";
            pagerControl1.InitControl();
        }

        

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            de申请日期B.Text = "";
            de申请日期E.Text = "";
            de审批日期B.Text = "";
            de审批日期E.Text = "";
            tllue机构.EditValue = Loginer.CurrentUser.所属机构;
            txt姓名.Text = "";
            txt身份证号.Text = "";
            txt档案号.Text = "";
            util.ControlsHelper.SetComboxData("", lue服务包类型);
            util.ControlsHelper.SetComboxData("", lue签约对象);
            //util.ControlsHelper.SetComboxData("", lue状态);
        }

        private void sbtnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "XLS file(*.xls)|*.xls";//"PDF file(*.pdf)|*.pdf";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                DataTable dt = gcSummary.DataSource as DataTable;
                AtomEHR.Business.bll机构信息 bll = new AtomEHR.Business.bll机构信息();
//                string fileds = @"[ID]
//      ,[档案号]
//      ,[联系电话]
//      ,[健康状况名称]
//      ,[服务包名称]
//      ,[签约日期]
//      ,convert(varchar(20), [生效日期],120) [生效日期]
//      ,convert(varchar(20), [到期日期],120) [到期日期]
//      ,[创建时间]
//      ,[解约人]
//      ,[解约原因]
//      ,[修改人]
//      ,[修改时间]
//      ,[履约状态]
//      ,[磁卡号]
//      ,[姓名]
//      ,[性别名称]
//      ,[身份证号]
//      ,[签约对象类型名称]
//      ,[签约医生姓名]
//      ,[签约状态]
//      ,[团队名称]
//      ,[创建人姓名]
//      ,[修改人姓名]
//      ,[所属机构名称] 档案所属机构名称
//      ,[居住地址]";
                DataTable newtable = new AtomEHR.Business.BLL_Base.bllBase().Get全部数据("vw_JTYS自助签约临时表", "*", _strWhere, "ID", "DESC");
                for (int i = 0; i < newtable.Rows.Count; i++)
                {
                    newtable.Rows[i]["姓名"] = Common.DESEncrypt.DES解密(newtable.Rows[i]["姓名"].ToString());
                    //newtable.Rows[i]["居住地址"] = bll.Return地区名称(bll.Return地区名称(newtable.Rows[i]["居委会"].ToString()) + newtable.Rows[i]["居住地址"].ToString());
                    //newtable.Rows[i]["所属机构"] = bll.Return机构名称(newtable.Rows[i]["所属机构"].ToString());
                    //newtable.Rows[i]["创建人"] = bll.Return用户名称(newtable.Rows[i]["创建人"].ToString());
                }
                gcSummary.DataSource = newtable;
                gcSummary.ExportToXls(dlg.FileName);
                gcSummary.DataSource = dt;
                //view.ExportToXls(dlg.FileName);
                //ExportToExcel(table, dlg.FileName);
            }
        }

        private void tllue机构_EditValueChanged(object sender, EventArgs e)
        {
            //局级管理员的权限
            if (Loginer.CurrentUser.所属机构.Length == 6)
            {
                string rgid = tllue机构.EditValue.ToString();
                DataTable dt服务包 = bll服务包.GetAllValidateDataWithCodeAndNameByRGID(rgid);
                util.ControlsHelper.BindComboxData(dt服务包, lue服务包类型, tb_JTYS服务包.ServiceID, tb_JTYS服务包.名称);
                util.ControlsHelper.SetComboxData("", lue服务包类型);
            }
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {
            Approve("审批通过");
        }

        private void Approve(string spyj)
        {
            int[] indexs = this.gvSummary.GetSelectedRows();
            if (indexs.Length == 0)
            {
                XtraMessageBox.Show("您未选择任何需要审批的记录");
                return;
            }
            List<string> idsList = new List<string>();
            System.Collections.Hashtable hash = new System.Collections.Hashtable();
            for (int ii = 0; ii < indexs.Length; ii++)
            {
                string teamidtemp = this.gvSummary.GetRowCellValue(indexs[ii], gcol团队ID).ToString();
                if(teamidtemp!=m_teamid)
                {
                    Msg.ShowInformation("您只有【"+m_teamname+"】的审批权限，请不要对由其他团队负责的自助签约进行审批。");
                    return;
                }

                string id = this.gvSummary.GetRowCellValue(indexs[ii], gcolID).ToString();
                idsList.Add(id);

                hash[id] = this.gvSummary.GetRowCellValue(indexs[ii], col身份证号).ToString();
            }

            bool checkApproved = bll自助签约.CheckApproved(idsList.ToArray());
            if(checkApproved)
            {
                Msg.ShowInformation("选中的自助签约存在已审批的情况，请重新查询后确认");
                return;
            }

            Frm审批 frm = new Frm审批(spyj, idsList.ToArray(), hash, signImage);
            //frm.ShowDialog();

            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DoSearchSummary();
            }
        }

        private void sbtnRefuse_Click(object sender, EventArgs e)
        {
            Approve("审批不通过");
        }

        private void gvSummary_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0)
                {
                    e.Info.DisplayText = (e.RowHandle + 1).ToString();
                }
            }
        }


    }
}
