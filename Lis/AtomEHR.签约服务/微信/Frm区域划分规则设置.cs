﻿using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.微信
{
    public partial class Frm区域划分规则设置 : AtomEHR.Library.frmBaseBusinessForm
    {
        public Frm区域划分规则设置()
        {
            InitializeComponent();
        }


        private void Frm区域划分规则设置_Load(object sender, EventArgs e)
        {
            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            //DataTable dt所属机构 = bll机构.Get机构("371321");
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            //util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");

            lue机构.Properties.DisplayMember = "机构名称";
            lue机构.Properties.ValueMember = "机构编号";
            lue机构.Properties.DataSource = dt所属机构;

            //判断当前操作者是否为医院公卫人员。
            DataRow[] drs = dt所属机构.Select("机构编号='"+Loginer.CurrentUser.所属机构+"'");
            if (Loginer.CurrentUser.所属机构.Length == 6)//局级人员
            {
                this.radioGroup1.Properties.ReadOnly = true;
            }
            else if(drs.Length>=0)//乡镇医院公卫人员
            {
                this.lue机构.Properties.ReadOnly = true; //不允许调整机构
                this.lue机构.EditValue = Loginer.CurrentUser.所属机构;
                this.radioGroup1.EditValueChanged +=radioGroup1_EditValueChanged; //允许修改管理规则
            }
            else//其他人员
            {
                this.radioGroup1.Properties.ReadOnly = true;
                this.lue机构.Properties.ReadOnly = true;
            }

            
        }

        bllJTYS全局参数Local bll = new bllJTYS全局参数Local();

        private void lue机构_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                string jgid = lue机构.EditValue.ToString();
                string value = bll.GetValueByCode(jgid, "TeamManageType");
                radioGroup1.EditValue = value;
            }
            catch(Exception ex)
            {
                Msg.ShowInformation(ex.Message);
            }
        }

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                string rgid = lue机构.EditValue.ToString();
                string value = this.radioGroup1.EditValue.ToString();
                bll.InsertOrUpdateCodeValue(rgid, "TeamManageType", value);
            }
            catch (Exception ex)
            {
                Msg.ShowInformation(ex.Message);
            }
        }


        private void sbtn刷新_Click(object sender, EventArgs e)
        {
            lue机构_EditValueChanged(null, null);
        }
    }
}
