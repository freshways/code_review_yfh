﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;
using System.Collections;
//using AtomEHR.WebServiceReference.WS_QY;
//using AtomEHR.WebServiceReference;

namespace AtomEHR.签约服务.微信
{
    public partial class Frm审批 : XtraForm
    {
        private string[] m_arrIDs;
        private string m_spzt;
        private bllJTYS自助签约临时表 bll自助 = new bllJTYS自助签约临时表();
        private Hashtable m_hash;
        private string m_signImage;
        public Frm审批(string spyj, string[] ids, Hashtable hash, string signImage)
        {
            InitializeComponent();
            this.cbo审批意见.Text = spyj;

            m_arrIDs = ids;
            m_spzt = spyj;
            m_hash = hash;
            m_signImage = signImage;
        }

        private void sbtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnOK_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(cbo审批意见.Text))
            {
                XtraMessageBox.Show("请选择审批意见");
                return;
            }

            if(cbo审批意见.Text=="审批不通过" && string.IsNullOrWhiteSpace(memoEdit审批备注.Text))
            {
                XtraMessageBox.Show("审批不通过时，请填写审批备注");
                return;
            }

            bool ret = false;
            if (m_spzt == "审批不通过")
            {
                ret = bll自助.Update审批不通过(memoEdit审批备注.Text.Trim(), m_arrIDs);

            }
            else if (m_spzt == "审批通过")
            {
                ret = bll自助.Update审批通过(memoEdit审批备注.Text.Trim(), m_arrIDs, m_signImage);
            }
            else
            {
            }

            if(ret)
            {
                if(checkEdit1.Checked)
                {
                    SendWeChatMessage(memoEdit审批备注.Text.Trim());
                }
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void Frm审批_Load(object sender, EventArgs e)
        {

        }

        
        private void SendWeChatMessage(string remark)
        {
            //using (JeecgWServiceIClient wechat = SoapClientFactory.CreateQYYSServiceClient())
            //{
            //    string[] arrstr = new string[] { "自助签约审批", bll自助.ServiceDateTime };
            //    if (m_spzt == "审批通过")
            //    {
            //        for (int index = 0; index < m_arrIDs.Length; index++)
            //        {
            //            wechat.sendWxMessage("yFwL5QocGuRn9jknf0_9vNXoAYYnKNxy361Vle0MiWc", m_hash[m_arrIDs[index]].ToString(), "", "您好，你的自助签约申请审批通过", remark, "", arrstr);
            //        }
            //    }
            //    else if (m_spzt == "审批不通过")
            //    {
            //        for (int index = 0; index < m_arrIDs.Length; index++)
            //        {
            //            wechat.sendWxMessage("yFwL5QocGuRn9jknf0_9vNXoAYYnKNxy361Vle0MiWc", m_hash[m_arrIDs[index]].ToString(), "", "您好，你的自助签约申请未审批通过", remark, "", arrstr);
            //        }
            //    }
            //    else
            //    { }
            //}
        }
    }
}
