﻿using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.微信
{
    public partial class Frm自助签约审批设置 : AtomEHR.Library.frmBaseBusinessForm
    {
        bllJTYS团队概要 _bll团队 = new bllJTYS团队概要();
        public Frm自助签约审批设置()
        {
            InitializeComponent();
        }

        private void Frm自助签约审批设置_Load(object sender, EventArgs e)
        {
            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");
            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;

                this.gcTeam.DoubleClick += new System.EventHandler(this.gcTeam_DoubleClick);
            }
        }
        string str机构 = "";
        private void btnQuery_Click(object sender, EventArgs e)
        {
            str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            try
            {
                if(string.IsNullOrWhiteSpace(str机构)){
                    Msg.ShowInformation("请先设置机构查询条件");
                    return;
                }
                //if (!string.IsNullOrWhiteSpace(textEdit名称.Text))
                //{
                //    string strTname = "";
                //    strTname = " and " + tb_JTYS团队概要.团队名称 + " like '%" + textEdit名称.Text.Trim() + "%' ";
                //}
                DataTable dtTeam = _bll团队.GetTeamInfoByRgid3(str机构);
                //DataTable dtTeamPeople=
                this.gcTeam.DataSource = dtTeam;

            }
            catch (Exception ex)
            {
                Msg.ShowInformation(ex.Message);
            }
        }

        private void gcTeam_DoubleClick(object sender, EventArgs e)
        {
           //try
            if (gvTeam.DataRowCount == 0 || gvTeam.FocusedRowHandle < 0 || this.gvTeam.DataSource == null)
            {
                return;
            }
            str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            if (str机构 != Loginer.CurrentUser.所属机构)
            {
                Msg.ShowInformation("对不起，您没有修改权限。区域管理只允许各医院自行设置。");
                //e.Cancel = true;
                return;
            }
            if (!string.IsNullOrWhiteSpace(this.gvTeam.GetFocusedRowCellValue("Account").ToString()))
            {
                 Msg.ShowInformation("对不起，此成员已经设置账号。");
                return;
            }

            string 团队ID = this.gvTeam.GetFocusedRowCellValue("团队ID").ToString();
            string 团队名称 = this.gvTeam.GetFocusedRowCellValue("团队名称").ToString();
            string 成员姓名 = this.gvTeam.GetFocusedRowCellValue("成员姓名").ToString();
            string 成员ID = this.gvTeam.GetFocusedRowCellValue("成员ID").ToString();
            string 机构名称 = cbo机构.Text;
            string btn = "1";
            //str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            //string 机构名称 = cbo机构.Text;
            Frm自助签约审批保存 Frm自助签约审批保存 = new Frm自助签约审批保存(团队ID, 团队名称, 成员姓名, 成员ID, str机构, 机构名称, btn);
            Frm自助签约审批保存.ShowDialog();
            
        }

        private void sbtnEdit_Click(object sender, EventArgs e)
        {
            if (gvTeam.DataRowCount == 0 || gvTeam.FocusedRowHandle < 0 || this.gvTeam.DataSource == null)
            {
                return;
            }
            str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            if (str机构 != Loginer.CurrentUser.所属机构)
            {
                Msg.ShowInformation("对不起，您没有修改权限。区域管理只允许各医院自行设置。");
                return;
            }
            int[] selectdIndex = gvTeam.GetSelectedRows();
            if (selectdIndex.Length == 0)
            {
                Msg.ShowInformation("请选择需要修改的行");
                return;
            }
            else if (selectdIndex.Length > 1)
            {
                Msg.ShowInformation("每次只能修改一条数据");
                return;
            }
            string 团队ID = this.gvTeam.GetFocusedRowCellValue("团队ID").ToString();
            string 团队名称 = this.gvTeam.GetFocusedRowCellValue("团队名称").ToString();
            string 成员姓名 = this.gvTeam.GetFocusedRowCellValue("成员姓名").ToString();
            string 成员ID = this.gvTeam.GetFocusedRowCellValue("成员ID").ToString();
            string 机构名称 = cbo机构.Text;
            string btn="0";
            Frm自助签约审批保存 Frm自助签约审批保存 = new Frm自助签约审批保存(团队ID, 团队名称, 成员姓名, 成员ID, str机构, 机构名称, btn);
            Frm自助签约审批保存.ShowDialog();
        }

    }
}
