﻿namespace AtomEHR.签约服务.微信
{
    partial class Frm团队卫生室划分
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm团队卫生室划分));
            this.label1 = new System.Windows.Forms.Label();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cbo机构 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gcTeam = new DevExpress.XtraGrid.GridControl();
            this.gvTeam = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolTeamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolTeamLeader = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcWss = new DevExpress.XtraGrid.GridControl();
            this.gvWss = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolRGID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolRGName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolSelect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcWss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcWss);
            this.tpSummary.Controls.Add(this.gcTeam);
            this.tpSummary.Controls.Add(this.panelControl1);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Size = new System.Drawing.Size(799, 453);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(805, 459);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(805, 459);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(805, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(627, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(430, 2);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(199, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(533, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "本窗口用于设置每个团队负责哪些卫生室。仅管理规则设置为“按卫生室划分团队管理区域”时生效。";
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.layoutControl1);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(799, 62);
            this.gcFindGroup.TabIndex = 134;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cbo机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(241, 163, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup3;
            this.layoutControl1.Size = new System.Drawing.Size(795, 64);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cbo机构
            // 
            this.cbo机构.Location = new System.Drawing.Point(75, 32);
            this.cbo机构.Name = "cbo机构";
            this.cbo机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo机构.Properties.NullText = "[EditValue is null]";
            this.cbo机构.Properties.PopupSizeable = true;
            this.cbo机构.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo机构.Size = new System.Drawing.Size(159, 20);
            this.cbo机构.StyleController = this.layoutControl1;
            this.cbo机构.TabIndex = 12;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "医生查询";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem7});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(795, 64);
            this.layoutControlGroup3.Text = "团队查询";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(226, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(549, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.cbo机构;
            this.layoutControlItem7.CustomizationFormText = "所属机构：";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem7.Text = "所属机构：";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(60, 14);
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(97, 14);
            this.btnQuery.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 22);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.btnQuery);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 62);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(799, 48);
            this.panelControl1.TabIndex = 136;
            // 
            // gcTeam
            // 
            this.gcTeam.Dock = System.Windows.Forms.DockStyle.Left;
            this.gcTeam.Location = new System.Drawing.Point(0, 110);
            this.gcTeam.MainView = this.gvTeam;
            this.gcTeam.Name = "gcTeam";
            this.gcTeam.Size = new System.Drawing.Size(436, 343);
            this.gcTeam.TabIndex = 137;
            this.gcTeam.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTeam});
            // 
            // gvTeam
            // 
            this.gvTeam.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolTeamID,
            this.gcolTeamName,
            this.gcolTeamLeader});
            this.gvTeam.GridControl = this.gcTeam;
            this.gvTeam.Name = "gvTeam";
            this.gvTeam.OptionsBehavior.Editable = false;
            this.gvTeam.OptionsView.ShowGroupPanel = false;
            this.gvTeam.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvTeam_FocusedRowChanged);
            // 
            // gcolTeamID
            // 
            this.gcolTeamID.Caption = "TeamID";
            this.gcolTeamID.FieldName = "团队ID";
            this.gcolTeamID.Name = "gcolTeamID";
            // 
            // gcolTeamName
            // 
            this.gcolTeamName.Caption = "团队名称";
            this.gcolTeamName.FieldName = "团队名称";
            this.gcolTeamName.Name = "gcolTeamName";
            this.gcolTeamName.Visible = true;
            this.gcolTeamName.VisibleIndex = 0;
            // 
            // gcolTeamLeader
            // 
            this.gcolTeamLeader.Caption = "团队长";
            this.gcolTeamLeader.FieldName = "团队长";
            this.gcolTeamLeader.Name = "gcolTeamLeader";
            this.gcolTeamLeader.Visible = true;
            this.gcolTeamLeader.VisibleIndex = 1;
            // 
            // gcWss
            // 
            this.gcWss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcWss.Location = new System.Drawing.Point(436, 110);
            this.gcWss.MainView = this.gvWss;
            this.gcWss.Name = "gcWss";
            this.gcWss.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gcWss.Size = new System.Drawing.Size(363, 343);
            this.gcWss.TabIndex = 138;
            this.gcWss.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvWss});
            // 
            // gvWss
            // 
            this.gvWss.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolRGID,
            this.gcolRGName,
            this.gcolSelect});
            this.gvWss.GridControl = this.gcWss;
            this.gvWss.Name = "gvWss";
            this.gvWss.OptionsView.ShowGroupPanel = false;
            // 
            // gcolRGID
            // 
            this.gcolRGID.Caption = "机构编码";
            this.gcolRGID.FieldName = "机构编号";
            this.gcolRGID.Name = "gcolRGID";
            this.gcolRGID.OptionsColumn.AllowEdit = false;
            this.gcolRGID.Visible = true;
            this.gcolRGID.VisibleIndex = 0;
            // 
            // gcolRGName
            // 
            this.gcolRGName.Caption = "机构名称";
            this.gcolRGName.FieldName = "机构名称";
            this.gcolRGName.Name = "gcolRGName";
            this.gcolRGName.OptionsColumn.AllowEdit = false;
            this.gcolRGName.Visible = true;
            this.gcolRGName.VisibleIndex = 1;
            // 
            // gcolSelect
            // 
            this.gcolSelect.Caption = "是否选择";
            this.gcolSelect.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gcolSelect.FieldName = "selected";
            this.gcolSelect.Name = "gcolSelect";
            this.gcolSelect.Visible = true;
            this.gcolSelect.VisibleIndex = 2;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEdit1_CheckedChanged);
            this.repositoryItemCheckEdit1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemCheckEdit1_EditValueChanging_1);
            // 
            // Frm团队卫生室划分
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 485);
            this.Name = "Frm团队卫生室划分";
            this.Text = "团队卫生室设置";
            this.Load += new System.EventHandler(this.Frm团队卫生室划分_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcWss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbo机构;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gcTeam;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTeam;
        private DevExpress.XtraGrid.GridControl gcWss;
        private DevExpress.XtraGrid.Views.Grid.GridView gvWss;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTeamID;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn gcolTeamLeader;
        private DevExpress.XtraGrid.Columns.GridColumn gcolRGID;
        private DevExpress.XtraGrid.Columns.GridColumn gcolRGName;
        private DevExpress.XtraGrid.Columns.GridColumn gcolSelect;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;

    }
}