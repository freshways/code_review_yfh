﻿using AtomEHR.Business.Security;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.签约服务.Business;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.微信
{
    public partial class Frm自助签约审批保存 : XtraForm
    {
        private string 团队ID;
        private string 团队名称;
        private string 成员姓名;
        private string str机构;
        private string 成员ID;
        private string 机构名称;
        private string btn;

        public Frm自助签约审批保存()
        {
            InitializeComponent();
        }

        //public Frm自助签约审批保存(string 团队ID, string 团队名称, string 成员姓名, string 成员ID, string str机构)
        //{
        //    InitializeComponent();
        //    // TODO: Complete member initialization
        //    this.团队ID = 团队ID;
        //    this.团队名称 = 团队名称;
        //    this.成员姓名 = 成员姓名;
        //    this.成员ID = 成员ID;
        //    this.str机构 = str机构;
        //}

        public Frm自助签约审批保存(string 团队ID, string 团队名称, string 成员姓名, string 成员ID, string str机构, string 机构名称, string btn)
        {
            InitializeComponent();
            // TODO: Complete member initialization
            this.团队ID = 团队ID;
            this.团队名称 = 团队名称;
            this.成员姓名 = 成员姓名;
            this.成员ID = 成员ID;
            this.str机构 = str机构;
            this.机构名称 = 机构名称;
            this.btn = btn;
        }

        private void sbtnSave_Click(object sender, EventArgs e)
        {
            if (cbo公卫人员.EditValue==null || string.IsNullOrWhiteSpace(cbo公卫人员.EditValue.ToString()))
            {
                Msg.ShowInformation("请选择账号!");
            }
            string yhbm = cbo公卫人员.EditValue.ToString();
            string username = cbo公卫人员.Text;
            if(btn=="1"){
                
                //string Bool = "1";
                bllJTYS团队Vs公卫人员 bll团队Vs公卫人员 = new bllJTYS团队Vs公卫人员();
                //bool newvalue = Convert.ToBoolean(e.NewValue);


                bool ret = bll团队Vs公卫人员.InsertOrUpdate(str机构, 团队ID, 团队名称, 成员姓名, 成员ID, yhbm, username, btn);
                if (!ret)
                {
                    Msg.ShowInformation("更新失败");
                }
                this.Close();
            }
            if(btn=="0"){
                //string yhbm = cbo公卫人员.EditValue.ToString();
                //string username = cbo公卫人员.Text;
                bllJTYS团队Vs公卫人员 bll团队Vs公卫人员 = new bllJTYS团队Vs公卫人员();
                //bool newvalue = Convert.ToBoolean(e.NewValue);
                string Bool = "1";

                bool ret = bll团队Vs公卫人员.InsertandUpdate(str机构, 团队ID, 团队名称, 成员姓名, 成员ID, yhbm, username, btn, Bool);
                if (!ret)
                {
                    Msg.ShowInformation("更新失败");
                }
                this.Close();
            }
            

            

        }

        private void sbtn取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm自助签约审批保存_Load(object sender, EventArgs e)
        {
            bllUser blluser= new bllUser();
            text团队.Text = 团队名称;
            text成员.Text = 成员姓名;
            DataTable useraccont = blluser.GetwxUser(str机构);

            cbo公卫人员.Properties.ValueMember = "用户编码";
            cbo公卫人员.Properties.DisplayMember = "UserName";
            //cbo机构.Properties.dis

            cbo公卫人员.Properties.DataSource = useraccont;
            //util.ControlsHelper.BindComboxData(useraccont, cbo机构, "机构编号", "机构名称");
            //if (Loginer.CurrentUser.所属机构.Length > 6)
            //{
            //    util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
            //    cbo机构.Enabled = false;
            //}
            //string sql = "SELECT [Account],[用户编码],[UserName],[IsLocked] FROM [tb_MyUser] where 所属机构=@jgbh and IsLocked='0'";
            //SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            //cmd.AddParam("@jgbh", SqlDbType.NVarChar, str机构);
            //DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, TUser.__TableName);
            //text公卫账号.va
            
        }
    }
}
