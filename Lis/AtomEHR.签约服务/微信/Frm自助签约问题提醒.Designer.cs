﻿namespace AtomEHR.签约服务.微信
{
    partial class Frm自助签约问题提醒
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm自助签约问题提醒));
            this.label1 = new System.Windows.Forms.Label();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cboHandle = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo机构 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.pagerControl1 = new TActionProject.PagerControl();
            this.gc个人健康档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcol所属医院 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol档案所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol档案机构名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol居住地址编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol详细地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol提醒内容 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol是否处理 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol处理人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol处理时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcolID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboHandle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gc个人健康档案);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Controls.Add(this.label1);
            this.tpSummary.Size = new System.Drawing.Size(737, 517);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(743, 523);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(743, 523);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(743, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(565, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(368, 2);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(153, 251);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(431, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "用来记录哪些人员签约时未根据档案所属卫生院、地区或卫生室 检索到服务团队";
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.layoutControl1);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(737, 70);
            this.gcFindGroup.TabIndex = 131;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cboHandle);
            this.layoutControl1.Controls.Add(this.cbo机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(271, 167, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(733, 66);
            this.layoutControl1.TabIndex = 35;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cboHandle
            // 
            this.cboHandle.Location = new System.Drawing.Point(361, 32);
            this.cboHandle.Name = "cboHandle";
            this.cboHandle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboHandle.Properties.Items.AddRange(new object[] {
            "",
            "已处理",
            "未处理"});
            this.cboHandle.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboHandle.Size = new System.Drawing.Size(98, 20);
            this.cboHandle.StyleController = this.layoutControl1;
            this.cboHandle.TabIndex = 8;
            // 
            // cbo机构
            // 
            this.cbo机构.Location = new System.Drawing.Point(87, 32);
            this.cbo机构.Name = "cbo机构";
            this.cbo机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo机构.Size = new System.Drawing.Size(195, 20);
            this.cbo机构.StyleController = this.layoutControl1;
            this.cbo机构.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "签约服务查询";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(733, 66);
            this.layoutControlGroup1.Text = "查询条件";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.cbo机构;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(274, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.cboHandle;
            this.layoutControlItem2.CustomizationFormText = "是否已处理：";
            this.layoutControlItem2.Location = new System.Drawing.Point(274, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(177, 26);
            this.layoutControlItem2.Text = "是否已处理：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(72, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(451, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(262, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btnUpdate);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 70);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(737, 30);
            this.flowLayoutPanel1.TabIndex = 132;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(103, 3);
            this.btnQuery.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 22);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(184, 3);
            this.btnUpdate.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(99, 22);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "标记已处理";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 477);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 26427);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 15;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(737, 40);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 133;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // gc个人健康档案
            // 
            this.gc个人健康档案.AllowBandedGridColumnSort = false;
            this.gc个人健康档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc个人健康档案.IsBestFitColumns = true;
            this.gc个人健康档案.Location = new System.Drawing.Point(0, 100);
            this.gc个人健康档案.MainView = this.gvSummary;
            this.gc个人健康档案.Name = "gc个人健康档案";
            this.gc个人健康档案.ShowContextMenu = true;
            this.gc个人健康档案.Size = new System.Drawing.Size(737, 377);
            this.gc个人健康档案.StrWhere = "";
            this.gc个人健康档案.TabIndex = 134;
            this.gc个人健康档案.UseCheckBox = true;
            this.gc个人健康档案.View = "";
            this.gc个人健康档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gvSummary.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gvSummary.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gvSummary.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.DetailTip.Options.UseFont = true;
            this.gvSummary.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Empty.Options.UseFont = true;
            this.gvSummary.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.EvenRow.Options.UseFont = true;
            this.gvSummary.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gvSummary.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FilterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FixedLine.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedCell.Options.UseFont = true;
            this.gvSummary.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.FocusedRow.Options.UseFont = true;
            this.gvSummary.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gvSummary.Appearance.FooterPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupButton.Options.UseFont = true;
            this.gvSummary.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupFooter.Options.UseFont = true;
            this.gvSummary.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupPanel.Options.UseFont = true;
            this.gvSummary.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.GroupRow.Options.UseFont = true;
            this.gvSummary.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvSummary.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gvSummary.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.HorzLine.Options.UseFont = true;
            this.gvSummary.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.OddRow.Options.UseFont = true;
            this.gvSummary.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Preview.Options.UseFont = true;
            this.gvSummary.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.Row.Options.UseFont = true;
            this.gvSummary.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.RowSeparator.Options.UseFont = true;
            this.gvSummary.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.SelectedRow.Options.UseFont = true;
            this.gvSummary.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.TopNewRow.Options.UseFont = true;
            this.gvSummary.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.VertLine.Options.UseFont = true;
            this.gvSummary.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gvSummary.Appearance.ViewCaption.Options.UseFont = true;
            this.gvSummary.ColumnPanelRowHeight = 30;
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcol所属医院,
            this.gcol档案号,
            this.gcol档案所属机构,
            this.gcol档案机构名称,
            this.gcol居住地址编码,
            this.gcol详细地址,
            this.gcol提醒内容,
            this.gcol创建时间,
            this.gcol是否处理,
            this.gcol处理人,
            this.gcol处理时间,
            this.gcolID});
            this.gvSummary.GridControl = this.gc个人健康档案;
            this.gvSummary.GroupPanelText = "DragColumn";
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSummary.OptionsView.EnableAppearanceOddRow = true;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            // 
            // gcol所属医院
            // 
            this.gcol所属医院.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol所属医院.AppearanceHeader.Options.UseFont = true;
            this.gcol所属医院.Caption = "所属医院";
            this.gcol所属医院.FieldName = "所属医院";
            this.gcol所属医院.Name = "gcol所属医院";
            this.gcol所属医院.OptionsColumn.ReadOnly = true;
            this.gcol所属医院.Visible = true;
            this.gcol所属医院.VisibleIndex = 0;
            // 
            // gcol档案号
            // 
            this.gcol档案号.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.gcol档案号.AppearanceCell.Options.UseBackColor = true;
            this.gcol档案号.AppearanceCell.Options.UseTextOptions = true;
            this.gcol档案号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol档案号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol档案号.AppearanceHeader.Options.UseFont = true;
            this.gcol档案号.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol档案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol档案号.Caption = "档案号";
            this.gcol档案号.FieldName = "档案号";
            this.gcol档案号.Name = "gcol档案号";
            this.gcol档案号.OptionsColumn.ReadOnly = true;
            this.gcol档案号.Visible = true;
            this.gcol档案号.VisibleIndex = 1;
            this.gcol档案号.Width = 127;
            // 
            // gcol档案所属机构
            // 
            this.gcol档案所属机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol档案所属机构.AppearanceHeader.Options.UseFont = true;
            this.gcol档案所属机构.Caption = "档案所属机构";
            this.gcol档案所属机构.FieldName = "档案所属机构";
            this.gcol档案所属机构.Name = "gcol档案所属机构";
            this.gcol档案所属机构.Visible = true;
            this.gcol档案所属机构.VisibleIndex = 2;
            this.gcol档案所属机构.Width = 150;
            // 
            // gcol档案机构名称
            // 
            this.gcol档案机构名称.AppearanceCell.Options.UseTextOptions = true;
            this.gcol档案机构名称.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol档案机构名称.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol档案机构名称.AppearanceHeader.Options.UseFont = true;
            this.gcol档案机构名称.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol档案机构名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol档案机构名称.Caption = "档案机构名称";
            this.gcol档案机构名称.FieldName = "档案机构名称";
            this.gcol档案机构名称.Name = "gcol档案机构名称";
            this.gcol档案机构名称.OptionsColumn.ReadOnly = true;
            this.gcol档案机构名称.Visible = true;
            this.gcol档案机构名称.VisibleIndex = 3;
            // 
            // gcol居住地址编码
            // 
            this.gcol居住地址编码.AppearanceCell.Options.UseTextOptions = true;
            this.gcol居住地址编码.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol居住地址编码.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol居住地址编码.AppearanceHeader.Options.UseFont = true;
            this.gcol居住地址编码.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol居住地址编码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol居住地址编码.Caption = "居住地址编码";
            this.gcol居住地址编码.FieldName = "居住地址编码";
            this.gcol居住地址编码.Name = "gcol居住地址编码";
            this.gcol居住地址编码.OptionsColumn.ReadOnly = true;
            this.gcol居住地址编码.Visible = true;
            this.gcol居住地址编码.VisibleIndex = 4;
            this.gcol居住地址编码.Width = 100;
            // 
            // gcol详细地址
            // 
            this.gcol详细地址.AppearanceCell.Options.UseTextOptions = true;
            this.gcol详细地址.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol详细地址.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol详细地址.AppearanceHeader.Options.UseFont = true;
            this.gcol详细地址.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol详细地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol详细地址.Caption = "详细地址";
            this.gcol详细地址.FieldName = "详细地址";
            this.gcol详细地址.Name = "gcol详细地址";
            this.gcol详细地址.OptionsColumn.ReadOnly = true;
            this.gcol详细地址.Visible = true;
            this.gcol详细地址.VisibleIndex = 5;
            this.gcol详细地址.Width = 140;
            // 
            // gcol提醒内容
            // 
            this.gcol提醒内容.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol提醒内容.AppearanceHeader.Options.UseFont = true;
            this.gcol提醒内容.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol提醒内容.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol提醒内容.Caption = "提醒内容";
            this.gcol提醒内容.FieldName = "提醒内容";
            this.gcol提醒内容.Name = "gcol提醒内容";
            this.gcol提醒内容.OptionsColumn.ReadOnly = true;
            this.gcol提醒内容.Visible = true;
            this.gcol提醒内容.VisibleIndex = 6;
            this.gcol提醒内容.Width = 163;
            // 
            // gcol创建时间
            // 
            this.gcol创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.gcol创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol创建时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol创建时间.AppearanceHeader.Options.UseFont = true;
            this.gcol创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.gcol创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcol创建时间.Caption = "创建时间";
            this.gcol创建时间.FieldName = "创建时间";
            this.gcol创建时间.Name = "gcol创建时间";
            this.gcol创建时间.OptionsColumn.ReadOnly = true;
            this.gcol创建时间.Visible = true;
            this.gcol创建时间.VisibleIndex = 7;
            // 
            // gcol是否处理
            // 
            this.gcol是否处理.Caption = "是否处理";
            this.gcol是否处理.FieldName = "是否处理";
            this.gcol是否处理.Name = "gcol是否处理";
            this.gcol是否处理.Visible = true;
            this.gcol是否处理.VisibleIndex = 8;
            // 
            // gcol处理人
            // 
            this.gcol处理人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol处理人.AppearanceHeader.Options.UseFont = true;
            this.gcol处理人.Caption = "处理人";
            this.gcol处理人.FieldName = "处理人";
            this.gcol处理人.Name = "gcol处理人";
            this.gcol处理人.Visible = true;
            this.gcol处理人.VisibleIndex = 9;
            // 
            // gcol处理时间
            // 
            this.gcol处理时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gcol处理时间.AppearanceHeader.Options.UseFont = true;
            this.gcol处理时间.Caption = "处理时间";
            this.gcol处理时间.FieldName = "处理时间";
            this.gcol处理时间.Name = "gcol处理时间";
            this.gcol处理时间.Visible = true;
            this.gcol处理时间.VisibleIndex = 10;
            // 
            // gcolID
            // 
            this.gcolID.Caption = "ID";
            this.gcolID.FieldName = "ID";
            this.gcolID.Name = "gcolID";
            // 
            // Frm自助签约问题提醒
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 549);
            this.Name = "Frm自助签约问题提醒";
            this.Text = "自助签约问题提醒";
            this.Load += new System.EventHandler(this.Frm自助签约问题提醒_Load);
            this.tpSummary.ResumeLayout(false);
            this.tpSummary.PerformLayout();
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboHandle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;
        private TActionProject.PagerControl pagerControl1;
        private Library.UserControls.DataGridControl gc个人健康档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn gcol所属医院;
        private DevExpress.XtraGrid.Columns.GridColumn gcol档案号;
        private DevExpress.XtraGrid.Columns.GridColumn gcol档案所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn gcol档案机构名称;
        private DevExpress.XtraGrid.Columns.GridColumn gcol居住地址编码;
        private DevExpress.XtraGrid.Columns.GridColumn gcol详细地址;
        private DevExpress.XtraGrid.Columns.GridColumn gcol提醒内容;
        private DevExpress.XtraGrid.Columns.GridColumn gcol创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn gcol处理人;
        private DevExpress.XtraGrid.Columns.GridColumn gcol处理时间;
        private DevExpress.XtraEditors.ComboBoxEdit cboHandle;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraGrid.Columns.GridColumn gcol是否处理;
        private DevExpress.XtraEditors.ComboBoxEdit cbo机构;
        private DevExpress.XtraGrid.Columns.GridColumn gcolID;

    }
}