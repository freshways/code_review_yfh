﻿using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;

namespace AtomEHR.签约服务.微信
{
    public partial class Frm团队地区划分 : AtomEHR.Library.frmBaseBusinessForm
    {

        bllJTYS团队概要 _bll团队 = new bllJTYS团队概要();
        bllJTYS团队地区管理关系 bll团队地区 = new bllJTYS团队地区管理关系();
        public Frm团队地区划分()
        {
            InitializeComponent();
        }

        private void Frm团队地区划分_Load(object sender, EventArgs e)
        {
            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");
            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;
            }
        }

        private void repositoryItemCheckEdit1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        string _str机构 = "";
        private void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                _str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
                if (string.IsNullOrWhiteSpace(_str机构))
                {
                    Msg.ShowInformation("请先设置机构查询条件");
                    return;
                }

                DataTable dtArea = bll团队地区.Get辖区内区域(_str机构);
                dtArea.Columns.Add("selected", typeof(System.Boolean));
                for (int index = 0; index < dtArea.Rows.Count; index++)
                {
                    dtArea.Rows[index]["selected"] = false;
                }
                this.gcArea.DataSource = dtArea;

                DataTable dt = _bll团队.GetTeamInfoByRgid(_str机构);
                this.gcTeam.DataSource = dt;

                gvTeam_FocusedRowChanged(null, null);
            }
            catch(Exception ex)
            {
                Msg.ShowInformation(ex.Message);
            }

        }

        private void gvTeam_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gvTeam.DataRowCount == 0 || gvTeam.FocusedRowHandle < 0 || this.gcArea.DataSource==null)
            {
                return;
            }

            string teamid = gvTeam.GetRowCellValue(gvTeam.FocusedRowHandle, "团队ID").ToString();

            DataTable dtManagedArea = bll团队地区.GetAreaManagedByTeam(teamid);

            DataTable dtArea = this.gcArea.DataSource as DataTable;
            for (int index = 0; index < dtArea.Rows.Count; index++)
            {
                DataRow[] drs = dtManagedArea.Select("地区编码='" + dtArea.Rows[index]["地区编码"] + "'");
                if (drs.Length > 0)
                {
                    dtArea.Rows[index]["selected"] = true;
                }
                else
                {
                    dtArea.Rows[index]["selected"] = false;
                }

            }
        }

        private void repositoryItemCheckEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            _str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            if (_str机构 != Loginer.CurrentUser.所属机构)
            {
                Msg.ShowInformation("对不起，您没有修改权限。区域管理只允许各医院自行设置。");
                e.Cancel = true;
                return;
            }

            if (gvTeam.DataRowCount == 0)
            {
                Msg.ShowInformation("对不起，未选择团队。");
                e.Cancel = true;
                return;
            }

            string teamid = this.gvTeam.GetRowCellValue(this.gvTeam.FocusedRowHandle, "团队ID").ToString();
            string areaid = this.gvArea.GetFocusedRowCellValue("地区编码").ToString();
            bool newvalue = Convert.ToBoolean(e.NewValue);

            bool ret = bll团队地区.InsertOrUpdate(teamid, areaid, newvalue);
            if (!ret)
            {
                Msg.ShowInformation("更新失败");
            }

        }
    }
}
