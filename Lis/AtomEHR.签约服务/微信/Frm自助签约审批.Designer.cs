﻿namespace AtomEHR.签约服务.微信
{
    partial class Frm自助签约审批
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm自助签约审批));
            this.pagerControl1 = new TActionProject.PagerControl();
            this.gcFindGroup = new DevExpress.XtraEditors.PanelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.de审批日期E = new DevExpress.XtraEditors.DateEdit();
            this.de审批日期B = new DevExpress.XtraEditors.DateEdit();
            this.de申请日期E = new DevExpress.XtraEditors.DateEdit();
            this.de申请日期B = new DevExpress.XtraEditors.DateEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.lue签约对象 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lue服务包类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lue状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tllue机构 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnQuery = new DevExpress.XtraEditors.SimpleButton();
            this.btnEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnAllow = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnRefuse = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnExport = new DevExpress.XtraEditors.SimpleButton();
            this.labNotice = new DevExpress.XtraEditors.LabelControl();
            this.gcSummary = new DevExpress.XtraGrid.GridControl();
            this.gvSummary = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol团队ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcol团队名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col健康档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col签约包 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col生效日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col审批人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col到期日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tpSummary.SuspendLayout();
            this.pnlSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).BeginInit();
            this.tcBusiness.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).BeginInit();
            this.gcNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).BeginInit();
            this.gcFindGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期E.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期E.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期B.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de申请日期E.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de申请日期E.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de申请日期B.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.de申请日期B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue签约对象.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue服务包类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            this.SuspendLayout();
            // 
            // tpSummary
            // 
            this.tpSummary.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpSummary.Appearance.PageClient.Options.UseBackColor = true;
            this.tpSummary.Controls.Add(this.gcSummary);
            this.tpSummary.Controls.Add(this.flowLayoutPanel1);
            this.tpSummary.Controls.Add(this.gcFindGroup);
            this.tpSummary.Controls.Add(this.pagerControl1);
            this.tpSummary.Size = new System.Drawing.Size(966, 508);
            // 
            // pnlSummary
            // 
            this.pnlSummary.Location = new System.Drawing.Point(0, 26);
            this.pnlSummary.Size = new System.Drawing.Size(972, 514);
            // 
            // tcBusiness
            // 
            this.tcBusiness.Size = new System.Drawing.Size(972, 514);
            // 
            // tpDetail
            // 
            this.tpDetail.Appearance.PageClient.BackColor = System.Drawing.SystemColors.Control;
            this.tpDetail.Appearance.PageClient.Options.UseBackColor = true;
            this.tpDetail.Size = new System.Drawing.Size(932, 589);
            // 
            // gcNavigator
            // 
            this.gcNavigator.Size = new System.Drawing.Size(972, 26);
            // 
            // controlNavigatorSummary
            // 
            this.controlNavigatorSummary.Buttons.Append.Visible = false;
            this.controlNavigatorSummary.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.Edit.Visible = false;
            this.controlNavigatorSummary.Buttons.EndEdit.Visible = false;
            this.controlNavigatorSummary.Buttons.NextPage.Visible = false;
            this.controlNavigatorSummary.Buttons.PrevPage.Visible = false;
            this.controlNavigatorSummary.Buttons.Remove.Visible = false;
            this.controlNavigatorSummary.Location = new System.Drawing.Point(794, 2);
            // 
            // lblAboutInfo
            // 
            this.lblAboutInfo.Location = new System.Drawing.Point(597, 2);
            // 
            // pagerControl1
            // 
            this.pagerControl1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pagerControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerControl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(78)))), ((int)(((byte)(151)))));
            this.pagerControl1.JumpText = "跳转";
            this.pagerControl1.Location = new System.Drawing.Point(0, 468);
            this.pagerControl1.MaximumSize = new System.Drawing.Size(0, 3667195);
            this.pagerControl1.Name = "pagerControl1";
            this.pagerControl1.PageIndex = 1;
            this.pagerControl1.PageSize = 15;
            this.pagerControl1.paramters = null;
            this.pagerControl1.RecordCount = 0;
            this.pagerControl1.Size = new System.Drawing.Size(966, 40);
            this.pagerControl1.SqlQuery = null;
            this.pagerControl1.TabIndex = 129;
            this.pagerControl1.OnPageChanged += new System.EventHandler(this.pagerControl1_OnPageChanged);
            // 
            // gcFindGroup
            // 
            this.gcFindGroup.Controls.Add(this.layoutControl1);
            this.gcFindGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.gcFindGroup.Location = new System.Drawing.Point(0, 0);
            this.gcFindGroup.Name = "gcFindGroup";
            this.gcFindGroup.Size = new System.Drawing.Size(966, 119);
            this.gcFindGroup.TabIndex = 130;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.de审批日期E);
            this.layoutControl1.Controls.Add(this.de审批日期B);
            this.layoutControl1.Controls.Add(this.de申请日期E);
            this.layoutControl1.Controls.Add(this.de申请日期B);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.lue签约对象);
            this.layoutControl1.Controls.Add(this.lue服务包类型);
            this.layoutControl1.Controls.Add(this.lue状态);
            this.layoutControl1.Controls.Add(this.tllue机构);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(271, 167, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(962, 115);
            this.layoutControl1.TabIndex = 35;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // de审批日期E
            // 
            this.de审批日期E.EditValue = null;
            this.de审批日期E.Location = new System.Drawing.Point(243, 57);
            this.de审批日期E.Name = "de审批日期E";
            this.de审批日期E.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de审批日期E.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de审批日期E.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de审批日期E.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de审批日期E.Size = new System.Drawing.Size(151, 20);
            this.de审批日期E.StyleController = this.layoutControl1;
            this.de审批日期E.TabIndex = 22;
            // 
            // de审批日期B
            // 
            this.de审批日期B.EditValue = null;
            this.de审批日期B.Location = new System.Drawing.Point(87, 57);
            this.de审批日期B.Name = "de审批日期B";
            this.de审批日期B.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de审批日期B.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de审批日期B.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de审批日期B.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de审批日期B.Size = new System.Drawing.Size(135, 20);
            this.de审批日期B.StyleController = this.layoutControl1;
            this.de审批日期B.TabIndex = 21;
            // 
            // de申请日期E
            // 
            this.de申请日期E.EditValue = null;
            this.de申请日期E.Location = new System.Drawing.Point(243, 32);
            this.de申请日期E.Name = "de申请日期E";
            this.de申请日期E.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de申请日期E.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de申请日期E.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de申请日期E.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de申请日期E.Size = new System.Drawing.Size(151, 20);
            this.de申请日期E.StyleController = this.layoutControl1;
            this.de申请日期E.TabIndex = 20;
            // 
            // de申请日期B
            // 
            this.de申请日期B.EditValue = null;
            this.de申请日期B.Location = new System.Drawing.Point(87, 32);
            this.de申请日期B.Name = "de申请日期B";
            this.de申请日期B.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de申请日期B.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.de申请日期B.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.de申请日期B.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.de申请日期B.Size = new System.Drawing.Size(135, 20);
            this.de申请日期B.StyleController = this.layoutControl1;
            this.de申请日期B.TabIndex = 19;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(781, 81);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(169, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 8;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(473, 81);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(229, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(473, 57);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Size = new System.Drawing.Size(229, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 6;
            // 
            // lue签约对象
            // 
            this.lue签约对象.Location = new System.Drawing.Point(781, 32);
            this.lue签约对象.Name = "lue签约对象";
            this.lue签约对象.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue签约对象.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lue签约对象.Size = new System.Drawing.Size(169, 20);
            this.lue签约对象.StyleController = this.layoutControl1;
            this.lue签约对象.TabIndex = 18;
            // 
            // lue服务包类型
            // 
            this.lue服务包类型.Location = new System.Drawing.Point(87, 81);
            this.lue服务包类型.Name = "lue服务包类型";
            this.lue服务包类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue服务包类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lue服务包类型.Size = new System.Drawing.Size(307, 20);
            this.lue服务包类型.StyleController = this.layoutControl1;
            this.lue服务包类型.TabIndex = 11;
            // 
            // lue状态
            // 
            this.lue状态.Location = new System.Drawing.Point(781, 57);
            this.lue状态.Name = "lue状态";
            this.lue状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lue状态.Properties.Items.AddRange(new object[] {
            "",
            "未审批",
            "审批通过",
            "审批未通过"});
            this.lue状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lue状态.Size = new System.Drawing.Size(169, 20);
            this.lue状态.StyleController = this.layoutControl1;
            this.lue状态.TabIndex = 17;
            // 
            // tllue机构
            // 
            this.tllue机构.Location = new System.Drawing.Point(473, 32);
            this.tllue机构.Name = "tllue机构";
            this.tllue机构.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tllue机构.Properties.NullText = "";
            this.tllue机构.Properties.PopupSizeable = false;
            this.tllue机构.Size = new System.Drawing.Size(229, 20);
            this.tllue机构.StyleController = this.layoutControl1;
            this.tllue机构.TabIndex = 4;
            this.tllue机构.EditValueChanged += new System.EventHandler(this.tllue机构_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "签约服务查询";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem13,
            this.layoutControlItem8,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(962, 115);
            this.layoutControlGroup1.Text = "自助签约审批查询";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tllue机构;
            this.layoutControlItem1.CustomizationFormText = "所属机构：";
            this.layoutControlItem1.Location = new System.Drawing.Point(386, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(308, 25);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "所属机构：";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.de申请日期B;
            this.layoutControlItem6.CustomizationFormText = "签约日期：";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(214, 25);
            this.layoutControlItem6.Text = "申请日期：";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.de申请日期E;
            this.layoutControlItem7.CustomizationFormText = "至";
            this.layoutControlItem7.Location = new System.Drawing.Point(214, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(172, 25);
            this.layoutControlItem7.Text = "至";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.de审批日期B;
            this.layoutControlItem10.CustomizationFormText = "生效日期：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(214, 24);
            this.layoutControlItem10.Text = "审批日期：";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.de审批日期E;
            this.layoutControlItem11.CustomizationFormText = "至";
            this.layoutControlItem11.Location = new System.Drawing.Point(214, 25);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItem11.Text = "至";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(12, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lue签约对象;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(694, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(248, 25);
            this.layoutControlItem2.Text = "签约对象：";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txt档案号;
            this.layoutControlItem3.CustomizationFormText = "姓名：";
            this.layoutControlItem3.Location = new System.Drawing.Point(386, 25);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(94, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(308, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "健康档案号：";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号：";
            this.layoutControlItem4.Location = new System.Drawing.Point(386, 49);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(308, 26);
            this.layoutControlItem4.Text = "身份证号：";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.lue状态;
            this.layoutControlItem13.CustomizationFormText = "状态：";
            this.layoutControlItem13.Location = new System.Drawing.Point(694, 25);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(248, 24);
            this.layoutControlItem13.Text = "签约状态：";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lue服务包类型;
            this.layoutControlItem8.CustomizationFormText = "服务包类型：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(386, 26);
            this.layoutControlItem8.Text = "服务包类型：";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(72, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt姓名;
            this.layoutControlItem5.CustomizationFormText = "健康档案号：";
            this.layoutControlItem5.Location = new System.Drawing.Point(694, 49);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(248, 26);
            this.layoutControlItem5.Text = "姓　　名：";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(72, 14);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnQuery);
            this.flowLayoutPanel1.Controls.Add(this.btnEmpty);
            this.flowLayoutPanel1.Controls.Add(this.sbtnAllow);
            this.flowLayoutPanel1.Controls.Add(this.sbtnRefuse);
            this.flowLayoutPanel1.Controls.Add(this.sbtnExport);
            this.flowLayoutPanel1.Controls.Add(this.labNotice);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 119);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(966, 31);
            this.flowLayoutPanel1.TabIndex = 131;
            // 
            // btnQuery
            // 
            this.btnQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnQuery.Image")));
            this.btnQuery.Location = new System.Drawing.Point(103, 3);
            this.btnQuery.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 22);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Image = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Image")));
            this.btnEmpty.Location = new System.Drawing.Point(184, 3);
            this.btnEmpty.MinimumSize = new System.Drawing.Size(75, 22);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(75, 22);
            this.btnEmpty.TabIndex = 6;
            this.btnEmpty.Text = "重置";
            this.btnEmpty.ToolTip = "情况查询条件";
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // sbtnAllow
            // 
            this.sbtnAllow.Image = ((System.Drawing.Image)(resources.GetObject("sbtnAllow.Image")));
            this.sbtnAllow.Location = new System.Drawing.Point(265, 3);
            this.sbtnAllow.Name = "sbtnAllow";
            this.sbtnAllow.Size = new System.Drawing.Size(100, 23);
            this.sbtnAllow.TabIndex = 7;
            this.sbtnAllow.Text = "审批通过";
            this.sbtnAllow.Click += new System.EventHandler(this.sbtnEdit_Click);
            // 
            // sbtnRefuse
            // 
            this.sbtnRefuse.Image = ((System.Drawing.Image)(resources.GetObject("sbtnRefuse.Image")));
            this.sbtnRefuse.Location = new System.Drawing.Point(371, 3);
            this.sbtnRefuse.Name = "sbtnRefuse";
            this.sbtnRefuse.Size = new System.Drawing.Size(100, 23);
            this.sbtnRefuse.TabIndex = 9;
            this.sbtnRefuse.Text = "审批不通过";
            this.sbtnRefuse.Click += new System.EventHandler(this.sbtnRefuse_Click);
            // 
            // sbtnExport
            // 
            this.sbtnExport.Image = ((System.Drawing.Image)(resources.GetObject("sbtnExport.Image")));
            this.sbtnExport.Location = new System.Drawing.Point(477, 3);
            this.sbtnExport.Name = "sbtnExport";
            this.sbtnExport.Size = new System.Drawing.Size(75, 23);
            this.sbtnExport.TabIndex = 8;
            this.sbtnExport.Text = "导出";
            this.sbtnExport.Click += new System.EventHandler(this.sbtnExport_Click);
            // 
            // labNotice
            // 
            this.labNotice.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labNotice.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labNotice.Location = new System.Drawing.Point(558, 8);
            this.labNotice.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.labNotice.Name = "labNotice";
            this.labNotice.Size = new System.Drawing.Size(130, 14);
            this.labNotice.TabIndex = 10;
            this.labNotice.Text = "无审批权限，仅可查询";
            this.labNotice.Visible = false;
            // 
            // gcSummary
            // 
            this.gcSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSummary.Location = new System.Drawing.Point(0, 150);
            this.gcSummary.MainView = this.gvSummary;
            this.gcSummary.Name = "gcSummary";
            this.gcSummary.Size = new System.Drawing.Size(966, 318);
            this.gcSummary.TabIndex = 132;
            this.gcSummary.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSummary});
            // 
            // gvSummary
            // 
            this.gvSummary.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolID,
            this.gcol团队ID,
            this.gcol团队名称,
            this.gridColumn9,
            this.col健康档案号,
            this.col姓名,
            this.col性别,
            this.col身份证号,
            this.gridColumn3,
            this.gridColumn2,
            this.col签约包,
            this.col生效日期,
            this.col状态,
            this.col审批人,
            this.col到期日期});
            this.gvSummary.GridControl = this.gcSummary;
            this.gvSummary.IndicatorWidth = 30;
            this.gvSummary.Name = "gvSummary";
            this.gvSummary.OptionsView.ColumnAutoWidth = false;
            this.gvSummary.OptionsView.ShowGroupPanel = false;
            this.gvSummary.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvSummary_CustomDrawRowIndicator);
            // 
            // gcolID
            // 
            this.gcolID.Caption = "ID";
            this.gcolID.FieldName = "ID";
            this.gcolID.Name = "gcolID";
            // 
            // gcol团队ID
            // 
            this.gcol团队ID.Caption = "团队ID";
            this.gcol团队ID.FieldName = "团队ID";
            this.gcol团队ID.Name = "gcol团队ID";
            this.gcol团队ID.ToolTip = "根据各团队的负责区域自动匹配自助签约的负责团队";
            // 
            // gcol团队名称
            // 
            this.gcol团队名称.Caption = "负责团队";
            this.gcol团队名称.FieldName = "团队名称";
            this.gcol团队名称.Name = "gcol团队名称";
            this.gcol团队名称.Visible = true;
            this.gcol团队名称.VisibleIndex = 1;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "档案所属机构";
            this.gridColumn9.FieldName = "所属机构名称";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            this.gridColumn9.Width = 103;
            // 
            // col健康档案号
            // 
            this.col健康档案号.AppearanceHeader.Options.UseTextOptions = true;
            this.col健康档案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col健康档案号.Caption = "档案号";
            this.col健康档案号.FieldName = "档案号";
            this.col健康档案号.Name = "col健康档案号";
            this.col健康档案号.OptionsColumn.ReadOnly = true;
            this.col健康档案号.Visible = true;
            this.col健康档案号.VisibleIndex = 2;
            this.col健康档案号.Width = 127;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 3;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别名称";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 4;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 5;
            this.col身份证号.Width = 140;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "签约对象类型";
            this.gridColumn3.FieldName = "签约对象类型名称";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 6;
            this.gridColumn3.Width = 100;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "健康状况";
            this.gridColumn2.FieldName = "健康状况名称";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 7;
            this.gridColumn2.Width = 100;
            // 
            // col签约包
            // 
            this.col签约包.AppearanceHeader.Options.UseTextOptions = true;
            this.col签约包.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col签约包.Caption = "签约服务包";
            this.col签约包.FieldName = "服务包名称";
            this.col签约包.Name = "col签约包";
            this.col签约包.OptionsColumn.ReadOnly = true;
            this.col签约包.Visible = true;
            this.col签约包.VisibleIndex = 8;
            this.col签约包.Width = 240;
            // 
            // col生效日期
            // 
            this.col生效日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col生效日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col生效日期.Caption = "申请日期";
            this.col生效日期.FieldName = "创建时间";
            this.col生效日期.Name = "col生效日期";
            this.col生效日期.OptionsColumn.ReadOnly = true;
            this.col生效日期.Visible = true;
            this.col生效日期.VisibleIndex = 9;
            // 
            // col状态
            // 
            this.col状态.AppearanceHeader.Options.UseTextOptions = true;
            this.col状态.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col状态.Caption = "状态";
            this.col状态.FieldName = "审批状态";
            this.col状态.Name = "col状态";
            this.col状态.OptionsColumn.ReadOnly = true;
            this.col状态.Visible = true;
            this.col状态.VisibleIndex = 10;
            // 
            // col审批人
            // 
            this.col审批人.AppearanceHeader.Options.UseTextOptions = true;
            this.col审批人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col审批人.Caption = "审批人";
            this.col审批人.FieldName = "审批人姓名";
            this.col审批人.Name = "col审批人";
            this.col审批人.OptionsColumn.ReadOnly = true;
            this.col审批人.Visible = true;
            this.col审批人.VisibleIndex = 12;
            // 
            // col到期日期
            // 
            this.col到期日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col到期日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col到期日期.Caption = "审批时间";
            this.col到期日期.FieldName = "审批时间";
            this.col到期日期.Name = "col到期日期";
            this.col到期日期.OptionsColumn.ReadOnly = true;
            this.col到期日期.Visible = true;
            this.col到期日期.VisibleIndex = 11;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.CustomizationFormText = "居住地址：";
            this.layoutControlItem33.Location = new System.Drawing.Point(294, 48);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(117, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(174, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "居住地址：";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // Frm自助签约审批
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 540);
            this.Name = "Frm自助签约审批";
            this.Text = "自助签约审批";
            this.Load += new System.EventHandler(this.Frm自助签约审批_Load);
            this.tpSummary.ResumeLayout(false);
            this.pnlSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcBusiness)).EndInit();
            this.tcBusiness.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigator)).EndInit();
            this.gcNavigator.ResumeLayout(false);
            this.gcNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFindGroup)).EndInit();
            this.gcFindGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期E.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期E.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期B.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de审批日期B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de申请日期E.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de申请日期E.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de申请日期B.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.de申请日期B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue签约对象.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue服务包类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lue状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tllue机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TActionProject.PagerControl pagerControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnQuery;
        private DevExpress.XtraEditors.PanelControl gcFindGroup;
        private DevExpress.XtraEditors.SimpleButton btnEmpty;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridControl gcSummary;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSummary;
        private DevExpress.XtraGrid.Columns.GridColumn col健康档案号;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn col签约包;
        private DevExpress.XtraGrid.Columns.GridColumn col生效日期;
        private DevExpress.XtraGrid.Columns.GridColumn col到期日期;
        private DevExpress.XtraGrid.Columns.GridColumn col状态;
        private DevExpress.XtraGrid.Columns.GridColumn col审批人;
        private DevExpress.XtraEditors.DateEdit de申请日期E;
        private DevExpress.XtraEditors.DateEdit de申请日期B;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.DateEdit de审批日期E;
        private DevExpress.XtraEditors.DateEdit de审批日期B;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraGrid.Columns.GridColumn gcolID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.SimpleButton sbtnAllow;
        private DevExpress.XtraEditors.ComboBoxEdit lue签约对象;
        private DevExpress.XtraEditors.ComboBoxEdit lue服务包类型;
        private DevExpress.XtraEditors.ComboBoxEdit lue状态;
        private DevExpress.XtraEditors.SimpleButton sbtnExport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.SimpleButton sbtnRefuse;
        private DevExpress.XtraEditors.LookUpEdit tllue机构;
        private DevExpress.XtraEditors.LabelControl labNotice;
        private DevExpress.XtraGrid.Columns.GridColumn gcol团队ID;
        private DevExpress.XtraGrid.Columns.GridColumn gcol团队名称;
    }
}