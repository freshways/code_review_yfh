﻿using AtomEHR.Common;
using AtomEHR.签约服务.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.微信
{
    public partial class Frm微信端服务包设置 : AtomEHR.Library.frmBaseBusinessForm
    {
        bllJTYS服务包 bll服务包 = new bllJTYS服务包();
        bllJTYS自助服务包 bll自助服务包 = new bllJTYS自助服务包();
        public Frm微信端服务包设置()
        {
            InitializeComponent();
        }

        private void Frm微信端服务包设置_Load(object sender, EventArgs e)
        {
            AtomEHR.Business.bll机构信息 bll机构 = new AtomEHR.Business.bll机构信息();
            string strJGTemp = Loginer.CurrentUser.所属机构.Length >= 6 ? Loginer.CurrentUser.所属机构.Substring(0, 6) : "";
            DataTable dt所属机构 = _bll机构信息.Get机构(strJGTemp);
            util.ControlsHelper.BindComboxData(dt所属机构, cbo机构, "机构编号", "机构名称");
            if (Loginer.CurrentUser.所属机构.Length > 6)
            {
                util.ControlsHelper.SetComboxData(Loginer.CurrentUser.所属机构, cbo机构);
                cbo机构.Enabled = false;
            }
        }

        string str机构 = "";
        private void btnQuery_Click(object sender, EventArgs e)
        {
            str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            try
            {
                if(string.IsNullOrWhiteSpace(str机构))
                {
                    Msg.ShowInformation("请先设置机构查询条件");
                    return;
                }

                

                DataTable dtPack = bll服务包.Get服务包(str机构);

                //dtPack.Columns.Add("selected", typeof(bool));

                //for (int index = 0; index < dtPack.Rows.Count; index++ )
                //{
                //    dtPack.Rows[index]["selected"] = false;
                //}

                this.gcPack.DataSource = dtPack;

            }
            catch(Exception ex)
            {
                Msg.ShowInformation(ex.Message);
            }
        }



        private void repositoryItemCheckEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            str机构 = util.ControlsHelper.GetComboxKey(cbo机构);
            if (str机构 != Loginer.CurrentUser.所属机构)
            {
                Msg.ShowInformation("对不起，您没有修改权限。区域管理只允许各医院自行设置。");
                e.Cancel = true;
                return;
            }

            //string packid = this.gvPack.GetRowCellValue(this.gvPack.FocusedRowHandle, "团队ID").ToString();
            string packid = this.gvPack.GetRowCellValue(this.gvPack.FocusedRowHandle, "ServiceID").ToString();
            bool newvalue = Convert.ToBoolean(e.NewValue);

            bool ret = bll自助服务包.InsertOrUpdate(packid, newvalue);
            if (!ret)
            {
                Msg.ShowInformation("更新失败");
            }
         }

        private void gvPack_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            //if (gvPack.DataRowCount == 0 || gvPack.FocusedRowHandle < 0 || this.gvPack.DataSource == null)
            //{
            //    return;
            //}

            //string packid = gvPack.GetRowCellValue(gvPack.FocusedRowHandle, "ID").ToString();

            //DataTable dtPack = bll自助服务包.GetPackBypackid(packid);

            ////DataTable dtArea = this.gcWss.DataSource as DataTable;
            //for (int index = 0; index < dtPack.Rows.Count; index++)
            //{
            //    DataRow[] drs = dtPack.Select("SPID='" + dtPack.Rows[index]["ID"] + "'");
            //    if (drs.Length > 0)
            //    {
            //        dtPack.Rows[index]["selected"] = true;
            //    }
            //    else
            //    {
            //        dtPack.Rows[index]["selected"] = false;
            //    }

            //}
        }
       
    }
}
