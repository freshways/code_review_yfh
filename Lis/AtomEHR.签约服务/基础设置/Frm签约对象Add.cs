﻿using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.签约服务.Business;
using AtomEHR.签约服务.Models;

namespace AtomEHR.签约服务.基础设置
{
    public partial class Frm签约对象Add : frmBase
    {
        bllJTYS签约对象类型 bll = new bllJTYS签约对象类型();
        public Frm签约对象Add()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sbtnOK_Click(object sender, EventArgs e)
        {
            bll.GetBusinessByKey("-1", true);
            bll.NewBusiness();
            bll.CurrentBusiness.Tables[0].Rows[0][tb_JTYS签约对象类型.类型名称] = txtName.Text.Trim();
            bll.Save(bll.CurrentBusiness);
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
