﻿using AtomEHR.签约服务.Business;
using AtomEHR.Library;
using AtomEHR.签约服务.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.签约服务.基础设置
{
    public partial class frm签约对象管理 : AtomEHR.Library.frmBaseBusinessForm
    {
        public frm签约对象管理()
        {
            InitializeComponent();
        }

        private void frm签约对象管理_Load(object sender, EventArgs e)
        {
            this.InitializeForm();
        }

        protected override void InitializeForm()
        {
            _BLL = new bllJTYS签约对象类型();// 业务逻辑层实例
            _SummaryView = new DevGridView(gvSummary);//每个业务窗体必需给这个变量赋值.

            base.InitButtonsBase();

            frmGridCustomize.RegisterGrid(gvSummary);

            BindingSummarySearchPanel(btnQuery, btnEmpty, gcFindGroup);
        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            Frm签约对象Add frm = new Frm签约对象Add();
            frm.ShowDialog();
        }
        
        protected override bool DoSearchSummary()
        {
            string _strWhere = "";// " and " + tb_JTYS签约对象类型.是否有效 + "=1";

            if(!string.IsNullOrWhiteSpace(textEdit编号.Text))
            {
                _strWhere = " and " + tb_JTYS签约对象类型.ID + "='" + textEdit编号.Text.Trim() + "' ";
            }
            if(!string.IsNullOrWhiteSpace(textEdit名称.Text))
            {
                _strWhere = " and " + tb_JTYS签约对象类型.类型名称 + "='" + textEdit名称.Text.Trim() + "' ";
            }


            DataTable dt = this.pagerControl1.GetQueryResultNew("tb_JTYS签约对象类型", "*", _strWhere, "ID", "DESC").Tables[0];
            this.pagerControl1.DrawControl();

            DoBindingSummaryGrid(dt); //绑定主表的Grid
            ShowSummaryPage(true); //显示Summary页面.   

            return dt != null && dt.Rows.Count > 0;
        }

        private void pagerControl1_OnPageChanged(object sender, EventArgs e)
        {
            DoSearchSummary();
        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            pagerControl1.InitControl();
        }
    }
}
