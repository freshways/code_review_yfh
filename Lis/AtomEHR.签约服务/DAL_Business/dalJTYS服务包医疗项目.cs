﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: JTYS服务包医疗项目的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2017-12-14 10:58:18
 *   最后修改: 2017-12-14 10:58:18
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS服务包医疗项目
    /// </summary>
    public class dalJTYS服务包医疗项目 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalJTYS服务包医疗项目(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_JTYS服务包医疗项目.__KeyName; //主表的主键字段
             _SummaryTableName = tb_JTYS服务包医疗项目.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_JTYS服务包医疗项目.__TableName) ORM = typeof(tb_JTYS服务包医疗项目);
             //if (tableName == tb_JTYS服务包医疗项目s.__TableName) ORM = typeof(tb_JTYS服务包医疗项目s);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS服务包医疗项目.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_JTYS服务包医疗项目.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_JTYS服务包医疗项目.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  //return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_JTYS服务包医疗项目]    where ["+tb_JTYS服务包医疗项目.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_JTYS服务包医疗项目s]   where ["+tb_JTYS服务包医疗项目.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS服务包医疗项目.__TableName;
              //ds.Tables[1].TableName =tb_JTYS服务包医疗项目s.__TableName;//子表
              return ds;
          }

          public DataSet GetBusinessByPackageID(string packageID)
          {
              string sql1 = " select * from [tb_JTYS服务包医疗项目]    where [" + tb_JTYS服务包医疗项目.ServiceID + "]=@packageID ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@packageID", SqlDbType.VarChar, packageID.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS服务包医疗项目.__TableName;
              return ds;
          }

          public DataSet GetValidateBusinessByPackageID(string packageID)
          {
              string sql1 = " select * from [tb_JTYS服务包医疗项目]    where [" + tb_JTYS服务包医疗项目.是否有效+ "]=1 and [" + tb_JTYS服务包医疗项目.ServiceID + "]=@packageID ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@packageID", SqlDbType.VarChar, packageID.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS服务包医疗项目.__TableName;
              return ds;
          }

          public DataSet GetHISItemList(string name)
          {
//              string sql1 = @" SELECT TOP 50 
//	CHARGE_ID 项目ID
//	,CHARGE_CODE 项目编码
//	,CHARGE_NAME 项目名称
//	,INPUT_CODE  拼音简码
//	,PRICE  单价
//	,MEASURE_UNIT_NAME 单位
//FROM    HISDB.COMM.COMM.CHARGE_PRICE AS A 
//WHERE A.HOSPITAL_ID = '10'
//      and A.FLAG_INVALID=0 AND (CHARGE_CODE like '%'+@name+'%' or CHARGE_NAME like '%'+@name+'%' or INPUT_CODE like '%'+@name+'%' )";

              string sql1 = @"SELECT TOP 50 [ORDER_ID] 项目ID
      ,[ORDER_CODE] 项目编码
      ,[ORDERS_NAME] 项目名称
      ,[INPUT_CODE] 拼音简码
      ,[PRICE] 单价
	  ,'次' 单位
  FROM [HISDB].[COMM].[DICT].[ORDERS] AS A 
WHERE A.HOSPITAL_ID = '10'
      and A.FLAG_INVALID=0 AND ([ORDER_CODE] like '%'+@name+'%' or [ORDERS_NAME] like '%'+@name+'%' or INPUT_CODE like '%'+@name+'%' )
";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@name", SqlDbType.VarChar, name.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds;
          }

          public DataSet GetHISItemList(string HospidorRgid, string name)
          {
              string sql1;
              if (HospidorRgid.StartsWith("371321"))//界湖的情况
              {
                   sql1 = @"SELECT TOP 50 [ORDER_ID] 项目ID
      ,[ORDER_CODE] 项目编码
      ,[ORDERS_NAME] 项目名称
      ,[INPUT_CODE] 拼音简码
      ,[PRICE] 单价
	  ,'次' 单位
  FROM [HISDB].[COMM].[DICT].[ORDERS] AS A 
WHERE A.HOSPITAL_ID = '10'
      and A.FLAG_INVALID=0 AND ([ORDER_CODE] like '%'+@name+'%' or [ORDERS_NAME] like '%'+@name+'%' or INPUT_CODE like '%'+@name+'%' )
";
              }
              else//蒙阴的情况
              {
                  sql1 = @"SELECT TOP 50 [ID] 项目ID
      ,[收费编码] 项目编码
      ,[收费名称] 项目名称
      ,[拼音代码] 拼音简码
      ,[单价]
	  ,[单位]
      ,[PACS检查项]
      ,[LIS检查项]
  FROM HISDB.[CHIS" + HospidorRgid + "].[dbo].[GY收费小项] where [是否禁用]=0 AND ([收费编码] like '%'+@name+'%' or [收费名称] like '%'+@name+'%' or 拼音代码 like '%'+@name+'%' ) ";
              }

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@name", SqlDbType.VarChar, name.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_JTYS服务包医疗项目s] where ["+tb_JTYS服务包医疗项目.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_JTYS服务包医疗项目] where ["+tb_JTYS服务包医疗项目.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "JTYS服务包医疗项目");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

     }
}

