﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.签约服务.Models;

/*==========================================
 *   程序说明: JTYS解约审批的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2018/09/25 03:46:22
 *   最后修改: 2018/09/25 03:46:22
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS解约审批
    /// </summary>
    public class dalJTYS解约审批 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalJTYS解约审批(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_JTYS解约审批.__KeyName; //主表的主键字段
             _SummaryTableName = tb_JTYS解约审批.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_JTYS解约审批.__TableName) ORM = typeof(tb_JTYS解约审批);
             //if (tableName == tb_JTYS解约审批s.__TableName) ORM = typeof(tb_JTYS解约审批s);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS解约审批.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_JTYS解约审批.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_JTYS解约审批.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  //return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_JTYS解约审批]    where ["+tb_JTYS解约审批.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_JTYS解约审批s]   where ["+tb_JTYS解约审批.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS解约审批.__TableName;
              //ds.Tables[1].TableName =tb_JTYS解约审批s.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_JTYS解约审批s] where ["+tb_JTYS解约审批.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_JTYS解约审批] where ["+tb_JTYS解约审批.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "JTYS解约审批");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

        /// <summary>
          /// 存在已解约或已申请解约时，返回true
        /// </summary>
        /// <param name="qyid"></param>
        /// <returns></returns>
          public bool Check已解约或已申请解约(string qyid)
          {
              string sql2 = "select count(1) from tb_JTYS解约审批 where [QYID]=@qyid and (审批状态='解约通过' or 审批状态='解约待审批')";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              object i = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
              return i.ToString() != "0" ? true : false;
          }

          public bool Check已申请解约(string qyid)
          {
              string sql2 = "select count(1) from tb_JTYS解约审批 where [QYID]=@qyid and 审批状态='解约待审批'";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              object i = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
              return i.ToString() != "0" ? true : false;
          }

        /// <summary>
        /// 
        /// </summary>
          /// <param name="qyid">签约ID</param>
        /// <param name="jyyx">解约原因</param>
        /// <param name="sqr">解约申请人</param>
        /// <param name="sqrxm">解约申请人姓名</param>
          public void Create待审批记录(string qyid, string jyyx, string sqr, string sqrxm)
          {
              string sql2 = "insert into tb_JTYS解约审批( [QYID],[申请人],[申请人姓名],[解约原因],[审批状态]) values(@qyid, @sqr, @sqrxm, @jyyx, '解约待审批') ";
              string sql3 = " update [dbo].[tb_JTYS签约信息] set [状态]='2' where ID=@qyid ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2+sql3);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              cmd.AddParam("@jyyx", SqlDbType.VarChar, jyyx.Trim());
              cmd.AddParam("@sqr", SqlDbType.VarChar, sqr.Trim());
              cmd.AddParam("@sqrxm", SqlDbType.VarChar, sqrxm.Trim());
              DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
          }

          public void Update待审批记录(string sqjyid, string spyj, string spbz, string spr, string sprxm, string spsj)
          {
              string sql2 = "update tb_JTYS解约审批 set [审批状态]=@spyj,[审批备注]=@spbz,[审批人]=@spr,[审批人姓名]=@sprxm,[审批时间]=@spsj where ID=@sqjyid ";
              string zt = "";
              if (spyj.Equals("允许解约"))
              {
                  zt = "5";
              }
              else
              {
                  zt = "1";
              }

              string sql3 = " update [dbo].[tb_JTYS签约信息] set [状态]='"+zt+"' where ID in (select [QYID] from [dbo].[tb_JTYS解约审批] where ID=@sqjyid) ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2 + sql3);
              cmd.AddParam("@sqjyid", SqlDbType.VarChar, sqjyid.Trim());
              cmd.AddParam("@spyj", SqlDbType.VarChar, spyj.Trim());
              cmd.AddParam("@spbz", SqlDbType.VarChar, spbz.Trim());
              cmd.AddParam("@spr", SqlDbType.VarChar, spr.Trim());
              cmd.AddParam("@sprxm", SqlDbType.VarChar, sprxm.Trim());
              cmd.AddParam("@spsj", SqlDbType.VarChar, spsj);
              DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
          }
     }
}

