﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: JTYS签约信息的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2017-12-14 05:37:43
 *   最后修改: 2017-12-14 05:37:43
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS签约信息
    /// </summary>
    public class dalJTYS签约信息 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dalJTYS签约信息(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_JTYS签约信息.__KeyName; //主表的主键字段
            _SummaryTableName = tb_JTYS签约信息.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_JTYS签约信息.__TableName) ORM = typeof(tb_JTYS签约信息);
            //if (tableName == tb_JTYS签约信息s.__TableName) ORM = typeof(tb_JTYS签约信息s);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS签约信息.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_JTYS签约信息.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_JTYS签约信息.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                //return null;
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_JTYS签约信息]    where [" + tb_JTYS签约信息.__KeyName + "]=@DocNo1 ";
            //string sql2 = " select * from [tb_JTYS签约信息s]   where ["+tb_JTYS签约信息.__KeyName+"]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_JTYS签约信息.__TableName;
            //ds.Tables[1].TableName =tb_JTYS签约信息s.__TableName;//子表
            return ds;
        }

        public DataSet GetBusinessByIdFromView(string ID)
        {
            string sql1 = " select * from [vw_JTYS签约信息] where [" + tb_JTYS签约信息.ID + "]=@ID ";
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@ID", SqlDbType.VarChar, ID.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            //string sql1 = "delete [tb_JTYS签约信息s] where ["+tb_JTYS签约信息.__KeyName+"]=@DocNo1 ";//删除子表
            string sql2 = "delete [tb_JTYS签约信息] where [" + tb_JTYS签约信息.__KeyName + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
            //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }

        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "JTYS签约信息");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        /// <summary>
        /// 检验是否存在有效的签约信息
        /// </summary>
        /// <param name="dah"></param>
        /// <returns></returns>
        public bool CheckValidateExists(string dah)
        {
            string sql = string.Format("SELECT COUNT(1) C FROM [{0}] WHERE ([状态]=1 or [状态] =2) and getdate() > [生效日期] and getdate() < [到期日期] and [{1}]=@dah", tb_JTYS签约信息.__TableName, tb_JTYS签约信息.档案号);
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@dah", SqlDbType.VarChar, dah);
            object o = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);

            return ConvertEx.ToInt(o) > 0;
        }

        public DataTable GetValidateSignedServiceidByDah(string dah)
        {
            string sql = "SELECT 签约服务包 FROM [" + tb_JTYS签约信息.__TableName + "] WHERE ([状态]=1 or [状态] =2) and getdate() > [生效日期] and getdate() < [到期日期] and [" + tb_JTYS签约信息.档案号 + "]=@dah";
            
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@dah", SqlDbType.VarChar, dah.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }

        public int GetValidateSignedCountByDah(string dah)
        {
            string sql = "SELECT count(1) FROM [" + tb_JTYS签约信息.__TableName + "] WHERE ([状态]=1 or [状态] =2) and convert(varchar, getdate(), 23) >= [生效日期] and convert(varchar, getdate(), 23) <= [到期日期] and [" + tb_JTYS签约信息.档案号 + "]=@dah";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@dah", SqlDbType.VarChar, dah.Trim());
            object obj = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return Convert.ToInt32(obj);
        }

        public DataTable GetServiceIdByQYID(string qyid)
        {
            string sql = "SELECT 签约服务包 FROM [" + tb_JTYS签约信息.__TableName + "] WHERE [状态]=1 and  [" + tb_JTYS签约信息.ID + "]=@qyid ";

            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }

        public string Get状态Code(string qyid)
        {
            string sql = string.Format("SELECT ["+tb_JTYS签约信息.状态+"] from ["+tb_JTYS签约信息.__TableName+"] where ID=@qyid " );
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@qyid", SqlDbType.VarChar, qyid);
            object obj = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
            return obj.ToString();
        }

        public DataSet GetBaseInfoByQYID(string qyid)
        {
            string sql = "select AA.档案号,BB.姓名,BB.身份证号 from dbo.tb_JTYS签约信息 AA LEFT OUTER JOIN dbo.tb_健康档案 BB on AA.档案号=BB.个人档案编号 where AA.ID=@qyid";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@qyid", SqlDbType.VarChar, qyid);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds; 
        }

        public DataSet Get签约数量(string str统计方式, string begindate, string enddate, string str服务包id)
        {
            string sql = "";

            if (str统计方式 == "2")
            {
                sql = @"select AA.*,BB.UserName from
(
	select 签约医生,COUNT(DISTINCT 档案号) 签约数量
	from dbo.tb_JTYS签约信息 AA 
	where AA.状态='1' AND 签约日期 >= @begindate and  签约日期<=@enddate and [签约服务包] like '%'+@fwbid+'%' ";

                if (!(Loginer.CurrentUser.IsAdmin() || Loginer.CurrentUser.IsSubAdmin()))
                {
                    sql = " and 签约医生=@qyys";
                }
                sql += @"
	GROUP BY AA.签约医生
) AA 
LEFT OUTER JOIN DBO.tb_MyUser BB ON AA.签约医生 = BB.用户编码";
            }
            else
            {
                sql = @"select AA.*,BB.UserName from
(
	select 签约医生,COUNT(1) 签约数量
	from dbo.tb_JTYS签约信息 AA 
	where AA.状态='1' AND 签约日期 >= @begindate and  签约日期<=@enddate and [签约服务包] like '%'+@fwbid+'%' ";

                if (!(Loginer.CurrentUser.IsAdmin() || Loginer.CurrentUser.IsSubAdmin()))
                {
                    sql = " and 签约医生=@qyys";
                }
                sql += @"
	GROUP BY AA.签约医生
) AA 
LEFT OUTER JOIN DBO.tb_MyUser BB ON AA.签约医生 = BB.用户编码";

            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@begindate", SqlDbType.VarChar, begindate);
            cmd.AddParam("@enddate", SqlDbType.VarChar, enddate);
            cmd.AddParam("@fwbid", SqlDbType.VarChar, str服务包id);
            cmd.AddParam("@qyys", SqlDbType.VarChar, Loginer.CurrentUser.用户编码);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds; 
        }

        public DataSet Get签约数量With团队(string str统计方式, string begindate, string enddate, string str服务包id)
        {
            string sql = "";

            if (str统计方式 == "2")
            {
                sql = @"select AA.*,BB.团队名称 from
(
	select 团队ID,COUNT(DISTINCT 档案号) 签约数量
	from dbo.tb_JTYS签约信息 AA 
	where AA.状态='1' AND 签约日期 >= @begindate and  签约日期<=@enddate and [签约服务包] like '%'+@fwbid+'%' ";

                if (!(Loginer.CurrentUser.IsAdmin() || Loginer.CurrentUser.IsSubAdmin()))
                {
                    sql = " and 签约医生=@qyys";
                }
                sql+= @"
	GROUP BY AA.团队ID
) AA 
LEFT OUTER JOIN DBO.tb_JTYS团队概要 BB ON AA.团队ID = BB.团队ID";
            }
            else
            {
                sql = @"select AA.*,BB.团队名称 from
(
	select 团队ID,COUNT(1) 签约数量
	from dbo.tb_JTYS签约信息 AA 
	where AA.状态='1' AND 签约日期 >= @begindate and  签约日期<=@enddate and [签约服务包] like '%'+@fwbid+'%' ";

                if (!(Loginer.CurrentUser.IsAdmin() || Loginer.CurrentUser.IsSubAdmin()))
                {
                    sql = " and 签约医生=@qyys";
                }
                sql += @"
	GROUP BY AA.团队ID
) AA 
LEFT OUTER JOIN DBO.tb_JTYS团队概要 BB ON AA.团队ID = BB.团队ID";

            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@begindate", SqlDbType.VarChar, begindate);
            cmd.AddParam("@enddate", SqlDbType.VarChar, enddate);
            cmd.AddParam("@fwbid", SqlDbType.VarChar, str服务包id);
            cmd.AddParam("@qyys", SqlDbType.VarChar, Loginer.CurrentUser.用户编码);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds;
        }

        public bool UpdatePrintedState(string qyid, bool isprinted)
        {
            string sql = "UPDATE dbo.[tb_JTYS签约信息] set [已打印协议]=@state where [ID]=@id";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@id", SqlDbType.Int, qyid);
            cmd.AddParam("@state", SqlDbType.Bit, isprinted);
            int count = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);

            return count > 0;
        }

        public DataTable GetInfoForPrint(string qyid)
        {
            string sql = @"SELECT TOP 1 aa.ID, aa.档案号,AA.[签约对象类型], bb.姓名, AA.联系电话, AA.[费用ID], AA.[团队ID],AA.[签约人],BB.本人电话,BB.身份证号,
 AA.签约日期,AA.签约服务包,AA.手签照,isnull(CC.地区名称,'')+isnull(DD.地区名称,'') 地址, AA.[录入人机构]
FROM
(
SELECT [ID]
      ,[档案号]
,[签约对象类型]
      ,[联系电话]
      ,[签约服务包]
      ,[签约日期]
      ,[手签照]
,[签约人]
,[费用ID]
,[团队ID]
,[录入人机构]
  FROM [dbo].[tb_JTYS签约信息] WHERE ID=@qyid) AA
	LEFT OUTER JOIN DBO.tb_健康档案 BB ON AA.档案号=BB.个人档案编号
	left outer join dbo.[tb_地区档案] CC ON BB.街道= CC.地区编码
	LEFT OUTER JOIN DBO.[tb_地区档案] DD ON BB.居委会= DD.地区编码";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@qyid", SqlDbType.VarChar, qyid);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }
        public DataTable GetInfoForPrint2(string qyid)
        {
            string sql = @"select AA.ID 签约ID ,aa.服务包名称
		,CC.项目名称
		,cc.单位
		,cc.数量
		,cc.单价
		,cc.总价
		,isnull((SELECT sum([执行数量]) FROM [dbo].[tb_JTYS履约执行明细] dd 
		where dd.[签约ID]=aa.ID and  dd.ServiceID=bb.item and dd.项目复合ID=cc.项目复合ID group by [签约ID], ServiceID,[项目复合ID]),0) 已执行数量
  from tb_JTYS签约信息 aa
	cross  apply fn_SplitWithoutEmpty(AA.签约服务包,',') bb
	left outer join 
	(
		SELECT AA.[ServiceID]
			  ,BB.名称 服务包名称
			  ,AA.[项目类别]+'_'+AA.[项目ID] 项目复合ID
			  ,AA.[项目编码]
			  ,AA.[项目名称]
			  ,AA.[单位]
			  ,AA.[数量]
				,AA.[单价]
				,AA.[总价]
		  FROM tb_JTYS服务包 BB
		  join [tb_JTYS服务包医疗项目] AA ON AA.ServiceID=BB.ServiceID
		  WHERE AA.是否有效 =1 AND BB.是否有效=1 
	 )CC on bb.item=cc.ServiceID
	  where  AA.状态='1' and AA.生效日期 <=convert(varchar(20), getdate(),23) and convert(varchar(20), getdate(),23) <=AA.到期日期 
		and aa.ID=@qyid
and CC.数量>0";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@qyid", SqlDbType.VarChar, qyid);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }

        public DataTable GetTeamidByQyid(string qyid)
        {
            string sql = @"select 团队ID,签约人 from  [dbo].[tb_JTYS签约信息] WHERE ID=@qyid ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@qyid", SqlDbType.VarChar, qyid);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }

        public string Get医生签名(string fyid)
        {
            string sql = @"SELECT top 1 [签字] FROM [dbo].[tb_JTYS签约医生签字] where [费用ID]=@fyid";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@fyid", SqlDbType.VarChar, fyid);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            if(ds!=null && ds.Tables.Count >0 && ds.Tables[0].Rows.Count>0)
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }

        public string Get履约状态(string qyid)
        {
            string sql = @"select [履约状态] from  [dbo].[tb_JTYS签约信息] WHERE ID=@qyid ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@qyid", SqlDbType.VarChar, qyid);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }

        public int Set履约完毕ByYQID(string qyid)
        {
            string sql = @"update [dbo].[tb_JTYS签约信息] set [履约状态]='履约完毕' WHERE ID=@qyid; insert into [dbo].[tb_JTYS手工履约完毕记录]([QYID],[内容],[OPTIME]) values(@qyid, '手工标记履约完毕', getdate()) ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@qyid", SqlDbType.VarChar, qyid);
            int count = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return count;
        }

        public DataTable GetSummaryBy人群(string yyid, string begdate, string enddate)
        {
            string end = Convert.ToDateTime(enddate).AddDays(1).ToString("yyyy-MM-dd");

            string sql = @"SELECT AA.[签约对象类型], BB.类型名称, count(1) 数量
  FROM [dbo].[tb_JTYS签约信息] AA
  LEFT OUTER JOIN DBO.tb_JTYS签约对象类型 BB ON AA.签约对象类型=BB.ID
  where 状态='1' and 签约日期 >=@begdate and 签约日期 <@enddate";
            if (!string.IsNullOrWhiteSpace(yyid))
            {
                sql += " and 档案机构 in (select 机构编号 from  tb_机构信息 a where a.上级机构=@yyid or a.机构编号=@yyid) ";
            }
            sql+=@"
  group by AA.[签约对象类型], BB.类型名称
  order by cast([签约对象类型] as int) ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@yyid", SqlDbType.VarChar, yyid);
            cmd.AddParam("@begdate", SqlDbType.VarChar, begdate);
            cmd.AddParam("@enddate", SqlDbType.VarChar, end);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0] ;
        }


        public DataTable GetSummaryBy健康状况(string yyid, string begdate, string enddate)
        {
            string end = Convert.ToDateTime(enddate).AddDays(1).ToString("yyyy-MM-dd");

            string sql = @"SELECT  bb.item 健康状况编号, cc.名称 健康状况名称, count(1) 数量
  FROM [dbo].[tb_JTYS签约信息] aa 
  cross apply dbo.[fn_SplitWithoutEmptyTrim](aa.健康状况,',') bb

  left outer join dbo.tb_JTYS健康状况 cc on bb.item=cc.ID
  where 状态='1' and 签约日期 >=@begdate and 签约日期 <@enddate";
            if (!string.IsNullOrWhiteSpace(yyid))
            {
                sql += " and 档案机构 in (select 机构编号 from  tb_机构信息 a where a.上级机构=@yyid or a.机构编号=@yyid) ";
            }
            sql += @"
  group by bb.item, cc.名称
order by cast(bb.item as int) ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@yyid", SqlDbType.VarChar, yyid);
            cmd.AddParam("@begdate", SqlDbType.VarChar, begdate);
            cmd.AddParam("@enddate", SqlDbType.VarChar, end);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }


        public DataSet Get签约数量With团队New(string yyid, string str统计方式, string begindate, string enddate, string str服务包id)
        {
            string sql = "";

            if (str统计方式 == "2")
            {
                sql = @"select AA.*,BB.团队名称,CC.[机构名称] from
(
	select 团队ID,COUNT(DISTINCT 档案号) 签约数量
	from dbo.tb_JTYS签约信息 AA 
	where AA.状态='1' AND 签约日期 >= @begindate and  签约日期<=@enddate and [签约服务包] like '%'+@fwbid+'%' 
	GROUP BY AA.团队ID
) AA 
LEFT OUTER JOIN DBO.tb_JTYS团队概要 BB ON AA.团队ID = BB.团队ID
left outer join dbo.[tb_机构信息] CC ON BB.[所属机构]=cc.[机构编号]
where  CC.[机构名称]<>'' and CC.[机构名称] is not null ";
                if (!string.IsNullOrWhiteSpace(yyid))
                {
                    sql += " and CC.机构编号=@yyid  ";
                }

                sql += @"
order by cc.机构名称,bb.id";
            }
            else
            {
                sql = @"select AA.*,BB.团队名称,CC.[机构名称] from
(
	select 团队ID,COUNT(1) 签约数量
	from dbo.tb_JTYS签约信息 AA 
	where AA.状态='1' AND 签约日期 >= @begindate and  签约日期<=@enddate and [签约服务包] like '%'+@fwbid+'%' 
	GROUP BY AA.团队ID
) AA 
LEFT OUTER JOIN DBO.tb_JTYS团队概要 BB ON AA.团队ID = BB.团队ID
left outer join dbo.[tb_机构信息] CC ON BB.[所属机构]=cc.[机构编号]
where  CC.[机构名称]<>'' and CC.[机构名称] is not null 
";
                if (!string.IsNullOrWhiteSpace(yyid))
                {
                    sql += " and CC.机构编号=@yyid  ";
                }

                sql += @"
order by cc.机构名称,bb.id";
            }
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@yyid", SqlDbType.VarChar, yyid);
            cmd.AddParam("@begindate", SqlDbType.VarChar, begindate);
            cmd.AddParam("@enddate", SqlDbType.VarChar, enddate);
            cmd.AddParam("@fwbid", SqlDbType.VarChar, str服务包id);
            cmd.AddParam("@qyys", SqlDbType.VarChar, Loginer.CurrentUser.用户编码);
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds;
        }
    }
}

