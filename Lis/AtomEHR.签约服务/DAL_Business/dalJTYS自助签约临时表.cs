﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.签约服务.Models;
/*==========================================
 *   程序说明: JTYS自助签约临时表的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2019-03-12 02:52:13
 *   最后修改: 2019-03-12 02:52:13
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS自助签约临时表
    /// </summary>
    public class dalJTYS自助签约临时表 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalJTYS自助签约临时表(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_JTYS自助签约临时表.__KeyName; //主表的主键字段
             _SummaryTableName = tb_JTYS自助签约临时表.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_JTYS自助签约临时表.__TableName) ORM = typeof(tb_JTYS自助签约临时表);
             //if (tableName == tb_JTYS自助签约临时表s.__TableName) ORM = typeof(tb_JTYS自助签约临时表s);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS自助签约临时表.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_JTYS自助签约临时表.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_JTYS自助签约临时表.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_JTYS自助签约临时表]    where ["+tb_JTYS自助签约临时表.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_JTYS自助签约临时表s]   where ["+tb_JTYS自助签约临时表.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS自助签约临时表.__TableName;
              //ds.Tables[1].TableName =tb_JTYS自助签约临时表s.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_JTYS自助签约临时表s] where ["+tb_JTYS自助签约临时表.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_JTYS自助签约临时表] where ["+tb_JTYS自助签约临时表.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "JTYS自助签约临时表");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          public bool Update审批不通过(string spzt, string spbz, string spr, string[] ids)
          {
              StringBuilder sb = new StringBuilder();
              sb.Append("update [tb_JTYS自助签约临时表] set [审批状态]=@spzt, [审批备注]=@spbz,[审批人]=@spr,[审批时间]=convert(varchar(30), getdate(), 120)  where ID IN (");
              for (int index = 0; index < ids.Length; index++)
              {
                  if(index>0)
                  {
                      sb.Append(",");
                  }
                  sb.Append("@id" + index.ToString());
              }
              sb.Append(")");

              sb.Append(" and [审批状态] ='未审批'");

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());
              cmd.AddParam("@spzt", SqlDbType.VarChar, spzt);
              cmd.AddParam("@spbz", SqlDbType.VarChar, spbz);
              cmd.AddParam("@spr", SqlDbType.VarChar, spr);
              for (int index = 0; index < ids.Length; index++)
              {
                  cmd.AddParam("id"+index.ToString(), SqlDbType.VarChar, ids[index].ToString());
              }
              
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
              return i != 0;
          }



          public bool Update审批通过(string spzt, string spbz, string spr, string[] ids, string signImage)
          {
              StringBuilder sb = new StringBuilder();
              sb.Append(@"update [tb_JTYS自助签约临时表] set [审批状态]=@spzt, [审批备注]=@spbz,[签约医生]=@spr, [审批人]=@spr,[审批时间]=convert(varchar(30), getdate(), 120),
[生效日期]=convert(varchar(20), getdate(), 23),[到期日期]=convert(varchar(20), dateadd(day, -1, dateadd(year,1,getdate())), 23)
where  ");

              StringBuilder sbWhere = new StringBuilder();
              sbWhere.Append(" ID in ( ");
              for (int index = 0; index < ids.Length; index++)
              {
                  if (index > 0)
                  {
                      sbWhere.Append(",");
                  }
                  sbWhere.Append("@id" + index.ToString());
              }
              sbWhere.Append(")");

              sb.Append(sbWhere);
              sb.Append(" and [审批状态]='未审批' ");

              sb.Append(@"

insert into [dbo].[tb_JTYS签约信息]([档案号]
      ,[联系电话]
      ,[签约对象类型]
      ,[健康状况]
      ,[健康状况名称]
      ,[签约服务包]
      ,[服务包名称]
      ,[团队ID]
      ,[签约医生]
      ,[签约日期]
      ,[状态]
      ,[生效日期]
      ,[到期日期]
      ,[创建时间]
      ,[收费金额]
      ,[履约状态]
      ,[签约来源]
      ,[签约人]
      ,[手签照]
      ,[费用ID]
      ,[档案机构]
      ,[录入人机构],[创建人])
select [档案号]
      ,[联系电话]
      ,[签约对象类型]
      ,[健康状况]
      ,[健康状况名称]
      ,[签约服务包]
      ,[服务包名称]
      ,[团队ID]
      ,[签约医生]
      ,[签约日期]
      ,'1'
      ,[生效日期]
      ,[到期日期]
      ,[创建时间]
      ,0
      ,'未履约'
      ,[签约来源]
      ,[签约人]
      ,[手签照]
      ,[唯一标志]
      ,[档案机构]
      ,[录入人机构],@spr
FROM [dbo].[tb_JTYS自助签约临时表]
where " + sbWhere.ToString() + @" AND [审批状态]='审批通过' and  [唯一标志] not in (select [费用ID] from [dbo].[tb_JTYS签约信息]  where [费用ID] is not null);


              merge [dbo].[tb_JTYS签约医生签字] as target
using(select [唯一标志] FROM [dbo].[tb_JTYS自助签约临时表] AA where " + sbWhere.ToString() + @" AND [审批状态]='审批通过' and exists (select [费用ID] from [dbo].[tb_JTYS签约信息] where  [费用ID] is not null and 费用ID=AA.[唯一标志]) ) as source([费用ID]) on target.[费用ID] = source.[费用ID]
when not matched then 
    insert ([费用ID],[签字]) values(source.[费用ID], @signImage);");

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());
              cmd.AddParam("@spzt", SqlDbType.VarChar, spzt);
              cmd.AddParam("@spbz", SqlDbType.VarChar, spbz);
              cmd.AddParam("@spr", SqlDbType.VarChar, spr);
              cmd.AddParam("@signImage", SqlDbType.VarChar, signImage);
              for (int index = 0; index < ids.Length; index++)
              {
                  cmd.AddParam("id" + index.ToString(), SqlDbType.VarChar, ids[index].ToString());
              }

              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
              return i != 0;
          }

          public bool CheckApproved(string[] ids)
          {
              StringBuilder sb = new StringBuilder();
              sb.Append("select count(1) from [tb_JTYS自助签约临时表]  where ID IN (");
              for (int index = 0; index < ids.Length; index++)
              {
                  if (index > 0)
                  {
                      sb.Append(",");
                  }
                  sb.Append("@id" + index.ToString());
              }
              sb.Append(")");

              sb.Append(" and [审批状态] <>'未审批'");

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sb.ToString());
              for (int index = 0; index < ids.Length; index++)
              {
                  cmd.AddParam("id" + index.ToString(), SqlDbType.VarChar, ids[index].ToString());
              }

              object obj = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
              return obj.ToString()=="0"? false:true;
          }
     }
}

