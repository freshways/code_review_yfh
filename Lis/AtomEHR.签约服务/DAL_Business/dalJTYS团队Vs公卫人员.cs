﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.签约服务.Models;
/*==========================================
 *   程序说明: JTYS团队Vs公卫人员的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2019-03-12 02:51:38
 *   最后修改: 2019-03-12 02:51:38
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS团队Vs公卫人员
    /// </summary>
    public class dalJTYS团队Vs公卫人员 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalJTYS团队Vs公卫人员(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_JTYS团队Vs公卫人员.__KeyName; //主表的主键字段
             _SummaryTableName = tb_JTYS团队Vs公卫人员.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_JTYS团队Vs公卫人员.__TableName) ORM = typeof(tb_JTYS团队Vs公卫人员);
             //if (tableName == tb_JTYS团队Vs公卫人员s.__TableName) ORM = typeof(tb_JTYS团队Vs公卫人员s);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS团队Vs公卫人员.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_JTYS团队Vs公卫人员.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_JTYS团队Vs公卫人员.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_JTYS团队Vs公卫人员]    where ["+tb_JTYS团队Vs公卫人员.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_JTYS团队Vs公卫人员s]   where ["+tb_JTYS团队Vs公卫人员.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS团队Vs公卫人员.__TableName;
              //ds.Tables[1].TableName =tb_JTYS团队Vs公卫人员s.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_JTYS团队Vs公卫人员s] where ["+tb_JTYS团队Vs公卫人员.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_JTYS团队Vs公卫人员] where ["+tb_JTYS团队Vs公卫人员.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "JTYS团队Vs公卫人员");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }


          public bool InsertOrUpdate(string str机构, string 团队ID, string 团队名称, string 成员姓名, string 成员ID, string yhbm, string username, string p, string btn)
          {
              string sql2 = @"INSERT INTO [dbo].[tb_JTYS团队Vs公卫人员]
           ([医院名称]
           ,[团队ID]
           ,[团队名称]
           ,[成员ID]
           ,[成员姓名]
           ,[公卫人员编码]
           ,[公卫人员姓名]
           ,[创建人]
           ,[创建时间]
           ,[是否有效])
     VALUES
           (@str机构
           ,@团队ID
		   ,@团队名称
		   ,@成员ID
           ,@成员姓名
           ,@yhbm
           ,@username
           ,@创建人
           ,getdate()
           ,@newvalue)";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@str机构", SqlDbType.VarChar, str机构.Trim());
              cmd.AddParam("@团队ID", SqlDbType.VarChar, 团队ID.Trim());
              cmd.AddParam("@团队名称", SqlDbType.VarChar, 团队名称.Trim());
              cmd.AddParam("@成员姓名", SqlDbType.VarChar, 成员姓名.Trim());
              cmd.AddParam("@成员ID", SqlDbType.VarChar, 成员ID.Trim());
              cmd.AddParam("@yhbm", SqlDbType.VarChar, yhbm.Trim());
              cmd.AddParam("@username", SqlDbType.VarChar, username.Trim());
              cmd.AddParam("@创建人", SqlDbType.VarChar,p.Trim());
              cmd.AddParam("@newvalue", SqlDbType.VarChar, btn.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
              return i != 0;
          }

          internal bool InsertAndUpdate(string str机构, string 团队ID, string 团队名称, string 成员姓名, string 成员ID, string yhbm, string username, string p, string btn, string Bool)
          {
              string sql = @"UPDATE [dbo].[tb_JTYS团队Vs公卫人员]
   SET [修改人] = @创建人
      ,[修改时间] =getdate()
      ,[是否有效] ='0'
 WHERE 医院名称=@str机构 and 团队ID=@团队ID and 成员ID=@成员ID and 是否有效='1'
INSERT INTO [dbo].[tb_JTYS团队Vs公卫人员]
           ([医院名称]
           ,[团队ID]
           ,[团队名称]
           ,[成员ID]
           ,[成员姓名]
           ,[公卫人员编码]
           ,[公卫人员姓名]
           ,[创建人]
           ,[创建时间]
           ,[是否有效])
     VALUES
           (@str机构
           ,@团队ID
		   ,@团队名称
		   ,@成员ID
           ,@成员姓名
           ,@yhbm
           ,@username
           ,@创建人
           ,getdate()
           ,@newvalue)";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@str机构", SqlDbType.VarChar, str机构.Trim());
              cmd.AddParam("@团队ID", SqlDbType.VarChar, 团队ID.Trim());
              cmd.AddParam("@团队名称", SqlDbType.VarChar, 团队名称.Trim());
              cmd.AddParam("@成员姓名", SqlDbType.VarChar, 成员姓名.Trim());
              cmd.AddParam("@成员ID", SqlDbType.VarChar, 成员ID.Trim());
              cmd.AddParam("@yhbm", SqlDbType.VarChar, yhbm.Trim());
              cmd.AddParam("@username", SqlDbType.VarChar, username.Trim());
              cmd.AddParam("@创建人", SqlDbType.VarChar, p.Trim());
              cmd.AddParam("@newvalue", SqlDbType.VarChar, Bool.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
              return i != 0;
          }

          public bool CheckAuthority(string xzid, string loginUser用户编码, out string teamid, out string teamName, out string cyid)
          {
              string sql2 = @"select aa.团队ID,bb.团队名称,aa.ID 成员ID
from tb_JTYS团队Vs公卫人员 aa
	left outer join tb_JTYS团队概要 bb on aa.团队ID=bb.团队ID
where aa.是否有效=1 and bb.是否有效=1 and bb.所属机构=@ssjg and aa.公卫人员编码=@gwid";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              cmd.AddParam("@ssjg", SqlDbType.VarChar, xzid.Trim());
              cmd.AddParam("@gwid", SqlDbType.VarChar, loginUser用户编码.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              
              if(ds==null || ds.Tables.Count==0 || ds.Tables[0].Rows.Count == 0)
              {
                  teamid = "";
                  teamName = "";
                  cyid = "";
                  return false;
              }
              else
              {
                  teamid = ds.Tables[0].Rows[0]["团队ID"].ToString();
                  teamName = ds.Tables[0].Rows[0]["团队名称"].ToString();
                  cyid = ds.Tables[0].Rows[0]["成员ID"].ToString();
                  return true;
              }
          }
    }
}

