﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.签约服务.Models;
/*==========================================
 *   程序说明: JTYS团队地区管理关系的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2019-03-12 02:49:43
 *   最后修改: 2019-03-12 02:49:43
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS团队地区管理关系
    /// </summary>
    public class dalJTYS团队地区管理关系 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalJTYS团队地区管理关系(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_JTYS团队地区管理关系.__KeyName; //主表的主键字段
             _SummaryTableName = tb_JTYS团队地区管理关系.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_JTYS团队地区管理关系.__TableName) ORM = typeof(tb_JTYS团队地区管理关系);
             //if (tableName == tb_JTYS团队地区管理关系s.__TableName) ORM = typeof(tb_JTYS团队地区管理关系s);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS团队地区管理关系.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_JTYS团队地区管理关系.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_JTYS团队地区管理关系.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_JTYS团队地区管理关系]    where ["+tb_JTYS团队地区管理关系.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_JTYS团队地区管理关系s]   where ["+tb_JTYS团队地区管理关系.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS团队地区管理关系.__TableName;
              //ds.Tables[1].TableName =tb_JTYS团队地区管理关系s.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_JTYS团队地区管理关系s] where ["+tb_JTYS团队地区管理关系.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_JTYS团队地区管理关系] where ["+tb_JTYS团队地区管理关系.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "JTYS团队地区管理关系");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          public DataTable Get辖区内区域(string rgid)
          {
              string sql1 = @" select cc.地区编码,CC.地区名称, cc.详细地区名称 
from dbo.tb_机构乡镇对照 BB
left outer join dbo.tb_地区档案 cc on bb.地区编码=cc.上级编码
where bb.机构编号=@rgid";
              
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@rgid", SqlDbType.VarChar, rgid.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds.Tables[0];
          }

          public DataTable GetAreaManagedByTeam(string teamid)
          {
              string sql1 = @"SELECT [地区编码] FROM [dbo].[tb_JTYS团队地区管理关系] where [是否有效]=1 and 团队ID=@teamid";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@teamid", SqlDbType.VarChar, teamid.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds.Tables[0];
          }

          public bool InsertOrUpdate(string teamid, string areaid, string opuser, bool newvalue)
          {
              string sql = @"
MERGE dbo.[tb_JTYS团队地区管理关系] AS target
    USING (select aa.所属机构,bb.机构名称, aa.团队ID,aa.团队名称,@newvalue 是否有效, @opuser OPuser,@dqid 地区编码, (select top 1 cc.地区名称 from dbo.tb_地区档案 cc where cc.地区编码=@dqid) [地区名称]
from dbo.tb_JTYS团队概要 aa
	left outer join dbo.tb_机构信息 bb on aa.所属机构=bb.机构编号
where aa.团队ID=@teamid ) AS source (所属机构, 机构名称,团队ID,团队名称,是否有效,OPuser,地区编码,[地区名称])  
    ON (target.团队ID = source.团队ID and target.地区编码 = source.地区编码)   
	WHEN MATCHED THEN 
		UPDATE SET 是否有效 = source.是否有效,[修改人]=source.OPuser, [修改时间]=getdate()
	WHEN  NOT MATCHED THEN  
		INSERT (所属机构, 机构名称,团队ID,团队名称,地区编码,[地区名称], 是否有效,创建人, 创建时间)  
		VALUES (source.所属机构, source.机构名称, source.团队ID, source.团队名称, source.地区编码, source.地区名称, source.是否有效, source.OPuser, getdate());
";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
              cmd.AddParam("@dqid", SqlDbType.VarChar, areaid.Trim());
              cmd.AddParam("@teamid", SqlDbType.VarChar, teamid.Trim());
              cmd.AddParam("@opuser", SqlDbType.VarChar, opuser.Trim());
              cmd.AddParam("@newvalue", SqlDbType.Bit, newvalue);
              int count = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);

              return count>0;
          }
     }
}

