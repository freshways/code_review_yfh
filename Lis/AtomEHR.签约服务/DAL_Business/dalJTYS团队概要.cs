﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: JTYS团队概要的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2017-12-13 02:23:28
 *   最后修改: 2017-12-13 02:23:28
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS团队概要
    /// </summary>
    public class dalJTYS团队概要 : dalBaseBusiness
    {
        /// <summary>
        /// 构造器
        /// </summary>
        /// <param name="loginer">当前登录用户</param>
        public dalJTYS团队概要(Loginer loginer)
            : base(loginer)
        {
            _SummaryKeyName = tb_JTYS团队概要.__KeyName; //主表的主键字段
            _SummaryTableName = tb_JTYS团队概要.__TableName;//主表表名
            _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
        }

        /// <summary>
        /// 根据表名获取该表的ＳＱＬ命令生成器
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
        {
            Type ORM = null;
            if (tableName == tb_JTYS团队概要.__TableName) ORM = typeof(tb_JTYS团队概要);
            //if (tableName == tb_JTYS团队概要s.__TableName) ORM = typeof(tb_JTYS团队概要s);//如有明细表请调整本行的代码
            if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
            return new GenerateSqlCmdByTableFields(ORM);
        }

        /// <summary>
        /// 查询功能，获取主表数据
        /// </summary>
        public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
        {
            StringBuilder sql = new StringBuilder();
            //
            //在这里生成SQL,根据自己的字段定义修改
            //
            //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS团队概要.__KeyName +>='  docNoFrom ');
            //if (docNoTo != string.Empty) sql.Append( and tb_JTYS团队概要.__KeyName + <=' + docNoTo + ');

            if (sql.ToString() != "") //有查询条件
            {
                string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName, cmd.SqlCommand, tb_JTYS团队概要.__TableName);
                return dt;
            }
            else //无查询条件不返回数据
            {
                throw new Exception("没有指定查询条件!");
                //return null;
            }
        }

        /// <summary>
        /// 获取一张业务单据的数据
        /// </summary>
        /// <param name="docNo">单据号码</param>
        /// <returns></returns>
        public System.Data.DataSet GetBusinessByKey(string docNo)
        {
            string sql1 = " select * from [tb_JTYS团队概要]    where [" + tb_JTYS团队概要.__KeyName + "]=@DocNo1 ";
            //string sql2 = " select * from [tb_JTYS团队概要s]   where ["+tb_JTYS团队概要.__KeyName+"]=@DocNo2 "; //子表
            //如果有多个子表请继续添加
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_JTYS团队概要.__TableName;
            //ds.Tables[1].TableName =tb_JTYS团队概要s.__TableName;//子表
            return ds;
        }

        public DataSet GetBusinessByTeamID(string teamid)
        {
            string sql1 = " select * from [tb_JTYS团队概要]    where [" + tb_JTYS团队概要.团队ID + "]=@teamid and [" + tb_JTYS团队概要.是否有效 + "]=1 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@teamid", SqlDbType.VarChar, teamid.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            ds.Tables[0].TableName = tb_JTYS团队概要.__TableName;
            return ds;
        }

        /// <summary>
        ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
        /// </summary>
        public bool Delete(string docNo)
        {
            //删除单据:从子表开始删除!!!
            //string sql1 = "delete [tb_JTYS团队概要s] where ["+tb_JTYS团队概要.__KeyName+"]=@DocNo1 ";//删除子表
            string sql2 = "delete [tb_JTYS团队概要] where [" + tb_JTYS团队概要.__KeyName + "]=@DocNo2 ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
            //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
            cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
            int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return i != 0;
        }


        public int Update是否禁用(string teamid, bool bvalidate)
        {
            string sql = "update [tb_JTYS团队概要] set [是否有效]=@bv where [团队ID]=@teamid ";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
            cmd.AddParam("@bv", SqlDbType.Bit, bvalidate);
            cmd.AddParam("@teamid", SqlDbType.VarChar, teamid);
            int count = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);
            return count;
        }
        /// <summary>
        //保存数据
        /// </summary>
        public SaveResult Update(DataSet data, bool createBusinessLog)
        {
            //其它处理...
            //调用基类的方法保存数据
            return base.Update(data);
        }

        /// <summary>
        //获取单据流水号码
        /// </summary>
        protected override string GetNumber(SqlTransaction tran)
        {
            string docNo = DocNoTool.GetNumber(tran, "JTYS团队概要");
            return docNo;
        }

        /// <summary>
        /// 获取报表数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
        {
            return null;
        }

        public DataSet GetTeamCapNameAndTel(string teamid)
        {
            string sql1 = @" select AA.*,BB.DocName,BB.TelNo
from 
(SELECT 
      [组长类别]
      ,[组长编号]
  FROM [dbo].[tb_JTYS团队概要]
  where [团队ID]=@teamid AND 组长类别='医生'
  ) AA
  LEFT OUTER JOIN dbo.tb_JTYS医生信息 BB ON AA.组长编号 =BB.DocID

  union all

select CC.*,DD.[UserName] DocName,DD.[Tel] TelNo
from 
(SELECT 
      [组长类别]
      ,[组长编号]
  FROM [dbo].[tb_JTYS团队概要]
  where [团队ID]=@teamid AND 组长类别='公卫人员'
  ) CC
  left outer join tb_MyUser DD on CC.组长编号 = DD.用户编码 

select AA.*,BB.DocName,BB.TelNo
from 
(SELECT 
      ID,[成员类别]
      ,[成员ID]
  FROM [dbo].[tb_JTYS团队成员]
  where [团队ID]=@teamid AND [成员类别]='医生' and [是否有效]=1
  ) AA
  LEFT OUTER JOIN dbo.tb_JTYS医生信息 BB ON AA.[成员ID] =BB.DocID 

  union all

select CC.*,DD.[UserName] DocName,DD.[Tel] TelNo
from 
(SELECT 
      ID,[成员类别]
      ,[成员ID]
  FROM [dbo].[tb_JTYS团队成员]
  where [团队ID]=@teamid AND [成员类别]='公卫人员' and [是否有效]=1
  ) CC
  left outer join tb_MyUser DD on CC.[成员ID] = DD.用户编码

";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@teamid", SqlDbType.VarChar, teamid.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

            return ds;
        }

        public DataSet GetTeamCapNameAndTelByXZ(string xz)
        {
            string sql1 = @"
declare @teamid varchar(50)
SELECT top 1 @teamid=[团队ID]
  FROM [dbo].[tb_JTYS团队成员]
  where [团队ID] in (   select [团队ID] 
                        from dbo.tb_JTYS团队概要 
						where [所属机构]=@xz and [是否有效]='1') 
		and [是否有效]='1'
  group by  [团队ID]
  order by count(1) desc

SELECT 
      [组长类别]
      ,[组长编号]
  FROM [dbo].[tb_JTYS团队概要]
  where [团队ID]=@teamid 

SELECT AAA.ID,AAA.成员ID,AAA.成员类别,AAA.DocName,AAA.TelNo,BBB.角色名称 DocRole
FROM
(
select AA.*,BB.DocName,BB.TelNo
from 
(SELECT 
      ID,[成员类别]
      ,[成员ID], DocRole
  FROM [dbo].[tb_JTYS团队成员]
  where [团队ID]=@teamid AND [成员类别]='医生' and [是否有效]=1
  ) AA
  LEFT OUTER JOIN dbo.tb_JTYS医生信息 BB ON AA.[成员ID] =BB.DocID 

  union all

select CC.*,DD.[UserName] DocName,DD.[Tel] TelNo
from 
(SELECT 
      ID,[成员类别]
      ,[成员ID], DocRole
  FROM [dbo].[tb_JTYS团队成员]
  where [团队ID]=@teamid AND [成员类别]='公卫人员' and [是否有效]=1
  ) CC
  left outer join tb_MyUser DD on CC.[成员ID] = DD.用户编码
  ) AAA
  LEFT OUTER JOIN DBO.tb_JTYS团队角色 BBB ON AAA.DocRole=BBB.ID
  ORDER BY BBB.排序

";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@xz", SqlDbType.VarChar, xz.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

            return ds;
        }

        public DataSet GetTeamInfoByteamid(string teamid)
        {
            string sql1 = @"
SELECT 
      [组长类别]
      ,[组长编号]
  FROM [dbo].[tb_JTYS团队概要]
  where [团队ID]=@teamid 

SELECT AAA.ID,AAA.成员ID,AAA.成员类别,AAA.DocName,AAA.TelNo,BBB.角色名称 DocRole
FROM
(
select AA.*,BB.DocName,BB.TelNo
from 
(SELECT 
      ID,[成员类别]
      ,[成员ID], DocRole
  FROM [dbo].[tb_JTYS团队成员]
  where [团队ID]=@teamid AND [成员类别]='医生' and [是否有效]=1
  ) AA
  LEFT OUTER JOIN dbo.tb_JTYS医生信息 BB ON AA.[成员ID] =BB.DocID 

  union all

select CC.*,DD.[UserName] DocName,DD.[Tel] TelNo
from 
(SELECT 
      ID,[成员类别]
      ,[成员ID], DocRole
  FROM [dbo].[tb_JTYS团队成员]
  where [团队ID]=@teamid AND [成员类别]='公卫人员' and [是否有效]=1
  ) CC
  left outer join tb_MyUser DD on CC.[成员ID] = DD.用户编码
  ) AAA
  LEFT OUTER JOIN DBO.tb_JTYS团队角色 BBB ON AAA.DocRole=BBB.ID
  ORDER BY BBB.排序

";

            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@teamid", SqlDbType.VarChar, teamid.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);

            return ds;
        }

        public DataTable GetTeamInfoByRgid(string rgid)
        {
            string sql1 = "SELECT [团队ID] ,[所属机构],[团队名称] FROM [dbo].[tb_JTYS团队概要] where 是否有效=1 and 所属机构=@rgid";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@rgid", SqlDbType.VarChar, rgid.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }
        public DataTable GetTeamInfoByRgid2(string rgid)
        {
            string sql1 = "SELECT a.ID,a.团队ID,a.团队名称,(SELECT b.成员姓名 FROM tb_JTYS团队成员 b where b.团队ID=a.团队ID and b.成员ID=a.组长编号) as 团队长 FROM tb_JTYS团队概要 a where 是否有效=1 and 所属机构=@rgid";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@rgid", SqlDbType.VarChar, rgid.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }

        public DataTable GetTeamInfoByRgid3(string rgid)
        {
            string sql1 = @"SELECT a.团队ID
,a.团队名称
,b.成员姓名
,b.ID as 成员ID
,d.Account
,d.UserName
FROM tb_JTYS团队概要 a 
left join tb_JTYS团队成员 b on a.团队ID=b.团队ID
left join tb_JTYS团队Vs公卫人员 c on a.团队ID=c.团队ID and b.ID=c.成员ID
left join tb_MyUser d on c.公卫人员编码=d.用户编码
where
1=1 
and a.是否有效='1'
and b.是否有效='1'
and (d.IsLocked='0' or c.是否有效 is null)
and (c.是否有效='1' or c.是否有效 is null)
and a.所属机构=@rgid
order by a.团队ID";
            SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
            cmd.AddParam("@rgid", SqlDbType.VarChar, rgid.Trim());
            DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
            return ds.Tables[0];
        }
    }
}

