﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: JTYSPageSP的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2018/07/16 07:30:06
 *   最后修改: 2018/07/16 07:30:06
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYSPageSP
    /// </summary>
    public class dalJTYSPageSP : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalJTYSPageSP(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_JTYSPageSP.__KeyName; //主表的主键字段
             _SummaryTableName = tb_JTYSPageSP.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_JTYSPageSP.__TableName) ORM = typeof(tb_JTYSPageSP);
             //if (tableName == tb_JTYSPageSPs.__TableName) ORM = typeof(tb_JTYSPageSPs);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_JTYSPageSP.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_JTYSPageSP.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_JTYSPageSP.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  //return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_JTYSPageSP]    where ["+tb_JTYSPageSP.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_JTYSPageSPs]   where ["+tb_JTYSPageSP.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYSPageSP.__TableName;
              //ds.Tables[1].TableName =tb_JTYSPageSPs.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_JTYSPageSPs] where ["+tb_JTYSPageSP.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_JTYSPageSP] where ["+tb_JTYSPageSP.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "JTYSPageSP");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

          public DataTable GetAllValidateSP(string rgid)
          {
              string sql1 = @"select AA.ServiceID,AA.名称,BB.name1,BB.name2,BB.name3,BB.pageNo,BB.orderby,BB.totalPrice
from 
(
	select * from dbo.tb_JTYS服务包 where 所属机构=@rgid and 是否有效=1
) AA
LEFT OUTER JOIN DBO.tb_JTYSPageSP BB ON AA.ServiceID=BB.SPID ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@rgid", SqlDbType.VarChar, rgid.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYSPageSP.__TableName;
              return ds.Tables[0];
          }


          public DataSet GetBusinessBySPID(string spid)
          {
              string sql1 = @"select * from DBO.tb_JTYSPageSP where SPID=@spid ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@spid", SqlDbType.VarChar, spid.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYSPageSP.__TableName;
              return ds;
          }

          public DataSet GetBusinessGroupByName1(string rgid, int pageno)
          {
              string sql = @"
select AA.name1
from dbo.tb_JTYSPageSP AA
	LEFT OUTER JOIN DBO.tb_JTYS服务包 BB ON AA.SPID=BB.ServiceID
	WHERE BB.所属机构=@ssjg and bb.是否有效=1 AND AA.pageNo=@pageno
group by AA.name1

select AA.name1,AA.name2,AA.name3,AA.pageNo,AA.orderby,AA.totalPrice total,
  BB.服务内容 contents,BB.适宜对象 Serobj,BB.年收费标准 actual
from dbo.tb_JTYSPageSP AA
	LEFT OUTER JOIN DBO.tb_JTYS服务包 BB ON AA.SPID=BB.ServiceID
	WHERE BB.所属机构=@ssjg and bb.是否有效=1 AND AA.pageNo=@pageno
ORDER BY AA.pageNo,AA.name1,AA.orderby";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
              cmd.AddParam("@ssjg", SqlDbType.VarChar, rgid.Trim());
              cmd.AddParam("@pageno", SqlDbType.Int, pageno);
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYSPageSP.__TableName;
              return ds;
          }

          public DataSet GetBusinessGroupByFYID(string rgid, string fyid, int pageno)
          {
              string sql = @"
select AA.name1
from dbo.tb_JTYSPageSP AA
	LEFT OUTER JOIN DBO.tb_JTYS服务包 BB ON AA.SPID=BB.ServiceID
	WHERE BB.所属机构=@ssjg and bb.是否有效=1 AND AA.pageNo=@pageno
group by AA.name1

select AA.SPID,AA.name1,AA.name2,AA.name3,AA.pageNo,AA.orderby,
  BB.服务内容 contents,BB.适宜对象 Serobj,
  case when AA.totalPrice =0 THEN '免费' else convert(varchar, AA.totalPrice) end total,
  case when AA.totalPrice=0 then null else isnull(CC.PRICE, BB.年收费标准) end actual,
  CASE WHEN AA.totalPrice=0 then null else AA.totalPrice-isnull(CC.PRICE, BB.年收费标准) end youhui
from dbo.tb_JTYSPageSP AA
	LEFT OUTER JOIN DBO.tb_JTYS服务包 BB ON AA.SPID=BB.ServiceID
	left outer join (
		SELECT * FROM DBO.tb_JTYS签约费用明细 WHERE 费用ID=@fyid
	) CC on aa.SPID=cc.SPID
	WHERE BB.所属机构=@ssjg and bb.是否有效=1 AND AA.pageNo=@pageno
ORDER BY AA.pageNo,AA.name1,AA.orderby";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
              cmd.AddParam("@fyid", SqlDbType.VarChar, fyid.Trim());
              cmd.AddParam("@ssjg", SqlDbType.VarChar, rgid.Trim());
              cmd.AddParam("@pageno", SqlDbType.Int, pageno);
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYSPageSP.__TableName;


              DataSet dsRet = new DataSet();
              if (ds.Tables.Count == 2)
              {
                  for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                  {
                      DataRow[] drs = ds.Tables[1].Select("name1='" + ds.Tables[0].Rows[index]["name1"].ToString() + "'");
                      if (drs.Length > 0)
                      {
                          DataTable dtTemp = drs.CopyToDataTable();

                          dsRet.Tables.Add(dtTemp);
                      }
                  }
              }

              return dsRet;
          }
     }
}

