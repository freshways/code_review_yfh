﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.签约服务.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;

/*==========================================
 *   程序说明: JTYS履约执行明细的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2017-12-27 10:41:40
 *   最后修改: 2017-12-27 10:41:40
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS履约执行明细
    /// </summary>
    public class dalJTYS履约执行明细 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalJTYS履约执行明细(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_JTYS履约执行明细.__KeyName; //主表的主键字段
             _SummaryTableName = tb_JTYS履约执行明细.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_JTYS履约执行明细.__TableName) ORM = typeof(tb_JTYS履约执行明细);
             //if (tableName == tb_JTYS履约执行明细s.__TableName) ORM = typeof(tb_JTYS履约执行明细s);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS履约执行明细.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_JTYS履约执行明细.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_JTYS履约执行明细.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  //return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_JTYS履约执行明细]    where ["+tb_JTYS履约执行明细.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_JTYS履约执行明细s]   where ["+tb_JTYS履约执行明细.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS履约执行明细.__TableName;
              //ds.Tables[1].TableName =tb_JTYS履约执行明细s.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_JTYS履约执行明细s] where ["+tb_JTYS履约执行明细.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_JTYS履约执行明细] where ["+tb_JTYS履约执行明细.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "JTYS履约执行明细");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qyid"></param>
        /// <returns>true:表示有履约记录，false:表示没有履约记录</returns>
          public bool CheckExistBy签约ID(string qyid)
          {
              string sql2 = "select count(1) from [tb_JTYS履约执行明细] where [" + tb_JTYS履约执行明细.签约ID + "]=@qyid ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              object obj = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
              return Convert.ToInt32(obj) != 0;
          }

          public DataSet Get服务包执行情况(string qyid, string[] serviceIDs)
          {
              StringBuilder pb = new StringBuilder();
              for(int index = 0; index < serviceIDs.Length; index++)
              {
                  pb.Append("@SID"+index.ToString());
                  if(index < serviceIDs.Length-1)
                  {
                      pb.Append(",");
                  }
              }
              string sql1 = "";
              if(pb.Length > 0)
              {
                  sql1 = @"select AAA.*
	,ISNULL(BBB.执行总量,0) 执行总量
    ,@qyid 签约ID
 from
(
	SELECT TOP 1000 AA.[ID]
		  ,AA.[ServiceID]
		  ,BB.名称 服务包名称
		  ,AA.[项目类别]+'_'+AA.[项目ID] 项目复合ID
		  ,AA.[项目编码]
		  ,AA.[项目名称]
		  ,AA.[单位]
		  ,AA.[数量]
         ,KK.[检验项目]
         ,KK.[检查项目]
         ,KK.[项目分组]
         ,AA.[总价]
	  FROM [tb_JTYS服务包医疗项目] AA
      left outer join [tb_JTYS自定义项目] KK ON (AA.[项目类别]='自定义' and AA.[项目ID]=KK.[项目ID])
	  join tb_JTYS服务包 BB ON AA.ServiceID=BB.ServiceID
	  WHERE AA.是否有效 =1 AND BB.是否有效=1
	  AND aa.ServiceID IN (" + pb.ToString()+ @")
  ) AAA
  LEFT OUTER JOIN 
  (
	SELECT ServiceID,[项目复合ID],sum([执行数量]) 执行总量
	  FROM [dbo].[tb_JTYS履约执行明细]
	  where [签约ID]=@qyid AND ServiceID IN (" + pb.ToString() + @")
	  group by ServiceID,[项目复合ID]
  ) BBB ON AAA.ServiceID=BBB.ServiceID and aaa.项目复合ID=bbb.项目复合ID ";
              }

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              for(int index = 0; index < serviceIDs.Length; index++)
              {
                  cmd.AddParam("@SID" + index.ToString(), SqlDbType.VarChar, serviceIDs[index].Trim());
              }
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds;
          }


          public DataSet Get执行明细ByQYID(string qyid)
          {
              string sql1 = @"select AA.ID
		,AA.签约ID
		,AA.[ServiceID]
		,aa.服务包名称
		,aa.项目复合ID
		,aa.项目名称
		,aa.执行数量
		,CONVERT(VARCHAR,AA.服务时间,23) 服务时间
		,AA.项目单位
        ,AA.执行条码
        ,AA.执行结果
		,isnull(bb.UserName,AA.服务医生) 服务医生姓名
   from [dbo].[tb_JTYS履约执行明细] AA
   left outer join dbo.tb_MyUser BB ON AA.服务医生=bb.用户编码 
where AA.签约ID=@qyid 
order by AA.服务时间 DESC,AA.创建时间 DESC,AA.[ServiceID],aa.项目复合ID  ";
              
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds;
          }

          public DataSet Get执行明细ByQYIDofDate(string qyid,string date)
          {
              string sql1 = @"select AA.ID
		,AA.签约ID
		,AA.[ServiceID]
		,aa.服务包名称
		,aa.项目复合ID
		,aa.项目名称
		,aa.执行数量
		,CONVERT(VARCHAR,AA.服务时间,23) 服务时间
		,AA.项目单位
        ,AA.执行条码
        ,AA.执行结果
		,bb.UserName 服务医生姓名
		,开单科室
		,执行科室
		,CC.单价
		,(CC.单价*AA.执行数量) as 总价
   from [dbo].[tb_JTYS履约执行明细] AA
   left outer join dbo.tb_MyUser BB ON AA.服务医生=bb.用户编码 
   left outer join dbo.tb_JTYS自定义项目 CC ON AA.项目复合ID=('自定义_'+cast(CC.项目ID as varchar))
where AA.签约ID=@qyid and AA.服务时间=@date
order by AA.服务时间 DESC,AA.创建时间 DESC,AA.[ServiceID],aa.项目复合ID  ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              cmd.AddParam("@date", SqlDbType.VarChar, date.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds;
          }

          public DataSet Get检验检查项目(string qyid, string createtime)
          {
              string sql1 = @"SELECT AA.*, bb.项目分组
  FROM [dbo].[tb_JTYS履约执行明细] AA
  left outer join dbo.tb_JTYS自定义项目 BB ON AA.项目复合ID=('自定义_'+cast(BB.项目ID as varchar))
  where BB.检验项目=1 and AA.签约ID=@qyid and AA.创建时间=@createtime";

//SELECT AA.*,bb.项目分组
//  FROM [dbo].[tb_JTYS履约执行明细] AA
//  left outer join dbo.tb_JTYS自定义项目 BB ON AA.项目复合ID=('自定义_'+cast(BB.项目ID as varchar))
//  where BB.检查项目=1 and AA.签约ID=@qyid and AA.创建时间=@createtime ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              cmd.AddParam("@createtime", SqlDbType.VarChar, createtime.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds;
          }

          public DataSet Get检验检查项目WithBarcode(string qyid, string barcode)
          {
              string sql1 = @"SELECT AA.[项目名称]
  FROM [dbo].[tb_JTYS履约执行明细] AA
  where AA.签约ID=@qyid and AA.执行条码=@barcode";

              //SELECT AA.*,bb.项目分组
              //  FROM [dbo].[tb_JTYS履约执行明细] AA
              //  left outer join dbo.tb_JTYS自定义项目 BB ON AA.项目复合ID=('自定义_'+cast(BB.项目ID as varchar))
              //  where BB.检查项目=1 and AA.签约ID=@qyid and AA.创建时间=@createtime ";

              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              cmd.AddParam("@barcode", SqlDbType.VarChar, barcode.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds;
          }

          public string GetSerialCode()
          {
              string sql1 = @"SP_GetBarSerialCode";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.SqlCommand.CommandType = CommandType.StoredProcedure;
              object obj = DataProvider.Instance.ExecuteScalar(_Loginer.DBName, cmd.SqlCommand);
              return obj.ToString();
          }

          public DataSet Get化验结果ByBarcode(string barcode)
          {
              string sql1 = @"select fitem_code,fitem_name,fitem_unit,fitem_ref,fitem_badge,forder_by,fvalue,fname_j  ,sfzh,fjy_date
                                from LISDB.SYSXT.dbo.vw_InterFace_Result  where fjy_zt='已审核' and  fapply_id=@barcode order by cast(forder_by as int)";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@barcode", SqlDbType.VarChar, barcode.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds;
          }

          public void UpdateLYZT(string qyid)
          {
              string sql1 = "usp_JTYSUpdateState";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@qyid", SqlDbType.VarChar, qyid.Trim());
              DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand, CommandType.StoredProcedure);
          }
     }
}

