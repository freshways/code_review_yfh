﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.ORM;
using AtomEHR.Server.DataAccess.DAL_Base;
using AtomEHR.Server.DataAccess.DAL_System;
using AtomEHR.签约服务.Models;
/*==========================================
 *   程序说明: JTYS自助服务包的数据访问层
 *   作者姓名: ATOM
 *   创建日期: 2019-03-12 03:12:12
 *   最后修改: 2019-03-12 03:12:12
 *   
 *   注: 本代码由[代码生成器]自动生成
 *   版权所有 Copyright © . 2015
 *==========================================*/

namespace AtomEHR.签约服务.Server.DataAccess
{
    /// <summary>
    /// dalJTYS自助服务包
    /// </summary>
    public class dalJTYS自助服务包 : dalBaseBusiness
    {
         /// <summary>
         /// 构造器
         /// </summary>
         /// <param name="loginer">当前登录用户</param>
         public dalJTYS自助服务包(Loginer loginer): base(loginer)
         {
             _SummaryKeyName = tb_JTYS自助服务包.__KeyName; //主表的主键字段
             _SummaryTableName = tb_JTYS自助服务包.__TableName;//主表表名
             _UpdateSummaryKeyMode = UpdateKeyMode.OnlyDocumentNo;//单据号码生成模式
         }

         /// <summary>
         /// 根据表名获取该表的ＳＱＬ命令生成器
         /// </summary>
         /// <param name="tableName">表名</param>
         /// <returns></returns>
         protected override IGenerateSqlCommand CreateSqlGenerator(string tableName)
         {
             Type ORM = null;
             if (tableName == tb_JTYS自助服务包.__TableName) ORM = typeof(tb_JTYS自助服务包);
             //if (tableName == tb_JTYS自助服务包s.__TableName) ORM = typeof(tb_JTYS自助服务包s);//如有明细表请调整本行的代码
             if (ORM == null) throw new Exception(tableName + "表没有ORM模型！");
             return new GenerateSqlCmdByTableFields(ORM);
         }

          /// <summary>
          /// 查询功能，获取主表数据
          /// </summary>
          public DataTable GetSummaryByParam(string docNoFrom, string docNoTo, DateTime docDateFrom, DateTime docDateTo)
          {
              StringBuilder sql = new StringBuilder();
              //
              //在这里生成SQL,根据自己的字段定义修改
              //
              //if (docNoFrom != string.Empty) sql.Append(and tb_JTYS自助服务包.__KeyName +>='  docNoFrom ');
              //if (docNoTo != string.Empty) sql.Append( and tb_JTYS自助服务包.__KeyName + <=' + docNoTo + ');

              if (sql.ToString() != "") //有查询条件
              {
                  string query = "select * from " + _SummaryTableName + " where 1=1 " + sql.ToString();
                  SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(query);
                  DataTable dt = DataProvider.Instance.GetTable(_Loginer.DBName,cmd.SqlCommand, tb_JTYS自助服务包.__TableName);
                  return dt;
              }
              else //无查询条件不返回数据
              {
                  throw new Exception("没有指定查询条件!");
                  return null;
              }
          }

          /// <summary>
          /// 获取一张业务单据的数据
          /// </summary>
          /// <param name="docNo">单据号码</param>
          /// <returns></returns>
          public System.Data.DataSet GetBusinessByKey(string docNo)
          {
              string sql1 = " select * from [tb_JTYS自助服务包]    where ["+tb_JTYS自助服务包.__KeyName+"]=@DocNo1 ";
              //string sql2 = " select * from [tb_JTYS自助服务包s]   where ["+tb_JTYS自助服务包.__KeyName+"]=@DocNo2 "; //子表
              //如果有多个子表请继续添加
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              //cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName,cmd.SqlCommand);
              ds.Tables[0].TableName = tb_JTYS自助服务包.__TableName;
              //ds.Tables[1].TableName =tb_JTYS自助服务包s.__TableName;//子表
              return ds;
          }

          /// <summary>
          ///删除一张单据:可以考虑非物理删除，这样有可挽回的余地!!!
          /// </summary>
          public bool Delete(string docNo)
          {
              //删除单据:从子表开始删除!!!
              //string sql1 = "delete [tb_JTYS自助服务包s] where ["+tb_JTYS自助服务包.__KeyName+"]=@DocNo1 ";//删除子表
              string sql2 = "delete [tb_JTYS自助服务包] where ["+tb_JTYS自助服务包.__KeyName+"]=@DocNo2 ";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql2);
              //cmd.AddParam("@DocNo1", SqlDbType.VarChar, docNo.Trim());
              cmd.AddParam("@DocNo2", SqlDbType.VarChar, docNo.Trim());
              int i = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName,cmd.SqlCommand);
              return i != 0;
          }

          /// <summary>
          //保存数据
          /// </summary>
          public SaveResult Update(DataSet data, bool createBusinessLog)
          {
              //其它处理...
              //调用基类的方法保存数据
              return base.Update(data);
          }

          /// <summary>
          //获取单据流水号码
          /// </summary>
          protected override string GetNumber(SqlTransaction tran)
          {
              string docNo = DocNoTool.GetNumber(tran, "JTYS自助服务包");
              return docNo;
          }

          /// <summary>
          /// 获取报表数据
          /// </summary>
          /// <returns></returns>
          public DataSet GetReportData(string DocNoFrom, string DocNoTo, DateTime DateFrom, DateTime DateTo)
          {
              return null;
          }


          public bool InsertOrUpdate(string packid, string opuser, bool newvalue)
          {
              string sql = @"MERGE dbo.[tb_JTYS自助服务包] AS target
    USING 
	(SELECT @packid,@newvalue, @opuser)	 AS source (SPID,是否有效,OPuser) ON (target.SPID = source.SPID)   
	WHEN MATCHED THEN 
		UPDATE SET 是否有效 = source.是否有效,[修改人]=source.OPuser, [修改时间]=getdate()
	WHEN  NOT MATCHED THEN  
		INSERT (SPID,是否有效,创建人, 创建时间)  
		VALUES (source.SPID, source.是否有效, source.OPuser, getdate());";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql);
              cmd.AddParam("@packid", SqlDbType.VarChar, packid.Trim());
              cmd.AddParam("@opuser", SqlDbType.VarChar, opuser.Trim());
              cmd.AddParam("@newvalue", SqlDbType.Bit, newvalue);
              int count = DataProvider.Instance.ExecuteNoQuery(_Loginer.DBName, cmd.SqlCommand);

              return count > 0;
          }

          public DataTable GetPackBypackid(string packid)
          {
              string sql1 = @"SELECT aa.SPID,bb.名称 as 名称 FROM tb_JTYS自助服务包 aa left join tb_JTYS服务包 bb on aa.SPID=bb.ServiceID where aa.是否有效='1' and aa.SPID=@teamid";
              SqlCommandBase cmd = SqlBuilder.BuildSqlCommandBase(sql1);
              cmd.AddParam("@packid", SqlDbType.VarChar, packid.Trim());
              DataSet ds = DataProvider.Instance.GetDataSet(_Loginer.DBName, cmd.SqlCommand);
              return ds.Tables[0];
          }
    }
}

