﻿using System;
using System.Collections.Generic;
using System.Text;

namespace yunLis.dao
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public class DBDataType
    {
        #region  Access
        /// <summary>
        /// 文本
        /// </summary>
        public static string strVarchar_Access = "TEXT";
        /// <summary>
        /// 数字
        /// </summary>
        public static string strFloat_Access = "FLOAT";
        #endregion

        #region  Sql Server
        /// <summary>
        /// 文本
        /// </summary>
        public static string strVarchar_SqlServer = "VARCHAR";
        /// <summary>
        /// 数字
        /// </summary>
        public static string strFloat_SqlServer = "FLOAT";
        #endregion

        #region Oracle
        /// <summary>
        /// 文本
        /// </summary>
        public static string strVarchar_Oracle = "VARCHAR";
        /// <summary>
        /// 数字
        /// </summary>
        public static string strFloat_Oracle = "FLOAT";
        #endregion
    }
}
