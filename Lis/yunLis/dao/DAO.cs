using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Management;


/*
 using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
 */
namespace yunLis.dao
{
    /// <summary>
    /// 数据库存取代理
    /// </summary>
    /// <remarks>
    /// 作者：tao
    /// 时间：2008.2
    /// 改动：[tao；2008.2]
    /// </remarks>
    public class DAO
    {


        public DAO()
        {
            ///
            /// TODO: 在此处添加构造函数逻辑
            ///       
            //string connString = "server=TAOYINZH;database=yssoft;uid=sa;pwd=";
            //Database nocofigdb = new Database(connString);           
        }
        /*
       #region 代理指定数据库连接
       
       /// <summary>
       /// 执行 数据集 只读
       /// </summary>
       /// <param name="strConn"></param>
       /// <param name="strSql"></param>
       /// <returns></returns>      
       public DataSet DbExecuteDataSetByConnSQL(string _strConn, string strSql)
       {
            
           DataSet DataSetObject = null;
           //data source=TAOYINZH;Initial Catalog=yssoft;User ID=sa;Password=;Provider=SQLOLEDB.1;Persist Security Info=True;
           string connString = "server=.;database=EnterpriseLibrary;uid=sa;pwd=";
           SqlDatabase db = new SqlDatabase(connString);          
           DbCommand dbCommand = db.GetSqlStringCommand(strSql);
           DataSetObject = db.ExecuteDataSet(dbCommand);
             
           return DataSetObject;
       }

       #endregion
       * */
        /// <summary>
        /// 流执行
        /// </summary>
        /// <param name="_strConn"></param>
        /// <param name="strSql"></param>
        /// <param name="strfid"></param>
        /// <param name="strfvalue"></param>
        /// <returns></returns>
        public int DbExecuteNonQueryByte(string _strConn, string strSql, string fid, string fname, string fdir_id, Byte[] fvalue, string fuse)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbCommand = db.GetSqlStringCommand(strSql);
            //db.AddInParameter(dbCommand, "@fid", DbType.String, fid);
            //db.AddInParameter(dbCommand, "@fname", DbType.String, fname);
            //db.AddInParameter(dbCommand, "@fdir_id", DbType.String, fdir_id);
            //db.AddInParameter(dbCommand, "@fvalue", DbType.Binary, fvalue);
            //db.AddInParameter(dbCommand, "@fuse", DbType.String, fuse);
            //return db.ExecuteNonQuery(dbCommand);
            return 0;
        }

        /// <summary>
        /// 增加 检验申请单条码号
        /// </summary>
        /// <param name="_strConn"></param>
        /// <param name="strStoredProcImg"></param>
        /// <param name="strfimg_type"></param>
        /// <param name="imgfbcode"></param>
        /// <param name="strfapply_id"></param>
        /// <returns></returns>
        public int DbApplyBcodeAdd(string _strConn, string strStoredProcImg, String strfapply_id, Byte[] imgfbcode)
        {
            //strStoredProcImg = "SAM_INSTR_IO_IMG_ADD";
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbCommand = db.GetStoredProcCommand(strStoredProcImg);
            //db.AddInParameter(dbCommand, "@fapply_id", DbType.String, strfapply_id);
            //db.AddInParameter(dbCommand, "@fbcode", DbType.Binary, imgfbcode);
            //return db.ExecuteNonQuery(dbCommand);
            return 0;
        }


        /// <summary>
        /// 增加检验报告图
        /// </summary>
        /// <param name="_strConn"></param>
        /// <param name="strStoredProcImg"></param>
        /// <param name="strfimg_type"></param>
        /// <param name="fimg"></param>
        /// <param name="strforder_by"></param>
        /// <param name="fremark"></param>
        /// <param name="strfio_id"></param>
        /// <returns></returns>
        public int DbInstrImgAdd(string _strConn, string strStoredProcImg, String strfimg_type, Byte[] fimg, String strforder_by, String fremark, String strfio_id)
        {
            //strStoredProcImg = "SAM_INSTR_IO_IMG_ADD";
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbCommand = db.GetStoredProcCommand(strStoredProcImg);
            //db.AddInParameter(dbCommand, "@fimg_id", DbType.String, WEISHENG.COMM.Helper.GuidHelper.DbGuid());
            //db.AddInParameter(dbCommand, "@fimg_type", DbType.String, strfimg_type);
            //db.AddInParameter(dbCommand, "@fimg", DbType.Binary, fimg);
            //db.AddInParameter(dbCommand, "@forder_by", DbType.String, strforder_by);
            //db.AddInParameter(dbCommand, "@fremark", DbType.String, fremark);
            //db.AddInParameter(dbCommand, "@fio_id", DbType.String, strfio_id);
            //return db.ExecuteNonQuery(dbCommand);
            return 0;
        }

        /// <summary>
        /// 增加检验报告图
        /// </summary>
        /// <param name="_strConn"></param>
        /// <param name="strStoredProcImg"></param>
        /// <param name="strfimg_type"></param>
        /// <param name="fimg"></param>
        /// <param name="strforder_by"></param>
        /// <param name="fremark"></param>
        /// <param name="strfsample_id"></param>
        /// <returns></returns>
        public int DbReportImgAdd(string _strConn, string strStoredProcImg, String strfimg_type, Byte[] fimg, String strforder_by, String fremark, String strfsample_id)
        {
            //strStoredProcImg = "SAM_JY_IMG_ADD";
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbCommand = db.GetStoredProcCommand(strStoredProcImg);
            //db.AddInParameter(dbCommand, "@fimg_id", DbType.String, WEISHENG.COMM.Helper.GuidHelper.DbGuid());
            //db.AddInParameter(dbCommand, "@fimg_type", DbType.String, strfimg_type);
            //db.AddInParameter(dbCommand, "@fimg", DbType.Binary, fimg);
            //db.AddInParameter(dbCommand, "@forder_by", DbType.String, strforder_by);
            //db.AddInParameter(dbCommand, "@fremark", DbType.String, fremark);
            //db.AddInParameter(dbCommand, "@fsample_id", DbType.String, strfsample_id);
            //return db.ExecuteNonQuery(dbCommand);
            return 0;
        }

        /// <summary>
        /// 事务处理 SQL列表 如果返回值为"true"，表示提交事务成功，否则返回异常。  
        /// IList sqlList = new ArrayList();
        /// sqlList.Add("delete sam_item where fitem_id='" + fitem_id + "'");
        /// return this.DbTransaction(sqlList);
        /// </summary>
        /// <param name="strConn">连接字串</param>
        /// <param name="sqlList">SQL列表</param>
        /// <returns></returns>
        public string DbTransaction(string _strConn, IList sqlList)
        {
            string result = "";
            int sqlCount = sqlList.Count;
            if (sqlCount > 0)
            {
                //Database db = DatabaseFactory.CreateDatabase(SqlHelper.connectionString);
                string sql = "";
                //DbCommand dbcomm = null;

                using (SqlConnection conn = new SqlConnection(SqlHelper.connectionString))
                {
                    //打开连接
                    conn.Open();
                    //创建事务
                    SqlTransaction trans = conn.BeginTransaction();
                    try
                    {
                        for (int i = 0; i < sqlCount; i++)
                        {
                            sql = sqlList[i].ToString();
                         
                            SqlHelper.ExecuteNonQuery(trans, CommandType.Text, sql);
                        }
                        //都执行成功则提交事务
                        trans.Commit();
                        result = "true";
                    }
                    catch (Exception ex)
                    {
                        //发生异常，事务回滚
                        trans.Rollback();
                        result = "发生异常，事务回滚。\n" + ex.ToString();
                    }
                    //关闭连接
                    conn.Close();
                }
            }
            else
            {
                result = "未执行事务，因为sql数为0！";
            }
            return result;
        }

        /// <summary>
        /// 装载 数据集 可读写
        /// </summary>
        /// <param name="strSql">SQL</param>
        /// <param name="strTableName">装载表名</param>
        /// <returns></returns>        
        public DataSet DbLoadDataSetBySqlString(string _strConn, string strSql, string strTableName)
        {
            //DataSet DataSetObject = new DataSet();
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbCommand = db.GetSqlStringCommand(strSql);
            //db.LoadDataSet(dbCommand, DataSetObject, strTableName);
            //return DataSetObject;
            return null;
        }

        /// <summary>
        /// 装载数据集 根据存过
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strStoredProcName"></param>
        /// <param name="strTableName"></param>
        /// <param name="strInParameterName"></param>
        /// <param name="strInParameterValue"></param>
        /// <returns></returns> 
        public DataSet DbLoadDataSetByStoredProc(string _strConn, string strStoredProcName, string strTableName,
            string strInParameterName, string strInParameterValue)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DataSet DataSetObject = new DataSet();
            //DbCommand dbCommand = db.GetStoredProcCommand(strStoredProcName);
            //db.AddInParameter(dbCommand, strInParameterName, DbType.String, strInParameterValue);
            //db.LoadDataSet(dbCommand, DataSetObject, strTableName);
            //return DataSetObject;
            return null;
        }

        /// <summary>
        /// 执行 数据集 只读
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql"></param>
        /// <returns></returns>      
        public DataSet DbExecuteDataSetBySqlString(string _strConn, string strSql)
        {
            return SqlHelper.ExecuteDataset( strSql);
        }

        public DataSet DbExecuteDataSetBySqlString(string _strConn, string strSql, SqlParameter[] paramsList)
        {
            DataSet DataSetObject = null;
            DataSetObject = SqlHelper.ExecuteDataset(strSql, paramsList);// db.ExecuteDataSet(dbCommand);
            return DataSetObject;
        }

        public DataSet GetPatientInfoBy住院号(string _strConn, string str住院号)
        {
            string strSql = $"SELECT * FROM 病人信息View where [fhz_zyh]={str住院号} ";
            return SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, strSql);
        }

        public DataSet GetPatientInfoBy档案号(string _strConn, string str档案号)
        {
            string strSql = "SELECT * FROM YSDB.YSDB.dbo.vw_lis_PATIENT where [fhz_id]=@dnh ";
            SqlParameter sqlparam = new SqlParameter("@dnh", str档案号);
            return SqlHelper.ExecuteDataset(strSql, sqlparam);
        }

        //add by wjz 20160226 公共卫生系统数据库变更，调整获取信息的数据源 ▽
        public DataSet GetPatientInfoBy档案号New(string _strConn, string str档案号)
        {
            string strSql = "SELECT * FROM YSDB.[AtomEHR.YSDB].dbo.vw_lis_PATIENT where [fhz_id]=@dnh ";
            //DataSet DataSetObject = null;
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbCommand = db.GetSqlStringCommand(strSql);
            SqlParameter sqlparam = new SqlParameter("@dnh", str档案号);
            return SqlHelper.ExecuteDataset(strSql, sqlparam);
            //    dbCommand.Parameters.Add(sqlparam);
            //    DataSetObject = db.ExecuteDataSet(dbCommand);
            //    return DataSetObject;
        }
        //add by wjz 20160226 公共卫生系统数据库变更，调整获取信息的数据源 △

        public DataTable DbExecuteDataTableBySqlString(string _strConn, string strSql)
        {
            DataTable dt = new DataTable();
            DataSet ds = DbExecuteDataSetBySqlString(_strConn, strSql);
            if (ds != null)
            {
                dt = ds.Tables[0];
            }
            return dt;
        }
        /// <summary>
        /// 执行数据集 根据存过
        /// </summary>
        /// <param name="strStoredProcName">存过名</param>
        /// <returns></returns>        
        public DataSet DbExecuteDataSetByStoredProc(string _strConn, string strStoredProcName)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DataSet DataSetObject = null;
            //string sqlCommand = strStoredProcName;
            //DbCommand dbCommand = db.GetStoredProcCommand(strStoredProcName);
            //DataSetObject = db.ExecuteDataSet(dbCommand);
            //return DataSetObject;
            return null;
        }

        public DataSet DbExecuteDataSetByStoredProc(string _strConn, string strStoredProcName, string[] sqlParams)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DataSet DataSetObject = null;
            //string sqlCommand = strStoredProcName;

            //DbCommand dbCommand = db.GetStoredProcCommand(strStoredProcName, sqlParams);
            //DataSetObject = db.ExecuteDataSet(dbCommand);
            //return DataSetObject;
            return null;
        }

        /// <summary>
        /// 装载数据集(分页、可读、写 LoadDataSet)
        /// </summary>
        /// <param name="tblName">物理表(视图)名</param>
        /// <param name="fldName">主键名</param>
        /// <param name="PageSize"> 页尺寸</param>
        /// <param name="PageIndex">页码</param>
        /// <param name="IsReCount">返回记录总数, 非 0 值则返回</param>
        /// <param name="OrderType">设置排序类型, 非 0 值则降序</param>
        /// <param name="strWhere">查询条件 (注意: 不要加 where)</param>
        ///   <param name="strLoadTableName">装载表名</param>
        /// <returns></returns>        
        public DataSet DbLoadDataSetPaging(string _strConn, string tblName, string fldName, Int32 PageSize, Int32 PageIndex, Int32 IsReCount, Int32 OrderType, string strWhere, string strLoadTableName)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DataSet DataSetObject = new DataSet();
            //string sqlCommand = "Sys_GetRecordByPage";
            //DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
            //db.AddInParameter(dbCommand, "tblName", DbType.String, tblName);
            //db.AddInParameter(dbCommand, "fldName", DbType.String, fldName);
            //db.AddInParameter(dbCommand, "PageSize", DbType.Int32, PageSize);
            //db.AddInParameter(dbCommand, "PageIndex", DbType.Int32, PageIndex);
            //db.AddInParameter(dbCommand, "IsReCount", DbType.Int32, IsReCount);
            //db.AddInParameter(dbCommand, "OrderType", DbType.Int32, OrderType);
            //db.AddInParameter(dbCommand, "strWhere", DbType.AnsiString, strWhere);
            //db.LoadDataSet(dbCommand, DataSetObject, strLoadTableName);
            ////记录总数为：
            ////ZAGetPageTableRowsCount = this.ExecuteScalar("SELECT COUNT(*) AS Total FROM COMMON_CODE");
            //return DataSetObject;
            return null;
        }

        /// <summary>
        /// 装载数据集(分页、只读ExecuteDataSet)
        /// </summary>
        /// <param name="tblName">物理表(视图)名</param>
        /// <param name="fldName">主键名</param>
        /// <param name="PageSize"> 页尺寸</param>
        /// <param name="PageIndex">页码</param>
        /// <param name="IsReCount">返回记录总数, 非 0 值则返回</param>
        /// <param name="OrderType">设置排序类型, 非 0 值则降序</param>
        /// <param name="strWhere">查询条件 (注意: 不要加 where)</param>       
        /// <returns></returns>        
        public DataSet DbExecuteDataSetPaging(string _strConn, string tblName, string fldName, Int32 PageSize, Int32 PageIndex, Int32 IsReCount, Int32 OrderType, string strWhere)
        {
            //SqlHelper.ExecuteDataset()
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DataSet DataSetObject = null;
            //string sqlCommand = "Sys_GetRecordByPage";
            //DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
            //db.AddInParameter(dbCommand, "tblName", DbType.String, tblName);
            //db.AddInParameter(dbCommand, "fldName", DbType.String, fldName);
            //db.AddInParameter(dbCommand, "PageSize", DbType.Int32, PageSize);
            //db.AddInParameter(dbCommand, "PageIndex", DbType.Int32, PageIndex);
            //db.AddInParameter(dbCommand, "IsReCount", DbType.Int32, IsReCount);
            //db.AddInParameter(dbCommand, "OrderType", DbType.Int32, OrderType);
            //db.AddInParameter(dbCommand, "strWhere", DbType.AnsiString, strWhere);

            //DataSetObject = db.ExecuteDataSet(dbCommand);
            //return DataSetObject;
            return null;
        }





        /// <summary>
        /// 将System.Type转换成成System.Data.DbType 
        /// </summary>
        /// <param name="type">System.Type</param>
        /// <returns></returns>
        protected static DbType DbTypeSet(Type type)
        {
            /*
              cmd.Parameters.Add("@UpdateImage", SqlDbType.Image);
                    cmd.Parameters["@UpdateImage"].Value = picbyte;
             */
            //SqlDbType.Image
            DbType result = DbType.String;
            if (type.Equals(typeof(int)) || type.IsEnum)
                result = DbType.Int32;
            else if (type.Equals(typeof(Int16)))
                result = DbType.Int16;
            else if (type.Equals(typeof(Int32)))
                result = DbType.Int32;
            else if (type.Equals(typeof(Int64)))
                result = DbType.Int64;
            else if (type.Equals(typeof(Object)))
                result = DbType.Object;
            else if (type.Equals(typeof(Byte)))
                result = DbType.Byte;
            else if (type.Equals(typeof(Single)))
                result = DbType.Single;
            else if (type.Equals(typeof(long)))
                result = DbType.Int32;
            else if (type.Equals(typeof(double)) || type.Equals(typeof(Double)))
                result = DbType.Decimal;
            else if (type.Equals(typeof(DateTime)))
                result = DbType.DateTime;
            else if (type.Equals(typeof(bool)))
                result = DbType.Boolean;
            else if (type.Equals(typeof(Boolean)))
                result = DbType.Boolean;
            else if (type.Equals(typeof(string)))
                result = DbType.String;
            else if (type.Equals(typeof(decimal)))
                result = DbType.Decimal;
            else if (type.Equals(typeof(byte[])))
                result = DbType.Binary;
            else if (type.Equals(typeof(Guid)))
                result = DbType.Guid;
            else if (type.Equals(typeof(Decimal)))
                result = DbType.Decimal;
            return result;

        }
        /// <summary>
        /// 执行ExecuteNonQuery
        /// 该方法返回的是SQL语句执行影响的行数，我们可以利用该方法来执行一些没有返回值的操作(Insert,Update,Delete)
        /// </summary>
        /// <param name="strSql">一般的SQL语句</param>        
        public int DbExecuteNonQueryBySqlString(string _strConn, string strSql)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbcomm = db.GetSqlStringCommand(strSql);
            return SqlHelper.ExecuteNonQuery(_strConn,CommandType.Text, strSql);
        }

        /// <summary>
        /// 执行ExecuteNonQuery
        /// 该方法返回的是SQL语句执行影响的行数，我们可以利用该方法来执行一些没有返回值的操作(Insert,Update,Delete)
        /// 与上面方法的区别：参数个数
        /// </summary>
        /// <param name="_strConn"></param>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public int DbExecuteNonQueryBySqlString(string _strConn, string strSql, DbParameter[] sqlParams)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbcomm = db.GetSqlStringCommand(strSql);
            //foreach (DbParameter param in sqlParams)
            //{
            //    dbcomm.Parameters.Add(param);
            //}
            return SqlHelper.ExecuteNonQuery(_strConn, strSql, sqlParams);// db.ExecuteNonQuery(dbcomm);
        }


        /// <summary>
        /// 合计列
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="str列名"></param>
        /// <returns></returns>
        public string DbComputeSum(DataTable dt, string str列名)
        {
            string str合计值 = "";
            object sumObject;
            sumObject = dt.Compute("Sum(" + str列名 + ")", "");//table.Compute("Sum(Total)", "EmpID = 5");
            if (sumObject != null)
            {
                str合计值 = sumObject.ToString();
            }
            return str合计值;
        }

        public string DbDate()
        {

            return DateTime.Now.ToString("yyyy-MM-dd");
        }
        public string DbDateTime()
        {

            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }


        public void WWFSaveLog(string strCon)
        {
            StreamWriter sw;
            //sw = File.AppendText(Server.MapPath(null) + "\\ZASuite~Log.log");//Web
            // <!--系统日志文件名--> <add key="SysLogFileName" value="Log.log" />
            //System.Configuration.ConfigurationSettings.AppSettings["SysName"] 
            sw = File.AppendText(@"Log/" + System.DateTime.Now.ToString("yyyy-MM-dd") + ".log");//Winform
            sw.WriteLine(System.DateTime.Now.ToString() + " " + strCon);
            sw.Flush();
            sw.Close();
        }
        /// <summary>
        /// 查询数据表 返回值
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public string DbDTSelect(DataTable dt, string strWhere, string strColumn)
        {
            string strValue = "";
            //DataRow[] drall = dt.Select("fcode='" + strCurrColumn + "'");
            DataRow[] drall = dt.Select(strWhere);
            if (drall != null)
            {
                foreach (DataRow dr in drall)
                {
                    strValue = dr[strColumn].ToString();
                }
            }
            return strValue;
        }

   

        /// <summary>
        /// 获取网卡MAC地址
        /// </summary>
        /// <returns></returns>
        public string DbGetNetCardMAC()
        {
            try
            {
                string stringMAC = "";
                ManagementClass MC = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection MOC = MC.GetInstances();

                foreach (ManagementObject MO in MOC)
                {
                    if ((bool)MO["IPEnabled"] == true)
                    {
                        stringMAC += MO["MACAddress"].ToString();

                    }
                }
                return stringMAC;
            }
            catch
            {
                return "";
            }
        }

        public bool DbRecordExistsParam(string _strConn, string uspName, params object[] values)
        {

            //object obj = db.ExecuteScalar(CommandType.Text, strSql.ToString());
            //object obj = db.ExecuteScalar(uspName, values);
            //int cmdresult = 0;
            //try
            //{
            //    cmdresult = int.Parse(obj.ToString());
            //}
            //catch
            //{
            //    return false;
            //}
            //if (cmdresult == 0)
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
            return false;
        }


        /// <summary>
        ///  是否存在该记录
        /// </summary>       
        /// <param name="strConn"></param>
        /// <param name="sql"> </param>
        /// <remarks>SELECT count(1) FROM v_sys_org_func where fparent_id='dc791e654f6e415da953281ea8358dff'</remarks>
        /// <returns></returns>
        public bool DbRecordExists(string _strConn, string sql)
        {          
            object obj = SqlHelper.ExecuteScalar(sql);
            int cmdresult = 0;
            try
            {
                cmdresult = int.Parse(obj.ToString());
            }
            catch
            {
                return false;
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 执行ExecuteScalar 返回对象
        /// (DateTime)obj
        /// Convert.ToInt32(obj)
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql"></param>
        /// <returns></returns>     
        public object DbExecuteScalarBySqlString(string _strConn, string strSql)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbcomm = db.GetSqlStringCommand(strSql);
            return SqlHelper.ExecuteScalar(strSql);
            //object obj = db.ExecuteScalar(dbcomm);
            //return obj;
        }

        public string DbExecuteScalarBySql(string _strConn, string strSql)
        {
            string strObj = "";
            object obj = SqlHelper.ExecuteScalar(strSql);
            if (obj != null)
            {
                strObj = obj.ToString();
            }
            return strObj;
        }


        /// <summary>
        /// 执行ExecuteScalar 返回对象
        /// (DateTime)obj
        /// Convert.ToInt32(obj)
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strStoredProc"></param>
        /// <returns></returns>     
        public object DbExecuteScalarByStoredProc(string _strConn, string strStoredProc)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbcomm = db.GetStoredProcCommand(strStoredProc);
            //object obj = db.ExecuteScalar(dbcomm);
            //return obj;
            return null;
        }

        public object DbExecuteScalarByStoredProc(string _strConn, string strStoredProc, DbParameter[] procParams)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //DbCommand dbcomm = db.GetStoredProcCommand(strStoredProc);
            //foreach (var param in procParams)
            //{
            //    dbcomm.Parameters.Add(param);
            //}
            //object obj = db.ExecuteScalar(dbcomm);
            //return obj;
            return null;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="strKey">主键字段名</param>
        /// <param name="strStoredProcInsertCommand">新增存储过程名</param>
        /// <param name="strStoredProcDeleteCommand">删除存储过程名</param>
        /// <param name="strStoredProcupdateCommand">修改存储过程名</param>
        /// <returns></returns>        
        /// <summary>
        ///  通过存储过程批量增、删、改数据集 一张表
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject"></param>
        /// <param name="strTableName"></param>
        /// <param name="strKey"></param>
        /// <param name="strStoredProcInsertCommand">新增存储过程名</param>
        /// <param name="strStoredProcDeleteCommand">删除存储过程名</param>
        /// <param name="strStoredProcupdateCommand">修改存储过程名</param>
        /// <param name="intUpdateBehavior">修改特性(Standard = 0,Continue = 1,Transactional = 2,)</param>
        /// <returns></returns>
        public int DbUpdateDataSet
            (string _strConn, DataSet dsObject, string strTableName, string strKey, string strStoredProcInsertCommand, string strStoredProcDeleteCommand, string strStoredProcupdateCommand, int intUpdateBehavior)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //String columnName = "";
            //System.Type columnType;
            //int columnsCount = 0;
            //DataSet ds = new DataSet();
            //ds = dsObject;
            //DataTable table = new DataTable();
            //table = ds.Tables[strTableName];
            //columnsCount = table.Columns.Count;

            //DbCommand insertCommand = db.GetStoredProcCommand(strStoredProcInsertCommand);
            //for (int i = 0; i < columnsCount; i++)
            //{
            //    columnType = table.Columns[i].DataType;
            //    columnName = table.Columns[i].ColumnName;
            //    db.AddInParameter(insertCommand, columnName, DbTypeSet(columnType), columnName, DataRowVersion.Current);
            //}

            //DbCommand deleteCommand = db.GetStoredProcCommand(strStoredProcDeleteCommand);
            //db.AddInParameter(deleteCommand, strKey, DbType.String, strKey, DataRowVersion.Current);

            //DbCommand updateCommand = db.GetStoredProcCommand(strStoredProcupdateCommand);
            //for (int ii = 0; ii < columnsCount; ii++)
            //{
            //    columnType = table.Columns[ii].DataType;
            //    columnName = table.Columns[ii].ColumnName;
            //    db.AddInParameter(updateCommand, columnName, DbTypeSet(columnType), columnName, DataRowVersion.Current);
            //}

            ///*
            //  Standard = 0,Continue = 1,Transactional = 2,
            // */
            //int rowsAffected = 0;
            //switch (intUpdateBehavior)
            //{
            //    case 0:
            //        rowsAffected =
            //            db.UpdateDataSet(ds, strTableName, insertCommand, updateCommand, deleteCommand, UpdateBehavior.Standard);

            //        break;
            //    case 1:
            //        rowsAffected =
            //            db.UpdateDataSet(ds, strTableName, insertCommand, updateCommand,
            //                                     deleteCommand, UpdateBehavior.Continue);
            //        break;
            //    case 2:
            //        rowsAffected =
            //            db.UpdateDataSet(ds, strTableName, insertCommand, updateCommand,
            //                                     deleteCommand, UpdateBehavior.Transactional);
            //        break;
            //    default:
            //        rowsAffected =
            //            db.UpdateDataSet(ds, strTableName, insertCommand, updateCommand,
            //                                     deleteCommand, UpdateBehavior.Standard);
            //        break;
            //}
            //return rowsAffected;
            return 0;

        }
        /// <summary>
        /// 数据更新 事务处理 (存过、两张表、插入新记录)
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject_1"></param>
        /// <param name="strTableName_1"></param>
        /// <param name="strStoredProcInsertCommand_1"></param>
        /// <param name="dsObject_2"></param>
        /// <param name="strTableName_2"></param>
        /// <param name="strStoredProcInsertCommand_2"></param>
        /// <returns></returns>
        public int DbUpdateDataSetStoredProcInsert2(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2)
        {
            //int result = 0;
            //Database db = DatabaseFactory.CreateDatabase(_strConn);

            ////表_1
            //DataSet ds_1 = new DataSet();
            //ds_1 = dsObject_1;
            //String columnName_1;
            //System.Type columnType_1;
            //int columnsCount_1 = 0;
            //DataTable table_1 = new DataTable();
            //table_1 = ds_1.Tables[strTableName_1];
            //columnsCount_1 = table_1.Columns.Count;
            //DbCommand insertCommand_1 = db.GetStoredProcCommand(strStoredProcInsertCommand_1);
            //for (int i = 0; i < columnsCount_1; i++)
            //{
            //    columnType_1 = table_1.Columns[i].DataType;
            //    columnName_1 = table_1.Columns[i].ColumnName;
            //    db.AddInParameter(insertCommand_1, columnName_1, DbTypeSet(columnType_1), columnName_1, DataRowVersion.Current);
            //}

            ////表_2
            //DataSet ds_2 = new DataSet();
            //ds_2 = dsObject_2;
            //String columnName_2;
            //System.Type columnType_2;
            //int columnsCount_2 = 0;
            //DataTable table_2 = new DataTable();
            //table_2 = ds_2.Tables[strTableName_2];
            //columnsCount_2 = table_2.Columns.Count;
            //DbCommand insertCommand_2 = db.GetStoredProcCommand(strStoredProcInsertCommand_2);
            //for (int i = 0; i < columnsCount_2; i++)
            //{
            //    columnType_2 = table_2.Columns[i].DataType;
            //    columnName_2 = table_2.Columns[i].ColumnName;
            //    db.AddInParameter(insertCommand_2, columnName_2, DbTypeSet(columnType_2), columnName_2, DataRowVersion.Current);
            //}

            //DbCommand deleteCommand = null;
            //DbCommand updateCommand = null;
            //using (DbConnection conn = db.CreateConnection())
            //{
            //    //打开连接
            //    conn.Open();
            //    //创建事务
            //    DbTransaction trans = conn.BeginTransaction();
            //    try
            //    {
            //        int i1, i2;
            //        i1 = db.UpdateDataSet(ds_1, strTableName_1, insertCommand_1, updateCommand,
            //                                      deleteCommand, trans);
            //        i2 = db.UpdateDataSet(ds_2, strTableName_2, insertCommand_2, updateCommand,
            //           deleteCommand, trans);
            //        //都执行成功则提交事务
            //        trans.Commit();
            //        result = i1 + i2;
            //    }
            //    catch (Exception)
            //    {
            //        //发生异常，事务回滚
            //        trans.Rollback();
            //    }
            //    //关闭连接
            //    conn.Close();
            //    return result;
            //}
            return 0;
        }

        /// <summary>
        /// 数据更新 事务处理 (存过、三张表、插入新记录)
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject_1"></param>
        /// <param name="strTableName_1"></param>
        /// <param name="strStoredProcInsertCommand_1"></param>
        /// <param name="dsObject_2"></param>
        /// <param name="strTableName_2"></param>
        /// <param name="strStoredProcInsertCommand_2"></param>
        /// <param name="dsObject_3"></param>
        /// <param name="strTableName_3"></param>
        /// <param name="strStoredProcInsertCommand_3"></param>
        /// <returns></returns>
        public int DbUpdateDataSetStoredProcInsert3(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcInsertCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcInsertCommand_2, DataSet dsObject_3, string strTableName_3, string strStoredProcInsertCommand_3)
        {
            //int result = 0;
            //Database db = DatabaseFactory.CreateDatabase(_strConn);

            ////表_1
            //DataSet ds_1 = new DataSet();
            //ds_1 = dsObject_1;
            //String columnName_1;
            //System.Type columnType_1;
            //int columnsCount_1 = 0;
            //DataTable table_1 = new DataTable();
            //table_1 = ds_1.Tables[strTableName_1];
            //columnsCount_1 = table_1.Columns.Count;
            //DbCommand insertCommand_1 = db.GetStoredProcCommand(strStoredProcInsertCommand_1);
            //for (int i = 0; i < columnsCount_1; i++)
            //{
            //    columnType_1 = table_1.Columns[i].DataType;
            //    columnName_1 = table_1.Columns[i].ColumnName;
            //    db.AddInParameter(insertCommand_1, columnName_1, DbTypeSet(columnType_1), columnName_1, DataRowVersion.Current);
            //}

            ////表_2
            //DataSet ds_2 = new DataSet();
            //ds_2 = dsObject_2;
            //String columnName_2;
            //System.Type columnType_2;
            //int columnsCount_2 = 0;
            //DataTable table_2 = new DataTable();
            //table_2 = ds_2.Tables[strTableName_2];
            //columnsCount_2 = table_2.Columns.Count;
            //DbCommand insertCommand_2 = db.GetStoredProcCommand(strStoredProcInsertCommand_2);
            //for (int i = 0; i < columnsCount_2; i++)
            //{
            //    columnType_2 = table_2.Columns[i].DataType;
            //    columnName_2 = table_2.Columns[i].ColumnName;
            //    db.AddInParameter(insertCommand_2, columnName_2, DbTypeSet(columnType_2), columnName_2, DataRowVersion.Current);
            //}


            ////表_3
            //DataSet ds_3 = new DataSet();
            //ds_3 = dsObject_3;
            //String columnName_3;
            //System.Type columnType_3;
            //int columnsCount_3 = 0;
            //DataTable table_3 = new DataTable();
            //table_3 = ds_3.Tables[strTableName_3];
            //columnsCount_3 = table_3.Columns.Count;
            //DbCommand insertCommand_3 = db.GetStoredProcCommand(strStoredProcInsertCommand_3);
            //for (int i = 0; i < columnsCount_3; i++)
            //{
            //    columnType_3 = table_3.Columns[i].DataType;
            //    columnName_3 = table_3.Columns[i].ColumnName;
            //    db.AddInParameter(insertCommand_3, columnName_3, DbTypeSet(columnType_3), columnName_3, DataRowVersion.Current);
            //}

            //DbCommand deleteCommand = null;
            //DbCommand updateCommand = null;
            //using (DbConnection conn = db.CreateConnection())
            //{
            //    //打开连接
            //    conn.Open();
            //    //创建事务
            //    DbTransaction trans = conn.BeginTransaction();
            //    try
            //    {
            //        int i1, i2, i3;
            //        i1 = db.UpdateDataSet(ds_1, strTableName_1, insertCommand_1, updateCommand,
            //                                      deleteCommand, trans);
            //        i2 = db.UpdateDataSet(ds_2, strTableName_2, insertCommand_2, updateCommand,
            //           deleteCommand, trans);
            //        i3 = db.UpdateDataSet(ds_3, strTableName_3, insertCommand_3, updateCommand,
            //           deleteCommand, trans);
            //        //都执行成功则提交事务
            //        trans.Commit();
            //        result = i1 + i2 + i3;
            //    }
            //    catch (Exception)
            //    {
            //        //发生异常，事务回滚
            //        trans.Rollback();
            //    }
            //    //关闭连接
            //    conn.Close();
            //    return result;
            //}
            return 0;
        }
        /// <summary>
        /// 数据更新 事务处理 (存过、二张表、修改记录)
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject_1"></param>
        /// <param name="strTableName_1"></param>
        /// <param name="strStoredProcUpdateCommand_1"></param>
        /// <param name="dsObject_2"></param>
        /// <param name="strTableName_2"></param>
        /// <param name="strStoredProcUpdateCommand_2"></param>
        /// <returns></returns>
        public int DbUpdateDataSetStoredProcUpdate2(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2)
        {
            //int result = 0;
            //Database db = DatabaseFactory.CreateDatabase(_strConn);

            ////表_1
            //DataSet ds_1 = new DataSet();
            //ds_1 = dsObject_1;
            //String columnName_1;
            //System.Type columnType_1;
            //int columnsCount_1 = 0;
            //DataTable table_1 = new DataTable();
            //table_1 = ds_1.Tables[strTableName_1];
            //columnsCount_1 = table_1.Columns.Count;
            //DbCommand updateCommand_1 = db.GetStoredProcCommand(strStoredProcUpdateCommand_1);
            //for (int i = 0; i < columnsCount_1; i++)
            //{
            //    columnType_1 = table_1.Columns[i].DataType;
            //    columnName_1 = table_1.Columns[i].ColumnName;
            //    db.AddInParameter(updateCommand_1, columnName_1, DbTypeSet(columnType_1), columnName_1, DataRowVersion.Current);
            //}


            ////表_2
            //DataSet ds_2 = new DataSet();
            //ds_2 = dsObject_2;
            //String columnName_2;
            //System.Type columnType_2;
            //int columnsCount_2 = 0;
            //DataTable table_2 = new DataTable();
            //table_2 = ds_2.Tables[strTableName_2];
            //columnsCount_2 = table_2.Columns.Count;
            //DbCommand updateCommand_2 = db.GetStoredProcCommand(strStoredProcUpdateCommand_2);
            //for (int i = 0; i < columnsCount_2; i++)
            //{
            //    columnType_2 = table_2.Columns[i].DataType;
            //    columnName_2 = table_2.Columns[i].ColumnName;
            //    db.AddInParameter(updateCommand_2, columnName_2, DbTypeSet(columnType_2), columnName_2, DataRowVersion.Current);
            //}


            //DbCommand deleteCommand = null;
            //DbCommand insertCommand = null;
            //using (DbConnection conn = db.CreateConnection())
            //{
            //    //打开连接
            //    conn.Open();
            //    //创建事务
            //    DbTransaction trans = conn.BeginTransaction();
            //    try
            //    {
            //        int i1, i2;
            //        i1 = db.UpdateDataSet(ds_1, strTableName_1, insertCommand, updateCommand_1,
            //                                      deleteCommand, trans);
            //        i2 = db.UpdateDataSet(ds_2, strTableName_2, insertCommand, updateCommand_2,
            //           deleteCommand, trans);

            //        //都执行成功则提交事务
            //        trans.Commit();
            //        result = i1 + i2;
            //    }
            //    catch (Exception)
            //    {
            //        //发生异常，事务回滚
            //        trans.Rollback();
            //    }
            //    //关闭连接
            //    conn.Close();
            //    return result;
            //}
            return 0;
        }
        /// <summary>
        /// 数据更新 事务处理 (存过、三张表、修改记录)
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="dsObject_1"></param>
        /// <param name="strTableName_1"></param>
        /// <param name="strStoredProcUpdateCommand_1"></param>
        /// <param name="dsObject_2"></param>
        /// <param name="strTableName_2"></param>
        /// <param name="strStoredProcUpdateCommand_2"></param>
        /// <param name="dsObject_3"></param>
        /// <param name="strTableName_3"></param>
        /// <param name="strStoredProcUpdateCommand_3"></param>
        /// <returns></returns>
        public int DbUpdateDataSetStoredProcUpdate3(string _strConn, DataSet dsObject_1, string strTableName_1, string strStoredProcUpdateCommand_1, DataSet dsObject_2, string strTableName_2, string strStoredProcUpdateCommand_2, DataSet dsObject_3, string strTableName_3, string strStoredProcUpdateCommand_3)
        {
            //int result = 0;
            //Database db = DatabaseFactory.CreateDatabase(_strConn);

            ////表_1
            //DataSet ds_1 = new DataSet();
            //ds_1 = dsObject_1;
            //String columnName_1;
            //System.Type columnType_1;
            //int columnsCount_1 = 0;
            //DataTable table_1 = new DataTable();
            //table_1 = ds_1.Tables[strTableName_1];
            //columnsCount_1 = table_1.Columns.Count;
            //DbCommand updateCommand_1 = db.GetStoredProcCommand(strStoredProcUpdateCommand_1);
            //for (int i = 0; i < columnsCount_1; i++)
            //{
            //    columnType_1 = table_1.Columns[i].DataType;
            //    columnName_1 = table_1.Columns[i].ColumnName;
            //    db.AddInParameter(updateCommand_1, columnName_1, DbTypeSet(columnType_1), columnName_1, DataRowVersion.Current);
            //}


            ////表_2
            //DataSet ds_2 = new DataSet();
            //ds_2 = dsObject_2;
            //String columnName_2;
            //System.Type columnType_2;
            //int columnsCount_2 = 0;
            //DataTable table_2 = new DataTable();
            //table_2 = ds_2.Tables[strTableName_2];
            //columnsCount_2 = table_2.Columns.Count;
            //DbCommand updateCommand_2 = db.GetStoredProcCommand(strStoredProcUpdateCommand_2);
            //for (int i = 0; i < columnsCount_2; i++)
            //{
            //    columnType_2 = table_2.Columns[i].DataType;
            //    columnName_2 = table_2.Columns[i].ColumnName;
            //    db.AddInParameter(updateCommand_2, columnName_2, DbTypeSet(columnType_2), columnName_2, DataRowVersion.Current);
            //}


            ////表_3
            //DataSet ds_3 = new DataSet();
            //ds_3 = dsObject_3;
            //String columnName_3;
            //System.Type columnType_3;
            //int columnsCount_3 = 0;
            //DataTable table_3 = new DataTable();
            //table_3 = ds_3.Tables[strTableName_3];
            //columnsCount_3 = table_3.Columns.Count;
            //DbCommand updateCommand_3 = db.GetStoredProcCommand(strStoredProcUpdateCommand_3);
            //for (int i = 0; i < columnsCount_3; i++)
            //{
            //    columnType_3 = table_3.Columns[i].DataType;
            //    columnName_3 = table_3.Columns[i].ColumnName;
            //    db.AddInParameter(updateCommand_3, columnName_3, DbTypeSet(columnType_3), columnName_3, DataRowVersion.Current);
            //}

            //DbCommand deleteCommand = null;
            //DbCommand insertCommand = null;
            //using (DbConnection conn = db.CreateConnection())
            //{
            //    //打开连接
            //    conn.Open();
            //    //创建事务
            //    DbTransaction trans = conn.BeginTransaction();
            //    try
            //    {
            //        int i1, i2, i3;
            //        i1 = db.UpdateDataSet(ds_1, strTableName_1, insertCommand, updateCommand_1,
            //                                      deleteCommand, trans);
            //        i2 = db.UpdateDataSet(ds_2, strTableName_2, insertCommand, updateCommand_2,
            //           deleteCommand, trans);
            //        i3 = db.UpdateDataSet(ds_3, strTableName_3, insertCommand, updateCommand_3,
            //           deleteCommand, trans);
            //        //都执行成功则提交事务
            //        trans.Commit();
            //        result = i1 + i2 + i3;
            //    }
            //    catch (Exception)
            //    {
            //        //发生异常，事务回滚
            //        trans.Rollback();
            //    }
            //    //关闭连接
            //    conn.Close();
            //    return result;
            //}
            return 0;
        }

        /// <summary>
        /// 事务处理 2sql
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql1"></param>
        /// <param name="strSql2"></param>
        /// <returns></returns>

        public bool DbExecuteNonQueryTransaction2(string _strConn, string strSql1, string strSql2)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //bool result = false;
            //DbCommand dbcomm1 = db.GetSqlStringCommand(strSql1);
            //DbCommand dbcomm2 = db.GetSqlStringCommand(strSql2);
            ////DbCommand dbcomm1 = db.GetSqlStringCommand("update person set name='pw'");
            ////DbCommand dbcomm2 = db.GetSqlStringCommand("delete from person where id=1");
            //using (DbConnection conn = db.CreateConnection())
            //{
            //    //打开连接
            //    conn.Open();
            //    //创建事务
            //    DbTransaction trans = conn.BeginTransaction();
            //    try
            //    {
            //        db.ExecuteNonQuery(dbcomm1, trans);
            //        db.ExecuteNonQuery(dbcomm2, trans);
            //        //都执行成功则提交事务
            //        trans.Commit();
            //        result = true;
            //    }
            //    catch (Exception)
            //    {
            //        //发生异常，事务回滚
            //        trans.Rollback();
            //    }
            //    //关闭连接
            //    conn.Close();
            //    return result;
            //}
            return false;
        }
        /// <summary>
        /// 事务处理  3sql
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql1"></param>
        /// <param name="strSql2"></param>
        /// <param name="strSql3"></param>
        /// <returns></returns>

        public bool DbExecuteNonQueryTransaction3(string _strConn, string strSql1, string strSql2, string strSql3)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //bool result = false;
            //DbCommand dbcomm1 = db.GetSqlStringCommand(strSql1);
            //DbCommand dbcomm2 = db.GetSqlStringCommand(strSql2);
            //DbCommand dbcomm3 = db.GetSqlStringCommand(strSql3);
            //using (DbConnection conn = db.CreateConnection())
            //{
            //    //打开连接
            //    conn.Open();
            //    //创建事务
            //    DbTransaction trans = conn.BeginTransaction();
            //    try
            //    {
            //        db.ExecuteNonQuery(dbcomm1, trans);
            //        db.ExecuteNonQuery(dbcomm2, trans);
            //        db.ExecuteNonQuery(dbcomm3, trans);
            //        //都执行成功则提交事务
            //        trans.Commit();
            //        result = true;
            //    }
            //    catch (Exception)
            //    {
            //        //发生异常，事务回滚
            //        trans.Rollback();
            //    }
            //    //关闭连接
            //    conn.Close();
            //    return result;
            //}
            return false;
        }
        /// <summary>
        /// 事务处理 4sql
        /// </summary>
        /// <param name="strConn"></param>
        /// <param name="strSql1"></param>
        /// <param name="strSql2"></param>
        /// <param name="strSql3"></param>
        /// <param name="strSql4"></param>
        /// <returns></returns>

        public bool DbExecuteNonQueryTransaction4(string _strConn, string strSql1, string strSql2, string strSql3, string strSql4)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            //bool result = false;
            //DbCommand dbcomm1 = db.GetSqlStringCommand(strSql1);
            //DbCommand dbcomm2 = db.GetSqlStringCommand(strSql2);
            //DbCommand dbcomm3 = db.GetSqlStringCommand(strSql3);
            //DbCommand dbcomm4 = db.GetSqlStringCommand(strSql4);
            //using (DbConnection conn = db.CreateConnection())
            //{
            //    //打开连接
            //    conn.Open();
            //    //创建事务
            //    DbTransaction trans = conn.BeginTransaction();
            //    try
            //    {
            //        db.ExecuteNonQuery(dbcomm1, trans);
            //        db.ExecuteNonQuery(dbcomm2, trans);
            //        db.ExecuteNonQuery(dbcomm3, trans);
            //        db.ExecuteNonQuery(dbcomm4, trans);
            //        //都执行成功则提交事务
            //        trans.Commit();
            //        result = true;
            //    }
            //    catch (Exception)
            //    {
            //        //发生异常，事务回滚
            //        trans.Rollback();
            //    }
            //    //关闭连接
            //    conn.Close();
            //    return result;
            //}
            return false;
        }

        /*
         for (int i = 0; i < lstSql.Count; i++)
            {
                sql = lstSql[i].ToString();
                MessageBox.Show(sql);
            }
         */
        /// <summary>
        /// 根据SQL取得 IDataReader 因为ExecuteReader方法在一开始执行时就打开了一个与数据库的连接，所以我们必须注意在使用结束时关闭连接
        /// </summary>
        /// <param name="strSql">SQL语句</param>
        /// <returns></returns>
        public IDataReader DbExecuteReaderBySqlString(string _strConn, string strSql)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            IDataReader dr = null;
            //DbCommand dbCommand = db.GetSqlStringCommand(strSql);
            //dr = db.ExecuteReader(dbCommand);
            //using (IDataReader dataReader = db.ExecuteReader(dbCommand))
            //{
            //    dr = dataReader;
            //}
            return dr;
        }
        /// <summary>
        /// 根据SQL取得 IDataReader 因为ExecuteReader方法在一开始执行时就打开了一个与数据库的连接，所以我们必须注意在使用结束时关闭连接
        /// </summary>
        /// <param name="strStoredProc">存过</param>
        /// <returns></returns>
        public IDataReader DbExecuteReaderByStoredProc(string _strConn, string strStoredProc)
        {
            //Database db = DatabaseFactory.CreateDatabase(_strConn);
            IDataReader dr = null;
            //DbCommand dbCommand = db.GetStoredProcCommand(strStoredProc);
            //dr = db.ExecuteReader(dbCommand);
            //using (IDataReader dataReader = db.ExecuteReader(dbCommand))
            //{
            //    dr = dataReader;
            //}
            return dr;
        }
        /// <summary>
        /// 取得32位GUID
        /// </summary>
        /// <returns></returns>
        public string DbGuid()
        {
            string strGuid = "";
            //string strGuid = System.Guid.NewGuid().ToString().ToUpper();
            strGuid = System.Guid.NewGuid().ToString();
            return strGuid.Replace("-", "");

        }


    }
}
/*
 创建一个默认的数据库实例
            Database defaultdb = DatabaseFactory.CreateDatabase();
            //创建一个名为Connection String的数据库实例
            Database db = DatabaseFactory.CreateDatabase("Connection String");
            //上面两种创建数据库实例的方法的数据库可以是任何类型的数据库，取决于Config文件中的配置信息
            //下面的这种方面展示了创建一个SQL数据库的实例,需引用Microsoft.Practices.EnterpriseLibrary.Data.Sql程序集
            SqlDatabase dbsql = DatabaseFactory.CreateDatabase("Connection String") as SqlDatabase;
            //我们同样也可以不通过配置文件来创建数据库实例，如下
            string connString = "server=.;database=EnterpriseLibrary;uid=sa;pwd=";
            SqlDatabase nocofigdb = new SqlDatabase(connString);
 */

/*
       /// <summary>
       /// 事务处理 SQL列表 如果返回值为"true"，表示提交事务成功，否则返回异常。
       /// </summary>
       /// <param name="strConn">连接字串</param>
       /// <param name="sqlList">SQL列表</param>
       /// <returns></returns>
       public string ExecuteNonQueryTransactionSqlListTest(IList sqlList)
       {
           StringBuilder strSql = new StringBuilder();
           int sqlCount = sqlList.Count;
           for (int i = 0; i < sqlCount; i++)
           {
               strSql.Append(sqlList[i].ToString());
           }
           return strSql.ToString();
       }*/
