﻿using HIS.Model;
using System;

namespace yunLis.Pojo
{

    public class Pojo报告单摘要 : SAM_JY
    {

        public string 姓名 { get; set; }

        public Double 年龄 { get; set; }

        public string 样本号 { get; set; }

        public string 住院号 { get; set; }

        public string 床号 { get; set; }

        public string 条码 { get; set; }

        public string 备注 { get; set; }

        public string 检验日期 { get; set; }

        public string 送检日期 { get; set; }

        public string 报告时间 { get; set; }

        public string 化验设备ID { get; set; }

        public string 报告样式 { get; set; }

        public string 病人类别 { get; set; }

        public string 性别 { get; set; }

        public string 年龄单位 { get; set; }

        public string 科室 { get; set; }

        public string 样本类型 { get; set; }

        public string 检验医师 { get; set; }

        public string 申请医师 { get; set; }

        public string 疾病名称 { get; set; }

        public string 审核者 { get; set; }

        public string 检验设备 { get; set; }

        public string 化验项目 { get; set; }        

        public string 打印状态 { get; set; }

    }

}
