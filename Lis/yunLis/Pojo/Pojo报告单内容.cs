﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yunLis.Pojo
{

    public class Pojo报告单内容
    {

        public string 项目代码 { get; set; }

        public string 项目名称 { get; set; }

        public string 单位 { get; set; }

        public string 参考值 { get; set; }

        public string 标记 { get; set; }

        public string 结果 { get; set; }

        public Int64 序号 { get; set; }

    }

}
