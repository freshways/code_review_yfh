using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using yunLis.dao;
using yunLis.wwfbll;
using ww.wwf.com;
using System.Collections;
namespace yunLis.lisbll.sam
{
    /// <summary>
    /// 检验报告规则
    /// </summary>
    public class JYBLL以前的 : DAOWWF
    {
       
        string table_jy = "";//报告表名
        string table_jy_result = "";//检验 结果表名
        string table_jy_img = "";//检验 图 表名
        // string userdefinedJYid = "2";//报告显示列表自定义ID
        UserdefinedBLL bllField = new UserdefinedBLL();
        public static string strSqlOK = "";
        int sam_jy_fsample_code = 4;//默认样本位数　来自 表wwf_num 行sam_jy_fsample_code
        NumBLL bllNum = new NumBLL();
        public JYBLL以前的()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	            

            table_jy = "sam_jy";
            table_jy_result = "sam_jy_result";
            table_jy_img = "sam_jy_img";
            sam_jy_fsample_code = bllNum.BllGetflengthByid("sam_jy_fsample_code");
        }
       

        #region  报告

        /// <summary>
        /// 取得报告列表
        /// </summary>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public DataTable BllJYdt(string strWhere)
        {
            if (strWhere == "" || strWhere == null)
            {
                return null;
            }
            else
            {
                strWhere = " Where " + strWhere + " order by fsample_code";
                return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, "Select * FROM " + table_jy + " as t " + strWhere).Tables[0];
            }

        }

        /// <summary>
        /// 增加一条数据 报告
        /// </summary>
        /// <param name="reportDow"></param>
        /// <returns></returns>
        public int BllJYAdd(DataRow reportDow)
        {
            if (BllJYExists(reportDow["finstr_id"].ToString(), reportDow["fjy_date"].ToString(), reportDow["fsample_code"].ToString()))
            {
                return 0;
            }
            else
            {
                int intFage = 0;
                if(reportDow["fage"]==null)
                    intFage =0;
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into " + table_jy + "(");
                strSql.Append("fjy_id,finstr_id,fjy_date,fsample_code,fprint_flag,fexamine_flag,fstate,fjygroup_id,fjytype_id,fsample_type_id,fhz_type_id,fhz_id,fsex,fname,fage,fage_unit,fage_year,fage_month,fage_day,fward_num,froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fzyh,fheat,fxyld,fcyy,fxhdb,fapply_code,fapply_user_id,fapply_dept_id,fapply_time,fsampling_user_id,fsampling_time,fget_user_id,fget_user_time,fsend_user_id,fsend_user_time,fcheck_user_id,fcheck_time,fexamine_user_id,fexamine_time,fhandling_user_id,fhandling_user_time,fprint_time,fprint_count,freport_dept_id,forder_by,fcharge_flag,fjyf,fcharge_id,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fremark");
                strSql.Append(")");
                strSql.Append(" values (");
                strSql.Append("'" + reportDow["fjy_id"] + "',");
                strSql.Append("'" + reportDow["finstr_id"] + "',");
                strSql.Append("'" + reportDow["fjy_date"] + "',");
                strSql.Append("'" + reportDow["fsample_code"] + "',");
                strSql.Append("" + reportDow["fprint_flag"] + ",");
                strSql.Append("" + reportDow["fexamine_flag"] + ",");
                strSql.Append("'" + reportDow["fstate"] + "',");
                strSql.Append("'" + reportDow["fjygroup_id"] + "',");
                strSql.Append("'" + reportDow["fjytype_id"] + "',");
                strSql.Append("'" + reportDow["fsample_type_id"] + "',");
                strSql.Append("'" + reportDow["fhz_type_id"] + "',");
                strSql.Append("'" + reportDow["fhz_id"] + "',");
                strSql.Append("'" + reportDow["fsex"] + "',");
                strSql.Append("'" + reportDow["fname"] + "',");

                strSql.Append("" + intFage + ",");
                strSql.Append("'" + reportDow["fage_unit"] + "',");
                strSql.Append("" + reportDow["fage_year"] + ",");
                strSql.Append("" + reportDow["fage_month"] + ",");
                strSql.Append("" + reportDow["fage_day"] + ",");
                strSql.Append("'" + reportDow["fward_num"] + "',");
                strSql.Append("'" + reportDow["froom_num"] + "',");
                strSql.Append("'" + reportDow["fbed_num"] + "',");
                strSql.Append("'" + reportDow["fblood_abo"] + "',");
                strSql.Append("'" + reportDow["fblood_rh"] + "',");
                strSql.Append("'" + reportDow["fjymd"] + "',");
                strSql.Append("'" + reportDow["fdiagnose"] + "',");
                strSql.Append("'" + reportDow["fzyh"] + "',");
                strSql.Append("" + reportDow["fheat"] + ",");
                strSql.Append("" + reportDow["fxyld"] + ",");
                strSql.Append("'" + reportDow["fcyy"] + "',");
                strSql.Append("" + reportDow["fxhdb"] + ",");
                strSql.Append("'" + reportDow["fapply_code"] + "',");
                strSql.Append("'" + reportDow["fapply_user_id"] + "',");
                strSql.Append("'" + reportDow["fapply_dept_id"] + "',");
                strSql.Append("'" + reportDow["fapply_time"] + "',");
                strSql.Append("'" + reportDow["fsampling_user_id"] + "',");
                strSql.Append("'" + reportDow["fsampling_time"] + "',");
                strSql.Append("'" + reportDow["fget_user_id"] + "',");
                strSql.Append("'" + reportDow["fget_user_time"] + "',");
                strSql.Append("'" + reportDow["fsend_user_id"] + "',");
                strSql.Append("'" + reportDow["fsend_user_time"] + "',");
                strSql.Append("'" + reportDow["fcheck_user_id"] + "',");
                strSql.Append("'" + reportDow["fcheck_time"] + "',");
                strSql.Append("'" + reportDow["fexamine_user_id"] + "',");
                strSql.Append("'" + reportDow["fexamine_time"] + "',");
                strSql.Append("'" + reportDow["fhandling_user_id"] + "',");
                strSql.Append("'" + reportDow["fhandling_user_time"] + "',");
                strSql.Append("'" + reportDow["fprint_time"] + "',");
                strSql.Append("" + reportDow["fprint_count"] + ",");
                strSql.Append("'" + reportDow["freport_dept_id"] + "',");
                strSql.Append("'" + reportDow["forder_by"] + "',");
                strSql.Append("" + reportDow["fcharge_flag"] + ",");
                strSql.Append("" + reportDow["fjyf"] + ",");
                strSql.Append("'" + reportDow["fcharge_id"] + "',");
                strSql.Append("'" + reportDow["fcreate_user_id"] + "',");
                strSql.Append("'" + reportDow["fcreate_time"] + "',");
                strSql.Append("'" + reportDow["fupdate_user_id"] + "',");
                strSql.Append("'" + reportDow["fupdate_time"] + "',");
                strSql.Append("'" + reportDow["fremark"] + "'");
                strSql.Append(")");
                return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, strSql.ToString());
            }
        }
        /// <summary>
        /// 更新一条数据 报告
        /// </summary>
        /// <param name="reportDow"></param>
        /// <returns></returns>
        public int BllJYUpdateRow(DataRowView reportDow)
        {
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, BllJYUpdateSql(reportDow));
        }
        /// <summary>
        /// 修改报告Sql
        /// </summary>
        /// <param name="reportDow"></param>
        /// <returns></returns>
        public string BllJYUpdateSql(DataRowView reportDow)
        {
          //  int intFage = 0;
           // if (reportDow["fage"] == null || reportDow["fage"].ToString() == "")
           // {
               // intFage = 0;
           // }

            StringBuilder strSql = new StringBuilder();
            strSql.Append("update " + table_jy + " set ");
            strSql.Append("finstr_id='" + reportDow["finstr_id"] + "',");
            strSql.Append("fjy_date='" + reportDow["fjy_date"] + "',");
            strSql.Append("fsample_code='" + reportDow["fsample_code"] + "',");
            strSql.Append("fprint_flag=" + reportDow["fprint_flag"] + ",");
            strSql.Append("fexamine_flag=" + reportDow["fexamine_flag"] + ",");
            strSql.Append("fstate='" + reportDow["fstate"] + "',");
            strSql.Append("fjygroup_id='" + reportDow["fjygroup_id"] + "',");
            strSql.Append("fjytype_id='" + reportDow["fjytype_id"] + "',");
            strSql.Append("fsample_type_id='" + reportDow["fsample_type_id"] + "',");
            strSql.Append("fhz_type_id='" + reportDow["fhz_type_id"] + "',");
            strSql.Append("fhz_id='" + reportDow["fhz_id"] + "',");
            strSql.Append("fsex='" + reportDow["fsex"] + "',");
            strSql.Append("fname='" + reportDow["fname"] + "',");
            strSql.Append("fage=" + reportDow["fage"] + ",");
            strSql.Append("fage_unit='" + reportDow["fage_unit"] + "',");
            strSql.Append("fage_year=" + reportDow["fage_year"] + ",");
            strSql.Append("fage_month=" + reportDow["fage_month"] + ",");
            strSql.Append("fage_day=" + reportDow["fage_day"] + ",");
            strSql.Append("fward_num='" + reportDow["fward_num"] + "',");
            strSql.Append("froom_num='" + reportDow["froom_num"] + "',");
            strSql.Append("fbed_num='" + reportDow["fbed_num"] + "',");
            strSql.Append("fblood_abo='" + reportDow["fblood_abo"] + "',");
            strSql.Append("fblood_rh='" + reportDow["fblood_rh"] + "',");
            strSql.Append("fjymd='" + reportDow["fjymd"] + "',");
            strSql.Append("fdiagnose='" + reportDow["fdiagnose"] + "',");
            strSql.Append("fzyh='" + reportDow["fzyh"] + "',");
            strSql.Append("fheat=" + reportDow["fheat"] + ",");
            strSql.Append("fxyld=" + reportDow["fxyld"] + ",");
            strSql.Append("fcyy='" + reportDow["fcyy"] + "',");
            strSql.Append("fxhdb=" + reportDow["fxhdb"] + ",");
            strSql.Append("fapply_code='" + reportDow["fapply_code"] + "',");
            strSql.Append("fapply_user_id='" + reportDow["fapply_user_id"] + "',");
            strSql.Append("fapply_dept_id='" + reportDow["fapply_dept_id"] + "',");
            strSql.Append("fapply_time='" + reportDow["fapply_time"] + "',");
            strSql.Append("fsampling_user_id='" + reportDow["fsampling_user_id"] + "',");
            strSql.Append("fsampling_time='" + reportDow["fsampling_time"] + "',");
            strSql.Append("fget_user_id='" + reportDow["fget_user_id"] + "',");
            strSql.Append("fget_user_time='" + reportDow["fget_user_time"] + "',");
            strSql.Append("fsend_user_id='" + reportDow["fsend_user_id"] + "',");
            strSql.Append("fsend_user_time='" + reportDow["fsend_user_time"] + "',");
            strSql.Append("fcheck_user_id='" + reportDow["fcheck_user_id"] + "',");
            strSql.Append("fcheck_time='" + reportDow["fcheck_time"] + "',");
            strSql.Append("fexamine_user_id='" + reportDow["fexamine_user_id"] + "',");
            strSql.Append("fexamine_time='" + reportDow["fexamine_time"] + "',");
            strSql.Append("fhandling_user_id='" + reportDow["fhandling_user_id"] + "',");
            strSql.Append("fhandling_user_time='" + reportDow["fhandling_user_time"] + "',");
            strSql.Append("fprint_time='" + reportDow["fprint_time"] + "',");
            strSql.Append("fprint_count=" + reportDow["fprint_count"] + ",");
            strSql.Append("freport_dept_id='" + reportDow["freport_dept_id"] + "',");
            strSql.Append("forder_by='" + reportDow["forder_by"] + "',");
            strSql.Append("fcharge_flag=" + reportDow["fcharge_flag"] + ",");
            strSql.Append("fjyf=" + reportDow["fjyf"] + ",");
            strSql.Append("fcharge_id='" + reportDow["fcharge_id"] + "',");
            strSql.Append("fcreate_user_id='" + reportDow["fcreate_user_id"] + "',");
            strSql.Append("fcreate_time='" + reportDow["fcreate_time"] + "',");
            strSql.Append("fupdate_user_id='" + reportDow["fupdate_user_id"] + "',");
            strSql.Append("fupdate_time='" + reportDow["fupdate_time"] + "',");
            strSql.Append("fremark='" + reportDow["fremark"] + "'");
            strSql.Append(" where fjy_id='" + reportDow["fjy_id"] + "'");
            return strSql.ToString();
        }
        /// <summary>
        /// 修改打印时间
        /// </summary>
        /// <param name="fjy_id"></param>
        /// <returns></returns>
        public  int BllJYUpdatePrintTime(string fjy_id)
        {
            string strTime = this.DbServerDateTim();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update " + table_jy + " set ");
            strSql.Append("fprint_time='" + strTime + "',");
            strSql.Append("fprint_count=fprint_count+1,");
            strSql.Append("fprint_flag=1,");            
            strSql.Append("fupdate_user_id='" + LoginBLL.strPersonID + "',");
            strSql.Append("fupdate_time='" + strTime + "'");
            strSql.Append(" where fjy_id='" + fjy_id + "'");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }
        //UPDATE SAM_JY_2008
//SET (fprint_time = '2008-11-11', fupdate_time = '2008-11-11')
//WHERE fjy_id = '1'
        public string BllJYUpdateAndResultRow(DataRowView reportDow,DataTable dtResult)
        {           
            IList lisSql = new ArrayList();
            lisSql.Add(BllJYUpdateSql(reportDow));
            StringBuilder strSql = null;
            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                strSql = new StringBuilder();
                strSql.Append("update " + table_jy_result + " set ");
                strSql.Append("fjy_id='" + dtResult.Rows[i]["fjy_id"] + "',");
                strSql.Append("fitem_id='" + dtResult.Rows[i]["fitem_id"] + "',");
                strSql.Append("forder_by='" + dtResult.Rows[i]["forder_by"] + "',");
                strSql.Append("fvalue='" + dtResult.Rows[i]["fvalue"] + "',");
                strSql.Append("fod='" + dtResult.Rows[i]["fod"] + "',");
                strSql.Append("fcutoff='" + dtResult.Rows[i]["fcutoff"] + "',");
                strSql.Append("fremark='" + dtResult.Rows[i]["fremark"] + "'");
                strSql.Append(" where fresult_id='" + dtResult.Rows[i]["fresult_id"] + "'");
                lisSql.Add(strSql.ToString());
            }
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, lisSql);
        }
        /// <summary>
        /// 审核报告 解了审核
        /// </summary>
        /// <param name="strid">报告ID</param>
        /// <returns></returns>
        public int BllJYExamine(string strid, int fexamine_flag)
        {
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, "Update " + table_jy + " set fexamine_flag=" + fexamine_flag + ",fstate=" + fexamine_flag + " where fjy_id='" + strid + "'");
        }
        /// <summary>
        /// 取得样本号 上或下
        /// </summary>
        /// <param name="fsample_code"></param>
        /// <param name="upORdown"></param>
        /// <returns></returns>
        public string BllGetfsampleCodeUpOrDown(string fsample_code, string upORdown)
        {
            if (fsample_code == "" || fsample_code == null)
            { }
            else {
                this.sam_jy_fsample_code = fsample_code.Length;
            }
            string strBW = "";
            switch (this.sam_jy_fsample_code)
            {
                case 2:
                    strBW = "0";
                    break;
                case 3:
                    strBW = "00";
                    break;
                case 4:
                    strBW = "000";
                    break;
                case 5:
                    strBW = "0000";
                    break;
                case 6:
                    strBW = "00000";
                    break;
                case 7:
                    strBW = "000000";
                    break;
                case 8:
                    strBW = "0000000";
                    break;
                case 9:
                    strBW = "00000000";
                    break;
                case 10:
                    strBW = "000000000";
                    break;
                default:
                    strBW = "";
                    break;
            }

            string strRCode = "";//返回的样本号
            if (fsample_code == "" || fsample_code == null || fsample_code.Length == 0)
            {
                if (upORdown == "up")
                {
                    fsample_code = strBW + "2";
                }
                else
                {
                    fsample_code = strBW + "0";
                }
            }
            int intLength = fsample_code.Length;


            int intDownNum = 1;
            if (upORdown == "up")
            {
                if (ww.wwf.com.Public.IsNumber(fsample_code))
                {
                    if (Convert.ToInt32(fsample_code) > 1)
                        intDownNum = Convert.ToInt32(fsample_code) - 1;
                }

            }
            if (upORdown == "down")
            {
                if (ww.wwf.com.Public.IsNumber(fsample_code))
                {
                    intDownNum = Convert.ToInt32(fsample_code) + 1;
                }

            }

            int intLiengthDownNum = intDownNum.ToString().Length;


            int intCX = intLength - intLiengthDownNum;



            switch (intCX)
            {
                case 1:
                    strRCode = "0" + intDownNum.ToString();
                    break;
                case 2:
                    strRCode = "00" + intDownNum.ToString();
                    break;
                case 3:
                    strRCode = "000" + intDownNum.ToString();
                    break;
                case 4:
                    strRCode = "0000" + intDownNum.ToString();
                    break;
                case 5:
                    strRCode = "00000" + intDownNum.ToString();
                    break;
                case 6:
                    strRCode = "000000" + intDownNum.ToString();
                    break;
                case 7:
                    strRCode = "0000000" + intDownNum.ToString();
                    break;
                case 8:
                    strRCode = "00000000" + intDownNum.ToString();
                    break;
                case 9:
                    strRCode = "000000000" + intDownNum.ToString();
                    break;
                case 10:
                    strRCode = "0000000000" + intDownNum.ToString();
                    break;
                default:
                    strRCode = intDownNum.ToString();
                    break;
            }


            return strRCode;
        }

        /// <summary>
        /// 报告存否 
        /// </summary>
        /// <param name="finstr_id">仪器ID</param>
        /// <param name="fjy_date">检验日期</param>
        /// <param name="fsample_code">样本号</param>

        /// <returns></returns>
        public bool BllJYExists(string finstr_id, string fjy_date, string fsample_code)
        {
            string sqlQuery = "SELECT count(1) FROM " + table_jy + " WHERE (finstr_id = '" + finstr_id + "') AND (fjy_date = '" + fjy_date + "') AND (fsample_code = '" + fsample_code + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn, sqlQuery);
        }
        /// <summary>
        /// 删除检验报告
        /// </summary>
        /// <param name="fjy_id"></param>
        /// <returns></returns>
        public bool BllJYDel(string fjy_id)
        {
            string sql1 = "delete from " + table_jy + " where fjy_id='" + fjy_id + "'";
            string sql2 = "delete from " + table_jy_result + " where fjy_id='" + fjy_id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(yunLis.wwfbll.WWFInit.strDBConn,sql1, sql2);
        }
       
        #endregion

        #region  结果

        /// <summary>
        /// 取得结果列表
        /// </summary>
        /// <param name="fjy_id"></param>
        /// <returns></returns>
        public DataTable BllResultDT(string fjy_id)
        {
            //string sql = "SELECT * FROM " + this.table_jy_result + " order by forder_by";
            string sql = "SELECT * FROM " + this.table_jy_result + " where fjy_id = '" + fjy_id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 新增一条记录
        /// </summary>
        /// <param name="drResult"></param>
        /// <returns></returns>
        public int BllResultAdd(DataRow drResult)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into " + this.table_jy_result + "(");
            strSql.Append("fresult_id,fjy_id,fitem_id,forder_by,fvalue,fod,fcutoff,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");
            strSql.Append("'" + drResult["fjy_id"] + "',");
            strSql.Append("'" + drResult["fitem_id"] + "',");
            strSql.Append("'" + drResult["forder_by"] + "',");
            strSql.Append("'" + drResult["fvalue"] + "',");
            strSql.Append("'" + drResult["fod"] + "',");
            strSql.Append("'" + drResult["fcutoff"] + "',");
            strSql.Append("'" + drResult["fremark"] + "'");
            strSql.Append(")");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }

        /// <summary>
        /// 更新一条数据 结果
        /// </summary>
        /// <param name="reportDow"></param>
        /// <returns></returns>
        public String BllResultUpdate(DataTable dtResult)
        {
            IList lisSql = new ArrayList();
            StringBuilder strSql = null;
            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                strSql = new StringBuilder();
                strSql.Append("update " + table_jy_result + " set ");
                strSql.Append("fjy_id='" + dtResult.Rows[i]["fjy_id"] + "',");
                strSql.Append("fitem_id='" + dtResult.Rows[i]["fitem_id"] + "',");
                strSql.Append("forder_by='" + dtResult.Rows[i]["forder_by"] + "',");
                strSql.Append("fvalue='" + dtResult.Rows[i]["fvalue"] + "',");
                strSql.Append("fod='" + dtResult.Rows[i]["fod"] + "',");
                strSql.Append("fcutoff='" + dtResult.Rows[i]["fcutoff"] + "',");
                strSql.Append("fremark='" + dtResult.Rows[i]["fremark"] + "'");
                strSql.Append(" where fresult_id='" + dtResult.Rows[i]["fresult_id"] + "'");
                lisSql.Add(strSql.ToString());
            }
          return  yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, lisSql);
        }
        /// <summary>
        /// 删除检验项目
        /// </summary>
        /// <param name="fresult_id"></param>
        /// <returns></returns>
        public int BllResultDel(string fresult_id)
        {
            string sql = "delete from " + table_jy_result + " where fresult_id='" + fresult_id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql);
        }

        #endregion

        #region  项目 参考值 标识
        /// <summary>
        /// 项目表
        /// </summary>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public DataTable BllItemDT(string fitem_id)
        {
            string sql = "SELECT (SELECT fname FROM SAM_TYPE t1 WHERE (ftype = '常用单位') AND (fcode = t.funit_name)) as funitname,* FROM SAM_ITEM t WHERE (fitem_id = '" + fitem_id + "')";
           
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 参考明细表
        /// </summary>
        /// <param name="fitem_id"></param>
        /// <returns></returns>
        public DataTable BllItemRefDT(string fitem_id)
        {
            string sql = "SELECT * FROM SAM_ITEM_REF WHERE (fitem_id = '" + fitem_id + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 取得 参考值
        /// </summary>
        /// <param name="drItem"></param>
        /// <returns></returns>
        public string BllItemfref(DataRow drItem, DataRowView drvReport)
        {
            string fref = "";
            string fitem_id = "";//项目ID
            string fitem_fname = "";//项目名称 
            string fref_if_age = "0";//年龄有关
            string fref_if_sex = "0";//性别有关
            string fref_if_sample = "0";//样本有关
            string fref_if_method = "0";//方法有关


            fitem_id = drItem["fitem_id"].ToString();
            fitem_fname = drItem["fname"].ToString();
            fref_if_age = drItem["fref_if_age"].ToString();
            fref_if_sex = drItem["fref_if_sex"].ToString();
            fref_if_sample = drItem["fref_if_sample"].ToString();
            fref_if_method = drItem["fref_if_method"].ToString();

            fref = drItem["fref"].ToString();//如果参考值没找到对照的明细那么取项目上设置默认那个
            if (fref_if_age == "0" & fref_if_sex == "0" & fref_if_sample == "0" & fref_if_method == "0")
            {
            }
            else
            {
                DataTable dtSAM_ITEM_REF = null;//项目参考值明细
                dtSAM_ITEM_REF = BllItemRefDT(fitem_id);//取得参考明细表   
                
                string intfage = "0";//取得当前年龄
                string strfsex = "0";//取得当前性别
                string strfsample_type_id = "";//取得当前样本ID               
                if (drvReport["fage"] != null)
                    intfage = drvReport["fage"].ToString();
                if (drvReport["fsex"] != null)
                    strfsex = drvReport["fsex"].ToString();
                if (drvReport["fsample_type_id"] != null)
                    strfsample_type_id = drvReport["fsample_type_id"].ToString();

                string strQuerySql = "";//查询参考值明细SQL构造
                //年龄-------
                if (fref_if_age == "1" & fref_if_sex == "0" & fref_if_sample == "0")//年龄
                {
                    strQuerySql = "(" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "0")//年龄+性别
                {
                    strQuerySql = "('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                if (fref_if_age == "1" & fref_if_sex == "1" & fref_if_sample == "1")//年龄+性别+样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex) AND (" + intfage + ">=fage_low) AND (" + intfage + "<fage_high)";
                }
                //性别-------
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "0")//性别
                {
                    strQuerySql = "('" + strfsex + "'=fsex)";
                }
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//性别+样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                }
                //样本-------
                if (fref_if_age == "0" & fref_if_sex == "0" & fref_if_sample == "1")//样本
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id)";
                }
                if (fref_if_age == "0" & fref_if_sex == "1" & fref_if_sample == "1")//样本+性别
                {
                    strQuerySql = "('" + strfsample_type_id + "'=fsample_type_id) AND ('" + strfsex + "'=fsex)";
                }
                //------------------
                DataRow[] frfList = dtSAM_ITEM_REF.Select(strQuerySql);
                if (frfList.Length > 0)
                {
                    foreach (DataRow drref in frfList)
                    {
                        fref = drref["fref"].ToString();
                        //MessageBox.Show("参考值为:" + fref + "\n\n" + strQuerySql);
                    }
                }
                //else
                //{
                //fref = fref;//如果参考值没找到对照的明细那么取项目上设置默认那个
                //WWMessage.MessageShowWarning("项目："+fitem_fname+" 的参考值设置有误！\n\n" + strQuerySql);
                //}
                // ↑↓
            }
            return fref;
        }
        /// <summary>
        /// 结果标记
        /// </summary>
        /// <param name="fvalue">值</param>
        /// <param name="fref">参考值</param>
        /// <returns>正常:0 偏低:1 偏高:2</returns>
        public int BllItemValueBJ(string fvalue, string fref)
        {
            int intBJ = 0;
            string[] strRefList = System.Text.RegularExpressions.Regex.Split(fref, @"--");
            if (Public.IsNumber(fvalue))
            {
                Double douFvalue = Convert.ToDouble(fvalue);
                if (strRefList.Length == 2)
                {
                    if (Public.IsNumber(strRefList[0].ToString()) & Public.IsNumber(strRefList[1].ToString()))
                    {
                        if (douFvalue < Convert.ToDouble(strRefList[0].ToString()))//太小
                        {
                            intBJ = 1;
                        }
                        if (douFvalue > Convert.ToDouble(strRefList[1].ToString()))//太大
                        {
                            intBJ = 2;
                        }

                    }
                }
            }
            return intBJ;
        }
        #endregion
    }
}
/*
 Name	Code	Data Type	Primary	Foreign Key	Mandatory
检验_id	fjy_id	varchar(32)	TRUE	FALSE	TRUE
仪器_id	finstr_id	varchar(32)	FALSE	FALSE	TRUE
检验日期	fjy_date	varchar(32)	FALSE	FALSE	TRUE
样本号	fsample_code	varchar(32)	FALSE	FALSE	TRUE
打印否	fprint_flag	int	FALSE	FALSE	FALSE
审核否	fexamine_flag	int	FALSE	FALSE	FALSE
状态	fstate	varchar(32)	FALSE	FALSE	FALSE
检验组_id	fjygroup_id	varchar(32)	FALSE	FALSE	FALSE
检验类型_id	fjytype_id	varchar(32)	FALSE	FALSE	FALSE
样本类型_id	fsample_type_id	varchar(32)	FALSE	FALSE	FALSE
P患者类型_id	fhz_type_id	varchar(32)	FALSE	FALSE	FALSE
P患者_id	fhz_id	varchar(32)	FALSE	FALSE	FALSE
P性别	fsex	varchar(32)	FALSE	FALSE	FALSE
P患者姓名	fname	varchar(32)	FALSE	FALSE	FALSE
P年龄	fage	int	FALSE	FALSE	FALSE
P年龄单	fage_year	int	FALSE	FALSE	FALSE
F年龄(月)	位	fage_unit	varchar(32)	FALSE	FALSE	FALSE
F年龄(年)fage_month	int	FALSE	FALSE	FALSE
F年龄(天)	fage_day	int	FALSE	FALSE	FALSE
P病区	fward_num	varchar(32)	FALSE	FALSE	FALSE
P房间号	froom_num	varchar(32)	FALSE	FALSE	FALSE
P床号	fbed_num	varchar(32)	FALSE	FALSE	FALSE
PABO血型	fblood_abo	varchar(32)	FALSE	FALSE	FALSE
PRH血型	fblood_rh	varchar(32)	FALSE	FALSE	FALSE
P检验目的	fjymd	varchar(200)	FALSE	FALSE	FALSE
P临床诊断	fdiagnose	varchar(200)	FALSE	FALSE	FALSE
P住院号	fzyh	varchar(32)	FALSE	FALSE	FALSE
P体温	fheat	float	FALSE	FALSE	FALSE
P吸氧浓度	fxyld	float	FALSE	FALSE	FALSE
P曾用药	fcyy	varchar(200)	FALSE	FALSE	FALSE
P血红蛋白	fxhdb	int	FALSE	FALSE	FALSE
R申请号	fapply_code	varchar(32)	FALSE	FALSE	FALSE
R申请人	fapply_user_id	varchar(32)	FALSE	FALSE	FALSE
R申请科室_id	fapply_dept_id	varchar(32)	FALSE	FALSE	FALSE
R申请时间	fapply_time	varchar(32)	FALSE	FALSE	FALSE
R采样人_id	fsampling_user_id	varchar(32)	FALSE	FALSE	FALSE
R采样时间	fsampling_time	varchar(32)	FALSE	FALSE	FALSE
R接收人_id	fget_user_id	varchar(32)	FALSE	FALSE	FALSE
R接样时间	fget_user_time	varchar(32)	FALSE	FALSE	FALSE
R送样人_id	fsend_user_id	varchar(32)	FALSE	FALSE	FALSE
R送样时间	fsend_user_time	varchar(32)	FALSE	FALSE	FALSE
R检验人_id	fcheck_user_id	varchar(32)	FALSE	FALSE	FALSE
R检验时间	fcheck_time	varchar(32)	FALSE	FALSE	FALSE
R审核医师_id	fexamine_user_id	varchar(32)	FALSE	FALSE	FALSE
R审核时	fexamine_time	varchar(32)	FALSE	FALSE	FALSE
R处理人_id	fhandling_user_id	varchar(32)	FALSE	FALSE	FALSE
R处理时间间	fhandling_user_time	varchar(32)	FALSE	FALSE	FALSE
P打印时间	fprint_time	varchar(32)	FALSE	FALSE	FALSE
P打印次数	fprint_count	int	FALSE	FALSE	FALSE
R报告科室	freport_dept_id	varchar(32)	FALSE	FALSE	FALSE
排序号	forder_by	varchar(32)	FALSE	FALSE	FALSE
收费否	fcharge_flag	int	FALSE	FALSE	FALSE
检验费	fjyf	float	FALSE	FALSE	FALSE
费别_id	fcharge_id	varchar(32)	FALSE	FALSE	FALSE
创建人_id	fcreate_user_id	varchar(32)	FALSE	FALSE	FALSE
创建时间	fcreate_time	varchar(32)	FALSE	FALSE	FALSE
修改人_id	fupdate_user_id	varchar(32)	FALSE	FALSE	FALSE
修改时间	fupdate_time	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
 */
/*
        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataTable BllJYdt(string strWhere)
        {
            if (strWhere == "" || strWhere == null)
            { }
            else
            {
                strWhere = " Where " + strWhere + " order by fsample_code";
            }
            StringBuilder sqljy = new StringBuilder();
            string fsex_ = "(SELECT fname FROM sam_type WHERE (ftype = '性别') AND (fcode = t.fsex)) as fsex_ ";//性别     
            string fhz_type_id_ = "(SELECT fname FROM sam_type WHERE (ftype = '病人类别') AND (fcode = t.fhz_type_id)) as fhz_type_id_ ";//病人类别  
            string fstate_ = "(SELECT fname FROM sam_type WHERE (ftype = '检验状态') AND (fcode = t.fstate)) as fstate_ ";//检验状态   
            string fcharge_flag_ = "(SELECT fname FROM sam_type WHERE (ftype = '收费否') AND (fcode = t.fcharge_flag)) as fcharge_flag_ ";//收费否   
            DataTable dtColumns = this.bllField.BllColumnsByftable_id(userdefinedJYid);
            string strColName = "";//当前列名
            for (int i = 0; i < dtColumns.Rows.Count; i++)
            {
                strColName = dtColumns.Rows[i]["fcode"].ToString();
                //if (strColName.Substring(strColName.Length - 1).ToString() == "_")
                //strColName = strColName + "_";

                if (strColName == "fsex_")
                {
                    strColName = fsex_;
                }
                if (strColName == "fhz_type_id_")
                {
                    strColName = fhz_type_id_;
                }
                if (strColName == "fstate_")
                {
                    strColName = fstate_;
                }
                if (strColName == "fcharge_flag_")
                {
                    strColName = fcharge_flag_;
                }
                if (i == 0)
                    sqljy.Append("SELECT " + strColName);
                else
                    sqljy.Append("," + strColName);

            }
            sqljy.Append(" FROM " + table_jy + " as t " + strWhere);
            strSqlOK = sqljy.ToString();
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sqljy.ToString()).Tables[0];
        }

        #region  报告管理
        /// <summary>
        /// 报告新增一行
        /// </summary>
        /// <param name="reportDow"></param>
        /// <returns></returns>
        public int BllJYAdd(DataRow reportDow)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into " + table_jy + "(");
            strSql.Append("fjy_id,finstr_id,fjy_date,fsample_code,fhz_type_id,fsex,fage_unit,fcheck_user_id,fexamine_user_id,fapply_time,fsampling_time,fcheck_time,fexamine_time,fcharge_flag,fstate,fprint_flag,fprint_count,fexamine_flag,fjygroup_id,fapply_code");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + reportDow["fjy_id"] + "',");
            strSql.Append("'" + reportDow["finstr_id"] + "',");
            strSql.Append("'" + reportDow["fjy_date"] + "',");
            strSql.Append("'" + reportDow["fsample_code"] + "',");

            strSql.Append("'" + reportDow["fhz_type_id"] + "',");
            strSql.Append("'" + reportDow["fsex"] + "',");
            strSql.Append("'" + reportDow["fage_unit"] + "',");
            strSql.Append("'" + reportDow["fcheck_user_id"] + "',");
            strSql.Append("'" + reportDow["fexamine_user_id"] + "',");
            strSql.Append("'" + reportDow["fapply_time"] + "',");
            strSql.Append("'" + reportDow["fsampling_time"] + "',");
            strSql.Append("'" + reportDow["fcheck_time"] + "',");
            strSql.Append("'" + reportDow["fexamine_time"] + "',");
            strSql.Append("'" + reportDow["fcharge_flag"] + "',");
            strSql.Append("'" + reportDow["fstate"] + "',");
            strSql.Append("'" + reportDow["fprint_flag"] + "',");
            strSql.Append("'" + reportDow["fprint_count"] + "',");
            strSql.Append("'" + reportDow["fexamine_flag"] + "',");
            strSql.Append("'" + reportDow["fjygroup_id"] + "',");
            strSql.Append("'" + reportDow["fapply_code"] + "'");
            strSql.Append(")");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, strSql.ToString());
            /*
             * ,,,,,,,,,,,,,,,,,,
              drReport["fhz_type_id"] = "0";
                    drReport["fsex"] = "0";
                    drReport["fage_unit"] = "0";
                    drReport["fcheck_user_id"] = yunLis.wwfbll.LoginBLL.strPersonID;
                    drReport["fexamine_user_id"] = yunLis.wwfbll.LoginBLL.strPersonID;
                    drReport["fapply_time"] = fapply_timeDateTimePicker.Value.ToLongDateString();
                    drReport["fsampling_time"] = fsampling_timeDateTimePicker.Value.ToLongDateString();
                    drReport["fcheck_time"] = fcheck_timeDateTimePicker.Value.ToLongDateString();
                    drReport["fexamine_time"] = fexamine_timeDateTimePicker.Value.ToLongDateString();
                    drReport["fcharge_flag"] = "1";//收费否
                    drReport["fstate"] = "0";//检验状态
                    drReport["fprint_flag"] = "0";//打印否
                    drReport["fprint_count"] = "0";//打印次数
                    drReport["fexamine_flag"] = "0";//审核否
                    drReport["fjygroup_id"] = yunLis.wwfbll.LoginBLL.strDeptID;//检验组
                    drReport["fapply_code"] = this.bllJY.DbNum("sam_jy_fapply_code");//申请单号
             
        }
*/ 