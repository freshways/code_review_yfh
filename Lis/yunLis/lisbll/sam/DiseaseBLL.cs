using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using yunLis.dao;
using yunLis.wwfbll;
namespace yunLis.lisbll.sam
{
    /// <summary>
    /// 
    /// </summary>
    public class DiseaseBLL : DAOWWF
    {
        public DiseaseBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }

        public DataTable BllDTByCode(string strfcode, string strfname)
        {
            string sql = "SELECT * FROM SAM_DISEASE where  (fcode='" + strfcode + "' or fname ='" + strfname + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        public DataTable BllDT(string strWhere)
        {
            string sql = "SELECT * FROM SAM_DISEASE " + strWhere + " ORDER BY fname";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];

        }

        #region  疾病 类型
        /// <summary>
        /// 仪器类型 列表 1为启用；0为未启用；其它如-1等为所有
        /// </summary>
        /// <returns></returns>
        public DataTable BllTypeDT(int fuse_if)
        {
            string sql = "";
            if (fuse_if == 1 || fuse_if == 0)
            {
                sql = "SELECT * FROM SAM_DISEASE_TYPE where fuse_if=" + fuse_if + " ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT * FROM SAM_DISEASE_TYPE  ORDER BY forder_by";
            }
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 上级取下级
        /// </summary>
        /// <param name="fp_id"></param>
        /// <returns></returns>
        public DataTable BllTypeDTByPid(string fp_id)
        {
            string sql = "SELECT * FROM SAM_DISEASE_TYPE where fp_id='" + fp_id + "' ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        /// <summary>
        /// 仪器类型 增加一条数据
        /// </summary>
        public int BllTypeAdd(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SAM_DISEASE_TYPE(");
            strSql.Append("ftype_id,fp_id,fcode,fname,fname_e,fuse_if,forder_by,fhelp_code,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["ftype_id"].ToString() + "',");
            strSql.Append("'" + dr["fp_id"].ToString() + "',");
            strSql.Append("'" + dr["fcode"].ToString() + "',");
            strSql.Append("'" + dr["fname"].ToString() + "',");
            strSql.Append("'" + dr["fname_e"].ToString() + "',");
            strSql.Append("" + dr["fuse_if"].ToString() + ",");
            strSql.Append("'" + dr["forder_by"].ToString() + "',");
            strSql.Append("'" + dr["fhelp_code"].ToString() + "',");
            strSql.Append("'" + dr["fremark"].ToString() + "'");
            strSql.Append(")");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }


        /// <summary>
        /// 仪器类型 更新一条数据
        /// </summary>
        public int BllTypeUpdate(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_DISEASE_TYPE set ");
            strSql.Append("fp_id='" + dr["fp_id"].ToString() + "',");
            strSql.Append("fcode='" + dr["fcode"].ToString() + "',");
            strSql.Append("fname='" + dr["fname"].ToString() + "',");
            strSql.Append("fname_e='" + dr["fname_e"].ToString() + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"].ToString() + ",");
            strSql.Append("forder_by='" + dr["forder_by"].ToString() + "',");
            strSql.Append("fhelp_code='" + dr["fhelp_code"].ToString() + "',");
            strSql.Append("fremark='" + dr["fremark"].ToString() + "'");
            strSql.Append(" where ftype_id='" + dr["ftype_id"].ToString() + "'");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());

        }

        /// <summary>
        /// 仪器类型 删除一条数据
        /// </summary>
        public int BllTypeDelete(string id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete FROM SAM_DISEASE_TYPE ");
            strSql.Append(" where ftype_id='" + id + "'");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }
        #endregion 

        #region  疾病 
        public DataTable BllDiseaseDTByTypeid(string ftype_id)
        {
            string sql = "SELECT * FROM SAM_DISEASE where ftype_id='" + ftype_id + "' ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 仪器类型 增加一条数据
        /// </summary>
        public int BllDiseaseAdd(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SAM_DISEASE(");
            strSql.Append("fid,ftype_id,fcode,fname,fname_e,fuse_if,forder_by,fhelp_code,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");
            strSql.Append("'" + dr["ftype_id"].ToString() + "',");           
            strSql.Append("'" + dr["fcode"].ToString() + "',");
            strSql.Append("'" + dr["fname"].ToString() + "',");
            strSql.Append("'" + dr["fname_e"].ToString() + "',");
            strSql.Append("" + dr["fuse_if"].ToString() + ",");
            strSql.Append("'" + dr["forder_by"].ToString() + "',");
            strSql.Append("'" + dr["fhelp_code"].ToString() + "',");
            strSql.Append("'" + dr["fremark"].ToString() + "'");
            strSql.Append(")");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
        /// <summary>
        /// 仪器类型 更新一条数据
        /// </summary>
        public int BllDiseaseUpdate(DataRow dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SAM_DISEASE set ");
            strSql.Append("ftype_id='" + dr["ftype_id"].ToString() + "',");
            strSql.Append("fcode='" + dr["fcode"].ToString() + "',");
            strSql.Append("fname='" + dr["fname"].ToString() + "',");
            strSql.Append("fname_e='" + dr["fname_e"].ToString() + "',");
            strSql.Append("fuse_if=" + dr["fuse_if"].ToString() + ",");
            strSql.Append("forder_by='" + dr["forder_by"].ToString() + "',");
            strSql.Append("fhelp_code='" + dr["fhelp_code"].ToString() + "',");
            strSql.Append("fremark='" + dr["fremark"].ToString() + "'");
            strSql.Append(" where fid='" + dr["fid"].ToString() + "'");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, strSql.ToString());

        }
        #endregion
    }
}
