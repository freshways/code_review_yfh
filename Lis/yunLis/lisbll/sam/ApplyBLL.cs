using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using yunLis.dao;
using yunLis.wwfbll;
using ww.wwf.com;
using System.Collections;
using System.Data.SqlClient;
using HIS.Model;
using System.Linq;
using HIS.COMM;
using WEISHENG.COMM;
using System.Data.Entity.Validation;

namespace yunLis.lisbll.sam
{
    /*
     SAM_APPLY_2008
     SAM_INSTR_IO_2008
     * SAM_INSTR_IO_IMG_2008
     * SAM_INSTR_IO_RESULT_2008
     * SAM_JY_IMG_2008
     * SAM_JY_RESULT_2008
     * SAM_SAMPLE_2008
     * LIS_REPORT_2008
     */
    /// <summary>
    /// 检验申请 规则
    /// </summary>
    public class ApplyBLL : DAOWWF
    {
        string table_SAM_APPLY = "";
        string table_SAM_JY_RESULT = "";
        string table_SAM_SAMPLE = "";
        public ApplyBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //           
            table_SAM_APPLY = "SAM_APPLY";
            table_SAM_JY_RESULT = "SAM_JY_RESULT";
            table_SAM_SAMPLE = "SAM_SAMPLE";
        }

        /// <summary>
        /// 申请 增加
        /// </summary>
        /// <param name="rowApply">主表</param>
        /// <param name="dtResult">结果表</param>
        /// <returns>返回true表示成功</returns>
        public string BllApplyAdd(ApplyModel model, DataTable dtResult, string fitem_group)
        {
            IList addList = new ArrayList();
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into " + table_SAM_APPLY + "(");
            strSql.Append("fapply_id,fjytype_id,fsample_type_id,fjz_flag,fcharge_flag,fcharge_id,fstate,fjyf,fapply_user_id,fapply_dept_id,fapply_time,ftype_id,fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fage,fjob,fgms,fjob_org,fadd,ftel,fage_unit,fage_year,fage_month,fage_day,fward_num,froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fheat,fxyld,fcyy,fxhdb,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fitem_group,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + model.fapply_id + "',");
            strSql.Append("'" + model.fjytype_id + "',");
            strSql.Append("'" + model.fsample_type_id + "',");
            strSql.Append("" + model.fjz_flag + ",");
            strSql.Append("" + model.fcharge_flag + ",");
            strSql.Append("'" + model.fcharge_id + "',");
            strSql.Append("'" + model.fstate + "',");
            strSql.Append("" + model.fjyf + ",");
            strSql.Append("'" + model.fapply_user_id + "',");
            strSql.Append("'" + model.fapply_dept_id + "',");
            strSql.Append("'" + model.fapply_time + "',");
            strSql.Append("'" + model.ftype_id + "',");
            strSql.Append("'" + model.fhz_id + "',");
            strSql.Append("'" + model.fhz_zyh + "',");
            strSql.Append("'" + model.fsex + "',");
            strSql.Append("'" + model.fname + "',");
            strSql.Append("'" + model.fvolk + "',");
            strSql.Append("'" + model.fhymen + "',");
            strSql.Append("" + model.fage + ",");
            strSql.Append("'" + model.fjob + "',");
            strSql.Append("'" + model.fgms + "',");
            strSql.Append("'" + model.fjob_org + "',");
            strSql.Append("'" + model.fadd + "',");
            strSql.Append("'" + model.ftel + "',");
            strSql.Append("'" + model.fage_unit + "',");
            strSql.Append("" + model.fage_year + ",");
            strSql.Append("" + model.fage_month + ",");
            strSql.Append("" + model.fage_day + ",");
            strSql.Append("'" + model.fward_num + "',");
            strSql.Append("'" + model.froom_num + "',");
            strSql.Append("'" + model.fbed_num + "',");
            strSql.Append("'" + model.fblood_abo + "',");
            strSql.Append("'" + model.fblood_rh + "',");
            strSql.Append("'" + model.fjymd + "',");
            strSql.Append("'" + model.fdiagnose + "',");
            strSql.Append("" + model.fheat + ",");
            strSql.Append("" + model.fxyld + ",");
            strSql.Append("'" + model.fcyy + "',");
            strSql.Append("" + model.fxhdb + ",");
            strSql.Append("'" + model.fcreate_user_id + "',");
            strSql.Append("'" + model.fcreate_time + "',");
            strSql.Append("'" + model.fupdate_user_id + "',");
            strSql.Append("'" + model.fupdate_time + "',");
            strSql.Append("'" + fitem_group + "',");
            strSql.Append("'" + model.fremark + "'");
            strSql.Append(")");
            addList.Add(strSql.ToString());
            StringBuilder strSqlSam = new StringBuilder();
            strSqlSam.Append("insert into " + table_SAM_SAMPLE + "(");
            strSqlSam.Append("fsample_id,fapply_id,fsample_barcode,fexamine_flag,fprint_flag,fprint_count,fread_flag,fstate");
            strSqlSam.Append(")");
            strSqlSam.Append(" values (");
            strSqlSam.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");
            strSqlSam.Append("'" + model.fapply_id + "',");
            strSqlSam.Append("'" + model.fapply_id + "',");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("'" + model.fstate + "'");
            strSqlSam.Append(")");
            addList.Add(strSqlSam.ToString());

            for (int i = 0; i < dtResult.Rows.Count; i++)
            {
                StringBuilder strSql_Result = new StringBuilder();
                strSql_Result.Append("insert into " + table_SAM_JY_RESULT + "(");
                strSql_Result.Append("fresult_id,fapply_id,fitem_id,fitem_code,fitem_name,fitem_unit,forder_by");
                strSql_Result.Append(")");
                strSql_Result.Append(" values (");
                strSql_Result.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");
                strSql_Result.Append("'" + model.fapply_id + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["fitem_id"] + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["fitem_code"] + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["fitem_name"] + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["fitem_unit"] + "',");
                strSql_Result.Append("'" + dtResult.Rows[i]["forder_by"] + "'");
                strSql_Result.Append(")");
                addList.Add(strSql_Result.ToString());
            }
            addList.Add("DELETE FROM SAM_APPLY_BCODE WHERE  (fapply_id = '" + model.fapply_id + "')");

            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, addList);
        }

        #region  申请
        /// <summary>
        /// 申请 增加
        /// </summary>
        /// <param name="rowApply">主表</param>
        /// <param name="listSamJyResult">结果表</param>
        /// <returns>返回true表示成功</returns>
        public bool BllApplyAdd(HIS.Model.SAM_APPLY samApply, List<SAM_JY_RESULT> listSamJyResult, string fitem_group, Byte[] imgfbcode)
        {
            using (LISEntities lisDB = new LISEntities(EF6Helper.GetLISEf6Conn(DBConnHelper.sLISConnString)))
            {
                using (var dbContextTransaction = lisDB.Database.BeginTransaction())
                {
                    try
                    {
                        lisDB.SAM_APPLY.Add(samApply);
                        SAM_SAMPLE sAM_SAMPLE = new SAM_SAMPLE()
                        {
                            fsample_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid(),
                            fapply_id = samApply.fapply_id,
                            fsample_barcode = samApply.fapply_id,
                            fexamine_flag = 0,
                            fprint_flag = 0,
                            fprint_count = 0,
                            fread_flag = 0,
                            fstate = samApply.fstate
                        };

                        lisDB.SAM_SAMPLE.Add(sAM_SAMPLE);
                        foreach (var item in listSamJyResult)
                        {
                            item.fapply_id = samApply.fapply_id;
                            item.fresult_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                        }
                        lisDB.SAM_JY_RESULT.AddRange(listSamJyResult);
                        lisDB.Database.ExecuteSqlCommand("DELETE FROM SAM_APPLY_BCODE WHERE  (fapply_id = '" + samApply.fapply_id + "')");
                        SAM_APPLY_BCODE sAM_APPLY_BCODE = new SAM_APPLY_BCODE()
                        {
                            fapply_id = samApply.fapply_id,
                            fbcode = imgfbcode
                        };
                        lisDB.SAM_APPLY_BCODE.Add(sAM_APPLY_BCODE);
                        lisDB.SaveChanges();
                        dbContextTransaction.Commit();
                        HIS.COMM.msgBalloonHelper.BalloonShow("数据保存成功");
                        return true;
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                                    validationErrors.Entry.Entity.GetType().FullName,
                                    validationError.PropertyName,
                                    validationError.ErrorMessage));
                            }
                        }
                        dbContextTransaction.Rollback();
                        HIS.COMM.msgBalloonHelper.ShowInformation(stringBuilder.ToString());
                        return false;
                    }
                    catch (Exception ex)
                    {
                        string errorMsg = "错误：";
                        if (ex.InnerException == null)
                            errorMsg += ex.Message + "，";
                        else if (ex.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.Message + "，";
                        else if (ex.InnerException.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.InnerException.Message;
                        dbContextTransaction.Rollback();
                        HIS.COMM.msgBalloonHelper.ShowInformation(errorMsg);
                        return false;
                    }
                }
            }

        }

        /// <summary>
        /// 申请 增加 本方法于20150623创建  与上面方法BllApplyAdd的区别：
        /// ①删除对SAM_JY_RESULT、SAM_APPLY_BCODE两个数据表的操作
        /// </summary>
        /// <param name="rowApply">主表</param>
        /// <param name="dtResult">结果表</param>
        /// <returns>返回true表示成功</returns>
        public string save_Sam_apply_Sample(HIS.Model.SAM_APPLY model)
        {
            string strReturn = string.Empty;
            IList addList = new ArrayList();
            StringBuilder strSql = new StringBuilder();
            //            strSql.Append(@"declare @applyid varchar(32) 
            //EXEC [GetApplyNo] '" + model.ftype_id + "','" + DateTime.Now.ToString("yyMMdd") + @"',@applyid output 
            strSql.Append("insert into SAM_APPLY (");
            strSql.Append(@"fapply_id,fjytype_id,fsample_type_id,fjz_flag,fcharge_flag,fcharge_id,fstate,fjyf,fapply_user_id,fapply_dept_id,
fapply_time,ftype_id,fhz_id,fhz_zyh,fsex,fname,fvolk,fhymen,fage,fjob,fgms,fjob_org,fadd,ftel,fage_unit,fage_year,fage_month,fage_day,fward_num,
froom_num,fbed_num,fblood_abo,fblood_rh,fjymd,fdiagnose,fheat,fxyld,fcyy,fxhdb,fcreate_user_id,fcreate_time,fupdate_user_id,fupdate_time,fitem_group,fremark,his_recordid,sfzh,card_no");//changed by wjz 20160122 追加了身份证号字段 //2019年11月12日 yfh 添加电子健康卡
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + model.fapply_id + "',");
            strSql.Append("'" + model.fjytype_id + "',");
            strSql.Append("'" + model.fsample_type_id + "',");
            strSql.Append("" + model.fjz_flag + ",");
            strSql.Append("" + model.fcharge_flag + ",");
            strSql.Append("'" + model.fcharge_id + "',");
            strSql.Append("'" + model.fstate + "',");
            strSql.Append("" + model.fjyf + ",");
            strSql.Append("'" + model.fapply_user_id + "',");
            strSql.Append("'" + model.fapply_dept_id + "',");
            strSql.Append("'" + model.fapply_time + "',");
            strSql.Append("'" + model.ftype_id + "',");
            strSql.Append("'" + model.fhz_id + "',");
            strSql.Append("'" + model.fhz_zyh + "',");
            strSql.Append("'" + model.fsex + "',");
            strSql.Append("'" + model.fname + "',");
            strSql.Append("'" + model.fvolk + "',");
            strSql.Append("'" + model.fhymen + "',");
            strSql.Append("" + model.fage + ",");
            strSql.Append("'" + model.fjob + "',");
            strSql.Append("'" + model.fgms + "',");
            strSql.Append("'" + model.fjob_org + "',");
            strSql.Append("'" + model.fadd + "',");
            strSql.Append("'" + model.ftel + "',");
            strSql.Append("'" + model.fage_unit + "',");
            strSql.Append("" + model.fage_year + ",");
            strSql.Append("" + model.fage_month + ",");
            strSql.Append("" + model.fage_day + ",");
            strSql.Append("'" + model.fward_num + "',");
            strSql.Append("'" + model.froom_num + "',");
            strSql.Append("'" + model.fbed_num + "',");
            strSql.Append("'" + model.fblood_abo + "',");
            strSql.Append("'" + model.fblood_rh + "',");
            strSql.Append("'" + model.fjymd + "',");
            strSql.Append("'" + model.fdiagnose + "',");
            strSql.Append("" + model.fheat + ",");
            strSql.Append("" + model.fxyld + ",");
            strSql.Append("'" + model.fcyy + "',");
            strSql.Append("" + model.fxhdb + ",");
            strSql.Append("'" + model.fcreate_user_id + "',");
            strSql.Append("convert(varchar(20), getdate(),20),");
            strSql.Append("'" + model.fupdate_user_id + "',");
            strSql.Append("convert(varchar(20), getdate(),20),");
            strSql.Append("'" + model.fitem_group + "',");
            strSql.Append("'" + model.fremark + "',");
            strSql.Append("'" + model.his_recordid + "'");    
            strSql.Append(",'" + model.sfzh + "'");
            strSql.Append(",'" + model.card_no + "'");
            strSql.Append(")");
            addList.Add(strSql.ToString());

            StringBuilder strSqlSam = new StringBuilder();
            strSqlSam.Append("insert into SAM_SAMPLE (");
            strSqlSam.Append("fsample_id,fapply_id,fsample_barcode,fexamine_flag,fprint_flag,fprint_count,fread_flag,fstate");
            strSqlSam.Append(")");
            strSqlSam.Append(" values (");
            strSqlSam.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");
            strSqlSam.Append("'" + model.fapply_id + "',");
            strSqlSam.Append("'" + model.fapply_id + "',");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("0,");
            strSqlSam.Append("'" + model.fstate + "'");
            strSqlSam.Append(") ");
            addList.Add(strSqlSam.ToString());

            string strRet = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, addList);

            if (strRet == "true")
            {
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    decimal dh = Convert.ToDecimal(model.his_recordid);
                    var item = chis.JY申请单摘要.Where(c => c.申请单号 == dh).FirstOrDefault();
                    item.状态 = "打印";
                    item.LIS结果ID = model.fapply_id;
                    chis.JY申请单摘要.Attach(item);
                    chis.Entry(item).Property(c => c.LIS结果ID).IsModified = true;
                    chis.Entry(item).Property(c => c.状态).IsModified = true;
                    chis.SaveChanges();
                }
            }
            strReturn = strRet;
            return strReturn;
        }

        #endregion


        /// <summary>
        /// 保存检验申请条码号
        /// </summary>
        /// <param name="strfapply_id"></param>
        /// <param name="imgfbcode"></param>
        /// <returns></returns>
        public int BllApplyBCodeAdd(String strfapply_id, Byte[] imgfbcode)
        {
            int intR = 0;
            //yunLis.wwfbll.WWFInit.strDBConn
            intR = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbApplyBcodeAdd(yunLis.wwfbll.WWFInit.strDBConn, "SAM_APPLY_BCODE_ADD", strfapply_id, imgfbcode);
            return intR;
        }


        #region  采样
        /// <summary>
        /// 取得采样主表记录
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataTable BllSamplingMainDT(string strWhere)
        {
            strWhere = " where " + strWhere + " order by t1.fhz_id";
            string strsql = @"SELECT t1.his_recordid,t1.fapply_id,t1.fjytype_id,t1.fsample_type_id,t1.fjz_flag, t1.fcharge_flag, t1.fcharge_id, t1.fjyf, t1.fapply_user_id, t1.fapply_dept_id, 
            t1.fapply_time, t1.ftype_id, t1.fhz_id, t1.fhz_zyh, t1.fsex, t1.fname, t1.fvolk, t1.fhymen, t1.fage, t1.fjob, t1.fgms, t1.fjob_org, t1.fadd,t1.ftel, t1.fage_unit, 
            t1.fage_year, t1.fage_month, t1.fage_day, t1.fward_num, t1.froom_num, t1.fbed_num, t1.fblood_abo, t1.fblood_rh, t1.fjymd, t1.fdiagnose, t1.fheat, 
            t1.fxyld, t1.fcyy, t1.fxhdb, t1.fcreate_user_id, t1.fcreate_time, t1.fupdate_user_id, t1.fupdate_time, t1.fremark,t1.fitem_group,t2.fread_flag, t2.fsample_id,  
            t2.fsample_barcode, t2.fsample_code, t1.fstate, t2.fexamine_flag, t2.fsampling_user_id, t2.fsampling_time, t2.fsampling_remark, t2.fsend_user_id, 
            t2.fsend_user_time, t2.fget_user_id, t2.fget_user_time, t2.fget_user_remark, t2.fjy_instr, t2.fjy_date, t2.fjy_group, t2.fjy_user_id, t2.fjy_time, 
            t2.fexamine_user_id, t2.fexamine_time, t2.fhandling_user_id, t2.fhandling_user_time, t2.fhandling_remark, t2.fprint_flag, t2.fprint_time, t2.fprint_count, 
            t2.frelease_time,t2.fread_user_id,t2.fremark FROM SAM_APPLY t1 left JOIN  SAM_SAMPLE  t2 ON t1.fapply_id = t2.fapply_id " + strWhere;

            //string sql = "SELECT * FROM SAM_APPLY t1 INNER JOIN  " + this.table_SAM_SAMPLE + " t2 ON t1.fapply_id = t2.fapply_id " + strWhere;
            return SqlHelper.ExecuteDataTable(strsql);
        }

        /// <summary>
        /// 从HIS中读取可用的LIS申请
        /// 1、关联样本类型、检查类型
        /// 2、已经写SAM_APPLY表的不显示
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="ChisDBName"></param>
        /// <returns></returns>
        public DataTable BllSamplingMainDTFromHis(string strWhere, string ChisDBName)
        {
            //这个地方是和HIS对接的，可以考虑添加接口或重新调整判断方式
            //2016-10-06 10:57:24 yufh 标记
            string strsql = "select isnull(t2.分类名称,'') 分类名称,t1.* from " + ChisDBName + ".dbo.vw_lis_request_inp t1 left join vw_检验项目分类 t2 on t1.fitem_group = t2.HIS检查名称 where 1=1 ";
            if (!(string.IsNullOrWhiteSpace(strWhere)))
            {
                strsql += " and " + strWhere;
            }
            //strsql += " and not exists (select 1 from SAM_APPLY ap where t1.fapply_id = ap.fapply_id)"; //排除本地已有的申请单
            strsql += " order by t1.fhz_id,t2.分类名称,t1.his_recordid  ";
            //strsql += " and cast(his_recordid as varchar(30)) not in (select isnull(his_recordid,'') from SAM_APPLY where " + strWhere + ")";
            //string strsql = "select * from his.chis2719.dbo.vw_lis_request_inp t1 " + strWhere + "" + " order by t1.fhz_id";
            return SqlHelper.ExecuteDataTable(strsql);
        }

        public DataTable BllSamplingMainDTFromLis(string strWhere)
        {
            string strSql = "select fapply_id, fstate, ftype_id, fhz_id, fhz_zyh,his_recordid from SAM_APPLY where 1=1 ";
            if (!(string.IsNullOrEmpty(strWhere)))
            {
                strSql += " and " + strWhere;
            }

            return SqlHelper.ExecuteDataTable(strSql);
        }


        /// <summary>
        /// 结果
        /// </summary>
        /// <param name="strfapply_id"></param>
        /// <returns></returns>
        public DataTable BllSamplingResultDT(string strfapply_id)
        {
            if (string.IsNullOrWhiteSpace(strfapply_id)) return null;

            string sql = "SELECT * FROM SAM_JY_RESULT where fapply_id='" + strfapply_id + "'";
            return SqlHelper.ExecuteDataTable(sql);
        }

        public string BllSamplingSave(IList ilistfsample_id)
        {
            string strState = "2";//状态
            string struserid = LoginBLL.strPersonID;
            string strTime = this.DbServerDateTim();
            string strremark = ww.wwf.com.Net.GetHostIpAddress() + "(" + ww.wwf.com.Net.GetHostName() + ")";
            IList updateList = new ArrayList();
            for (int i = 0; i < ilistfsample_id.Count; i++)
            {
                string strid = ilistfsample_id[i].ToString();
                string strSqla = "UPDATE SAM_SAMPLE SET fstate = '" + strState + "', fsampling_user_id = '" + struserid + "', fsampling_time = '" + strTime + "', fsampling_remark = '" + strremark + "' WHERE (fsample_id = '" + strid + "')";
                updateList.Add(strSqla);
            }
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, updateList);
        }
        #endregion

        public string Get最新申请单号(string type, string yyMMdd)
        {
            string storeProc = "GetApplyNo";
            SqlParameter[] procParams = new SqlParameter[2];
            procParams[0] = new SqlParameter("@jzlx", type);
            procParams[1] = new SqlParameter("@yyMMdd", yyMMdd);
            object obj = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarByStoredProc(yunLis.wwfbll.WWFInit.strDBConn, storeProc, procParams);
            if (obj == null)
            {
                return "";
            }
            else
            {
                return obj.ToString();
            }
        }


        //状态值 SAM_TYPE表中的数据
        //0	NULL	已作废申请
        //1	NULL	已执行申请
        //2	NULL	已打印条码
        //3	NULL	已采集
        //4	NULL	已接收
        //5	NULL	已审核
        public string Update更新申请状态(string applyID, string str状态值, string updateUserID)
        {
            IList updateList = new ArrayList();
            string UpdateSamApply = "update [SAM_APPLY] set [fstate] = '" + str状态值 + "',[fupdate_user_id] = '" + updateUserID + @"',
[fupdate_time]=Convert(varchar(20),getdate(),20) where [fapply_id] = '" + applyID + "'";
            updateList.Add(UpdateSamApply);

            string UpdateSamSample = "update SAM_SAMPLE set fstate = '" + str状态值 + "' where [fapply_id] = '" + applyID + "'";
            updateList.Add(UpdateSamSample);

            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, updateList);

        }

        public DataTable Get申请单列表(string recorderID)
        {
            string strSql = "select fapply_id, fstate, ftype_id, fhz_id, fhz_zyh,his_recordid,fapply_dept_id,fsex,fname,(select top 1 fjy_id from SAM_JY  where SAM_JY.fapply_id = SAM_APPLY.fapply_id ) as fjy_id,fitem_group from SAM_APPLY where 1=1 ";
            if (!(string.IsNullOrEmpty(recorderID)))
            {
                strSql += " and his_recordid='" + recorderID + "' ";
            }

            return SqlHelper.ExecuteDataTable(strSql);
        }

        public DataTable Get化验明细(string strfjy_id)
        {
            if (string.IsNullOrWhiteSpace(strfjy_id)) return null;

            string sql = "SELECT * FROM SAM_JY_RESULT where fjy_id='" + strfjy_id + "'";
            return SqlHelper.ExecuteDataTable(sql);
        }
        /// <summary>
        /// 扫码退费
        /// </summary>
        /// <param name="ilistfsample_id"></param>
        /// <param name="zyh"></param>
        /// <returns></returns>
        public string BllDoRefund(IList ilistfsample_id, string zyh)
        {
            string struserid = LoginBLL.strPersonID;
            string strTime = this.DbServerDateTim();
            string strremark = ww.wwf.com.Net.GetHostIpAddress() + "(" + ww.wwf.com.Net.GetHostName() + ")";
            IList updateList = new ArrayList();
            for (int i = 0; i < ilistfsample_id.Count; i++)
            {
                string strid = ilistfsample_id[i].ToString();
                string strProc = @"exec dbo.uSp_化验项目计费 '1','" + struserid + "','" + strTime + "',3,'" + strid + "','" + zyh + "',1 ";
                updateList.Add(strProc);
            }
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strCHisDBConn, updateList);
        }

    }
}
