using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using yunLis.dao;
using yunLis.wwfbll;
using System.Collections;
namespace yunLis.lisbll.sam
{
    /// <summary>
    /// 
    /// </summary>
    public class SampleTypeBLL : DAOWWF
    {
        public SampleTypeBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }


        public DataTable BllDTByCode(string strfcode, string strfname)
        {
            string sql = "SELECT * FROM SAM_SAMPLE_TYPE where  (fsample_type_id='" + strfcode + "' or fname ='" + strfname + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        public DataTable BllDT(string strWhere)
        {
            string sql = "SELECT * FROM sam_sample_type " + strWhere + " ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
            
        }
        public string BllSampleNameByID(string strfsample_type_id)
        {
            return (String)yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn, "SELECT fname FROM SAM_SAMPLE_TYPE WHERE (fsample_type_id = '" + strfsample_type_id + "')");
        }
        /// <summary>
        /// 生成助 符
        /// </summary>
        /// <param name="lisSql"></param>
        /// <returns></returns>
        public string BllPersonfhelp_codeUpdate(IList lisSql)
        {
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, lisSql);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int BllAdd(DataRow drRow)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into sam_sample_type(");
            strSql.Append("fsample_type_id,fp_id,fcode,fname,fname_e,fuse_if,forder_by,fremark,fhelp_code");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + drRow["fsample_type_id"]+"',");
            strSql.Append("'" + drRow["fp_id"]+"',");
            strSql.Append("'" + drRow["fcode"]+"',");
            strSql.Append("'" + drRow["fname"]+"',");
            strSql.Append("'" + drRow["fname_e"]+"',");
            strSql.Append("" + drRow["fuse_if"]+",");
            strSql.Append("'" + drRow["forder_by"]+"',");
            strSql.Append("'" + drRow["fremark"]+"',");
            strSql.Append("'" + drRow["fhelp_code"]+"'");
            strSql.Append(")");
           return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int BllUpdate(DataRow drRow)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update sam_sample_type set ");
            strSql.Append("fp_id='" + drRow["fp_id"]+"',");
            strSql.Append("fcode='" + drRow["fcode"]+"',");
            strSql.Append("fname='" + drRow["fname"]+"',");
            strSql.Append("fname_e='" + drRow["fname_e"]+"',");
            strSql.Append("fuse_if=" + drRow["fuse_if"]+",");
            strSql.Append("forder_by='" + drRow["forder_by"]+"',");
            strSql.Append("fremark='" + drRow["fremark"]+"',");
            strSql.Append("fhelp_code='" + drRow["fhelp_code"]+"'");
            strSql.Append(" where fsample_type_id='" + drRow["fsample_type_id"]+"'");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, strSql.ToString());
        }
    }
}
