﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace yunLis.lisbll.sam
{
    [Serializable]
    public class WJZ_reportmodel
    {
        /// <summary>
        /// fdj_id
        /// </summary>
        [DisplayName("fdj_id")]
        public int fdj_id { get; set; }

        /// <summary>
        /// 报告登记日期
        /// </summary>
        [DisplayName("报告日期")]
        public string fdj_date { get; set; }

        /// <summary>
        /// 患者姓名
        /// </summary>
        [DisplayName("患者姓名")]
        public string fhz_name { get; set; }

        /// <summary>
        /// 患者性别
        /// </summary>
        [DisplayName("患者性别")]
        public string fhz_sex { get; set; }

        /// <summary>
        /// 患者年龄
        /// </summary>
        [DisplayName("患者年龄")]
        public string fhz_age { get; set; }

        /// <summary>
        /// 年龄单位
        /// </summary>
        [DisplayName("年龄单位")]
        public string fhz_age_unit { get; set; }

        /// <summary>
        /// 住院号
        /// </summary>
        [DisplayName("住院号")]
        public string fhz_zyh { get; set; }

        /// <summary>
        /// 危急值内容
        /// </summary>
        [DisplayName("危急值内容")]
        public string fwjz_nr { get; set; }

        /// <summary>
        /// 报告科室
        /// </summary>
        [DisplayName("报告科室")]
        public string fbg_dept { get; set; }

        /// <summary>
        /// 报告人
        /// </summary>
        [DisplayName("报告人")]
        public string fbg_name { get; set; }

        /// <summary>
        /// 报告日期时
        /// </summary>
        [DisplayName("报告日期时")]
        public string fbg_date1 { get; set; }

        /// <summary>
        /// 报告日期分
        /// </summary>
        [DisplayName("报告日期分")]
        public string fbg_date2 { get; set; }

        /// <summary>
        /// 接收科室
        /// </summary>
        [DisplayName("接收科室")]
        public string fjs_dept { get; set; }

        /// <summary>
        /// 接收电话
        /// </summary>
        [DisplayName("接收电话")]
        public string fjs_tel { get; set; }

        /// <summary>
        /// 接收人
        /// </summary>
        [DisplayName("接收人")]
        public string fjs_name { get; set; }

        /// <summary>
        /// 接收时间时
        /// </summary>
        [DisplayName("接收时间时")]
        public string fjs_date1 { get; set; }

        /// <summary>
        /// 接收时间分
        /// </summary>
        [DisplayName("接收时间分")]
        public string fjs_date2 { get; set; }

        /// <summary>
        /// 处理措施
        /// </summary>
        [DisplayName("处理措施")]
        public string f_clcs { get; set; }

        /// <summary>
        /// 处理人
        /// </summary>
        [DisplayName("处理人")]
        public string f_clz { get; set; }

        /// <summary>
        /// 检验ID
        /// </summary>
        [DisplayName("检验ID")]
        public string fjy_id { get; set; }

    }
}
