﻿using HIS.Model;
using System;
using System.Data;
using System.Linq;
using WEISHENG.COMM;
using yunLis.lisbll.sam;
using yunLis.wwfbll;

namespace yunLis.lis.sam.Report
{
    public class bllReportNftData
    {
        //public static DataTable getDate(string strfsample_id)
        //{
        //    LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString));
        //    var sam_jy = lisEntities.SAM_JY.Where(c => c.fjy_id == strfsample_id).FirstOrDefault();
        //    var reportSetting = lisEntities.SAM_REPORT.Where(c => c.finstr_id == sam_jy.fjy_instr).FirstOrDefault();
        //    samDataSet.sam_jy_printDataTable dt打印数据 = new samDataSet.sam_jy_printDataTable();
        //    jybll bllReport = new jybll();

        //    dt打印数据.Columns.Remove("结果图1");
        //    dt打印数据.Columns.Remove("结果图2");
        //    dt打印数据.Columns.Remove("结果图3");
        //    dt打印数据.Columns.Remove("结果图4");
        //    dt打印数据.Columns.Remove("结果图5");
        //    DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));
        //    dt打印数据.Columns.Add(D1);

        //    DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
        //    dt打印数据.Columns.Add(D2);

        //    DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
        //    dt打印数据.Columns.Add(D3);

        //    DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
        //    dt打印数据.Columns.Add(D4);

        //    DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
        //    dt打印数据.Columns.Add(D5);


        //    if (dt打印数据.Columns.Contains("检验者手签照"))
        //    {
        //        dt打印数据.Columns.Remove("检验者手签照");
        //    }
        //    if (dt打印数据.Columns.Contains("审核者手签照"))
        //    {
        //        dt打印数据.Columns.Remove("审核者手签照");
        //    }

        //    DataColumn D6 = new DataColumn("检验者手签照", typeof(System.Byte[]));
        //    dt打印数据.Columns.Add(D6);

        //    DataColumn D7 = new DataColumn("审核者手签照", typeof(System.Byte[]));
        //    dt打印数据.Columns.Add(D7);

        //    DataTable dt检验数据 = null;// bllReport.get_dtSAM_JY(" fjy_id='" + strfsample_id + "'");
        //    if (dt检验数据.Rows.Count == 0)
        //    {
        //        msgHelper.ShowInformation("报告为空，操作失败！请选择报告后重试。");
        //        return null;
        //    }
        //    string str仪器结果id = "";
        //    DataTable dtIMG = new DataTable();
        //    if (dt检验数据.Rows[0]["finstr_result_id"] != null)
        //    {
        //        str仪器结果id = dt检验数据.Rows[0]["finstr_result_id"].ToString();
        //        dtIMG = bllReport.BllImgDT(strfsample_id, str仪器结果id);
        //    }

        //    DataTable dtResult = bllReport.Get_SAM_JY_RESULT(" t.fjy_id='" + strfsample_id + "'");
        //    int intResultCount = dtResult.Rows.Count;//结果记录数
        //    if (intResultCount == 0)
        //    {
        //        var itemSam_JY = lisEntities.SAM_JY.Where(c => c.fjy_id == strfsample_id).FirstOrDefault();
        //        var itemSam_Apply = lisEntities.SAM_APPLY.Where(c => c.fapply_id == itemSam_JY.fapply_id).FirstOrDefault();
        //        if (itemSam_Apply != null)
        //        {
        //            HIS.COMM.msgBalloonHelper.BalloonShow($"【{itemSam_Apply.fname}，{itemSam_Apply.fitem_group}】化验报告结果未生成，请与化验室联系确认。");
        //            return null;
        //        }
        //        HIS.COMM.msgBalloonHelper.BalloonShow($"【{itemSam_JY.fhz_name}】化验报告结果未生成，请与化验室联系确认。");
        //        return null;
        //    }

        //    //string strfinstr_id = "";//仪器ID 
        //    //if (dtReport.Rows[0]["fjy_instr"] != null) strfinstr_id = dtReport.Rows[0]["fjy_instr"].ToString();
        //    //DataTable dtInstrReport = this.bllInstr.BllReportDT(strfinstr_id, 1);
        //    //string strReportCode = "yunLis.lis.sam.Report.ReportD280A5";//默认报表代码         ReportComLittle
        //    //string strReportName = "";//报表名称
        //    //if (sam_report != null)
        //    //{
        //    //    if (!string.IsNullOrWhiteSpace(sam_report.fcode))
        //    //    {
        //    //        strReportCode = sam_report.fcode;
        //    //    }
        //    //}
        //    //string strTimeAP = "";//检验时间安排 f1
        //    //string strCYdd = "";//采样地点 f2
        //    //if (dtInstrReport.Rows.Count > 0)
        //    //{
        //    //    //strReportCode = dtInstrReport.Rows[0]["fcode"].ToString();
        //    //    strReportName = dtInstrReport.Rows[0]["fname"].ToString();
        //    //    strTimeAP = dtInstrReport.Rows[0]["f1"].ToString();
        //    //    strCYdd = dtInstrReport.Rows[0]["f2"].ToString();
        //    //}
        //    //---------结果值
        //    int int序号 = 0;
        //    for (int i = 0; i < intResultCount; i++)
        //    {
        //        int序号 = i + 1;
        //        DataRow printRow = dt打印数据.NewRow();
        //        printRow["结果序号"] = int序号;
        //        printRow["结果项目代码"] = dtResult.Rows[i]["fitem_code"];
        //        printRow["结果项目名称"] = dtResult.Rows[i]["fitem_name"];
        //        printRow["结果值"] = dtResult.Rows[i]["fvalue"];
        //        printRow["结果项目单位"] = dtResult.Rows[i]["fitem_unit"];
        //        printRow["结果标记"] = dtResult.Rows[i]["fitem_badge"];
        //        printRow["结果参考值"] = dtResult.Rows[i]["fitem_ref"];
        //        dt打印数据.Rows.Add(printRow);
        //    }

        //    //----------报告值
        //    dt打印数据.Rows[0]["医院名称"] = LoginBLL.CustomerName;
        //    dt打印数据.Rows[0]["医院地址"] = LoginBLL.CustomerAdd;
        //    dt打印数据.Rows[0]["检验实验室"] = LoginBLL.strDeptName;
        //    dt打印数据.Rows[0]["医院联系电话"] = LoginBLL.CustomerTel;
        //    dt打印数据.Rows[0]["检验时间安排"] = reportSetting.f1;  //    strTimeAP = dtInstrReport.Rows[0]["f1"].ToString();
        //    dt打印数据.Rows[0]["报表名称"] = reportSetting.fname;           //    strReportName = dtInstrReport.Rows[0]["fname"].ToString();
        //    dt打印数据.Rows[0]["采样地点"] = reportSetting.f2;//    strCYdd = dtInstrReport.Rows[0]["f2"].ToString();
        //    dt打印数据.Rows[0]["姓名"] = dt检验数据.Rows[0]["fhz_name"];
        //    //dtPrintDataTable.Rows[0]["性别"] = dtReport.Rows[0]["fhz_sex"];
        //    dt打印数据.Rows[0]["性别"] = dt检验数据.Rows[0]["性别"];
        //    dt打印数据.Rows[0]["住院号"] = dt检验数据.Rows[0]["fhz_zyh"];
        //    dt打印数据.Rows[0]["年龄"] = dt检验数据.Rows[0]["fhz_age"];
        //    dt打印数据.Rows[0]["年龄单位"] = dt检验数据.Rows[0]["年龄单位"];
        //    dt打印数据.Rows[0]["申请单号"] = dt检验数据.Rows[0]["fapply_id"];
        //    dt打印数据.Rows[0]["样本条码号"] = dt检验数据.Rows[0]["fapply_id"];
        //    dt打印数据.Rows[0]["样本号"] = dt检验数据.Rows[0]["fjy_yb_code"];
        //    dt打印数据.Rows[0]["检验类别"] = dt检验数据.Rows[0]["类型"];
        //    dt打印数据.Rows[0]["样本类别"] = dt检验数据.Rows[0]["样本"];
        //    dt打印数据.Rows[0]["送检科室"] = dt检验数据.Rows[0]["科室"];
        //    dt打印数据.Rows[0]["申请者"] = dt检验数据.Rows[0]["申请医师"];
        //    dt打印数据.Rows[0]["检验者"] = dt检验数据.Rows[0]["检验医师"];
        //    //if (dtReport.Rows[0]["fjy_user_id"].ToString() != "")
        //    //{
        //    //    s1 = Convert.ToInt32(dtReport.Rows[0]["fjy_user_id"]);
        //    //}
        //    //else
        //    //{
        //    //}
        //    if (HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认检验者编码", "", "") == "")
        //    {
        //        msgHelper.ShowInformation("LIS默认检验者编码未设置");
        //        return null;
        //    }
        //    var img1 = HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认检验者编码", "", ""))).FirstOrDefault().手签照片;
        //    if (img1 == null)
        //    {
        //        msgHelper.ShowInformation("LIS默认检验者手签照片未设置");
        //        return null;
        //    }
        //    dt打印数据.Rows[0]["检验者手签照"] = img1;
        //    dt打印数据.Rows[0]["审核者"] = dt检验数据.Rows[0]["审核者"];
        //    //int s2;
        //    //if (dtReport.Rows[0]["fchenk_user_id"].ToString() != "")
        //    //{
        //    //    s2 = Convert.ToInt32(dtReport.Rows[0]["fchenk_user_id"]);
        //    //}
        //    //else
        //    //{
        //    //s2 = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认核对者编码", "", ""));
        //    //}
        //    if (HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认核对者编码", "", "") == "")
        //    {
        //        msgHelper.ShowInformation("LIS默认核对者编码未设置");
        //        return null;
        //    }
        //    var img2 = HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认核对者编码", "", ""))).FirstOrDefault().手签照片;
        //    if (img2 == null)
        //    {
        //        msgHelper.ShowInformation("LIS默认核对者手签照片未设置");
        //        return null;
        //    }
        //    dt打印数据.Rows[0]["审核者手签照"] = img2;
        //    dt打印数据.Rows[0]["床号"] = dt检验数据.Rows[0]["fhz_bed"];
        //    dt打印数据.Rows[0]["病人ID"] = dt检验数据.Rows[0]["fhz_id"];
        //    //开始2015-06-03修改疾病名称的获取方式
        //    //dtPrintDataTable.Rows[0]["诊断"] = dtReport.Rows[0]["fjy_lczd"];
        //    dt打印数据.Rows[0]["诊断"] = dt检验数据.Rows[0]["疾病名称"].ToString();
        //    //结束修改
        //    dt打印数据.Rows[0]["申请时间"] = dt检验数据.Rows[0]["fapply_time"];
        //    dt打印数据.Rows[0]["采样时间"] = dt检验数据.Rows[0]["fsampling_time"];
        //    //dtPrintDataTable.Rows[0]["收样时间"] = dtReport.Rows[0]["fget_user_time"];
        //    try
        //    {
        //        //if (dtReport.Rows[0]["fsys_time"].ToString().Equals("")) { }
        //        //else
        //        //{
        //        //    dtPrintDataTable.Rows[0]["检验时间"] = Convert.ToDateTime(dtReport.Rows[0]["fsys_time"].ToString()).ToString("MM-dd HH:mm");
        //        //}
        //        dt打印数据.Rows[0]["检验时间"] = dt检验数据.Rows[0]["fjy_date"].ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    try
        //    {
        //        if (dt检验数据.Rows[0]["freport_time"].ToString().Equals("")) { }
        //        else
        //        {
        //            //dtPrintDataTable.Rows[0]["审核时间"] = Convert.ToDateTime(dtReport.Rows[0]["fcheck_time"].ToString()).ToString("MM-dd HH:mm");
        //            dt打印数据.Rows[0]["审核时间"] = System.Convert.ToDateTime(dt检验数据.Rows[0]["freport_time"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    if (dt检验数据.Rows[0]["fprint_time"].ToString().Equals(""))
        //    {
        //        dt打印数据.Rows[0]["打印时间"] = DateTime.Now.ToString("MM-dd HH:mm");//dtReport.Rows[0]["fprint_time"];
        //    }
        //    else
        //    {
        //        try
        //        {
        //            dt打印数据.Rows[0]["打印时间"] = Convert.ToDateTime(dt检验数据.Rows[0]["fprint_time"].ToString()).ToString("MM-dd HH:mm");
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }

        //    dt打印数据.Rows[0]["备注"] = dt检验数据.Rows[0]["fremark"];
        //    //dsPrint.Tables.Add(dt打印数据);
        //    //----------图值
        //    if (dtIMG.Rows.Count > 0)
        //    {
        //        for (int iimg = 0; iimg < dtIMG.Rows.Count; iimg++)
        //        {
        //            switch (iimg)
        //            {
        //                case 0:
        //                    dt打印数据.Rows[0]["结果图1"] = dtIMG.Rows[iimg]["FImg"];
        //                    dt打印数据.Rows[0]["结果图1_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
        //                    break;
        //                case 1:
        //                    dt打印数据.Rows[0]["结果图2"] = dtIMG.Rows[iimg]["FImg"];
        //                    dt打印数据.Rows[0]["结果图2_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
        //                    break;
        //                case 2:
        //                    dt打印数据.Rows[0]["结果图3"] = dtIMG.Rows[iimg]["FImg"];
        //                    dt打印数据.Rows[0]["结果图3_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
        //                    break;
        //                case 3:
        //                    dt打印数据.Rows[0]["结果图4"] = dtIMG.Rows[iimg]["FImg"];
        //                    dt打印数据.Rows[0]["结果图4_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
        //                    break;
        //                case 4:
        //                    dt打印数据.Rows[0]["结果图5"] = dtIMG.Rows[iimg]["FImg"];
        //                    dt打印数据.Rows[0]["结果图5_名称"] = dtIMG.Rows[iimg]["FImgNmae"];
        //                    break;
        //                default:
        //                    break;
        //            }
        //        }
        //    }
        //    return dt打印数据;
        //}
    }
}
