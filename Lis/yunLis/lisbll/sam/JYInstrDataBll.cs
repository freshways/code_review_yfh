using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using yunLis.dao;
using yunLis.wwfbll;
using System.Collections;
using System.Data.SqlClient;

namespace yunLis.lisbll.sam
{
    /// <summary>
    ///接口业务逻辑
    /// </summary>
    /// <remarks>
    /// 作者：tao
    /// 时间：2008.2
    /// 改动：[tao；2008.2]
    /// </remarks>
    public class JYInstrDataBll : DAOWWF
    {

        public JYInstrDataBll()
        {
            ///
            /// TODO: 在此处添加构造函数逻辑
            ///     

        }



        #region  仪器接口 数据
        /// <summary>
        /// 取得接口表
        /// </summary>
        /// <returns></returns>
        public DataTable dt_Lis_Ins_Result(string strWhere)
        {
            string strSql = "SELECT * FROM Lis_Ins_Result " + strWhere;
            return SqlHelper.ExecuteDataTable(strSql);
        }

        /// <summary>
        /// 仪器原始数据新增
        /// </summary>
        /// <param name="drRowIO">接口原始数据行</param>
        /// <param name="dtImg">接口图</param>
        /// <param name="dtResult">译后结果</param>
        /// <returns></returns>
        public string BllIOAdd(DataRow drRowIO, DataTable dtImg, DataTable dtResult)
        {
            IList sqlList = new ArrayList();
            StringBuilder strSqlIO = new StringBuilder();
            strSqlIO.Append("insert into SAM_INSTR_IO(");
            strSqlIO.Append("fio_id,finstr_id,fdate,fsample_code,fsam_type_id,fjz,fqc,fstate,fcontent,ftime,fremark");
            strSqlIO.Append(")");
            strSqlIO.Append(" values (");
            strSqlIO.Append("'" + drRowIO["fio_id"] + "',");
            strSqlIO.Append("'" + drRowIO["finstr_id"] + "',");
            strSqlIO.Append("'" + drRowIO["fdate"] + "',");
            strSqlIO.Append("'" + drRowIO["fsample_code"] + "',");
            strSqlIO.Append("'" + drRowIO["fsam_type_id"] + "',");
            strSqlIO.Append("" + drRowIO["fjz"] + ",");
            strSqlIO.Append("" + drRowIO["fqc"] + ",");
            strSqlIO.Append("'" + drRowIO["fstate"] + "',");
            strSqlIO.Append("'" + drRowIO["fcontent"] + "',");
            strSqlIO.Append("'" + drRowIO["ftime"] + "',");
            strSqlIO.Append("'" + drRowIO["fremark"] + "'");
            strSqlIO.Append(")");

            sqlList.Add(strSqlIO.ToString());
            for (int i = 0; i < dtImg.Rows.Count; i++)
            {
                DataRow drRowImg = dtImg.Rows[i];
                StringBuilder strSqlImg = new StringBuilder();
                strSqlImg.Append("insert into SAM_INSTR_IO_IMG(");
                strSqlImg.Append("fimg_id,fio_id,fimg_type,fimg,forder_by,fremark");
                strSqlImg.Append(")");
                strSqlImg.Append(" values (");
                strSqlImg.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");
                strSqlImg.Append("'" + drRowImg["fio_id"] + "',");
                strSqlImg.Append("'" + drRowImg["fimg_type"] + "',");
                strSqlImg.Append("" + drRowImg["fimg"] + ",");
                strSqlImg.Append("'" + drRowImg["forder_by"] + "',");
                strSqlImg.Append("'" + drRowImg["fremark"] + "'");
                strSqlImg.Append(")");
                sqlList.Add(strSqlImg.ToString());
            }

            for (int ii = 0; ii < dtResult.Rows.Count; ii++)
            {
                DataRow drResult = dtResult.Rows[ii];
                StringBuilder strSqlResult = new StringBuilder();
                strSqlResult.Append("insert into SAM_INSTR_IO_RESULT(");
                strSqlResult.Append("fresult_id,fio_id,fitem_code,fvalue,fod_value,fcutoff_value,fstate,ftime,fremark");
                strSqlResult.Append(")");
                strSqlResult.Append(" values (");
                strSqlResult.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");
                strSqlResult.Append("'" + drResult["fio_id"] + "',");
                strSqlResult.Append("'" + drResult["fitem_code"] + "',");
                strSqlResult.Append("'" + drResult["fvalue"] + "',");
                strSqlResult.Append("'" + drResult["fod_value"] + "',");
                strSqlResult.Append("'" + drResult["fcutoff_value"] + "',");
                strSqlResult.Append("'" + drResult["fstate"] + "',");
                strSqlResult.Append("'" + drResult["ftime"] + "',");
                strSqlResult.Append("'" + drResult["fremark"] + "'");
                strSqlResult.Append(")");
                sqlList.Add(strSqlResult.ToString());
            }
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, sqlList);
        }
        #endregion

        public string BllODel(string FTaskID)
        {
            IList list = new ArrayList();
            string value = string.Concat(new string[]
            {
                "delete from Lis_Ins_Result_Img where FTaskID = '",
                FTaskID,
                "' \r\n                                        delete from Lis_Ins_Result_Detail where FTaskID = '",
                FTaskID,
                "'\r\n                                        delete from Lis_Ins_Result where FTaskID = '",
                FTaskID,
                "'"
            });
            list.Add(value);
            return WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, list);
        }

        #region  仪器接口 图
        /// <summary>
        /// 取得图表
        /// </summary>
        /// <returns></returns>
        public DataTable BllImgDT(string strTaskId)
        {
            string sql = "SELECT * FROM Lis_Ins_Result_Img WHERE (FTaskID = '" + strTaskId + "') ORDER BY FOrder";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql);
        }
        #endregion

    

     

        public string GetInsResultID(string str日期, string str仪器ID, int int样本号)
        {
            string strSql = @"SELECT [FTaskID],[FInstrID],[FResultID],[FDateTime] FROM [Lis_Ins_Result]
            where FDateTime >= @testDate and FDateTime < CAST(@testDate as datetime)+1 and FInstrID=@instrID and FResultID=@resultid";

            SqlParameter[] arrParams = new SqlParameter[3];
            arrParams[0] = new SqlParameter("@testDate", str日期);
            arrParams[1] = new SqlParameter("@instrID", str仪器ID);
            arrParams[2] = new SqlParameter("@resultid", int样本号);

            DataSet ds = null;
            string strReturn = null;

            ds = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, strSql, arrParams);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                strReturn = ds.Tables[0].Rows[0]["FTaskID"].ToString();
            }
            else
            {
                strReturn = null;
            }
            return strReturn;

        }

    }
}
