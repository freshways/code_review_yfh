﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using yunLis.lisbll.sam;
using yunLis.wwfbll;
namespace yunLis.lis.com
{
    public partial class InstrDrvieForm : Form
    {
        InstrBLL bllInstr = new InstrBLL();
        private WindowsResult r;
        string strfinstr_id = "";//仪器ID
        DataTable dtDrive = new DataTable();
        string strfdrive_id = "";//驱动ID

        public InstrDrvieForm()
        {
            InitializeComponent();           
        }


        public InstrDrvieForm(WindowsResult r, string finstr_id)
            : this()
        {

            this.r = r;
            this.strfinstr_id = finstr_id;
           // this.strfinstr_id = finstr_id;
          //  this.strfinstr_name = iname;

        }

        private void InstrDrvieForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.strfdrive_id = "";
              //  MessageBox.Show("仪器ID为：" + strfinstr_id);
                this.dtDrive = this.bllInstr.BllDriveDT(strfinstr_id);
               
                if (this.dtDrive.Rows.Count > 0)
                {
                    this.strfdrive_id = dtDrive.Rows[0]["fdrive_id"].ToString();
                }
                else
                {
                    this.strfdrive_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                    DataRow drDr = this.dtDrive.NewRow();
                    drDr["fdrive_id"] = strfdrive_id;
                    drDr["finstr_id"] = strfinstr_id;

                    drDr["fport"] = "COM1";//端口:COM1COM2COM3COM4COM5COM6COM7COM8                   
                    drDr["fbaud_rate"] = "9600";//波特率;300 600 1200 2400 4800 9600 14400 28800 36000 115000
                    drDr["fdata_bit"] = "8";//数据位 7 8 9
                    drDr["fstop_bit"] = "One";//停止位 1 2 3
                    drDr["fparity"] = "None";//校验:None 无校验 Even 奇校验 Odd 偶校验
                    drDr["finfo_mode"] = "Text";//通信模式 Text Hex
                    drDr["freadbufferSize"] = "1024";//输入缓冲区 512 1024 2048 4096 8192
                    drDr["fwritebufferSize"] = "8192";//输出缓冲区
                    drDr["freadtimeout"] = "3";//读取超时之前的毫秒数
                    drDr["fwritetimeout"] = "3";//写入超时之前的毫秒数

                    drDr["fjz_flag"] = "0";
                    drDr["fimg_flag"] = "0"; 
                   
                    this.dtDrive.Rows.Add(drDr);
                    this.bllInstr.BllDriveAdd(drDr);
                }
                this.sAM_INSTR_DRIVEBindingSource.DataSource = this.dtDrive;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /*
          // 摘要:
    //     指定在 System.IO.Ports.SerialPort 对象上使用的停止位数。
    public enum StopBits
    {
        // 摘要:
        //     必使用停止位。
        None = 0,
        //
        // 摘要:
        //     使用一个停止位。
        One = 1,
        //
        // 摘要:
        //     使用两个停止位。
        Two = 2,
        //
        // 摘要:
        //     使用 1.5 个停止位。
        OnePointFive = 3,
    }
         */

      

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.sAM_INSTR_DRIVEBindingSource.EndEdit();
                // dataGridView1.DataSource = this.dtDrive;
                this.bllInstr.BllDriveUpdate(this.dtDrive.Rows[0]);
                r.ChangeTextString("ok");
                this.Close();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

    }
}