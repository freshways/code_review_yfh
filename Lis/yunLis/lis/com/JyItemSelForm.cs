﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using yunLis.lisbll.sam;
namespace yunLis.lis.com
{
    public partial class JyItemSelForm : Form
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        public string strfinstr_id = "";//仪器ＩＤ
        public string strfinstr_name = "";//仪器名称

        //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目▽
        public string strDate = "";
        public string str样本号 = "";
        //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目△

        public  IList selItemList = null;       

        public JyItemSelForm()
        {
            InitializeComponent();
            DataGridViewObject.AutoGenerateColumns = false;
        }
        
        
        private void ItemSelectForm_Load(object sender, EventArgs e)
        {
            this.Text = this.Text +" "+ strfinstr_name;
            //labelYQ.Text = this.strfinstr_name;
            GetItemDT(strfinstr_id,"");
        }

        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
        /// <param name="strInstrID"></param>
        private void GetItemDT(string strInstrID, string fhelp_code)
        {
            try
            {
                //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目▽
                #region old code
                //this.sam_itemBindingSource.DataSource = this.bllItem.BllItem(1, strInstrID, fhelp_code);
                #endregion
                if(string.IsNullOrWhiteSpace(strDate) || string.IsNullOrWhiteSpace(str样本号))
                {
                    this.sam_itemBindingSource.DataSource = this.bllItem.BllItem(1, strInstrID, fhelp_code);
                }
                else
                {
                    this.sam_itemBindingSource.DataSource = this.bllItem.BllItem(1, strInstrID, fhelp_code, strDate, str样本号);
                }
                //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目△

                this.DataGridViewObject.DataSource = sam_itemBindingSource;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 过滤
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            GetItemDT(strfinstr_id, this.textBox1.Text);
        }
       
      
        private void GelSelValueList()
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                selItemList = new ArrayList();
                string strfitem_id = "";
                Object objSel = null;
                int intSelValue = 0;

                for (int i = 0; i < this.DataGridViewObject.Rows.Count; i++)
                {
                    if (this.DataGridViewObject.Rows[i].Cells["fitem_id"].Value != null)
                        strfitem_id = this.DataGridViewObject.Rows[i].Cells["fitem_id"].Value.ToString();
                    objSel = this.DataGridViewObject.Rows[i].Cells["fselect"].Value;
                    if (objSel != null)
                        intSelValue = Convert.ToInt32(objSel);
                    else
                        intSelValue = 0;

                    if (intSelValue == 1)
                    {
                        this.selItemList.Add(strfitem_id);
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        

      
        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                GelSelValueList();
              
                this.Close();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

  

        private void DataGridViewObject_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    try
                    {
                        GelSelValueList();
                       
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
                    }

                }
                catch (Exception ex)
                {
                    yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

        private void DataGridViewObject_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                selItemList = new ArrayList();
                string strfitem_id = "";
               
                if (this.DataGridViewObject.Rows.Count > 0)
                {
                    strfitem_id = this.DataGridViewObject.CurrentRow.Cells["fitem_id"].Value.ToString();
                    this.selItemList.Add(strfitem_id);
                }
                
                this.Close();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonAll_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                for (int i = 0; i < this.DataGridViewObject.Rows.Count; i++)
                {
                    this.DataGridViewObject.Rows[i].Cells["fselect"].Value = "1";
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonNO1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                for (int i = 0; i < this.DataGridViewObject.Rows.Count; i++)
                {
                    this.DataGridViewObject.Rows[i].Cells["fselect"].Value = "0";
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
      
    }
}