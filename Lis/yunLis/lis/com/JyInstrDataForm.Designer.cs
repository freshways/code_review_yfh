﻿namespace yunLis.lis.com
{
    partial class JyInstrDataForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JyInstrDataForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingSourceIO = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQ = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorOK = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonNo = new System.Windows.Forms.ToolStripButton();
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.FResultID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTransFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FValidity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FInstrID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTaskID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceRelust = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.fitem_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FCutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.checkBox未读取 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.fsample_codeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxImg = new System.Windows.Forms.GroupBox();
            this.l_img_5 = new System.Windows.Forms.Label();
            this.l_img_4 = new System.Windows.Forms.Label();
            this.l_img_3 = new System.Windows.Forms.Label();
            this.l_img_2 = new System.Windows.Forms.Label();
            this.l_img_1 = new System.Windows.Forms.Label();
            this.img5 = new System.Windows.Forms.PictureBox();
            this.contextMenuStripimg1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.导入图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清空图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.图片浏览ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lIS_REPORT_IMGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new yunLis.lis.sam.samDataSet();
            this.img4 = new System.Windows.Forms.PictureBox();
            this.img3 = new System.Windows.Forms.PictureBox();
            this.img2 = new System.Windows.Forms.PictureBox();
            this.img1 = new System.Windows.Forms.PictureBox();
            fsample_codeLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceRelust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            this.groupBoxif.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxImg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img5)).BeginInit();
            this.contextMenuStripimg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).BeginInit();
            this.SuspendLayout();
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(419, 22);
            fsample_codeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(60, 15);
            fsample_codeLabel.TabIndex = 21;
            fsample_codeLabel.Text = "样本号:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(16, 22);
            fjy_dateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(75, 15);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.BindingSource = this.bindingSourceIO;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonQ,
            this.bindingNavigatorOK,
            this.toolStripButtonNo});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 588);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(1056, 44);
            this.bindingNavigator1.TabIndex = 126;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingSourceIO
            // 
            this.bindingSourceIO.PositionChanged += new System.EventHandler(this.bindingSourceIO_PositionChanged);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(38, 41);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 44);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(65, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 44);
            // 
            // toolStripButtonQ
            // 
            this.toolStripButtonQ.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQ.Image")));
            this.toolStripButtonQ.Name = "toolStripButtonQ";
            this.toolStripButtonQ.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQ.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQ.Size = new System.Drawing.Size(97, 41);
            this.toolStripButtonQ.Text = "查 询(&Q)  ";
            this.toolStripButtonQ.Click += new System.EventHandler(this.toolStripButtonQ_Click);
            // 
            // bindingNavigatorOK
            // 
            this.bindingNavigatorOK.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorOK.Image")));
            this.bindingNavigatorOK.Name = "bindingNavigatorOK";
            this.bindingNavigatorOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.bindingNavigatorOK.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorOK.Size = new System.Drawing.Size(123, 41);
            this.bindingNavigatorOK.Text = "选择样本(&O)  ";
            this.bindingNavigatorOK.Click += new System.EventHandler(this.bindingNavigatorOK_Click);
            // 
            // toolStripButtonNo
            // 
            this.toolStripButtonNo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNo.Image")));
            this.toolStripButtonNo.Name = "toolStripButtonNo";
            this.toolStripButtonNo.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonNo.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonNo.Size = new System.Drawing.Size(95, 41);
            this.toolStripButtonNo.Text = "关 闭(&C)  ";
            this.toolStripButtonNo.Click += new System.EventHandler(this.toolStripButtonNo_Click);
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToDeleteRows = false;
            this.dataGridViewReport.AllowUserToOrderColumns = true;
            this.dataGridViewReport.AllowUserToResizeColumns = false;
            this.dataGridViewReport.AllowUserToResizeRows = false;
            this.dataGridViewReport.AutoGenerateColumns = false;
            this.dataGridViewReport.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FResultID,
            this.FDateTime,
            this.FTransFlag,
            this.FValidity,
            this.FInstrID,
            this.FType,
            this.FTaskID});
            this.dataGridViewReport.DataSource = this.bindingSourceIO;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReport.Location = new System.Drawing.Point(0, 54);
            this.dataGridViewReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewReport.MultiSelect = false;
            this.dataGridViewReport.Name = "dataGridViewReport";
            this.dataGridViewReport.ReadOnly = true;
            this.dataGridViewReport.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridViewReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.RowHeadersWidth = 20;
            this.dataGridViewReport.RowTemplate.Height = 23;
            this.dataGridViewReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReport.Size = new System.Drawing.Size(452, 534);
            this.dataGridViewReport.TabIndex = 131;
            this.dataGridViewReport.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewReport_DataError);
            this.dataGridViewReport.DoubleClick += new System.EventHandler(this.dataGridViewReport_DoubleClick);
            // 
            // FResultID
            // 
            this.FResultID.DataPropertyName = "FResultID";
            this.FResultID.HeaderText = "样本号";
            this.FResultID.Name = "FResultID";
            this.FResultID.ReadOnly = true;
            this.FResultID.Width = 65;
            // 
            // FDateTime
            // 
            this.FDateTime.DataPropertyName = "FDateTime";
            this.FDateTime.HeaderText = "日期";
            this.FDateTime.Name = "FDateTime";
            this.FDateTime.ReadOnly = true;
            this.FDateTime.Width = 115;
            // 
            // FTransFlag
            // 
            this.FTransFlag.DataPropertyName = "FTransFlag";
            this.FTransFlag.HeaderText = "处理否";
            this.FTransFlag.Name = "FTransFlag";
            this.FTransFlag.ReadOnly = true;
            this.FTransFlag.Visible = false;
            // 
            // FValidity
            // 
            this.FValidity.DataPropertyName = "FValidity";
            this.FValidity.HeaderText = "有效否";
            this.FValidity.Name = "FValidity";
            this.FValidity.ReadOnly = true;
            this.FValidity.Visible = false;
            // 
            // FInstrID
            // 
            this.FInstrID.DataPropertyName = "FInstrID";
            this.FInstrID.HeaderText = "仪器";
            this.FInstrID.Name = "FInstrID";
            this.FInstrID.ReadOnly = true;
            this.FInstrID.Width = 130;
            // 
            // FType
            // 
            this.FType.DataPropertyName = "FType";
            this.FType.HeaderText = "类型";
            this.FType.Name = "FType";
            this.FType.ReadOnly = true;
            this.FType.Visible = false;
            // 
            // FTaskID
            // 
            this.FTaskID.DataPropertyName = "FTaskID";
            this.FTaskID.HeaderText = "结果id";
            this.FTaskID.Name = "FTaskID";
            this.FTaskID.ReadOnly = true;
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToResizeColumns = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.AutoGenerateColumns = false;
            this.dataGridViewResult.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_name,
            this.FValue,
            this.FOD,
            this.FCutoff,
            this.FItem,
            this.fitem_id});
            this.dataGridViewResult.DataSource = this.bindingSourceRelust;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResult.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewResult.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewResult.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            this.dataGridViewResult.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidth = 30;
            this.dataGridViewResult.RowTemplate.Height = 23;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewResult.ShowCellToolTips = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.Size = new System.Drawing.Size(604, 382);
            this.dataGridViewResult.TabIndex = 132;
            this.dataGridViewResult.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewResult_DataError);
            // 
            // fitem_name
            // 
            this.fitem_name.DataPropertyName = "fitem_name";
            this.fitem_name.HeaderText = "检验项目";
            this.fitem_name.Name = "fitem_name";
            this.fitem_name.ReadOnly = true;
            this.fitem_name.Width = 140;
            // 
            // FValue
            // 
            this.FValue.DataPropertyName = "FValue";
            this.FValue.HeaderText = "值";
            this.FValue.Name = "FValue";
            this.FValue.ReadOnly = true;
            this.FValue.Width = 70;
            // 
            // FOD
            // 
            this.FOD.DataPropertyName = "FOD";
            this.FOD.HeaderText = "OD";
            this.FOD.Name = "FOD";
            this.FOD.ReadOnly = true;
            this.FOD.Width = 70;
            // 
            // FCutoff
            // 
            this.FCutoff.DataPropertyName = "FCutoff";
            this.FCutoff.HeaderText = "Cutoff";
            this.FCutoff.Name = "FCutoff";
            this.FCutoff.ReadOnly = true;
            this.FCutoff.Width = 80;
            // 
            // FItem
            // 
            this.FItem.DataPropertyName = "FItem";
            this.FItem.HeaderText = "代码";
            this.FItem.Name = "FItem";
            this.FItem.ReadOnly = true;
            this.FItem.Width = 70;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.ReadOnly = true;
            this.fitem_id.Visible = false;
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(this.checkBox未读取);
            this.groupBoxif.Controls.Add(this.button1);
            this.groupBoxif.Controls.Add(this.fsample_codeTextBox);
            this.groupBoxif.Controls.Add(fsample_codeLabel);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker2);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker1);
            this.groupBoxif.Controls.Add(fjy_dateLabel);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 0);
            this.groupBoxif.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxif.Size = new System.Drawing.Size(1056, 54);
            this.groupBoxif.TabIndex = 130;
            this.groupBoxif.TabStop = false;
            // 
            // checkBox未读取
            // 
            this.checkBox未读取.AutoSize = true;
            this.checkBox未读取.Location = new System.Drawing.Point(619, 20);
            this.checkBox未读取.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBox未读取.Name = "checkBox未读取";
            this.checkBox未读取.Size = new System.Drawing.Size(89, 19);
            this.checkBox未读取.TabIndex = 24;
            this.checkBox未读取.Text = "未读取过";
            this.checkBox未读取.UseVisualStyleBackColor = true;
            this.checkBox未读取.CheckedChanged += new System.EventHandler(this.checkBox状态_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(725, 16);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 29);
            this.button1.TabIndex = 22;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fsample_codeTextBox
            // 
            this.fsample_codeTextBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.fsample_codeTextBox.Location = new System.Drawing.Point(492, 18);
            this.fsample_codeTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fsample_codeTextBox.Name = "fsample_codeTextBox";
            this.fsample_codeTextBox.Size = new System.Drawing.Size(115, 25);
            this.fsample_codeTextBox.TabIndex = 20;
            this.fsample_codeTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsample_codeTextBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(249, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(275, 18);
            this.fjy_dateDateTimePicker2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(132, 25);
            this.fjy_dateDateTimePicker2.TabIndex = 13;
            this.fjy_dateDateTimePicker2.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker2_CloseUp);
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(105, 18);
            this.fjy_dateDateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(132, 25);
            this.fjy_dateDateTimePicker1.TabIndex = 11;
            this.fjy_dateDateTimePicker1.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker1_CloseUp);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(448, 54);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 534);
            this.splitter1.TabIndex = 133;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridViewResult);
            this.panel1.Controls.Add(this.groupBoxImg);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(452, 54);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(604, 534);
            this.panel1.TabIndex = 134;
            // 
            // groupBoxImg
            // 
            this.groupBoxImg.Controls.Add(this.l_img_5);
            this.groupBoxImg.Controls.Add(this.l_img_4);
            this.groupBoxImg.Controls.Add(this.l_img_3);
            this.groupBoxImg.Controls.Add(this.l_img_2);
            this.groupBoxImg.Controls.Add(this.l_img_1);
            this.groupBoxImg.Controls.Add(this.img5);
            this.groupBoxImg.Controls.Add(this.img4);
            this.groupBoxImg.Controls.Add(this.img3);
            this.groupBoxImg.Controls.Add(this.img2);
            this.groupBoxImg.Controls.Add(this.img1);
            this.groupBoxImg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxImg.Location = new System.Drawing.Point(0, 382);
            this.groupBoxImg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxImg.Name = "groupBoxImg";
            this.groupBoxImg.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxImg.Size = new System.Drawing.Size(604, 152);
            this.groupBoxImg.TabIndex = 133;
            this.groupBoxImg.TabStop = false;
            this.groupBoxImg.Text = "图";
            this.groupBoxImg.Visible = false;
            // 
            // l_img_5
            // 
            this.l_img_5.AutoSize = true;
            this.l_img_5.Location = new System.Drawing.Point(432, 120);
            this.l_img_5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.l_img_5.Name = "l_img_5";
            this.l_img_5.Size = new System.Drawing.Size(63, 15);
            this.l_img_5.TabIndex = 55;
            this.l_img_5.Text = "l_img_5";
            this.l_img_5.Visible = false;
            // 
            // l_img_4
            // 
            this.l_img_4.AutoSize = true;
            this.l_img_4.Location = new System.Drawing.Point(332, 120);
            this.l_img_4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.l_img_4.Name = "l_img_4";
            this.l_img_4.Size = new System.Drawing.Size(63, 15);
            this.l_img_4.TabIndex = 54;
            this.l_img_4.Text = "l_img_4";
            this.l_img_4.Visible = false;
            // 
            // l_img_3
            // 
            this.l_img_3.AutoSize = true;
            this.l_img_3.Location = new System.Drawing.Point(232, 120);
            this.l_img_3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.l_img_3.Name = "l_img_3";
            this.l_img_3.Size = new System.Drawing.Size(63, 15);
            this.l_img_3.TabIndex = 53;
            this.l_img_3.Text = "l_img_3";
            this.l_img_3.Visible = false;
            // 
            // l_img_2
            // 
            this.l_img_2.AutoSize = true;
            this.l_img_2.Location = new System.Drawing.Point(132, 120);
            this.l_img_2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.l_img_2.Name = "l_img_2";
            this.l_img_2.Size = new System.Drawing.Size(63, 15);
            this.l_img_2.TabIndex = 52;
            this.l_img_2.Text = "l_img_2";
            this.l_img_2.Visible = false;
            // 
            // l_img_1
            // 
            this.l_img_1.AutoSize = true;
            this.l_img_1.Location = new System.Drawing.Point(32, 120);
            this.l_img_1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.l_img_1.Name = "l_img_1";
            this.l_img_1.Size = new System.Drawing.Size(63, 15);
            this.l_img_1.TabIndex = 51;
            this.l_img_1.Text = "l_img_1";
            this.l_img_1.Visible = false;
            // 
            // img5
            // 
            this.img5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img5.ContextMenuStrip = this.contextMenuStripimg1;
            this.img5.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图5", true));
            this.img5.Location = new System.Drawing.Point(416, 24);
            this.img5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.img5.Name = "img5";
            this.img5.Size = new System.Drawing.Size(93, 87);
            this.img5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img5.TabIndex = 50;
            this.img5.TabStop = false;
            this.img5.Visible = false;
            // 
            // contextMenuStripimg1
            // 
            this.contextMenuStripimg1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStripimg1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导入图片ToolStripMenuItem,
            this.导出图片ToolStripMenuItem,
            this.清空图片ToolStripMenuItem,
            this.toolStripSeparator1,
            this.图片浏览ToolStripMenuItem});
            this.contextMenuStripimg1.Name = "contextMenuStripLICENSE_PIC";
            this.contextMenuStripimg1.Size = new System.Drawing.Size(139, 106);
            this.contextMenuStripimg1.Text = "图片管理";
            // 
            // 导入图片ToolStripMenuItem
            // 
            this.导入图片ToolStripMenuItem.Name = "导入图片ToolStripMenuItem";
            this.导入图片ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.导入图片ToolStripMenuItem.Text = "导入图片";
            this.导入图片ToolStripMenuItem.Click += new System.EventHandler(this.导入图片ToolStripMenuItem_Click);
            // 
            // 导出图片ToolStripMenuItem
            // 
            this.导出图片ToolStripMenuItem.Name = "导出图片ToolStripMenuItem";
            this.导出图片ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.导出图片ToolStripMenuItem.Text = "导出图片";
            this.导出图片ToolStripMenuItem.Click += new System.EventHandler(this.导出图片ToolStripMenuItem_Click);
            // 
            // 清空图片ToolStripMenuItem
            // 
            this.清空图片ToolStripMenuItem.Name = "清空图片ToolStripMenuItem";
            this.清空图片ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.清空图片ToolStripMenuItem.Text = "清空图片";
            this.清空图片ToolStripMenuItem.Click += new System.EventHandler(this.清空图片ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(135, 6);
            // 
            // 图片浏览ToolStripMenuItem
            // 
            this.图片浏览ToolStripMenuItem.Name = "图片浏览ToolStripMenuItem";
            this.图片浏览ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.图片浏览ToolStripMenuItem.Text = "图片浏览";
            this.图片浏览ToolStripMenuItem.Click += new System.EventHandler(this.图片浏览ToolStripMenuItem_Click);
            // 
            // lIS_REPORT_IMGBindingSource
            // 
            this.lIS_REPORT_IMGBindingSource.DataMember = "LIS_REPORT_IMG";
            this.lIS_REPORT_IMGBindingSource.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // img4
            // 
            this.img4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img4.ContextMenuStrip = this.contextMenuStripimg1;
            this.img4.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图4", true));
            this.img4.Location = new System.Drawing.Point(316, 24);
            this.img4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.img4.Name = "img4";
            this.img4.Size = new System.Drawing.Size(93, 87);
            this.img4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img4.TabIndex = 49;
            this.img4.TabStop = false;
            this.img4.Visible = false;
            // 
            // img3
            // 
            this.img3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img3.ContextMenuStrip = this.contextMenuStripimg1;
            this.img3.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图3", true));
            this.img3.Location = new System.Drawing.Point(216, 24);
            this.img3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.img3.Name = "img3";
            this.img3.Size = new System.Drawing.Size(93, 87);
            this.img3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img3.TabIndex = 48;
            this.img3.TabStop = false;
            this.img3.Visible = false;
            // 
            // img2
            // 
            this.img2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img2.ContextMenuStrip = this.contextMenuStripimg1;
            this.img2.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图2", true));
            this.img2.Location = new System.Drawing.Point(116, 24);
            this.img2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.img2.Name = "img2";
            this.img2.Size = new System.Drawing.Size(93, 87);
            this.img2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img2.TabIndex = 47;
            this.img2.TabStop = false;
            this.img2.Visible = false;
            // 
            // img1
            // 
            this.img1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img1.ContextMenuStrip = this.contextMenuStripimg1;
            this.img1.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图1", true));
            this.img1.Location = new System.Drawing.Point(16, 24);
            this.img1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.img1.Name = "img1";
            this.img1.Size = new System.Drawing.Size(93, 87);
            this.img1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img1.TabIndex = 46;
            this.img1.TabStop = false;
            this.img1.Visible = false;
            this.img1.DoubleClick += new System.EventHandler(this.img1_DoubleClick);
            // 
            // JyInstrDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 632);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.dataGridViewReport);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(this.groupBoxif);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "JyInstrDataForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "仪器接收数据";
            this.Load += new System.EventHandler(this.InstrDataForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceRelust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBoxImg.ResumeLayout(false);
            this.groupBoxImg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img5)).EndInit();
            this.contextMenuStripimg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorOK;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonNo;
        private System.Windows.Forms.DataGridView dataGridViewReport;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.TextBox fsample_codeTextBox;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ToolStripButton toolStripButtonQ;
        private System.Windows.Forms.BindingSource bindingSourceIO;
        private System.Windows.Forms.BindingSource bindingSourceRelust;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBoxImg;
        private System.Windows.Forms.PictureBox img1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripimg1;
        private System.Windows.Forms.ToolStripMenuItem 导入图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 清空图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 图片浏览ToolStripMenuItem;
        private System.Windows.Forms.PictureBox img5;
        private System.Windows.Forms.PictureBox img4;
        private System.Windows.Forms.PictureBox img3;
        private System.Windows.Forms.PictureBox img2;
        private System.Windows.Forms.BindingSource lIS_REPORT_IMGBindingSource;
        private yunLis.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn FValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn FOD;
        private System.Windows.Forms.DataGridViewTextBoxColumn FCutoff;
        private System.Windows.Forms.DataGridViewTextBoxColumn FItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox未读取;
        private System.Windows.Forms.DataGridViewTextBoxColumn FResultID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTransFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn FValidity;
        private System.Windows.Forms.DataGridViewTextBoxColumn FInstrID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FType;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTaskID;
        private System.Windows.Forms.Label l_img_2;
        private System.Windows.Forms.Label l_img_1;
        private System.Windows.Forms.Label l_img_3;
        private System.Windows.Forms.Label l_img_5;
        private System.Windows.Forms.Label l_img_4;
    }
}