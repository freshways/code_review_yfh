﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;

namespace yunLis.lis.com
{
    public partial class WindowsPersonForm : Form
    {
        PersonBLL bllPerson = new PersonBLL();
        DataTable dtCurrAll = new DataTable();
        //DataTable dtCurr = new DataTable();
        DeptBll bllDept = new DeptBll();
        DataTable dtDept = new DataTable();
        int iinit = 0;
        DataTable dtThis = null;

        public string strid = "";
        public string strname = "";
        public WindowsPersonForm()
        {
            InitializeComponent();
            
            
        }
        private WindowsResult r;
        public WindowsPersonForm(WindowsResult r)
            : this()
        {
            this.r = r;

        }

      
      
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = dtDept;
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fdept_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        private void ApplyUserForm_Load(object sender, EventArgs e)
        {
            try
            {
               
                dtDept = this.bllDept.BllDeptDTuse();
                this.com_listBindingSource.DataSource = dtDept;

                GetTree("-1");

                //add by wjz 20160520 调整医师数据的加载方式 ▽
                //GetDT("");
                InitDT();
                //add by wjz 20160520 调整医师数据的加载方式 △
              
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        //add by wjz 20160520 调整医师数据的加载方式 ▽
        private DataView dvPerson;
        private void InitDT()
        {
            try
            {
                dtThis = this.bllPerson.BllDTByfhelp_code("", "");
                dvPerson = dtThis.DefaultView;
                bindingSource1.DataSource = dvPerson;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        //add by wjz 20160520 调整医师数据的加载方式 △


        private void GetDT(string fdept_id)
        {
            try
            {
                //string sql = "SELECT * FROM wwf_person where fhelp_code='%" + textBox1.Text + "%' order by forder_by";
                //this.richTextBox1.Text = sql;
                dtThis = this.bllPerson.BllDTByfhelp_code(textBox1.Text, fdept_id);
                
                bindingSource1.DataSource = dtThis;
              //  this.dataGridView1.DataSource = dtThis;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //changed by wjz 20160520 调整医师数据的加载方式 ▽
                //GetDT("");
                dvPerson.RowFilter = "fhelp_code like '%" + textBox1.Text + "%'";
                bindingSource1_PositionChanged(null, null);
                //changed by wjz 20160520 调整医师数据的加载方式 △
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        string str当前树id = "";
        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                str当前树id = e.Node.Name.ToString();
                //changed by wjz 20160520 调整医师数据的加载方式 ▽
                //GetDT(str当前树id);
                dvPerson.RowFilter = "fhelp_code like '%" + textBox1.Text + "%' and fdept_id='" + str当前树id + "'";
                bindingSource1_PositionChanged(null, null);
                //changed by wjz 20160520 调整医师数据的加载方式 △
                
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally {
                iinit = 1;
            }
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                //if (this.dataGridView1.Rows.Count > 0)
                //{
                //    string fperson_id = this.dataGridView1.CurrentRow.Cells["fperson_id"].Value.ToString();
                //    r.ChangeTextString(fperson_id);
                    this.Close();
                //}
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                this.bindingSource1.PositionChanged -= new EventHandler(bindingSource1_PositionChanged);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.No;
            //this.Close();
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                //if (this.dataGridView1.Rows.Count > 0)
                //{
                //    string fperson_id = this.dataGridView1.CurrentRow.Cells["fperson_id"].Value.ToString();
                //    r.ChangeTextString(fperson_id);
                    this.Close();
                //}
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void bindingSource1_PositionChanged(object sender, EventArgs e)
        {
            try
            {

                DataRowView rowCurrent = (DataRowView)bindingSource1.Current;
                if (rowCurrent != null && rowCurrent[0].ToString() != "")
                {
                    strid = rowCurrent["fperson_id"].ToString();
                    strname = rowCurrent["fname"].ToString();
                }
                //changed by wjz 20160520 调整医师数据的加载方式 ▽
                else
                {
                    strid = "";
                    strname = "";
                }
                //changed by wjz 20160520 调整医师数据的加载方式 △
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void wwTreeView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                //changed by wjz 20160520 调整医师数据的加载方式 ▽
                //GetDT(str当前树id);
                dvPerson.RowFilter = "fhelp_code like '%" + textBox1.Text + "%' and fdept_id='" + str当前树id + "'";
                bindingSource1_PositionChanged(null, null);
                //changed by wjz 20160520 调整医师数据的加载方式 △

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                iinit = 1;
            }
        }
    }
}