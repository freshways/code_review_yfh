﻿namespace yunLis.lis.com
{
    partial class ItemSelectForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemSelectForm));
            this.sam_itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorOK = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonNo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonNO1 = new System.Windows.Forms.ToolStripButton();
            this.fselect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhelp_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.sam_itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sam_itemBindingSource
            // 
            this.sam_itemBindingSource.DataMember = "sam_item";
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(472, 25);
            this.textBox1.TabIndex = 123;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGridViewObject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fselect,
            this.fitem_code,
            this.fname,
            this.fhelp_code,
            this.fitem_id});
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridViewObject.Location = new System.Drawing.Point(0, 25);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 30;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.ShowCellToolTips = false;
            this.DataGridViewObject.ShowEditingIcon = false;
            this.DataGridViewObject.Size = new System.Drawing.Size(472, 386);
            this.DataGridViewObject.TabIndex = 124;
            this.DataGridViewObject.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridViewObject_KeyDown);
            this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            this.DataGridViewObject.DoubleClick += new System.EventHandler(this.DataGridViewObject_DoubleClick);
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorOK;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.BindingSource = this.sam_itemBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator2,
            this.bindingNavigatorOK,
            this.toolStripButtonNo,
            this.toolStripSeparator1,
            this.toolStripButtonAll,
            this.toolStripButtonNO1});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 411);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(472, 35);
            this.bindingNavigator1.TabIndex = 125;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorOK
            // 
            this.bindingNavigatorOK.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorOK.Image")));
            this.bindingNavigatorOK.Name = "bindingNavigatorOK";
            this.bindingNavigatorOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.bindingNavigatorOK.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorOK.Size = new System.Drawing.Size(85, 32);
            this.bindingNavigatorOK.Text = "确 定(&O)  ";
            this.bindingNavigatorOK.Click += new System.EventHandler(this.bindingNavigatorOK_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 12);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 35);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonNo
            // 
            this.toolStripButtonNo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNo.Image")));
            this.toolStripButtonNo.Name = "toolStripButtonNo";
            this.toolStripButtonNo.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonNo.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonNo.Size = new System.Drawing.Size(85, 32);
            this.toolStripButtonNo.Text = "取 消(&C)  ";
            this.toolStripButtonNo.Click += new System.EventHandler(this.toolStripButtonNo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonAll
            // 
            this.toolStripButtonAll.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAll.Image")));
            this.toolStripButtonAll.Name = "toolStripButtonAll";
            this.toolStripButtonAll.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAll.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAll.Size = new System.Drawing.Size(85, 32);
            this.toolStripButtonAll.Text = "全 选(&A)  ";
            this.toolStripButtonAll.Click += new System.EventHandler(this.toolStripButtonAll_Click);
            // 
            // toolStripButtonNO1
            // 
            this.toolStripButtonNO1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNO1.Image")));
            this.toolStripButtonNO1.Name = "toolStripButtonNO1";
            this.toolStripButtonNO1.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonNO1.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonNO1.Size = new System.Drawing.Size(103, 32);
            this.toolStripButtonNO1.Text = "取消全选(&N)  ";
            this.toolStripButtonNO1.Click += new System.EventHandler(this.toolStripButtonNO1_Click);
            // 
            // fselect
            // 
            this.fselect.DataPropertyName = "fselect";
            this.fselect.FalseValue = "0";
            this.fselect.HeaderText = "选";
            this.fselect.IndeterminateValue = "0";
            this.fselect.Name = "fselect";
            this.fselect.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fselect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fselect.TrueValue = "1";
            this.fselect.Width = 30;
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "项目代码";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Width = 120;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "项目名称";
            this.fname.Name = "fname";
            this.fname.ReadOnly = true;
            this.fname.Width = 200;
            // 
            // fhelp_code
            // 
            this.fhelp_code.DataPropertyName = "fhelp_code";
            this.fhelp_code.HeaderText = "助记符";
            this.fhelp_code.Name = "fhelp_code";
            this.fhelp_code.ReadOnly = true;
            this.fhelp_code.Width = 90;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.Visible = false;
            // 
            // ItemSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 446);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.bindingNavigator1);
            this.Name = "ItemSelectForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "项目选择";
            this.Load += new System.EventHandler(this.ItemSelectForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sam_itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sam_itemBindingSource;
        private System.Windows.Forms.TextBox textBox1;
        protected System.Windows.Forms.DataGridView DataGridViewObject;
        protected System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorOK;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonNo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAll;
        private System.Windows.Forms.ToolStripButton toolStripButtonNO1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fselect;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhelp_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
    }
}