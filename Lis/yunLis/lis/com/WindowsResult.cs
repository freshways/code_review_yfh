﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;

namespace yunLis.lis.com
{
    public delegate void TextChangedHandlerString(string str1);
    public delegate void IlistChangedHandlerIlist(IList ilist1, int intEidt);
    public delegate void IlistChangedIlistAndDataTable(IList ilist1, int intEidt, DataTable dtIMG);
    //public delegate void TextChangedHandlerDataTable(DataTable dt1);
    /// <summary>
    /// 弹出窗口返回值
    /// </summary>
    public class WindowsResult
    {
        public event TextChangedHandlerString TextChangedString;
        public event IlistChangedHandlerIlist IlistChangedIlist;
        public event IlistChangedIlistAndDataTable IlistChangedIlistAndDataTable;
        //public event TextChangedHandlerDataTable TextChangedDataTable;
        public void ChangeTextString(string str1)
        {
            if (TextChangedString != null)
                TextChangedString(str1);
        }
        public void ChangeIlist(IList ilist1, int intEidt)
        {
            if (IlistChangedIlist != null)
                IlistChangedIlist(ilist1, intEidt);
        }
        /// <summary>
        /// 返回表 仪器数据选择
        /// </summary>
        /// <param name="ilist1"></param>
        /// <param name="intEidt"></param>
        /// <param name="dtIMG"></param>
        public void ChangeIlistAndDataTable(IList ilist1, int intEidt, DataTable dtIMG)
        {
            if (IlistChangedIlistAndDataTable != null)
                IlistChangedIlistAndDataTable(ilist1, intEidt, dtIMG);
        }
        /*
        public void ChangeTextDataTable(DataTable dt1)
        {
            if (TextChangedDataTable != null)
                TextChangedDataTable(dt1);
        }*/
    }
}
