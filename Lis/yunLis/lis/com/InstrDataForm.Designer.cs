﻿namespace yunLis.lis.com
{
    partial class InstrDataForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label finstr_idLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstrDataForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingSourceIO = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQ = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorOK = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonNo = new System.Windows.Forms.ToolStripButton();
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.fsample_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finstr_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjz = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fqc = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ftime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsam_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fstate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcontent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fio_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.fselect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fitem_code_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fod_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcutoff_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.r_fstate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.r_ftime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.r_fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.r_fio_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fresult_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceRelust = new System.Windows.Forms.BindingSource(this.components);
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.fsample_codeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.finstr_idComboBox = new System.Windows.Forms.ComboBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxImg = new System.Windows.Forms.GroupBox();
            this.img5 = new System.Windows.Forms.PictureBox();
            this.contextMenuStripimg1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.导入图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清空图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.图片浏览ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lIS_REPORT_IMGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new yunLis.lis.sam.samDataSet();
            this.img4 = new System.Windows.Forms.PictureBox();
            this.img3 = new System.Windows.Forms.PictureBox();
            this.img2 = new System.Windows.Forms.PictureBox();
            this.img1 = new System.Windows.Forms.PictureBox();
            fsample_codeLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            finstr_idLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceRelust)).BeginInit();
            this.groupBoxif.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxImg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img5)).BeginInit();
            this.contextMenuStripimg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).BeginInit();
            this.SuspendLayout();
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(493, 16);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(47, 12);
            fsample_codeLabel.TabIndex = 21;
            fsample_codeLabel.Text = "样本号:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(6, 16);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // finstr_idLabel
            // 
            finstr_idLabel.AutoSize = true;
            finstr_idLabel.Location = new System.Drawing.Point(274, 16);
            finstr_idLabel.Name = "finstr_idLabel";
            finstr_idLabel.Size = new System.Drawing.Size(35, 12);
            finstr_idLabel.TabIndex = 10;
            finstr_idLabel.Text = "仪器:";
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.BindingSource = this.bindingSourceIO;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonQ,
            this.bindingNavigatorOK,
            this.toolStripButtonNo});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 471);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(792, 35);
            this.bindingNavigator1.TabIndex = 126;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingSourceIO
            // 
            this.bindingSourceIO.PositionChanged += new System.EventHandler(this.bindingSourceIO_PositionChanged);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 32);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 35);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonQ
            // 
            this.toolStripButtonQ.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQ.Image")));
            this.toolStripButtonQ.Name = "toolStripButtonQ";
            this.toolStripButtonQ.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQ.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQ.Size = new System.Drawing.Size(85, 32);
            this.toolStripButtonQ.Text = "查 询(&Q)  ";
            this.toolStripButtonQ.Click += new System.EventHandler(this.toolStripButtonQ_Click);
            // 
            // bindingNavigatorOK
            // 
            this.bindingNavigatorOK.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorOK.Image")));
            this.bindingNavigatorOK.Name = "bindingNavigatorOK";
            this.bindingNavigatorOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.bindingNavigatorOK.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorOK.Size = new System.Drawing.Size(85, 32);
            this.bindingNavigatorOK.Text = "确 定(&O)  ";
            this.bindingNavigatorOK.Click += new System.EventHandler(this.bindingNavigatorOK_Click);
            // 
            // toolStripButtonNo
            // 
            this.toolStripButtonNo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNo.Image")));
            this.toolStripButtonNo.Name = "toolStripButtonNo";
            this.toolStripButtonNo.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonNo.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonNo.Size = new System.Drawing.Size(85, 32);
            this.toolStripButtonNo.Text = "取 消(&C)  ";
            this.toolStripButtonNo.Click += new System.EventHandler(this.toolStripButtonNo_Click);
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToDeleteRows = false;
            this.dataGridViewReport.AllowUserToOrderColumns = true;
            this.dataGridViewReport.AllowUserToResizeColumns = false;
            this.dataGridViewReport.AllowUserToResizeRows = false;
            this.dataGridViewReport.AutoGenerateColumns = false;
            this.dataGridViewReport.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fsample_code,
            this.finstr_id,
            this.fdate,
            this.fjz,
            this.fqc,
            this.ftime,
            this.fsam_type_id,
            this.fstate,
            this.fcontent,
            this.fremark,
            this.fio_id});
            this.dataGridViewReport.DataSource = this.bindingSourceIO;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReport.Location = new System.Drawing.Point(0, 43);
            this.dataGridViewReport.MultiSelect = false;
            this.dataGridViewReport.Name = "dataGridViewReport";
            this.dataGridViewReport.ReadOnly = true;
            this.dataGridViewReport.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridViewReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.RowHeadersWidth = 20;
            this.dataGridViewReport.RowTemplate.Height = 23;
            this.dataGridViewReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReport.Size = new System.Drawing.Size(363, 428);
            this.dataGridViewReport.TabIndex = 131;
            this.dataGridViewReport.DoubleClick += new System.EventHandler(this.dataGridViewReport_DoubleClick);
            // 
            // fsample_code
            // 
            this.fsample_code.DataPropertyName = "fsample_code";
            this.fsample_code.HeaderText = "样本号";
            this.fsample_code.Name = "fsample_code";
            this.fsample_code.ReadOnly = true;
            this.fsample_code.Width = 60;
            // 
            // finstr_id
            // 
            this.finstr_id.DataPropertyName = "finstr_id";
            this.finstr_id.HeaderText = "仪器";
            this.finstr_id.Name = "finstr_id";
            this.finstr_id.ReadOnly = true;
            this.finstr_id.Width = 80;
            // 
            // fdate
            // 
            this.fdate.DataPropertyName = "fdate";
            this.fdate.HeaderText = "日期";
            this.fdate.Name = "fdate";
            this.fdate.ReadOnly = true;
            this.fdate.Width = 80;
            // 
            // fjz
            // 
            this.fjz.DataPropertyName = "fjz";
            this.fjz.FalseValue = "0";
            this.fjz.HeaderText = "急诊";
            this.fjz.IndeterminateValue = "0";
            this.fjz.Name = "fjz";
            this.fjz.ReadOnly = true;
            this.fjz.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fjz.TrueValue = "1";
            this.fjz.Width = 40;
            // 
            // fqc
            // 
            this.fqc.DataPropertyName = "fqc";
            this.fqc.FalseValue = "0";
            this.fqc.HeaderText = "质控";
            this.fqc.IndeterminateValue = "0";
            this.fqc.Name = "fqc";
            this.fqc.ReadOnly = true;
            this.fqc.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fqc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fqc.TrueValue = "1";
            this.fqc.Width = 40;
            // 
            // ftime
            // 
            this.ftime.DataPropertyName = "ftime";
            this.ftime.HeaderText = "时间";
            this.ftime.Name = "ftime";
            this.ftime.ReadOnly = true;
            this.ftime.Width = 120;
            // 
            // fsam_type_id
            // 
            this.fsam_type_id.DataPropertyName = "fsam_type_id";
            this.fsam_type_id.HeaderText = "样本类型";
            this.fsam_type_id.Name = "fsam_type_id";
            this.fsam_type_id.ReadOnly = true;
            this.fsam_type_id.Width = 80;
            // 
            // fstate
            // 
            this.fstate.DataPropertyName = "fstate";
            this.fstate.HeaderText = "状态";
            this.fstate.Name = "fstate";
            this.fstate.ReadOnly = true;
            // 
            // fcontent
            // 
            this.fcontent.DataPropertyName = "fcontent";
            this.fcontent.HeaderText = "内容";
            this.fcontent.Name = "fcontent";
            this.fcontent.ReadOnly = true;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            this.fremark.ReadOnly = true;
            // 
            // fio_id
            // 
            this.fio_id.DataPropertyName = "fio_id";
            this.fio_id.HeaderText = "fio_id";
            this.fio_id.Name = "fio_id";
            this.fio_id.ReadOnly = true;
            this.fio_id.Visible = false;
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToResizeColumns = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.AutoGenerateColumns = false;
            this.dataGridViewResult.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fselect,
            this.fitem_code_name,
            this.fitem_name,
            this.fvalue,
            this.fod_value,
            this.fcutoff_value,
            this.r_fstate,
            this.r_ftime,
            this.r_fremark,
            this.fitem_code,
            this.r_fio_id,
            this.fresult_id});
            this.dataGridViewResult.DataSource = this.bindingSourceRelust;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResult.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewResult.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            this.dataGridViewResult.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidth = 30;
            this.dataGridViewResult.RowTemplate.Height = 23;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewResult.ShowCellToolTips = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.Size = new System.Drawing.Size(429, 332);
            this.dataGridViewResult.TabIndex = 132;
            // 
            // fselect
            // 
            this.fselect.DataPropertyName = "fselect";
            this.fselect.FalseValue = "0";
            this.fselect.HeaderText = "*";
            this.fselect.IndeterminateValue = "0";
            this.fselect.Name = "fselect";
            this.fselect.ReadOnly = true;
            this.fselect.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fselect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fselect.TrueValue = "1";
            this.fselect.Width = 25;
            // 
            // fitem_code_name
            // 
            this.fitem_code_name.DataPropertyName = "fitem_code_name";
            this.fitem_code_name.HeaderText = "项目代码";
            this.fitem_code_name.Name = "fitem_code_name";
            this.fitem_code_name.ReadOnly = true;
            this.fitem_code_name.Width = 80;
            // 
            // fitem_name
            // 
            this.fitem_name.DataPropertyName = "fitem_name";
            this.fitem_name.HeaderText = "项目名称";
            this.fitem_name.Name = "fitem_name";
            this.fitem_name.ReadOnly = true;
            // 
            // fvalue
            // 
            this.fvalue.DataPropertyName = "fvalue";
            this.fvalue.HeaderText = "值";
            this.fvalue.Name = "fvalue";
            this.fvalue.ReadOnly = true;
            this.fvalue.Width = 75;
            // 
            // fod_value
            // 
            this.fod_value.DataPropertyName = "fod_value";
            this.fod_value.HeaderText = "OD值";
            this.fod_value.Name = "fod_value";
            this.fod_value.ReadOnly = true;
            this.fod_value.Width = 75;
            // 
            // fcutoff_value
            // 
            this.fcutoff_value.DataPropertyName = "fcutoff_value";
            this.fcutoff_value.HeaderText = "Cutoff值";
            this.fcutoff_value.Name = "fcutoff_value";
            this.fcutoff_value.ReadOnly = true;
            this.fcutoff_value.Width = 80;
            // 
            // r_fstate
            // 
            this.r_fstate.DataPropertyName = "fstate";
            this.r_fstate.HeaderText = "状态";
            this.r_fstate.Name = "r_fstate";
            this.r_fstate.ReadOnly = true;
            this.r_fstate.Visible = false;
            // 
            // r_ftime
            // 
            this.r_ftime.DataPropertyName = "ftime";
            this.r_ftime.HeaderText = "时间";
            this.r_ftime.Name = "r_ftime";
            this.r_ftime.ReadOnly = true;
            this.r_ftime.Visible = false;
            // 
            // r_fremark
            // 
            this.r_fremark.DataPropertyName = "fremark";
            this.r_fremark.HeaderText = "备注";
            this.r_fremark.Name = "r_fremark";
            this.r_fremark.ReadOnly = true;
            this.r_fremark.Visible = false;
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "项目ID";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Visible = false;
            // 
            // r_fio_id
            // 
            this.r_fio_id.DataPropertyName = "fio_id";
            this.r_fio_id.HeaderText = "fio_id";
            this.r_fio_id.Name = "r_fio_id";
            this.r_fio_id.ReadOnly = true;
            this.r_fio_id.Visible = false;
            // 
            // fresult_id
            // 
            this.fresult_id.DataPropertyName = "fresult_id";
            this.fresult_id.HeaderText = "fresult_id";
            this.fresult_id.Name = "fresult_id";
            this.fresult_id.ReadOnly = true;
            this.fresult_id.Visible = false;
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(this.fsample_codeTextBox);
            this.groupBoxif.Controls.Add(fsample_codeLabel);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker2);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker1);
            this.groupBoxif.Controls.Add(fjy_dateLabel);
            this.groupBoxif.Controls.Add(this.finstr_idComboBox);
            this.groupBoxif.Controls.Add(finstr_idLabel);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 0);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Size = new System.Drawing.Size(792, 43);
            this.groupBoxif.TabIndex = 130;
            this.groupBoxif.TabStop = false;
            // 
            // fsample_codeTextBox
            // 
            this.fsample_codeTextBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.fsample_codeTextBox.Location = new System.Drawing.Point(543, 12);
            this.fsample_codeTextBox.Name = "fsample_codeTextBox";
            this.fsample_codeTextBox.Size = new System.Drawing.Size(87, 21);
            this.fsample_codeTextBox.TabIndex = 20;
            this.fsample_codeTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsample_codeTextBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(159, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(171, 12);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 13;
            this.fjy_dateDateTimePicker2.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker2_CloseUp);
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(71, 12);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 11;
            this.fjy_dateDateTimePicker1.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker1_CloseUp);
            // 
            // finstr_idComboBox
            // 
            this.finstr_idComboBox.DisplayMember = "fname";
            this.finstr_idComboBox.FormattingEnabled = true;
            this.finstr_idComboBox.Items.AddRange(new object[] {
            ""});
            this.finstr_idComboBox.Location = new System.Drawing.Point(313, 12);
            this.finstr_idComboBox.Name = "finstr_idComboBox";
            this.finstr_idComboBox.Size = new System.Drawing.Size(155, 20);
            this.finstr_idComboBox.TabIndex = 9;
            this.finstr_idComboBox.ValueMember = "finstr_id";
            this.finstr_idComboBox.DropDownClosed += new System.EventHandler(this.finstr_idComboBox_DropDownClosed);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(360, 43);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 428);
            this.splitter1.TabIndex = 133;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridViewResult);
            this.panel1.Controls.Add(this.groupBoxImg);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(363, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(429, 428);
            this.panel1.TabIndex = 134;
            // 
            // groupBoxImg
            // 
            this.groupBoxImg.Controls.Add(this.img5);
            this.groupBoxImg.Controls.Add(this.img4);
            this.groupBoxImg.Controls.Add(this.img3);
            this.groupBoxImg.Controls.Add(this.img2);
            this.groupBoxImg.Controls.Add(this.img1);
            this.groupBoxImg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxImg.Location = new System.Drawing.Point(0, 332);
            this.groupBoxImg.Name = "groupBoxImg";
            this.groupBoxImg.Size = new System.Drawing.Size(429, 96);
            this.groupBoxImg.TabIndex = 133;
            this.groupBoxImg.TabStop = false;
            this.groupBoxImg.Text = "图";
            this.groupBoxImg.Visible = false;
            // 
            // img5
            // 
            this.img5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img5.ContextMenuStrip = this.contextMenuStripimg1;
            this.img5.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图5", true));
            this.img5.Location = new System.Drawing.Point(312, 19);
            this.img5.Name = "img5";
            this.img5.Size = new System.Drawing.Size(70, 70);
            this.img5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img5.TabIndex = 50;
            this.img5.TabStop = false;
            this.img5.Visible = false;
            // 
            // contextMenuStripimg1
            // 
            this.contextMenuStripimg1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导入图片ToolStripMenuItem,
            this.导出图片ToolStripMenuItem,
            this.清空图片ToolStripMenuItem,
            this.toolStripSeparator1,
            this.图片浏览ToolStripMenuItem});
            this.contextMenuStripimg1.Name = "contextMenuStripLICENSE_PIC";
            this.contextMenuStripimg1.Size = new System.Drawing.Size(119, 98);
            this.contextMenuStripimg1.Text = "图片管理";
            // 
            // 导入图片ToolStripMenuItem
            // 
            this.导入图片ToolStripMenuItem.Name = "导入图片ToolStripMenuItem";
            this.导入图片ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.导入图片ToolStripMenuItem.Text = "导入图片";
            this.导入图片ToolStripMenuItem.Click += new System.EventHandler(this.导入图片ToolStripMenuItem_Click);
            // 
            // 导出图片ToolStripMenuItem
            // 
            this.导出图片ToolStripMenuItem.Name = "导出图片ToolStripMenuItem";
            this.导出图片ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.导出图片ToolStripMenuItem.Text = "导出图片";
            this.导出图片ToolStripMenuItem.Click += new System.EventHandler(this.导出图片ToolStripMenuItem_Click);
            // 
            // 清空图片ToolStripMenuItem
            // 
            this.清空图片ToolStripMenuItem.Name = "清空图片ToolStripMenuItem";
            this.清空图片ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.清空图片ToolStripMenuItem.Text = "清空图片";
            this.清空图片ToolStripMenuItem.Click += new System.EventHandler(this.清空图片ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(115, 6);
            // 
            // 图片浏览ToolStripMenuItem
            // 
            this.图片浏览ToolStripMenuItem.Name = "图片浏览ToolStripMenuItem";
            this.图片浏览ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.图片浏览ToolStripMenuItem.Text = "图片浏览";
            this.图片浏览ToolStripMenuItem.Click += new System.EventHandler(this.图片浏览ToolStripMenuItem_Click);
            // 
            // lIS_REPORT_IMGBindingSource
            // 
            this.lIS_REPORT_IMGBindingSource.DataMember = "LIS_REPORT_IMG";
            this.lIS_REPORT_IMGBindingSource.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // img4
            // 
            this.img4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img4.ContextMenuStrip = this.contextMenuStripimg1;
            this.img4.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图4", true));
            this.img4.Location = new System.Drawing.Point(237, 19);
            this.img4.Name = "img4";
            this.img4.Size = new System.Drawing.Size(70, 70);
            this.img4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img4.TabIndex = 49;
            this.img4.TabStop = false;
            this.img4.Visible = false;
            // 
            // img3
            // 
            this.img3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img3.ContextMenuStrip = this.contextMenuStripimg1;
            this.img3.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图3", true));
            this.img3.Location = new System.Drawing.Point(162, 19);
            this.img3.Name = "img3";
            this.img3.Size = new System.Drawing.Size(70, 70);
            this.img3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img3.TabIndex = 48;
            this.img3.TabStop = false;
            this.img3.Visible = false;
            // 
            // img2
            // 
            this.img2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img2.ContextMenuStrip = this.contextMenuStripimg1;
            this.img2.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图2", true));
            this.img2.Location = new System.Drawing.Point(87, 19);
            this.img2.Name = "img2";
            this.img2.Size = new System.Drawing.Size(70, 70);
            this.img2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img2.TabIndex = 47;
            this.img2.TabStop = false;
            this.img2.Visible = false;
            // 
            // img1
            // 
            this.img1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img1.ContextMenuStrip = this.contextMenuStripimg1;
            this.img1.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图1", true));
            this.img1.Location = new System.Drawing.Point(12, 19);
            this.img1.Name = "img1";
            this.img1.Size = new System.Drawing.Size(70, 70);
            this.img1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img1.TabIndex = 46;
            this.img1.TabStop = false;
            this.img1.Visible = false;
            this.img1.DoubleClick += new System.EventHandler(this.img1_DoubleClick);
            // 
            // InstrDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 506);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.dataGridViewReport);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(this.groupBoxif);
            this.Name = "InstrDataForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "仪器接收数据";
            this.Load += new System.EventHandler(this.InstrDataForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceRelust)).EndInit();
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBoxImg.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img5)).EndInit();
            this.contextMenuStripimg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorOK;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonNo;
        private System.Windows.Forms.DataGridView dataGridViewReport;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.TextBox fsample_codeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox finstr_idComboBox;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ToolStripButton toolStripButtonQ;
        private System.Windows.Forms.BindingSource bindingSourceIO;
        private System.Windows.Forms.BindingSource bindingSourceRelust;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fselect;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue;
        private System.Windows.Forms.DataGridViewTextBoxColumn fod_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcutoff_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn r_fstate;
        private System.Windows.Forms.DataGridViewTextBoxColumn r_ftime;
        private System.Windows.Forms.DataGridViewTextBoxColumn r_fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn r_fio_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fresult_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsample_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn finstr_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fdate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fjz;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fqc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftime;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsam_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fstate;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcontent;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn fio_id;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBoxImg;
        private System.Windows.Forms.PictureBox img1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripimg1;
        private System.Windows.Forms.ToolStripMenuItem 导入图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 清空图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 图片浏览ToolStripMenuItem;
        private System.Windows.Forms.PictureBox img5;
        private System.Windows.Forms.PictureBox img4;
        private System.Windows.Forms.PictureBox img3;
        private System.Windows.Forms.PictureBox img2;
        private System.Windows.Forms.BindingSource lIS_REPORT_IMGBindingSource;
        private yunLis.lis.sam.samDataSet samDataSet;
    }
}