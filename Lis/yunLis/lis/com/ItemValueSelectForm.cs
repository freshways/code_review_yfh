﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using yunLis.lisbll.sam;
namespace yunLis.lis.com
{
    public partial class ItemValueSelectForm : Form
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        private string stritem_id = "";//      
        private string strValue = "";//返回的值
        private WindowsResult r;

        public ItemValueSelectForm()
        {
            InitializeComponent();
            DataGridViewObject.AutoGenerateColumns = false;
        }

        
        public ItemValueSelectForm(WindowsResult r, string item_id)
            : this()
        {
            
            this.r = r;
            this.stritem_id = item_id;            
           
        }

        private void ItemValueSelectForm_Load(object sender, EventArgs e)
        {
            GetItemDT();
        }

        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
        /// <param name="strInstrID"></param>
        private void GetItemDT()
        {
            try
            {
                this.sam_itemBindingSource.DataSource = this.bllItem.BllItemValueDT(this.stritem_id);
              
                this.DataGridViewObject.DataSource = sam_itemBindingSource;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
       
      
                

      
        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.DataGridViewObject.CurrentRow.Cells["fvalue"].Value != null)
                    strValue = this.DataGridViewObject.CurrentRow.Cells["fvalue"].Value.ToString();
                r.ChangeTextString(this.strValue);
                this.Close();


            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }




        private void DataGridViewObject_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    if (this.DataGridViewObject.CurrentRow.Cells["fvalue"].Value != null)
                        strValue = this.DataGridViewObject.CurrentRow.Cells["fvalue"].Value.ToString();
                    r.ChangeTextString(this.strValue);
                    this.Close();


                }
                catch (Exception ex)
                {
                    yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

        private void DataGridViewObject_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.DataGridViewObject.CurrentRow.Cells["fvalue"].Value != null)
                    strValue = this.DataGridViewObject.CurrentRow.Cells["fvalue"].Value.ToString();
                r.ChangeTextString(this.strValue);
                this.Close();


            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
       
    }
}