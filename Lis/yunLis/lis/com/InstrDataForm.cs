﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;
using yunLis.lisbll.sam;
using System.Collections;
using System.IO;

namespace yunLis.lis.com
{
    public partial class InstrDataForm : Form
    { /// <summary>
        /// 逻辑_仪器
        /// </summary>
        private InstrBLL bllInstr = new InstrBLL();
        private InstrIOBll bllInstrIO = new InstrIOBll();
        private string strfinstr_id = "";//仪器ＩＤ       
        private WindowsResult r;
        private DataTable dtResult = new DataTable();
        IList selItemList = null;
        ImgBLL img = new ImgBLL();
        DataTable dtIMG = new DataTable();
        DataTable dtIMG_1 = new DataTable();//返回的图列表
        public InstrDataForm()
        {
            InitializeComponent();
            this.dataGridViewReport.AutoGenerateColumns = false;
            this.dataGridViewResult.AutoGenerateColumns = false;


            DataColumn D0 = new DataColumn("FID", typeof(System.String));
            dtIMG.Columns.Add(D0);

            DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));
            dtIMG.Columns.Add(D1);

            DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
            dtIMG.Columns.Add(D2);

            DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
            dtIMG.Columns.Add(D3);

            DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
            dtIMG.Columns.Add(D4);

            DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
            dtIMG.Columns.Add(D5);
         
        }
        public InstrDataForm(WindowsResult r, string finstr_id)
            : this()
        {
            
            this.r = r;
            this.strfinstr_id = finstr_id;            
           
        }
        private void InstrDataForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.finstr_idComboBox.DataSource = this.bllInstr.BllInstrDT(1);//已经启用的所有仪器
                this.finstr_idComboBox.SelectedValue = this.strfinstr_id;
                WWQuery();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void WWImg(string strid)
        {
            try
            {
                if (this.dtIMG.Rows.Count > 0)
                    this.dtIMG.Clear();
                if (this.dtIMG_1.Rows.Count > 0)
                    this.dtIMG_1.Clear();
                dtIMG_1 = this.bllInstrIO.BllImgDT(strid);              
                DataRow drimg = dtIMG.NewRow();               
                int intImgCount = dtIMG_1.Rows.Count;
                if (intImgCount > 0)
                {
                    groupBoxImg.Visible = true;
                    drimg["FID"] = dtIMG_1.Rows[0]["fio_id"];
                    for (int iimg = 0; iimg < intImgCount; iimg++)
                    {
                        switch (iimg)
                        {
                            case 0:
                                img1.Visible = true;
                                img2.Visible = false;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图1"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 1:
                                img2.Visible = true;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图2"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 2:
                                img3.Visible = true;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图3"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 3:
                                img4.Visible = true;
                                img5.Visible = false;
                                drimg["结果图4"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 4:
                                img5.Visible = true;
                                drimg["结果图5"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            default:
                                break;
                        }
                    }

                }
                else
                {
                    groupBoxImg.Visible = false;
                    img1.Visible = false;
                    img2.Visible = false;
                    img3.Visible = false;
                    img4.Visible = false;
                    img5.Visible = false;
                }
                dtIMG.Rows.Add(drimg);
                lIS_REPORT_IMGBindingSource.DataSource = this.dtIMG;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void GelSelValueList()
        {
            try
            {
                this.Validate();
                this.bindingSourceRelust.EndEdit();
                selItemList = new ArrayList();
                string strfitem_id = "";
                string strfitem_value = "";
                Object objSel = null;
                int intSelValue = 0;

                for (int i = 0; i < this.dataGridViewResult.Rows.Count; i++)
                {
                    strfitem_id = "";
                    strfitem_value = "";
                    if (this.dataGridViewResult.Rows[i].Cells["fitem_code"].Value != null)
                        strfitem_id = this.dataGridViewResult.Rows[i].Cells["fitem_code"].Value.ToString();
                    if (this.dataGridViewResult.Rows[i].Cells["fvalue"].Value != null)
                        strfitem_value = this.dataGridViewResult.Rows[i].Cells["fvalue"].Value.ToString();
                    objSel = this.dataGridViewResult.Rows[i].Cells["fselect"].Value;
                    if (objSel != null)
                        intSelValue = Convert.ToInt32(objSel);
                    else
                        intSelValue = 0;

                    if (intSelValue == 1)
                    {
                        this.selItemList.Add(strfitem_id + ";" + strfitem_value);
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                GelSelValueList();
                r.ChangeIlistAndDataTable(selItemList, 1, dtIMG_1);
                this.Close();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButtonQ_Click(object sender, EventArgs e)
        {
            WWQuery();
        }
        private void WWQuery()
        {
            try
            {
               
                string fsample_code = "";//样本号                        
                string finstr_id = "";//仪器ID               
                string fjy_dateWhere = "";
                string fjy_date1 = "";
                string fjy_date2 = "";
                fsample_code = this.fsample_codeTextBox.Text;
                               try
                {
                    finstr_id = finstr_idComboBox.SelectedValue.ToString();
                }
                catch { }              
                
                fjy_date1 = this.bllInstr.DbDateTime1(fjy_dateDateTimePicker1);
                fjy_date2 = this.bllInstr.DbDateTime1(fjy_dateDateTimePicker2);               

                if (finstr_idComboBox.Text == "" || finstr_idComboBox.Text == null)
                    finstr_id = " (finstr_id is not null) ";
                else
                    finstr_id = " (finstr_id='" + finstr_id + "') ";

                fjy_dateWhere = " and (fdate>='" + fjy_date1 + "' and fdate<='" + fjy_date2 + "') ";

                if (fsample_code == "" || fsample_code == null)
                    fsample_code = " and (fsample_code is not null) ";
                else
                    fsample_code = " and (fsample_code='" + fsample_code + "')";

                string strWhere = finstr_id + fjy_dateWhere + fsample_code;
                //this.textBox1.Text = strWhere;
                //this.textBox1.Text = strWhere;
               // sam_jyBindingSource.DataSource = this.bllJY.BllJYdt(strWhere);
              //  WWReportGridViesStyle();
                this.bindingSourceIO.DataSource = this.bllInstrIO.BllIODT(strWhere);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
           
        }
        private void WWResult(string strid)
        {
            try
            {
                this.bindingSourceRelust.DataSource = this.bllInstrIO.BllResultDT(strid);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

            try
            {
                this.Validate();
                this.bindingSourceRelust.EndEdit();
                for (int i = 0; i < this.dataGridViewResult.Rows.Count; i++)
                {
                    this.dataGridViewResult.Rows[i].Cells["fselect"].Value = "1";
                }
            }
            catch (Exception ex)
            {
               WWMessage.MessageShowError(ex.ToString());
            }
        }
        
        
       

        private void finstr_idComboBox_DropDownClosed(object sender, EventArgs e)
        {
            WWQuery();
        }

        private void bindingSourceIO_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.dtResult != null)
                    this.dtResult .Clear();
                bindingSourceRelust.DataSource = this.dtResult;

                DataRowView rowCurrent = (DataRowView)bindingSourceIO.Current;               
                if (rowCurrent != null)
                {
                    string strfio_id = rowCurrent["fio_id"].ToString();
                    WWResult(strfio_id);
                    WWImg(strfio_id);
                }

            }

            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridViewReport_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                GelSelValueList();
                r.ChangeIlistAndDataTable(selItemList, 1, dtIMG_1);
                this.Close();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void fjy_dateDateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            WWQuery();
        }

        private void fjy_dateDateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            WWQuery();
        }

        private void 导入图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {                
                img.ImgIn(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 导出图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                img.ImgOut(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 清空图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                img.ImgNull(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 图片浏览ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void img1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void fsample_codeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                WWQuery();
            }  
        }
    }
}