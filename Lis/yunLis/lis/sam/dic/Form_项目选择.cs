﻿using System;
using System.Collections;
using System.Windows.Forms;
using yunLis.lisbll.sam;

namespace yunLis.lis.sam.dic
{
    public partial class Form_项目选择 : Form
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        public string strfinstr_id = "";//仪器ＩＤ
        public string strfinstr_name = "";//仪器名称
        public IList selItemList = null;
        //public string[] items = { "","","",""};
        public HIS.Model.GY收费小项 gy收费小项;

        public Form_项目选择()
        {
            InitializeComponent();
            DataGridViewObject.AutoGenerateColumns = false;
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void Form_项目选择_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " " + strfinstr_name;
            GetItemDT("");
        }

        #region 方法
        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
        /// <param name="fhelp_code"></param>
        private void GetItemDT(string fhelp_code)
        {
            try
            {
                this.sam_itemBindingSource.DataSource = this.bllItem.BllGet收费小项(fhelp_code);
                this.DataGridViewObject.DataSource = sam_itemBindingSource;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void GelSelValueList()
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                HIS.Model.GY收费小项 model = DataGridViewObject.CurrentRow.DataBoundItem as HIS.Model.GY收费小项;
                if (model != null)
                {
                    gy收费小项 = model;
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        #endregion

        #region 按钮事件
        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                GelSelValueList();
                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.No;
        }

        #endregion

        #region 事件
        /// <summary>
        /// 过滤
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            GetItemDT(this.textBox1.Text);
        }

        private void DataGridViewObject_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();

                GelSelValueList();

                this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void DataGridViewObject_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    try
                    {
                        GelSelValueList();

                        this.DialogResult = System.Windows.Forms.DialogResult.Yes;
                    }
                    catch (Exception ex)
                    {
                        yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
                    }

                }
                catch (Exception ex)
                {
                    yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }
        #endregion

    }
}
