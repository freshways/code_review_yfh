﻿namespace yunLis.lis.sam.dic
{
    partial class Form_项目选择
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_项目选择));
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonNo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorOK = new System.Windows.Forms.ToolStripButton();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.sam_itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.fselect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.s收费编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s收费名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.i单价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s拼音代码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s归并编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonNo
            // 
            this.toolStripButtonNo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonNo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNo.Image")));
            this.toolStripButtonNo.Name = "toolStripButtonNo";
            this.toolStripButtonNo.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonNo.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonNo.Size = new System.Drawing.Size(80, 32);
            this.toolStripButtonNo.Text = "取 消(&C)  ";
            this.toolStripButtonNo.Click += new System.EventHandler(this.toolStripButtonNo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 35);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 35);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 32);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorOK
            // 
            this.bindingNavigatorOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.bindingNavigatorOK.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorOK.Image")));
            this.bindingNavigatorOK.Name = "bindingNavigatorOK";
            this.bindingNavigatorOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.bindingNavigatorOK.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorOK.Size = new System.Drawing.Size(82, 32);
            this.bindingNavigatorOK.Text = "确 定(&O)  ";
            this.bindingNavigatorOK.Click += new System.EventHandler(this.bindingNavigatorOK_Click);
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToDeleteRows = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGridViewObject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fselect,
            this.s收费编码,
            this.s收费名称,
            this.i单价,
            this.s拼音代码,
            this.s归并编码});
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridViewObject.Location = new System.Drawing.Point(0, 25);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            this.DataGridViewObject.ReadOnly = true;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 30;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.ShowCellToolTips = false;
            this.DataGridViewObject.ShowEditingIcon = false;
            this.DataGridViewObject.Size = new System.Drawing.Size(511, 386);
            this.DataGridViewObject.TabIndex = 127;
            this.DataGridViewObject.DoubleClick += new System.EventHandler(this.DataGridViewObject_DoubleClick);
            this.DataGridViewObject.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridViewObject_KeyDown);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(511, 25);
            this.textBox1.TabIndex = 126;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // sam_itemBindingSource
            // 
            this.sam_itemBindingSource.DataMember = "sam_item";
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorOK;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.BindingSource = this.sam_itemBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator2,
            this.toolStripButtonNo,
            this.toolStripSeparator1,
            this.bindingNavigatorOK});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 411);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(511, 35);
            this.bindingNavigator1.TabIndex = 128;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // fselect
            // 
            this.fselect.DataPropertyName = "fselect";
            this.fselect.FalseValue = "0";
            this.fselect.HeaderText = "选";
            this.fselect.IndeterminateValue = "0";
            this.fselect.Name = "fselect";
            this.fselect.ReadOnly = true;
            this.fselect.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fselect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fselect.TrueValue = "1";
            this.fselect.Visible = false;
            this.fselect.Width = 30;
            // 
            // s收费编码
            // 
            this.s收费编码.DataPropertyName = "收费编码";
            this.s收费编码.HeaderText = "收费编码";
            this.s收费编码.Name = "s收费编码";
            this.s收费编码.ReadOnly = true;
            this.s收费编码.Width = 120;
            // 
            // s收费名称
            // 
            this.s收费名称.DataPropertyName = "收费名称";
            this.s收费名称.HeaderText = "收费名称";
            this.s收费名称.Name = "s收费名称";
            this.s收费名称.ReadOnly = true;
            this.s收费名称.Width = 200;
            // 
            // i单价
            // 
            this.i单价.DataPropertyName = "单价";
            this.i单价.HeaderText = "单价";
            this.i单价.Name = "i单价";
            this.i单价.ReadOnly = true;
            this.i单价.Width = 80;
            // 
            // s拼音代码
            // 
            this.s拼音代码.DataPropertyName = "拼音代码";
            this.s拼音代码.HeaderText = "助记符";
            this.s拼音代码.Name = "s拼音代码";
            this.s拼音代码.ReadOnly = true;
            // 
            // s归并编码
            // 
            this.s归并编码.DataPropertyName = "归并编码";
            this.s归并编码.HeaderText = "归类编码";
            this.s归并编码.Name = "s归并编码";
            this.s归并编码.ReadOnly = true;
            this.s归并编码.Visible = false;
            // 
            // Form_项目选择
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 446);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.bindingNavigator1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form_项目选择";
            this.Text = "项目选择";
            this.Load += new System.EventHandler(this.Form_项目选择_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonNo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorOK;
        protected System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.BindingSource sam_itemBindingSource;
        protected System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fselect;
        private System.Windows.Forms.DataGridViewTextBoxColumn s收费编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn s收费名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn i单价;
        private System.Windows.Forms.DataGridViewTextBoxColumn s拼音代码;
        private System.Windows.Forms.DataGridViewTextBoxColumn s归并编码;
    }
}