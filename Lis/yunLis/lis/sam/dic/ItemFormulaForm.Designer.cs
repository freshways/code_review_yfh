﻿namespace yunLis.lis.sam.dic
{
    partial class ItemFormulaForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemFormulaForm));
            this.richTextBoxValue = new System.Windows.Forms.RichTextBox();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.butSave = new System.Windows.Forms.Button();
            this.butDel = new System.Windows.Forms.Button();
            this.buttonJ = new System.Windows.Forms.Button();
            this.buttonJian = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonCu = new System.Windows.Forms.Button();
            this.buttonKH1 = new System.Windows.Forms.Button();
            this.buttonKH2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBoxValue
            // 
            this.richTextBoxValue.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBoxValue.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxValue.Name = "richTextBoxValue";
            this.richTextBoxValue.Size = new System.Drawing.Size(592, 102);
            this.richTextBoxValue.TabIndex = 0;
            this.richTextBoxValue.Text = "";
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.ColumnHeadersHeight = 21;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code,
            this.fname});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Left;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(0, 102);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            this.DataGridViewObject.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 35;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(345, 314);
            this.DataGridViewObject.TabIndex = 122;
            this.DataGridViewObject.Click += new System.EventHandler(this.DataGridViewObject_Click);
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "代号";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.ReadOnly = true;
            this.fname.Width = 240;
            // 
            // butSave
            // 
            this.butSave.Image = ((System.Drawing.Image)(resources.GetObject("butSave.Image")));
            this.butSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.Location = new System.Drawing.Point(388, 357);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(80, 23);
            this.butSave.TabIndex = 123;
            this.butSave.Text = "确定";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butDel
            // 
            this.butDel.Image = ((System.Drawing.Image)(resources.GetObject("butDel.Image")));
            this.butDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butDel.Location = new System.Drawing.Point(485, 357);
            this.butDel.Name = "butDel";
            this.butDel.Size = new System.Drawing.Size(80, 23);
            this.butDel.TabIndex = 124;
            this.butDel.Text = "取消";
            this.butDel.UseVisualStyleBackColor = true;
            this.butDel.Click += new System.EventHandler(this.butDel_Click);
            // 
            // buttonJ
            // 
            this.buttonJ.Location = new System.Drawing.Point(388, 118);
            this.buttonJ.Name = "buttonJ";
            this.buttonJ.Size = new System.Drawing.Size(80, 23);
            this.buttonJ.TabIndex = 125;
            this.buttonJ.Text = "+ 加";
            this.buttonJ.UseVisualStyleBackColor = true;
            this.buttonJ.Click += new System.EventHandler(this.buttonJ_Click);
            // 
            // buttonJian
            // 
            this.buttonJian.Location = new System.Drawing.Point(485, 118);
            this.buttonJian.Name = "buttonJian";
            this.buttonJian.Size = new System.Drawing.Size(80, 23);
            this.buttonJian.TabIndex = 126;
            this.buttonJian.Text = "- 减";
            this.buttonJian.UseVisualStyleBackColor = true;
            this.buttonJian.Click += new System.EventHandler(this.buttonJian_Click);
            // 
            // buttonC
            // 
            this.buttonC.Location = new System.Drawing.Point(485, 160);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(80, 23);
            this.buttonC.TabIndex = 127;
            this.buttonC.Text = "* 乘";
            this.buttonC.UseVisualStyleBackColor = true;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonCu
            // 
            this.buttonCu.Location = new System.Drawing.Point(388, 160);
            this.buttonCu.Name = "buttonCu";
            this.buttonCu.Size = new System.Drawing.Size(80, 23);
            this.buttonCu.TabIndex = 128;
            this.buttonCu.Text = "/ 除";
            this.buttonCu.UseVisualStyleBackColor = true;
            this.buttonCu.Click += new System.EventHandler(this.buttonCu_Click);
            // 
            // buttonKH1
            // 
            this.buttonKH1.Location = new System.Drawing.Point(388, 201);
            this.buttonKH1.Name = "buttonKH1";
            this.buttonKH1.Size = new System.Drawing.Size(80, 23);
            this.buttonKH1.TabIndex = 129;
            this.buttonKH1.Text = "(";
            this.buttonKH1.UseVisualStyleBackColor = true;
            this.buttonKH1.Click += new System.EventHandler(this.buttonKH1_Click);
            // 
            // buttonKH2
            // 
            this.buttonKH2.Location = new System.Drawing.Point(485, 201);
            this.buttonKH2.Name = "buttonKH2";
            this.buttonKH2.Size = new System.Drawing.Size(80, 23);
            this.buttonKH2.TabIndex = 130;
            this.buttonKH2.Text = ")";
            this.buttonKH2.UseVisualStyleBackColor = true;
            this.buttonKH2.Click += new System.EventHandler(this.buttonKH2_Click);
            // 
            // ItemFormulaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 416);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.buttonKH2);
            this.Controls.Add(this.buttonKH1);
            this.Controls.Add(this.buttonCu);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.buttonJian);
            this.Controls.Add(this.buttonJ);
            this.Controls.Add(this.butDel);
            this.Controls.Add(this.butSave);
            this.Controls.Add(this.richTextBoxValue);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ItemFormulaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "计算项目公式";
            this.Load += new System.EventHandler(this.ItemFormulaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxValue;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butDel;
        private System.Windows.Forms.Button buttonJ;
        private System.Windows.Forms.Button buttonJian;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonCu;
        private System.Windows.Forms.Button buttonKH1;
        private System.Windows.Forms.Button buttonKH2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
    }
}