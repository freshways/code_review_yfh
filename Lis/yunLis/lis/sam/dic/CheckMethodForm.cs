﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace yunLis.lis.sam.dic
{
    public partial class CheckMethodForm : ComTypeForm
    {
        public CheckMethodForm()
        {
            InitializeComponent();
        }

        private void CheckMethodForm_Load(object sender, EventArgs e)
        {
            this.strType = "检验方法";
            this.GetComCheckType();
        }
    }
}