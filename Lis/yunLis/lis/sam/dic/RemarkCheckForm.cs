﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace yunLis.lis.sam.dic
{
    public partial class RemarkCheckForm : ComTypeForm
    {
        public RemarkCheckForm()
        {
            InitializeComponent();
        }

        private void RemarkCheckForm_Load(object sender, EventArgs e)
        {
            this.strType = "检验常见备注";
            this.GetComCheckType();
        }
    }
}