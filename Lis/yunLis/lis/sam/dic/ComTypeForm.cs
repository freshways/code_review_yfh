﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using yunLis.lisbll.sam;
using System.Collections;
namespace yunLis.lis.sam.dic
{
    public partial class ComTypeForm : yunLis.wwf.SysBaseForm
    {
        TypeBLL bllType = new TypeBLL();//类别
        DataTable dtCurr = new DataTable();
        DataRowView rowCurr = null;
        private string _strType;
        /// <summary>
        /// 数据库连接字串
        /// </summary>
        protected string strType
        {
            set { _strType = value; }
            get { return _strType; }
        }
        public ComTypeForm()
        {
            InitializeComponent();
        }

        private void ComTypeForm_Load(object sender, EventArgs e)
        {
            this.DataGridViewObject.AutoGenerateColumns = false;           
        }
        /// <summary>
        /// 取得类别
        /// </summary>
        protected void GetComCheckType()
        {
            try
            {                
                dtCurr = this.bllType.BllComTypeDT(this._strType);
                bindingSourceObject.DataSource = dtCurr;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }

        private void bindingSourceObject_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurr = (DataRowView)bindingSourceObject.Current;//行
                /*
                string fname = "";
                if (rowCurrent != null)
                {
                    fname = drObject["fname"].ToString();
                    MessageBox.Show(fname);
                }*/
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }



        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;
                dr = this.dtCurr.NewRow();
                dr["fid"] = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                dr["ftype"] = this._strType;
                dr["fcode"] = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                dr["forder_by"] = "0";
                dr["fuse_if"] = "1";
                if (this.bllType.BllComTypeAdd(dr) > 0)
                {
                    this.dtCurr.Rows.Add(dr);
                    this.bindingSourceObject.Position = bindingSourceObject.Count;
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                for (int i = 0; i < this.dtCurr.Rows.Count; i++)
                {
                    this.bllType.BllComTypeUpdate(this.dtCurr.Rows[i]);
                }
                GetComCheckType();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetComCheckType();
        }

        private void toolStripButtonHelpCode_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                string sqlHelp = "";
                IList lissql = new ArrayList();
                for (int i = 0; i < DataGridViewObject.Rows.Count; i++)
                {
                    sqlHelp = "";
                    try
                    {
                        sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim());
                    }
                    catch { }
                    lissql.Add("UPDATE SAM_TYPE SET fhelp_code = '" + sqlHelp + "' WHERE (fid = '" + DataGridViewObject.Rows[i].Cells["fid"].Value.ToString() + "')");
                    //string ss = DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim() +"-"+ i.ToString();
                    // lissql.Add("UPDATE sam_sample_type SET fname = '" + ss + "' WHERE (fsample_type_id = '" + DataGridViewObject.Rows[i].Cells["fsample_type_id"].Value.ToString() + "')");
                }
                this.bllType.BllComTypeHelpCodeUpdate(lissql);
                GetComCheckType();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

    
    }
}