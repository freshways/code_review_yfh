﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace yunLis.lis.sam.dic
{
    public partial class ComPatientTypeForm : ComTypeForm
    {
        public ComPatientTypeForm()
        {
            InitializeComponent();
        }

        private void ComPatientTypeForm_Load(object sender, EventArgs e)
        {
            this.strType = "病人类别";
            this.GetComCheckType();
        }
    }
}