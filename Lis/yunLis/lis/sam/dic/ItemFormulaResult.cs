﻿using System;
using System.Collections.Generic;
using System.Text;

namespace yunLis.lis.sam.dic
{
    public delegate void TextChangedHandler1(string str1);
    /// <summary>
    /// 计算公式返回
    /// </summary>
    public class ItemFormulaResult
    {
        public event TextChangedHandler1 TextChanged;
        public void ChangeText(string str1)
        {
            if (TextChanged != null)
                TextChanged(str1);
        }
    }
}
