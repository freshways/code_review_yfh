﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwf;
using yunLis.lisbll.sam;
using yunLis.wwfbll;
using System.Collections;
namespace yunLis.lis.sam.dic
{
    public partial class Form_收费项目对照 : SysBaseForm
    {
        /// <summary>
        /// 逻辑_仪器
        /// </summary>
        protected InstrBLL bllInstr = new InstrBLL();
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        TypeBLL bllType = new TypeBLL();//类别
        DataTable dtItemGroup = new DataTable();
        DataTable dt检验项目 = new DataTable();
        string strfgroup_id = "";//组ID
        public Form_收费项目对照()
        {
            InitializeComponent();
            this.DataGridView收费小项.AutoGenerateColumns = false;
        }

        private void Form_收费项目对照_Load(object sender, EventArgs e)
        {
            DataTable dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, yunLis.wwfbll.LoginBLL.strDeptID);//仪器列表
            this.comboBoxInstr.DataSource = dtInstr;
            GetTree("-1");
            
        }
        
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView收费大项.ZADataTable = this.bllItem.BllGe收费大项();
                this.wwTreeView收费大项.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView收费大项.ZAParentFieldName = "fpid";//上级字段名称
                this.wwTreeView收费大项.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView收费大项.ZAKeyFieldName = "fid";//主键字段名称
                this.wwTreeView收费大项.ZAToolTipTextName = "tag";
                this.wwTreeView收费大项.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView收费大项.SelectedNode = wwTreeView收费大项.Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        string str归并编码 = "";

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (this.dtItemGroup.Rows.Count > 0)
                this.dtItemGroup.Clear();
            str归并编码 = "";
            string strEdit = e.Node.ToolTipText;
            if (strEdit == "CheckType")
            {
                bN.Enabled = false;                
            }
            else
            {
                bN.Enabled = true;
                //str归并编码 = e.Node.Parent.Name.ToString();
                str归并编码 = e.Node.Name.ToString();
                GetItemGroup();
            }
        }

        /// <summary>
        /// 取得 项目组
        /// </summary>
        private void GetItemGroup()
        {
            try
            {
                this.dtItemGroup = this.bllItem.BllGet项目列表(1, comboBoxInstr.SelectedValue.ToString(), "");
                this.bindingSource收费小项.DataSource = dtItemGroup;
            }
            catch (Exception ex)
            {
              WWMessage.MessageShowError(ex.Message.ToString());
            }

        }


        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSource收费小项.EndEdit();

                DataRow[] rows = dtItemGroup.Select("选择='1'");
                if(rows!=null &&rows.Length>0)
                {
                    for (int i = 0; i < rows.Length; i++)
                    {
                        string s收费编码 = rows[i]["收费编码"].ToString();
                        string s收费名称 = rows[i]["收费名称"].ToString();
                        string s收费单价 = rows[i]["单价"].ToString();
                        string s检验id = rows[i]["检验ID"].ToString();
                        string s检验编码 = rows[i]["检验编码"].ToString();
                        string s检验名称 = rows[i]["检验项目"].ToString();
                        string s检验设备 = rows[i]["设备"].ToString();

                        if (this.bllItem.Bll检查对应项目(s检验id))
                        {
                            //yunLis.wwfbll.WWMessage.MessageShowWarning("此项目已经存！");
                            this.bllItem.Bll更新对应项目(s收费编码, s收费名称, s收费单价, s检验id, s检验编码, s检验名称, s检验设备);
                        }
                        else
                        {
                            this.bllItem.Bll插入对应项目(s收费编码, s收费名称, s收费单价, s检验id);                            
                        }
                        GetItemGroup();
                    }
                }
                
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 删除对照
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSource收费小项.EndEdit();
                //DataGridView收费小项.
                DataRow[] rows = dtItemGroup.Select("选择='1'");
                if (rows != null && rows.Length > 0)
                {
                    for (int i = 0; i < rows.Length; i++)
                    {
                        string s检验ID = rows[i]["检验ID"].ToString();

                        this.bllItem.BllI删除对应项目(s检验ID);
                        GetItemGroup();
                    }
                }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }


        private void DataGridView收费小项_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (DataGridView收费小项.Columns[e.ColumnIndex].Name == "s收费编码")
            {
                if (true)//Convert.ToString(DataGridView收费小项["s检验项目", e.RowIndex].EditedFormattedValue) == ""
                {
                    Form_项目选择 frm = new Form_项目选择();
                    if (frm.ShowDialog() == DialogResult.Yes)
                    {
                        HIS.Model.GY收费小项 s = frm.gy收费小项;
                        //MessageBox.Show("111！，该功能暂不完善，未开放！");
                        DataGridView收费小项["s收费编码", e.RowIndex].Value =s.收费编码;
                        DataGridView收费小项["s收费名称", e.RowIndex].Value =s.收费名称;
                        DataGridView收费小项["i单价", e.RowIndex].Value =s.单价;
                        //DataGridView收费小项["s设备", e.RowIndex].Value = s[3].ToString();
                        DataGridView收费小项["fuse_if", e.RowIndex].Value = 1;
                        #region 结束当前行编辑，用于刷新缓存值，解决赋值问题
                        if (e.RowIndex == DataGridView收费小项.Rows.Count - 1)
                            this.DataGridView收费小项.CurrentCell = null;
                        else
                            SendKeys.Send("{Down}");
                        #endregion
                    }
                }
            }
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            GetItemGroup();
        }

        private void buttonAll_Click(object sender, EventArgs e)
        {
            GetItemGroup();
        }
        
               
    }
}