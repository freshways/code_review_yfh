﻿namespace yunLis.lis.sam.dic
{
    partial class InstrReportSetForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstrReportSetForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.bindingSourceObject = new System.Windows.Forms.BindingSource(this.components);
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.fuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.finstr_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjygroup_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSourceJYGroup = new System.Windows.Forms.BindingSource(this.components);
            this.fjytype_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSourceJYType = new System.Windows.Forms.BindingSource(this.components);
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ftype_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sam_instr_driveBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new yunLis.lis.sam.samDataSet();
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.repfuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.repfname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fprinter_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fpaper_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fpage_width = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fpage_height = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fmargin_top = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fmargin_bottom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fmargin_left = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fmargin_right = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forientation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reportfremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceJYGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceJYType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_instr_driveBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // bindingSourceObject
            // 
            this.bindingSourceObject.PositionChanged += new System.EventHandler(this.bindingSourceObject_PositionChanged);
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 0);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(160, 491);
            this.wwTreeView1.TabIndex = 117;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(160, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 491);
            this.splitter1.TabIndex = 119;
            this.splitter1.TabStop = false;
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fuse_if,
            this.finstr_id,
            this.fname,
            this.fjygroup_id,
            this.fjytype_id,
            this.forder_by,
            this.fremark,
            this.ftype_id});
            this.DataGridViewObject.DataSource = this.bindingSourceObject;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(163, 0);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            this.DataGridViewObject.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(560, 491);
            this.DataGridViewObject.TabIndex = 120;
            this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            // 
            // fuse_if
            // 
            this.fuse_if.DataPropertyName = "fuse_if";
            this.fuse_if.FalseValue = "0";
            this.fuse_if.HeaderText = "启用";
            this.fuse_if.Name = "fuse_if";
            this.fuse_if.ReadOnly = true;
            this.fuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_if.TrueValue = "1";
            this.fuse_if.Width = 40;
            // 
            // finstr_id
            // 
            this.finstr_id.DataPropertyName = "finstr_id";
            this.finstr_id.HeaderText = "仪器编号";
            this.finstr_id.Name = "finstr_id";
            this.finstr_id.ReadOnly = true;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "仪器名称";
            this.fname.Name = "fname";
            this.fname.ReadOnly = true;
            this.fname.Width = 150;
            // 
            // fjygroup_id
            // 
            this.fjygroup_id.DataPropertyName = "fjygroup_id";
            this.fjygroup_id.DataSource = this.bindingSourceJYGroup;
            this.fjygroup_id.HeaderText = "检验组";
            this.fjygroup_id.Name = "fjygroup_id";
            this.fjygroup_id.ReadOnly = true;
            this.fjygroup_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjygroup_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // fjytype_id
            // 
            this.fjytype_id.DataPropertyName = "fjytype_id";
            this.fjytype_id.DataSource = this.bindingSourceJYType;
            this.fjytype_id.HeaderText = "检验种类";
            this.fjytype_id.Name = "fjytype_id";
            this.fjytype_id.ReadOnly = true;
            this.fjytype_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjytype_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.ReadOnly = true;
            this.forder_by.Width = 60;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            this.fremark.ReadOnly = true;
            // 
            // ftype_id
            // 
            this.ftype_id.DataPropertyName = "ftype_id";
            this.ftype_id.HeaderText = "仪器类型";
            this.ftype_id.Name = "ftype_id";
            this.ftype_id.ReadOnly = true;
            // 
            // sam_instr_driveBindingSource
            // 
            this.sam_instr_driveBindingSource.DataMember = "sam_instr_drive";
            this.sam_instr_driveBindingSource.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToResizeRows = false;
            this.dataGridViewReport.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.repfuse_if,
            this.fcode,
            this.repfname,
            this.f1,
            this.f2,
            this.fprinter_name,
            this.fpaper_name,
            this.fpage_width,
            this.fpage_height,
            this.fmargin_top,
            this.fmargin_bottom,
            this.fmargin_left,
            this.fmargin_right,
            this.forientation,
            this.reportfremark});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReport.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewReport.EnableHeadersVisualStyles = false;
            this.dataGridViewReport.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewReport.MultiSelect = false;
            this.dataGridViewReport.Name = "dataGridViewReport";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.RowTemplate.Height = 23;
            this.dataGridViewReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReport.Size = new System.Drawing.Size(546, 283);
            this.dataGridViewReport.TabIndex = 123;
            this.dataGridViewReport.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewReport_DataError);
            // 
            // repfuse_if
            // 
            this.repfuse_if.DataPropertyName = "fuse_if";
            this.repfuse_if.FalseValue = "0";
            this.repfuse_if.HeaderText = "启";
            this.repfuse_if.IndeterminateValue = "0";
            this.repfuse_if.Name = "repfuse_if";
            this.repfuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.repfuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.repfuse_if.TrueValue = "1";
            this.repfuse_if.Width = 25;
            // 
            // fcode
            // 
            this.fcode.DataPropertyName = "fcode";
            this.fcode.HeaderText = "报表代码";
            this.fcode.Name = "fcode";
            this.fcode.Width = 90;
            // 
            // repfname
            // 
            this.repfname.DataPropertyName = "fname";
            this.repfname.HeaderText = "报表名称";
            this.repfname.Name = "repfname";
            // 
            // f1
            // 
            this.f1.DataPropertyName = "f1";
            this.f1.HeaderText = "检验时间安排";
            this.f1.Name = "f1";
            // 
            // f2
            // 
            this.f2.DataPropertyName = "f2";
            this.f2.HeaderText = "采样地点";
            this.f2.Name = "f2";
            // 
            // fprinter_name
            // 
            this.fprinter_name.DataPropertyName = "fprinter_name";
            this.fprinter_name.HeaderText = "打印机名";
            this.fprinter_name.Name = "fprinter_name";
            this.fprinter_name.Width = 80;
            // 
            // fpaper_name
            // 
            this.fpaper_name.DataPropertyName = "fpaper_name";
            this.fpaper_name.HeaderText = "纸张名";
            this.fpaper_name.Name = "fpaper_name";
            this.fpaper_name.Width = 70;
            // 
            // fpage_width
            // 
            this.fpage_width.DataPropertyName = "fpage_width";
            this.fpage_width.HeaderText = "面宽";
            this.fpage_width.Name = "fpage_width";
            this.fpage_width.Width = 50;
            // 
            // fpage_height
            // 
            this.fpage_height.DataPropertyName = "fpage_height";
            this.fpage_height.HeaderText = "面高";
            this.fpage_height.Name = "fpage_height";
            this.fpage_height.Width = 50;
            // 
            // fmargin_top
            // 
            this.fmargin_top.DataPropertyName = "fmargin_top";
            this.fmargin_top.HeaderText = "边顶";
            this.fmargin_top.Name = "fmargin_top";
            this.fmargin_top.Width = 50;
            // 
            // fmargin_bottom
            // 
            this.fmargin_bottom.DataPropertyName = "fmargin_bottom";
            this.fmargin_bottom.HeaderText = "边底";
            this.fmargin_bottom.Name = "fmargin_bottom";
            this.fmargin_bottom.Width = 50;
            // 
            // fmargin_left
            // 
            this.fmargin_left.DataPropertyName = "fmargin_left";
            this.fmargin_left.HeaderText = "边左";
            this.fmargin_left.Name = "fmargin_left";
            this.fmargin_left.Width = 50;
            // 
            // fmargin_right
            // 
            this.fmargin_right.DataPropertyName = "fmargin_right";
            this.fmargin_right.HeaderText = "边右";
            this.fmargin_right.Name = "fmargin_right";
            this.fmargin_right.Width = 50;
            // 
            // forientation
            // 
            this.forientation.DataPropertyName = "forientation";
            this.forientation.HeaderText = "方向";
            this.forientation.Name = "forientation";
            this.forientation.Width = 50;
            // 
            // reportfremark
            // 
            this.reportfremark.DataPropertyName = "fremark";
            this.reportfremark.HeaderText = "备注";
            this.reportfremark.Name = "reportfremark";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewReport);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(552, 289);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "报表设置";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(163, 147);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(560, 314);
            this.tabControl1.TabIndex = 121;
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.bindingSource1;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonS});
            this.bN.Location = new System.Drawing.Point(163, 461);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(560, 30);
            this.bN.TabIndex = 123;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // InstrReportSetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 491);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.bN);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.wwTreeView1);
            this.Name = "InstrReportSetForm";
            this.Text = "仪器报表设置";
            this.Load += new System.EventHandler(this.InstrReportSetForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceJYGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceJYType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_instr_driveBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion


        

        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.BindingSource bindingSourceObject;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.BindingSource bindingSourceJYGroup;
        private System.Windows.Forms.BindingSource bindingSourceJYType;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn finstr_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewComboBoxColumn fjygroup_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fjytype_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftype_id;
        private System.Windows.Forms.BindingSource sam_instr_driveBindingSource;
        private yunLis.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.DataGridView dataGridViewReport;
        private System.Windows.Forms.DataGridViewCheckBoxColumn repfuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn repfname;
        private System.Windows.Forms.DataGridViewTextBoxColumn f1;
        private System.Windows.Forms.DataGridViewTextBoxColumn f2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprinter_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fpaper_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fpage_width;
        private System.Windows.Forms.DataGridViewTextBoxColumn fpage_height;
        private System.Windows.Forms.DataGridViewTextBoxColumn fmargin_top;
        private System.Windows.Forms.DataGridViewTextBoxColumn fmargin_bottom;
        private System.Windows.Forms.DataGridViewTextBoxColumn fmargin_left;
        private System.Windows.Forms.DataGridViewTextBoxColumn fmargin_right;
        private System.Windows.Forms.DataGridViewTextBoxColumn forientation;
        private System.Windows.Forms.DataGridViewTextBoxColumn reportfremark;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}