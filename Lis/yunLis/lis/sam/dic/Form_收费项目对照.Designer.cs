﻿namespace yunLis.lis.sam.dic
{
    partial class Form_收费项目对照
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_收费项目对照));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bindingSource收费小项 = new System.Windows.Forms.BindingSource(this.components);
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.DataGridView收费小项 = new System.Windows.Forms.DataGridView();
            this.fuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.s检验id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s检验项目 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s检验编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s设备 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s助记符 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s收费编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s收费名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.i单价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s归类编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.wwTreeView收费大项 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonAll = new System.Windows.Forms.Button();
            this.comboBoxInstr = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource收费小项)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView收费小项)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // DataGridView收费小项
            // 
            this.DataGridView收费小项.AllowUserToAddRows = false;
            this.DataGridView收费小项.AllowUserToDeleteRows = false;
            this.DataGridView收费小项.AllowUserToResizeRows = false;
            this.DataGridView收费小项.AutoGenerateColumns = false;
            this.DataGridView收费小项.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridView收费小项.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView收费小项.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridView收费小项.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fuse_if,
            this.s检验id,
            this.s检验项目,
            this.s检验编码,
            this.s设备,
            this.s助记符,
            this.s收费编码,
            this.s收费名称,
            this.i单价,
            this.s归类编码});
            this.DataGridView收费小项.DataSource = this.bindingSource收费小项;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridView收费小项.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridView收费小项.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridView收费小项.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridView收费小项.EnableHeadersVisualStyles = false;
            this.DataGridView收费小项.Location = new System.Drawing.Point(183, 66);
            this.DataGridView收费小项.MultiSelect = false;
            this.DataGridView收费小项.Name = "DataGridView收费小项";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView收费小项.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridView收费小项.RowHeadersVisible = false;
            this.DataGridView收费小项.RowTemplate.Height = 23;
            this.DataGridView收费小项.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridView收费小项.Size = new System.Drawing.Size(852, 443);
            this.DataGridView收费小项.TabIndex = 132;
            this.DataGridView收费小项.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridView收费小项_CellMouseDoubleClick);
            // 
            // fuse_if
            // 
            this.fuse_if.DataPropertyName = "选择";
            this.fuse_if.FalseValue = "0";
            this.fuse_if.HeaderText = "选择";
            this.fuse_if.IndeterminateValue = "0";
            this.fuse_if.Name = "fuse_if";
            this.fuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_if.TrueValue = "1";
            this.fuse_if.Width = 40;
            // 
            // s检验id
            // 
            this.s检验id.DataPropertyName = "检验id";
            this.s检验id.HeaderText = "检验id";
            this.s检验id.Name = "s检验id";
            this.s检验id.Visible = false;
            // 
            // s检验项目
            // 
            this.s检验项目.DataPropertyName = "检验项目";
            this.s检验项目.HeaderText = "检验项目";
            this.s检验项目.Name = "s检验项目";
            this.s检验项目.ReadOnly = true;
            this.s检验项目.Width = 120;
            // 
            // s检验编码
            // 
            this.s检验编码.DataPropertyName = "检验编码";
            this.s检验编码.HeaderText = "检验编码";
            this.s检验编码.Name = "s检验编码";
            this.s检验编码.ReadOnly = true;
            // 
            // s设备
            // 
            this.s设备.DataPropertyName = "设备";
            this.s设备.HeaderText = "设备";
            this.s设备.Name = "s设备";
            this.s设备.ReadOnly = true;
            // 
            // s助记符
            // 
            this.s助记符.DataPropertyName = "助记符";
            this.s助记符.HeaderText = "助记符";
            this.s助记符.Name = "s助记符";
            this.s助记符.ReadOnly = true;
            // 
            // s收费编码
            // 
            this.s收费编码.DataPropertyName = "收费编码";
            this.s收费编码.HeaderText = "收费编码";
            this.s收费编码.Name = "s收费编码";
            // 
            // s收费名称
            // 
            this.s收费名称.DataPropertyName = "收费名称";
            this.s收费名称.HeaderText = "收费名称";
            this.s收费名称.Name = "s收费名称";
            this.s收费名称.ReadOnly = true;
            this.s收费名称.Width = 200;
            // 
            // i单价
            // 
            this.i单价.DataPropertyName = "单价";
            this.i单价.HeaderText = "单价";
            this.i单价.Name = "i单价";
            this.i单价.ReadOnly = true;
            this.i单价.Width = 80;
            // 
            // s归类编码
            // 
            this.s归类编码.DataPropertyName = "归并编码";
            this.s归类编码.HeaderText = "归类编码";
            this.s归类编码.Name = "s归类编码";
            this.s归类编码.Visible = false;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(180, 66);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 443);
            this.splitter1.TabIndex = 131;
            this.splitter1.TabStop = false;
            // 
            // wwTreeView收费大项
            // 
            this.wwTreeView收费大项.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView收费大项.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView收费大项.ImageIndex = 0;
            this.wwTreeView收费大项.ImageList = this.imageListTree;
            this.wwTreeView收费大项.Location = new System.Drawing.Point(0, 30);
            this.wwTreeView收费大项.Name = "wwTreeView收费大项";
            this.wwTreeView收费大项.SelectedImageIndex = 1;
            this.wwTreeView收费大项.Size = new System.Drawing.Size(180, 479);
            this.wwTreeView收费大项.TabIndex = 130;
            this.wwTreeView收费大项.ZADataTable = null;
            this.wwTreeView收费大项.ZADisplayFieldName = "";
            this.wwTreeView收费大项.ZAKeyFieldName = "";
            this.wwTreeView收费大项.ZAParentFieldName = "";
            this.wwTreeView收费大项.ZAToolTipTextName = "";
            this.wwTreeView收费大项.ZATreeViewRootValue = "";
            this.wwTreeView收费大项.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.bindingSource收费小项;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonS,
            this.toolStripButtonDel,
            this.toolStripButtonQuery});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(1035, 30);
            this.bN.TabIndex = 129;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Enabled = false;
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(76, 27);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(75, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(77, 27);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = global::yunLis.Properties.Resources.refresh;
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQuery.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQuery.Size = new System.Drawing.Size(73, 27);
            this.toolStripButtonQuery.Text = "刷新(F5)";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonAll);
            this.panel1.Controls.Add(this.comboBoxInstr);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(180, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(855, 36);
            this.panel1.TabIndex = 134;
            // 
            // buttonAll
            // 
            this.buttonAll.Image = ((System.Drawing.Image)(resources.GetObject("buttonAll.Image")));
            this.buttonAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAll.Location = new System.Drawing.Point(275, 5);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(85, 23);
            this.buttonAll.TabIndex = 4;
            this.buttonAll.Text = "所有项目 ";
            this.buttonAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAll.UseVisualStyleBackColor = true;
            this.buttonAll.Click += new System.EventHandler(this.buttonAll_Click);
            // 
            // comboBoxInstr
            // 
            this.comboBoxInstr.DisplayMember = "ShowName";
            this.comboBoxInstr.FormattingEnabled = true;
            this.comboBoxInstr.Location = new System.Drawing.Point(83, 6);
            this.comboBoxInstr.Name = "comboBoxInstr";
            this.comboBoxInstr.Size = new System.Drawing.Size(186, 20);
            this.comboBoxInstr.TabIndex = 3;
            this.comboBoxInstr.ValueMember = "finstr_id";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "仪器过滤:";
            // 
            // Form_收费项目对照
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1035, 509);
            this.Controls.Add(this.DataGridView收费小项);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.wwTreeView收费大项);
            this.Controls.Add(this.bN);
            this.Name = "Form_收费项目对照";
            this.Text = "收费项目对照";
            this.Load += new System.EventHandler(this.Form_收费项目对照_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource收费小项)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView收费小项)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView收费大项;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView DataGridView收费小项;
        private System.Windows.Forms.BindingSource bindingSource收费小项;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonAll;
        private System.Windows.Forms.ComboBox comboBoxInstr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn s检验id;
        private System.Windows.Forms.DataGridViewTextBoxColumn s检验项目;
        private System.Windows.Forms.DataGridViewTextBoxColumn s检验编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn s设备;
        private System.Windows.Forms.DataGridViewTextBoxColumn s助记符;
        private System.Windows.Forms.DataGridViewTextBoxColumn s收费编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn s收费名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn i单价;
        private System.Windows.Forms.DataGridViewTextBoxColumn s归类编码;
    }
}