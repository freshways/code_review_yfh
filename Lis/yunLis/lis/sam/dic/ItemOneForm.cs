﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwf;
using yunLis.lisbll.sam;
using yunLis.wwfbll;
using System.Collections;
namespace yunLis.lis.sam.dic
{
    public partial class ItemOneForm : SysBaseForm
    {
        TypeBLL bllType = new TypeBLL();//公共类型逻辑
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        DataTable dtItemOne = new DataTable();
        public ItemOneForm()
        {
            InitializeComponent();
            this.DataGridViewObject.AutoGenerateColumns = false;
        }

        private void ItemOneForm_Load(object sender, EventArgs e)
        {
            GetComItemType();
            GetTree("-1");
           
        }
        /// <summary> 
        /// 取得公共类型 1。 结果类型；2。单位
        /// </summary>
        private void GetComItemType()
        {
            try
            {
                this.com_listBindingSourceUnit.DataSource = this.bllType.BllComTypeDT("常用单位", 1,"");

                this.bindingSource_fcheck_type_id.DataSource = this.bllType.BllCheckTypeDT(1);

                this.bindingSource_fsam_type_id.DataSource = this.bllType.BllSamTypeDT(1);

                DataTable dtInstr = this.bllItem.BllInstrDT(1, LoginBLL.strDeptID);
                comboBoxInstr.DataSource = dtInstr;
                this.bindingSource_finstr_id.DataSource = dtInstr;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bllItem.BllCheckAndSampleTypeDT();
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fpid";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fid";//主键字段名称
                this.wwTreeView1.ZAToolTipTextName = "tag";
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
       
        string strfcheck_type_id = "";
        string strfsample_type_id = "";
        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (this.dtItemOne.Rows.Count > 0)
                this.dtItemOne.Clear();
            strfcheck_type_id = "";
            strfsample_type_id = "";
            string strEdit = e.Node.ToolTipText;
            if (strEdit == "CheckType")
            {
                bN.Enabled = false;                
            }
            else
            {
                bN.Enabled = true;
                strfcheck_type_id = e.Node.Parent.Name.ToString();
                strfsample_type_id = e.Node.Name.ToString();
                GetItemOne();
                //MessageBox.Show("检验类别：" + strfcheck_type_id);
                //MessageBox.Show("样本类别为：" + strfsample_type_id);
            }
        }

        /// <summary>
        /// 取得 项目组
        /// </summary>
        private void GetItemOne()
        {
            try
            {
                this.dtItemOne = this.bllItem.BllItem(" fcheck_type_id='" + strfcheck_type_id + "' and fsam_type_id='" + strfsample_type_id + "'");
                this.bindingSourceItemOne.DataSource = dtItemOne;
            }
            catch (Exception ex)
            {
              WWMessage.MessageShowError(ex.Message.ToString());
            }

        }

        private void toolStripButtonHelpCode_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceItemOne.EndEdit();
                string sqlHelp = "";
                IList lissql = new ArrayList();
                for (int i = 0; i < DataGridViewObject.Rows.Count; i++)
                {
                    sqlHelp = "";
                    try
                    {
                        sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim());
                    }
                    catch { }
                    lissql.Add("UPDATE SAM_ITEM SET fhelp_code = '" + sqlHelp + "' WHERE (fitem_id = '" + DataGridViewObject.Rows[i].Cells["fitem_id"].Value.ToString() + "')");
                    //string ss = DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim() +"-"+ i.ToString();
                    // lissql.Add("UPDATE sam_sample_type SET fname = '" + ss + "' WHERE (fsample_type_id = '" + DataGridViewObject.Rows[i].Cells["fsample_type_id"].Value.ToString() + "')");
                }
                this.bllItem.BllItemHelpCodeUpdate(lissql);
                GetItemOne();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {               
                if (strfsample_type_id == "" || strfsample_type_id == null)
                {
                    WWMessage.MessageShowWarning("样本类别不能为空，请选择后重试！");
                    return;
                }              
                if (this.bllItem.BllItemAdd("", WEISHENG.COMM.Helper.GuidHelper.DbGuid(), strfcheck_type_id, strfsample_type_id) > 0)
                {
                    GetItemOne();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.bindingSourceItemOne.EndEdit();
                for (int i = 0; i < this.dtItemOne.Rows.Count; i++)
                {
                    this.bllItem.BllItemUpdate(this.dtItemOne.Rows[i]);

                }
                GetItemOne();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceItemOne.EndEdit();
                if (DataGridViewObject.Rows.Count > 0)
                {
                    if (WWMessage.MessageDialogResult("确认删除？"))
                    {
                        string strRet = this.bllItem.BllItemDel(this.strCurrItemGuid);
                        if (strRet == "true")
                            GetItemOne();
                        else
                            WWMessage.MessageShowError(strRet);
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        DataRowView rowCurrItem = null;
        string strCurrItemGuid = "";
        private void bindingSourceItemOne_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrItem = (DataRowView)bindingSourceItemOne.Current;//行
                if (rowCurrItem != null)
                {
                    strCurrItemGuid = rowCurrItem["fitem_id"].ToString();
                    
                }
                else
                {                   
                    strCurrItemGuid = "";
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void OpenDec()
        {
            try
            {
                this.Validate();
                this.bindingSourceItemOne.EndEdit();
                ItemDecForm itemdec = new ItemDecForm(strCurrItemGuid);
                itemdec.ShowDialog();
                GetItemOne(); 
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
       

        private void toolStripButtonDec_Click(object sender, EventArgs e)
        {
            OpenDec();
        }

        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }


        private void comboBoxInstr_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                string strfinstr_id = comboBoxInstr.SelectedValue.ToString();
               
                this.dtItemOne = this.bllItem.BllItem("finstr_id='" + strfinstr_id + "'");
                this.bindingSourceItemOne.DataSource = dtItemOne;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void buttonAll_Click(object sender, EventArgs e)
        {
            try
            {
                this.dtItemOne = this.bllItem.BllItem("");
                this.bindingSourceItemOne.DataSource = dtItemOne;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void DataGridViewObject_DoubleClick(object sender, EventArgs e)
        {
            OpenDec();
        }

      
    }
}