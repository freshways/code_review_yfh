﻿namespace yunLis.lis.sam.dic
{
    partial class InstrForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstrForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.bindingSourceObject = new System.Windows.Forms.BindingSource(this.components);
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.textBoxReturn = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.fuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.finstr_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjygroup_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSourceJYGroup = new System.Windows.Forms.BindingSource(this.components);
            this.fjytype_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bindingSourceJYType = new System.Windows.Forms.BindingSource(this.components);
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ftype_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sam_instr_driveBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new yunLis.lis.sam.samDataSet();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.fport = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fbaud_rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fdata_bit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fstop_bit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fparity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finfo_mode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Intfremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonR = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDrive = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceJYGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceJYType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_instr_driveBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // bindingSourceObject
            // 
            this.bindingSourceObject.PositionChanged += new System.EventHandler(this.bindingSourceObject_PositionChanged);
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 30);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(200, 532);
            this.wwTreeView1.TabIndex = 117;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // textBoxReturn
            // 
            this.textBoxReturn.Location = new System.Drawing.Point(57, 529);
            this.textBoxReturn.Name = "textBoxReturn";
            this.textBoxReturn.Size = new System.Drawing.Size(100, 21);
            this.textBoxReturn.TabIndex = 123;
            this.textBoxReturn.Visible = false;
            this.textBoxReturn.TextChanged += new System.EventHandler(this.textBoxReturn_TextChanged);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(200, 30);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 532);
            this.splitter1.TabIndex = 119;
            this.splitter1.TabStop = false;
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToResizeColumns = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fuse_if,
            this.finstr_id,
            this.fname,
            this.fjygroup_id,
            this.fjytype_id,
            this.forder_by,
            this.fremark,
            this.ftype_id});
            this.DataGridViewObject.DataSource = this.bindingSourceObject;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle8;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(203, 30);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(589, 416);
            this.DataGridViewObject.TabIndex = 120;
            this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            // 
            // fuse_if
            // 
            this.fuse_if.DataPropertyName = "fuse_if";
            this.fuse_if.FalseValue = "0";
            this.fuse_if.HeaderText = "启用";
            this.fuse_if.Name = "fuse_if";
            this.fuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_if.TrueValue = "1";
            this.fuse_if.Width = 40;
            // 
            // finstr_id
            // 
            this.finstr_id.DataPropertyName = "finstr_id";
            this.finstr_id.HeaderText = "仪器编号";
            this.finstr_id.Name = "finstr_id";
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "仪器名称";
            this.fname.Name = "fname";
            this.fname.Width = 150;
            // 
            // fjygroup_id
            // 
            this.fjygroup_id.DataPropertyName = "fjygroup_id";
            this.fjygroup_id.DataSource = this.bindingSourceJYGroup;
            this.fjygroup_id.HeaderText = "检验组";
            this.fjygroup_id.Name = "fjygroup_id";
            this.fjygroup_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjygroup_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // fjytype_id
            // 
            this.fjytype_id.DataPropertyName = "fjytype_id";
            this.fjytype_id.DataSource = this.bindingSourceJYType;
            this.fjytype_id.HeaderText = "检验种类";
            this.fjytype_id.Name = "fjytype_id";
            this.fjytype_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjytype_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Width = 60;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            // 
            // ftype_id
            // 
            this.ftype_id.DataPropertyName = "ftype_id";
            this.ftype_id.HeaderText = "仪器类型";
            this.ftype_id.Name = "ftype_id";
            this.ftype_id.ReadOnly = true;
            this.ftype_id.Visible = false;
            // 
            // sam_instr_driveBindingSource
            // 
            this.sam_instr_driveBindingSource.DataMember = "sam_instr_drive";
            this.sam_instr_driveBindingSource.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fport,
            this.fbaud_rate,
            this.fdata_bit,
            this.fstop_bit,
            this.fparity,
            this.finfo_mode,
            this.Intfremark});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(575, 84);
            this.dataGridView1.TabIndex = 122;
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            // 
            // fport
            // 
            this.fport.DataPropertyName = "fport";
            this.fport.HeaderText = "通信端口";
            this.fport.Name = "fport";
            this.fport.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // fbaud_rate
            // 
            this.fbaud_rate.DataPropertyName = "fbaud_rate";
            this.fbaud_rate.HeaderText = "波特率";
            this.fbaud_rate.Name = "fbaud_rate";
            this.fbaud_rate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // fdata_bit
            // 
            this.fdata_bit.DataPropertyName = "fdata_bit";
            this.fdata_bit.HeaderText = "数据位";
            this.fdata_bit.Name = "fdata_bit";
            this.fdata_bit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // fstop_bit
            // 
            this.fstop_bit.DataPropertyName = "fstop_bit";
            this.fstop_bit.HeaderText = "停止位";
            this.fstop_bit.Name = "fstop_bit";
            this.fstop_bit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // fparity
            // 
            this.fparity.DataPropertyName = "fparity";
            this.fparity.HeaderText = "校验";
            this.fparity.Name = "fparity";
            this.fparity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // finfo_mode
            // 
            this.finfo_mode.DataPropertyName = "finfo_mode";
            this.finfo_mode.HeaderText = "通信模式";
            this.finfo_mode.Name = "finfo_mode";
            this.finfo_mode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Intfremark
            // 
            this.Intfremark.DataPropertyName = "fremark";
            this.Intfremark.HeaderText = "备注";
            this.Intfremark.Name = "Intfremark";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(203, 446);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(589, 116);
            this.tabControl1.TabIndex = 121;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(581, 90);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "通信参数";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.bindingSourceObject;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonS,
            this.toolStripSeparator1,
            this.toolStripButtonUpdate,
            this.toolStripSeparator2,
            this.toolStripButtonDel,
            this.toolStripButtonR,
            this.toolStripButtonDrive});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(792, 30);
            this.bN.TabIndex = 122;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(80, 27);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonUpdate.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(81, 27);
            this.toolStripButtonUpdate.Text = "修改(&U)  ";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(81, 27);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Visible = false;
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // toolStripButtonR
            // 
            this.toolStripButtonR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonR.Image")));
            this.toolStripButtonR.Name = "toolStripButtonR";
            this.toolStripButtonR.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonR.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonR.Size = new System.Drawing.Size(80, 27);
            this.toolStripButtonR.Text = "刷新(&R)  ";
            this.toolStripButtonR.Click += new System.EventHandler(this.toolStripButtonR_Click);
            // 
            // toolStripButtonDrive
            // 
            this.toolStripButtonDrive.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDrive.Image")));
            this.toolStripButtonDrive.Name = "toolStripButtonDrive";
            this.toolStripButtonDrive.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDrive.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDrive.Size = new System.Drawing.Size(80, 27);
            this.toolStripButtonDrive.Text = "通信参数";
            this.toolStripButtonDrive.Click += new System.EventHandler(this.toolStripButtonDrive_Click);
            // 
            // InstrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 562);
            this.Controls.Add(this.textBoxReturn);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.wwTreeView1);
            this.Controls.Add(this.bN);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "InstrForm";
            this.Text = "InstrForm";
            this.Load += new System.EventHandler(this.InstrForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceJYGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceJYType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sam_instr_driveBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        

        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.BindingSource bindingSourceObject;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.BindingSource bindingSourceJYGroup;
        private System.Windows.Forms.BindingSource bindingSourceJYType;
        private System.Windows.Forms.BindingSource sam_instr_driveBindingSource;
        private yunLis.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TextBox textBoxReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fport;
        private System.Windows.Forms.DataGridViewTextBoxColumn fbaud_rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn fdata_bit;
        private System.Windows.Forms.DataGridViewTextBoxColumn fstop_bit;
        private System.Windows.Forms.DataGridViewTextBoxColumn fparity;
        private System.Windows.Forms.DataGridViewTextBoxColumn finfo_mode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Intfremark;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        protected System.Windows.Forms.ToolStripButton toolStripButtonR;
        private System.Windows.Forms.ToolStripButton toolStripButtonDrive;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn finstr_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewComboBoxColumn fjygroup_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fjytype_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftype_id;
    }
}