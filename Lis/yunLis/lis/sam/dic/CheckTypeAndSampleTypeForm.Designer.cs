﻿namespace yunLis.lis.sam.dic
{
    partial class CheckTypeAndSampleTypeForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.fuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fcheck_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcheck_type_id_01 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSourceCheckType = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewSampletypeAll = new System.Windows.Forms.DataGridView();
            this.fname_all = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhelp_code_all = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsample_type_id_all = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewSampletypeOk = new System.Windows.Forms.DataGridView();
            this.fname_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhelp_code_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fid_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonNo = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCheckType)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSampletypeAll)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSampletypeOk)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fuse_if,
            this.fcheck_type_id,
            this.fname,
            this.forder_by,
            this.fremark,
            this.fcheck_type_id_01});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(686, 168);
            this.DataGridViewObject.TabIndex = 121;
            // 
            // fuse_if
            // 
            this.fuse_if.DataPropertyName = "fuse_if";
            this.fuse_if.FalseValue = "0";
            this.fuse_if.HeaderText = "启用";
            this.fuse_if.Name = "fuse_if";
            this.fuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_if.TrueValue = "1";
            this.fuse_if.Width = 40;
            // 
            // fcheck_type_id
            // 
            this.fcheck_type_id.DataPropertyName = "fcheck_type_id";
            this.fcheck_type_id.HeaderText = "编号";
            this.fcheck_type_id.Name = "fcheck_type_id";
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.Width = 200;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Width = 60;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            this.fremark.Width = 200;
            // 
            // fcheck_type_id_01
            // 
            this.fcheck_type_id_01.DataPropertyName = "fcheck_type_id";
            this.fcheck_type_id_01.HeaderText = "fcheck_type_id";
            this.fcheck_type_id_01.Name = "fcheck_type_id_01";
            this.fcheck_type_id_01.Visible = false;
            // 
            // bindingSourceCheckType
            // 
            this.bindingSourceCheckType.PositionChanged += new System.EventHandler(this.bindingSourceCheckType_PositionChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DataGridViewObject);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(692, 188);
            this.groupBox1.TabIndex = 122;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "检验类别";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewSampletypeAll);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(0, 188);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 318);
            this.groupBox2.TabIndex = 123;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "样本类别 - 所有";
            // 
            // dataGridViewSampletypeAll
            // 
            this.dataGridViewSampletypeAll.AllowUserToAddRows = false;
            this.dataGridViewSampletypeAll.AllowUserToResizeRows = false;
            this.dataGridViewSampletypeAll.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewSampletypeAll.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSampletypeAll.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewSampletypeAll.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fname_all,
            this.fhelp_code_all,
            this.fsample_type_id_all});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewSampletypeAll.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewSampletypeAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSampletypeAll.EnableHeadersVisualStyles = false;
            this.dataGridViewSampletypeAll.Location = new System.Drawing.Point(3, 38);
            this.dataGridViewSampletypeAll.MultiSelect = false;
            this.dataGridViewSampletypeAll.Name = "dataGridViewSampletypeAll";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSampletypeAll.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewSampletypeAll.RowHeadersVisible = false;
            this.dataGridViewSampletypeAll.RowTemplate.Height = 23;
            this.dataGridViewSampletypeAll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSampletypeAll.Size = new System.Drawing.Size(248, 277);
            this.dataGridViewSampletypeAll.TabIndex = 122;
            // 
            // fname_all
            // 
            this.fname_all.DataPropertyName = "fname";
            this.fname_all.HeaderText = "名称";
            this.fname_all.Name = "fname_all";
            // 
            // fhelp_code_all
            // 
            this.fhelp_code_all.DataPropertyName = "fhelp_code";
            this.fhelp_code_all.HeaderText = "助记符";
            this.fhelp_code_all.Name = "fhelp_code_all";
            // 
            // fsample_type_id_all
            // 
            this.fsample_type_id_all.DataPropertyName = "fsample_type_id";
            this.fsample_type_id_all.HeaderText = "ID";
            this.fsample_type_id_all.Name = "fsample_type_id_all";
            this.fsample_type_id_all.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Location = new System.Drawing.Point(3, 17);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(248, 21);
            this.textBox1.TabIndex = 124;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridViewSampletypeOk);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(368, 188);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(324, 318);
            this.groupBox3.TabIndex = 123;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "样本类别 - 已经分配";
            // 
            // dataGridViewSampletypeOk
            // 
            this.dataGridViewSampletypeOk.AllowUserToAddRows = false;
            this.dataGridViewSampletypeOk.AllowUserToResizeRows = false;
            this.dataGridViewSampletypeOk.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewSampletypeOk.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSampletypeOk.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewSampletypeOk.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fname_ok,
            this.fhelp_code_ok,
            this.forder_by_ok,
            this.fid_ok});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewSampletypeOk.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewSampletypeOk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSampletypeOk.EnableHeadersVisualStyles = false;
            this.dataGridViewSampletypeOk.Location = new System.Drawing.Point(3, 17);
            this.dataGridViewSampletypeOk.MultiSelect = false;
            this.dataGridViewSampletypeOk.Name = "dataGridViewSampletypeOk";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSampletypeOk.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewSampletypeOk.RowHeadersVisible = false;
            this.dataGridViewSampletypeOk.RowTemplate.Height = 23;
            this.dataGridViewSampletypeOk.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSampletypeOk.Size = new System.Drawing.Size(318, 298);
            this.dataGridViewSampletypeOk.TabIndex = 123;
            // 
            // fname_ok
            // 
            this.fname_ok.DataPropertyName = "fname";
            this.fname_ok.HeaderText = "名称";
            this.fname_ok.Name = "fname_ok";
            // 
            // fhelp_code_ok
            // 
            this.fhelp_code_ok.DataPropertyName = "fhelp_code";
            this.fhelp_code_ok.HeaderText = "助记符";
            this.fhelp_code_ok.Name = "fhelp_code_ok";
            // 
            // forder_by_ok
            // 
            this.forder_by_ok.DataPropertyName = "forder_by";
            this.forder_by_ok.HeaderText = "序";
            this.forder_by_ok.Name = "forder_by_ok";
            // 
            // fid_ok
            // 
            this.fid_ok.DataPropertyName = "fid";
            this.fid_ok.HeaderText = "ID";
            this.fid_ok.Name = "fid_ok";
            this.fid_ok.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonNo);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(254, 188);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(114, 318);
            this.panel1.TabIndex = 124;
            // 
            // buttonNo
            // 
            this.buttonNo.Location = new System.Drawing.Point(19, 101);
            this.buttonNo.Name = "buttonNo";
            this.buttonNo.Size = new System.Drawing.Size(75, 23);
            this.buttonNo.TabIndex = 1;
            this.buttonNo.Text = "取消";
            this.buttonNo.UseVisualStyleBackColor = true;
            this.buttonNo.Click += new System.EventHandler(this.buttonNo_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(19, 40);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "分配";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // CheckTypeAndSampleTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 506);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CheckTypeAndSampleTypeForm";
            this.Text = "检验类别和样本类别关系";
            this.Load += new System.EventHandler(this.CheckTypeAndSampleTypeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCheckType)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSampletypeAll)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSampletypeOk)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.BindingSource bindingSourceCheckType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonNo;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.DataGridView dataGridViewSampletypeAll;
        private System.Windows.Forms.DataGridView dataGridViewSampletypeOk;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcheck_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcheck_type_id_01;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname_all;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhelp_code_all;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsample_type_id_all;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhelp_code_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn fid_ok;
    }
}