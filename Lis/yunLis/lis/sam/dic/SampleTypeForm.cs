﻿using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using WEISHENG.COMM.PluginsAttribute;
using yunLis.lisbll.sam;
namespace yunLis.lis.sam.dic
{
    [ClassInfoMark(GUID = "8EA123B0-439F-4B56-98B9-1DFD3810E508", 键ID = "8EA123B0-439F-4B56-98B9-1DFD3810E508", 父ID = "864BDFE3-63E6-408F-98F3-270953B15BCD",
     功能名称 = "样本类型", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.dic.SampleTypeForm", 显示顺序 = 4, 传递参数 = "",
     菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public partial class SampleTypeForm : yunLis.wwf.SysBaseForm
    {
        SampleTypeBLL bll = new SampleTypeBLL();
        DataTable dtCurr = new DataTable();
       
        public SampleTypeForm()
        {
            InitializeComponent();
            this.DataGridViewObject.AutoGenerateColumns = false;
            
        }
        private void ApplyUserForm_Load(object sender, EventArgs e)
        {
            GetDT();
        }

       

        private void ReturnValue()
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                for(int i=0;i<this.dtCurr.Rows.Count;i++)
                {
                    this.bll.BllUpdate(this.dtCurr.Rows[i]);
                    
                }
                GetDT();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        

        private void GetDT()
        {
            try
            {
                this.dtCurr = this.bll.BllDT(" WHERE (fhelp_code LIKE '%" + this.textBox1.Text + "%') "); // (fuse_if = 1) AND
               
                bindingSourceObject.DataSource = dtCurr;

                this.DataGridViewObject.DataSource = bindingSourceObject;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            GetDT();
        }

        

        private void DataGridViewObject_DoubleClick(object sender, EventArgs e)
        {
            ReturnValue();
        }

        private void DataGridViewObject_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                ReturnValue();
            }
        }

        
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;
                dr = this.dtCurr.NewRow();

                dr["fsample_type_id"] = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                dr["forder_by"] = "0";
                dr["fuse_if"] = "1";

                if (this.bll.BllAdd(dr) > 0)
                {
                    this.dtCurr.Rows.Add(dr);

                    this.bindingSourceObject.Position = bindingSourceObject.Count;
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            ReturnValue();
        }

        private void toolStripButtonHelpCode_Click(object sender, EventArgs e)
        {
            try
            {
                string sqlHelp = "";
                IList lissql = new ArrayList();
                for (int i = 0; i < DataGridViewObject.Rows.Count; i++)
                {
                    sqlHelp = "";
                    try
                    {
                        sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim());
                    }
                    catch { }
                    lissql.Add("UPDATE sam_sample_type SET fhelp_code = '" + sqlHelp + "' WHERE (fsample_type_id = '" + DataGridViewObject.Rows[i].Cells["fsample_type_id"].Value.ToString() + "')");
                    //string ss = DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim() +"-"+ i.ToString();
                    // lissql.Add("UPDATE sam_sample_type SET fname = '" + ss + "' WHERE (fsample_type_id = '" + DataGridViewObject.Rows[i].Cells["fsample_type_id"].Value.ToString() + "')");
                }
                this.bll.BllPersonfhelp_codeUpdate(lissql);
                GetDT();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetDT();
        }

      

        
    }
}