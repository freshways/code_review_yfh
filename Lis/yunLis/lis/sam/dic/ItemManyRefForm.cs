﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace yunLis.lis.sam.dic
{
    public partial class ItemManyRefForm : Form
    {
        public ItemManyRefForm()
        {
            InitializeComponent();
        }
        private ItemFormulaResult r;
        public ItemManyRefForm(ItemFormulaResult r)
            : this()
        {
            this.r = r;
        }
        private void ItemManyRefForm_Load(object sender, EventArgs e)
        {

        }

        private void butSave_Click(object sender, EventArgs e)
        {         
            
            r.ChangeText(this.richTextBoxValue.Text);
            this.Close();
        }

        private void butDel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}