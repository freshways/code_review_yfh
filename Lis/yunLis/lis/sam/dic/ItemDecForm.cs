﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using yunLis.lisbll.sam;

namespace yunLis.lis.sam.dic
{
   // public partial class ItemDecForm : SysBaseForm
    public partial class ItemDecForm : Form
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        TypeBLL bllType = new TypeBLL();//公共类型逻辑
        DataTable dtItem = new DataTable();//当前项目表     
        DataTable dtRef = new DataTable();//当前参考值明细
        //DataRowView rowCurrItem = null;//当前项目行
        string strCurrItemGuid = "";//当前行Guid
        //string strInstrID = "";//仪器ID

        DataTable dtInstr = new DataTable(); //fjytype_id
      //  string strfjytype_id = "";//检验类别为
       
        public ItemDecForm(string fitem_id)
        {
            InitializeComponent();
            this.fcheck_type_idComboBox.DisplayMember = "fname";
            this.fcheck_type_idComboBox.ValueMember = "fcheck_type_id";

            this.fsam_type_idComboBox.DisplayMember = "fname";
            this.fsam_type_idComboBox.ValueMember = "fsample_type_id";

            this.fitem_type_idComboBox.DisplayMember = "fname";
            this.fitem_type_idComboBox.ValueMember = "fcode";

            this.funit_nameComboBox.DisplayMember = "fname";
            this.funit_nameComboBox.ValueMember = "fcode";

            this.fprint_type_idComboBox.DisplayMember = "fname";
            this.fprint_type_idComboBox.ValueMember = "fcode";

            this.fvalue_ddComboBox.DisplayMember = "fname";
            this.fvalue_ddComboBox.ValueMember = "fcode";

            this.fcheck_method_idComboBox.DisplayMember = "fname";
            this.fcheck_method_idComboBox.ValueMember = "fcode";
                       

            //this.bindingSource_ref.DataSource = dtRef;     
            this.dataGridViewRef.AutoGenerateColumns = false;
            this.dataGridViewRef.DataSource = bindingSource_ref;

            this.fsex.DisplayMember = "fname";
            this.fsex.ValueMember = "fname";

            this.fvalue_flag.DisplayMember = "fname";
            this.fvalue_flag.ValueMember = "fcode";
            this.dataGridViewValue.AutoGenerateColumns = false;

            this.fsample_type_id.DisplayMember = "fname";
            this.fsample_type_id.ValueMember = "fsample_type_id";

            this.dataGridViewIO.AutoGenerateColumns = false;

            this.strCurrItemGuid = fitem_id;
        }

        private void ItemDecForm_Load(object sender, EventArgs e)
        {
          
            try
            {
                GetInstrDT();
                GetCheckType();
                GetSamType();
                GetComItemType();
                GetItemDT();
                GetRef();
                GetPrintList();
                GetIO();
                GetItemValue();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
       
        private void GetItemDT()
        {
            try
            {
                if (dtItem != null)
                    dtItem.Clear();
                dtItem = this.bllItem.BllItem("fitem_id='" + this.strCurrItemGuid + "'");
                this.sam_itemBindingSource.DataSource = dtItem;
                
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得参考值明细
        /// </summary>
        private void GetRef()
        {
            try
            {
                this.dtRef = this.bllItem.BllItemRefDT(this.strCurrItemGuid);//当前参考值明细
                this.bindingSource_ref.DataSource = dtRef;
                SetrefShow();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void GetPrintList()
        {
            try
            {
                this.dataGridViewPrint.AutoGenerateColumns = false;
                DataTable dtPrint = this.bllItem.BllItem("fitem_id='"+this.strCurrItemGuid+"'");
                this.dataGridViewPrint.DataSource = dtPrint;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary> 
        /// 取得公共类型 1。 结果类型；2。单位
        /// </summary>
        private void GetComItemType()
        {
            try
            {
                this.fitem_type_idComboBox.DataSource = this.bllType.BllComTypeDT("结果类型", 1,"");
                this.funit_nameComboBox.DataSource = this.bllType.BllComTypeDT("常用单位", 1,"");
                this.fprint_type_idComboBox.DataSource = this.bllType.BllComTypeDT("项目结果打印类型", 1,"");
                this.fvalue_ddComboBox.DataSource = this.bllType.BllComTypeDT("项目结果小数位数", 1,"");
                this.fcheck_method_idComboBox.DataSource = this.bllType.BllComTypeDT("项目检验方法", 1,"");
                this.fsex.DataSource = this.bllType.BllComTypeDT("性别", 1,"");
                this.fvalue_flag.DataSource = this.bllType.BllComTypeDT("结果标记", 1,"");
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 取得检验类别
        /// </summary>
        private void GetCheckType()
        {
            try
            {

                this.fcheck_type_idComboBox.DataSource = this.bllType.BllCheckTypeDT(1);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 取得样本类型
        /// </summary>
        private void GetSamType()
        {
            try
            {
                DataTable dtsam = this.bllType.BllSamTypeDT(1);
                this.fsam_type_idComboBox.DataSource = dtsam;
                this.fsample_type_id.DataSource = dtsam;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }

       
        /// <summary>
        /// 已经启用的仪器
        /// </summary>
        private void GetInstrDT()
        {
            try
            {
                if (dtInstr.Rows.Count>0)
                    dtInstr.Clear();
               
                dtInstr = this.bllItem.BllInstrDT(1, yunLis.wwfbll.LoginBLL.strDeptID);
                // dtInstr = this.bllItem.BllInstrDT(1, "kfz");
                this.comboBoxInstr.DataSource = dtInstr;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

       
        /// <summary>
        /// 取得接口 参数
        /// </summary>
        private void GetIO()
        {
            try
            {
                this.dataGridViewIO.DataSource = this.bllItem.BllItemIODT(this.strCurrItemGuid);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 项目常用取值
        /// </summary>
        private void GetItemValue()
        {
            try
            {
                this.dataGridViewValue.DataSource = this.bllItem.BllItemValueDT(this.strCurrItemGuid);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        

        /// <summary>
        /// 自动加帮助符
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fnameTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                this.fhelp_codeTextBox.Text = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(this.fnameTextBox.Text.Trim());
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        

      

        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
        /// <summary>
        /// 设计计算公式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelformula_Click(object sender, EventArgs e)
        {
            ItemFormulaResult r = new ItemFormulaResult();
            r.TextChanged += new TextChangedHandler1(this.EventResultChanged);
            ItemFormulaForm fc = new ItemFormulaForm(r, dtItem);
            fc.ShowDialog();
        }
        private void EventResultChanged(string s)
        {
            this.fjx_formulaTextBox.Text = s;
        }

        private void labelref_Click(object sender, EventArgs e)
        {
            ItemFormulaResult r = new ItemFormulaResult();
            r.TextChanged += new TextChangedHandler1(this.EventResultChangedManyRef);
            ItemManyRefForm fc = new ItemManyRefForm(r);
            fc.ShowDialog();
        }
        private void EventResultChangedManyRef(string s)
        {
            this.frefTextBox.Text = s;
        }

        private void SetrefShow()
        {
            if (fref_if_sexCheckBox.Checked)
                this.fsex.Visible = true;
            else
                this.fsex.Visible = false;
            if (fref_if_ageCheckBox.Checked)
            {
                this.fage_high.Visible = true;
                this.fage_low.Visible = true;
            }
            else
            {
                this.fage_high.Visible = false;
                this.fage_low.Visible = false;
            }
            if (fref_if_sampleCheckBox.Checked)
                this.fsample_type_id.Visible = true;
            else
                this.fsample_type_id.Visible = false;
        }

        private void fref_if_sexCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SetrefShow();

        }

        private void fref_if_ageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SetrefShow();
        }

        private void fref_if_sampleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SetrefShow();
        }
        /// <summary>
        /// 新增参考值明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonrefAdd_Click(object sender, EventArgs e)
        {
            try
            {

                this.bllItem.BllItemRefAdd(strCurrItemGuid);
                GetRef();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 删除参考明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonrefDel_Click(object sender, EventArgs e)
        {
            try
            {
                
                    if (yunLis.wwfbll.WWMessage.MessageDialogResult("确认要删除此参考值明细？"))
                    {
                        string fref_id = this.dataGridViewRef.CurrentRow.Cells["fref_id"].Value.ToString();
                        this.bllItem.BllItemRefDel(fref_id);
                        GetRef();
                    }
               
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }

        }
        /// <summary>
        /// 保存参考值明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonrefSave_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < this.dtRef.Rows.Count; i++)
                {


                    this.bllItem.BllItemRefUpdate(this.dtRef.Rows[i]);

                }
                GetRef();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
      

       
        private void fref_highTextBox_Leave(object sender, EventArgs e)
        {
            frefTextBox.Text = fref_lowTextBox.Text + "--" + fref_highTextBox.Text;
        }

        private void fref_lowTextBox_Leave(object sender, EventArgs e)
        {
            frefTextBox.Text = fref_lowTextBox.Text + "--" + fref_highTextBox.Text;
        }

        private void dataGridViewRef_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
        /// <summary>
        /// 新增接口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIOAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.bllItem.BllItemIOAdd(strCurrItemGuid);
                GetIO();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 保存接口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIOSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Cursor = Cursors.WaitCursor;
                this.Validate();
                this.dataGridViewIO.EndEdit();
                for (int i = 0; i < this.dataGridViewIO.Rows.Count; i++)
                {
                    this.bllItem.BllItemIOUpdate(this.dataGridViewIO.Rows[i].Cells["fio_id"].Value.ToString(), this.dataGridViewIO.Rows[i].Cells["fcode"].Value.ToString(), comboBoxInstr.SelectedValue.ToString());
                }
                GetIO();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 删除接口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonIODel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewIO.Rows.Count > 0)
                {
                    if (yunLis.wwfbll.WWMessage.MessageDialogResult("确认要删除？"))
                    {
                        string fio_id = this.dataGridViewIO.CurrentRow.Cells["fio_id"].Value.ToString();
                        this.bllItem.BllItemIODel(fio_id);
                        GetIO();
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridView2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void buttonPrintRef_Click(object sender, EventArgs e)
        {
            try
            {
                GetPrintList();
                GetItemDT();;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonPrintSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                //this.Validate();
                //this.sam_itemBindingSource.EndEdit();
                if (dataGridViewPrint.Rows.Count > 0)
                {
                    string strnum = "";
                    string strid = "";
                    for (int i = 0; i < this.dataGridViewPrint.Rows.Count; i++)
                    {
                        strid = this.dataGridViewPrint.Rows[i].Cells["fitem_id"].Value.ToString();
                        strnum = this.dataGridViewPrint.Rows[i].Cells["Itemfprint_num"].Value.ToString();
                        this.bllItem.BllItemUpdatePrintNum(strid, strnum);
                    }
                }
                GetPrintList();
                GetItemDT();;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buttonItemValueAdd_Click(object sender, EventArgs e)
        {
            try
            {
                this.bllItem.BllItemValueAdd(strCurrItemGuid);
                GetItemValue();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonItemValueSave_Click(object sender, EventArgs e)
        {
            try
            {
                //Cursor = Cursors.WaitCursor;
                this.Validate();
                this.dataGridViewValue.EndEdit();
                for (int i = 0; i < this.dataGridViewValue.Rows.Count; i++)
                {
                    //MessageBox.Show(this.dataGridViewValue.Rows[i].Cells["fvalue_flag"].Value.ToString());
                    this.bllItem.BllItemValueUpdate(this.dataGridViewValue.Rows[i].Cells["fvalue_id"].Value.ToString(), this.dataGridViewValue.Rows[i].Cells["fvalue"].Value.ToString(), this.dataGridViewValue.Rows[i].Cells["fvalue_flag"].Value.ToString(), this.dataGridViewValue.Rows[i].Cells["forder_by"].Value.ToString());
                }
                GetItemValue();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonItemValueDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewValue.Rows.Count > 0)
                {
                    if (yunLis.wwfbll.WWMessage.MessageDialogResult("确认要删除？"))
                    {
                        string fio_id = this.dataGridViewValue.CurrentRow.Cells["fvalue_id"].Value.ToString();
                        this.bllItem.BllItemValueDel(fio_id);
                        GetItemValue();
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void bindingNavigatorOK_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                this.bllItem.BllItemUpdate(this.dtItem.Rows[0]);
                this.Close();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}