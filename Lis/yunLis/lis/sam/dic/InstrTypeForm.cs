﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using yunLis.lisbll.sam;

namespace yunLis.lis.sam.dic
{

    public partial class InstrTypeForm : yunLis.wwf.SysBaseForm
    {
        InstrBLL bll = new InstrBLL();
        string currfp_id = "";//当前上级ID
        DataTable dtCurr = new DataTable();
        DataRowView rowCurr = null;

        public InstrTypeForm()
        {
            InitializeComponent();
        }

        private void InstrTypeForm_Load(object sender, EventArgs e)
        {
            GetTree("-1");
           
        }
        /// <summary>
        /// 树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllTypeDT(2);
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "ftype_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        private void GetCurrDT()
        {
            //dtCurr = new DataTable();
            if (dtCurr != null)
               dtCurr.Clear();
            dtCurr = this.bll.BllTypeDTByPid(currfp_id);
            bindingSourceObject.DataSource = dtCurr;
            
        }
        

        private void bindingSourceObject_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurr = (DataRowView)bindingSourceObject.Current;//行
                /*
                string fname = "";
                if (rowCurrent != null)
                {
                    fname = drObject["fname"].ToString();
                    MessageBox.Show(fname);
                }*/
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

       
       

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                currfp_id = e.Node.Name.ToString();
                GetCurrDT();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

      

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;
                dr = this.dtCurr.NewRow();
                string guid = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                dr["ftype_id"] = guid;
                dr["fp_id"] = this.currfp_id;
                dr["forder_by"] = "0";
                dr["fuse_if"] = "1";
                this.dtCurr.Rows.Add(dr);
                this.bindingSourceObject.Position = bindingSourceObject.Count;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                if (this.bll.BllTypeAdd(rowCurr) > 0)
                    GetCurrDT();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                if (this.bll.BllTypeUpdate(rowCurr) > 0)
                    GetCurrDT();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetTree("-1");
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (yunLis.wwfbll.WWMessage.MessageDialogResult("是否真要删除此类型？"))
                {
                    if (this.bll.BllTypeDelete(rowCurr["ftype_id"].ToString()) > 0)
                        GetCurrDT();
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        
    }
}