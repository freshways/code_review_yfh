﻿namespace yunLis.lis.sam.dic
{
    partial class ItemDecForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fitem_codeLabel;
            System.Windows.Forms.Label fcheck_type_idLabel;
            System.Windows.Forms.Label fitem_type_idLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fname_jLabel;
            System.Windows.Forms.Label fhelp_codeLabel;
            System.Windows.Forms.Label fhis_item_codeLabel;
            System.Windows.Forms.Label funit_nameLabel;
            System.Windows.Forms.Label fxsLabel;
            System.Windows.Forms.Label fvalue_ddLabel;
            System.Windows.Forms.Label fprint_type_idLabel;
            System.Windows.Forms.Label fpriceLabel;
            System.Windows.Forms.Label fcheck_method_idLabel;
            System.Windows.Forms.Label fuse_ifLabel;
            System.Windows.Forms.Label fjx_ifLabel;
            System.Windows.Forms.Label fjx_formulaLabel;
            System.Windows.Forms.Label fref_highLabel;
            System.Windows.Forms.Label fref_lowLabel;
            System.Windows.Forms.Label frefLabel;
            System.Windows.Forms.Label fname_eLabel;
            System.Windows.Forms.Label fjg_value_sxLabel;
            System.Windows.Forms.Label fjg_value_xxLabel;
            System.Windows.Forms.Label fvalue_day_numLabel;
            System.Windows.Forms.Label fvalue_day_noLabel;
            System.Windows.Forms.Label fsam_type_idLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemDecForm));
            this.comboBoxInstr = new System.Windows.Forms.ComboBox();
            this.sam_itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new yunLis.lis.sam.samDataSet();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.fvalue_day_noTextBox = new System.Windows.Forms.TextBox();
            this.fvalue_day_numTextBox = new System.Windows.Forms.TextBox();
            this.fjg_value_xxTextBox = new System.Windows.Forms.TextBox();
            this.fjg_value_sxTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.fref_if_sampleCheckBox = new System.Windows.Forms.CheckBox();
            this.fref_if_ageCheckBox = new System.Windows.Forms.CheckBox();
            this.fref_if_sexCheckBox = new System.Windows.Forms.CheckBox();
            this.labelref = new System.Windows.Forms.Label();
            this.frefTextBox = new System.Windows.Forms.TextBox();
            this.fref_lowTextBox = new System.Windows.Forms.TextBox();
            this.fref_highTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fsam_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fname_eTextBox = new System.Windows.Forms.TextBox();
            this.labelformula = new System.Windows.Forms.Label();
            this.fjx_formulaTextBox = new System.Windows.Forms.TextBox();
            this.fjx_ifCheckBox = new System.Windows.Forms.CheckBox();
            this.fuse_ifCheckBox = new System.Windows.Forms.CheckBox();
            this.fcheck_method_idComboBox = new System.Windows.Forms.ComboBox();
            this.fpriceTextBox = new System.Windows.Forms.TextBox();
            this.fprint_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fvalue_ddComboBox = new System.Windows.Forms.ComboBox();
            this.fxsTextBox = new System.Windows.Forms.TextBox();
            this.funit_nameComboBox = new System.Windows.Forms.ComboBox();
            this.fhis_item_codeTextBox = new System.Windows.Forms.TextBox();
            this.fhelp_codeTextBox = new System.Windows.Forms.TextBox();
            this.fname_jTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fitem_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fcheck_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fitem_codeTextBox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewRef = new System.Windows.Forms.DataGridView();
            this.fsex = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fage_low = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fage_high = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsample_type_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fref_low = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fref_high = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fref1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fref_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonrefSave = new System.Windows.Forms.Button();
            this.buttonrefDel = new System.Windows.Forms.Button();
            this.buttonrefAdd = new System.Windows.Forms.Button();
            this.tabPagefyy = new System.Windows.Forms.TabPage();
            this.fyyRichTextBox = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridViewIO = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonIOSave = new System.Windows.Forms.Button();
            this.buttonIODel = new System.Windows.Forms.Button();
            this.buttonIOAdd = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dataGridViewPrint = new System.Windows.Forms.DataGridView();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Itemfprint_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonPrintSave = new System.Windows.Forms.Button();
            this.buttonPrintRef = new System.Windows.Forms.Button();
            this.tabPageValue = new System.Windows.Forms.TabPage();
            this.dataGridViewValue = new System.Windows.Forms.DataGridView();
            this.fvalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue_flag = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valuefitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.buttonItemValueSave = new System.Windows.Forms.Button();
            this.buttonItemValueDel = new System.Windows.Forms.Button();
            this.buttonItemValueAdd = new System.Windows.Forms.Button();
            this.bindingSource_ref = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.bindingSource_fsex = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource_fsample_type_id = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorOK = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonNo = new System.Windows.Forms.ToolStripButton();
            this.fcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.换算率 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iofitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iofuse_if = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fio_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            fitem_codeLabel = new System.Windows.Forms.Label();
            fcheck_type_idLabel = new System.Windows.Forms.Label();
            fitem_type_idLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fname_jLabel = new System.Windows.Forms.Label();
            fhelp_codeLabel = new System.Windows.Forms.Label();
            fhis_item_codeLabel = new System.Windows.Forms.Label();
            funit_nameLabel = new System.Windows.Forms.Label();
            fxsLabel = new System.Windows.Forms.Label();
            fvalue_ddLabel = new System.Windows.Forms.Label();
            fprint_type_idLabel = new System.Windows.Forms.Label();
            fpriceLabel = new System.Windows.Forms.Label();
            fcheck_method_idLabel = new System.Windows.Forms.Label();
            fuse_ifLabel = new System.Windows.Forms.Label();
            fjx_ifLabel = new System.Windows.Forms.Label();
            fjx_formulaLabel = new System.Windows.Forms.Label();
            fref_highLabel = new System.Windows.Forms.Label();
            fref_lowLabel = new System.Windows.Forms.Label();
            frefLabel = new System.Windows.Forms.Label();
            fname_eLabel = new System.Windows.Forms.Label();
            fjg_value_sxLabel = new System.Windows.Forms.Label();
            fjg_value_xxLabel = new System.Windows.Forms.Label();
            fvalue_day_numLabel = new System.Windows.Forms.Label();
            fvalue_day_noLabel = new System.Windows.Forms.Label();
            fsam_type_idLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sam_itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRef)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabPagefyy.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIO)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrint)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabPageValue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewValue)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_ref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsample_type_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fitem_codeLabel
            // 
            fitem_codeLabel.AutoSize = true;
            fitem_codeLabel.Location = new System.Drawing.Point(12, 67);
            fitem_codeLabel.Name = "fitem_codeLabel";
            fitem_codeLabel.Size = new System.Drawing.Size(59, 12);
            fitem_codeLabel.TabIndex = 0;
            fitem_codeLabel.Text = "项目代码:";
            // 
            // fcheck_type_idLabel
            // 
            fcheck_type_idLabel.AutoSize = true;
            fcheck_type_idLabel.Location = new System.Drawing.Point(12, 45);
            fcheck_type_idLabel.Name = "fcheck_type_idLabel";
            fcheck_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fcheck_type_idLabel.TabIndex = 2;
            fcheck_type_idLabel.Text = "检验类型:";
            // 
            // fitem_type_idLabel
            // 
            fitem_type_idLabel.AutoSize = true;
            fitem_type_idLabel.Location = new System.Drawing.Point(308, 67);
            fitem_type_idLabel.Name = "fitem_type_idLabel";
            fitem_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fitem_type_idLabel.TabIndex = 4;
            fitem_type_idLabel.Text = "结果类型:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(12, 90);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(59, 12);
            fnameLabel.TabIndex = 8;
            fnameLabel.Text = "项目名称:";
            // 
            // fname_jLabel
            // 
            fname_jLabel.AutoSize = true;
            fname_jLabel.Location = new System.Drawing.Point(12, 113);
            fname_jLabel.Name = "fname_jLabel";
            fname_jLabel.Size = new System.Drawing.Size(59, 12);
            fname_jLabel.TabIndex = 10;
            fname_jLabel.Text = "项目简称:";
            // 
            // fhelp_codeLabel
            // 
            fhelp_codeLabel.AutoSize = true;
            fhelp_codeLabel.Location = new System.Drawing.Point(24, 136);
            fhelp_codeLabel.Name = "fhelp_codeLabel";
            fhelp_codeLabel.Size = new System.Drawing.Size(47, 12);
            fhelp_codeLabel.TabIndex = 12;
            fhelp_codeLabel.Text = "助记符:";
            // 
            // fhis_item_codeLabel
            // 
            fhis_item_codeLabel.AutoSize = true;
            fhis_item_codeLabel.Location = new System.Drawing.Point(314, 210);
            fhis_item_codeLabel.Name = "fhis_item_codeLabel";
            fhis_item_codeLabel.Size = new System.Drawing.Size(53, 12);
            fhis_item_codeLabel.TabIndex = 14;
            fhis_item_codeLabel.Text = "HIS代码:";
            // 
            // funit_nameLabel
            // 
            funit_nameLabel.AutoSize = true;
            funit_nameLabel.Location = new System.Drawing.Point(36, 159);
            funit_nameLabel.Name = "funit_nameLabel";
            funit_nameLabel.Size = new System.Drawing.Size(35, 12);
            funit_nameLabel.TabIndex = 16;
            funit_nameLabel.Text = "单位:";
            // 
            // fxsLabel
            // 
            fxsLabel.AutoSize = true;
            fxsLabel.Location = new System.Drawing.Point(308, 141);
            fxsLabel.Name = "fxsLabel";
            fxsLabel.Size = new System.Drawing.Size(59, 12);
            fxsLabel.TabIndex = 18;
            fxsLabel.Text = "调整系数:";
            // 
            // fvalue_ddLabel
            // 
            fvalue_ddLabel.AutoSize = true;
            fvalue_ddLabel.Location = new System.Drawing.Point(12, 182);
            fvalue_ddLabel.Name = "fvalue_ddLabel";
            fvalue_ddLabel.Size = new System.Drawing.Size(59, 12);
            fvalue_ddLabel.TabIndex = 20;
            fvalue_ddLabel.Text = "小数位数:";
            // 
            // fprint_type_idLabel
            // 
            fprint_type_idLabel.AutoSize = true;
            fprint_type_idLabel.Location = new System.Drawing.Point(308, 164);
            fprint_type_idLabel.Name = "fprint_type_idLabel";
            fprint_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fprint_type_idLabel.TabIndex = 22;
            fprint_type_idLabel.Text = "打印类型:";
            // 
            // fpriceLabel
            // 
            fpriceLabel.AutoSize = true;
            fpriceLabel.Location = new System.Drawing.Point(36, 205);
            fpriceLabel.Name = "fpriceLabel";
            fpriceLabel.Size = new System.Drawing.Size(35, 12);
            fpriceLabel.TabIndex = 24;
            fpriceLabel.Text = "价格:";
            // 
            // fcheck_method_idLabel
            // 
            fcheck_method_idLabel.AutoSize = true;
            fcheck_method_idLabel.Location = new System.Drawing.Point(308, 187);
            fcheck_method_idLabel.Name = "fcheck_method_idLabel";
            fcheck_method_idLabel.Size = new System.Drawing.Size(59, 12);
            fcheck_method_idLabel.TabIndex = 29;
            fcheck_method_idLabel.Text = "检验方法:";
            // 
            // fuse_ifLabel
            // 
            fuse_ifLabel.AutoSize = true;
            fuse_ifLabel.Location = new System.Drawing.Point(314, 22);
            fuse_ifLabel.Name = "fuse_ifLabel";
            fuse_ifLabel.Size = new System.Drawing.Size(47, 12);
            fuse_ifLabel.TabIndex = 30;
            fuse_ifLabel.Text = "启用否:";
            // 
            // fjx_ifLabel
            // 
            fjx_ifLabel.AutoSize = true;
            fjx_ifLabel.Location = new System.Drawing.Point(12, 229);
            fjx_ifLabel.Name = "fjx_ifLabel";
            fjx_ifLabel.Size = new System.Drawing.Size(59, 12);
            fjx_ifLabel.TabIndex = 31;
            fjx_ifLabel.Text = "计算项目:";
            // 
            // fjx_formulaLabel
            // 
            fjx_formulaLabel.AutoSize = true;
            fjx_formulaLabel.Location = new System.Drawing.Point(103, 230);
            fjx_formulaLabel.Name = "fjx_formulaLabel";
            fjx_formulaLabel.Size = new System.Drawing.Size(35, 12);
            fjx_formulaLabel.TabIndex = 32;
            fjx_formulaLabel.Text = "公式:";
            // 
            // fref_highLabel
            // 
            fref_highLabel.AutoSize = true;
            fref_highLabel.Location = new System.Drawing.Point(295, 18);
            fref_highLabel.Name = "fref_highLabel";
            fref_highLabel.Size = new System.Drawing.Size(71, 12);
            fref_highLabel.TabIndex = 0;
            fref_highLabel.Text = "参考值上线:";
            // 
            // fref_lowLabel
            // 
            fref_lowLabel.AutoSize = true;
            fref_lowLabel.Location = new System.Drawing.Point(0, 18);
            fref_lowLabel.Name = "fref_lowLabel";
            fref_lowLabel.Size = new System.Drawing.Size(71, 12);
            fref_lowLabel.TabIndex = 2;
            fref_lowLabel.Text = "参考值下线:";
            // 
            // frefLabel
            // 
            frefLabel.AutoSize = true;
            frefLabel.Location = new System.Drawing.Point(30, 45);
            frefLabel.Name = "frefLabel";
            frefLabel.Size = new System.Drawing.Size(47, 12);
            frefLabel.TabIndex = 4;
            frefLabel.Text = "参考值:";
            // 
            // fname_eLabel
            // 
            fname_eLabel.AutoSize = true;
            fname_eLabel.Location = new System.Drawing.Point(320, 118);
            fname_eLabel.Name = "fname_eLabel";
            fname_eLabel.Size = new System.Drawing.Size(47, 12);
            fname_eLabel.TabIndex = 34;
            fname_eLabel.Text = "英文名:";
            // 
            // fjg_value_sxLabel
            // 
            fjg_value_sxLabel.AutoSize = true;
            fjg_value_sxLabel.Location = new System.Drawing.Point(297, 23);
            fjg_value_sxLabel.Name = "fjg_value_sxLabel";
            fjg_value_sxLabel.Size = new System.Drawing.Size(71, 12);
            fjg_value_sxLabel.TabIndex = 0;
            fjg_value_sxLabel.Text = "警告值上限:";
            // 
            // fjg_value_xxLabel
            // 
            fjg_value_xxLabel.AutoSize = true;
            fjg_value_xxLabel.Location = new System.Drawing.Point(24, 23);
            fjg_value_xxLabel.Name = "fjg_value_xxLabel";
            fjg_value_xxLabel.Size = new System.Drawing.Size(71, 12);
            fjg_value_xxLabel.TabIndex = 2;
            fjg_value_xxLabel.Text = "警告值下限:";
            // 
            // fvalue_day_numLabel
            // 
            fvalue_day_numLabel.AutoSize = true;
            fvalue_day_numLabel.Location = new System.Drawing.Point(278, 48);
            fvalue_day_numLabel.Name = "fvalue_day_numLabel";
            fvalue_day_numLabel.Size = new System.Drawing.Size(89, 12);
            fvalue_day_numLabel.TabIndex = 4;
            fvalue_day_numLabel.Text = "天内不可能相差";
            // 
            // fvalue_day_noLabel
            // 
            fvalue_day_noLabel.AutoSize = true;
            fvalue_day_noLabel.Location = new System.Drawing.Point(19, 48);
            fvalue_day_noLabel.Name = "fvalue_day_noLabel";
            fvalue_day_noLabel.Size = new System.Drawing.Size(113, 12);
            fvalue_day_noLabel.TabIndex = 6;
            fvalue_day_noLabel.Text = "病人两次检验结果在";
            // 
            // fsam_type_idLabel
            // 
            fsam_type_idLabel.AutoSize = true;
            fsam_type_idLabel.Location = new System.Drawing.Point(308, 45);
            fsam_type_idLabel.Name = "fsam_type_idLabel";
            fsam_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fsam_type_idLabel.TabIndex = 35;
            fsam_type_idLabel.Text = "样本类型:";
            // 
            // comboBoxInstr
            // 
            this.comboBoxInstr.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "finstr_id", true));
            this.comboBoxInstr.DisplayMember = "ShowName";
            this.comboBoxInstr.Enabled = false;
            this.comboBoxInstr.FormattingEnabled = true;
            this.comboBoxInstr.Location = new System.Drawing.Point(78, 18);
            this.comboBoxInstr.Name = "comboBoxInstr";
            this.comboBoxInstr.Size = new System.Drawing.Size(200, 20);
            this.comboBoxInstr.TabIndex = 1;
            this.comboBoxInstr.ValueMember = "finstr_id";
            // 
            // sam_itemBindingSource
            // 
            this.sam_itemBindingSource.DataMember = "sam_item";
            this.sam_itemBindingSource.DataSource = this.samDataSet;
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "仪器:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPagefyy);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPageValue);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(612, 471);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(604, 446);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "项目信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(fvalue_day_noLabel);
            this.groupBox3.Controls.Add(this.fvalue_day_noTextBox);
            this.groupBox3.Controls.Add(fvalue_day_numLabel);
            this.groupBox3.Controls.Add(this.fvalue_day_numTextBox);
            this.groupBox3.Controls.Add(fjg_value_xxLabel);
            this.groupBox3.Controls.Add(this.fjg_value_xxTextBox);
            this.groupBox3.Controls.Add(fjg_value_sxLabel);
            this.groupBox3.Controls.Add(this.fjg_value_sxTextBox);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 360);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(598, 83);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "医疗审核条件";
            // 
            // fvalue_day_noTextBox
            // 
            this.fvalue_day_noTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fvalue_day_no", true));
            this.fvalue_day_noTextBox.Location = new System.Drawing.Point(144, 44);
            this.fvalue_day_noTextBox.Name = "fvalue_day_noTextBox";
            this.fvalue_day_noTextBox.Size = new System.Drawing.Size(114, 21);
            this.fvalue_day_noTextBox.TabIndex = 7;
            // 
            // fvalue_day_numTextBox
            // 
            this.fvalue_day_numTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fvalue_day_num", true));
            this.fvalue_day_numTextBox.Location = new System.Drawing.Point(372, 44);
            this.fvalue_day_numTextBox.Name = "fvalue_day_numTextBox";
            this.fvalue_day_numTextBox.Size = new System.Drawing.Size(200, 21);
            this.fvalue_day_numTextBox.TabIndex = 5;
            // 
            // fjg_value_xxTextBox
            // 
            this.fjg_value_xxTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fjg_value_xx", true));
            this.fjg_value_xxTextBox.Location = new System.Drawing.Point(99, 19);
            this.fjg_value_xxTextBox.Name = "fjg_value_xxTextBox";
            this.fjg_value_xxTextBox.Size = new System.Drawing.Size(178, 21);
            this.fjg_value_xxTextBox.TabIndex = 3;
            // 
            // fjg_value_sxTextBox
            // 
            this.fjg_value_sxTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fjg_value_sx", true));
            this.fjg_value_sxTextBox.Location = new System.Drawing.Point(372, 19);
            this.fjg_value_sxTextBox.Name = "fjg_value_sxTextBox";
            this.fjg_value_sxTextBox.Size = new System.Drawing.Size(200, 21);
            this.fjg_value_sxTextBox.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.fref_if_sampleCheckBox);
            this.groupBox2.Controls.Add(this.fref_if_ageCheckBox);
            this.groupBox2.Controls.Add(this.fref_if_sexCheckBox);
            this.groupBox2.Controls.Add(this.labelref);
            this.groupBox2.Controls.Add(frefLabel);
            this.groupBox2.Controls.Add(this.frefTextBox);
            this.groupBox2.Controls.Add(fref_lowLabel);
            this.groupBox2.Controls.Add(this.fref_lowTextBox);
            this.groupBox2.Controls.Add(fref_highLabel);
            this.groupBox2.Controls.Add(this.fref_highTextBox);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 255);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(598, 105);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "参考值";
            // 
            // fref_if_sampleCheckBox
            // 
            this.fref_if_sampleCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fref_if_sample", true));
            this.fref_if_sampleCheckBox.Location = new System.Drawing.Point(299, 68);
            this.fref_if_sampleCheckBox.Name = "fref_if_sampleCheckBox";
            this.fref_if_sampleCheckBox.Size = new System.Drawing.Size(125, 24);
            this.fref_if_sampleCheckBox.TabIndex = 40;
            this.fref_if_sampleCheckBox.Text = "参考值与样本有关";
            this.fref_if_sampleCheckBox.CheckedChanged += new System.EventHandler(this.fref_if_sampleCheckBox_CheckedChanged);
            // 
            // fref_if_ageCheckBox
            // 
            this.fref_if_ageCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fref_if_age", true));
            this.fref_if_ageCheckBox.Location = new System.Drawing.Point(171, 68);
            this.fref_if_ageCheckBox.Name = "fref_if_ageCheckBox";
            this.fref_if_ageCheckBox.Size = new System.Drawing.Size(122, 24);
            this.fref_if_ageCheckBox.TabIndex = 38;
            this.fref_if_ageCheckBox.Text = "参考值与年龄有关";
            this.fref_if_ageCheckBox.CheckedChanged += new System.EventHandler(this.fref_if_ageCheckBox_CheckedChanged);
            // 
            // fref_if_sexCheckBox
            // 
            this.fref_if_sexCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fref_if_sex", true));
            this.fref_if_sexCheckBox.Location = new System.Drawing.Point(37, 68);
            this.fref_if_sexCheckBox.Name = "fref_if_sexCheckBox";
            this.fref_if_sexCheckBox.Size = new System.Drawing.Size(128, 24);
            this.fref_if_sexCheckBox.TabIndex = 36;
            this.fref_if_sexCheckBox.Text = "参考值与性别有关";
            this.fref_if_sexCheckBox.CheckedChanged += new System.EventHandler(this.fref_if_sexCheckBox_CheckedChanged);
            // 
            // labelref
            // 
            this.labelref.AutoSize = true;
            this.labelref.ForeColor = System.Drawing.Color.Navy;
            this.labelref.Location = new System.Drawing.Point(359, 45);
            this.labelref.Name = "labelref";
            this.labelref.Size = new System.Drawing.Size(83, 12);
            this.labelref.TabIndex = 35;
            this.labelref.Text = "多行参考值...";
            this.labelref.Click += new System.EventHandler(this.labelref_Click);
            // 
            // frefTextBox
            // 
            this.frefTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fref", true));
            this.frefTextBox.Location = new System.Drawing.Point(77, 41);
            this.frefTextBox.Name = "frefTextBox";
            this.frefTextBox.Size = new System.Drawing.Size(276, 21);
            this.frefTextBox.TabIndex = 5;
            // 
            // fref_lowTextBox
            // 
            this.fref_lowTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fref_low", true));
            this.fref_lowTextBox.Location = new System.Drawing.Point(77, 14);
            this.fref_lowTextBox.Name = "fref_lowTextBox";
            this.fref_lowTextBox.Size = new System.Drawing.Size(200, 21);
            this.fref_lowTextBox.TabIndex = 3;
            this.fref_lowTextBox.Leave += new System.EventHandler(this.fref_lowTextBox_Leave);
            // 
            // fref_highTextBox
            // 
            this.fref_highTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fref_high", true));
            this.fref_highTextBox.Location = new System.Drawing.Point(372, 14);
            this.fref_highTextBox.Name = "fref_highTextBox";
            this.fref_highTextBox.Size = new System.Drawing.Size(200, 21);
            this.fref_highTextBox.TabIndex = 1;
            this.fref_highTextBox.Leave += new System.EventHandler(this.fref_highTextBox_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxInstr);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(fsam_type_idLabel);
            this.groupBox1.Controls.Add(this.fsam_type_idComboBox);
            this.groupBox1.Controls.Add(fname_eLabel);
            this.groupBox1.Controls.Add(this.fname_eTextBox);
            this.groupBox1.Controls.Add(this.labelformula);
            this.groupBox1.Controls.Add(fjx_formulaLabel);
            this.groupBox1.Controls.Add(this.fjx_formulaTextBox);
            this.groupBox1.Controls.Add(fjx_ifLabel);
            this.groupBox1.Controls.Add(this.fjx_ifCheckBox);
            this.groupBox1.Controls.Add(fuse_ifLabel);
            this.groupBox1.Controls.Add(this.fuse_ifCheckBox);
            this.groupBox1.Controls.Add(fcheck_method_idLabel);
            this.groupBox1.Controls.Add(this.fcheck_method_idComboBox);
            this.groupBox1.Controls.Add(fpriceLabel);
            this.groupBox1.Controls.Add(this.fpriceTextBox);
            this.groupBox1.Controls.Add(fprint_type_idLabel);
            this.groupBox1.Controls.Add(this.fprint_type_idComboBox);
            this.groupBox1.Controls.Add(fvalue_ddLabel);
            this.groupBox1.Controls.Add(this.fvalue_ddComboBox);
            this.groupBox1.Controls.Add(fxsLabel);
            this.groupBox1.Controls.Add(this.fxsTextBox);
            this.groupBox1.Controls.Add(funit_nameLabel);
            this.groupBox1.Controls.Add(this.funit_nameComboBox);
            this.groupBox1.Controls.Add(fhis_item_codeLabel);
            this.groupBox1.Controls.Add(this.fhis_item_codeTextBox);
            this.groupBox1.Controls.Add(fhelp_codeLabel);
            this.groupBox1.Controls.Add(this.fhelp_codeTextBox);
            this.groupBox1.Controls.Add(fname_jLabel);
            this.groupBox1.Controls.Add(this.fname_jTextBox);
            this.groupBox1.Controls.Add(fnameLabel);
            this.groupBox1.Controls.Add(this.fnameTextBox);
            this.groupBox1.Controls.Add(fitem_type_idLabel);
            this.groupBox1.Controls.Add(this.fitem_type_idComboBox);
            this.groupBox1.Controls.Add(fcheck_type_idLabel);
            this.groupBox1.Controls.Add(this.fcheck_type_idComboBox);
            this.groupBox1.Controls.Add(fitem_codeLabel);
            this.groupBox1.Controls.Add(this.fitem_codeTextBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(598, 252);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基础信息";
            // 
            // fsam_type_idComboBox
            // 
            this.fsam_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fsam_type_id", true));
            this.fsam_type_idComboBox.Enabled = false;
            this.fsam_type_idComboBox.FormattingEnabled = true;
            this.fsam_type_idComboBox.Location = new System.Drawing.Point(372, 41);
            this.fsam_type_idComboBox.Name = "fsam_type_idComboBox";
            this.fsam_type_idComboBox.Size = new System.Drawing.Size(200, 20);
            this.fsam_type_idComboBox.TabIndex = 36;
            // 
            // fname_eTextBox
            // 
            this.fname_eTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fname_e", true));
            this.fname_eTextBox.Location = new System.Drawing.Point(372, 114);
            this.fname_eTextBox.Name = "fname_eTextBox";
            this.fname_eTextBox.Size = new System.Drawing.Size(200, 21);
            this.fname_eTextBox.TabIndex = 35;
            // 
            // labelformula
            // 
            this.labelformula.AutoSize = true;
            this.labelformula.ForeColor = System.Drawing.Color.Navy;
            this.labelformula.Location = new System.Drawing.Point(370, 229);
            this.labelformula.Name = "labelformula";
            this.labelformula.Size = new System.Drawing.Size(47, 12);
            this.labelformula.TabIndex = 34;
            this.labelformula.Text = "设置...";
            this.labelformula.Click += new System.EventHandler(this.labelformula_Click);
            // 
            // fjx_formulaTextBox
            // 
            this.fjx_formulaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fjx_formula", true));
            this.fjx_formulaTextBox.Location = new System.Drawing.Point(144, 226);
            this.fjx_formulaTextBox.Name = "fjx_formulaTextBox";
            this.fjx_formulaTextBox.ReadOnly = true;
            this.fjx_formulaTextBox.Size = new System.Drawing.Size(201, 21);
            this.fjx_formulaTextBox.TabIndex = 33;
            // 
            // fjx_ifCheckBox
            // 
            this.fjx_ifCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fjx_if", true));
            this.fjx_ifCheckBox.Location = new System.Drawing.Point(77, 223);
            this.fjx_ifCheckBox.Name = "fjx_ifCheckBox";
            this.fjx_ifCheckBox.Size = new System.Drawing.Size(19, 24);
            this.fjx_ifCheckBox.TabIndex = 32;
            // 
            // fuse_ifCheckBox
            // 
            this.fuse_ifCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sam_itemBindingSource, "fuse_if", true));
            this.fuse_ifCheckBox.Location = new System.Drawing.Point(372, 16);
            this.fuse_ifCheckBox.Name = "fuse_ifCheckBox";
            this.fuse_ifCheckBox.Size = new System.Drawing.Size(27, 24);
            this.fuse_ifCheckBox.TabIndex = 31;
            // 
            // fcheck_method_idComboBox
            // 
            this.fcheck_method_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fcheck_method_id", true));
            this.fcheck_method_idComboBox.FormattingEnabled = true;
            this.fcheck_method_idComboBox.Location = new System.Drawing.Point(372, 183);
            this.fcheck_method_idComboBox.Name = "fcheck_method_idComboBox";
            this.fcheck_method_idComboBox.Size = new System.Drawing.Size(200, 20);
            this.fcheck_method_idComboBox.TabIndex = 30;
            // 
            // fpriceTextBox
            // 
            this.fpriceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fprice", true));
            this.fpriceTextBox.Location = new System.Drawing.Point(77, 201);
            this.fpriceTextBox.Name = "fpriceTextBox";
            this.fpriceTextBox.Size = new System.Drawing.Size(200, 21);
            this.fpriceTextBox.TabIndex = 25;
            // 
            // fprint_type_idComboBox
            // 
            this.fprint_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fprint_type_id", true));
            this.fprint_type_idComboBox.FormattingEnabled = true;
            this.fprint_type_idComboBox.Location = new System.Drawing.Point(372, 160);
            this.fprint_type_idComboBox.Name = "fprint_type_idComboBox";
            this.fprint_type_idComboBox.Size = new System.Drawing.Size(200, 20);
            this.fprint_type_idComboBox.TabIndex = 23;
            // 
            // fvalue_ddComboBox
            // 
            this.fvalue_ddComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fvalue_dd", true));
            this.fvalue_ddComboBox.FormattingEnabled = true;
            this.fvalue_ddComboBox.Location = new System.Drawing.Point(77, 178);
            this.fvalue_ddComboBox.Name = "fvalue_ddComboBox";
            this.fvalue_ddComboBox.Size = new System.Drawing.Size(200, 20);
            this.fvalue_ddComboBox.TabIndex = 21;
            // 
            // fxsTextBox
            // 
            this.fxsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fxs", true));
            this.fxsTextBox.Location = new System.Drawing.Point(372, 137);
            this.fxsTextBox.Name = "fxsTextBox";
            this.fxsTextBox.Size = new System.Drawing.Size(200, 21);
            this.fxsTextBox.TabIndex = 19;
            // 
            // funit_nameComboBox
            // 
            this.funit_nameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "funit_name", true));
            this.funit_nameComboBox.FormattingEnabled = true;
            this.funit_nameComboBox.Location = new System.Drawing.Point(77, 155);
            this.funit_nameComboBox.Name = "funit_nameComboBox";
            this.funit_nameComboBox.Size = new System.Drawing.Size(200, 20);
            this.funit_nameComboBox.TabIndex = 17;
            // 
            // fhis_item_codeTextBox
            // 
            this.fhis_item_codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fhis_item_code", true));
            this.fhis_item_codeTextBox.Location = new System.Drawing.Point(372, 206);
            this.fhis_item_codeTextBox.Name = "fhis_item_codeTextBox";
            this.fhis_item_codeTextBox.Size = new System.Drawing.Size(200, 21);
            this.fhis_item_codeTextBox.TabIndex = 15;
            // 
            // fhelp_codeTextBox
            // 
            this.fhelp_codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fhelp_code", true));
            this.fhelp_codeTextBox.Location = new System.Drawing.Point(77, 132);
            this.fhelp_codeTextBox.Name = "fhelp_codeTextBox";
            this.fhelp_codeTextBox.Size = new System.Drawing.Size(200, 21);
            this.fhelp_codeTextBox.TabIndex = 13;
            // 
            // fname_jTextBox
            // 
            this.fname_jTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fname_j", true));
            this.fname_jTextBox.Location = new System.Drawing.Point(77, 109);
            this.fname_jTextBox.Name = "fname_jTextBox";
            this.fname_jTextBox.Size = new System.Drawing.Size(200, 21);
            this.fname_jTextBox.TabIndex = 11;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fname", true));
            this.fnameTextBox.Location = new System.Drawing.Point(77, 86);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(495, 21);
            this.fnameTextBox.TabIndex = 9;
            this.fnameTextBox.Leave += new System.EventHandler(this.fnameTextBox_Leave);
            // 
            // fitem_type_idComboBox
            // 
            this.fitem_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fitem_type_id", true));
            this.fitem_type_idComboBox.FormattingEnabled = true;
            this.fitem_type_idComboBox.Location = new System.Drawing.Point(372, 63);
            this.fitem_type_idComboBox.Name = "fitem_type_idComboBox";
            this.fitem_type_idComboBox.Size = new System.Drawing.Size(200, 20);
            this.fitem_type_idComboBox.TabIndex = 5;
            // 
            // fcheck_type_idComboBox
            // 
            this.fcheck_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sam_itemBindingSource, "fcheck_type_id", true));
            this.fcheck_type_idComboBox.Enabled = false;
            this.fcheck_type_idComboBox.FormattingEnabled = true;
            this.fcheck_type_idComboBox.Location = new System.Drawing.Point(77, 41);
            this.fcheck_type_idComboBox.Name = "fcheck_type_idComboBox";
            this.fcheck_type_idComboBox.Size = new System.Drawing.Size(200, 20);
            this.fcheck_type_idComboBox.TabIndex = 3;
            // 
            // fitem_codeTextBox
            // 
            this.fitem_codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fitem_code", true));
            this.fitem_codeTextBox.Location = new System.Drawing.Point(77, 63);
            this.fitem_codeTextBox.Name = "fitem_codeTextBox";
            this.fitem_codeTextBox.Size = new System.Drawing.Size(200, 21);
            this.fitem_codeTextBox.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridViewRef);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 21);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(604, 446);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "参考值明细";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridViewRef
            // 
            this.dataGridViewRef.AllowUserToAddRows = false;
            this.dataGridViewRef.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewRef.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewRef.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewRef.ColumnHeadersHeight = 35;
            this.dataGridViewRef.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fsex,
            this.fage_low,
            this.fage_high,
            this.fsample_type_id,
            this.fref_low,
            this.fref_high,
            this.fref1,
            this.fref_id});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewRef.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewRef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewRef.EnableHeadersVisualStyles = false;
            this.dataGridViewRef.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewRef.MultiSelect = false;
            this.dataGridViewRef.Name = "dataGridViewRef";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewRef.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewRef.RowHeadersVisible = false;
            this.dataGridViewRef.RowHeadersWidth = 45;
            this.dataGridViewRef.RowTemplate.Height = 23;
            this.dataGridViewRef.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewRef.Size = new System.Drawing.Size(508, 440);
            this.dataGridViewRef.TabIndex = 122;
            this.dataGridViewRef.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewRef_DataError);
            // 
            // fsex
            // 
            this.fsex.DataPropertyName = "fsex";
            this.fsex.HeaderText = "性别";
            this.fsex.Name = "fsex";
            this.fsex.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fsex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fsex.Width = 40;
            // 
            // fage_low
            // 
            this.fage_low.DataPropertyName = "fage_low";
            this.fage_low.HeaderText = "年龄下限";
            this.fage_low.Name = "fage_low";
            this.fage_low.Width = 40;
            // 
            // fage_high
            // 
            this.fage_high.DataPropertyName = "fage_high";
            this.fage_high.HeaderText = "年龄上限";
            this.fage_high.Name = "fage_high";
            this.fage_high.Width = 40;
            // 
            // fsample_type_id
            // 
            this.fsample_type_id.DataPropertyName = "fsample_type_id";
            this.fsample_type_id.HeaderText = "样本类型";
            this.fsample_type_id.Name = "fsample_type_id";
            this.fsample_type_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fsample_type_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fsample_type_id.Width = 80;
            // 
            // fref_low
            // 
            this.fref_low.DataPropertyName = "fref_low";
            this.fref_low.HeaderText = "参考下限";
            this.fref_low.Name = "fref_low";
            this.fref_low.Width = 60;
            // 
            // fref_high
            // 
            this.fref_high.DataPropertyName = "fref_high";
            this.fref_high.HeaderText = "参考上限";
            this.fref_high.Name = "fref_high";
            this.fref_high.Width = 60;
            // 
            // fref1
            // 
            this.fref1.DataPropertyName = "fref";
            this.fref1.HeaderText = "参考值";
            this.fref1.Name = "fref1";
            this.fref1.Width = 120;
            // 
            // fref_id
            // 
            this.fref_id.DataPropertyName = "fref_id";
            this.fref_id.HeaderText = "fref_id";
            this.fref_id.Name = "fref_id";
            this.fref_id.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonrefSave);
            this.panel3.Controls.Add(this.buttonrefDel);
            this.panel3.Controls.Add(this.buttonrefAdd);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(511, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(90, 440);
            this.panel3.TabIndex = 125;
            // 
            // buttonrefSave
            // 
            this.buttonrefSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonrefSave.Location = new System.Drawing.Point(6, 59);
            this.buttonrefSave.Name = "buttonrefSave";
            this.buttonrefSave.Size = new System.Drawing.Size(80, 23);
            this.buttonrefSave.TabIndex = 127;
            this.buttonrefSave.Text = "保存";
            this.buttonrefSave.UseVisualStyleBackColor = true;
            this.buttonrefSave.Click += new System.EventHandler(this.buttonrefSave_Click);
            // 
            // buttonrefDel
            // 
            this.buttonrefDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonrefDel.Location = new System.Drawing.Point(6, 107);
            this.buttonrefDel.Name = "buttonrefDel";
            this.buttonrefDel.Size = new System.Drawing.Size(80, 23);
            this.buttonrefDel.TabIndex = 126;
            this.buttonrefDel.Text = "删除";
            this.buttonrefDel.UseVisualStyleBackColor = true;
            this.buttonrefDel.Click += new System.EventHandler(this.buttonrefDel_Click);
            // 
            // buttonrefAdd
            // 
            this.buttonrefAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonrefAdd.Location = new System.Drawing.Point(6, 11);
            this.buttonrefAdd.Name = "buttonrefAdd";
            this.buttonrefAdd.Size = new System.Drawing.Size(80, 23);
            this.buttonrefAdd.TabIndex = 125;
            this.buttonrefAdd.Text = "新增";
            this.buttonrefAdd.UseVisualStyleBackColor = true;
            this.buttonrefAdd.Click += new System.EventHandler(this.buttonrefAdd_Click);
            // 
            // tabPagefyy
            // 
            this.tabPagefyy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPagefyy.Controls.Add(this.fyyRichTextBox);
            this.tabPagefyy.Location = new System.Drawing.Point(4, 21);
            this.tabPagefyy.Name = "tabPagefyy";
            this.tabPagefyy.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagefyy.Size = new System.Drawing.Size(604, 446);
            this.tabPagefyy.TabIndex = 2;
            this.tabPagefyy.Text = "临床意义";
            this.tabPagefyy.UseVisualStyleBackColor = true;
            // 
            // fyyRichTextBox
            // 
            this.fyyRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fyyRichTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sam_itemBindingSource, "fyy", true));
            this.fyyRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fyyRichTextBox.Location = new System.Drawing.Point(3, 3);
            this.fyyRichTextBox.Name = "fyyRichTextBox";
            this.fyyRichTextBox.Size = new System.Drawing.Size(594, 436);
            this.fyyRichTextBox.TabIndex = 2;
            this.fyyRichTextBox.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridViewIO);
            this.tabPage4.Controls.Add(this.panel4);
            this.tabPage4.Location = new System.Drawing.Point(4, 21);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(604, 446);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "仪器接口";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridViewIO
            // 
            this.dataGridViewIO.AllowUserToAddRows = false;
            this.dataGridViewIO.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewIO.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewIO.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewIO.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fcode,
            this.换算率,
            this.iofitem_id,
            this.iofuse_if,
            this.fio_id});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewIO.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewIO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewIO.EnableHeadersVisualStyles = false;
            this.dataGridViewIO.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewIO.MultiSelect = false;
            this.dataGridViewIO.Name = "dataGridViewIO";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewIO.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewIO.RowHeadersVisible = false;
            this.dataGridViewIO.RowHeadersWidth = 45;
            this.dataGridViewIO.RowTemplate.Height = 23;
            this.dataGridViewIO.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewIO.Size = new System.Drawing.Size(508, 440);
            this.dataGridViewIO.TabIndex = 123;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.buttonIOSave);
            this.panel4.Controls.Add(this.buttonIODel);
            this.panel4.Controls.Add(this.buttonIOAdd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(511, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(90, 440);
            this.panel4.TabIndex = 126;
            // 
            // buttonIOSave
            // 
            this.buttonIOSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonIOSave.Location = new System.Drawing.Point(6, 59);
            this.buttonIOSave.Name = "buttonIOSave";
            this.buttonIOSave.Size = new System.Drawing.Size(80, 23);
            this.buttonIOSave.TabIndex = 127;
            this.buttonIOSave.Text = "保存";
            this.buttonIOSave.UseVisualStyleBackColor = true;
            this.buttonIOSave.Click += new System.EventHandler(this.buttonIOSave_Click);
            // 
            // buttonIODel
            // 
            this.buttonIODel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonIODel.Location = new System.Drawing.Point(6, 107);
            this.buttonIODel.Name = "buttonIODel";
            this.buttonIODel.Size = new System.Drawing.Size(80, 23);
            this.buttonIODel.TabIndex = 126;
            this.buttonIODel.Text = "删除";
            this.buttonIODel.UseVisualStyleBackColor = true;
            this.buttonIODel.Click += new System.EventHandler(this.buttonIODel_Click);
            // 
            // buttonIOAdd
            // 
            this.buttonIOAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonIOAdd.Location = new System.Drawing.Point(6, 11);
            this.buttonIOAdd.Name = "buttonIOAdd";
            this.buttonIOAdd.Size = new System.Drawing.Size(80, 23);
            this.buttonIOAdd.TabIndex = 125;
            this.buttonIOAdd.Text = "新增";
            this.buttonIOAdd.UseVisualStyleBackColor = true;
            this.buttonIOAdd.Click += new System.EventHandler(this.buttonIOAdd_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dataGridViewPrint);
            this.tabPage5.Controls.Add(this.panel5);
            this.tabPage5.Location = new System.Drawing.Point(4, 21);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(604, 446);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "打印排序";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPrint
            // 
            this.dataGridViewPrint.AllowUserToAddRows = false;
            this.dataGridViewPrint.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewPrint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPrint.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewPrint.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.Itemfprint_num,
            this.fitem_id});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPrint.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPrint.EnableHeadersVisualStyles = false;
            this.dataGridViewPrint.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewPrint.MultiSelect = false;
            this.dataGridViewPrint.Name = "dataGridViewPrint";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPrint.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewPrint.RowHeadersVisible = false;
            this.dataGridViewPrint.RowHeadersWidth = 45;
            this.dataGridViewPrint.RowTemplate.Height = 23;
            this.dataGridViewPrint.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewPrint.Size = new System.Drawing.Size(508, 440);
            this.dataGridViewPrint.TabIndex = 124;
            this.dataGridViewPrint.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView2_DataError);
            // 
            // ItemCode
            // 
            this.ItemCode.DataPropertyName = "fitem_code";
            this.ItemCode.HeaderText = "项目代号";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.ReadOnly = true;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "fname";
            this.ItemName.HeaderText = "项目名称";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            this.ItemName.Width = 160;
            // 
            // Itemfprint_num
            // 
            this.Itemfprint_num.DataPropertyName = "fprint_num";
            this.Itemfprint_num.HeaderText = "打印序号";
            this.Itemfprint_num.Name = "Itemfprint_num";
            this.Itemfprint_num.Width = 80;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonPrintSave);
            this.panel5.Controls.Add(this.buttonPrintRef);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(511, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(90, 440);
            this.panel5.TabIndex = 127;
            // 
            // buttonPrintSave
            // 
            this.buttonPrintSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrintSave.Location = new System.Drawing.Point(6, 11);
            this.buttonPrintSave.Name = "buttonPrintSave";
            this.buttonPrintSave.Size = new System.Drawing.Size(80, 23);
            this.buttonPrintSave.TabIndex = 127;
            this.buttonPrintSave.Text = "保存";
            this.buttonPrintSave.UseVisualStyleBackColor = true;
            this.buttonPrintSave.Click += new System.EventHandler(this.buttonPrintSave_Click);
            // 
            // buttonPrintRef
            // 
            this.buttonPrintRef.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrintRef.Location = new System.Drawing.Point(6, 59);
            this.buttonPrintRef.Name = "buttonPrintRef";
            this.buttonPrintRef.Size = new System.Drawing.Size(80, 23);
            this.buttonPrintRef.TabIndex = 126;
            this.buttonPrintRef.Text = "刷新";
            this.buttonPrintRef.UseVisualStyleBackColor = true;
            this.buttonPrintRef.Click += new System.EventHandler(this.buttonPrintRef_Click);
            // 
            // tabPageValue
            // 
            this.tabPageValue.Controls.Add(this.dataGridViewValue);
            this.tabPageValue.Controls.Add(this.panel6);
            this.tabPageValue.Location = new System.Drawing.Point(4, 21);
            this.tabPageValue.Name = "tabPageValue";
            this.tabPageValue.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageValue.Size = new System.Drawing.Size(604, 446);
            this.tabPageValue.TabIndex = 5;
            this.tabPageValue.Text = "常用取值";
            this.tabPageValue.UseVisualStyleBackColor = true;
            // 
            // dataGridViewValue
            // 
            this.dataGridViewValue.AllowUserToAddRows = false;
            this.dataGridViewValue.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewValue.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewValue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fvalue,
            this.fvalue_flag,
            this.forder_by,
            this.fvalue_id,
            this.valuefitem_id});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewValue.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewValue.EnableHeadersVisualStyles = false;
            this.dataGridViewValue.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewValue.MultiSelect = false;
            this.dataGridViewValue.Name = "dataGridViewValue";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewValue.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewValue.RowHeadersVisible = false;
            this.dataGridViewValue.RowHeadersWidth = 45;
            this.dataGridViewValue.RowTemplate.Height = 23;
            this.dataGridViewValue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewValue.Size = new System.Drawing.Size(508, 440);
            this.dataGridViewValue.TabIndex = 127;
            // 
            // fvalue
            // 
            this.fvalue.DataPropertyName = "fvalue";
            this.fvalue.HeaderText = "值";
            this.fvalue.Name = "fvalue";
            // 
            // fvalue_flag
            // 
            this.fvalue_flag.DataPropertyName = "fvalue_flag";
            this.fvalue_flag.HeaderText = "标记";
            this.fvalue_flag.Name = "fvalue_flag";
            this.fvalue_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fvalue_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fvalue_flag.Width = 80;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Width = 50;
            // 
            // fvalue_id
            // 
            this.fvalue_id.DataPropertyName = "fvalue_id";
            this.fvalue_id.HeaderText = "fvalue_id";
            this.fvalue_id.Name = "fvalue_id";
            this.fvalue_id.Visible = false;
            // 
            // valuefitem_id
            // 
            this.valuefitem_id.DataPropertyName = "fitem_id";
            this.valuefitem_id.HeaderText = "fitem_id";
            this.valuefitem_id.Name = "valuefitem_id";
            this.valuefitem_id.Visible = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.buttonItemValueSave);
            this.panel6.Controls.Add(this.buttonItemValueDel);
            this.panel6.Controls.Add(this.buttonItemValueAdd);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(511, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(90, 440);
            this.panel6.TabIndex = 128;
            // 
            // buttonItemValueSave
            // 
            this.buttonItemValueSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonItemValueSave.Location = new System.Drawing.Point(6, 59);
            this.buttonItemValueSave.Name = "buttonItemValueSave";
            this.buttonItemValueSave.Size = new System.Drawing.Size(80, 23);
            this.buttonItemValueSave.TabIndex = 127;
            this.buttonItemValueSave.Text = "保存";
            this.buttonItemValueSave.UseVisualStyleBackColor = true;
            this.buttonItemValueSave.Click += new System.EventHandler(this.buttonItemValueSave_Click);
            // 
            // buttonItemValueDel
            // 
            this.buttonItemValueDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonItemValueDel.Location = new System.Drawing.Point(6, 107);
            this.buttonItemValueDel.Name = "buttonItemValueDel";
            this.buttonItemValueDel.Size = new System.Drawing.Size(80, 23);
            this.buttonItemValueDel.TabIndex = 126;
            this.buttonItemValueDel.Text = "删除";
            this.buttonItemValueDel.UseVisualStyleBackColor = true;
            this.buttonItemValueDel.Click += new System.EventHandler(this.buttonItemValueDel_Click);
            // 
            // buttonItemValueAdd
            // 
            this.buttonItemValueAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonItemValueAdd.Location = new System.Drawing.Point(6, 11);
            this.buttonItemValueAdd.Name = "buttonItemValueAdd";
            this.buttonItemValueAdd.Size = new System.Drawing.Size(80, 23);
            this.buttonItemValueAdd.TabIndex = 125;
            this.buttonItemValueAdd.Text = "新增";
            this.buttonItemValueAdd.UseVisualStyleBackColor = true;
            this.buttonItemValueAdd.Click += new System.EventHandler(this.buttonItemValueAdd_Click);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(261, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "   修改(&U)";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(177, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "   刷新(&R)";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(93, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "   保存(&S)";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(345, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 23);
            this.button4.TabIndex = 1;
            this.button4.Text = "   删除(&A)";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(9, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(80, 23);
            this.button5.TabIndex = 0;
            this.button5.Text = "   新增(&A)";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorOK;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.BindingSource = this.sam_itemBindingSource;
            this.bindingNavigator1.CountItem = this.toolStripLabel1;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator3,
            this.bindingNavigatorOK,
            this.toolStripButtonNo});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 471);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.toolStripTextBox1;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(612, 35);
            this.bindingNavigator1.TabIndex = 129;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorOK
            // 
            this.bindingNavigatorOK.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorOK.Image")));
            this.bindingNavigatorOK.Name = "bindingNavigatorOK";
            this.bindingNavigatorOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.bindingNavigatorOK.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorOK.Size = new System.Drawing.Size(108, 32);
            this.bindingNavigatorOK.Text = "确 定(&S)  ";
            this.bindingNavigatorOK.Click += new System.EventHandler(this.bindingNavigatorOK_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(48, 32);
            this.toolStripLabel1.Text = "/ {0}";
            this.toolStripLabel1.ToolTipText = "总项数";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "位置";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 21);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "当前位置";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonNo
            // 
            this.toolStripButtonNo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNo.Image")));
            this.toolStripButtonNo.Name = "toolStripButtonNo";
            this.toolStripButtonNo.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonNo.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonNo.Size = new System.Drawing.Size(108, 32);
            this.toolStripButtonNo.Text = "取 消(&C)  ";
            this.toolStripButtonNo.Click += new System.EventHandler(this.toolStripButtonNo_Click);
            // 
            // fcode
            // 
            this.fcode.DataPropertyName = "fcode";
            this.fcode.HeaderText = "代号";
            this.fcode.Name = "fcode";
            this.fcode.Width = 200;
            // 
            // 换算率
            // 
            this.换算率.HeaderText = "换算率";
            this.换算率.Name = "换算率";
            // 
            // iofitem_id
            // 
            this.iofitem_id.DataPropertyName = "fitem_id";
            this.iofitem_id.HeaderText = "fitem_id";
            this.iofitem_id.Name = "iofitem_id";
            this.iofitem_id.Visible = false;
            // 
            // iofuse_if
            // 
            this.iofuse_if.DataPropertyName = "fuse_if";
            this.iofuse_if.HeaderText = "fuse_if";
            this.iofuse_if.Name = "iofuse_if";
            this.iofuse_if.Visible = false;
            // 
            // fio_id
            // 
            this.fio_id.DataPropertyName = "fio_id";
            this.fio_id.HeaderText = "fio_id";
            this.fio_id.Name = "fio_id";
            this.fio_id.Visible = false;
            // 
            // ItemDecForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(612, 506);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.bindingNavigator1);
            this.Name = "ItemDecForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "项目明细";
            this.Load += new System.EventHandler(this.ItemDecForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sam_itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRef)).EndInit();
            this.panel3.ResumeLayout(false);
            this.tabPagefyy.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIO)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPrint)).EndInit();
            this.panel5.ResumeLayout(false);
            this.tabPageValue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewValue)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_ref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource_fsample_type_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxInstr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPagefyy;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox fitem_codeTextBox;
        private System.Windows.Forms.BindingSource sam_itemBindingSource;
        private yunLis.lis.sam.samDataSet samDataSet;
        private System.Windows.Forms.ComboBox fcheck_type_idComboBox;
        private System.Windows.Forms.ComboBox fitem_type_idComboBox;
        private System.Windows.Forms.TextBox fhelp_codeTextBox;
        private System.Windows.Forms.TextBox fname_jTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fhis_item_codeTextBox;
        private System.Windows.Forms.ComboBox funit_nameComboBox;
        private System.Windows.Forms.ComboBox fvalue_ddComboBox;
        private System.Windows.Forms.TextBox fxsTextBox;
        private System.Windows.Forms.ComboBox fprint_type_idComboBox;
        private System.Windows.Forms.TextBox fpriceTextBox;
        private System.Windows.Forms.ComboBox fcheck_method_idComboBox;
        private System.Windows.Forms.CheckBox fuse_ifCheckBox;
        private System.Windows.Forms.TextBox fjx_formulaTextBox;
        private System.Windows.Forms.CheckBox fjx_ifCheckBox;
        private System.Windows.Forms.Label labelformula;
        private System.Windows.Forms.TextBox fref_lowTextBox;
        private System.Windows.Forms.TextBox fref_highTextBox;
        private System.Windows.Forms.TextBox frefTextBox;
        private System.Windows.Forms.Label labelref;
        private System.Windows.Forms.CheckBox fref_if_sexCheckBox;
        private System.Windows.Forms.CheckBox fref_if_ageCheckBox;
        private System.Windows.Forms.CheckBox fref_if_sampleCheckBox;
        private System.Windows.Forms.TextBox fname_eTextBox;
        private System.Windows.Forms.TextBox fvalue_day_noTextBox;
        private System.Windows.Forms.TextBox fvalue_day_numTextBox;
        private System.Windows.Forms.TextBox fjg_value_xxTextBox;
        private System.Windows.Forms.TextBox fjg_value_sxTextBox;
        private System.Windows.Forms.ComboBox fsam_type_idComboBox;
        private System.Windows.Forms.DataGridView dataGridViewRef;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button buttonrefDel;
        private System.Windows.Forms.Button buttonrefAdd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.BindingSource bindingSource_fsex;
        private System.Windows.Forms.BindingSource bindingSource_ref;
        private System.Windows.Forms.BindingSource bindingSource_fsample_type_id;
        private System.Windows.Forms.Button buttonrefSave;
        private System.Windows.Forms.DataGridViewComboBoxColumn fsex;
        private System.Windows.Forms.DataGridViewTextBoxColumn fage_low;
        private System.Windows.Forms.DataGridViewTextBoxColumn fage_high;
        private System.Windows.Forms.DataGridViewComboBoxColumn fsample_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref_low;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref_high;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fref_id;
        private System.Windows.Forms.RichTextBox fyyRichTextBox;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button buttonIOSave;
        private System.Windows.Forms.Button buttonIODel;
        private System.Windows.Forms.Button buttonIOAdd;
        private System.Windows.Forms.DataGridView dataGridViewIO;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonPrintSave;
        private System.Windows.Forms.Button buttonPrintRef;
        private System.Windows.Forms.DataGridView dataGridViewPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Itemfprint_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.TabPage tabPageValue;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button buttonItemValueSave;
        private System.Windows.Forms.Button buttonItemValueDel;
        private System.Windows.Forms.Button buttonItemValueAdd;
        private System.Windows.Forms.DataGridView dataGridViewValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue;
        private System.Windows.Forms.DataGridViewComboBoxColumn fvalue_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn valuefitem_id;
        protected System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorOK;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButtonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn 换算率;
        private System.Windows.Forms.DataGridViewTextBoxColumn iofitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn iofuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn fio_id;
    }
}