﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using yunLis.lisbll.sam;
namespace yunLis.lis.sam.dic
{
    public partial class InstrReportSetForm : yunLis.wwf.SysBaseForm
    {
        InstrBLL bll = new InstrBLL();
        TypeBLL bllType = new TypeBLL();//类别
        string currfp_id = "";//当前上级ID
        DataTable dtCurr = new DataTable();
        DataRowView rowCurr = null;
        DataTable dtCurrReport = new DataTable();
        public InstrReportSetForm()
        {
            InitializeComponent();
            dataGridViewReport.AutoGenerateColumns = false;
        }

        private void InstrReportSetForm_Load(object sender, EventArgs e)
        {
            GetCheckType();
            GetCheckGroup();
            GetTree("-1");
        }
        private void GetCurrDT()
        {
            try
            {                //dtCurr = new DataTable();
                if (dtCurr != null)
                    dtCurr.Clear();
                dtCurr = this.bll.BllInstrDTByTypeID(currfp_id);
                bindingSourceObject.DataSource = dtCurr;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        /// <summary>
        /// 取得检验类别
        /// </summary>
        private void GetCheckType()
        {
            try
            {
                this.fjytype_id.ValueMember = "fcheck_type_id";
                this.fjytype_id.DisplayMember = "fname";
                this.bindingSourceJYType.DataSource = this.bllType.BllCheckTypeDT(1);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 取得检验组
        /// </summary>
        private void GetCheckGroup()
        {
            try
            {
                this.fjygroup_id.ValueMember = "fdept_id";
                this.fjygroup_id.DisplayMember = "fname";
                this.bindingSourceJYGroup.DataSource = this.bllType.BllCheckGroupDT(1);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllTypeDT(2);
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "ftype_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                currfp_id = e.Node.Name.ToString();
                GetCurrDT();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void bindingSourceObject_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurr = (DataRowView)bindingSourceObject.Current;//行   
                if (this.dtCurrReport.Rows.Count > 0)
                    this.dtCurrReport.Clear();
                if (rowCurr != null)
                {
                   
                    this.dtCurrReport = this.bll.BllReportDT(rowCurr["finstr_id"].ToString());
                    this.dataGridViewReport.DataSource = dtCurrReport;
                    bindingSource1.DataSource = dtCurrReport;
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

       
       
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {

            try
            {
                if (rowCurr != null)
                {
                    DataRow dr = null;
                    dr = this.dtCurrReport.NewRow();
                    //string guid = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                    dr["fsetting_id"] = WEISHENG.COMM.Helper.GuidHelper.DbGuid(); 
                    dr["finstr_id"] = rowCurr["finstr_id"].ToString();//仪器ID
                    //dr["ftype_id"] = rowCurr["ftype_id"].ToString();//检验类别
                    dr["fuse_if"] = "1";
                    dr["fpage_width"] = "1";
                    dr["fpage_height"] = "1";
                    dr["fmargin_top"] = "1";
                    dr["fmargin_bottom"] = "1";
                    dr["fmargin_left"] = "1";
                    dr["fmargin_right"] = "1";
                    dr["frows"] = "1";
                    dr["fone_count"] = "1";
                    if (this.bll.BllReportAdd(dr) > 0)
                        this.dtCurrReport.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.dataGridViewReport.EndEdit();
                for (int i = 0; i < this.dtCurrReport.Rows.Count; i++)
                {
                    this.bll.BllReportUpdate(this.dtCurrReport.Rows[i]);
                }
                if (rowCurr != null)
                {
                    this.dtCurrReport = this.bll.BllReportDT(rowCurr["finstr_id"].ToString());
                    this.dataGridViewReport.DataSource = dtCurrReport;
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

    }
}

/*
 Name	Code	Data Type	Primary	Foreign Key	Mandatory
报表设置_id	fsetting_id	varchar(32)	TRUE	FALSE	TRUE
报表类型_id	ftype_id	varchar(32)	FALSE	TRUE	FALSE
仪器_id	finstr_id	varchar(32)	FALSE	FALSE	FALSE
报表代码	fcode	varchar(32)	FALSE	FALSE	FALSE
报表名称	fname	varchar(128)	FALSE	FALSE	FALSE
打印机名	fprinter_name	varchar(128)	FALSE	FALSE	FALSE
纸张名	fpaper_name	varchar(128)	FALSE	FALSE	FALSE
面宽	fpage_width	float	FALSE	FALSE	FALSE
面高	fpage_height	float	FALSE	FALSE	FALSE
边顶	fmargin_top	float	FALSE	FALSE	FALSE
边底	fmargin_bottom	float	FALSE	FALSE	FALSE
边左	fmargin_left	float	FALSE	FALSE	FALSE
边右	fmargin_right	float	FALSE	FALSE	FALSE
方向	forientation	varchar(32)	FALSE	FALSE	FALSE
启用否	fuse_if	int	FALSE	FALSE	FALSE
序号	forder_by	varchar(32)	FALSE	FALSE	FALSE
第页固定行数	frows	int	FALSE	FALSE	FALSE
项目数超过数打第二页	fone_count	int	FALSE	FALSE	FALSE
阳性结果提示	fy_mess	varchar(32)	FALSE	FALSE	FALSE
弱阳性结果提示	fry_mess	varchar(32)	FALSE	FALSE	FALSE
偏高结果提示	fhight_mess	varchar(32)	FALSE	FALSE	FALSE
偏低结果提示	flow_mess	varchar(32)	FALSE	FALSE	FALSE
错误结果提示	ferror_mess	varchar(32)	FALSE	FALSE	FALSE
自定义1	f1	varchar(32)	FALSE	FALSE	FALSE
自定义2	f2	varchar(32)	FALSE	FALSE	FALSE
自定义3	f3	varchar(32)	FALSE	FALSE	FALSE
自定义4	f4	varchar(32)	FALSE	FALSE	FALSE
自定义5	f5	varchar(32)	FALSE	FALSE	FALSE
自定义6	f6	varchar(32)	FALSE	FALSE	FALSE
自定义7	f7	varchar(32)	FALSE	FALSE	FALSE
自定义8	f8	varchar(32)	FALSE	FALSE	FALSE
自定义9	f9	varchar(32)	FALSE	FALSE	FALSE
自定义10	f10	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
 */