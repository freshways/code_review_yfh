﻿namespace yunLis.lis.sam.dic
{
    partial class ItemGroupForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemGroupForm));
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList();
            this.buttonNo = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton确定 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl组合内项目 = new DevExpress.XtraGrid.GridControl();
            this.gridView组合内项目 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grd项目内_col代号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd项目内_col名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd项目内_col排序 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd项目内_col收费名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd项目内_col收费单价 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl全部项目 = new DevExpress.XtraGrid.GridControl();
            this.gridView全部项目 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col代号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd全部项目_col名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd全部项目_col助记码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd全部项目_col排序 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd全部项目_colfcheck_type_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grd全部项目_colfsam_type_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.searchControl筛选组合内项目 = new DevExpress.XtraEditors.SearchControl();
            this.searchControl筛选全部项目 = new DevExpress.XtraEditors.SearchControl();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupBottom = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupLeft = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.gridControl全部组合 = new DevExpress.XtraGrid.GridControl();
            this.gridView全部组合 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col启用 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.col急诊 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col非急诊 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col助记符 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col顺序 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col备注 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfcheck_type_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfsam_type_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col收费编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelpCode = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator();
            this.toolStripButton收费项目 = new System.Windows.Forms.ToolStripButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.searchControl筛选项目组合 = new DevExpress.XtraEditors.SearchControl();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTopLeft = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl组合内项目)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView组合内项目)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl全部项目)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView全部项目)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl筛选组合内项目.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl筛选全部项目.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl全部组合)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView全部组合)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl筛选项目组合.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTopLeft)).BeginInit();
            this.SuspendLayout();
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 0);
            this.wwTreeView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(233, 502);
            this.wwTreeView1.TabIndex = 130;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // buttonNo
            // 
            this.buttonNo.Location = new System.Drawing.Point(298, 81);
            this.buttonNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonNo.Name = "buttonNo";
            this.buttonNo.Size = new System.Drawing.Size(82, 36);
            this.buttonNo.StyleController = this.layoutControl1;
            this.buttonNo.TabIndex = 3;
            this.buttonNo.Text = "<=";
            this.buttonNo.Click += new System.EventHandler(this.buttonNo_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton确定);
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.gridControl组合内项目);
            this.layoutControl1.Controls.Add(this.gridControl全部项目);
            this.layoutControl1.Controls.Add(this.searchControl筛选组合内项目);
            this.layoutControl1.Controls.Add(this.searchControl筛选全部项目);
            this.layoutControl1.Controls.Add(this.buttonNo);
            this.layoutControl1.Controls.Add(this.buttonOk);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(741, 437, 844, 543);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(785, 196);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton确定
            // 
            this.simpleButton确定.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton确定.ImageOptions.Image")));
            this.simpleButton确定.Location = new System.Drawing.Point(605, 155);
            this.simpleButton确定.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton确定.Name = "simpleButton确定";
            this.simpleButton确定.Size = new System.Drawing.Size(80, 27);
            this.simpleButton确定.StyleController = this.layoutControl1;
            this.simpleButton确定.TabIndex = 144;
            this.simpleButton确定.Text = "确定";
            this.simpleButton确定.Click += new System.EventHandler(this.simpleButton确定_Click);
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton取消.ImageOptions.Image")));
            this.simpleButton取消.Location = new System.Drawing.Point(689, 155);
            this.simpleButton取消.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(82, 27);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 143;
            this.simpleButton取消.Text = "取消";
            this.simpleButton取消.Click += new System.EventHandler(this.simpleButton取消_Click_1);
            // 
            // gridControl组合内项目
            // 
            this.gridControl组合内项目.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl组合内项目.Location = new System.Drawing.Point(385, 40);
            this.gridControl组合内项目.MainView = this.gridView组合内项目;
            this.gridControl组合内项目.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl组合内项目.Name = "gridControl组合内项目";
            this.gridControl组合内项目.Size = new System.Drawing.Size(388, 109);
            this.gridControl组合内项目.TabIndex = 142;
            this.gridControl组合内项目.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView组合内项目});
            // 
            // gridView组合内项目
            // 
            this.gridView组合内项目.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.grd项目内_col代号,
            this.grd项目内_col名称,
            this.grd项目内_col排序,
            this.grd项目内_col收费名称,
            this.grd项目内_col收费单价});
            this.gridView组合内项目.DetailHeight = 525;
            this.gridView组合内项目.FixedLineWidth = 3;
            this.gridView组合内项目.GridControl = this.gridControl组合内项目;
            this.gridView组合内项目.Name = "gridView组合内项目";
            this.gridView组合内项目.OptionsBehavior.Editable = false;
            this.gridView组合内项目.OptionsView.ShowFooter = true;
            this.gridView组合内项目.OptionsView.ShowGroupPanel = false;
            // 
            // grd项目内_col代号
            // 
            this.grd项目内_col代号.Caption = "代号";
            this.grd项目内_col代号.FieldName = "fitem_code";
            this.grd项目内_col代号.MinWidth = 26;
            this.grd项目内_col代号.Name = "grd项目内_col代号";
            this.grd项目内_col代号.Visible = true;
            this.grd项目内_col代号.VisibleIndex = 0;
            this.grd项目内_col代号.Width = 99;
            // 
            // grd项目内_col名称
            // 
            this.grd项目内_col名称.Caption = "名称";
            this.grd项目内_col名称.FieldName = "fname";
            this.grd项目内_col名称.MinWidth = 26;
            this.grd项目内_col名称.Name = "grd项目内_col名称";
            this.grd项目内_col名称.Visible = true;
            this.grd项目内_col名称.VisibleIndex = 1;
            this.grd项目内_col名称.Width = 99;
            // 
            // grd项目内_col排序
            // 
            this.grd项目内_col排序.Caption = "排序";
            this.grd项目内_col排序.FieldName = "fprint_num";
            this.grd项目内_col排序.MinWidth = 26;
            this.grd项目内_col排序.Name = "grd项目内_col排序";
            this.grd项目内_col排序.Visible = true;
            this.grd项目内_col排序.VisibleIndex = 2;
            this.grd项目内_col排序.Width = 99;
            // 
            // grd项目内_col收费名称
            // 
            this.grd项目内_col收费名称.Caption = "收费名称";
            this.grd项目内_col收费名称.FieldName = "收费名称";
            this.grd项目内_col收费名称.MinWidth = 26;
            this.grd项目内_col收费名称.Name = "grd项目内_col收费名称";
            this.grd项目内_col收费名称.Visible = true;
            this.grd项目内_col收费名称.VisibleIndex = 3;
            this.grd项目内_col收费名称.Width = 99;
            // 
            // grd项目内_col收费单价
            // 
            this.grd项目内_col收费单价.Caption = "收费单价";
            this.grd项目内_col收费单价.FieldName = "收费单价";
            this.grd项目内_col收费单价.MinWidth = 26;
            this.grd项目内_col收费单价.Name = "grd项目内_col收费单价";
            this.grd项目内_col收费单价.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "收费单价", "SUM={0:0.##}")});
            this.grd项目内_col收费单价.Visible = true;
            this.grd项目内_col收费单价.VisibleIndex = 4;
            this.grd项目内_col收费单价.Width = 99;
            // 
            // gridControl全部项目
            // 
            this.gridControl全部项目.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl全部项目.Location = new System.Drawing.Point(13, 41);
            this.gridControl全部项目.MainView = this.gridView全部项目;
            this.gridControl全部项目.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl全部项目.Name = "gridControl全部项目";
            this.gridControl全部项目.Size = new System.Drawing.Size(281, 107);
            this.gridControl全部项目.TabIndex = 141;
            this.gridControl全部项目.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView全部项目});
            // 
            // gridView全部项目
            // 
            this.gridView全部项目.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col代号,
            this.grd全部项目_col名称,
            this.grd全部项目_col助记码,
            this.grd全部项目_col排序,
            this.grd全部项目_colfcheck_type_id,
            this.grd全部项目_colfsam_type_id});
            this.gridView全部项目.DetailHeight = 525;
            this.gridView全部项目.FixedLineWidth = 3;
            this.gridView全部项目.GridControl = this.gridControl全部项目;
            this.gridView全部项目.Name = "gridView全部项目";
            this.gridView全部项目.OptionsBehavior.Editable = false;
            this.gridView全部项目.OptionsView.ShowGroupPanel = false;
            // 
            // col代号
            // 
            this.col代号.Caption = "代号";
            this.col代号.FieldName = "fitem_code";
            this.col代号.MinWidth = 26;
            this.col代号.Name = "col代号";
            this.col代号.Visible = true;
            this.col代号.VisibleIndex = 0;
            this.col代号.Width = 99;
            // 
            // grd全部项目_col名称
            // 
            this.grd全部项目_col名称.Caption = "名称";
            this.grd全部项目_col名称.FieldName = "fname";
            this.grd全部项目_col名称.MinWidth = 26;
            this.grd全部项目_col名称.Name = "grd全部项目_col名称";
            this.grd全部项目_col名称.Visible = true;
            this.grd全部项目_col名称.VisibleIndex = 1;
            this.grd全部项目_col名称.Width = 99;
            // 
            // grd全部项目_col助记码
            // 
            this.grd全部项目_col助记码.Caption = "助记码";
            this.grd全部项目_col助记码.FieldName = "fhelp_code";
            this.grd全部项目_col助记码.MinWidth = 26;
            this.grd全部项目_col助记码.Name = "grd全部项目_col助记码";
            this.grd全部项目_col助记码.Visible = true;
            this.grd全部项目_col助记码.VisibleIndex = 2;
            this.grd全部项目_col助记码.Width = 99;
            // 
            // grd全部项目_col排序
            // 
            this.grd全部项目_col排序.Caption = "排序";
            this.grd全部项目_col排序.FieldName = "fprint_num";
            this.grd全部项目_col排序.MinWidth = 26;
            this.grd全部项目_col排序.Name = "grd全部项目_col排序";
            this.grd全部项目_col排序.Visible = true;
            this.grd全部项目_col排序.VisibleIndex = 3;
            this.grd全部项目_col排序.Width = 99;
            // 
            // grd全部项目_colfcheck_type_id
            // 
            this.grd全部项目_colfcheck_type_id.Caption = "fcheck_type_id";
            this.grd全部项目_colfcheck_type_id.FieldName = "fcheck_type_id";
            this.grd全部项目_colfcheck_type_id.MinWidth = 26;
            this.grd全部项目_colfcheck_type_id.Name = "grd全部项目_colfcheck_type_id";
            this.grd全部项目_colfcheck_type_id.Width = 99;
            // 
            // grd全部项目_colfsam_type_id
            // 
            this.grd全部项目_colfsam_type_id.Caption = "fsam_type_id";
            this.grd全部项目_colfsam_type_id.FieldName = "fsam_type_id";
            this.grd全部项目_colfsam_type_id.MinWidth = 26;
            this.grd全部项目_colfsam_type_id.Name = "grd全部项目_colfsam_type_id";
            this.grd全部项目_colfsam_type_id.Width = 99;
            // 
            // searchControl筛选组合内项目
            // 
            this.searchControl筛选组合内项目.Location = new System.Drawing.Point(508, 12);
            this.searchControl筛选组合内项目.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.searchControl筛选组合内项目.Name = "searchControl筛选组合内项目";
            this.searchControl筛选组合内项目.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl筛选组合内项目.Size = new System.Drawing.Size(265, 24);
            this.searchControl筛选组合内项目.StyleController = this.layoutControl1;
            this.searchControl筛选组合内项目.TabIndex = 140;
            // 
            // searchControl筛选全部项目
            // 
            this.searchControl筛选全部项目.Location = new System.Drawing.Point(136, 13);
            this.searchControl筛选全部项目.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.searchControl筛选全部项目.Name = "searchControl筛选全部项目";
            this.searchControl筛选全部项目.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl筛选全部项目.Size = new System.Drawing.Size(158, 24);
            this.searchControl筛选全部项目.StyleController = this.layoutControl1;
            this.searchControl筛选全部项目.TabIndex = 139;
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(298, 31);
            this.buttonOk.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 36);
            this.buttonOk.StyleController = this.layoutControl1;
            this.buttonOk.TabIndex = 2;
            this.buttonOk.Text = "=>";
            this.buttonOk.Click += new System.EventHandler(this.button分配_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlGroupBottom,
            this.layoutControlGroupLeft});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(785, 196);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.searchControl筛选组合内项目;
            this.layoutControlItem9.Location = new System.Drawing.Point(373, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(392, 28);
            this.layoutControlItem9.Text = "筛选组合内项目：";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(120, 18);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.gridControl组合内项目;
            this.layoutControlItem11.Location = new System.Drawing.Point(373, 28);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(392, 113);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlGroupBottom
            // 
            this.layoutControlGroupBottom.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3,
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroupBottom.Location = new System.Drawing.Point(0, 141);
            this.layoutControlGroupBottom.Name = "layoutControlGroupBottom";
            this.layoutControlGroupBottom.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroupBottom.Size = new System.Drawing.Size(765, 35);
            this.layoutControlGroupBottom.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupBottom.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(591, 31);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton确定;
            this.layoutControlItem2.Location = new System.Drawing.Point(591, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(84, 31);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton取消;
            this.layoutControlItem1.Location = new System.Drawing.Point(675, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 31);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupLeft
            // 
            this.layoutControlGroupLeft.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.emptySpaceItem4,
            this.layoutControlItem10,
            this.layoutControlItem3,
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.emptySpaceItem1});
            this.layoutControlGroupLeft.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupLeft.Name = "layoutControlGroupLeft";
            this.layoutControlGroupLeft.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupLeft.Size = new System.Drawing.Size(373, 141);
            this.layoutControlGroupLeft.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupLeft.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.searchControl筛选全部项目;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(285, 28);
            this.layoutControlItem8.Text = "筛选全部项目：";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(120, 18);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(285, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(86, 18);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.gridControl全部项目;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(285, 111);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonOk;
            this.layoutControlItem3.Location = new System.Drawing.Point(285, 18);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 40);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 40);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 40);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(285, 58);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(86, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonNo;
            this.layoutControlItem4.Location = new System.Drawing.Point(285, 68);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 40);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 40);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 40);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(285, 108);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(86, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(86, 15);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(86, 31);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // gridControl全部组合
            // 
            this.gridControl全部组合.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl全部组合.Location = new System.Drawing.Point(12, 40);
            this.gridControl全部组合.MainView = this.gridView全部组合;
            this.gridControl全部组合.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl全部组合.Name = "gridControl全部组合";
            this.gridControl全部组合.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3});
            this.gridControl全部组合.Size = new System.Drawing.Size(511, 248);
            this.gridControl全部组合.TabIndex = 136;
            this.gridControl全部组合.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView全部组合});
            // 
            // gridView全部组合
            // 
            this.gridView全部组合.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col启用,
            this.col急诊,
            this.col非急诊,
            this.col名称,
            this.col助记符,
            this.col顺序,
            this.col备注,
            this.colfcheck_type_id,
            this.colfsam_type_id,
            this.col收费编码});
            this.gridView全部组合.DetailHeight = 525;
            this.gridView全部组合.FixedLineWidth = 3;
            this.gridView全部组合.GridControl = this.gridControl全部组合;
            this.gridView全部组合.Name = "gridView全部组合";
            this.gridView全部组合.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView全部组合.OptionsSelection.MultiSelect = true;
            this.gridView全部组合.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView全部组合.OptionsView.ShowGroupPanel = false;
            this.gridView全部组合.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView全部组合_SelectionChanged);
            this.gridView全部组合.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView组合列表_FocusedRowChanged);
            this.gridView全部组合.GotFocus += new System.EventHandler(this.gridView全部组合_GotFocus);
            // 
            // col启用
            // 
            this.col启用.Caption = "启用";
            this.col启用.ColumnEdit = this.repositoryItemCheckEdit3;
            this.col启用.FieldName = "fuse_if";
            this.col启用.MinWidth = 26;
            this.col启用.Name = "col启用";
            this.col启用.OptionsFilter.AllowAutoFilter = false;
            this.col启用.OptionsFilter.AllowFilter = false;
            this.col启用.Visible = true;
            this.col启用.VisibleIndex = 6;
            this.col启用.Width = 26;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.QueryCheckStateByValue += new DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventHandler(this.repositoryItemCheckEdit3_QueryCheckStateByValue);
            // 
            // col急诊
            // 
            this.col急诊.Caption = "急诊";
            this.col急诊.ColumnEdit = this.repositoryItemCheckEdit3;
            this.col急诊.FieldName = "fjz_if";
            this.col急诊.MinWidth = 26;
            this.col急诊.Name = "col急诊";
            this.col急诊.OptionsFilter.AllowAutoFilter = false;
            this.col急诊.OptionsFilter.AllowFilter = false;
            this.col急诊.Visible = true;
            this.col急诊.VisibleIndex = 7;
            this.col急诊.Width = 26;
            // 
            // col非急诊
            // 
            this.col非急诊.Caption = "非急诊";
            this.col非急诊.ColumnEdit = this.repositoryItemCheckEdit3;
            this.col非急诊.FieldName = "fjz_no_if";
            this.col非急诊.MinWidth = 26;
            this.col非急诊.Name = "col非急诊";
            this.col非急诊.OptionsFilter.AllowAutoFilter = false;
            this.col非急诊.OptionsFilter.AllowFilter = false;
            this.col非急诊.Visible = true;
            this.col非急诊.VisibleIndex = 8;
            this.col非急诊.Width = 26;
            // 
            // col名称
            // 
            this.col名称.Caption = "组合名称";
            this.col名称.FieldName = "fname";
            this.col名称.MinWidth = 26;
            this.col名称.Name = "col名称";
            this.col名称.OptionsFilter.AllowAutoFilter = false;
            this.col名称.OptionsFilter.AllowFilter = false;
            this.col名称.Visible = true;
            this.col名称.VisibleIndex = 1;
            this.col名称.Width = 194;
            // 
            // col助记符
            // 
            this.col助记符.Caption = "助记符";
            this.col助记符.FieldName = "fhelp_code";
            this.col助记符.MinWidth = 26;
            this.col助记符.Name = "col助记符";
            this.col助记符.OptionsFilter.AllowAutoFilter = false;
            this.col助记符.OptionsFilter.AllowFilter = false;
            this.col助记符.Visible = true;
            this.col助记符.VisibleIndex = 2;
            this.col助记符.Width = 91;
            // 
            // col顺序
            // 
            this.col顺序.Caption = "顺序";
            this.col顺序.FieldName = "forder_by";
            this.col顺序.MinWidth = 26;
            this.col顺序.Name = "col顺序";
            this.col顺序.OptionsFilter.AllowAutoFilter = false;
            this.col顺序.OptionsFilter.AllowFilter = false;
            this.col顺序.Visible = true;
            this.col顺序.VisibleIndex = 3;
            this.col顺序.Width = 53;
            // 
            // col备注
            // 
            this.col备注.Caption = "备注";
            this.col备注.FieldName = "fremark";
            this.col备注.MinWidth = 26;
            this.col备注.Name = "col备注";
            this.col备注.OptionsFilter.AllowAutoFilter = false;
            this.col备注.OptionsFilter.AllowFilter = false;
            this.col备注.Visible = true;
            this.col备注.VisibleIndex = 5;
            this.col备注.Width = 151;
            // 
            // colfcheck_type_id
            // 
            this.colfcheck_type_id.Caption = "fcheck_type_id";
            this.colfcheck_type_id.FieldName = "fcheck_type_id";
            this.colfcheck_type_id.MinWidth = 26;
            this.colfcheck_type_id.Name = "colfcheck_type_id";
            this.colfcheck_type_id.Width = 99;
            // 
            // colfsam_type_id
            // 
            this.colfsam_type_id.Caption = "fsam_type_id";
            this.colfsam_type_id.FieldName = "fsam_type_id";
            this.colfsam_type_id.MinWidth = 26;
            this.colfsam_type_id.Name = "colfsam_type_id";
            this.colfsam_type_id.Width = 99;
            // 
            // col收费编码
            // 
            this.col收费编码.Caption = "收费编码";
            this.col收费编码.FieldName = "收费编码";
            this.col收费编码.MinWidth = 25;
            this.col收费编码.Name = "col收费编码";
            this.col收费编码.Visible = true;
            this.col收费编码.VisibleIndex = 4;
            this.col收费编码.Width = 94;
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 45);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(66, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(38, 42);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 45);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(92, 42);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(90, 42);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // toolStripButtonHelpCode
            // 
            this.toolStripButtonHelpCode.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelpCode.Image")));
            this.toolStripButtonHelpCode.Name = "toolStripButtonHelpCode";
            this.toolStripButtonHelpCode.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonHelpCode.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonHelpCode.Size = new System.Drawing.Size(116, 42);
            this.toolStripButtonHelpCode.Text = "生成助记符  ";
            this.toolStripButtonHelpCode.Click += new System.EventHandler(this.toolStripButtonHelpCode_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(92, 42);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = null;
            this.bindingNavigator1.AutoSize = false;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = null;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonS,
            this.toolStripButtonHelpCode,
            this.toolStripButton收费项目,
            this.toolStripButtonDel});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(1024, 45);
            this.bindingNavigator1.TabIndex = 129;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // toolStripButton收费项目
            // 
            this.toolStripButton收费项目.Image = global::yunLis.Properties.Resources.wwNavigatorQuery;
            this.toolStripButton收费项目.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton收费项目.Name = "toolStripButton收费项目";
            this.toolStripButton收费项目.Size = new System.Drawing.Size(93, 42);
            this.toolStripButton收费项目.Text = "收费项目";
            this.toolStripButton收费项目.Click += new System.EventHandler(this.toolStripButton收费项目_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 45);
            this.splitContainerControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.wwTreeView1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1024, 502);
            this.splitContainerControl1.SplitterPosition = 233;
            this.splitContainerControl1.TabIndex = 137;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.layoutControl2);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.layoutControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(785, 502);
            this.splitContainerControl2.SplitterPosition = 300;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.richTextBox1);
            this.layoutControl2.Controls.Add(this.searchControl筛选项目组合);
            this.layoutControl2.Controls.Add(this.gridControl全部组合);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(260, 604, 650, 400);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(785, 300);
            this.layoutControl2.TabIndex = 137;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(527, 12);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(246, 276);
            this.richTextBox1.TabIndex = 139;
            this.richTextBox1.Text = "";
            // 
            // searchControl筛选项目组合
            // 
            this.searchControl筛选项目组合.Location = new System.Drawing.Point(80, 12);
            this.searchControl筛选项目组合.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.searchControl筛选项目组合.Name = "searchControl筛选项目组合";
            this.searchControl筛选项目组合.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl筛选项目组合.Size = new System.Drawing.Size(443, 24);
            this.searchControl筛选项目组合.StyleController = this.layoutControl2;
            this.searchControl筛选项目组合.TabIndex = 138;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItemTopLeft});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(785, 300);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl全部组合;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(515, 252);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.searchControl筛选项目组合;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(515, 28);
            this.layoutControlItem7.Text = "筛选组合:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(65, 18);
            // 
            // layoutControlItemTopLeft
            // 
            this.layoutControlItemTopLeft.Control = this.richTextBox1;
            this.layoutControlItemTopLeft.Location = new System.Drawing.Point(515, 0);
            this.layoutControlItemTopLeft.Name = "layoutControlItemTopLeft";
            this.layoutControlItemTopLeft.Size = new System.Drawing.Size(250, 280);
            this.layoutControlItemTopLeft.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemTopLeft.TextVisible = false;
            // 
            // ItemGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 547);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.bindingNavigator1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "ItemGroupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "检验项目组合";
            this.Load += new System.EventHandler(this.ItemGroupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl组合内项目)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView组合内项目)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl全部项目)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView全部项目)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl筛选组合内项目.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl筛选全部项目.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl全部组合)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView全部组合)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl筛选项目组合.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTopLeft)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.ImageList imageListTree;
        private DevExpress.XtraEditors.SimpleButton buttonNo;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraGrid.GridControl gridControl全部组合;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView全部组合;
        private DevExpress.XtraGrid.Columns.GridColumn col启用;
        private DevExpress.XtraGrid.Columns.GridColumn col急诊;
        private DevExpress.XtraGrid.Columns.GridColumn col非急诊;
        private DevExpress.XtraGrid.Columns.GridColumn col名称;
        private DevExpress.XtraGrid.Columns.GridColumn col助记符;
        private DevExpress.XtraGrid.Columns.GridColumn col顺序;
        private DevExpress.XtraGrid.Columns.GridColumn col备注;
        private DevExpress.XtraGrid.Columns.GridColumn colfcheck_type_id;
        private DevExpress.XtraGrid.Columns.GridColumn colfsam_type_id;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelpCode;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        protected System.Windows.Forms.BindingNavigator bindingNavigator1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.SearchControl searchControl筛选项目组合;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTopLeft;
        private DevExpress.XtraEditors.SearchControl searchControl筛选组合内项目;
        private DevExpress.XtraEditors.SearchControl searchControl筛选全部项目;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraGrid.GridControl gridControl组合内项目;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView组合内项目;
        private DevExpress.XtraGrid.GridControl gridControl全部项目;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView全部项目;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraGrid.Columns.GridColumn col代号;
        private DevExpress.XtraGrid.Columns.GridColumn grd全部项目_col名称;
        private DevExpress.XtraGrid.Columns.GridColumn grd全部项目_col助记码;
        private DevExpress.XtraGrid.Columns.GridColumn grd全部项目_col排序;
        private DevExpress.XtraGrid.Columns.GridColumn grd全部项目_colfcheck_type_id;
        private DevExpress.XtraGrid.Columns.GridColumn grd全部项目_colfsam_type_id;
        private DevExpress.XtraGrid.Columns.GridColumn grd项目内_col代号;
        private DevExpress.XtraGrid.Columns.GridColumn grd项目内_col名称;
        private DevExpress.XtraGrid.Columns.GridColumn grd项目内_col排序;
        private DevExpress.XtraGrid.Columns.GridColumn grd项目内_col收费名称;
        private DevExpress.XtraGrid.Columns.GridColumn grd项目内_col收费单价;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton simpleButton确定;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupBottom;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupLeft;
        private DevExpress.XtraGrid.Columns.GridColumn col收费编码;
        private System.Windows.Forms.ToolStripButton toolStripButton收费项目;
    }
}