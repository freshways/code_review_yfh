﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.lisbll.sam;

namespace yunLis.lis.sam.dic
{
    public partial class CheckTypeForm :yunLis.wwf.SysBaseForm
    {
        TypeBLL bllType = new TypeBLL();//类别
        public CheckTypeForm()
        {
            InitializeComponent();
        }

        private void CheckTypeForm_Load(object sender, EventArgs e)
        {
            GetCheckType();
        }
        /// <summary>
        /// 取得检验类别
        /// </summary>
        private void GetCheckType()
        {
            try
            {
                bindingSource1.DataSource = this.bllType.BllCheckTypeDT(1);
                this.DataGridViewObject.AutoGenerateColumns = false;
                this.DataGridViewObject.DataSource = bindingSource1;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
    }
}