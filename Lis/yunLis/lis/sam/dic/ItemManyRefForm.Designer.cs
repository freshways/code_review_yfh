﻿namespace yunLis.lis.sam.dic
{
    partial class ItemManyRefForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemManyRefForm));
            this.butSave = new System.Windows.Forms.Button();
            this.richTextBoxValue = new System.Windows.Forms.RichTextBox();
            this.butDel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // butSave
            // 
            this.butSave.Image = ((System.Drawing.Image)(resources.GetObject("butSave.Image")));
            this.butSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.Location = new System.Drawing.Point(339, 214);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(80, 23);
            this.butSave.TabIndex = 124;
            this.butSave.Text = "确定";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // richTextBoxValue
            // 
            this.richTextBoxValue.Dock = System.Windows.Forms.DockStyle.Left;
            this.richTextBoxValue.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxValue.Name = "richTextBoxValue";
            this.richTextBoxValue.Size = new System.Drawing.Size(321, 299);
            this.richTextBoxValue.TabIndex = 125;
            this.richTextBoxValue.Text = "";
            // 
            // butDel
            // 
            this.butDel.Image = ((System.Drawing.Image)(resources.GetObject("butDel.Image")));
            this.butDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butDel.Location = new System.Drawing.Point(339, 252);
            this.butDel.Name = "butDel";
            this.butDel.Size = new System.Drawing.Size(80, 23);
            this.butDel.TabIndex = 126;
            this.butDel.Text = "取消";
            this.butDel.UseVisualStyleBackColor = true;
            this.butDel.Click += new System.EventHandler(this.butDel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(337, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 12);
            this.label1.TabIndex = 127;
            this.label1.Text = "每行用\"/\"结束，如下：";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Enabled = false;
            this.richTextBox1.Location = new System.Drawing.Point(339, 57);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(116, 151);
            this.richTextBox1.TabIndex = 128;
            this.richTextBox1.Text = "1--5/\n9--10/\n";
            // 
            // ItemManyRefForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 299);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.butDel);
            this.Controls.Add(this.richTextBoxValue);
            this.Controls.Add(this.butSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ItemManyRefForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "多行参考值";
            this.Load += new System.EventHandler(this.ItemManyRefForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.RichTextBox richTextBoxValue;
        private System.Windows.Forms.Button butDel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}