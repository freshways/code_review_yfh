﻿using System;
using System.Data;
using System.Windows.Forms;
using WEISHENG.COMM.PluginsAttribute;
using yunLis.lis.com;
using yunLis.lisbll.sam;
using yunLis.wwfbll;
namespace yunLis.lis.sam.dic
{
    [ClassInfoMark(GUID = "5743B38F-BFF0-4851-9158-E76B7DE66DA6", 键ID = "5743B38F-BFF0-4851-9158-E76B7DE66DA6", 父ID = "864BDFE3-63E6-408F-98F3-270953B15BCD",
    功能名称 = "仪器设置", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.dic.InstrForm", 显示顺序 = 1,
    传递参数 = "", 菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public partial class InstrForm : yunLis.wwf.SysBaseForm
    {
        InstrBLL bll = new InstrBLL();
        TypeBLL bllType = new TypeBLL();//类别
        string currfp_id = "";//当前上级ID
        DataTable dtCurr = new DataTable();
        DataRowView rowCurr = null;
        string strfinstr_id = "";//仪器ID
        public InstrForm()
        {
            InitializeComponent();
            this.dataGridView1.AutoGenerateColumns = false;
        }

        private void InstrForm_Load(object sender, EventArgs e)
        {
            GetCheckType();
            GetCheckGroup();
            GetTree("-1");
        }
        private void GetCurrDT()
        {
            try
            {                //dtCurr = new DataTable();
                if (dtCurr != null)
                    dtCurr.Clear();
                dtCurr = this.bll.BllInstrDTByTypeID(currfp_id);
                bindingSourceObject.DataSource = dtCurr;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        /// <summary>
        /// 取得检验类别
        /// </summary>
        private void GetCheckType()
        {
            try
            {
                this.fjytype_id.ValueMember = "fcheck_type_id";
                this.fjytype_id.DisplayMember = "fname";
                this.bindingSourceJYType.DataSource = this.bllType.BllCheckTypeDT(1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 取得检验组
        /// </summary>
        private void GetCheckGroup()
        {
            try
            {
                this.fjygroup_id.ValueMember = "fdept_id";
                this.fjygroup_id.DisplayMember = "fname";
                this.bindingSourceJYGroup.DataSource = this.bllType.BllCheckGroupDT(1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllTypeDT(2);
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "ftype_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                currfp_id = e.Node.Name.ToString();
                GetCurrDT();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

     

      


        private void EventResultChangedString(string s)
        {
            this.textBoxReturn.Text = s;
        }

        private void textBoxReturn_TextChanged(object sender, EventArgs e)
        {
            try
            {
              
               
                    BllDrive(strfinstr_id);
               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        private void bindingSourceObject_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurr = (DataRowView)bindingSourceObject.Current;//行                
                if (rowCurr != null)
                {
                    strfinstr_id = rowCurr["finstr_id"].ToString();
                    BllDrive(strfinstr_id);
                }
                else
                {
                    this.dataGridView1.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        private void BllDrive(string finstr_id)
        {
            try
            {
                this.dataGridView1.DataSource = this.bll.BllDriveDT(finstr_id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {

            try
            {
                textBoxReturn.Text = "";
                if (rowCurr != null)
                {
                    WindowsResult r = new WindowsResult();
                    r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedString);

                    InstrDrvieForm fca = new InstrDrvieForm(r, strfinstr_id);
                    fca.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 是否为添加操作，是：添加，否：修改操作。
        /// </summary>
        bool isAdd = false;
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow dr = null;
                dr = this.dtCurr.NewRow();
                //string guid = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                dr["finstr_id"] = "";
                dr["ftype_id"] = this.currfp_id;
                dr["forder_by"] = "0";
                dr["fuse_if"] = "1";
                this.dtCurr.Rows.Add(dr);
                this.bindingSourceObject.Position = bindingSourceObject.Count;

                isAdd = true;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();

                if (isAdd)
                {
                    if (this.bll.BllInstrAdd(rowCurr) > 0)
                    {
                        GetCurrDT();
                    }
                    isAdd = false;
                }
                else
                {
                    this.bll.BllInstrUpdate(rowCurr);
                    GetCurrDT();
                }


            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError("\n请先新增记录后重试！");
            }
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.bindingSourceObject.EndEdit();
                this.bll.BllInstrUpdate(rowCurr);
                GetCurrDT();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetCheckType();
            GetCheckGroup();
            GetTree("-1");
        }

        private void toolStripButtonDrive_Click(object sender, EventArgs e)
        {

            try
            {
                textBoxReturn.Text = "";
                if (rowCurr != null)
                {
                    WindowsResult r = new WindowsResult();
                    r.TextChangedString += new TextChangedHandlerString(this.EventResultChangedString);

                    InstrDrvieForm fca = new InstrDrvieForm(r, strfinstr_id);
                    fca.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {

        }

     
    }
}

/*
 Name	Code	Data Type	Primary	Foreign Key	Mandatory
报表设置_id	fsetting_id	varchar(32)	TRUE	FALSE	TRUE
报表类型_id	ftype_id	varchar(32)	FALSE	TRUE	FALSE
仪器_id	finstr_id	varchar(32)	FALSE	FALSE	FALSE
报表代码	fcode	varchar(32)	FALSE	FALSE	FALSE
报表名称	fname	varchar(128)	FALSE	FALSE	FALSE
打印机名	fprinter_name	varchar(128)	FALSE	FALSE	FALSE
纸张名	fpaper_name	varchar(128)	FALSE	FALSE	FALSE
面宽	fpage_width	float	FALSE	FALSE	FALSE
面高	fpage_height	float	FALSE	FALSE	FALSE
边顶	fmargin_top	float	FALSE	FALSE	FALSE
边底	fmargin_bottom	float	FALSE	FALSE	FALSE
边左	fmargin_left	float	FALSE	FALSE	FALSE
边右	fmargin_right	float	FALSE	FALSE	FALSE
方向	forientation	varchar(32)	FALSE	FALSE	FALSE
启用否	fuse_if	int	FALSE	FALSE	FALSE
序号	forder_by	varchar(32)	FALSE	FALSE	FALSE
第页固定行数	frows	int	FALSE	FALSE	FALSE
项目数超过数打第二页	fone_count	int	FALSE	FALSE	FALSE
阳性结果提示	fy_mess	varchar(32)	FALSE	FALSE	FALSE
弱阳性结果提示	fry_mess	varchar(32)	FALSE	FALSE	FALSE
偏高结果提示	fhight_mess	varchar(32)	FALSE	FALSE	FALSE
偏低结果提示	flow_mess	varchar(32)	FALSE	FALSE	FALSE
错误结果提示	ferror_mess	varchar(32)	FALSE	FALSE	FALSE
自定义1	f1	varchar(32)	FALSE	FALSE	FALSE
自定义2	f2	varchar(32)	FALSE	FALSE	FALSE
自定义3	f3	varchar(32)	FALSE	FALSE	FALSE
自定义4	f4	varchar(32)	FALSE	FALSE	FALSE
自定义5	f5	varchar(32)	FALSE	FALSE	FALSE
自定义6	f6	varchar(32)	FALSE	FALSE	FALSE
自定义7	f7	varchar(32)	FALSE	FALSE	FALSE
自定义8	f8	varchar(32)	FALSE	FALSE	FALSE
自定义9	f9	varchar(32)	FALSE	FALSE	FALSE
自定义10	f10	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
 */