﻿using System;
using System.Data;
using System.Windows.Forms;
using WEISHENG.COMM.PluginsAttribute;
using yunLis.lisbll.sam;

namespace yunLis.lis.sam.dic
{
    [ClassInfoMark(GUID = "CF2F7928-CD15-4841-BAF0-B45C31FD2383", 键ID = "CF2F7928-CD15-4841-BAF0-B45C31FD2383", 父ID = "864BDFE3-63E6-408F-98F3-270953B15BCD",
    功能名称 = "检验类别样本类型", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.dic.CheckTypeAndSampleTypeForm", 显示顺序 = 5, 传递参数 = "",
    菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public partial class CheckTypeAndSampleTypeForm :yunLis.wwf.SysBaseForm
    {
        TypeBLL bllType = new TypeBLL();//类别
        SampleTypeBLL bllSample = new SampleTypeBLL();
        public CheckTypeAndSampleTypeForm()
        {
            InitializeComponent();
            DataGridViewObject.AutoGenerateColumns = false;
            this.dataGridViewSampletypeAll.AutoGenerateColumns = false;
            this.dataGridViewSampletypeOk.AutoGenerateColumns = false;
        }

        private void CheckTypeAndSampleTypeForm_Load(object sender, EventArgs e)
        {
            GetCheckType();
            GetSampleAll();
        }
        /// <summary>
        /// 取得检验类别
        /// </summary>
        private void GetCheckType()
        {
            try
            {
                bindingSourceCheckType.DataSource = this.bllType.BllCheckTypeDT(1);
                this.DataGridViewObject.AutoGenerateColumns = false;
                this.DataGridViewObject.DataSource = bindingSourceCheckType;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 
        /// </summary>
        private void GetSampleAll()
        {
            try
            {

                this.dataGridViewSampletypeAll.DataSource =
                    this.bllSample.BllDT(" WHERE (fuse_if = 1) AND (fhelp_code LIKE '%" + this.textBox1.Text + "%') ");
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void GetSampleOk(string fcheck_type_id)
        {          
            try
            {                
                this.dataGridViewSampletypeOk.DataSource = this.bllType.BllCheckAndSamTypeDT(fcheck_type_id);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        string strfcheck_type_id = "";
        private void bindingSourceCheckType_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                strfcheck_type_id = "";
                DataRowView rowCurrent = (DataRowView)bindingSourceCheckType.Current;
                if (rowCurrent != null)
                {
                    strfcheck_type_id = rowCurrent["fcheck_type_id"].ToString();
                    GetSampleOk(strfcheck_type_id);
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            GetSampleAll();
        }

       

        private void buttonNo_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.dataGridViewSampletypeOk.Rows.Count > 0)
                {
                    if (yunLis.wwfbll.WWMessage.MessageDialogResult("确定取消？"))
                    {
                        string fid = this.dataGridViewSampletypeOk.CurrentRow.Cells["fid_ok"].Value.ToString();
                        this.bllType.BllCheckAndSamTypeDel(fid);
                        GetSampleOk(strfcheck_type_id);
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dataGridViewSampletypeAll.Rows.Count > 0)
                {
                    string jytype = this.DataGridViewObject.CurrentRow.Cells["fcheck_type_id_01"].Value.ToString();
                    string ybtype = this.dataGridViewSampletypeAll.CurrentRow.Cells["fsample_type_id_all"].Value.ToString();
                    if (this.bllType.BllCheckAndSamTypeExists(jytype, ybtype))
                    {
                        yunLis.wwfbll.WWMessage.MessageShowWarning("此样本类别已经存！");
                    }
                    else
                    {
                        this.bllType.BllCheckAndSamTypeAdd(jytype, ybtype);
                        GetSampleOk(strfcheck_type_id);
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}