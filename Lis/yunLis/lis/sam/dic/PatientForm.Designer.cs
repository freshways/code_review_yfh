﻿namespace yunLis.lis.sam.dic
{
    partial class PatientForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fjy_dateLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatientForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingSourceObject = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.fhz_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ftime_registration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fbirthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fdept_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fward_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.froom_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_zyh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fbed_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fdiagnose = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fuse_if = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fhymen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvolk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjob_org = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fadd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ftel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhelp_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ftime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            fjy_dateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(7, 16);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 16;
            fjy_dateLabel.Text = "挂号日期:";
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.bindingSourceObject;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonS,
            this.toolStripButtonDel});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(874, 30);
            this.bN.TabIndex = 129;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(76, 27);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(75, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(77, 27);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 30);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(188, 385);
            this.wwTreeView1.TabIndex = 130;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fhz_id,
            this.ftime_registration,
            this.fname,
            this.fsex,
            this.fbirthday,
            this.fdept_id,
            this.fward_num,
            this.froom_num,
            this.fhz_zyh,
            this.fbed_num,
            this.fdiagnose,
            this.fuse_if,
            this.fhymen,
            this.fvolk,
            this.fjob_org,
            this.fadd,
            this.ftel,
            this.fhelp_code,
            this.fremark,
            this.ftime});
            this.dataGridView1.DataSource = this.bindingSourceObject;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.Location = new System.Drawing.Point(191, 66);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 30;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(683, 349);
            this.dataGridView1.TabIndex = 131;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(188, 30);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 385);
            this.splitter1.TabIndex = 132;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.fjy_dateDateTimePicker2);
            this.panel1.Controls.Add(this.fjy_dateDateTimePicker1);
            this.panel1.Controls.Add(fjy_dateLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(191, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(683, 36);
            this.panel1.TabIndex = 133;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(160, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(172, 12);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 17;
            this.fjy_dateDateTimePicker2.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker2_CloseUp);
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(72, 12);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(85, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 15;
            this.fjy_dateDateTimePicker1.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker1_CloseUp);
            // 
            // fhz_id
            // 
            this.fhz_id.DataPropertyName = "fhz_id";
            this.fhz_id.HeaderText = "患者_id";
            this.fhz_id.Name = "fhz_id";
            this.fhz_id.Width = 80;
            // 
            // ftime_registration
            // 
            this.ftime_registration.DataPropertyName = "ftime_registration";
            this.ftime_registration.HeaderText = "挂号时间";
            this.ftime_registration.Name = "ftime_registration";
            this.ftime_registration.Width = 130;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "患者姓名";
            this.fname.Name = "fname";
            this.fname.Width = 80;
            // 
            // fsex
            // 
            this.fsex.DataPropertyName = "fsex1";
            this.fsex.HeaderText = "性别";
            this.fsex.Name = "fsex";
            this.fsex.Width = 60;
            // 
            // fbirthday
            // 
            this.fbirthday.DataPropertyName = "fbirthday";
            dataGridViewCellStyle1.NullValue = null;
            this.fbirthday.DefaultCellStyle = dataGridViewCellStyle1;
            this.fbirthday.HeaderText = "生日";
            this.fbirthday.Name = "fbirthday";
            this.fbirthday.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fbirthday.Width = 80;
            // 
            // fdept_id
            // 
            this.fdept_id.DataPropertyName = "fdept_id";
            this.fdept_id.HeaderText = "所在科室";
            this.fdept_id.Name = "fdept_id";
            this.fdept_id.Width = 80;
            // 
            // fward_num
            // 
            this.fward_num.DataPropertyName = "fward_num";
            this.fward_num.HeaderText = "病区";
            this.fward_num.Name = "fward_num";
            this.fward_num.Width = 60;
            // 
            // froom_num
            // 
            this.froom_num.DataPropertyName = "froom_num";
            this.froom_num.HeaderText = "房间号";
            this.froom_num.Name = "froom_num";
            this.froom_num.Width = 70;
            // 
            // fhz_zyh
            // 
            this.fhz_zyh.DataPropertyName = "fhz_zyh";
            this.fhz_zyh.HeaderText = "住院号";
            this.fhz_zyh.Name = "fhz_zyh";
            this.fhz_zyh.Width = 70;
            // 
            // fbed_num
            // 
            this.fbed_num.DataPropertyName = "fbed_num";
            this.fbed_num.HeaderText = "床号";
            this.fbed_num.Name = "fbed_num";
            this.fbed_num.Width = 60;
            // 
            // fdiagnose
            // 
            this.fdiagnose.DataPropertyName = "fdiagnose";
            this.fdiagnose.HeaderText = "临床诊断";
            this.fdiagnose.Name = "fdiagnose";
            // 
            // fuse_if
            // 
            this.fuse_if.DataPropertyName = "fuse_if";
            this.fuse_if.FalseValue = "0";
            this.fuse_if.HeaderText = "启";
            this.fuse_if.IndeterminateValue = "0";
            this.fuse_if.Name = "fuse_if";
            this.fuse_if.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_if.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_if.TrueValue = "1";
            this.fuse_if.Width = 25;
            // 
            // fhymen
            // 
            this.fhymen.DataPropertyName = "fhymen";
            this.fhymen.HeaderText = "婚姻";
            this.fhymen.Name = "fhymen";
            this.fhymen.Visible = false;
            // 
            // fvolk
            // 
            this.fvolk.DataPropertyName = "fvolk";
            this.fvolk.HeaderText = "民族";
            this.fvolk.Name = "fvolk";
            this.fvolk.Visible = false;
            // 
            // fjob_org
            // 
            this.fjob_org.DataPropertyName = "fjob_org";
            this.fjob_org.HeaderText = "工作单位";
            this.fjob_org.Name = "fjob_org";
            this.fjob_org.Visible = false;
            // 
            // fadd
            // 
            this.fadd.DataPropertyName = "fadd";
            this.fadd.HeaderText = "住址";
            this.fadd.Name = "fadd";
            this.fadd.Visible = false;
            // 
            // ftel
            // 
            this.ftel.DataPropertyName = "ftel";
            this.ftel.HeaderText = "电话";
            this.ftel.Name = "ftel";
            this.ftel.Visible = false;
            // 
            // fhelp_code
            // 
            this.fhelp_code.DataPropertyName = "fhelp_code";
            this.fhelp_code.HeaderText = "助记符";
            this.fhelp_code.Name = "fhelp_code";
            this.fhelp_code.Visible = false;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            this.fremark.Visible = false;
            // 
            // ftime
            // 
            this.ftime.DataPropertyName = "ftime";
            this.ftime.HeaderText = "系统时间";
            this.ftime.Name = "ftime";
            this.ftime.ReadOnly = true;
            this.ftime.Width = 120;
            // 
            // PatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 415);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.wwTreeView1);
            this.Controls.Add(this.bN);
            this.Name = "PatientForm";
            this.Text = "患者信息维护";
            this.Load += new System.EventHandler(this.PatientForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        protected System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.BindingSource bindingSourceObject;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftime_registration;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsex;
        private System.Windows.Forms.DataGridViewTextBoxColumn fbirthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn fdept_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fward_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn froom_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_zyh;
        private System.Windows.Forms.DataGridViewTextBoxColumn fbed_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn fdiagnose;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_if;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhymen;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvolk;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjob_org;
        private System.Windows.Forms.DataGridViewTextBoxColumn fadd;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftel;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhelp_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftime;
    }
}