﻿using DevExpress.Data.TreeList;
using HIS.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WEISHENG.COMM;
using WEISHENG.COMM.PluginsAttribute;
using yunLis.lisbll.sam;
using yunLis.wwfbll;
namespace yunLis.lis.sam.dic
{
    [ClassInfoMark(GUID = "30F923E3-C01C-407F-8C91-F2640F0F38F6", 键ID = "30F923E3-C01C-407F-8C91-F2640F0F38F6", 父ID = "864BDFE3-63E6-408F-98F3-270953B15BCD",
     功能名称 = "项目组合", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.dic.ItemGroupForm", 显示顺序 = 3, 传递参数 = "",
     菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public partial class ItemGroupForm : DevExpress.XtraEditors.XtraForm
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString));
        public List<HIS.Model.SAM_ITEM_GROUP> listSelectedGroup = new List<SAM_ITEM_GROUP>();
        List<HIS.Model.Pojo.LIS.PojoItem_GroupRel_SF> list_Item_SF = new List<HIS.Model.Pojo.LIS.PojoItem_GroupRel_SF>();
        string openType = "";

        public ItemGroupForm()
        {
            InitializeComponent();
            layoutControlGroupBottom.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }
        public ItemGroupForm(string openType)
        {
            InitializeComponent();
            if (openType == "弹窗引用")
            {
                layoutControlItemTopLeft.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlGroupLeft.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                bindingNavigator1.Visible = false;
                splitContainerControl1.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel2;
                gridView全部组合.OptionsBehavior.Editable = false;
                richTextBox1.ReadOnly = true;
                splitContainerControl2.Horizontal = true;
                splitContainerControl2.SplitterPosition = 400;
                col启用.Visible = false;
                col急诊.Visible = false;
                col非急诊.Visible = false;
            }
        }
        private void ItemGroupForm_Load(object sender, EventArgs e)
        {
            GetTree("-1");
            gridControl全部组合.DataSource = lisEntities.SAM_ITEM_GROUP.OrderBy(c => c.forder_by).ToList();
            gridControl全部项目.DataSource = lisEntities.SAM_ITEM.OrderBy(c => c.fprint_num).ToList();
            gridView全部组合.BestFitMaxRowCount = 30;
            gridView全部组合.BestFitColumns();


            this.gridControl组合内项目.DataSource = list_Item_SF;
            this.searchControl筛选项目组合.Client = this.gridControl全部组合;
            this.searchControl筛选全部项目.Client = this.gridControl全部项目;
            this.searchControl筛选组合内项目.Client = this.gridControl组合内项目;
        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bllItem.BllCheckAndSampleTypeDT();
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fpid";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fid";//主键字段名称
                this.wwTreeView1.ZAToolTipTextName = "tag";
                this.wwTreeView1.ZATreeViewShow();//显示树      
                this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0];
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        string strfcheck_type_id = "";
        string strfsample_type_id = "";
        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            strfcheck_type_id = "";
            strfsample_type_id = "";
            string strEdit = e.Node.ToolTipText;
            if (strEdit == "CheckType")
            {
                bindingNavigator1.Enabled = false;
            }
            else
            {
                bindingNavigator1.Enabled = true;
                strfcheck_type_id = e.Node.Parent.Name.ToString();
                strfsample_type_id = e.Node.Name.ToString();
                vFilter_Group();
                vFilter_AllItem();
                HIS.Model.SAM_ITEM_GROUP _ITEM_GROUP = gridView全部组合.GetFocusedRow() as SAM_ITEM_GROUP;
                if (_ITEM_GROUP == null)
                {
                    vFilter_Item组合内("null");
                }
                else
                {
                    vFilter_Item组合内(_ITEM_GROUP.fgroup_id);
                }
            }
        }
        /// <summary>
        /// 取得 项目组
        /// </summary>
        private void vFilter_AllItem()
        {
            try
            {
                gridView全部项目.ActiveFilterString = $"[fcheck_type_id] ='{strfcheck_type_id}' And [fsam_type_id]='{strfsample_type_id}'";
                gridView全部项目.RefreshData();
                gridView全部项目.BestFitColumns();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        /// <summary>
        /// 筛选组合
        /// </summary>
        private void vFilter_Group()
        {
            try
            {
                gridView全部组合.ActiveFilterString = $"[fcheck_type_id] ='{strfcheck_type_id}' And [fsam_type_id]='{strfsample_type_id}'";
                gridView全部组合.RefreshData();
                gridView全部组合.BestFitColumns();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void vFilter_Item组合内(string fgroup_id)
        {
            try
            {
                list_Item_SF.Clear();
                list_Item_SF.AddRange(this.bllItem.getList_Item_SF(fgroup_id));
                gridView组合内项目.RefreshData();
                gridView组合内项目.BestFitColumns();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        private void toolStripButtonHelpCode_Click(object sender, EventArgs e)
        {
            try
            {
                HIS.Model.SAM_ITEM_GROUP _ITEM_GROUP = gridView全部组合.GetFocusedRow() as SAM_ITEM_GROUP;
                if (_ITEM_GROUP == null)
                {
                    return;
                }
                _ITEM_GROUP.fhelp_code = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(_ITEM_GROUP.fname.ToString().Trim());
                gridView全部组合.RefreshData();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (strfsample_type_id == "" || strfsample_type_id == null)
                {
                    WWMessage.MessageShowWarning("样本类别不能为空，请选择后重试！");
                    return;
                }

                HIS.Model.SAM_ITEM_GROUP dr = new SAM_ITEM_GROUP();
                dr.fgroup_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                dr.fjz_if = 0;
                dr.fjz_no_if = 1;
                dr.fuse_if = 1;
                dr.fcheck_type_id = strfcheck_type_id;
                dr.fsam_type_id = strfsample_type_id;
                lisEntities.SAM_ITEM_GROUP.Add(dr);
                lisEntities.SaveChanges();
                gridControl全部组合.DataSource = lisEntities.SAM_ITEM_GROUP.ToList();
                gridView全部组合.RefreshData();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                gridView全部组合.CloseEditor();
                if (!lisEntities.ChangeTracker.HasChanges())
                {
                    msgHelper.ShowInformation("当前数据未修改");
                    return;
                }
                var modified = lisEntities.ChangeTracker.Entries().Where(c => c.State == System.Data.Entity.EntityState.Modified);
                var test = modified.ToList();
                foreach (var item in modified)
                {
                    var item2 = (HIS.Model.SAM_ITEM_GROUP)item.Entity;
                }
                lisEntities.SaveChanges();
                msgHelper.ShowInformation("保存成功");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                HIS.Model.SAM_ITEM_GROUP item = gridView全部组合.GetFocusedRow() as HIS.Model.SAM_ITEM_GROUP;
                if (item != null)
                {
                    if (WWMessage.MessageDialogResult($"确定删除【{item.fname}】？记录删除后将不可恢复！"))
                    {
                        using (LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
                        {
                            lisEntities.Database.ExecuteSqlCommand($"delete SAM_ITEM_GROUP_REL where fgroup_id='{item.fgroup_id}'");
                            lisEntities.SAM_ITEM_GROUP.Attach(item);
                            lisEntities.SAM_ITEM_GROUP.Remove(item);
                            lisEntities.SaveChanges();
                            gridView全部组合.DeleteRow(gridView全部组合.FocusedRowHandle);
                        }
                        vFilter_Group();
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void button分配_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                HIS.Model.SAM_ITEM_GROUP _ITEM_GROUP = gridView全部组合.GetFocusedRow() as SAM_ITEM_GROUP;
                HIS.Model.SAM_ITEM _ITEM = gridView全部项目.GetFocusedRow() as HIS.Model.SAM_ITEM;

                if (_ITEM == null || _ITEM_GROUP == null)
                {
                    return;
                }
                //弹窗引用只操作界面，不操作数据库
                if (openType == "弹窗引用")
                {
                    HIS.Model.Pojo.LIS.PojoItem_GroupRel_SF newItem = new HIS.Model.Pojo.LIS.PojoItem_GroupRel_SF();
                    HIS.Model.Helper.modelHelper.copyInstanceValue(_ITEM, newItem);
                    string hiscode = _ITEM.fhis_item_code;
                    if (!string.IsNullOrWhiteSpace(hiscode))
                    {
                        var int_hiscode = Convert.ToInt32(hiscode);
                        var item收费小项 = HIS.COMM.BLL.CacheData.Gy收费小项.Where(c => c.收费编码 == int_hiscode).FirstOrDefault();
                        newItem.收费名称 = item收费小项.收费名称;
                        newItem.收费单价 = item收费小项.单价;
                    }

                }
                else
                {
                    if (this.bllItem.BllItemGroupRefExists(_ITEM_GROUP.fgroup_id, _ITEM.fitem_id))
                    {
                        yunLis.wwfbll.WWMessage.MessageShowWarning("此项目已经存！");
                    }
                    else
                    {
                        this.bllItem.BllItemGroupRefAdd(_ITEM_GROUP.fgroup_id, _ITEM.fitem_id);
                        vFilter_Item组合内(_ITEM_GROUP.fgroup_id);
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buttonNo_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                HIS.Model.Pojo.LIS.PojoItem_GroupRel_SF item = gridView组合内项目.GetFocusedRow() as HIS.Model.Pojo.LIS.PojoItem_GroupRel_SF;
                if (item == null)
                {
                    return;
                }

                if (yunLis.wwfbll.WWMessage.MessageDialogResult($"确定取消检查项目【{item.fname}】？"))
                {
                    this.bllItem.BllItemGroupRefDel(item.fid);
                    vFilter_Item组合内(item.fgroup_id);
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void gridView组合列表_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            vRefresh组合内();
        }

        void vRefresh组合内()
        {
            HIS.Model.SAM_ITEM_GROUP item = gridView全部组合.GetFocusedRow() as SAM_ITEM_GROUP;
            if (item != null)
            {
                vFilter_Item组合内(item.fgroup_id);
            }
        }
        private void repositoryItemCheckEdit3_QueryCheckStateByValue(object sender, DevExpress.XtraEditors.Controls.QueryCheckStateByValueEventArgs e)
        {
            string val = "";
            if (e.Value != null)
            {
                val = e.Value.ToString();
            }
            else
            {
                val = "False";//默认为不选   
            }
            switch (val)
            {
                case "True":
                case "Yes":
                case "1":
                    e.CheckState = CheckState.Checked;
                    break;
                case "False":
                case "No":
                case "0":
                    e.CheckState = CheckState.Unchecked;
                    break;
                default:
                    e.CheckState = CheckState.Checked;
                    break;
            }
            e.Handled = true;
        }

        private void simpleButton取消_Click_1(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }


        void vGetSelectGroup()
        {
            listSelectedGroup.Clear();
            int[] rownumber = this.gridView全部组合.GetSelectedRows();
            foreach (var item in rownumber)
            {
                listSelectedGroup.Add(gridView全部组合.GetRow(item) as HIS.Model.SAM_ITEM_GROUP);
            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            vGetSelectGroup();
            this.DialogResult = DialogResult.OK;
        }

        private void toolStripButton收费项目_Click(object sender, EventArgs e)
        {
            HIS.Model.SAM_ITEM_GROUP item = gridView全部组合.GetFocusedRow() as SAM_ITEM_GROUP;
            if (item == null)
            {
                return;
            }

            Form_项目选择 frm = new Form_项目选择();
            if (frm.ShowDialog() == DialogResult.Yes)
            {
                HIS.Model.GY收费小项 s = frm.gy收费小项;
                item.收费编码 = s.收费编码.ToString();
            }
            gridView全部组合.CloseEditor();
            gridView全部组合.RefreshData();
        }

        private void gridView全部组合_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            vRefresh组合内();
        }

        private void gridView全部组合_GotFocus(object sender, EventArgs e)
        {
            vRefresh组合内();
        }
    }
}