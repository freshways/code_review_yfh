﻿namespace yunLis.lis.sam.Report
{
    partial class XtraForm化验结果
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm化验结果));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.col打印状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col申请医生 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.cmb病人来源 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit门诊或住院号 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton直接打印 = new DevExpress.XtraEditors.SimpleButton();
            this.gc化验明细 = new DevExpress.XtraGrid.GridControl();
            this.gv化验明细 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton预览打印 = new DevExpress.XtraEditors.SimpleButton();
            this.gc化验列表 = new DevExpress.XtraGrid.GridControl();
            this.gv化验列表 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col申请单号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col检验日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col样本号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病人来源 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col住院号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col床号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病人姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col年龄 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col化验项目 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col化验设备 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col化验设备ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colfjy_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit截止时间 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit开始时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt申请科室 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem门诊或住院号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.col检查费 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb病人来源.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit门诊或住院号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc化验明细)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv化验明细)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc化验列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv化验列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit截止时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit开始时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt申请科室.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem门诊或住院号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            this.SuspendLayout();
            // 
            // col打印状态
            // 
            this.col打印状态.Caption = "打印状态";
            this.col打印状态.FieldName = "打印状态";
            this.col打印状态.Name = "col打印状态";
            this.col打印状态.OptionsColumn.AllowEdit = false;
            this.col打印状态.Visible = true;
            this.col打印状态.VisibleIndex = 9;
            // 
            // col申请医生
            // 
            this.col申请医生.Caption = "申请医生";
            this.col申请医生.FieldName = "申请医生";
            this.col申请医生.Name = "col申请医生";
            this.col申请医生.OptionsColumn.AllowEdit = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.cmb病人来源;
            this.layoutControlItem8.CustomizationFormText = "病人来源";
            this.layoutControlItem8.Location = new System.Drawing.Point(596, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(162, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(162, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "病人来源";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // cmb病人来源
            // 
            this.cmb病人来源.EditValue = "住院";
            this.cmb病人来源.Location = new System.Drawing.Point(661, 12);
            this.cmb病人来源.Name = "cmb病人来源";
            this.cmb病人来源.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb病人来源.Properties.Items.AddRange(new object[] {
            "住院",
            "门诊"});
            this.cmb病人来源.Size = new System.Drawing.Size(105, 20);
            this.cmb病人来源.StyleController = this.layoutControl1;
            this.cmb病人来源.TabIndex = 29;
            this.cmb病人来源.SelectedValueChanged += new System.EventHandler(this.cmb病人来源_SelectedValueChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit门诊或住院号);
            this.layoutControl1.Controls.Add(this.simpleButton直接打印);
            this.layoutControl1.Controls.Add(this.gc化验明细);
            this.layoutControl1.Controls.Add(this.cmb病人来源);
            this.layoutControl1.Controls.Add(this.simpleButton预览打印);
            this.layoutControl1.Controls.Add(this.gc化验列表);
            this.layoutControl1.Controls.Add(this.textEdit截止时间);
            this.layoutControl1.Controls.Add(this.simpleButton刷新);
            this.layoutControl1.Controls.Add(this.textEdit开始时间);
            this.layoutControl1.Controls.Add(this.txt申请科室);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(241, 249, 922, 618);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(1285, 572);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit门诊或住院号
            // 
            this.textEdit门诊或住院号.Location = new System.Drawing.Point(815, 12);
            this.textEdit门诊或住院号.Name = "textEdit门诊或住院号";
            this.textEdit门诊或住院号.Size = new System.Drawing.Size(170, 20);
            this.textEdit门诊或住院号.StyleController = this.layoutControl1;
            this.textEdit门诊或住院号.TabIndex = 33;
            // 
            // simpleButton直接打印
            // 
            this.simpleButton直接打印.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton直接打印.ImageOptions.Image")));
            this.simpleButton直接打印.Location = new System.Drawing.Point(1189, 12);
            this.simpleButton直接打印.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.simpleButton直接打印.Name = "simpleButton直接打印";
            this.simpleButton直接打印.Size = new System.Drawing.Size(99, 22);
            this.simpleButton直接打印.StyleController = this.layoutControl1;
            this.simpleButton直接打印.TabIndex = 32;
            this.simpleButton直接打印.Text = "直接打印";
            this.simpleButton直接打印.ToolTip = "只打未打印的单据，已打印的不再打印！";
            this.simpleButton直接打印.Click += new System.EventHandler(this.simpleButton打印全部_Click);
            // 
            // gc化验明细
            // 
            this.gc化验明细.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gc化验明细.Location = new System.Drawing.Point(821, 38);
            this.gc化验明细.MainView = this.gv化验明细;
            this.gc化验明细.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gc化验明细.Name = "gc化验明细";
            this.gc化验明细.Size = new System.Drawing.Size(467, 505);
            this.gc化验明细.TabIndex = 30;
            this.gc化验明细.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv化验明细});
            // 
            // gv化验明细
            // 
            this.gv化验明细.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gv化验明细.GridControl = this.gc化验明细;
            this.gv化验明细.Name = "gv化验明细";
            this.gv化验明细.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "项目名称";
            this.gridColumn1.FieldName = "项目名称";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 173;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "结果";
            this.gridColumn2.FieldName = "结果";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            this.gridColumn2.Width = 133;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "标记";
            this.gridColumn3.FieldName = "标记";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            this.gridColumn3.Width = 76;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "参考值";
            this.gridColumn4.FieldName = "参考值";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 6;
            this.gridColumn4.Width = 275;
            // 
            // simpleButton预览打印
            // 
            this.simpleButton预览打印.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton预览打印.ImageOptions.Image")));
            this.simpleButton预览打印.Location = new System.Drawing.Point(1093, 12);
            this.simpleButton预览打印.Name = "simpleButton预览打印";
            this.simpleButton预览打印.Size = new System.Drawing.Size(92, 22);
            this.simpleButton预览打印.StyleController = this.layoutControl1;
            this.simpleButton预览打印.TabIndex = 28;
            this.simpleButton预览打印.Text = "预览打印";
            this.simpleButton预览打印.Click += new System.EventHandler(this.simpleButton查看_Click);
            // 
            // gc化验列表
            // 
            this.gc化验列表.Location = new System.Drawing.Point(12, 38);
            this.gc化验列表.MainView = this.gv化验列表;
            this.gc化验列表.Name = "gc化验列表";
            this.gc化验列表.Size = new System.Drawing.Size(805, 505);
            this.gc化验列表.TabIndex = 7;
            this.gc化验列表.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv化验列表});
            this.gc化验列表.Click += new System.EventHandler(this.gc化验列表_Click);
            // 
            // gv化验列表
            // 
            this.gv化验列表.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col申请单号,
            this.col检验日期,
            this.col样本号,
            this.col病人来源,
            this.col住院号,
            this.col床号,
            this.col病人姓名,
            this.col性别,
            this.col年龄,
            this.col身份证,
            this.col化验项目,
            this.col化验设备,
            this.col化验设备ID,
            this.col打印状态,
            this.colfjy_id});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.LightSkyBlue;
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.col打印状态;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "已打印";
            styleFormatCondition1.Value2 = "未打印";
            this.gv化验列表.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gv化验列表.GridControl = this.gc化验列表;
            this.gv化验列表.Name = "gv化验列表";
            this.gv化验列表.OptionsSelection.MultiSelect = true;
            this.gv化验列表.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gv化验列表.OptionsView.ShowGroupPanel = false;
            this.gv化验列表.DoubleClick += new System.EventHandler(this.gv化验列表_DoubleClick);
            // 
            // col申请单号
            // 
            this.col申请单号.Caption = "申请单号";
            this.col申请单号.FieldName = "条码";
            this.col申请单号.Name = "col申请单号";
            this.col申请单号.OptionsColumn.AllowEdit = false;
            this.col申请单号.Visible = true;
            this.col申请单号.VisibleIndex = 1;
            // 
            // col检验日期
            // 
            this.col检验日期.Caption = "检验日期";
            this.col检验日期.FieldName = "检验日期";
            this.col检验日期.Name = "col检验日期";
            this.col检验日期.OptionsColumn.AllowEdit = false;
            this.col检验日期.Visible = true;
            this.col检验日期.VisibleIndex = 2;
            // 
            // col样本号
            // 
            this.col样本号.Caption = "样本号";
            this.col样本号.FieldName = "样本号";
            this.col样本号.Name = "col样本号";
            this.col样本号.OptionsColumn.AllowEdit = false;
            this.col样本号.Visible = true;
            this.col样本号.VisibleIndex = 3;
            // 
            // col病人来源
            // 
            this.col病人来源.Caption = "病人来源";
            this.col病人来源.FieldName = "病人类别";
            this.col病人来源.Name = "col病人来源";
            this.col病人来源.OptionsColumn.AllowEdit = false;
            // 
            // col住院号
            // 
            this.col住院号.Caption = "住院号/门诊号";
            this.col住院号.FieldName = "住院号";
            this.col住院号.Name = "col住院号";
            this.col住院号.OptionsColumn.AllowEdit = false;
            this.col住院号.Visible = true;
            this.col住院号.VisibleIndex = 4;
            // 
            // col床号
            // 
            this.col床号.Caption = "床号";
            this.col床号.FieldName = "床号";
            this.col床号.Name = "col床号";
            this.col床号.OptionsColumn.AllowEdit = false;
            this.col床号.Visible = true;
            this.col床号.VisibleIndex = 5;
            // 
            // col病人姓名
            // 
            this.col病人姓名.Caption = "病人姓名";
            this.col病人姓名.FieldName = "姓名";
            this.col病人姓名.Name = "col病人姓名";
            this.col病人姓名.OptionsColumn.AllowEdit = false;
            this.col病人姓名.Visible = true;
            this.col病人姓名.VisibleIndex = 6;
            // 
            // col性别
            // 
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            // 
            // col年龄
            // 
            this.col年龄.Caption = "年龄";
            this.col年龄.FieldName = "年龄";
            this.col年龄.Name = "col年龄";
            this.col年龄.OptionsColumn.AllowEdit = false;
            // 
            // col身份证
            // 
            this.col身份证.Caption = "身份证";
            this.col身份证.FieldName = "身份证";
            this.col身份证.Name = "col身份证";
            this.col身份证.OptionsColumn.AllowEdit = false;
            // 
            // col化验项目
            // 
            this.col化验项目.Caption = "化验项目";
            this.col化验项目.FieldName = "化验项目";
            this.col化验项目.Name = "col化验项目";
            this.col化验项目.OptionsColumn.AllowEdit = false;
            this.col化验项目.Visible = true;
            this.col化验项目.VisibleIndex = 7;
            // 
            // col化验设备
            // 
            this.col化验设备.Caption = "化验设备";
            this.col化验设备.FieldName = "检验设备";
            this.col化验设备.Name = "col化验设备";
            this.col化验设备.OptionsColumn.AllowEdit = false;
            this.col化验设备.Visible = true;
            this.col化验设备.VisibleIndex = 8;
            // 
            // col化验设备ID
            // 
            this.col化验设备ID.Caption = "化验设备ID";
            this.col化验设备ID.FieldName = "化验设备ID";
            this.col化验设备ID.Name = "col化验设备ID";
            this.col化验设备ID.OptionsColumn.AllowEdit = false;
            this.col化验设备ID.Visible = true;
            this.col化验设备ID.VisibleIndex = 10;
            // 
            // colfjy_id
            // 
            this.colfjy_id.Caption = "检验ID";
            this.colfjy_id.FieldName = "fjy_id";
            this.colfjy_id.Name = "colfjy_id";
            this.colfjy_id.OptionsColumn.AllowEdit = false;
            // 
            // textEdit截止时间
            // 
            this.textEdit截止时间.Location = new System.Drawing.Point(269, 12);
            this.textEdit截止时间.Name = "textEdit截止时间";
            this.textEdit截止时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.textEdit截止时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit截止时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit截止时间.Size = new System.Drawing.Size(139, 20);
            this.textEdit截止时间.StyleController = this.layoutControl1;
            this.textEdit截止时间.TabIndex = 20;
            // 
            // simpleButton刷新
            // 
            this.simpleButton刷新.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton刷新.ImageOptions.Image")));
            this.simpleButton刷新.Location = new System.Drawing.Point(999, 12);
            this.simpleButton刷新.Name = "simpleButton刷新";
            this.simpleButton刷新.Size = new System.Drawing.Size(90, 22);
            this.simpleButton刷新.StyleController = this.layoutControl1;
            this.simpleButton刷新.TabIndex = 6;
            this.simpleButton刷新.Text = "刷新";
            this.simpleButton刷新.Click += new System.EventHandler(this.simpleButton刷新_Click);
            // 
            // textEdit开始时间
            // 
            this.textEdit开始时间.Location = new System.Drawing.Point(69, 12);
            this.textEdit开始时间.Name = "textEdit开始时间";
            this.textEdit开始时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.textEdit开始时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit开始时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit开始时间.Size = new System.Drawing.Size(139, 20);
            this.textEdit开始时间.StyleController = this.layoutControl1;
            this.textEdit开始时间.TabIndex = 19;
            // 
            // txt申请科室
            // 
            this.txt申请科室.Location = new System.Drawing.Point(465, 12);
            this.txt申请科室.Name = "txt申请科室";
            this.txt申请科室.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt申请科室.Properties.NullText = "请选择...";
            this.txt申请科室.Properties.PopupSizeable = true;
            this.txt申请科室.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.txt申请科室.Size = new System.Drawing.Size(139, 20);
            this.txt申请科室.StyleController = this.layoutControl1;
            this.txt申请科室.TabIndex = 21;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem8,
            this.layoutControlItem门诊或住院号});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1300, 555);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit开始时间;
            this.layoutControlItem2.CustomizationFormText = "开始时间:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.Name = "layoutControlItem3";
            this.layoutControlItem2.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "开始时间:";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(52, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton刷新;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem5.Location = new System.Drawing.Point(987, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(94, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.Name = "layoutControlItem2";
            this.layoutControlItem5.Size = new System.Drawing.Size(94, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit截止时间;
            this.layoutControlItem6.CustomizationFormText = "截止时间:";
            this.layoutControlItem6.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.Name = "layoutControlItem4";
            this.layoutControlItem6.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "截止时间:";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(52, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt申请科室;
            this.layoutControlItem7.CustomizationFormText = "病区选择:";
            this.layoutControlItem7.Location = new System.Drawing.Point(400, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem7.Name = "layoutControlItem5";
            this.layoutControlItem7.Size = new System.Drawing.Size(196, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "申请科室";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(48, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gc化验列表;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(809, 509);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(977, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton预览打印;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(1081, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(96, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.gc化验明细;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(809, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(471, 509);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton直接打印;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(1177, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(103, 0);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(103, 26);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem门诊或住院号
            // 
            this.layoutControlItem门诊或住院号.Control = this.textEdit门诊或住院号;
            this.layoutControlItem门诊或住院号.Location = new System.Drawing.Point(758, 0);
            this.layoutControlItem门诊或住院号.MaxSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem门诊或住院号.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem门诊或住院号.Name = "layoutControlItem门诊或住院号";
            this.layoutControlItem门诊或住院号.Size = new System.Drawing.Size(219, 26);
            this.layoutControlItem门诊或住院号.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem门诊或住院号.Text = "住院号:";
            this.layoutControlItem门诊或住院号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem门诊或住院号.TextSize = new System.Drawing.Size(40, 14);
            this.layoutControlItem门诊或住院号.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.CustomizationFormText = "开始时间:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(233, 26);
            this.layoutControlItem3.Text = "开始时间:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.CustomizationFormText = "截止时间:";
            this.layoutControlItem4.Location = new System.Drawing.Point(233, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(229, 26);
            this.layoutControlItem4.Text = "截止时间:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(895, 497);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // col检查费
            // 
            this.col检查费.Caption = "检查费";
            this.col检查费.FieldName = "检查费";
            this.col检查费.Name = "col检查费";
            this.col检查费.OptionsColumn.AllowEdit = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "序号";
            this.gridColumn5.FieldName = "序号";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 36;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "项目代码";
            this.gridColumn6.FieldName = "项目代码";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 132;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "单位";
            this.gridColumn7.FieldName = "单位";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            this.gridColumn7.Width = 122;
            // 
            // XtraForm化验结果
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1285, 572);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "XtraForm化验结果";
            this.Text = "XtraForm化验结果";
            this.Load += new System.EventHandler(this.XtraForm化验结果_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb病人来源.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit门诊或住院号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc化验明细)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv化验明细)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc化验列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv化验列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit截止时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit开始时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt申请科室.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem门诊或住院号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn col申请医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.ComboBoxEdit cmb病人来源;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton预览打印;
        private DevExpress.XtraGrid.GridControl gc化验列表;
        private DevExpress.XtraGrid.Views.Grid.GridView gv化验列表;
        private DevExpress.XtraGrid.Columns.GridColumn col申请单号;
        private DevExpress.XtraGrid.Columns.GridColumn col检验日期;
        private DevExpress.XtraGrid.Columns.GridColumn col样本号;
        private DevExpress.XtraGrid.Columns.GridColumn col病人来源;
        private DevExpress.XtraGrid.Columns.GridColumn col住院号;
        private DevExpress.XtraGrid.Columns.GridColumn col病人姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col年龄;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证;
        private DevExpress.XtraGrid.Columns.GridColumn col化验项目;
        private DevExpress.XtraEditors.TextEdit textEdit截止时间;
        private DevExpress.XtraEditors.SimpleButton simpleButton刷新;
        private DevExpress.XtraEditors.TextEdit textEdit开始时间;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.Columns.GridColumn col检查费;
        private DevExpress.XtraEditors.ComboBoxEdit txt申请科室;
        private DevExpress.XtraGrid.Columns.GridColumn col化验设备;
        private DevExpress.XtraGrid.GridControl gc化验明细;
        private DevExpress.XtraGrid.Views.Grid.GridView gv化验明细;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraGrid.Columns.GridColumn colfjy_id;
        private DevExpress.XtraGrid.Columns.GridColumn col化验设备ID;
        private DevExpress.XtraGrid.Columns.GridColumn col床号;
        private DevExpress.XtraGrid.Columns.GridColumn col打印状态;
        private DevExpress.XtraEditors.SimpleButton simpleButton直接打印;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit textEdit门诊或住院号;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem门诊或住院号;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
    }
}