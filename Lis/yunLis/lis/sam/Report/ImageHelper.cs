﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace yunLis.lis.sam.Report
{
    class ImageHelper
    {
        public static Image GetImageBytes(byte[] bytes)
        {
            Image photo = null;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                ms.Write(bytes, 0, bytes.Length);
                photo = Image.FromStream(ms, true);
            }
            return photo;
        }

        //public static Bitmap ToGray(Bitmap bmp)
        //{
        //    for (int i = 0; i < bmp.Width; i++)
        //    {
        //        for (int j = 0; j < bmp.Height; j++)
        //        {
        //            //获取该点的像素的RGB的颜色
        //            Color color = bmp.GetPixel(i, j);
        //            //利用公式计算灰度值
        //            int gray = (int)(color.R * 0.3 + color.G * 0.59 + color.B * 0.11);
        //            Color newColor = Color.FromArgb(gray, gray, gray);
        //            bmp.SetPixel(i, j, newColor);
        //        }
        //    }
        //    return bmp;
        //}

        public static Bitmap ToGray(Bitmap bmp)
        {
            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    //获取该点的像素的RGB的颜色
                    Color color = bmp.GetPixel(i, j);
                    //利用公式计算灰度值
                    //int gray = (int)(color.R * 0.3 + color.G * 0.59 + color.B * 0.11);
                    //Color newColor = Color.FromArgb(gray, gray, gray);

                    int colorR = 255 - color.R;
                    int colorG = 255 - color.G;
                    int colorB = 255 - color.B;
                    Color newColor = Color.FromArgb(colorR, colorG, colorB);
                    bmp.SetPixel(i, j, newColor);
                }
            }
            return bmp;
        }

        public static Image ImageToGray(byte[] bytes)
        {
            Image photo = null;
            Bitmap bmp = null;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                ms.Write(bytes, 0, bytes.Length);
                try
                {
                    photo = Image.FromStream(ms);
                    bmp = new Bitmap(photo);
                    ToGray(bmp);
                }
                catch(Exception ex)
                {
                    bmp = null;
                }
            }
            return bmp; 
        }
    }
}
