﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace yunLis.lis.sam.Report
{
    public partial class Report20A4C1_Little : DevExpress.XtraReports.UI.XtraReport
    {
        //public Report20A5C1L_Little()
        //{
        //    InitializeComponent();
        //}

        private string strJYid = null;

        private bool printSuccess = false;

        public bool GetPrintResult()
        {
            return printSuccess;
        }
        public Report20A4C1_Little(string strid, DataTable dtDataSource)
        {
            InitializeComponent();

            strJYid = strid;

            //设置报告头信息
            xrLabelTitle.Text = dtDataSource.Rows[0]["医院名称"].ToString() + dtDataSource.Rows[0]["报表名称"].ToString();//+ "检验报告单";//
            xrLabel姓名.Text = dtDataSource.Rows[0]["姓名"].ToString();
            xrLabel性别.Text=dtDataSource.Rows[0]["性别"].ToString();
            xrLabel年龄.Text=dtDataSource.Rows[0]["年龄"].ToString()+dtDataSource.Rows[0]["年龄单位"].ToString();
            xrLabel科室.Text=dtDataSource.Rows[0]["送检科室"].ToString();
            xrLabel床号.Text =dtDataSource.Rows[0]["床号"].ToString();
            xrLabel样本类型.Text = dtDataSource.Rows[0]["样本类别"].ToString();
            xrLabel患者类别.Text = dtDataSource.Rows[0]["检验类别"].ToString();
            xrLabel病历号.Text = dtDataSource.Rows[0]["住院号"].ToString();
            xrLabel临床诊断.Text=dtDataSource.Rows[0]["诊断"].ToString();
            xrLabel样本号.Text=dtDataSource.Rows[0]["样本号"].ToString();
            xrLabel条码.Text=dtDataSource.Rows[0]["样本条码号"].ToString();
            xrLabel送检医师.Text=dtDataSource.Rows[0]["申请者"].ToString();

            xrLabel备注.Text = dtDataSource.Rows[0]["备注"].ToString();
            xrLabel检验时间.Text =dtDataSource.Rows[0]["检验时间"].ToString();
            xrLabel检验者.Text = dtDataSource.Rows[0]["检验者"].ToString();
            xrLabel医院地址.Text = dtDataSource.Rows[0]["医院地址"].ToString();
            //xrLabel检验时间安排.Text = dtDataSource.Rows[0]["检验时间安排"].ToString();
            //xrLabel采样地点.Text = dtDataSource.Rows[0]["采样地点"].ToString();

            xrLabel报告时间.Text = dtDataSource.Rows[0]["审核时间"].ToString();
            xrLabel审核者.Text = dtDataSource.Rows[0]["审核者"].ToString();

            if (dtDataSource.Rows[0]["检验者手签照"] == null || dtDataSource.Rows[0]["检验者手签照"].ToString() == "")
            {
                xrPictureBox检验者.Visible = false;
            }
            else
            {
                xrLabel检验者.Visible = false;
                xrPictureBox检验者.Image = ImageHelper.GetImageBytes((byte[])(dtDataSource.Rows[0]["检验者手签照"]));
            }

            if (dtDataSource.Rows[0]["审核者手签照"] == null || dtDataSource.Rows[0]["审核者手签照"].ToString() == "")
            {
                xrPictureBox审核者.Visible = false;
            }
            else
            {
                xrLabel审核者.Visible = false;
                xrPictureBox审核者.Image = ImageHelper.GetImageBytes((byte[])(dtDataSource.Rows[0]["审核者手签照"]));
            }

            //xrLabel打印时间.Text = dtDataSource.Rows[0]["打印时间"].ToString();
            xrLabel打印时间.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            xrLabel实验室.Text = dtDataSource.Rows[0]["检验实验室"].ToString();
            xrLabel联系电话.Text = dtDataSource.Rows[0]["医院联系电话"].ToString();

            xrTableCell序号.DataBindings.Add("Text", null, "结果序号");
            xrTableCell项目代码.DataBindings.Add("Text", null, "结果项目代码");
            xrTableCell项目名称.DataBindings.Add("Text", null, "结果项目名称");
            xrTableCell结果.DataBindings.Add("Text", null, "结果值");
            xrTableCell单位.DataBindings.Add("Text", null, "结果项目单位");
            xrTableCell标记.DataBindings.Add("Text", null, "结果标记");
            xrTableCell参考值.DataBindings.Add("Text", null, "结果参考值");

            //printRow["结果序号"] = int序号;
            //printRow["结果项目代码"] = dtResult.Rows[i]["fitem_code"];
            //printRow["结果项目名称"] = dtResult.Rows[i]["fitem_name"];
            //printRow["结果值"] = dtResult.Rows[i]["fvalue"];
            //printRow["结果项目单位"] = dtResult.Rows[i]["fitem_unit"];
            //printRow["结果标记"] = dtResult.Rows[i]["fitem_badge"];
            //printRow["结果参考值"] = dtResult.Rows[i]["fitem_ref"];
            this.DataSource = dtDataSource;
        }

        //private void ReportCom_AfterPrint(object sender, EventArgs e)
        //{
        //    //MessageBox.Show("测试");
        //    if(!(string.IsNullOrWhiteSpace(strJYid)))
        //    {
        //        yunLis.lisbll.sam.jybll bllReport = new yunLis.lisbll.sam.jybll();
        //        bllReport.BllReportUpdatePrintTime(strJYid);
        //    }
        //}

        private void Report20A5C1L_Little_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            if (!printSuccess && !(string.IsNullOrWhiteSpace(strJYid)))
            {
                yunLis.lisbll.sam.jybll bllReport = new yunLis.lisbll.sam.jybll();
                bllReport.BllReportUpdatePrintTime(strJYid);
            }
            printSuccess = true;
        }

    }
}
