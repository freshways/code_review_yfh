﻿using HIS.COMM.Helper;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Data;
using System.Linq;
using yunLis.lisbll.sam;


namespace yunLis.lis.sam.Report
{
    public partial class ReportD280A5 : DevExpress.XtraReports.UI.XtraReport
    {

        private string strJYid = string.Empty;

        private bool printSuccess = false;

        public bool GetPrintResult()
        {
            return printSuccess;
        }

        //public ReportD280A5(string strid, DataTable dtDataSource)
        public ReportD280A5(string jyid, DataTable dtReport, DataTable dtDetail)
        {
            InitializeComponent();

            strJYid = jyid;
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {

                string s住院号码 = dtReport.Rows[0]["住院号"].ToString();
                var item = chis.ZY病人信息.Where(c => c.住院号码 == s住院号码).OrderByDescending(c => c.ID).FirstOrDefault();
                if (item != null)
                {
                    Model年龄 item年龄 = new Model年龄();
                    if (!string.IsNullOrWhiteSpace(item.身份证号))
                    {
                        AgeHelper.GetAgeAndUnitBySFZH(item.身份证号, item年龄);
                        xrLabel年龄.Text = item年龄.默认格式化年龄;
                    }
                    else
                    {
                        AgeHelper.GetOutAgeAndUnitByBirthday(Convert.ToDateTime(item.出生日期), DateTime.Now, item年龄);
                        xrLabel年龄.Text = item年龄.默认格式化年龄;
                    }
                    xrLabel临床诊断.Text = item.疾病名称;
                    xrLabel姓名.Text = item.病人姓名;
                    xrLabel性别.Text = item.性别;

                }
                else
                {
                    xrLabel临床诊断.Text = dtReport.Rows[0]["疾病名称"].ToString();
                    xrLabel姓名.Text = dtReport.Rows[0]["姓名"].ToString(); 
                    xrLabel性别.Text = dtReport.Rows[0]["性别"].ToString();
                    xrLabel年龄.Text = dtReport.Rows[0]["年龄"].ToString();
                }
                //设置报告头信息
                xrLabelTitle.Text = dtReport.Rows[0]["医院名称"].ToString() + dtReport.Rows[0]["报表名称"].ToString();
        
                xrLabel科室.Text = dtReport.Rows[0]["科室"].ToString();
                xrLabel床号.Text = dtReport.Rows[0]["床号"].ToString();
                //xrLabel样本类型.Text = dtReport.Rows[0]["样本类型"].ToString();
                xrLabel患者类别.Text = dtReport.Rows[0]["病人类别"].ToString();
                xrLabel病历号.Text = dtReport.Rows[0]["住院号"].ToString();

       
                xrLabel样本号.Text = dtReport.Rows[0]["样本号"].ToString();
                xrLabel条码.Text = dtReport.Rows[0]["条码"].ToString();
                xrLabel送检医师.Text = dtReport.Rows[0]["申请医师"].ToString();

                xrLabel备注.Text = dtReport.Rows[0]["备注"].ToString();
                xrLabel检验时间.Text = dtReport.Rows[0]["送检日期"].ToString();
                //xrLabel检验者.Text = dtReport.Rows[0]["检验医师"].ToString();

                xrLabel报告时间.Text = dtReport.Rows[0]["报告时间"].ToString();
                //xrLabel审核者.Text = dtReport.Rows[0]["审核者"].ToString();

                xrTableCell序号.DataBindings.Add("Text", null, "forder_by");
                xrTableCell项目代码.DataBindings.Add("Text", null, "fitem_code");
                xrTableCell项目名称.DataBindings.Add("Text", null, "fitem_name");
                xrTableCell结果.DataBindings.Add("Text", null, "fvalue");
                xrTableCell单位.DataBindings.Add("Text", null, "fitem_unit");
                xrTableCell标记.DataBindings.Add("Text", null, "fitem_badge");
                xrTableCell参考值.DataBindings.Add("Text", null, "fitem_ref");





                int s1;

                s1 = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认检验者编码", "", ""));

                xrPic检验者.Image = WEISHENG.COMM.Helper.ImageHelper.BytesToImage(HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == s1).FirstOrDefault().手签照片);


                int s2;
                s2 = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认核对者编码", "", ""));

                xrPic审核者.Image = WEISHENG.COMM.Helper.ImageHelper.BytesToImage(HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == s2).FirstOrDefault().手签照片);


                this.DataSource = dtDetail;
            }
        }


        private void ReportD280A5_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            if (!printSuccess && !(string.IsNullOrWhiteSpace(strJYid)))
            {

                new jybll().BllReportUpdatePrintTimeNew(strJYid);
            }
            printSuccess = true;
        }

    }
}
