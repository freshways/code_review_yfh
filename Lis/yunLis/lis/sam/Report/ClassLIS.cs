﻿using DevExpress.XtraReports.UI;
using HIS.COMM;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace yunLis.lis.sam.Report
{
    /// <summary>
    /// LIS数据库交互业务
    /// </summary>
    public class ClassLIS
    {

        public static bool _s报告单打印(string s住院号或门诊号, string s检验日期, string s样本号, string s检验设备, string s病人来源, bool 直接打印)
        {
            string SQL = "SELECT fjy_id ,fjy_date ,fjy_instr ,fjy_yb_code ,fhz_name ,fprint_zt ,fhz_zyh ,fapply_id " +
                            "FROM [SAM_JY] aa where fjy_zt='已审核' " +
                            " and fjy_date='" + s检验日期 + "' and fjy_yb_code='" + s样本号 + "' and fjy_instr='" + s检验设备 + "' " +
                            " and fhz_zyh='" + s住院号或门诊号 + "' ";
            //获取一条病人的化验信息
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.sLISConnString, CommandType.Text, SQL).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                PrintHelper print = new PrintHelper();
                //print.
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    print.GetData_PrintViewer(dt.Rows[index]["fjy_id"].ToString(), 直接打印);
                }

                return true;
            }
            else
                return false;
        }



        public static bool _s显示化验结果(string s住院号或门诊号, string s检验日期, string s样本号, string s检验设备, string s病人来源)
        {
            string SQL基本信息 = "";
            DataTable dt病人信息 = null;
            if (s病人来源 == "住院")
            {
                SQL基本信息 =
                    "select '" + HIS.COMM.zdInfo.ModelUserInfo.单位编码 + "' 医院名称,'' 报表名称,住院号码, 病人姓名 姓名, 性别,'' 年龄,'' 年龄单位, bb.[科室名称] 送检科室, '' 床号," + "\r\n" +
                    "       '' 样本类别, '' 检验类别, '' 诊断, '' 样本号, '' 样本条码号, bb.[用户名] 申请者," + "\r\n" +
                    "       '' 备注, '' 检验时间, '' 检验者, '' 医院地址, '' 审核时间, '' 检验实验室," + "\r\n" +
                    "       '' 医院联系电话" + "\r\n" +
                    "from   [zy病人信息] aa left outer join pubUser bb on aa.主治医生编码 = bb.[用户编码]" + "\r\n" +
                    "where  aa.住院号码 = " + s住院号或门诊号;
            }
            else if (s病人来源 == "门诊")
            {
                SQL基本信息 =
                     "select '" + HIS.COMM.zdInfo.ModelUserInfo.单位编码 + "' 医院名称,'' 报表名称,住院号码, 病人姓名 姓名, 性别, 年龄, 年龄单位, bb.[科室名称] 送检科室, '' 床号," + "\r\n" +
                     "       '' 样本类别, '' 检验类别, 临床诊断 诊断, '' 样本号, '' 样本条码号, bb.[用户名] 申请者," + "\r\n" +
                     "       '' 备注, '' 检验时间, '' 检验者, '' 医院地址, '' 审核时间, '' 检验实验室," + "\r\n" +
                     "       '' 医院联系电话" + "\r\n" +
                     "from   [MF门诊摘要] aa left outer join pubUser bb on aa.医生编码 = bb.[用户编码]" + "\r\n" +
                     "where  MZID = " + s住院号或门诊号;
            }

            dt病人信息 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL基本信息).Tables[0];

            string SQL检验结果 =
                "SELECT [申请单号], [检验日期], [检验设备], [病人ID], [住院号], [病人姓名], [年龄], [身份证], [检验ID]," + "\r\n" +
                "       [检验编码], [检验名称], [单位], [参考值], [升降], [检验值], [排序], [样本号]" + "\r\n" +
                "FROM   [JY检验结果]" + "\r\n" +
                "WHERE  [住院号] = '" + s住院号或门诊号 + "' and [检验日期]='" + s检验日期 + "' and [样本号]='" + s样本号 + "' and [检验设备]='" + s检验设备 + "' order by convert(int,排序)  ";
            DataTable dt检验结果 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL检验结果).Tables[0];

            ReportComLittle report = new ReportComLittle(dt病人信息, dt检验结果);
            report.ShowPreviewDialog();//软件暂停，等用户响应
            return true;
        }

        public static bool b显示门诊结果(string _sMZID)
        {
            try
            {
                string SQL基本信息 =
                     "select '" + HIS.COMM.zdInfo.ModelUserInfo.单位编码 + "' 医院名称,'' 报表名称, 病人姓名 姓名, 性别, 年龄, 年龄单位, bb.[科室名称] 送检科室, '' 床号," + "\r\n" +
                     "       '' 样本类别, '' 检验类别, 临床诊断 诊断, '' 样本号, '' 样本条码号, bb.[用户名] 申请者," + "\r\n" +
                     "       '' 备注, '' 检验时间, '' 检验者, '' 医院地址, '' 审核时间, '' 检验实验室," + "\r\n" +
                     "       '' 医院联系电话" + "\r\n" +
                     "from   [MF门诊摘要] aa left outer join pubUser bb on aa.医生编码 = bb.[用户编码]" + "\r\n" +
                     "where  MZID = " + _sMZID;

                DataTable dt病人信息 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL基本信息).Tables[0];

                string SQL检验结果 =
                "SELECT [申请单号], [检验日期], [检验设备], [病人ID], [住院号], [病人姓名], [年龄], [身份证], [检验ID]," + "\r\n" +
                "       [检验编码], [检验名称], [单位], [参考值], [升降], [检验值], [排序], [样本号]" + "\r\n" +
                "FROM   [JY检验结果]" + "\r\n" +
                "WHERE  [申请单号] = '" + yunLis.Business.s用申请唯一标识获取申请单号(_sMZID) + "'";
                
                DataTable dt检验结果 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL检验结果).Tables[0];

                ReportComLittle report = new ReportComLittle(dt病人信息, dt检验结果);
                report.ShowPreviewDialog();//软件暂停，等用户响应
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public static bool b显示住院结果(string _sLISID, Pojo病人信息 _person)
        {
            string fapply_id = yunLis.Business.s用申请唯一标识获取申请单号(_sLISID);
            if (string.IsNullOrEmpty(fapply_id)) return false;
            string SQL = "SELECT fjy_id ,fjy_date ,fjy_instr ,fjy_yb_code ,fhz_name ,fprint_zt ,fhz_zyh ,fapply_id " +
                            "FROM [SAM_JY] aa where fjy_zt='已审核' " +
                            " and fapply_id='" + yunLis.Business.s用申请唯一标识获取申请单号(_sLISID) + "' ";
            //获取一条病人的化验信息
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.sLISConnString, CommandType.Text, SQL).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                PrintHelper print = new PrintHelper();
                //print.
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    print.GetData_PrintViewer(dt.Rows[index]["fjy_id"].ToString(), false);
                }

                return true;
            }
            else
                return false;
            //try
            //{
            //    string SQL基本信息 =
            // "select '" + WEISHENG.COMM.zdInfo.sDwmc + "' 医院名称,'' 报表名称,住院号码, 病人姓名 姓名, 性别,'" + _person.S年龄 + "' 年龄,'" + _person.S年龄单位 + "' 年龄单位, bb.[科室名称] 送检科室, 病床 床号," + "\r\n" +
            // "       '血清' 样本类别, '住院' 检验类别, '"+_person.s疾病名称+"' 诊断, '' 样本号, '' 样本条码号, bb.[用户名] 申请者," + "\r\n" +
            // "       '' 备注, '' 检验时间, '' 检验者, '' 医院地址, '' 审核时间, '' 检验实验室," + "\r\n" +
            // "       '' 医院联系电话" + "\r\n" +
            // "from   [zy病人信息] aa left outer join pubUser bb on aa.主治医生编码 = bb.[用户编码]" + "\r\n" +
            // "where  zyid = " + _person.sZYID父;

            //    DataTable dt病人信息 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL基本信息).Tables[0];

            //    string SQL检验结果 =
            //    "SELECT [申请单号], [检验日期], [检验设备], [病人ID], [住院号], [病人姓名], [年龄], [身份证], [检验ID]," + "\r\n" +
            //    "       [检验编码], [检验名称], [单位], [参考值], [升降], [检验值], [排序], [样本号]" + "\r\n" +
            //    "FROM   [JY检验结果]" + "\r\n" +
            //    "WHERE  病人ID='" + _person.sZYID父 + "' and [申请单号] = '" + s用申请唯一标识获取申请单号(_sLISID) +
            //    "'  order by 检验设备,样本号,convert(int,排序) ";
            //    DataTable dt检验结果 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL检验结果).Tables[0];

            //    InterFaceLIS.ReportComLittle report = new InterFaceLIS.ReportComLittle(dt病人信息, dt检验结果);
            //    report.ShowPreviewDialog();//软件暂停，等用户响应
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //    throw ex;
            //}
        }

    }
}
