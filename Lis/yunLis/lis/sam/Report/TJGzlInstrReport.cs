﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace yunLis.lis.sam.Report
{
    public partial class TJGzlInstrReport : DevExpress.XtraReports.UI.XtraReport
    {
        public TJGzlInstrReport()
        {
            InitializeComponent();

            xrLabelDate.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

            xrtCellDetail样本人次.DataBindings.Add("Text", null, "样本人次");
            xrtCellDetail项次.DataBindings.Add("Text", null, "项次");
        }

        public void AddColumn(string colName,string fieldName)
        {
            int newCellLoc = this.xrTableHeader.Rows[0].Cells.Count-2;
            if(newCellLoc < 0)
            {
                return;
            }

            float newCellWidth = this.xrTableHeader.WidthF / (xrTableHeader.Rows[0].Cells.Count + 1);

            #region 表头
            //添加新表头
            XRTableCell cellHeader = new XRTableCell();
            cellHeader.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top;
            cellHeader.Name = "header" + colName;
            cellHeader.Text = colName;
            this.xrTableHeader.Rows[0].InsertCell(cellHeader, newCellLoc);
            //重新设置各表头的宽度
            for (int index = 0; index < this.xrTableHeader.Rows[0].Cells.Count; index++ )
            {
                this.xrTableHeader.Rows[0].Cells[index].WidthF = newCellWidth;
            }
            #endregion

            #region 表中
            //添加Detail里的内容单元格
            XRTableCell cellDetail = new XRTableCell();
            cellDetail.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top;
            cellDetail.Name = "detail"+colName;
            cellDetail.DataBindings.Add("Text", null, fieldName);

            if(newCellLoc==0)
            {
                cellDetail.ProcessDuplicates = ValueSuppressType.MergeByValue;
            }
			else
			{
				cellDetail.BeforePrint += xrTableCell_BeforePrint;
			}
			
            this.xrTableDetail.Rows[0].InsertCell(cellDetail, newCellLoc);
            //重新设置各单元格的宽度
            for (int index = 0; index < this.xrTableDetail.Rows[0].Cells.Count; index++ )
            {
                this.xrTableDetail.Rows[0].Cells[index].WidthF = newCellWidth;
            }
            #endregion

            #region 表尾
            //添加表尾单元格
            XRTableCell cellSummary = new XRTableCell();
            cellSummary.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top| DevExpress.XtraPrinting.BorderSide.Bottom;
            cellSummary.Name = "summary" + colName;
            if(newCellLoc==0)
            {
                cellSummary.Text = "合计";
            }
            this.xrTableSummary.Rows[0].InsertCell(cellSummary, newCellLoc);
            //重新设置各表尾的宽度
            for (int index = 0; index < this.xrTableDetail.Rows[0].Cells.Count; index++)
            {
                this.xrTableSummary.Rows[0].Cells[index].WidthF = newCellWidth;
            }
            #endregion

            this.CreateDocument();
        }

        private void xrTableCell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                XRTableCell currentTableCell = sender as XRTableCell;
                XRTableRow currentTableRow = currentTableCell.Parent as XRTableRow;
                currentTableCell.Borders = DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top;
                currentTableCell.ForeColor = Color.Black;

                int currCellIndex = currentTableCell.Index;
                int currRowIndex = this.CurrentRowIndex;
                int totalColCount = this.xrTableDetail.Rows[0].Cells.Count;

                if (currCellIndex > 0 && currRowIndex > 0 && currCellIndex < (totalColCount - 2))
                {
                    //var currentField = currentCell.DataBindings["Text"];
                    var currColumnField = currentTableCell.DataBindings["Text"].DataMember;
                    var prevColumnField = currentTableRow.Cells[currCellIndex - 1].DataBindings["Text"].DataMember;

                    string cell11 = this.GetPreviousColumnValue(prevColumnField).ToString();
                    string cell12 = this.GetPreviousColumnValue(currColumnField).ToString();
                    string cell21 = this.GetCurrentColumnValue(prevColumnField).ToString();
                    string cell22 = this.GetCurrentColumnValue(currColumnField).ToString();

                    if (cell11 == cell21 && cell12 == cell22)
                    {
                        currentTableCell.Borders = DevExpress.XtraPrinting.BorderSide.Left;
                        currentTableCell.ForeColor = Color.White;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void RemoveColumn(string colName)
        {
            int colLoc = -1;
            int cellsCount = this.xrTableHeader.Rows[0].Cells.Count;

            for(int index=0; index < cellsCount; index ++)
            {
                if (colName == this.xrTableHeader.Rows[0].Cells[index].Text)
                {
                    colLoc = index;
                    break;
                }
            }

            cellsCount--;
            float newWidth = this.xrTableHeader.WidthF / cellsCount;

            this.xrTableHeader.Rows[0].Cells.RemoveAt(colLoc);
            for(int index =0; index<cellsCount; index++)
            {
                this.xrTableHeader.Rows[0].Cells[index].WidthF = newWidth;
            }

            this.xrTableDetail.Rows[0].Cells.RemoveAt(colLoc);
            for (int index = 0; index < cellsCount; index++)
            {
                this.xrTableDetail.Rows[0].Cells[index].WidthF = newWidth;
                this.xrTableDetail.Rows[0].Cells[0].ProcessDuplicates = ValueSuppressType.Leave;
            }
            if(cellsCount > 2)
            {
                this.xrTableDetail.Rows[0].Cells[0].ProcessDuplicates = ValueSuppressType.MergeByValue;
            }

            this.xrTableSummary.Rows[0].Cells.RemoveAt(colLoc);
            for (int index = 0; index < cellsCount; index++)
            {
                this.xrTableSummary.Rows[0].Cells[index].WidthF = newWidth;
            }
            if(colLoc == 0 && cellsCount > 2)
            {
                this.xrTableSummary.Rows[0].Cells[0].Text = "合计";
            }

            this.CreateDocument();
        }

        public void Set统计条件(string context)
        {
            xrLabelTJ.Text = context;
            //this.CreateDocument();
        }

        public void SetDataSource(DataTable dtSource)
        {
            xrLabelDate.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            this.DataSource = dtSource;
            //this.CreateDocument();
        }

        public void SetSummary(int i样本人次, int i项次)
        {
            //xrTableCellSumary样本人次.Text = i样本人次.ToString();
            //xrTableCellSumary项次.Text = i项次.ToString();
            //this.CreateDocument();
        }

         
    }
}
