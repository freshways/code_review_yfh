﻿using HIS.COMM.Helper;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using yunLis.lisbll.sam;
using yunLis.Pojo;

namespace yunLis.lis.sam.Report.UniversalReport
{
    public partial class UniversalReport : DevExpress.XtraReports.UI.XtraReport
    {
        Pojo报告单摘要 item表头;
        private bool printSuccess = false;
        public bool GetPrintResult()
        {
            return printSuccess;
        }

        public UniversalReport(Pojo报告单摘要 _item表头, List<Pojo报告单内容> list报告内容)
        {
            InitializeComponent();

            this.item表头 = _item表头;
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                var item = chis.ZY病人信息.Where(c => c.住院号码 == item表头.住院号).OrderByDescending(c => c.ID).FirstOrDefault();
                if (item != null)
                {
                    Model年龄 item年龄 = new Model年龄();
                    if (!string.IsNullOrWhiteSpace(item.身份证号))
                    {
                        AgeHelper.GetAgeAndUnitBySFZH(item.身份证号, item年龄);
                        xrLabel年龄.Text = item年龄.默认格式化年龄;
                    }
                    else
                    {
                        AgeHelper.GetOutAgeAndUnitByBirthday(Convert.ToDateTime(item.出生日期), DateTime.Now, item年龄);
                        xrLabel年龄.Text = item年龄.默认格式化年龄;
                    }
                    xrLabel临床诊断.Text = item.疾病名称;
                    xrLabel姓名.Text = item.病人姓名;
                    xrLabel性别.Text = item.性别;

                }
                else
                {
                    xrLabel临床诊断.Text = item表头.疾病名称;
                    xrLabel姓名.Text = item表头.姓名;
                    xrLabel性别.Text = item表头.性别;
                    xrLabel年龄.Text = item表头.年龄.ToString();
                }
                //设置报告头信息
                xrLabelTitle.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc + "检验报告单";

                xrLabel科室.Text = item表头.科室;
                xrLabel床号.Text = item表头.床号;
                //xrLabel样本类型.Text = dtReport.Rows[0]["样本类型"].ToString();
                xrLabel患者类别.Text = item表头.病人类别;
                xrLabel病历号.Text = item表头.住院号;


                xrLabel样本号.Text = item表头.样本号;
                xrLabel条码.Text = item表头.条码;
                xrLabel送检医师.Text = item表头.申请医师;

                xrLabel备注.Text = item表头.备注;
                xrLabel检验时间.Text = item表头.送检日期;
                //xrLabel检验者.Text = item表头.检验医师;
                xrLabel报告时间.Text = item表头.报告时间;
                //xrLabel审核者.Text = item表头.审核者;

                xrTableCell序号.DataBindings.Add("Text", null, "序号");
                xrTableCell项目代码.DataBindings.Add("Text", null, "项目代码");
                xrTableCell项目名称.DataBindings.Add("Text", null, "项目名称");
                xrTableCell结果.DataBindings.Add("Text", null, "结果");
                xrTableCell单位.DataBindings.Add("Text", null, "项目单位");
                xrTableCell标记.DataBindings.Add("Text", null, "标记");
                xrTableCell参考值.DataBindings.Add("Text", null, "参考值");
                this.DataSource = list报告内容;

                int s1 = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认检验者编码", "", ""));
                xrPic检验者.Image = WEISHENG.COMM.Helper.ImageHelper.BytesToImage(HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == s1).FirstOrDefault().手签照片);

                int s2 = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认核对者编码", "", ""));
                xrPic审核者.Image = WEISHENG.COMM.Helper.ImageHelper.BytesToImage(HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == s2).FirstOrDefault().手签照片);
            }
        }


        private void ReportD280A5_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            if (!printSuccess && !(string.IsNullOrWhiteSpace(item表头.fjy_id)))
            {
                new jybll().BllReportUpdatePrintTimeNew(item表头.fjy_id);
            }
            printSuccess = true;
        }
    }
}
