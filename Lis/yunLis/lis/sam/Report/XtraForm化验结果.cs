﻿using HIS.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WEISHENG.COMM;
using WEISHENG.COMM.Helper.Datatable2ModelCode;
using WEISHENG.COMM.PluginsAttribute;

namespace yunLis.lis.sam.Report
{
    [ClassInfoMarkAttribute(GUID = "E36B6B11-DF16-4C7C-85B8-7AB1D35D00EA", 父ID = "BE22B406-5548-4F27-9F9D-4582D583E701", 键ID = "E36B6B11-DF16-4C7C-85B8-7AB1D35D00EA", 传递参数 = "",
   菜单类型 = "WinForm", 功能名称 = "化验结果", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.Report.XtraForm化验结果", 全屏打开 = false, 图标名称 = "嘱托维护.png", 显示顺序 = 1, 是否显示 = true)]

    public partial class XtraForm化验结果 : DevExpress.XtraEditors.XtraForm
    {
        LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString));
        public XtraForm化验结果()
        {
            InitializeComponent();
        }

        private void XtraForm化验结果_Load(object sender, EventArgs e)
        {

            this.textEdit开始时间.Text = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            this.textEdit截止时间.Text = DateTime.Now.ToString("yyyy-MM-dd");
            this.cmb病人来源.SelectedIndex = 0;
            Get病人病区();
            txt申请科室.Text = HIS.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        private void Get病人病区()
        {
            DataTable dt = new DataTable();
            dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text,
                "select 科室名称,科室编码 from GY科室设置 where 是否住院科室=1 ").Tables[0];

            txt申请科室.Properties.Items.Clear();

            foreach (DataRow row in dt.Rows)
            {
                txt申请科室.Properties.Items.Add(row["科室名称"]);
            }
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            _Binding检查结果();
        }

        private void simpleButton查看_Click(object sender, EventArgs e)
        {
            try
            {
                print(false);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        private void _Binding检查结果()
        {

            string sqlWhere = "fjy_zt = '已审核' ";
            if (textEdit开始时间.Text != "") sqlWhere += " and 检验日期>='" + textEdit开始时间.Text + "' ";
            if (textEdit截止时间.Text != "") sqlWhere += " and 检验日期<='" + textEdit截止时间.Text + "' ";

            if (cmb病人来源.Text != "" && cmb病人来源.Text == "门诊")
            {
                sqlWhere += " and 病人类别='门诊' ";
            }
            else if (cmb病人来源.Text != "" && cmb病人来源.Text == "住院")
            {
                sqlWhere += " and 病人类别='住院' ";
            }


            if (textEdit门诊或住院号.Text != "")
            {
                sqlWhere += $" and 住院号={textEdit门诊或住院号.Text}";
            }
            if (txt申请科室.Text != "") sqlWhere += " and 科室='" + txt申请科室.Text + "' ";

            gc化验列表.DataSource = Bll.DataHelper.getList报告摘要(sqlWhere);

            this.gv化验列表.BestFitColumns();
            if (gv化验列表.RowCount > 0)
            {
                this.gv化验列表.FocusedRowHandle = 0;
                gc化验列表_Click(null, null);
            }
            else
            {
                gc化验列表.DataSource = null;
                gc化验明细.DataSource = null;
            }

            for (int i = 0; i < gv化验列表.RowCount; i++)
            {
                var row = gv化验列表.GetRow(i) as Pojo.Pojo报告单摘要;
                if (row.打印状态 == "未打印")
                {
                    gv化验列表.SelectRow(i);
                }
            }
        }

        private void gc化验列表_Click(object sender, EventArgs e)
        {
            if (gv化验列表.FocusedRowHandle < 0) return;
            var item = gv化验列表.GetFocusedRow() as Pojo.Pojo报告单摘要;

            gc化验明细.DataSource = Bll.DataHelper.get报告内容(item.fjy_id);

            this.gv化验明细.BestFitColumns();
        }

        /// <summary>
        /// 打印方法
        /// </summary>
        /// <param name="b直接打印">直接打印</param>
        void print(bool b直接打印)
        {
            if (gv化验列表.GetSelectedRows().Length == 0)
            {
                msgHelper.ShowInformation("请选择要打印的报告单");
                return;
            }

            List<Pojo.Pojo报告单摘要> dt选中报告单 = new List<Pojo.Pojo报告单摘要>();
            foreach (int i in gv化验列表.GetSelectedRows())
            {
                var item = gv化验列表.GetRow(i) as Pojo.Pojo报告单摘要;
                dt选中报告单.Add(item);
            }

            var A4Items = dt选中报告单.Where(c => c.报告样式 == "yunLis.lis.sam.Report.NF_T.NF_T");
            var A5Items = dt选中报告单.Where(c => c.报告样式 != "yunLis.lis.sam.Report.NF_T.NF_T");
            bool b取消A4报告选择 = false;
            if (A5Items.Count() > 0 && A4Items.Count() > 0)
            {
                msgHelper.ShowInformation($"当前选择报告包含A4格式{A4Items.Count()}张，A5格式{A5Items.Count()}张。\r\n请注意报告纸张类型,本次只打印A5格式报告单。");
                b取消A4报告选择 = true;
            }

            foreach (int i in gv化验列表.GetSelectedRows())
            {
                //判断A4纸,目前已知只有yunLis.lis.sam.Report.NF_T.NF_T是A4
                var row = gv化验列表.GetRow(i) as Pojo.Pojo报告单摘要;
                if (row.报告样式 == "yunLis.lis.sam.Report.NF_T.NF_T" && b取消A4报告选择)
                {
                    gv化验列表.UnselectRow(i);
                    continue;
                }

                yunLis.lis.sam.Report.PrintHelper pr = new yunLis.lis.sam.Report.PrintHelper();
                bool printSuccess = pr.GetData_PrintViewer(row.fjy_id, b直接打印);
                if (printSuccess)
                {
                    row.打印状态 = "已打印";
                }
            }
        }

        private void simpleButton打印全部_Click(object sender, EventArgs e)
        {
            try
            {
                print(true);
                //if (dtMain == null) return;
                //foreach (DataRow row in dtMain.Rows)
                //{
                //    if (row["打印状态"].ToString() == "已打印") continue;
                //    string sMZID = row["住院号"].ToString();
                //    string s检验日期 = row["检验日期"].ToString();
                //    string s样本号 = row["样本号"].ToString();
                //    string s检验设备 = row["化验设备ID"].ToString();
                //    string s病人来源 = row["病人类别"].ToString();
                //    string s检验ID = row["fjy_id"].ToString();
                //    //bool tru = ClassLIS._s报告单打印(sMZID, s检验日期, s样本号, s检验设备, s病人来源, true);

                //    //string s检验ID = gv化验列表.GetDataRow(gv化验列表.FocusedRowHandle)["fjy_id"].ToString();

                //    yunLis.lis.sam.Report.PrintHelper pr = new yunLis.lis.sam.Report.PrintHelper();
                //    bool printSuccess = pr.BllPrintViewer(s检验ID, false);

                //    if (printSuccess)
                //    {
                //        row["打印状态"] = "已打印";
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                _Binding检查结果();
            }
        }

        private void cmb病人来源_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmb病人来源.Text == "住院")
            {
                layoutControlItem门诊或住院号.Text = "住院号：";
            }
            else
            {
                layoutControlItem门诊或住院号.Text = "门诊号：";
            }
        }

        private void gv化验列表_DoubleClick(object sender, EventArgs e)
        {
            if (gv化验列表.FocusedRowHandle < 0)
            {
                return;
            }
            var item = gv化验列表.GetFocusedRow() as Pojo.Pojo报告单摘要;
            textEdit门诊或住院号.Text = item.住院号;
            simpleButton刷新_Click(null, null);
        }
    }
}
