﻿using HIS.Model;
using System;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WEISHENG.COMM;
using yunLis.lisbll.sam;
using yunLis.wwfbll;

namespace yunLis.lis.sam.Report
{
    public class PrintHelper
    {
        /// <summary>
        /// 报告规则
        /// </summary>
        private jybll bllReport = new jybll();
        LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString));
        public PrintHelper()
        {

        }


        /// <summary>
        /// 使用指定报表打印化验报告
        /// </summary>
        /// <param name="b直接打印"></param>
        /// <param name="fjy_id"></param>
        /// <param name="strReportName"></param>
        /// <param name="strReportPath"></param>
        /// <returns></returns>
        private bool Print(bool b直接打印, string fjy_id, string strReportName, string strReportPath)
        {
            bool printSuccess = false;
            try
            {
                // //报表公共窗口
                // ReportViewer feportviewer = new ReportViewer();
                // //报表数据集
                // feportviewer.MainDataSet = dsPrint;
                // //报表名
                // feportviewer.ReportName = strReportName;
                // //报表数据源名
                // feportviewer.MainDataSourceName = strMainDataSourceName;
                // //报表文档
                // feportviewer.ReportPath = strReportPath;
                // //显示报表窗口
                // feportviewer.ShowDialog();
                // this.bllReport.BllReportUpdatePrintTime(fjy_id);
                //string str样本号 = dt打印数据.Rows[0]["样本号"].ToString();

                var item表头 = Bll.DataHelper.getModel报告摘要(fjy_id);
                var list报告内容 = Bll.DataHelper.get报告内容(fjy_id);
                switch (strReportPath)
                {
                    case "":
                    case "通用":
                    case "yunLis.lis.sam.Report.ReportD280A5"://通用样式
                    default:
                        UniversalReport.UniversalReport repComm = new UniversalReport.UniversalReport(item表头, list报告内容);
                        var printParam = new HIS.COMM.Report.PrintParams { invokeModuleName = "化验单默认纸张" };
                        printParam.paperName = "化验单默认纸张";
                        printParam.Landscape = false;
                        printParam.width = 2100;
                        printParam.height = 1480;
                        printParam.marginTop = 30;
                        printParam.marginLeft = 20;
                        printParam.marginRight = 0;
                        printParam.marginBottom = 0;
                        printParam.isPreview = !b直接打印;
                        HIS.COMM.Helper.ReportHelper2.Print(repComm, printParam);
                        break;
                    case "yunLis.lis.sam.Report.NF_T.NF_T"://A4纸的血栓弹力图
                        //var data = bllReportNftData.getDate(fjy_id);
                        yunLis.lis.sam.Report.NF_T.NF_T repComm3 = new yunLis.lis.sam.Report.NF_T.NF_T(item表头, list报告内容);
                        var printParam3 = new HIS.COMM.Report.PrintParams { invokeModuleName = "化验单_血栓弹力图" };
                        printParam3.paperName = "化验单_血栓弹力图";
                        printParam3.Landscape = false;
                        printParam3.width = 2100;
                        printParam3.height = 2970;
                        printParam3.marginTop = 30;
                        printParam3.marginLeft = 20;
                        printParam3.marginRight = 0;
                        printParam3.marginBottom = 0;
                        printParam3.isPreview = !b直接打印;
                        HIS.COMM.Helper.ReportHelper2.Print(repComm3, printParam3);
                        break;
                    case "反射加载":
                        try
                        {
                            Type type = Type.GetType(strReportPath);
                            object[] parameters = new object[2];
                            parameters[0] = fjy_id;
                            parameters[1] = null;//dt打印摘要数据2
                            parameters[2] = null;//dt打印明细数据2
                            object obj = System.Activator.CreateInstance(type, parameters);

                            DevExpress.XtraReports.UI.XtraReport rep = obj as DevExpress.XtraReports.UI.XtraReport;

                            var printParam2 = new HIS.COMM.Report.PrintParams { invokeModuleName = "化验单默认纸张" };
                            printParam2.paperName = "化验单默认纸张";
                            printParam2.Landscape = false;
                            printParam2.width = 2100;
                            printParam2.height = 1480;
                            printParam2.marginTop = 30;
                            printParam2.marginLeft = 20;
                            printParam2.marginRight = 0;
                            printParam2.marginBottom = 0;
                            printParam2.isPreview = !b直接打印;
                            HIS.COMM.Helper.ReportHelper2.Print(rep, printParam2);
                            //此处没有对 printSuccess赋值，由于使用了反射机制，获取report对象里的printSuccess值                    
                            System.Reflection.MethodInfo method = type.GetMethod("GetPrintResult");
                            Object ret = method.Invoke(obj, null);
                            printSuccess = (bool)ret;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("打印报告单时出现异常\n" + ex.Message);
                            printSuccess = false;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return printSuccess;
        }


        /// <summary>
        /// 构建数据源，调用指定报表
        /// </summary>
        /// <param name="b直接打印">false：打印预览；true：打印</param>
        /// <param name="strfsample_id">样本ID</param>
        public bool GetData_PrintViewer(string strfsample_id, bool b直接打印)
        {
            bool printSuccess = false;
            try
            {
                var sam_jy = lisEntities.SAM_JY.Where(c => c.fjy_id == strfsample_id).FirstOrDefault();
                var reportSetting = lisEntities.SAM_REPORT.Where(c => c.finstr_id == sam_jy.fjy_instr).FirstOrDefault();
                if (reportSetting == null)
                {
                    try
                    {
                        reportSetting = new SAM_REPORT();
                        reportSetting.fsetting_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                        reportSetting.finstr_id = sam_jy.fjy_instr;
                        reportSetting.fcode = "通用";
                        lisEntities.SAM_REPORT.Add(reportSetting);
                        lisEntities.SaveChanges();
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                                    validationErrors.Entry.Entity.GetType().FullName,
                                    validationError.PropertyName,
                                    validationError.ErrorMessage));
                            }
                        }
                        HIS.COMM.msgBalloonHelper.ShowInformation(stringBuilder.ToString());
                    }
                    catch (Exception ex)
                    {
                        string errorMsg = "错误：";
                        if (ex.InnerException == null)
                            errorMsg += ex.Message + "，";
                        else if (ex.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.Message + "，";
                        else if (ex.InnerException.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.InnerException.Message;
                        HIS.COMM.msgBalloonHelper.ShowInformation(errorMsg);
                    }
                }

                printSuccess = Print(b直接打印, strfsample_id, reportSetting.fname, reportSetting.fcode);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return printSuccess;
        }

    }
}
