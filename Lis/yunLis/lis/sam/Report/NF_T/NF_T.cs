﻿using HIS.COMM.Helper;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WEISHENG.COMM;
using yunLis.Pojo;

namespace yunLis.lis.sam.Report.NF_T
{
    public partial class NF_T : DevExpress.XtraReports.UI.XtraReport
    {
        LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString));
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        Pojo报告单摘要 item表头;
        private bool printSuccess = false;

        public bool GetPrintResult()
        {
            return printSuccess;
        }
        public NF_T(Pojo报告单摘要 _item表头, List<Pojo报告单内容> list报告内容)
        {
            InitializeComponent();
            this.item表头 = _item表头;
            //获取打印图片，1、检验结果表；2、设备结果表；

            var samjy = lisEntities.SAM_JY.Where(c => c.fjy_id == item表头.fjy_id).FirstOrDefault();
            decimal zyid = Convert.ToDecimal(samjy.fhz_id);
            var zy病人信息 = chis.ZY病人信息.Where(c => c.ZYID == zyid).FirstOrDefault();
            if (samjy == null)
            {
                msgHelper.ShowInformation("获取检验结果信息异常");
            }
            var img = lisEntities.SAM_JY_IMG.Where(c => c.fsample_id == item表头.fjy_id).FirstOrDefault();
            if (img == null)
            {
                var img设备 = lisEntities.Lis_Ins_Result_Img.Where(c => c.FTaskID == samjy.finstr_result_id).FirstOrDefault();
                if (img设备 != null)
                {
                    try
                    {
                        xrPictureBox1.Image = WEISHENG.COMM.Helper.ImageHelper.BytesToImage(img设备.FImg);
                    }
                    catch (Exception ex)
                    {
                        xrPictureBox1.Image = null;
                        msgHelper.ShowInformation($@"转换{samjy.finstr_result_id}图像失败，请重新传输数据/r/n{ex.Message}");
                    }
                }
            }
            else
            {
                xrPictureBox1.Image = WEISHENG.COMM.Helper.ImageHelper.BytesToImage(img.fimg);
            }


            //设置报告头信息
            xrLabel姓名.Text = zy病人信息.病人姓名;
            xrLabel性别.Text = zy病人信息.性别;
            Model年龄 item年龄 = new Model年龄();
            if (!string.IsNullOrWhiteSpace(zy病人信息.身份证号))
            {
                AgeHelper.GetAgeAndUnitBySFZH(zy病人信息.身份证号, item年龄);
                xrLabel年龄.Text = item年龄.默认格式化年龄;
            }
            else
            {
                AgeHelper.GetAgeAndUnitByBirthday(Convert.ToDateTime(zy病人信息.出生日期), item年龄);
                xrLabel年龄.Text = item年龄.默认格式化年龄;
            }

            xrLabelTitle.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc + "检验报告单";
            
            xrLabel床号.Text = item表头.床号;

            xrLabel病历号.Text = item表头.住院号;
            if (zy病人信息 == null)
            {
                xrLabel临床诊断.Text = item表头.疾病名称;
            }
            else
            {
                xrLabel临床诊断.Text = zy病人信息.疾病名称;
                this.xrBarCode1.Text = zy病人信息.住院号码;
            }

           
            xrLabel科室.Text = item表头.科室;
            xrLabel患者类别.Text = item表头.病人类别;
            xrLabel条码.Text = item表头.条码;
            xrLabel送检医师.Text = item表头.申请医师;
            xrLabel报告时间.Text = item表头.报告时间;
            xrLabel检验时间.Text = item表头.送检日期;       
            xrLabel备注.Text = item表头.备注;
            xrLabel样本号.Text = item表头.样本号;
            //xrLabel审核者.Text = item表头.审核者;
            //xrLabel检验者.Text = item表头.检验者;
            int s1 = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认检验者编码", "", ""));
            xrPictureBox检验者.Image = WEISHENG.COMM.Helper.ImageHelper.BytesToImage(HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == s1).FirstOrDefault().手签照片);

            int s2 = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认核对者编码", "", ""));
            xrPictureBox审核者.Image = WEISHENG.COMM.Helper.ImageHelper.BytesToImage(HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == s2).FirstOrDefault().手签照片);

            //xrLabel打印时间.Text = item表头.打印时间;
            //xrLabel实验室.Text = item表头.检验实验室;
            //xrLabel联系电话.Text = item表头.医院联系电话;
            //xrLabel医院地址.Text = item表头.医院地址;
            //xrLabel检验时间安排.Text = item表头.检验时间安排;
            //xrLabel采样地点.Text = item表头.采样地点;
            //xrLabel样本类型.Text = item表头.样本类别;          
            //xrLabel病历号.Text=


            //if (item表头.检验者手签照"] == null || item表头.检验者手签照 == "")
            //{
            //    xrPictureBox检验者.Visible = false;
            //}
            //else
            //{
            //    xrLabel检验者.Visible = false;
            //    xrPictureBox检验者.Image = ImageHelper.GetImageBytes((byte[])(item表头.检验者手签照));
            //}

            //if (item表头.审核者手签照 == null || item表头.审核者手签照 == "")
            //{
            //    xrPictureBox审核者.Visible = false;
            //}
            //else
            //{
            //    xrLabel审核者.Visible = false;
            //    xrPictureBox审核者.Image = ImageHelper.GetImageBytes((byte[])(item表头.审核者手签照));
            //}



            xrLabel打印时间.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
      

            xrTableCell序号.DataBindings.Add("Text", null, "序号");
            xrTableCell项目代码.DataBindings.Add("Text", null, "项目代码");
            xrTableCell项目名称.DataBindings.Add("Text", null, "项目名称");
            xrTableCell结果.DataBindings.Add("Text", null, "结果");
            xrTableCell单位.DataBindings.Add("Text", null, "项目单位");
            xrTableCell标记.DataBindings.Add("Text", null, "标记");
            xrTableCell参考值.DataBindings.Add("Text", null, "参考值");
            this.DataSource = list报告内容;
        }


        private void ReportComLittle_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            if (!printSuccess && !(string.IsNullOrWhiteSpace(item表头.fjy_id)))
            {
                yunLis.lisbll.sam.jybll bllReport = new yunLis.lisbll.sam.jybll();
                bllReport.BllReportUpdatePrintTime(item表头.fjy_id);
            }
            printSuccess = true;
        }

    }
}
