﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;
using yunLis.lisbll.sam;

namespace yunLis.lis.sam.qc
{
    public partial class ItemSetForm : yunLis.wwf.SysBaseForm
    {
        ItemBLL bllItem = new ItemBLL();//项目逻辑        
        DataTable dtItem = new DataTable();//当前项目表    
        DataRowView rowCurrItem = null;//当前项目行
        string strCurrItemGuid = "";
        string strInstrID = "";//仪器ID
        DataTable dtInstr = new DataTable(); //fjytype_id     
        InstrBLL bllInstr = new InstrBLL(); 

        public ItemSetForm()
        {
            InitializeComponent();
        }

        private void ItemSetForm_Load(object sender, EventArgs e)
        {
            GetInstrDT();
        }
        /// <summary>
        /// 已经启用的仪器
        /// </summary>
        private void GetInstrDT()
        {
            try
            {
                if (dtInstr.Rows.Count > 0)
                    dtInstr.Clear();
             //   dtInstr = this.bllItem.BllInstrDT(1, LoginBLL.strDeptID);
                this.dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, yunLis.wwfbll.LoginBLL.strDeptID);//仪器列表
                // dtInstr = this.bllItem.BllInstrDT(1, "kfz");
                this.comboBoxInstr.DataSource = dtInstr;

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = yunLis.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    //this.comboBoxInstr.SelectedValue = strInstrID;
                    this.comboBoxInstr.SelectedItem = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得仪器所检项目
        /// </summary>
        /// <param name="strInstrID"></param>
        private void GetItemDT(string strInstrID)
        {
            try
            {
                if (dtItem != null)
                    dtItem.Clear();
                dtItem = this.bllItem.BllItem(1, strInstrID);
                this.sam_itemBindingSource.DataSource = dtItem;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void comboBoxInstr_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                strInstrID = this.comboBoxInstr.SelectedValue.ToString();
                GetItemDT(strInstrID);              
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.sam_itemBindingSource.EndEdit();
                string strfitem_id = "";
                string strfzk_if = "";
                for (int i = 0; i < this.dtItem.Rows.Count; i++)
                {
                    strfitem_id = this.dtItem.Rows[i]["fitem_id"].ToString();
                    if (this.dtItem.Rows[i]["fzk_if"] != null)
                        strfzk_if = this.dtItem.Rows[i]["fzk_if"].ToString();
                    else
                        strfzk_if = "0";
                    this.bllItem.BllItemUpdatefzk_if(strfitem_id, strfzk_if);
                }
                GetItemDT(strInstrID);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void sam_itemBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrItem = (DataRowView)sam_itemBindingSource.Current;//行
                if (rowCurrItem != null)
                {
                    strCurrItemGuid = rowCurrItem["fitem_id"].ToString();                  
                }
                else
                {                  
                    strCurrItemGuid = "";
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetItemDT(strInstrID);          
        }

        

    }
}