using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.IO;
using ww.form.wwf.print;
using ww.wwf.wwfbll;
using ZedGraph;


namespace ww.form.lis.sam.qc
{
    public partial class QCImgForm : ww.form.wwf.SysBaseForm
    {
        private EMFStreamPrintDocument printDoc = null;
        
        public QCImgForm()
        {
            InitializeComponent();                
        }
        private void ReportViewer_Load(object sender, EventArgs e)
        {
            try
            {
                bllInit();               
                
            }
            catch (Exception ex)
            {
                 WWMessage.MessageShowError(ex.ToString());               
            }
        }
        #region 方法
        /// <summary>
        /// 系统初始化
        /// </summary>
        private void bllInit()
        {
            try
            {
                DateTime dtime = System.DateTime.Now;
                string strtime = dtime.ToString("yyyy") + "-" + dtime.ToString("MM") + "-" + "01";
                dtime_s.Value = Convert.ToDateTime(strtime);
                zedGraphControl1.Visible = false;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.Message.ToString());
            }
        }
        private void buttonQuery_Click(object sender, EventArgs e)
        {
            bllReport();
        }
        private void bllReport()
        {
            try
            {

                double dou靶值 = 10;//靶值
                double douSD值 = 1;//SD值
                Double[] dou项目值 = { 9.1, 8, 10, 12, 11, 8.5 };
                DateTime[] dtime项目值日期 = { Convert.ToDateTime("2013-08-01"), Convert.ToDateTime("2013-08-02"), Convert.ToDateTime("2013-08-03"), Convert.ToDateTime("2013-08-04"), Convert.ToDateTime("2013-08-05"), Convert.ToDateTime("2013-08-06") };
                Bll_L_J_Chart(dou项目值, dtime项目值日期, dou靶值, douSD值);
                //getImage("taotao.jpg");



                //数据
                DataSet dsPrint = new DataSet();
                samDataSet.sam_zkDataTable dtPrintDataTable = new samDataSet.sam_zkDataTable();


                //string strYear = this.comboBoxDateS.SelectedItem.ToString();
                //string strMonth = this.comboBoxDateE.SelectedItem.ToString();
                string strSql = "SELECT  fzk_date as 日期,  fvalue as 值 FROM  SAM_ZK_VALUE ORDER BY fzk_date";
                DataTable dt = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow printRow = dtPrintDataTable.NewRow();
                    printRow["日期"] = dt.Rows[i]["日期"].ToString();
                    Double douValue = Convert.ToDouble(dt.Rows[i]["值"].ToString());
                    printRow["值"] = douValue;


                    ImageConverter ic = new ImageConverter();
                    printRow["L_J图"] = (byte[])ic.ConvertTo(zedGraphControl1.GetImage(), typeof(byte[]));

                  


                    dtPrintDataTable.Rows.Add(printRow);
                }
                dsPrint.Tables.Add(dtPrintDataTable);

                #region 报表设置值
                MainDataSet = dsPrint;//报表数集               
                ReportName = "QCImgReport"; //报表名              
                MainDataSourceName = "samDataSet_sam_zk";  //报表数据源名
                //MainDataSourceName2 = "DataSetHCMS_CMS_REPORT_TYPE1";               
                ReportPath = "ww.form.lis.sam.qc.QCImgReport.rdlc"; //报表文档
                this.AddReportDataSource(this.m_MainDataSet, this.m_MainDataSourceName, m_MainDataSourceName2);
                rptViewer.LocalReport.Refresh();
                rptViewer.RefreshReport();
                #endregion
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }

        //public  byte[] BitmapToBytes(Image Bitmap)
        //{
        //    MemoryStream ms = null;
        //    try
        //    {
        //        ms = new MemoryStream();
        //        Bitmap.Save(ms, Bitmap.RawFormat);
        //        byte[] byteImage = new Byte[ms.Length];
        //        byteImage = ms.ToArray();
        //        return byteImage;
        //    }
        //    catch (ArgumentNullException ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ms.Close();
        //    }
        //}

        /// <summary>
        /// 生成图型文件
        /// </summary>
        /// <param name="s"></param>
        public void getImage(string s)
        {
            Console.WriteLine(s);
            
            zedGraphControl1.GetImage().Save(s, System.Drawing.Imaging.ImageFormat.Jpeg);
            //ImageConverter ic = new ImageConverter();

            //byte[] bData = (byte[])ic.ConvertTo(zedGraphControl1.GetImage(), typeof(byte[]));
            //string s = Convert.ToBase64String(bData);
            //ImageConverter ic = new ImageConverter();
            //byte[] bData = (byte[])ic.ConvertTo(zedGraphControl1.GetImage(), typeof(byte[]));
            //string s = Convert.ToBase64String(bData);
            //MessageBox.Show(s);
        }
        /// <summary>
        /// L_J质控图
        /// </summary>
        /// <param name="dou项目值"></param>
        /// <param name="dtime项目值日期"></param>
        /// <param name="dou靶值"></param>
        /// <param name="douSD值"></param>
        private void Bll_L_J_Chart(Double[] dou项目值, DateTime[] dtime项目值日期, double dou靶值, double douSD值)
        {
            try
            {
                int cntDays = 1;
                int inumber2 = 1;

                zedGraphControl1.Dock = DockStyle.Fill;
                zedGraphControl1.Visible = true;
                zedGraphControl1.IsShowPointValues = true;
                inumber2 = dou项目值.Length;//4
                if (dtime项目值日期.Length <= 0)
                {
                    return;
                }

                //时间区间 比如2013-08-01 至 2013-08-06 为1----6
                DateTime dtime1;
                DateTime dtime2;
                dtime1 = dtime_s.Value.Date;
                dtime2 = dtime_e.Value.Date;
                double time1 = dtime1.ToOADate();
                double time2 = dtime2.Date.AddDays(1).ToOADate();
                int itime1 = Convert.ToInt32(time1);
                int itime2 = Convert.ToInt32(time2);
                cntDays = itime2 - itime1;

                double[] arrDaysSelected = new double[cntDays];
                arrDaysSelected[0] = time1;
                arrDaysSelected[cntDays - 1] = time1 + cntDays;

                Double[] avg_l = new Double[cntDays];
                Double[] std_l1_1 = new Double[cntDays];
                Double[] std_l1_2 = new Double[cntDays];
                Double[] std_l2_1 = new Double[cntDays];
                Double[] std_l2_2 = new Double[cntDays];
                Double[] std_l3_1 = new Double[cntDays];
                Double[] std_l3_2 = new Double[cntDays];
                Double[] intd = new Double[cntDays];

                #region    绘图相关属性
                GraphPane myPane = zedGraphControl1.GraphPane;
                myPane.CurveList.Clear();
                myPane.GraphObjList.Clear();
                myPane.XAxis.Type = AxisType.Date;      //数据显示方式:按日期方式显示
                myPane.XAxis.Scale.Format = "dd";       //X轴的显示格式         
                myPane.Title.Text = "L_J质控图";
                myPane.Title.FontSpec.Size = 14;
                myPane.Title.IsVisible = false;
                myPane.Border.IsVisible = false;

                //myPane.XAxis.Title.Text = Convert.ToString(dtime1.Date.ToShortDateString() + "至" + dtime2.Date.ToShortDateString());
                myPane.XAxis.Title.IsVisible = false;
                myPane.YAxis.Title.Text = "";
                myPane.Margin.Left = 20;
                myPane.Chart.Fill = new Fill(Color.White);

                #region    相关数据的准备
                PointPairList list = new PointPairList();
                PointPairList lstAvg = new PointPairList();
                PointPairList lstSd1_1 = new PointPairList();
                PointPairList lstSd1_2 = new PointPairList();
                PointPairList lstSd2_1 = new PointPairList();
                PointPairList lstSd2_2 = new PointPairList();
                PointPairList lstSd3_1 = new PointPairList();
                PointPairList lstSd3_2 = new PointPairList();
                for (int i = 0; i < inumber2; i++)
                {
                    double x = (double)new XDate(dtime项目值日期[i].Date);
                    double y = dou项目值[i];
                    list.Add(x, y);
                }
                for (int i = 0; i < cntDays; i++)
                {
                    avg_l[i] = dou靶值;// Sender.ItemProperty.FAvg;
                    std_l1_1[i] = dou靶值 + douSD值;
                    std_l1_2[i] = dou靶值 - douSD值;
                    std_l2_1[i] = dou靶值 + 2 * douSD值;
                    std_l2_2[i] = dou靶值 - 2 * douSD值;
                    std_l3_1[i] = dou靶值 + 3 * douSD值;
                    std_l3_2[i] = dou靶值 - 3 * douSD值;
                }
                lstAvg.Add(arrDaysSelected, avg_l);
                lstSd1_1.Add(arrDaysSelected, std_l1_1);
                lstSd1_2.Add(arrDaysSelected, std_l1_2);
                lstSd2_1.Add(arrDaysSelected, std_l2_1);
                lstSd2_2.Add(arrDaysSelected, std_l2_2);
                lstSd3_1.Add(arrDaysSelected, std_l3_1);
                lstSd3_2.Add(arrDaysSelected, std_l3_2);
                #endregion

                #region    在点旁标注值
                if (chkShowValue.Checked == true)
                {
                    for (int i = 0; i < dou项目值.Length; i++)
                    {
                        double x = (double)new XDate(dtime项目值日期[i].Date);
                        TextObj theText = new TextObj(dou项目值[i].ToString(), x + 0.01, dou项目值[i] + dou项目值[i] * 0.003, CoordType.AxisXY2Scale, AlignH.Left, AlignV.Center);
                        theText.FontSpec.FontColor = Color.Blue;
                        theText.ZOrder = ZOrder.A_InFront;
                        theText.FontSpec.Border.IsVisible = false;
                        theText.FontSpec.Fill.IsVisible = false;
                        theText.FontSpec.Size = 13;
                        myPane.GraphObjList.Add(theText);
                    }
                }
                #endregion

                LineItem myCurve = myPane.AddCurve("", list, Color.Green, SymbolType.Circle);
                myCurve.Line.IsVisible = true;
                myCurve.Symbol.Size = 5;
                myCurve.Symbol.Fill = new Fill(Color.Green);
                myCurve.IsY2Axis = true;
                myCurve.YAxisIndex = 1;

                #region    X轴的相关设置
                myPane.XAxis.Scale.MajorStep = 1;       //X轴的最大刻度单位
                myPane.XAxis.Scale.MinorStep = 1;       //X轴的最小刻度单位
                myPane.XAxis.MajorGrid.IsVisible = true;        //虚线网格是否显示
                myPane.XAxis.MajorGrid.DashOn = 2;
                myPane.XAxis.MajorGrid.DashOff = 2;
                myPane.XAxis.MajorGrid.PenWidth = 1.5f;
                myPane.XAxis.MajorGrid.Color = Color.LightGray;
                myPane.XAxis.Scale.IsSkipLastLabel = true;
                myPane.XAxis.Scale.BaseTic = time1;
                myPane.XAxis.Scale.Max = time2;
                myPane.XAxis.Scale.Min = time1 - 1;
                #endregion

                #region    绘制AVG、SD、2SD、3SD、-SD、-2SD、-3SD线
                LineItem myCurve1 = myPane.AddCurve("", lstAvg, Color.Black, SymbolType.None);
                myCurve1.IsY2Axis = true;
                myCurve1.YAxisIndex = 1;

                LineItem myCurve2 = myPane.AddCurve("", lstSd1_1, Color.DarkGoldenrod, SymbolType.None);
                myCurve2.IsY2Axis = true;
                myCurve2.YAxisIndex = 1;

                LineItem myCurve3 = myPane.AddCurve("", lstSd1_2, Color.DarkGoldenrod, SymbolType.None);
                myCurve3.IsY2Axis = true;
                myCurve3.YAxisIndex = 1;

                LineItem myCurve4 = myPane.AddCurve("", lstSd2_1, Color.Red, SymbolType.None);
                myCurve4.IsY2Axis = true;
                myCurve4.YAxisIndex = 1;

                LineItem myCurve5 = myPane.AddCurve("", lstSd2_2, Color.Red, SymbolType.None);
                myCurve5.IsY2Axis = true;
                myCurve5.YAxisIndex = 1;

                LineItem myCurve6 = myPane.AddCurve("", lstSd3_1, Color.Blue, SymbolType.None);
                myCurve6.IsY2Axis = true;
                myCurve6.YAxisIndex = 1;

                LineItem myCurve7 = myPane.AddCurve("", lstSd3_2, Color.Blue, SymbolType.None);
                myCurve7.IsY2Axis = true;
                myCurve7.YAxisIndex = 1;
                #endregion

                #region    第一个Y轴坐标的相关设置
                myPane.YAxis.Scale.FontSpec.FontColor = Color.Red;
                myPane.YAxis.MajorTic.IsInside = false;
                myPane.YAxis.MajorTic.IsOutside = false;
                myPane.YAxis.MinorTic.IsInside = false;
                myPane.YAxis.MinorTic.IsOutside = false;
                myPane.YAxis.MajorTic.IsOpposite = false;
                myPane.YAxis.MinorTic.IsOpposite = false;
                myPane.YAxis.MajorGrid.IsZeroLine = false;
                myPane.YAxis.MajorGrid.IsVisible = false;        //虚线网格是否显示
                myPane.YAxis.Type = AxisType.Text;

                string[] ylabs = new string[8];
                ylabs[0] = "-3SD";
                ylabs[1] = "-2SD";
                ylabs[2] = "-1SD";
                ylabs[3] = "AVG";
                ylabs[4] = "+1SD";
                ylabs[5] = "+2SD";
                ylabs[6] = "+3SD";
                ylabs[7] = "";

                myPane.YAxis.Scale.Min = 0;
                myPane.YAxis.Scale.Max = 8;
                myPane.YAxis.Scale.TextLabels = ylabs;
                myPane.YAxis.Scale.Align = AlignP.Inside;
                #endregion

                #region    第二个Y轴坐标的相关设置
                myPane.Y2Axis.Scale.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.Title.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.IsVisible = true;
                myPane.Y2Axis.MajorTic.IsOpposite = false;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.Scale.Align = AlignP.Inside;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.Scale.Align = AlignP.Inside;
                myPane.Y2Axis.Scale.Min = dou靶值 - 4 * douSD值;
                myPane.Y2Axis.Scale.Max = dou靶值 + 4 * douSD值;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.MajorGrid.Color = Color.LightGray;
                #endregion

                zedGraphControl1.AxisChange();
                zedGraphControl1.Refresh();
                #endregion

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }
        

         


        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == (Keys.F5))
            {
                buttonQuery.PerformClick();
                return true;
            }
            //else if (keyData == (Keys.F9))
            //{
            //    button审核.PerformClick();
            //    return true;
            //}

            return base.ProcessCmdKey(ref   msg, keyData);
        }
        #endregion




        #region 事件
       

        /// <summary>
        /// Set the DataSource for the report using a strong-typed dataset
        /// Call it in Form_Load event
        /// </summary>
        private void AddReportDataSource(object ReportSource, string ReportDataSetName1, string ReportDataSetName2)
        {

            rptViewer.LocalReport.DataSources.Clear();//陶银洲 新加
            System.Type type = ReportSource.GetType();
            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n   The datasource is of the wrong type!");
                return;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        if (ReportDataSetName1 == string.Empty)
                        {
                            ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                            return;
                        }

                        this.rptViewer.LocalReport.DataSources.Add(
                            new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName1,
                            (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                            );

                        this.rptViewer.LocalReport.DataSources.Add(
                          new Microsoft.Reporting.WinForms.ReportDataSource(ReportDataSetName2,
                          (piData.GetValue(ReportSource, null) as System.Data.DataTableCollection)[0])
                          );

                        this.rptViewer.RefreshReport();
                        break;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
            }
        }

        private System.Data.DataTableCollection GetTableCollection(object ReportSource)
        {
            System.Type type = ReportSource.GetType();

            if (type == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n     The datasource is of the wrong type!");
                return null;
            }
            else
            {
                System.Reflection.PropertyInfo[] picData = type.GetProperties();
                bool bolExist = false;
                foreach (System.Reflection.PropertyInfo piData in picData)
                {
                    if (piData.Name == "Tables")
                    {
                        bolExist = true;
                        return piData.GetValue(ReportSource, null) as System.Data.DataTableCollection;
                    }
                }

                if (!bolExist)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return null;
                }
            }
            return null;
        }

       
        
        private void rptViewer_Drillthrough(object sender, Microsoft.Reporting.WinForms.DrillthroughEventArgs e)
        {
            if (this.m_DrillDataSet == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The dataset name for the report does not exist or is empty!");
                return;
            }
            else
            {
                if (this.m_DrillDataSourceName == string.Empty)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewr: \r\n    The datasource is of the wrong type!");
                    return;
                }
                else
                {
                    Microsoft.Reporting.WinForms.LocalReport report = e.Report as Microsoft.Reporting.WinForms.LocalReport;
                    report.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource(this.m_DrillDataSourceName, this.GetTableCollection(this.m_DrillDataSet)[0]));
                }
            }
        }


  


        private void printView()
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                       
                        this.printDoc = null;
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                        PageSettings frm = new PageSettings(this.m_ReportName);
                        frm.ShowDialog();
                        frm.Dispose();     
                        return;
                    }
                }

              
               
               // this.PreviewDialog.Dispose();
               // this.printDoc = null;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();     
            }
        }

        private string GetTimeStamp()
        {
            string strRet = string.Empty;
            System.DateTime dtNow = System.DateTime.Now;
            strRet += dtNow.Year.ToString() +
                        dtNow.Month.ToString("00") +
                        dtNow.Day.ToString("00") +
                        dtNow.Hour.ToString("00") +
                        dtNow.Minute.ToString("00") +
                        dtNow.Second.ToString("00") +
                        System.DateTime.Now.Millisecond.ToString("000");
            return strRet;

        }
        private void ExcelOut()
        {
            try
            {
                Microsoft.Reporting.WinForms.Warning[] Warnings;
                string[] strStreamIds;
                string strMimeType;
                string strEncoding;
                string strFileNameExtension;

                byte[] bytes = this.rptViewer.LocalReport.Render("Excel", null, out strMimeType, out strEncoding, out strFileNameExtension, out strStreamIds, out Warnings);

                string strFilePath = @"C:\" + this.GetTimeStamp() + ".xls";
                //*
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "XLS文件|*.xls|所有文件|*.*";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    strFilePath = saveFileDialog.FileName;
                    using (System.IO.FileStream fs = new FileStream(strFilePath, FileMode.Create))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }
                    
                    if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("导出Excel文件成功：" + strFilePath + "\r\n\n 是否打开此文件：" + strFilePath + "?"))
                    {
                        System.Diagnostics.Process.Start(strFilePath);
                    }
                }
                //*                
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        


        

        private void toolLast_Click(object sender, EventArgs e)
        {

        }

        private void tool25_Click(object sender, EventArgs e)
        {

        }

        private void tool50_Click(object sender, EventArgs e)
        {

        }

        private void tool100_Click(object sender, EventArgs e)
        {

        }

        private void tool200_Click(object sender, EventArgs e)
        {

        }

        private void tool400_Click(object sender, EventArgs e)
        {

        }

        private void toolWhole_Click(object sender, EventArgs e)
        {

        }

        private void toolPageWidth_Click(object sender, EventArgs e)
        {

        }


        

        private void toolPrevious_Click(object sender, EventArgs e)
        {

        }

        private void toolNext_Click(object sender, EventArgs e)
        {

        }
 

        private void 导出PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

       

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("Report Viewer: \r\n    " + this.printDoc.ErrorMessage);
                        this.printDoc = null;
                        return;
                    }
                }

                this.printDoc.Print();
                this.printDoc = null;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.rptViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                 ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void ssToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.rptViewer.CancelRendering(0);
        }

        private void 回退ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (this.rptViewer.LocalReport.IsDrillthroughReport)
                this.rptViewer.PerformBack();
        }

         

        
        private void toolExcel_Click(object sender, EventArgs e)
        {

        }

       
   
        private void toolFirst_Click(object sender, EventArgs e)
        {

        }

        private void toolJump_Click(object sender, EventArgs e)
        {

        }

        private void toolFirst_Click_1(object sender, EventArgs e)
        {

        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {

        }

        private void toolFirst_Click_2(object sender, EventArgs e)
        {

        }

        private void toolLast_Click_1(object sender, EventArgs e)
        {

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {

        }

#endregion

        private void chkShowValue_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void dtime_e_ValueChanged(object sender, EventArgs e)
        {

        }

        private void lblNote2_Click(object sender, EventArgs e)
        {

        }

        private void dtime_s_ValueChanged(object sender, EventArgs e)
        {

        }

        private void lblNote1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void toolStripButton3_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (this.printDoc == null)
                {
                    this.printDoc = new EMFStreamPrintDocument(this.rptViewer.LocalReport, this.m_ReportName);
                    if (this.printDoc.ErrorMessage != string.Empty)
                    {

                        this.printDoc = null;
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                        PageSettings frm = new PageSettings(this.m_ReportName);
                        frm.ShowDialog();
                        frm.Dispose();
                        return;
                    }
                }



                // this.PreviewDialog.Dispose();
                // this.printDoc = null;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("页面设置有误，请重新设置后重试");
                PageSettings frm = new PageSettings(this.m_ReportName);
                frm.ShowDialog();
                frm.Dispose();
            }
        }
        
         

      
       
    }
}
