﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.IO;
using ww.form.wwf.print;
using ww.wwf.wwfbll;
using ZedGraph;

namespace ww.form.lis.sam.qc
{
    public partial class QCL_JForm : ww.form.wwf.SysBaseForm
    {
        public QCL_JForm()
        {
            InitializeComponent();
        }

        private void QCL_JForm_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime dtime = System.DateTime.Now;
                string strtime = dtime.ToString("yyyy") + "-" + dtime.ToString("MM") + "-" + "01";
                dtime_s.Value = Convert.ToDateTime(strtime);
                zedGraphControl1.Visible = false;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.Message.ToString());
            }
        }
        /// <summary>
        /// 生成图型文件
        /// </summary>
        /// <param name="s"></param>
        public void getImage(string s)
        {
            Console.WriteLine(s);
            zedGraphControl1.GetImage().Save(s, System.Drawing.Imaging.ImageFormat.Jpeg);
            //ImageConverter ic = new ImageConverter();
            //byte[] bData = (byte[])ic.ConvertTo(zedGraphControl1.GetImage(), typeof(byte[]));
            //string s = Convert.ToBase64String(bData);
            //MessageBox.Show(s);
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            double dou靶值 = 10;//靶值
            double douSD值 = 1;//SD值
            Double[] dou项目值 = { 9.1, 8, 10, 12, 11,8.5};
            DateTime[] dtime项目值日期 = { Convert.ToDateTime("2013-08-01"), Convert.ToDateTime("2013-08-02"), Convert.ToDateTime("2013-08-03"), Convert.ToDateTime("2013-08-04"), Convert.ToDateTime("2013-08-05"), Convert.ToDateTime("2013-08-06") };
            Bll_L_J_Chart(dou项目值, dtime项目值日期, dou靶值, douSD值);
            getImage("taotao.jpg");            
        }
       
         /// <summary>
        /// L_J质控图
         /// </summary>
         /// <param name="dou项目值"></param>
         /// <param name="dtime项目值日期"></param>
         /// <param name="dou靶值"></param>
         /// <param name="douSD值"></param>
        private void Bll_L_J_Chart(Double[] dou项目值, DateTime[] dtime项目值日期, double dou靶值, double douSD值)
        {
            try
            {
                int cntDays = 1;
                int inumber2 = 1;
               
                zedGraphControl1.Dock = DockStyle.Fill;
                zedGraphControl1.Visible = true;
                zedGraphControl1.IsShowPointValues = true;
                inumber2 = dou项目值.Length;//4
                if (dtime项目值日期.Length <= 0)
                {
                    return;
                }

                //时间区间 比如2013-08-01 至 2013-08-06 为1----6
                DateTime dtime1;
                DateTime dtime2;               
                dtime1 = dtime_s.Value.Date;
                dtime2 = dtime_e.Value.Date;           
                double time1 = dtime1.ToOADate();                
                double time2 = dtime2.Date.AddDays(1).ToOADate();
                int itime1 = Convert.ToInt32(time1);
                int itime2 = Convert.ToInt32(time2);
                cntDays = itime2 - itime1;             
                
                double[] arrDaysSelected = new double[cntDays];
                arrDaysSelected[0] = time1;
                arrDaysSelected[cntDays - 1] = time1 + cntDays;            
               
                Double[] avg_l = new Double[cntDays];
                Double[] std_l1_1 = new Double[cntDays];
                Double[] std_l1_2 = new Double[cntDays];
                Double[] std_l2_1 = new Double[cntDays];
                Double[] std_l2_2 = new Double[cntDays];
                Double[] std_l3_1 = new Double[cntDays];
                Double[] std_l3_2 = new Double[cntDays];
                Double[] intd = new Double[cntDays];

                #region    绘图相关属性
                GraphPane myPane = zedGraphControl1.GraphPane;
                myPane.CurveList.Clear();
                myPane.GraphObjList.Clear();
                myPane.XAxis.Type = AxisType.Date;      //数据显示方式:按日期方式显示
                myPane.XAxis.Scale.Format = "dd";       //X轴的显示格式         
                myPane.Title.Text = "L_J质控图";
                myPane.Title.FontSpec.Size = 14;
                myPane.Title.IsVisible = false;
                myPane.Border.IsVisible = false;

                //myPane.XAxis.Title.Text = Convert.ToString(dtime1.Date.ToShortDateString() + "至" + dtime2.Date.ToShortDateString());
                myPane.XAxis.Title.IsVisible = false;
                myPane.YAxis.Title.Text = "";
                myPane.Margin.Left = 20;
                myPane.Chart.Fill = new Fill(Color.White);

                #region    相关数据的准备
                PointPairList list = new PointPairList();
                PointPairList lstAvg = new PointPairList();
                PointPairList lstSd1_1 = new PointPairList();
                PointPairList lstSd1_2 = new PointPairList();
                PointPairList lstSd2_1 = new PointPairList();
                PointPairList lstSd2_2 = new PointPairList();
                PointPairList lstSd3_1 = new PointPairList();
                PointPairList lstSd3_2 = new PointPairList();
                for (int i = 0; i < inumber2; i++)
                {
                    double x = (double)new XDate(dtime项目值日期[i].Date);
                    double y = dou项目值[i];
                    list.Add(x, y);
                }               
                for (int i = 0; i < cntDays; i++)
                {
                    avg_l[i] = dou靶值;// Sender.ItemProperty.FAvg;
                    std_l1_1[i] = dou靶值 + douSD值;
                    std_l1_2[i] = dou靶值 - douSD值;
                    std_l2_1[i] = dou靶值 + 2 * douSD值;
                    std_l2_2[i] = dou靶值 - 2 * douSD值;
                    std_l3_1[i] = dou靶值 + 3 * douSD值;
                    std_l3_2[i] = dou靶值 - 3 * douSD值;
                }
                lstAvg.Add(arrDaysSelected, avg_l);
                lstSd1_1.Add(arrDaysSelected, std_l1_1);
                lstSd1_2.Add(arrDaysSelected, std_l1_2);
                lstSd2_1.Add(arrDaysSelected, std_l2_1);
                lstSd2_2.Add(arrDaysSelected, std_l2_2);
                lstSd3_1.Add(arrDaysSelected, std_l3_1);
                lstSd3_2.Add(arrDaysSelected, std_l3_2);
                #endregion

                #region    在点旁标注值
                if (chkShowValue.Checked == true)
                {
                    for (int i = 0; i < dou项目值.Length; i++)
                    {
                        double x = (double)new XDate(dtime项目值日期[i].Date);
                        TextObj theText = new TextObj(dou项目值[i].ToString(), x + 0.01, dou项目值[i] + dou项目值[i] * 0.003, CoordType.AxisXY2Scale, AlignH.Left, AlignV.Center);
                        theText.FontSpec.FontColor = Color.Blue;
                        theText.ZOrder = ZOrder.A_InFront;
                        theText.FontSpec.Border.IsVisible = false;
                        theText.FontSpec.Fill.IsVisible = false;
                        theText.FontSpec.Size = 13;
                        myPane.GraphObjList.Add(theText);
                    }
                }
                #endregion
                
                LineItem myCurve = myPane.AddCurve("", list, Color.Green, SymbolType.Circle);
                myCurve.Line.IsVisible = true;
                myCurve.Symbol.Size = 5;
                myCurve.Symbol.Fill = new Fill(Color.Green);
                myCurve.IsY2Axis = true;
                myCurve.YAxisIndex = 1;

                #region    X轴的相关设置
                myPane.XAxis.Scale.MajorStep = 1;       //X轴的最大刻度单位
                myPane.XAxis.Scale.MinorStep = 1;       //X轴的最小刻度单位
                myPane.XAxis.MajorGrid.IsVisible = true;        //虚线网格是否显示
                myPane.XAxis.MajorGrid.DashOn = 2;
                myPane.XAxis.MajorGrid.DashOff = 2;
                myPane.XAxis.MajorGrid.PenWidth = 1.5f;
                myPane.XAxis.MajorGrid.Color = Color.LightGray;
                myPane.XAxis.Scale.IsSkipLastLabel = true;
                myPane.XAxis.Scale.BaseTic = time1;
                myPane.XAxis.Scale.Max = time2;
                myPane.XAxis.Scale.Min = time1 - 1;               
                #endregion

                #region    绘制AVG、SD、2SD、3SD、-SD、-2SD、-3SD线
                LineItem myCurve1 = myPane.AddCurve("", lstAvg, Color.Black, SymbolType.None);
                myCurve1.IsY2Axis = true;
                myCurve1.YAxisIndex = 1;

                LineItem myCurve2 = myPane.AddCurve("", lstSd1_1, Color.DarkGoldenrod, SymbolType.None);
                myCurve2.IsY2Axis = true;
                myCurve2.YAxisIndex = 1;

                LineItem myCurve3 = myPane.AddCurve("", lstSd1_2, Color.DarkGoldenrod, SymbolType.None);
                myCurve3.IsY2Axis = true;
                myCurve3.YAxisIndex = 1;

                LineItem myCurve4 = myPane.AddCurve("", lstSd2_1, Color.Red, SymbolType.None);
                myCurve4.IsY2Axis = true;
                myCurve4.YAxisIndex = 1;

                LineItem myCurve5 = myPane.AddCurve("", lstSd2_2, Color.Red, SymbolType.None);
                myCurve5.IsY2Axis = true;
                myCurve5.YAxisIndex = 1;

                LineItem myCurve6 = myPane.AddCurve("", lstSd3_1, Color.Blue, SymbolType.None);
                myCurve6.IsY2Axis = true;
                myCurve6.YAxisIndex = 1;

                LineItem myCurve7 = myPane.AddCurve("", lstSd3_2, Color.Blue, SymbolType.None);
                myCurve7.IsY2Axis = true;
                myCurve7.YAxisIndex = 1;
                #endregion

                #region    第一个Y轴坐标的相关设置
                myPane.YAxis.Scale.FontSpec.FontColor = Color.Red;
                myPane.YAxis.MajorTic.IsInside = false;
                myPane.YAxis.MajorTic.IsOutside = false;
                myPane.YAxis.MinorTic.IsInside = false;
                myPane.YAxis.MinorTic.IsOutside = false;
                myPane.YAxis.MajorTic.IsOpposite = false;
                myPane.YAxis.MinorTic.IsOpposite = false;
                myPane.YAxis.MajorGrid.IsZeroLine = false;
                myPane.YAxis.MajorGrid.IsVisible = false;        //虚线网格是否显示
                myPane.YAxis.Type = AxisType.Text;

                string[] ylabs = new string[8];
                ylabs[0] = "-3SD";
                ylabs[1] = "-2SD";
                ylabs[2] = "-1SD";
                ylabs[3] = "AVG";
                ylabs[4] = "+1SD";
                ylabs[5] = "+2SD";
                ylabs[6] = "+3SD";
                ylabs[7] = "";

                myPane.YAxis.Scale.Min = 0;
                myPane.YAxis.Scale.Max = 8;
                myPane.YAxis.Scale.TextLabels = ylabs;
                myPane.YAxis.Scale.Align = AlignP.Inside;
                #endregion

                #region    第二个Y轴坐标的相关设置
                myPane.Y2Axis.Scale.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.Title.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.IsVisible = true;
                myPane.Y2Axis.MajorTic.IsOpposite = false;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.Scale.Align = AlignP.Inside;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.Scale.Align = AlignP.Inside;
                myPane.Y2Axis.Scale.Min = dou靶值 - 4 * douSD值;
                myPane.Y2Axis.Scale.Max = dou靶值 + 4 * douSD值;
                myPane.Y2Axis.MajorGrid.IsZeroLine = false;
                myPane.Y2Axis.MajorGrid.Color = Color.LightGray;
                #endregion

                zedGraphControl1.AxisChange();
                zedGraphControl1.Refresh();
                #endregion

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }


    }
}
