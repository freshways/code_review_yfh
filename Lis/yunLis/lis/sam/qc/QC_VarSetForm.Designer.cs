﻿namespace yunLis.lis.sam.qc
{
    partial class QC_VarSetForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QC_VarSetForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.bindingSource参数 = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBox质控品 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtime_s = new System.Windows.Forms.DateTimePicker();
            this.comboBoxInstr = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonR = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSource项目 = new System.Windows.Forms.BindingSource(this.components);
            this.fzk_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fzkp_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fzkp_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fld = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fbz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fio_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource参数)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource项目)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.ColumnHeadersHeight = 21;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fzk_date,
            this.fzkp_id,
            this.fzkp_code,
            this.fld,
            this.fbz,
            this.fsd,
            this.fio_code});
            this.DataGridViewObject.DataSource = this.bindingSource参数;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(180, 62);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 35;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(638, 379);
            this.DataGridViewObject.TabIndex = 126;
            this.DataGridViewObject.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewObject_DataError);
            this.DataGridViewObject.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewObject_CellContentClick);
            // 
            // bindingSource参数
            // 
            this.bindingSource参数.PositionChanged += new System.EventHandler(this.bindingSource参数_PositionChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.comboBox质控品);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.dtime_s);
            this.panel2.Controls.Add(this.comboBoxInstr);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 30);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(818, 32);
            this.panel2.TabIndex = 125;
            // 
            // comboBox质控品
            // 
            this.comboBox质控品.FormattingEnabled = true;
            this.comboBox质控品.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboBox质控品.Location = new System.Drawing.Point(486, 4);
            this.comboBox质控品.Name = "comboBox质控品";
            this.comboBox质控品.Size = new System.Drawing.Size(62, 20);
            this.comboBox质控品.TabIndex = 174;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(434, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 173;
            this.label3.Text = "质控品";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 172;
            this.label2.Text = "起始日期";
            // 
            // dtime_s
            // 
            this.dtime_s.CustomFormat = "yyyy-MM-dd";
            this.dtime_s.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtime_s.Location = new System.Drawing.Point(338, 4);
            this.dtime_s.Name = "dtime_s";
            this.dtime_s.Size = new System.Drawing.Size(85, 21);
            this.dtime_s.TabIndex = 171;
            // 
            // comboBoxInstr
            // 
            this.comboBoxInstr.DisplayMember = "ShowName";
            this.comboBoxInstr.FormattingEnabled = true;
            this.comboBoxInstr.Location = new System.Drawing.Point(54, 4);
            this.comboBoxInstr.Name = "comboBoxInstr";
            this.comboBoxInstr.Size = new System.Drawing.Size(209, 20);
            this.comboBoxInstr.TabIndex = 1;
            this.comboBoxInstr.ValueMember = "finstr_id";
            this.comboBoxInstr.SelectedIndexChanged += new System.EventHandler(this.comboBoxInstr_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "仪器:";
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripButton1,
            this.toolStripSeparator1,
            this.toolStripButton2,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonS,
            this.toolStripSeparator3,
            this.toolStripButtonR});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(818, 30);
            this.bN.TabIndex = 127;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::yunLis.Properties.Resources.wwNavigatorAdd;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(76, 27);
            this.toolStripButton1.Text = "增加(&A)  ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::yunLis.Properties.Resources.wwNavigatorDelete;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(77, 27);
            this.toolStripButton2.Text = "删除(&D)  ";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(75, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonR
            // 
            this.toolStripButtonR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonR.Image")));
            this.toolStripButtonR.Name = "toolStripButtonR";
            this.toolStripButtonR.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonR.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonR.Size = new System.Drawing.Size(76, 27);
            this.toolStripButtonR.Text = "刷新(&R)  ";
            this.toolStripButtonR.Click += new System.EventHandler(this.toolStripButtonR_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 62);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 379);
            this.groupBox1.TabIndex = 167;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "质控项目";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeight = 21;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code,
            this.fname,
            this.fitem_id});
            this.dataGridView1.DataSource = this.bindingSource项目;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(3, 17);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 35;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(174, 359);
            this.dataGridView1.TabIndex = 166;
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "代号";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.Width = 50;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.Width = 120;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.Visible = false;
            // 
            // bindingSource项目
            // 
            this.bindingSource项目.PositionChanged += new System.EventHandler(this.bindingSource项目_PositionChanged);
            // 
            // fzk_date
            // 
            this.fzk_date.DataPropertyName = "fzk_date";
            dataGridViewCellStyle2.Format = "D";
            dataGridViewCellStyle2.NullValue = null;
            this.fzk_date.DefaultCellStyle = dataGridViewCellStyle2;
            this.fzk_date.HeaderText = "启用日期";
            this.fzk_date.Name = "fzk_date";
            this.fzk_date.Width = 90;
            // 
            // fzkp_id
            // 
            this.fzkp_id.DataPropertyName = "fzkp_id";
            this.fzkp_id.HeaderText = "质控品";
            this.fzkp_id.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.fzkp_id.Name = "fzkp_id";
            this.fzkp_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fzkp_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fzkp_id.Width = 80;
            // 
            // fzkp_code
            // 
            this.fzkp_code.DataPropertyName = "fzkp_code";
            this.fzkp_code.HeaderText = "批号";
            this.fzkp_code.Name = "fzkp_code";
            // 
            // fld
            // 
            this.fld.DataPropertyName = "fld";
            this.fld.HeaderText = "浓度";
            this.fld.Items.AddRange(new object[] {
            "高",
            "低",
            "中"});
            this.fld.Name = "fld";
            this.fld.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fld.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fld.Width = 80;
            // 
            // fbz
            // 
            this.fbz.DataPropertyName = "fbz";
            this.fbz.HeaderText = "靶值";
            this.fbz.Name = "fbz";
            // 
            // fsd
            // 
            this.fsd.DataPropertyName = "fsd";
            this.fsd.HeaderText = "SD";
            this.fsd.Name = "fsd";
            // 
            // fio_code
            // 
            this.fio_code.DataPropertyName = "fio_code";
            this.fio_code.HeaderText = "接口样本号";
            this.fio_code.Name = "fio_code";
            // 
            // QC_VarSetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 441);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.bN);
            this.Name = "QC_VarSetForm";
            this.Text = "质控参数设置";
            this.Load += new System.EventHandler(this.ItemSetForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource参数)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource项目)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBoxInstr;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        protected System.Windows.Forms.ToolStripButton toolStripButtonR;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.BindingSource bindingSource项目;
        private System.Windows.Forms.BindingSource bindingSource参数;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtime_s;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox质控品;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fzk_date;
        private System.Windows.Forms.DataGridViewComboBoxColumn fzkp_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fzkp_code;
        private System.Windows.Forms.DataGridViewComboBoxColumn fld;
        private System.Windows.Forms.DataGridViewTextBoxColumn fbz;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsd;
        private System.Windows.Forms.DataGridViewTextBoxColumn fio_code;
    }
}