namespace yunLis.lis.sam.jy
{
    partial class TJGzlInstrForm
    {
        #region  用户定义
        /// <summary>
        /// 报表路径
        /// </summary>
        public string ReportPath
        {
            /*
            get
            {
                return this.rptViewer.LocalReport.ReportPath;
            }
            set
            {
                this.rptViewer.LocalReport.ReportPath = value;
            }*/
            get
            {
                return this.rptViewer.LocalReport.ReportEmbeddedResource;
            }
            set
            {
                this.rptViewer.LocalReport.ReportEmbeddedResource = value;
            }
        }

        private string m_ReportName = string.Empty;
        /// <summary>
        /// 报表名称
        /// </summary>
        public string ReportName
        {
            get
            {
                return this.m_ReportName;
            }
            set
            {
                this.m_ReportName = value;
            }
        }

        /// <summary>
        /// DataSource of the Main Report
        /// </summary>
        private object m_MainDataSet = null;
        /// <summary>
        /// 报表数据集  主
        /// </summary>
        public object MainDataSet
        {
            get
            {
                return this.m_MainDataSet;
            }
            set
            {
                this.m_MainDataSet = value;
            }
        }

        /// <summary>
        /// DataSource of the DrillThrough Report
        /// </summary>
        private object m_DrillDataSet = null;
        /// <summary>
        /// 报表数据集  子
        /// </summary>
        public object DrillDataSet
        {
            get
            {
                return this.m_DrillDataSet;
            }
            set
            {
                this.m_DrillDataSet = value;
            }
        }
        /// <summary>
        /// Data Source Name of the Main Report
        /// </summary>
        private string m_MainDataSourceName = string.Empty;
        /// <summary>
        /// 主数据源名
        /// </summary>
        public string MainDataSourceName
        {
            get
            {
                return this.m_MainDataSourceName;
            }
            set
            {
                this.m_MainDataSourceName = value;
            }
        }

        /// <summary>
        /// Data Source Name of the DrillThrough Report
        /// </summary>
        private string m_DrillDataSourceName = string.Empty;
        /// <summary>
        /// 子数据源名
        /// </summary>
        public string DrillDataSourceName
        {
            get
            {
                return this.m_DrillDataSourceName;
            }
            set
            {
                this.m_DrillDataSourceName = value;
            }
        }



        /// <summary>
        /// Data Source Name of the Main Report
        /// </summary>
        private string m_MainDataSourceName2 = string.Empty;
        public string MainDataSourceName2
        {
            get
            {
                return this.m_MainDataSourceName2;
            }
            set
            {
                this.m_MainDataSourceName2 = value;
            }
        }
        #endregion 
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fhz_type_idLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label finstr_idLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TJGzlInstrForm));
            this.dnReport = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolExport = new System.Windows.Forms.ToolStripSplitButton();
            this.toolExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.导出PDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tspbZoom = new System.Windows.Forms.ToolStripSplitButton();
            this.tool25 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool50 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool100 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool200 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool400 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolWhole = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPageWidth = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.rptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.PreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.ftype_idComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.fjy_instrComboBox = new System.Windows.Forms.ComboBox();
            fhz_type_idLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            finstr_idLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dnReport)).BeginInit();
            this.dnReport.SuspendLayout();
            this.groupBoxif.SuspendLayout();
            this.SuspendLayout();
            // 
            // fhz_type_idLabel
            // 
            fhz_type_idLabel.AutoSize = true;
            fhz_type_idLabel.Location = new System.Drawing.Point(478, 16);
            fhz_type_idLabel.Name = "fhz_type_idLabel";
            fhz_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fhz_type_idLabel.TabIndex = 19;
            fhz_type_idLabel.Text = "病人类别:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(12, 16);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // finstr_idLabel
            // 
            finstr_idLabel.AutoSize = true;
            finstr_idLabel.Location = new System.Drawing.Point(296, 16);
            finstr_idLabel.Name = "finstr_idLabel";
            finstr_idLabel.Size = new System.Drawing.Size(35, 12);
            finstr_idLabel.TabIndex = 10;
            finstr_idLabel.Text = "仪器:";
            // 
            // dnReport
            // 
            this.dnReport.AddNewItem = null;
            this.dnReport.AutoSize = false;
            this.dnReport.CountItem = null;
            this.dnReport.DeleteItem = null;
            this.dnReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonQuery,
            this.toolStripSeparator1,
            this.toolStripButton2,
            this.toolStripButton4,
            this.toolExport,
            this.tspbZoom,
            this.toolStripSeparator5});
            this.dnReport.Location = new System.Drawing.Point(0, 0);
            this.dnReport.MoveFirstItem = null;
            this.dnReport.MoveLastItem = null;
            this.dnReport.MoveNextItem = null;
            this.dnReport.MovePreviousItem = null;
            this.dnReport.Name = "dnReport";
            this.dnReport.PositionItem = null;
            this.dnReport.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.dnReport.Size = new System.Drawing.Size(792, 35);
            this.dnReport.TabIndex = 0;
            this.dnReport.Text = "bindingNavigator1";
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQuery.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQuery.Size = new System.Drawing.Size(77, 32);
            this.toolStripButtonQuery.Text = "查询(F5) ";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::yunLis.Properties.Resources.config1;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(76, 32);
            this.toolStripButton2.Text = "打印设置";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::yunLis.Properties.Resources.button_print1;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(60, 32);
            this.toolStripButton4.Text = "打 印 ";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolExport
            // 
            this.toolExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolExcel,
            this.导出PDFToolStripMenuItem});
            this.toolExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolExport.Name = "toolExport";
            this.toolExport.Size = new System.Drawing.Size(56, 32);
            this.toolExport.Text = "导 出 ";
            // 
            // toolExcel
            // 
            this.toolExcel.Name = "toolExcel";
            this.toolExcel.Size = new System.Drawing.Size(129, 22);
            this.toolExcel.Text = "导出Excel";
            this.toolExcel.Click += new System.EventHandler(this.toolExcel_Click);
            // 
            // 导出PDFToolStripMenuItem
            // 
            this.导出PDFToolStripMenuItem.Name = "导出PDFToolStripMenuItem";
            this.导出PDFToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.导出PDFToolStripMenuItem.Text = "导出PDF";
            this.导出PDFToolStripMenuItem.Click += new System.EventHandler(this.导出PDFToolStripMenuItem_Click);
            // 
            // tspbZoom
            // 
            this.tspbZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool25,
            this.tool50,
            this.tool100,
            this.tool200,
            this.tool400,
            this.toolWhole,
            this.toolPageWidth});
            this.tspbZoom.Image = ((System.Drawing.Image)(resources.GetObject("tspbZoom.Image")));
            this.tspbZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tspbZoom.Name = "tspbZoom";
            this.tspbZoom.Size = new System.Drawing.Size(92, 32);
            this.tspbZoom.Text = "显示比例 ";
            // 
            // tool25
            // 
            this.tool25.Name = "tool25";
            this.tool25.Size = new System.Drawing.Size(108, 22);
            this.tool25.Text = "25%";
            this.tool25.Click += new System.EventHandler(this.tool25_Click);
            // 
            // tool50
            // 
            this.tool50.Name = "tool50";
            this.tool50.Size = new System.Drawing.Size(108, 22);
            this.tool50.Text = "50%";
            this.tool50.Click += new System.EventHandler(this.tool50_Click);
            // 
            // tool100
            // 
            this.tool100.Name = "tool100";
            this.tool100.Size = new System.Drawing.Size(108, 22);
            this.tool100.Text = "100%";
            this.tool100.Click += new System.EventHandler(this.tool100_Click);
            // 
            // tool200
            // 
            this.tool200.Name = "tool200";
            this.tool200.Size = new System.Drawing.Size(108, 22);
            this.tool200.Text = "200%";
            this.tool200.Click += new System.EventHandler(this.tool200_Click);
            // 
            // tool400
            // 
            this.tool400.Name = "tool400";
            this.tool400.Size = new System.Drawing.Size(108, 22);
            this.tool400.Text = "400%";
            this.tool400.Click += new System.EventHandler(this.tool400_Click);
            // 
            // toolWhole
            // 
            this.toolWhole.Name = "toolWhole";
            this.toolWhole.Size = new System.Drawing.Size(108, 22);
            this.toolWhole.Text = "整页";
            this.toolWhole.Click += new System.EventHandler(this.toolWhole_Click);
            // 
            // toolPageWidth
            // 
            this.toolPageWidth.Name = "toolPageWidth";
            this.toolPageWidth.Size = new System.Drawing.Size(108, 22);
            this.toolPageWidth.Text = "页宽";
            this.toolPageWidth.Click += new System.EventHandler(this.toolPageWidth_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 35);
            // 
            // rptViewer
            // 
            this.rptViewer.AutoScroll = true;
            this.rptViewer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.rptViewer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptViewer.DocumentMapCollapsed = true;
            this.rptViewer.IsDocumentMapWidthFixed = true;
            this.rptViewer.LocalReport.ReportEmbeddedResource = "RDLCPrint.rptWuLiao.rdlc";
            this.rptViewer.Location = new System.Drawing.Point(0, 80);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.ShowToolBar = false;
            this.rptViewer.Size = new System.Drawing.Size(792, 426);
            this.rptViewer.TabIndex = 1;
            this.rptViewer.Drillthrough += new Microsoft.Reporting.WinForms.DrillthroughEventHandler(this.rptViewer_Drillthrough);
            // 
            // PreviewDialog
            // 
            this.PreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.PreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.PreviewDialog.ClientSize = new System.Drawing.Size(396, 296);
            this.PreviewDialog.Enabled = true;
            this.PreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("PreviewDialog.Icon")));
            this.PreviewDialog.Name = "printPreviewDialog1";
            this.PreviewDialog.ShowIcon = false;
            this.PreviewDialog.Visible = false;
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(this.ftype_idComboBox);
            this.groupBoxif.Controls.Add(fhz_type_idLabel);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker2);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker1);
            this.groupBoxif.Controls.Add(fjy_dateLabel);
            this.groupBoxif.Controls.Add(this.fjy_instrComboBox);
            this.groupBoxif.Controls.Add(finstr_idLabel);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 35);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Size = new System.Drawing.Size(792, 45);
            this.groupBoxif.TabIndex = 146;
            this.groupBoxif.TabStop = false;
            // 
            // ftype_idComboBox
            // 
            this.ftype_idComboBox.DisplayMember = "fname";
            this.ftype_idComboBox.FormattingEnabled = true;
            this.ftype_idComboBox.Items.AddRange(new object[] {
            ""});
            this.ftype_idComboBox.Location = new System.Drawing.Point(542, 12);
            this.ftype_idComboBox.Name = "ftype_idComboBox";
            this.ftype_idComboBox.Size = new System.Drawing.Size(87, 20);
            this.ftype_idComboBox.TabIndex = 18;
            this.ftype_idComboBox.ValueMember = "fcode";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(178, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(194, 12);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(97, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 13;
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(76, 12);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(97, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 11;
            // 
            // fjy_instrComboBox
            // 
            this.fjy_instrComboBox.DisplayMember = "ShowName";
            this.fjy_instrComboBox.FormattingEnabled = true;
            this.fjy_instrComboBox.Items.AddRange(new object[] {
            ""});
            this.fjy_instrComboBox.Location = new System.Drawing.Point(336, 12);
            this.fjy_instrComboBox.Name = "fjy_instrComboBox";
            this.fjy_instrComboBox.Size = new System.Drawing.Size(137, 20);
            this.fjy_instrComboBox.TabIndex = 9;
            this.fjy_instrComboBox.ValueMember = "finstr_id";
            // 
            // TJGzlInstrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 506);
            this.Controls.Add(this.rptViewer);
            this.Controls.Add(this.groupBoxif);
            this.Controls.Add(this.dnReport);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TJGzlInstrForm";
            this.ShowIcon = false;
            this.Text = "仪器工作量统计";
            this.Load += new System.EventHandler(this.ReportViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dnReport)).EndInit();
            this.dnReport.ResumeLayout(false);
            this.dnReport.PerformLayout();
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingNavigator dnReport;
        private Microsoft.Reporting.WinForms.ReportViewer rptViewer;
        private System.Windows.Forms.PrintPreviewDialog PreviewDialog;
        private System.Windows.Forms.ToolStripSplitButton toolExport;
        private System.Windows.Forms.ToolStripMenuItem toolExcel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSplitButton tspbZoom;
        private System.Windows.Forms.ToolStripMenuItem tool25;
        private System.Windows.Forms.ToolStripMenuItem tool50;
        private System.Windows.Forms.ToolStripMenuItem tool100;
        private System.Windows.Forms.ToolStripMenuItem tool200;
        private System.Windows.Forms.ToolStripMenuItem tool400;
        private System.Windows.Forms.ToolStripMenuItem toolWhole;
        private System.Windows.Forms.ToolStripMenuItem toolPageWidth;
        private System.Windows.Forms.ToolStripMenuItem 导出PDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.ComboBox ftype_idComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox fjy_instrComboBox;

    }
}