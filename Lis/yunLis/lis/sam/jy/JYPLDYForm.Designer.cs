﻿namespace yunLis.lis.sam.jy
{
    partial class JYPLDYForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox设备型号 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn取消 = new System.Windows.Forms.Button();
            this.btn确定 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxEnd样本号 = new System.Windows.Forms.TextBox();
            this.textBoxBegin样本号 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker样本日期 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox提示信息 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.comboBox设备型号);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btn取消);
            this.panel1.Controls.Add(this.btn确定);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxEnd样本号);
            this.panel1.Controls.Add(this.textBoxBegin样本号);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dateTimePicker样本日期);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(443, 163);
            this.panel1.TabIndex = 0;
            // 
            // comboBox设备型号
            // 
            this.comboBox设备型号.DisplayMember = "fname";
            this.comboBox设备型号.FormattingEnabled = true;
            this.comboBox设备型号.Location = new System.Drawing.Point(127, 16);
            this.comboBox设备型号.Name = "comboBox设备型号";
            this.comboBox设备型号.Size = new System.Drawing.Size(203, 20);
            this.comboBox设备型号.TabIndex = 1;
            this.comboBox设备型号.ValueMember = "finstr_id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(47, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 10;
            this.label4.Text = "设备型号：";
            // 
            // btn取消
            // 
            this.btn取消.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn取消.Image = global::yunLis.Properties.Resources.wwNavigatorDelete;
            this.btn取消.Location = new System.Drawing.Point(335, 132);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(75, 23);
            this.btn取消.TabIndex = 6;
            this.btn取消.Text = "取消";
            this.btn取消.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn取消.UseVisualStyleBackColor = true;
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // btn确定
            // 
            this.btn确定.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn确定.Image = global::yunLis.Properties.Resources.button_tj;
            this.btn确定.Location = new System.Drawing.Point(239, 132);
            this.btn确定.Name = "btn确定";
            this.btn确定.Size = new System.Drawing.Size(75, 23);
            this.btn确定.TabIndex = 5;
            this.btn确定.Text = "确定";
            this.btn确定.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn确定.UseVisualStyleBackColor = true;
            this.btn确定.Click += new System.EventHandler(this.btn确定_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(233, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "--";
            // 
            // textBoxEnd样本号
            // 
            this.textBoxEnd样本号.Location = new System.Drawing.Point(260, 91);
            this.textBoxEnd样本号.Name = "textBoxEnd样本号";
            this.textBoxEnd样本号.Size = new System.Drawing.Size(100, 21);
            this.textBoxEnd样本号.TabIndex = 4;
            this.textBoxEnd样本号.Text = "1";
            this.textBoxEnd样本号.Enter += new System.EventHandler(this.SelectAll);
            this.textBoxEnd样本号.MouseHover += new System.EventHandler(this.SelectAll);
            // 
            // textBoxBegin样本号
            // 
            this.textBoxBegin样本号.Location = new System.Drawing.Point(127, 91);
            this.textBoxBegin样本号.Name = "textBoxBegin样本号";
            this.textBoxBegin样本号.Size = new System.Drawing.Size(100, 21);
            this.textBoxBegin样本号.TabIndex = 3;
            this.textBoxBegin样本号.Text = "1";
            this.textBoxBegin样本号.Enter += new System.EventHandler(this.SelectAll);
            this.textBoxBegin样本号.MouseHover += new System.EventHandler(this.SelectAll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(47, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "样 本 号：";
            // 
            // dateTimePicker样本日期
            // 
            this.dateTimePicker样本日期.Location = new System.Drawing.Point(130, 53);
            this.dateTimePicker样本日期.Name = "dateTimePicker样本日期";
            this.dateTimePicker样本日期.Size = new System.Drawing.Size(200, 21);
            this.dateTimePicker样本日期.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(47, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "样本日期：";
            // 
            // textBox提示信息
            // 
            this.textBox提示信息.Location = new System.Drawing.Point(12, 181);
            this.textBox提示信息.Name = "textBox提示信息";
            this.textBox提示信息.ReadOnly = true;
            this.textBox提示信息.Size = new System.Drawing.Size(443, 21);
            this.textBox提示信息.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(221, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "批量打印只对已经审核通过的样本有效。";
            // 
            // JYPLDYForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 214);
            this.Controls.Add(this.textBox提示信息);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "JYPLDYForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "批量打印";
            this.Load += new System.EventHandler(this.JYPLDYForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn取消;
        private System.Windows.Forms.Button btn确定;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxEnd样本号;
        private System.Windows.Forms.TextBox textBoxBegin样本号;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker样本日期;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox提示信息;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox设备型号;
        private System.Windows.Forms.Label label5;

    }
}