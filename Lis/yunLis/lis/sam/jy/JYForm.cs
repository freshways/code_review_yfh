using HIS.COMM;
using HIS.Model;
using HIS.Model.Pojo;
using HIS.Model.Pojo.LIS;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WEISHENG.COMM;
using WEISHENG.COMM.PluginsAttribute;
using ww.wwf.com;
using yunLis.dao;
using yunLis.lisbll.sam;
using yunLis.Pojo;
using yunLis.wwfbll;

namespace yunLis.lis.sam.jy
{
    [ClassInfoMark(GUID = "12E7BE7C-8D4A-4F4B-8705-A965D124C273", 键ID = "12E7BE7C-8D4A-4F4B-8705-A965D124C273", 父ID = "BE22B406-5548-4F27-9F9D-4582D583E701",
        功能名称 = "报告录入", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.jy.JYForm",
        传递参数 = "", 显示顺序 = 0,
        菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public partial class JYForm : DevExpress.XtraEditors.XtraForm// yunLis.wwf.SysBaseForm
    {
        LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString));

        jybll blljy = new jybll();
        /// <summary>
        /// 当前样本 主表
        /// </summary>
        HIS.Model.SAM_JY sam_jy = new HIS.Model.SAM_JY();
        TypeBLL bllType = new TypeBLL();
        DeptBll bllDept = new DeptBll();
        PersonBLL bllPerson = new PersonBLL();
        DiseaseBLL bllDisease = new DiseaseBLL();
        PatientBLL bllPatient = new PatientBLL();
        SampleTypeBLL bllSampleType = new SampleTypeBLL();


        List<PojoSam_Jy_Result> dt_SAM_JY_RESULT = new List<PojoSam_Jy_Result>();
        List<SAM_ITEM> listSam_item = new List<SAM_ITEM>();
        private bool print = false;
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString));

        List<HIS.Model.pubUser> list送检医师 = new List<HIS.Model.pubUser>();
        List<HIS.Model.pubUser> list检验医师 = new List<HIS.Model.pubUser>();
        List<GY科室设置> list科室 = new List<GY科室设置>();

        bool b扫码计费 = false;
        string str仪器结果FTaskID = "";



        Pojo报告单摘要 row当前样本 = null;
        string s当前样本Id = "";  /// 当前选择的样的本id

        //取电子健康卡支持
        private ScanerHook listener = new ScanerHook();
        public JYForm()
        {
            InitializeComponent();
            listener.ScanerEvent += Listener_ScanerEvent;
        }
        private void Listener_ScanerEvent(ScanerHook.ScanerCodes codes)
        {
            string code = codes.Result.Replace(';', ':');
            if (!string.IsNullOrEmpty(code))
            {
                ExecuteReq(code);
            }
        }
        void ExecuteReq(string cardno)
        {
            Ylzehc.Ylzehc.ApiUrl = "http://192.168.10.171:1811/ehcService/gateway.do";   // 可按系统配置赋值
            Ylzehc.Ylzehc.AppKey = "1DOB630TT0510100007F0000D6D2E81F";   // 可按系统配置赋值

            // 请求头
            JObject request = new JObject();
            request["app_id"] = "1DOB630TT0500100007F0000CC065635";   // 可按系统配置赋值
            request["term_id"] = "371300210001";   // 可按系统配置赋值
            request["method"] = "ehc.ehealthcode.verify"; // *** 根据具体调用接口入参 *** 
            request["timestamp"] = DateTime.Now.ToString("yyyyMMddHHmmss");   // *** 根据具体调用时间入参 *** 
            request["sign_type"] = "MD5";   // 固定，支持MD5\SM3，SDK内部会自动根据该算法计算sign
            request["version"] = "X.M.0.1";   // 固定
            request["enc_type"] = "AES";    //  固定，支持AES\SM4，SDK内部会自动根据该算法加密

            // 业务参数

            // **********根据接口文档入参，当前仅为二维码验证示例**********//
            JObject bizParam = new JObject();
            bizParam["ehealth_code"] = cardno;//"3C7600201CD41759A266852EB9FF24A5B486290A778E31ED68EA25DEC1D370F4:1::3502A0001:";
            bizParam["out_verify_time"] = DateTime.Now.ToString("yyyyMMddHHmmss");
            bizParam["out_verify_no"] = System.Guid.NewGuid().ToString("N"); // 唯一编号
            bizParam["operator_id"] = "001";
            bizParam["operator_name"] = "测试";
            bizParam["treatment_code"] = "010101";
            // **********根据接口文档入参，当前仅为二维码验证示例**********//

            request["biz_content"] = JsonConvert.SerializeObject(bizParam);

            JObject res = Ylzehc.Ylzehc.Execute(request);
            if (res["ret_code"].ToString().Equals("0000"))
            {
                JObject user = JObject.Parse(res["biz_content"].ToString());
                this.txt姓名.Text = user["user_name"].ToString();
                if (sam_jy != null)
                {
                    sam_jy.sfzh = user["id_no"].ToString();
                    this.txt身份证号.Text = user["id_no"].ToString();
                    sam_jy.f1 = user["card_no"].ToString();
                    this.txt电子健康卡.Text = user["card_no"].ToString();
                }
                //this.txt_手机号.Text = user["mobile_phone"].ToString();
                //this.textBoxSFZH.Text = user["id_no"].ToString();
                //MessageBox.Show("电子健康卡读取成功！\n\r[请求参数：]" + bizParam.ToString());
                listener.Stop();
            }
        }




        private void JYForm_Load(object sender, EventArgs e)
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                list科室 = chis.GY科室设置.Where(c => c.分院编码 == HIS.COMM.zdInfo.Model站点信息.分院编码 && c.是否住院科室 == true).ToList();
                list送检医师 = chis.pubUsers.Where(c => c.是否有处方权 == true && c.是否禁用 == false).OrderBy(c => c.用户名).ToList();

                int i科室编码 = Convert.ToInt32(HIS.COMM.zdInfo.ModelUserInfo.科室编码);
                var item科室 = chis.GY科室设置.Where(c => c.科室编码 == i科室编码).FirstOrDefault();
                if (item科室.科室类型 != "LIS科室")
                {
                    msgBalloonHelper.ShowInformation($"您登录的用户所在科室{item科室.科室名称}不是LIS科室");
                    return;
                }
                list检验医师 = chis.pubUsers.Where(c => c.科室编码 == i科室编码.ToString() && c.是否禁用 == false).OrderBy(c => c.用户名).ToList();
            }
            b扫码计费 = HIS.COMM.Helper.EnvironmentHelper.CommBoolGet("LIS扫码计费", "true", "");
            button读卡.Visible = LoginBLL.sendauto;

            try
            {
                dataGridView样本.AutoGenerateColumns = false;
                dataGrid_SAM_JY_Result.AutoGenerateColumns = false;
                dataGridView比较.AutoGenerateColumns = false;
                comboBox过滤样本_病人类别.SelectedItem = "所有";
                //del by wjz 20160308 画面第一次加载时,修改样本号，样本信息不变更 ▽
                //fjy_yb_code样本的Enter事件触发时，tag中会记录当前样本号，此时执行下面语句，tag中记录的样本号为1，而不是对应样本的样本号，这种情况容易引起bug.故将此句下移
                //this.ActiveControl = fjy_yb_code样本;
                //del by wjz 20160308 画面第一次加载时,修改样本号，样本信息不变更 △
                InitSexComboBox();
                Init病人类型();
                Init年龄单位();
                Init送检科室Dept();
                this.cmb送检医师.Properties.DataSource = list送检医师;
                this.cmb检验医师.Properties.DataSource = list检验医师;
                this.cmb核对医师.Properties.DataSource = list检验医师;
                this.cmb样本类型.DataSource = lisEntities.SAM_SAMPLE_TYPE.Where(c => c.fuse_if == 1).ToList();
                this.cmb费别.DataSource = lisEntities.SAM_TYPE.Where(c => c.ftype == "费别").OrderBy(c => c.forder_by).ToList();
                bllInit初始化();
                //del by wjz 20160128 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 ▽
                //方法（bllInit初始化）会调用事件（fjy_instrComboBox_仪器_SelectedIndexChanged）
                //fjy_instrComboBox_仪器_SelectedIndexChanged 里又调用了 bllYBList方法，此处的调用重复，故删除
                //bllYBList();
                //del by wjz 20160128 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 △
                this.ActiveControl = txt样本号;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void bll比较()
        {
            try
            {

                List<Pojo比较值> dtBJValue = new List<Pojo比较值>();

                if (txt门诊或住院号.Text == "")
                {
                    this.上一次.HeaderText = "上一次";
                    this.上二次.HeaderText = "上二次";
                    this.上三次.HeaderText = "上三次";
                }
                else
                {
                    string strWhere = " (住院号 = '" + txt门诊或住院号.Text + "') AND (化验设备ID = '" + cmb检验仪器.SelectedValue.ToString() + "') AND (检验日期 <= '" + dtp样本日期.Value.ToString("yyyy-MM-dd") + "') ";
                    string sqlOrderby = "fjy_date DESC";
                    var dtBJ = Bll.DataHelper.getList报告摘要(strWhere, sqlOrderby);
                    int intBJCount = dtBJ.Count;
                    dtBJValue = Bll.DataHelper.GetList比较值(dtBJ[0].fjy_id);
                    this.最近.HeaderText = dtBJ[0].fjy_date + "[" + dtBJ[0].fjy_yb_code + "]";
                    this.col比较_最近.Caption = dtBJ[0].fjy_date + "[" + dtBJ[0].fjy_yb_code + "]";
                    if (intBJCount == 1)
                    {
                        this.上一次.HeaderText = "上一次";
                        this.上二次.HeaderText = "上二次";
                        this.上三次.HeaderText = "上三次";
                        this.col比较_上一次.Caption = "上一次";
                        this.col比较_上二次.Caption = "上二次";
                        this.col比较_上三次.Caption = "上三次";
                    }
                    else if (intBJCount == 2)
                    {
                        this.上一次.HeaderText = dtBJ[1].fjy_date + "[" + dtBJ[1].fjy_yb_code + "]";
                        this.上二次.HeaderText = "上二次";
                        this.上三次.HeaderText = "上三次";
                        this.col比较_上一次.Caption = dtBJ[1].fjy_date + "[" + dtBJ[1].fjy_yb_code + "]";
                        this.col比较_上二次.Caption = "上二次";
                        this.col比较_上三次.Caption = "上三次";

                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ[1].fjy_id, "上一次");

                    }

                    else if (intBJCount == 3)
                    {
                        this.上一次.HeaderText = dtBJ[1].fjy_date + "[" + dtBJ[1].fjy_yb_code + "]";
                        this.上二次.HeaderText = dtBJ[2].fjy_date + "[" + dtBJ[2].fjy_yb_code + "]";
                        this.上三次.HeaderText = "上三次";

                        this.col比较_上一次.Caption = dtBJ[1].fjy_date + "[" + dtBJ[1].fjy_yb_code + "]";
                        this.col比较_上二次.Caption = dtBJ[2].fjy_date + "[" + dtBJ[2].fjy_yb_code + "]";
                        this.col比较_上三次.Caption = "上三次";

                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ[1].fjy_id, "上一次");
                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ[2].fjy_id, "上二次");

                    }
                    else if (intBJCount == 4)
                    {
                        this.上一次.HeaderText = dtBJ[1].fjy_date + "[" + dtBJ[1].fjy_yb_code + "]";
                        this.上二次.HeaderText = dtBJ[2].fjy_date + "[" + dtBJ[2].fjy_yb_code + "]";
                        this.上三次.HeaderText = dtBJ[3].fjy_date + "[" + dtBJ[3].fjy_yb_code + "]";

                        this.col比较_上一次.Caption = dtBJ[1].fjy_date + "[" + dtBJ[1].fjy_yb_code + "]";
                        this.col比较_上二次.Caption = dtBJ[2].fjy_date + "[" + dtBJ[2].fjy_yb_code + "]";
                        this.col比较_上三次.Caption = dtBJ[3].fjy_date + "[" + dtBJ[3].fjy_yb_code + "]";

                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ[1].fjy_id, "上一次");
                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ[2].fjy_id, "上二次");
                        dtBJValue = bll比较设置返回(dtBJValue, dtBJ[3].fjy_id, "上三次");
                    }
                    else
                    {
                        this.上一次.HeaderText = "上一次";
                        this.上二次.HeaderText = "上二次";
                        this.上三次.HeaderText = "上三次";
                        this.col比较_上一次.Caption = "上一次";
                        this.col比较_上二次.Caption = "上二次";
                        this.col比较_上三次.Caption = "上三次";
                    }
                }

                dataGridView比较.DataSource = dtBJValue;
                gridControl比较.DataSource = dtBJValue;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private List<Pojo比较值> bll比较设置返回(List<Pojo比较值> dtBJValue, string strfjy_id, string str第几次)
        {
            //   DataTable dt1 = bll比较值(dtBJ.Rows[1]["fjy_id"].ToString());
            var dt1 = Bll.DataHelper.GetList比较值(strfjy_id);
            foreach (var item in dtBJValue)
            {
                string stritemid = item.项目id;
                var rows = dt1.Where(c => c.项目id == stritemid).ToList();
                if (rows.Count > 0)
                {
                    foreach (var dr in rows)
                    {
                        switch (str第几次)
                        {
                            default:
                            case "上一次":
                                item.上一次 = dr.项目值;
                                break;
                            case "上二次":
                                item.上二次 = dr.项目值;
                                break;
                            case "上三次":
                                item.上三次 = dr.项目值;
                                break;
                        }

                    }

                }
            }

            return dtBJValue;
        }




        /// <summary>
        /// 刷新检验结果SAM_JY_RESULT，如果未审核，从设备结果Ins_Result_Detail刷新，如果已审核，则显示审核后固定下来的结果集
        /// </summary>
        private void refresh_SAM_JY_RESULT_if_need_from_Ins_Result_Detail()
        {
            string strWhereResult = " (t.fjy_id = '" + sam_jy.fjy_id + "') ";
            dt_SAM_JY_RESULT = Bll.DataHelper.Get_SAM_JY_RESULT(strWhereResult);
            //当单据未审核时，比对结果，没有的进行添加或更新值
            if (label样本审核状态.Text == "未审核")
            {
                Ins_Result_Detail_to_dt_SAM_JY_RESULT();
            }
            bllResultShow显示结果颜色();
        }

        /// <summary>
        /// 结果列表显示
        /// </summary>       
        private void bllResultShow显示结果颜色()
        {
            try
            {

                #region 给列设置值
                //2015-05-27 14:47:34 yufh注释：不用每次都计算参考值，保存时计算
                string _instr_id = cmb检验仪器.SelectedValue.ToString();
                List<SAM_ITEM> dtItem = lis.SAM_ITEM.Where(c => c.finstr_id == _instr_id).ToList();
                foreach (var item in dt_SAM_JY_RESULT)
                {
                    string strItemId = item.fitem_id;
                    string strfvalue = item.fvalue;
                    var dr = dtItem.Where(c => c.fitem_id == strItemId).FirstOrDefault();
                    if (dr != null)
                    {

                        item.fitem_code = dr.fitem_code;
                        item.fitem_name = dr.fname;
                        item.fitem_unit = dr.funit_name;
                        item.forder_by = dr.fprint_num;

                        string str参考值 = GetRefValueByItemCode(strItemId);

                        item.fitem_ref = str参考值;


                        string str危急值 = GetJGRefValueByItemCode(strItemId);
                        item.fitem_jgref = str危急值;

                        if (strfvalue.Equals("") || strfvalue.Length == 0 || strfvalue == null) { }
                        else
                        {
                            item.fitem_badge = this.blljy.Bll结果标记(strfvalue, str参考值, str危急值);
                        }
                    }
                    else
                    {
                        item.fitem_code = "";
                        item.fitem_name = "";
                        item.fitem_unit = "";
                        item.forder_by = "0";
                        item.fitem_ref = "";
                        item.fitem_badge = "";
                    }
                }
                for (int i = 0; i < dt_SAM_JY_RESULT.Count(); i++)
                {

                }
                #endregion

                dataGrid_SAM_JY_Result.DataSource = dt_SAM_JY_RESULT;
                gridControl化验结果.DataSource = dt_SAM_JY_RESULT;
                gridView化验结果.PopulateColumns();
                #region 根据标识显示颜色
                string warning = string.Empty;
                if (this.dataGrid_SAM_JY_Result.Rows.Count > 0)
                {
                    for (int intGrid = 0; intGrid < this.dataGrid_SAM_JY_Result.Rows.Count; intGrid++)
                    {
                        string strBJ = this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_badge"].Value.ToString();

                        if (strBJ.Equals("↑"))
                        {
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fvalue"].Style.ForeColor = Color.Red;
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_badge"].Style.ForeColor = Color.Red;
                        }
                        else if (strBJ.Equals("↓"))
                        {
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fvalue"].Style.ForeColor = Color.Blue;
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_badge"].Style.ForeColor = Color.Blue;
                        }
                        else if (strBJ.Equals("↑↑"))
                        {
                            string fitem_name = this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_name"].Value.ToString();
                            string fvalue = this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fvalue"].Value.ToString();
                            warning += $"[{fitem_name}]:[{fvalue}]";
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_badge"].Style.BackColor = Color.Red;
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_jgref"].Style.ForeColor = Color.Wheat;
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_jgref"].Style.BackColor = Color.Red;
                        }
                        else if (strBJ.Equals("↓↓"))
                        {
                            string fitem_name = this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_name"].Value.ToString();
                            string fvalue = this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fvalue"].Value.ToString();
                            warning += $"[{fitem_name}]:[{fvalue}]";
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_badge"].Style.BackColor = Color.Blue;
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_jgref"].Style.ForeColor = Color.Wheat;
                            this.dataGrid_SAM_JY_Result.Rows[intGrid].Cells["fitem_jgref"].Style.BackColor = Color.Red;
                        }
                    }
                    if (label样本审核状态.Text == "未审核" && !string.IsNullOrEmpty(warning))
                    {
                        MessageBox.Show("项目：" + warning + "超危急值！请注意及时处理！", "危急值提醒", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 保存结果明细 ，先删除，再新增。
        /// </summary>
        /// <param name="lisResultSql"></param>
        /// <param name="str主表id"></param>
        /// <returns></returns>
        private IList make先删后加Sql_SAM_JY_RESULT(IList lisResultSql, string str主表id)
        {
            //子表保存             
            this.Validate();
            dataGrid_SAM_JY_Result.EndEdit();
            lisResultSql.Add(this.blljy.makeDelSql_SAM_JY_RESULT(str主表id));
            for (int i = 0; i < dataGrid_SAM_JY_Result.Rows.Count; i++)
            {
                string strItemid = "";
                string strItemValue = "";

                if (dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_id"] != null)
                {
                    strItemid = dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_id"].Value.ToString();
                }
                if (dataGrid_SAM_JY_Result.Rows[i].Cells["fvalue"] != null)
                {
                    strItemValue = dataGrid_SAM_JY_Result.Rows[i].Cells["fvalue"].Value.ToString();
                }


                var item = listSam_item.Where(c => c.fitem_id == strItemid).FirstOrDefault();

                int? str是否计算 = item.fjx_if;
                if (str是否计算.Equals(1))
                {
                    string str公式 = item.fjx_formula;
                    strItemValue = bll公式计算值(str公式);
                }

                jyresultmodel resultModel = new jyresultmodel();
                string str参考值 = GetRefValueByItemCode(dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_id"].Value.ToString());
                if (str参考值.Equals("") || str参考值.Length == 0 || str参考值 == null) { }
                else
                {
                    resultModel.fitem_badge = this.blljy.Bll结果标记(strItemValue, str参考值);
                }
                resultModel.fresult_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                resultModel.fjy_id = str主表id;
                resultModel.fitem_id = dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_id"].Value.ToString();
                resultModel.fitem_code = dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_code"].Value.ToString();
                resultModel.fitem_name = dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_name"].Value.ToString();
                resultModel.fitem_unit = dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_unit"].Value.ToString();
                resultModel.fitem_ref = dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_ref"].Value.ToString();
                //resultModel.fitem_badge = dataGridViewResult.Rows[i].Cells["fitem_badge"].Value.ToString();
                resultModel.fvalue = strItemValue;
                resultModel.fod = dataGrid_SAM_JY_Result.Rows[i].Cells["fod"].Value.ToString();
                resultModel.fcutoff = dataGrid_SAM_JY_Result.Rows[i].Cells["fcutoff"].Value.ToString();
                resultModel.forder_by = dataGrid_SAM_JY_Result.Rows[i].Cells["forder_by"].Value.ToString();
                resultModel.fremark = dataGrid_SAM_JY_Result.Rows[i].Cells["fremark"].Value.ToString();
                lisResultSql.Add(this.blljy.makeAddsql_SAM_JY_RESULT(resultModel));
            }
            return lisResultSql;
        }


        //add by wjz 根据datatable中的数据进行计算 △

        private string bll公式计算值(string str公式)
        {
            string strR = "";
            try
            {
                this.Validate();
                dataGrid_SAM_JY_Result.EndEdit();
                string strgs_新 = str公式;
                for (int i = 0; i < dataGrid_SAM_JY_Result.Rows.Count; i++)
                {
                    string strItemCode = "";
                    string strItemValue = "";
                    if (dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_code"] != null)
                    {
                        strItemCode = "[" + dataGrid_SAM_JY_Result.Rows[i].Cells["fitem_code"].Value.ToString() + "]";
                    }
                    if (dataGrid_SAM_JY_Result.Rows[i].Cells["fvalue"] != null)
                    {
                        strItemValue = dataGrid_SAM_JY_Result.Rows[i].Cells["fvalue"].Value.ToString();
                    }
                    strgs_新 = Bll.StyleHelper.DbIndexOfStrRep(strgs_新, strItemCode, strItemValue);
                }
                strgs_新 = strgs_新.Replace("[", "").Trim();
                strgs_新 = strgs_新.Replace("]", "").Trim();

                strR = yunLis.wwfbll.FormulaGenerator.EvaluationResult(strgs_新);
                try
                {
                    double dou1 = Convert.ToDouble(strR.ToString());
                    strR = Math.Round(dou1, 2, MidpointRounding.AwayFromZero).ToString();
                }
                catch
                {
                    strR = "";
                }
                //MessageBox.Show("公式：" + strgs_新 + " 计算结果：" + strR);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.Message.ToString());
            }
            return strR;
        }




        /// <summary>
        /// 新增结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button新增_Click(object sender, EventArgs e)
        {
            //20160612 wjz 已经审核通过的项目不允许再添加新项目
            if (sam_jy.fjy_zt.Equals("已审核"))
            {
                MessageBox.Show("当期项目已经审核通过，若要修改此样本的检验项目，请先解除审核。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            try
            {
                using (yunLis.lis.com.JyItemSelForm jsf = new yunLis.lis.com.JyItemSelForm())
                {
                    jsf.strfinstr_id = this.cmb检验仪器.SelectedValue.ToString();
                    jsf.strfinstr_name = this.cmb检验仪器.SelectedText;

                    //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目▽
                    jsf.strDate = dtp样本日期.Value.ToString("yyyy-MM-dd");
                    try
                    {
                        jsf.str样本号 = Convert.ToInt64(txt样本号.Text).ToString();
                    }
                    catch
                    {
                        jsf.str样本号 = "";
                    }
                    //add by wjz 20151122 在日期和样本号不为空的情况下，只显示列表中没有的化验项目△

                    jsf.ShowDialog();
                    IList lisSel = jsf.selItemList;
                    if (lisSel != null)
                    {
                        for (int i = 0; i < lisSel.Count; i++)
                        {
                            var rows = dt_SAM_JY_RESULT.Where(c => c.fitem_id == lisSel[i].ToString());

                            if (rows.Count() > 0)
                            {
                            }
                            else
                            {
                                var drNew = new PojoSam_Jy_Result();
                                drNew.fresult_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                                drNew.fitem_id = lisSel[i].ToString();

                                SAM_ITEM dtItem = lis.SAM_ITEM.Where(c => c.fitem_id == lisSel[i].ToString()).FirstOrDefault();
                                if (dtItem != null)
                                {
                                    drNew.fitem_code = dtItem.fitem_code;
                                    drNew.fitem_name = dtItem.fname;
                                    drNew.fitem_unit = dtItem.funit_name;
                                    drNew.forder_by = dtItem.fprint_num;


                                    string str参考值 = GetRefValueByItemCode(lisSel[i].ToString());

                                    drNew.fitem_ref = str参考值;


                                    if (str参考值.Contains("--") && Regex.IsMatch(str参考值, @"\d+"))
                                    {

                                    }
                                    else
                                    {
                                        drNew.fvalue = str参考值;
                                    }
                                    //add yfh 20200917
                                    string str危急值 = string.Empty;

                                    str危急值 = GetJGRefValueByItemCode(lisSel[i].ToString());
                                    drNew.fitem_jgref = str危急值;


                                }
                                this.dt_SAM_JY_RESULT.Add(drNew);
                            }
                        }
                    }
                    //bllResultShow显示结果();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        ///  删除结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button删除_Click(object sender, EventArgs e)
        {
            if (label状态.Visible)
            {
                WWMessage.MessageShowWarning("当期样本已经审核，若要编辑此样本的项目信息，请先解除审核。");
                return;
            }
            try
            {
                if (WWMessage.MessageDialogResult("你确认删除当前项目？"))
                {
                    this.Validate();
                    dataGrid_SAM_JY_Result.EndEdit();
                    if (dt_SAM_JY_RESULT.Count() > 0)
                    {
                        if (dataGrid_SAM_JY_Result.Rows.Count > 0)
                        {
                            // string strid = this.dataGridViewResult.CurrentRow.Cells["fresult_id"].Value.ToString();
                            int intHH = this.dataGrid_SAM_JY_Result.CurrentRow.Index;
                            //todo:改到新的控件
                            //dt_SAM_JY_RESULT.Rows[intHH].Delete();
                            //dt_SAM_JY_RESULT.AcceptChanges();
                        }
                    }
                    //MessageBox.Show("明细数："+dtResult.Rows.Count.ToString());
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }




        /// <summary>
        /// 界面按钮操作方式
        /// </summary>
        enum OperType
        {
            选择结果,
            保存,
            审核,
            下一个,
            上一个
        }
        /// <summary>
        /// 样本号跳转方式
        /// </summary>
        enum MoveType
        {
            自增1,
            不变,
            自减1
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intMoveType">1：样本号变更类型</param>
        /// <param name="operType">操作类型，保存或审核</param>
        private void bll保存或审核(MoveType intMoveType, OperType operType)
        {
            try
            {
                bool b新增标记 = false;
                if (txt样本号.Text.Equals("") || txt样本号.Text.Length == 0)
                {
                    return;
                }
                if (operType == OperType.选择结果)
                {

                }
                else
                {
                    //bll仪器取数(0); //2015-05-27 14:04:42 yufh注释：取数方法重定义
                }
                int int样本号 = Convert.ToInt32(txt样本号.Text);
                txt样本号.Text = set样本号样式(int样本号);

                #region 判断病人信息、化验结果是否为空
                DataTable temp_SAM_JY_Result = dataGrid_SAM_JY_Result.DataSource as DataTable;
                if (string.IsNullOrWhiteSpace(this.txt申请号.Text.Trim()) && string.IsNullOrWhiteSpace(this.txt姓名.Text.Trim())
                    && string.IsNullOrWhiteSpace(this.txt门诊或住院号.Text.Trim()) && (temp_SAM_JY_Result == null || temp_SAM_JY_Result.Rows.Count == 0))
                {
                    try
                    {
                        if (intMoveType == MoveType.自增1)
                        {
                            int样本号 = Convert.ToInt32(txt样本号.Text) + 1;
                        }
                        else if (intMoveType == MoveType.不变)//点击“打印”按钮后，先执行审批操作，在执行打印操作。
                        {
                            int样本号 = Convert.ToInt32(txt样本号.Text);
                        }
                        else
                        {
                            if (Convert.ToInt32(txt样本号.Text) > 1)
                            {
                                int样本号 = Convert.ToInt32(txt样本号.Text) - 1;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    txt样本号.Text = set样本号样式(int样本号);
                    this.ActiveControl = txt样本号;

                    vRfresh右侧列表_Sam_JY_by仪器ID();

                    //billSave保存样本，间接调用：btnSave_Click(保存)，button审核_Click，button打印_Click，button下一个_Click，button上一个_Click，ProcessCmdKey，SaveSampleInfo未用，
                    //toolStripMenuItem1_Click(右键打印预览-已隐藏)，打印ToolStripMenuItem_Click(右键打印-已隐藏)

                    return;//左侧信息为空的时候，刷新右侧列表
                }
                #endregion




                IList lisSql_SAM_JY_SAM_JY_Result_SAM_Patient = new ArrayList();
                #region 主表赋值
                sam_jy.fhz_zyh = this.txt门诊或住院号.Text;
                sam_jy.fhz_name = this.txt姓名.Text;
                sam_jy.fjy_f = 0;
                sam_jy.fprint_count = 0;
                if (this.txt年龄.Text == "" || this.txt年龄.Text.Length == 0)
                {
                    this.txt年龄.Text = "0";
                }
                sam_jy.fhz_age = Convert.ToDouble(this.txt年龄.Text);
                sam_jy.fhz_bed = this.txt床号.Text;
                sam_jy.fapply_id = this.txt申请号.Text;
                sam_jy.fapply_time = this.dtp送检时间.Value.ToString("yyyy-MM-dd HH:mm:ss");
                sam_jy.fsampling_time = this.dtp采样时间.Value.ToString("yyyy-MM-dd HH:mm:ss");
                sam_jy.freport_time = this.dtp报告时间.Value.ToString("yyyy-MM-dd HH:mm:ss");
                sam_jy.fremark = this.txt备注或档案号.Text;
                //页面添加身份证和电子卡号控件，用页面覆盖model
                sam_jy.sfzh = this.txt身份证号.Text.Trim();
                sam_jy.f1 = this.txt电子健康卡.Text.Trim();
                sam_jy.fhz_age_date = this.bllPatient.BllBirthdayByAge(Convert.ToDouble(sam_jy.fhz_age), this.cmb年龄单位.SelectedValue.ToString());
                sam_jy.fsys_user = LoginBLL.strPersonID;
                sam_jy.fsys_dept = LoginBLL.strDeptID;
                sam_jy.fsys_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                sam_jy.finstr_result_id = this.str仪器结果FTaskID;

                if (sam_jy.fhz_type_id == ((int)HIS.COMM.ClassEnum.enLIS病人类型.住院).ToString())
                {
                    decimal decZYID = Convert.ToDecimal(sam_jy.fhz_id);
                    var item病人信息 = chis.ZY病人信息.Where(c => c.ZYID == decZYID).FirstOrDefault();
                    if (item病人信息 != null)
                    {
                        sam_jy.fhz_dept = item病人信息.病区.ToString();
                    }
                }

                //if (dataGridViewResult.Rows.Count > 0 && strSelRelt.Equals("审核"))
                //{
                //    modeljy.fjy_zt检验状态 = "已审核";//如果有结果明细时，保存就把主表设置为已经审核。
                //    modeljy.fchenk_审核医师ID = LoginBLL.strPersonID;
                //    modeljy.fcheck_审核时间 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //}

                if (dataGrid_SAM_JY_Result.Rows.Count > 0 && operType == OperType.审核)
                {
                    sam_jy.fjy_zt = "已审核";//如果有结果明细时，保存就把主表设置为已经审核。
                    if (string.IsNullOrWhiteSpace(LoginBLL.strCheckID))
                    {
                        sam_jy.fchenk_user_id = LoginBLL.strPersonID;
                    }
                    else
                    {
                        sam_jy.fchenk_user_id = LoginBLL.strCheckID;
                    }
                    sam_jy.fcheck_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                #endregion


                //取出检验id判断是否新增/修改
                string strSql = "SELECT fjy_id,fjy_zt FROM SAM_JY where fjy_date='" + dtp样本日期.Value.ToString("yyyy-MM-dd") + "' and fjy_instr='" +
                    cmb检验仪器.SelectedValue.ToString() + "' and fjy_yb_code='" + txt样本号.Text + "' ";

                string s样本日期 = dtp样本日期.Value.ToString("yyyy-MM-dd");
                DataTable dtOK = SqlHelper.ExecuteDataTable(strSql);
                string s检验ID = "";
                string s检验状态 = "";
                if (dtOK.Rows.Count > 0)
                {
                    s检验ID = dtOK.Rows[0]["fjy_id"].ToString();
                    s检验状态 = dtOK.Rows[0]["fjy_zt"].ToString();
                }
                if (Convert.ToString(sam_jy.fhz_id) == "") sam_jy.fhz_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();

                #region 新增/修改
                if (s检验ID.Equals("") || s检验ID.Length == 0)
                {//新增
                    sam_jy.fjy_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                    //子-s                       
                    lisSql_SAM_JY_SAM_JY_Result_SAM_Patient = make先删后加Sql_SAM_JY_RESULT(lisSql_SAM_JY_SAM_JY_Result_SAM_Patient, sam_jy.fjy_id);
                    //主                     
                    lisSql_SAM_JY_SAM_JY_Result_SAM_Patient.Add(this.blljy.makeAddSql_SAM_JY(this.sam_jy));
                    b新增标记 = true;
                }
                else
                {//修改
                    if (s检验状态.Equals("未审核"))
                    {
                        sam_jy.fjy_id = s检验ID;
                        lisSql_SAM_JY_SAM_JY_Result_SAM_Patient = make先删后加Sql_SAM_JY_RESULT(lisSql_SAM_JY_SAM_JY_Result_SAM_Patient, sam_jy.fjy_id);
                        var Update_SAM_JY = this.blljy.makeUpdateSql_SAM_JY(this.sam_jy);
                        lisSql_SAM_JY_SAM_JY_Result_SAM_Patient.Add(Update_SAM_JY);
                    }
                }


                //如果信息完成，写 SAM_PATIENT 表
                if (string.IsNullOrWhiteSpace(this.sam_jy.sfzh) || string.IsNullOrWhiteSpace(this.sam_jy.fhz_zyh))
                {

                }
                else
                {
                    if (operType == OperType.审核)
                    {
                        string AddSql_SAM_PATIENT = this.blljy.makeAddSql_SAM_PATIENT(this.sam_jy);
                        if (AddSql_SAM_PATIENT == "" || AddSql_SAM_PATIENT.Length == 0) { }
                        else
                        {
                            lisSql_SAM_JY_SAM_JY_Result_SAM_Patient.Add(AddSql_SAM_PATIENT);
                        }
                    }
                }

                string sSucess = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql_SAM_JY_SAM_JY_Result_SAM_Patient);//执行数据库写入操作

                if (HIS.COMM.Helper.EnvironmentHelper.CommBoolGet("启用电子健康卡微信推送", "true", ""))
                {
                    if (sSucess == "true" && (operType == OperType.审核 || this.print))
                    {
                        try
                        {
                            //微信推送:这个地方做个说明吧，接口地址是陈忠华、东辉他们弄的，我负责调用
                            //原来代码丢了，现在找回来一版，能推送成功，yfh 371122201403077416 这个测试身份证
                            string text5 = "http://192.168.10.118/weixintuisongController.do?mycheckts&idcard={0}&fjyid={1}&rq={2}&mc={3}&flag=1";
                            text5 = string.Format(text5, new object[]
                            {
                                    this.sam_jy.sfzh,
                                    HIS.COMM.zdInfo.Model单位信息.iDwid.ToString() + "_" + this.sam_jy.fjy_id,
                                    this.sam_jy.fjy_date,
                                    HIS.COMM.zdInfo.Model单位信息.sDwmc
                            });
                            string text6 = JYForm.SendRequest(text5, Encoding.UTF8);
                        }
                        catch (Exception ex)
                        {
                            throw (ex);
                        }
                        //电子健康卡推送消息
                        if (LoginBLL.sendauto)
                        {
                            #region 电子健康卡推送消息
                            //患者信息
                            JObject user = new JObject();
                            user["Name"] = sam_jy.fhz_name;
                            user["bcard"] = sam_jy.f1;
                            user["reportid"] = sam_jy.fjy_id;
                            Common.SendEcardMessage(user);
                            #endregion
                        }
                    }
                }

                //从LIS中取出已经审核过的数据，并将其写入HIS
                #region 20150701 wjz 审核时向His写入检验数据 
                if (sSucess.Equals("true") && operType.Equals("审核"))
                {
                    //从LIS中取出已经审核过的数据，并将其写入HIS，只有申请单号不为空时才允许回写数据
                    //DataTable dt审核 = jybll接口数据读取.BllResultDT(modeljy.fjy_检验id);
                    string str日期 = dtp样本日期.Value.ToString("yyyy-MM-dd");
                    this.blljy.WriteDataToHisBy门诊住院号(sam_jy.fhz_zyh, str日期, cmb检验仪器.SelectedValue.ToString(), txt样本号.Text);
                }


                if (sSucess == "true" && b新增标记)
                {
                    ////添加新行
                    //DataTable dt = (DataTable)bindingSource样本.DataSource;
                    //DataRow dr = dt.NewRow();
                    //dr["fjy_yb_code"] = fjy_yb_code样本.Text;
                    //dr["fhz_name"] = modeljy.fhz_姓名;
                    //dr["fhz_zyh"] = modeljy.fhz_住院号;
                    //dr["fhz_bed"] = modeljy.fhz_床号;
                    //dr["性别"] = fhz_sex性别.Text;
                    //dr["fhz_age"] = modeljy.fhz_年龄;
                    //dr["年龄单位"] = fhz_age_unittextBox.Text;
                    //dr["科室"] = fhz_dept科室.Text;
                    //dr["样本"] = fjy_yb_typetextBox.Text;
                    //dr["检验医师"] = fjy_user_idtextBox.Text;
                    //dr["fprint_zt"] = "未打印";
                    //dr["fjy_zt"] = "未审核";
                    //dr["类型"] = fhz_type_idtextBox_name.Text;
                    //dr["fapply_id"] = modeljy.fapply_申请单ID;
                    //dr["fjy_id"] = modeljy.fjy_检验id;
                    //dt.Rows.Add(dr);
                    //bindingSource样本.DataSource = dt;
                    //bindingSource样本.MoveLast();
                }
                #endregion
                try
                {
                    if (intMoveType == MoveType.自增1)
                    {
                        int样本号 = Convert.ToInt32(txt样本号.Text) + 1;
                    }
                    else if (intMoveType == MoveType.不变)
                    {
                        int样本号 = Convert.ToInt32(txt样本号.Text);
                    }
                    else
                    {
                        if (Convert.ToInt32(txt样本号.Text) > 1)
                        {
                            int样本号 = Convert.ToInt32(txt样本号.Text) - 1;
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                //目的：焦点位于备注，回车显示先一个样本信息
                if (intMoveType == MoveType.自增1 && operType == OperType.保存)
                {
                    txt样本号.Text = set样本号样式(int样本号);
                    this.ActiveControl = txt样本号;
                }


                if (operType == OperType.保存 && s检验状态 != "已审核")
                {

                }
                else
                {
                    txt样本号.Text = set样本号样式(int样本号);
                    this.ActiveControl = txt样本号;
                }
                vRfresh右侧列表_Sam_JY_by仪器ID();
                //billSave保存样本，间接调用：btnSave_Click(保存)，button审核_Click，button打印_Click，button下一个_Click，button上一个_Click，
                //ProcessCmdKey，SaveSampleInfo未用，toolStripMenuItem1_Click(右键打印预览-已隐藏)，打印ToolStripMenuItem_Click(右键打印-已隐藏)
                //bllYBListByYbCode();                
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.ToString());
            }
        }
        private string set样本号样式(int int样本号)
        {
            string str样本号设置 = "";
            //if (int样本号.ToString().Length == 1)
            //{
            //    str样本号设置 = "00" + int样本号.ToString();
            //}
            //else if (int样本号.ToString().Length == 2)
            //{
            //    str样本号设置 = "0" + int样本号.ToString();
            //}
            //else
            //{
            //    str样本号设置 = int样本号.ToString();
            //}
            str样本号设置 = int样本号.ToString();
            return str样本号设置;
        }


        /// <summary>
        /// 删除样本
        /// </summary>
        private void bllDel()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (WWMessage.MessageDialogResult("你确认删除当前 " + txt样本号.Text + " 号样本？"))
                {
                    // MessageBox.Show(strCurrSelfjy_id);
                    if (this.s当前样本Id.Equals("") || s当前样本Id.Length == 0)
                    { }
                    else
                    {
                        string strSql = "SELECT fjy_id FROM SAM_JY where fjy_id='" + s当前样本Id + "' and fjy_zt='已审核' ";
                        string strValue = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySql(yunLis.wwfbll.WWFInit.strDBConn, strSql);
                        if (strValue.Equals("") || strValue.Length == 0)
                        {
                            this.blljy.Delete(s当前样本Id);

                            //changed by wjz 20160128 修改逻辑错误，改之。删除样本时，用户信息的删除，不能依据下面三个控件：姓名、性别、科室。 ▽
                            #region old code
                            //if (fhz_name姓名.Text != "" && fhz_sex性别.Text != "" && fhz_dept科室.Text != "")////////////////////////
                            //{ //如果三者都不为空，默认是有效数据不进行删除，否则删除样本时同时删除病人信息
                            //}
                            #endregion

                            //if (fhz_name姓名.Text != "" && fhz_sex性别.Text != "" && fhz_dept科室.Text != "")//===============
                            if (dataGridView样本.CurrentRow.Cells["fhz_name"].ToString() != "" && dataGridView样本.CurrentRow.Cells["性别"].ToString() != "" && dataGridView样本.CurrentRow.Cells["科室"].ToString() != "")
                            {
                            }
                            //changed by wjz 20160128 修改逻辑错误，改之。删除样本时，用户信息的删除，不能依据下面三个控件：姓名、性别、科室。 △
                            else
                                bllPatient.BllPatientDel(sam_jy.fhz_id);
                            //bllYBList();
                            bindingSource样本.RemoveAt(dataGridView样本.CurrentRow.Index);
                            //add by wjz 20160227 取消审核无反应，改之 ▽
                            //dataGridView样本_Click(null, null);

                            RemoveSexAgeTextChangedEvent();

                            //add by wjz 20160228 删除样本时，中间的样本项目信息不更新，改之 ▽
                            //bll显示样本();
                            Refresh_Display_First_SAM_JY_and_Result();
                            //add by wjz 20160228 删除样本时，中间的样本项目信息不更新，改之 △

                            AddSexAgeTextChangedEvent();
                            //add by wjz 20160227 取消审核无反应，改之 △
                        }
                        else
                        {
                            WWMessage.MessageShowWarning("当前样本 " + this.txt门诊或住院号.Text + " 已审核，不可删除。 ");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        /// 取消审核
        /// </summary>
        private void bll取消审核()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (this.s当前样本Id.Equals("") || s当前样本Id.Length == 0)
                { }
                else
                {
                    #region  此部分的逻辑做调整  已经注释，修改后的逻辑参考下方的 #region
                    //string strSql = "SELECT fjy_id FROM SAM_JY where fjy_id='" + strCurrSelfjy_id + "' and fjy_zt='已审核' ";
                    //string strValue = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySql(yunLis.wwfbll.WWFInit.strDBConn, strSql);
                    //if (strValue.Equals("") || strValue.Length == 0)
                    //{
                    //    WWMessage.MessageShowWarning("当前样本 " + this.fhz_zyh住院号.Text + " 未审核，不可解除审核。 ");

                    //}
                    //else
                    //{
                    //    int intRRR = this.blljy.UpdateZT(strCurrSelfjy_id);
                    //    if (intRRR > 0)
                    //    {
                    //        // this.label状态.Visible = false;
                    //        //bllYBListByYbCode();
                    //        dataGridView样本_Click(null, null);
                    //    }

                    //}
                    #endregion
                    #region  修改后的逻辑
                    string strSql = "SELECT fjy_id,fjy_date,fjy_instr,fjy_yb_code,fapply_id,fhz_zyh FROM SAM_JY where fjy_id='" + s当前样本Id + "' and fjy_zt='已审核' ";//调整 20150702 wjz
                    DataTable dtTemp检验 = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(yunLis.wwfbll.WWFInit.strDBConn, strSql);   //调整过 20150702 wjz
                    if (dtTemp检验 == null || dtTemp检验.Rows.Count == 0) //调整过 20150702 wjz
                    //if (strValue.Equals("") || strValue.Length == 0)
                    {
                        WWMessage.MessageShowWarning("当前样本 " + this.txt门诊或住院号.Text + " 未审核，不可解除审核。 ");

                    }
                    else
                    {
                        int intRRR = this.blljy.UpdateZT(s当前样本Id);
                        if (intRRR > 0)
                        {
                            //添加 20150702 wjz begin
                            //string str申请单号 = dtTemp检验.Rows[0]["fapply_id"].ToString();
                            //string str日期 = dtTemp检验.Rows[0]["fjy_date"].ToString();
                            //string str设备ID = dtTemp检验.Rows[0]["fjy_instr"].ToString();
                            //string str样本号 = dtTemp检验.Rows[0]["fjy_yb_code"].ToString();

                            //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                            //string str门诊住院号 = dtTemp检验.Rows[0]["fhz_zyh"].ToString();
                            //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △

                            //changed 20150814 wjz 以下两行
                            //bool b是否回写 = yunLis.Properties.Settings.Default.WriteJYDataToHis;
                            //this.blljy.Del化验结果FromHis(str申请单号, str日期, str设备ID, str样本号, b是否回写);
                            //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                            //this.blljy.Del化验结果FromHis(str申请单号, str日期, str设备ID, str样本号);
                            //this.blljy.Del化验结果FromHisBy门诊住院号(str门诊住院号, str日期, str设备ID, str样本号);
                            //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △
                            //添加 20150702 wjz end

                            // this.label状态.Visible = false;
                            //bllYBListByYbCode();

                            //add by wjz 20160227 取消审核无反应，改之 ▽
                            //dataGridView样本_Click(null, null);

                            RemoveSexAgeTextChangedEvent();

                            bll显示样本();

                            AddSexAgeTextChangedEvent();
                            //add by wjz 20160227 取消审核无反应，改之 △
                        }

                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }



        /// <summary>
        /// 结果列显示 
        /// </summary>
        private void bllResultShow(string str仪器id)
        {
            if (str仪器id.Equals("mby_lb_mk3"))
            {
                this.fod.Visible = true;
                this.fcutoff.Visible = true;
            }
            else
            {
                this.fod.Visible = false;
                this.fcutoff.Visible = false;
            }
        }
        #endregion


        #region 方法
        /// <summary>
        /// 初始化
        /// </summary>
        private void bllInit初始化()
        {

            try
            {
                this.cmb检验仪器.SelectedIndexChanged -= cmb检验仪器_SelectedIndexChanged;
                //仪器
                this.cmb检验仪器.DataSource = lis.SAM_INSTR.Where(c => c.fjygroup_id == yunLis.wwfbll.LoginBLL.strDeptID).OrderBy(c => c.forder_by).ToList(); ;
                this.cmb检验仪器.ValueMember = "finstr_id";
                this.cmb检验仪器.DisplayMember = "ShowName";
                this.cmb检验仪器.SelectedValue = yunLis.wwf.Customization.DefaultInstrument.GetInstrID();
                //add by wjz 20160214 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 ▽
                //调用此事件方法的目的：调用方法（BllItemList仪器项目列表），从而为DataTable(dtItemList)赋值；
                //点击“审核”按钮时，需要使用此DataTable(dtItemList)。
                cmb检验仪器_SelectedIndexChanged(null, null);
                //add by wjz 20160214 画面加载时，事件fjy_instrComboBox_仪器_SelectedIndexChanged会被触发3次，先移除此事件，本方法结束时在附加此事件 △
                //20150610 end    add by wjz 添加本机默认仪器功能
                this.cmb检验仪器.SelectedIndexChanged += cmb检验仪器_SelectedIndexChanged;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }


        private void InitSexComboBox()
        {
            DataTable dtSex = new DataTable();
            dtSex.Columns.Add("fcode");
            dtSex.Columns.Add("fname");
            //空白行
            DataRow drSpace = dtSex.NewRow();
            drSpace["fcode"] = "";
            drSpace["fname"] = "";
            dtSex.Rows.Add(drSpace);
            //添加男性
            DataRow drMale = dtSex.NewRow();
            drMale["fcode"] = "1";
            drMale["fname"] = "男";
            dtSex.Rows.Add(drMale);
            //添加女性
            DataRow drFemale = dtSex.NewRow();
            drFemale["fcode"] = "2";
            drFemale["fname"] = "女";
            dtSex.Rows.Add(drFemale);
            this.cmb性别.DataSource = dtSex;
        }



        private void Init病人类型()
        {
            DataTable dtPatientType = new DataTable();
            dtPatientType.Columns.Add("fcode");
            dtPatientType.Columns.Add("fname");

            //空白行
            DataRow drSpace = dtPatientType.NewRow();
            drSpace["fcode"] = "";
            drSpace["fname"] = "";
            dtPatientType.Rows.Add(drSpace);

            DataRow drMZ = dtPatientType.NewRow();
            drMZ["fcode"] = "1";
            drMZ["fname"] = "门诊";
            dtPatientType.Rows.Add(drMZ);

            DataRow drJZ = dtPatientType.NewRow();
            drJZ["fcode"] = "2";
            drJZ["fname"] = "急诊";
            dtPatientType.Rows.Add(drJZ);

            DataRow drZY = dtPatientType.NewRow();
            drZY["fcode"] = "3";
            drZY["fname"] = "住院";
            dtPatientType.Rows.Add(drZY);

            DataRow drCT = dtPatientType.NewRow();
            drCT["fcode"] = "4";
            drCT["fname"] = "查体";
            dtPatientType.Rows.Add(drCT);

            this.cmb病人类型.DataSource = dtPatientType;
        }


        private void Init年龄单位()
        {
            DataTable dtAgeUnit = new DataTable();
            dtAgeUnit.Columns.Add("fcode");
            dtAgeUnit.Columns.Add("fname");

            //空白行
            DataRow drSpace = dtAgeUnit.NewRow();
            drSpace["fcode"] = "";
            drSpace["fname"] = "";
            dtAgeUnit.Rows.Add(drSpace);

            DataRow drYear = dtAgeUnit.NewRow();
            drYear["fcode"] = "1";
            drYear["fname"] = "岁";
            dtAgeUnit.Rows.Add(drYear);

            DataRow drMonth = dtAgeUnit.NewRow();
            drMonth["fcode"] = "2";
            drMonth["fname"] = "月";
            dtAgeUnit.Rows.Add(drMonth);

            DataRow drDay = dtAgeUnit.NewRow();
            drDay["fcode"] = "3";
            drDay["fname"] = "天";
            dtAgeUnit.Rows.Add(drDay);
            this.cmb年龄单位.DataSource = dtAgeUnit;
        }


        /// <summary>
        /// 取得当前仪器+日期样本列表
        /// </summary>
        private void vRfresh右侧列表_Sam_JY_by仪器ID()
        {
            if (cmb检验仪器.SelectedValue == null)
            {
                return;
            }
            string strWhere = " (检验日期='" + dtp样本日期.Value.ToString("yyyy-MM-dd") + "' and 化验设备ID='" + cmb检验仪器.SelectedValue.ToString() + "') ";
            string strWhere_1 = "";
            string strTJ = comboBox过滤样本_病人类别.SelectedItem.ToString();
            if (strTJ.Equals("门诊"))
            {
                strWhere_1 = " and (病人类别='门诊')";
            }
            else if (strTJ.Equals("急诊"))
            {
                strWhere_1 = " and (病人类别='急诊')";
            }
            else if (strTJ.Equals("住院"))
            {
                strWhere_1 = " and (病人类别='住院')";
            }
            else if (strTJ.Equals("未打印"))
            {
                strWhere_1 = " and (打印状态='未打印')";
            }
            else if (strTJ.Equals("已打印"))
            {
                strWhere_1 = " and (打印状态='已打印')";
            }
            else if (strTJ.Equals("未审核"))
            {
                strWhere_1 = " and (化验状态='未审核')";
            }
            else if (strTJ.Equals("已审核"))
            {
                strWhere_1 = " and (化验状态='已审核')";
            }
            else if (strTJ.Equals("未打印未审核"))
            {
                strWhere_1 = " and (打印状态='未打印' and 化验状态='未审核')";
            }
            else if (strTJ.Equals("未打印已审核"))
            {
                strWhere_1 = " and (打印状态='未打印' and 化验状态='已审核')";
            }
            else if (strTJ.Equals("已打印未审核"))
            {
                strWhere_1 = " and (打印状态='已打印' and 化验状态='未审核')";
            }
            else if (strTJ.Equals("已打印已审核"))
            {
                strWhere_1 = " and (打印状态='已打印' and 化验状态='已审核')";
            }
            else
            {
                strWhere_1 = " and 1=1 ";
            }
            var dtSAM_JY = Bll.DataHelper.getList报告摘要(strWhere + strWhere_1);//+ " Order by convert(int,fjy_yb_code)"

            int iOldCurrentIndex = this.bindingSource样本.Position;//记录旧的位置索引
            bFlagForPostionChanged = true;

            this.bindingSource样本.DataSource = dtSAM_JY;

            this.dataGridView样本.AutoGenerateColumns = false;
            this.dataGridView样本.DataSource = bindingSource样本;
            gridControl样本.DataSource = bindingSource样本;
            gridView样本.PopulateColumns();

            bFlagForPostionChanged = false;
            int iNewCurrentIndex = this.bindingSource样本.Position;//记录新的位置索引
            if ((iNewCurrentIndex == iOldCurrentIndex) || (iNewCurrentIndex == -1))//新数据源当前行为-1时（新数据源为空），postionchanged事件，bllYBListByYbCode也未被调用，此处需要补上
            {
                Refresh_Display_First_SAM_JY_and_Result();//位置没变的情况下，“bindingSource样本”的postionchanged事件未触发，bllYBListByYbCode未被调用，此时需主动调用一次
            }
            else
            {
                //位置变更时，不做任何处理。“bindingSource样本”的postionchanged事件中已调用bllYBListByYbCode
            }
        }

        private bool bFlagForPostionChanged = false;

        void Refresh_SAM_JY_左侧显示第一个SAM_JY记录()
        {
            try
            {
                Convert.ToInt32(this.txt样本号.Text);
            }
            catch
            {
                WWMessage.MessageShowError("样本号录入有误，请录入数值后重试。");
                this.ActiveControl = txt样本号;
                return;
            }
            sam_jy = new SAM_JY();

            string strWhere = "fjy_date='" + dtp样本日期.Value.ToString("yyyy-MM-dd") + "' and fjy_instr='" + cmb检验仪器.SelectedValue.ToString() + "' and fjy_yb_code='" + txt样本号.Text + "'";

            var dtSAM_JY = Bll.DataHelper.getList报告摘要(strWhere).FirstOrDefault();

            if (dtSAM_JY != null)
            {
                sam_jy.fjy_id = dtSAM_JY.fjy_id;
                sam_jy.fjy_instr = dtSAM_JY.fjy_instr;
                sam_jy.sfzh = dtSAM_JY.sfzh;
                sam_jy.f1 = dtSAM_JY.f1;      //电子健康卡号
                sam_jy.fjy_date = dtSAM_JY.fjy_date;
                sam_jy.fjy_yb_code = this.set样本号样式(Convert.ToInt32(dtSAM_JY.fjy_yb_code));
                sam_jy.fhz_id = dtSAM_JY.fhz_id;
                sam_jy.fhz_type_id = dtSAM_JY.fhz_type_id;
                sam_jy.fhz_zyh = dtSAM_JY.fhz_zyh;
                sam_jy.fhz_name = dtSAM_JY.fhz_name;
                sam_jy.fhz_sex = dtSAM_JY.fhz_sex;
                sam_jy.fhz_age = Convert.ToDouble(dtSAM_JY.fhz_age);
                sam_jy.fhz_age_unit = dtSAM_JY.fhz_age_unit;
                sam_jy.fjy_sf_type = dtSAM_JY.fjy_sf_type;//收费类型
                sam_jy.fhz_dept = dtSAM_JY.fhz_dept;
                sam_jy.fhz_bed = dtSAM_JY.fhz_bed;//床号
                sam_jy.fapply_id = dtSAM_JY.fapply_id;
                sam_jy.fjy_yb_type = dtSAM_JY.fjy_yb_type;//样本类型
                sam_jy.fapply_user_id = dtSAM_JY.fapply_user_id;//送检 人 申请人
                sam_jy.fapply_time = dtSAM_JY.fapply_time;
                sam_jy.fsampling_time = dtSAM_JY.fsampling_time;
                sam_jy.freport_time = dtSAM_JY.freport_time;
                sam_jy.fjy_user_id = dtSAM_JY.fjy_user_id;     //检验医师                                                                            
                sam_jy.fchenk_user_id = dtSAM_JY.fchenk_user_id;  //核对医师
                sam_jy.fjy_lczd = dtSAM_JY.fjy_lczd;
                sam_jy.fremark = dtSAM_JY.fremark;
                sam_jy.fjy_zt = dtSAM_JY.fjy_zt;
                sam_jy.fprint_zt = dtSAM_JY.fprint_zt;
                sam_jy.fsys_user = dtSAM_JY.fsys_user;
                sam_jy.fsys_time = dtSAM_JY.fsys_time;


                cmb检验仪器.SelectedValue = sam_jy.fjy_instr;



                if (!string.IsNullOrEmpty(sam_jy.sfzh))
                {
                    this.txt身份证号.Text = sam_jy.sfzh;
                }
                else
                {
                    this.txt身份证号.Text = "";
                }

                if (!string.IsNullOrEmpty(sam_jy.f1))
                {
                    this.txt电子健康卡.Text = sam_jy.f1;
                }
                else
                {
                    this.txt电子健康卡.Text = "";
                }

                if (!sam_jy.fjy_date.Equals("") & sam_jy.fjy_date.Length > 0)
                {
                    dtp样本日期.Value = Convert.ToDateTime(sam_jy.fjy_date);
                }

                this.txt样本号.Text = sam_jy.fjy_yb_code;

                this.cmb病人类型.SelectedValue = sam_jy.fhz_type_id;

                this.txt门诊或住院号.Text = sam_jy.fhz_zyh;




                this.txt姓名.Text = sam_jy.fhz_name;
                cmb性别.SelectedValue = sam_jy.fhz_sex;
                this.txt年龄.Text = sam_jy.fhz_age.ToString();
                this.cmb年龄单位.SelectedValue = sam_jy.fhz_age_unit;
                try
                {
                    this.cmb费别.SelectedValue = sam_jy.fjy_sf_type;
                }
                catch
                {
                    this.cmb费别.SelectedValue = "";
                }
                try
                {
                    this.cmb科室.SelectedValue = this.sam_jy.fhz_dept;
                }
                catch
                {
                    this.cmb科室.SelectedValue = "";
                }

                this.txt床号.Text = sam_jy.fhz_bed;
                this.cmb样本类型.SelectedValue = this.sam_jy.fjy_yb_type;

                try
                {
                    this.cmb送检医师.EditValue = Convert.ToInt32(sam_jy.fapply_user_id);
                }
                catch
                {
                    this.cmb送检医师.EditValue = "";
                }


                if (sam_jy.fapply_time != "" & sam_jy.fapply_time.Length > 0)
                {
                    dtp送检时间.Value = Convert.ToDateTime(sam_jy.fapply_time);
                }

                if (sam_jy.fsampling_time != null & sam_jy.fsampling_time.Length > 0)
                {
                    dtp采样时间.Value = Convert.ToDateTime(sam_jy.fsampling_time);
                }


                if (sam_jy.freport_time != "" & sam_jy.freport_time.Length > 0)
                {
                    dtp报告时间.Value = Convert.ToDateTime(sam_jy.freport_time);
                }


                try
                {
                    this.cmb检验医师.EditValue = Convert.ToInt32(this.sam_jy.fjy_user_id);
                }
                catch
                {
                    this.cmb检验医师.EditValue = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认检验者编码", "", ""));
                }


                try
                {
                    this.cmb核对医师.EditValue = Convert.ToInt32(this.sam_jy.fchenk_user_id);
                }
                catch
                {
                    this.cmb核对医师.EditValue = Convert.ToInt32(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS默认核对者编码", "", ""));
                }
                //临床诊断

                this.txt申请号.TextChanged -= txt申请号_TextChanged;//新增的代码
                this.txt申请号.Text = sam_jy.fapply_id;
                this.txt申请号.TextChanged -= txt申请号_TextChanged;//新增的代码
                this.txt申请号.TextChanged += txt申请号_TextChanged;//新增的代码      

                this.txt临床诊断.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM SAM_DISEASE where fcode='" + sam_jy.fjy_lczd + "'");

                this.txt备注或档案号.Text = sam_jy.fremark;
                label样本审核状态.Text = sam_jy.fjy_zt;

                if (sam_jy.fjy_zt.Equals("已审核"))
                {
                    label状态.Visible = true;
                    this.dataGrid_SAM_JY_Result.Columns["fvalue"].ReadOnly = true;
                }
                else
                {
                    label状态.Visible = false;
                    this.dataGrid_SAM_JY_Result.Columns["fvalue"].ReadOnly = false;
                }

                label样本打印状态.Text = sam_jy.fprint_zt;

                label最后修改.Text = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON where fperson_id='" + sam_jy.fchenk_user_id + "'");

                fcheck_timelabel.Text = dtSAM_JY.fcheck_time;
            }
            else
            {
                label状态.Visible = false;
                this.dataGrid_SAM_JY_Result.Columns["fvalue"].ReadOnly = false;
                //modeljy.fjy_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                sam_jy.fjy_instr = cmb检验仪器.SelectedValue.ToString();
                sam_jy.fjy_date = dtp样本日期.Value.ToString("yyyy-MM-dd");
                sam_jy.fjy_yb_code = this.set样本号样式(Convert.ToInt32(this.txt样本号.Text));
                sam_jy.fhz_age_unit = "1";//年龄单位默认为1
                this.cmb年龄单位.SelectedValue = sam_jy.fhz_age_unit;
                sam_jy.fjy_user_id = LoginBLL.strPersonID;//检验医师
                try
                {
                    this.cmb检验医师.EditValue = this.sam_jy.fjy_user_id;
                }
                catch
                {
                    this.cmb检验医师.EditValue = "";
                }
                sam_jy.fjy_zt = "未审核";
                label样本审核状态.Text = sam_jy.fjy_zt;
                sam_jy.fprint_zt = "未打印";
                label样本打印状态.Text = sam_jy.fprint_zt;
                this.cmb病人类型.SelectedValue = "";
                txt门诊或住院号.Text = "";
                txt姓名.Text = "";
                cmb性别.SelectedValue = "";
                txt年龄.Text = "";
                this.cmb费别.SelectedValue = "";
                this.cmb科室.SelectedValue = "";
                txt床号.Text = "";
                txt申请号.Text = "";
                this.cmb样本类型.SelectedValue = "";
                this.cmb送检医师.EditValue = "";
                this.cmb核对医师.EditValue = "";
                dtp送检时间.Value = System.DateTime.Now;
                dtp采样时间.Value = System.DateTime.Now;
                dtp报告时间.Value = System.DateTime.Now;
                txt临床诊断.Text = "";
                txt备注或档案号.Text = "";
                this.txt身份证号.Text = "";
                this.txt电子健康卡.Text = "";
            }
        }
        /// <summary>
        /// 取得一个样本内容
        /// </summary>
        /// <param name="strid"></param>
        private void Refresh_Display_First_SAM_JY_and_Result()
        {
            try
            {
                Refresh_SAM_JY_左侧显示第一个SAM_JY记录();
                refresh_SAM_JY_RESULT_if_need_from_Ins_Result_Detail();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 根据住院号取得患者信息
        /// </summary>
        /// <param name="_s住院号"></param>
        private void bllPatInfo(string _s住院号)
        {
            try
            {
                if ((sam_jy.fjy_id == null || label样本审核状态.Text == "未审核") && _s住院号 != "")
                {
                    // MessageBox.Show("第一次来计算年龄");
                    DataTable dtPat = new DataTable();
                    //2015年5月18日注视掉
                    //2015-06-02 08:20:43于凤海修改，先取本地数据库，如果不存在则从接口视图中获取
                    dtPat = this.bllPatient.BllPatientDT(" and fhz_zyh='" + _s住院号 + "'");
                    if (dtPat == null || dtPat.Rows.Count == 0)
                        dtPat = this.bllPatient.BllPatientDTFromHisDBby住院号(_s住院号);

                    if ((dtPat != null) && (dtPat.Rows.Count > 0))
                    {
                        sam_jy.fhz_id = dtPat.Rows[0]["fhz_id"].ToString();

                        #region 加载病人信息时，防止对检验项目列表中的参考值多次更新，所以将TextChanged参考值去掉 add by wjz 20151204
                        //fhz_age年龄.TextChanged -= fhz_age年龄_TextChanged;
                        //fhz_sex性别.TextChanged -= fhz_sex性别_TextChanged;
                        //fhz_age_unittextBox.TextChanged -= fhz_age_unittextBox_TextChanged;
                        RemoveSexAgeTextChangedEvent();
                        #endregion

                        //2015-12-02 changed by wjz 门诊病人的年龄、年龄单位 ▽
                        //从HIS视图中查询出的门诊病人信息中，remark字段的值格式为：  “身份证号|年龄单位:年龄”（不包含引号）
                        if (dtPat.Rows[0]["ftype_id"].ToString() == "1")
                        {
                            string remarktemp = dtPat.Rows[0]["fremark"].ToString();

                            string[] remarks = remarktemp.Split('|');
                            if (remarks.Length == 2)
                            {
                                //remarks[0]里存储的是身份证号，先根据身份证号计算年龄，若身份证号是空，再取门诊里的年龄、年龄单位
                                if (remarks[0].Length == 18)
                                {
                                    string mzbirthday = remarks[0].Substring(6, 4) + "-" + remarks[0].Substring(10, 2) + "-" + remarks[0].Substring(12, 2);

                                    string mzGetAge = this.bllPatient.BllPatientAge(mzbirthday);
                                    if (!(string.IsNullOrWhiteSpace(mzGetAge)))
                                    {
                                        string[] sPatientAge = mzGetAge.Split(':');

                                        //2015-05-18代码位置做了调整
                                        //this.fhz_nametextBox.Text = dtPat.Rows[0]["fname"].ToString();//姓名
                                        //this.fhz_bedtextBox.Text = dtPat.Rows[0]["fbed_num"].ToString();//床号

                                        this.txt年龄.Text = sPatientAge[1].ToString();//年龄

                                        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                                        //年龄单位
                                        //this.fhz_age_unittextBox.Text = sPatientAge[0].ToString();
                                        sam_jy.fhz_age_unit = bllPatNameByCode("年龄单位", sPatientAge[0].ToString());
                                        this.cmb年龄单位.SelectedValue = sam_jy.fhz_age_unit;
                                        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                                        //2015-05-18注视掉
                                    }
                                }
                                else if (remarks[1].Length > 1)
                                {
                                    string[] mzage = remarks[1].Split(':');
                                    this.txt年龄.Text = mzage[1].ToString();//年龄

                                    //年龄单位
                                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                                    //this.fhz_age_unittextBox.Text = mzage[0].ToString();
                                    sam_jy.fhz_age_unit = bllPatNameByCode("年龄单位", mzage[0].ToString());
                                    this.cmb年龄单位.SelectedValue = this.sam_jy.fhz_age_unit;
                                    //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                                }
                                else { }
                            }
                        }
                        else //此处的else 与下方的if是匹配的
                             //2015-12-02 changed by wjz 门诊病人的年龄、年龄单位 △
                             //2015-05-18注视掉
                             //if (dtPat.Rows[0]["fbirthday"] != null)
                            if (!(string.IsNullOrWhiteSpace(dtPat.Rows[0]["fbirthday"].ToString())))
                        {
                            string strfbirthday = dtPat.Rows[0]["fbirthday"].ToString();
                            string strGetAge = this.bllPatient.BllPatientAge(strfbirthday);
                            //2015-05-18注视掉 下方的if内语句也做了调整
                            //if (strGetAge != "" || strGetAge != null)
                            if (!(string.IsNullOrWhiteSpace(strGetAge)))
                            {
                                string[] sPatientAge = strGetAge.Split(':');

                                //2015-05-18代码位置做了调整
                                //this.fhz_nametextBox.Text = dtPat.Rows[0]["fname"].ToString();//姓名
                                //this.fhz_bedtextBox.Text = dtPat.Rows[0]["fbed_num"].ToString();//床号

                                this.txt年龄.Text = sPatientAge[1].ToString();//年龄

                                //年龄单位
                                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                                //this.fhz_age_unittextBox.Text = sPatientAge[0].ToString();
                                sam_jy.fhz_age_unit = bllPatNameByCode("年龄单位", sPatientAge[0].ToString());
                                this.cmb年龄单位.SelectedValue = sam_jy.fhz_age_unit;
                                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                                //2015-05-18注视掉
                            }
                        }




                        //2015-05-18代码位置做了调整
                        this.txt姓名.Text = dtPat.Rows[0]["fname"].ToString();//姓名
                        this.txt床号.Text = dtPat.Rows[0]["fbed_num"].ToString();//床号

                        //性别
                        //his数据库中的视图，直接取出的是汉字男女，在这需要转换一下，对应关系1-男 2-女，
                        //string str性别 = dtPat.Rows[0]["fsex"].ToString();
                        //modeljy.fhz_性别 = (str性别.Equals("男")) ? "1" : "2";
                        sam_jy.fhz_sex = dtPat.Rows[0]["fsex"].ToString();

                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                        //this.fhz_sex性别.Text = bllPatNameByCode("性别", modeljy.fhz_性别);//=============
                        this.cmb性别.SelectedValue = sam_jy.fhz_sex;
                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                        //病人类别
                        //HIS数据库的视图中，门诊病人的类型设定为了1，住院病人的类型为3，这点与页面程序的选项保持一致
                        sam_jy.fhz_type_id = dtPat.Rows[0]["ftype_id"].ToString();

                        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
                        //this.fhz_type_idtextBox_name.Text = bllPatNameByCode("病人类别", modeljy.fhz_患者类型id);
                        this.cmb病人类型.SelectedValue = sam_jy.fhz_type_id;
                        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △

                        this.cmb费别.Text = dtPat.Rows[0]["ffb_id"].ToString();
                        this.sam_jy.fjy_sf_type = this.cmb费别.SelectedValue.ToString();

                        //科室
                        sam_jy.fhz_dept = dtPat.Rows[0]["fdept_id"].ToString();

                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                        //this.fhz_dept科室.Text = bllPatNameByCode("科室", modeljy.fhz_dept患者科室);
                        try
                        {
                            this.cmb科室.SelectedValue = this.sam_jy.fhz_dept;
                        }
                        catch
                        {
                            this.cmb科室.SelectedValue = "";
                        }
                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                        //人员
                        sam_jy.fapply_user_id = dtPat.Rows[0]["fsjys_id"].ToString();
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                        //this.fapply_user_id送检医师.Text = bllPatNameByCode("人员", modeljy.fapply_user_id);
                        try
                        {
                            this.cmb送检医师.EditValue = this.sam_jy.fapply_user_id;
                        }
                        catch
                        {
                            this.cmb送检医师.EditValue = "";
                        }
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

                        //临床诊断
                        //chanaged begin 2015-05-28
                        //del by wjz 20160216 删除临床诊断的获取 ▽
                        sam_jy.fjy_lczd = dtPat.Rows[0]["fdiagnose"].ToString();
                        this.txt临床诊断.Text = bllPatNameByCode("临床诊断", sam_jy.fjy_lczd);
                        //del by wjz 20160216 删除临床诊断的获取 △
                        //string strICD = dtPat.Rows[0]["ICD码"].ToString();
                        //if(string.IsNullOrWhiteSpace(strICD) || strICD.Trim().Equals("-1"))
                        //{
                        //    strICD = dtPat.Rows[0]["fdiagnose"].ToString();
                        //}
                        //modeljy.fjy_lczd = strICD;
                        //this.fjy_lczdtextBox.Text = dtPat.Rows[0]["fdiagnose"].ToString();
                        //changed end 2015-05-28

                        sam_jy.sfzh = dtPat.Rows[0]["sfzh"].ToString();
                        this.txt身份证号.Text = sam_jy.sfzh;
                        if (dtPat.Columns.Contains("card_no"))
                        {
                            sam_jy.f1 = dtPat.Rows[0]["card_no"].ToString();
                            this.txt电子健康卡.Text = sam_jy.f1;
                        }
                        //2015-05-18注视掉
                        //}
                        //}

                        #region 加载病人信息时，防止对检验项目列表中的参考值多次更新，所以将TextChanged参考值去掉 add by wjz 20151204
                        //加载性别前，将TextChanged事件再还原回去
                        //fhz_age年龄.TextChanged += fhz_age年龄_TextChanged;
                        //fhz_sex性别.TextChanged += fhz_sex性别_TextChanged;
                        //fhz_age_unittextBox.TextChanged += fhz_age_unittextBox_TextChanged;
                        AddSexAgeTextChangedEvent();
                        #endregion
                    }
                    else
                    {
                        sam_jy.fhz_id = ""; //如果没有获取到患者信息，把id置空让系统重新生成guid
                        this.txt姓名.Text = "";//姓名
                        this.txt年龄.Text = "";//年龄

                        //年龄单位
                        sam_jy.fhz_age_unit = "1";

                        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                        //this.fhz_age_unittextBox.Text = "岁";
                        this.cmb年龄单位.SelectedValue = sam_jy.fhz_age_unit;
                        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △


                        //性别
                        sam_jy.fhz_sex = "";

                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                        //this.fhz_sex性别.Text = "";//===========
                        this.cmb性别.SelectedValue = "";
                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                        //病人类别
                        //  modeljy.fhz_type_id = "";
                        // this.fhz_type_idtextBox_name.Text = "";


                        //科室
                        sam_jy.fhz_dept = "";

                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                        //this.fhz_dept科室.Text = "";
                        this.cmb科室.SelectedValue = "";
                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                        //人员
                        sam_jy.fapply_user_id = "";
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                        //this.fapply_user_id送检医师.Text = "";
                        this.cmb送检医师.EditValue = "";
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

                        //临床诊断
                        sam_jy.fjy_lczd = "";
                        this.txt临床诊断.Text = "";
                        //身份证
                        this.txt身份证号.Text = "";
                        //电子卡
                        this.txt电子健康卡.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        //20150629 wjz add
        private string blPatCodeByName(string strType, string strName)
        {
            string strCode = "";
            if (strType.Equals("科室"))
            {
                strCode = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fdept_id FROM WWF_DEPT WHERE (fname = '" + strName + "')");
            }
            else if (strType.Equals("性别"))
            {
                if (strName.Equals("男"))
                {
                    strCode = "1";
                }
                else if (strName.Equals("女"))
                {
                    strCode = "2";
                }
            }
            else if (strType.Equals("病人类型"))
            {
                if (strName.Equals("门诊"))
                {
                    strCode = "1";
                }
                else if (strName.Equals("急诊"))
                {
                    strCode = "2";
                }
                else if (strName.Equals("住院"))
                {
                    strCode = "3";
                }
                else if (strName.Equals("查体"))
                {
                    strCode = "4";
                }
            }

            return strCode;
        }

        private string bllPatNameByCode(string strType, string strCode)
        {
            string strName = "";
            if (strType.Equals("年龄单位"))
            {
                if (strCode.Equals("岁"))
                {
                    strName = "1";
                }
                else if (strCode.Equals("月"))
                {
                    strName = "2";
                }
                else if (strCode.Equals("天"))
                {
                    strName = "3";
                }
            }
            else if (strType.Equals("性别"))
            {
                if (strCode.Equals("1"))
                {
                    strName = "男";
                }
                else if (strCode.Equals("2"))
                {
                    strName = "女";
                }
                ////2015-05-18添加else
                //else
                //{
                //    strName = strCode;
                //}

            }
            else if (strType.Equals("病人类别"))
            {
                if (strCode.Equals("1"))
                {
                    strName = "门诊";
                }
                else if (strCode.Equals("2"))
                {
                    strName = "急诊";
                }
                else if (strCode.Equals("3"))
                {
                    strName = "住院";
                }
                else if (strCode.Equals("4"))
                {
                    strName = "查体";
                }
                ////2015-05-18添加else
                //else
                //{
                //    strName = strCode;
                //}
            }
            else if (strType.Equals("科室"))
            {
                strName = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT fname FROM WWF_DEPT WHERE (fdept_id = '" + strCode + "')");
                ////2015-05-18添加if处理
                //if(string.IsNullOrWhiteSpace((strName)))
                //{
                //    strName = strCode;
                //}
            }
            else if (strType.Equals("人员"))
            {
                strName = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT  fname FROM   WWF_PERSON WHERE  (fperson_id = '" + strCode + "')");
                ////2015-05-18添加if处理
                //if (string.IsNullOrWhiteSpace((strName)))
                //{
                //    strName = strCode;
                //}
            }
            else if (strType.Equals("临床诊断"))
            {
                strName = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT  fname FROM  SAM_DISEASE WHERE  (fcode = '" + strCode + "')");
                //2015-05-18添加if处理
                if (string.IsNullOrWhiteSpace((strName)))
                {
                    strName = strCode;
                }
            }
            else
            {
                ////2015-05-18注释掉
                strName = "";
                //strName = strCode;
            }
            //
            return strName;
        }
        /*
         1	病人类别	1	MZ	门诊	1	1	
3	病人类别	3	ZY	住院	3	1	NULL
2	病人类别	2	JZ	急诊	2	1	
         */

        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            //如果当前焦点在仪器、日期两个控件上，则禁止两个控件的操作
            if ((keyData == (Keys.Down) || keyData == (Keys.Up))
                && (this.ActiveControl == this.cmb检验仪器 || this.ActiveControl == this.dtp样本日期
                    || this.ActiveControl == this.cmb年龄单位 || this.ActiveControl == this.cmb核对医师
                    || this.ActiveControl == this.cmb性别 || this.ActiveControl == this.cmb样本类型
                    || this.ActiveControl == this.cmb送检医师 || this.ActiveControl == this.cmb检验医师
                    || this.ActiveControl == this.cmb核对医师 || this.ActiveControl == this.dtp送检时间
                    || this.ActiveControl == this.dtp采样时间 || this.ActiveControl == this.dtp报告时间
                    || this.ActiveControl == this.cmb病人类型
                    || this.ActiveControl == this.cmb费别
                    || this.ActiveControl == this.cmb科室
                    )
                )
            {
                return true;
            }
            else if (keyData == (Keys.F6))
            {
                button新增.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F9))
            {
                button审核.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F7))
            {
                this.button删除.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F8))
            {
                this.button仪器数据.PerformClick();
                return true;
            }

            else if (keyData == (Keys.F4) || keyData == (Keys.PageUp))
            {
                this.button上一个.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F5) || keyData == (Keys.PageDown))
            {
                this.button下一个.PerformClick();
                return true;
            }
            else if (keyData == (Keys.F2))
            {
                this.button打印.PerformClick();
                return true;
            }
            else if (keyData == Keys.Enter)
            {
                if (this.ActiveControl.Parent.Parent.Name == "dataGrid_SAM_JY_Result")
                    SendKeys.Send("{Down}");
                else
                    SendKeys.Send("{TAB}");
                return true;
            }
            else if (keyData == (Keys.Control | Keys.S))
            {
                bll保存或审核(MoveType.不变, OperType.保存);
            }
            //if ( this.textBox1.Focused)
            //{
            //    this.ActiveControl.BackColor = System.Drawing.Color.Bisque;
            //}
            //else
            //{
            //    this.ActiveControl.BackColor = System.Drawing.Color.White;
            //}

            //  private void fhz_type_idtextBox_name_Enter(object sender, EventArgs e)
            // {
            // this.fhz_type_idtextBox_name.BackColor = System.Drawing.Color.Bisque;  
            //this.ActiveControl.BackColor = System.Drawing.Color.Bisque;
            //  }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        #endregion





        private void button上一个_Click(object sender, EventArgs e)
        {
            RemoveSexAgeTextChangedEvent();

            bll保存或审核(MoveType.不变, OperType.上一个);
            //this.ActiveControl = fhz_type_idtextBox_name; 
            //bindingSource样本.MovePrevious();  
            this.ActiveControl = txt样本号;


            AddSexAgeTextChangedEvent();

        }

        private void button下一个_Click(object sender, EventArgs e)
        {
            RemoveSexAgeTextChangedEvent();
            bll保存或审核(MoveType.自增1, OperType.下一个);
            //bindingSource样本.MoveNext();
            this.ActiveControl = txt样本号;
            AddSexAgeTextChangedEvent();
        }

        private void button打印_Click(object sender, EventArgs e)
        {
            RemoveSexAgeTextChangedEvent();
            ////Next:此处是不是先审核更好？
            //if (!(sam_jy.fjy_zt.Equals("已审核")))
            //{
            //    this.print = true;
            //    bll保存或审核(MoveType.不变, OperType.保存);
            //}
            yunLis.lis.sam.Report.PrintHelper pr = new yunLis.lis.sam.Report.PrintHelper();
            bool printSuccess = pr.GetData_PrintViewer(sam_jy.fjy_id, false);
            if (printSuccess)
            {
                int rowIndex = -1;
                for (int index = 0; index < this.dataGridView样本.Rows.Count; index++)
                {
                    string strTemp = this.dataGridView样本.Rows[index].Cells["fjy_id"].Value.ToString();
                    if (sam_jy.fjy_id == strTemp)
                    {
                        rowIndex = index;
                        break;
                    }
                }

                if (rowIndex != -1)
                {
                    this.dataGridView样本.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    this.dataGridView样本.Rows[rowIndex].Cells["fprint_zt"].Value = "已打印";

                    string str状态 = this.dataGridView样本.Rows[rowIndex].Cells["fjy_zt"].Value.ToString();
                    if (!(str状态.Equals("已审核"))) //状态当前不是“已审核”状态
                    {
                        this.blljy.BllReportUpdate审核状态(sam_jy.fjy_id);
                        this.dataGridView样本.Rows[rowIndex].Cells["fjy_zt"].Value = "已审核";
                        sam_jy.fjy_zt = "已审核";
                        label状态.Visible = true;
                        this.dataGrid_SAM_JY_Result.Columns["fvalue"].ReadOnly = true;
                        string str日期 = dtp样本日期.Value.ToString("yyyy-MM-dd");
                        this.blljy.WriteDataToHisBy门诊住院号(sam_jy.fhz_zyh, str日期, cmb检验仪器.SelectedValue.ToString(), txt样本号.Text);
                    }
                }
            }
            //打印后不跳转套下一个 
            //bindingSource样本.MoveNext();
            this.Focus();
            this.ActiveControl = button打印;
            AddSexAgeTextChangedEvent();
            this.print = false;
        }

        private void bll显示样本()
        {
            try
            {
                this.Validate();
                bindingSource样本.EndEdit();
                this.dataGridView样本.EndEdit();
                row当前样本 = (Pojo报告单摘要)bindingSource样本.Current;
                if (row当前样本 != null)
                {
                    string strid = row当前样本.fjy_yb_code;
                    s当前样本Id = row当前样本.fjy_id;
                    this.txt样本号.Text = strid;
                    Refresh_Display_First_SAM_JY_and_Result();
                    bll比较();
                    label21比较住院号.Text = "住院号：" + txt门诊或住院号.Text;
                }
                else
                {
                    label21比较住院号.Text = "住院号：";
                    DataTable dtBJValue = new DataTable();
                    this.dataGridView比较.DataSource = dtBJValue;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }


            //try
            //{

            //    this.Validate();
            //    bindingSource样本.EndEdit();
            //    this.dataGridView样本.EndEdit();
            //    if (dataGridView样本.Rows.Count > 0)
            //    {
            //        string strid = this.dataGridView样本.CurrentRow.Cells["fjy_yb_code"].Value.ToString();
            //        strCurrSelfjy_id = this.dataGridView样本.CurrentRow.Cells["fjy_id"].Value.ToString();
            //        this.fjy_yb_codeTextBox.Text = strid;
            //        bllYBListByYbCode();
            //        bll比较();
            //        label21比较住院号.Text = "住院号：" + fhz_zyhtextBox.Text;
            //    }
            //    else
            //    {
            //        label21比较住院号.Text = "住院号：";
            //        DataTable dtBJValue = new DataTable();
            //        this.dataGridView1.DataSource = dtBJValue;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            //}

        }

        /// <summary>
        /// 数据源上移下移
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingSource样本_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                if (bFlagForPostionChanged)
                {
                    RemoveSexAgeTextChangedEvent();
                    bll显示样本();
                    AddSexAgeTextChangedEvent();
                }
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }
        /// <summary>
        /// 样本日期
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fjy_dateDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            //del 20151022 wjz
            //bllYBList();
            //del 20151022 wjz
        }

        /// <summary>
        /// 样本号 鼠标移开
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fjy_yb_codeTextBox_Leave(object sender, EventArgs e)
        {
            //changed by wjz 20151022 ▽
            //bllYBListByYbCode(); old code
            if (txt样本号.Text.Equals(txt样本号.Tag.ToString()) || string.IsNullOrWhiteSpace(txt样本号.Text))
            {
                txt样本号.Tag = "";
            }
            else
            {

                RemoveSexAgeTextChangedEvent();


                Refresh_Display_First_SAM_JY_and_Result();


                AddSexAgeTextChangedEvent();


            }
        }



        // <summary>
        // 样本号 回车
        // </summary>
        // <param name="sender"></param>
        // <param name="e"></param>
        //private void fjy_yb_codeTextBox_KeyDown(object sender, KeyEventArgs e)
        //{
        //    MessageBox.Show("到此");
        //   if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 

        //    {
        //        this.ActiveControl = fsample_barcodeTextBox;
        //        bllYBListByYbCode();
        //    }
        //}

        #region  病人类型
        /// <summary>
        /// 病人类型
        /// </summary>
        /// <param name="intType">1:双击一定打开弹出窗口 0:移动</param>
        private void bll病人类型(int intType)
        {
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //try
            //{
            //    string str默认 = "";
            //    string strtype = "病人类别";
            //    if (intType == 1)
            //    {
            //        using (com.HZTypeForm fm = new yunLis.lis.com.HZTypeForm(strtype))
            //        {
            //            if (fm.ShowDialog() != DialogResult.No)
            //            {
            //                modeljy.fhz_患者类型id = fm.strid;

            //                this.fhz_type_idtextBox_name.Text = fm.strname;

            //                str默认 = fm.str备注;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        //if (fhz_type_idtextBox_name.Text == "")
            //        //    fhz_type_idtextBox_name.Text = "1";
            //        DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.fhz_type_idtextBox_name.Text.Trim(), this.fhz_type_idtextBox_name.Text.Trim());
            //        if (dt.Rows.Count > 0)
            //        {
            //            modeljy.fhz_患者类型id = dt.Rows[0]["fcode"].ToString();
            //            this.fhz_type_idtextBox_name.Text = dt.Rows[0]["fname"].ToString();
            //            str默认 = dt.Rows[0]["fremark"].ToString();
            //        }
            //        else
            //        {
            //            //using (com.HZTypeForm fm = new yunLis.lis.com.HZTypeForm(strtype))
            //            //{
            //            //    fm.ShowDialog();
            //            //    modeljy.fhz_患者类型id = fm.strid;
            //            //    this.fhz_type_idtextBox_name.Text = fm.strname;
            //            //}
            //        }
            //    }
            //    try
            //    {
            //        if (str默认 != "")
            //        {//添加体检类型默认处理
            //            string[] aa = str默认.Split(';');
            //            if (aa.Length > 0)
            //            {
            //                foreach (string ss in aa)
            //                {
            //                    string[] bb = ss.Split(',');
            //                    if (bb.Length > 0)
            //                    {
            //                        if (bb[0].ToString() == "科室")
            //                            fhz_dept科室.Text = bb[1].ToString();
            //                        if (bb[0].ToString() == "送检医师")
            //                            fapply_user_id送检医师.Text = bb[1].ToString();
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    catch   { }
            //    this.ActiveControl = fhz_zyh住院号;
            //}
            //catch (Exception ex)
            //{
            //    WWMessage.MessageShowError(ex.ToString());
            //}
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
        }
        /// <summary>
        /// 病人类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fhz_type_idtextBox_name_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //bll病人类型(1);
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
        }
        private void fhz_type_idtextBox_name_Enter(object sender, EventArgs e)
        {
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //this.fhz_type_idtextBox_name.BackColor = System.Drawing.Color.Bisque;
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
        }
        private void fhz_type_idtextBox_name_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //bll病人类型(0);
            //this.fhz_type_idtextBox_name.BackColor = System.Drawing.Color.White;
            //del by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
        }

        #endregion

        #region 住院号

        private void fhz_zyhtextBox_Enter(object sender, EventArgs e)
        {
            this.txt门诊或住院号.BackColor = System.Drawing.Color.Bisque;

            //add by wjz 20160121 如果值没有变化，则不执行病人信息的查询 ▽
            this.txt门诊或住院号.Tag = this.txt门诊或住院号.Text.Trim();
            //add by wjz 20160121 如果值没有变化，则不执行病人信息的查询 △
        }
        private void fhz_zyhtextBox_Leave(object sender, EventArgs e)
        {
            this.txt门诊或住院号.BackColor = System.Drawing.Color.White;

            //add by wjz 20160121 如果值没有变化，则不执行病人信息的查询 ▽
            try
            {
                if ((this.txt门诊或住院号.Tag.ToString() == this.txt门诊或住院号.Text.Trim())
                    || string.IsNullOrWhiteSpace(this.txt门诊或住院号.Text))
                {
                    return;
                }
            }
            catch
            {
                return;
            }
            //add by wjz 20160121 如果值没有变化，则不执行病人信息的查询 △

            //如果是通过申请号获取了病人信息，则不再通过住院号获取病人信息
            //changed by wjz 20160121 修改逻辑处理，如果住院号发生变更的话，不管是否有申请号，都重新查询病人信息 ▽
            //string str申请号 = this.fapply_idtextBox.Text;
            //if(str申请号.Length > 0)
            //{
            //    return;
            //}
            this.txt申请号.Text = "";
            //changed by wjz 20160121 修改逻辑处理，如果住院号发生变更的话，不管是否有申请号，都重新查询病人信息 △


            RemoveSexAgeTextChangedEvent();


            //Begin 2015-05-28 修改
            string str门诊住院号 = txt门诊或住院号.Text;

            bllPatInfo(str门诊住院号);
            vSet参考值();
            AddSexAgeTextChangedEvent();

        }
        #endregion

        #region  性别
        private void bll病人性别(int intType)
        {
            try
            {
                string strtype = "性别";
                if (intType == 1)
                {
                    using (com.HZTypeForm fm = new yunLis.lis.com.HZTypeForm(strtype))
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            sam_jy.fhz_sex = fm.strid;

                            //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                            //this.fhz_sex性别.Text = fm.strname;//==========
                            this.cmb性别.SelectedValue = sam_jy.fhz_sex;
                            //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                        }
                    }
                }
                else
                {
                    //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                    //DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.fhz_sex性别.Text.Trim(), this.fhz_sex性别.Text.Trim());//=========
                    DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.cmb性别.Text.Trim(), this.cmb性别.Text.Trim());
                    //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                    if (dt.Rows.Count > 0)
                    {
                        sam_jy.fhz_sex = dt.Rows[0]["fcode"].ToString();

                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                        //this.fhz_sex性别.Text = dt.Rows[0]["fname"].ToString();//===================
                        this.cmb性别.SelectedValue = sam_jy.fhz_sex;
                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                    }
                    else
                    {
                        //20150618 wjz 注释掉， 原因：化验室希望离开性别文本框时，没有窗口弹出
                        //using (com.HZTypeForm fm = new yunLis.lis.com.HZTypeForm(strtype))
                        //{
                        //    fm.ShowDialog();
                        //    modeljy.fhz_性别 = fm.strid;
                        //    this.fhz_sex性别.Text = fm.strname;
                        //}
                    }
                }
                //this.ActiveControl = fhz_age年龄;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }


        private void fhz_sextextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //bll病人性别(1);
            //del by wjz 20160128 性别控件由TextBox换为ComboBox △
        }
        private void fhz_sextextBox_Enter(object sender, EventArgs e)
        {
            //del by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //this.fhz_sex性别.BackColor = System.Drawing.Color.Bisque;
            //del by wjz 20160128 性别控件由TextBox换为ComboBox △
        }

        private void fhz_sextextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //bll病人性别(0);
            //this.fhz_sex性别.BackColor = System.Drawing.Color.White;
            //del by wjz 20160128 性别控件由TextBox换为ComboBox △
        }

        #endregion

        #region 年龄单位
        private void bll年龄单位(int intType)
        {
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //try
            //{
            //    string strtype = "年龄单位";
            //    if (intType == 1)
            //    {
            //        using (com.HZTypeForm fm = new yunLis.lis.com.HZTypeForm(strtype))
            //        {
            //            fm.ShowDialog();
            //            modeljy.fhz_年龄单位 = fm.strid;
            //            this.fhz_age_unittextBox.Text = fm.strname;
            //        }
            //    }
            //    else
            //    {
            //        DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.fhz_age_unittextBox.Text.Trim(), this.fhz_age_unittextBox.Text.Trim());
            //        if (dt.Rows.Count > 0)
            //        {
            //            modeljy.fhz_年龄单位 = dt.Rows[0]["fcode"].ToString();
            //            this.fhz_age_unittextBox.Text = dt.Rows[0]["fname"].ToString();
            //        }
            //        else
            //        {
            //            using (com.HZTypeForm fm = new yunLis.lis.com.HZTypeForm(strtype))
            //            {
            //                fm.ShowDialog();
            //                modeljy.fhz_年龄单位 = fm.strid;
            //                this.fhz_age_unittextBox.Text = fm.strname;
            //            }
            //        }
            //    }
            //    //this.ActiveControl = fjy_sf_typetextBox;
            //}
            //catch (Exception ex)
            //{
            //    WWMessage.MessageShowError(ex.ToString());
            //}
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △

        }
        private void fhz_age_unittextBox_DoubleClick(object sender, EventArgs e)
        {
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //bll年龄单位(1);
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
        }

        private void fhz_age_unittextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //this.fhz_age_unittextBox.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
        }

        private void fhz_age_unittextBox_Leave(object sender, EventArgs e)
        {
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
            //bll年龄单位(0);
            //this.fhz_age_unittextBox.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
        }
        #endregion

        #region 年龄
        private void fhz_agetextBox_Enter(object sender, EventArgs e)
        {
            this.txt年龄.BackColor = System.Drawing.Color.Bisque;

            //add by wjz 20151205 防止年龄修改时，TextChanged事件被多次调用 ▽
            txt年龄.TextChanged -= fhz_age年龄_TextChanged;
            this.txt年龄.Tag = this.txt年龄.Text;
            //add by wjz 20151205 防止年龄修改时，TextChanged事件被多次调用 △
        }

        private void fhz_agetextBox_Leave(object sender, EventArgs e)
        {
            this.txt年龄.BackColor = System.Drawing.Color.White;

            //add by wjz 20151205 防止年龄修改时，TextChanged事件被多次调用 ▽
            if (this.txt年龄.Text.Trim() == this.txt年龄.Tag.ToString().Trim())
            { }
            else
            {
                fhz_age年龄_TextChanged(null, null);
            }
            txt年龄.TextChanged -= fhz_age年龄_TextChanged;
            txt年龄.TextChanged += fhz_age年龄_TextChanged;
            //add by wjz 20151205 防止年龄修改时，TextChanged事件被多次调用 △
        }
        #endregion

        #region 姓名
        private void fhz_nametextBox_Enter(object sender, EventArgs e)
        {
            this.txt姓名.BackColor = System.Drawing.Color.Bisque;
        }

        private void fhz_nametextBox_Leave(object sender, EventArgs e)
        {
            this.txt姓名.BackColor = System.Drawing.Color.White;
        }
        #endregion

        #region 费别
        private void bll费别(int intType)
        {
            try
            {
                string strtype = "费别";
                if (intType == 1)
                {
                    using (com.HZTypeForm fm = new yunLis.lis.com.HZTypeForm(strtype))
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            sam_jy.fjy_sf_type = fm.strid;
                            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                            //this.fjy_sf_typetextBox.Text = fm.strname;
                            this.cmb费别.SelectedValue = fm.strid;
                            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
                        }
                    }
                }
                else
                {
                    //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                    DataTable dt = this.bllType.BllComTypeDTByCode(strtype, this.cmb费别.Text.Trim(), this.cmb费别.Text.Trim());
                    //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
                    if (dt.Rows.Count > 0)
                    {
                        sam_jy.fjy_sf_type = dt.Rows[0]["fcode"].ToString();
                        //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                        //this.fjy_sf_typetextBox.Text = dt.Rows[0]["fname"].ToString();
                        this.cmb费别.SelectedValue = dt.Rows[0]["fcode"].ToString();
                        //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
                    }
                    else
                    {
                        //using (com.HZTypeForm fm = new yunLis.lis.com.HZTypeForm(strtype))
                        //{
                        //    fm.ShowDialog();
                        //    modeljy.fjy_收费类型ID = fm.strid;
                        //    this.fjy_sf_typetextBox.Text = fm.strname;
                        //}
                    }
                }
                //this.ActiveControl = fhz_dept科室;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private void fjy_sf_typetextBox_DoubleClick(object sender, EventArgs e)
        {
            bll费别(1);
        }
        private void fjy_sf_typetextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
            //this.fjy_sf_typetextBox.BackColor = System.Drawing.Color.Bisque;
            this.cmb费别.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
        }
        private void fjy_sf_typetextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
            //bll费别(0);
            //del by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
            //this.fjy_sf_typetextBox.BackColor = System.Drawing.Color.White;
            this.cmb费别.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △
        }
        #endregion

        #region 科室
        private void bll科室(int intType)
        {
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
            //try
            //{
            //    if (intType == 1)
            //    {
            //        using (com.HZDeptSelForm fm = new yunLis.lis.com.HZDeptSelForm())
            //        {
            //            if (fm.ShowDialog() != DialogResult.No)
            //            {
            //                modeljy.fhz_dept患者科室 = fm.strid;
            //                this.fhz_dept科室.Text = fm.strname;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        DataTable dt = this.bllDept.BllDeptDTByCode(this.fhz_dept科室.Text.Trim(), this.fhz_dept科室.Text.Trim());
            //        if (dt.Rows.Count > 0)
            //        {
            //            modeljy.fhz_dept患者科室 = dt.Rows[0]["fdept_id"].ToString();
            //            this.fhz_dept科室.Text = dt.Rows[0]["fname"].ToString();
            //        }
            //        else
            //        {
            //            //using (com.HZDeptSelForm fm = new yunLis.lis.com.HZDeptSelForm())
            //            //{
            //            //    fm.ShowDialog();
            //            //    modeljy.fhz_dept = fm.strid;
            //            //    this.fhz_depttextBox.Text = fm.strname;
            //            //}
            //        }
            //    }
            //    //this.ActiveControl = fhz_bedtextBox;
            //}
            //catch (Exception ex)
            //{
            //    WWMessage.MessageShowError(ex.ToString());
            //}
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
        }
        private void fhz_depttextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
            //bll科室(1);
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
        }

        private void fhz_depttextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
            //this.fhz_dept科室.BackColor = System.Drawing.Color.Bisque;
            this.cmb科室.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
        }

        private void fhz_depttextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
            //bll科室(0);
            //del by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

            //add by wjz 20160520 输入科室编码或名称，自动修改fhz_dept科室cbo的SelectedValue ▽
            try
            {
                string str科室Text = this.cmb科室.Text;
                DataTable dtDept = this.cmb科室.DataSource as DataTable;
                if (dtDept != null)
                {
                    DataRow[] drs = dtDept.Select("fdept_id='" + str科室Text + "' or fname='" + str科室Text + "'");
                    if (drs.Length == 0)
                    {
                        this.cmb科室.SelectedValue = "";
                    }
                    else if ((this.cmb科室.SelectedValue == null)
                        || ((this.cmb科室.SelectedValue != null) && (drs[0]["fdept_id"].ToString() != this.cmb科室.SelectedValue.ToString())))
                    {
                        this.cmb科室.SelectedValue = drs[0]["fdept_id"].ToString();
                    }
                }
            }
            catch
            {
                this.cmb科室.SelectedValue = "";
            }
            //add by wjz 20160520 输入科室编码或名称，自动修改fhz_dept科室cbo的SelectedValue △

            //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
            //this.fhz_dept科室.BackColor = System.Drawing.Color.White;
            this.cmb科室.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
        }

        //add by wjz 20160520 输入科室编码或名称，自动修改fhz_dept科室cbo的SelectedValue ▽
        private DateTime dt科室1;
        private DateTime dt科室2;
        private void fhz_dept科室cbo_Click(object sender, EventArgs e)
        {
            dt科室2 = DateTime.Now;
            if (dt科室1 != null && (dt科室2 - dt科室1).TotalSeconds < 0.5)
            {
                using (com.HZDeptSelForm fm = new yunLis.lis.com.HZDeptSelForm())
                {
                    if (fm.ShowDialog() != DialogResult.No)
                    {
                        this.cmb科室.SelectedValue = fm.strid;
                    }
                }
            }
            dt科室1 = dt科室2;
        }
        //add by wjz 20160520 输入科室编码或名称，自动修改fhz_dept科室cbo的SelectedValue △

        #endregion

        private void fhz_bedtextBox_Enter(object sender, EventArgs e)
        {
            this.txt床号.BackColor = System.Drawing.Color.Bisque;
        }

        private void fhz_bedtextBox_Leave(object sender, EventArgs e)
        {
            this.txt床号.BackColor = System.Drawing.Color.White;
        }

        private void txt申请号_Enter(object sender, EventArgs e)
        {
            this.txt申请号.BackColor = System.Drawing.Color.Bisque;
        }

        private void fapply_idtextBox_Leave(object sender, EventArgs e)
        {
            this.txt申请号.BackColor = System.Drawing.Color.White;

        }
        #region 样本类型
        //  WindowsSampleTypeForm fcac = new WindowsSampleTypeForm(r);
        private void bll样本类型(int intType)
        {
            try
            {
                if (intType == 1)
                {
                    using (com.WindowsSampleTypeForm fm = new yunLis.lis.com.WindowsSampleTypeForm())
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            sam_jy.fjy_yb_type = fm.strid;

                            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                            //this.fjy_yb_typetextBox.Text = fm.strname;
                            this.cmb样本类型.SelectedValue = fm.strid;
                            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
                        }

                        //add by wjz 化验项目参考值与样本类型有关系 ▽
                        else
                        {
                            sam_jy.fjy_yb_type = "";
                            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                            //this.fjy_yb_typetextBox.Text = "";
                            this.cmb样本类型.SelectedValue = "";
                            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
                        }
                        //add by wjz 化验项目参考值与样本类型有关系 △
                    }
                }
                else
                {
                    //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                    //DataTable dt = this.bllSampleType.BllDTByCode(this.fjy_yb_typetextBox.Text.Trim(), this.fjy_yb_typetextBox.Text.Trim());
                    //if (dt.Rows.Count > 0)
                    //{
                    //    modeljy.fjy_yb_type = dt.Rows[0]["fsample_type_id"].ToString();
                    //    this.fjy_yb_typetextBox.Text = dt.Rows[0]["fname"].ToString();
                    //}
                    DataTable dt = this.bllSampleType.BllDTByCode(this.cmb样本类型.Text.Trim(), this.cmb样本类型.Text.Trim());
                    if (dt.Rows.Count > 0)
                    {
                        sam_jy.fjy_yb_type = dt.Rows[0]["fsample_type_id"].ToString();
                        this.cmb样本类型.SelectedValue = sam_jy.fjy_yb_type;
                    }
                    //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
                    //else
                    //{
                    //    using (com.WindowsSampleTypeForm fm = new yunLis.lis.com.WindowsSampleTypeForm())
                    //    {
                    //        fm.ShowDialog();
                    //        modeljy.fjy_yb_type = fm.strid;
                    //        this.fjy_yb_typetextBox.Text = fm.strname;
                    //    }
                    //}
                }
                //this.ActiveControl = fapply_user_id送检医师;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private void fjy_yb_typetextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //bll样本类型(1);
            //del by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △

            // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整 ▽
            if (string.IsNullOrWhiteSpace(sam_jy.fjy_yb_type))
            {
            }
            else
            {
                vSet参考值();
            }
            // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整 △
        }

        private void fjy_yb_typetextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //this.fjy_yb_typetextBox.BackColor = System.Drawing.Color.Bisque;
            this.cmb样本类型.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
        }

        private void fjy_yb_typetextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //bll样本类型(0);
            //del by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //this.fjy_yb_typetextBox.BackColor = System.Drawing.Color.White;
            this.cmb样本类型.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △
        }
        #endregion


        #region 送检医师
        private void bll人员(int intType, string strPersonType)
        {
            try
            {
                if (intType == 1)
                {
                    using (com.WindowsPersonForm fm = new yunLis.lis.com.WindowsPersonForm())
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            if (strPersonType.Equals("送检医师"))
                            {
                                sam_jy.fapply_user_id = fm.strid;
                                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                                //this.fapply_user_id送检医师.Text = fm.strname;
                                try
                                {
                                    this.cmb送检医师.EditValue = fm.strid;
                                }
                                catch
                                {
                                    this.cmb送检医师.EditValue = "";
                                }
                                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
                            }
                            else if (strPersonType.Equals("检验医师"))
                            {
                                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                                sam_jy.fjy_user_id = fm.strid;
                                //this.fjy_user_idtextBox.Text = fm.strname;
                                try
                                {
                                    this.cmb检验医师.EditValue = fm.strid;
                                }
                                catch
                                {
                                    this.cmb检验医师.EditValue = "";
                                }
                                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
                            }
                            else if (strPersonType.Equals("核对医师"))
                            {
                                sam_jy.fchenk_user_id = fm.strid;

                                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                                //this.fchenk_user_idtextBox.Text = fm.strname;
                                try
                                {
                                    this.cmb核对医师.EditValue = fm.strid;
                                }
                                catch
                                {
                                    this.cmb核对医师.EditValue = "";
                                }
                                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
                            }
                        }
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    if (strPersonType.Equals("送检医师"))
                    {
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                        //dt = this.bllPerson.BllDTByCode(this.fapply_user_id送检医师.Text.Trim(), this.fapply_user_id送检医师.Text.Trim());
                        //if (dt.Rows.Count > 0)
                        //{
                        //    modeljy.fapply_user_id = dt.Rows[0]["fperson_id"].ToString();
                        //    this.fapply_user_id送检医师.Text = dt.Rows[0]["fname"].ToString();
                        //}
                        //else
                        //{
                        //    //using (com.WindowsPersonForm fm = new yunLis.lis.com.WindowsPersonForm())
                        //    //{
                        //    //    fm.ShowDialog();
                        //    //    modeljy.fapply_user_id = fm.strid;
                        //    //    this.fapply_user_id送检医师.Text = fm.strname;
                        //    //}
                        //}
                        dt = this.bllPerson.BllDTByCode(this.cmb送检医师.Text.Trim(), this.cmb送检医师.Text.Trim());
                        if (dt.Rows.Count > 0)
                        {
                            sam_jy.fapply_user_id = dt.Rows[0]["fperson_id"].ToString();
                            try
                            {
                                this.cmb送检医师.EditValue = dt.Rows[0]["fperson_id"].ToString();
                            }
                            catch
                            {
                                this.cmb送检医师.EditValue = "";
                            }
                        }
                        else
                        {
                            //using (com.WindowsPersonForm fm = new yunLis.lis.com.WindowsPersonForm())
                            //{
                            //    fm.ShowDialog();
                            //    modeljy.fapply_user_id = fm.strid;
                            //    this.fapply_user_id送检医师.Text = fm.strname;
                            //}
                        }
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
                    }
                    else if (strPersonType.Equals("检验医师"))
                    {
                        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                        //dt = this.bllPerson.BllDTByCode(this.fjy_user_idtextBox.Text.Trim(), this.fjy_user_idtextBox.Text.Trim());
                        //if (dt.Rows.Count > 0)
                        //{
                        //    modeljy.fjy_user_id = dt.Rows[0]["fperson_id"].ToString();
                        //    this.fjy_user_idtextBox.Text = dt.Rows[0]["fname"].ToString();
                        //}
                        //else
                        //{
                        //    //using (com.WindowsPersonForm fm = new yunLis.lis.com.WindowsPersonForm())
                        //    //{
                        //    //    fm.ShowDialog();
                        //    //    modeljy.fjy_user_id = fm.strid;
                        //    //    this.fjy_user_idtextBox.Text = fm.strname;
                        //    //}
                        //}
                        dt = this.bllPerson.BllDTByCode(this.cmb检验医师.Text.Trim(), this.cmb检验医师.Text.Trim());
                        if (dt.Rows.Count > 0)
                        {
                            sam_jy.fjy_user_id = dt.Rows[0]["fperson_id"].ToString();
                            try
                            {
                                this.cmb检验医师.EditValue = dt.Rows[0]["fperson_id"].ToString();
                            }
                            catch
                            {
                                this.cmb检验医师.EditValue = "";
                            }
                        }
                        else
                        {
                            //using (com.WindowsPersonForm fm = new yunLis.lis.com.WindowsPersonForm())
                            //{
                            //    fm.ShowDialog();
                            //    modeljy.fjy_user_id = fm.strid;
                            //    this.fjy_user_idtextBox.Text = fm.strname;
                            //}
                        }
                        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
                    }
                    else if (strPersonType.Equals("核对医师"))
                    {
                        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                        //dt = this.bllPerson.BllDTByCode(this.fchenk_user_idtextBox.Text.Trim(), this.fchenk_user_idtextBox.Text.Trim());
                        //if (dt.Rows.Count > 0)
                        //{
                        //    modeljy.fchenk_审核医师ID = dt.Rows[0]["fperson_id"].ToString();
                        //    this.fchenk_user_idtextBox.Text = dt.Rows[0]["fname"].ToString();
                        //}
                        //else
                        //{
                        //    //using (com.WindowsPersonForm fm = new yunLis.lis.com.WindowsPersonForm())
                        //    //{
                        //    //    fm.ShowDialog();
                        //    //    modeljy.fchenk_审核医师ID = fm.strid;
                        //    //    this.fchenk_user_idtextBox.Text = fm.strname;
                        //    //}
                        //}
                        dt = this.bllPerson.BllDTByCode(this.cmb核对医师.Text.Trim(), this.cmb核对医师.Text.Trim());
                        if (dt.Rows.Count > 0)
                        {
                            sam_jy.fchenk_user_id = dt.Rows[0]["fperson_id"].ToString();
                            this.cmb核对医师.EditValue = dt.Rows[0]["fperson_id"].ToString();
                        }
                        else
                        {
                            //using (com.WindowsPersonForm fm = new yunLis.lis.com.WindowsPersonForm())
                            //{
                            //    fm.ShowDialog();
                            //    modeljy.fchenk_审核医师ID = fm.strid;
                            //    this.fchenk_user_idtextBox.Text = fm.strname;
                            //}
                        }
                        //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
                    }
                }
                //if (strPersonType.Equals("送检医师"))
                //{
                //    this.ActiveControl = fapply_timeDateTimePicker;
                //}
                //else if (strPersonType.Equals("检验医师"))
                //{
                //    this.ActiveControl = fchenk_user_idtextBox;
                //}
                //else if (strPersonType.Equals("核对医师"))
                //{
                //    this.ActiveControl = fjy_lczdtextBox;
                //}
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private void fapply_user_idtextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(1, "送检医师");
            //del by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
        }

        private void fapply_user_idtextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
            //this.fapply_user_id送检医师.BackColor = System.Drawing.Color.Bisque;
            this.cmb送检医师.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
        }

        private void fapply_user_idtextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(0, "送检医师");
            //del by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

            //add by wjz 允许输入医师姓名或医师编码 ▽
            try
            {
                //string strApplyuser = fapply_user_id送检医师cbo.Text;
                //DataTable dtApplyUser = this.fapply_user_id送检医师cbo.DataSource as DataTable;
                //DataRow[] drs = dtApplyUser.Select("fperson_id='" + strApplyuser + "' or fname='" + strApplyuser + "' ");
                //if (drs.Length == 0)
                //{
                //    this.fapply_user_id送检医师cbo.SelectedValue = "";
                //}
                //else// if (drs[0]["fperson_id"].ToString() != strApplyuser)
                //{
                //    this.fapply_user_id送检医师cbo.SelectedValue = drs[0]["fperson_id"].ToString();
                //}
            }
            catch
            {
                this.cmb送检医师.EditValue = "";
            }
            //add by wjz 允许输入医师姓名或医师编码 △

            //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
            //this.fapply_user_id送检医师.BackColor = System.Drawing.Color.White;
            this.cmb送检医师.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
        }
        #endregion

        private void fapply_timeDateTimePicker_Enter(object sender, EventArgs e)
        {
            this.dtp送检时间.BackColor = System.Drawing.Color.Bisque;
        }

        private void fapply_timeDateTimePicker_Leave(object sender, EventArgs e)
        {
            this.dtp送检时间.BackColor = System.Drawing.Color.White;
        }

        private void fsampling_timedateTimePicker_Enter(object sender, EventArgs e)
        {
            this.dtp采样时间.BackColor = System.Drawing.Color.Bisque;
        }

        private void fsampling_timedateTimePicker_Leave(object sender, EventArgs e)
        {
            this.dtp采样时间.BackColor = System.Drawing.Color.White;
        }

        private void freport_timedateTimePicker_Enter(object sender, EventArgs e)
        {
            this.dtp报告时间.BackColor = System.Drawing.Color.Bisque;
        }

        private void freport_timedateTimePicker_Leave(object sender, EventArgs e)
        {
            this.dtp报告时间.BackColor = System.Drawing.Color.White;
        }
        #region 检验医师
        private void fjy_user_idtextBox_DoubleClick(object sender, EventArgs e)
        {
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(1, "检验医师");
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }
        private void fjy_user_idtextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //this.fjy_user_idtextBox.BackColor = System.Drawing.Color.Bisque;
            this.cmb检验医师.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }

        private void fjy_user_idtextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(0, "检验医师");
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //this.fjy_user_idtextBox.BackColor = System.Drawing.Color.White;
            this.cmb检验医师.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

        }
        #endregion
        #region 核对医师
        private void fchenk_user_idtextBox_DoubleClick(object sender, EventArgs e)
        {
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(1, "核对医师");
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }

        private void fchenk_user_idtextBox_Enter(object sender, EventArgs e)
        {
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //this.fchenk_user_idtextBox.BackColor = System.Drawing.Color.Bisque;
            this.cmb核对医师.BackColor = System.Drawing.Color.Bisque;
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }

        private void fchenk_user_idtextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //bll人员(0, "核对医师");
            //del by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
            //this.fchenk_user_idtextBox.BackColor = System.Drawing.Color.White;
            this.cmb核对医师.BackColor = System.Drawing.Color.White;
            //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △
        }
        #endregion

        #region 临床诊断
        private void bll临床诊断(int intType)
        {
            try
            {
                if (intType == 1)
                {
                    using (com.HZDiseaseForm fm = new yunLis.lis.com.HZDiseaseForm())
                    {
                        if (fm.ShowDialog() != DialogResult.No)
                        {
                            sam_jy.fjy_lczd = fm.strid;
                            this.txt临床诊断.Text = fm.strname;
                        }
                    }
                }
                else
                {
                    DataTable dt = this.bllDisease.BllDTByCode(this.txt临床诊断.Text.Trim(), this.txt临床诊断.Text.Trim());
                    if (dt.Rows.Count > 0)
                    {
                        sam_jy.fjy_lczd = dt.Rows[0]["fcode"].ToString();
                        this.txt临床诊断.Text = dt.Rows[0]["fname"].ToString();
                    }
                    else
                    {
                        //using (com.HZDiseaseForm fm = new yunLis.lis.com.HZDiseaseForm())
                        //{
                        //    fm.ShowDialog();
                        //    modeljy.fjy_lczd = fm.strid;
                        //    this.fjy_lczdtextBox.Text = fm.strname;
                        //}
                    }
                }
                //this.ActiveControl = fremarktextBox;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }
        private void fjy_lczdtextBox_DoubleClick(object sender, EventArgs e)
        {
            bll临床诊断(1);
        }

        private void fjy_lczdtextBox_Enter(object sender, EventArgs e)
        {
            this.txt临床诊断.BackColor = System.Drawing.Color.Bisque;
        }

        private void fjy_lczdtextBox_Leave(object sender, EventArgs e)
        {
            //del by wjz 20160216 删除临床诊断的获取 ▽
            //bll临床诊断(0);
            //del by wjz 20160216 删除临床诊断的获取 △
            this.txt临床诊断.BackColor = System.Drawing.Color.White;
        }
        #endregion

        private void fremarktextBox_Enter(object sender, EventArgs e)
        {
            this.txt备注或档案号.BackColor = System.Drawing.Color.Bisque;
        }


        private void fremarktextBox_Leave(object sender, EventArgs e)
        {
            //return; // 以下无法访问代码无关紧要，因为没有添加开关，暂时手动跳出
            //add by wjz 20151224 ▽
            RemoveSexAgeTextChangedEvent();
            //add by wjz 20151224 △

            this.txt备注或档案号.BackColor = System.Drawing.Color.White;
            //#region 暂时这部分代码注释，注释原因：健康档案借口暂未做调整，此处需要控制病人信息的显示与加载，待调整后再恢复此部分代码 del by wjz 20151224
            //changed by 2016-02-26 重新添加公共卫生借口，只根据健康档案号获取查体人员信息 ▽
            //if ((Regex.IsMatch(fremarktextBox.Text, @"^(^\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$", RegexOptions.IgnoreCase)))
            //{//简单验证身份证
            //    modeljy.S身份证号 = fremarktextBox.Text;
            //    bll获取查体患者(fremarktextBox.Text);
            //}
            //else
            //{
            //    if (fremarktextBox.Text.Length == 12)
            //    {
            //        fremarktextBox.Text = "371323" + fremarktextBox.Text;
            //        bll获取查体患者(fremarktextBox.Text);
            //    }
            //}
            if (txt备注或档案号.Text.Trim().Length == 12)
            {
                txt备注或档案号.Text = "371323" + txt备注或档案号.Text.Trim();
                bll获取查体患者(txt备注或档案号.Text.Trim());
            }
            else if (txt备注或档案号.Text.Trim().Length == 16)
            {
                txt备注或档案号.Text = "37" + txt备注或档案号.Text.Trim();
                bll获取查体患者(txt备注或档案号.Text.Trim());
            }
            else if (txt备注或档案号.Text.Trim().Length == 18)
            {
                bll获取查体患者(txt备注或档案号.Text.Trim());
            }
            else if (txt备注或档案号.Text.Trim().Length > 0)
            {
                bll获取查体患者(txt备注或档案号.Text.Trim());
            }
            //changed by 2016-02-26 重新添加公共卫生借口，只根据健康档案号获取查体人员信息 △
            //#endregion

            //add by wjz 20151224 ▽
            AddSexAgeTextChangedEvent();
            //add by wjz 20151224 △

            this.ActiveControl = button审核;
        }
        /// <summary>
        /// 根据身份证号取得患者信息
        /// </summary>
        /// <param name="strPatCode"></param>
        private void bll获取查体患者(string strPatCode)
        {
            try
            {
                if ((sam_jy.fjy_id == null || label样本审核状态.Text == "未审核") && strPatCode != "")
                {
                    DataTable dtPat = new DataTable();
                    //changed by wjz 20160226 公共卫生系统变更，修改获取查体人员个人信息的方式，不要被函数名称迷惑，数据是从公共卫生数据库获取的，不是His数据库 ▽
                    //dtPat = this.bllPatient.BllPatientDT(" and fhz_id='" + strPatCode + "'");
                    //if (dtPat == null || dtPat.Rows.Count == 0)
                    //{
                    //    dtPat = this.bllPatient.BllPatientDTFromHisDBby档案号(strPatCode);
                    dtPat = this.bllPatient.BllPatientDTFromHisDBby档案号New(strPatCode);
                    //}
                    //changed by wjz 20160226 公共卫生系统变更，修改获取查体人员个人信息的方式，不要被函数名称迷惑，数据是从公共卫生数据库获取的，不是His数据库 △

                    if ((dtPat != null) && (dtPat.Rows.Count > 0))
                    {
                        sam_jy.fhz_id = dtPat.Rows[0]["fhz_id"].ToString();
                        if (!(string.IsNullOrWhiteSpace(dtPat.Rows[0]["fbirthday"].ToString())))
                        {
                            string strfbirthday = dtPat.Rows[0]["fbirthday"].ToString();
                            string strGetAge = this.bllPatient.BllPatientAge(strfbirthday);
                            if (!(string.IsNullOrWhiteSpace(strGetAge)))
                            {
                                string[] sPatientAge = strGetAge.Split(':');

                                this.txt年龄.Text = sPatientAge[1].ToString();//年龄
                                //年龄单位
                                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                                //this.fhz_age_unittextBox.Text = sPatientAge[0].ToString();
                                sam_jy.fhz_age_unit = bllPatNameByCode("年龄单位", sPatientAge[0].ToString());
                                this.cmb年龄单位.SelectedValue = sam_jy.fhz_age_unit;
                                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
                            }
                        }
                        //判断姓名是否需要解密
                        if (dtPat.Rows[0]["fname"].ToString().Length > 10)
                            this.txt姓名.Text = DESEncrypt.DES解密(dtPat.Rows[0]["fname"].ToString());//姓名
                        else
                            this.txt姓名.Text = dtPat.Rows[0]["fname"].ToString();//姓名
                        this.txt床号.Text = dtPat.Rows[0]["fbed_num"].ToString();//床号

                        sam_jy.fhz_sex = dtPat.Rows[0]["fsex"].ToString();

                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                        //this.fhz_sex性别.Text = bllPatNameByCode("性别", modeljy.fhz_性别);//===========
                        this.cmb性别.SelectedValue = sam_jy.fhz_sex;
                        //changed by wjz 20160128 性别控件由TextBox换为ComboBox △

                        sam_jy.fhz_type_id = dtPat.Rows[0]["ftype_id"].ToString();
                        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
                        //this.fhz_type_idtextBox_name.Text = bllPatNameByCode("病人类别", modeljy.fhz_患者类型id);
                        this.cmb病人类型.SelectedValue = sam_jy.fhz_type_id;
                        //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
                        //科室
                        sam_jy.fhz_dept = dtPat.Rows[0]["fdept_id"].ToString();

                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
                        //this.fhz_dept科室.Text = bllPatNameByCode("科室", modeljy.fhz_dept患者科室);
                        try
                        {
                            this.cmb科室.SelectedValue = this.sam_jy.fhz_dept;
                        }
                        catch
                        {
                            this.cmb科室.SelectedValue = "";
                        }
                        //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △

                        //人员
                        sam_jy.fapply_user_id = dtPat.Rows[0]["fsjys_id"].ToString();
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                        //this.fapply_user_id送检医师.Text = bllPatNameByCode("人员", modeljy.fapply_user_id);
                        try
                        {
                            this.cmb送检医师.EditValue = this.sam_jy.fapply_user_id;
                        }
                        catch
                        {
                            this.cmb送检医师.EditValue = "";
                        }
                        //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △

                        //临床诊断
                        //chanaged begin 2015-05-28
                        //del by wjz 20160216 删除临床诊断的获取 ▽
                        //modeljy.fjy_lczd = dtPat.Rows[0]["fdiagnose"].ToString();
                        //this.fjy_lczdtextBox.Text = bllPatNameByCode("临床诊断", modeljy.fjy_lczd);
                        //del by wjz 20160216 删除临床诊断的获取 △

                        sam_jy.sfzh = dtPat.Rows[0]["sfzh"].ToString();
                        this.txt身份证号.Text = sam_jy.sfzh;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void button审核_Click(object sender, EventArgs e)
        {
            RemoveSexAgeTextChangedEvent();
            bll保存或审核(MoveType.自增1, OperType.审核);
            //bindingSource样本.MoveNext();              
            this.ActiveControl = cmb病人类型;
            AddSexAgeTextChangedEvent();
        }

        private void dataGridView样本_Click(object sender, EventArgs e)
        {
            RemoveSexAgeTextChangedEvent();
            bll显示样本();
            AddSexAgeTextChangedEvent();
        }

        private void button刷新样本_Click(object sender, EventArgs e)
        {
            RemoveSexAgeTextChangedEvent();
            vRfresh右侧列表_Sam_JY_by仪器ID();//右上角“刷新”按钮
            AddSexAgeTextChangedEvent();
        }

        private void comboBox过滤样本_DropDownClosed(object sender, EventArgs e)
        {
            vRfresh右侧列表_Sam_JY_by仪器ID();//右上角下拉列表的选择列表关闭时触发
        }

        private void button删除样本_Click(object sender, EventArgs e)
        {
            //var list = chis.Database.SqlQuery<YS住院医嘱>("select * from [YS住院医嘱] where [开嘱时间]>'2020-9-1' and (项目类型='检查套餐' or [lis申请单号]<>'') order by [lis申请单号]").ToList();
            //foreach (var item父医嘱ByJY申请摘要 in list)
            //{
            //    if (item父医嘱ByJY申请摘要.执行时间 == null)
            //    {
            //        chis.Database.ExecuteSqlCommand($"delete zy在院费用 where zyid={item父医嘱ByJY申请摘要.ZYID} and 记费时间>'2020-9-1' and [项目编码]=16 and [医嘱ID] is not null");
            //        continue;
            //    }
            //    //decimal decID = Convert.ToDecimal(itemJY申请摘要.申请唯一标识);
            //    //var item父医嘱ByJY申请摘要 = chis.YS住院医嘱.Where(c => c.ID == decID).FirstOrDefault();
            //    if (item父医嘱ByJY申请摘要 == null)
            //    {
            //        msgBalloonHelper.ShowInformation("获取医嘱信息异常，请联系信息管理员处理");
            //    }

            //    var list子医嘱 = chis.YS住院子医嘱.Where(c => c.子嘱ID == item父医嘱ByJY申请摘要.子嘱ID && c.ZYID == item父医嘱ByJY申请摘要.ZYID).ToList();
            //    foreach (var item子医嘱 in list子医嘱)
            //    {
            //        var fee2 = HIS.COMM.comm.get收费小项By收费编码(item子医嘱.项目编码);
            //        if (fee2 == null)
            //        {
            //            msgBalloonHelper.ShowInformation($"{item子医嘱.项目名称}检索设置信息失败，请联系信息科维护");
            //        }
            //        if (fee2.单价 == 0)
            //        {
            //            msgHelper.ShowInformation($"医嘱项目【{fee2.收费名称}】价格未维护，不能继续执行医嘱，请联系信息科维护");
            //        }
            //        if (chis.ZY在院费用.Any(c => c.医嘱ID == item子医嘱.ID && c.ZYID == item子医嘱.ZYID))
            //        {
            //            msgBalloonHelper.BalloonShow($"【{fee2.收费名称}】已经记费，不能重复记费");
            //        }
            //        else
            //        {
            //            chis.ZY在院费用.Add(new ZY在院费用()
            //            {
            //                ZYID = item子医嘱.ZYID,
            //                项目编码 = fee2.归并编码,
            //                费用编码 = item子医嘱.项目编码,
            //                费用名称 = item子医嘱.项目名称,
            //                金额 = Math.Round(fee2.单价, 2),
            //                数量 = 1,
            //                单价 = fee2.单价,
            //                已发药标记 = false,
            //                YPID = 0,
            //                药房编码 = item子医嘱.药房编码,
            //                医生编码 = item子医嘱.开嘱医生编码,
            //                记费人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码,
            //                记费时间 = DateTime.Now,
            //                单位编码 = HIS.COMM.zdInfo.Model单位信息.iDwid,
            //                分院编码 = HIS.COMM.zdInfo.Model站点信息.分院编码,
            //                CreateTime = DateTime.Now,
            //                医嘱ID = item子医嘱.ID
            //            });
            //        }
            //        chis.SaveChanges();
            //    }
            //}
            bllDel();
        }

        private void 解除审核ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bll取消审核();
        }

        //private void 打印ToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    //add by wjz 20160127 此事件关联的按钮不再显示，但为防止意外，先修改此代码 ▽
        //    //报告录入界面右侧，点击“刷新”时，显示当前设备、当前日期所有的样本检测结果，点击右侧结果时，strCurrSelfjy_id获取的值可能为空字符。
        //    if (string.IsNullOrWhiteSpace(strCurrSelfjy_id))
        //    {
        //        yunLis.wwfbll.WWMessage.MessageShowInformation("请先审核，然后再打印。");
        //    }
        //    //add by wjz 20160127 此事件关联的按钮不再显示，但为防止意外，先修改此代码 △

        //    //2015-06-11 10;03 begin  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。
        //    if (!(sam_jy.fjy_zt.Equals("已审核")))
        //    {
        //        bll保存或审核(MoveType.不变, OperType.审核);
        //    }
        //    //2015-06-11 10;03 end  wjz 修改目的：点击“打印”按钮后，先执行审批操作，在执行打印操作。

        //    bool printSuccess = WWPrint(0, strCurrSelfjy_id);//获取是否打印成功

        //    //2015-06-11 10;03 begin  wjz 修改目的：右键打印后，更改数据行的颜色。
        //    //rowCurrent = (DataRowView)bindingSource样本.Current;
        //    DataGridViewRow row = dataGridView_SAM_JY.CurrentRow;
        //    //20150618 wjz 逻辑做了调整 begin 
        //    if (printSuccess && row != null)
        //    {
        //        if (!(row.Cells["fprint_zt"].Value.ToString().Equals("已打印")))
        //        {
        //            row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
        //            row.Cells["fprint_zt"].Value = "已打印";
        //        }

        //        if (!(row.Cells["fjy_zt"].Value.ToString().Equals("已审核")))
        //        {
        //            this.blljy.BllReportUpdate审核状态(sam_jy.fjy_id);
        //            row.Cells["fjy_zt"].Value = "已审核";
        //        }
        //    }
        //    //20150618 wjz 逻辑做了调整 end 
        //    //2015-06-11 10;03 begin  wjz 修改目的：右键打印后，更改数据行的颜色。
        //}

        private void bll仪器取数(int int选择结果值)
        {
            try
            {
                str仪器结果FTaskID = "";
                string strSql = $@"SELECT  COUNT(1) AS Expr1 FROM SAM_JY WHERE (fjy_date = '{ dtp样本日期.Value.ToString("yyyy-MM-dd")}') AND 
                                (fjy_instr = '{ cmb检验仪器.SelectedValue.ToString() }') AND (fjy_yb_code = '{ txt样本号.Text }') and fjy_zt='已审核'";

                bool bool报告审核否 = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn, strSql);
                if (bool报告审核否 == false)
                {
                    using (com.JyInstrDataForm jsi = new yunLis.lis.com.JyInstrDataForm())
                    {
                        jsi.str仪器id = this.cmb检验仪器.SelectedValue.ToString();
                        jsi.str仪器名称 = this.cmb检验仪器.Text.ToString();
                        jsi.str样本号 = this.txt样本号.Text;

                        //changed by wjz 调整日期格式 ▽
                        //jsi.str检验日期 = this.fjy_date样本日期.Text;
                        if (this.dtp样本日期.Value == null)
                        {
                            jsi.str检验日期 = DateTime.Now.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            jsi.str检验日期 = this.dtp样本日期.Value.ToString("yyyy-MM-dd");
                        }
                        //changed by wjz 调整日期格式 △

                        jsi.int选择结果否 = int选择结果值;
                        if (jsi.ShowDialog() == DialogResult.Cancel)
                            return;

                        str仪器结果FTaskID = jsi.str仪器结果FTaskID;
                        // textBox1.Text = str仪器结果FTaskID;
                        IList lisSel = jsi.selItemList;

                        if (lisSel != null)
                        {
                            for (int i = 0; i < lisSel.Count; i++)
                            {
                                string strItemidAndName = lisSel[i].ToString();
                                string[] sArray = strItemidAndName.Split(';');
                                string strItemId = "";
                                string strItemValue = "";
                                //foreach (string zz in sArray)
                                //{
                                if (sArray.Length > 0)
                                {
                                    strItemId = sArray[0].ToString();
                                    strItemValue = sArray[1].ToString();
                                    //}
                                    if (strItemId != "" & strItemId.Length > 0)
                                    {
                                        var rows = dt_SAM_JY_RESULT.Where(c => c.fitem_id == strItemId).FirstOrDefault();
                                        if (rows == null) { }
                                        else
                                        {
                                            //richTextBox2.AppendText(lisSel[i].ToString() + "\r");
                                            PojoSam_Jy_Result drNew = new PojoSam_Jy_Result();
                                            drNew.fresult_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                                            drNew.fitem_id = strItemId;
                                            drNew.fvalue = strItemValue;
                                            drNew.fitem_code = sArray[2].ToString();
                                            drNew.fitem_name = sArray[3].ToString();
                                            drNew.fitem_unit = sArray[4].ToString();
                                            drNew.forder_by = sArray[5].ToString();

                                            //changed by wjz 20151202 根据性别、年龄获取参考值 ▽
                                            #region old code
                                            //string str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                                            #endregion

                                            #region new code
                                            string str参考值;
                                            //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_sex性别.Text)
                                            //    || string.IsNullOrWhiteSpace(fhz_age_unittextBox.Text) || (fhz_age_unittextBox.Text == "岁" && fhz_age年龄.Text.Trim() == "0"))
                                            //{
                                            //    //str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                                            //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                            //    //str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                                            //    str参考值 = this.blljy.Bll参考值(strItemId, -1, "", "");
                                            //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                            //}
                                            //else
                                            //{
                                            //    //float age = -1;
                                            //    //string strsex = "";
                                            //    //try
                                            //    //{
                                            //    //    age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                                            //    //    str参考值 = this.blljy.Bll参考值New(strItemId, age, fhz_sex性别.Text.Trim(), "");
                                            //    //}
                                            //    //catch (FormatException ex)
                                            //    //{
                                            //    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
                                            //    //    //str参考值 = this.blljy.Bll参考值(strItemId, 0, "", "");
                                            //    //    str参考值 = this.blljy.Bll参考值(strItemId, -1, "", "");
                                            //    //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
                                            //    //}
                                            //    //str参考值 = this.blljy.Bll参考值New(strItemId, age, fhz_sex性别.Text.Trim(), "");
                                            //    str参考值 = GetRefValueByItemCode(strItemId);
                                            //}

                                            drNew.fitem_ref = GetRefValueByItemCode(strItemId);
                                            drNew.fitem_jgref = GetJGRefValueByItemCode(strItemId);
                                            this.dt_SAM_JY_RESULT.Add(drNew);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        bllResultShow显示结果颜色();
                    }
                }
                else
                {
                    yunLis.wwfbll.WWMessage.MessageShowInformation("当前报告已审核。");
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void button仪器数据_Click(object sender, EventArgs e)
        {

            RemoveSexAgeTextChangedEvent();
            bll仪器取数(1);
            AddSexAgeTextChangedEvent();
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            label21比较住院号.Text = "住院号：" + txt门诊或住院号.Text;
            bll比较();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void fhz_depttextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void fhz_zyh住院号_TextChanged(object sender, EventArgs e)
        {
            if (txt门诊或住院号.Text == "")
                sam_jy.fhz_id = "";
        }


        /// <summary>
        /// 核心代码为 bllYBList()
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmb检验仪器_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmb检验仪器.SelectedValue == null)
            {
                return;
            }
            string str仪器id = this.cmb检验仪器.SelectedValue.ToString();
            RemoveSexAgeTextChangedEvent();
            try
            {
                //BllItemList仪器项目列表 这个方法不可以忽略
                //点击“审核”按钮时，需要使用此函数的(dtItemList)。
                listSam_item = lis.SAM_ITEM.Where(c => c.finstr_id == str仪器id).OrderBy(c => c.fprint_num).ToList();
                //bllResultShow 这个方法的作用可以忽略，mby_lb_mk3这个设备id不知道是干什么的
                bllResultShow(str仪器id);

                vRfresh右侧列表_Sam_JY_by仪器ID();//设备ID的SelectedIndexChanged事件
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowInformation(ex.Message);
            }

            AddSexAgeTextChangedEvent();
        }

        //private void toolStripMenuItem1_Click(object sender, EventArgs e)
        //{
        //    //add by wjz 20160127 此事件关联的按钮不再显示，但为防止意外，先修改此代码 ▽
        //    //报告录入界面右侧，点击“刷新”时，显示当前设备、当前日期所有的样本检测结果，点击右侧结果时，strCurrSelfjy_id获取的值可能为空字符。
        //    if (string.IsNullOrWhiteSpace(strCurrSelfjy_id))
        //    {
        //        yunLis.wwfbll.WWMessage.MessageShowInformation("请先审核，然后再打印。");
        //    }
        //    //add by wjz 20160127 此事件关联的按钮不再显示，但为防止意外，先修改此代码 △

        //    //if(dtResult)
        //    //先判断项目信息是否有变更，如果有则执行保存审核操作
        //    if (IsDataTableChanged(dt_SAM_JY_RESULT) && !(sam_jy.fjy_zt.Equals("已审核")) && dt_SAM_JY_RESULT.Rows.Count > 0)
        //    {
        //        //billSave保存样本(2, "审核");
        //        bll保存或审核(MoveType.不变, OperType.保存);
        //    }
        //    //判断项目是否已经审核，若是，不保存修改内容
        //    //else if (!(IsDataTableChanged(dtResult)) && !(modeljy.fjy_zt检验状态.Equals("已审核")) && dtResult.Rows.Count > 0)
        //    //{
        //    //    //先审核后打印
        //    //    int updateRowCount = this.blljy.BllReportUpdate审核状态(strCurrSelfjy_id);
        //    //}

        //    bool printSuccess = WWPrint(1, strCurrSelfjy_id);

        //    //2015-06-11 10;03 begin  wjz 修改目的：右键打印后，更改数据行的颜色。
        //    //20150618 wjz  逻辑做了调整 begin 
        //    DataGridViewRow row = dataGridView_SAM_JY.CurrentRow;
        //    //if (printSuccess)
        //    if (printSuccess && (row != null))
        //    {
        //        //ADD wjz 20150702 begin  打印样本信息后，判断此样本之前是否已经审核，如果是则不进行==========
        //        string str日期 = dtp样本日期.Value.ToString("yyyy-MM-dd");
        //        string str申请单号 = row.Cells["fapply_id"].Value.ToString();
        //        string str样本号 = row.Cells["fjy_yb_code"].Value.ToString();
        //        //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
        //        string str门诊住院号 = row.Cells["fhz_zyh"].Value.ToString();
        //        //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △

        //        //changed 20150814 wjz 以下两行
        //        //bool b是否回写 = yunLis.Properties.Settings.Default.WriteJYDataToHis;
        //        //this.blljy.WriteDataToHis(str申请单号, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), str样本号, b是否回写);

        //        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
        //        //this.blljy.WriteDataToHis(str申请单号, str日期, fjy_instrComboBox_仪器.SelectedValue.ToString(), str样本号);
        //        this.blljy.WriteDataToHisBy门诊住院号(str门诊住院号, str日期, cmb检验仪器.SelectedValue.ToString(), str样本号);
        //        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △
        //        //ADD wjz 20150702 end    打印样本信息后，判断此样本之前是否已经审核，如果是则不进行==========

        //        //DataGridViewRow row = dataGridView样本.CurrentRow;
        //        if (!(row.Cells["fjy_zt"].Value.ToString().Equals("已审核")))
        //        {
        //            this.blljy.BllReportUpdate审核状态(strCurrSelfjy_id);
        //            row.Cells["fjy_zt"].Value = "已审核";
        //        }

        //        if (!(row.Cells["fprint_zt"].Value.ToString().Equals("已打印")))
        //        {
        //            row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
        //            row.Cells["fprint_zt"].Value = "已打印";
        //        }

        //        //if (!(modeljy.fjy_zt检验状态.Equals("已审核")))
        //        //{
        //        //    //此处打印后再修改后审核状态
        //        //    int updateRowCount = this.blljy.BllReportUpdate审核状态(strCurrSelfjy_id);
        //        //    if (row != null)
        //        //    {
        //        //        row.Cells["fjy_zt"].Value = "已审核";
        //        //    }
        //        //}

        //        //if (row != null)
        //        //{
        //        //    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
        //        //    row.Cells["fprint_zt"].Value = "已打印";
        //        //}
        //    }
        //    //20150618 wjz 逻辑做了调整 end
        //    //2015-06-11 10;03 begin  wjz 修改目的：右键打印后，更改数据行的颜色。
        //}

        private void button刷新结果_Click(object sender, EventArgs e)
        {
            refresh_SAM_JY_RESULT_if_need_from_Ins_Result_Detail();
        }

        private void dataGridView样本_DoubleClick(object sender, EventArgs e)
        {
            RemoveSexAgeTextChangedEvent();
            bll显示样本();
            AddSexAgeTextChangedEvent();
        }


        /// <summary>
        /// 从设备传输结果表中获取数据
        /// 1、更新Lis_Ins_Result传输标记
        /// 2、修改内存表dt_SAM_JY_RESULT
        /// </summary>
        private void Ins_Result_Detail_to_dt_SAM_JY_RESULT()
        {
            try
            {
                JYInstrDataBll jybll接口数据读取 = new JYInstrDataBll();
                DataTable dt_Lis_Ins_Result = new DataTable();
                string strWhere = "";
                if (true)
                {
                    strWhere = " where finstrid='" + this.cmb检验仪器.SelectedValue.ToString() + "' "
                         + " and (CONVERT(varchar(100), FDateTime, 23) >='" + dtp样本日期.Value.ToString("yyyy-MM-dd") +
                        "' and CONVERT(varchar(100), FDateTime, 23) <='" + dtp样本日期.Value.ToString("yyyy-MM-dd") + "' ) "
                        + " and dbo.[IsCumstomEqual](FResultID," + this.txt样本号.Text.Trim() + ")=1 "
                        + "  order by FResultID,FDateTime desc ";
                }
                dt_Lis_Ins_Result = jybll接口数据读取.dt_Lis_Ins_Result(strWhere);
                //根据主表获取明细
                if (dt_Lis_Ins_Result != null && dt_Lis_Ins_Result.Rows.Count > 0)
                {
                    for (int i = 0; i < dt_Lis_Ins_Result.Rows.Count; i++)
                    {
                        string str结果id = dt_Lis_Ins_Result.Rows[i]["FTaskID"].ToString();
                        this.str仪器结果FTaskID = str结果id;

                        var list设备数据关联设备设置 = Bll.DataHelper.dt_Lis_Ins_Result_Detail(str结果id, cmb检验仪器.SelectedValue.ToString());
                        foreach (var item in list设备数据关联设备设置)
                        {
                            //if (Convert.ToString(dt_Lis_Ins_Result_Detail.Rows[ii]["fvalue"]) == "")
                            //    continue;
                            string strfitem_id = "";
                            string strfitem_value = "";
                            if (item.fitem_id != null)
                            {
                                strfitem_id = item.fitem_id;
                            }
                            if (item.FValue != null)
                            {

                                strfitem_value = item.FValue;
                                if (ww.wwf.com.Public.IsNumber(strfitem_value))
                                {
                                    strfitem_value = Bll.StyleHelper.Set小数格式(strfitem_id, strfitem_value, listSam_item);
                                }
                            }

                            var rows = dt_SAM_JY_RESULT.Where(c => c.fitem_id == strfitem_id).FirstOrDefault();
                            if (rows != null)
                            {
                                string fremark = "";
                                if (rows.fremark != null)
                                {
                                    fremark = rows.fremark.ToString();
                                }

                                if (fremark != "1")
                                {
                                    rows.fvalue = strfitem_value;
                                    string str参考值;
                                    str参考值 = GetRefValueByItemCode(strfitem_id);
                                    if (!(string.IsNullOrWhiteSpace(strfitem_value)))
                                    {
                                        rows.fitem_badge = this.blljy.Bll结果标记(strfitem_value, str参考值);
                                    }
                                }
                            }
                            else
                            {
                                PojoSam_Jy_Result drNew = new PojoSam_Jy_Result();
                                drNew.fresult_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                                drNew.fitem_id = strfitem_id;
                                drNew.fvalue = strfitem_value;
                                drNew.fitem_code = item.FItem;
                                drNew.fitem_name = item.fitem_name;
                                drNew.fitem_unit = item.funit_name;
                                drNew.forder_by = item.fprint_num;
                                drNew.fitem_ref = GetRefValueByItemCode(strfitem_id);
                                if (strfitem_value.Equals("") || strfitem_value.Length == 0 || strfitem_value == null) { }
                                else
                                {
                                    drNew.fitem_badge = this.blljy.Bll结果标记(strfitem_value, drNew.fitem_ref);
                                }
                                this.dt_SAM_JY_RESULT.Add(drNew);
                            }
                        }

                    }
                    //add by wjz 计算项目的添加 ▽
                    //获取计算项目列表
                    //循环列表，判断dtResult中是否存在计算项目，筛选出未出现过的计算项目
                    //循环未出现过的计算项目，提取计算公式中的所有的项目名称，并判断所有的项目名称是否存在于dtResult列表中，若是将此计算项目加入dtResult，若否跳过。
                    //从计算公式中提取项目名称：公式中的项目名称都被[]包裹着（正常情况下是这样的），根据此特征去提取字符串
                    //如何将dtResult中的项目按照print_num进行排序？

                    // fitem_id,FItem,fitem_name,funit_name,fprint_num,fref,fjx_formula
                    var dtCal = Bll.DataHelper.BllGetCaluatedItems(cmb检验仪器.SelectedValue.ToString());
                    List<string> list = new List<string>();
                    foreach (var item in dtCal)
                    {
                        string _fitem_id = item.fitem_id;
                        var rows = dt_SAM_JY_RESULT.Where(c => c.fitem_id == _fitem_id).FirstOrDefault();
                        if (rows != null)
                        {
                            continue;
                        }
                        else
                        {
                            list.Clear();
                            Bll.StyleHelper.SplitItems(item.fjx_formula, list);
                            //判断列表中的项目是否全部存在于dtResult
                            bool ret = CheckExist(dt_SAM_JY_RESULT, list);
                            if (ret)
                            {
                                //调整排序时使用
                                //isAddItem = true;

                                string strItemValue = Bll.StyleHelper.bll公式计算值ByDataTable(item.fjx_formula, dt_SAM_JY_RESULT);

                                var drNew = new PojoSam_Jy_Result();
                                drNew.fresult_id = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                                drNew.fitem_id = item.fitem_id;

                                //计算参考值
                                drNew.fvalue = strItemValue;

                                drNew.fitem_code = item.FItem;
                                drNew.fitem_name = item.fitem_name;
                                drNew.fitem_unit = item.funit_name;
                                drNew.forder_by = item.fprint_num;
                                drNew.fitem_ref = GetRefValueByItemCode(item.fitem_id);
                                if (string.IsNullOrWhiteSpace(strItemValue)) { }
                                else
                                {
                                    drNew.fitem_badge = this.blljy.Bll结果标记(strItemValue, drNew.fitem_ref);
                                }
                                this.dt_SAM_JY_RESULT.Add(drNew);
                            }
                        }
                    }

                    //todo:保存数据
                    //lisUpdateFlag.Add("update Lis_Ins_Result set FTransFlag='True' FROM Lis_Ins_Result where FTaskID='" + str结果id + "'");
                    //if (lisUpdateFlag.Count > 0)
                    //{
                    //    yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(HIS.Model.Dal.SqlHelper.connectionString, lisUpdateFlag);
                    //}
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        private bool CheckExist(List<PojoSam_Jy_Result> dt, List<string> list)
        {
            if (list.Count == 0)
            {
                return false;
            }

            bool ret = true;

            for (int inner = 0; inner < list.Count; inner++)
            {
                var releatedItem = dt.Where(c => c.fitem_code == list[inner]).FirstOrDefault();
                if (releatedItem == null)
                {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        //add by wjz 计算项目的添加 △

        /// <summary>
        /// 双击选择常用取值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewResult_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                if (Convert.ToString(dataGrid_SAM_JY_Result["fvalue", e.RowIndex].EditedFormattedValue) == "")
                {
                    MessageBox.Show("选择常用取值！，该功能暂不完善，未开放！");
                    //dataGridViewResult["fvalue", e.RowIndex].Value = "112";
                    #region 结束当前行编辑，用于刷新缓存值，解决赋值问题
                    if (e.RowIndex == dataGrid_SAM_JY_Result.Rows.Count - 1)
                        this.dataGrid_SAM_JY_Result.CurrentCell = null;
                    else
                        SendKeys.Send("{Down}");
                    #endregion
                }
            }

        }

        private void dataGridView样本_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView样本.Rows)
            {
                string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
                if (str打印状态.Equals("已打印"))
                {
                    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                }
            }
        }

        private bool IsDataTableChanged(DataTable dt)
        {
            bool ret = false;

            foreach (DataRow dr in dt.Rows)
            {
                if (dr.RowState != DataRowState.Unchanged)
                {
                    ret = true;
                    break;
                }
            }

            return ret;
        }

        //当焦点位于“备注”文本框，按下回车调用此函数
        private void SaveSampleInfo()
        {
            //首先判断左侧病人信息一栏 任一文本框手动输入了信息 或者 中间检验项目是否有值
            //如果是，则执行保存操作，否则不执行
            //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 ▽
            //if (fhz_type_idtextBox_name.Text != "" ||
            if (this.cmb病人类型.Text != "" ||
                //changed by wjz 20160210 病人类型控件由TextBox调整为combobox控件 △
                txt门诊或住院号.Text != "" ||
                txt姓名.Text != "" ||
                //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                //fhz_sex性别.Text != "" ||     //=================================
                this.cmb性别.Text != "" ||
                //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                txt年龄.Text != "" ||

                //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 ▽
                //fjy_sf_typetextBox.Text != "" ||
                this.cmb费别.Text != "" ||
                //changed by wjz 20160216 费别类型控件由TextBox调整为ComboBox控件 △

                //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 ▽
                //fhz_dept科室.Text != "" ||
                this.cmb科室.Text != "" ||
                //changed by wjz 20160215 部门控件由TextBox调整为comboBox控件 △
                txt床号.Text != "" ||
                txt申请号.Text != "" ||

                //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
                //fjy_yb_typetextBox.Text != "" ||
                this.cmb样本类型.Text != "" ||
                //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 △

                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 ▽
                //fapply_user_id送检医师.Text != "" ||
                cmb送检医师.Text != "" ||
                //changed by wjz 20160216 送检医师控件由TextBox调整为comboBox控件 △
                //fjy_user_idtextBox.Text != "" ||

                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 ▽
                //fchenk_user_idtextBox.Text != "" ||
                this.cmb核对医师.Text != "" ||
                //changed by wjz 20160216 检验医师控件由TextBox调整为comboBox控件 △

                txt临床诊断.Text != "" ||
                txt备注或档案号.Text != "" ||
                dt_SAM_JY_RESULT.Count() > 0 //或者检验项目表中有检验项目
                )
            {
                bll保存或审核(MoveType.自增1, OperType.保存);
            }
            else
            {
                try
                {
                    int int样本号 = Convert.ToInt32(txt样本号.Text) + 1;
                    txt样本号.Text = int样本号.ToString();
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError("样本号不是数字。\n" + ex.Message.ToString());
                }
            }
            this.ActiveControl = txt样本号;
        }

        SAM_APPLY v复合记费(string s申请号)
        {
            if (s申请号.Substring(0, 1) == "4")
            {
                using (LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
                {
                    var list = lisEntities.Print_Group.Where(c => c.组合号 == s申请号).ToList();
                    foreach (var item in list)
                    {
                        v记费(item.HIS申请号);
                    }
                    var item2 = list[0].HIS申请号;
                    return lisEntities.SAM_APPLY.Where(c => c.fapply_id == item2).FirstOrDefault();
                }
            }
            else
            {
                return v记费(s申请号);
            }
        }

        SAM_APPLY v记费(string str申请单号)
        {
            try
            {
                var returnItem = lisEntities.SAM_APPLY.Where(c => c.fapply_id == str申请单号).FirstOrDefault();
                if (returnItem == null)
                {
                    msgHelper.ShowInformation("获取条码信息异常，请重新打印条码生成申请信息");
                }
                decimal dec申请单号2 = Convert.ToDecimal(str申请单号);
                var jy申请摘要Item = chis.JY申请单摘要.Where(c => c.申请单号 == dec申请单号2).FirstOrDefault();
                if (jy申请摘要Item != null)
                {
                    if (jy申请摘要Item.病人姓名 != returnItem.fname)
                    {
                        msgHelper.ShowInformation("条码信息冲突");
                        return returnItem;
                    }
                }

                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {

                    var item父医嘱 = chis.YS住院医嘱.Where(c => c.lis申请单号 == str申请单号).FirstOrDefault();
                    if (item父医嘱 != null)//当可以关联到医嘱时，是使用了新的直接些 sam_apply表的模式
                    {
                        if (item父医嘱.项目编码 != null)//当父医有费用编码时
                        {
                            var fee2 = HIS.COMM.comm.get收费小项By收费编码(Convert.ToInt32(item父医嘱.项目编码));
                            if (fee2 == null)
                            {
                                msgBalloonHelper.ShowInformation($"{item父医嘱.项目名称}检索设置信息失败，请联系信息科维护");
                            }
                            if (fee2.单价 == 0)
                            {
                                msgBalloonHelper.ShowInformation($"医嘱项目【{fee2.收费名称}】价格未维护，不能继续执行医嘱，请联系信息科维护");
                            }
                            if (chis.ZY在院费用.Any(c => c.医嘱ID == item父医嘱.ID && c.ZYID == item父医嘱.ZYID))
                            {
                                msgBalloonHelper.BalloonShow($"【{fee2.收费名称}】已经记费，不能重复记费");
                            }
                            else
                            {
                                chis.ZY在院费用.Add(new ZY在院费用()
                                {
                                    ZYID = item父医嘱.ZYID,
                                    项目编码 = fee2.归并编码,
                                    费用编码 = item父医嘱.项目编码,
                                    费用名称 = item父医嘱.项目名称,
                                    金额 = Math.Round(fee2.单价, 2),
                                    数量 = 1,
                                    单价 = fee2.单价,
                                    已发药标记 = false,
                                    YPID = 0,
                                    药房编码 = item父医嘱.药房编码,
                                    医生编码 = item父医嘱.开嘱医生编码,
                                    记费人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码,
                                    记费时间 = DateTime.Now,
                                    单位编码 = HIS.COMM.zdInfo.Model单位信息.iDwid,
                                    分院编码 = HIS.COMM.zdInfo.Model站点信息.分院编码,
                                    CreateTime = DateTime.Now,
                                    医嘱ID = item父医嘱.ID
                                });
                            }
                        }
                        else//从子医嘱获取信息记费
                        {
                            var list子医嘱 = chis.YS住院子医嘱.Where(c => c.子嘱ID == item父医嘱.子嘱ID && c.ZYID == item父医嘱.ZYID).ToList();
                            foreach (var item子医嘱 in list子医嘱)
                            {
                                var fee2 = HIS.COMM.comm.get收费小项By收费编码(Convert.ToInt32(item子医嘱.项目编码));
                                if (fee2 == null)
                                {
                                    msgBalloonHelper.ShowInformation($"{item子医嘱.项目名称}检索设置信息失败，请联系信息科维护");
                                }
                                if (fee2.单价 == 0)
                                {
                                    msgHelper.ShowInformation($"医嘱项目【{fee2.收费名称}】价格未维护，不能继续执行医嘱，请联系信息科维护");
                                }
                                if (chis.ZY在院费用.Any(c => c.医嘱ID == item子医嘱.ID && c.ZYID == item子医嘱.ZYID))
                                {
                                    msgBalloonHelper.BalloonShow($"【{fee2.收费名称}】已经记费，不能重复记费");
                                }
                                else
                                {
                                    chis.ZY在院费用.Add(new ZY在院费用()
                                    {
                                        ZYID = item子医嘱.ZYID,
                                        项目编码 = fee2.归并编码,
                                        费用编码 = item子医嘱.项目编码,
                                        费用名称 = item子医嘱.项目名称,
                                        金额 = Math.Round(fee2.单价, 2),
                                        数量 = 1,
                                        单价 = fee2.单价,
                                        已发药标记 = false,
                                        YPID = 0,
                                        药房编码 = item子医嘱.药房编码,
                                        医生编码 = item子医嘱.开嘱医生编码,
                                        记费人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码,
                                        记费时间 = DateTime.Now,
                                        单位编码 = HIS.COMM.zdInfo.Model单位信息.iDwid,
                                        分院编码 = HIS.COMM.zdInfo.Model站点信息.分院编码,
                                        CreateTime = DateTime.Now,
                                        医嘱ID = item子医嘱.ID
                                    });
                                }
                            }
                        }
                    }
                    else
                    {
                        decimal dec申请单号 = Convert.ToDecimal(str申请单号);
                        var itemJY申请摘要 = chis.JY申请单摘要.Where(c => c.申请单号 == dec申请单号).FirstOrDefault();
                        if (itemJY申请摘要 == null)
                        {
                            msgBalloonHelper.ShowInformation($"获取【{str申请单号}】申请信息异常，请联系信息管理员处理");
                        }
                        decimal decID = Convert.ToDecimal(itemJY申请摘要.申请唯一标识);
                        var item父医嘱ByJY申请摘要 = chis.YS住院医嘱.Where(c => c.ID == decID).FirstOrDefault();
                        if (item父医嘱ByJY申请摘要 == null)
                        {
                            msgBalloonHelper.ShowInformation("获取医嘱信息异常，请联系信息管理员处理");
                        }

                        if (item父医嘱ByJY申请摘要.项目编码 != null)
                        {
                            var fee2 = HIS.COMM.comm.get收费小项By收费编码(Convert.ToInt32(item父医嘱ByJY申请摘要.项目编码));
                            if (fee2 == null)
                            {
                                msgBalloonHelper.ShowInformation($"{item父医嘱.项目名称}检索设置信息失败，请联系信息科维护");
                            }
                            if (fee2.单价 == 0)
                            {
                                msgHelper.ShowInformation($"医嘱项目【{fee2.收费名称}】价格未维护，不能继续执行医嘱，请联系信息科维护");
                            }
                            if (chis.ZY在院费用.Any(c => c.医嘱ID == item父医嘱ByJY申请摘要.ID && c.ZYID == item父医嘱ByJY申请摘要.ZYID))
                            {
                                msgBalloonHelper.BalloonShow($"【{fee2.收费名称}】已经记费，不能重复记费");
                            }
                            else
                            {
                                chis.ZY在院费用.Add(new ZY在院费用()
                                {
                                    ZYID = item父医嘱ByJY申请摘要.ZYID,
                                    项目编码 = fee2.归并编码,
                                    费用编码 = item父医嘱ByJY申请摘要.项目编码,
                                    费用名称 = item父医嘱ByJY申请摘要.项目名称,
                                    金额 = Math.Round(fee2.单价, 2),
                                    数量 = 1,
                                    单价 = fee2.单价,
                                    已发药标记 = false,
                                    YPID = 0,
                                    药房编码 = item父医嘱ByJY申请摘要.药房编码,
                                    医生编码 = item父医嘱ByJY申请摘要.开嘱医生编码,
                                    记费人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码,
                                    记费时间 = DateTime.Now,
                                    单位编码 = HIS.COMM.zdInfo.Model单位信息.iDwid,
                                    分院编码 = HIS.COMM.zdInfo.Model站点信息.分院编码,
                                    CreateTime = DateTime.Now,
                                    医嘱ID = item父医嘱ByJY申请摘要.ID
                                });
                            }
                        }
                        else
                        {
                            var list子医嘱 = chis.YS住院子医嘱.Where(c => c.子嘱ID == item父医嘱ByJY申请摘要.子嘱ID && c.ZYID == item父医嘱ByJY申请摘要.ZYID).ToList();
                            foreach (var item子医嘱 in list子医嘱)
                            {
                                var fee2 = HIS.COMM.comm.get收费小项By收费编码(Convert.ToInt32(item子医嘱.项目编码));
                                if (fee2 == null)
                                {
                                    msgBalloonHelper.ShowInformation($"{item子医嘱.项目名称}检索设置信息失败，请联系信息科维护");
                                }
                                if (fee2.单价 == 0)
                                {
                                    msgHelper.ShowInformation($"医嘱项目【{fee2.收费名称}】价格未维护，不能继续执行医嘱，请联系信息科维护");
                                }
                                if (chis.ZY在院费用.Any(c => c.医嘱ID == item子医嘱.ID && c.ZYID == item子医嘱.ZYID))
                                {
                                    msgBalloonHelper.BalloonShow($"【{fee2.收费名称}】已经记费，不能重复记费");
                                }
                                else
                                {
                                    chis.ZY在院费用.Add(new ZY在院费用()
                                    {
                                        ZYID = item子医嘱.ZYID,
                                        项目编码 = fee2.归并编码,
                                        费用编码 = item子医嘱.项目编码,
                                        费用名称 = item子医嘱.项目名称,
                                        金额 = Math.Round(fee2.单价, 2),
                                        数量 = 1,
                                        单价 = fee2.单价,
                                        已发药标记 = false,
                                        YPID = 0,
                                        药房编码 = item子医嘱.药房编码,
                                        医生编码 = item子医嘱.开嘱医生编码,
                                        记费人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码,
                                        记费时间 = DateTime.Now,
                                        单位编码 = HIS.COMM.zdInfo.Model单位信息.iDwid,
                                        分院编码 = HIS.COMM.zdInfo.Model站点信息.分院编码,
                                        CreateTime = DateTime.Now,
                                        医嘱ID = item子医嘱.ID
                                    });
                                }
                            }
                        }

                    }



                    if (!chis.ChangeTracker.HasChanges())
                    {
                        msgHelper.ShowInformation($"【{str申请单号}】记费失败");
                        return returnItem;
                    }

                    var added = chis.ChangeTracker.Entries().Where(c => c.State == System.Data.Entity.EntityState.Added);
                    var test = added.ToList();
                    StringBuilder sb = new StringBuilder();
                    foreach (var item in added)
                    {
                        var item2 = (HIS.Model.ZY在院费用)item.Entity;
                        sb.Append($"{item2.费用名称},{item2.金额}\r\n");
                    }
                    chis.SaveChanges();
                    MessageBox.Show("计费成功！\r\n" + sb.ToString());

                }
                return returnItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show("计费失败！请联系信息管理员维护基础信息！" + ex.Message);
                return null;
            }
        }

        private void txt申请号_TextChanged(object sender, EventArgs e)
        {
            string str住院号 = this.txt门诊或住院号.Text;
            string str备注 = this.txt备注或档案号.Text;
            string str申请单号 = this.txt申请号.Text;
            //申请单号长度不是12的时候，不进行任何处理
            if (str申请单号.Length != 12)
            {
                return;
            }
            RemoveSexAgeTextChangedEvent();

            SAM_APPLY samApply;
            //取到住院病人信息后进行计费
            if (b扫码计费)
            {
                samApply = v复合记费(str申请单号);
            }
            else
            {
                samApply = lisEntities.SAM_APPLY.Where(c => c.fapply_id == str申请单号).FirstOrDefault();
                if (samApply == null)
                {
                    msgBalloonHelper.ShowInformation("获取LIS申请信息失败，请核对条码");
                    return;
                }
            }

            if (samApply.ftype_id == "住院")
            {
                decimal decZYID = Convert.ToDecimal(samApply.fhz_id);
                var item病人信息 = chis.ZY病人信息.Where(c => c.ZYID == decZYID).FirstOrDefault();

                Model年龄 item年龄 = new Model年龄();
                if (string.IsNullOrEmpty(item病人信息.身份证号))
                {
                    HIS.COMM.Helper.AgeHelper.GetOutAgeAndUnitByBirthday(Convert.ToDateTime(item病人信息.出生日期), DateTime.Now, item年龄);
                }
                else
                {
                    HIS.COMM.Helper.AgeHelper.GetOutAgeAndUnitBySFZH(item病人信息.身份证号, item年龄);
                }

                samApply.fage = Convert.ToInt16(item年龄.默认年龄数值);
                samApply.fage_unit = item年龄.默认年龄单位;

                samApply.fapply_dept_id = item病人信息.病区.ToString();
                samApply.fbed_num = item病人信息.病床;
            }
            else
            {
                decimal mzid = Convert.ToDecimal(samApply.fhz_id);
                var item病人信息 = chis.MF门诊摘要.Where(c => c.MZID == mzid).FirstOrDefault();
                samApply.fage = Convert.ToInt16(item病人信息.年龄);
                samApply.fage_unit = item病人信息.年龄单位;
            }


            //住院号
            if (samApply.fage == null)
            {
                samApply.fage = 0;
            }
            if (samApply.sfzh == null)
            {
                samApply.sfzh = "";
            }
            if (samApply.card_no == null)
            {
                samApply.card_no = "";
            }
            if (samApply.fage_unit == null)
            {
                samApply.fage_unit = "";
            }
            if (samApply.fbed_num == null)
            {
                samApply.fbed_num = "";
            }
            if (samApply.fcreate_time == null)
            {
                samApply.fcreate_time = DateTime.Now.ToString();
            }

            sam_jy.fhz_zyh = samApply.fhz_zyh.ToString();
            sam_jy.fhz_id = samApply.fhz_id.ToString();
            if (samApply.fage_unit != null)
            {
                sam_jy.fhz_age_unit = bllPatNameByCode("年龄单位", samApply.fage_unit.ToString());
            }
            sam_jy.fhz_sex = blPatCodeByName("性别", samApply.fsex.ToString());
            sam_jy.fhz_type_id = blPatCodeByName("病人类型", samApply.ftype_id.ToString()); //病人类别
            sam_jy.fhz_dept = samApply.fapply_dept_id.ToString();

            this.txt门诊或住院号.Text = samApply.fhz_zyh.ToString();
            this.txt年龄.Text = samApply.fage.ToString();//年龄          
            this.cmb年龄单位.SelectedValue = sam_jy.fhz_age_unit;
            this.txt姓名.Text = samApply.fname.ToString();//姓名
            this.txt床号.Text = samApply.fbed_num.ToString();//床号      
            this.cmb性别.SelectedValue = sam_jy.fhz_sex;
            this.cmb病人类型.SelectedValue = sam_jy.fhz_type_id;
            //sam_jy.fhz_dept = blPatCodeByName("科室", samApply.fapply_dept_id.ToString());
            //人员
            sam_jy.fapply_user_id = samApply.fapply_user_id.ToString();
            sam_jy.sfzh = samApply.sfzh;
            this.sam_jy.fjy_lczd = samApply.fdiagnose.ToString();
            sam_jy.f1 = samApply.card_no;


            try
            {
                this.cmb科室.SelectedValue = Convert.ToInt32(this.sam_jy.fhz_dept);
            }
            catch
            {
                this.cmb科室.SelectedValue = "";
            }

            try
            {
                this.cmb送检医师.EditValue = Convert.ToInt32(this.sam_jy.fapply_user_id);
            }
            catch
            {
                this.cmb送检医师.EditValue = "";
            }
            this.txt临床诊断.Text = this.bllPatNameByCode("临床诊断", this.sam_jy.fjy_lczd);
            this.txt身份证号.Text = samApply.sfzh.ToString();
            this.txt电子健康卡.Text = samApply.card_no.ToString();
            try
            {
                DateTime datetime送检 = Convert.ToDateTime(samApply.fcreate_time.ToString());
                dtp送检时间.Value = datetime送检;
                dtp采样时间.Value = datetime送检;
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
            vSet参考值();
            AddSexAgeTextChangedEvent();
        }


        private void fjy_date样本日期_Enter(object sender, EventArgs e)
        {
            dtp样本日期.Tag = dtp样本日期.Text;
        }

        private void fjy_date样本日期_Leave(object sender, EventArgs e)
        {
            if (dtp样本日期.Text.Equals(dtp样本日期.Tag.ToString()))
            {

                //不进行任何处理
                dtp样本日期.Tag = "";
            }
            else
            {

                RemoveSexAgeTextChangedEvent();


                vRfresh右侧列表_Sam_JY_by仪器ID();//左上角日期控件Leave事件


                AddSexAgeTextChangedEvent();

            }
        }

        private void fjy_yb_code样本_Enter(object sender, EventArgs e)
        {
            txt样本号.Tag = txt样本号.Text;
        }
        //add by wjz 20151022 △

        //add by wjz 20151202 项目参考值根据性别、年龄变化 ▽
        private void fhz_sex性别_TextChanged(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(fhz_sex性别.Text) || string.IsNullOrWhiteSpace(fhz_age年龄.Text) || (fhz_age_unittextBox.Text != "岁"))
            //{
            //}
            //else
            //{
            //    //设定参考值
            //    SetRef();
            //}

            //del by wjz 20160128 性别控件由TextBox换为ComboBox ▽
            //string temp = fhz_sex性别.Text;//======================================
            //del by wjz 20160128 性别控件由TextBox换为ComboBox △
            vSet参考值();
        }

        private void fhz_age年龄_TextChanged(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(fhz_sex性别.Text) || string.IsNullOrWhiteSpace(fhz_age年龄.Text) || (fhz_age_unittextBox.Text != "岁"))
            //{
            //}
            //else
            //{
            //    //设定参考值
            //    SetRef();
            //}
            vSet参考值();
        }

        private void fhz_age_unittextBox_TextChanged(object sender, EventArgs e)
        {
            //if (string.IsNullOrWhiteSpace(fhz_sex性别.Text) || string.IsNullOrWhiteSpace(fhz_age年龄.Text) || (fhz_age_unittextBox.Text != "岁"))
            //{
            //}
            //else
            //{
            //    //设定参考值
            //    SetRef();
            //}
            vSet参考值();
        }

        /// <summary>
        /// 移除年龄、性别、年龄单位触发事件
        /// </summary>
        private void RemoveSexAgeTextChangedEvent()
        {
            txt年龄.TextChanged -= fhz_age年龄_TextChanged;
            this.cmb年龄单位.SelectedValueChanged -= comboBoxAgeUnit_SelectedValueChanged;
            cmb性别.SelectedValueChanged -= fhz_sex性别Combo_SelectedValueChanged;
        }

        private void AddSexAgeTextChangedEvent()
        {
            txt年龄.TextChanged -= fhz_age年龄_TextChanged;
            this.cmb年龄单位.SelectedValueChanged -= comboBoxAgeUnit_SelectedValueChanged;
            cmb性别.SelectedValueChanged -= fhz_sex性别Combo_SelectedValueChanged;


            txt年龄.TextChanged += fhz_age年龄_TextChanged;
            this.cmb年龄单位.SelectedValueChanged += comboBoxAgeUnit_SelectedValueChanged;
            cmb性别.SelectedValueChanged += fhz_sex性别Combo_SelectedValueChanged;
        }


        private void vSet参考值()
        {
            string strfitem_id = null;
            string strfitem_value = null;
            string str参考值 = null;
            float age = -1;
            try
            {
                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
                //if (fhz_age_unittextBox.Text == "岁")
                //{
                //    age = Convert.ToInt32(fhz_age年龄.Text.Trim());
                //    if(age == 0)
                //    {
                //        age = -1;
                //    }
                //}
                //else if(fhz_age_unittextBox.Text == "月")
                //{
                //    age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
                //}
                //else if(fhz_age_unittextBox.Text == "天")
                //{
                //    age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365F;
                //}
                if (cmb年龄单位.Text == "岁")
                {
                    age = Convert.ToInt32(txt年龄.Text.Trim());
                    if (age == 0)
                    {
                        age = -1;
                    }
                }
                else if (cmb年龄单位.Text == "月")
                {
                    age = Convert.ToInt32(txt年龄.Text.Trim()) / 12.0F;
                }
                else if (cmb年龄单位.Text == "天")
                {
                    age = Convert.ToInt32(txt年龄.Text.Trim()) / 365F;
                }
                //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △
            }
            catch
            {
                age = -1;
                //return;
            }

            // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整，类似于此情况都适用 ▽
            string strSampleTypeid = "";
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            //if (string.IsNullOrWhiteSpace(modeljy.fjy_yb_type) || string.IsNullOrWhiteSpace(fjy_yb_typetextBox.Text))
            if (string.IsNullOrWhiteSpace(sam_jy.fjy_yb_type) || string.IsNullOrWhiteSpace(this.cmb样本类型.Text))
            //changed by wjz 20160216 样本类型控件由TextBox调整为comboBox控件 ▽
            { }
            else
            {
                strSampleTypeid = sam_jy.fjy_yb_type.Trim();
            }

            // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整，类似于此情况都适用 △
            foreach (var item in dt_SAM_JY_RESULT)
            {
                strfitem_value = item.fvalue;
                strfitem_id = item.fitem_id;
                // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整 ▽
                #region old code
                //str参考值 = this.blljy.Bll参考值New(strfitem_id, age, fhz_sex性别.Text.Trim(), "");
                #endregion

                //changed by wjz 20160128 性别控件由TextBox换为ComboBox ▽
                //str参考值 = this.blljy.Bll参考值New(strfitem_id, age, fhz_sex性别.Text.Trim(), strSampleTypeid);//==============
                str参考值 = this.blljy.Bll参考值New(strfitem_id, age, cmb性别.Text.Trim(), strSampleTypeid);
                //changed by wjz 20160128 性别控件由TextBox换为ComboBox △
                // add by wjz 王庄大生化设备即化验血又化验尿，而且“淀粉酶”在血、尿化验中均有，且参考值不一样，针对此情况进行调整 △
                item.fitem_ref = str参考值;
                if (!(string.IsNullOrWhiteSpace(strfitem_value)))
                {
                    item.fitem_badge = this.blljy.Bll结果标记(strfitem_value, str参考值);
                }
            }
        }


        private string GetRefValueByItemCode(string itemid)
        {
            #region 修改此方法的内部实现，原因：如果化验项目的参考值只与性别有关，参考值可能回取错
            //string str参考值 = "";
            //if (string.IsNullOrWhiteSpace(fhz_age年龄.Text) || string.IsNullOrWhiteSpace(fhz_sex性别.Text)
            //        || string.IsNullOrWhiteSpace(fhz_age_unittextBox.Text) || (fhz_age_unittextBox.Text == "岁" && fhz_age年龄.Text.Trim() == "0"))
            //{
            //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
            //    //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), 0, "", "");
            //    str参考值 = this.blljy.Bll参考值(itemid, -1, "", "");
            //    //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
            //}
            //else
            //{
            //    float age = -1;
            //    string strsex = "";
            //    try
            //    {
            //        if (fhz_age_unittextBox.Text.Trim() == "月")
            //        {
            //            age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 12.0F;
            //        }
            //        else if (fhz_age_unittextBox.Text.Trim() == "天")
            //        {
            //            age = Convert.ToInt32(fhz_age年龄.Text.Trim()) / 365.0F;
            //        }
            //        else if (fhz_age_unittextBox.Text.Trim() == "岁")
            //        {
            //            age = Convert.ToInt32(fhz_age年龄.Text.Trim());
            //        }

            //        strsex = fhz_sex性别.Text.Trim();
            //        //str参考值 = this.blljy.Bll参考值New(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), age, fhz_sex性别.Text.Trim(), "");
            //    }
            //    catch (Exception ex)
            //    {
            //        age = -1;
            //        strsex = "";
            //        //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。▽
            //        //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), 0, "", "");
            //        //str参考值 = this.blljy.Bll参考值(dataGridViewResult.Rows[i].Cells["fitem_id"].Value.ToString(), -1, "", "");
            //        //changed by wjz 20151204 参数年龄0改为-1，新生儿的年龄可能为0--0.02岁（0.02不准确）为防止传入0造成取错参考值。△
            //    }

            //    str参考值 = this.blljy.Bll参考值New(itemid, age, strsex, "");
            //}
            //return str参考值;
            #endregion

            //先取dtItemList里的数据，如果参考值不跟性别、年龄、样本等关联，则直接从dtItemList里取参考值 ▽
            var drItems = listSam_item.Where(c => c.fitem_id == itemid).ToList();
            var drItem = drItems[0];
            string fref_if_age = drItem.fref_if_age.ToString();
            string fref_if_sex = drItem.fref_if_sex.ToString();
            string fref_if_sample = drItem.fref_if_sample.ToString();
            string fref_if_method = drItem.fref_if_method.ToString();
            string fref = drItem.fref.ToString();

            if (fref_if_age == "0" && fref_if_sex == "0" && fref_if_sample == "0" && fref_if_method == "0")
            {
                return fref;
            }
            //先取dtItemList里的数据，如果参考值不跟性别、年龄、样本等关联，则直接从dtItemList里取参考值 △

            string strSex = this.cmb性别.Text.Trim();
            string strSampleTypeid = "";
            float fAge = -1;
            string str参考值 = "";

            if (string.IsNullOrWhiteSpace(txt年龄.Text) || string.IsNullOrWhiteSpace(cmb年龄单位.Text)
                || (cmb年龄单位.Text == "岁" && txt年龄.Text.Trim() == "0"))
            {
                fAge = -1f;
            }
            else
            {
                try
                {
                    if (cmb年龄单位.Text.Trim() == "月")
                    {
                        fAge = Convert.ToInt32(txt年龄.Text.Trim()) / 12.0F;
                    }
                    else if (cmb年龄单位.Text.Trim() == "天")
                    {
                        fAge = Convert.ToInt32(txt年龄.Text.Trim()) / 365.0F;
                    }
                    else if (cmb年龄单位.Text.Trim() == "岁")
                    {
                        fAge = Convert.ToInt32(txt年龄.Text.Trim());
                    }
                }
                catch (Exception ex)
                {
                    fAge = -1f;
                }
            }

            if (string.IsNullOrWhiteSpace(this.cmb样本类型.Text))
            {
                strSampleTypeid = "";
            }
            else
            {
                strSampleTypeid = sam_jy.fjy_yb_type;
            }
            str参考值 = this.blljy.Bll参考值New(itemid, fAge, strSex, strSampleTypeid);
            return str参考值;
        }

        /// <summary>
        /// 获取危急值
        /// </summary>
        /// <param name="itemid"></param>
        /// <returns></returns>
        private string GetJGRefValueByItemCode(string itemid)
        {
            //add by yfh 202001917 获取危急值参考 ▽
            var drItems = listSam_item.Where(c => c.fitem_id == itemid).ToList();
            var drItem = drItems[0];
            string fref_if_age = drItem.fref_if_age.ToString();
            string fref_if_sex = drItem.fref_if_sex.ToString();
            string fref_if_sample = drItem.fref_if_sample.ToString();
            string fref_if_method = drItem.fref_if_method.ToString();
            string fref = drItem.fjg_value_xx.ToString() + "--" + drItem.fjg_value_sx.ToString();

            if (fref_if_age == "0" && fref_if_sex == "0" && fref_if_sample == "0" && fref_if_method == "0")
            {
                return fref;
            }
            //add by yfh 202001917 获取危急值参考 △

            string strSex = "";
            string strSampleTypeid = "";
            float fAge = -1;
            string str危急值 = "";

            #region 计算年龄
            if (string.IsNullOrWhiteSpace(txt年龄.Text) || string.IsNullOrWhiteSpace(cmb年龄单位.Text)
                || (cmb年龄单位.Text == "岁" && txt年龄.Text.Trim() == "0"))
            {
                fAge = -1f;
            }
            else
            {
                try
                {
                    if (cmb年龄单位.Text.Trim() == "月")
                    {
                        fAge = Convert.ToInt32(txt年龄.Text.Trim()) / 12.0F;
                    }
                    else if (cmb年龄单位.Text.Trim() == "天")
                    {
                        fAge = Convert.ToInt32(txt年龄.Text.Trim()) / 365.0F;
                    }
                    else if (cmb年龄单位.Text.Trim() == "岁")
                    {
                        fAge = Convert.ToInt32(txt年龄.Text.Trim());
                    }
                }
                catch (Exception ex)
                {
                    fAge = -1f;
                }
            }
            #endregion

            #region 计算性别
            strSex = this.cmb性别.Text.Trim();
            #endregion

            #region 获取样本
            if (string.IsNullOrWhiteSpace(this.cmb样本类型.Text))
            {
                strSampleTypeid = "";
            }
            else
            {
                strSampleTypeid = sam_jy.fjy_yb_type;
            }
            #endregion

            str危急值 = this.blljy.Bll危急值(itemid, fAge, strSex, strSampleTypeid);
            return str危急值;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            RemoveSexAgeTextChangedEvent();
            if (label样本审核状态.Text == "已审核")
            {
                WWMessage.MessageShowWarning("样本已审核，不再允许执行“保存”操作。");
            }
            else
            {
                bll保存或审核(MoveType.不变, OperType.保存);
            }
            this.ActiveControl = txt样本号;
            AddSexAgeTextChangedEvent();
        }

        private void fhz_sex性别Combo_SelectedValueChanged(object sender, EventArgs e)
        {
            //add by wjz 20160308 手工录入时性别不能保存 ▽
            sam_jy.fhz_sex = this.cmb性别.SelectedValue.ToString();
            //add by wjz 20160308 手工录入时性别不能保存 △
            vSet参考值();
        }

        private void fhz_sex性别Combo_Enter(object sender, EventArgs e)
        {
            this.cmb性别.BackColor = System.Drawing.Color.Bisque;
        }

        private void fhz_sex性别Combo_Leave(object sender, EventArgs e)
        {
            string sexText = this.cmb性别.Text.Trim();

            if (sexText == "1")
            {
                this.cmb性别.SelectedValue = "1";
                sam_jy.fhz_sex = "1";
            }
            if (sexText == "2")
            {
                this.cmb性别.SelectedValue = "2";
                sam_jy.fhz_sex = "2";
            }
            else if (sexText == "")
            {
                this.cmb性别.SelectedValue = "";
                sam_jy.fhz_sex = "";
            }
            else
            { }

            this.cmb性别.BackColor = System.Drawing.Color.White;
        }
        //add by wjz 20160128 性别控件由TextBox换为ComboBox △

        //add by wjz 20160128 病人类型控件由TextBox换为ComboBox ▽
        private void cboPatientType_Enter(object sender, EventArgs e)
        {
            this.cmb病人类型.BackColor = System.Drawing.Color.Bisque;
        }

        private void cboPatientType_Leave(object sender, EventArgs e)
        {
            string type = this.cmb病人类型.Text.Trim();
            if (type == "1")
            {
                this.cmb病人类型.SelectedValue = "1";
            }
            else if (type == "2")
            {
                this.cmb病人类型.SelectedValue = "2";
            }
            else if (type == "3")
            {
                this.cmb病人类型.SelectedValue = "3";
            }
            else if (type == "4")
            {
                this.cmb病人类型.SelectedValue = "4";
            }
            else if (type == "")
            {
                this.cmb病人类型.SelectedValue = "";
            }
            else
            { }
            sam_jy.fhz_type_id = this.cmb病人类型.SelectedValue.ToString();
            this.cmb病人类型.BackColor = System.Drawing.Color.White;
        }

        private void cboPatientType_SelectedValueChanged(object sender, EventArgs e)
        {
            this.sam_jy.fhz_type_id = this.cmb病人类型.SelectedValue.ToString();
        }
        //add by wjz 20160128 病人类型控件由TextBox换为ComboBox △

        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 ▽
        private void comboBoxAgeUnit_Enter(object sender, EventArgs e)
        {
            this.cmb年龄单位.BackColor = System.Drawing.Color.Bisque;
        }

        private void comboBoxAgeUnit_Leave(object sender, EventArgs e)
        {
            string ageunit = this.cmb年龄单位.Text.Trim();
            if (ageunit == "1")
            {
                this.cmb年龄单位.SelectedValue = "1";
            }
            else if (ageunit == "2")
            {
                this.cmb年龄单位.SelectedValue = "2";
            }
            else if (ageunit == "3")
            {
                this.cmb年龄单位.SelectedValue = "3";
            }
            else if (ageunit == "")
            {
                this.cmb年龄单位.SelectedValue = "";
            }
            else
            { }

            this.sam_jy.fhz_age_unit = this.cmb年龄单位.SelectedValue.ToString();

            this.cmb年龄单位.BackColor = System.Drawing.Color.White;
        }

        private void comboBoxAgeUnit_SelectedValueChanged(object sender, EventArgs e)
        {
            this.sam_jy.fhz_age_unit = this.cmb年龄单位.SelectedValue.ToString();
            vSet参考值();
        }

        //changed by wjz 20160210 年龄单位控件由TextBox调整为combobox控件 △


        private void Init送检科室Dept()
        {
            try
            {
                this.cmb科室.DataSource = list科室;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void fhz_dept科室cbo_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cmb科室.SelectedValue == null)
            {
                return;
            }
            this.sam_jy.fhz_dept = this.cmb科室.SelectedValue.ToString();
        }



        private void fapply_user_id送检医师cbo_SelectedValueChanged(object sender, EventArgs e)
        {
            this.sam_jy.fapply_user_id = this.cmb送检医师.EditValue.ToString();
        }

        private void cmb检验医师_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cmb检验医师.EditValue == null)
            {
                return;
            }
            this.sam_jy.fjy_user_id = this.cmb检验医师.EditValue.ToString();
        }

        private void fchenk_user_idComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cmb核对医师.EditValue == null)
            {
                return;
            }
            this.sam_jy.fchenk_user_id = this.cmb核对医师.EditValue.ToString();
        }

        private void cmb样本类型_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cmb样本类型.SelectedValue == null)
            {
                return;
            }
            this.sam_jy.fjy_yb_type = this.cmb样本类型.SelectedValue.ToString();
            vSet参考值();
        }




        private void fjy_sf_typetextBoxCombox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.cmb费别.SelectedValue == null)
            {
                return;
            }
            this.sam_jy.fjy_sf_type = this.cmb费别.SelectedValue.ToString();
        }

        private DateTime dt送检医生1;
        private DateTime dt送检医生2;
        private void fapply_user_id送检医师cbo_Click(object sender, EventArgs e)
        {
            dt送检医生2 = DateTime.Now;
            if (dt送检医生1 != null && (dt送检医生2 - dt送检医生1).TotalSeconds < 0.5)
            {
                using (com.WindowsPersonForm fm = new yunLis.lis.com.WindowsPersonForm())
                {
                    if (fm.ShowDialog() != DialogResult.No)
                    {
                        cmb送检医师.EditValue = fm.strid;
                    }
                }
            }
            dt送检医生1 = dt送检医生2;
        }
        //add by wjz 双击送检医师时，弹出医师选择窗口 △
        private void dataGridViewResult_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //结果值在第2列中，如果显示结果的DataGridView中结果列的位置发生了变更，则下方if中的2也需要变更。
            if (e.RowIndex >= 0 && e.ColumnIndex == 2)
            {
                if (this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells[e.ColumnIndex] != this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fvalue"])
                {
                    return;
                }
                string newvalue = this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fvalue"].Value.ToString();
                string refvalue = this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_ref"].Value.ToString();//参考值位于第6列中

                string[] strArr = { "--" };
                string[] lowhigh = refvalue.Split(strArr, StringSplitOptions.RemoveEmptyEntries);
                if (lowhigh.Length != 2)
                {
                    return;
                }

                string biaoji = "";
                try
                {
                    double dnewvalue = Convert.ToDouble(newvalue);
                    double dlow = Convert.ToDouble(lowhigh[0]);
                    double dhigh = Convert.ToDouble(lowhigh[1]);

                    if (dnewvalue < dlow && dnewvalue < dhigh)
                    {
                        //箭头向下"↓"
                        biaoji = "↓";
                    }
                    else if (dnewvalue > dhigh && dnewvalue > dlow)
                    {
                        //箭头向上"↑"
                        biaoji = "↑";
                    }
                }
                catch
                { }

                this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fremark"].Value = "1";
                this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_badge"].Value = biaoji;

                if (biaoji.Equals("↑"))
                {
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fvalue"].Style.ForeColor = Color.Red;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_badge"].Style.ForeColor = Color.Red;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.ForeColor = Color.Black;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.BackColor = System.Drawing.SystemColors.Control;
                }
                else if (biaoji.Equals("↑↑"))
                {
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fvalue"].Style.ForeColor = Color.Red;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_badge"].Style.ForeColor = Color.Red;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.ForeColor = Color.Wheat;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.BackColor = Color.Red;
                }
                else if (biaoji.Equals("↓"))
                {
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fvalue"].Style.ForeColor = Color.Blue;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_badge"].Style.ForeColor = Color.Blue;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.ForeColor = Color.Black;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.BackColor = System.Drawing.SystemColors.Control;
                }
                else if (biaoji.Equals("↓↓"))
                {
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fvalue"].Style.ForeColor = Color.Blue;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_badge"].Style.ForeColor = Color.Blue;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.ForeColor = Color.Wheat;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.BackColor = Color.Red;
                }
                else
                {
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fvalue"].Style.ForeColor = Color.Black;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.ForeColor = Color.Black;
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_jgref"].Style.BackColor = System.Drawing.SystemColors.Control;
                }
            }
        }

        private void dataGridViewResult_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ///fitem_id
            try
            {
                if (this.sam_jy.fjy_zt == "已审核")
                {
                    return;
                }

                ItemBLL itembllTemp = new ItemBLL();
                string itemidTemp = this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fitem_id"].Value.ToString();
                if (string.IsNullOrWhiteSpace(itemidTemp))
                {
                    return;
                }

                DataTable dtItemValueList = itembllTemp.GetItemValueList(itemidTemp);
                if (dtItemValueList == null || dtItemValueList.Rows.Count == 0)
                {
                    return;
                }

                FrmItemValueList frm = new FrmItemValueList(dtItemValueList);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells["fvalue"].Value = frm.itemValue;

                    this.dataGrid_SAM_JY_Result.CurrentCell = null;
                    this.dataGrid_SAM_JY_Result.CurrentCell = this.dataGrid_SAM_JY_Result.Rows[e.RowIndex].Cells[e.ColumnIndex];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
        private void button读卡_Click(object sender, EventArgs e)
        {
            try
            {
                //启动钩子服务
                listener.Start();
                //HIS.readPersonInfo.Form读卡 frm = new HIS.readPersonInfo.Form读卡();
                //frm.ShowDialog();
                //if (frm.person is ClassYBJK.basePerson)
                //{
                //    this.fremarktextBox.Text = frm.person.S身份证号;
                //    this.fhz_name姓名.Text = frm.person.S姓名;
                //}
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning(ex.Message);
            }
        }

        public static string SendRequest(string url, Encoding encoding)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), encoding);
            string result;
            try
            {
                result = streamReader.ReadLine();
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Close();
                }
            }
            return result;
        }

    }
}
