﻿namespace yunLis.lis.sam.jy
{
    partial class ReportEditForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fjy_instrLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label fsample_barcodeLabel;
            System.Windows.Forms.Label ftype_idLabel;
            System.Windows.Forms.Label fhz_zyhLabel;
            System.Windows.Forms.Label fhz_idLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fageLabel;
            System.Windows.Forms.Label fapply_dept_idLabel;
            System.Windows.Forms.Label froom_numLabel;
            System.Windows.Forms.Label fbed_numLabel;
            System.Windows.Forms.Label fdiagnoseLabel;
            System.Windows.Forms.Label fsample_type_idLabel;
            System.Windows.Forms.Label fapply_user_idLabel;
            System.Windows.Forms.Label fapply_timeLabel;
            System.Windows.Forms.Label fsampling_user_idLabel;
            System.Windows.Forms.Label fsampling_timeLabel;
            System.Windows.Forms.Label fjy_user_idLabel;
            System.Windows.Forms.Label fjy_timeLabel;
            System.Windows.Forms.Label fexamine_user_idLabel;
            System.Windows.Forms.Label fexamine_timeLabel;
            System.Windows.Forms.Label fremarkLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportEditForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.lIS_REPORTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new yunLis.lis.sam.samDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDelItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtoninstrDate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonU = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonD = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonJCSH = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.fexamine_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fprint_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fstate = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.com_listBindingSource_fstate = new System.Windows.Forms.BindingSource(this.components);
            this.fjytype_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.com_listBindingSource_fjytype_id = new System.Windows.Forms.BindingSource(this.components);
            this.fsample_type_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.com_listBindingSource_fsample_type_id = new System.Windows.Forms.BindingSource(this.components);
            this.fapply_user_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.com_listBindingSource_fapply_user_id = new System.Windows.Forms.BindingSource(this.components);
            this.fapply_dept_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.com_listBindingSource_fapply_dept_id = new System.Windows.Forms.BindingSource(this.components);
            this.fjy_user_id = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.com_listBindingSource_fjy_user_id = new System.Windows.Forms.BindingSource(this.components);
            this.contextMenuStripReport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem_ReportCol = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemExamine = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemExamineNO = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemReportDel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Ref = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_result = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemIntData = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemResultAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemResultValue = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemResultDel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemPrinYL = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemLS = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.关闭ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButtonJZ = new System.Windows.Forms.RadioButton();
            this.radioButtonCG = new System.Windows.Forms.RadioButton();
            this.fremarkTextBox = new System.Windows.Forms.TextBox();
            this.fexamine_timeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.fexamine_user_idComboBox = new System.Windows.Forms.ComboBox();
            this.fjy_timeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.fjy_user_idComboBox = new System.Windows.Forms.ComboBox();
            this.fsampling_timeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.fsampling_user_idComboBox = new System.Windows.Forms.ComboBox();
            this.fapply_timeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.buttonfapply_user_id = new System.Windows.Forms.Button();
            this.fapply_user_idComboBox = new System.Windows.Forms.ComboBox();
            this.buttonSample = new System.Windows.Forms.Button();
            this.fsample_type_idComboBox = new System.Windows.Forms.ComboBox();
            this.fdiagnoseTextBox = new System.Windows.Forms.TextBox();
            this.fbed_numTextBox = new System.Windows.Forms.TextBox();
            this.froom_numTextBox = new System.Windows.Forms.TextBox();
            this.buttonfapply_dept_id = new System.Windows.Forms.Button();
            this.fapply_dept_idComboBox = new System.Windows.Forms.ComboBox();
            this.fage_unitComboBox = new System.Windows.Forms.ComboBox();
            this.fageTextBox = new System.Windows.Forms.TextBox();
            this.fsexComboBox = new System.Windows.Forms.ComboBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fhz_idTextBox = new System.Windows.Forms.TextBox();
            this.fhz_zyhTextBox = new System.Windows.Forms.TextBox();
            this.ftype_idComboBox = new System.Windows.Forms.ComboBox();
            this.fsample_barcodeTextBox = new System.Windows.Forms.TextBox();
            this.fsample_codeTextBox = new System.Windows.Forms.TextBox();
            this.fjy_dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.fjy_instrComboBox = new System.Windows.Forms.ComboBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.bindingSourceResult = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageReportList = new System.Windows.Forms.TabPage();
            this.tabPageImg = new System.Windows.Forms.TabPage();
            this.img5 = new System.Windows.Forms.PictureBox();
            this.lIS_REPORT_IMGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.img1 = new System.Windows.Forms.PictureBox();
            this.contextMenuStripimg1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.导入图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清空图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.图片浏览ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.img4 = new System.Windows.Forms.PictureBox();
            this.img3 = new System.Windows.Forms.PictureBox();
            this.img2 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            fjy_instrLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            fsample_codeLabel = new System.Windows.Forms.Label();
            fsample_barcodeLabel = new System.Windows.Forms.Label();
            ftype_idLabel = new System.Windows.Forms.Label();
            fhz_zyhLabel = new System.Windows.Forms.Label();
            fhz_idLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fageLabel = new System.Windows.Forms.Label();
            fapply_dept_idLabel = new System.Windows.Forms.Label();
            froom_numLabel = new System.Windows.Forms.Label();
            fbed_numLabel = new System.Windows.Forms.Label();
            fdiagnoseLabel = new System.Windows.Forms.Label();
            fsample_type_idLabel = new System.Windows.Forms.Label();
            fapply_user_idLabel = new System.Windows.Forms.Label();
            fapply_timeLabel = new System.Windows.Forms.Label();
            fsampling_user_idLabel = new System.Windows.Forms.Label();
            fsampling_timeLabel = new System.Windows.Forms.Label();
            fjy_user_idLabel = new System.Windows.Forms.Label();
            fjy_timeLabel = new System.Windows.Forms.Label();
            fexamine_user_idLabel = new System.Windows.Forms.Label();
            fexamine_timeLabel = new System.Windows.Forms.Label();
            fremarkLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjytype_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fsample_type_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_user_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_dept_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjy_user_id)).BeginInit();
            this.contextMenuStripReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceResult)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageReportList.SuspendLayout();
            this.tabPageImg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).BeginInit();
            this.contextMenuStripimg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).BeginInit();
            this.SuspendLayout();
            // 
            // fjy_instrLabel
            // 
            fjy_instrLabel.AutoSize = true;
            fjy_instrLabel.Location = new System.Drawing.Point(28, 30);
            fjy_instrLabel.Name = "fjy_instrLabel";
            fjy_instrLabel.Size = new System.Drawing.Size(35, 12);
            fjy_instrLabel.TabIndex = 0;
            fjy_instrLabel.Text = "仪器:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(4, 51);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 2;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(16, 73);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(47, 12);
            fsample_codeLabel.TabIndex = 4;
            fsample_codeLabel.Text = "样本号:";
            // 
            // fsample_barcodeLabel
            // 
            fsample_barcodeLabel.AutoSize = true;
            fsample_barcodeLabel.Location = new System.Drawing.Point(16, 95);
            fsample_barcodeLabel.Name = "fsample_barcodeLabel";
            fsample_barcodeLabel.Size = new System.Drawing.Size(47, 12);
            fsample_barcodeLabel.TabIndex = 6;
            fsample_barcodeLabel.Text = "条码号:";
            // 
            // ftype_idLabel
            // 
            ftype_idLabel.AutoSize = true;
            ftype_idLabel.Location = new System.Drawing.Point(4, 117);
            ftype_idLabel.Name = "ftype_idLabel";
            ftype_idLabel.Size = new System.Drawing.Size(59, 12);
            ftype_idLabel.TabIndex = 8;
            ftype_idLabel.Text = "病人类别:";
            // 
            // fhz_zyhLabel
            // 
            fhz_zyhLabel.AutoSize = true;
            fhz_zyhLabel.Location = new System.Drawing.Point(16, 138);
            fhz_zyhLabel.Name = "fhz_zyhLabel";
            fhz_zyhLabel.Size = new System.Drawing.Size(47, 12);
            fhz_zyhLabel.TabIndex = 10;
            fhz_zyhLabel.Text = "住院号:";
            // 
            // fhz_idLabel
            // 
            fhz_idLabel.AutoSize = true;
            fhz_idLabel.Location = new System.Drawing.Point(16, 160);
            fhz_idLabel.Name = "fhz_idLabel";
            fhz_idLabel.Size = new System.Drawing.Size(47, 12);
            fhz_idLabel.TabIndex = 12;
            fhz_idLabel.Text = "病人ID:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(4, 182);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(59, 12);
            fnameLabel.TabIndex = 14;
            fnameLabel.Text = "病人姓名:";
            // 
            // fageLabel
            // 
            fageLabel.AutoSize = true;
            fageLabel.Location = new System.Drawing.Point(4, 204);
            fageLabel.Name = "fageLabel";
            fageLabel.Size = new System.Drawing.Size(59, 12);
            fageLabel.TabIndex = 18;
            fageLabel.Text = "病人年龄:";
            // 
            // fapply_dept_idLabel
            // 
            fapply_dept_idLabel.AutoSize = true;
            fapply_dept_idLabel.Location = new System.Drawing.Point(4, 226);
            fapply_dept_idLabel.Name = "fapply_dept_idLabel";
            fapply_dept_idLabel.Size = new System.Drawing.Size(59, 12);
            fapply_dept_idLabel.TabIndex = 21;
            fapply_dept_idLabel.Text = "病人科别:";
            // 
            // froom_numLabel
            // 
            froom_numLabel.AutoSize = true;
            froom_numLabel.Location = new System.Drawing.Point(4, 248);
            froom_numLabel.Name = "froom_numLabel";
            froom_numLabel.Size = new System.Drawing.Size(59, 12);
            froom_numLabel.TabIndex = 126;
            froom_numLabel.Text = "病人房间:";
            // 
            // fbed_numLabel
            // 
            fbed_numLabel.AutoSize = true;
            fbed_numLabel.Location = new System.Drawing.Point(115, 248);
            fbed_numLabel.Name = "fbed_numLabel";
            fbed_numLabel.Size = new System.Drawing.Size(35, 12);
            fbed_numLabel.TabIndex = 129;
            fbed_numLabel.Text = "床号:";
            // 
            // fdiagnoseLabel
            // 
            fdiagnoseLabel.AutoSize = true;
            fdiagnoseLabel.Location = new System.Drawing.Point(4, 270);
            fdiagnoseLabel.Name = "fdiagnoseLabel";
            fdiagnoseLabel.Size = new System.Drawing.Size(59, 12);
            fdiagnoseLabel.TabIndex = 129;
            fdiagnoseLabel.Text = "临床诊断:";
            // 
            // fsample_type_idLabel
            // 
            fsample_type_idLabel.AutoSize = true;
            fsample_type_idLabel.Location = new System.Drawing.Point(4, 292);
            fsample_type_idLabel.Name = "fsample_type_idLabel";
            fsample_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fsample_type_idLabel.TabIndex = 130;
            fsample_type_idLabel.Text = "样本种类:";
            // 
            // fapply_user_idLabel
            // 
            fapply_user_idLabel.AutoSize = true;
            fapply_user_idLabel.Location = new System.Drawing.Point(4, 314);
            fapply_user_idLabel.Name = "fapply_user_idLabel";
            fapply_user_idLabel.Size = new System.Drawing.Size(59, 12);
            fapply_user_idLabel.TabIndex = 132;
            fapply_user_idLabel.Text = "送检医师:";
            // 
            // fapply_timeLabel
            // 
            fapply_timeLabel.AutoSize = true;
            fapply_timeLabel.Location = new System.Drawing.Point(4, 336);
            fapply_timeLabel.Name = "fapply_timeLabel";
            fapply_timeLabel.Size = new System.Drawing.Size(59, 12);
            fapply_timeLabel.TabIndex = 134;
            fapply_timeLabel.Text = "送检时间:";
            // 
            // fsampling_user_idLabel
            // 
            fsampling_user_idLabel.AutoSize = true;
            fsampling_user_idLabel.Location = new System.Drawing.Point(4, 358);
            fsampling_user_idLabel.Name = "fsampling_user_idLabel";
            fsampling_user_idLabel.Size = new System.Drawing.Size(59, 12);
            fsampling_user_idLabel.TabIndex = 135;
            fsampling_user_idLabel.Text = "采样医师:";
            // 
            // fsampling_timeLabel
            // 
            fsampling_timeLabel.AutoSize = true;
            fsampling_timeLabel.Location = new System.Drawing.Point(4, 379);
            fsampling_timeLabel.Name = "fsampling_timeLabel";
            fsampling_timeLabel.Size = new System.Drawing.Size(59, 12);
            fsampling_timeLabel.TabIndex = 136;
            fsampling_timeLabel.Text = "采样时间:";
            // 
            // fjy_user_idLabel
            // 
            fjy_user_idLabel.AutoSize = true;
            fjy_user_idLabel.Location = new System.Drawing.Point(4, 401);
            fjy_user_idLabel.Name = "fjy_user_idLabel";
            fjy_user_idLabel.Size = new System.Drawing.Size(59, 12);
            fjy_user_idLabel.TabIndex = 137;
            fjy_user_idLabel.Text = "检验医师:";
            // 
            // fjy_timeLabel
            // 
            fjy_timeLabel.AutoSize = true;
            fjy_timeLabel.Location = new System.Drawing.Point(4, 422);
            fjy_timeLabel.Name = "fjy_timeLabel";
            fjy_timeLabel.Size = new System.Drawing.Size(59, 12);
            fjy_timeLabel.TabIndex = 138;
            fjy_timeLabel.Text = "检验时间:";
            // 
            // fexamine_user_idLabel
            // 
            fexamine_user_idLabel.AutoSize = true;
            fexamine_user_idLabel.Location = new System.Drawing.Point(4, 444);
            fexamine_user_idLabel.Name = "fexamine_user_idLabel";
            fexamine_user_idLabel.Size = new System.Drawing.Size(59, 12);
            fexamine_user_idLabel.TabIndex = 139;
            fexamine_user_idLabel.Text = "审核医师:";
            // 
            // fexamine_timeLabel
            // 
            fexamine_timeLabel.AutoSize = true;
            fexamine_timeLabel.Location = new System.Drawing.Point(4, 465);
            fexamine_timeLabel.Name = "fexamine_timeLabel";
            fexamine_timeLabel.Size = new System.Drawing.Size(59, 12);
            fexamine_timeLabel.TabIndex = 140;
            fexamine_timeLabel.Text = "审核时间:";
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(28, 487);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(35, 12);
            fremarkLabel.TabIndex = 141;
            fremarkLabel.Text = "备注:";
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.lIS_REPORTBindingSource;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator5,
            this.toolStripButtonAddItem,
            this.toolStripButtonDelItem,
            this.toolStripSeparator9,
            this.toolStripButtoninstrDate,
            this.toolStripSeparator7,
            this.toolStripButtonU,
            this.toolStripButtonD,
            this.toolStripButtonSave,
            this.toolStripButtonJCSH,
            this.toolStripButtonPrint,
            this.toolStripSeparator6,
            this.toolStripButtonHelp});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(788, 30);
            this.bN.TabIndex = 129;
            this.bN.Text = "bindingNavigator1";
            // 
            // lIS_REPORTBindingSource
            // 
            this.lIS_REPORTBindingSource.DataMember = "LIS_REPORT";
            this.lIS_REPORTBindingSource.DataSource = this.samDataSet;
            this.lIS_REPORTBindingSource.PositionChanged += new System.EventHandler(this.lIS_REPORTBindingSource_PositionChanged);
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(48, 16);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(25, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAddItem
            // 
            this.toolStripButtonAddItem.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddItem.Image")));
            this.toolStripButtonAddItem.Name = "toolStripButtonAddItem";
            this.toolStripButtonAddItem.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAddItem.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAddItem.Size = new System.Drawing.Size(92, 27);
            this.toolStripButtonAddItem.Text = "新增(F1)";
            this.toolStripButtonAddItem.Click += new System.EventHandler(this.toolStripButtonAddItem_Click);
            // 
            // toolStripButtonDelItem
            // 
            this.toolStripButtonDelItem.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDelItem.Image")));
            this.toolStripButtonDelItem.Name = "toolStripButtonDelItem";
            this.toolStripButtonDelItem.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDelItem.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDelItem.Size = new System.Drawing.Size(92, 27);
            this.toolStripButtonDelItem.Text = "取消(F2)";
            this.toolStripButtonDelItem.Click += new System.EventHandler(this.toolStripButtonDelItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtoninstrDate
            // 
            this.toolStripButtoninstrDate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtoninstrDate.Image")));
            this.toolStripButtoninstrDate.Name = "toolStripButtoninstrDate";
            this.toolStripButtoninstrDate.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtoninstrDate.RightToLeftAutoMirrorImage = true;
            this.toolStripButtoninstrDate.Size = new System.Drawing.Size(124, 27);
            this.toolStripButtoninstrDate.Text = "仪器数据(F3)";
            this.toolStripButtoninstrDate.Click += new System.EventHandler(this.toolStripButtoninstrDate_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonU
            // 
            this.toolStripButtonU.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonU.Image")));
            this.toolStripButtonU.Name = "toolStripButtonU";
            this.toolStripButtonU.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonU.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonU.Size = new System.Drawing.Size(108, 27);
            this.toolStripButtonU.Text = "上一个(F4)";
            this.toolStripButtonU.Click += new System.EventHandler(this.toolStripButtonU_Click);
            // 
            // toolStripButtonD
            // 
            this.toolStripButtonD.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonD.Image")));
            this.toolStripButtonD.Name = "toolStripButtonD";
            this.toolStripButtonD.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonD.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonD.Size = new System.Drawing.Size(108, 27);
            this.toolStripButtonD.Text = "下一个(F5)";
            this.toolStripButtonD.Click += new System.EventHandler(this.toolStripButtonD_Click);
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSave.Image")));
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonSave.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonSave.Size = new System.Drawing.Size(92, 27);
            this.toolStripButtonSave.Text = "保存(F6)";
            this.toolStripButtonSave.Click += new System.EventHandler(this.toolStripButtonSave_Click);
            // 
            // toolStripButtonJCSH
            // 
            this.toolStripButtonJCSH.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonJCSH.Image")));
            this.toolStripButtonJCSH.Name = "toolStripButtonJCSH";
            this.toolStripButtonJCSH.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonJCSH.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonJCSH.Size = new System.Drawing.Size(92, 27);
            this.toolStripButtonJCSH.Text = "审核(F8)";
            this.toolStripButtonJCSH.Click += new System.EventHandler(this.toolStripButtonJCSH_Click);
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrint.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonPrint.Size = new System.Drawing.Size(92, 27);
            this.toolStripButtonPrint.Text = "打印(F9)";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButtonPrint_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(60, 20);
            this.toolStripButtonHelp.Text = "帮助";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToDeleteRows = false;
            this.dataGridViewReport.AllowUserToOrderColumns = true;
            this.dataGridViewReport.AllowUserToResizeRows = false;
            this.dataGridViewReport.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fexamine_flag,
            this.fprint_flag,
            this.fstate,
            this.fjytype_id,
            this.fsample_type_id,
            this.fapply_user_id,
            this.fapply_dept_id,
            this.fjy_user_id});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReport.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewReport.MultiSelect = false;
            this.dataGridViewReport.Name = "dataGridViewReport";
            this.dataGridViewReport.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridViewReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.RowHeadersWidth = 20;
            this.dataGridViewReport.RowTemplate.Height = 23;
            this.dataGridViewReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReport.Size = new System.Drawing.Size(330, 477);
            this.dataGridViewReport.TabIndex = 12;
            this.dataGridViewReport.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewReport_DataError);
            this.dataGridViewReport.Click += new System.EventHandler(this.dataGridViewReport_Click);
            // 
            // fexamine_flag
            // 
            this.fexamine_flag.DataPropertyName = "fexamine_flag";
            this.fexamine_flag.FalseValue = "0";
            this.fexamine_flag.HeaderText = "fexamine_flag";
            this.fexamine_flag.IndeterminateValue = "0";
            this.fexamine_flag.Name = "fexamine_flag";
            this.fexamine_flag.ReadOnly = true;
            this.fexamine_flag.TrueValue = "1";
            // 
            // fprint_flag
            // 
            this.fprint_flag.DataPropertyName = "fprint_flag";
            this.fprint_flag.FalseValue = "0";
            this.fprint_flag.HeaderText = "fprint_flag";
            this.fprint_flag.IndeterminateValue = "0";
            this.fprint_flag.Name = "fprint_flag";
            this.fprint_flag.ReadOnly = true;
            this.fprint_flag.TrueValue = "1";
            // 
            // fstate
            // 
            this.fstate.DataPropertyName = "fstate";
            this.fstate.DataSource = this.com_listBindingSource_fstate;
            this.fstate.DisplayMember = "fname";
            this.fstate.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fstate.HeaderText = "fstate";
            this.fstate.Name = "fstate";
            this.fstate.ValueMember = "fcode";
            // 
            // com_listBindingSource_fstate
            // 
            this.com_listBindingSource_fstate.DataMember = "com_list";
            this.com_listBindingSource_fstate.DataSource = this.samDataSet;
            // 
            // fjytype_id
            // 
            this.fjytype_id.DataPropertyName = "fjytype_id";
            this.fjytype_id.DataSource = this.com_listBindingSource_fjytype_id;
            this.fjytype_id.DisplayMember = "fname";
            this.fjytype_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fjytype_id.HeaderText = "fjytype_id";
            this.fjytype_id.Name = "fjytype_id";
            this.fjytype_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fjytype_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fjytype_id.ValueMember = "fcheck_type_id";
            // 
            // com_listBindingSource_fjytype_id
            // 
            this.com_listBindingSource_fjytype_id.DataMember = "com_list";
            this.com_listBindingSource_fjytype_id.DataSource = this.samDataSet;
            // 
            // fsample_type_id
            // 
            this.fsample_type_id.DataPropertyName = "fsample_type_id";
            this.fsample_type_id.DataSource = this.com_listBindingSource_fsample_type_id;
            this.fsample_type_id.DisplayMember = "fname";
            this.fsample_type_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fsample_type_id.HeaderText = "fsample_type_id";
            this.fsample_type_id.Name = "fsample_type_id";
            this.fsample_type_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fsample_type_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fsample_type_id.ValueMember = "fsample_type_id";
            // 
            // com_listBindingSource_fsample_type_id
            // 
            this.com_listBindingSource_fsample_type_id.DataMember = "com_list";
            this.com_listBindingSource_fsample_type_id.DataSource = this.samDataSet;
            // 
            // fapply_user_id
            // 
            this.fapply_user_id.DataPropertyName = "fapply_user_id";
            this.fapply_user_id.DataSource = this.com_listBindingSource_fapply_user_id;
            this.fapply_user_id.DisplayMember = "fname";
            this.fapply_user_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fapply_user_id.HeaderText = "fapply_user_id";
            this.fapply_user_id.Name = "fapply_user_id";
            this.fapply_user_id.ValueMember = "fperson_id";
            // 
            // com_listBindingSource_fapply_user_id
            // 
            this.com_listBindingSource_fapply_user_id.DataMember = "com_list";
            this.com_listBindingSource_fapply_user_id.DataSource = this.samDataSet;
            // 
            // fapply_dept_id
            // 
            this.fapply_dept_id.DataPropertyName = "fapply_dept_id";
            this.fapply_dept_id.DataSource = this.com_listBindingSource_fapply_dept_id;
            this.fapply_dept_id.DisplayMember = "fname";
            this.fapply_dept_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fapply_dept_id.HeaderText = "fapply_dept_id";
            this.fapply_dept_id.Name = "fapply_dept_id";
            this.fapply_dept_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fapply_dept_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fapply_dept_id.ValueMember = "fdept_id";
            // 
            // com_listBindingSource_fapply_dept_id
            // 
            this.com_listBindingSource_fapply_dept_id.DataMember = "com_list";
            this.com_listBindingSource_fapply_dept_id.DataSource = this.samDataSet;
            // 
            // fjy_user_id
            // 
            this.fjy_user_id.DataPropertyName = "fjy_user_id";
            this.fjy_user_id.DataSource = this.com_listBindingSource_fjy_user_id;
            this.fjy_user_id.DisplayMember = "fname";
            this.fjy_user_id.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.fjy_user_id.HeaderText = "fjy_user_id";
            this.fjy_user_id.Name = "fjy_user_id";
            this.fjy_user_id.ValueMember = "fperson_id";
            // 
            // com_listBindingSource_fjy_user_id
            // 
            this.com_listBindingSource_fjy_user_id.DataMember = "com_list";
            this.com_listBindingSource_fjy_user_id.DataSource = this.samDataSet;
            // 
            // contextMenuStripReport
            // 
            this.contextMenuStripReport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_ReportCol,
            this.toolStripMenuItemExamine,
            this.toolStripMenuItemExamineNO,
            this.toolStripMenuItemReportDel,
            this.toolStripMenuItem_Ref,
            this.toolStripSeparator2,
            this.toolStripMenuItem_result,
            this.toolStripMenuItemIntData,
            this.toolStripMenuItemResultAdd,
            this.toolStripMenuItemResultValue,
            this.toolStripMenuItemResultDel,
            this.toolStripSeparator1,
            this.toolStripMenuItemPrint,
            this.toolStripMenuItemPrinYL,
            this.toolStripSeparator3,
            this.toolStripMenuItemLS,
            this.toolStripSeparator4,
            this.关闭ToolStripMenuItem});
            this.contextMenuStripReport.Name = "contextMenuStripReport";
            this.contextMenuStripReport.Size = new System.Drawing.Size(205, 336);
            // 
            // toolStripMenuItem_ReportCol
            // 
            this.toolStripMenuItem_ReportCol.Name = "toolStripMenuItem_ReportCol";
            this.toolStripMenuItem_ReportCol.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItem_ReportCol.Text = "报告格式";
            this.toolStripMenuItem_ReportCol.Click += new System.EventHandler(this.toolStripMenuItem_ReportCol_Click);
            // 
            // toolStripMenuItemExamine
            // 
            this.toolStripMenuItemExamine.Name = "toolStripMenuItemExamine";
            this.toolStripMenuItemExamine.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemExamine.Text = "报告审核";
            this.toolStripMenuItemExamine.Click += new System.EventHandler(this.toolStripMenuItemExamine_Click);
            // 
            // toolStripMenuItemExamineNO
            // 
            this.toolStripMenuItemExamineNO.Name = "toolStripMenuItemExamineNO";
            this.toolStripMenuItemExamineNO.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemExamineNO.Text = "报告解出审核";
            this.toolStripMenuItemExamineNO.Click += new System.EventHandler(this.toolStripMenuItemSHJC_Click);
            // 
            // toolStripMenuItemReportDel
            // 
            this.toolStripMenuItemReportDel.Name = "toolStripMenuItemReportDel";
            this.toolStripMenuItemReportDel.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemReportDel.Text = "报告删除";
            this.toolStripMenuItemReportDel.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem_Ref
            // 
            this.toolStripMenuItem_Ref.Name = "toolStripMenuItem_Ref";
            this.toolStripMenuItem_Ref.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItem_Ref.Text = "报告刷新";
            this.toolStripMenuItem_Ref.Click += new System.EventHandler(this.toolStripMenuItem_Ref_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripMenuItem_result
            // 
            this.toolStripMenuItem_result.Name = "toolStripMenuItem_result";
            this.toolStripMenuItem_result.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItem_result.Text = "结果格式";
            this.toolStripMenuItem_result.Click += new System.EventHandler(this.toolStripMenuItem_result_Click);
            // 
            // toolStripMenuItemIntData
            // 
            this.toolStripMenuItemIntData.Name = "toolStripMenuItemIntData";
            this.toolStripMenuItemIntData.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemIntData.Text = "仪器数据";
            this.toolStripMenuItemIntData.Click += new System.EventHandler(this.toolStripMenuItemIntData_Click);
            // 
            // toolStripMenuItemResultAdd
            // 
            this.toolStripMenuItemResultAdd.Name = "toolStripMenuItemResultAdd";
            this.toolStripMenuItemResultAdd.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemResultAdd.Text = "结果项目添加";
            this.toolStripMenuItemResultAdd.Click += new System.EventHandler(this.toolStripMenuItemResultAdd_Click);
            // 
            // toolStripMenuItemResultValue
            // 
            this.toolStripMenuItemResultValue.Name = "toolStripMenuItemResultValue";
            this.toolStripMenuItemResultValue.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemResultValue.Text = "结果项目常用取值";
            this.toolStripMenuItemResultValue.Click += new System.EventHandler(this.toolStripMenuItemResultValue_Click);
            // 
            // toolStripMenuItemResultDel
            // 
            this.toolStripMenuItemResultDel.Name = "toolStripMenuItemResultDel";
            this.toolStripMenuItemResultDel.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemResultDel.Text = "结果项目删除";
            this.toolStripMenuItemResultDel.Click += new System.EventHandler(this.toolStripMenuItemResultDel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripMenuItemPrint
            // 
            this.toolStripMenuItemPrint.Name = "toolStripMenuItemPrint";
            this.toolStripMenuItemPrint.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemPrint.Text = "打印";
            this.toolStripMenuItemPrint.Click += new System.EventHandler(this.toolStripMenuItemPrint_Click);
            // 
            // toolStripMenuItemPrinYL
            // 
            this.toolStripMenuItemPrinYL.Name = "toolStripMenuItemPrinYL";
            this.toolStripMenuItemPrinYL.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemPrinYL.Text = "打印预览";
            this.toolStripMenuItemPrinYL.Click += new System.EventHandler(this.toolStripMenuItemPrinYL_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripMenuItemLS
            // 
            this.toolStripMenuItemLS.Name = "toolStripMenuItemLS";
            this.toolStripMenuItemLS.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemLS.Text = "历史";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(201, 6);
            // 
            // 关闭ToolStripMenuItem
            // 
            this.关闭ToolStripMenuItem.Name = "关闭ToolStripMenuItem";
            this.关闭ToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.关闭ToolStripMenuItem.Text = "关闭";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(441, 30);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 508);
            this.splitter1.TabIndex = 130;
            this.splitter1.TabStop = false;
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResult.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewResult.Location = new System.Drawing.Point(208, 30);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidth = 30;
            this.dataGridViewResult.RowTemplate.Height = 23;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewResult.ShowCellToolTips = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.Size = new System.Drawing.Size(233, 508);
            this.dataGridViewResult.TabIndex = 131;
            this.dataGridViewResult.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewResult_CellDoubleClick);
            this.dataGridViewResult.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewResult_DataError);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(fremarkLabel);
            this.panel1.Controls.Add(this.fremarkTextBox);
            this.panel1.Controls.Add(fexamine_timeLabel);
            this.panel1.Controls.Add(this.fexamine_timeDateTimePicker);
            this.panel1.Controls.Add(fexamine_user_idLabel);
            this.panel1.Controls.Add(this.fexamine_user_idComboBox);
            this.panel1.Controls.Add(fjy_timeLabel);
            this.panel1.Controls.Add(this.fjy_timeDateTimePicker);
            this.panel1.Controls.Add(fjy_user_idLabel);
            this.panel1.Controls.Add(this.fjy_user_idComboBox);
            this.panel1.Controls.Add(fsampling_timeLabel);
            this.panel1.Controls.Add(this.fsampling_timeDateTimePicker);
            this.panel1.Controls.Add(fsampling_user_idLabel);
            this.panel1.Controls.Add(this.fsampling_user_idComboBox);
            this.panel1.Controls.Add(fapply_timeLabel);
            this.panel1.Controls.Add(this.fapply_timeDateTimePicker);
            this.panel1.Controls.Add(this.buttonfapply_user_id);
            this.panel1.Controls.Add(fapply_user_idLabel);
            this.panel1.Controls.Add(this.fapply_user_idComboBox);
            this.panel1.Controls.Add(this.buttonSample);
            this.panel1.Controls.Add(fsample_type_idLabel);
            this.panel1.Controls.Add(this.fsample_type_idComboBox);
            this.panel1.Controls.Add(fdiagnoseLabel);
            this.panel1.Controls.Add(this.fdiagnoseTextBox);
            this.panel1.Controls.Add(fbed_numLabel);
            this.panel1.Controls.Add(this.fbed_numTextBox);
            this.panel1.Controls.Add(froom_numLabel);
            this.panel1.Controls.Add(this.froom_numTextBox);
            this.panel1.Controls.Add(this.buttonfapply_dept_id);
            this.panel1.Controls.Add(fapply_dept_idLabel);
            this.panel1.Controls.Add(this.fapply_dept_idComboBox);
            this.panel1.Controls.Add(this.fage_unitComboBox);
            this.panel1.Controls.Add(fageLabel);
            this.panel1.Controls.Add(this.fageTextBox);
            this.panel1.Controls.Add(this.fsexComboBox);
            this.panel1.Controls.Add(fnameLabel);
            this.panel1.Controls.Add(this.fnameTextBox);
            this.panel1.Controls.Add(fhz_idLabel);
            this.panel1.Controls.Add(this.fhz_idTextBox);
            this.panel1.Controls.Add(fhz_zyhLabel);
            this.panel1.Controls.Add(this.fhz_zyhTextBox);
            this.panel1.Controls.Add(ftype_idLabel);
            this.panel1.Controls.Add(this.ftype_idComboBox);
            this.panel1.Controls.Add(fsample_barcodeLabel);
            this.panel1.Controls.Add(this.fsample_barcodeTextBox);
            this.panel1.Controls.Add(fsample_codeLabel);
            this.panel1.Controls.Add(this.fsample_codeTextBox);
            this.panel1.Controls.Add(fjy_dateLabel);
            this.panel1.Controls.Add(this.fjy_dateDateTimePicker);
            this.panel1.Controls.Add(fjy_instrLabel);
            this.panel1.Controls.Add(this.fjy_instrComboBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(205, 508);
            this.panel1.TabIndex = 132;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButtonJZ);
            this.panel3.Controls.Add(this.radioButtonCG);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(205, 22);
            this.panel3.TabIndex = 143;
            // 
            // radioButtonJZ
            // 
            this.radioButtonJZ.AutoSize = true;
            this.radioButtonJZ.Location = new System.Drawing.Point(117, 3);
            this.radioButtonJZ.Name = "radioButtonJZ";
            this.radioButtonJZ.Size = new System.Drawing.Size(47, 16);
            this.radioButtonJZ.TabIndex = 1;
            this.radioButtonJZ.Text = "急诊";
            this.radioButtonJZ.UseVisualStyleBackColor = true;
            // 
            // radioButtonCG
            // 
            this.radioButtonCG.AutoSize = true;
            this.radioButtonCG.Checked = true;
            this.radioButtonCG.Location = new System.Drawing.Point(43, 3);
            this.radioButtonCG.Name = "radioButtonCG";
            this.radioButtonCG.Size = new System.Drawing.Size(47, 16);
            this.radioButtonCG.TabIndex = 0;
            this.radioButtonCG.TabStop = true;
            this.radioButtonCG.Text = "常规";
            this.radioButtonCG.UseVisualStyleBackColor = true;
            // 
            // fremarkTextBox
            // 
            this.fremarkTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fremark", true));
            this.fremarkTextBox.Location = new System.Drawing.Point(63, 483);
            this.fremarkTextBox.Multiline = true;
            this.fremarkTextBox.Name = "fremarkTextBox";
            this.fremarkTextBox.Size = new System.Drawing.Size(140, 25);
            this.fremarkTextBox.TabIndex = 142;
            this.fremarkTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fremarkTextBox_KeyDown);
            // 
            // fexamine_timeDateTimePicker
            // 
            this.fexamine_timeDateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.fexamine_timeDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.lIS_REPORTBindingSource, "fexamine_time", true));
            this.fexamine_timeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fexamine_timeDateTimePicker.Location = new System.Drawing.Point(63, 461);
            this.fexamine_timeDateTimePicker.Name = "fexamine_timeDateTimePicker";
            this.fexamine_timeDateTimePicker.ShowUpDown = true;
            this.fexamine_timeDateTimePicker.Size = new System.Drawing.Size(140, 21);
            this.fexamine_timeDateTimePicker.TabIndex = 141;
            this.fexamine_timeDateTimePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fexamine_timeDateTimePicker_KeyDown);
            // 
            // fexamine_user_idComboBox
            // 
            this.fexamine_user_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "fexamine_user_id", true));
            this.fexamine_user_idComboBox.DisplayMember = "fshow_name";
            this.fexamine_user_idComboBox.FormattingEnabled = true;
            this.fexamine_user_idComboBox.Location = new System.Drawing.Point(63, 440);
            this.fexamine_user_idComboBox.Name = "fexamine_user_idComboBox";
            this.fexamine_user_idComboBox.Size = new System.Drawing.Size(140, 20);
            this.fexamine_user_idComboBox.TabIndex = 140;
            this.fexamine_user_idComboBox.ValueMember = "fperson_id";
            this.fexamine_user_idComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fexamine_user_idComboBox_KeyDown);
            // 
            // fjy_timeDateTimePicker
            // 
            this.fjy_timeDateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.fjy_timeDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.lIS_REPORTBindingSource, "fjy_time", true));
            this.fjy_timeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fjy_timeDateTimePicker.Location = new System.Drawing.Point(63, 418);
            this.fjy_timeDateTimePicker.Name = "fjy_timeDateTimePicker";
            this.fjy_timeDateTimePicker.ShowUpDown = true;
            this.fjy_timeDateTimePicker.Size = new System.Drawing.Size(140, 21);
            this.fjy_timeDateTimePicker.TabIndex = 139;
            this.fjy_timeDateTimePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fjy_timeDateTimePicker_KeyDown);
            // 
            // fjy_user_idComboBox
            // 
            this.fjy_user_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "fjy_user_id", true));
            this.fjy_user_idComboBox.DisplayMember = "fshow_name";
            this.fjy_user_idComboBox.FormattingEnabled = true;
            this.fjy_user_idComboBox.Location = new System.Drawing.Point(63, 397);
            this.fjy_user_idComboBox.Name = "fjy_user_idComboBox";
            this.fjy_user_idComboBox.Size = new System.Drawing.Size(140, 20);
            this.fjy_user_idComboBox.TabIndex = 138;
            this.fjy_user_idComboBox.ValueMember = "fperson_id";
            this.fjy_user_idComboBox.SelectedIndexChanged += new System.EventHandler(this.fjy_user_idComboBox_SelectedIndexChanged);
            this.fjy_user_idComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fjy_user_idComboBox_KeyDown);
            // 
            // fsampling_timeDateTimePicker
            // 
            this.fsampling_timeDateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.fsampling_timeDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.lIS_REPORTBindingSource, "fsampling_time", true));
            this.fsampling_timeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fsampling_timeDateTimePicker.Location = new System.Drawing.Point(63, 375);
            this.fsampling_timeDateTimePicker.Name = "fsampling_timeDateTimePicker";
            this.fsampling_timeDateTimePicker.ShowUpDown = true;
            this.fsampling_timeDateTimePicker.Size = new System.Drawing.Size(140, 21);
            this.fsampling_timeDateTimePicker.TabIndex = 137;
            this.fsampling_timeDateTimePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsampling_timeDateTimePicker_KeyDown);
            // 
            // fsampling_user_idComboBox
            // 
            this.fsampling_user_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "fsampling_user_id", true));
            this.fsampling_user_idComboBox.DisplayMember = "fshow_name";
            this.fsampling_user_idComboBox.FormattingEnabled = true;
            this.fsampling_user_idComboBox.Location = new System.Drawing.Point(63, 354);
            this.fsampling_user_idComboBox.Name = "fsampling_user_idComboBox";
            this.fsampling_user_idComboBox.Size = new System.Drawing.Size(140, 20);
            this.fsampling_user_idComboBox.TabIndex = 136;
            this.fsampling_user_idComboBox.ValueMember = "fperson_id";
            this.fsampling_user_idComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsampling_user_idComboBox_KeyDown);
            // 
            // fapply_timeDateTimePicker
            // 
            this.fapply_timeDateTimePicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.fapply_timeDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.lIS_REPORTBindingSource, "fapply_time", true));
            this.fapply_timeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fapply_timeDateTimePicker.Location = new System.Drawing.Point(63, 332);
            this.fapply_timeDateTimePicker.Name = "fapply_timeDateTimePicker";
            this.fapply_timeDateTimePicker.ShowUpDown = true;
            this.fapply_timeDateTimePicker.Size = new System.Drawing.Size(140, 21);
            this.fapply_timeDateTimePicker.TabIndex = 135;
            this.fapply_timeDateTimePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fapply_timeDateTimePicker_KeyDown);
            // 
            // buttonfapply_user_id
            // 
            this.buttonfapply_user_id.Image = ((System.Drawing.Image)(resources.GetObject("buttonfapply_user_id.Image")));
            this.buttonfapply_user_id.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonfapply_user_id.Location = new System.Drawing.Point(178, 309);
            this.buttonfapply_user_id.Name = "buttonfapply_user_id";
            this.buttonfapply_user_id.Size = new System.Drawing.Size(25, 23);
            this.buttonfapply_user_id.TabIndex = 134;
            this.buttonfapply_user_id.UseVisualStyleBackColor = true;
            this.buttonfapply_user_id.Click += new System.EventHandler(this.buttonfapply_user_id_Click);
            this.buttonfapply_user_id.KeyDown += new System.Windows.Forms.KeyEventHandler(this.buttonfapply_user_id_KeyDown);
            // 
            // fapply_user_idComboBox
            // 
            this.fapply_user_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "fapply_user_id", true));
            this.fapply_user_idComboBox.DisplayMember = "fshow_name";
            this.fapply_user_idComboBox.FormattingEnabled = true;
            this.fapply_user_idComboBox.Location = new System.Drawing.Point(63, 310);
            this.fapply_user_idComboBox.Name = "fapply_user_idComboBox";
            this.fapply_user_idComboBox.Size = new System.Drawing.Size(109, 20);
            this.fapply_user_idComboBox.TabIndex = 133;
            this.fapply_user_idComboBox.ValueMember = "fperson_id";
            this.fapply_user_idComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fapply_user_idComboBox_KeyDown);
            // 
            // buttonSample
            // 
            this.buttonSample.Image = ((System.Drawing.Image)(resources.GetObject("buttonSample.Image")));
            this.buttonSample.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSample.Location = new System.Drawing.Point(178, 287);
            this.buttonSample.Name = "buttonSample";
            this.buttonSample.Size = new System.Drawing.Size(25, 23);
            this.buttonSample.TabIndex = 132;
            this.buttonSample.UseVisualStyleBackColor = true;
            this.buttonSample.Click += new System.EventHandler(this.buttonSample_Click);
            this.buttonSample.KeyDown += new System.Windows.Forms.KeyEventHandler(this.buttonSample_KeyDown);
            // 
            // fsample_type_idComboBox
            // 
            this.fsample_type_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "fsample_type_id", true));
            this.fsample_type_idComboBox.DisplayMember = "fname";
            this.fsample_type_idComboBox.FormattingEnabled = true;
            this.fsample_type_idComboBox.Location = new System.Drawing.Point(63, 288);
            this.fsample_type_idComboBox.Name = "fsample_type_idComboBox";
            this.fsample_type_idComboBox.Size = new System.Drawing.Size(109, 20);
            this.fsample_type_idComboBox.TabIndex = 131;
            this.fsample_type_idComboBox.ValueMember = "fsample_type_id";
            this.fsample_type_idComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsample_type_idComboBox_KeyDown);
            // 
            // fdiagnoseTextBox
            // 
            this.fdiagnoseTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fdiagnose", true));
            this.fdiagnoseTextBox.Location = new System.Drawing.Point(63, 266);
            this.fdiagnoseTextBox.Name = "fdiagnoseTextBox";
            this.fdiagnoseTextBox.Size = new System.Drawing.Size(140, 21);
            this.fdiagnoseTextBox.TabIndex = 130;
            this.fdiagnoseTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fdiagnoseTextBox_KeyDown);
            // 
            // fbed_numTextBox
            // 
            this.fbed_numTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fbed_num", true));
            this.fbed_numTextBox.Location = new System.Drawing.Point(155, 244);
            this.fbed_numTextBox.Name = "fbed_numTextBox";
            this.fbed_numTextBox.Size = new System.Drawing.Size(48, 21);
            this.fbed_numTextBox.TabIndex = 128;
            this.fbed_numTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fbed_numTextBox_KeyDown);
            // 
            // froom_numTextBox
            // 
            this.froom_numTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "froom_num", true));
            this.froom_numTextBox.Location = new System.Drawing.Point(63, 244);
            this.froom_numTextBox.Name = "froom_numTextBox";
            this.froom_numTextBox.Size = new System.Drawing.Size(48, 21);
            this.froom_numTextBox.TabIndex = 127;
            this.froom_numTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.froom_numTextBox_KeyDown);
            // 
            // buttonfapply_dept_id
            // 
            this.buttonfapply_dept_id.Image = ((System.Drawing.Image)(resources.GetObject("buttonfapply_dept_id.Image")));
            this.buttonfapply_dept_id.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonfapply_dept_id.Location = new System.Drawing.Point(178, 221);
            this.buttonfapply_dept_id.Name = "buttonfapply_dept_id";
            this.buttonfapply_dept_id.Size = new System.Drawing.Size(25, 23);
            this.buttonfapply_dept_id.TabIndex = 126;
            this.buttonfapply_dept_id.UseVisualStyleBackColor = true;
            this.buttonfapply_dept_id.Click += new System.EventHandler(this.buttonfapply_dept_id_Click);
            this.buttonfapply_dept_id.KeyDown += new System.Windows.Forms.KeyEventHandler(this.buttonfapply_dept_id_KeyDown);
            // 
            // fapply_dept_idComboBox
            // 
            this.fapply_dept_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "fapply_dept_id", true));
            this.fapply_dept_idComboBox.DisplayMember = "fname";
            this.fapply_dept_idComboBox.FormattingEnabled = true;
            this.fapply_dept_idComboBox.Location = new System.Drawing.Point(63, 222);
            this.fapply_dept_idComboBox.Name = "fapply_dept_idComboBox";
            this.fapply_dept_idComboBox.Size = new System.Drawing.Size(109, 20);
            this.fapply_dept_idComboBox.TabIndex = 22;
            this.fapply_dept_idComboBox.ValueMember = "fdept_id";
            this.fapply_dept_idComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fapply_dept_idComboBox_KeyDown);
            // 
            // fage_unitComboBox
            // 
            this.fage_unitComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "fage_unit", true));
            this.fage_unitComboBox.DisplayMember = "fname";
            this.fage_unitComboBox.FormattingEnabled = true;
            this.fage_unitComboBox.Location = new System.Drawing.Point(155, 200);
            this.fage_unitComboBox.Name = "fage_unitComboBox";
            this.fage_unitComboBox.Size = new System.Drawing.Size(48, 20);
            this.fage_unitComboBox.TabIndex = 21;
            this.fage_unitComboBox.ValueMember = "fname";
            this.fage_unitComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fage_unitComboBox_KeyDown);
            // 
            // fageTextBox
            // 
            this.fageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fage", true));
            this.fageTextBox.Location = new System.Drawing.Point(63, 200);
            this.fageTextBox.Name = "fageTextBox";
            this.fageTextBox.Size = new System.Drawing.Size(87, 21);
            this.fageTextBox.TabIndex = 19;
            this.fageTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fageTextBox_KeyDown);
            // 
            // fsexComboBox
            // 
            this.fsexComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "fsex", true));
            this.fsexComboBox.DisplayMember = "fname";
            this.fsexComboBox.FormattingEnabled = true;
            this.fsexComboBox.Location = new System.Drawing.Point(155, 179);
            this.fsexComboBox.Name = "fsexComboBox";
            this.fsexComboBox.Size = new System.Drawing.Size(48, 20);
            this.fsexComboBox.TabIndex = 17;
            this.fsexComboBox.ValueMember = "fname";
            this.fsexComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsexComboBox_KeyDown);
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fname", true));
            this.fnameTextBox.Location = new System.Drawing.Point(63, 178);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(87, 21);
            this.fnameTextBox.TabIndex = 15;
            this.fnameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fnameTextBox_KeyDown);
            // 
            // fhz_idTextBox
            // 
            this.fhz_idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fhz_id", true));
            this.fhz_idTextBox.Location = new System.Drawing.Point(63, 156);
            this.fhz_idTextBox.Name = "fhz_idTextBox";
            this.fhz_idTextBox.Size = new System.Drawing.Size(140, 21);
            this.fhz_idTextBox.TabIndex = 13;
            this.fhz_idTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fhz_idTextBox_KeyDown);
            // 
            // fhz_zyhTextBox
            // 
            this.fhz_zyhTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fhz_zyh", true));
            this.fhz_zyhTextBox.Location = new System.Drawing.Point(63, 134);
            this.fhz_zyhTextBox.Name = "fhz_zyhTextBox";
            this.fhz_zyhTextBox.Size = new System.Drawing.Size(140, 21);
            this.fhz_zyhTextBox.TabIndex = 11;
            this.fhz_zyhTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fhz_zyhTextBox_KeyDown);
            // 
            // ftype_idComboBox
            // 
            this.ftype_idComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIS_REPORTBindingSource, "ftype_id", true));
            this.ftype_idComboBox.DisplayMember = "fname";
            this.ftype_idComboBox.FormattingEnabled = true;
            this.ftype_idComboBox.Location = new System.Drawing.Point(63, 113);
            this.ftype_idComboBox.Name = "ftype_idComboBox";
            this.ftype_idComboBox.Size = new System.Drawing.Size(140, 20);
            this.ftype_idComboBox.TabIndex = 9;
            this.ftype_idComboBox.ValueMember = "fname";
            this.ftype_idComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ftype_idComboBox_KeyDown);
            // 
            // fsample_barcodeTextBox
            // 
            this.fsample_barcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fsample_barcode", true));
            this.fsample_barcodeTextBox.Location = new System.Drawing.Point(63, 91);
            this.fsample_barcodeTextBox.Name = "fsample_barcodeTextBox";
            this.fsample_barcodeTextBox.Size = new System.Drawing.Size(140, 21);
            this.fsample_barcodeTextBox.TabIndex = 7;
            this.fsample_barcodeTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsample_barcodeTextBox_KeyDown);
            // 
            // fsample_codeTextBox
            // 
            this.fsample_codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIS_REPORTBindingSource, "fsample_code", true));
            this.fsample_codeTextBox.Location = new System.Drawing.Point(63, 69);
            this.fsample_codeTextBox.Name = "fsample_codeTextBox";
            this.fsample_codeTextBox.Size = new System.Drawing.Size(140, 21);
            this.fsample_codeTextBox.TabIndex = 5;
            this.fsample_codeTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fsample_codeTextBox_KeyDown);
            // 
            // fjy_dateDateTimePicker
            // 
            this.fjy_dateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.lIS_REPORTBindingSource, "fjy_date", true));
            this.fjy_dateDateTimePicker.Location = new System.Drawing.Point(63, 47);
            this.fjy_dateDateTimePicker.Name = "fjy_dateDateTimePicker";
            this.fjy_dateDateTimePicker.Size = new System.Drawing.Size(140, 21);
            this.fjy_dateDateTimePicker.TabIndex = 3;
            this.fjy_dateDateTimePicker.CloseUp += new System.EventHandler(this.fjy_dateDateTimePicker_CloseUp);
            // 
            // fjy_instrComboBox
            // 
            this.fjy_instrComboBox.DisplayMember = "fname";
            this.fjy_instrComboBox.FormattingEnabled = true;
            this.fjy_instrComboBox.Location = new System.Drawing.Point(63, 26);
            this.fjy_instrComboBox.Name = "fjy_instrComboBox";
            this.fjy_instrComboBox.Size = new System.Drawing.Size(140, 20);
            this.fjy_instrComboBox.TabIndex = 1;
            this.fjy_instrComboBox.ValueMember = "finstr_id";
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(205, 30);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 508);
            this.splitter2.TabIndex = 133;
            this.splitter2.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(210, 337);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(225, 170);
            this.textBox1.TabIndex = 134;
            this.textBox1.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageReportList);
            this.tabControl1.Controls.Add(this.tabPageImg);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.tabControl1.Location = new System.Drawing.Point(444, 30);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(344, 508);
            this.tabControl1.TabIndex = 135;
            // 
            // tabPageReportList
            // 
            this.tabPageReportList.Controls.Add(this.dataGridViewReport);
            this.tabPageReportList.Location = new System.Drawing.Point(4, 21);
            this.tabPageReportList.Name = "tabPageReportList";
            this.tabPageReportList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageReportList.Size = new System.Drawing.Size(336, 483);
            this.tabPageReportList.TabIndex = 0;
            this.tabPageReportList.Text = "报告";
            this.tabPageReportList.UseVisualStyleBackColor = true;
            // 
            // tabPageImg
            // 
            this.tabPageImg.AutoScroll = true;
            this.tabPageImg.Controls.Add(this.img5);
            this.tabPageImg.Controls.Add(this.img1);
            this.tabPageImg.Controls.Add(this.img4);
            this.tabPageImg.Controls.Add(this.img3);
            this.tabPageImg.Controls.Add(this.img2);
            this.tabPageImg.Location = new System.Drawing.Point(4, 21);
            this.tabPageImg.Name = "tabPageImg";
            this.tabPageImg.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageImg.Size = new System.Drawing.Size(336, 483);
            this.tabPageImg.TabIndex = 2;
            this.tabPageImg.Text = "图形";
            this.tabPageImg.UseVisualStyleBackColor = true;
            // 
            // img5
            // 
            this.img5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img5.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图5", true));
            this.img5.Location = new System.Drawing.Point(6, 345);
            this.img5.Name = "img5";
            this.img5.Size = new System.Drawing.Size(170, 80);
            this.img5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img5.TabIndex = 55;
            this.img5.TabStop = false;
            // 
            // lIS_REPORT_IMGBindingSource
            // 
            this.lIS_REPORT_IMGBindingSource.DataMember = "LIS_REPORT_IMG";
            this.lIS_REPORT_IMGBindingSource.DataSource = this.samDataSet;
            // 
            // img1
            // 
            this.img1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img1.ContextMenuStrip = this.contextMenuStripimg1;
            this.img1.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图1", true));
            this.img1.Location = new System.Drawing.Point(6, 9);
            this.img1.Name = "img1";
            this.img1.Size = new System.Drawing.Size(170, 80);
            this.img1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img1.TabIndex = 51;
            this.img1.TabStop = false;
            this.img1.DoubleClick += new System.EventHandler(this.img1_DoubleClick);
            // 
            // contextMenuStripimg1
            // 
            this.contextMenuStripimg1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导入图片ToolStripMenuItem,
            this.导出图片ToolStripMenuItem,
            this.清空图片ToolStripMenuItem,
            this.toolStripSeparator8,
            this.图片浏览ToolStripMenuItem});
            this.contextMenuStripimg1.Name = "contextMenuStripLICENSE_PIC";
            this.contextMenuStripimg1.Size = new System.Drawing.Size(153, 120);
            this.contextMenuStripimg1.Text = "图片管理";
            // 
            // 导入图片ToolStripMenuItem
            // 
            this.导入图片ToolStripMenuItem.Name = "导入图片ToolStripMenuItem";
            this.导入图片ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.导入图片ToolStripMenuItem.Text = "导入图片";
            this.导入图片ToolStripMenuItem.Click += new System.EventHandler(this.导入图片ToolStripMenuItem_Click);
            // 
            // 导出图片ToolStripMenuItem
            // 
            this.导出图片ToolStripMenuItem.Name = "导出图片ToolStripMenuItem";
            this.导出图片ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.导出图片ToolStripMenuItem.Text = "导出图片";
            this.导出图片ToolStripMenuItem.Click += new System.EventHandler(this.导出图片ToolStripMenuItem_Click);
            // 
            // 清空图片ToolStripMenuItem
            // 
            this.清空图片ToolStripMenuItem.Name = "清空图片ToolStripMenuItem";
            this.清空图片ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.清空图片ToolStripMenuItem.Text = "清空图片";
            this.清空图片ToolStripMenuItem.Click += new System.EventHandler(this.清空图片ToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(149, 6);
            // 
            // 图片浏览ToolStripMenuItem
            // 
            this.图片浏览ToolStripMenuItem.Name = "图片浏览ToolStripMenuItem";
            this.图片浏览ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.图片浏览ToolStripMenuItem.Text = "图片浏览";
            this.图片浏览ToolStripMenuItem.Click += new System.EventHandler(this.图片浏览ToolStripMenuItem_Click);
            // 
            // img4
            // 
            this.img4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img4.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图4", true));
            this.img4.Location = new System.Drawing.Point(6, 261);
            this.img4.Name = "img4";
            this.img4.Size = new System.Drawing.Size(170, 80);
            this.img4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img4.TabIndex = 54;
            this.img4.TabStop = false;
            // 
            // img3
            // 
            this.img3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img3.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图3", true));
            this.img3.Location = new System.Drawing.Point(6, 177);
            this.img3.Name = "img3";
            this.img3.Size = new System.Drawing.Size(170, 80);
            this.img3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img3.TabIndex = 53;
            this.img3.TabStop = false;
            // 
            // img2
            // 
            this.img2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img2.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图2", true));
            this.img2.Location = new System.Drawing.Point(6, 93);
            this.img2.Name = "img2";
            this.img2.Size = new System.Drawing.Size(170, 80);
            this.img2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.img2.TabIndex = 52;
            this.img2.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(257, 135);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 136;
            this.button1.Text = "测试";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ReportEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 538);
            this.ContextMenuStrip = this.contextMenuStripReport;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridViewResult);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.bN);
            this.Name = "ReportEditForm";
            this.Text = "ReportEditForm";
            this.Load += new System.EventHandler(this.ReportEditForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportEditForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjytype_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fsample_type_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_user_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_dept_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjy_user_id)).EndInit();
            this.contextMenuStripReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceResult)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageReportList.ResumeLayout(false);
            this.tabPageImg.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).EndInit();
            this.contextMenuStripimg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        protected System.Windows.Forms.ToolStripButton toolStripButtonAddItem;
        protected System.Windows.Forms.ToolStripButton toolStripButtonDelItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        protected System.Windows.Forms.ToolStripButton toolStripButtoninstrDate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        protected System.Windows.Forms.ToolStripButton toolStripButtonU;
        protected System.Windows.Forms.ToolStripButton toolStripButtonD;
        private System.Windows.Forms.ToolStripButton toolStripButtonJCSH;
        protected System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.DataGridView dataGridViewReport;
        private samDataSet samDataSet;
        private System.Windows.Forms.BindingSource lIS_REPORTBindingSource;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripReport;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_ReportCol;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExamine;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExamineNO;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemReportDel;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Ref;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_result;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemResultValue;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemResultDel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPrint;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPrinYL;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 关闭ToolStripMenuItem;
        private System.Windows.Forms.BindingSource com_listBindingSource_fstate;
        private System.Windows.Forms.BindingSource com_listBindingSource_fjytype_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fsample_type_id;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingSource com_listBindingSource_fjy_user_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fapply_user_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fapply_dept_id;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.ComboBox fjy_instrComboBox;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker;
        private System.Windows.Forms.TextBox fsample_codeTextBox;
        private System.Windows.Forms.TextBox fsample_barcodeTextBox;
        private System.Windows.Forms.ComboBox ftype_idComboBox;
        private System.Windows.Forms.TextBox fhz_zyhTextBox;
        private System.Windows.Forms.TextBox fhz_idTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fageTextBox;
        private System.Windows.Forms.ComboBox fage_unitComboBox;
        private System.Windows.Forms.ComboBox fapply_dept_idComboBox;
        private System.Windows.Forms.Button buttonfapply_dept_id;
        private System.Windows.Forms.TextBox froom_numTextBox;
        private System.Windows.Forms.TextBox fbed_numTextBox;
        private System.Windows.Forms.TextBox fdiagnoseTextBox;
        private System.Windows.Forms.ComboBox fsample_type_idComboBox;
        private System.Windows.Forms.Button buttonSample;
        private System.Windows.Forms.ComboBox fapply_user_idComboBox;
        private System.Windows.Forms.Button buttonfapply_user_id;
        private System.Windows.Forms.DateTimePicker fapply_timeDateTimePicker;
        private System.Windows.Forms.ComboBox fsampling_user_idComboBox;
        private System.Windows.Forms.DateTimePicker fsampling_timeDateTimePicker;
        private System.Windows.Forms.ComboBox fjy_user_idComboBox;
        private System.Windows.Forms.DateTimePicker fjy_timeDateTimePicker;
        private System.Windows.Forms.ComboBox fexamine_user_idComboBox;
        private System.Windows.Forms.DateTimePicker fexamine_timeDateTimePicker;
        private System.Windows.Forms.TextBox fremarkTextBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButtonJZ;
        private System.Windows.Forms.RadioButton radioButtonCG;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemResultAdd;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fexamine_flag;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fprint_flag;
        private System.Windows.Forms.DataGridViewComboBoxColumn fstate;
        private System.Windows.Forms.DataGridViewComboBoxColumn fjytype_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fsample_type_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fapply_user_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fapply_dept_id;
        private System.Windows.Forms.DataGridViewComboBoxColumn fjy_user_id;
        private System.Windows.Forms.BindingSource bindingSourceResult;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemIntData;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageReportList;
        private System.Windows.Forms.PictureBox img5;
        private System.Windows.Forms.PictureBox img4;
        private System.Windows.Forms.PictureBox img3;
        private System.Windows.Forms.PictureBox img2;
        private System.Windows.Forms.PictureBox img1;
        private System.Windows.Forms.BindingSource lIS_REPORT_IMGBindingSource;
        private System.Windows.Forms.TabPage tabPageImg;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripimg1;
        private System.Windows.Forms.ToolStripMenuItem 导入图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 清空图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem 图片浏览ToolStripMenuItem;
        private System.Windows.Forms.ComboBox fsexComboBox;
        private System.Windows.Forms.ToolStripButton toolStripButtonSave;
        private System.Windows.Forms.Button button1;
    }
}