﻿namespace yunLis.lis.sam.jy
{
    partial class JYQueryForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label fhz_type_idLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label finstr_idLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JYQueryForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lIS_REPORTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new yunLis.lis.sam.samDataSet();
            this.com_listBindingSource_fstate = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fjytype_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fsample_type_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fapply_user_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fapply_dept_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fjy_user_id = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceResult = new System.Windows.Forms.BindingSource(this.components);
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.btn_推送化验结果 = new System.Windows.Forms.Button();
            this.fhz_idtextBox = new System.Windows.Forms.TextBox();
            this.fapply_codetextBox = new System.Windows.Forms.TextBox();
            this.fzyhtextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fsample_code样本号 = new System.Windows.Forms.TextBox();
            this.cobx病人类别 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.fjy_instrComboBox = new System.Windows.Forms.ComboBox();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrintYL = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPLDY = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonJCSH = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonExportToXls = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton上传 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButto体检 = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_badge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_ref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fresult_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxImg = new System.Windows.Forms.GroupBox();
            this.img5 = new System.Windows.Forms.PictureBox();
            this.lIS_REPORT_IMGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.img4 = new System.Windows.Forms.PictureBox();
            this.img3 = new System.Windows.Forms.PictureBox();
            this.img2 = new System.Windows.Forms.PictureBox();
            this.img1 = new System.Windows.Forms.PictureBox();
            this.contextMenuStripimg1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.导入图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导出图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清空图片ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.图片浏览ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.fjy_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_instr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_yb_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_zyh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_bed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.性别 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.年龄单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.科室 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.样本 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.检验医师 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fprint_zt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_zt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fapply_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.患者ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fsample_codeLabel = new System.Windows.Forms.Label();
            fhz_type_idLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            finstr_idLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjytype_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fsample_type_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_user_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_dept_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjy_user_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceResult)).BeginInit();
            this.groupBoxif.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            this.groupBoxImg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.img5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).BeginInit();
            this.contextMenuStripimg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(286, 42);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(47, 12);
            label4.TabIndex = 29;
            label4.Text = "病人ID:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(590, 17);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(59, 12);
            label3.TabIndex = 27;
            label3.Text = "申请单号:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(602, 42);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(47, 12);
            label2.TabIndex = 25;
            label2.Text = "住院号:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(7, 42);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(59, 12);
            fnameLabel.TabIndex = 22;
            fnameLabel.Text = "病人姓名:";
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(443, 42);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(47, 12);
            fsample_codeLabel.TabIndex = 21;
            fsample_codeLabel.Text = "样本号:";
            // 
            // fhz_type_idLabel
            // 
            fhz_type_idLabel.AutoSize = true;
            fhz_type_idLabel.Location = new System.Drawing.Point(431, 17);
            fhz_type_idLabel.Name = "fhz_type_idLabel";
            fhz_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fhz_type_idLabel.TabIndex = 19;
            fhz_type_idLabel.Text = "病人类别:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(7, 17);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // finstr_idLabel
            // 
            finstr_idLabel.AutoSize = true;
            finstr_idLabel.Location = new System.Drawing.Point(298, 17);
            finstr_idLabel.Name = "finstr_idLabel";
            finstr_idLabel.Size = new System.Drawing.Size(35, 12);
            finstr_idLabel.TabIndex = 10;
            finstr_idLabel.Text = "仪器:";
            // 
            // lIS_REPORTBindingSource
            // 
            this.lIS_REPORTBindingSource.DataMember = "LIS_REPORT";
            this.lIS_REPORTBindingSource.DataSource = this.samDataSet;
            this.lIS_REPORTBindingSource.PositionChanged += new System.EventHandler(this.lIS_REPORTBindingSource_PositionChanged);
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // com_listBindingSource_fstate
            // 
            this.com_listBindingSource_fstate.DataMember = "com_list";
            this.com_listBindingSource_fstate.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fjytype_id
            // 
            this.com_listBindingSource_fjytype_id.DataMember = "com_list";
            this.com_listBindingSource_fjytype_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fsample_type_id
            // 
            this.com_listBindingSource_fsample_type_id.DataMember = "com_list";
            this.com_listBindingSource_fsample_type_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fapply_user_id
            // 
            this.com_listBindingSource_fapply_user_id.DataMember = "com_list";
            this.com_listBindingSource_fapply_user_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fapply_dept_id
            // 
            this.com_listBindingSource_fapply_dept_id.DataMember = "com_list";
            this.com_listBindingSource_fapply_dept_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fjy_user_id
            // 
            this.com_listBindingSource_fjy_user_id.DataMember = "com_list";
            this.com_listBindingSource_fjy_user_id.DataSource = this.samDataSet;
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(this.btn_推送化验结果);
            this.groupBoxif.Controls.Add(label4);
            this.groupBoxif.Controls.Add(this.fhz_idtextBox);
            this.groupBoxif.Controls.Add(label3);
            this.groupBoxif.Controls.Add(this.fapply_codetextBox);
            this.groupBoxif.Controls.Add(label2);
            this.groupBoxif.Controls.Add(this.fzyhtextBox);
            this.groupBoxif.Controls.Add(this.fnameTextBox);
            this.groupBoxif.Controls.Add(fnameLabel);
            this.groupBoxif.Controls.Add(this.fsample_code样本号);
            this.groupBoxif.Controls.Add(fsample_codeLabel);
            this.groupBoxif.Controls.Add(this.cobx病人类别);
            this.groupBoxif.Controls.Add(fhz_type_idLabel);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker2);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker1);
            this.groupBoxif.Controls.Add(fjy_dateLabel);
            this.groupBoxif.Controls.Add(this.fjy_instrComboBox);
            this.groupBoxif.Controls.Add(finstr_idLabel);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 30);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Size = new System.Drawing.Size(859, 64);
            this.groupBoxif.TabIndex = 145;
            this.groupBoxif.TabStop = false;
            // 
            // btn_推送化验结果
            // 
            this.btn_推送化验结果.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btn_推送化验结果.Location = new System.Drawing.Point(754, 13);
            this.btn_推送化验结果.Name = "btn_推送化验结果";
            this.btn_推送化验结果.Size = new System.Drawing.Size(67, 46);
            this.btn_推送化验结果.TabIndex = 33;
            this.btn_推送化验结果.Text = "化验结果推送";
            this.btn_推送化验结果.UseVisualStyleBackColor = true;
            this.btn_推送化验结果.Click += new System.EventHandler(this.btn_推送化验结果_Click);
            // 
            // fhz_idtextBox
            // 
            this.fhz_idtextBox.Location = new System.Drawing.Point(337, 38);
            this.fhz_idtextBox.Name = "fhz_idtextBox";
            this.fhz_idtextBox.Size = new System.Drawing.Size(87, 21);
            this.fhz_idtextBox.TabIndex = 28;
            // 
            // fapply_codetextBox
            // 
            this.fapply_codetextBox.Location = new System.Drawing.Point(652, 13);
            this.fapply_codetextBox.Name = "fapply_codetextBox";
            this.fapply_codetextBox.Size = new System.Drawing.Size(87, 21);
            this.fapply_codetextBox.TabIndex = 26;
            // 
            // fzyhtextBox
            // 
            this.fzyhtextBox.Location = new System.Drawing.Point(652, 38);
            this.fzyhtextBox.Name = "fzyhtextBox";
            this.fzyhtextBox.Size = new System.Drawing.Size(87, 21);
            this.fzyhtextBox.TabIndex = 24;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.Location = new System.Drawing.Point(72, 38);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(211, 21);
            this.fnameTextBox.TabIndex = 23;
            // 
            // fsample_code样本号
            // 
            this.fsample_code样本号.ForeColor = System.Drawing.SystemColors.Desktop;
            this.fsample_code样本号.Location = new System.Drawing.Point(493, 38);
            this.fsample_code样本号.Name = "fsample_code样本号";
            this.fsample_code样本号.Size = new System.Drawing.Size(87, 21);
            this.fsample_code样本号.TabIndex = 20;
            // 
            // cobx病人类别
            // 
            this.cobx病人类别.DisplayMember = "fname";
            this.cobx病人类别.FormattingEnabled = true;
            this.cobx病人类别.Items.AddRange(new object[] {
            ""});
            this.cobx病人类别.Location = new System.Drawing.Point(493, 13);
            this.cobx病人类别.Name = "cobx病人类别";
            this.cobx病人类别.Size = new System.Drawing.Size(87, 20);
            this.cobx病人类别.TabIndex = 18;
            this.cobx病人类别.ValueMember = "fname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(186, 13);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(97, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 13;
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(72, 13);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(97, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 11;
            // 
            // fjy_instrComboBox
            // 
            this.fjy_instrComboBox.DisplayMember = "ShowName";
            this.fjy_instrComboBox.FormattingEnabled = true;
            this.fjy_instrComboBox.Items.AddRange(new object[] {
            ""});
            this.fjy_instrComboBox.Location = new System.Drawing.Point(337, 13);
            this.fjy_instrComboBox.Name = "fjy_instrComboBox";
            this.fjy_instrComboBox.Size = new System.Drawing.Size(87, 20);
            this.fjy_instrComboBox.TabIndex = 9;
            this.fjy_instrComboBox.ValueMember = "finstr_id";
            this.fjy_instrComboBox.DropDownClosed += new System.EventHandler(this.fjy_instrComboBox_DropDownClosed);
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.lIS_REPORTBindingSource;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator5,
            this.toolStripButtonQuery,
            this.toolStripButtonPrint,
            this.toolStripButtonPrintYL,
            this.toolStripButtonPLDY,
            this.toolStripButtonJCSH,
            this.toolStripSeparator6,
            this.toolStripButtonExportToXls,
            this.toolStripButtonHelp,
            this.toolStripSeparator1,
            this.toolStripButton上传,
            this.toolStripButto体检});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(859, 30);
            this.bN.TabIndex = 146;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQuery.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQuery.Size = new System.Drawing.Size(77, 27);
            this.toolStripButtonQuery.Text = "查询(F5) ";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrint.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonPrint.Size = new System.Drawing.Size(77, 27);
            this.toolStripButtonPrint.Text = "打印(F8) ";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButtonPrint_Click);
            // 
            // toolStripButtonPrintYL
            // 
            this.toolStripButtonPrintYL.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrintYL.Image")));
            this.toolStripButtonPrintYL.Name = "toolStripButtonPrintYL";
            this.toolStripButtonPrintYL.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrintYL.Size = new System.Drawing.Size(101, 27);
            this.toolStripButtonPrintYL.Text = "打印预览(F9) ";
            this.toolStripButtonPrintYL.Click += new System.EventHandler(this.toolStripButtonPrintYL_Click);
            // 
            // toolStripButtonPLDY
            // 
            this.toolStripButtonPLDY.Image = global::yunLis.Properties.Resources.button_print2;
            this.toolStripButtonPLDY.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPLDY.Name = "toolStripButtonPLDY";
            this.toolStripButtonPLDY.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPLDY.Size = new System.Drawing.Size(76, 27);
            this.toolStripButtonPLDY.Text = "批量打印";
            this.toolStripButtonPLDY.Click += new System.EventHandler(this.toolStripButtonPLDY_Click);
            // 
            // toolStripButtonJCSH
            // 
            this.toolStripButtonJCSH.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonJCSH.Image")));
            this.toolStripButtonJCSH.Name = "toolStripButtonJCSH";
            this.toolStripButtonJCSH.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonJCSH.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonJCSH.Size = new System.Drawing.Size(104, 27);
            this.toolStripButtonJCSH.Text = "解除审核(F10)";
            this.toolStripButtonJCSH.Visible = false;
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonExportToXls
            // 
            this.toolStripButtonExportToXls.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonExportToXls.Image")));
            this.toolStripButtonExportToXls.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExportToXls.Name = "toolStripButtonExportToXls";
            this.toolStripButtonExportToXls.Size = new System.Drawing.Size(81, 27);
            this.toolStripButtonExportToXls.Text = "导出Excel";
            this.toolStripButtonExportToXls.Click += new System.EventHandler(this.toolStripButtonExportToXls_Click);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(81, 27);
            this.toolStripButtonHelp.Text = "帮助(F1)  ";
            this.toolStripButtonHelp.Visible = false;
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButton上传
            // 
            this.toolStripButton上传.Image = global::yunLis.Properties.Resources.button_tj;
            this.toolStripButton上传.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton上传.Name = "toolStripButton上传";
            this.toolStripButton上传.Size = new System.Drawing.Size(100, 27);
            this.toolStripButton上传.Text = "上传健康档案";
            this.toolStripButton上传.Visible = false;
            this.toolStripButton上传.Click += new System.EventHandler(this.toolStripButton上传_Click);
            // 
            // toolStripButto体检
            // 
            this.toolStripButto体检.Image = global::yunLis.Properties.Resources.wwNavigatorQuery;
            this.toolStripButto体检.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButto体检.Name = "toolStripButto体检";
            this.toolStripButto体检.Size = new System.Drawing.Size(76, 27);
            this.toolStripButto体检.Text = "体检信息";
            this.toolStripButto体检.Visible = false;
            this.toolStripButto体检.Click += new System.EventHandler(this.toolStripButto体检_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridViewResult);
            this.panel1.Controls.Add(this.groupBoxImg);
            this.panel1.Controls.Add(this.splitter1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(453, 94);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(406, 440);
            this.panel1.TabIndex = 147;
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code,
            this.fitem_name,
            this.fvalue,
            this.fitem_badge,
            this.fitem_ref,
            this.fod,
            this.fcutoff,
            this.fitem_unit,
            this.forder_by,
            this.fitem_id,
            this.fresult_id});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResult.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewResult.Location = new System.Drawing.Point(3, 0);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidth = 30;
            this.dataGridViewResult.RowTemplate.Height = 23;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewResult.ShowCellToolTips = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.Size = new System.Drawing.Size(403, 344);
            this.dataGridViewResult.TabIndex = 135;
            this.dataGridViewResult.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewResult_DataError_1);
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "编码";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Width = 60;
            // 
            // fitem_name
            // 
            this.fitem_name.DataPropertyName = "fitem_name";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_name.DefaultCellStyle = dataGridViewCellStyle2;
            this.fitem_name.HeaderText = "检验项目";
            this.fitem_name.Name = "fitem_name";
            this.fitem_name.ReadOnly = true;
            // 
            // fvalue
            // 
            this.fvalue.DataPropertyName = "fvalue";
            this.fvalue.HeaderText = "结果";
            this.fvalue.Name = "fvalue";
            this.fvalue.Width = 65;
            // 
            // fitem_badge
            // 
            this.fitem_badge.DataPropertyName = "fitem_badge";
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_badge.DefaultCellStyle = dataGridViewCellStyle3;
            this.fitem_badge.HeaderText = ".";
            this.fitem_badge.Name = "fitem_badge";
            this.fitem_badge.ReadOnly = true;
            this.fitem_badge.Width = 25;
            // 
            // fitem_ref
            // 
            this.fitem_ref.DataPropertyName = "fitem_ref";
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_ref.DefaultCellStyle = dataGridViewCellStyle4;
            this.fitem_ref.HeaderText = "参考值";
            this.fitem_ref.Name = "fitem_ref";
            this.fitem_ref.ReadOnly = true;
            this.fitem_ref.Width = 80;
            // 
            // fod
            // 
            this.fod.DataPropertyName = "fod";
            this.fod.HeaderText = "OD值";
            this.fod.Name = "fod";
            this.fod.Visible = false;
            this.fod.Width = 65;
            // 
            // fcutoff
            // 
            this.fcutoff.DataPropertyName = "fcutoff";
            this.fcutoff.HeaderText = "Cutoff";
            this.fcutoff.Name = "fcutoff";
            this.fcutoff.Visible = false;
            this.fcutoff.Width = 70;
            // 
            // fitem_unit
            // 
            this.fitem_unit.DataPropertyName = "fitem_unit";
            this.fitem_unit.HeaderText = "单位";
            this.fitem_unit.Name = "fitem_unit";
            this.fitem_unit.ReadOnly = true;
            this.fitem_unit.Width = 65;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "序号";
            this.forder_by.Name = "forder_by";
            this.forder_by.Visible = false;
            this.forder_by.Width = 60;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "项目id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.ReadOnly = true;
            this.fitem_id.Visible = false;
            // 
            // fresult_id
            // 
            this.fresult_id.DataPropertyName = "fresult_id";
            this.fresult_id.HeaderText = "fresult_id";
            this.fresult_id.Name = "fresult_id";
            this.fresult_id.ReadOnly = true;
            this.fresult_id.Visible = false;
            // 
            // groupBoxImg
            // 
            this.groupBoxImg.Controls.Add(this.img5);
            this.groupBoxImg.Controls.Add(this.img4);
            this.groupBoxImg.Controls.Add(this.img3);
            this.groupBoxImg.Controls.Add(this.img2);
            this.groupBoxImg.Controls.Add(this.img1);
            this.groupBoxImg.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxImg.Location = new System.Drawing.Point(3, 344);
            this.groupBoxImg.Name = "groupBoxImg";
            this.groupBoxImg.Size = new System.Drawing.Size(403, 96);
            this.groupBoxImg.TabIndex = 134;
            this.groupBoxImg.TabStop = false;
            this.groupBoxImg.Text = "图";
            this.groupBoxImg.Visible = false;
            // 
            // img5
            // 
            this.img5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img5.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图5", true));
            this.img5.Location = new System.Drawing.Point(312, 19);
            this.img5.Name = "img5";
            this.img5.Size = new System.Drawing.Size(70, 70);
            this.img5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img5.TabIndex = 50;
            this.img5.TabStop = false;
            this.img5.Visible = false;
            // 
            // lIS_REPORT_IMGBindingSource
            // 
            this.lIS_REPORT_IMGBindingSource.DataMember = "LIS_REPORT_IMG";
            this.lIS_REPORT_IMGBindingSource.DataSource = this.samDataSet;
            // 
            // img4
            // 
            this.img4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img4.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图4", true));
            this.img4.Location = new System.Drawing.Point(237, 19);
            this.img4.Name = "img4";
            this.img4.Size = new System.Drawing.Size(70, 70);
            this.img4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img4.TabIndex = 49;
            this.img4.TabStop = false;
            this.img4.Visible = false;
            // 
            // img3
            // 
            this.img3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img3.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图3", true));
            this.img3.Location = new System.Drawing.Point(162, 19);
            this.img3.Name = "img3";
            this.img3.Size = new System.Drawing.Size(70, 70);
            this.img3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img3.TabIndex = 48;
            this.img3.TabStop = false;
            this.img3.Visible = false;
            // 
            // img2
            // 
            this.img2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img2.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图2", true));
            this.img2.Location = new System.Drawing.Point(87, 19);
            this.img2.Name = "img2";
            this.img2.Size = new System.Drawing.Size(70, 70);
            this.img2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img2.TabIndex = 47;
            this.img2.TabStop = false;
            this.img2.Visible = false;
            // 
            // img1
            // 
            this.img1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.img1.ContextMenuStrip = this.contextMenuStripimg1;
            this.img1.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.lIS_REPORT_IMGBindingSource, "结果图1", true));
            this.img1.Location = new System.Drawing.Point(12, 19);
            this.img1.Name = "img1";
            this.img1.Size = new System.Drawing.Size(70, 70);
            this.img1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img1.TabIndex = 46;
            this.img1.TabStop = false;
            this.img1.Visible = false;
            this.img1.DoubleClick += new System.EventHandler(this.img1_DoubleClick);
            // 
            // contextMenuStripimg1
            // 
            this.contextMenuStripimg1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导入图片ToolStripMenuItem,
            this.导出图片ToolStripMenuItem,
            this.清空图片ToolStripMenuItem,
            this.toolStripSeparator8,
            this.图片浏览ToolStripMenuItem});
            this.contextMenuStripimg1.Name = "contextMenuStripLICENSE_PIC";
            this.contextMenuStripimg1.Size = new System.Drawing.Size(125, 98);
            this.contextMenuStripimg1.Text = "图片管理";
            // 
            // 导入图片ToolStripMenuItem
            // 
            this.导入图片ToolStripMenuItem.Name = "导入图片ToolStripMenuItem";
            this.导入图片ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.导入图片ToolStripMenuItem.Text = "导入图片";
            this.导入图片ToolStripMenuItem.Click += new System.EventHandler(this.导入图片ToolStripMenuItem_Click);
            // 
            // 导出图片ToolStripMenuItem
            // 
            this.导出图片ToolStripMenuItem.Name = "导出图片ToolStripMenuItem";
            this.导出图片ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.导出图片ToolStripMenuItem.Text = "导出图片";
            this.导出图片ToolStripMenuItem.Click += new System.EventHandler(this.导出图片ToolStripMenuItem_Click);
            // 
            // 清空图片ToolStripMenuItem
            // 
            this.清空图片ToolStripMenuItem.Name = "清空图片ToolStripMenuItem";
            this.清空图片ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.清空图片ToolStripMenuItem.Text = "清空图片";
            this.清空图片ToolStripMenuItem.Click += new System.EventHandler(this.清空图片ToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(121, 6);
            // 
            // 图片浏览ToolStripMenuItem
            // 
            this.图片浏览ToolStripMenuItem.Name = "图片浏览ToolStripMenuItem";
            this.图片浏览ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.图片浏览ToolStripMenuItem.Text = "图片浏览";
            this.图片浏览ToolStripMenuItem.Click += new System.EventHandler(this.图片浏览ToolStripMenuItem_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 440);
            this.splitter1.TabIndex = 132;
            this.splitter1.TabStop = false;
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToDeleteRows = false;
            this.dataGridViewReport.AllowUserToResizeRows = false;
            this.dataGridViewReport.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fjy_date,
            this.fjy_instr,
            this.fjy_yb_code,
            this.fhz_name,
            this.fhz_zyh,
            this.fhz_bed,
            this.性别,
            this.fhz_age,
            this.年龄单位,
            this.科室,
            this.样本,
            this.检验医师,
            this.fprint_zt,
            this.fjy_zt,
            this.类型,
            this.fapply_id,
            this.fjy_id,
            this.fremark,
            this.患者ID});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReport.Location = new System.Drawing.Point(0, 94);
            this.dataGridViewReport.MultiSelect = false;
            this.dataGridViewReport.Name = "dataGridViewReport";
            this.dataGridViewReport.ReadOnly = true;
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.RowHeadersWidth = 30;
            this.dataGridViewReport.RowTemplate.Height = 23;
            this.dataGridViewReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReport.ShowCellToolTips = false;
            this.dataGridViewReport.ShowEditingIcon = false;
            this.dataGridViewReport.Size = new System.Drawing.Size(453, 440);
            this.dataGridViewReport.TabIndex = 148;
            this.dataGridViewReport.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridViewReport_DataBindingComplete);
            this.dataGridViewReport.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewReport_DataError_1);
            this.dataGridViewReport.Click += new System.EventHandler(this.dataGridViewReport_Click);
            // 
            // fjy_date
            // 
            this.fjy_date.DataPropertyName = "fjy_date";
            this.fjy_date.HeaderText = "日期";
            this.fjy_date.Name = "fjy_date";
            this.fjy_date.ReadOnly = true;
            this.fjy_date.Width = 75;
            // 
            // fjy_instr
            // 
            this.fjy_instr.DataPropertyName = "fjy_instr";
            this.fjy_instr.HeaderText = "仪器";
            this.fjy_instr.Name = "fjy_instr";
            this.fjy_instr.ReadOnly = true;
            this.fjy_instr.Width = 65;
            // 
            // fjy_yb_code
            // 
            this.fjy_yb_code.DataPropertyName = "fjy_yb_code";
            this.fjy_yb_code.HeaderText = "样本号";
            this.fjy_yb_code.Name = "fjy_yb_code";
            this.fjy_yb_code.ReadOnly = true;
            this.fjy_yb_code.Width = 64;
            // 
            // fhz_name
            // 
            this.fhz_name.DataPropertyName = "fhz_name";
            this.fhz_name.HeaderText = "姓名";
            this.fhz_name.Name = "fhz_name";
            this.fhz_name.ReadOnly = true;
            this.fhz_name.Width = 55;
            // 
            // fhz_zyh
            // 
            this.fhz_zyh.DataPropertyName = "fhz_zyh";
            this.fhz_zyh.HeaderText = "住院号";
            this.fhz_zyh.Name = "fhz_zyh";
            this.fhz_zyh.ReadOnly = true;
            this.fhz_zyh.Width = 65;
            // 
            // fhz_bed
            // 
            this.fhz_bed.DataPropertyName = "fhz_bed";
            this.fhz_bed.HeaderText = "床号";
            this.fhz_bed.Name = "fhz_bed";
            this.fhz_bed.ReadOnly = true;
            this.fhz_bed.Width = 55;
            // 
            // 性别
            // 
            this.性别.DataPropertyName = "性别";
            this.性别.HeaderText = "性别";
            this.性别.Name = "性别";
            this.性别.ReadOnly = true;
            this.性别.Width = 55;
            // 
            // fhz_age
            // 
            this.fhz_age.DataPropertyName = "fhz_age";
            this.fhz_age.HeaderText = "年";
            this.fhz_age.Name = "fhz_age";
            this.fhz_age.ReadOnly = true;
            this.fhz_age.Width = 30;
            // 
            // 年龄单位
            // 
            this.年龄单位.DataPropertyName = "年龄单位";
            this.年龄单位.HeaderText = "龄";
            this.年龄单位.Name = "年龄单位";
            this.年龄单位.ReadOnly = true;
            this.年龄单位.Width = 30;
            // 
            // 科室
            // 
            this.科室.DataPropertyName = "科室";
            this.科室.HeaderText = "科室";
            this.科室.Name = "科室";
            this.科室.ReadOnly = true;
            this.科室.Width = 55;
            // 
            // 样本
            // 
            this.样本.DataPropertyName = "样本";
            this.样本.HeaderText = "样本";
            this.样本.Name = "样本";
            this.样本.ReadOnly = true;
            this.样本.Width = 55;
            // 
            // 检验医师
            // 
            this.检验医师.DataPropertyName = "检验医师";
            this.检验医师.HeaderText = "检验";
            this.检验医师.Name = "检验医师";
            this.检验医师.ReadOnly = true;
            this.检验医师.Width = 55;
            // 
            // fprint_zt
            // 
            this.fprint_zt.DataPropertyName = "fprint_zt";
            this.fprint_zt.HeaderText = "打印";
            this.fprint_zt.Name = "fprint_zt";
            this.fprint_zt.ReadOnly = true;
            this.fprint_zt.Width = 55;
            // 
            // fjy_zt
            // 
            this.fjy_zt.DataPropertyName = "fjy_zt";
            this.fjy_zt.HeaderText = "审核";
            this.fjy_zt.Name = "fjy_zt";
            this.fjy_zt.ReadOnly = true;
            this.fjy_zt.Width = 55;
            // 
            // 类型
            // 
            this.类型.DataPropertyName = "类型";
            this.类型.HeaderText = "类型";
            this.类型.Name = "类型";
            this.类型.ReadOnly = true;
            this.类型.Width = 55;
            // 
            // fapply_id
            // 
            this.fapply_id.DataPropertyName = "fapply_id";
            this.fapply_id.HeaderText = "申请号";
            this.fapply_id.Name = "fapply_id";
            this.fapply_id.ReadOnly = true;
            this.fapply_id.Width = 65;
            // 
            // fjy_id
            // 
            this.fjy_id.DataPropertyName = "fjy_id";
            this.fjy_id.HeaderText = "fjy_id";
            this.fjy_id.Name = "fjy_id";
            this.fjy_id.ReadOnly = true;
            this.fjy_id.Visible = false;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            this.fremark.ReadOnly = true;
            // 
            // 患者ID
            // 
            this.患者ID.DataPropertyName = "fhz_id";
            this.患者ID.HeaderText = "患者ID";
            this.患者ID.Name = "患者ID";
            this.患者ID.ReadOnly = true;
            this.患者ID.Visible = false;
            // 
            // JYQueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 534);
            this.Controls.Add(this.dataGridViewReport);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBoxif);
            this.Controls.Add(this.bN);
            this.Name = "JYQueryForm";
            this.Text = "JYQueryForm";
            this.Load += new System.EventHandler(this.ReportQueryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjytype_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fsample_type_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_user_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_dept_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjy_user_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceResult)).EndInit();
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            this.groupBoxImg.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.img5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).EndInit();
            this.contextMenuStripimg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private samDataSet samDataSet;
        private System.Windows.Forms.BindingSource lIS_REPORTBindingSource;
        private System.Windows.Forms.BindingSource com_listBindingSource_fstate;
        private System.Windows.Forms.BindingSource com_listBindingSource_fjytype_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fsample_type_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fjy_user_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fapply_user_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fapply_dept_id;
        private System.Windows.Forms.BindingSource bindingSourceResult;
        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.TextBox fhz_idtextBox;
        private System.Windows.Forms.TextBox fapply_codetextBox;
        private System.Windows.Forms.TextBox fzyhtextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fsample_code样本号;
        private System.Windows.Forms.ComboBox cobx病人类别;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox fjy_instrComboBox;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrintYL;
        private System.Windows.Forms.ToolStripButton toolStripButtonJCSH;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBoxImg;
        private System.Windows.Forms.PictureBox img5;
        private System.Windows.Forms.PictureBox img4;
        private System.Windows.Forms.PictureBox img3;
        private System.Windows.Forms.PictureBox img2;
        private System.Windows.Forms.PictureBox img1;
        private System.Windows.Forms.BindingSource lIS_REPORT_IMGBindingSource;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripimg1;
        private System.Windows.Forms.ToolStripMenuItem 导入图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 导出图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 清空图片ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem 图片浏览ToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridViewReport;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_badge;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_ref;
        private System.Windows.Forms.DataGridViewTextBoxColumn fod;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcutoff;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fresult_id;
        private System.Windows.Forms.ToolStripButton toolStripButtonPLDY;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton上传;
        private System.Windows.Forms.ToolStripButton toolStripButto体检;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_instr;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_yb_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_zyh;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_bed;
        private System.Windows.Forms.DataGridViewTextBoxColumn 性别;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_age;
        private System.Windows.Forms.DataGridViewTextBoxColumn 年龄单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 科室;
        private System.Windows.Forms.DataGridViewTextBoxColumn 样本;
        private System.Windows.Forms.DataGridViewTextBoxColumn 检验医师;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprint_zt;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_zt;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn fapply_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn 患者ID;
        private System.Windows.Forms.ToolStripButton toolStripButtonExportToXls;
        private System.Windows.Forms.Button btn_推送化验结果;
    }
}