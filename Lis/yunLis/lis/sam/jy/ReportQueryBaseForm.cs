﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using ww.wwf.com;
using ww.wwf.wwfbll;
using ww.lis.lisbll.sam;
using ww.form.lis.com;
namespace ww.form.lis.sam.jy
{
    public partial class ReportQueryBaseForm : ww.form.wwf.SysBaseForm
    {
        /// <summary>
        /// 报告ID 用户自定义表
        /// </summary>
        protected string strTaleIDreport = "4";
        /// <summary> 
        /// 结果列表ID 用户自定义表
        /// </summary>
        protected string strTaleIDresult = "5";
        /// <summary>
        /// 报告格式 用户自定义表
        /// </summary>
        private UserdefinedFieldUpdateForm usersetRepot = new UserdefinedFieldUpdateForm("4");
        /// <summary>
        /// 结果格式 用户自定义表
        /// </summary>
        private UserdefinedFieldUpdateForm usersetResult = new UserdefinedFieldUpdateForm("5");
        /// <summary>
        /// 逻辑_部门
        /// </summary>
        protected DeptBll bllDept = new DeptBll();
        /// <summary>
        /// 逻辑_人员
        /// </summary>
        protected PersonBLL bllPerson = new PersonBLL();
        /// <summary>
        /// 报告检验ID
        /// </summary>
        private string comjyfjy_id = "";
        /// <summary>
        /// 仪器ID
        /// </summary>
        private string comfinstr_id = "";
        /// <summary>
        /// 逻辑_公共类型
        /// </summary>
        protected TypeBLL bllType = new TypeBLL();//公共类型逻辑  
        /// <summary>
        /// 数据表_报告
        /// </summary>
        //  DataTable dtJY = new DataTable();
        /// <summary>
        /// 数据表_结果
        /// </summary>
        protected DataTable dtResult = new DataTable();
        /// <summary>
        /// 数据行_当前报告行
        /// </summary>
        protected DataRowView rowCurrent = null;
        /// <summary>
        /// 逻辑_检验
        /// </summary>
        protected JYBLL bllJY = new JYBLL();
        /// <summary>
        /// 逻辑_仪器
        /// </summary>
        protected InstrBLL bllInstr = new InstrBLL();

        /// <summary>
        /// 病人类别
        /// </summary>
        protected DataTable dtsam_type_fhz_type_id = new DataTable();
        /// <summary>
        /// 数据表_仪器
        /// </summary>
        protected DataTable dtInstr = new DataTable();

        /// <summary>
        /// 结果标识
        /// </summary>
        DataTable dtJGBJ = null;


        public ReportQueryBaseForm()
        {

            InitializeComponent();
            WWComTypeList();
            this.dataGridViewReport.DataSource = this.sam_jyBindingSource;
            this.dataGridViewResult.DataSource = this.sam_jy_resultBindingSource;
        }

        private void ReportQueryBaseForm_Load(object sender, EventArgs e)
        {
            try
            {
                sam_jyBindingSource.DataSource = this.bllJY.BllJYdt("(fjy_id <> fjy_id)");
                sam_jy_resultBindingSource.DataSource = this.bllJY.BllResultDT("");
                this.usersetRepot.DataGridViewSetStyle(this.dataGridViewReport);
                this.usersetResult.DataGridViewSetStyle(this.dataGridViewResult);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        #region  自定义方法
        DataTable dtfsex = null;
        DataTable dtfhz_type_id = null;
        DataTable dtfage_unit = null;
        DataTable dtfsample_type_id = null;
        DataTable dtfjytype_id = null;
        DataTable dtfperson_id = null;
        DataTable dtfdept_id = null;
        /// <summary> 
        /// 取得初始 List
        /// </summary>
        private void WWComTypeList()
        {
            try
            {

                this.dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID);//仪器列表
                this.finstr_idComboBox.DataSource = dtInstr;
                this.com_listBindingSource_finstr_id.DataSource = this.bllInstr.BllInstrDT(2);//已经启用的所有仪器

                dtfhz_type_id = this.bllType.BllComTypeDT("病人类别", 1);
                this.com_listBindingSource_fhz_type_id.DataSource = dtfhz_type_id;

                dtfage_unit = this.bllType.BllComTypeDT("年龄单位", 1);
                this.com_listBindingSource_fage_unit.DataSource = dtfage_unit;

                dtfsex = this.bllType.BllComTypeDT("性别", 1);
                this.com_listBindingSource_fsex.DataSource = dtfsex;

                this.com_listBindingSource_fstate.DataSource = this.bllType.BllComTypeDT("检验状态", 1);

                dtfsample_type_id = this.bllType.BllSamTypeDT(2);//样本类型     
                this.com_listBindingSource_fsample_type_id.DataSource = dtfsample_type_id;

                dtfjytype_id = this.bllType.BllCheckTypeDT(2);//检验类别;

                this.com_listBindingSource_fjytype_id.DataSource = dtfjytype_id;

                dtfperson_id = this.bllPerson.BllDTAllPerson("1"); //人员           
                this.com_listBindingSource_fperson_id.DataSource = dtfperson_id;

                dtfdept_id = this.bllDept.BllDeptDT();//部门
                this.com_listBindingSource_fdept_id.DataSource = dtfdept_id;
                this.fhz_type_idComboBox.DataSource = com_listBindingSource_fhz_type_id;

                dtJGBJ = this.bllType.BllComTypeDT("结果标记", 1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        ///取得当前报表列表 设置SQL条件 
        /// </summary>
        private void WWReportList()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                string fhz_id = "";//患者ID
                string fapply_code = "";//申请单号
                string fzyh = "";//住院号
                string fsample_code = "";//样本号
                string fname = "";//姓名                
                string finstr_id = "";//仪器ID
                string fhz_type_id = "";//患者类别
                string fjy_date1 = "";//检验日期1
                string fjy_date2 = "";//检验日期2
                string fjy_dateWhere = "";
                fsample_code = this.fsample_codeTextBox.Text;
                fname = this.fnameTextBox.Text;
                fzyh = fzyhtextBox.Text;
                fhz_id = this.fhz_idtextBox.Text;
                fapply_code = this.fapply_codetextBox.Text;

                try
                {
                    finstr_id = finstr_idComboBox.SelectedValue.ToString();
                }
                catch { }
                try
                {
                    fhz_type_id = fhz_type_idComboBox.SelectedValue.ToString();
                }
                catch { }
                String strYear = fjy_dateDateTimePicker1.Value.Year.ToString();
                String strMonth = fjy_dateDateTimePicker1.Value.Month.ToString();
                string strDay = fjy_dateDateTimePicker1.Value.Day.ToString();
                if (strMonth.Length == 1)
                    strMonth = "0" + strMonth;
                if (strDay.Length == 1)
                    strDay = "0" + strDay;
                fjy_date1 = strYear + "-" + strMonth + "-" + strDay;
                String strYear2 = fjy_dateDateTimePicker2.Value.Year.ToString();
                String strMonth2 = fjy_dateDateTimePicker2.Value.Month.ToString();
                string strDay2 = fjy_dateDateTimePicker2.Value.Day.ToString();
                if (strMonth2.Length == 1)
                    strMonth2 = "0" + strMonth2;
                if (strDay2.Length == 1)
                    strDay2 = "0" + strDay2;
                fjy_date2 = strYear2 + "-" + strMonth2 + "-" + strDay2;

                if (finstr_idComboBox.Text == "" || finstr_idComboBox.Text == null)
                    finstr_id = " (finstr_id is not null) ";
                else
                    finstr_id = " (finstr_id='" + finstr_id + "') ";

                if (this.fhz_type_idComboBox.Text == "" || this.fhz_type_idComboBox.Text == null)
                    fhz_type_id = " and (fhz_type_id is not null)";
                else
                    fhz_type_id = " and (fhz_type_id='" + fhz_type_id + "') ";

                fjy_dateWhere = " and (fjy_date>='" + fjy_date1 + "' and fjy_date<='" + fjy_date2 + "') ";

                if (fsample_code == "" || fsample_code == null)
                    fsample_code = " and (fsample_code is not null) ";
                else
                    fsample_code = " and (fsample_code='" + fsample_code + "')";
                if (fname == "" || fname == null)
                    fname = " and (fname is not null) ";
                else
                    fname = " and (fname='" + fname + "')";

                if (fzyh == "" || fzyh == null)
                    fzyh = " and (fzyh is not null) ";
                else
                    fzyh = " and (fzyh='" + fzyh + "')";


                if (fhz_id == "" || fhz_id == null)
                    fhz_id = " and (fhz_id is not null) ";
                else
                    fhz_id = " and (fhz_id='" + fhz_id + "')";

                if (fapply_code == "" || fapply_code == null)
                    fapply_code = " and (fapply_code is not null) ";
                else
                    fapply_code = " and (fapply_code='" + fapply_code + "')";

                string strWhere = finstr_id + fhz_type_id + fjy_dateWhere + fsample_code + fname + fzyh + fhz_id + fapply_code;
                  //this.textBox1.Text = strWhere;
                sam_jyBindingSource.DataSource = this.bllJY.BllJYdt(strWhere);
                WWReportGridViesStyle();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        /// 列表风格设置
        /// </summary>
        private void WWReportGridViesStyle()
        {
            try
            {
                int intfstate = 0;
                for (int i = 0; i < this.dataGridViewReport.Rows.Count; i++)
                {
                    intfstate = Convert.ToInt32(this.dataGridViewReport.Rows[i].Cells["fstate"].Value.ToString());
                    switch (intfstate)
                    {
                        //case 0:
                        //this.dataGridViewReport.Rows[i].Cells["fstate"].Style.ForeColor = Color.Red;
                        // break;
                        case 1:
                            this.dataGridViewReport.Rows[i].DefaultCellStyle.ForeColor = Color.Blue; //System.Drawing.SystemColors.Desktop;
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {            
            if (keyData == (Keys.F5))
            {
                toolStripButtonQuery.PerformClick();
                return true;
            }
            if (keyData == (Keys.F8))
            {
                toolStripButtonPrint.PerformClick();
                return true;
            }
            if (keyData == (Keys.F9))
            {
                toolStripButtonPrintYL.PerformClick();
                return true;
            }
            if (keyData == (Keys.F10))
            {
                toolStripButtonJCSH.PerformClick();
                return true;
            }
                 
            
            return base.ProcessCmdKey(ref   msg, keyData);
        }

        /// <summary>
        /// 据报告ID取得结果列表
        /// </summary>
        /// <param name="strfjy_id"></param>
        private void BllGetResult(string strfjy_id)
        {
            try
            {
                this.dtResult = this.bllJY.BllResultDT(strfjy_id);
                this.sam_jy_resultBindingSource.DataSource = this.dtResult;
                WWGridViewResultValueSet();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        ///  结果值设置
        /// </summary>
        private void WWGridViewResultValueSet()
        {
            try
            {
                if (this.dataGridViewResult.Rows.Count > 0)
                {
                    DataTable dtItem = null;
                    string strItem_id = "";
                    string strValue = "";//值；
                    string strFref = "";//参考值；
                    for (int intGrid = 0; intGrid < this.dataGridViewResult.Rows.Count; intGrid++)
                    {
                        if (this.dataGridViewResult.Rows[intGrid].Cells["fitem_id"].Value != null)
                            strItem_id = this.dataGridViewResult.Rows[intGrid].Cells["fitem_id"].Value.ToString();
                        else
                            strItem_id = "";

                        if (this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Value != null)
                            strValue = this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Value.ToString();
                        else
                            strValue = "";

                        if (this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Value != null)
                            strFref = this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Value.ToString();
                        else
                            strFref = "";

                        if (strItem_id != "")
                        {
                            dtItem = this.bllJY.BllItemDT(strItem_id);
                            this.dataGridViewResult.Rows[intGrid].Cells["fitem_code"].Value = dtItem.Rows[0]["fitem_code"];
                            this.dataGridViewResult.Rows[intGrid].Cells["fitem_name"].Value = dtItem.Rows[0]["fname"];
                            this.dataGridViewResult.Rows[intGrid].Cells["funitname"].Value = dtItem.Rows[0]["funitname"];
                            strFref = this.bllJY.BllItemfref(dtItem.Rows[0], rowCurrent);
                            this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Value = strFref;
                            int intBJ = this.bllJY.BllItemValueBJ(strValue, strFref);
                            string strBJ = dtJGBJ.Rows[0]["fname"].ToString();
                            if (intBJ == 1)
                            {
                                strBJ = dtJGBJ.Rows[1]["fname"].ToString();
                            }
                            if (intBJ == 2)
                            {
                                strBJ = dtJGBJ.Rows[2]["fname"].ToString();
                            }
                            this.dataGridViewResult.Rows[intGrid].Cells["fvalue_bj"].Value = strBJ;

                            switch (intBJ)
                            {
                                case 1:
                                    this.dataGridViewResult.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue; //System.Drawing.
                                    //this.dataGridViewResult.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Blue; //System.Drawing.
                                   // this.dataGridViewResult.Rows[intGrid].Cells["fvalue_bj"].Style.ForeColor = Color.Red; //System.Drawing.
                                   // this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Style.ForeColor = Color.Red; //System.Drawing.
                                    break;
                                case 2:
                                    this.dataGridViewResult.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red; //System.Drawing.
                                    //this.dataGridViewResult.Rows[intGrid].DefaultCellStyle.ForeColor = Color.Red; //System.Drawing.SystemColors.Desktop; 
                                   // this.dataGridViewResult.Rows[intGrid].Cells["fvalue_bj"].Style.ForeColor = Color.Red; //System.Drawing.
                                    //this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Style.ForeColor = Color.Red; //System.Drawing.
                                    break;
                                default:
                                    break;
                            }



                            // this.dataGridViewResult.Rows[intGrid].Cells["fitem_ref"].Value = thisBllItemfref(dtItem.Rows[0], rowCurrent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType"></param>
        private void WWPrint(int printType)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.sam_jyBindingSource.EndEdit();
                this.sam_jy_resultBindingSource.EndEdit();

                string strfsex = "";//性别 
                string strfage_unit = "";//年龄单位
                string strfapply_dept_id = "";//送检科别
                string strfsample_type = "";//样本类别
                string strfapply_user_id = "";//送检医生
                string strfcheck_user_id = "";//检验者
                string strfexamine_user_id = "";//审核者

                if (rowCurrent != null)
                {
                    /*
                    DataTable dtfsex = null;
                    DataTable dtfhz_type_id = null;
                    DataTable dtfage_unit = null;
                    DataTable dtfsample_type_id = null;
                    DataTable dtfjytype_id = null;
                    DataTable dtfperson_id = null;
                    DataTable dtfdept_id = null;
                     * */
                    //fcode

                    DataRow[] dataRowfsex = dtfsex.Select("fcode='" + rowCurrent["fsex"].ToString() + "'");
                    strfsex = dataRowfsex[0]["fname"].ToString();

                    DataRow[] dataRowfage_unit = dtfage_unit.Select("fcode='" + rowCurrent["fage_unit"].ToString() + "'");
                    strfage_unit = dataRowfage_unit[0]["fname"].ToString();

                    DataRow[] dataRowfapply_dept_id = dtfdept_id.Select("fdept_id='" + rowCurrent["fapply_dept_id"].ToString() + "'");
                    strfapply_dept_id = dataRowfapply_dept_id[0]["fname"].ToString();

                    DataRow[] dataRowfsample_type = dtfsample_type_id.Select("fsample_type_id='" + rowCurrent["fsample_type_id"].ToString() + "'");
                    strfsample_type = dataRowfsample_type[0]["fname"].ToString();

                    DataRow[] datafapply_user_id = dtfperson_id.Select("fperson_id='" + rowCurrent["fapply_user_id"].ToString() + "'");
                    strfapply_user_id = datafapply_user_id[0]["fname"].ToString();

                    DataRow[] datafcheck_user_id = dtfperson_id.Select("fperson_id='" + rowCurrent["fcheck_user_id"].ToString() + "'");
                    strfcheck_user_id = datafcheck_user_id[0]["fname"].ToString();

                    DataRow[] datafexamine_user_id = dtfperson_id.Select("fperson_id='" + rowCurrent["fexamine_user_id"].ToString() + "'");
                    strfexamine_user_id = datafexamine_user_id[0]["fname"].ToString();

                }
                ww.form.lis.sam.Report.PrintCom pr = new ww.form.lis.sam.Report.PrintCom();
               // pr.BllPrintViewer(printType, this.rowCurrent, this.dataGridViewResult, comfinstr_id, strfsex, strfage_unit, strfapply_dept_id, strfsample_type, strfapply_user_id, strfcheck_user_id, strfexamine_user_id);

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        /// <summary>
        /// 报告解出审核
        /// </summary>
        private void WWReportExamineNO()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (this.dataGridViewReport.Rows.Count > 0)
                {
                    if (rowCurrent != null)
                    {
                        if (rowCurrent["fexamine_flag"].ToString() == "1")
                        {
                            if (WWMessage.MessageDialogResult("确定解出选中报告的审核？"))
                            {
                                if (this.bllJY.BllJYExamine(this.comjyfjy_id, 0) > 0)
                                    WWReportList();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        #endregion

        
       

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            WWReportList();
        }

        private void toolStripButtonPrint_Click(object sender, EventArgs e)
        {
            WWPrint(0);
        }

        private void toolStripButtonPrintYL_Click(object sender, EventArgs e)
        {
            WWPrint(1);
        }

        private void toolStripButtonJCSH_Click(object sender, EventArgs e)
        {
            WWReportExamineNO();
        }

       
        private void dataGridViewResult_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void 报表格式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                UserdefinedFieldUpdateForm userset1 = new UserdefinedFieldUpdateForm(strTaleIDreport);//主表ID 报告
                userset1.ShowDialog();
            }
            catch (Exception ex)
            {
               WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 结果模式ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                UserdefinedFieldUpdateForm userset1 = new UserdefinedFieldUpdateForm(this.strTaleIDresult);//结果列表
                userset1.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }

        }

        private void dataGridViewReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void groupBoxif_Enter(object sender, EventArgs e)
        {

        }

        private void dataGridViewReport_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void fhz_type_idLabel_Click(object sender, EventArgs e)
        {

        }

        private void fhz_type_idComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void finstr_idComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void sam_jyBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                comjyfjy_id = "";
                comfinstr_id = "";
                rowCurrent = (DataRowView)sam_jyBindingSource.Current;
                if (this.dtResult != null)
                    this.dtResult.Clear();
                if (rowCurrent != null)
                {
                    comjyfjy_id = rowCurrent["fjy_id"].ToString();
                    comfinstr_id = rowCurrent["finstr_id"].ToString();
                    BllGetResult(comjyfjy_id);
                }                

            }

            catch (Exception ex)
            {
               WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            try
            {
                HelpForm help = new HelpForm("84823c3f5e5a4df9985f9ab69d33a9cb");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }        
      
    }
}