﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace yunLis.lis.sam.jy
{
    public partial class FrmItemValueList : Form
    {
        public string itemValue = "";
        public FrmItemValueList(DataTable dt)
        {
            InitializeComponent();

            this.dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GetItemValue();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetItemValue();
        }

        private void GetItemValue()
        {
            try
            {
                DataGridViewRow row = this.dataGridView1.CurrentRow;
                if (row != null)
                {
                    itemValue = row.Cells[0].Value.ToString();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
    }
}
