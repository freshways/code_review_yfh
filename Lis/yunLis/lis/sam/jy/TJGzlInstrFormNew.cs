﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using yunLis.lisbll.sam;
using yunLis.wwfbll;

namespace yunLis.lis.sam.jy
{
    public partial class TJGzlInstrFormNew : yunLis.wwf.SysBaseForm
    {
        private DataTable dtResult = null;
        private DataTable dtDoctor = null;
        private Report.TJGzlInstrReport rep = new Report.TJGzlInstrReport();
        private List<string> colList = new List<string>();//记录统计项目及项目顺序
        ItemBLL bllItem = new ItemBLL();//项目逻辑


        public TJGzlInstrFormNew()
        {
            InitializeComponent();

            documentViewer1.DocumentSource = rep;
            
        }
        private void TJGzlInstrFormNew_Load(object sender, EventArgs e)
        {
            InitDate();

            InitDoctorInfo();
            InitApplyUserID();
            InitCheckUser();

            InitInst();

            InitDept();

            InitType();

            InitSexComboBox();

        }

        private void InitType()
        {
            DataTable dtPatientType = new DataTable();
            dtPatientType.Columns.Add("fcode");
            dtPatientType.Columns.Add("fname");

            //空白行
            DataRow drSpace = dtPatientType.NewRow();
            drSpace["fcode"] = "";
            drSpace["fname"] = "";
            dtPatientType.Rows.Add(drSpace);

            DataRow drMZ = dtPatientType.NewRow();
            drMZ["fcode"] = "1";
            drMZ["fname"] = "门诊";
            dtPatientType.Rows.Add(drMZ);

            DataRow drJZ = dtPatientType.NewRow();
            drJZ["fcode"] = "2";
            drJZ["fname"] = "急诊";
            dtPatientType.Rows.Add(drJZ);

            DataRow drZY = dtPatientType.NewRow();
            drZY["fcode"] = "3";
            drZY["fname"] = "住院";
            dtPatientType.Rows.Add(drZY);

            DataRow drCT = dtPatientType.NewRow();
            drCT["fcode"] = "4";
            drCT["fname"] = "查体";
            dtPatientType.Rows.Add(drCT);

            //this.cboPatientType.DisplayMember = "fname";
            //this.cboPatientType.ValueMember = "fcode";
            this.cbo病人类别.DataSource = dtPatientType;
        }

        private void InitSexComboBox()
        {
            //try
            //{
            //    DataTable dtSex = bllType.BllComTypeDT("性别");

            //    //空白行
            //    DataRow drSpace = dtSex.NewRow();
            //    drSpace["fcode"] = "";
            //    drSpace["fname"] = "";
            //    dtSex.Rows.InsertAt(drSpace, 0);

            //    this.fhz_sex性别Combo.DataSource = dtSex;

            //    this.fhz_sex性别Combo.SelectedIndex = 0;
            //}
            //catch
            //{
            DataTable dtSex = new DataTable();
            dtSex.Columns.Add("fcode");
            dtSex.Columns.Add("fname");

            //空白行
            DataRow drSpace = dtSex.NewRow();
            drSpace["fcode"] = "";
            drSpace["fname"] = "";
            dtSex.Rows.Add(drSpace);

            //添加男性
            DataRow drMale = dtSex.NewRow();
            drMale["fcode"] = "1";
            drMale["fname"] = "男";
            dtSex.Rows.Add(drMale);

            //添加女性
            DataRow drFemale = dtSex.NewRow();
            drFemale["fcode"] = "2";
            drFemale["fname"] = "女";
            dtSex.Rows.Add(drFemale);

            this.cbo性别.DataSource = dtSex;
            //}
        }

        private void InitDept()
        {
            DataTable dtDept = null;
            try
            {

                DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                    "SELECT '' fdept_id,'' fname union all SELECT fdept_id,fname FROM WWF_DEPT where fuse_flag='1'");
                dtDept = ds.Tables[0];

                this.cbo送检科室.DataSource = dtDept;
            }
            catch
            {
                dtDept = new DataTable();
                dtDept.Columns.Add("fdept_id");
                dtDept.Columns.Add("fname");

                DataRow drSpace = dtDept.NewRow();
                drSpace["fdept_id"] = "";
                drSpace["fname"] = "";
                dtDept.Rows.Add(drSpace);

                this.cbo送检科室.DataSource = dtDept;
                this.cbo送检科室.SelectedValue = "";
            }
        }

        private void InitDate()
        {
            jyDateBegin.Value = DateTime.Now;
            jyDateEnd.Value = DateTime.Now;
        }

        private void InitDoctorInfo()
        {
            try
            {
                DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                    "SELECT '' fperson_id,'' fname union all SELECT [fperson_id],[fname] FROM [WWF_PERSON] WHERE fuse_flag = 1 order by fname ");
                dtDoctor = ds.Tables[0];
            }
            catch
            {
                dtDoctor = new DataTable();
                dtDoctor.Columns.Add("fperson_id");
                dtDoctor.Columns.Add("fname");

                DataRow drSpace = dtDoctor.NewRow();
                drSpace["fperson_id"] = "";
                drSpace["fname"] = "";
                dtDoctor.Rows.Add(drSpace);
            }
        }
        private void InitApplyUserID()
        {
            DataTable dtApplyUser = null;
            try
            {
                dtApplyUser = dtDoctor.Copy();//减少数据库访问次数
                this.cbo送检医生.DataSource = dtApplyUser;
            }
            catch
            {
                dtApplyUser = new DataTable();
                dtApplyUser.Columns.Add("fperson_id");
                dtApplyUser.Columns.Add("fname");

                DataRow drSpace = dtApplyUser.NewRow();
                drSpace["fperson_id"] = "";
                drSpace["fname"] = "";
                dtApplyUser.Rows.Add(drSpace);

                this.cbo送检医生.DataSource = dtApplyUser;
            }
        }
        private void InitCheckUser()
        {
            DataTable dtJYUser = null;
            try
            {
                dtJYUser = dtDoctor.Copy();

                this.cbo检验医生.DataSource = dtJYUser;
            }
            catch
            {
                dtJYUser = new DataTable();
                dtJYUser.Columns.Add("fperson_id");
                dtJYUser.Columns.Add("fname");

                DataRow drSpace = dtJYUser.NewRow();
                drSpace["fperson_id"] = "";
                drSpace["fname"] = "";
                dtJYUser.Rows.Add(drSpace);

                this.cbo检验医生.DataSource = dtJYUser;
            }
        }

        private void InitInst()
        {
            try
            {
                InstrBLL bllInstr = new InstrBLL();
                DataTable dtInstr = bllInstr.BllInstrDTByUseAndGroupID(1, yunLis.wwfbll.LoginBLL.strDeptID);//仪器列表
                this.cbo设备名称.DataSource = dtInstr;

                string strInstrID = yunLis.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    DataRow[] drInstr = dtInstr.Select("finstr_id='" + strInstrID + "'");
                    if (drInstr.Length > 0)
                    {
                        this.cbo设备名称.SelectedValue = strInstrID;
                    }
                    else if(dtInstr.Rows.Count > 0)
                    {
                        this.cbo设备名称.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        
        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkbox = sender as CheckBox;
            string colName = checkbox.Text;

            dtResult = null;//清空数据源
            rep.Set统计条件("");
            rep.SetDataSource(null);//置空数据源
            //rep.SetSummary(0, 0);

            if (checkbox.Checked)
            {
                rep.AddColumn(colName, colName);
                colList.Add(colName);
            }
            else
            {
                rep.RemoveColumn(colName);
                colList.Remove(colName);
            }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (this.cbo设备名称.SelectedValue == null || string.IsNullOrWhiteSpace(this.cbo设备名称.SelectedValue.ToString()))
            {
                MessageBox.Show("没有选择设备名称，无法统计工作量。", "提示",MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            dtResult = GetData();

            string str查询条件 = Get查询条件();

            SetReport(str查询条件, dtResult);
        }

        private string Get查询条件()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("检验日期：" + jyDateBegin.Value.ToString("yyyy-MM-dd") + "至" + jyDateEnd.Value.ToString("yyyy-MM-dd"));
            builder.Append("，设备名称:" + cbo设备名称.Text);

            if (cbo检验项目.SelectedValue == null || string.IsNullOrWhiteSpace(cbo检验项目.SelectedValue.ToString()))
            {
            }
            else
            {
                builder.Append("，检验项目:" + cbo检验项目.Text);
            }

            if (cbo送检科室.SelectedValue == null || string.IsNullOrWhiteSpace(cbo送检科室.SelectedValue.ToString()))
            { }
            else
            {
                builder.Append("，送检科室:" + cbo送检科室.Text);
            }

            if (cbo送检医生.SelectedValue == null || string.IsNullOrWhiteSpace(cbo送检医生.SelectedValue.ToString()))
            { }
            else
            {
                builder.Append("，送检医生:" + cbo送检医生.Text);
            }

            if (cbo检验医生.SelectedValue == null || string.IsNullOrWhiteSpace(cbo检验医生.SelectedValue.ToString()))
            { }
            else
            {
                builder.Append("，检验医生:" + cbo检验医生.Text);
            }

            if (cbo病人类别.SelectedValue == null || string.IsNullOrWhiteSpace(cbo病人类别.SelectedValue.ToString()))
            { }
            else
            {
                builder.Append("，病人类别:" + cbo病人类别.Text);
            }

            if (cbo性别.SelectedValue == null || string.IsNullOrWhiteSpace(cbo性别.SelectedValue.ToString()))
            { }
            else
            {
                builder.Append("，性别：" + cbo性别.Text);
            }
            return builder.ToString();
        }

        private void SetReport(string content, DataTable dtSource)
        {
            rep.Set统计条件(content);

            rep.SetDataSource(dtSource);

            //rep.SetSummary(0, 0);

            rep.CreateDocument();
        }

        private DataTable GetData()
        {
            try
            {
                List<string> paramList = new List<string>();
                //@begin varchar(12), @end varchar(12),--开始截止日期
                //@inst varchar(50),--设备id
                //@item varchar(50), --itemid，不是item code 或者 item name
                //@applyuser varchar(20),--送检医生id
                //@jyuser varchar(20), --检验医生id
                //@dept varchar(20), --送检科室id
                //@type varchar(10), --病人类型id
                //@sex varchar(4), --性别id
                //@list varchar(200)--统计项目list
                paramList.Add(jyDateBegin.Value.ToString("yyyy-MM-dd"));
                paramList.Add(jyDateEnd.Value.ToString("yyyy-MM-dd"));
                paramList.Add(cbo设备名称.SelectedValue.ToString());
                paramList.Add((cbo检验项目.SelectedValue == null ? "" : cbo检验项目.SelectedValue.ToString()));
                paramList.Add((cbo送检医生.SelectedValue == null ? "" : cbo送检医生.SelectedValue.ToString()));
                paramList.Add((cbo检验医生.SelectedValue == null ? "" : cbo检验医生.SelectedValue.ToString()));
                paramList.Add((cbo送检科室.SelectedValue == null ? "" : cbo送检科室.SelectedValue.ToString()));
                paramList.Add((cbo病人类别.SelectedValue == null ? "" : cbo病人类别.SelectedValue.ToString()));
                paramList.Add((cbo性别.SelectedValue == null ? "" : cbo性别.SelectedValue.ToString()));

                string strcolList = GetColList(colList);
                paramList.Add(strcolList);

                string stroedProce = "Pro_GzltjInstr";
                DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetByStoredProc(WWFInit.strDBConn, stroedProce, paramList.ToArray());
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("查询过程中出现异常，异常信息：" + ex.Message);
                return null;
            }
        }

        private string GetColList(List<string> list)
        {
            //1	病人类型 
            //2	科室
            //3	送检医生
            //4	性别
            //5	检验医生
            //6	项目
            //7	样本类型
            //8	费别
            StringBuilder builder = new StringBuilder();
            string[,] cols = new string[8,2] {{"1","病人类型"},
                                {"2","科室"},
                                {"3","送检医生"},
                                {"4","性别"},
                                {"5","检验医生"},
                                {"6","项目"},
                                {"7","样本类型"},
                                {"8","费别"}};
            for(int index = 0; index < list.Count; index++)
            {
                for(int inner = 0; inner < cols.GetLength(0); inner++)
                {
                    if(list[index] == cols[inner,1])
                    {
                        builder.Append(cols[inner,0]);
                        break;
                    }
                }
            }

            return builder.ToString();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraReports.UI.ReportPrintTool tool = new DevExpress.XtraReports.UI.ReportPrintTool(rep);
                tool.Print();
            }
            catch(Exception ex)
            {
                MessageBox.Show("打印过程中出现异常，异常信息：" + ex.Message);
            }
        }

        private void btnExportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialogPdf.Filter = "PDF Files|*.pdf";
                
                DialogResult result = saveFileDialogPdf.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }

                string filePath = saveFileDialogPdf.FileName;
                documentViewer1.PrintingSystem.ExportToPdf(filePath);
            }
            catch
            {
                MessageBox.Show("导出PDF文档时出现异常信息。","提示");
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //if (dtResult == null || dtResult.Rows.Count == 0)
                //{
                //    MessageBox.Show("统计出的数据是空的，请先执行查询操作。", "提示");
                //    return;
                //}

                DialogResult result = MessageBox.Show("是否将统计报表中的内容导出至Excel文档中？\n“是”：原样导出统计报表；“否”：只导出统计数据；\n“取消”：不执行导出操作。", "确认", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                //DialogResult result = MessageBox.Show("是否将统计报表中的内容导出至Excel文档中？\n“是”：原样导出统计报表；\n“否”：不执行导出操作。", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (result == DialogResult.Cancel)
                //if(result == DialogResult.Cancel || result==DialogResult.No)
                {
                    return;
                }

                saveFileDialogPdf.Filter = "xls files(*.xlsx)|*.xlsx";
                saveFileDialogPdf.ShowDialog();
                string filePath = saveFileDialogPdf.FileName;

                //if(System.IO.File.Exists(filePath))
                //{
                //    MessageBox.Show("文件名已存在！");
                //    return;
                //}

                if(result == DialogResult.Yes)
                {
                    documentViewer1.PrintingSystem.ExportToXlsx(filePath);
                }
                else
                {
                    ExportDataTableToExcel(dtResult, filePath);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("导出Excel表格时出现异常信息。\n异常信息："+ex.Message, "提示");
            }
        }

        private void cbo设备名称_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtItem = this.bllItem.BllItem(1, (cbo设备名称.SelectedValue == null ? "" : cbo设备名称.SelectedValue.ToString()), "");

                DataRow dr = dtItem.NewRow();
                dr["fitem_id"] = "";
                dr["fitem_code"] = "";
                dr["fname"] = "";
                dtItem.Rows.InsertAt(dr, 0);

                this.cbo检验项目.DataSource = dtItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show("查询设备项目信息时出现异常。异常信息：" + ex.Message, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.cbo检验项目.DataSource = "";
            }
        }

        //路径必须是*.xlsx的文件路径
        private void ExportDataTableToExcel(DataTable dt, string filePath)
        {
            try
            {
               ExcelHelper.DataTableToExcel(dt, filePath);
            }
            catch(Exception ex)
            {
                MessageBox.Show("导出数据是出现异常。异常信息：" + ex.Message, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

    }
}
