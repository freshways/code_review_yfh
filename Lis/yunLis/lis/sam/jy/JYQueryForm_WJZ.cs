﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;
using yunLis.lis.com;
using System.Collections;
using yunLis.lisbll.sam;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ww.wwf.com;
using WEISHENG.COMM.PluginsAttribute;

namespace yunLis.lis.sam.jy
{
    [ClassInfoMark(GUID = "B1B39175-AE87-40E5-A458-DA26C417554B", 键ID = "B1B39175-AE87-40E5-A458-DA26C417554B", 父ID = "BE22B406-5548-4F27-9F9D-4582D583E701",
    功能名称 = "危急值查询", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.jy.JYQueryForm_WJZ",
    传递参数 = "", 显示顺序 = 1,
    菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public partial class JYQueryForm_WJZ : com.JyBaseForm
    {
        #region 自定义变量
        jybll blljy = new jybll();

        DataTable dtIMG = new DataTable();

        /// <summary>
        /// 当前行 样本
        /// </summary>
        DataRowView rowCurrent = null;
        /// <summary>
        /// 样本ID
        /// </summary>
        string strfsample_id = "";
        /// <summary>
        /// 申请ID
        /// </summary>
        string strfapply_id = "";
        /// <summary>
        /// 审核标记
        /// </summary>
        string strfexamine_flag = "0";
        /// <summary>
        /// 仪器列表
        /// </summary>
        DataTable dtInstr = null;
        #endregion

        #region 自定义方法
        /// <summary>
        /// 本页初始化方法
        /// </summary>
        private void WWInit()
        {
            try
            {
                this.com_listBindingSource_fstate.DataSource = this.WWComTypeSampleState();//检验状态
                this.com_listBindingSource_fjytype_id.DataSource = this.WWCheckType();//检验类别
                this.com_listBindingSource_fsample_type_id.DataSource = this.WWSampleType();//样本类别
                this.com_listBindingSource_fapply_dept_id.DataSource = this.WWApplyDept();//申请部门
                this.com_listBindingSource_fapply_user_id.DataSource = this.WWUserApply();//申请人
                this.com_listBindingSource_fjy_user_id.DataSource = this.WWUserCheck();//检验人
                dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, yunLis.wwfbll.LoginBLL.strDeptID);//仪器列表
                this.fjy_instrComboBox.DataSource = dtInstr;
                fjy_instrComboBox.SelectedValue = "";

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = yunLis.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.fjy_instrComboBox.SelectedValue = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能

                this.cobx病人类别.DataSource = this.WWComTypeftype_id();//病人类别 
                cobx病人类别.SelectedValue = "";

                dataGridViewReport.AutoGenerateColumns = false;
                dataGridViewResult.AutoGenerateColumns = false;

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 结果列显示 
        /// </summary>
        private void bllResultShow(string str仪器id)
        {
            if (str仪器id.Equals("mby_lb_mk3"))
            {
                this.fod.Visible = true;
                this.fcutoff.Visible = true;
            }
            else
            {
                this.fod.Visible = false;
                this.fcutoff.Visible = false;
            }
        }


        /// <summary>
        /// 取得报告列表
        /// </summary>
        private void WWSetReportList()
        {
            if (this.dtReportResult != null)
                this.dtReportResult.Clear();

            try
            {
                Cursor = Cursors.WaitCursor;
                string finstr_id = "";//仪器ID
                string f患者类别 = "";//患者类别
                string fjy_date1 = this.bllReport.DbDateTime1(this.fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllReport.DbDateTime1(this.fjy_dateDateTimePicker2);
                string fjy_dateWhere = "";

                try
                {
                    finstr_id = fjy_instrComboBox.SelectedValue.ToString();
                }
                catch { }
                try
                {
                    f患者类别 = cobx病人类别.SelectedValue.ToString();
                }
                catch { }

                if (fjy_instrComboBox.Text == "" || fjy_instrComboBox.Text == null)
                    finstr_id = " (fjy_instr is not null) ";
                else
                    finstr_id = " (fjy_instr='" + finstr_id + "') ";

                if (this.cobx病人类别.Text == "" || this.cobx病人类别.Text == null)
                    f患者类别 = " and (fhz_type_id is not null)";
                else
                    f患者类别 = " and (fhz_type_id='" + f患者类别 + "') ";

                fjy_dateWhere = " and (fjy_date>='" + fjy_date1 + "' and fjy_date<='" + fjy_date2 + "') ";
                fjy_dateWhere += @" and t.fjy_id in(
SELECT re.fjy_id FROM [dbo].[SAM_JY_RESULT] re
where 1=1 and (fitem_badge='↑↑' or fitem_badge='↓↓')) ";

                string strWhere = finstr_id + f患者类别 + fjy_dateWhere + " order by fjy_date desc,convert(int,fjy_yb_code)  ";

                this.WWSetdtReport(strWhere);
                this.lIS_REPORTBindingSource.DataSource = this.dtReport;
                dataGridViewReport.DataSource = this.lIS_REPORTBindingSource;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        /// <summary>
        /// 获取危急值
        /// </summary>
        /// <param name="itemid"></param>
        /// <returns></returns>
        private string GetJGRefValueByItemCode(string itemid)
        {
            //add by yfh 202001917 获取危急值参考 ▽
            DataRow[] drItems = dtItemList.Select("fitem_id='" + itemid + "'");
            DataRow drItem = drItems[0];
            string fref_if_age = drItem["fref_if_age"].ToString();
            string fref_if_sex = drItem["fref_if_sex"].ToString();
            string fref_if_sample = drItem["fref_if_sample"].ToString();
            string fref_if_method = drItem["fref_if_method"].ToString();
            string fref = drItem["fjg_value_xx"].ToString() + "--" + drItem["fjg_value_sx"].ToString();

            if (fref_if_age == "0" && fref_if_sex == "0" && fref_if_sample == "0" && fref_if_method == "0")
            {
                return fref;
            }
            //add by yfh 202001917 获取危急值参考 △

            string str危急值 = "";
            string strSampleTypeid = "0";
            if (dtItemList.Columns.Contains("fjy_yb_type"))
            {
                strSampleTypeid = drItem["fjy_yb_type"].ToString();
            }
            str危急值 = this.blljy.Bll危急值(itemid, Convert.ToInt32(fref_if_age), fref_if_sex, strSampleTypeid);
            return str危急值;
        }

        private void WWResultList()
        {
            try
            {
                this.WWSetdtReportResult(this.strfapply_id);
                foreach (var item in dtReportResult)
                {
                    string strItemId = item.fitem_id;
                    string strfvalue = item.fvalue;
                    string str参考值 = item.fitem_ref;

                    item.fitem_jgref = GetJGRefValueByItemCode(strItemId);

                    if (strfvalue.Equals("") || strfvalue.Length == 0 || strfvalue == null) { }
                    else
                    {
                        item.fitem_badge = this.blljy.Bll结果标记(strfvalue, str参考值, item.fitem_jgref);
                    }

                }

                this.bindingSourceResult.DataSource = this.dtReportResult;
                this.dataGridViewResult.DataSource = this.bindingSourceResult;

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == (Keys.F1))
            {
                toolStripButtonHelp.PerformClick();
                return true;
            }
            if (keyData == (Keys.F5))
            {
                toolStripButtonQuery.PerformClick();
                return true;
            }
            if (keyData == (Keys.F8))
            {
                toolStripButtonPrint.PerformClick();
                return true;
            }
            if (keyData == (Keys.F9))
            {
                toolStripButtonPrintYL.PerformClick();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType"></param>
        private bool WWPrint(int printType) //20150610 wjz 修改返回类型 void ==> bool
        {
            bool ret = false;
            try
            {
                yunLis.lis.sam.Report.PrintHelper pr = new yunLis.lis.sam.Report.PrintHelper();
                ret = pr.GetData_PrintViewer(this.strfapply_id, true); //  strfsample_id
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return ret;
        }
        DataTable dtIMG_1 = new DataTable();
        #endregion

        public JYQueryForm_WJZ()
        {
            InitializeComponent();
            WWInit();
        }

        DataTable dtItemList = new DataTable();
        private void JYQueryForm_WJZ_Load(object sender, EventArgs e)
        {
            try
            {
                string sql = "SELECT * FROM sam_item t where 1=1 order by fprint_num";
                dtItemList = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
                WWSetReportList();
            }

            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        string strfinstr_result_id = "";
        private void lIS_REPORTBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrent = (DataRowView)this.lIS_REPORTBindingSource.Current;
                if (this.dtReportResult.Count > 0)
                    this.dtReportResult.Clear();
                if (rowCurrent != null)
                {
                    strfapply_id = rowCurrent["fjy_id"].ToString();
                    strfsample_id = rowCurrent["fjy_id"].ToString();
                    strfinstr_result_id = rowCurrent["finstr_result_id"].ToString();



                    if (rowCurrent["fjy_zt"] != null)
                        strfexamine_flag = rowCurrent["fjy_zt"].ToString();
                    WWResultList();
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }


        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            this.WWOpenHelp("lis_jyquery");
        }

        private void toolStripButtonPrintYL_Click(object sender, EventArgs e)
        {
            bool printSuccess = WWPrint(1);
        }

        private void toolStripButtonPrint_Click(object sender, EventArgs e) //此函数变更比较大
        {
            bool printResult = WWPrint(0);
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            WWSetReportList();
        }

        private void dataGridViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewReport.Rows.Count > 0)
                {

                    strfapply_id = this.dataGridViewReport.CurrentRow.Cells["fjy_id"].Value.ToString();
                    WWResultList();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        private void fjy_instrComboBox_DropDownClosed(object sender, EventArgs e)
        {
            string str仪器id = "";//
            {
                str仪器id = Convert.ToString(this.fjy_instrComboBox.SelectedValue);
            }
            bllResultShow(str仪器id);

        }

        private void dataGridViewReport_DataError_1(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewResult_DataError_1(object sender, DataGridViewDataErrorEventArgs e)
        {

        }


        private void dataGridViewReport_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //20150611 add begin wjz 已打印报表显示浅蓝色
            foreach (DataGridViewRow row in dataGridViewReport.Rows)
            {
                string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
                if (str打印状态.Equals("已打印"))
                {
                    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                }
            }
            //20150611 add end wjz 已打印报表显示浅蓝色
        }

        private void toolStripButtonExportToXls_Click(object sender, EventArgs e)
        {
            if (dataGridViewReport.RowCount == 0)
            {
                MessageBox.Show("没有需要导出的数据。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                ExcelHelper.DataGridViewToExcel(dataGridViewReport);
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出过程中出现未知错误！,异常信息：" + ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_推送化验结果_Click(object sender, EventArgs e)
        {
            WJZ_reportmodel model = new WJZ_reportmodel();
            jybll blljy = new jybll();

            DataTable dt = ((System.Windows.Forms.BindingSource)(dataGridViewReport.DataSource)).DataSource as DataTable;
            int index = dataGridViewReport.CurrentRow.Index;
            string s检验日期 = dataGridViewReport.Rows[index].Cells["fjy_date"].Value.ToString();
            string s姓名 = dataGridViewReport.Rows[index].Cells["fhz_name"].Value.ToString();
            string s性别 = dataGridViewReport.Rows[index].Cells["性别"].Value.ToString();
            string s年龄 = dataGridViewReport.Rows[index].Cells["fhz_age"].Value.ToString();
            string s年龄单位 = dataGridViewReport.Rows[index].Cells["年龄单位"].Value.ToString();
            string s住院号 = dataGridViewReport.Rows[index].Cells["fhz_zyh"].Value.ToString();
            string s科室 = dataGridViewReport.Rows[index].Cells["科室"].Value.ToString();
            string bcard = dt.Rows[index]["f1"].ToString();
            string reportid = dt.Rows[index]["fjy_id"].ToString();
            //患者信息
            model.fdj_date = s检验日期;
            model.fhz_name = s姓名;
            model.fhz_sex = s性别;
            model.fhz_age = s年龄;
            model.fhz_age_unit = s年龄单位;
            model.fhz_zyh = s住院号;
            model.fjs_dept = s科室;
            model.fjy_id = reportid;
            model.fbg_name = HIS.COMM.zdInfo.ModelUserInfo.用户名;
            model.fbg_date1 = DateTime.Now.Hour.ToString();
            model.fbg_date2 = DateTime.Now.Minute.ToString();
            #region 根据标识显示颜色
            string warning = string.Empty;
            if (this.dataGridViewResult.Rows.Count > 0)
            {
                for (int intGrid = 0; intGrid < this.dataGridViewResult.Rows.Count; intGrid++)
                {
                    string strBJ = this.dataGridViewResult.Rows[intGrid].Cells["fitem_badge"].Value.ToString();
                    if (strBJ.Equals("↑↑"))
                    {
                        string fitem_name = this.dataGridViewResult.Rows[intGrid].Cells["fitem_name"].Value.ToString();
                        string fvalue = this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Value.ToString();
                        warning += "[" + fitem_name + "]:[" + fvalue + "]";
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_badge"].Style.ForeColor = Color.Red;
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_jgref"].Style.ForeColor = Color.Wheat;
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_jgref"].Style.BackColor = Color.Red;
                    }
                    else if (strBJ.Equals("↓↓"))
                    {
                        string fitem_name = this.dataGridViewResult.Rows[intGrid].Cells["fitem_name"].Value.ToString();
                        string fvalue = this.dataGridViewResult.Rows[intGrid].Cells["fvalue"].Value.ToString();
                        warning += "[" + fitem_name + "]:[" + fvalue + "]";
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_badge"].Style.ForeColor = Color.Blue;
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_jgref"].Style.ForeColor = Color.Wheat;
                        this.dataGridViewResult.Rows[intGrid].Cells["fitem_jgref"].Style.BackColor = Color.Red;
                    }
                }
            }
            #endregion
            model.fwjz_nr = warning;

            JYQueryForm_WJZAddForm frm = new JYQueryForm_WJZAddForm(model);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                IList lisSql = new ArrayList();
                StringBuilder strSql = new StringBuilder();
                //blljy.AddSql(model);
                strSql.Append("delete from WJZ_report where fjy_id='" + model.fjy_id + "' ");
                strSql.Append("insert into WJZ_report(");
                strSql.Append("[fdj_date] ,[fhz_name] ,[fhz_sex] ,[fhz_age] ,[fhz_age_unit] ,[fhz_zyh] ,[fwjz_nr] ,[fbg_dept] ,[fbg_name] ,[fbg_date1] ,[fbg_date2] ,[fjs_dept] ,[fjs_tel] ,[fjs_name] ,[fjs_date1] ,[fjs_date2] ,[f_clcs] ,[f_clz],[fjy_id]");
                strSql.Append(")");
                strSql.Append(" values (");
                strSql.Append("'" + model.fdj_date + "',");
                strSql.Append("'" + model.fhz_name + "',");
                strSql.Append("'" + model.fhz_sex + "',");
                strSql.Append("'" + model.fhz_age + "',");
                strSql.Append("'" + model.fhz_age_unit + "',");
                strSql.Append("'" + model.fhz_zyh + "',");
                strSql.Append("'" + model.fwjz_nr + "',");
                strSql.Append("'" + model.fbg_dept + "',");
                strSql.Append("'" + model.fbg_name + "',");
                strSql.Append("'" + model.fbg_date1 + "',");
                strSql.Append("'" + model.fbg_date2 + "',");
                strSql.Append("'" + model.fjs_dept + "',");
                strSql.Append("'" + model.fjs_tel + "',");
                strSql.Append("'" + model.fjs_name + "',");
                strSql.Append("'" + model.fjs_date1 + "',");
                strSql.Append("'" + model.fjs_date2 + "',");
                strSql.Append("'" + model.f_clcs + "',");
                strSql.Append("'" + model.f_clz + "',");
                strSql.Append("'" + model.fjy_id + "'");
                strSql.Append(")");
                lisSql.Add(strSql);
                string strRRR = WWFInit.wwfRemotingDao.DbTransaction(WWFInit.strDBConn, lisSql);
                if (strRRR.Equals("true"))
                {
                    MessageBox.Show("保存成功，上报完成！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

        }
    }
}