﻿namespace yunLis.lis.sam.jy
{
    partial class BatchInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BatchInputForm));
            this.comboBox设备型号 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxEnd样本号 = new System.Windows.Forms.TextBox();
            this.textBoxBegin样本号 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker样本日期 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkedListBoxItems = new System.Windows.Forms.CheckedListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.labelMessage = new System.Windows.Forms.Label();
            this.textBox提示信息 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox设备型号
            // 
            this.comboBox设备型号.DisplayMember = "fname";
            this.comboBox设备型号.FormattingEnabled = true;
            this.comboBox设备型号.Location = new System.Drawing.Point(83, 17);
            this.comboBox设备型号.Name = "comboBox设备型号";
            this.comboBox设备型号.Size = new System.Drawing.Size(179, 20);
            this.comboBox设备型号.TabIndex = 1;
            this.comboBox设备型号.ValueMember = "finstr_id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(12, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "仪器型号：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(163, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "--";
            // 
            // textBoxEnd样本号
            // 
            this.textBoxEnd样本号.Location = new System.Drawing.Point(190, 45);
            this.textBoxEnd样本号.Name = "textBoxEnd样本号";
            this.textBoxEnd样本号.Size = new System.Drawing.Size(72, 21);
            this.textBoxEnd样本号.TabIndex = 4;
            this.textBoxEnd样本号.Text = "1";
            this.textBoxEnd样本号.Enter += new System.EventHandler(this.SelectAll);
            this.textBoxEnd样本号.MouseHover += new System.EventHandler(this.SelectAll);
            // 
            // textBoxBegin样本号
            // 
            this.textBoxBegin样本号.Location = new System.Drawing.Point(83, 45);
            this.textBoxBegin样本号.Name = "textBoxBegin样本号";
            this.textBoxBegin样本号.Size = new System.Drawing.Size(74, 21);
            this.textBoxBegin样本号.TabIndex = 3;
            this.textBoxBegin样本号.Text = "1";
            this.textBoxBegin样本号.Enter += new System.EventHandler(this.SelectAll);
            this.textBoxBegin样本号.MouseHover += new System.EventHandler(this.SelectAll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "样 本 号：";
            // 
            // dateTimePicker样本日期
            // 
            this.dateTimePicker样本日期.Location = new System.Drawing.Point(363, 14);
            this.dateTimePicker样本日期.Name = "dateTimePicker样本日期";
            this.dateTimePicker样本日期.Size = new System.Drawing.Size(131, 21);
            this.dateTimePicker样本日期.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(292, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 12;
            this.label1.Text = "样本日期：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(292, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "结　　果：";
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(363, 44);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(131, 21);
            this.textBoxValue.TabIndex = 5;
            this.textBoxValue.Enter += new System.EventHandler(this.SelectAll);
            this.textBoxValue.MouseHover += new System.EventHandler(this.SelectAll);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkedListBoxItems);
            this.groupBox1.Location = new System.Drawing.Point(14, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(497, 177);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "项目信息";
            // 
            // checkedListBoxItems
            // 
            this.checkedListBoxItems.CheckOnClick = true;
            this.checkedListBoxItems.FormattingEnabled = true;
            this.checkedListBoxItems.Items.AddRange(new object[] {
            "HBsAb",
            "HBsAg",
            "HBeAg",
            "HBeAb",
            "HBcAb",
            "Pre-S1",
            "anti-HC",
            "抗-HIV",
            "TPPA",
            "K＋",
            "Na＋",
            "CL－",
            "iCa",
            "PH",
            "TCO2"});
            this.checkedListBoxItems.Location = new System.Drawing.Point(6, 20);
            this.checkedListBoxItems.MultiColumn = true;
            this.checkedListBoxItems.Name = "checkedListBoxItems";
            this.checkedListBoxItems.Size = new System.Drawing.Size(485, 148);
            this.checkedListBoxItems.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(305, 255);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 18;
            this.btnAdd.Text = "追加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(419, 255);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "关闭";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.Font = new System.Drawing.Font("宋体", 10F);
            this.labelMessage.ForeColor = System.Drawing.Color.Red;
            this.labelMessage.Location = new System.Drawing.Point(20, 259);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(77, 14);
            this.labelMessage.TabIndex = 20;
            this.labelMessage.Text = "结果提示：";
            this.labelMessage.Visible = false;
            // 
            // textBox提示信息
            // 
            this.textBox提示信息.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox提示信息.ForeColor = System.Drawing.Color.Red;
            this.textBox提示信息.Location = new System.Drawing.Point(4, 282);
            this.textBox提示信息.Name = "textBox提示信息";
            this.textBox提示信息.ReadOnly = true;
            this.textBox提示信息.Size = new System.Drawing.Size(516, 21);
            this.textBox提示信息.TabIndex = 21;
            // 
            // BatchInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 308);
            this.Controls.Add(this.textBox提示信息);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.comboBox设备型号);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxValue);
            this.Controls.Add(this.textBoxEnd样本号);
            this.Controls.Add(this.textBoxBegin样本号);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker样本日期);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BatchInputForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "手工项目";
            this.Load += new System.EventHandler(this.BatchInputForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox设备型号;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxEnd样本号;
        private System.Windows.Forms.TextBox textBoxBegin样本号;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker样本日期;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckedListBox checkedListBoxItems;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.TextBox textBox提示信息;

    }
}