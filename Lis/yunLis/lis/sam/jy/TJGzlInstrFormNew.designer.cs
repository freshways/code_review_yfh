﻿namespace yunLis.lis.sam.jy
{
    partial class TJGzlInstrFormNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label fhz_type_idLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label finstr_idLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            this.groupBoxif = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.btnExportPDF = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbo性别 = new System.Windows.Forms.ComboBox();
            this.cbo检验医生 = new System.Windows.Forms.ComboBox();
            this.cbo送检医生 = new System.Windows.Forms.ComboBox();
            this.cbo设备名称 = new System.Windows.Forms.ComboBox();
            this.cbo病人类别 = new System.Windows.Forms.ComboBox();
            this.cbo送检科室 = new System.Windows.Forms.ComboBox();
            this.cbo检验项目 = new System.Windows.Forms.ComboBox();
            this.jyDateBegin = new System.Windows.Forms.DateTimePicker();
            this.jyDateEnd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.documentViewer1 = new DevExpress.XtraPrinting.Preview.DocumentViewer();
            this.saveFileDialogPdf = new System.Windows.Forms.SaveFileDialog();
            fhz_type_idLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            finstr_idLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            this.groupBoxif.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fhz_type_idLabel
            // 
            fhz_type_idLabel.AutoSize = true;
            fhz_type_idLabel.Location = new System.Drawing.Point(344, 78);
            fhz_type_idLabel.Name = "fhz_type_idLabel";
            fhz_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fhz_type_idLabel.TabIndex = 19;
            fhz_type_idLabel.Text = "病人类别:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(9, 26);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // finstr_idLabel
            // 
            finstr_idLabel.AutoSize = true;
            finstr_idLabel.Location = new System.Drawing.Point(9, 52);
            finstr_idLabel.Name = "finstr_idLabel";
            finstr_idLabel.Size = new System.Drawing.Size(59, 12);
            finstr_idLabel.TabIndex = 10;
            finstr_idLabel.Text = "仪器名称:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(176, 52);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(59, 12);
            label2.TabIndex = 10;
            label2.Text = "检验项目:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(474, 78);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(35, 12);
            label3.TabIndex = 19;
            label3.Text = "性别:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(9, 78);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(59, 12);
            label4.TabIndex = 10;
            label4.Text = "送检科室:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(344, 26);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(59, 12);
            label5.TabIndex = 19;
            label5.Text = "送检医生:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(344, 52);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(59, 12);
            label6.TabIndex = 19;
            label6.Text = "检验医生:";
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(this.groupBox3);
            this.groupBoxif.Controls.Add(this.groupBox2);
            this.groupBoxif.Controls.Add(this.groupBox1);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 0);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Size = new System.Drawing.Size(979, 120);
            this.groupBoxif.TabIndex = 151;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnQuery);
            this.groupBox3.Controls.Add(this.btnPrint);
            this.groupBox3.Controls.Add(this.btnExportExcel);
            this.groupBox3.Controls.Add(this.btnExportPDF);
            this.groupBox3.Location = new System.Drawing.Point(733, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(189, 99);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "操作";
            // 
            // btnQuery
            // 
            this.btnQuery.Image = global::yunLis.Properties.Resources.wwNavigatorQuery;
            this.btnQuery.Location = new System.Drawing.Point(6, 61);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(82, 32);
            this.btnQuery.TabIndex = 17;
            this.btnQuery.Text = "查  询";
            this.btnQuery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::yunLis.Properties.Resources.button_print1;
            this.btnPrint.Location = new System.Drawing.Point(94, 61);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(89, 32);
            this.btnPrint.TabIndex = 20;
            this.btnPrint.Text = "打  印";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Image = global::yunLis.Properties.Resources.button_Next;
            this.btnExportExcel.Location = new System.Drawing.Point(94, 20);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(89, 32);
            this.btnExportExcel.TabIndex = 19;
            this.btnExportExcel.Text = "导出Excel";
            this.btnExportExcel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnExportPDF
            // 
            this.btnExportPDF.Image = global::yunLis.Properties.Resources.button_Next;
            this.btnExportPDF.Location = new System.Drawing.Point(6, 20);
            this.btnExportPDF.Name = "btnExportPDF";
            this.btnExportPDF.Size = new System.Drawing.Size(82, 32);
            this.btnExportPDF.TabIndex = 18;
            this.btnExportPDF.Text = "导出PDF";
            this.btnExportPDF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExportPDF.UseVisualStyleBackColor = true;
            this.btnExportPDF.Click += new System.EventHandler(this.btnExportPDF_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox8);
            this.groupBox2.Controls.Add(this.checkBox6);
            this.groupBox2.Controls.Add(this.checkBox4);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Controls.Add(this.checkBox7);
            this.groupBox2.Controls.Add(this.checkBox5);
            this.groupBox2.Controls.Add(this.checkBox3);
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Location = new System.Drawing.Point(579, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(148, 104);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "统计项目";
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(93, 83);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(48, 16);
            this.checkBox8.TabIndex = 16;
            this.checkBox8.Text = "费别";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(93, 62);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(48, 16);
            this.checkBox6.TabIndex = 14;
            this.checkBox6.Text = "项目";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(93, 41);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(48, 16);
            this.checkBox4.TabIndex = 12;
            this.checkBox4.Text = "性别";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(93, 20);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(48, 16);
            this.checkBox2.TabIndex = 10;
            this.checkBox2.Text = "科室";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(12, 83);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(72, 16);
            this.checkBox7.TabIndex = 15;
            this.checkBox7.Text = "样本类型";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(12, 62);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(72, 16);
            this.checkBox5.TabIndex = 13;
            this.checkBox5.Text = "检验医生";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(12, 41);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(72, 16);
            this.checkBox3.TabIndex = 11;
            this.checkBox3.Text = "送检医生";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 20);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 16);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "病人类型";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(label4);
            this.groupBox1.Controls.Add(this.cbo性别);
            this.groupBox1.Controls.Add(finstr_idLabel);
            this.groupBox1.Controls.Add(this.cbo检验医生);
            this.groupBox1.Controls.Add(label2);
            this.groupBox1.Controls.Add(this.cbo送检医生);
            this.groupBox1.Controls.Add(this.cbo设备名称);
            this.groupBox1.Controls.Add(this.cbo病人类别);
            this.groupBox1.Controls.Add(this.cbo送检科室);
            this.groupBox1.Controls.Add(label3);
            this.groupBox1.Controls.Add(this.cbo检验项目);
            this.groupBox1.Controls.Add(label6);
            this.groupBox1.Controls.Add(fjy_dateLabel);
            this.groupBox1.Controls.Add(label5);
            this.groupBox1.Controls.Add(this.jyDateBegin);
            this.groupBox1.Controls.Add(fhz_type_idLabel);
            this.groupBox1.Controls.Add(this.jyDateEnd);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 104);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // cbo性别
            // 
            this.cbo性别.DisplayMember = "fname";
            this.cbo性别.FormattingEnabled = true;
            this.cbo性别.Items.AddRange(new object[] {
            ""});
            this.cbo性别.Location = new System.Drawing.Point(515, 74);
            this.cbo性别.Name = "cbo性别";
            this.cbo性别.Size = new System.Drawing.Size(40, 20);
            this.cbo性别.TabIndex = 8;
            this.cbo性别.ValueMember = "fcode";
            // 
            // cbo检验医生
            // 
            this.cbo检验医生.DisplayMember = "fname";
            this.cbo检验医生.FormattingEnabled = true;
            this.cbo检验医生.Items.AddRange(new object[] {
            ""});
            this.cbo检验医生.Location = new System.Drawing.Point(409, 49);
            this.cbo检验医生.Name = "cbo检验医生";
            this.cbo检验医生.Size = new System.Drawing.Size(146, 20);
            this.cbo检验医生.TabIndex = 5;
            this.cbo检验医生.ValueMember = "fperson_id";
            // 
            // cbo送检医生
            // 
            this.cbo送检医生.DisplayMember = "fname";
            this.cbo送检医生.FormattingEnabled = true;
            this.cbo送检医生.Items.AddRange(new object[] {
            ""});
            this.cbo送检医生.Location = new System.Drawing.Point(409, 23);
            this.cbo送检医生.Name = "cbo送检医生";
            this.cbo送检医生.Size = new System.Drawing.Size(146, 20);
            this.cbo送检医生.TabIndex = 2;
            this.cbo送检医生.ValueMember = "fperson_id";
            // 
            // cbo设备名称
            // 
            this.cbo设备名称.DisplayMember = "fname";
            this.cbo设备名称.FormattingEnabled = true;
            this.cbo设备名称.Items.AddRange(new object[] {
            ""});
            this.cbo设备名称.Location = new System.Drawing.Point(73, 49);
            this.cbo设备名称.Name = "cbo设备名称";
            this.cbo设备名称.Size = new System.Drawing.Size(97, 20);
            this.cbo设备名称.TabIndex = 3;
            this.cbo设备名称.ValueMember = "finstr_id";
            this.cbo设备名称.SelectedValueChanged += new System.EventHandler(this.cbo设备名称_SelectedValueChanged);
            // 
            // cbo病人类别
            // 
            this.cbo病人类别.DisplayMember = "fname";
            this.cbo病人类别.FormattingEnabled = true;
            this.cbo病人类别.Items.AddRange(new object[] {
            ""});
            this.cbo病人类别.Location = new System.Drawing.Point(408, 74);
            this.cbo病人类别.Name = "cbo病人类别";
            this.cbo病人类别.Size = new System.Drawing.Size(60, 20);
            this.cbo病人类别.TabIndex = 7;
            this.cbo病人类别.ValueMember = "fcode";
            // 
            // cbo送检科室
            // 
            this.cbo送检科室.DisplayMember = "fname";
            this.cbo送检科室.FormattingEnabled = true;
            this.cbo送检科室.Items.AddRange(new object[] {
            ""});
            this.cbo送检科室.Location = new System.Drawing.Point(73, 75);
            this.cbo送检科室.Name = "cbo送检科室";
            this.cbo送检科室.Size = new System.Drawing.Size(260, 20);
            this.cbo送检科室.TabIndex = 6;
            this.cbo送检科室.ValueMember = "fdept_id";
            // 
            // cbo检验项目
            // 
            this.cbo检验项目.DisplayMember = "fname";
            this.cbo检验项目.FormattingEnabled = true;
            this.cbo检验项目.Items.AddRange(new object[] {
            ""});
            this.cbo检验项目.Location = new System.Drawing.Point(236, 49);
            this.cbo检验项目.Name = "cbo检验项目";
            this.cbo检验项目.Size = new System.Drawing.Size(97, 20);
            this.cbo检验项目.TabIndex = 4;
            this.cbo检验项目.ValueMember = "fitem_id";
            // 
            // jyDateBegin
            // 
            this.jyDateBegin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.jyDateBegin.Location = new System.Drawing.Point(73, 22);
            this.jyDateBegin.Name = "jyDateBegin";
            this.jyDateBegin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.jyDateBegin.Size = new System.Drawing.Size(97, 21);
            this.jyDateBegin.TabIndex = 0;
            // 
            // jyDateEnd
            // 
            this.jyDateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.jyDateEnd.Location = new System.Drawing.Point(236, 22);
            this.jyDateEnd.Name = "jyDateEnd";
            this.jyDateEnd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.jyDateEnd.Size = new System.Drawing.Size(97, 21);
            this.jyDateEnd.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(191, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "--";
            // 
            // documentViewer1
            // 
            this.documentViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.documentViewer1.IsMetric = true;
            this.documentViewer1.Location = new System.Drawing.Point(0, 120);
            this.documentViewer1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.documentViewer1.LookAndFeel.UseWindowsXPTheme = true;
            this.documentViewer1.Name = "documentViewer1";
            this.documentViewer1.Size = new System.Drawing.Size(979, 315);
            this.documentViewer1.TabIndex = 152;
            // 
            // TJGzlInstrFormNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 435);
            this.Controls.Add(this.documentViewer1);
            this.Controls.Add(this.groupBoxif);
            this.Name = "TJGzlInstrFormNew";
            this.Text = "仪器工作量统计";
            this.Load += new System.EventHandler(this.TJGzlInstrFormNew_Load);
            this.groupBoxif.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel groupBoxif;
        private System.Windows.Forms.ComboBox cbo病人类别;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker jyDateEnd;
        private System.Windows.Forms.DateTimePicker jyDateBegin;
        private System.Windows.Forms.ComboBox cbo设备名称;
        private System.Windows.Forms.ComboBox cbo检验项目;
        private System.Windows.Forms.ComboBox cbo性别;
        private System.Windows.Forms.ComboBox cbo送检科室;
        private System.Windows.Forms.ComboBox cbo送检医生;
        private System.Windows.Forms.ComboBox cbo检验医生;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox1;
        private DevExpress.XtraPrinting.Preview.DocumentViewer documentViewer1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnExportPDF;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.SaveFileDialog saveFileDialogPdf;


    }
}