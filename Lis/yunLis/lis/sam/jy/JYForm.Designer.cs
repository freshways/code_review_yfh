﻿namespace yunLis.lis.sam.jy
{
    partial class JYForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label fjy_instrLabel;
            System.Windows.Forms.Label fsample_codeLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cmb核对医师 = new DevExpress.XtraEditors.LookUpEdit();
            this.cmb检验医师 = new DevExpress.XtraEditors.LookUpEdit();
            this.txt电子健康卡 = new System.Windows.Forms.TextBox();
            this.txt身份证号 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cmb年龄单位 = new System.Windows.Forms.ComboBox();
            this.cmb病人类型 = new System.Windows.Forms.ComboBox();
            this.cmb性别 = new System.Windows.Forms.ComboBox();
            this.fcheck_timelabel = new System.Windows.Forms.Label();
            this.label状态 = new System.Windows.Forms.Label();
            this.txt临床诊断 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label最后修改 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label样本打印状态 = new System.Windows.Forms.Label();
            this.label样本审核状态 = new System.Windows.Forms.Label();
            this.label样本状态 = new System.Windows.Forms.Label();
            this.txt备注或档案号 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dtp报告时间 = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dtp采样时间 = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.dtp送检时间 = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmb样本类型 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt申请号 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt床号 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmb科室 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmb费别 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt年龄 = new System.Windows.Forms.TextBox();
            this.txt姓名 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt门诊或住院号 = new System.Windows.Forms.TextBox();
            this.cmb送检医师 = new DevExpress.XtraEditors.LookUpEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt样本号 = new System.Windows.Forms.TextBox();
            this.dtp样本日期 = new System.Windows.Forms.DateTimePicker();
            this.cmb检验仪器 = new System.Windows.Forms.ComboBox();
            this.dataGrid_SAM_JY_Result = new System.Windows.Forms.DataGridView();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_badge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_ref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_jgref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fresult_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.解除审核ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gridControl样本 = new DevExpress.XtraGrid.GridControl();
            this.gridView样本 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dataGridView样本 = new System.Windows.Forms.DataGridView();
            this.fjy_yb_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_zyh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_bed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.性别 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.年龄单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.科室 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.样本 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.检验医师 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fprint_zt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_zt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fapply_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSource样本 = new System.Windows.Forms.BindingSource(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.comboBox过滤样本_病人类别 = new System.Windows.Forms.ComboBox();
            this.button删除样本 = new System.Windows.Forms.Button();
            this.button刷新样本 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridControl比较 = new DevExpress.XtraGrid.GridControl();
            this.gridView比较 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dataGridView比较 = new System.Windows.Forms.DataGridView();
            this.项目 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.最近 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上一次 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上二次 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.上三次 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.项目id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button比较 = new System.Windows.Forms.Button();
            this.label21比较住院号 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.button读卡 = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.button刷新结果 = new System.Windows.Forms.Button();
            this.button仪器数据 = new System.Windows.Forms.Button();
            this.button删除 = new System.Windows.Forms.Button();
            this.button打印 = new System.Windows.Forms.Button();
            this.button下一个 = new System.Windows.Forms.Button();
            this.button上一个 = new System.Windows.Forms.Button();
            this.button审核 = new System.Windows.Forms.Button();
            this.button新增 = new System.Windows.Forms.Button();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl化验结果 = new DevExpress.XtraGrid.GridControl();
            this.gridView化验结果 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.col比较_项目 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col比较_最近 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col比较_上一次 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col比较_上二次 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col比较_上三次 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col比较_项目id = new DevExpress.XtraGrid.Columns.GridColumn();
            fjy_dateLabel = new System.Windows.Forms.Label();
            fjy_instrLabel = new System.Windows.Forms.Label();
            fsample_codeLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmb核对医师.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb检验医师.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb送检医师.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_SAM_JY_Result)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl样本)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView样本)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView样本)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource样本)).BeginInit();
            this.panel5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl比较)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView比较)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView比较)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl化验结果)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView化验结果)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            this.SuspendLayout();
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(6, 37);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(55, 14);
            fjy_dateLabel.TabIndex = 6;
            fjy_dateLabel.Text = "样本日期";
            // 
            // fjy_instrLabel
            // 
            fjy_instrLabel.AutoSize = true;
            fjy_instrLabel.Location = new System.Drawing.Point(6, 8);
            fjy_instrLabel.Name = "fjy_instrLabel";
            fjy_instrLabel.Size = new System.Drawing.Size(55, 14);
            fjy_instrLabel.TabIndex = 4;
            fjy_instrLabel.Text = "检验仪器";
            // 
            // fsample_codeLabel
            // 
            fsample_codeLabel.AutoSize = true;
            fsample_codeLabel.Location = new System.Drawing.Point(21, 65);
            fsample_codeLabel.Name = "fsample_codeLabel";
            fsample_codeLabel.Size = new System.Drawing.Size(43, 14);
            fsample_codeLabel.TabIndex = 8;
            fsample_codeLabel.Text = "样本号";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(3, 34);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(55, 14);
            label1.TabIndex = 12;
            label1.Text = "病人类型";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("宋体", 8F);
            label2.Location = new System.Drawing.Point(0, 62);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(66, 11);
            label2.TabIndex = 14;
            label2.Text = "门诊/住院号";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.cmb核对医师);
            this.panel3.Controls.Add(this.cmb检验医师);
            this.panel3.Controls.Add(this.txt电子健康卡);
            this.panel3.Controls.Add(this.txt身份证号);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.cmb年龄单位);
            this.panel3.Controls.Add(this.cmb病人类型);
            this.panel3.Controls.Add(this.cmb性别);
            this.panel3.Controls.Add(this.fcheck_timelabel);
            this.panel3.Controls.Add(label2);
            this.panel3.Controls.Add(this.label状态);
            this.panel3.Controls.Add(this.txt临床诊断);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.label最后修改);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.label样本打印状态);
            this.panel3.Controls.Add(this.label样本审核状态);
            this.panel3.Controls.Add(this.label样本状态);
            this.panel3.Controls.Add(this.txt备注或档案号);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.dtp报告时间);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.dtp采样时间);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.dtp送检时间);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.cmb样本类型);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.txt申请号);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.txt床号);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.cmb科室);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.cmb费别);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.txt年龄);
            this.panel3.Controls.Add(this.txt姓名);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txt门诊或住院号);
            this.panel3.Controls.Add(label1);
            this.panel3.Controls.Add(this.cmb送检医师);
            this.panel3.Location = new System.Drawing.Point(12, 102);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(268, 630);
            this.panel3.TabIndex = 1;
            // 
            // cmb核对医师
            // 
            this.cmb核对医师.Location = new System.Drawing.Point(79, 380);
            this.cmb核对医师.Name = "cmb核对医师";
            this.cmb核对医师.Properties.AutoHeight = false;
            this.cmb核对医师.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb核对医师.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户编码", "医生编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户名", "医生姓名")});
            this.cmb核对医师.Properties.DisplayMember = "用户名";
            this.cmb核对医师.Properties.NullText = "";
            this.cmb核对医师.Properties.ValueMember = "用户编码";
            this.cmb核对医师.Size = new System.Drawing.Size(170, 22);
            this.cmb核对医师.TabIndex = 162;
            // 
            // cmb检验医师
            // 
            this.cmb检验医师.Location = new System.Drawing.Point(79, 352);
            this.cmb检验医师.Name = "cmb检验医师";
            this.cmb检验医师.Properties.AutoHeight = false;
            this.cmb检验医师.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb检验医师.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户编码", "医生编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户名", "医生姓名")});
            this.cmb检验医师.Properties.DisplayMember = "用户名";
            this.cmb检验医师.Properties.NullText = "";
            this.cmb检验医师.Properties.ValueMember = "用户编码";
            this.cmb检验医师.Size = new System.Drawing.Size(170, 22);
            this.cmb检验医师.TabIndex = 161;
            // 
            // txt电子健康卡
            // 
            this.txt电子健康卡.Location = new System.Drawing.Point(80, 489);
            this.txt电子健康卡.Name = "txt电子健康卡";
            this.txt电子健康卡.Size = new System.Drawing.Size(171, 22);
            this.txt电子健康卡.TabIndex = 160;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(80, 461);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Size = new System.Drawing.Size(171, 22);
            this.txt身份证号.TabIndex = 159;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 492);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 14);
            this.label22.TabIndex = 158;
            this.label22.Text = "电子卡号";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 464);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 14);
            this.label21.TabIndex = 157;
            this.label21.Text = "身份证号";
            // 
            // cmb年龄单位
            // 
            this.cmb年龄单位.DisplayMember = "fname";
            this.cmb年龄单位.FormattingEnabled = true;
            this.cmb年龄单位.Location = new System.Drawing.Point(211, 108);
            this.cmb年龄单位.Name = "cmb年龄单位";
            this.cmb年龄单位.Size = new System.Drawing.Size(40, 22);
            this.cmb年龄单位.TabIndex = 22;
            this.cmb年龄单位.ValueMember = "fcode";
            this.cmb年龄单位.SelectedValueChanged += new System.EventHandler(this.comboBoxAgeUnit_SelectedValueChanged);
            this.cmb年龄单位.Enter += new System.EventHandler(this.comboBoxAgeUnit_Enter);
            this.cmb年龄单位.Leave += new System.EventHandler(this.comboBoxAgeUnit_Leave);
            // 
            // cmb病人类型
            // 
            this.cmb病人类型.DisplayMember = "fname";
            this.cmb病人类型.FormattingEnabled = true;
            this.cmb病人类型.Location = new System.Drawing.Point(80, 28);
            this.cmb病人类型.Name = "cmb病人类型";
            this.cmb病人类型.Size = new System.Drawing.Size(171, 22);
            this.cmb病人类型.TabIndex = 11;
            this.cmb病人类型.ValueMember = "fcode";
            this.cmb病人类型.SelectedValueChanged += new System.EventHandler(this.cboPatientType_SelectedValueChanged);
            this.cmb病人类型.Enter += new System.EventHandler(this.cboPatientType_Enter);
            this.cmb病人类型.Leave += new System.EventHandler(this.cboPatientType_Leave);
            // 
            // cmb性别
            // 
            this.cmb性别.DisplayMember = "fname";
            this.cmb性别.FormattingEnabled = true;
            this.cmb性别.Location = new System.Drawing.Point(80, 108);
            this.cmb性别.Name = "cmb性别";
            this.cmb性别.Size = new System.Drawing.Size(40, 22);
            this.cmb性别.TabIndex = 20;
            this.cmb性别.ValueMember = "fcode";
            this.cmb性别.SelectedValueChanged += new System.EventHandler(this.fhz_sex性别Combo_SelectedValueChanged);
            this.cmb性别.Enter += new System.EventHandler(this.fhz_sex性别Combo_Enter);
            this.cmb性别.Leave += new System.EventHandler(this.fhz_sex性别Combo_Leave);
            // 
            // fcheck_timelabel
            // 
            this.fcheck_timelabel.AutoSize = true;
            this.fcheck_timelabel.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.fcheck_timelabel.Location = new System.Drawing.Point(80, 586);
            this.fcheck_timelabel.Name = "fcheck_timelabel";
            this.fcheck_timelabel.Size = new System.Drawing.Size(11, 14);
            this.fcheck_timelabel.TabIndex = 156;
            this.fcheck_timelabel.Text = " ";
            // 
            // label状态
            // 
            this.label状态.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label状态.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label状态.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Bold);
            this.label状态.ForeColor = System.Drawing.Color.Red;
            this.label状态.Location = new System.Drawing.Point(1, 79);
            this.label状态.Name = "label状态";
            this.label状态.Size = new System.Drawing.Size(26, 142);
            this.label状态.TabIndex = 155;
            this.label状态.Text = "当\r\n前\r\n样\r\n本\r\n不\r\n可\r\n修\r\n改";
            this.label状态.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label状态.Visible = false;
            // 
            // txt临床诊断
            // 
            this.txt临床诊断.Location = new System.Drawing.Point(80, 405);
            this.txt临床诊断.Name = "txt临床诊断";
            this.txt临床诊断.Size = new System.Drawing.Size(171, 22);
            this.txt临床诊断.TabIndex = 145;
            this.txt临床诊断.DoubleClick += new System.EventHandler(this.fjy_lczdtextBox_DoubleClick);
            this.txt临床诊断.Enter += new System.EventHandler(this.fjy_lczdtextBox_Enter);
            this.txt临床诊断.Leave += new System.EventHandler(this.fjy_lczdtextBox_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 411);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 14);
            this.label20.TabIndex = 153;
            this.label20.Text = "临床诊断";
            // 
            // label最后修改
            // 
            this.label最后修改.AutoSize = true;
            this.label最后修改.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.label最后修改.Location = new System.Drawing.Point(80, 555);
            this.label最后修改.Name = "label最后修改";
            this.label最后修改.Size = new System.Drawing.Size(11, 14);
            this.label最后修改.TabIndex = 151;
            this.label最后修改.Text = " ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 555);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 14);
            this.label19.TabIndex = 150;
            this.label19.Text = "最后修改";
            // 
            // label样本打印状态
            // 
            this.label样本打印状态.AutoSize = true;
            this.label样本打印状态.ForeColor = System.Drawing.Color.Red;
            this.label样本打印状态.Location = new System.Drawing.Point(145, 528);
            this.label样本打印状态.Name = "label样本打印状态";
            this.label样本打印状态.Size = new System.Drawing.Size(43, 14);
            this.label样本打印状态.TabIndex = 149;
            this.label样本打印状态.Text = "未打印";
            // 
            // label样本审核状态
            // 
            this.label样本审核状态.AutoSize = true;
            this.label样本审核状态.ForeColor = System.Drawing.Color.Blue;
            this.label样本审核状态.Location = new System.Drawing.Point(80, 528);
            this.label样本审核状态.Name = "label样本审核状态";
            this.label样本审核状态.Size = new System.Drawing.Size(43, 14);
            this.label样本审核状态.TabIndex = 148;
            this.label样本审核状态.Text = "未审核";
            // 
            // label样本状态
            // 
            this.label样本状态.AutoSize = true;
            this.label样本状态.Location = new System.Drawing.Point(3, 528);
            this.label样本状态.Name = "label样本状态";
            this.label样本状态.Size = new System.Drawing.Size(55, 14);
            this.label样本状态.TabIndex = 147;
            this.label样本状态.Text = "样本状态";
            // 
            // txt备注或档案号
            // 
            this.txt备注或档案号.Location = new System.Drawing.Point(80, 433);
            this.txt备注或档案号.Name = "txt备注或档案号";
            this.txt备注或档案号.Size = new System.Drawing.Size(171, 22);
            this.txt备注或档案号.TabIndex = 146;
            this.txt备注或档案号.Enter += new System.EventHandler(this.fremarktextBox_Enter);
            this.txt备注或档案号.Leave += new System.EventHandler(this.fremarktextBox_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 8F);
            this.label17.Location = new System.Drawing.Point(0, 439);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 11);
            this.label17.TabIndex = 145;
            this.label17.Text = "备注(档案号)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 384);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 14);
            this.label16.TabIndex = 143;
            this.label16.Text = "核对者";
            // 
            // dtp报告时间
            // 
            this.dtp报告时间.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtp报告时间.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp报告时间.Location = new System.Drawing.Point(80, 324);
            this.dtp报告时间.Name = "dtp报告时间";
            this.dtp报告时间.ShowUpDown = true;
            this.dtp报告时间.Size = new System.Drawing.Size(171, 22);
            this.dtp报告时间.TabIndex = 140;
            this.dtp报告时间.Enter += new System.EventHandler(this.freport_timedateTimePicker_Enter);
            this.dtp报告时间.Leave += new System.EventHandler(this.freport_timedateTimePicker_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 330);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 14);
            this.label15.TabIndex = 141;
            this.label15.Text = "报告时间";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 357);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 14);
            this.label14.TabIndex = 139;
            this.label14.Text = "检验者";
            // 
            // dtp采样时间
            // 
            this.dtp采样时间.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtp采样时间.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp采样时间.Location = new System.Drawing.Point(80, 297);
            this.dtp采样时间.Name = "dtp采样时间";
            this.dtp采样时间.ShowUpDown = true;
            this.dtp采样时间.Size = new System.Drawing.Size(171, 22);
            this.dtp采样时间.TabIndex = 138;
            this.dtp采样时间.Enter += new System.EventHandler(this.fsampling_timedateTimePicker_Enter);
            this.dtp采样时间.Leave += new System.EventHandler(this.fsampling_timedateTimePicker_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 303);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 14);
            this.label13.TabIndex = 137;
            this.label13.Text = "采样时间";
            // 
            // dtp送检时间
            // 
            this.dtp送检时间.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtp送检时间.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp送检时间.Location = new System.Drawing.Point(80, 271);
            this.dtp送检时间.Name = "dtp送检时间";
            this.dtp送检时间.ShowUpDown = true;
            this.dtp送检时间.Size = new System.Drawing.Size(171, 22);
            this.dtp送检时间.TabIndex = 136;
            this.dtp送检时间.Enter += new System.EventHandler(this.fapply_timeDateTimePicker_Enter);
            this.dtp送检时间.Leave += new System.EventHandler(this.fapply_timeDateTimePicker_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 276);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 14);
            this.label12.TabIndex = 35;
            this.label12.Text = "送检时间";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 250);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 14);
            this.label9.TabIndex = 33;
            this.label9.Text = "送检医师";
            // 
            // cmb样本类型
            // 
            this.cmb样本类型.DisplayMember = "fname";
            this.cmb样本类型.Location = new System.Drawing.Point(80, 217);
            this.cmb样本类型.Name = "cmb样本类型";
            this.cmb样本类型.Size = new System.Drawing.Size(171, 22);
            this.cmb样本类型.TabIndex = 32;
            this.cmb样本类型.ValueMember = "fsample_type_id";
            this.cmb样本类型.DoubleClick += new System.EventHandler(this.fjy_yb_typetextBox_DoubleClick);
            this.cmb样本类型.SelectedValueChanged += new System.EventHandler(this.cmb样本类型_SelectedValueChanged);
            this.cmb样本类型.Enter += new System.EventHandler(this.fjy_yb_typetextBox_Enter);
            this.cmb样本类型.Leave += new System.EventHandler(this.fjy_yb_typetextBox_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 223);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 14);
            this.label10.TabIndex = 31;
            this.label10.Text = "样本类型";
            // 
            // txt申请号
            // 
            this.txt申请号.Location = new System.Drawing.Point(80, 1);
            this.txt申请号.Name = "txt申请号";
            this.txt申请号.Size = new System.Drawing.Size(171, 22);
            this.txt申请号.TabIndex = 10;
            this.txt申请号.TextChanged += new System.EventHandler(this.txt申请号_TextChanged);
            this.txt申请号.Enter += new System.EventHandler(this.txt申请号_Enter);
            this.txt申请号.Leave += new System.EventHandler(this.fapply_idtextBox_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 14);
            this.label11.TabIndex = 29;
            this.label11.Text = "申请号";
            // 
            // txt床号
            // 
            this.txt床号.Location = new System.Drawing.Point(80, 189);
            this.txt床号.Name = "txt床号";
            this.txt床号.Size = new System.Drawing.Size(171, 22);
            this.txt床号.TabIndex = 28;
            this.txt床号.Enter += new System.EventHandler(this.fhz_bedtextBox_Enter);
            this.txt床号.Leave += new System.EventHandler(this.fhz_bedtextBox_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 14);
            this.label8.TabIndex = 27;
            this.label8.Text = "床号";
            // 
            // cmb科室
            // 
            this.cmb科室.DisplayMember = "科室名称";
            this.cmb科室.Location = new System.Drawing.Point(80, 162);
            this.cmb科室.Name = "cmb科室";
            this.cmb科室.Size = new System.Drawing.Size(171, 22);
            this.cmb科室.TabIndex = 26;
            this.cmb科室.ValueMember = "科室编码";
            this.cmb科室.DoubleClick += new System.EventHandler(this.fhz_depttextBox_DoubleClick);
            this.cmb科室.SelectedValueChanged += new System.EventHandler(this.fhz_dept科室cbo_SelectedValueChanged);
            this.cmb科室.TextChanged += new System.EventHandler(this.fhz_depttextBox_TextChanged);
            this.cmb科室.Click += new System.EventHandler(this.fhz_dept科室cbo_Click);
            this.cmb科室.Enter += new System.EventHandler(this.fhz_depttextBox_Enter);
            this.cmb科室.Leave += new System.EventHandler(this.fhz_depttextBox_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "科室";
            // 
            // cmb费别
            // 
            this.cmb费别.DisplayMember = "fname";
            this.cmb费别.Location = new System.Drawing.Point(80, 135);
            this.cmb费别.Name = "cmb费别";
            this.cmb费别.Size = new System.Drawing.Size(171, 22);
            this.cmb费别.TabIndex = 24;
            this.cmb费别.ValueMember = "fcode";
            this.cmb费别.DoubleClick += new System.EventHandler(this.fjy_sf_typetextBox_DoubleClick);
            this.cmb费别.SelectedValueChanged += new System.EventHandler(this.fjy_sf_typetextBoxCombox_SelectedValueChanged);
            this.cmb费别.Enter += new System.EventHandler(this.fjy_sf_typetextBox_Enter);
            this.cmb费别.Leave += new System.EventHandler(this.fjy_sf_typetextBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "费别";
            // 
            // txt年龄
            // 
            this.txt年龄.Location = new System.Drawing.Point(168, 108);
            this.txt年龄.Name = "txt年龄";
            this.txt年龄.Size = new System.Drawing.Size(40, 22);
            this.txt年龄.TabIndex = 21;
            this.txt年龄.TextChanged += new System.EventHandler(this.fhz_age年龄_TextChanged);
            this.txt年龄.Enter += new System.EventHandler(this.fhz_agetextBox_Enter);
            this.txt年龄.Leave += new System.EventHandler(this.fhz_agetextBox_Leave);
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(80, 82);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(171, 22);
            this.txt姓名.TabIndex = 19;
            this.txt姓名.Enter += new System.EventHandler(this.fhz_nametextBox_Enter);
            this.txt姓名.Leave += new System.EventHandler(this.fhz_nametextBox_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(125, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "年龄";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "性别";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "姓名";
            // 
            // txt门诊或住院号
            // 
            this.txt门诊或住院号.Location = new System.Drawing.Point(80, 55);
            this.txt门诊或住院号.Name = "txt门诊或住院号";
            this.txt门诊或住院号.Size = new System.Drawing.Size(171, 22);
            this.txt门诊或住院号.TabIndex = 13;
            this.txt门诊或住院号.TextChanged += new System.EventHandler(this.fhz_zyh住院号_TextChanged);
            this.txt门诊或住院号.Enter += new System.EventHandler(this.fhz_zyhtextBox_Enter);
            this.txt门诊或住院号.Leave += new System.EventHandler(this.fhz_zyhtextBox_Leave);
            // 
            // cmb送检医师
            // 
            this.cmb送检医师.Location = new System.Drawing.Point(80, 244);
            this.cmb送检医师.Name = "cmb送检医师";
            this.cmb送检医师.Properties.AutoHeight = false;
            this.cmb送检医师.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb送检医师.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户编码", "医生编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("用户名", "医生姓名")});
            this.cmb送检医师.Properties.DisplayMember = "用户名";
            this.cmb送检医师.Properties.NullText = "";
            this.cmb送检医师.Properties.ValueMember = "用户编码";
            this.cmb送检医师.Size = new System.Drawing.Size(170, 22);
            this.cmb送检医师.TabIndex = 34;
            this.cmb送检医师.Click += new System.EventHandler(this.fapply_user_id送检医师cbo_Click);
            this.cmb送检医师.DoubleClick += new System.EventHandler(this.fapply_user_idtextBox_DoubleClick);
            this.cmb送检医师.Enter += new System.EventHandler(this.fapply_user_idtextBox_Enter);
            this.cmb送检医师.Leave += new System.EventHandler(this.fapply_user_idtextBox_Leave);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(fsample_codeLabel);
            this.panel2.Controls.Add(this.txt样本号);
            this.panel2.Controls.Add(fjy_dateLabel);
            this.panel2.Controls.Add(this.dtp样本日期);
            this.panel2.Controls.Add(fjy_instrLabel);
            this.panel2.Controls.Add(this.cmb检验仪器);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(268, 86);
            this.panel2.TabIndex = 0;
            // 
            // txt样本号
            // 
            this.txt样本号.Location = new System.Drawing.Point(80, 59);
            this.txt样本号.Name = "txt样本号";
            this.txt样本号.Size = new System.Drawing.Size(171, 22);
            this.txt样本号.TabIndex = 9;
            this.txt样本号.Text = "1";
            this.txt样本号.Enter += new System.EventHandler(this.fjy_yb_code样本_Enter);
            this.txt样本号.Leave += new System.EventHandler(this.fjy_yb_codeTextBox_Leave);
            // 
            // dtp样本日期
            // 
            this.dtp样本日期.Location = new System.Drawing.Point(80, 31);
            this.dtp样本日期.Name = "dtp样本日期";
            this.dtp样本日期.Size = new System.Drawing.Size(171, 22);
            this.dtp样本日期.TabIndex = 7;
            this.dtp样本日期.ValueChanged += new System.EventHandler(this.fjy_dateDateTimePicker_ValueChanged);
            this.dtp样本日期.Enter += new System.EventHandler(this.fjy_date样本日期_Enter);
            this.dtp样本日期.Leave += new System.EventHandler(this.fjy_date样本日期_Leave);
            // 
            // cmb检验仪器
            // 
            this.cmb检验仪器.DisplayMember = "fname";
            this.cmb检验仪器.FormattingEnabled = true;
            this.cmb检验仪器.Location = new System.Drawing.Point(80, 3);
            this.cmb检验仪器.Name = "cmb检验仪器";
            this.cmb检验仪器.Size = new System.Drawing.Size(171, 22);
            this.cmb检验仪器.TabIndex = 5;
            this.cmb检验仪器.ValueMember = "finstr_id";
            this.cmb检验仪器.SelectedIndexChanged += new System.EventHandler(this.cmb检验仪器_SelectedIndexChanged);
            // 
            // dataGrid_SAM_JY_Result
            // 
            this.dataGrid_SAM_JY_Result.AllowUserToAddRows = false;
            this.dataGrid_SAM_JY_Result.AllowUserToResizeRows = false;
            this.dataGrid_SAM_JY_Result.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGrid_SAM_JY_Result.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGrid_SAM_JY_Result.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGrid_SAM_JY_Result.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code,
            this.fitem_name,
            this.fvalue,
            this.fod,
            this.fcutoff,
            this.fitem_badge,
            this.fitem_ref,
            this.fitem_jgref,
            this.fitem_unit,
            this.forder_by,
            this.fitem_id,
            this.fresult_id,
            this.fremark});
            this.dataGrid_SAM_JY_Result.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGrid_SAM_JY_Result.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGrid_SAM_JY_Result.Location = new System.Drawing.Point(289, 12);
            this.dataGrid_SAM_JY_Result.MultiSelect = false;
            this.dataGrid_SAM_JY_Result.Name = "dataGrid_SAM_JY_Result";
            this.dataGrid_SAM_JY_Result.RowHeadersVisible = false;
            this.dataGrid_SAM_JY_Result.RowHeadersWidth = 30;
            this.dataGrid_SAM_JY_Result.RowTemplate.Height = 23;
            this.dataGrid_SAM_JY_Result.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGrid_SAM_JY_Result.ShowCellToolTips = false;
            this.dataGrid_SAM_JY_Result.ShowEditingIcon = false;
            this.dataGrid_SAM_JY_Result.Size = new System.Drawing.Size(370, 358);
            this.dataGrid_SAM_JY_Result.TabIndex = 132;
            this.dataGrid_SAM_JY_Result.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewResult_CellDoubleClick);
            this.dataGrid_SAM_JY_Result.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewResult_CellMouseDoubleClick);
            this.dataGrid_SAM_JY_Result.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewResult_CellValueChanged);
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_code.DefaultCellStyle = dataGridViewCellStyle10;
            this.fitem_code.HeaderText = "编码";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Width = 60;
            // 
            // fitem_name
            // 
            this.fitem_name.DataPropertyName = "fitem_name";
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_name.DefaultCellStyle = dataGridViewCellStyle11;
            this.fitem_name.HeaderText = "检验项目";
            this.fitem_name.Name = "fitem_name";
            this.fitem_name.ReadOnly = true;
            this.fitem_name.Width = 120;
            // 
            // fvalue
            // 
            this.fvalue.DataPropertyName = "fvalue";
            this.fvalue.HeaderText = "结果";
            this.fvalue.Name = "fvalue";
            this.fvalue.Width = 70;
            // 
            // fod
            // 
            this.fod.DataPropertyName = "fod";
            this.fod.HeaderText = "OD值";
            this.fod.Name = "fod";
            this.fod.Visible = false;
            this.fod.Width = 65;
            // 
            // fcutoff
            // 
            this.fcutoff.DataPropertyName = "fcutoff";
            this.fcutoff.HeaderText = "Cutoff";
            this.fcutoff.Name = "fcutoff";
            this.fcutoff.Visible = false;
            this.fcutoff.Width = 70;
            // 
            // fitem_badge
            // 
            this.fitem_badge.DataPropertyName = "fitem_badge";
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_badge.DefaultCellStyle = dataGridViewCellStyle12;
            this.fitem_badge.HeaderText = ".";
            this.fitem_badge.Name = "fitem_badge";
            this.fitem_badge.ReadOnly = true;
            this.fitem_badge.Width = 25;
            // 
            // fitem_ref
            // 
            this.fitem_ref.DataPropertyName = "fitem_ref";
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_ref.DefaultCellStyle = dataGridViewCellStyle13;
            this.fitem_ref.HeaderText = "参考值";
            this.fitem_ref.Name = "fitem_ref";
            this.fitem_ref.ReadOnly = true;
            this.fitem_ref.Width = 80;
            // 
            // fitem_jgref
            // 
            this.fitem_jgref.DataPropertyName = "fitem_jgref";
            this.fitem_jgref.HeaderText = "危急值";
            this.fitem_jgref.Name = "fitem_jgref";
            this.fitem_jgref.ReadOnly = true;
            this.fitem_jgref.Width = 80;
            // 
            // fitem_unit
            // 
            this.fitem_unit.DataPropertyName = "fitem_unit";
            this.fitem_unit.HeaderText = "项目单位";
            this.fitem_unit.Name = "fitem_unit";
            this.fitem_unit.ReadOnly = true;
            this.fitem_unit.Visible = false;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "序号";
            this.forder_by.Name = "forder_by";
            this.forder_by.Visible = false;
            this.forder_by.Width = 60;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "项目id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.ReadOnly = true;
            this.fitem_id.Visible = false;
            // 
            // fresult_id
            // 
            this.fresult_id.DataPropertyName = "fresult_id";
            this.fresult_id.HeaderText = "fresult_id";
            this.fresult_id.Name = "fresult_id";
            this.fresult_id.ReadOnly = true;
            this.fresult_id.Visible = false;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "修改标识";
            this.fremark.Name = "fremark";
            this.fremark.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.打印ToolStripMenuItem,
            this.toolStripSeparator1,
            this.解除审核ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 76);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem1.Text = "打印预览";
            this.toolStripMenuItem1.Visible = false;
            // 
            // 打印ToolStripMenuItem
            // 
            this.打印ToolStripMenuItem.Name = "打印ToolStripMenuItem";
            this.打印ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.打印ToolStripMenuItem.Text = "打印";
            this.打印ToolStripMenuItem.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(121, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // 解除审核ToolStripMenuItem
            // 
            this.解除审核ToolStripMenuItem.Name = "解除审核ToolStripMenuItem";
            this.解除审核ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.解除审核ToolStripMenuItem.Text = "解除审核";
            this.解除审核ToolStripMenuItem.Click += new System.EventHandler(this.解除审核ToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(668, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(478, 720);
            this.tabControl1.TabIndex = 135;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridControl样本);
            this.tabPage1.Controls.Add(this.dataGridView样本);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(470, 694);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "列表";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gridControl样本
            // 
            this.gridControl样本.Location = new System.Drawing.Point(8, 259);
            this.gridControl样本.MainView = this.gridView样本;
            this.gridControl样本.Name = "gridControl样本";
            this.gridControl样本.Size = new System.Drawing.Size(447, 411);
            this.gridControl样本.TabIndex = 134;
            this.gridControl样本.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView样本});
            // 
            // gridView样本
            // 
            this.gridView样本.GridControl = this.gridControl样本;
            this.gridView样本.Name = "gridView样本";
            // 
            // dataGridView样本
            // 
            this.dataGridView样本.AllowUserToAddRows = false;
            this.dataGridView样本.AllowUserToDeleteRows = false;
            this.dataGridView样本.AllowUserToResizeRows = false;
            this.dataGridView样本.AutoGenerateColumns = false;
            this.dataGridView样本.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView样本.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView样本.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView样本.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView样本.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fjy_yb_code,
            this.fhz_name,
            this.fhz_zyh,
            this.fhz_bed,
            this.性别,
            this.fhz_age,
            this.年龄单位,
            this.科室,
            this.样本,
            this.检验医师,
            this.fprint_zt,
            this.fjy_zt,
            this.类型,
            this.fapply_id,
            this.fjy_id});
            this.dataGridView样本.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView样本.DataSource = this.bindingSource样本;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView样本.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView样本.Location = new System.Drawing.Point(3, 43);
            this.dataGridView样本.MultiSelect = false;
            this.dataGridView样本.Name = "dataGridView样本";
            this.dataGridView样本.ReadOnly = true;
            this.dataGridView样本.RowHeadersVisible = false;
            this.dataGridView样本.RowHeadersWidth = 30;
            this.dataGridView样本.RowTemplate.Height = 23;
            this.dataGridView样本.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView样本.ShowCellToolTips = false;
            this.dataGridView样本.ShowEditingIcon = false;
            this.dataGridView样本.Size = new System.Drawing.Size(471, 184);
            this.dataGridView样本.TabIndex = 133;
            this.dataGridView样本.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView样本_DataBindingComplete);
            this.dataGridView样本.Click += new System.EventHandler(this.dataGridView样本_Click);
            this.dataGridView样本.DoubleClick += new System.EventHandler(this.dataGridView样本_DoubleClick);
            // 
            // fjy_yb_code
            // 
            this.fjy_yb_code.DataPropertyName = "fjy_yb_code";
            this.fjy_yb_code.HeaderText = "样本号";
            this.fjy_yb_code.Name = "fjy_yb_code";
            this.fjy_yb_code.ReadOnly = true;
            this.fjy_yb_code.Width = 64;
            // 
            // fhz_name
            // 
            this.fhz_name.DataPropertyName = "fhz_name";
            this.fhz_name.HeaderText = "姓名";
            this.fhz_name.Name = "fhz_name";
            this.fhz_name.ReadOnly = true;
            this.fhz_name.Width = 55;
            // 
            // fhz_zyh
            // 
            this.fhz_zyh.DataPropertyName = "fhz_zyh";
            this.fhz_zyh.HeaderText = "住院号";
            this.fhz_zyh.Name = "fhz_zyh";
            this.fhz_zyh.ReadOnly = true;
            this.fhz_zyh.Width = 65;
            // 
            // fhz_bed
            // 
            this.fhz_bed.DataPropertyName = "fhz_bed";
            this.fhz_bed.HeaderText = "床号";
            this.fhz_bed.Name = "fhz_bed";
            this.fhz_bed.ReadOnly = true;
            this.fhz_bed.Width = 55;
            // 
            // 性别
            // 
            this.性别.DataPropertyName = "性别";
            this.性别.HeaderText = "性别";
            this.性别.Name = "性别";
            this.性别.ReadOnly = true;
            this.性别.Width = 55;
            // 
            // fhz_age
            // 
            this.fhz_age.DataPropertyName = "fhz_age";
            this.fhz_age.HeaderText = "年";
            this.fhz_age.Name = "fhz_age";
            this.fhz_age.ReadOnly = true;
            this.fhz_age.Width = 30;
            // 
            // 年龄单位
            // 
            this.年龄单位.DataPropertyName = "年龄单位";
            this.年龄单位.HeaderText = "龄";
            this.年龄单位.Name = "年龄单位";
            this.年龄单位.ReadOnly = true;
            this.年龄单位.Width = 30;
            // 
            // 科室
            // 
            this.科室.DataPropertyName = "科室";
            this.科室.HeaderText = "科室";
            this.科室.Name = "科室";
            this.科室.ReadOnly = true;
            this.科室.Width = 55;
            // 
            // 样本
            // 
            this.样本.DataPropertyName = "样本";
            this.样本.HeaderText = "样本";
            this.样本.Name = "样本";
            this.样本.ReadOnly = true;
            this.样本.Width = 55;
            // 
            // 检验医师
            // 
            this.检验医师.DataPropertyName = "检验医师";
            this.检验医师.HeaderText = "检验";
            this.检验医师.Name = "检验医师";
            this.检验医师.ReadOnly = true;
            this.检验医师.Width = 55;
            // 
            // fprint_zt
            // 
            this.fprint_zt.DataPropertyName = "fprint_zt";
            this.fprint_zt.HeaderText = "打印";
            this.fprint_zt.Name = "fprint_zt";
            this.fprint_zt.ReadOnly = true;
            this.fprint_zt.Width = 55;
            // 
            // fjy_zt
            // 
            this.fjy_zt.DataPropertyName = "fjy_zt";
            this.fjy_zt.HeaderText = "审核";
            this.fjy_zt.Name = "fjy_zt";
            this.fjy_zt.ReadOnly = true;
            this.fjy_zt.Width = 55;
            // 
            // 类型
            // 
            this.类型.DataPropertyName = "类型";
            this.类型.HeaderText = "类型";
            this.类型.Name = "类型";
            this.类型.ReadOnly = true;
            this.类型.Width = 55;
            // 
            // fapply_id
            // 
            this.fapply_id.DataPropertyName = "fapply_id";
            this.fapply_id.HeaderText = "申请号";
            this.fapply_id.Name = "fapply_id";
            this.fapply_id.ReadOnly = true;
            this.fapply_id.Width = 65;
            // 
            // fjy_id
            // 
            this.fjy_id.DataPropertyName = "fjy_id";
            this.fjy_id.HeaderText = "fjy_id";
            this.fjy_id.Name = "fjy_id";
            this.fjy_id.ReadOnly = true;
            this.fjy_id.Visible = false;
            // 
            // bindingSource样本
            // 
            this.bindingSource样本.PositionChanged += new System.EventHandler(this.bindingSource样本_PositionChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.comboBox过滤样本_病人类别);
            this.panel5.Controls.Add(this.button删除样本);
            this.panel5.Controls.Add(this.button刷新样本);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(464, 40);
            this.panel5.TabIndex = 0;
            // 
            // comboBox过滤样本_病人类别
            // 
            this.comboBox过滤样本_病人类别.DisplayMember = "fname";
            this.comboBox过滤样本_病人类别.Font = new System.Drawing.Font("宋体", 10.5F);
            this.comboBox过滤样本_病人类别.FormattingEnabled = true;
            this.comboBox过滤样本_病人类别.Items.AddRange(new object[] {
            "所有",
            "门诊",
            "住院",
            "急诊",
            "未打印",
            "已打印",
            "未审核",
            "已审核",
            "未打印未审核",
            "未打印已审核",
            "已打印未审核",
            "已打印已审核"});
            this.comboBox过滤样本_病人类别.Location = new System.Drawing.Point(194, 5);
            this.comboBox过滤样本_病人类别.Name = "comboBox过滤样本_病人类别";
            this.comboBox过滤样本_病人类别.Size = new System.Drawing.Size(156, 22);
            this.comboBox过滤样本_病人类别.TabIndex = 6;
            this.comboBox过滤样本_病人类别.ValueMember = "finstr_id";
            this.comboBox过滤样本_病人类别.DropDownClosed += new System.EventHandler(this.comboBox过滤样本_DropDownClosed);
            // 
            // button删除样本
            // 
            this.button删除样本.Location = new System.Drawing.Point(99, 5);
            this.button删除样本.Name = "button删除样本";
            this.button删除样本.Size = new System.Drawing.Size(87, 27);
            this.button删除样本.TabIndex = 1;
            this.button删除样本.Text = "删除(&&D)";
            this.button删除样本.UseVisualStyleBackColor = true;
            this.button删除样本.Click += new System.EventHandler(this.button删除样本_Click);
            // 
            // button刷新样本
            // 
            this.button刷新样本.Location = new System.Drawing.Point(5, 5);
            this.button刷新样本.Name = "button刷新样本";
            this.button刷新样本.Size = new System.Drawing.Size(87, 27);
            this.button刷新样本.TabIndex = 0;
            this.button刷新样本.Text = "刷新(&R)";
            this.button刷新样本.UseVisualStyleBackColor = true;
            this.button刷新样本.Click += new System.EventHandler(this.button刷新样本_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridControl比较);
            this.tabPage2.Controls.Add(this.dataGridView比较);
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(470, 694);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "比较";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridControl比较
            // 
            this.gridControl比较.Location = new System.Drawing.Point(8, 470);
            this.gridControl比较.MainView = this.gridView比较;
            this.gridControl比较.Name = "gridControl比较";
            this.gridControl比较.Size = new System.Drawing.Size(400, 200);
            this.gridControl比较.TabIndex = 136;
            this.gridControl比较.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView比较});
            // 
            // gridView比较
            // 
            this.gridView比较.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col比较_项目,
            this.col比较_最近,
            this.col比较_上一次,
            this.col比较_上二次,
            this.col比较_上三次,
            this.col比较_项目id});
            this.gridView比较.GridControl = this.gridControl比较;
            this.gridView比较.Name = "gridView比较";
            // 
            // dataGridView比较
            // 
            this.dataGridView比较.AllowUserToAddRows = false;
            this.dataGridView比较.AllowUserToResizeRows = false;
            this.dataGridView比较.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView比较.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView比较.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridView比较.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView比较.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.项目,
            this.最近,
            this.上一次,
            this.上二次,
            this.上三次,
            this.项目id});
            this.dataGridView比较.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView比较.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView比较.Location = new System.Drawing.Point(3, 43);
            this.dataGridView比较.MultiSelect = false;
            this.dataGridView比较.Name = "dataGridView比较";
            this.dataGridView比较.ReadOnly = true;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView比较.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridView比较.RowHeadersVisible = false;
            this.dataGridView比较.RowHeadersWidth = 30;
            this.dataGridView比较.RowTemplate.Height = 23;
            this.dataGridView比较.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView比较.ShowCellToolTips = false;
            this.dataGridView比较.ShowEditingIcon = false;
            this.dataGridView比较.Size = new System.Drawing.Size(591, 275);
            this.dataGridView比较.TabIndex = 134;
            this.dataGridView比较.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView比较.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // 项目
            // 
            this.项目.DataPropertyName = "项目";
            this.项目.HeaderText = "项目";
            this.项目.Name = "项目";
            this.项目.ReadOnly = true;
            this.项目.Width = 80;
            // 
            // 最近
            // 
            this.最近.DataPropertyName = "项目值";
            this.最近.HeaderText = "最近";
            this.最近.Name = "最近";
            this.最近.ReadOnly = true;
            // 
            // 上一次
            // 
            this.上一次.DataPropertyName = "上一次";
            this.上一次.HeaderText = "上一次";
            this.上一次.Name = "上一次";
            this.上一次.ReadOnly = true;
            // 
            // 上二次
            // 
            this.上二次.DataPropertyName = "上二次";
            this.上二次.HeaderText = "上二次";
            this.上二次.Name = "上二次";
            this.上二次.ReadOnly = true;
            // 
            // 上三次
            // 
            this.上三次.DataPropertyName = "上三次";
            this.上三次.HeaderText = "上三次";
            this.上三次.Name = "上三次";
            this.上三次.ReadOnly = true;
            // 
            // 项目id
            // 
            this.项目id.DataPropertyName = "项目id";
            this.项目id.HeaderText = "项目id";
            this.项目id.Name = "项目id";
            this.项目id.ReadOnly = true;
            this.项目id.Visible = false;
            this.项目id.Width = 95;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.button比较);
            this.panel6.Controls.Add(this.label21比较住院号);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(464, 40);
            this.panel6.TabIndex = 135;
            // 
            // button比较
            // 
            this.button比较.Location = new System.Drawing.Point(5, 5);
            this.button比较.Name = "button比较";
            this.button比较.Size = new System.Drawing.Size(87, 27);
            this.button比较.TabIndex = 3;
            this.button比较.Text = "刷新(&R)";
            this.button比较.UseVisualStyleBackColor = true;
            this.button比较.Click += new System.EventHandler(this.button1_Click);
            // 
            // label21比较住院号
            // 
            this.label21比较住院号.AutoSize = true;
            this.label21比较住院号.Location = new System.Drawing.Point(117, 10);
            this.label21比较住院号.Name = "label21比较住院号";
            this.label21比较住院号.Size = new System.Drawing.Size(53, 12);
            this.label21比较住院号.TabIndex = 2;
            this.label21比较住院号.Text = "住院号：";
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(1158, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 771);
            this.splitter2.TabIndex = 136;
            this.splitter2.TabStop = false;
            // 
            // button读卡
            // 
            this.button读卡.Image = global::yunLis.Properties.Resources.wwNavigatorQuery;
            this.button读卡.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button读卡.Location = new System.Drawing.Point(957, 734);
            this.button读卡.Name = "button读卡";
            this.button读卡.Size = new System.Drawing.Size(94, 27);
            this.button读卡.TabIndex = 10;
            this.button读卡.Text = " 读电子卡";
            this.button读卡.UseVisualStyleBackColor = true;
            this.button读卡.Visible = false;
            this.button读卡.Click += new System.EventHandler(this.button读卡_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::yunLis.Properties.Resources.button_save;
            this.btnSave.Location = new System.Drawing.Point(357, 734);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 27);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "保存";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button刷新结果
            // 
            this.button刷新结果.Image = global::yunLis.Properties.Resources.refresh;
            this.button刷新结果.Location = new System.Drawing.Point(857, 734);
            this.button刷新结果.Name = "button刷新结果";
            this.button刷新结果.Size = new System.Drawing.Size(94, 27);
            this.button刷新结果.TabIndex = 8;
            this.button刷新结果.Text = "刷新结果";
            this.button刷新结果.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button刷新结果.UseVisualStyleBackColor = true;
            this.button刷新结果.Click += new System.EventHandler(this.button刷新结果_Click);
            // 
            // button仪器数据
            // 
            this.button仪器数据.Image = global::yunLis.Properties.Resources.button_tj;
            this.button仪器数据.Location = new System.Drawing.Point(757, 734);
            this.button仪器数据.Name = "button仪器数据";
            this.button仪器数据.Size = new System.Drawing.Size(94, 27);
            this.button仪器数据.TabIndex = 7;
            this.button仪器数据.Text = "选择结果(F8)";
            this.button仪器数据.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button仪器数据.UseVisualStyleBackColor = true;
            this.button仪器数据.Click += new System.EventHandler(this.button仪器数据_Click);
            // 
            // button删除
            // 
            this.button删除.Image = global::yunLis.Properties.Resources.button_del;
            this.button删除.Location = new System.Drawing.Point(157, 734);
            this.button删除.Name = "button删除";
            this.button删除.Size = new System.Drawing.Size(94, 27);
            this.button删除.TabIndex = 6;
            this.button删除.Text = "删除(F7)";
            this.button删除.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button删除.UseVisualStyleBackColor = true;
            this.button删除.Click += new System.EventHandler(this.button删除_Click);
            // 
            // button打印
            // 
            this.button打印.Image = global::yunLis.Properties.Resources.button_print;
            this.button打印.Location = new System.Drawing.Point(657, 734);
            this.button打印.Name = "button打印";
            this.button打印.Size = new System.Drawing.Size(94, 27);
            this.button打印.TabIndex = 5;
            this.button打印.Text = "打印(F2)";
            this.button打印.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button打印.UseVisualStyleBackColor = true;
            this.button打印.Click += new System.EventHandler(this.button打印_Click);
            // 
            // button下一个
            // 
            this.button下一个.Image = global::yunLis.Properties.Resources.button_Next;
            this.button下一个.Location = new System.Drawing.Point(557, 734);
            this.button下一个.Name = "button下一个";
            this.button下一个.Size = new System.Drawing.Size(94, 27);
            this.button下一个.TabIndex = 4;
            this.button下一个.Text = "下一个(F5)";
            this.button下一个.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button下一个.UseVisualStyleBackColor = true;
            this.button下一个.Click += new System.EventHandler(this.button下一个_Click);
            // 
            // button上一个
            // 
            this.button上一个.Image = global::yunLis.Properties.Resources.button_Previous;
            this.button上一个.Location = new System.Drawing.Point(457, 734);
            this.button上一个.Name = "button上一个";
            this.button上一个.Size = new System.Drawing.Size(94, 27);
            this.button上一个.TabIndex = 3;
            this.button上一个.Text = "上一个(F4)";
            this.button上一个.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button上一个.UseVisualStyleBackColor = true;
            this.button上一个.Click += new System.EventHandler(this.button上一个_Click);
            // 
            // button审核
            // 
            this.button审核.Image = global::yunLis.Properties.Resources.button_save;
            this.button审核.Location = new System.Drawing.Point(257, 734);
            this.button审核.Name = "button审核";
            this.button审核.Size = new System.Drawing.Size(94, 27);
            this.button审核.TabIndex = 2;
            this.button审核.Text = "审核(F9)";
            this.button审核.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button审核.UseVisualStyleBackColor = true;
            this.button审核.Click += new System.EventHandler(this.button审核_Click);
            // 
            // button新增
            // 
            this.button新增.Image = global::yunLis.Properties.Resources.button_add;
            this.button新增.Location = new System.Drawing.Point(63, 734);
            this.button新增.Name = "button新增";
            this.button新增.Size = new System.Drawing.Size(88, 27);
            this.button新增.TabIndex = 0;
            this.button新增.Text = "新增(F6)";
            this.button新增.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button新增.UseVisualStyleBackColor = true;
            this.button新增.Click += new System.EventHandler(this.button新增_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridControl化验结果);
            this.layoutControl1.Controls.Add(this.button读卡);
            this.layoutControl1.Controls.Add(this.panel3);
            this.layoutControl1.Controls.Add(this.button刷新结果);
            this.layoutControl1.Controls.Add(this.btnSave);
            this.layoutControl1.Controls.Add(this.button仪器数据);
            this.layoutControl1.Controls.Add(this.button打印);
            this.layoutControl1.Controls.Add(this.tabControl1);
            this.layoutControl1.Controls.Add(this.button下一个);
            this.layoutControl1.Controls.Add(this.panel2);
            this.layoutControl1.Controls.Add(this.button上一个);
            this.layoutControl1.Controls.Add(this.button删除);
            this.layoutControl1.Controls.Add(this.dataGrid_SAM_JY_Result);
            this.layoutControl1.Controls.Add(this.button审核);
            this.layoutControl1.Controls.Add(this.button新增);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(798, 317, 650, 576);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1158, 771);
            this.layoutControl1.TabIndex = 140;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControl化验结果
            // 
            this.gridControl化验结果.Location = new System.Drawing.Point(289, 374);
            this.gridControl化验结果.MainView = this.gridView化验结果;
            this.gridControl化验结果.Name = "gridControl化验结果";
            this.gridControl化验结果.Size = new System.Drawing.Size(370, 358);
            this.gridControl化验结果.TabIndex = 136;
            this.gridControl化验结果.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView化验结果});
            // 
            // gridView化验结果
            // 
            this.gridView化验结果.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView化验结果.GridControl = this.gridControl化验结果;
            this.gridView化验结果.Name = "gridView化验结果";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "gridColumn3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "gridColumn4";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "gridColumn5";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "gridColumn6";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.splitterItem1,
            this.splitterItem2,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem15});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1158, 771);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panel2;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(272, 90);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(651, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 724);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.Location = new System.Drawing.Point(272, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(5, 724);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panel3;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(272, 634);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dataGrid_SAM_JY_Result;
            this.layoutControlItem3.Location = new System.Drawing.Point(277, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(374, 362);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.tabControl1;
            this.layoutControlItem4.Location = new System.Drawing.Point(656, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(482, 724);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.button新增;
            this.layoutControlItem5.Location = new System.Drawing.Point(50, 724);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem5.Size = new System.Drawing.Size(94, 27);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.button删除;
            this.layoutControlItem6.Location = new System.Drawing.Point(144, 724);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem6.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.button审核;
            this.layoutControlItem7.Location = new System.Drawing.Point(244, 724);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem7.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnSave;
            this.layoutControlItem8.Location = new System.Drawing.Point(344, 724);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem8.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.button上一个;
            this.layoutControlItem9.Location = new System.Drawing.Point(444, 724);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem9.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.button下一个;
            this.layoutControlItem10.Location = new System.Drawing.Point(544, 724);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem10.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.button打印;
            this.layoutControlItem11.Location = new System.Drawing.Point(644, 724);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem11.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.button仪器数据;
            this.layoutControlItem12.Location = new System.Drawing.Point(744, 724);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(100, 27);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(100, 27);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem12.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.button刷新结果;
            this.layoutControlItem13.Location = new System.Drawing.Point(844, 724);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem13.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.button读卡;
            this.layoutControlItem14.Location = new System.Drawing.Point(944, 724);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(26, 20);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem14.Size = new System.Drawing.Size(100, 27);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(3, 3, 0, 0);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 724);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(50, 27);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(1044, 724);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(94, 27);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.gridControl化验结果;
            this.layoutControlItem15.Location = new System.Drawing.Point(277, 362);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(374, 362);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // col比较_项目
            // 
            this.col比较_项目.Caption = "项目";
            this.col比较_项目.FieldName = "项目";
            this.col比较_项目.Name = "col比较_项目";
            this.col比较_项目.Visible = true;
            this.col比较_项目.VisibleIndex = 0;
            // 
            // col比较_最近
            // 
            this.col比较_最近.Caption = "最近";
            this.col比较_最近.FieldName = "项目值";
            this.col比较_最近.Name = "col比较_最近";
            this.col比较_最近.Visible = true;
            this.col比较_最近.VisibleIndex = 1;
            // 
            // col比较_上一次
            // 
            this.col比较_上一次.Caption = "上一次";
            this.col比较_上一次.FieldName = "上一次";
            this.col比较_上一次.Name = "col比较_上一次";
            this.col比较_上一次.Visible = true;
            this.col比较_上一次.VisibleIndex = 2;
            // 
            // col比较_上二次
            // 
            this.col比较_上二次.Caption = "上二次";
            this.col比较_上二次.FieldName = "上二次";
            this.col比较_上二次.Name = "col比较_上二次";
            this.col比较_上二次.Visible = true;
            this.col比较_上二次.VisibleIndex = 3;
            // 
            // col比较_上三次
            // 
            this.col比较_上三次.Caption = "上三次";
            this.col比较_上三次.FieldName = "上三次";
            this.col比较_上三次.Name = "col比较_上三次";
            this.col比较_上三次.Visible = true;
            this.col比较_上三次.VisibleIndex = 4;
            // 
            // col比较_项目id
            // 
            this.col比较_项目id.Caption = "项目id";
            this.col比较_项目id.FieldName = "项目id";
            this.col比较_项目id.Name = "col比较_项目id";
            this.col比较_项目id.Visible = true;
            this.col比较_项目id.VisibleIndex = 5;
            // 
            // JYForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 771);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.splitter2);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "JYForm";
            this.Text = "检验报告输入";
            this.Load += new System.EventHandler(this.JYForm_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmb核对医师.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb检验医师.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb送检医师.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_SAM_JY_Result)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl样本)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView样本)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView样本)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource样本)).EndInit();
            this.panel5.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl比较)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView比较)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView比较)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl化验结果)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView化验结果)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGrid_SAM_JY_Result;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.DateTimePicker dtp样本日期;
        private System.Windows.Forms.ComboBox cmb检验仪器;
        private System.Windows.Forms.TextBox txt样本号;
        private System.Windows.Forms.Button button新增;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox comboBox过滤样本_病人类别;
        private System.Windows.Forms.Button button删除样本;
        private System.Windows.Forms.Button button刷新样本;
        private System.Windows.Forms.DataGridView dataGridView样本;
        private System.Windows.Forms.Button button审核;
        private System.Windows.Forms.Button button上一个;
        private System.Windows.Forms.Button button下一个;
        private System.Windows.Forms.Button button删除;
        private System.Windows.Forms.BindingSource bindingSource样本;
        private System.Windows.Forms.TextBox txt门诊或住院号;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt年龄;
        private System.Windows.Forms.TextBox txt姓名;
        private System.Windows.Forms.ComboBox cmb科室;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmb费别;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmb样本类型;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt申请号;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt床号;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtp送检时间;
        private System.Windows.Forms.DateTimePicker dtp采样时间;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtp报告时间;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt备注或档案号;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label样本打印状态;
        private System.Windows.Forms.Label label样本审核状态;
        private System.Windows.Forms.Label label样本状态;
        private System.Windows.Forms.Label label最后修改;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt临床诊断;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_yb_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_zyh;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_bed;
        private System.Windows.Forms.DataGridViewTextBoxColumn 性别;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_age;
        private System.Windows.Forms.DataGridViewTextBoxColumn 年龄单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 科室;
        private System.Windows.Forms.DataGridViewTextBoxColumn 样本;
        private System.Windows.Forms.DataGridViewTextBoxColumn 检验医师;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprint_zt;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_zt;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn fapply_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_id;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 打印ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 解除审核ToolStripMenuItem;
        private System.Windows.Forms.Label fcheck_timelabel;
        private System.Windows.Forms.Button button仪器数据;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView比较;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label21比较住院号;
        private System.Windows.Forms.Button button比较;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Button button打印;
        private System.Windows.Forms.Button button刷新结果;
        private System.Windows.Forms.DataGridViewTextBoxColumn 项目;
        private System.Windows.Forms.DataGridViewTextBoxColumn 最近;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上一次;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上二次;
        private System.Windows.Forms.DataGridViewTextBoxColumn 上三次;
        private System.Windows.Forms.DataGridViewTextBoxColumn 项目id;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button button读卡;
        private System.Windows.Forms.ComboBox cmb性别;
        private System.Windows.Forms.ComboBox cmb病人类型;
        private System.Windows.Forms.ComboBox cmb年龄单位;
        private System.Windows.Forms.TextBox txt电子健康卡;
        private System.Windows.Forms.TextBox txt身份证号;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit cmb送检医师;
        private DevExpress.XtraEditors.LookUpEdit cmb核对医师;
        private DevExpress.XtraEditors.LookUpEdit cmb检验医师;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue;
        private System.Windows.Forms.DataGridViewTextBoxColumn fod;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcutoff;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_badge;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_ref;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_jgref;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fresult_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.GridControl gridControl化验结果;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView化验结果;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.GridControl gridControl样本;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView样本;
        private DevExpress.XtraGrid.GridControl gridControl比较;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView比较;
        private DevExpress.XtraGrid.Columns.GridColumn col比较_项目;
        private DevExpress.XtraGrid.Columns.GridColumn col比较_最近;
        private DevExpress.XtraGrid.Columns.GridColumn col比较_上一次;
        private DevExpress.XtraGrid.Columns.GridColumn col比较_上二次;
        private DevExpress.XtraGrid.Columns.GridColumn col比较_上三次;
        private DevExpress.XtraGrid.Columns.GridColumn col比较_项目id;
    }
}