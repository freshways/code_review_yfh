﻿using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using WEISHENG.COMM.PluginsAttribute;
using yunLis.lisbll.sam;
using yunLis.wwfbll;

namespace yunLis.lis.sam.jy
{
    [ClassInfoMark(GUID = "111ECF3C-3064-459D-84F0-D9686DF91C71", 键ID = "111ECF3C-3064-459D-84F0-D9686DF91C71", 父ID = "BE22B406-5548-4F27-9F9D-4582D583E701",
    功能名称 = "报告查询", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.jy.JYQueryForm",
    传递参数 = "", 显示顺序 = 1,
    菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public partial class JYQueryForm : com.JyBaseForm
    {
        #region 自定义变量
        jybll blljy = new jybll();

        DataTable dtIMG = new DataTable();

        /// <summary>
        /// 当前行 样本
        /// </summary>
        DataRowView rowCurrent = null;
        /// <summary>
        /// 样本ID
        /// </summary>
        string strfsample_id = "";
        /// <summary>
        /// 申请ID
        /// </summary>
        string strfapply_id = "";
        /// <summary>
        /// 审核标记
        /// </summary>
        string strfexamine_flag = "0";
        /// <summary>
        /// 仪器列表
        /// </summary>
        DataTable dtInstr = null;
        #endregion

        #region 自定义方法
        /// <summary>
        /// 本页初始化方法
        /// </summary>
        private void WWInit()
        {
            try
            {
                this.com_listBindingSource_fstate.DataSource = this.WWComTypeSampleState();//检验状态
                this.com_listBindingSource_fjytype_id.DataSource = this.WWCheckType();//检验类别
                this.com_listBindingSource_fsample_type_id.DataSource = this.WWSampleType();//样本类别
                this.com_listBindingSource_fapply_dept_id.DataSource = this.WWApplyDept();//申请部门
                this.com_listBindingSource_fapply_user_id.DataSource = this.WWUserApply();//申请人
                this.com_listBindingSource_fjy_user_id.DataSource = this.WWUserCheck();//检验人
                dtInstr = this.bllInstr.BllInstrDTByUseAndGroupID(1, yunLis.wwfbll.LoginBLL.strDeptID);//仪器列表
                this.fjy_instrComboBox.DataSource = dtInstr;
                fjy_instrComboBox.SelectedValue = "";

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = yunLis.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.fjy_instrComboBox.SelectedValue = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能

                this.cobx病人类别.DataSource = this.WWComTypeftype_id();//病人类别 
                cobx病人类别.SelectedValue = "";

                dataGridViewReport.AutoGenerateColumns = false;
                dataGridViewResult.AutoGenerateColumns = false;

                DataColumn D0 = new DataColumn("FID", typeof(System.String));
                dtIMG.Columns.Add(D0);

                DataColumn D1 = new DataColumn("结果图1", typeof(System.Byte[]));
                dtIMG.Columns.Add(D1);

                DataColumn D2 = new DataColumn("结果图2", typeof(System.Byte[]));
                dtIMG.Columns.Add(D2);

                DataColumn D3 = new DataColumn("结果图3", typeof(System.Byte[]));
                dtIMG.Columns.Add(D3);

                DataColumn D4 = new DataColumn("结果图4", typeof(System.Byte[]));
                dtIMG.Columns.Add(D4);

                DataColumn D5 = new DataColumn("结果图5", typeof(System.Byte[]));
                dtIMG.Columns.Add(D5);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 结果列显示 
        /// </summary>
        private void bllResultShow(string str仪器id)
        {
            if (str仪器id.Equals("mby_lb_mk3"))
            {
                this.fod.Visible = true;
                this.fcutoff.Visible = true;
            }
            else
            {
                this.fod.Visible = false;
                this.fcutoff.Visible = false;
            }
        }


        /// <summary>
        /// 取得报告列表
        /// </summary>
        private void WWSetReportList()
        {
            if (this.dtReportResult != null)
                this.dtReportResult.Clear();

            try
            {
                Cursor = Cursors.WaitCursor;
                string fhz_id = "";//患者ID
                string fapply_code = "";//申请单号
                string fzyh = "";//住院号
                string fjy_yb_code = "";//样本号
                string fname = "";//姓名                
                string finstr_id = "";//仪器ID
                string f患者类别 = "";//患者类别
                string fjy_date1 = this.bllReport.DbDateTime1(this.fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllReport.DbDateTime1(this.fjy_dateDateTimePicker2);
                string fjy_dateWhere = "";
                fjy_yb_code = this.fsample_code样本号.Text;
                fname = this.fnameTextBox.Text;
                fzyh = fzyhtextBox.Text;
                fhz_id = this.fhz_idtextBox.Text;
                fapply_code = this.fapply_codetextBox.Text;

                try
                {
                    finstr_id = fjy_instrComboBox.SelectedValue.ToString();
                }
                catch { }
                try
                {
                    f患者类别 = cobx病人类别.SelectedValue.ToString();
                }
                catch { }

                if (fjy_instrComboBox.Text == "" || fjy_instrComboBox.Text == null)
                    finstr_id = " (fjy_instr is not null) ";
                else
                    finstr_id = " (fjy_instr='" + finstr_id + "') ";

                if (this.cobx病人类别.Text == "" || this.cobx病人类别.Text == null)
                    f患者类别 = " and (fhz_type_id is not null)";
                else
                    f患者类别 = " and (fhz_type_id='" + f患者类别 + "') ";

                fjy_dateWhere = " and (fjy_date>='" + fjy_date1 + "' and fjy_date<='" + fjy_date2 + "') ";

                if (fjy_yb_code == "" || fjy_yb_code == null)
                    fjy_yb_code = " and (fjy_yb_code is not null) ";
                else
                    fjy_yb_code = " and (fjy_yb_code='" + fjy_yb_code + "')";
                if (fname == "" || fname == null)
                    fname = " and (fhz_name is not null) ";
                else
                    fname = " and (fhz_name like '" + fname + "%')";

                if (fzyh == "" || fzyh == null)
                    fzyh = " and (fhz_zyh is not null) ";
                else
                    fzyh = " and (fhz_zyh='" + fzyh + "')";


                if (fhz_id == "" || fhz_id == null)
                    fhz_id = " and (fhz_id is not null) ";
                else
                    fhz_id = " and (fhz_id='" + fhz_id + "')";

                if (fapply_code == "" || fapply_code == null)
                    fapply_code = " and (fjy_id is not null) ";
                else
                    fapply_code = " and (fjy_id='" + fapply_code + "')";

                string strWhere = finstr_id + f患者类别 + fjy_dateWhere + fjy_yb_code + fname + fzyh + fhz_id + fapply_code + " order by fjy_date desc,convert(int,fjy_yb_code)  ";

                this.WWSetdtReport(strWhere);
                this.lIS_REPORTBindingSource.DataSource = this.dtReport;
                dataGridViewReport.DataSource = this.lIS_REPORTBindingSource;

                ////20150611 add begin wjz 已打印报表显示浅蓝色
                //foreach (DataGridViewRow row in dataGridViewReport.Rows)
                //{
                //    string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
                //    if (str打印状态.Equals("已打印"))
                //    {
                //        row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                //    }
                //}
                ////20150611 add end wjz 已打印报表显示浅蓝色


            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void WWResultList()
        {
            try
            {
                this.WWSetdtReportResult(this.strfapply_id);
                this.bindingSourceResult.DataSource = this.dtReportResult;
                this.dataGridViewResult.DataSource = this.bindingSourceResult;

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == (Keys.F1))
            {
                toolStripButtonHelp.PerformClick();
                return true;
            }
            if (keyData == (Keys.F5))
            {
                toolStripButtonQuery.PerformClick();
                return true;
            }
            if (keyData == (Keys.F8))
            {
                toolStripButtonPrint.PerformClick();
                return true;
            }
            if (keyData == (Keys.F9))
            {
                toolStripButtonPrintYL.PerformClick();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="b直接打印"></param>
        private bool WWPrint(bool b直接打印) //20150610 wjz 修改返回类型 void ==> bool
        {
            bool ret = false;
            try
            {
                yunLis.lis.sam.Report.PrintHelper pr = new yunLis.lis.sam.Report.PrintHelper();
                ret = pr.GetData_PrintViewer(this.strfapply_id, b直接打印); //  strfsample_id
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            return ret;
        }
        DataTable dtIMG_1 = new DataTable();
        private void WWImg()
        {
            try
            {
                if (this.dtIMG.Rows.Count > 0)
                    this.dtIMG.Clear();
                if (this.dtIMG_1.Rows.Count > 0)
                    this.dtIMG_1.Clear();
                dtIMG_1 = this.bllReport.BllImgDT(this.strfsample_id, strfinstr_result_id);
                DataRow drimg = dtIMG.NewRow();
                int intImgCount = dtIMG_1.Rows.Count;
                if (intImgCount > 0)
                {
                    groupBoxImg.Visible = true;

                    drimg["FID"] = dtIMG_1.Rows[0]["FImgID"];
                    for (int iimg = 0; iimg < intImgCount; iimg++)
                    {
                        switch (iimg)
                        {
                            case 0:
                                img1.Visible = true;
                                img2.Visible = false;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图1"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 1:
                                img2.Visible = true;
                                img3.Visible = false;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图2"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 2:
                                img3.Visible = true;
                                img4.Visible = false;
                                img5.Visible = false;
                                drimg["结果图3"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 3:
                                img4.Visible = true;
                                img5.Visible = false;
                                drimg["结果图4"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            case 4:
                                img5.Visible = true;
                                drimg["结果图5"] = dtIMG_1.Rows[iimg]["fimg"];
                                break;
                            default:
                                break;
                        }
                    }

                }
                else
                {
                    groupBoxImg.Visible = false;
                    img1.Visible = false;
                    img2.Visible = false;
                    img3.Visible = false;
                    img4.Visible = false;
                    img5.Visible = false;
                }
                dtIMG.Rows.Add(drimg);
                lIS_REPORT_IMGBindingSource.DataSource = this.dtIMG;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        #endregion

        public JYQueryForm()
        {
            InitializeComponent();
            WWInit();
        }

        private void ReportQueryForm_Load(object sender, EventArgs e)
        {
            try
            {
                WWSetReportList();

            }

            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        string strfinstr_result_id = "";
        private void lIS_REPORTBindingSource_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                rowCurrent = (DataRowView)this.lIS_REPORTBindingSource.Current;
                if (this.dtReportResult.Count > 0)
                    this.dtReportResult.Clear();
                if (rowCurrent != null)
                {
                    strfapply_id = rowCurrent["fjy_id"].ToString();
                    strfsample_id = rowCurrent["fjy_id"].ToString();
                    strfinstr_result_id = rowCurrent["finstr_result_id"].ToString();



                    if (rowCurrent["fjy_zt"] != null)
                        strfexamine_flag = rowCurrent["fjy_zt"].ToString();
                    WWResultList();
                    WWImg();
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }


        }

        private void dataGridViewResult_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            this.WWOpenHelp("lis_jyquery");
        }

        private void toolStripButtonPrintYL_Click(object sender, EventArgs e)
        {
            bool printSuccess = WWPrint(false);
            if (printSuccess)
            {
                //正常情况下，应该是先审核后打印，但限于既有的程序，目前只能是先打印后修改审核状态
                int updateRowCount = this.bllReport.BllReportUpdate审核状态(this.strfsample_id);

                DataGridViewRow row = dataGridViewReport.CurrentRow;

                //add by wjz 20151222 从下方已过来的 ▽
                string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
                if (!(str打印状态.Equals("已打印")))
                {
                    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    row.Cells["fprint_zt"].Value = "已打印";
                }
                //add by wjz 20151222 从下方已过来的 △

                if ((row != null) && (updateRowCount > 0))
                {
                    //del by wjz 20151222 移到上方 ▽
                    //string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
                    //del by wjz 20151222 移到上方 △
                    string str审核状态 = row.Cells["fjy_zt"].Value.ToString();

                    //del by wjz 20151222 移到上方 ▽
                    //if (!(str打印状态.Equals("已打印")))
                    //{
                    //    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    //    row.Cells["fprint_zt"].Value = "已打印";
                    //}
                    //del by wjz 20151222 移到上方 △
                    if (!(str审核状态.Equals("已审核")))
                    {
                        row.Cells["fjy_zt"].Value = "已审核";

                        //向His中回写检验数据 add 20160702 wjz   未审核的情况下，向His回写数据
                        string str申请单号 = row.Cells["fapply_id"].Value.ToString();
                        string strDate = row.Cells["fjy_date"].Value.ToString();
                        string str设备ID = row.Cells["fjy_instr"].Value.ToString();
                        string str样本号 = row.Cells["fjy_yb_code"].Value.ToString();

                        //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                        string str门诊住院号 = row.Cells["fhz_zyh"].Value.ToString();
                        //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △

                        //changed 20150814 wjz 以下两行
                        //bool b是否回写 = yunLis.Properties.Settings.Default.WriteJYDataToHis;
                        //this.blljy.WriteDataToHis(str申请单号, strDate, str设备ID, str样本号, b是否回写);
                        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                        //this.blljy.WriteDataToHis(str申请单号, strDate, str设备ID, str样本号);
                        this.blljy.WriteDataToHisBy门诊住院号(str门诊住院号, strDate, str设备ID, str样本号);
                        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △
                        //向His中回写检验数据 add 20160702 wjz
                    }
                }
            }
        }

        private void toolStripButtonPrint_Click(object sender, EventArgs e) //此函数变更比较大
        {
            //  MessageBox.Show(this.strfsample_id);

            bool printResult = WWPrint(true);


            if (printResult)
            {
                //正常情况下，应该是先审核后打印，但限于既有的程序，目前只能是先打印后修改审核状态
                int updateRowCount = this.bllReport.BllReportUpdate审核状态(this.strfsample_id);

                DataGridViewRow row = dataGridViewReport.CurrentRow;
                if ((row != null) && (updateRowCount > 0))
                {
                    string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
                    string str审核状态 = row.Cells["fjy_zt"].Value.ToString();
                    if (!(str打印状态.Equals("已打印")))
                    {
                        row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        row.Cells["fprint_zt"].Value = "已打印";
                    }
                    if (!(str审核状态.Equals("已审核")))
                    {
                        row.Cells["fjy_zt"].Value = "已审核";

                        //向His中回写检验数据 add 20160702 wjz
                        string str申请单号 = row.Cells["fapply_id"].Value.ToString();
                        string strDate = row.Cells["fjy_date"].Value.ToString();
                        string str设备ID = row.Cells["fjy_instr"].Value.ToString();
                        string str样本号 = row.Cells["fjy_yb_code"].Value.ToString();

                        //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                        string str门诊住院号 = row.Cells["fhz_zyh"].Value.ToString();
                        //add by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △

                        //changed 20150814 wjz 以下两行
                        //bool b是否回写 = yunLis.Properties.Settings.Default.WriteJYDataToHis;
                        //this.blljy.WriteDataToHis(str申请单号, strDate, str设备ID, str样本号, b是否回写);

                        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 ▽
                        //this.blljy.WriteDataToHis(str申请单号, strDate, str设备ID, str样本号);
                        this.blljy.WriteDataToHisBy门诊住院号(str门诊住院号, strDate, str设备ID, str样本号);
                        //changed by wjz 20160125 以前是根据申请单号回传结果，现在修改为根据住院号门诊号回传结果 △

                        //向His中回写检验数据 add 20160702 wjz
                    }
                }
            }
        }

        private void toolStripButtonQuery_Click(object sender, EventArgs e)
        {
            WWSetReportList();

        }



        private void dataGridViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewReport.Rows.Count > 0)
                {

                    strfapply_id = this.dataGridViewReport.CurrentRow.Cells["fjy_id"].Value.ToString();
                    WWResultList();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void dataGridViewReport_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void 导入图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ImgBLL img = new ImgBLL();
                img.ImgIn(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 导出图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ImgBLL img = new ImgBLL();
                img.ImgOut(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 清空图片ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ImgBLL img = new ImgBLL();
                img.ImgNull(img1);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 图片浏览ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void img1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Bitmap bit = new Bitmap(img1.Image);
                ImgShowForm imgform = new ImgShowForm(bit);
                imgform.ShowDialog();
                imgform.Dispose();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void fjy_instrComboBox_DropDownClosed(object sender, EventArgs e)
        {
            string str仪器id = "";//
            {
                str仪器id = Convert.ToString(this.fjy_instrComboBox.SelectedValue);
            }
            bllResultShow(str仪器id);

        }

        private void dataGridViewReport_DataError_1(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewResult_DataError_1(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButtonPLDY_Click(object sender, EventArgs e)
        {
            JYPLDYForm frm = new JYPLDYForm();
            frm.ShowDialog();

            //添加此句的目的：批量打印后刷新打印状态、审核状态
            WWSetReportList();
        }

        private void toolStripButton上传_Click(object sender, EventArgs e)
        {
            if (dataGridViewReport == null && dataGridViewReport.Rows.Count <= 0)
            { return; }
            foreach (DataGridViewRow dtr in dataGridViewReport.Rows)
            {
                string str身份证 = dtr.Cells["fremark"].Value.ToString();
                string str类别 = dtr.Cells["类型"].Value.ToString();
                string str审核 = dtr.Cells["fjy_zt"].Value.ToString();
                string str样本日期 = dtr.Cells["fjy_date"].Value.ToString();
                string str检验ID = dtr.Cells["fjy_id"].Value.ToString();
                if (str身份证 != "" && str类别 == "查体" && str审核 == "已审核")
                {
                    int rowback = 0;
                    string dn编号 = "";
                    string s机构号 = "";
                    DataTable dndt = yunLis.dao.DB2OdbcHelper.ExecuteDs("select d_grdabh,p_rgid from DB2ADMIN.T_DA_JKDA_RKXZL where d_grdabh='" + str身份证 + "' ").Tables[0]; // or D_sfzh='" + str身份证 + "'
                    if (dndt != null && dndt.Rows.Count == 1)
                    {
                        dn编号 = dndt.Rows[0]["d_grdabh"].ToString();
                        s机构号 = dndt.Rows[0]["p_rgid"].ToString();
                    }
                    if (dn编号 == "")
                    {
                        MessageBox.Show("[" + str身份证 + "]未找到该病人信息！");
                        continue;
                    }
                    string checkSql = "select * from DB2ADMIN.T_JK_JKTJ where d_grdabh='" + dn编号 + "' and happentime='" + Convert.ToDateTime(str样本日期).ToString("yyyy-MM-dd") + "'";
                    DataTable dt = new DataTable();
                    try
                    {
                        dt = yunLis.dao.DB2OdbcHelper.ExecuteDs(checkSql).Tables[0];
                    }
                    catch { dt = null; }
                    string s体检id = "";
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        s体检id = dt.Rows[0]["ID"].ToString();
                    }
                    else
                    {
                        string insertsql = "";

                        #region 插入语句
                        insertsql = "insert into db2admin.T_JK_JKTJ (G_ZHZH, G_TW, G_HX, G_MB, G_XYYC1, G_XYYC2," +
                                           "G_XYZC1, G_XYZC2, G_SG, G_YW, G_TZH, G_TZHZH, G_LNRRZ, G_LNRQG, G_LNRRZFEN," +
                                           "G_LNRQGFEN, G_ZYSL, G_YYSL, G_ZYJZ, G_YYJZ, G_TL, G_YDGN, G_PFGM, G_LBJ,  " +
                                           "G_LBJQT, G_TZX, G_HXY, G_HXYYC, G_LY, G_LYYC, G_XINLV, G_XINLVCI, G_ZAYIN," +
                                           "G_ZAYINYO, G_YATO, G_YATOYO, G_BK, G_BKYO, G_GANDA, G_GANDAYO, G_PIDA, G_PIDAYO," +
                                           "G_ZHUOYIN, G_ZHUOYINYO, G_XZSZ, G_GMZHZH, G_GMZHZHYI, G_QLX, G_CTQT, WBC, HB, PLT," +
                                           "G_XCGQT, G_NDB, G_NT, G_NTT, G_NQX, G_NCGQT, G_DBQX, ALT, AST, ALB," +
                                           "TBIL, DBIL, SCR, BUN, CHO, TG, LDLC, HDLC, G_KFXT, HBSAG, G_YAND, G_YANDYI, G_XINDT," +
                                           "G_XINDTYI, G_XIONGP, G_XIONGPYC, G_BCHAO, G_BCHAOYI, G_FUZHUQT, G_JLJJY,  " +
                                           "D_GRDABH, CREATREGION, CREATETIME, CREATEUSER, UPDATETIME, UPDATEUSER,    " +
                                           "HAPPENTIME, P_RGID, FIELD1,  FIELD2, FIELD3, FIELD4, WBC_SUP, PLT_SUP, G_TUNWEI," +
                                           "G_YTWBZ, G_DLPL, G_MCDLSJ, G_JCDLSJ, G_DLFS, G_YSXG, G_XYZK, G_RXYL, G_KSXYNL," +
                                           "G_JYNL, G_YJPL, G_RYJL, G_SFJJ, G_JJNL, G_KSYJNL, G_YNNSFYJ, G_YJZL, G_YJZLQT," +
                                           "G_YWZYBL, G_JTZY, G_CYSJ, G_HXP, G_HXPFHCS, G_HXPFHCSJT, G_DUWU, G_DWFHCS, G_DWFHCSQT," +
                                           "G_SHEXIAN, G_SXFHCS, G_SXFHCSQT, G_KOUCHUN, G_CHILEI, G_YANBU, G_PFQT, G_GONGMO, G_GMQT," +
                                           "G_ZBDMMD, G_RUXIAN, G_RUXIANQT, G_WAIYIN, G_WAIYINYC, G_YINDAO, G_YINDAOYC, G_GONGJING," +
                                           "G_GONGJINGYC,G_GONGTI, G_GONGTIYC, G_FUJIAN, G_FUJIANYC, G_SGNXJND, G_SGNXNND," +
                                           "G_THXHDB, G_GJTP, G_GJTPYC, G_PHZ, G_QXZ, G_YANGXZ, G_YINXZ, G_TSZ, G_SRZ," +
                                           "G_XYZ, G_QYZ, G_TBZ, G_NXGJB, G_NXGJBQT, G_SZJB, G_SZJBQT, G_XZJB, G_XZJBQT, G_XGJB," +
                                           "G_XGJBQT, G_YBJB, G_YBJBQT, G_SJXTJB, G_SJXTJBQT, G_QTXTJB, G_QTXTJBQT, G_JKPJ, G_JKPJYC1,   " +
                                           "G_JKPJYC2, G_JKPJYC3, G_JKPJYC4, G_JKZD, G_WXYSKZ, G_WXYSTZ, G_WSYSYM, G_WXYSQT, FIELD5,     " +
                                           "G_ZZQT, G_XYYC, G_XYZC, G_QTZHZH, QDQXZ, KCHQT, CHLQT, YBQT, YDGNQT,G_CHXT, LNRZKPG, LNRZLPG," +
                                           "FENCHEN, WULIYINSU, BLQITA, FCHCS, WLCS, BLQTCS, FCHY, WLY, QTY, TNBFXJF, " +
                                           "ZCYY, YCYY, NWLBDB, WZD, G_CLQUE, G_CLQU, G_CLYI)" +
                                           "values (1,null, null, null, '', '', '', '', null, null, null, '', null," +
                                           "null, null, null, null, null, null,'',1,1,1,1,null,2,1,null,1,null,null,1,1," +
                                           "null,1,null,1,null,1,null,1,null,1,null,1,null,null,null,'','','',''," +
                                           "'','','','','','',null,'','','','','','','','','','','',''," +
                                           "null,null,null,null,null,null,null,null,null,'',null,'" + dn编号 + "'," +
                                           "'" + s机构号 + "','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','371323B100190001','" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','371323B100190001'," +
                                           "'" + Convert.ToDateTime(str样本日期).ToString("yyyy-MM-dd") + "','" + s机构号 + "',null,'',null,null,null,null,null,null,4,null,null,null,null, " +
                                           "1,null,null,null,1,null,null,null,null,null,null,null,1," +
                                           "null,null,null,null,null,null,null,null,null,null,null,1,1,1,null,1,null,1," +
                                           "null,null,null,null,null,null,null,null,null,null,null,null,'','',''," +
                                           "null,null,null,null,null,null,null,null,null,null,null," +
                                           "1,null,1,null,1,null,1,null,1,null,1,'',1,'',1," +
                                           "null,null,null,null,null,null,null,null,null,null,null,null,null,null,14," +
                                           "null,null,null,null,'',null,null,null,null,null,null,null,null,null,null,null,null," +
                                           "'','','','71','0@0@0@0','0@0@0@0','0@0@0@0')";
                        #endregion
                        rowback = yunLis.dao.DB2OdbcHelper.ExecuteNonQuery(insertsql);
                        if (rowback == 1)
                        {//重新获取体检id
                            string getSql = "select * from DB2ADMIN.T_JK_JKTJ where d_grdabh='" + dn编号 + "' and happentime='" + Convert.ToDateTime(str样本日期).ToString("yyyy-MM-dd") + "'";
                            DataTable dtt = new DataTable();
                            try
                            {
                                dtt = yunLis.dao.DB2OdbcHelper.ExecuteDs(checkSql).Tables[0];
                            }
                            catch { dt = null; }
                            if (dtt != null && dtt.Rows.Count > 0)
                            {
                                s体检id = dtt.Rows[0]["ID"].ToString();
                            }
                        }
                    }
                    if (rowback == 0 && s体检id == "")
                        return;
                    this.WWSetdtReportResult(str检验ID);
                    //this.bindingSourceResult.DataSource = this.dtReportResult;
                    string upsql = "update DB2ADMIN.T_JK_JKTJ set ";

                    foreach (var dr in dtReportResult)
                    {
                        #region 肝功能
                        if (dr.fitem_code == "ALT")
                        {//谷丙转氨酶
                            upsql += " ALT='" + dr.fvalue + "' ,";
                        }
                        if (dr.fitem_code == "AST")
                        {//谷草转氨酶
                            upsql += " AST='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "ALB")
                        {//白蛋白
                            upsql += " ALB='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "DBIL")
                        {//直接胆红素
                            upsql += " DBIL='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "TBIL")
                        {//总胆红素
                            upsql += " TBIL='" + dr.fvalue + "', ";
                        }
                        #endregion

                        #region 肾功能
                        if (dr.fitem_code == "CRE")
                        {//肌酐
                            upsql += " SCR='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "UREA")
                        {//尿素氮
                            upsql += " BUN='" + dr.fvalue + "', ";
                        }
                        #endregion

                        #region 血脂
                        if (dr.fitem_code == "CHO")
                        {//总胆固醇
                            upsql += " CHO='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "TG")
                        {//甘油三酯
                            upsql += " TG='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "LDL-C")
                        {//低密度脂蛋白
                            upsql += " LDLC='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "HDL-C")
                        {//高密度脂蛋
                            upsql += " HDLC='" + dr.fvalue + "', ";
                        }
                        #endregion

                        #region 血糖/葡萄糖
                        if (dr.fitem_code == "GLU")
                        {//空腹血糖
                            upsql += " G_KFXT='" + dr.fvalue + "', ";
                        }
                        #endregion

                        #region 血常规
                        if (dr.fitem_code == "WBC")
                        {//白细胞
                            upsql += " WBC='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "HGB")
                        {//血红蛋白
                            upsql += " HB='" + dr.fvalue + "', ";
                        }
                        if (dr.fitem_code == "PLT")
                        {//血小板
                            upsql += " PLT='" + dr.fvalue + "', ";
                        }
                        #endregion
                    }


                    upsql += "UPDATETIME='" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' where id='" + s体检id + "'";
                    yunLis.dao.DB2OdbcHelper.ExecuteNonQuery(upsql);
                }
            }
        }

        private void toolStripButto体检_Click(object sender, EventArgs e)
        {
            string dn编号 = "";
            string str身份证 = this.dataGridViewReport.CurrentRow.Cells["fremark"].Value.ToString();
            string str样本日期 = this.dataGridViewReport.CurrentRow.Cells["fjy_date"].Value.ToString();
            DataTable dndt = yunLis.dao.DB2OdbcHelper.ExecuteDs("select d_grdabh,p_rgid from DB2ADMIN.T_DA_JKDA_RKXZL where d_grdabh='" + str身份证 + "'").Tables[0]; //D_sfzh=
            if (dndt != null && dndt.Rows.Count == 1)
            {
                dn编号 = dndt.Rows[0]["d_grdabh"].ToString();
            }
            if (dn编号 == "")
            {
                MessageBox.Show("未找到该病人信息！");
                return;
            }
            string checkSql = "select * from DB2ADMIN.T_JK_JKTJ where d_grdabh='" + dn编号 + "' and happentime='" + Convert.ToDateTime(str样本日期).ToString("yyyy-MM-dd") + "'";
            DataTable dt = new DataTable();
            try
            {
                dt = yunLis.dao.DB2OdbcHelper.ExecuteDs(checkSql).Tables[0];
            }
            catch { dt = null; }
            string s体检id = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                s体检id = dt.Rows[0]["ID"].ToString();
            }
            else
            {
                MessageBox.Show("未找到该病人[" + str样本日期 + "]体检信息！");
                return;
            }
            FormWebBro frm = new FormWebBro();
            frm.dn编号 = dn编号;
            frm.dnID = s体检id;
            frm.Show();
        }

        private void dataGridViewReport_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //20150611 add begin wjz 已打印报表显示浅蓝色
            foreach (DataGridViewRow row in dataGridViewReport.Rows)
            {
                string str打印状态 = row.Cells["fprint_zt"].Value.ToString();
                if (str打印状态.Equals("已打印"))
                {
                    row.DefaultCellStyle.BackColor = Color.LightSteelBlue;
                }
            }
            //20150611 add end wjz 已打印报表显示浅蓝色
        }

        private void toolStripButtonExportToXls_Click(object sender, EventArgs e)
        {
            if (dataGridViewReport.RowCount == 0)
            {
                MessageBox.Show("没有需要导出的数据。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                ExcelHelper.DataGridViewToExcel(dataGridViewReport);
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出过程中出现未知错误！,异常信息：" + ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_推送化验结果_Click(object sender, EventArgs e)
        {
            DataTable dt = ((System.Windows.Forms.BindingSource)(dataGridViewReport.DataSource)).DataSource as DataTable;
            int index = dataGridViewReport.CurrentRow.Index;
            string s姓名 = dataGridViewReport.Rows[index].Cells["fhz_name"].Value.ToString();
            string bcard = dt.Rows[index]["f1"].ToString();
            string reportid = dt.Rows[index]["fjy_id"].ToString();
            //患者信息
            JObject user = new JObject();
            user["Name"] = s姓名;
            user["bcard"] = bcard;
            user["reportid"] = reportid;
            Common.SendEcardMessage(user);
        }
    }
}