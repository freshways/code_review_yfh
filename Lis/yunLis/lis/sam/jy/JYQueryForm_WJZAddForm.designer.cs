﻿namespace yunLis.lis.sam.jy
{
    partial class JYQueryForm_WJZAddForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label fperson_idLabel;
            System.Windows.Forms.Label fdept_idLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fname_eLabel;
            System.Windows.Forms.Label fpassLabel;
            System.Windows.Forms.Label forder_byLabel;
            System.Windows.Forms.Label fuse_flagLabel;
            System.Windows.Forms.Label fremarkLabel;
            System.Windows.Forms.Label ftypeLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JYQueryForm_WJZAddForm));
            this.fhz_name = new System.Windows.Forms.TextBox();
            this.fhz_age = new System.Windows.Forms.TextBox();
            this.fhz_age_unit = new System.Windows.Forms.TextBox();
            this.fhz_zyh = new System.Windows.Forms.TextBox();
            this.fhz_sex = new System.Windows.Forms.TextBox();
            this.forder_byTextBox = new System.Windows.Forms.TextBox();
            this.fwjz_nr = new System.Windows.Forms.TextBox();
            this.panelTool = new System.Windows.Forms.Panel();
            this.butSave = new System.Windows.Forms.Button();
            this.butReturn = new System.Windows.Forms.Button();
            this.fbg_date1 = new System.Windows.Forms.TextBox();
            this.fdj_date = new System.Windows.Forms.DateTimePicker();
            this.fbg_name = new System.Windows.Forms.TextBox();
            this.fbg_date2 = new System.Windows.Forms.TextBox();
            this.fjs_dept = new System.Windows.Forms.TextBox();
            this.fjs_tel = new System.Windows.Forms.TextBox();
            fperson_idLabel = new System.Windows.Forms.Label();
            fdept_idLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fname_eLabel = new System.Windows.Forms.Label();
            fpassLabel = new System.Windows.Forms.Label();
            forder_byLabel = new System.Windows.Forms.Label();
            fuse_flagLabel = new System.Windows.Forms.Label();
            fremarkLabel = new System.Windows.Forms.Label();
            ftypeLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            this.panelTool.SuspendLayout();
            this.SuspendLayout();
            // 
            // fperson_idLabel
            // 
            fperson_idLabel.AutoSize = true;
            fperson_idLabel.Location = new System.Drawing.Point(49, 58);
            fperson_idLabel.Name = "fperson_idLabel";
            fperson_idLabel.Size = new System.Drawing.Size(59, 12);
            fperson_idLabel.TabIndex = 1;
            fperson_idLabel.Text = "患者姓名:";
            // 
            // fdept_idLabel
            // 
            fdept_idLabel.AutoSize = true;
            fdept_idLabel.Location = new System.Drawing.Point(73, 82);
            fdept_idLabel.Name = "fdept_idLabel";
            fdept_idLabel.Size = new System.Drawing.Size(35, 12);
            fdept_idLabel.TabIndex = 2;
            fdept_idLabel.Text = "年龄:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(205, 85);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(59, 12);
            fnameLabel.TabIndex = 4;
            fnameLabel.Text = "年龄单位:";
            // 
            // fname_eLabel
            // 
            fname_eLabel.AutoSize = true;
            fname_eLabel.Location = new System.Drawing.Point(31, 112);
            fname_eLabel.Name = "fname_eLabel";
            fname_eLabel.Size = new System.Drawing.Size(77, 12);
            fname_eLabel.TabIndex = 6;
            fname_eLabel.Text = "住院/门诊号:";
            // 
            // fpassLabel
            // 
            fpassLabel.AutoSize = true;
            fpassLabel.Location = new System.Drawing.Point(229, 58);
            fpassLabel.Name = "fpassLabel";
            fpassLabel.Size = new System.Drawing.Size(35, 12);
            fpassLabel.TabIndex = 8;
            fpassLabel.Text = "性别:";
            // 
            // forder_byLabel
            // 
            forder_byLabel.AutoSize = true;
            forder_byLabel.Location = new System.Drawing.Point(49, 211);
            forder_byLabel.Name = "forder_byLabel";
            forder_byLabel.Size = new System.Drawing.Size(59, 12);
            forder_byLabel.TabIndex = 10;
            forder_byLabel.Text = "报告科室:";
            // 
            // fuse_flagLabel
            // 
            fuse_flagLabel.AutoSize = true;
            fuse_flagLabel.Location = new System.Drawing.Point(61, 239);
            fuse_flagLabel.Name = "fuse_flagLabel";
            fuse_flagLabel.Size = new System.Drawing.Size(47, 12);
            fuse_flagLabel.TabIndex = 12;
            fuse_flagLabel.Text = "报告人:";
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(37, 140);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(71, 12);
            fremarkLabel.TabIndex = 14;
            fremarkLabel.Text = "危急值内容:";
            // 
            // ftypeLabel
            // 
            ftypeLabel.AutoSize = true;
            ftypeLabel.Location = new System.Drawing.Point(25, 267);
            ftypeLabel.Name = "ftypeLabel";
            ftypeLabel.Size = new System.Drawing.Size(83, 12);
            ftypeLabel.TabIndex = 116;
            ftypeLabel.Text = "报告日期(时):";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(49, 32);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(59, 12);
            label1.TabIndex = 2;
            label1.Text = "上报日期:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(25, 295);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(83, 12);
            label2.TabIndex = 12;
            label2.Text = "报告日期(分):";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(49, 323);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(59, 12);
            label3.TabIndex = 12;
            label3.Text = "接收科室:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(49, 351);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(59, 12);
            label4.TabIndex = 12;
            label4.Text = "接收电话:";
            // 
            // fhz_name
            // 
            this.fhz_name.Location = new System.Drawing.Point(114, 55);
            this.fhz_name.Name = "fhz_name";
            this.fhz_name.ReadOnly = true;
            this.fhz_name.Size = new System.Drawing.Size(109, 21);
            this.fhz_name.TabIndex = 1;
            // 
            // fhz_age
            // 
            this.fhz_age.Location = new System.Drawing.Point(114, 82);
            this.fhz_age.Name = "fhz_age";
            this.fhz_age.ReadOnly = true;
            this.fhz_age.Size = new System.Drawing.Size(85, 21);
            this.fhz_age.TabIndex = 0;
            // 
            // fhz_age_unit
            // 
            this.fhz_age_unit.Location = new System.Drawing.Point(270, 82);
            this.fhz_age_unit.Name = "fhz_age_unit";
            this.fhz_age_unit.ReadOnly = true;
            this.fhz_age_unit.Size = new System.Drawing.Size(62, 21);
            this.fhz_age_unit.TabIndex = 3;
            // 
            // fhz_zyh
            // 
            this.fhz_zyh.Location = new System.Drawing.Point(114, 109);
            this.fhz_zyh.Name = "fhz_zyh";
            this.fhz_zyh.ReadOnly = true;
            this.fhz_zyh.Size = new System.Drawing.Size(218, 21);
            this.fhz_zyh.TabIndex = 4;
            // 
            // fhz_sex
            // 
            this.fhz_sex.Location = new System.Drawing.Point(270, 55);
            this.fhz_sex.Name = "fhz_sex";
            this.fhz_sex.ReadOnly = true;
            this.fhz_sex.Size = new System.Drawing.Size(62, 21);
            this.fhz_sex.TabIndex = 2;
            // 
            // forder_byTextBox
            // 
            this.forder_byTextBox.Location = new System.Drawing.Point(114, 207);
            this.forder_byTextBox.Name = "forder_byTextBox";
            this.forder_byTextBox.ReadOnly = true;
            this.forder_byTextBox.Size = new System.Drawing.Size(218, 21);
            this.forder_byTextBox.TabIndex = 5;
            this.forder_byTextBox.Text = "化验室";
            // 
            // fwjz_nr
            // 
            this.fwjz_nr.Location = new System.Drawing.Point(114, 137);
            this.fwjz_nr.Multiline = true;
            this.fwjz_nr.Name = "fwjz_nr";
            this.fwjz_nr.Size = new System.Drawing.Size(218, 63);
            this.fwjz_nr.TabIndex = 7;
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.butSave);
            this.panelTool.Controls.Add(this.butReturn);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTool.Location = new System.Drawing.Point(0, 392);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(359, 46);
            this.panelTool.TabIndex = 116;
            // 
            // butSave
            // 
            this.butSave.Image = ((System.Drawing.Image)(resources.GetObject("butSave.Image")));
            this.butSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.Location = new System.Drawing.Point(72, 6);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(80, 23);
            this.butSave.TabIndex = 8;
            this.butSave.Text = "   保存(&S)";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butReturn
            // 
            this.butReturn.Image = ((System.Drawing.Image)(resources.GetObject("butReturn.Image")));
            this.butReturn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butReturn.Location = new System.Drawing.Point(178, 6);
            this.butReturn.Name = "butReturn";
            this.butReturn.Size = new System.Drawing.Size(80, 23);
            this.butReturn.TabIndex = 9;
            this.butReturn.Text = "   返回(&R)";
            this.butReturn.UseVisualStyleBackColor = true;
            this.butReturn.Click += new System.EventHandler(this.butReturn_Click);
            // 
            // fbg_date1
            // 
            this.fbg_date1.Location = new System.Drawing.Point(114, 263);
            this.fbg_date1.Name = "fbg_date1";
            this.fbg_date1.Size = new System.Drawing.Size(109, 21);
            this.fbg_date1.TabIndex = 117;
            // 
            // fdj_date
            // 
            this.fdj_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fdj_date.Location = new System.Drawing.Point(114, 27);
            this.fdj_date.Name = "fdj_date";
            this.fdj_date.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fdj_date.Size = new System.Drawing.Size(218, 21);
            this.fdj_date.TabIndex = 118;
            // 
            // fbg_name
            // 
            this.fbg_name.Location = new System.Drawing.Point(114, 235);
            this.fbg_name.Name = "fbg_name";
            this.fbg_name.Size = new System.Drawing.Size(218, 21);
            this.fbg_name.TabIndex = 117;
            // 
            // fbg_date2
            // 
            this.fbg_date2.Location = new System.Drawing.Point(114, 291);
            this.fbg_date2.Name = "fbg_date2";
            this.fbg_date2.Size = new System.Drawing.Size(109, 21);
            this.fbg_date2.TabIndex = 117;
            // 
            // fjs_dept
            // 
            this.fjs_dept.Location = new System.Drawing.Point(114, 319);
            this.fjs_dept.Name = "fjs_dept";
            this.fjs_dept.Size = new System.Drawing.Size(218, 21);
            this.fjs_dept.TabIndex = 117;
            // 
            // fjs_tel
            // 
            this.fjs_tel.Location = new System.Drawing.Point(114, 347);
            this.fjs_tel.Name = "fjs_tel";
            this.fjs_tel.Size = new System.Drawing.Size(218, 21);
            this.fjs_tel.TabIndex = 117;
            // 
            // JYQueryForm_WJZAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 438);
            this.Controls.Add(this.fdj_date);
            this.Controls.Add(ftypeLabel);
            this.Controls.Add(this.fbg_name);
            this.Controls.Add(this.fjs_tel);
            this.Controls.Add(this.fjs_dept);
            this.Controls.Add(this.fbg_date2);
            this.Controls.Add(this.fbg_date1);
            this.Controls.Add(this.panelTool);
            this.Controls.Add(fremarkLabel);
            this.Controls.Add(this.fwjz_nr);
            this.Controls.Add(label4);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(fuse_flagLabel);
            this.Controls.Add(forder_byLabel);
            this.Controls.Add(this.forder_byTextBox);
            this.Controls.Add(fpassLabel);
            this.Controls.Add(this.fhz_sex);
            this.Controls.Add(fname_eLabel);
            this.Controls.Add(this.fhz_zyh);
            this.Controls.Add(fnameLabel);
            this.Controls.Add(this.fhz_age_unit);
            this.Controls.Add(label1);
            this.Controls.Add(fdept_idLabel);
            this.Controls.Add(this.fhz_age);
            this.Controls.Add(fperson_idLabel);
            this.Controls.Add(this.fhz_name);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JYQueryForm_WJZAddForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "危急值上报";
            this.Load += new System.EventHandler(this.JYQueryForm_WJZAddForm_Load);
            this.panelTool.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fhz_name;
        private System.Windows.Forms.TextBox fhz_age;
        private System.Windows.Forms.TextBox fhz_age_unit;
        private System.Windows.Forms.TextBox fhz_zyh;
        private System.Windows.Forms.TextBox fhz_sex;
        private System.Windows.Forms.TextBox forder_byTextBox;
        private System.Windows.Forms.TextBox fwjz_nr;
        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butReturn;
        private System.Windows.Forms.TextBox fbg_date1;
        private System.Windows.Forms.DateTimePicker fdj_date;
        private System.Windows.Forms.TextBox fbg_name;
        private System.Windows.Forms.TextBox fbg_date2;
        private System.Windows.Forms.TextBox fjs_dept;
        private System.Windows.Forms.TextBox fjs_tel;
    }
}