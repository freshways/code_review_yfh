﻿namespace yunLis.lis.sam.jy
{
    partial class JYQueryForm_WJZ
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fhz_type_idLabel;
            System.Windows.Forms.Label fjy_dateLabel;
            System.Windows.Forms.Label finstr_idLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JYQueryForm_WJZ));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lIS_REPORTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.samDataSet = new yunLis.lis.sam.samDataSet();
            this.com_listBindingSource_fstate = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fjytype_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fsample_type_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fapply_user_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fapply_dept_id = new System.Windows.Forms.BindingSource(this.components);
            this.com_listBindingSource_fjy_user_id = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceResult = new System.Windows.Forms.BindingSource(this.components);
            this.groupBoxif = new System.Windows.Forms.GroupBox();
            this.btn_推送化验结果 = new System.Windows.Forms.Button();
            this.cobx病人类别 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.fjy_instrComboBox = new System.Windows.Forms.ComboBox();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPrintYL = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonExportToXls = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.lIS_REPORT_IMGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewReport = new System.Windows.Forms.DataGridView();
            this.fjy_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_instr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_yb_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_zyh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_bed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.性别 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fhz_age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.年龄单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.科室 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.样本 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.检验医师 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fprint_zt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_zt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.类型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fapply_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fjy_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.患者ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fvalue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_badge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_ref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_jgref = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fresult_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            fhz_type_idLabel = new System.Windows.Forms.Label();
            fjy_dateLabel = new System.Windows.Forms.Label();
            finstr_idLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fstate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjytype_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fsample_type_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_user_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_dept_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjy_user_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceResult)).BeginInit();
            this.groupBoxif.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).BeginInit();
            this.SuspendLayout();
            // 
            // fhz_type_idLabel
            // 
            fhz_type_idLabel.AutoSize = true;
            fhz_type_idLabel.Location = new System.Drawing.Point(431, 17);
            fhz_type_idLabel.Name = "fhz_type_idLabel";
            fhz_type_idLabel.Size = new System.Drawing.Size(59, 12);
            fhz_type_idLabel.TabIndex = 19;
            fhz_type_idLabel.Text = "病人类别:";
            // 
            // fjy_dateLabel
            // 
            fjy_dateLabel.AutoSize = true;
            fjy_dateLabel.Location = new System.Drawing.Point(7, 17);
            fjy_dateLabel.Name = "fjy_dateLabel";
            fjy_dateLabel.Size = new System.Drawing.Size(59, 12);
            fjy_dateLabel.TabIndex = 12;
            fjy_dateLabel.Text = "检验日期:";
            // 
            // finstr_idLabel
            // 
            finstr_idLabel.AutoSize = true;
            finstr_idLabel.Location = new System.Drawing.Point(298, 17);
            finstr_idLabel.Name = "finstr_idLabel";
            finstr_idLabel.Size = new System.Drawing.Size(35, 12);
            finstr_idLabel.TabIndex = 10;
            finstr_idLabel.Text = "仪器:";
            // 
            // lIS_REPORTBindingSource
            // 
            this.lIS_REPORTBindingSource.DataMember = "LIS_REPORT";
            this.lIS_REPORTBindingSource.DataSource = this.samDataSet;
            this.lIS_REPORTBindingSource.PositionChanged += new System.EventHandler(this.lIS_REPORTBindingSource_PositionChanged);
            // 
            // samDataSet
            // 
            this.samDataSet.DataSetName = "samDataSet";
            this.samDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // com_listBindingSource_fstate
            // 
            this.com_listBindingSource_fstate.DataMember = "com_list";
            this.com_listBindingSource_fstate.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fjytype_id
            // 
            this.com_listBindingSource_fjytype_id.DataMember = "com_list";
            this.com_listBindingSource_fjytype_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fsample_type_id
            // 
            this.com_listBindingSource_fsample_type_id.DataMember = "com_list";
            this.com_listBindingSource_fsample_type_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fapply_user_id
            // 
            this.com_listBindingSource_fapply_user_id.DataMember = "com_list";
            this.com_listBindingSource_fapply_user_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fapply_dept_id
            // 
            this.com_listBindingSource_fapply_dept_id.DataMember = "com_list";
            this.com_listBindingSource_fapply_dept_id.DataSource = this.samDataSet;
            // 
            // com_listBindingSource_fjy_user_id
            // 
            this.com_listBindingSource_fjy_user_id.DataMember = "com_list";
            this.com_listBindingSource_fjy_user_id.DataSource = this.samDataSet;
            // 
            // groupBoxif
            // 
            this.groupBoxif.Controls.Add(this.btn_推送化验结果);
            this.groupBoxif.Controls.Add(this.cobx病人类别);
            this.groupBoxif.Controls.Add(fhz_type_idLabel);
            this.groupBoxif.Controls.Add(this.label1);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker2);
            this.groupBoxif.Controls.Add(this.fjy_dateDateTimePicker1);
            this.groupBoxif.Controls.Add(fjy_dateLabel);
            this.groupBoxif.Controls.Add(this.fjy_instrComboBox);
            this.groupBoxif.Controls.Add(finstr_idLabel);
            this.groupBoxif.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxif.Location = new System.Drawing.Point(0, 30);
            this.groupBoxif.Name = "groupBoxif";
            this.groupBoxif.Size = new System.Drawing.Size(1105, 43);
            this.groupBoxif.TabIndex = 145;
            this.groupBoxif.TabStop = false;
            // 
            // btn_推送化验结果
            // 
            this.btn_推送化验结果.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btn_推送化验结果.Location = new System.Drawing.Point(600, 12);
            this.btn_推送化验结果.Name = "btn_推送化验结果";
            this.btn_推送化验结果.Size = new System.Drawing.Size(88, 22);
            this.btn_推送化验结果.TabIndex = 33;
            this.btn_推送化验结果.Text = "上报";
            this.btn_推送化验结果.UseVisualStyleBackColor = true;
            this.btn_推送化验结果.Click += new System.EventHandler(this.btn_推送化验结果_Click);
            // 
            // cobx病人类别
            // 
            this.cobx病人类别.DisplayMember = "fname";
            this.cobx病人类别.FormattingEnabled = true;
            this.cobx病人类别.Items.AddRange(new object[] {
            ""});
            this.cobx病人类别.Location = new System.Drawing.Point(493, 13);
            this.cobx病人类别.Name = "cobx病人类别";
            this.cobx病人类别.Size = new System.Drawing.Size(87, 20);
            this.cobx病人类别.TabIndex = 18;
            this.cobx病人类别.ValueMember = "fname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "-";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(186, 13);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(97, 21);
            this.fjy_dateDateTimePicker2.TabIndex = 13;
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(72, 13);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(97, 21);
            this.fjy_dateDateTimePicker1.TabIndex = 11;
            // 
            // fjy_instrComboBox
            // 
            this.fjy_instrComboBox.DisplayMember = "ShowName";
            this.fjy_instrComboBox.FormattingEnabled = true;
            this.fjy_instrComboBox.Items.AddRange(new object[] {
            ""});
            this.fjy_instrComboBox.Location = new System.Drawing.Point(337, 13);
            this.fjy_instrComboBox.Name = "fjy_instrComboBox";
            this.fjy_instrComboBox.Size = new System.Drawing.Size(87, 20);
            this.fjy_instrComboBox.TabIndex = 9;
            this.fjy_instrComboBox.ValueMember = "finstr_id";
            this.fjy_instrComboBox.DropDownClosed += new System.EventHandler(this.fjy_instrComboBox_DropDownClosed);
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.lIS_REPORTBindingSource;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.toolStripSeparator5,
            this.toolStripButtonQuery,
            this.toolStripButtonPrint,
            this.toolStripButtonPrintYL,
            this.toolStripButtonExportToXls,
            this.toolStripButtonHelp});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(1105, 30);
            this.bN.TabIndex = 146;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonQuery.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonQuery.Size = new System.Drawing.Size(86, 27);
            this.toolStripButtonQuery.Text = "查询(F5) ";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripButtonPrint
            // 
            this.toolStripButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrint.Image")));
            this.toolStripButtonPrint.Name = "toolStripButtonPrint";
            this.toolStripButtonPrint.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrint.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonPrint.Size = new System.Drawing.Size(86, 27);
            this.toolStripButtonPrint.Text = "打印(F8) ";
            this.toolStripButtonPrint.Click += new System.EventHandler(this.toolStripButtonPrint_Click);
            // 
            // toolStripButtonPrintYL
            // 
            this.toolStripButtonPrintYL.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPrintYL.Image")));
            this.toolStripButtonPrintYL.Name = "toolStripButtonPrintYL";
            this.toolStripButtonPrintYL.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonPrintYL.Size = new System.Drawing.Size(114, 27);
            this.toolStripButtonPrintYL.Text = "打印预览(F9) ";
            this.toolStripButtonPrintYL.Click += new System.EventHandler(this.toolStripButtonPrintYL_Click);
            // 
            // toolStripButtonExportToXls
            // 
            this.toolStripButtonExportToXls.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonExportToXls.Image")));
            this.toolStripButtonExportToXls.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonExportToXls.Name = "toolStripButtonExportToXls";
            this.toolStripButtonExportToXls.Size = new System.Drawing.Size(91, 27);
            this.toolStripButtonExportToXls.Text = "导出Excel";
            this.toolStripButtonExportToXls.Click += new System.EventHandler(this.toolStripButtonExportToXls_Click);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(90, 27);
            this.toolStripButtonHelp.Text = "帮助(F1)  ";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridViewResult);
            this.panel1.Controls.Add(this.splitter1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(600, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(505, 461);
            this.panel1.TabIndex = 147;
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AllowUserToAddRows = false;
            this.dataGridViewResult.AllowUserToResizeRows = false;
            this.dataGridViewResult.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code,
            this.fitem_name,
            this.fvalue,
            this.fitem_badge,
            this.fitem_ref,
            this.fitem_jgref,
            this.fod,
            this.fcutoff,
            this.fitem_unit,
            this.forder_by,
            this.fitem_id,
            this.fresult_id});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewResult.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewResult.Location = new System.Drawing.Point(3, 0);
            this.dataGridViewResult.MultiSelect = false;
            this.dataGridViewResult.Name = "dataGridViewResult";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewResult.RowHeadersVisible = false;
            this.dataGridViewResult.RowHeadersWidth = 30;
            this.dataGridViewResult.RowTemplate.Height = 23;
            this.dataGridViewResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewResult.ShowCellToolTips = false;
            this.dataGridViewResult.ShowEditingIcon = false;
            this.dataGridViewResult.Size = new System.Drawing.Size(502, 461);
            this.dataGridViewResult.TabIndex = 135;
            this.dataGridViewResult.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewResult_DataError_1);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 461);
            this.splitter1.TabIndex = 132;
            this.splitter1.TabStop = false;
            // 
            // lIS_REPORT_IMGBindingSource
            // 
            this.lIS_REPORT_IMGBindingSource.DataMember = "LIS_REPORT_IMG";
            this.lIS_REPORT_IMGBindingSource.DataSource = this.samDataSet;
            // 
            // dataGridViewReport
            // 
            this.dataGridViewReport.AllowUserToAddRows = false;
            this.dataGridViewReport.AllowUserToDeleteRows = false;
            this.dataGridViewReport.AllowUserToResizeRows = false;
            this.dataGridViewReport.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fjy_date,
            this.fjy_instr,
            this.fjy_yb_code,
            this.fhz_name,
            this.fhz_zyh,
            this.fhz_bed,
            this.性别,
            this.fhz_age,
            this.年龄单位,
            this.科室,
            this.样本,
            this.检验医师,
            this.fprint_zt,
            this.fjy_zt,
            this.类型,
            this.fapply_id,
            this.fjy_id,
            this.fremark,
            this.患者ID});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewReport.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewReport.Location = new System.Drawing.Point(0, 73);
            this.dataGridViewReport.MultiSelect = false;
            this.dataGridViewReport.Name = "dataGridViewReport";
            this.dataGridViewReport.ReadOnly = true;
            this.dataGridViewReport.RowHeadersVisible = false;
            this.dataGridViewReport.RowHeadersWidth = 30;
            this.dataGridViewReport.RowTemplate.Height = 23;
            this.dataGridViewReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewReport.ShowCellToolTips = false;
            this.dataGridViewReport.ShowEditingIcon = false;
            this.dataGridViewReport.Size = new System.Drawing.Size(600, 461);
            this.dataGridViewReport.TabIndex = 148;
            this.dataGridViewReport.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridViewReport_DataBindingComplete);
            this.dataGridViewReport.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewReport_DataError_1);
            this.dataGridViewReport.Click += new System.EventHandler(this.dataGridViewReport_Click);
            // 
            // fjy_date
            // 
            this.fjy_date.DataPropertyName = "fjy_date";
            this.fjy_date.HeaderText = "日期";
            this.fjy_date.Name = "fjy_date";
            this.fjy_date.ReadOnly = true;
            this.fjy_date.Width = 75;
            // 
            // fjy_instr
            // 
            this.fjy_instr.DataPropertyName = "fjy_instr";
            this.fjy_instr.HeaderText = "仪器";
            this.fjy_instr.Name = "fjy_instr";
            this.fjy_instr.ReadOnly = true;
            this.fjy_instr.Width = 65;
            // 
            // fjy_yb_code
            // 
            this.fjy_yb_code.DataPropertyName = "fjy_yb_code";
            this.fjy_yb_code.HeaderText = "样本号";
            this.fjy_yb_code.Name = "fjy_yb_code";
            this.fjy_yb_code.ReadOnly = true;
            this.fjy_yb_code.Width = 64;
            // 
            // fhz_name
            // 
            this.fhz_name.DataPropertyName = "fhz_name";
            this.fhz_name.HeaderText = "姓名";
            this.fhz_name.Name = "fhz_name";
            this.fhz_name.ReadOnly = true;
            this.fhz_name.Width = 55;
            // 
            // fhz_zyh
            // 
            this.fhz_zyh.DataPropertyName = "fhz_zyh";
            this.fhz_zyh.HeaderText = "住院号";
            this.fhz_zyh.Name = "fhz_zyh";
            this.fhz_zyh.ReadOnly = true;
            this.fhz_zyh.Width = 65;
            // 
            // fhz_bed
            // 
            this.fhz_bed.DataPropertyName = "fhz_bed";
            this.fhz_bed.HeaderText = "床号";
            this.fhz_bed.Name = "fhz_bed";
            this.fhz_bed.ReadOnly = true;
            this.fhz_bed.Width = 55;
            // 
            // 性别
            // 
            this.性别.DataPropertyName = "性别";
            this.性别.HeaderText = "性别";
            this.性别.Name = "性别";
            this.性别.ReadOnly = true;
            this.性别.Width = 55;
            // 
            // fhz_age
            // 
            this.fhz_age.DataPropertyName = "fhz_age";
            this.fhz_age.HeaderText = "年";
            this.fhz_age.Name = "fhz_age";
            this.fhz_age.ReadOnly = true;
            this.fhz_age.Width = 30;
            // 
            // 年龄单位
            // 
            this.年龄单位.DataPropertyName = "年龄单位";
            this.年龄单位.HeaderText = "龄";
            this.年龄单位.Name = "年龄单位";
            this.年龄单位.ReadOnly = true;
            this.年龄单位.Width = 30;
            // 
            // 科室
            // 
            this.科室.DataPropertyName = "科室";
            this.科室.HeaderText = "科室";
            this.科室.Name = "科室";
            this.科室.ReadOnly = true;
            this.科室.Width = 55;
            // 
            // 样本
            // 
            this.样本.DataPropertyName = "样本";
            this.样本.HeaderText = "样本";
            this.样本.Name = "样本";
            this.样本.ReadOnly = true;
            this.样本.Width = 55;
            // 
            // 检验医师
            // 
            this.检验医师.DataPropertyName = "检验医师";
            this.检验医师.HeaderText = "检验";
            this.检验医师.Name = "检验医师";
            this.检验医师.ReadOnly = true;
            this.检验医师.Width = 55;
            // 
            // fprint_zt
            // 
            this.fprint_zt.DataPropertyName = "fprint_zt";
            this.fprint_zt.HeaderText = "打印";
            this.fprint_zt.Name = "fprint_zt";
            this.fprint_zt.ReadOnly = true;
            this.fprint_zt.Width = 55;
            // 
            // fjy_zt
            // 
            this.fjy_zt.DataPropertyName = "fjy_zt";
            this.fjy_zt.HeaderText = "审核";
            this.fjy_zt.Name = "fjy_zt";
            this.fjy_zt.ReadOnly = true;
            this.fjy_zt.Width = 55;
            // 
            // 类型
            // 
            this.类型.DataPropertyName = "类型";
            this.类型.HeaderText = "类型";
            this.类型.Name = "类型";
            this.类型.ReadOnly = true;
            this.类型.Width = 55;
            // 
            // fapply_id
            // 
            this.fapply_id.DataPropertyName = "fapply_id";
            this.fapply_id.HeaderText = "申请号";
            this.fapply_id.Name = "fapply_id";
            this.fapply_id.ReadOnly = true;
            this.fapply_id.Width = 65;
            // 
            // fjy_id
            // 
            this.fjy_id.DataPropertyName = "fjy_id";
            this.fjy_id.HeaderText = "fjy_id";
            this.fjy_id.Name = "fjy_id";
            this.fjy_id.ReadOnly = true;
            this.fjy_id.Visible = false;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            this.fremark.ReadOnly = true;
            // 
            // 患者ID
            // 
            this.患者ID.DataPropertyName = "fhz_id";
            this.患者ID.HeaderText = "患者ID";
            this.患者ID.Name = "患者ID";
            this.患者ID.ReadOnly = true;
            this.患者ID.Visible = false;
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "编码";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Width = 60;
            // 
            // fitem_name
            // 
            this.fitem_name.DataPropertyName = "fitem_name";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_name.DefaultCellStyle = dataGridViewCellStyle2;
            this.fitem_name.HeaderText = "检验项目";
            this.fitem_name.Name = "fitem_name";
            this.fitem_name.ReadOnly = true;
            // 
            // fvalue
            // 
            this.fvalue.DataPropertyName = "fvalue";
            this.fvalue.HeaderText = "结果";
            this.fvalue.Name = "fvalue";
            this.fvalue.Width = 65;
            // 
            // fitem_badge
            // 
            this.fitem_badge.DataPropertyName = "fitem_badge";
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_badge.DefaultCellStyle = dataGridViewCellStyle3;
            this.fitem_badge.HeaderText = ".";
            this.fitem_badge.Name = "fitem_badge";
            this.fitem_badge.ReadOnly = true;
            this.fitem_badge.Width = 35;
            // 
            // fitem_ref
            // 
            this.fitem_ref.DataPropertyName = "fitem_ref";
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_ref.DefaultCellStyle = dataGridViewCellStyle4;
            this.fitem_ref.HeaderText = "参考值";
            this.fitem_ref.Name = "fitem_ref";
            this.fitem_ref.ReadOnly = true;
            this.fitem_ref.Width = 80;
            // 
            // fitem_jgref
            // 
            this.fitem_jgref.DataPropertyName = "fitem_jgref";
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            this.fitem_jgref.DefaultCellStyle = dataGridViewCellStyle5;
            this.fitem_jgref.HeaderText = "危急值";
            this.fitem_jgref.Name = "fitem_jgref";
            this.fitem_jgref.ReadOnly = true;
            this.fitem_jgref.Width = 80;
            // 
            // fod
            // 
            this.fod.DataPropertyName = "fod";
            this.fod.HeaderText = "OD值";
            this.fod.Name = "fod";
            this.fod.Visible = false;
            this.fod.Width = 65;
            // 
            // fcutoff
            // 
            this.fcutoff.DataPropertyName = "fcutoff";
            this.fcutoff.HeaderText = "Cutoff";
            this.fcutoff.Name = "fcutoff";
            this.fcutoff.Visible = false;
            this.fcutoff.Width = 70;
            // 
            // fitem_unit
            // 
            this.fitem_unit.DataPropertyName = "fitem_unit";
            this.fitem_unit.HeaderText = "单位";
            this.fitem_unit.Name = "fitem_unit";
            this.fitem_unit.ReadOnly = true;
            this.fitem_unit.Width = 65;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "序号";
            this.forder_by.Name = "forder_by";
            this.forder_by.Visible = false;
            this.forder_by.Width = 60;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "项目id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.ReadOnly = true;
            this.fitem_id.Visible = false;
            // 
            // fresult_id
            // 
            this.fresult_id.DataPropertyName = "fresult_id";
            this.fresult_id.HeaderText = "fresult_id";
            this.fresult_id.Name = "fresult_id";
            this.fresult_id.ReadOnly = true;
            this.fresult_id.Visible = false;
            // 
            // JYQueryForm_WJZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1105, 534);
            this.Controls.Add(this.dataGridViewReport);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBoxif);
            this.Controls.Add(this.bN);
            this.Name = "JYQueryForm_WJZ";
            this.Text = "危急值统计查询";
            this.Load += new System.EventHandler(this.JYQueryForm_WJZ_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReportImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.samDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fstate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjytype_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fsample_type_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_user_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fapply_dept_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com_listBindingSource_fjy_user_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceResult)).EndInit();
            this.groupBoxif.ResumeLayout(false);
            this.groupBoxif.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIS_REPORT_IMGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private samDataSet samDataSet;
        private System.Windows.Forms.BindingSource lIS_REPORTBindingSource;
        private System.Windows.Forms.BindingSource com_listBindingSource_fstate;
        private System.Windows.Forms.BindingSource com_listBindingSource_fjytype_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fsample_type_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fjy_user_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fapply_user_id;
        private System.Windows.Forms.BindingSource com_listBindingSource_fapply_dept_id;
        private System.Windows.Forms.BindingSource bindingSourceResult;
        private System.Windows.Forms.GroupBox groupBoxif;
        private System.Windows.Forms.ComboBox cobx病人类别;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        private System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox fjy_instrComboBox;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        protected System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrint;
        private System.Windows.Forms.ToolStripButton toolStripButtonPrintYL;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingSource lIS_REPORT_IMGBindingSource;
        private System.Windows.Forms.DataGridView dataGridViewReport;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_instr;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_yb_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_zyh;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_bed;
        private System.Windows.Forms.DataGridViewTextBoxColumn 性别;
        private System.Windows.Forms.DataGridViewTextBoxColumn fhz_age;
        private System.Windows.Forms.DataGridViewTextBoxColumn 年龄单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 科室;
        private System.Windows.Forms.DataGridViewTextBoxColumn 样本;
        private System.Windows.Forms.DataGridViewTextBoxColumn 检验医师;
        private System.Windows.Forms.DataGridViewTextBoxColumn fprint_zt;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_zt;
        private System.Windows.Forms.DataGridViewTextBoxColumn 类型;
        private System.Windows.Forms.DataGridViewTextBoxColumn fapply_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fjy_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn 患者ID;
        private System.Windows.Forms.ToolStripButton toolStripButtonExportToXls;
        private System.Windows.Forms.Button btn_推送化验结果;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fvalue;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_badge;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_ref;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_jgref;
        private System.Windows.Forms.DataGridViewTextBoxColumn fod;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcutoff;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fresult_id;
    }
}