﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.lisbll.sam;
using yunLis.wwfbll;

namespace yunLis.lis.sam.jy
{
    public partial class JYQueryForm_WJZAddForm : Form
    {
        int intEdit = 0;//0为新增，1为修改
        WJZ_reportmodel wjzmodel = new WJZ_reportmodel();
        public JYQueryForm_WJZAddForm(WJZ_reportmodel model)
        {
            InitializeComponent();
            //wjzmodel = model;

            fdj_date.DataBindings.Add("Text", model, "fdj_date");
            fhz_name.DataBindings.Add("Text", model, "fhz_name");
            fhz_sex.DataBindings.Add("Text", model, "fhz_sex");
            fhz_age.DataBindings.Add("Text", model, "fhz_age");
            fhz_age_unit.DataBindings.Add("Text", model, "fhz_age_unit");
            fhz_zyh.DataBindings.Add("Text", model, "fhz_zyh");
            fwjz_nr.DataBindings.Add("Text", model, "fwjz_nr");
            fbg_name.DataBindings.Add("Text", model, "fbg_name");
            fbg_date1.DataBindings.Add("Text", model, "fbg_date1");
            fbg_date2.DataBindings.Add("Text", model, "fbg_date2");
            fjs_dept.DataBindings.Add("Text", model, "fjs_dept");
            fjs_tel.DataBindings.Add("Text", model, "fjs_tel");
        }

        private void JYQueryForm_WJZAddForm_Load(object sender, EventArgs e)
        {
            try
            {
                //this.fdept_idTextBox.Text = bllDept.BllDeptNameByID(strDeptID);
                //fjy_dateDateTimePicker1.Text = wjzmodel.fdj_date;

                if (intEdit == 0)
                {
                }
                else if (intEdit == 1)
                {
                }
            
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void SaveUpdate()
        {
            try
            {               
                if (this.fhz_age_unit.Text == "" || this.fhz_age_unit.Text == null)
                {
                    yunLis.wwfbll.WWMessage.MessageShowWarning("姓名不能为空，请填写后重试！");
                    return;
                }                
                else
                {                   
                   // dtPerson.Rows[0]["fname"] = this.fnameTextBox.Text;
                   //// dtPerson.Rows[0]["fpass"] = ww.wwf.com.Security.StrToEncrypt("MD5", this.fpassTextBox.Text);
                   // dtPerson.Rows[0]["fname_e"] = this.fname_eTextBox.Text;
                   // dtPerson.Rows[0]["forder_by"] = this.forder_byTextBox.Text;
                   // dtPerson.Rows[0]["ftype"] = this.ftypeTextBox.Text;              
                   // dtPerson.Rows[0]["fremark"] = this.fremarkTextBox.Text;
                   // dtPerson.Rows[0]["fshow_name"] = "(" + dtPerson.Rows[0]["fperson_id"].ToString() + ")" + this.fnameTextBox.Text;
                   // this.Validate();
                   // if (this.bll.BllUpdate(dtPerson) > 0)
                   // {
                   //     ww.wwf.wwfbll.WWMessage.MessageShowInformation("用户修改成功！");
                   //     this.Close();
                   // }
                }
            }
            catch(Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void SaveAdd()
        {
            try
            {
                //if (this.fperson_idTextBox.Text == "" || this.fperson_idTextBox.Text == null)
                //{
                //    ww.wwf.wwfbll.WWMessage.MessageShowWarning("登录名不能为空，请填写后重试！");
                //    return;
                //}
                //if (this.fnameTextBox.Text == "" || this.fnameTextBox.Text == null)
                //{
                //    ww.wwf.wwfbll.WWMessage.MessageShowWarning("姓名不能为空，请填写后重试！");
                //    return;
                //}
                ////if (this.bll.BllPersonExists(this.fperson_idTextBox.Text))
                ////{
                ////    ww.wwf.wwfbll.WWMessage.MessageShowWarning("登录名为" + this.fperson_idTextBox.Text + "的用户已经存在！");
                ////    return;
                ////}
                //else
                //{

                //   // dtPerson.Rows[0]["fperson_id"] = this.fperson_idTextBox.Text;
                //   // dtPerson.Rows[0]["fdept_id"] = this.strDeptID;
                //   // dtPerson.Rows[0]["fname"] = this.fnameTextBox.Text;
                //   // dtPerson.Rows[0]["fpass"] = ww.wwf.com.Security.StrToEncrypt("MD5", this.fpassTextBox.Text);
                //   // dtPerson.Rows[0]["fname_e"] = this.fname_eTextBox.Text;
                //   // dtPerson.Rows[0]["forder_by"] = this.forder_byTextBox.Text;
                //   // dtPerson.Rows[0]["ftype"] = this.ftypeTextBox.Text;


                //   // dtPerson.Rows[0]["fhelp_code"] = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(fnameTextBox.Text);

                //   //// sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim());


                //   // dtPerson.Rows[0]["fremark"] = this.fremarkTextBox.Text;
                //   // dtPerson.Rows[0]["fshow_name"] = "(" + this.fperson_idTextBox.Text + ")" + this.fnameTextBox.Text;

                //   // this.Validate();
                //   // if (this.bll.BllAdd(dtPerson) > 0)
                //   // {
                //   //     ww.wwf.wwfbll.WWMessage.MessageShowInformation("用户增加成功！");
                //   //     AddNewRow();
                //   // }
                //}
            }
            catch(Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError("请新增功能后再保存！"+ex.Message.ToString());
            }
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            if (intEdit == 0)
            {
                SaveAdd();
            }
            else
            {
                SaveUpdate();
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void butReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}