﻿namespace yunLis.lis.sam.sq
{
    partial class ApplyQueryNewForm2
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplyQueryNewForm2));
            this.check包含已打印 = new System.Windows.Forms.CheckBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSFZH = new System.Windows.Forms.TextBox();
            this.textBox门诊住院号 = new System.Windows.Forms.TextBox();
            this.txt申请科室 = new System.Windows.Forms.ComboBox();
            this.comboBox病人类型 = new System.Windows.Forms.ComboBox();
            this.fjy_dateDateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fjy_dateDateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.gridControl_SamApply = new DevExpress.XtraGrid.GridControl();
            this.gridView_SamApply = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton危急值查看 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton查询 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton打印 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton关闭 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton打印病人全部条码 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton打印预览 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_SamApply)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_SamApply)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            this.SuspendLayout();
            // 
            // check包含已打印
            // 
            this.check包含已打印.Location = new System.Drawing.Point(1099, 43);
            this.check包含已打印.Name = "check包含已打印";
            this.check包含已打印.Size = new System.Drawing.Size(147, 20);
            this.check包含已打印.TabIndex = 27;
            this.check包含已打印.Text = "包含已打印";
            this.check包含已打印.UseVisualStyleBackColor = true;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(441, 68);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(283, 20);
            this.textBoxName.TabIndex = 25;
            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // textBoxSFZH
            // 
            this.textBoxSFZH.Location = new System.Drawing.Point(84, 68);
            this.textBoxSFZH.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxSFZH.Name = "textBoxSFZH";
            this.textBoxSFZH.Size = new System.Drawing.Size(281, 20);
            this.textBoxSFZH.TabIndex = 25;
            this.textBoxSFZH.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // textBox门诊住院号
            // 
            this.textBox门诊住院号.Location = new System.Drawing.Point(800, 68);
            this.textBox门诊住院号.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox门诊住院号.Name = "textBox门诊住院号";
            this.textBox门诊住院号.Size = new System.Drawing.Size(282, 20);
            this.textBox门诊住院号.TabIndex = 25;
            this.textBox门诊住院号.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox门诊住院号_KeyPress);
            // 
            // txt申请科室
            // 
            this.txt申请科室.DisplayMember = "fname";
            this.txt申请科室.FormattingEnabled = true;
            this.txt申请科室.Location = new System.Drawing.Point(816, 43);
            this.txt申请科室.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txt申请科室.Name = "txt申请科室";
            this.txt申请科室.Size = new System.Drawing.Size(279, 21);
            this.txt申请科室.TabIndex = 21;
            this.txt申请科室.ValueMember = "fname";
            // 
            // comboBox病人类型
            // 
            this.comboBox病人类型.DisplayMember = "fname";
            this.comboBox病人类型.FormattingEnabled = true;
            this.comboBox病人类型.Location = new System.Drawing.Point(480, 43);
            this.comboBox病人类型.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox病人类型.Name = "comboBox病人类型";
            this.comboBox病人类型.Size = new System.Drawing.Size(270, 21);
            this.comboBox病人类型.TabIndex = 21;
            this.comboBox病人类型.ValueMember = "fname";
            // 
            // fjy_dateDateTimePicker2
            // 
            this.fjy_dateDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker2.Location = new System.Drawing.Point(272, 43);
            this.fjy_dateDateTimePicker2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.fjy_dateDateTimePicker2.Name = "fjy_dateDateTimePicker2";
            this.fjy_dateDateTimePicker2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker2.Size = new System.Drawing.Size(147, 23);
            this.fjy_dateDateTimePicker2.TabIndex = 17;
            // 
            // fjy_dateDateTimePicker1
            // 
            this.fjy_dateDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fjy_dateDateTimePicker1.Location = new System.Drawing.Point(84, 43);
            this.fjy_dateDateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.fjy_dateDateTimePicker1.Name = "fjy_dateDateTimePicker1";
            this.fjy_dateDateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fjy_dateDateTimePicker1.Size = new System.Drawing.Size(175, 23);
            this.fjy_dateDateTimePicker1.TabIndex = 15;
            // 
            // gridControl_SamApply
            // 
            this.gridControl_SamApply.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControl_SamApply.Location = new System.Drawing.Point(12, 92);
            this.gridControl_SamApply.MainView = this.gridView_SamApply;
            this.gridControl_SamApply.Name = "gridControl_SamApply";
            this.gridControl_SamApply.Size = new System.Drawing.Size(1257, 560);
            this.gridControl_SamApply.TabIndex = 138;
            this.gridControl_SamApply.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView_SamApply});
            // 
            // gridView_SamApply
            // 
            this.gridView_SamApply.GridControl = this.gridControl_SamApply;
            this.gridView_SamApply.Name = "gridView_SamApply";
            this.gridView_SamApply.OptionsView.ShowGroupPanel = false;
            // 
            // gridControl2
            // 
            this.gridControl2.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControl2.Location = new System.Drawing.Point(1273, 92);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(168, 560);
            this.gridControl2.TabIndex = 137;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton危急值查看);
            this.layoutControl1.Controls.Add(this.check包含已打印);
            this.layoutControl1.Controls.Add(this.textBox门诊住院号);
            this.layoutControl1.Controls.Add(this.textBoxName);
            this.layoutControl1.Controls.Add(this.simpleButton查询);
            this.layoutControl1.Controls.Add(this.textBoxSFZH);
            this.layoutControl1.Controls.Add(this.simpleButton打印);
            this.layoutControl1.Controls.Add(this.simpleButton关闭);
            this.layoutControl1.Controls.Add(this.simpleButton打印病人全部条码);
            this.layoutControl1.Controls.Add(this.simpleButton打印预览);
            this.layoutControl1.Controls.Add(this.gridControl2);
            this.layoutControl1.Controls.Add(this.txt申请科室);
            this.layoutControl1.Controls.Add(this.gridControl_SamApply);
            this.layoutControl1.Controls.Add(this.comboBox病人类型);
            this.layoutControl1.Controls.Add(this.fjy_dateDateTimePicker1);
            this.layoutControl1.Controls.Add(this.fjy_dateDateTimePicker2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(263, 392, 812, 500);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1453, 664);
            this.layoutControl1.TabIndex = 140;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton危急值查看
            // 
            this.simpleButton危急值查看.Location = new System.Drawing.Point(1206, 12);
            this.simpleButton危急值查看.Name = "simpleButton危急值查看";
            this.simpleButton危急值查看.Size = new System.Drawing.Size(173, 22);
            this.simpleButton危急值查看.StyleController = this.layoutControl1;
            this.simpleButton危急值查看.TabIndex = 145;
            this.simpleButton危急值查看.Text = "危急值查看";
            this.simpleButton危急值查看.Click += new System.EventHandler(this.simpleButton危急值查看_Click);
            // 
            // simpleButton查询
            // 
            this.simpleButton查询.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton查询.ImageOptions.Image")));
            this.simpleButton查询.Location = new System.Drawing.Point(830, 12);
            this.simpleButton查询.Name = "simpleButton查询";
            this.simpleButton查询.Size = new System.Drawing.Size(86, 27);
            this.simpleButton查询.StyleController = this.layoutControl1;
            this.simpleButton查询.TabIndex = 143;
            this.simpleButton查询.Text = "查询(F8)";
            this.simpleButton查询.Click += new System.EventHandler(this.simpleButton查询_Click);
            // 
            // simpleButton打印
            // 
            this.simpleButton打印.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton打印.ImageOptions.Image")));
            this.simpleButton打印.Location = new System.Drawing.Point(1040, 12);
            this.simpleButton打印.Name = "simpleButton打印";
            this.simpleButton打印.Size = new System.Drawing.Size(58, 27);
            this.simpleButton打印.StyleController = this.layoutControl1;
            this.simpleButton打印.TabIndex = 142;
            this.simpleButton打印.Text = "打印";
            this.simpleButton打印.Click += new System.EventHandler(this.tb打印_Click);
            // 
            // simpleButton关闭
            // 
            this.simpleButton关闭.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton关闭.ImageOptions.Image")));
            this.simpleButton关闭.Location = new System.Drawing.Point(1383, 12);
            this.simpleButton关闭.Name = "simpleButton关闭";
            this.simpleButton关闭.Size = new System.Drawing.Size(58, 27);
            this.simpleButton关闭.StyleController = this.layoutControl1;
            this.simpleButton关闭.TabIndex = 141;
            this.simpleButton关闭.Text = "关闭";
            this.simpleButton关闭.Click += new System.EventHandler(this.simpleButton关闭_Click);
            // 
            // simpleButton打印病人全部条码
            // 
            this.simpleButton打印病人全部条码.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton打印病人全部条码.ImageOptions.Image")));
            this.simpleButton打印病人全部条码.Location = new System.Drawing.Point(1102, 12);
            this.simpleButton打印病人全部条码.Name = "simpleButton打印病人全部条码";
            this.simpleButton打印病人全部条码.Size = new System.Drawing.Size(100, 27);
            this.simpleButton打印病人全部条码.StyleController = this.layoutControl1;
            this.simpleButton打印病人全部条码.TabIndex = 140;
            this.simpleButton打印病人全部条码.Text = "打印全部条码";
            this.simpleButton打印病人全部条码.Click += new System.EventHandler(this.simpleButton打印病人全部条码_Click);
            // 
            // simpleButton打印预览
            // 
            this.simpleButton打印预览.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton打印预览.ImageOptions.Image")));
            this.simpleButton打印预览.Location = new System.Drawing.Point(920, 12);
            this.simpleButton打印预览.Name = "simpleButton打印预览";
            this.simpleButton打印预览.Size = new System.Drawing.Size(116, 27);
            this.simpleButton打印预览.StyleController = this.layoutControl1;
            this.simpleButton打印预览.TabIndex = 139;
            this.simpleButton打印预览.Text = "打印预览(F3)";
            this.simpleButton打印预览.Click += new System.EventHandler(this.tb打印_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.emptySpaceItem3,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem17});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1453, 664);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(818, 31);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl_SamApply;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1261, 564);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl2;
            this.layoutControlItem2.Location = new System.Drawing.Point(1261, 80);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(172, 564);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(1238, 31);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(195, 25);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton打印预览;
            this.layoutControlItem3.Location = new System.Drawing.Point(908, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(120, 31);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(120, 31);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(120, 31);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton打印病人全部条码;
            this.layoutControlItem4.Location = new System.Drawing.Point(1090, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(104, 0);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(104, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(104, 31);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton关闭;
            this.layoutControlItem5.Location = new System.Drawing.Point(1371, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(62, 31);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(62, 31);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(62, 31);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton打印;
            this.layoutControlItem6.Location = new System.Drawing.Point(1028, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(62, 31);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(62, 31);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(62, 31);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButton查询;
            this.layoutControlItem7.Location = new System.Drawing.Point(818, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(90, 31);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(90, 31);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(90, 31);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.fjy_dateDateTimePicker1;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(251, 25);
            this.layoutControlItem8.Text = "日期范围:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(69, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.fjy_dateDateTimePicker2;
            this.layoutControlItem9.Location = new System.Drawing.Point(251, 31);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(160, 25);
            this.layoutControlItem9.Text = "-";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(4, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.comboBox病人类型;
            this.layoutControlItem10.Location = new System.Drawing.Point(411, 31);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(331, 25);
            this.layoutControlItem10.Text = "病人类型:";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(52, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt申请科室;
            this.layoutControlItem11.Location = new System.Drawing.Point(742, 31);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(345, 25);
            this.layoutControlItem11.Text = "科室/部门:";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(57, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.check包含已打印;
            this.layoutControlItem12.Location = new System.Drawing.Point(1087, 31);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(151, 25);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(1074, 56);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(359, 24);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.textBoxSFZH;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(357, 24);
            this.layoutControlItem13.Text = "身份证号:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(69, 14);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.textBoxName;
            this.layoutControlItem14.Location = new System.Drawing.Point(357, 56);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(359, 24);
            this.layoutControlItem14.Text = "病人姓名:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(69, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.textBox门诊住院号;
            this.layoutControlItem15.Location = new System.Drawing.Point(716, 56);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(358, 24);
            this.layoutControlItem15.Text = "门诊/住院号:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(69, 14);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.simpleButton危急值查看;
            this.layoutControlItem17.Location = new System.Drawing.Point(1194, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(177, 31);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            this.layoutControlItem17.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // ApplyQueryNewForm2
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1453, 664);
            this.Controls.Add(this.layoutControl1);
            this.Font = new System.Drawing.Font("宋体", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ApplyQueryNewForm2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "申请查询基础窗口";
            this.Load += new System.EventHandler(this.ApplyQueryNewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl_SamApply)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView_SamApply)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker2;
        protected System.Windows.Forms.DateTimePicker fjy_dateDateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox病人类型;
        private System.Windows.Forms.TextBox textBox门诊住院号;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSFZH;
        private System.Windows.Forms.ComboBox txt申请科室;
        private System.Windows.Forms.CheckBox check包含已打印;
        private DevExpress.XtraGrid.GridControl gridControl_SamApply;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView_SamApply;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton查询;
        private DevExpress.XtraEditors.SimpleButton simpleButton打印;
        private DevExpress.XtraEditors.SimpleButton simpleButton关闭;
        private DevExpress.XtraEditors.SimpleButton simpleButton打印病人全部条码;
        private DevExpress.XtraEditors.SimpleButton simpleButton打印预览;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.SimpleButton simpleButton危急值查看;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
    }
}