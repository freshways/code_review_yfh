﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwf;
using yunLis.lisbll.sam;
using yunLis.wwfbll;
using System.Collections;
using yunLis.lis.com;
 
using System.Runtime.InteropServices;
using System.IO;
namespace yunLis.lis.sam.sq
{
    public partial class ApplyForm : SysBaseForm
    {
        PatientBLL bllPatient = new PatientBLL();
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        DeptBll bllDept = new DeptBll();//部门
        TypeBLL bllType = new TypeBLL();//公共类型逻辑  
        DataTable dtSelectOK = new DataTable();//已经选择的项目
        ApplyBLL bllApply = new ApplyBLL();//申请规则
        private string str申请单id = "";


        public ApplyForm()
        {
            InitializeComponent();
            WWComTypeList();
            dataGridViewAllItem.AutoGenerateColumns = false;
            DataGridViewObject.AutoGenerateColumns = false;
            dataGridViewOK.AutoGenerateColumns = false;
        }

        private void ApplyForm_Load(object sender, EventArgs e)
        {
            try
            {

                CreatedtSelectOK();
                this.cbftype_id.SelectedValue = "门诊";
                GetTreeHZ();
                GetTreeCheckType("-1");

                this.tbfapply_user_id.Text = LoginBLL.strPersonID;
                this.tbfapply_user_name.Text= LoginBLL.strPersonName;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        #region  业务方法
        /// <summary>
        /// 创建已经选择项目临时表
        /// </summary>
        private void CreatedtSelectOK()
        {
            try
            {
                DataColumn dataColumn1 = new DataColumn("fitem_id", typeof(string));
                dtSelectOK.Columns.Add(dataColumn1);

                DataColumn dataColumn2 = new DataColumn("fitem_code", typeof(string));
                dtSelectOK.Columns.Add(dataColumn2);

                DataColumn dataColumn3 = new DataColumn("fitem_name", typeof(string));
                dtSelectOK.Columns.Add(dataColumn3);
                DataColumn dataColumn4 = new DataColumn("fitem_unit", typeof(string));
                dtSelectOK.Columns.Add(dataColumn4);

                DataColumn dataColumn5 = new DataColumn("forder_by", typeof(string));
                dtSelectOK.Columns.Add(dataColumn5);
                
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 公共列表
        /// </summary>
        private void WWComTypeList()
        {
            try
            {
                this.cbftype_id.DataSource = this.bllType.BllComTypeDT("病人类别", 1,"");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得检验类别树结构
        /// </summary>
        /// <param name="pid"></param>
        public void GetTreeCheckType(string pid)
        {
            try
            {
                this.wwTreeViewCheckType.ZADataTable = this.bllItem.BllCheckAndSampleTypeDT();
                this.wwTreeViewCheckType.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeViewCheckType.ZAParentFieldName = "fpid";//上级字段名称
                this.wwTreeViewCheckType.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeViewCheckType.ZAKeyFieldName = "fid";//主键字段名称
                this.wwTreeViewCheckType.ZAToolTipTextName = "tag";
                this.wwTreeViewCheckType.ZATreeViewShow();//显示树        
               // try
                //{
                  //  this.wwTreeViewCheckType.SelectedNode = wwTreeViewCheckType.Nodes[0];
               // }
               // catch { }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得患者树
        /// </summary>
        public void GetTreeHZ()
        {
            try
            {
                string pid = "-1";
                string fjy_date1 = this.bllPatient.DbDateTime1(fjy_dateDateTimePicker1);
                string fjy_date2 = this.bllPatient.DbDateTime2(fjy_dateDateTimePicker2);
                string strDateif = " and (ftime_registration>='" + fjy_date1 + "' and ftime_registration<='" + fjy_date2 + "')";
                this.wwTreeViewHZ.ZADataTable = this.bllPatient.BllDeptAndPatientDT(strDateif);
                this.wwTreeViewHZ.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeViewHZ.ZAParentFieldName = "fpid";//上级字段名称
                this.wwTreeViewHZ.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeViewHZ.ZAKeyFieldName = "fid";//主键字段名称
                this.wwTreeViewHZ.ZAToolTipTextName = "tag";
                this.wwTreeViewHZ.ZATreeViewShow();//显示树        
                //try
                //{
                   // this.wwTreeViewHZ.SelectedNode = wwTreeViewHZ.Nodes[0];
                //}
                //catch { }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 据患者ID设置患者信息
        /// </summary>
        /// <param name="fhz_id"></param>
        private void SetHZInfo(string fhz_id)
        {
            try
            {
                DataTable dthz = this.bllPatient.BllPatientDTByID(fhz_id);
                if (dthz != null)
                {
                    string fsex = "";//性别
                    string fbirthday = "";//生日
                    string fdept_id = "";//科室

                    if (dthz.Rows[0]["fsex"] != null)
                        fsex = dthz.Rows[0]["fsex"].ToString();
                    if (fsex == "" || fsex == null)
                        fsex = "?";
                    if (dthz.Rows[0]["fbirthday"] != null)
                        fbirthday = dthz.Rows[0]["fbirthday"].ToString();

                    //生日-----------------------
                    string strGetAge = this.bllPatient.BllPatientAge(fbirthday);
                    if (strGetAge == "" || strGetAge == null)
                    {
                        this.tbfage.Text = "0";
                        this.cbfage_unit.SelectedItem = "?";
                    }
                    else
                    {
                        string[] sPatientAge = strGetAge.Split(':');
                        this.tbfage.Text = sPatientAge[1].ToString();
                        this.cbfage_unit.SelectedItem = sPatientAge[0].ToString();
                    }
                    //---------------------------
                    if (dthz.Rows[0]["fdept_id"] != null)
                        fdept_id = dthz.Rows[0]["fdept_id"].ToString();

                    this.tbfdept_id.Text = fdept_id;
                    this.tbfdept_name.Text = bllDept.BllDeptName(fdept_id);

                    this.tbfname.Text = dthz.Rows[0]["fname"].ToString();
                    this.tbfhz_id.Text = dthz.Rows[0]["fhz_id"].ToString();
                    this.tbfhz_zyh.Text = dthz.Rows[0]["fhz_zyh"].ToString();
                    this.tbfbed_num.Text = dthz.Rows[0]["fbed_num"].ToString();
                    this.tbfroom_num.Text = dthz.Rows[0]["froom_num"].ToString();
                    //MessageBox.Show(fsex);
                    this.cbfsex.SelectedItem = fsex;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取消所选项目
        /// </summary>
        /// <param name="intEidt"></param>
        private void DelOKItem(int intEidt)
        {
            try
            {
                if (intEidt == 1)
                {
                    if (WWMessage.MessageDialogResult("确定取消所有已经选择的项目？"))
                    {
                        if (this.dtSelectOK.Rows.Count > 0)
                            dtSelectOK.Clear();
                        this.dataGridViewOK.DataSource = dtSelectOK;
                    }
                }
                else
                {
                    if (this.dtSelectOK.Rows.Count > 0)
                        dtSelectOK.Clear();
                    this.dataGridViewOK.DataSource = dtSelectOK;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        /// <summary>
        /// 取得 项目
        /// </summary>
        private void GetItemOne()
        {
            try
            {

                this.dataGridViewAllItem.DataSource = this.bllItem.BllItem(" fcheck_type_id='" + strfcheck_type_id + "' and fsam_type_id='" + strfsample_type_id + "'");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 取得 项目组
        /// </summary>
        private void GetItemGroup()
        {
            try
            {

                this.DataGridViewObject.DataSource = this.bllItem.BllItemGroupDT("fcheck_type_id='" + strfcheck_type_id + "' and fsam_type_id='" + strfsample_type_id + "'");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
       
        #endregion     
        
        private void fjy_dateDateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            GetTreeHZ();
        }

        private void fjy_dateDateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            GetTreeHZ();
        }

        
        private void wwTreeViewHZ_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                string strEdit = "";
                string strfhz_id = "";
                strEdit = e.Node.ToolTipText;
                strfhz_id = e.Node.Name;
                if (strEdit == "patient")
                {
                    SetHZInfo(strfhz_id);
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

       

        private void tbfapply_user_name_DoubleClick(object sender, EventArgs e)
        {

            SelPerson();
        }
        private void SelPerson()
        {
            try
            {
                WindowsResult r = new WindowsResult();
                r.TextChangedString += new yunLis.lis.com.TextChangedHandlerString(this.EventResultChangedString);
                WindowsPersonForm fc = new WindowsPersonForm(r);
                fc.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void SelDept()
        {
            try
            {
                WindowsResult r = new WindowsResult();
                r.TextChangedString += new yunLis.lis.com.TextChangedHandlerString(this.EventResultChangedStringDept);

                WindowsDeptForm fca = new WindowsDeptForm(r);
                fca.ShowDialog();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }   /// <summary>
        /// 返回表ID
        /// </summary>
        /// <param name="s"></param>
        private void EventResultChangedStringDept(string s)
        {
            try
            {
                this.tbfdept_id.Text = s;
                this.tbfdept_name.Text = bllDept.BllDeptName(s);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 返回表ID
        /// </summary>
        /// <param name="s"></param>
        private void EventResultChangedString(string s)
        {
            try
            {
                this.tbfapply_user_id.Text = s;
                this.tbfapply_user_name.Text = bllDept.BllPersonName(s);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonuser_id_Click(object sender, EventArgs e)
        {
            SelPerson();
        }

        private void buttonDept_Click(object sender, EventArgs e)
        {
            SelDept();
        }
        private void tbfdept_name_DoubleClick(object sender, EventArgs e)
        {
            SelDept();
        }
        string strfcheck_type_id = "";
        string strfsample_type_id = "";
        private void wwTreeViewCheckType_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (this.dtSelectOK.Rows.Count > 0)
                dtSelectOK.Clear();
            this.dataGridViewOK.DataSource = dtSelectOK;
            strfcheck_type_id = "";
            strfsample_type_id = "";
            string strEdit = e.Node.ToolTipText;
            if (strEdit == "CheckType")
            {
                //bN.Enabled = false;
            }
            else
            {
                //bN.Enabled = true;
                strfcheck_type_id = e.Node.Parent.Name.ToString();
                strfsample_type_id = e.Node.Name.ToString();
                GetItemGroup();
                GetItemOne();
                
            }
        }



        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridViewAllItem_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void DataGridViewObject_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    for (int i = 0; i < DataGridViewObject.Rows.Count; i++)
                    {
                        DataGridViewObject.Rows[i].Cells["sel"].Value = 0;
                        DataGridViewObject.CurrentRow.Cells["sel"].Value = 1;
                    }
                    int selOK = 0;
                    //选择否
                    if (DataGridViewObject.CurrentRow.Cells["sel"].Value != null)
                        selOK = Convert.ToInt32(DataGridViewObject.CurrentRow.Cells["sel"].Value);
                    if (selOK == 1)
                    {
                        DataTable dtGroupOk = this.bllItem.BllItemAndGroupRefDT(DataGridViewObject.CurrentRow.Cells["fgroup_id"].Value.ToString());
                        for (int ii = 0; ii < dtGroupOk.Rows.Count; ii++)
                        {
                            string stritemid = dtGroupOk.Rows[ii]["fitem_id"].ToString();
                            DataRow[] currcolumXS = this.dtSelectOK.Select("fitem_id='" + stritemid + "'");
                            if (currcolumXS.Length > 0)
                            {
                            }
                            else
                            {
                                DataRow drNew = dtSelectOK.NewRow();
                                if (dtGroupOk.Rows[ii]["fitem_id"] != null)
                                    drNew["fitem_id"] = dtGroupOk.Rows[ii]["fitem_id"].ToString();
                                if (dtGroupOk.Rows[ii]["fitem_code"] != null)
                                    drNew["fitem_code"] = dtGroupOk.Rows[ii]["fitem_code"].ToString();
                                if (dtGroupOk.Rows[ii]["fname"] != null)
                                    drNew["fitem_name"] = dtGroupOk.Rows[ii]["fname"].ToString();
                                if (dtGroupOk.Rows[ii]["funit_name_zw"] != null)
                                    drNew["fitem_unit"] = dtGroupOk.Rows[ii]["funit_name_zw"].ToString();
                                if (dtGroupOk.Rows[ii]["fprint_num"] != null)
                                    drNew["forder_by"] = dtGroupOk.Rows[ii]["fprint_num"].ToString();
                                dtSelectOK.Rows.Add(drNew);
                            }
                        }
                        this.dataGridViewOK.DataSource = dtSelectOK;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }

        private void dataGridViewAllItem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    for (int i = 0; i < dataGridViewAllItem.Rows.Count; i++)
                    {
                        dataGridViewAllItem.Rows[i].Cells["sel_item"].Value = 0;
                        dataGridViewAllItem.CurrentRow.Cells["sel_item"].Value = 1;
                    }
                    int selOK = 0;
                    //选择否
                    if (dataGridViewAllItem.CurrentRow.Cells["sel_item"].Value != null)
                        selOK = Convert.ToInt32(dataGridViewAllItem.CurrentRow.Cells["sel_item"].Value);
                    if (selOK == 1)
                    {
                        string stritemid = dataGridViewAllItem.CurrentRow.Cells["fitem_id"].Value.ToString();
                        string strfitem_code = "";
                        string strfname_item = "";
                        string strfitem_unit = "";
                        string strforder_by = "";
                        if (dataGridViewAllItem.CurrentRow.Cells["fitem_code"].Value != null)
                            strfitem_code = dataGridViewAllItem.CurrentRow.Cells["fitem_code"].Value.ToString();
                        if (dataGridViewAllItem.CurrentRow.Cells["fname_item"].Value != null)
                            strfname_item = dataGridViewAllItem.CurrentRow.Cells["fname_item"].Value.ToString();
                        if (dataGridViewAllItem.CurrentRow.Cells["funit_name_zw"].Value != null)
                            strfitem_unit = dataGridViewAllItem.CurrentRow.Cells["funit_name_zw"].Value.ToString();
                        if (dataGridViewAllItem.CurrentRow.Cells["forder_by_item"].Value != null)
                            strforder_by = dataGridViewAllItem.CurrentRow.Cells["forder_by_item"].Value.ToString();
                        DataRow[] currcolumXS = this.dtSelectOK.Select("fitem_id='" + stritemid + "'");
                        if (currcolumXS.Length > 0)
                        {
                        }
                        else
                        {
                            DataRow drNew = dtSelectOK.NewRow();
                            drNew["fitem_id"] = stritemid;
                            drNew["fitem_code"] = strfitem_code;
                            drNew["fitem_name"] = strfname_item;
                            drNew["fitem_unit"] = strfitem_unit;
                            drNew["forder_by"] = strforder_by;
                            dtSelectOK.Rows.Add(drNew);
                        }
                        this.dataGridViewOK.DataSource = dtSelectOK;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        
       

        private void toolStripButtonC_Click(object sender, EventArgs e)
        {
            DelOKItem(1);
        }
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
          
            if (keyData == (Keys.F5))
            {
                toolStripButtonS.PerformClick();
                return true;
            }
            if (keyData == (Keys.F2))
            {
                toolStripButtonC.PerformClick();
                return true;
            }
            if (keyData == (Keys.F1))
            {
                toolStripButtonHelp.PerformClick();
                return true;
            }
            
             if (keyData == (Keys.F3))
            {
                toolStripButton1.PerformClick();
                return true;
            }
            
            return base.ProcessCmdKey(ref   msg, keyData);
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                str申请单id = "";
                if (ww.wwf.com.Public.IsNumber(this.tbfage.Text)) 
                { }
                else
                {
                    WWMessage.MessageShowWarning("年龄必须为数字，请填写后重试！");
                    this.ActiveControl = this.tbfage;
                    return;
                }
                if (this.dtSelectOK.Rows.Count > 0)
                { }
                else
                {
                    WWMessage.MessageShowWarning("检验项目不能为空，请选择项目后重试！");                    
                    return;
                }
                if (WWMessage.MessageDialogResult("确定提交申请单？"))
                {
                    string strTime = this.bllApply.DbServerDateTim();
                    ApplyModel applyModel = new ApplyModel();
                    this.str申请单id = this.bllApply.DbNum("sam_fapply_id");
                    applyModel.fapply_id = str申请单id;

                    barcodeNETWindows1.BarcodeText = str申请单id;

                    applyModel.fjytype_id = strfcheck_type_id;
                    applyModel.fsample_type_id = strfsample_type_id;
                    if (radioButtonJZ.Checked)
                        applyModel.fjz_flag = 2;//急诊否
                    else
                        applyModel.fjz_flag = 1;
                    applyModel.fcharge_flag = 0;//收费否
                    applyModel.fstate = "1";//状态
                    applyModel.fapply_user_id = this.tbfapply_user_id.Text;
                    applyModel.fapply_dept_id = this.tbfdept_id.Text;
                    applyModel.fapply_time = strTime;
                    applyModel.ftype_id = this.cbftype_id.SelectedValue.ToString();
                    applyModel.fhz_id = this.tbfhz_id.Text;
                    applyModel.fhz_zyh = this.tbfhz_zyh.Text;
                    applyModel.fsex = this.cbfsex.SelectedItem.ToString();
                    applyModel.fname = this.tbfname.Text;
                    applyModel.fage = Convert.ToInt32(this.tbfage.Text);
                    applyModel.fage_unit = this.cbfage_unit.SelectedItem.ToString();
                    applyModel.froom_num = this.tbfroom_num.Text;
                    applyModel.fbed_num = this.tbfbed_num.Text;
                    applyModel.fdiagnose = this.tbfdiagnose.Text;
                    applyModel.fcreate_user_id = LoginBLL.strPersonID;
                    applyModel.fcreate_time = strTime;
                    applyModel.fupdate_user_id = LoginBLL.strPersonID;
                    applyModel.fupdate_time = strTime;
                    //
                    applyModel.fxhdb = 0;
                    applyModel.fxyld = 0;
                    applyModel.fheat = 0;
                    applyModel.fage_day = 0;
                    applyModel.fage_month = 0;
                    applyModel.fage_year = 0;
                    applyModel.fjyf = 0;//检验费                   
                    //
                    string stritemgroupname = "";
                    if (DataGridViewObject.Rows.Count > 0)
                    {
                        if (DataGridViewObject.CurrentRow.Cells["fname"].Value != null)
                            stritemgroupname = DataGridViewObject.CurrentRow.Cells["fname"].Value.ToString();
                    }
                    string strSave = this.bllApply.BllApplyAdd(applyModel, this.dtSelectOK, stritemgroupname);
                    if (strSave == "true")
                    {
                        //保存检验申请条码号    
                        //  dr["FBarCodeImg"] = barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG); ;//条形码  
                        int strSaveBCode = this.bllApply.BllApplyBCodeAdd(str申请单id, barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG)); //条形码  );

                        DelOKItem(0);
                    }
                    else
                    {
                        WWMessage.MessageShowError(strSave);
                    }

                    WWPrint(1);
                    /*
                     Name	Code	Data Type	Primary	Foreign Key	Mandatory
申请_id/申请条码号	fapply_id	varchar(32)	TRUE	FALSE	TRUE
检验类型_id	fjytype_id	varchar(32)	FALSE	FALSE	FALSE
样本类型_id	fsample_type_id	varchar(32)	FALSE	FALSE	FALSE
急诊否	fjz_flag	int	FALSE	FALSE	FALSE
收费否	fcharge_flag	int	FALSE	FALSE	FALSE
费别_id	fcharge_id	varchar(32)	FALSE	FALSE	FALSE
状态	fstate	varchar(32)	FALSE	FALSE	FALSE
检验费	fjyf	float	FALSE	FALSE	FALSE
R申请人	fapply_user_id	varchar(32)	FALSE	FALSE	FALSE
R申请科室_id	fapply_dept_id	varchar(32)	FALSE	FALSE	FALSE
R申请时间	fapply_time	varchar(32)	FALSE	FALSE	FALSE
P患者类型_id	ftype_id	varchar(32)	FALSE	FALSE	FALSE
P患者_id	fhz_id	varchar(32)	FALSE	FALSE	FALSE
P住院号	fhz_zyh	varchar(32)	FALSE	FALSE	FALSE
P性别	fsex	varchar(32)	FALSE	FALSE	FALSE
P患者姓名	fname	varchar(32)	FALSE	FALSE	FALSE
P民族	fvolk	varchar(32)	FALSE	FALSE	FALSE
P婚姻	fhymen	varchar(32)	FALSE	FALSE	FALSE
P年龄	fage	int	FALSE	FALSE	FALSE
P职业	fjob	varchar(32)	FALSE	FALSE	FALSE
P过敏史	Pfgms	varchar(100)	FALSE	FALSE	FALSE
P工作单位	fjob_org	varchar(128)	FALSE	FALSE	FALSE
P住址	fadd	varchar(200)	FALSE	FALSE	FALSE
PTel	ftel	varchar(64)	FALSE	FALSE	FALSE
P年龄单位	fage_unit	varchar(32)	FALSE	FALSE	FALSE
F年龄(年)	fage_year	int	FALSE	FALSE	FALSE
F年龄(月)	fage_month	int	FALSE	FALSE	FALSE
F年龄(天)	fage_day	int	FALSE	FALSE	FALSE
P病区	fward_num	varchar(32)	FALSE	FALSE	FALSE
P房间号	froom_num	varchar(32)	FALSE	FALSE	FALSE
P床号	fbed_num	varchar(32)	FALSE	FALSE	FALSE
PABO血型	fblood_abo	varchar(32)	FALSE	FALSE	FALSE
PRH血型	fblood_rh	varchar(32)	FALSE	FALSE	FALSE
P检验目的	fjymd	varchar(200)	FALSE	FALSE	FALSE
P临床诊断	fdiagnose	varchar(200)	FALSE	FALSE	FALSE
P体温	fheat	float	FALSE	FALSE	FALSE
P吸氧浓度	fxyld	float	FALSE	FALSE	FALSE
P曾用药	fcyy	varchar(200)	FALSE	FALSE	FALSE
P血红蛋白	fxhdb	int	FALSE	FALSE	FALSE
创建人_id	fcreate_user_id	varchar(32)	FALSE	FALSE	FALSE
创建时间	fcreate_time	varchar(32)	FALSE	FALSE	FALSE
修改人_id	fupdate_user_id	varchar(32)	FALSE	FALSE	FALSE
修改时间	fupdate_time	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
                     */
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {
            
            try
            {
                HelpForm help = new HelpForm("c7e684b9348a475880b1085e1d829e71");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

       
        

        private void tbfhz_id_TextChanged(object sender, EventArgs e)
        {

        }


        #region 打印
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType"></param>
        private void WWPrint(int printType)
        {
            try
            {
                ApplyPrint pr = new ApplyPrint();
                pr.BllPrintViewer(printType, this.str申请单id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        #endregion

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            WWPrint(1);
          //  barcodeNETWindows1.BarcodeText = "9999";
          //  dr["FBarCodeImg"] = barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG); ;//条形码  
        }

        
       
    }
}