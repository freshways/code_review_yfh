﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using yunLis.wwf.print;
using yunLis.wwfbll;
using yunLis.lisbll.sam;
using System.Windows.Forms;

namespace yunLis.lis.sam.sq
{
    public class ApplyPrint
    { 
        /// <summary>
        /// 报告规则
        /// </summary>
        
        private InstrBLL bllInstr = new InstrBLL();
        private PersonBLL bllPerson = new PersonBLL();
        private SampleTypeBLL bllSample = new SampleTypeBLL();
        DeptBll bllDept = new DeptBll();
        DataSet dsPrint = new DataSet();
        samDataSet.sam_applyDataTable dtPrintDataTable = new samDataSet.sam_applyDataTable();
        string strMainDataSourceName = "samDataSet_sam_apply";//sam_jy_printDataTable
        public ApplyPrint()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
           // dtPrintDataTable.Columns.Remove("条码");            
           // DataColumn D1 = new DataColumn("条码", typeof(System.Byte[]));
           // dtPrintDataTable.Columns.Add(D1);
             
        }
        /// <summary>
        /// 打印 条码
        /// </summary>
        /// <param name="printType">1：打印预览；0：打印</param>
        /// <param name="strfsample_id">样本ID</param>
        public void BllPrintApplyBCode(int printType, string str申请id)
        {
            try
            {
                //
                string sql = "SELECT  t.fapply_id AS 申请单id, (SELECT z1.fbcode FROM  SAM_APPLY_BCODE z1 WHERE (z1.fapply_id = t.fapply_id )) as 申请单条码,(SELECT  top 1 z2.fname FROM    SAM_TYPE z2 WHERE  (z2.ftype = '病人类别') and z2.fcode=t.fjz_flag) as 送检类型,(SELECT top 1 fname FROM SAM_CHECK_TYPE z3 where z3.fcheck_type_id=t.fjytype_id ) as 检验类型,(SELECT top 1 fname FROM WWF_DEPT z3 where z3.fdept_id=t.fapply_dept_id ) as 科室,t.fname as 姓名,t.fsex as 性别,t.fage as 年龄,t.fage_unit as 年龄单位,t.froom_num as 病室,t.fhz_id as 患者ID,t.fbed_num as 床号,t.fhz_zyh as 住院号,t.fapply_user_id as 申请医师,t.fapply_time as 申请日期  FROM  SAM_APPLY t where  (t.fapply_id = '" + str申请id + "')";
                DataTable dtApply = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql);



                if (dtApply.Rows.Count > 0)
                {
                    string strReportName = "条码打印";//报表名称
                    string strReportCode = "yunLis.lis.sam.sq.ApplyBCodeReport.rdlc";//报表代码


                    DataRow printRow = dtPrintDataTable.NewRow();
                    printRow["申请单id"] = dtApply.Rows[0]["申请单id"];
                    printRow["申请单条码"] = dtApply.Rows[0]["申请单条码"];
                    printRow["医院名称"] = LoginBLL.CustomerName;
                    printRow["检验类型"] = dtApply.Rows[0]["检验类型"];
                    printRow["送检类型"] = dtApply.Rows[0]["送检类型"];
                    printRow["实验室联系电话"] = LoginBLL.CustomerTel;
                    printRow["取报告小时"] = "";

                    printRow["姓名"] = dtApply.Rows[0]["姓名"];
                    printRow["性别"] = dtApply.Rows[0]["性别"];
                    printRow["年龄"] = dtApply.Rows[0]["年龄"];
                    printRow["年龄单位"] = dtApply.Rows[0]["年龄单位"];
                    printRow["科室"] = dtApply.Rows[0]["科室"];
                    printRow["病室"] = dtApply.Rows[0]["病室"];
                    printRow["患者ID"] = dtApply.Rows[0]["患者ID"];
                    printRow["床号"] = dtApply.Rows[0]["床号"];
                    printRow["住院号"] = dtApply.Rows[0]["住院号"];
                    printRow["申请医师"] = dtApply.Rows[0]["申请医师"];
                    printRow["申请日期"] = dtApply.Rows[0]["申请日期"];
                    printRow["收费类型"] = "";
                    printRow["检验费"] = "";
                    printRow["采样费"] = "";
                    printRow["检验费"] = "";

                   

                    dtPrintDataTable.Rows.Add(printRow);
                    dsPrint.Tables.Add(dtPrintDataTable);
                    WWPrintViewer(printType, str申请id, strReportName, strReportCode);
                }
                else
                {
                    WWMessage.MessageShowWarning("申请单号为：" + str申请id + " 的申请单不存在。");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 打印 检验申请
        /// </summary>
        /// <param name="printType">1：打印预览；0：打印</param>
        /// <param name="strfsample_id">样本ID</param>
        public void BllPrintViewer(int printType, string str申请id)
        {
            try
            {
                //
                string sql = "SELECT  t.fapply_id AS 申请单id, (SELECT z1.fbcode FROM  SAM_APPLY_BCODE z1 WHERE (z1.fapply_id = t.fapply_id )) as 申请单条码,(SELECT  top 1 z2.fname FROM    SAM_TYPE z2 WHERE  (z2.ftype = '病人类别') and z2.fcode=t.fjz_flag) as 送检类型,(SELECT top 1 fname FROM SAM_CHECK_TYPE z3 where z3.fcheck_type_id=t.fjytype_id ) as 检验类型,(SELECT top 1 fname FROM WWF_DEPT z3 where z3.fdept_id=t.fapply_dept_id ) as 科室,t.fname as 姓名,t.fsex as 性别,t.fage as 年龄,t.fage_unit as 年龄单位,t.froom_num as 病室,t.fhz_id as 患者ID,t.fbed_num as 床号,t.fhz_zyh as 住院号,t.fapply_user_id as 申请医师,t.fapply_time as 申请日期  FROM  SAM_APPLY t where  (t.fapply_id = '" + str申请id + "')";
                DataTable dtApply= yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql);



                if (dtApply.Rows.Count > 0)
                {
                    string strReportName = "检验申请单";//报表名称
                    string strReportCode = "yunLis.lis.sam.sq.ApplyReport.rdlc";//报表代码


                    DataRow printRow = dtPrintDataTable.NewRow();
                    printRow["申请单id"] = dtApply.Rows[0]["申请单id"];
                    printRow["申请单条码"] = dtApply.Rows[0]["申请单条码"];
                    printRow["医院名称"] = LoginBLL.CustomerName;
                    printRow["检验类型"] = dtApply.Rows[0]["检验类型"];
                    printRow["送检类型"] = dtApply.Rows[0]["送检类型"];
                    printRow["实验室联系电话"] = LoginBLL.CustomerTel;
                    printRow["取报告小时"] = "";

                    printRow["姓名"] = dtApply.Rows[0]["姓名"];
                    printRow["性别"] = dtApply.Rows[0]["性别"];
                    printRow["年龄"] = dtApply.Rows[0]["年龄"];
                    printRow["年龄单位"] = dtApply.Rows[0]["年龄单位"];
                    printRow["科室"] = dtApply.Rows[0]["科室"];
                    printRow["病室"] = dtApply.Rows[0]["病室"];
                    printRow["患者ID"] = dtApply.Rows[0]["患者ID"];
                    printRow["床号"] = dtApply.Rows[0]["床号"];
                    printRow["住院号"] = dtApply.Rows[0]["住院号"];
                    printRow["申请医师"] = dtApply.Rows[0]["申请医师"];
                    printRow["申请日期"] = dtApply.Rows[0]["申请日期"];
                    printRow["收费类型"] = "";
                    printRow["检验费"] = "";
                    printRow["采样费"] = "";
                    printRow["检验费"] = "";

                    //检验项目
                  
                    string sqlItem = "SELECT  * FROM         SAM_JY_RESULT WHERE     (fapply_id = '" + str申请id + "') ORDER BY forder_by";
                    DataTable dtApplyItem = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataTableBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sqlItem);
                    string str检验项目 = "";
                    for (int i = 0; i < dtApplyItem.Rows.Count; i++)
                    {
                        string fitem_name = "";
                        if (dtApplyItem.Rows[i]["fitem_name"] != null)
                        {
                            fitem_name = dtApplyItem.Rows[i]["fitem_name"].ToString();
                        }
                        if (i == 0)
                        {
                            str检验项目 = fitem_name;
                        }
                        else
                        {
                            str检验项目 = str检验项目 + "；  " + fitem_name;
                        }
                    }
                    printRow["检验项目"] = str检验项目;

                    //dtPrintDataTable.Rows[0]["医院地址"] = LoginBLL.CustomerAdd;
                    //dtPrintDataTable.Rows[0]["检验实验室"] = LoginBLL.strDeptName;
                    //dtPrintDataTable.Rows[0]["医院联系电话"] = LoginBLL.CustomerTel;

                    dtPrintDataTable.Rows.Add(printRow);
                    dsPrint.Tables.Add(dtPrintDataTable);
                    WWPrintViewer(printType, str申请id, strReportName, strReportCode);
                }
                else
                {
                    WWMessage.MessageShowWarning("申请单号为：" + str申请id + " 的申请单不存在。");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strReportName">报表数据源名</param>
        /// <param name="strReportPath">报表文档</param>
        private void WWPrintViewer(int printType,string fjy_id,string strReportName, string strReportPath)
        {
            try
            {
               
                if (printType == 1)
                {
                    //报表公共窗口
                    ReportViewer feportviewer = new ReportViewer();
                    //报表数据集
                    feportviewer.MainDataSet = dsPrint;
                    //报表名
                    feportviewer.ReportName = strReportName;
                    //报表数据源名
                    feportviewer.MainDataSourceName = strMainDataSourceName;
                    //报表文档
                    feportviewer.ReportPath = strReportPath;
                    //显示报表窗口
                    feportviewer.ShowDialog();
                    
                }
                else {
                    //报表公共窗口
                    ReportPrint feportviewer = new ReportPrint();
                    //报表数据集
                    feportviewer.MainDataSet = dsPrint;
                    //报表名
                    feportviewer.ReportName = strReportName;
                    //报表数据源名
                    feportviewer.MainDataSourceName = strMainDataSourceName;
                    //报表文档
                    feportviewer.ReportPath = strReportPath;
                    //显示报表窗口
                    feportviewer.ShowDialog();
                   
                }
               
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        

    }
}
