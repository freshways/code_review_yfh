﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using yunLis.wwf;
using yunLis.lisbll.sam;
using yunLis.wwfbll;

namespace yunLis.lis.sam.sq
{
    public partial class RefundableForm : SysBaseForm
    {

        ApplyBLL bllApply = new ApplyBLL();//申请规则
        DataTable dtMain = new DataTable();
        DataTable dtResult = new DataTable();
        private yunLis.wwfbll.UserdefinedFieldUpdateForm gridMain = new yunLis.wwfbll.UserdefinedFieldUpdateForm("8");
        private yunLis.wwfbll.UserdefinedFieldUpdateForm gridResult = new yunLis.wwfbll.UserdefinedFieldUpdateForm("9");


        public RefundableForm()
        {
            InitializeComponent();
        }
        

        private void RefundableForm_Load(object sender, EventArgs e)
        {
            this.dataGridViewApply.DataSource = this.bllApply.Get申请单列表("-1");//初始化病人列表
            this.dataGridViewItem.DataSource = this.bllApply.BllSamplingResultDT("-1");//初始化项目列表
            //绑定显示字段
            this.gridMain.DataGridViewSetStyle(this.dataGridViewApply);
            this.gridResult.DataGridViewSetStyle(this.dataGridViewItem);
        }


        private void Refund_barcode_KeyDown(object sender, KeyEventArgs e)
        {
            this.labelMess.Text = "";
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    GetMailList();                    
                }
                catch (Exception ex)
                {
                    WWMessage.MessageShowError(ex.ToString());
                }
                finally
                {
                    Cursor = Cursors.Arrow;
                }
            }
        }

        #region 方法
        private void GetMailList()
        {
            this.bindingSource1.DataSource = this.bllApply.Get申请单列表(this.Refund_barcode.Text.Trim());
            this.dataGridViewApply.DataSource = this.bindingSource1;
            this.Refund_barcode.Text = "";
            this.ActiveControl = this.Refund_barcode;
            //显示一下明细
            DataRowView rowCurrent = (DataRowView)this.bindingSource1.Current;
            
            if (rowCurrent != null)
            {
                string strfapply_id = rowCurrent["fjy_id"].ToString();
                GetResultList(strfapply_id);
            }
        }
        private void GetResultList(string fapply_id)
        {
            this.dtResult = this.bllApply.Get化验明细(fapply_id);
            this.dataGridViewItem.DataSource = this.dtResult;
        }
        private void DoRefunding()
        {
            if (dataGridViewApply.Rows.Count <= 0) return;
            this.Validate();
            this.bindingSource1.EndEdit();
            IList ilistfsample_id = new ArrayList();
            string  zyh = "";
            for (int i = 0; i < this.dataGridViewApply.Rows.Count; i++)
            {
                string strid = dataGridViewApply.Rows[i].Cells["fapply_id"].Value.ToString();
                ilistfsample_id.Add(strid);
                zyh  = dataGridViewApply.Rows[i].Cells["fhz_zyh"].Value.ToString();
            }
            string strsaveret = this.bllApply.BllDoRefund(ilistfsample_id, zyh);
            if (strsaveret == "true")
            {
                this.labelMess.Text = "取消登记成功！";
                //把明细和列表清空
                this.dataGridViewApply.DataSource = null;
                this.dataGridViewItem.DataSource = null;
            }
            else
                this.labelMess.Text = "取消登记失败！" + strsaveret;
        }
        #endregion

        /// <summary>
        /// 按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDoRefund_Click(object sender, EventArgs e)
        {
            DoRefunding();
        }

        private void bindingSource1_PositionChanged(object sender, EventArgs e)
        {
            try
            {
                DataRowView rowCurrent = (DataRowView)this.bindingSource1.Current;
                if (this.dtResult.Rows.Count > 0)
                    this.dtResult.Clear();
                if (rowCurrent != null)
                {
                    string strfapply_id = rowCurrent["fjy_id"].ToString();
                    GetResultList(strfapply_id);
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        #region contentMenu Click
        private void toolStripMenuItem_applygs_Click(object sender, EventArgs e)
        {
            try
            {
                // UserdefinedFieldUpdateForm userset1 = new UserdefinedFieldUpdateForm(gridMain);//主表ID 报告
                gridMain.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripMenuItem_checkidtemgs_Click(object sender, EventArgs e)
        {
            try
            {
                //UserdefinedFieldUpdateForm userset1 = new UserdefinedFieldUpdateForm(this.gridResult);
                gridResult.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                HelpForm help = new HelpForm("c629e6a854134cb2a24fc6465bc35b77");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void 关闭ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        } 
        #endregion

    }
}
