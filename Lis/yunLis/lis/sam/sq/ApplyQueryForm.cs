﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace yunLis.lis.sam.sq
{
    public partial class ApplyQueryForm : yunLis.lis.com.ApplyQueryBaseForm
    {
        public ApplyQueryForm()
        {
            InitializeComponent();
          
        }

        private void ApplyQueryForm_Load(object sender, EventArgs e)
        {
            this.WWComTypeList();
            this.WWGetTreeHZ();
            this.WWSetShowApply("lis_sam_apply_query_apply");
            this.WWSetShowResult("lis_sam_apply_query_result");

            this.strHelpID = "78b5005b3a4f40b39d8390902c7201b1";

        }
    }
}