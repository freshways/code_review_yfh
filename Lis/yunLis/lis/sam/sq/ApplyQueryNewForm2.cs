﻿using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using HIS.Model;
using HIS.Model.Pojo;
using HIS.Model.Pojo.LIS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TableXtraReport;
using WEISHENG.COMM.PluginsAttribute;
using yunLis.lisbll.sam;
using yunLis.wwfbll;
using static HIS.COMM.ClassEnum;

//从HIS.InterFaceLIS中迁移过来的功能
namespace yunLis.lis.sam.sq
{
    [ClassInfoMarkAttribute(GUID = "C53DEE6A-0CFB-4EFD-9993-87CCEF429C60", 键ID = "C53DEE6A-0CFB-4EFD-9993-87CCEF429C60", 父ID = "72E5C77C-CDC2-469C-9A39-148DF0A489FF", 传递参数 = "",
菜单类型 = "WinForm", 功能名称 = "条码打印2", 程序集名称 = "yunLis", 程序集调用类地址 = "lis.sam.sq.ApplyQueryNewForm2", 全屏打开 = false, 图标名称 = "嘱托维护.png", 显示顺序 = 0, 是否显示 = true)]
    public partial class ApplyQueryNewForm2 : DevExpress.XtraEditors.XtraForm
    {
        protected ApplyBLL bllApply = new ApplyBLL();//申请规则
        //protected string strHelpID = "";//帮助ID
        protected DataTable dtResult = new DataTable();
        List<HIS.Model.Pojo.LIS.Pojo条码打印> listPojoSAM_Apply = new List<HIS.Model.Pojo.LIS.Pojo条码打印>();
        string str打印条码号 = "";
        public ApplyQueryNewForm2()
        {
            InitializeComponent();
        }


        private void ApplyQueryNewForm_Load(object sender, EventArgs e)
        {
            this.Set病人类型();
            Get病人病区();
            txt申请科室.Text = HIS.COMM.zdInfo.ModelUserInfo.科室名称;
            gridControl_SamApply.DataSource = listPojoSAM_Apply;
            HIS.COMM.frmGridCustomize.RegisterGrid(gridView_SamApply);
        }
        private void Get病人病区()
        {
            DataTable dt = new DataTable();
            dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text,
                "select 科室名称,科室编码 from GY科室设置 where 是否住院科室=1 ").Tables[0];

            txt申请科室.Items.Clear();

            foreach (DataRow row in dt.Rows)
            {
                txt申请科室.Items.Add(row["科室名称"]);
            }
        }

        private void Set病人类型()
        {
            string sql = "SELECT * FROM sam_type where (ftype='病人类别' and fuse_if='1')  ORDER BY forder_by ";            //
            DataTable dt = //this.bllType.BllComTypeDT("病人类别", 1, "");
            HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.sLISConnString, CommandType.Text, sql).Tables[0];
            this.comboBox病人类型.DataSource = dt;
        }

        protected void WWComTypeList()
        {
            try
            {

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }



        /// <summary>
        /// 获取申请列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="ChisDBName"></param>
        /// <returns></returns>
        private DataTable BllSamplingMainDTFromHis(string strWhere)
        {
            //" + WEISHENG.COMM.zdInfo.iDwid + "
            string strsql = "select isnull(t2.分类名称,'') 分类名称,t1.* from CHIS.dbo.vw_lis_request_inp t1 left join vw_检验项目分类 t2 on t1.fitem_group = t2.HIS检查名称 where 1=1 ";
            if (!(string.IsNullOrWhiteSpace(strWhere)))
            {
                strsql += " and " + strWhere;
            }
            if (!check包含已打印.Checked)
                strsql += " and not exists (select 1 from SAM_APPLY ap where t1.fapply_id = ap.fapply_id)"; //排除本地已有的申请单
            strsql += " order by t1.fhz_id,t2.分类名称,t1.his_recordid  ";

            return HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.sLISConnString, CommandType.Text, strsql).Tables[0];
        }


        private List<HIS.Model.Pojo.LIS.Pojo条码打印> Get打印Pojos(string strWhere)
        {
            string sqlJY申请摘要 = $@"
                            SELECT 
	                            t1.* 
                            FROM
	                            (select 申请类别 reqtype,convert(varchar(12),申请单号) HIS申请单号,convert(varchar(12),病人ID)  病人ID,
	                            病历号 住院号或门诊号,病人类型 病人类别,
	                            a.病人姓名,b.性别 性别, b.出生日期 pat_birth, 
	                            convert(varchar(3), datediff(year,b.出生日期,GETDATE())) 年龄,
	                            收费类型 charge_typeno,b.疾病编码 fdiagnose,0 fward_num,b.病床,
	                            申请科室, 申请医生, CONVERT(char(20),申请时间,20) 申请日期,0 emer_flag,
	                            '' specimen_name,执行科室 perform_dept,'' original_reqno,
	                            -1 req_itemcode,cast(HIS组合名称 as varchar(200)) 检查项目, '1' item_price, '1' Qty, 
	                            '' 状态,convert(varchar(20),a.ID) HIS记录ID 
	                            ,b.身份证号
	                            ,'' fage_unit
	                            ,a.状态 打印状态
	                            from JY申请单摘要 a 
	                            left join ZY病人信息 b on a.病人ID = b.ZYID
	                            where 病人类型='住院' and b.已出院标记=0
	                            and 申请唯一标识 in(select ID from YS住院医嘱 )
	                            union all
	                            select 申请类别 reqtype,convert(varchar(12),申请单号) fapply_id,convert(varchar(30),b.ID) as fhz_id,
	                            申请唯一标识 fhz_zyh,病人类型 ftype_id,
	                            b.病人姓名 fname,b.性别 fsex, convert(datetime,null) pat_birth, 
	                            convert(varchar(3),  case when isnull(年龄,'')='' then '0' else 年龄 end) fage,
	                            收费类型 charge_typeno,b.ICD码 fdiagnose,0 fward_num,床号,
	                            申请科室 fapply_dept_id, 申请医生 req_docno, CONVERT(char(20),申请时间,20) fapply_time,0 emer_flag,
	                            '' specimen_name,执行科室 perform_dept,'' original_reqno,
	                            -1 req_itemcode,HIS组合名称 fitem_group, '1' item_price, '1' Qty, 
	                            '' fstate,convert(varchar(20),a.ID) his_recordid 
	                            ,b.身份证号 sfzh
	                            ,b.年龄单位 fage_unit
	                            ,a.状态
	                            from JY申请单摘要 a 
	                            left join MF门诊摘要 b on a.申请唯一标识 = b.MZID
	                            where 病人类型='门诊' and a.状态='已收款') t1
                            WHERE
	                            1 = 1 
	                            {strWhere}
                            ORDER BY
	                            t1.病人ID,
	                            t1.HIS记录ID";

            List<HIS.Model.Pojo.LIS.Pojo条码打印> list = new List<HIS.Model.Pojo.LIS.Pojo条码打印>();
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                list.AddRange(chis.Database.SqlQuery<HIS.Model.Pojo.LIS.Pojo条码打印>(sqlJY申请摘要).ToList());
            }

            using (LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {
                string sqlSamApply = $@"select * from SAM_APPLY where 1=1 {strWhere.Replace("申请日期", "fapply_time").Replace("病人类别", "ftype_id")
                    .Replace("住院号或门诊号", "fhz_zyh").Replace("申请科室", "fapply_dept_id")}";
                var lisSam = lisEntities.Database.SqlQuery<SAM_APPLY>(sqlSamApply).ToList();
                foreach (var item in lisSam)
                {
                    var isHave = list.Where(c => c.HIS申请单号 == item.fapply_id).FirstOrDefault();
                    if (isHave != null)
                    {
                        list.Remove(isHave);
                    }
                    var itemAdd = new HIS.Model.Pojo.LIS.Pojo条码打印();
                    itemAdd.HIS申请单号 = item.fapply_id;
                    itemAdd.打印状态 = ((enLIS申请单状态)Enum.Parse(typeof(enLIS申请单状态), item.fstate)).ToString();
                    itemAdd.病人类别 = item.ftype_id;
                    itemAdd.病人姓名 = item.fname;
                    itemAdd.年龄 = item.fage.ToString();
                    itemAdd.性别 = item.fsex;
                    itemAdd.病人ID = item.fhz_id;
                    itemAdd.床号 = item.fbed_num;
                    itemAdd.住院号或门诊号 = item.fhz_zyh;
                    itemAdd.检查项目 = item.fitem_group;
                    itemAdd.HIS记录ID = item.his_recordid;
                    itemAdd.申请医生 = item.fapply_user_id;
                    itemAdd.分类名称 = item.fremark;
                    itemAdd.申请日期 = item.fapply_time;
                    itemAdd.申请科室 = item.fapply_dept_id;
                    var itemGroup = lisEntities.SAM_ITEM_GROUP.Where(c => c.fname == item.fitem_group).FirstOrDefault();
                    if (itemGroup == null)
                    {
                        itemAdd.条码打印组合号 = WEISHENG.COMM.Helper.GuidHelper.DbGuid();
                    }
                    else
                    {
                        itemAdd.条码打印组合号 = itemGroup.条码打印组合号;
                    }
                    list.Add(itemAdd);
                }
            }

            list.RemoveAll(c => c.打印状态 == "已生成申请");

            if (!check包含已打印.Checked)
            {
                list.RemoveAll(c => c.打印状态.Contains("打印"));
            }
            return list;
        }


        DataTable dtCurr = new DataTable();
        /// <summary>
        /// 定义Grid格式
        /// </summary>
        /// <param name="dgvShow"></param>
        public void DataGridViewSetStyleNew(DataGridView dgvShow)
        {
            try
            {
                DataRow[] colList_OK = dtCurr.Select("fshow_flag>=0", "forder_by");
                int intOrder = 0;
                int fread_flage = 0;
                if (colList_OK.Length > 0)
                {
                    foreach (DataRow drC in colList_OK)
                    {
                        if (drC["fread_flage"].ToString() != null || drC["fread_flage"].ToString() != "")
                        {
                            fread_flage = Convert.ToInt32(drC["fread_flage"].ToString());
                        }
                        if (drC["forder_by"].ToString() != null || drC["forder_by"].ToString() != "")
                        {
                            intOrder = Convert.ToInt32(drC["forder_by"].ToString());
                            if (intOrder >= colList_OK.Length)
                                intOrder = colList_OK.Length - 1;
                        }
                        else
                            intOrder = colList_OK.Length - 1;

                        DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
                        string strCName_OK = drC["fcode"].ToString();
                        string strfvalue_type = drC["fvalue_type"].ToString();
                        int intfshow_flag = Convert.ToInt32(drC["fshow_flag"].ToString());
                        if (strfvalue_type == "预定义")
                        {
                            dgvShow.Columns[strCName_OK].Width = Convert.ToInt32(drC["fshow_width"].ToString());

                            dgvShow.Columns[strCName_OK].DisplayIndex = intOrder;
                            dgvShow.Columns[strCName_OK].HeaderText = drC["fname"].ToString();
                            if (fread_flage == 0)
                                dgvShow.Columns[strCName_OK].ReadOnly = false;
                            else
                                dgvShow.Columns[strCName_OK].ReadOnly = true;
                        }
                        else
                        {
                            newCol.DataPropertyName = strCName_OK;
                            newCol.Name = strCName_OK;
                            if (drC["fshow_width"].ToString() != "" || drC["fshow_width"].ToString() != null)
                                newCol.Width = Convert.ToInt32(drC["fshow_width"].ToString());
                            if (drC["fname"].ToString() != null || drC["fname"].ToString() != "")
                                newCol.HeaderText = drC["fname"].ToString();
                            newCol.DisplayIndex = intOrder;

                            if (fread_flage == 0)
                                newCol.ReadOnly = false;
                            else
                                newCol.ReadOnly = true;

                            dgvShow.Columns.AddRange(new DataGridViewColumn[] { newCol });
                        }
                        if ((intfshow_flag == 0) || (strfvalue_type == "主键"))
                            dgvShow.Columns[strCName_OK].Visible = false;
                        else
                            dgvShow.Columns[strCName_OK].Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 取得申请列表
        /// </summary>
        private void vRefreshGridViewSamApply()
        {
            try
            {
                string fjy_date1 = this.fjy_dateDateTimePicker1.Value.ToString("yyyy-MM-dd");
                string fjy_date2 = this.fjy_dateDateTimePicker2.Value.ToString("yyyy-MM-dd 23:59:59");
                string fjy_病人类型 = this.comboBox病人类型.SelectedValue.ToString();
                string fjy_门诊住院号 = this.textBox门诊住院号.Text;
                if (string.IsNullOrWhiteSpace(this.textBox门诊住院号.Text))
                {
                }
                else if (this.textBox门诊住院号.Text.Trim().Length == 4)
                {
                    this.textBox门诊住院号.Text = DateTime.Now.ToString("yyyyMMdd") + "." + this.textBox门诊住院号.Text.Trim();
                    fjy_门诊住院号 = this.textBox门诊住院号.Text;
                }

                //设置查询条件
                string strDateif = " cast(fapply_time as datetime)>='" + fjy_date1 + "' and cast(fapply_time as datetime)<='" + fjy_date2 + "' and ftype_id='" + fjy_病人类型 + "' and fhz_zyh like '%" + fjy_门诊住院号 + "%'";
                if (!string.IsNullOrWhiteSpace(textBoxName.Text))
                {
                    strDateif += " and fname='" + textBoxName.Text + "'";
                }
                if (!string.IsNullOrWhiteSpace(textBoxSFZH.Text))
                {
                    strDateif += " and sfzh='" + textBoxSFZH.Text + "' ";
                }
                if (!string.IsNullOrEmpty(txt申请科室.Text))
                {
                    strDateif += " and fapply_dept_id='" + txt申请科室.Text + "' ";
                }
                //if (fjy_病人类型.Trim() == "门诊")
                //{
                //    strDateif += " and 状态='已收款'";
                //}
                //dtMain = this.BllSamplingMainDTFromHis(strDateif);

                string strDateif2 = " and cast(申请日期 as datetime)>='" + fjy_date1 + "' and cast(申请日期 as datetime)<='" + fjy_date2 +
                    "' and 病人类别='" + fjy_病人类型 + "' and 住院号或门诊号 like '%" + fjy_门诊住院号 + "%'";
                if (!string.IsNullOrWhiteSpace(textBoxName.Text))
                {
                    strDateif2 += " and 病人姓名='" + textBoxName.Text + "'";
                }
                if (!string.IsNullOrWhiteSpace(textBoxSFZH.Text))
                {
                    strDateif2 += " and 身份证号='" + textBoxSFZH.Text + "' ";
                }
                if (!string.IsNullOrEmpty(txt申请科室.Text))
                {
                    strDateif2 += " and 申请科室='" + txt申请科室.Text + "' ";
                }
                //if (fjy_病人类型.Trim() == "门诊")
                //{
                //    strDateif2 += " and 状态='已收款'";
                //}

                listPojoSAM_Apply.Clear();
                listPojoSAM_Apply.AddRange(Get打印Pojos(strDateif2));
                gridView_SamApply.Columns["HIS申请单号"].SortOrder = ColumnSortOrder.Ascending;
                gridView_SamApply.RefreshData();
                gridView_SamApply.BestFitColumns();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == (Keys.F8))
            {
                simpleButton查询.PerformClick();
                return true;
            }
            if (keyData == (Keys.F3))
            {
                simpleButton打印预览.PerformClick();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void tb打印_Click(object sender, EventArgs e)
        {
            HIS.Model.Pojo.LIS.Pojo条码打印 selectedPojo打印 = gridView_SamApply.GetFocusedRow() as HIS.Model.Pojo.LIS.Pojo条码打印;
            if (selectedPojo打印 == null)
            {
                return;
            }
            if (selectedPojo打印.打印状态.Equals("打印"))
            {
                if (!WWMessage.MessageDialogResult("该条码已打印，是否重新打印？"))
                    return;
            }
            var button = sender as SimpleButton;
            PrintAction(selectedPojo打印, button.Name.Contains("预览"));
        }

        void PrintAction(HIS.Model.Pojo.LIS.Pojo条码打印 selectedPojo打印, bool b是否预览)
        {
            try
            {
                var isHave = listPojoSAM_Apply.Where(c => c.HIS申请单号 == selectedPojo打印.HIS申请单号).FirstOrDefault();
                if (isHave == null)
                {
                    return;
                }
                var s复合申请号 = yunLis.Business.s生成申请单号(HIS.COMM.ClassEnum.enLIS申请类型.打印组合);
                str打印条码号 = selectedPojo打印.HIS申请单号;
                string s分类 = selectedPojo打印.分类名称;
                string s住院号 = selectedPojo打印.住院号或门诊号;
                string s检查项目 = selectedPojo打印.检查项目;

                if (!string.IsNullOrEmpty(selectedPojo打印.条码打印组合号))
                {
                    //如果分类是空的则不处理，使用当前行的申请单id和检查项目，否则重新根据分类获取、2015-11-9 09:52:41 yufh添加
                    //如果分类是空的情况下，不进行任何操作
                    //按照住院号和检验分类进行筛选数据，组织打印项目
                    List<HIS.Model.Pojo.LIS.Pojo条码打印> listPojo打印 = listPojoSAM_Apply.Where(c => c.住院号或门诊号 == selectedPojo打印.住院号或门诊号
                    && c.条码打印组合号 == selectedPojo打印.条码打印组合号).OrderByDescending(c => c.HIS申请单号).ToList();
                    //重新排序后，取第一个申请单号作为lis申请单号，这样就可以不用重新生成
                    str打印条码号 = listPojo打印.FirstOrDefault().HIS申请单号;// drs[0]["fapply_id"].ToString();
                    s检查项目 = "";

                    if (listPojo打印.Count > 1)
                    {
                        using (LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
                        {
                            foreach (var item in listPojo打印)
                            {
                                HIS.Model.Print_Group printItem = new Print_Group() { HIS申请号 = item.HIS申请单号, 组合号 = s复合申请号 };
                                lisEntities.Print_Group.Add(printItem);
                                s检查项目 += item.检查项目 + ",";
                            }
                            str打印条码号 = s复合申请号;
                            lisEntities.SaveChanges();
                        }
                    }
                    else
                    {
                        s检查项目 = listPojo打印[0].检查项目;
                    }
                    s检查项目 = s检查项目.Substring(0, s检查项目.Length - 1);
                }
                if (!(string.IsNullOrWhiteSpace(str打印条码号)))
                {
                    CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
                    decimal zyid = Convert.ToDecimal(selectedPojo打印.病人ID);
                    string _s年龄;
                    //string _s年龄单位;
                    if (string.IsNullOrEmpty(selectedPojo打印.身份证号))
                    {
                        _s年龄 = selectedPojo打印.年龄 + selectedPojo打印.年龄单位;
                    }
                    else
                    {
                        Model年龄 item年龄 = new Model年龄();
                        HIS.COMM.Helper.AgeHelper.GetOutAgeAndUnitBySFZH(selectedPojo打印.身份证号, item年龄);
                        _s年龄 = item年龄.默认格式化年龄;
                    }

                    string s姓名 = selectedPojo打印.病人姓名;
                    string s性别 = selectedPojo打印.性别;
                    string s年龄 = _s年龄;
                    string s类型 = selectedPojo打印.病人类别;

                    int _i医生编码 = Convert.ToInt32(selectedPojo打印.申请医生);
                    string s申请医生 = HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == _i医生编码).FirstOrDefault().用户名;


                    string s病人信息 = "";
                    if (s类型 == "住院")
                    {
                        var item病人信息 = chis.ZY病人信息.Where(c => c.ZYID == zyid).FirstOrDefault();
                        s病人信息 = s姓名 + "/" + s性别 + "/" + s年龄 + "  " + s类型 + "/" + item病人信息.病床 + "/" + s申请医生;
                    }
                    else
                        s病人信息 = s姓名 + "/" + s性别 + "/" + s年龄 + "  " + s类型 + "/" + s申请医生;
                    try
                    {
                        if (b是否预览)
                            Print(true, s病人信息, s检查项目, str打印条码号);
                        else
                            Print(false, s病人信息, s检查项目, str打印条码号);

                        vRefreshGridViewSamApply();
                    }
                    catch (Exception ex)
                    {
                        WWMessage.MessageShowWarning("打印失败！" + ex.Message);
                    }
                }
                else
                {
                    WWMessage.MessageShowWarning("所要打印的申请项目没有申请号。");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowWarning("打印过程出现异常，异常信息如下：\n" + ex.Message);
            }
        }

        //add by wjz 20160504 打印时将该病人所有的化验项目都打印出来 ▽
        private void tb打印全部_Click(object sender, EventArgs e)
        {

        }

        private void PrintAll(bool b是否预览)
        {
            var selectedPojo打印 = gridView_SamApply.GetFocusedRow() as HIS.Model.Pojo.LIS.Pojo条码打印;
            if (selectedPojo打印 == null)
            {
                WWMessage.MessageShowWarning("没有选择任何行！");
                return;
            }


            string s住院号 = selectedPojo打印.住院号或门诊号;// dataGridViewApply.CurrentRow.Cells["fhz_zyh"].Value.ToString();
            string s姓名 = selectedPojo打印.病人姓名;// dataGridViewApply.CurrentRow.Cells["fname"].Value.ToString();
            string s性别 = selectedPojo打印.性别;// dataGridViewApply.CurrentRow.Cells["fsex"].Value.ToString();
            string s年龄 = selectedPojo打印.年龄;// dataGridViewApply.CurrentRow.Cells["fage"].Value.ToString();
            string s类型 = selectedPojo打印.病人类别;// dataGridViewApply.CurrentRow.Cells["ftype_id"].Value.ToString();
            string s床号 = selectedPojo打印.床号;// dataGridViewApply.CurrentRow.Cells["fbed_num"].Value.ToString();
            int i用户编码 = Convert.ToInt32(selectedPojo打印.申请医生);
            string s申请医生 = HIS.COMM.BLL.CacheData.Pubuser.Where(c => c.用户编码 == i用户编码).FirstOrDefault().用户名;// HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, "select 用户名 from pubUser where 用户编码='" + dataGridViewApply.CurrentRow.Cells["req_docno"].Value.ToString() + "'").ToString();


            List<HIS.Model.Pojo.LIS.Pojo条码打印> listPojo打印 = listPojoSAM_Apply.Where(c => c.住院号或门诊号 == selectedPojo打印.住院号或门诊号
  && c.分类名称 == selectedPojo打印.分类名称).OrderByDescending(c => c.HIS申请单号).ToList();

            Dictionary<String, String> applyiddic = new Dictionary<string, string>();
            TableXReport report = new TableXReport();

            string s病人信息 = "";
            if (s类型 == "住院")
                s病人信息 = s姓名 + "/" + s性别 + "/" + s年龄 + "  " + s类型 + "/" + s床号 + "/" + s申请医生;
            else
                s病人信息 = s姓名 + "/" + s性别 + "/" + s年龄 + "  " + s类型 + "/" + s申请医生;
            //add by wjz 20160620 为自助打印机添加条码打印 ▽
            if (s类型.Contains("门诊"))
            {
                AppendReport(ref report, s病人信息, "自助打印", listPojo打印.FirstOrDefault().HIS申请单号, s住院号, "");
            }
            //add by wjz 20160620 为自助打印机添加条码打印 △

            #region 目的：记录申请项目不为空的申请号,同时将申请项目按照“分类名称”进行汇总
            foreach (var item in listPojo打印)
            {
                if (string.IsNullOrWhiteSpace(item.检查项目.ToString()))
                {
                    continue;
                }

                string s申请单号 = item.HIS申请单号;
                string s检查 = item.检查项目;
                applyiddic.Add(s申请单号, s申请单号);
                //分类名称是空的，每个单独汇总
                if (string.IsNullOrWhiteSpace(item.分类名称.ToString()))
                {
                    //追加报表，修改记录
                    AppendReport(ref report, s病人信息, s检查, s申请单号, s住院号, s申请医生);
                }
                else
                {   //分类不是空的，需要找到相同分类的化验项目
                    string s分类Temp = item.分类名称.ToString();
                    var list相同分类 = listPojo打印.Where(c => c.分类名称 == s分类Temp).ToList();
                    foreach (var item相同分类 in list相同分类)
                    {
                        s检查 += "," + item相同分类.检查项目;
                        //applyidList.Add(drs[index]["fapply_id"].ToString());
                        applyiddic.Add(item.HIS申请单号, s申请单号);
                    }
                    AppendReport(ref report, s病人信息, s检查, s申请单号, s住院号, s申请医生);
                }
            }
            #endregion

            #region 打印操作
            bool bPrintSuccess = false;
            report.PrintProgress += (sender, e) => { bPrintSuccess = true; };

            if (b是否预览)
            {
                report.PrinterName = HIS.COMM.baseInfo.S默认条码打印;
                report.ShowPreviewDialog();
            }
            else
            {
                report.Print(HIS.COMM.baseInfo.S默认条码打印);
            }
            #endregion

            if (bPrintSuccess)
            {
                //applyidList记录了已经打印出来的检验项目的申请号。接下来的操作：根据申请号向LIS中写入申请信息
                foreach (var item in applyiddic)
                {
                    SaveHisApplyNew(item.Key);
                }
                //刷新查询结果
                vRefreshGridViewSamApply();
            }
            else
            {
                WWMessage.MessageShowWarning("打印失败！");
            }
        }

        private void toolStripButtonClose_Click(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="是否预览">true or false</param>
        /// <param name="s病人信息">包含姓名、性别、年龄等组合字符串</param>
        /// <param name="s检查项目">检查项目</param>
        /// <param name="s申请单号">申请单号==>条码</param>
        private void Print(bool 是否预览, string s病人信息, string s检查项目, string s申请单号)
        {
            #region add by wjz 修改此方法的实现，化验项目如果在一个条码纸上打不开的话，可以用分页打印 ▽

            Font ft = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            int size = 20;


            string[] sub检查项目 = s检查项目.Split(',');
            if (sub检查项目.Length == 1)
            { }
            else
            {
                Array.Sort(sub检查项目);
                s检查项目 = sub检查项目[0];
                for (int index = 1; index < sub检查项目.Length; index++)
                {
                    s检查项目 += "," + sub检查项目[index];
                }
            }


            TableXReport xtr = new TableXReport();

            if (s检查项目.Length < 2 * size)
            {
                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s病人信息, true, ft, TextAlignment.TopLeft, Size.Empty);


                if (s检查项目.Length > size)
                {
                    xtr.SetReportTitle(s检查项目.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                    xtr.SetReportTitle(s检查项目.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                }
                else
                {
                    xtr.SetReportTitle(s检查项目, true, ft, TextAlignment.TopLeft, Size.Empty);
                }

                xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");
            }
            else
            {
                string[] arr检查 = s检查项目.Split(',');
                List<string> list检验 = new List<string>();

                list检验.Add(arr检查[0]);
                for (int index = 1; index < arr检查.Length; index++)
                {
                    if (list检验[list检验.Count - 1].Length + arr检查[index].Length < size * 2)
                    {
                        list检验[list检验.Count - 1] = list检验[list检验.Count - 1] + "," + arr检查[index];
                    }
                    else
                    {
                        list检验.Add(arr检查[index]);
                    }
                }

                xtr.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s病人信息, true, ft, TextAlignment.TopLeft, Size.Empty);


                if (list检验[0].Length > size)
                {
                    xtr.SetReportTitle(list检验[0].Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                    xtr.SetReportTitle(list检验[0].Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                }
                else
                {
                    xtr.SetReportTitle(list检验[0], true, ft, TextAlignment.TopLeft, Size.Empty);
                }

                xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
                xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

                xtr.CreateDocument();

                for (int index = 1; index < list检验.Count; index++)
                {
                    CombineReports(xtr, ft, ft7, s病人信息, list检验[index], s申请单号);
                }

                xtr.PrintingSystem.ContinuousPageNumbering = true;
            }
            #endregion

            xtr.PrintProgress += new PrintProgressEventHandler(xtr_PrintProgress);

            if (是否预览)
            {
                #region 隐藏打印按钮
                //创建报表示例，指定到一个打印工具
                ReportPrintTool pt = new ReportPrintTool(xtr);

                // 获取打印工具的PrintingSystem
                PrintingSystemBase ps = pt.PrintingSystem;

                //// 隐藏Watermark 工具按钮和菜单项.
                //if (ps.GetCommandVisibility(PrintingSystemCommand.Watermark) != CommandVisibility.None)
                //{
                //    ps.SetCommandVisibility(PrintingSystemCommand.Watermark,CommandVisibility.None);                    
                //}
                //// 显示Document Map工具按钮和菜单项
                //ps.SetCommandVisibility(PrintingSystemCommand.DocumentMap, CommandVisibility.All);

                //隐藏打印按钮
                ps.SetCommandVisibility(new DevExpress.XtraPrinting.PrintingSystemCommand[]
    {
        DevExpress.XtraPrinting.PrintingSystemCommand.Background ,
        DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Customize ,
            DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap ,
            DevExpress.XtraPrinting.PrintingSystemCommand.File ,
            DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Open ,
            DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Print ,
            DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect ,
            DevExpress.XtraPrinting.PrintingSystemCommand.Save,
            DevExpress.XtraPrinting.PrintingSystemCommand.Watermark,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx,
            DevExpress.XtraPrinting.PrintingSystemCommand.ExportXps,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendFile,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendMht,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXls,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx,
            DevExpress.XtraPrinting.PrintingSystemCommand.SendXps,
            DevExpress.XtraPrinting.PrintingSystemCommand.SubmitParameters
        }, CommandVisibility.None);

                #endregion
                //显示报表预览.
                pt.ShowPreview();
            }
            else
                xtr.Print(HIS.COMM.baseInfo.S默认条码打印);

            SaveHisApplyNew(s申请单号);
        }

        //add by wjz 20151226 一个条码纸上可能无法将所有的项目全部打印出来，添加此方法的目的：实现分页打印 ▽
        private void CombineReports(TableXReport firstRep, Font ft9, Font ft7, string s病人信息, string sub检查项目, string s申请单号)
        {
            TableXReport subRep = new TableXReport();
            subRep.SetReportTitle("打印时间 " + System.DateTime.Now.ToString(), true, ft9, TextAlignment.TopLeft, Size.Empty);
            subRep.SetReportTitle(s病人信息, true, ft9, TextAlignment.TopLeft, Size.Empty);

            //xtr.SetReportTitle(s检查, true, ft, TextAlignment.TopLeft, Size.Empty);
            int size = 20;
            if (sub检查项目.Length > size)
            {
                subRep.SetReportTitle(sub检查项目.Substring(0, size), true, ft7, TextAlignment.TopLeft, Size.Empty);
                subRep.SetReportTitle(sub检查项目.Substring(size), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                subRep.SetReportTitle(sub检查项目, true, ft9, TextAlignment.TopLeft, Size.Empty);
            }

            subRep.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            subRep.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

            subRep.CreateDocument();

            firstRep.Pages.AddRange(subRep.Pages);
        }

        private void xtr_PrintProgress(object sender, PrintProgressEventArgs e)
        {
            #region HIS负责条码的生成，LIS段负责打印条码后，将申请信息写入LIS数据库，这部分代码是新增的
            //string strUpdate = SaveHisApply();
            //更新显示状态
            //DataGridViewRow row = this.dataGridViewApply.CurrentRow;
            //if (strUpdate.Equals("true") && row != null)
            //{
            //    //this.dtMain.Rows[applySelectedIndex]["fstate"] = 2;
            //    row.Cells["fstate"].Value = "2";
            //    row.DefaultCellStyle.ForeColor = Color.Blue;
            //}
            #endregion
        }

        /// <summary>
        /// 此方法负责保存从HIS中取出的化验申请，申请条码的生成由HIS段负责（Lis段不再操作申请条码）
        /// 2015-08-20 18:49:48 yufh 调整按照分类进行生成条码，打印并保存
        /// </summary>
        //private string SaveHisApply()
        //{
        //    var item = gridView_SamApply.GetFocusedRow() as HIS.Model.Pojo.LIS.Pojo条码打印;

        //    if (item == null)
        //    {
        //        WWMessage.MessageShowWarning("没有选中任何行");
        //        return "false";
        //    }
        //    string s检查分类 = item.分类名称;// row.Cells["分类名称"].Value.ToString();
        //    string s住院号 = item.住院号或门诊号;// row.Cells["fhz_zyh"].Value.ToString();
        //    string strID = item.HIS申请单号;// row.Cells["fapply_id"].Value.ToString();

        //    DataRow[] drs;
        //    if (s检查分类 == null || s检查分类 == "")
        //    {
        //        drs = dtMain.Select("fapply_id='" + strID + "'");
        //    }
        //    else
        //    {
        //        drs = dtMain.Select("fhz_zyh='" + s住院号 + "' and 分类名称='" + s检查分类 + "'");
        //    }
        //    this.str申请单id = drs[0]["fapply_id"].ToString();
        //    //循环保存单据
        //    string 执行状态 = "false";
        //    foreach (DataRow dr in drs)
        //    {
        //        DataRow SelectedRow = dr;
        //        if (SelectedRow == null)
        //        {
        //            WWMessage.MessageShowWarning("操作数据出现异常。");
        //            return "false";
        //        }

        //        //验证Lis数据库中是不是有这个申请单号的信息，如果有，则不再向LIS数据库中插入这条申请的信息 add 20150630 wjz
        //        string strSql = "select fapply_id, fstate, ftype_id, fhz_id, fhz_zyh,his_recordid from SAM_APPLY where 1=1 ";
        //        strSql += " and fapply_id='" + SelectedRow["fapply_id"].ToString() + "'";

        //        DataTable dtFromLis = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.sLISConnString, CommandType.Text, strSql).Tables[0];
        //        if (dtFromLis != null && dtFromLis.Rows.Count > 0)
        //        {
        //            continue;
        //        }

        //        HIS.Model.SAM_APPLY itemSam_apply = new HIS.Model.SAM_APPLY();
        //        //申请单号：使用存储过程生成的申请单号
        //        itemSam_apply.fapply_id = SelectedRow["fapply_id"].ToString();
        //        //判断分类是否为空！如果是空（没有分类的情况）则需要单独生成一个申请 2015-11-9 09:44:38 yufh 添加
        //        if (s检查分类 == "" || s检查分类 == null)
        //            itemSam_apply.his_recordid = SelectedRow["fapply_id"].ToString();
        //        else
        //            itemSam_apply.his_recordid = this.str申请单id;
        //        itemSam_apply.fjytype_id = "";//暂时设成空值
        //        itemSam_apply.fsample_type_id = "";//暂时设成空值
        //        itemSam_apply.fjz_flag = 1; //急诊否
        //        itemSam_apply.fcharge_flag = 0;//收费否
        //        itemSam_apply.fstate = "2";//状态
        //        itemSam_apply.fapply_user_id = SelectedRow["req_docno"].ToString();
        //        itemSam_apply.fapply_dept_id = SelectedRow["fapply_dept_id"].ToString(); //申请部门
        //        itemSam_apply.fapply_time = SelectedRow["fapply_time"].ToString(); //申请时间
        //        itemSam_apply.ftype_id = SelectedRow["ftype_id"].ToString(); //病人类型
        //        itemSam_apply.fhz_id = SelectedRow["fhz_id"].ToString(); //患者id
        //        itemSam_apply.fhz_zyh = SelectedRow["fhz_zyh"].ToString(); //住院号
        //        itemSam_apply.fsex = SelectedRow["fsex"].ToString(); //性别
        //        itemSam_apply.fname = SelectedRow["fname"].ToString(); //姓名
        //        itemSam_apply.sfzh = SelectedRow["sfzh"].ToString();
        //        //changed by wjz 20160122 修改年龄的计算方式 ▽
        //        //applyModel.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
        //        //applyModel.fage_unit = "岁";
        //        try
        //        {
        //            if (string.IsNullOrWhiteSpace(itemSam_apply.sfzh) || itemSam_apply.sfzh.Length != 18)
        //            {
        //                itemSam_apply.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
        //                itemSam_apply.fage_unit = SelectedRow["fage_unit"].ToString();
        //                if (string.IsNullOrWhiteSpace(itemSam_apply.fage_unit))//如果没有取到年龄，则默认为“岁”
        //                {
        //                    itemSam_apply.fage_unit = "岁";
        //                }
        //            }
        //            else
        //            {
        //                string birthday = itemSam_apply.sfzh.Substring(6, 4) + "-"
        //                                + itemSam_apply.sfzh.Substring(10, 2) + "-"
        //                                + itemSam_apply.sfzh.Substring(12, 2);

        //                string ages = DAOWWF.BllPatientAge(birthday);
        //                string[] arrage = ages.Split(':');

        //                itemSam_apply.fage = Convert.ToInt32(arrage[1]);
        //                itemSam_apply.fage_unit = arrage[0];
        //            }
        //        }
        //        catch
        //        {
        //            itemSam_apply.fage = Convert.ToInt32(SelectedRow["fage"]); //年龄
        //            itemSam_apply.fage_unit = SelectedRow["fage_unit"].ToString();
        //            if (string.IsNullOrWhiteSpace(itemSam_apply.fage_unit))//如果没有取到年龄，则默认为“岁”
        //            {
        //                itemSam_apply.fage_unit = "岁";
        //            }
        //        }
        //        //changed by wjz 20160122 修改年龄的计算方式 △
        //        //岁	1
        //        //月	2
        //        //天	3
        //        itemSam_apply.froom_num = "";
        //        itemSam_apply.fbed_num = SelectedRow["fbed_num"].ToString();
        //        itemSam_apply.fdiagnose = SelectedRow["fdiagnose"].ToString();
        //        itemSam_apply.fcreate_user_id = HIS.COMM.zdInfo.ModelUserInfo.用户编码.ToString();//LoginBLL.strPersonID;
        //        //applyModel.fcreate_time = strTime;
        //        itemSam_apply.fupdate_user_id = HIS.COMM.zdInfo.ModelUserInfo.用户编码.ToString();  //LoginBLL.strPersonID;
        //        //applyModel.fupdate_time = strTime;                //
        //        itemSam_apply.fxhdb = 0;
        //        itemSam_apply.fxyld = 0;
        //        itemSam_apply.fheat = 0;
        //        itemSam_apply.fage_day = 0;
        //        itemSam_apply.fage_month = 0;
        //        itemSam_apply.fage_year = 0;
        //        itemSam_apply.fjyf = 0;//检验费
        //        itemSam_apply.fitem_group = SelectedRow["fitem_group"].ToString();
        //        itemSam_apply.fremark = SelectedRow["分类名称"].ToString();
        //        string strSave = this.bllApply.save_Sam_apply_Sample(itemSam_apply);
        //        if (strSave == "true")
        //        {
        //            执行状态 = strSave;
        //        }
        //    }
        //    return 执行状态;

        //}

        private void textBox门诊住院号_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                vRefreshGridViewSamApply();
            }
        }


        //Print(bool 是否预览, string s姓名, string s性别, string s年龄, string s类型, string s检查, string s申请单号, string s申请医生)
        private void AppendReport(ref TableXReport report, string s病人信息, string s检查, string s申请单号, string s住院号, string s申请医生)
        {
            TableXReport rep = GetSubReport(s病人信息, s检查, s申请单号, s住院号, s申请医生);

            report.Pages.AddRange(rep.Pages);
        }

        private TableXReport GetSubReport(string s病人信息, string s检查, string s申请单号, string s住院号, string s申请医生)
        {
            Font ft = new System.Drawing.Font("宋体", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            Font ft7 = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            int size = 20;

            TableXReport xtr = new TableXReport();

            if (s检查.Length < 2 * size)
            {
                SetFirstReport(ref xtr, ft, ft7, size, s病人信息, s检查, s申请单号, s住院号, s申请医生);
            }
            else
            {
                List<string> list检验 = new List<string>();

                SplitItems(ref list检验, s检查, size);

                SetFirstReport(ref xtr, ft, ft7, size, s病人信息, list检验[0], s申请单号, s住院号, s申请医生);

                for (int index = 1; index < list检验.Count; index++)
                {
                    CombineReportsNew(xtr, ft, ft7, size, s病人信息, list检验[index], s申请单号, s住院号, s申请医生);
                }
            }
            return xtr;
        }

        private void CombineReportsNew(TableXReport firstRep, Font ft9, Font ft7, int colMaxSize,
                                    string s病人信息, string sub检查, string s申请单号, string s住院号, string s申请医生)
        {
            TableXReport subRep = new TableXReport();

            SetFirstReport(ref subRep, ft9, ft7, colMaxSize, s病人信息, sub检查, s申请单号, s住院号, s申请医生);

            firstRep.Pages.AddRange(subRep.Pages);
        }

        private void SplitItems(ref List<string> list检验, string s检查, int colMaxSize)
        {
            string[] arr检查 = s检查.Split(',');

            list检验.Add(arr检查[0]);
            for (int index = 1; index < arr检查.Length; index++)
            {
                if (list检验[list检验.Count - 1].Length + arr检查[index].Length < colMaxSize * 2)
                {
                    list检验[list检验.Count - 1] = list检验[list检验.Count - 1] + "," + arr检查[index];
                }
                else
                {
                    list检验.Add(arr检查[index]);
                }
            }
        }

        private void SetFirstReport(ref TableXReport xtr, Font ft, Font ft7, int colMaxSize, string s病人信息, string s检查, string s申请单号, string s住院号, string s申请医生)
        {

            xtr.SetReportTitle("打印时间" + System.DateTime.Now.ToString() + " " + s申请医生, true, ft, TextAlignment.TopLeft, Size.Empty);
            xtr.SetReportTitle(s病人信息, true, ft, TextAlignment.TopLeft, Size.Empty);


            if (s检查.Length > colMaxSize)
            {
                xtr.SetReportTitle(s检查.Substring(0, colMaxSize), true, ft7, TextAlignment.TopLeft, Size.Empty);
                xtr.SetReportTitle(s检查.Substring(colMaxSize), true, ft7, TextAlignment.TopLeft, Size.Empty);
            }
            else
            {
                xtr.SetReportTitle(s检查, true, ft7, TextAlignment.TopLeft, Size.Empty);
            }

            xtr.SetReportBarCode(s申请单号, new Point(5, 150), new Size(400, 125), null, true);
            xtr.SetReportMain(0, 0, 1, ContentAlignment.TopCenter, false, new System.Drawing.Printing.Margins(10, 10, 10, 10), System.Drawing.Printing.PaperKind.Custom, new Size(500, 315), 0, 0, "");

            xtr.CreateDocument();//如果不加这一行，report可能不能显示
        }

        private bool SaveHisApplyNew(string sHIS申请单号)
        {
            var item = gridView_SamApply.GetFocusedRow() as HIS.Model.Pojo.LIS.Pojo条码打印;
            //验证Lis数据库中是不是有这个申请单号的信息，如果有，则不再向LIS数据库中插入这条申请的信息
            using (LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {
                if (sHIS申请单号.Substring(0, 1) == "4")//组合打印
                {
                    var listPrintGroup = lisEntities.Print_Group.Where(c => c.组合号 == sHIS申请单号).ToList();
                    foreach (var itemPrint in listPrintGroup)
                    {
                        var itemHave = lisEntities.SAM_APPLY.Where(c => c.fapply_id == itemPrint.HIS申请号).FirstOrDefault();
                        if (itemHave != null)
                        {
                            itemHave.fstate = ((int)enLIS申请单状态.已打印条码).ToString();
                            lisEntities.SAM_APPLY.Attach(itemHave);
                            lisEntities.Entry(itemHave).Property(c => c.fstate).IsModified = true;
                            lisEntities.SaveChanges();

                        }
                        else
                        {
                            HIS.Model.SAM_APPLY applyModel = new HIS.Model.SAM_APPLY();
                            //申请单号：使用存储过程生成的申请单号
                            applyModel.fapply_id = itemPrint.HIS申请号;
                            //判断分类是否为空！如果是空（没有分类的情况）则需要单独生成一个申请 2015-11-9 09:44:38 yufh 添加
                            //if (s检查分类 == "" || s检查分类 == null)
                            //    applyModel.His申请单号 = SelectedRow["fapply_id"].ToString();
                            //else
                            //    applyModel.His申请单号 = this.str申请单id;
                            applyModel.his_recordid = itemPrint.HIS申请号;
                            applyModel.fjytype_id = "";//暂时设成空值
                            applyModel.fsample_type_id = "";//暂时设成空值
                            applyModel.fjz_flag = 0; //急诊否
                            applyModel.fcharge_flag = 0;//收费否
                            applyModel.fstate = ((int)enLIS申请单状态.已打印条码).ToString();//状态
                            applyModel.fapply_user_id = item.申请医生;// SelectedRow["req_docno"].ToString();
                            applyModel.fapply_dept_id = item.申请科室;//SelectedRow["fapply_dept_id"].ToString(); //申请部门
                            applyModel.fapply_time = item.申请日期;// SelectedRow["fapply_time"].ToString(); //申请时间
                            applyModel.ftype_id = item.病人类别;// SelectedRow["ftype_id"].ToString(); //病人类型
                            applyModel.fhz_id = item.病人ID;// SelectedRow["fhz_id"].ToString(); //患者id
                            applyModel.fhz_zyh = item.住院号或门诊号;// SelectedRow["fhz_zyh"].ToString(); //住院号
                            applyModel.fsex = item.性别;// SelectedRow["fsex"].ToString(); //性别
                            applyModel.fname = item.病人姓名;// SelectedRow["fname"].ToString(); //姓名
                            applyModel.sfzh = item.身份证号;// SelectedRow["sfzh"].ToString();
                            applyModel.fage = Convert.ToInt32(item.年龄); //Convert.ToInt32(SelectedRow["fage"]); //年龄
                            applyModel.fage_unit = item.年龄单位;// SelectedRow["fage_unit"].ToString();
                            applyModel.froom_num = "";
                            applyModel.fbed_num = item.床号;
                            applyModel.fdiagnose = "";
                            applyModel.fcreate_user_id = HIS.COMM.zdInfo.ModelUserInfo.用户编码.ToString();
                            applyModel.fupdate_user_id = HIS.COMM.zdInfo.ModelUserInfo.用户编码.ToString();
                            applyModel.fxhdb = 0;
                            applyModel.fxyld = 0;
                            applyModel.fheat = 0;
                            applyModel.fage_day = 0;
                            applyModel.fage_month = 0;
                            applyModel.fage_year = 0;
                            applyModel.fjyf = 0;//检验费
                            applyModel.fitem_group = item.检查项目;
                            applyModel.fremark = item.分类名称;
                            lisEntities.SAM_APPLY.Add(applyModel);
                            lisEntities.SaveChanges();
                        }


                    }
                }
                else
                {
                    var itemHave = lisEntities.SAM_APPLY.Where(c => c.fapply_id == sHIS申请单号).FirstOrDefault();
                    if (itemHave != null)
                    {
                        itemHave.fstate = ((int)enLIS申请单状态.已打印条码).ToString();
                        lisEntities.SAM_APPLY.Attach(itemHave);
                        lisEntities.Entry(itemHave).Property(c => c.fstate).IsModified = true;
                        lisEntities.SaveChanges();

                    }
                    else
                    {
                        HIS.Model.SAM_APPLY applyModel = new HIS.Model.SAM_APPLY();
                        //申请单号：使用存储过程生成的申请单号
                        applyModel.fapply_id = sHIS申请单号;
                        //判断分类是否为空！如果是空（没有分类的情况）则需要单独生成一个申请 2015-11-9 09:44:38 yufh 添加
                        //if (s检查分类 == "" || s检查分类 == null)
                        //    applyModel.His申请单号 = SelectedRow["fapply_id"].ToString();
                        //else
                        //    applyModel.His申请单号 = this.str申请单id;
                        applyModel.his_recordid = sHIS申请单号;
                        applyModel.fjytype_id = "";//暂时设成空值
                        applyModel.fsample_type_id = "";//暂时设成空值
                        applyModel.fjz_flag = 0; //急诊否
                        applyModel.fcharge_flag = 0;//收费否
                        applyModel.fstate = ((int)enLIS申请单状态.已打印条码).ToString();//状态
                        applyModel.fapply_user_id = item.申请医生;// SelectedRow["req_docno"].ToString();
                        applyModel.fapply_dept_id = item.申请科室;//SelectedRow["fapply_dept_id"].ToString(); //申请部门
                        applyModel.fapply_time = item.申请日期;// SelectedRow["fapply_time"].ToString(); //申请时间
                        applyModel.ftype_id = item.病人类别;// SelectedRow["ftype_id"].ToString(); //病人类型
                        applyModel.fhz_id = item.病人ID;// SelectedRow["fhz_id"].ToString(); //患者id
                        applyModel.fhz_zyh = item.住院号或门诊号;// SelectedRow["fhz_zyh"].ToString(); //住院号
                        applyModel.fsex = item.性别;// SelectedRow["fsex"].ToString(); //性别
                        applyModel.fname = item.病人姓名;// SelectedRow["fname"].ToString(); //姓名
                        applyModel.sfzh = item.身份证号;// SelectedRow["sfzh"].ToString();
                        applyModel.fage = Convert.ToInt32(item.年龄); //Convert.ToInt32(SelectedRow["fage"]); //年龄
                        applyModel.fage_unit = item.年龄单位;// SelectedRow["fage_unit"].ToString();
                        applyModel.froom_num = "";
                        applyModel.fbed_num = item.床号;
                        applyModel.fdiagnose = "";
                        applyModel.fcreate_user_id = HIS.COMM.zdInfo.ModelUserInfo.用户编码.ToString();
                        applyModel.fupdate_user_id = HIS.COMM.zdInfo.ModelUserInfo.用户编码.ToString();
                        applyModel.fxhdb = 0;
                        applyModel.fxyld = 0;
                        applyModel.fheat = 0;
                        applyModel.fage_day = 0;
                        applyModel.fage_month = 0;
                        applyModel.fage_year = 0;
                        applyModel.fjyf = 0;//检验费
                        applyModel.fitem_group = item.检查项目;
                        applyModel.fremark = item.分类名称;
                        lisEntities.SAM_APPLY.Add(applyModel);
                        lisEntities.SaveChanges();

                    }


                }

            }
            return true;
        }

        private void simpleButton关闭_Click(object sender, EventArgs e)
        {
            this.Close();
        }




        private void simpleButton打印病人全部条码_Click(object sender, EventArgs e)
        {
            List<HIS.Model.Pojo.LIS.Pojo条码打印> listPojoSAM_Apply_Print = HIS.Model.Helper.modelHelper.ListClone<Pojo条码打印>(listPojoSAM_Apply);
            foreach (var item in listPojoSAM_Apply_Print)
            {
                PrintAction(item, false);
            }
            //PrintAll(true);
        }

        private void simpleButton查询_Click(object sender, EventArgs e)
        {
            vRefreshGridViewSamApply();
        }

        private void simpleButton危急值查看_Click(object sender, EventArgs e)
        {
            //string toSfzh = "371323197803051424";
            //var msg = new SignalRMessage()
            //{
            //    Content = "化验室测试危急值发送到个人",
            //    From = HIS.COMM.zdInfo.ModelUserInfo.用户名,
            //    To = "",
            //    MessageType = EnumMessageType.危急值
            //};
            ////await HIS.COMM.zdInfo.SignalRHelper.SendToPerson(toSfzh, msg);
            ////await HIS.COMM.zdInfo.SignalRHelper.SendToAll( msg);

            //await HIS.COMM.zdInfo.SignalRHelper.SendToGroup("",msg);
        }
    }
}