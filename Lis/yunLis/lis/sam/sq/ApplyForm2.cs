﻿using DevExpress.XtraTreeList;
using HIS.COMM;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using yunLis.dao;
using yunLis.lis.com;
using yunLis.lisbll.sam;
using yunLis.wwfbll;

namespace yunLis.lis.sam.sq
{
    public partial class ApplyForm2 : DevExpress.XtraEditors.XtraForm
    {
        PatientBLL bllPatient = new PatientBLL();
        ItemBLL bllItem = new ItemBLL();//项目逻辑
        DeptBll bllDept = new DeptBll();//部门
        TypeBLL bllType = new TypeBLL();//公共类型逻辑  
        List<SAM_JY_RESULT> listSam_Jy_Result = new List<SAM_JY_RESULT>();//已经选择的项目
        ApplyBLL bllApply = new ApplyBLL();//申请规则
        private string str申请单id = "";
        SAM_PATIENT lisPatient;


        public ApplyForm2(Pojo病人信息 pjInpatient)
        {
            InitializeComponent();
            this.lisPatient = new SAM_PATIENT()
            {
                sfzh = pjInpatient.身份证号,
                fhz_id = pjInpatient.ZYID.ToString(),
                fname = pjInpatient.病人姓名,
                fsex = pjInpatient.性别,
                fdept_id = pjInpatient.科室名称.ToString(),
                fsjys_id = pjInpatient.主治医生姓名.ToString(),
                fbed_num = pjInpatient.病床,
                fdiagnose = pjInpatient.疾病名称,
                fbirthday = pjInpatient.出生日期.ToString(),
                fadd = pjInpatient.家庭地址,
                fhymen = pjInpatient.婚姻状况,
                fjob = pjInpatient.职业,
                fjob_org = pjInpatient.工作单位,
                fhz_zyh = pjInpatient.住院号码,
                froom_num = pjInpatient.房间号
            };
            WWComTypeList();
            this.cbftype_id.SelectedValue = "住院";
            this.cbftype_id.Enabled = false;
        }
        public ApplyForm2()
        {
            InitializeComponent();
            this.lisPatient = new SAM_PATIENT()
            {
                //sfzh = pjInpatient.身份证号,
                //fhz_id = pjInpatient.ZYID.ToString(),
                //fname = pjInpatient.病人姓名,
                //fsex = pjInpatient.性别,
                //fdept_id = pjInpatient.科室名称.ToString(),
                //fsjys_id = pjInpatient.主治医生姓名.ToString(),
                //fbed_num = pjInpatient.病床,
                //fdiagnose = pjInpatient.疾病名称,
                //fbirthday = pjInpatient.出生日期.ToString(),
                //fadd = pjInpatient.家庭地址,
                //fhymen = pjInpatient.婚姻状况,
                //fjob = pjInpatient.职业,
                //fjob_org = pjInpatient.工作单位,
                //fhz_zyh = pjInpatient.住院号码,
                //froom_num = pjInpatient.房间号
            };
            WWComTypeList();
            this.cbftype_id.SelectedValue = "门诊";
            this.cbftype_id.Enabled = false;
        }

        //public ApplyForm(MF门诊摘要 sAM_PATIENT)
        //{
        //    InitializeComponent();
        //    //this.lisPatient = new SAM_PATIENT()
        //    //{

        //    //};
        //    //WWComTypeList();
        //    //dataGridViewAllItem.AutoGenerateColumns = false;
        //    //DataGridViewObject.AutoGenerateColumns = false;
        //    //dataGridViewOK.AutoGenerateColumns = false;
        //}
        private void ApplyForm_Load(object sender, EventArgs e)
        {
            try
            {
                tb病人姓名.DataBindings.Add("Text", lisPatient, "fname");
                tb科室名称.DataBindings.Add("Text", lisPatient, "fdept_id");
                tb房间号.DataBindings.Add("Text", lisPatient, "froom_num");
                tb床号.DataBindings.Add("Text", lisPatient, "fbed_num");
                tb住院号码.DataBindings.Add("Text", lisPatient, "fhz_zyh");
                tb患者ID.DataBindings.Add("Text", lisPatient, "fhz_id");
                cb性别.DataBindings.Add("Text", lisPatient, "fsex");
                //tb年龄.DataBindings.Add("Text", lisPatient.f, "fdept_id");
                //cb年龄单位.DataBindings.Add("Text", lisPatient, "fdept_id");
                tb诊断.DataBindings.Add("Text", lisPatient, "fdiagnose");
                tb主管医生姓名.DataBindings.Add("Text", lisPatient, "fsjys_id");

                //GetTreeHZ();
                GetTreeCheckType("-1");

                this.tb主管医生编码.Text = LoginBLL.strPersonID;
                this.tb主管医生姓名.Text = LoginBLL.strPersonName;

                //WWComTypeList();
                dataGridViewAllItem.AutoGenerateColumns = false;
                DataGridView组合名称.AutoGenerateColumns = false;
                dataGridView选中项目.AutoGenerateColumns = false;
                vLoadTree();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }


        void vLoadTree()
        {
            try
            {
                CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
                //string sql = $@"select 编号 KeyFieldName, -1 ParentFieldName,套餐名称,编号 套餐编号,isnull(显示顺序,999) 显示顺序
                //                from   gy套餐摘要
                //                where 创建人 = '{HIS.COMM.zdInfo.ModelUserInfo.用户名}' and 数据类型 = '个人'
                //                union all
                //                select 编号 KeyFieldName, -2 ParentFieldName,套餐名称,编号,isnull(显示顺序,999) 显示顺序
                //                from   gy套餐摘要
                //                where 科室编码 = '{HIS.COMM.zdInfo.Model科室信息.科室编码}' and 数据类型 = '科室'
                //                union all
                //                select 编号 KeyFieldName, -3 ParentFieldName,套餐名称,编号,isnull(显示顺序,999) 显示顺序
                //                from   gy套餐摘要
                //                where 数据类型 = '全院'";
                //PojoExaminationGroup item1 = new PojoExaminationGroup()
                //{
                //    KeyFieldName = -1,
                //    ParentFieldName = 0,
                //    套餐名称 = "本人",
                //    套餐编号 = -1
                //};
                //PojoExaminationGroup item2 = new PojoExaminationGroup()
                //{
                //    KeyFieldName = -2,
                //    ParentFieldName = 0,
                //    套餐名称 = "本科",
                //    套餐编号 = -1
                //};
                //PojoExaminationGroup item3 = new PojoExaminationGroup()
                //{
                //    KeyFieldName = -3,
                //    ParentFieldName = 0,
                //    套餐名称 = "全院",
                //    套餐编号 = -1
                //};
                //List<HIS.Model.Pojo.PojoExaminationGroup> listGroups = new List<PojoExaminationGroup>();
                //listGroups.Add(item1);
                //listGroups.Add(item2);
                //listGroups.Add(item3);

                //var list1 = chis.Database.SqlQuery<PojoExaminationGroup>(sql).OrderBy(c => c.显示顺序).ToList();
                //listGroups.AddRange(list1);
                string sql = $@"SELECT fname 组合名称,fgroup_id 组合ID,fhelp_code 拼音代码,
                                (select fname from SAM_CHECK_TYPE where fcheck_type_id=aa.fcheck_type_id) 检查类型,
                                (select fname from SAM_SAMPLE_TYPE where fsample_type_id=aa.fsam_type_id) 样本类型
                                 FROM SAM_ITEM_GROUP aa order by forder_by";
                DataTable dataTable = SqlHelper.ExecuteDataTable(sql);

                treeList组合名称.KeyFieldName = "组合ID";
                treeList组合名称.ParentFieldName = "组合ID";
                treeList组合名称.ExpandAll();
                treeList组合名称.DataSource = dataTable;
                //treeList组合名称.Columns["套餐编号"].Visible = false;
                //treeList组合名称.Columns["显示顺序"].Visible = false;
                treeList组合名称.ExpandAll();

                this.searchControlFilter.Client = this.treeList组合名称;
                treeList组合名称.OptionsBehavior.EnableFiltering = true;
                //过滤模式，枚举1.Default 2.Extended（推荐） 3.Smart（推荐） 4.Standard
                treeList组合名称.OptionsFilter.FilterMode = FilterMode.Smart;
                treeList组合名称.FilterNode += treeList1_FilterNode;
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }
        private void treeList1_FilterNode(object sender, DevExpress.XtraTreeList.FilterNodeEventArgs e)
        {
            if (treeList组合名称.DataSource == null) return;
            string NodeText = e.Node.GetDisplayText("套餐名称");//参数填写FieldName
            if (string.IsNullOrWhiteSpace(NodeText)) return;
            bool IsVisible = NodeText.ToUpper().IndexOf(searchControlFilter.Text.ToUpper()) >= 0;
            if (IsVisible)
            {
                DevExpress.XtraTreeList.Nodes.TreeListNode Node = e.Node.ParentNode;
                while (Node != null)
                {
                    if (!Node.Visible)
                    {
                        Node.Visible = true;
                        Node = Node.ParentNode;
                    }
                    else
                        break;
                }
            }
            e.Node.Visible = IsVisible;
            e.Handled = true;
        }

        #region  业务方法
        /// <summary>
        /// 创建已经选择项目临时表
        /// </summary>
        //private void CreatedtSelectOK()
        //{
        //    try
        //    {
        //        DataColumn dataColumn1 = new DataColumn("fitem_id", typeof(string));
        //        dtSelectOK.Columns.Add(dataColumn1);

        //        DataColumn dataColumn2 = new DataColumn("fitem_code", typeof(string));
        //        dtSelectOK.Columns.Add(dataColumn2);

        //        DataColumn dataColumn3 = new DataColumn("fitem_name", typeof(string));
        //        dtSelectOK.Columns.Add(dataColumn3);
        //        DataColumn dataColumn4 = new DataColumn("fitem_unit", typeof(string));
        //        dtSelectOK.Columns.Add(dataColumn4);

        //        DataColumn dataColumn5 = new DataColumn("forder_by", typeof(string));
        //        dtSelectOK.Columns.Add(dataColumn5);

        //    }
        //    catch (Exception ex)
        //    {
        //        WWMessage.MessageShowError(ex.ToString());
        //    }
        //}
        /// <summary>
        /// 公共列表
        /// </summary>
        private void WWComTypeList()
        {
            try
            {
                this.cbftype_id.DataSource = this.bllType.BllComTypeDT("病人类别", 1, "");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得检验类别树结构
        /// </summary>
        /// <param name="pid"></param>
        public void GetTreeCheckType(string pid)
        {
            try
            {
                //this.treeList1.DataSource= this.bllItem.BllCheckAndSampleTypeDT();//this.bllItem.BllCheckAndSampleTypeDT2();
                //treeList1.ParentFieldName = "fpid";
                ////treeList1.KeyFieldName = "fid";
                //treeList1.TreeViewFieldName = "fname";


                this.wwTreeViewCheckType.ZADataTable = this.bllItem.BllCheckAndSampleTypeDT();
                this.wwTreeViewCheckType.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeViewCheckType.ZAParentFieldName = "fpid";//上级字段名称
                this.wwTreeViewCheckType.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeViewCheckType.ZAKeyFieldName = "fid";//主键字段名称
                this.wwTreeViewCheckType.ZAToolTipTextName = "tag";
                this.wwTreeViewCheckType.ZATreeViewShow();//显示树        
                                                          // try
                                                          //{
                                                          //  this.wwTreeViewCheckType.SelectedNode = wwTreeViewCheckType.Nodes[0];
                                                          // }
                                                          // catch { }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取得患者树
        /// </summary>
        public void GetTreeHZ()
        {
            //try
            //{
            //    string pid = "-1";
            //    string fjy_date1 = this.bllPatient.DbDateTime1(fjy_dateDateTimePicker1);
            //    string fjy_date2 = this.bllPatient.DbDateTime2(fjy_dateDateTimePicker2);
            //    string strDateif = " and (ftime_registration>='" + fjy_date1 + "' and ftime_registration<='" + fjy_date2 + "')";
            //    this.wwTreeViewHZ.ZADataTable = this.bllPatient.BllDeptAndPatientDT(strDateif);
            //    this.wwTreeViewHZ.ZATreeViewRootValue = pid;//树 RootId　根节点值
            //    this.wwTreeViewHZ.ZAParentFieldName = "fpid";//上级字段名称
            //    this.wwTreeViewHZ.ZADisplayFieldName = "fname";//显示字段名称
            //    this.wwTreeViewHZ.ZAKeyFieldName = "fid";//主键字段名称
            //    this.wwTreeViewHZ.ZAToolTipTextName = "tag";
            //    this.wwTreeViewHZ.ZATreeViewShow();//显示树        
            //                                       //try
            //                                       //{
            //                                       // this.wwTreeViewHZ.SelectedNode = wwTreeViewHZ.Nodes[0];
            //                                       //}
            //                                       //catch { }

            //}
            //catch (Exception ex)
            //{
            //    yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            //}
        }
        /// <summary>
        /// 据患者ID设置患者信息
        /// </summary>
        /// <param name="fhz_id"></param>
        private void SetHZInfo(string fhz_id)
        {
            try
            {
                DataTable dthz = this.bllPatient.BllPatientDTByID(fhz_id);
                if (dthz != null)
                {
                    string fsex = "";//性别
                    string fbirthday = "";//生日
                    string fdept_id = "";//科室

                    if (dthz.Rows[0]["fsex"] != null)
                        fsex = dthz.Rows[0]["fsex"].ToString();
                    if (fsex == "" || fsex == null)
                        fsex = "?";
                    if (dthz.Rows[0]["fbirthday"] != null)
                        fbirthday = dthz.Rows[0]["fbirthday"].ToString();

                    //生日-----------------------
                    string strGetAge = this.bllPatient.BllPatientAge(fbirthday);
                    if (strGetAge == "" || strGetAge == null)
                    {
                        this.tb年龄.Text = "0";
                        this.cb年龄单位.SelectedItem = "?";
                    }
                    else
                    {
                        string[] sPatientAge = strGetAge.Split(':');
                        this.tb年龄.Text = sPatientAge[1].ToString();
                        this.cb年龄单位.SelectedItem = sPatientAge[0].ToString();
                    }
                    //---------------------------
                    if (dthz.Rows[0]["fdept_id"] != null)
                        fdept_id = dthz.Rows[0]["fdept_id"].ToString();

                    this.tb科室编码.Text = fdept_id;
                    this.tb科室名称.Text = bllDept.BllDeptName(fdept_id);

                    this.tb病人姓名.Text = dthz.Rows[0]["fname"].ToString();
                    this.tb患者ID.Text = dthz.Rows[0]["fhz_id"].ToString();
                    this.tb住院号码.Text = dthz.Rows[0]["fhz_zyh"].ToString();
                    this.tb床号.Text = dthz.Rows[0]["fbed_num"].ToString();
                    this.tb房间号.Text = dthz.Rows[0]["froom_num"].ToString();
                    //MessageBox.Show(fsex);
                    this.cb性别.SelectedItem = fsex;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 取消所选项目
        /// </summary>
        /// <param name="intEidt"></param>
        private void v取消所选项目(int intEidt)
        {
            try
            {
                if (intEidt == 1)
                {
                    if (WWMessage.MessageDialogResult("确定取消所有已经选择的项目？"))
                    {
                        if (this.listSam_Jy_Result.Count > 0)
                            listSam_Jy_Result.Clear();
                        this.dataGridView选中项目.DataSource = listSam_Jy_Result;
                    }
                }
                else
                {
                    if (this.listSam_Jy_Result.Count > 0)
                        listSam_Jy_Result.Clear();
                    this.dataGridView选中项目.DataSource = listSam_Jy_Result;
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
        /// <summary>
        /// 取得 项目
        /// </summary>
        private void GetItemOne()
        {
            try
            {

                this.dataGridViewAllItem.DataSource = this.bllItem.BllItem(" fcheck_type_id='" + strfCheck_Type_id + "' and fsam_type_id='" + strfSample_Type_id + "'");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }
        /// <summary>
        /// 取得 项目组
        /// </summary>
        private void GetItemGroup()
        {
            try
            {

                this.DataGridView组合名称.DataSource = this.bllItem.BllItemGroupDT("fcheck_type_id='" + strfCheck_Type_id + "' and fsam_type_id='" + strfSample_Type_id + "'");
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }

        #endregion

        private void fjy_dateDateTimePicker1_CloseUp(object sender, EventArgs e)
        {
            GetTreeHZ();
        }

        private void fjy_dateDateTimePicker2_CloseUp(object sender, EventArgs e)
        {
            GetTreeHZ();
        }


        private void wwTreeViewHZ_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                string strEdit = "";
                string strfhz_id = "";
                strEdit = e.Node.ToolTipText;
                strfhz_id = e.Node.Name;
                if (strEdit == "patient")
                {
                    SetHZInfo(strfhz_id);
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }



        private void tbfapply_user_name_DoubleClick(object sender, EventArgs e)
        {

            SelPerson();
        }
        private void SelPerson()
        {
            try
            {
                WindowsResult r = new WindowsResult();
                r.TextChangedString += new yunLis.lis.com.TextChangedHandlerString(this.EventResultChangedString);
                WindowsPersonForm fc = new WindowsPersonForm(r);
                fc.ShowDialog();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void SelDept()
        {
            try
            {
                WindowsResult r = new WindowsResult();
                r.TextChangedString += new yunLis.lis.com.TextChangedHandlerString(this.EventResultChangedStringDept);

                WindowsDeptForm fca = new WindowsDeptForm(r);
                fca.ShowDialog();

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }   /// <summary>
            /// 返回表ID
            /// </summary>
            /// <param name="s"></param>
        private void EventResultChangedStringDept(string s)
        {
            try
            {
                this.tb科室编码.Text = s;
                this.tb科室名称.Text = bllDept.BllDeptName(s);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 返回表ID
        /// </summary>
        /// <param name="s"></param>
        private void EventResultChangedString(string s)
        {
            try
            {
                this.tb主管医生编码.Text = s;
                this.tb主管医生姓名.Text = bllDept.BllPersonName(s);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonuser_id_Click(object sender, EventArgs e)
        {
            SelPerson();
        }

        private void buttonDept_Click(object sender, EventArgs e)
        {
            SelDept();
        }
        private void tbfdept_name_DoubleClick(object sender, EventArgs e)
        {
            SelDept();
        }
        string strfCheck_Type_id = "";
        string strfSample_Type_id = "";
        private void tree检验类型_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (this.listSam_Jy_Result.Count > 0)
                listSam_Jy_Result.Clear();
            this.dataGridView选中项目.DataSource = listSam_Jy_Result;
            strfCheck_Type_id = "";
            strfSample_Type_id = "";
            string strEdit = e.Node.ToolTipText;
            if (strEdit == "CheckType")
            {
                //bN.Enabled = false;
            }
            else
            {
                //bN.Enabled = true;
                strfCheck_Type_id = e.Node.Parent.Name.ToString();
                strfSample_Type_id = e.Node.Name.ToString();
                GetItemGroup();
                GetItemOne();
            }
        }




        private void dataGridViewAllItem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    for (int i = 0; i < dataGridViewAllItem.Rows.Count; i++)
                    {
                        dataGridViewAllItem.Rows[i].Cells["sel_item"].Value = 0;
                        dataGridViewAllItem.CurrentRow.Cells["sel_item"].Value = 1;
                    }
                    int selOK = 0;
                    //选择否
                    if (dataGridViewAllItem.CurrentRow.Cells["sel_item"].Value != null)
                        selOK = Convert.ToInt32(dataGridViewAllItem.CurrentRow.Cells["sel_item"].Value);
                    if (selOK == 1)
                    {
                        string stritemid = dataGridViewAllItem.CurrentRow.Cells["fitem_id"].Value.ToString();
                        string strfitem_code = "";
                        string strfname_item = "";
                        string strfitem_unit = "";
                        string strforder_by = "";
                        if (dataGridViewAllItem.CurrentRow.Cells["fitem_code"].Value != null)
                            strfitem_code = dataGridViewAllItem.CurrentRow.Cells["fitem_code"].Value.ToString();
                        if (dataGridViewAllItem.CurrentRow.Cells["fname_item"].Value != null)
                            strfname_item = dataGridViewAllItem.CurrentRow.Cells["fname_item"].Value.ToString();
                        if (dataGridViewAllItem.CurrentRow.Cells["funit_name_zw"].Value != null)
                            strfitem_unit = dataGridViewAllItem.CurrentRow.Cells["funit_name_zw"].Value.ToString();
                        if (dataGridViewAllItem.CurrentRow.Cells["forder_by_item"].Value != null)
                            strforder_by = dataGridViewAllItem.CurrentRow.Cells["forder_by_item"].Value.ToString();
                        var currcolumXS = this.listSam_Jy_Result.Where(c => c.fitem_id == stritemid).ToList();
                        if (currcolumXS.Count > 0)
                        {
                        }
                        else
                        {
                            SAM_JY_RESULT drNew = new SAM_JY_RESULT();
                            drNew.fitem_id = stritemid;
                            drNew.fitem_code = strfitem_code;
                            drNew.fitem_name = strfname_item;
                            drNew.fitem_unit = strfitem_unit;
                            drNew.forder_by = strforder_by;
                            listSam_Jy_Result.Add(drNew);

                            //strSql_Result.Append("insert into " + table_SAM_JY_RESULT + "(");
                            //strSql_Result.Append("fresult_id,fapply_id,fitem_id,fitem_code,fitem_name,fitem_unit,forder_by");
                            //strSql_Result.Append(")");
                            //strSql_Result.Append(" values (");
                            //strSql_Result.Append("'" + this.() + "',");
                            //strSql_Result.Append("'" + model.fapply_id + "',");

                            //strSql_Result.Append(")");
                            //addList.Add(strSql_Result.ToString());

                        }
                        this.dataGridView选中项目.DataSource = new BindingList<SAM_JY_RESULT>(listSam_Jy_Result);
                        this.dataGridView选中项目.Refresh();
                        this.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }



        private void toolStripButtonC_Click(object sender, EventArgs e)
        {
            v取消所选项目(1);
        }
        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {

            if (keyData == (Keys.F5))
            {
                toolStripButtonS.PerformClick();
                return true;
            }
            if (keyData == (Keys.F2))
            {
                toolStripButtonC.PerformClick();
                return true;
            }
            if (keyData == (Keys.F1))
            {
                toolStripButtonHelp.PerformClick();
                return true;
            }

            if (keyData == (Keys.F3))
            {
                toolStripButton1.PerformClick();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                str申请单id = "";
                if (!ww.wwf.com.Public.IsNumber(this.tb年龄.Text))
                {
                    WWMessage.MessageShowWarning("年龄必须为数字，请填写后重试！");
                    this.ActiveControl = this.tb年龄;
                    return;
                }
                if (this.listSam_Jy_Result.Count == 0)
                {
                    WWMessage.MessageShowWarning("检验项目不能为空，请选择项目后重试！");
                    return;
                }
                if (WWMessage.MessageDialogResult("确定提交申请单？"))
                {
                    string strTime = this.bllApply.DbServerDateTim();
                    HIS.Model.SAM_APPLY sam_apply = new HIS.Model.SAM_APPLY();

                    this.str申请单id = this.bllApply.DbNum("sam_fapply_id");
                    sam_apply.fapply_id = str申请单id;

                    barcodeNETWindows1.BarcodeText = str申请单id;

                    sam_apply.fjytype_id = strfCheck_Type_id;
                    sam_apply.fsample_type_id = strfSample_Type_id;
                    if (radioButtonJZ.Checked)
                        sam_apply.fjz_flag = 2;//急诊否
                    else
                        sam_apply.fjz_flag = 1;
                    sam_apply.fcharge_flag = 0;//收费否
                    sam_apply.fstate = "1";//状态
                    sam_apply.fapply_user_id = this.tb主管医生编码.Text;
                    sam_apply.fapply_dept_id = this.tb科室编码.Text;
                    sam_apply.fapply_time = strTime;
                    sam_apply.ftype_id = this.cbftype_id.SelectedValue.ToString();
                    sam_apply.fhz_id = this.tb患者ID.Text;
                    sam_apply.fhz_zyh = this.tb住院号码.Text;
                    sam_apply.fsex = this.cb性别.SelectedItem.ToString();
                    sam_apply.fname = this.tb病人姓名.Text;
                    sam_apply.fage = Convert.ToInt32(this.tb年龄.Text);
                    sam_apply.fage_unit = this.cb年龄单位.SelectedItem.ToString();
                    sam_apply.froom_num = this.tb房间号.Text;
                    sam_apply.fbed_num = this.tb床号.Text;
                    sam_apply.fdiagnose = this.tb诊断.Text;
                    sam_apply.fcreate_user_id = LoginBLL.strPersonID;
                    sam_apply.fcreate_time = strTime;
                    sam_apply.fupdate_user_id = LoginBLL.strPersonID;
                    sam_apply.fupdate_time = strTime;
                    //
                    sam_apply.fxhdb = 0;
                    sam_apply.fxyld = 0;
                    sam_apply.fheat = 0;
                    sam_apply.fage_day = 0;
                    sam_apply.fage_month = 0;
                    sam_apply.fage_year = 0;
                    sam_apply.fjyf = 0;//检验费                   
                    //
                    string strItemGroupName = "";
                    if (DataGridView组合名称.Rows.Count > 0)
                    {
                        if (DataGridView组合名称.CurrentRow.Cells["fname"].Value != null)
                            strItemGroupName = DataGridView组合名称.CurrentRow.Cells["fname"].Value.ToString();
                    }

                    if (this.bllApply.BllApplyAdd(sam_apply, listSam_Jy_Result, strItemGroupName, barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG)))
                    {
                        //保存检验申请条码号    
                        //  dr["FBarCodeImg"] = barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG); ;//条形码  
                        //int strSaveBCode = this.bllApply.BllApplyBCodeAdd(str申请单id, barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG)); //条形码  );

                        v取消所选项目(0);
                    }
                    else
                    {
                        WWMessage.MessageShowError("数据保存异常");
                    }
                    WWPrint(1);
                    /*
                     Name	Code	Data Type	Primary	Foreign Key	Mandatory
申请_id/申请条码号	fapply_id	varchar(32)	TRUE	FALSE	TRUE
检验类型_id	fjytype_id	varchar(32)	FALSE	FALSE	FALSE
样本类型_id	fsample_type_id	varchar(32)	FALSE	FALSE	FALSE
急诊否	fjz_flag	int	FALSE	FALSE	FALSE
收费否	fcharge_flag	int	FALSE	FALSE	FALSE
费别_id	fcharge_id	varchar(32)	FALSE	FALSE	FALSE
状态	fstate	varchar(32)	FALSE	FALSE	FALSE
检验费	fjyf	float	FALSE	FALSE	FALSE
R申请人	fapply_user_id	varchar(32)	FALSE	FALSE	FALSE
R申请科室_id	fapply_dept_id	varchar(32)	FALSE	FALSE	FALSE
R申请时间	fapply_time	varchar(32)	FALSE	FALSE	FALSE
P患者类型_id	ftype_id	varchar(32)	FALSE	FALSE	FALSE
P患者_id	fhz_id	varchar(32)	FALSE	FALSE	FALSE
P住院号	fhz_zyh	varchar(32)	FALSE	FALSE	FALSE
P性别	fsex	varchar(32)	FALSE	FALSE	FALSE
P患者姓名	fname	varchar(32)	FALSE	FALSE	FALSE
P民族	fvolk	varchar(32)	FALSE	FALSE	FALSE
P婚姻	fhymen	varchar(32)	FALSE	FALSE	FALSE
P年龄	fage	int	FALSE	FALSE	FALSE
P职业	fjob	varchar(32)	FALSE	FALSE	FALSE
P过敏史	Pfgms	varchar(100)	FALSE	FALSE	FALSE
P工作单位	fjob_org	varchar(128)	FALSE	FALSE	FALSE
P住址	fadd	varchar(200)	FALSE	FALSE	FALSE
PTel	ftel	varchar(64)	FALSE	FALSE	FALSE
P年龄单位	fage_unit	varchar(32)	FALSE	FALSE	FALSE
F年龄(年)	fage_year	int	FALSE	FALSE	FALSE
F年龄(月)	fage_month	int	FALSE	FALSE	FALSE
F年龄(天)	fage_day	int	FALSE	FALSE	FALSE
P病区	fward_num	varchar(32)	FALSE	FALSE	FALSE
P房间号	froom_num	varchar(32)	FALSE	FALSE	FALSE
P床号	fbed_num	varchar(32)	FALSE	FALSE	FALSE
PABO血型	fblood_abo	varchar(32)	FALSE	FALSE	FALSE
PRH血型	fblood_rh	varchar(32)	FALSE	FALSE	FALSE
P检验目的	fjymd	varchar(200)	FALSE	FALSE	FALSE
P临床诊断	fdiagnose	varchar(200)	FALSE	FALSE	FALSE
P体温	fheat	float	FALSE	FALSE	FALSE
P吸氧浓度	fxyld	float	FALSE	FALSE	FALSE
P曾用药	fcyy	varchar(200)	FALSE	FALSE	FALSE
P血红蛋白	fxhdb	int	FALSE	FALSE	FALSE
创建人_id	fcreate_user_id	varchar(32)	FALSE	FALSE	FALSE
创建时间	fcreate_time	varchar(32)	FALSE	FALSE	FALSE
修改人_id	fupdate_user_id	varchar(32)	FALSE	FALSE	FALSE
修改时间	fupdate_time	varchar(32)	FALSE	FALSE	FALSE
备注	fremark	varchar(200)	FALSE	FALSE	FALSE
                     */
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void toolStripButtonHelp_Click(object sender, EventArgs e)
        {

            try
            {
                HelpForm help = new HelpForm("c7e684b9348a475880b1085e1d829e71");
                help.Show();
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }




        private void tbfhz_id_TextChanged(object sender, EventArgs e)
        {

        }


        #region 打印
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="printType"></param>
        private void WWPrint(int printType)
        {
            try
            {
                ApplyPrint pr = new ApplyPrint();
                pr.BllPrintViewer(printType, this.str申请单id);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        #endregion

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            WWPrint(1);
            //  barcodeNETWindows1.BarcodeText = "9999";
            //  dr["FBarCodeImg"] = barcodeNETWindows1.GetBarcodeBitmap(BarcodeNETWorkShop.Core.FILE_FORMAT.JPG); ;//条形码  
        }

        private void treeList组合名称_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {
            var model = treeList组合名称.GetDataRecordByNode(e.Node) as PojoExaminationGroup;
            refreshContent(model.套餐编号);
        }

        private void treeList组合名称_AfterCheckNode(object sender, NodeEventArgs e)
        {
            try
            {
                //selectedGroup.Clear();
                //if (e.Node.ParentNode == null)
                //{
                //    return;
                //}
                //var treeListNodes = e.Node.ParentNode.Nodes.Where(c => c.Checked == true).ToList();
                //if (treeListNodes.Count == 0)
                //{
                //    return;
                //}
                //foreach (var item in treeListNodes)
                //{
                //    var model = treeListExaminationGroup.GetDataRecordByNode(item) as PojoExaminationGroup;
                //    selectedGroup.Add(model);
                //}
                //var item2 = treeListExaminationGroup.GetDataRecordByNode(treeListNodes.LastOrDefault()) as PojoExaminationGroup;
                //refreshContent(item2.套餐编号);
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        void refreshContent(int bh)
        {
            //ExaminationGroupContentes = chis.GY套餐明细.Where(c => c.套餐编号 == bh).ToList();
            //gridControlExaminationGroup.DataSource = ExaminationGroupContentes;
            //gridViewExaminationGroup.PopulateColumns();
            //gridViewExaminationGroup.Columns["IsChanged"].Visible = false;
            //gridViewExaminationGroup.Columns["ID"].Visible = false;
            //gridViewExaminationGroup.Columns["是否药品"].Visible = false;
            //gridViewExaminationGroup.Columns["createTime"].Visible = false;
            //gridViewExaminationGroup.BestFitColumns();
        }

    }
}