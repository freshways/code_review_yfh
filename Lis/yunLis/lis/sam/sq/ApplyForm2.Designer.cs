﻿namespace yunLis.lis.sam.sq
{
    partial class ApplyForm2
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplyForm2));
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.wwTreeViewCheckType = new ww.wwf.control.WWControlLib.WWTreeView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButtonJZ = new System.Windows.Forms.RadioButton();
            this.radioButtonCG = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb房间号 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb年龄 = new System.Windows.Forms.TextBox();
            this.buttonDept = new System.Windows.Forms.Button();
            this.buttonuser_id = new System.Windows.Forms.Button();
            this.tb诊断 = new System.Windows.Forms.TextBox();
            this.tb主管医生姓名 = new System.Windows.Forms.TextBox();
            this.tb主管医生编码 = new System.Windows.Forms.TextBox();
            this.cbftype_id = new System.Windows.Forms.ComboBox();
            this.tb科室名称 = new System.Windows.Forms.TextBox();
            this.tb科室编码 = new System.Windows.Forms.TextBox();
            this.tb床号 = new System.Windows.Forms.TextBox();
            this.tb住院号码 = new System.Windows.Forms.TextBox();
            this.cb年龄单位 = new System.Windows.Forms.ComboBox();
            this.cb性别 = new System.Windows.Forms.ComboBox();
            this.tb患者ID = new System.Windows.Forms.TextBox();
            this.tb病人姓名 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.barcodeNETWindows1 = new BarcodeNETWorkShop.BarcodeNETWindows();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridViewAllItem = new System.Windows.Forms.DataGridView();
            this.sel_item = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fitem_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname_item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funit_name_zw = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by_item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DataGridView组合名称 = new System.Windows.Forms.DataGridView();
            this.sel = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsam_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcheck_type_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fgroup_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dataGridView选中项目 = new System.Windows.Forms.DataGridView();
            this.fitem_code_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_name_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fitem_id_ok = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonC = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.searchControlFilter = new DevExpress.XtraEditors.SearchControl();
            this.treeList组合名称 = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridControl选中的项目 = new DevExpress.XtraGrid.GridControl();
            this.gridView选中的项目 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllItem)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView组合名称)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView选中项目)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControlFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList组合名称)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl选中的项目)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView选中的项目)).BeginInit();
            this.SuspendLayout();
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(0, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 341);
            this.panel1.TabIndex = 134;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.wwTreeViewCheckType);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 245);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(231, 96);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "检验类别";
            // 
            // wwTreeViewCheckType
            // 
            this.wwTreeViewCheckType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeViewCheckType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wwTreeViewCheckType.ImageIndex = 0;
            this.wwTreeViewCheckType.ImageList = this.imageListTree;
            this.wwTreeViewCheckType.Location = new System.Drawing.Point(3, 44);
            this.wwTreeViewCheckType.Name = "wwTreeViewCheckType";
            this.wwTreeViewCheckType.SelectedImageIndex = 1;
            this.wwTreeViewCheckType.Size = new System.Drawing.Size(225, 49);
            this.wwTreeViewCheckType.TabIndex = 134;
            this.wwTreeViewCheckType.ZADataTable = null;
            this.wwTreeViewCheckType.ZADisplayFieldName = "";
            this.wwTreeViewCheckType.ZAKeyFieldName = "";
            this.wwTreeViewCheckType.ZAParentFieldName = "";
            this.wwTreeViewCheckType.ZAToolTipTextName = "";
            this.wwTreeViewCheckType.ZATreeViewRootValue = "";
            this.wwTreeViewCheckType.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tree检验类型_AfterSelect);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButtonJZ);
            this.panel3.Controls.Add(this.radioButtonCG);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 18);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(225, 26);
            this.panel3.TabIndex = 0;
            // 
            // radioButtonJZ
            // 
            this.radioButtonJZ.AutoSize = true;
            this.radioButtonJZ.Location = new System.Drawing.Point(92, 5);
            this.radioButtonJZ.Name = "radioButtonJZ";
            this.radioButtonJZ.Size = new System.Drawing.Size(49, 18);
            this.radioButtonJZ.TabIndex = 1;
            this.radioButtonJZ.Text = "急诊";
            this.radioButtonJZ.UseVisualStyleBackColor = true;
            // 
            // radioButtonCG
            // 
            this.radioButtonCG.AutoSize = true;
            this.radioButtonCG.Checked = true;
            this.radioButtonCG.Location = new System.Drawing.Point(16, 5);
            this.radioButtonCG.Name = "radioButtonCG";
            this.radioButtonCG.Size = new System.Drawing.Size(49, 18);
            this.radioButtonCG.TabIndex = 0;
            this.radioButtonCG.TabStop = true;
            this.radioButtonCG.Text = "常规";
            this.radioButtonCG.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb房间号);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tb年龄);
            this.groupBox1.Controls.Add(this.buttonDept);
            this.groupBox1.Controls.Add(this.buttonuser_id);
            this.groupBox1.Controls.Add(this.tb诊断);
            this.groupBox1.Controls.Add(this.tb主管医生姓名);
            this.groupBox1.Controls.Add(this.tb主管医生编码);
            this.groupBox1.Controls.Add(this.cbftype_id);
            this.groupBox1.Controls.Add(this.tb科室名称);
            this.groupBox1.Controls.Add(this.tb科室编码);
            this.groupBox1.Controls.Add(this.tb床号);
            this.groupBox1.Controls.Add(this.tb住院号码);
            this.groupBox1.Controls.Add(this.cb年龄单位);
            this.groupBox1.Controls.Add(this.cb性别);
            this.groupBox1.Controls.Add(this.tb患者ID);
            this.groupBox1.Controls.Add(this.tb病人姓名);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(231, 245);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "患者信息";
            // 
            // tb房间号
            // 
            this.tb房间号.Location = new System.Drawing.Point(58, 62);
            this.tb房间号.Name = "tb房间号";
            this.tb房间号.Size = new System.Drawing.Size(51, 22);
            this.tb房间号.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 66);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 14);
            this.label12.TabIndex = 27;
            this.label12.Text = "房间:";
            // 
            // tb年龄
            // 
            this.tb年龄.Location = new System.Drawing.Point(136, 164);
            this.tb年龄.Name = "tb年龄";
            this.tb年龄.Size = new System.Drawing.Size(28, 22);
            this.tb年龄.TabIndex = 13;
            // 
            // buttonDept
            // 
            this.buttonDept.Image = ((System.Drawing.Image)(resources.GetObject("buttonDept.Image")));
            this.buttonDept.Location = new System.Drawing.Point(178, 35);
            this.buttonDept.Name = "buttonDept";
            this.buttonDept.Size = new System.Drawing.Size(29, 27);
            this.buttonDept.TabIndex = 26;
            this.buttonDept.UseVisualStyleBackColor = true;
            this.buttonDept.Click += new System.EventHandler(this.buttonDept_Click);
            // 
            // buttonuser_id
            // 
            this.buttonuser_id.Image = ((System.Drawing.Image)(resources.GetObject("buttonuser_id.Image")));
            this.buttonuser_id.Location = new System.Drawing.Point(178, 215);
            this.buttonuser_id.Name = "buttonuser_id";
            this.buttonuser_id.Size = new System.Drawing.Size(29, 27);
            this.buttonuser_id.TabIndex = 25;
            this.buttonuser_id.UseVisualStyleBackColor = true;
            this.buttonuser_id.Click += new System.EventHandler(this.buttonuser_id_Click);
            // 
            // tb诊断
            // 
            this.tb诊断.Location = new System.Drawing.Point(58, 190);
            this.tb诊断.Name = "tb诊断";
            this.tb诊断.Size = new System.Drawing.Size(149, 22);
            this.tb诊断.TabIndex = 24;
            // 
            // tb主管医生姓名
            // 
            this.tb主管医生姓名.Location = new System.Drawing.Point(80, 216);
            this.tb主管医生姓名.Name = "tb主管医生姓名";
            this.tb主管医生姓名.ReadOnly = true;
            this.tb主管医生姓名.Size = new System.Drawing.Size(94, 22);
            this.tb主管医生姓名.TabIndex = 23;
            this.tb主管医生姓名.DoubleClick += new System.EventHandler(this.tbfapply_user_name_DoubleClick);
            // 
            // tb主管医生编码
            // 
            this.tb主管医生编码.Location = new System.Drawing.Point(135, 216);
            this.tb主管医生编码.Name = "tb主管医生编码";
            this.tb主管医生编码.Size = new System.Drawing.Size(39, 22);
            this.tb主管医生编码.TabIndex = 22;
            this.tb主管医生编码.Visible = false;
            // 
            // cbftype_id
            // 
            this.cbftype_id.DisplayMember = "fname";
            this.cbftype_id.FormattingEnabled = true;
            this.cbftype_id.Location = new System.Drawing.Point(58, 12);
            this.cbftype_id.Name = "cbftype_id";
            this.cbftype_id.Size = new System.Drawing.Size(149, 22);
            this.cbftype_id.TabIndex = 21;
            this.cbftype_id.ValueMember = "fname";
            // 
            // tb科室名称
            // 
            this.tb科室名称.Location = new System.Drawing.Point(58, 36);
            this.tb科室名称.Name = "tb科室名称";
            this.tb科室名称.ReadOnly = true;
            this.tb科室名称.Size = new System.Drawing.Size(116, 22);
            this.tb科室名称.TabIndex = 19;
            this.tb科室名称.DoubleClick += new System.EventHandler(this.tbfdept_name_DoubleClick);
            // 
            // tb科室编码
            // 
            this.tb科室编码.Location = new System.Drawing.Point(68, 36);
            this.tb科室编码.Name = "tb科室编码";
            this.tb科室编码.Size = new System.Drawing.Size(60, 22);
            this.tb科室编码.TabIndex = 17;
            this.tb科室编码.Visible = false;
            // 
            // tb床号
            // 
            this.tb床号.Location = new System.Drawing.Point(156, 62);
            this.tb床号.Name = "tb床号";
            this.tb床号.Size = new System.Drawing.Size(51, 22);
            this.tb床号.TabIndex = 16;
            // 
            // tb住院号码
            // 
            this.tb住院号码.Location = new System.Drawing.Point(58, 87);
            this.tb住院号码.Name = "tb住院号码";
            this.tb住院号码.Size = new System.Drawing.Size(149, 22);
            this.tb住院号码.TabIndex = 15;
            // 
            // cb年龄单位
            // 
            this.cb年龄单位.FormattingEnabled = true;
            this.cb年龄单位.Items.AddRange(new object[] {
            "岁",
            "月",
            "天",
            "?"});
            this.cb年龄单位.Location = new System.Drawing.Point(167, 164);
            this.cb年龄单位.Name = "cb年龄单位";
            this.cb年龄单位.Size = new System.Drawing.Size(40, 22);
            this.cb年龄单位.TabIndex = 14;
            // 
            // cb性别
            // 
            this.cb性别.FormattingEnabled = true;
            this.cb性别.Items.AddRange(new object[] {
            "男",
            "女",
            "?"});
            this.cb性别.Location = new System.Drawing.Point(58, 164);
            this.cb性别.Name = "cb性别";
            this.cb性别.Size = new System.Drawing.Size(40, 22);
            this.cb性别.TabIndex = 12;
            // 
            // tb患者ID
            // 
            this.tb患者ID.Location = new System.Drawing.Point(58, 113);
            this.tb患者ID.Name = "tb患者ID";
            this.tb患者ID.Size = new System.Drawing.Size(149, 22);
            this.tb患者ID.TabIndex = 11;
            this.tb患者ID.TextChanged += new System.EventHandler(this.tbfhz_id_TextChanged);
            // 
            // tb病人姓名
            // 
            this.tb病人姓名.Location = new System.Drawing.Point(58, 139);
            this.tb病人姓名.Name = "tb病人姓名";
            this.tb病人姓名.Size = new System.Drawing.Size(149, 22);
            this.tb病人姓名.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 220);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 14);
            this.label10.TabIndex = 9;
            this.label10.Text = "主管医生:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 14);
            this.label9.TabIndex = 8;
            this.label9.Text = "诊断:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(100, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 7;
            this.label8.Text = "年龄:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 169);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 6;
            this.label7.Text = "性别:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 14);
            this.label6.TabIndex = 5;
            this.label6.Text = "姓名:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 4;
            this.label5.Text = "患者ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "住院号:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(112, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "床号:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "科室:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "类别:";
            // 
            // barcodeNETWindows1
            // 
            this.barcodeNETWindows1.AntiAlias = false;
            this.barcodeNETWindows1.AutoSize = true;
            this.barcodeNETWindows1.BackColor = System.Drawing.Color.White;
            this.barcodeNETWindows1.BarcodeColor = System.Drawing.Color.Black;
            this.barcodeNETWindows1.BarcodeGap = new BarcodeNETWorkShop.Core.TextGap(0, 0, 1, 0);
            this.barcodeNETWindows1.BarcodeMargins = new BarcodeNETWorkShop.Core.MarginBound(2, 2, 0, 0);
            this.barcodeNETWindows1.BarcodeText = "000000";
            this.barcodeNETWindows1.BarcodeType = BarcodeNETWorkShop.Core.BARCODE_TYPE.EAN128C;
            this.barcodeNETWindows1.BarHeight = 20;
            this.barcodeNETWindows1.BarWidth = 1;
            this.barcodeNETWindows1.BgColor = System.Drawing.Color.White;
            this.barcodeNETWindows1.CustomText = "";
            this.barcodeNETWindows1.ExceptionType = BarcodeNETWorkShop.Core.EXCEPTION_TYPE.DEFAULT_MSG;
            this.barcodeNETWindows1.FileFormat = BarcodeNETWorkShop.Core.FILE_FORMAT.JPG;
            this.barcodeNETWindows1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.barcodeNETWindows1.IncludeChecksumDigit = true;
            this.barcodeNETWindows1.IsRounded = false;
            this.barcodeNETWindows1.Location = new System.Drawing.Point(145, 185);
            this.barcodeNETWindows1.Name = "barcodeNETWindows1";
            this.barcodeNETWindows1.RotateAngle = BarcodeNETWorkShop.Core.ROTATE_ANGLE.R0;
            this.barcodeNETWindows1.ShowBarcodeText = true;
            this.barcodeNETWindows1.ShowBorder = false;
            this.barcodeNETWindows1.SilentMode = false;
            this.barcodeNETWindows1.Size = new System.Drawing.Size(73, 41);
            this.barcodeNETWindows1.SupplementalText = "";
            this.barcodeNETWindows1.SupplementalTextStyle = BarcodeNETWorkShop.Core.SUPPLEMENTAL_TEXT_STYLE.TOP;
            this.barcodeNETWindows1.TabIndex = 62;
            this.barcodeNETWindows1.Tag = ".";
            this.barcodeNETWindows1.TextColor = System.Drawing.Color.Black;
            this.barcodeNETWindows1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.barcodeNETWindows1.TopText = ".";
            this.barcodeNETWindows1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Location = new System.Drawing.Point(216, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(635, 132);
            this.panel2.TabIndex = 135;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.barcodeNETWindows1);
            this.groupBox4.Controls.Add(this.dataGridViewAllItem);
            this.groupBox4.Location = new System.Drawing.Point(210, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(435, 354);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "单项";
            // 
            // dataGridViewAllItem
            // 
            this.dataGridViewAllItem.AllowUserToAddRows = false;
            this.dataGridViewAllItem.AllowUserToResizeRows = false;
            this.dataGridViewAllItem.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewAllItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewAllItem.ColumnHeadersHeight = 21;
            this.dataGridViewAllItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sel_item,
            this.fitem_code,
            this.fname_item,
            this.funit_name_zw,
            this.forder_by_item,
            this.fitem_id});
            this.dataGridViewAllItem.EnableHeadersVisualStyles = false;
            this.dataGridViewAllItem.Location = new System.Drawing.Point(3, 20);
            this.dataGridViewAllItem.MultiSelect = false;
            this.dataGridViewAllItem.Name = "dataGridViewAllItem";
            this.dataGridViewAllItem.RowHeadersVisible = false;
            this.dataGridViewAllItem.RowHeadersWidth = 35;
            this.dataGridViewAllItem.RowTemplate.Height = 23;
            this.dataGridViewAllItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAllItem.Size = new System.Drawing.Size(385, 113);
            this.dataGridViewAllItem.TabIndex = 134;
            this.dataGridViewAllItem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAllItem_CellContentClick);
            // 
            // sel_item
            // 
            this.sel_item.FalseValue = "0";
            this.sel_item.HeaderText = "选";
            this.sel_item.IndeterminateValue = "0";
            this.sel_item.Name = "sel_item";
            this.sel_item.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sel_item.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sel_item.TrueValue = "1";
            this.sel_item.Width = 25;
            // 
            // fitem_code
            // 
            this.fitem_code.DataPropertyName = "fitem_code";
            this.fitem_code.HeaderText = "代号";
            this.fitem_code.Name = "fitem_code";
            this.fitem_code.ReadOnly = true;
            this.fitem_code.Width = 75;
            // 
            // fname_item
            // 
            this.fname_item.DataPropertyName = "fname";
            this.fname_item.HeaderText = "名称";
            this.fname_item.Name = "fname_item";
            this.fname_item.ReadOnly = true;
            this.fname_item.Width = 130;
            // 
            // funit_name_zw
            // 
            this.funit_name_zw.DataPropertyName = "funit_name_zw";
            this.funit_name_zw.HeaderText = "单位";
            this.funit_name_zw.Name = "funit_name_zw";
            this.funit_name_zw.Width = 80;
            // 
            // forder_by_item
            // 
            this.forder_by_item.DataPropertyName = "fprint_num";
            this.forder_by_item.HeaderText = "forder_by";
            this.forder_by_item.Name = "forder_by_item";
            this.forder_by_item.Visible = false;
            // 
            // fitem_id
            // 
            this.fitem_id.DataPropertyName = "fitem_id";
            this.fitem_id.HeaderText = "fitem_id";
            this.fitem_id.Name = "fitem_id";
            this.fitem_id.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.DataGridView组合名称);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(210, 132);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "组合项目";
            // 
            // DataGridView组合名称
            // 
            this.DataGridView组合名称.AllowUserToAddRows = false;
            this.DataGridView组合名称.AllowUserToResizeRows = false;
            this.DataGridView组合名称.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridView组合名称.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGridView组合名称.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sel,
            this.fname,
            this.fsam_type_id,
            this.fcheck_type_id,
            this.fgroup_id});
            this.DataGridView组合名称.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridView组合名称.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DataGridView组合名称.EnableHeadersVisualStyles = false;
            this.DataGridView组合名称.Location = new System.Drawing.Point(3, 18);
            this.DataGridView组合名称.MultiSelect = false;
            this.DataGridView组合名称.Name = "DataGridView组合名称";
            this.DataGridView组合名称.RowHeadersVisible = false;
            this.DataGridView组合名称.RowTemplate.Height = 23;
            this.DataGridView组合名称.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridView组合名称.Size = new System.Drawing.Size(204, 111);
            this.DataGridView组合名称.TabIndex = 133;
            this.DataGridView组合名称.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAllItem_CellContentClick);
            // 
            // sel
            // 
            this.sel.DataPropertyName = "sel";
            this.sel.FalseValue = "0";
            this.sel.HeaderText = "选";
            this.sel.IndeterminateValue = "0";
            this.sel.Name = "sel";
            this.sel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sel.TrueValue = "1";
            this.sel.Width = 25;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.ReadOnly = true;
            this.fname.Width = 200;
            // 
            // fsam_type_id
            // 
            this.fsam_type_id.DataPropertyName = "fsam_type_id";
            this.fsam_type_id.HeaderText = "fsam_type_id";
            this.fsam_type_id.Name = "fsam_type_id";
            this.fsam_type_id.Visible = false;
            // 
            // fcheck_type_id
            // 
            this.fcheck_type_id.DataPropertyName = "fcheck_type_id";
            this.fcheck_type_id.HeaderText = "fcheck_type_id";
            this.fcheck_type_id.Name = "fcheck_type_id";
            this.fcheck_type_id.Visible = false;
            // 
            // fgroup_id
            // 
            this.fgroup_id.DataPropertyName = "fgroup_id";
            this.fgroup_id.HeaderText = "fgroup_id";
            this.fgroup_id.Name = "fgroup_id";
            this.fgroup_id.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dataGridView选中项目);
            this.groupBox5.Location = new System.Drawing.Point(306, 220);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(419, 124);
            this.groupBox5.TabIndex = 136;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "选中项目";
            // 
            // dataGridView选中项目
            // 
            this.dataGridView选中项目.AllowUserToAddRows = false;
            this.dataGridView选中项目.AllowUserToResizeRows = false;
            this.dataGridView选中项目.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView选中项目.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView选中项目.ColumnHeadersHeight = 21;
            this.dataGridView选中项目.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fitem_code_ok,
            this.fitem_name_ok,
            this.fitem_unit,
            this.fitem_id_ok,
            this.forder_by});
            this.dataGridView选中项目.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView选中项目.EnableHeadersVisualStyles = false;
            this.dataGridView选中项目.Location = new System.Drawing.Point(3, 18);
            this.dataGridView选中项目.MultiSelect = false;
            this.dataGridView选中项目.Name = "dataGridView选中项目";
            this.dataGridView选中项目.RowHeadersVisible = false;
            this.dataGridView选中项目.RowHeadersWidth = 35;
            this.dataGridView选中项目.RowTemplate.Height = 23;
            this.dataGridView选中项目.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView选中项目.Size = new System.Drawing.Size(413, 103);
            this.dataGridView选中项目.TabIndex = 135;
            // 
            // fitem_code_ok
            // 
            this.fitem_code_ok.DataPropertyName = "fitem_code";
            this.fitem_code_ok.HeaderText = "代号";
            this.fitem_code_ok.Name = "fitem_code_ok";
            this.fitem_code_ok.ReadOnly = true;
            // 
            // fitem_name_ok
            // 
            this.fitem_name_ok.DataPropertyName = "fitem_name";
            this.fitem_name_ok.HeaderText = "名称";
            this.fitem_name_ok.Name = "fitem_name_ok";
            this.fitem_name_ok.ReadOnly = true;
            this.fitem_name_ok.Width = 200;
            // 
            // fitem_unit
            // 
            this.fitem_unit.DataPropertyName = "fitem_unit";
            this.fitem_unit.HeaderText = "单位";
            this.fitem_unit.Name = "fitem_unit";
            this.fitem_unit.Width = 80;
            // 
            // fitem_id_ok
            // 
            this.fitem_id_ok.DataPropertyName = "fitem_id";
            this.fitem_id_ok.HeaderText = "fitem_id";
            this.fitem_id_ok.Name = "fitem_id_ok";
            this.fitem_id_ok.Visible = false;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Visible = false;
            this.forder_by.Width = 40;
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.CountItem = null;
            this.bN.DeleteItem = null;
            this.bN.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.toolStripButtonS,
            this.toolStripSeparator2,
            this.toolStripButtonC,
            this.toolStripSeparator1,
            this.toolStripButton1,
            this.toolStripSeparator3,
            this.toolStripButtonHelp});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = null;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(1015, 35);
            this.bN.TabIndex = 138;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = global::yunLis.Properties.Resources.button_tj;
            this.toolStripButtonS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Size = new System.Drawing.Size(129, 32);
            this.toolStripButtonS.Text = "提交申请单(F5)    ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonC
            // 
            this.toolStripButtonC.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonC.Image")));
            this.toolStripButtonC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonC.Name = "toolStripButtonC";
            this.toolStripButtonC.Size = new System.Drawing.Size(133, 32);
            this.toolStripButtonC.Text = "取消所选项目(F2)  ";
            this.toolStripButtonC.Click += new System.EventHandler(this.toolStripButtonC_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::yunLis.Properties.Resources.button_print1;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(93, 32);
            this.toolStripButton1.Text = "打印(F3)    ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(85, 32);
            this.toolStripButtonHelp.Text = "帮助(F1)  ";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.searchControlFilter);
            this.layoutControl1.Controls.Add(this.treeList组合名称);
            this.layoutControl1.Location = new System.Drawing.Point(8, 383);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(432, 183);
            this.layoutControl1.TabIndex = 139;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // searchControlFilter
            // 
            this.searchControlFilter.Location = new System.Drawing.Point(42, 10);
            this.searchControlFilter.Name = "searchControlFilter";
            this.searchControlFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControlFilter.Size = new System.Drawing.Size(379, 20);
            this.searchControlFilter.StyleController = this.layoutControl1;
            this.searchControlFilter.TabIndex = 140;
            // 
            // treeList组合名称
            // 
            this.treeList组合名称.Location = new System.Drawing.Point(11, 34);
            this.treeList组合名称.Name = "treeList组合名称";
            this.treeList组合名称.OptionsBehavior.Editable = false;
            this.treeList组合名称.OptionsView.ShowCheckBoxes = true;
            this.treeList组合名称.Size = new System.Drawing.Size(410, 139);
            this.treeList组合名称.TabIndex = 4;
            this.treeList组合名称.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeList组合名称_AfterCheckNode);
            this.treeList组合名称.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeList组合名称_FocusedNodeChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem1});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(432, 183);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.treeList组合名称;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(414, 143);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.searchControlFilter;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(414, 24);
            this.layoutControlItem1.Text = "筛选:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(28, 14);
            // 
            // gridControl选中的项目
            // 
            this.gridControl选中的项目.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl选中的项目.Location = new System.Drawing.Point(481, 349);
            this.gridControl选中的项目.MainView = this.gridView选中的项目;
            this.gridControl选中的项目.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl选中的项目.Name = "gridControl选中的项目";
            this.gridControl选中的项目.Size = new System.Drawing.Size(472, 156);
            this.gridControl选中的项目.TabIndex = 140;
            this.gridControl选中的项目.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView选中的项目});
            // 
            // gridView选中的项目
            // 
            this.gridView选中的项目.DetailHeight = 272;
            this.gridView选中的项目.GridControl = this.gridControl选中的项目;
            this.gridView选中的项目.Name = "gridView选中的项目";
            // 
            // ApplyForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 583);
            this.Controls.Add(this.gridControl选中的项目);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bN);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "ApplyForm2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LIS申请";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ApplyForm_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllItem)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView组合名称)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView选中项目)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControlFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList组合名称)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl选中的项目)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView选中的项目)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButtonJZ;
        private System.Windows.Forms.RadioButton radioButtonCG;
        private System.Windows.Forms.GroupBox groupBox5;
        private ww.wwf.control.WWControlLib.WWTreeView wwTreeViewCheckType;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.TextBox tb病人姓名;
        private System.Windows.Forms.TextBox tb患者ID;
        private System.Windows.Forms.ComboBox cb性别;
        private System.Windows.Forms.ComboBox cb年龄单位;
        private System.Windows.Forms.TextBox tb年龄;
        private System.Windows.Forms.TextBox tb住院号码;
        private System.Windows.Forms.TextBox tb床号;
        private System.Windows.Forms.TextBox tb科室编码;
        private System.Windows.Forms.TextBox tb科室名称;
        private System.Windows.Forms.ComboBox cbftype_id;
        private System.Windows.Forms.TextBox tb主管医生编码;
        private System.Windows.Forms.TextBox tb主管医生姓名;
        private System.Windows.Forms.TextBox tb诊断;
        private System.Windows.Forms.Button buttonuser_id;
        private System.Windows.Forms.Button buttonDept;
        private System.Windows.Forms.DataGridView DataGridView组合名称;
        private System.Windows.Forms.DataGridView dataGridViewAllItem;
        private System.Windows.Forms.DataGridView dataGridView选中项目;
        private System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonC;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sel;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn fsam_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcheck_type_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fgroup_id;
        private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb房间号;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sel_item;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname_item;
        private System.Windows.Forms.DataGridViewTextBoxColumn funit_name_zw;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by_item;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_code_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_name_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn fitem_id_ok;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private BarcodeNETWorkShop.BarcodeNETWindows barcodeNETWindows1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraTreeList.TreeList treeList组合名称;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.SearchControl searchControlFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.GridControl gridControl选中的项目;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView选中的项目;
    }
}