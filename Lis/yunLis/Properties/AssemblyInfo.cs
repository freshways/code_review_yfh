﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using WEISHENG.COMM.PluginsAttribute;
//using System.Security;

// 有关程序集的常规信息通过下列属性集
// 控制。更改这些属性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("yunLis")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("form")]
[assembly: AssemblyCopyright("版权所有 (C)  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 使此程序集中的类型
// 对 COM 组件不可见。如果需要从 COM 访问此程序集中的类型，
// 则将该类型上的 ComVisible 属性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("060ed752-079a-48f1-afbe-d53097466624")]

// 程序集的版本信息由下面四个值组成:
//
//      主版本
//      次版本 
//      内部版本号
//      修订号
//
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]

//[assembly: AllowPartiallyTrustedCallers()] 
[assembly: PluginAssemblyInfo(插件名称 = "LIS", 功能说明 = "检验设备接入模块", 插件版本 = "2.0.0.0", 维护团队 = "",
    联系方式 = "", 插件文件名 = "yunLis", 插件授权 = "", 插件更新日期 = "2020-08-06", 插件是否有效 = true,
    插件网址 = "", 插件GUID = "060ed752-079a-48f1-afbe-d53097466624", 插件运行目录 = "", Tag = null)]
