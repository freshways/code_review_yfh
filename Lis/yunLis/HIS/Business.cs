﻿using HIS.COMM;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using static HIS.COMM.ClassEnum;

namespace yunLis
{
    public class Business
    {

        private static DataTable _dtHISLIS项目对照表;

        public static DataTable dtHISLIS项目对照表
        {
            get
            {
                if (DBConnHelper.sLISConnString != "" && _dtHISLIS项目对照表 == null)
                {
                    string SQL =
                   "SELECT" + "\r\n" +
                   "  [HIS编码]," + "\r\n" +
                   "  [HIS名称]," + "\r\n" +
                   "  [LIS编码]," + "\r\n" +
                   "  [LIS名称]," + "\r\n" +
                   "  [LISID],LIS设备" + "\r\n" +
                   "FROM" + "\r\n" +
                   "  [vw_HisLis项目对照]";
                    _dtHISLIS项目对照表 = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.sLISConnString, CommandType.Text, SQL).Tables[0];
                }
                return _dtHISLIS项目对照表;
            }
            set { _dtHISLIS项目对照表 = value; }
        }


        public static string s用申请唯一标识获取申请单号(string _sID)
        {
            try
            {
                string sql = "select 申请单号 from dbo.JY申请单摘要 where 申请唯一标识='" + _sID + "'";
                return HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, sql).ToString();
            }
            catch (Exception)
            {
                return "";
            }

        }


        public static bool b更新门诊申请单状态(string _sMZID, enLIS申请单状态 _en状态, CHISEntities chis)
        {
            try
            {
                if (baseInfo.B启用LIS申请功能)
                {
                    using (LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
                    {
                        var item = lisEntities.SAM_APPLY.Where(c => c.fhz_id == _sMZID).FirstOrDefault();
                        if (item != null)
                        {
                            item.fstate = ((int)_en状态).ToString();
                            lisEntities.SaveChanges();
                        }
                    }
                }
                else
                {
                    var item申请摘要 = chis.JY申请单摘要.Where(c => c.申请唯一标识 == _sMZID).FirstOrDefault();
                    if (item申请摘要 != null)
                    {
                        item申请摘要.状态 = _en状态.ToString();
                    }                    
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool b生成住院申请单(CHISEntities chis, ZY病人信息 inpatient, HIS.Model.Pojo.Pojo医嘱执行 pojo父医嘱申请摘要, List<HIS.Model.Pojo.Pojo医嘱执行> pojo子医嘱申请明细)
        {
            try
            {

                //bool b是否提交 = true;
                //DataTable dt申请明细 = dt电子医嘱;
                //DataTable dt申请序列 = dt申请明细.DefaultView.ToTable(true, "lisID");
                //var lis申请序列 = pojo子医嘱申请明细.Select(p => new { lisID = p.lisID, HIS组合名称 = p.项目名称 }).Distinct().ToList();



                string s申请单号 = s生成申请单号(enLIS申请类型.住院);
                var inpatient2 = chis.ZY病人信息.Where(c => c.ZYID == inpatient.ZYID).FirstOrDefault();


                Model年龄 item年龄 = new Model年龄();
                HIS.COMM.Helper.AgeHelper.GetAgeByBirthdate(Convert.ToDateTime(inpatient2.出生日期), item年龄);

                chis.JY申请单摘要.Add(new JY申请单摘要()
                {
                    申请类别 = "检验",
                    申请单号 = Convert.ToDecimal(s申请单号),
                    病人ID = Convert.ToInt32(inpatient2.ZYID),
                    病历号 = inpatient2.住院号码,
                    病人类型 = "住院",
                    病人姓名 = inpatient2.病人姓名,
                    性别 = inpatient2.性别,
                    病人年龄 = item年龄.默认格式化年龄,
                    收费类型 = inpatient2.医保类型,
                    临床诊断 = inpatient2.疾病名称,
                    病人病区 = Convert.ToString(inpatient2.病区),
                    床号 = inpatient2.病床,
                    申请科室 = chis.GY科室设置.Where(c => c.科室编码 == inpatient2.科室).SingleOrDefault().科室名称,
                    申请医生 = inpatient2.主治医生编码.ToString(),
                    申请时间 = DateTime.Now,
                    加急标志 = "0",
                    标本类型 = "标本类型",
                    执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称,
                    申请唯一标识 = pojo父医嘱申请摘要.ID.ToString(),
                    身份证号 = inpatient2.身份证号,
                    健康档案号 = "",
                    状态 = "已申请",
                    HIS组合名称 = pojo父医嘱申请摘要.项目名称,
                });
                //string SQL申请摘要 =
                //    "INSERT INTO [JY申请单摘要]([申请类别], [申请单号], [病人ID], [病历号], [病人类型]," + "\r\n" +
                //    "              [病人姓名], [性别],  [病人年龄], [收费类型]," + "\r\n" +
                //    "              [临床诊断], [病人病区], [床号], [申请科室], [申请医生]," + "\r\n" +
                //    "              [申请时间], [加急标志], [标本类型], [执行科室], [申请唯一标识]," + "\r\n" +
                //    "              [身份证号], [健康档案号], [状态], [HIS组合名称]) " + "\r\n" +
                //    "VALUES      ('检验', " + s申请单号 + ", " + zyPerson.sZYID父 + ", '" + zyPerson.住院号码 + "', '住院', '" + zyPerson.S姓名 + "', '" + zyPerson.S性别 + "'," + "\r\n" +
                //    "              '" + zyPerson.S年龄 + "', '" + zyPerson.S医保类型 + "', '" + zyPerson.s疾病名称 + "', '', '', '" + zyPerson.S科室名称 + "'," + "\r\n" +
                //    "             '" + zyPerson.主治医生编码 + "', '" + DateTime.Now.ToString() + "', '0', '标本类型', '" + HIS.COMM.baseInfo.sLIS执行科室名称 + "', '" + row摘要["lisID"].ToString() + "', '" + zyPerson.身份证号 + "'," + "\r\n" +
                //    "             '', '已申请', '" + _sHIS组合名称 + "')";
                //int reSult = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL申请摘要);
                //if (reSult != 1)
                //{
                //    b是否提交 = false;
                //}

                //DataTable dt本序列明细 = HIS.Model.Dal.autoDb.GetNewDataTable(dt申请明细, "lisID=" + row摘要["lisID"].ToString());
                //var lis序列明细 = pojo子医嘱申请明细.Where(c => c.lisID == pojo父医嘱申请摘要.lisID).ToList();
                foreach (var row明细 in pojo子医嘱申请明细)
                {
                    var _sLIS条目 = HIS.COMM.BLL.CacheData.Lis_HisLis项目对照.Where(c => c.his编码 == row明细.项目编码).FirstOrDefault();
                    if (_sLIS条目 == null)
                    {
                        WEISHENG.COMM.msgHelper.ShowInformation(row明细.项目名称 + "获取LIS对照失败，请联系管理员进行设置");
                        return false;
                    }
                    //strLIS条目 _sLIS条目 = sGetLis对照编码(row明细.项目编码.ToString());
                    //if (_sLIS条目.sLIS编码 != null)
                    //{
                    chis.JY申请单明细.Add(new JY申请单明细()
                    {
                        申请单号 = s申请单号,
                        lis申请项目ID = _sLIS条目.lisID,
                        lis申请项目代码 = _sLIS条目.lis编码,
                        lis申请项目名称 = _sLIS条目.lis名称,
                        his申请项目单价 = 0,
                        his申请项目数量 = 1,
                        计价标志 = "0",
                        医嘱ID = pojo父医嘱申请摘要.ID.ToString(),
                        计费ID = "-1",
                        执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称

                    });
                    //string SQL申请明细 =
                    // "INSERT INTO [JY申请单明细]([申请单号], [lis申请项目ID], [lis申请项目代码], [lis申请项目名称]," + "\r\n" +
                    // "              [his申请项目单价], [his申请项目数量], [计价标志], [医嘱ID]," + "\r\n" +
                    // "              [计费ID], [执行科室])" + "\r\n" +
                    // "VALUES      ('" + s申请单号 + "', '" + _sLIS条目.sLISID + "', '" + _sLIS条目.sLIS编码 + "', '" + _sLIS条目.sLIS名称 + "', 0," + "\r\n" +
                    // "             1, '0', '-1', '-1', '" + HIS.COMM.baseInfo.sLIS执行科室名称 + "')";
                    //int reSult2 = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL申请明细);
                    //if (reSult2 != 1)
                    //{
                    //    b是否提交 = false;
                    //}
                    //}
                }
                //}

                return true;

            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
                return false;
            }
        }



        public static bool b生成门诊申请单(CHISEntities chis, MF门诊摘要 mf门诊摘要, List<MZ检查治疗> list检查治疗)
        {
            try
            {
                bool b是否提交 = true;
                List<JY申请单摘要> list检验申请单摘要 = new List<JY申请单摘要>();
                List<JY申请单明细> list检验申请单明细 = new List<JY申请单明细>();
                string s申请单号 = "";
                foreach (var item in list检查治疗)
                {
                    if (item.LIS检查项 == true)
                    {
                        string s现有申请单号 = "0";
                        if (list检验申请单摘要.Count > 0)
                        {
                            s现有申请单号 = list检验申请单摘要.Max(c => c.申请单号).ToString();
                        }

                        //写入JY申请单摘要表
                        if (list检验申请单摘要.Where(c => c.HIS组合名称 == item.套餐名称).ToList().Count == 0 && list检验申请单摘要.Where(c => c.HIS组合名称 == item.诊疗名称).ToList().Count == 0)
                        {
                            s申请单号 = s生成申请单号(enLIS申请类型.门诊, s现有申请单号);
                            JY申请单摘要 jy申请单摘要 = new JY申请单摘要();
                            jy申请单摘要.申请类别 = "检验";
                            jy申请单摘要.申请单号 = Convert.ToDecimal(s申请单号);
                            jy申请单摘要.病人ID = -1;
                            jy申请单摘要.病历号 = "";
                            jy申请单摘要.病人类型 = "门诊";
                            jy申请单摘要.病人姓名 = mf门诊摘要.病人姓名;
                            jy申请单摘要.性别 = mf门诊摘要.性别;
                            jy申请单摘要.病人年龄 = mf门诊摘要.年龄.ToString();
                            jy申请单摘要.收费类型 = mf门诊摘要.医保类型;
                            jy申请单摘要.临床诊断 = mf门诊摘要.临床诊断;
                            jy申请单摘要.病人病区 = "";
                            jy申请单摘要.床号 = "";
                            jy申请单摘要.申请科室 = chis.pubUsers.Where(c => c.用户编码 == mf门诊摘要.医生编码).SingleOrDefault().科室名称;
                            jy申请单摘要.申请医生 = mf门诊摘要.医生编码.ToString();
                            jy申请单摘要.申请时间 = DateTime.Now;
                            jy申请单摘要.加急标志 = "0";
                            jy申请单摘要.标本类型 = "标本类型";
                            jy申请单摘要.执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称;
                            jy申请单摘要.申请唯一标识 = mf门诊摘要.MZID.ToString();
                            jy申请单摘要.身份证号 = mf门诊摘要.身份证号;
                            jy申请单摘要.健康档案号 = "";
                            jy申请单摘要.状态 = "已申请";
                            jy申请单摘要.HIS组合名称 = item.套餐名称 == null ? item.诊疗名称 : item.套餐名称;
                            list检验申请单摘要.Add(jy申请单摘要);
                        }


                        //写入JY申请单明细表
                        var _sLIS条目 = HIS.COMM.BLL.CacheData.Lis_HisLis项目对照.Where(c => c.his编码 == item.诊疗编码).FirstOrDefault();
                        if (_sLIS条目 == null)
                        {
                            _sLIS条目 = new lis_HisLis项目对照();
                            //WEISHENG.COMM.msgHelper.ShowInformation(item.诊疗名称 + "获取LIS对照失败，请联系管理员进行设置");
                            //return false;
                        }

                        JY申请单明细 jy申请单明细 = new JY申请单明细();
                        jy申请单明细.申请单号 = s申请单号;
                        jy申请单明细.lis申请项目ID = _sLIS条目.lisID;
                        jy申请单明细.lis申请项目代码 = _sLIS条目.lis编码;
                        jy申请单明细.lis申请项目名称 = _sLIS条目.lis名称;
                        jy申请单明细.his申请项目单价 = 0;
                        jy申请单明细.his申请项目数量 = 1;
                        jy申请单明细.计价标志 = "0";
                        jy申请单明细.医嘱ID = "-1";
                        jy申请单明细.计费ID = "-1";
                        jy申请单明细.执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称;
                        list检验申请单明细.Add(jy申请单明细);
                    }
                }
                if (list检验申请单摘要.Count > 0)
                {
                    chis.JY申请单摘要.AddRange(list检验申请单摘要);
                }
                if (list检验申请单明细.Count > 0)
                {
                    chis.JY申请单明细.AddRange(list检验申请单明细);
                }
                return true;
            }
            catch (Exception ex)
            {
                WEISHENG.COMM.msgHelper.ShowInformation("生成LIS申请单失败:" + ex.Message);
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public static bool b生成门诊申请单(HIS.COMM.Person门诊病人 mzPerson, DataTable dt电子处方)
        {
            try
            {
                //bool b是否提交 = true;
                //DataTable dt检验申请 = HIS.Model.Dal.autoDb.GetNewDataTable(dt电子处方, "YPID=0 and LIS检查项=1");
                //DataTable dt申请序列 = dt检验申请.DefaultView.ToTable(true, "套餐编号", "套餐名称");
                //foreach (DataRow row摘要 in dt申请序列.Rows)
                //{
                //    string s申请单号 = s生成申请单号(InterFaceLIS.ClassLIS.enLIS申请类型.门诊);
                //    string SQL申请摘要 =
                //        "INSERT INTO [JY申请单摘要]([申请类别], [申请单号], [病人ID], [病历号], [病人类型]," + "\r\n" +
                //        "              [病人姓名], [性别],  [病人年龄], [收费类型]," + "\r\n" +
                //        "              [临床诊断], [病人病区], [床号], [申请科室], [申请医生]," + "\r\n" +
                //        "              [申请时间], [加急标志], [标本类型], [执行科室], [申请唯一标识]," + "\r\n" +
                //        "              [身份证号], [健康档案号], [状态], [HIS组合名称])" + "\r\n" +
                //        "VALUES      ('检验', " + s申请单号 + ", -1, '', '门诊', '" + mzPerson.S姓名 + "', '" + mzPerson.S性别 + "'," + "\r\n" +
                //        "              '" + mzPerson.S年龄 + "', '" + mzPerson.En医保类型.ToString() + "', '" + mzPerson.S临床诊断 + "', '', '', '" + HIS.COMM.zdInfo.ModelUserInfo.科室名称 + "'," + "\r\n" +
                //        "             '" + mzPerson.S医生编码 + "', '" + DateTime.Now.ToString() + "', '0', '标本类型', '" + HIS.COMM.baseInfo.sLIS执行科室名称 + "', '" + mzPerson.sMZID + "', '" + mzPerson.S身份证号 + "'," + "\r\n" +
                //        "             '', '已申请', '" + row摘要["套餐名称"].ToString() + "')";
                //    int reSult = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL申请摘要);
                //    if (reSult != 1)
                //    {
                //        MessageBox.Show("写申请摘要影响行数:" + reSult.ToString(), "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        b是否提交 = false;
                //    }
                //    DataTable dt本序列明细 = HIS.Model.Dal.autoDb.GetNewDataTable(dt检验申请, "套餐名称='" + row摘要["套餐名称"].ToString() + "'");
                //    foreach (DataRow row in dt本序列明细.Rows)
                //    {
                //        strLIS条目 _sLIS条目 = sGetLis对照编码(row["费用序号"].ToString());
                //        if (_sLIS条目.sLIS编码 != null)
                //        {
                //            string SQL申请明细 =
                //             "INSERT INTO [JY申请单明细]([申请单号], [lis申请项目ID], [lis申请项目代码], [lis申请项目名称]," + "\r\n" +
                //             "              [his申请项目单价], [his申请项目数量], [计价标志], [医嘱ID]," + "\r\n" +
                //             "              [计费ID], [执行科室])" + "\r\n" +
                //             "VALUES      ('" + s申请单号 + "', '" + _sLIS条目.sLISID + "', '" + _sLIS条目.sLIS编码 + "', '" + _sLIS条目.sLIS名称 + "', 0," + "\r\n" +
                //             "             1, '0', '-1', '-1', '" + HIS.COMM.baseInfo.sLIS执行科室名称 + "')";
                //            int reSult2 = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL申请明细);
                //            if (reSult2 != 1)
                //            {
                //                MessageBox.Show("写申请摘要影响行数:" + reSult2.ToString(), "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //                b是否提交 = false;
                //            }
                //        }
                //    }
                //}
                //if (b是否提交)
                //{
                //    return true;
                //}
                //else
                //{
                return false;
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }










        private static string s生成申请单号(enLIS申请类型 _en申请类型, string s现有申请单号 = "0")
        {

            decimal dec数据库号 = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text,
              "select isnull(max(申请单号),0) from JY申请单摘要 where left(申请单号,1)=" + ((int)_en申请类型).ToString()));
            decimal dec计算号 = Convert.ToDecimal(((int)_en申请类型).ToString() + DateTime.Now.ToString("yyyyMMdd").Substring(2, 6) + "00000");

            decimal dec返回号 = Convert.ToDecimal(s现有申请单号);


            if (dec计算号 >= dec数据库号)
            {
                if (dec返回号 >= dec计算号)
                {
                    return (dec返回号 + 1).ToString();
                }
                else
                {
                    return (dec计算号 + 1).ToString();
                }
            }
            else
            {
                if (dec返回号 >= dec数据库号)
                {
                    return (dec返回号 + 1).ToString();
                }
                else
                {
                    return (dec数据库号 + 1).ToString();
                }
            }
        }

        public static strLIS条目 sGetLis对照编码(string sHIS编码)
        {
            DataTable dt对照表 = dtHISLIS项目对照表;
            strLIS条目 resultStr = new strLIS条目();
            try
            {
                DataRow[] rows = dt对照表.Select("HIS编码='" + sHIS编码 + "'");
                if (rows.Length == 0)
                {
                    //resultStr = null;
                }
                else
                {
                    resultStr.sLIS编码 = rows[0]["LIS编码"].ToString();
                    resultStr.sLIS名称 = rows[0]["LIS名称"].ToString();
                    resultStr.s设备名称 = rows[0]["LIS设备"].ToString();
                    resultStr.sLISID = rows[0]["LISID"].ToString();
                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
            return resultStr;
        }

        public struct strLIS条目
        {
            public string sLIS编码;
            public string sLIS名称;
            public string sLISID;
            public string s设备名称;
        }
        //CREATE=生成条码 PRINT=打印 DELETE=删除/作废 COLLECT=采样确认 SEND=送出确认 RECIEVE=接收标本 ROLLBACK=剔回 FINISH=报告审核完成 CALLBACK=解除审核召回报告；ADDPRELISREQ=预制条码LIS开单完成；DELPRELISREQ=预制条码LIS开单作废；CANCELRECIEVE=取消签收（和剔回不同）


        //写sam_apply表和sample表
        //bool b处理LIS医嘱(Pojo医嘱执行 item, decimal? execNumber)
        //{
        //    decimal dec申请单号 = Convert.ToDecimal(yunLis.Business.s生成申请单号(enLIS申请类型.住院));
        //    var fee2 = HIS.COMM.comm.get收费小项By收费编码(item.项目编码);
        //    if (fee2 == null)
        //    {
        //        msgBalloonHelper.ShowInformation($"{item.项目名称}检索设置信息失败，请联系信息科维护");
        //        return false;
        //    }

        //    if (HIS.COMM.baseInfo.B写检验申请单)
        //    {
        //        var itemLIS = List收费检验项目对照.Where(c => c.收费编码 == item.项目编码).FirstOrDefault();
        //        if (itemLIS == null)
        //        {
        //            msgHelper.ShowInformation($"医嘱项目【{item.项目名称}】生成条码失败，\r\n请联系信息科维护基础信息。");
        //            return false;
        //        }

        //        //如果项目有对照并且是检验科确认费用，就不用写费用明细表，在LIS扫码的时候写费用明细
        //        if (listLIS申请摘要.Any(c => c.申请单号 == dec申请单号) == false)
        //        {
        //            JY申请单摘要 new申请摘要 = new JY申请单摘要()
        //            {
        //                申请类别 = "检验",
        //                申请单号 = dec申请单号,
        //                病人ID = Convert.ToInt32(pojo病人信息.ZYID),
        //                病历号 = pojo病人信息.住院号码,
        //                病人类型 = "住院",
        //                病人姓名 = pojo病人信息.病人姓名,
        //                性别 = pojo病人信息.性别,
        //                病人年龄 = pojo病人信息.S年龄,
        //                收费类型 = pojo病人信息.医保类型,
        //                临床诊断 = pojo病人信息.疾病名称,
        //                病人病区 = Convert.ToString(pojo病人信息.病区),
        //                床号 = pojo病人信息.病床,
        //                申请科室 = pojo病人信息.科室名称,
        //                申请医生 = pojo病人信息.主治医生编码.ToString(),
        //                申请时间 = DateTime.Now,
        //                加急标志 = "0",
        //                标本类型 = "标本类型",
        //                执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称,
        //                申请唯一标识 = item.医嘱ID.ToString(),
        //                身份证号 = pojo病人信息.身份证号,
        //                健康档案号 = "",
        //                状态 = "已申请",
        //                HIS组合名称 = item.项目名称,
        //            };
        //            listLIS申请摘要.Add(new申请摘要);
        //        }
        //        JY申请单明细 itemLIS申请单明细 = new JY申请单明细()
        //        {
        //            申请单号 = dec申请单号.ToString(),
        //            lis申请项目ID = itemLIS.检验ID,
        //            lis申请项目代码 = itemLIS.检验编码,
        //            lis申请项目名称 = itemLIS.检验名称,
        //            his申请项目单价 = 0,
        //            his申请项目数量 = 1,
        //            计价标志 = "0",
        //            医嘱ID = item.医嘱ID.ToString(),// pojo父医嘱申请摘要.lisID.ToString(),
        //            计费ID = "-1",
        //            执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称
        //        };
        //        listLIS申请明细.Add(itemLIS申请单明细);

        //        //foreach (var row明细 in pojo子医嘱申请明细)
        //        //{

        //        //    var _sLIS条目 = List收费检验项目对照.Where(c => c.收费编码 == row明细.项目编码).FirstOrDefault();
        //        //    if (_sLIS条目 == null)
        //        //    {
        //        //        WEISHENG.COMM.msgHelper.ShowInformation(row明细.项目名称 + "获取LIS对照失败，请联系管理员进行设置");
        //        //        return false;
        //        //    }

        //        //    chis.JY申请单明细.Add(new JY申请单明细()
        //        //    {
        //        //        申请单号 = dec申请单号.ToString(),
        //        //        lis申请项目ID = _sLIS条目.检验ID,
        //        //        lis申请项目代码 = _sLIS条目.检验编码,
        //        //        lis申请项目名称 = _sLIS条目.检验名称,
        //        //        his申请项目单价 = 0,
        //        //        his申请项目数量 = 1,
        //        //        计价标志 = "0",
        //        //        医嘱ID = pojo父医嘱申请摘要.lisID.ToString(),
        //        //        计费ID = "-1",
        //        //        执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称

        //        //    });
        //        //}

        //    }
        //    if (!HIS.COMM.baseInfo.B检验科确认费用)
        //    {
        //        if (!b处理在院费用(item, execNumber, fee2))
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}





        /// <summary>
        /// 从 护士站 迁移的方法
        /// </summary>
        /// <param name="_en申请类型"></param>
        /// <returns></returns>
        public static string s生成申请单号(enLIS申请类型 _en申请类型)
        {
            string LIS申请号 = "";
            if (LIS申请号 == "")
            {
                decimal dec当日计算号 = Convert.ToDecimal(((int)_en申请类型).ToString() + DateTime.Now.ToString("yyyyMMdd").Substring(2, 6) + "00000");
                decimal dec全局设置号 = Convert.ToDecimal(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS申请单号" + _en申请类型.ToString(), dec当日计算号.ToString(), "", false));
                HIS.COMM.Helper.EnvironmentHelper.UpdateValue("LIS申请单号" + _en申请类型.ToString(), (dec全局设置号 + 1).ToString(), "");

                decimal dec已保存记录最大值Sam = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.DBConnHelper.sLISConnString, CommandType.Text,
                            "select isnull(max(fapply_id),0)  from SAM_APPLY where left(fapply_id, 1) = " + ((int)_en申请类型).ToString()));

                decimal dec已保存记录最大值HIS = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text,
                            "select isnull(max(申请单号),0) from JY申请单摘要 where left(申请单号,1)=" + ((int)_en申请类型).ToString()));

                decimal dec已保存记录最大值 = dec已保存记录最大值Sam > dec已保存记录最大值HIS ? dec已保存记录最大值Sam : dec已保存记录最大值HIS;



                decimal dec最大计算号 = dec当日计算号 > dec全局设置号 ? dec当日计算号 : dec全局设置号;
                if (dec最大计算号 >= dec已保存记录最大值)
                {
                    LIS申请号 = (dec最大计算号 + 1).ToString();
                }
                else
                {
                    LIS申请号 = (dec已保存记录最大值 + 1).ToString();
                }
            }
            else
            {
                LIS申请号 = Convert.ToDecimal(Convert.ToDecimal(LIS申请号) + 1).ToString();
            }
            HIS.COMM.Helper.EnvironmentHelper.UpdateValue("LIS申请单号" + _en申请类型.ToString(), (Convert.ToDecimal(LIS申请号) + 1).ToString(), "");
            return LIS申请号;
        }

        //public bool b生成住院申请单(ZY病人信息 inpatient, Pojo医嘱执行 pojo父医嘱申请摘要, List<Pojo医嘱执行> pojo子医嘱申请明细)
        //{
        //try
        //{
        //    var dec申请单号 = Convert.ToDecimal(s生成申请单号(enLIS申请类型.住院));
        //    var pojo病人信息 = chis.ZY病人信息.Where(c => c.ZYID == inpatient.ZYID).FirstOrDefault();
        //    chis.JY申请单摘要.Add(new JY申请单摘要()
        //    {
        //        申请类别 = "检验",
        //        申请单号 = dec申请单号,
        //        病人ID = Convert.ToInt32(pojo病人信息.ZYID),
        //        病历号 = pojo病人信息.住院号码,
        //        病人类型 = "住院",
        //        病人姓名 = pojo病人信息.病人姓名,
        //        性别 = pojo病人信息.性别,
        //        病人年龄 = HIS.COMM.Helper.ageHelper.GetAgeByBirthdate(Convert.ToDateTime(pojo病人信息.出生日期)).ToString(),
        //        收费类型 = pojo病人信息.医保类型,
        //        临床诊断 = pojo病人信息.疾病名称,
        //        病人病区 = Convert.ToString(pojo病人信息.病区),
        //        床号 = pojo病人信息.病床,
        //        申请科室 = chis.GY科室设置.Where(c => c.科室编码 == pojo病人信息.科室).SingleOrDefault().科室名称,
        //        申请医生 = pojo病人信息.主治医生编码.ToString(),
        //        申请时间 = DateTime.Now,
        //        加急标志 = "0",
        //        标本类型 = "标本类型",
        //        执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称,
        //        申请唯一标识 = pojo父医嘱申请摘要.医嘱ID.ToString(),
        //        身份证号 = pojo病人信息.身份证号,
        //        健康档案号 = "",
        //        状态 = "已申请",
        //        HIS组合名称 = pojo父医嘱申请摘要.项目名称,
        //    });

        //    foreach (var row明细 in pojo子医嘱申请明细)
        //    {

        //        var _sLIS条目 = List收费检验项目对照.Where(c => c.收费编码 == row明细.项目编码).FirstOrDefault();
        //        if (_sLIS条目 == null)
        //        {
        //            WEISHENG.COMM.msgHelper.ShowInformation(row明细.项目名称 + "获取LIS对照失败，请联系管理员进行设置");
        //            return false;
        //        }
        //        chis.JY申请单明细.Add(new JY申请单明细()
        //        {
        //            申请单号 = dec申请单号.ToString(),
        //            lis申请项目ID = _sLIS条目.检验ID,
        //            lis申请项目代码 = _sLIS条目.检验编码,
        //            lis申请项目名称 = _sLIS条目.检验名称,
        //            his申请项目单价 = 0,
        //            his申请项目数量 = 1,
        //            计价标志 = "0",
        //            医嘱ID = pojo父医嘱申请摘要.医嘱ID.ToString(),
        //            计费ID = "-1",
        //            执行科室 = HIS.COMM.baseInfo.sLIS执行科室名称

        //        });
        //    }
        //    return true;
        //}
        //catch (Exception ex)
        //{
        //    msgBalloonHelper.ShowInformation(ex.Message);
        //    return false;
        //}
        //}





    }
}
