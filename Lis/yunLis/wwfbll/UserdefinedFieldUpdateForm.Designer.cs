﻿namespace yunLis.wwfbll
{
    partial class UserdefinedFieldUpdateForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonNO = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.wwfcolumnsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.fshow_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fshow_width = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_bya = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcolumns_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.wwfcolumnsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "选择您想显示的此列表的详细信息";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "详细信息:";
            // 
            // buttonNO
            // 
            this.buttonNO.Location = new System.Drawing.Point(361, 113);
            this.buttonNO.Name = "buttonNO";
            this.buttonNO.Size = new System.Drawing.Size(75, 23);
            this.buttonNO.TabIndex = 8;
            this.buttonNO.Text = "取消";
            this.buttonNO.UseVisualStyleBackColor = true;
            this.buttonNO.Click += new System.EventHandler(this.buttonNO_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(361, 59);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 9;
            this.buttonOK.Text = "确定";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fshow_flag,
            this.fname,
            this.fshow_width,
            this.forder_bya,
            this.fcolumns_id});
            this.dataGridView1.Location = new System.Drawing.Point(14, 59);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 20;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(340, 435);
            this.dataGridView1.TabIndex = 11;
            // 
            // fshow_flag
            // 
            this.fshow_flag.DataPropertyName = "fshow_flag";
            this.fshow_flag.FalseValue = "0";
            this.fshow_flag.HeaderText = "可见";
            this.fshow_flag.IndeterminateValue = "0";
            this.fshow_flag.Name = "fshow_flag";
            this.fshow_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fshow_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fshow_flag.TrueValue = "1";
            this.fshow_flag.Width = 35;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "显示名称";
            this.fname.Name = "fname";
            this.fname.Width = 165;
            // 
            // fshow_width
            // 
            this.fshow_width.DataPropertyName = "fshow_width";
            this.fshow_width.HeaderText = "宽度";
            this.fshow_width.Name = "fshow_width";
            this.fshow_width.Width = 70;
            // 
            // forder_bya
            // 
            this.forder_bya.DataPropertyName = "forder_by";
            this.forder_bya.HeaderText = "排序";
            this.forder_bya.Name = "forder_bya";
            this.forder_bya.Width = 60;
            // 
            // fcolumns_id
            // 
            this.fcolumns_id.HeaderText = "fcolumns_id";
            this.fcolumns_id.Name = "fcolumns_id";
            this.fcolumns_id.Visible = false;
            // 
            // UserdefinedFieldUpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 506);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonNO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserdefinedFieldUpdateForm";
            this.ShowIcon = false;
            this.Text = "选择详细信息";
            this.Load += new System.EventHandler(this.UserdefinedFieldUpdateForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wwfcolumnsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonNO;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.BindingSource wwfcolumnsBindingSource;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fshow_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn fshow_width;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_bya;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcolumns_id;
    }
}