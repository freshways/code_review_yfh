using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using yunLis.dao;
namespace yunLis.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class SysBLL : DAOWWF
    {
        public SysBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }


        /// <summary>
        /// 所有列表
        /// </summary>
        /// <returns></returns>
        public DataTable BllDT()
        {
            string sql = "SELECT * FROM wwf_sys ORDER BY fremark";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        public int BllUpdate(DataRowView dr)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update wwf_sys set ");           
            strSql.Append("fvalue='" + dr["fvalue"].ToString() + "'");
            strSql.Append(" where fsys_id='" + dr["fsys_id"].ToString() + "'");
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());
        }

        public static DateTime GetServerTime()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("Select CONVERT(varchar(50), GETDATE(), 120)");
            string str = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySql(yunLis.wwfbll.WWFInit.strDBConn, strSql.ToString());
            return Convert.ToDateTime(str);
        }
    }
}