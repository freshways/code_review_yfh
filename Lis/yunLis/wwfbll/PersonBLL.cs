using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using System.Collections;
using yunLis.dao;
using HIS.COMM;

namespace yunLis.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class PersonBLL : DAOWWF
    {
        public PersonBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
        public DataTable BllDTByCode(string strfcode, string strfname)
        {
            string sql = "SELECT * FROM WWF_PERSON where  (fperson_id='" + strfcode + "' or fname ='" + strfname + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 启用的人员
        /// </summary>
        /// <returns></returns>
        public DataTable BllDTAllPerson(string fuse_flag)
        {
           // string sql = "SELECT '(' + (SELECT fname FROM wwf_dept t1 WHERE t1.fdept_id = t.fdept_id) + ')(' + fperson_id + ')' + fname AS PersonName, t.* FROM wwf_person t WHERE (fuse_flag = " + fuse_flag + ") ORDER BY fdept_id";    
            string sql = "SELECT  t.fshow_name AS PersonName, t.* FROM wwf_person t WHERE (fuse_flag = " + fuse_flag + ") ORDER BY fdept_id";    
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 取得人员名称
        /// </summary>
        /// <param name="strfperson_id"></param>
        /// <returns></returns>
        public string BllPersonNameByID(string strfperson_id)
        {
            return (string)yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn, "SELECT fname FROM WWF_PERSON WHERE (fperson_id = '" + strfperson_id + "')");
        }
        /// <summary>
        /// 取得人员
        /// </summary>
        /// <param name="fuse_flag">启用的</param>
        /// <param name="ftype">类型</param>
        /// <returns></returns>
        public DataTable BllDTAllPersonByTypeAndUse(int fuse_flag, string ftype)
        {

            string sql = "SELECT fperson_id, fname, fshow_name FROM wwf_person WHERE ((ftype = '" + ftype + "')or(ftype = '开发')) AND (fuse_flag = " + fuse_flag + ")";
         
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0]; 
        }
        public DataTable BllDTAllPersonByTypeAndUse(string ftype)
        {

            string sql = "SELECT * FROM wwf_person WHERE (ftype = '" + ftype + "') AND (fuse_flag = 1)";

            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        public DataTable BllDTAllis0()
        {

            string sql = "SELECT * FROM wwf_person WHERE fperson_id='-1'";

            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 所有人员列表 如果部门ID为空或是部门ID为0那么显示所有用户
        /// </summary>
        /// <param name="fdept_id"></param>
        /// <returns></returns>
        public DataTable BllDT(string fdept_id)
        {
            string sql = "";
            if (fdept_id == "" || fdept_id == null || fdept_id == "0")
            {
                sql = "SELECT * FROM wwf_person ORDER BY forder_by";
            }
            else
            {
                sql = "SELECT * FROM wwf_person where fdept_id='" + fdept_id + "' ORDER BY forder_by";
            }
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }


        /// <summary>
        /// 根据ID取得列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataTable BllDTByID(string id)
        {
            string sql = "SELECT * FROM wwf_person where fperson_id='" + id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool BllPersonExists(string fperson_id)
        {
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn, "SELECT COUNT(1) FROM wwf_person WHERE (fperson_id = '" + fperson_id + "')");
        }
        /// <summary>
        /// 生成助 符
        /// </summary>
        /// <param name="lisSql"></param>
        /// <returns></returns>
        public string BllPersonfhelp_codeUpdate(IList lisSql)
        {
           return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn, lisSql);
        }
        public DataTable BllDTByfhelp_code(string fhelp_code, string fdept_id)
        {
            string sql = "";
            if (fdept_id == "")
                sql = "SELECT * FROM wwf_person where fhelp_code like '" + fhelp_code + "%'   order by forder_by";
            else
                sql = "SELECT * FROM wwf_person where fhelp_code like '" + fhelp_code + "%'  and fdept_id='" + fdept_id + "' order by forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
            //string sql = "";
            //if (fdept_id == "")
            //    sql = "SELECT * FROM wwf_person where fhelp_code like '" + fhelp_code + "%' and ftype='医生' order by forder_by";
            //else
            //    sql = "SELECT * FROM wwf_person where fhelp_code like '" + fhelp_code + "%' and ftype='医生' and fdept_id='" + fdept_id + "' order by forder_by";
            //return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int BllAdd(DataTable dt)
        {
            int returnValue = 0;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into wwf_person(");
            strSql.Append("fperson_id,fdept_id,fname,fname_e,fpass,forder_by,ftype,fuse_flag,fshow_name,fhelp_code,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dt.Rows[0]["fperson_id"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fdept_id"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fname"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fname_e"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fpass"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["forder_by"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["ftype"].ToString() + "',");
            strSql.Append("" + dt.Rows[0]["fuse_flag"].ToString() + ",");
            strSql.Append("'" + dt.Rows[0]["fshow_name"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fhelp_code"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fremark"].ToString() + "'");
            strSql.Append(")");
            returnValue = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());

            return returnValue;
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int BllUpdate(DataTable dt)
        {
            int returnValue = 0;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update wwf_person set ");
            strSql.Append("fname='" + dt.Rows[0]["fname"].ToString() + "',");
            strSql.Append("fname_e='" + dt.Rows[0]["fname_e"].ToString() + "',");
            strSql.Append("forder_by='" + dt.Rows[0]["forder_by"].ToString() + "',");
            strSql.Append("ftype='" + dt.Rows[0]["ftype"].ToString() + "',");
            strSql.Append("fuse_flag='" + dt.Rows[0]["fuse_flag"].ToString() + "',");
            strSql.Append("fshow_name='" + dt.Rows[0]["fshow_name"].ToString() + "',");
            
            strSql.Append("fremark='" + dt.Rows[0]["fremark"].ToString() + "'");
            strSql.Append(" where fperson_id='" + dt.Rows[0]["fperson_id"].ToString() + "'");

            StringBuilder strSqlOrg = new StringBuilder();
            strSqlOrg.Append("update wwf_org set ");
            strSqlOrg.Append("fname='" + dt.Rows[0]["fname"].ToString() + "'");
            strSqlOrg.Append(" where fperson_id='" + dt.Rows[0]["fperson_id"].ToString() + "' and ftype='person'");

            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString(), strSqlOrg.ToString()))
                returnValue = 1;

            return returnValue;
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        public int BllUpdatePass(DataTable dt)
        {
            int returnValue = 0;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update wwf_person set ");
            strSql.Append("fpass='" + dt.Rows[0]["fpass"].ToString() + "'");
            strSql.Append(" where fperson_id='" + dt.Rows[0]["fperson_id"].ToString() + "'");
            returnValue = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());
            return returnValue;
        }
        /// <summary>
        /// 删除
        /// </summary>
        public string BllDelete(string fperson_id)
        {
            string strRetrun = "";

            IList sqlList = new ArrayList();
            DataTable dtOrg = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,"SELECT forg_id FROM wwf_org WHERE (fperson_id = '" + fperson_id + "') AND (ftype = 'person')").Tables[0];
            for (int i = 0; i < dtOrg.Rows.Count; i++)
            {
                sqlList.Add("delete FROM wwf_org_func where forg_id='" + dtOrg.Rows[i]["forg_id"].ToString() + "'");
            }
            sqlList.Add("delete FROM wwf_person where fperson_id='" + fperson_id + "'");
            sqlList.Add("delete FROM wwf_org where (fperson_id = '" + fperson_id + "') AND (ftype = 'person')");
            strRetrun = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(yunLis.wwfbll.WWFInit.strDBConn,sqlList);

            return strRetrun;
        }

        public byte[] GetSQZ(string fperson_id)
        {
            string sql =$"SELECT  [usercode],[手签照片]  FROM [dbo].[SQZ] where [usercode]='{fperson_id}' ";
             DataSet ds = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.sLISConnString, CommandType.Text,sql);
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0 || ds.Tables[0].Rows[0]["手签照片"] == null || ds.Tables[0].Rows[0]["手签照片"].ToString() == "")
            {
                return null;
            }
            else
            {
                return (byte[])ds.Tables[0].Rows[0]["手签照片"];
            }
        }
    }
}