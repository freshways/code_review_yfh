using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using yunLis.dao;
namespace yunLis.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class PositionBLL : DAOWWF
    {
        public PositionBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }


        /// <summary>
        /// 所有岗位列表
        /// </summary>
        /// <returns></returns>
        public DataTable BllDT()
        {
            string sql = "SELECT * FROM WWF_POSITION ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
       

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int BllUpdate(DataRowView dr)
        {            
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update WWF_POSITION set ");
            
            strSql.Append("fname='" + dr["fname"].ToString() + "',");
            strSql.Append("forder_by='" + dr["forder_by"].ToString() + "',");
            strSql.Append("fuse_flag=" + dr["fuse_flag"].ToString() + ",");
            strSql.Append("fremark='" + dr["fremark"].ToString() + "'");
            strSql.Append(" where fposition_id='" + dr["fposition_id"].ToString() + "'");

            StringBuilder strSqlOrg = new StringBuilder();
            strSqlOrg.Append("update wwf_org set ");
            strSqlOrg.Append("fname='" + dr["fname"].ToString() + "'");
            strSqlOrg.Append(" where fposition_id='" + dr["fposition_id"].ToString() + "' and ftype = 'position'");

            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString(), strSqlOrg.ToString()) )
                return 1;
            else
                return 0;

        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int BllAdd(DataRowView dr)
        {
            int returnValue = 0;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into WWF_POSITION(");
            strSql.Append("fposition_id,fname,forder_by,fuse_flag,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dr["fposition_id"].ToString() + "',");
            strSql.Append("'" + dr["fname"].ToString() + "',");
            strSql.Append("'" + dr["forder_by"].ToString() + "',");
            strSql.Append("" + dr["fuse_flag"].ToString() + ",");
            strSql.Append("'" + dr["fremark"].ToString() + "'");
            strSql.Append(")");
            returnValue = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString());
            return returnValue;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public int BllDelete(string id)
        {
            string sql1 = "delete FROM WWF_POSITION where fposition_id='" + id + "'";
            string sql2 = "delete FROM wwf_org where fposition_id='" + id + "'";
            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(yunLis.wwfbll.WWFInit.strDBConn,sql1, sql2))
                return 1;
            else
                return 0;
        }

        public int BllDelete7777(string id)
        {
            string sql1 = "delete FROM WWF_POSITION where fposition_id='" + id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql1);
            //string sql2 = "delete ";
        }

            /*
              '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   '[功能] 删除数据库中的记录
   '[参数] strTableName 需要删除的记录的所在的表名 StrDelSql删除条件
   '[返回] 1成功 0失败
   '[使用] 直接调用
   '[作者]  12/22
   '---------------------------------  
             * 
             * 
             * 
             '删除选项表
  DeleteContent=ASP_DeleteContent("VOTE_OPTION","OPTION_ID="&OPTION_ID&"")
  
  '删除答案表
  DeleteContent=ASP_DeleteContent("VOTE_ANSWER","OPTION_ID="&OPTION_ID&"")
             * 
   Function ASP_DeleteContent(strTableName,StrDelSql)
      On Error Resume Next
      intColCount=ASP_GetTableColCount(strTableName,StrDelSql)
      If intColCount="" or isnull(intColCount) or Cint(intColCount)=0 Then
        ASP_DeleteContent=0
      Else     
        strDelSQL="DELETE FROM "&strTableName&" "&StrDelSql
        set objContentrs=objWebConnectionObj.Execute(strDelSQL)
        Call ASP_ShowDataError(objWebConnectionObj,2)
        Call ASP_CloseObj(objContentrs)                          '释放存储对象
        ASP_DeleteContent=1
      End if
   end Function
             */
        
       
    }
}
