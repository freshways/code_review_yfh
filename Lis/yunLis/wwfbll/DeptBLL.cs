using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using yunLis.dao;
namespace yunLis.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class DeptBll : DAOWWF
    {
        public DeptBll()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
        public DataTable BllDeptDTByCode(string strfcode, string strfname)
        {
            string sql = "SELECT * FROM wwf_dept where  (fdept_id='" + strfcode + "' or fname ='" + strfname + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 所有功能列表
        /// </summary>
        /// <returns></returns>
        public DataTable BllDeptDT()
        {
            string sql = "SELECT ('['+fdept_id+']'+fname) as fname_show,* FROM wwf_dept ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        public string BllDeptName(string id)
        {
            string sql = "SELECT fname FROM wwf_dept where fdept_id='" + id + "'";
            return (String)yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql);
        }

        public string Bllfp_id(string id)
        {
            string sql = "SELECT fp_id FROM wwf_dept where fdept_id='" + id + "'";
            return (String)yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql);
        }
        public string BllPersonName(string id)
        {
            string sql = "SELECT fname FROM WWF_PERSON where fperson_id='" + id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).ToString();
        }
        /// <summary>
        /// 功能列表 已经启用的
        /// </summary>
        /// <returns></returns>
        public DataTable BllDeptDTuse()
        {
            string sql = "SELECT ('['+fdept_id+']'+fname) as fname_show, * FROM wwf_dept  where fuse_flag=1 ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        public DataTable BllDeptDTuse(string strname)
        {
            string sql = "SELECT ('['+fdept_id+']'+fname) as fname_show, * FROM wwf_dept  where fuse_flag=1 and fremark like '%" + strname + "%' ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 所有功能列表 上级取下级
        /// </summary>
        /// <returns></returns>
        public DataTable BllDeptDT(string fp_id)
        {
            string sql = "SELECT * FROM wwf_dept where fp_id='" + fp_id + "' ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 所上级取得下级
        /// </summary>
        /// <param name="fp_id">上级ID</param>
        /// <param name="fuse_flag">启用否</param>
        /// <returns></returns>
        public DataTable BllDeptDT(string fp_id,int fuse_flag)
        {
            string sql = "SELECT * FROM wwf_dept where fp_id='" + fp_id + "' and fuse_flag=" + fuse_flag + " ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        /// <summary>
        /// 所有功能列表 上级取下级
        /// </summary>
        /// <returns></returns>
        public DataTable BllDeptDTByID(string fdept_id)
        {
            string sql = "SELECT * FROM wwf_dept where fdept_id='" + fdept_id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int BllUpdate(DataTable dt,string strid)
        {            
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update wwf_dept set ");
            strSql.Append("fdept_id='" + dt.Rows[0]["fdept_id"].ToString() + "',");
            strSql.Append("fp_id='" + dt.Rows[0]["fp_id"].ToString() + "',");
            strSql.Append("fname='" + dt.Rows[0]["fname"].ToString() + "',");
            strSql.Append("fname_e='" + dt.Rows[0]["fname_e"].ToString() + "',");
            strSql.Append("forder_by='" + dt.Rows[0]["forder_by"].ToString() + "',");
            strSql.Append("fuse_flag=" + dt.Rows[0]["fuse_flag"].ToString() + ",");
            strSql.Append("fremark='" + dt.Rows[0]["fremark"].ToString() + "'");
            strSql.Append(" where fdept_id='" + strid + "'");

            StringBuilder strSqlOrg = new StringBuilder();
            strSqlOrg.Append("update wwf_org set ");
            strSqlOrg.Append("fname='" + dt.Rows[0]["fname"].ToString() + "'");
            strSqlOrg.Append(" where forg_id='" + strid + "'");

            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString(), strSqlOrg.ToString()) )
                return 1;
            else
                return 0;

        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int BllAdd(DataTable dt)
        {
            int returnValue = 0;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into wwf_dept(");
            strSql.Append("fdept_id,fp_id,fname,fname_e,forder_by,fuse_flag,fremark");
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append("'" + dt.Rows[0]["fdept_id"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fp_id"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fname"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["fname_e"].ToString() + "',");
            strSql.Append("'" + dt.Rows[0]["forder_by"].ToString() + "',");
            strSql.Append("" + dt.Rows[0]["fuse_flag"].ToString() + ",");
            strSql.Append("'" + dt.Rows[0]["fremark"].ToString() + "'");
            strSql.Append(")");

            StringBuilder strSqlOrg = new StringBuilder();
            strSqlOrg.Append("insert into wwf_org(");
            strSqlOrg.Append("forg_id,ftype,fp_id,fdept_id,fname,fname_e,forder_by,fuse_flag,fremark");
            strSqlOrg.Append(")");
            strSqlOrg.Append(" values (");
            strSqlOrg.Append("'" + dt.Rows[0]["fdept_id"].ToString() + "',");
            strSqlOrg.Append("'dept',");
            strSqlOrg.Append("'" + dt.Rows[0]["fp_id"].ToString() + "',");
            strSqlOrg.Append("'" + dt.Rows[0]["fdept_id"].ToString() + "',");
            strSqlOrg.Append("'" + dt.Rows[0]["fname"].ToString() + "',");
            strSqlOrg.Append("'" + dt.Rows[0]["fname_e"].ToString() + "',");
            strSqlOrg.Append("'" + dt.Rows[0]["forder_by"].ToString() + "',");
            strSqlOrg.Append("" + dt.Rows[0]["fuse_flag"].ToString() + ",");
            strSqlOrg.Append("'" + dt.Rows[0]["fremark"].ToString() + "'");
            strSqlOrg.Append(")");

            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(yunLis.wwfbll.WWFInit.strDBConn,strSql.ToString(), strSqlOrg.ToString()))
                returnValue = 1;

            return returnValue;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public int BllDelete(string id)
        {
            string sql1 = "delete FROM wwf_dept where fdept_id='" + id + "'";
            string sql2 = "delete FROM wwf_org where forg_id='" + id + "'";
            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(yunLis.wwfbll.WWFInit.strDBConn,sql1, sql2))
                return 1;
            else
                return 0;
        }
        /// <summary>
        /// 是否有下级
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool BllChildExists(string id)
        {
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn, "SELECT COUNT(1) FROM wwf_dept WHERE (fp_id = '" + id + "')");
        }

        public string BllDeptNameByID(string id)
        {
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn,"SELECT fname FROM wwf_dept WHERE (fdept_id = '"+id+"')").ToString();
        }

    }
}