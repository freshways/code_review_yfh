﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.com;

namespace yunLis.wwfbll
{
    public partial class ClientIDForm : Form
    {
        HardInfo hardinfo = new HardInfo();
        public ClientIDForm()
        {
            InitializeComponent();
            try
            {
                this.richTextBoxInfo.AppendText(" *** 电脑信息 *** ");
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("机器码：" + yunLis.wwfbll.WWFInit.strClientID);
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("电脑IP：" + hardinfo.GetHostIpAddress());
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("电脑名：" + hardinfo.GetHostName());
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("\n");


                this.richTextBoxInfo.AppendText(" *** 单位信息 *** ");
                this.richTextBoxInfo.AppendText("\n");               
                this.richTextBoxInfo.AppendText("姓名："+LoginBLL.strPersonName);
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("科室：" + LoginBLL.strDeptName);
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("单位：" + LoginBLL.CustomerName);
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("地址：" + LoginBLL.CustomerAdd);
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("邮编：" + LoginBLL.CoZC);
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("联系电话：" + LoginBLL.CustomerTel);
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("联系人：" + LoginBLL.CustomerContact);
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("备注：" + LoginBLL.sysName + LoginBLL.sysVar);   
              
               
                this.richTextBoxInfo.AppendText("\n\n");
                this.richTextBoxInfo.AppendText(" *** 服务信息 *** ");
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("联系人：tao");
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("电话：11111111111");                
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("");
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("QQ：249116367");
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("MSN：taoyinzhou@hotmail.com");
                this.richTextBoxInfo.AppendText("\n");
                this.richTextBoxInfo.AppendText("网址：http://www.815lis.cn");
                this.richTextBoxInfo.AppendText("\n");  
             
            }
            catch (Exception ex) { MessageBox.Show(ex.Message.ToString()); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ClientIDForm_Load(object sender, EventArgs e)
        {

        }

        
    }
}