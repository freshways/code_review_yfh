using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using yunLis.dao;
using yunLis.wwfbll;

namespace yunLis.wwfbll
{
    public class ReportSettingsRules : DAOWWF
    {      
        public ReportSettingsRules()
        {            
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }        
        public  DataTable BllReportDT(int fuse_if)
        {
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, "SELECT * FROM SAM_REPORT t WHERE (fuse_if = " + fuse_if + ")").Tables[0];
        }

        /// <summary>
        /// 报表文件设置 如返回值为 1文件已存在；为2文件新生成成功;为0表示异常
        /// </summary>
        /// <returns></returns>
        public void BllReportSettings(string strFileName)
        {        
            FileInfo rs = new FileInfo(strFileName);
            if (rs.Exists!=true)
            {
                StreamWriter sw = new StreamWriter(strFileName, true);
                sw.WriteLine("<?xml version='1.0' standalone='yes'?>");
                sw.WriteLine("<ReportSettings>");
                DataTable dt = new DataTable();
                dt = BllReportDT(1);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = null;
                        dr = dt.Rows[i];
                        sw.WriteLine("<" + dr["fcode"].ToString() + ">");
                        sw.WriteLine("<ReportName>" + dr["fcode"].ToString() + "</ReportName>");
                        sw.WriteLine("<PrinterName>" + dr["fprinter_name"].ToString() + "</PrinterName>");
                        sw.WriteLine("<PaperName>" + dr["fpaper_name"].ToString() + "</PaperName>");
                        sw.WriteLine("<PageWidth>" + dr["fpage_width"].ToString() + "</PageWidth>");
                        sw.WriteLine("<PageHeight>" + dr["fpage_height"].ToString() + "</PageHeight>");
                        sw.WriteLine("<MarginTop>" + dr["fmargin_top"].ToString() + "</MarginTop>");
                        sw.WriteLine("<MarginBottom>" + dr["fmargin_bottom"].ToString() + "</MarginBottom>");
                        sw.WriteLine("<MarginLeft>" + dr["fmargin_left"].ToString() + "</MarginLeft>");
                        sw.WriteLine("<MarginRight>" + dr["fmargin_right"].ToString() + "</MarginRight>");
                        sw.WriteLine("<Orientation>" + dr["forientation"].ToString() + "</Orientation>");
                        sw.WriteLine("</" + dr["fcode"].ToString() + ">");

                    }
                }
                else
                {
                    sw.WriteLine("<Template>");
                    sw.WriteLine("<ReportName>Template</ReportName>");
                    sw.WriteLine("<PrinterName>Microsoft Office Document Image Writer</PrinterName>");
                    sw.WriteLine("<PaperName>A4</PaperName>");
                    sw.WriteLine("<PageWidth>21.01</PageWidth>");
                    sw.WriteLine("<PageHeight>29.69</PageHeight>");
                    sw.WriteLine("<MarginTop>1</MarginTop>");
                    sw.WriteLine("<MarginBottom>1</MarginBottom>");
                    sw.WriteLine("<MarginLeft>1</MarginLeft>");
                    sw.WriteLine("<MarginRight>1</MarginRight>");
                    sw.WriteLine("<Orientation>H</Orientation>");
                    sw.WriteLine("</Template>");
                }
                sw.WriteLine("</ReportSettings>");
                sw.WriteLine(" ");
                sw.Close();
            }
           
        }
    }
}
