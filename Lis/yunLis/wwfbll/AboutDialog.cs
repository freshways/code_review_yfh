using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using yunLis.wwfbll;

namespace yunLis.wwfbll
{
    public partial class AboutDialog : Form
    {
        public AboutDialog()
        {
            InitializeComponent();
        }

        private void AboutDialog_Load(object sender, EventArgs e)
        {
            try
            {
                
                this.labelSysName.Text = LoginBLL.sysName;
                this.Text = this.Text + " " + LoginBLL.sysName;
                this.sysVar.Text = LoginBLL.sysVar;
                this.Copyright.Text = LoginBLL.Copyright;
                this.CoContact.Text = LoginBLL.CoContact;
                this.CoTel.Text = LoginBLL.CoTel;
                this.CoEmail.Text = LoginBLL.CoEmail;
                this.CoAdd.Text = LoginBLL.CoAdd;
                this.CoZC.Text = LoginBLL.CoZC;
                this.CoHttp.Text = LoginBLL.CoHttp;
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("IEXPLORE.EXE", CoHttp.Text);
            }
            catch (Exception ex)
            {
               WWMessage.MessageShowError(ex.ToString());
            }

        }

        private void AboutDialog_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}