using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using yunLis.wwfbll;
namespace yunLis.wwfbll
{
    public partial class HelpForm : Form
    {
        LoginBLL bll = new LoginBLL();//组织功能
        string thisStrUserOrgId = "";//"61590ec8451a4fc2aedf4139d9b37c10";
       // DockContent formFunDockContent = null;
        //Form formFunForm = new Form();
        string strUrl = "";//System.Environment.CurrentDirectory;// +@"\" + GetTime() + ".xls";
        string strFuncID = "";
        int intInit = 0;
        /// <summary>
        /// 功能ID
        /// </summary>
        /// <param name="strFuncID"></param>
        public HelpForm(string funcid)
        {
            InitializeComponent();
            this.strFuncID = funcid;
            
           // browserAddressTextBox.Text = strUrl;
        }
        /// <summary>
        /// 初始进入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpForm_Load(object sender, EventArgs e)
        {
            try
            {
                // this.browserAddressTextBox.Text = SysHttpUrl;

                thisStrUserOrgId = LoginBLL.strOrgID;//"61590ec8451a4fc2aedf4139d9b37c10";              
                GetxPanderListTree("0");

                //string strHelpName = this.bll.BllFuncHelp(this.strFuncID);
                DataTable dtFunc = this.bll.BllFuncListByID(this.strFuncID);
                string strHelpName = "";
                string strfname = "";
                try
                {
                    if (dtFunc.Rows[0]["FHELP"] != null)
                        strHelpName = dtFunc.Rows[0]["FHELP"].ToString();
                }
                catch { }
                try
                {
                    if (dtFunc.Rows[0]["FNAME"] != null)
                        strfname = dtFunc.Rows[0]["FNAME"].ToString();
                }
                catch { }


                this.labelCurrUrl.Text = strfname;

                if (strFuncID == "" || strFuncID == null)
                {
                    browserAddressTextBox.Text = System.Environment.CurrentDirectory.ToString() + @"\Help\index.htm";
                    Navigate();
                }
                else if (strHelpName == "" || strHelpName == null)
                {
                    //browserAddressTextBox.Text = LoginBLL.sysHelpAdd;
                    browserAddressTextBox.Text = System.Environment.CurrentDirectory.ToString() + @"\Help\error.htm";
                    Navigate();
                }
                else
                {
                    //browserAddressTextBox.Text = System.Environment.CurrentDirectory.ToString() + @"\" + strHelpName;
                    browserAddressTextBox.Text = System.Environment.CurrentDirectory.ToString() + @"\" + strHelpName;
                    //browserAddressTextBox.Text = LoginBLL.sysHelpAdd +  strHelpName; 
                    Navigate();
                }

            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
        
        /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetxPanderListTree(string strp)
        {
            try
            {
                this.zaSuiteTreeView1.ZADataTable = bll.BllOrgFuncDT(thisStrUserOrgId); ;//装载数据集
                this.zaSuiteTreeView1.ZATreeViewRootValue = strp;//树 RootId　根节点值
                this.zaSuiteTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.zaSuiteTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.zaSuiteTreeView1.ZAKeyFieldName = "ffunc_id";//主键字段名称
                this.zaSuiteTreeView1.ZATreeViewShow();//显示树  

                try
                {
                    this.zaSuiteTreeView1.SelectedNode = zaSuiteTreeView1.Nodes[0].Nodes[0].Nodes[0].Nodes[0];
                }
                catch { }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }
       
        private void browserGoButton_Click(object sender, EventArgs e)
        {            
            Navigate();
        }

        private void browserBackButton_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void browserForwardButton_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void browserStopButton_Click(object sender, EventArgs e)
        {
            webBrowser1.Stop();
        }

        private void browserRefreshButton_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        } 
        // Provide an accessor for the browserAddressTextBox control
        private string browserUrl
        {
            get { return browserAddressTextBox.Text.Trim(); }
            set { browserAddressTextBox.Text = value; }
        }
        private void Navigate()
        {
           // if (!browserUrl.StartsWith("http://"))
           // {
               // browserUrl = "http://" + browserUrl;
           // }
            browserUrl = browserUrl;
            try
            {
                webBrowser1.Navigate(new Uri(browserUrl));
            }
            catch(Exception ex)
            {
              WWMessage.MessageShowError(ex.ToString());
            }
        }

       

        private void zaSuiteTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                if (intInit == 0) { }
                else
                {
                    // MessageBox.Show("AfterSelect");
                    strUrl = System.Environment.CurrentDirectory.ToString();
                    DataTable dtthislist = bll.BllFuncListByID(e.Node.Name);
                    string strFHELP = dtthislist.Rows[0]["FHELP"].ToString();
                    string strParent = "wwf";
                    try
                    {
                        strParent = e.Node.Parent.Text;
                    }
                    catch
                    {
                        strParent = "wwf";
                    }
                    this.labelCurrUrl.Text = strParent + " >> " + e.Node.Text.ToString();
                    /*
                    if (strFHELP == "" || strFHELP == null)
                    {
                        browserAddressTextBox.Text = "";
                    }
                    else
                    {
                        browserAddressTextBox.Text = strUrl + @"\" + strFHELP;
                        Navigate();
                    }
                    */
                    if (strFHELP == "" || strFHELP == null)
                    {
                       // browserAddressTextBox.Text = LoginBLL.sysHelpAdd + "error.htm";
                      //  browserAddressTextBox.Text = System.Environment.CurrentDirectory.ToString() + @"\" ++ "error.htm";
                           browserAddressTextBox.Text = System.Environment.CurrentDirectory.ToString() + @"\Help\error.htm";
                        Navigate();
                    }
                    else
                    {
                        browserAddressTextBox.Text = System.Environment.CurrentDirectory.ToString() + @"\" + strFHELP;
                       // browserAddressTextBox.Text = LoginBLL.sysHelpAdd + strFHELP;
                        Navigate();
                    }
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                intInit = 1;
                Cursor = Cursors.Arrow;
            }
        }

     

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void zaSuiteTreeView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;                
                strUrl = System.Environment.CurrentDirectory.ToString();
                DataTable dtthislist = bll.BllFuncListByID(zaSuiteTreeView1.SelectedNode.Name.ToString());
                string strFHELP = dtthislist.Rows[0]["FHELP"].ToString();
                string strParent = "wwf";
                try
                {
                    strParent = zaSuiteTreeView1.SelectedNode.Parent.Text;
                }
                catch
                {
                    strParent = "wwf";
                }
                this.labelCurrUrl.Text = strParent + " >> " + zaSuiteTreeView1.SelectedNode.Text.ToString();

                if (strFHELP == "" || strFHELP == null)
                {
                    browserAddressTextBox.Text = "";
                }
                else
                {
                    browserAddressTextBox.Text = strUrl + @"\" + strFHELP;
                    Navigate();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void browserAddressTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Space))//或(e.KeyCode==Keys.Enter) 
            {
                Navigate();
            }
        }

        private void 打开ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                strUrl = System.Environment.CurrentDirectory.ToString();
                DataTable dtthislist = bll.BllFuncListByID(zaSuiteTreeView1.SelectedNode.Name.ToString());
                string strFHELP = dtthislist.Rows[0]["FHELP"].ToString();
                string strParent = "wwf";
                try
                {
                    strParent = zaSuiteTreeView1.SelectedNode.Parent.Text;
                }
                catch
                {
                    strParent = "wwf";
                }
                this.labelCurrUrl.Text = strParent + " >> " + zaSuiteTreeView1.SelectedNode.Text.ToString();

                if (strFHELP == "" || strFHELP == null)
                {
                    browserAddressTextBox.Text = "";
                }
                else
                {
                    browserAddressTextBox.Text = strUrl + @"\" + strFHELP;
                    Navigate();
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                AboutDialog aaa = new AboutDialog();
                aaa.ShowDialog();
                //MessageBox.Show(LoginBLL.strHelpContact, "服务 联系方式", MessageBoxButtons.OK, MessageBoxIcon.Question);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.ToString());
            }
        }

        
    }
}