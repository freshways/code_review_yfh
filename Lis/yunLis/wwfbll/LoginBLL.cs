using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using yunLis.dao;
using HIS.Model;
using System.Linq;

namespace yunLis.wwfbll
{
    /// <summary>
    /// 系统登录业务逻辑
    /// </summary>
    public class LoginBLL : DAOWWF
    {

        public static string strTest = "对不起，您当前使用的版本不能使用此功能！";
        /// <summary>
        /// 公司地址
        /// </summary>
        public static string CoAdd = "";
        /// <summary>
        /// 公司联系人
        /// </summary>
        public static string CoContact = "";
        /// <summary>
        /// 公司软件版权所有
        /// </summary>
        public static string Copyright = "";
        /// <summary>
        /// 公司电话
        /// </summary>
        public static string CoTel = "";

        //-------------------------------------------------------------------     
        /// <summary>
        /// 公司银行
        /// </summary>
        public static string CoBank = "";
        /// <summary>
        /// 公司银行账号
        /// </summary>
        public static string CoNA = "";
        /// <summary>
        /// 公司联系部门
        /// </summary>
        public static string CoContactDept = "";
        /// <summary>
        /// 公司邮箱
        /// </summary>
        public static string CoEmail = "";
        /// <summary>
        /// 公司传真
        /// </summary>
        public static string CoFax = "";
        /// <summary>
        /// 公司网址
        /// </summary>
        public static string CoHttp = "";
        /// <summary>
        /// 公司监督电话
        /// </summary>
        public static string CoMonitorTel = "";
        /// <summary>
        /// 公司邮编
        /// </summary>
        public static string CoZC = "";
        //-----------------------------------------------------------------------


        public static string CustomerAdd = "";//	wwf	上海XXX	用户地址
        public static string CustomerContact = "";//	wwf	张三	用户联系人
        public static string CustomerName = "";//	wwf	安二司 	用户单位
        public static string CustomerTel = "";//	wwf	0871-3173128	用户电话

        public static string sysDB = "";//Access SqlServer、Oracle
        public static string sysName = "";//wwf	LIS业务管理系统	软件名称
        public static string sysNum = "";//	wwf	软件序列号	软件序列号
        public static string sysVar = "";//wwf	v1.0	软件版本号
        public static string pidSys = "";//系统上级ID
        /// <summary>
        /// 业务上级ID
        /// </summary>
        public static string pidBusiness = "";//
        /// <summary>
        /// 系统帮助地址
        /// </summary>
        public static string sysHelpAdd = "";

        //----------
        public static string strPersonID = "";
        public static string strPersonName = "";

        public static string strPositionID = "";
        public static string strPositionName = "";

        private static string _strDeptID = "";
        public static string strDeptName = "";

        public static string strOrgID = "";

        public static string strCheckID = "";
        public static string strCheckName = "";

        // public static string strHelpContact = "电话：13629424054\n\n\n      taoyinzhou@163.com\n\n网址：815lis.googlepages.com\n\nQQ：249116367\n\n联系人：tao";
        /// <summary>
        /// 取得当前登录选择的年度
        /// </summary>
        //public static string strCurrYear = "2008";
        /// <summary>
        /// 当前套账
        /// </summary>
        //public static string strCurrTZ = "001";//

        /// <summary>
        /// 起始页
        /// </summary>
        public static string SysStartPage = "";

        public LoginBLL()
        {

            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }
        /// <summary>
        /// 设置系统信息
        /// </summary>
        public static void BllSetSysInfo()
        {
            DataTable dt = SqlHelper.ExecuteDataTable("SELECT fsys_id, ftype, fvalue, fremark FROM wwf_sys");
            string fsys_id = "";//参数ID
            string fvalue = "";//参数值
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                fsys_id = dt.Rows[i]["fsys_id"].ToString();
                fvalue = dt.Rows[i]["fvalue"].ToString();
                switch (fsys_id)
                {
                    case "CoAdd":
                        CoAdd = fvalue;
                        break;
                    case "CoContact":
                        CoContact = fvalue;
                        break;
                    case "Copyright":
                        Copyright = fvalue;
                        break;
                    case "CoTel":
                        CoTel = fvalue;
                        break;
                    case "CustomerAdd":
                        CustomerAdd = fvalue;
                        break;
                    case "CustomerContact":
                        CustomerContact = fvalue;
                        break;
                    case "CustomerName":
                        CustomerName = fvalue;
                        break;
                    case "CustomerTel":
                        CustomerTel = fvalue;
                        break;
                    case "sysDB":
                        sysDB = fvalue;
                        break;
                    case "sysName":
                        sysName = fvalue;
                        break;
                    case "sysNum":
                        sysNum = fvalue;
                        break;
                    case "sysVar":
                        sysVar = fvalue;
                        break;
                    case "sysHelpAdd":
                        sysHelpAdd = fvalue;
                        break;
                    case "pidSys":
                        pidSys = fvalue;
                        break;
                    case "pidBusiness":
                        pidBusiness = fvalue;
                        break;

                    //--------------

                    case "CoBank":
                        CoBank = fvalue;
                        break;
                    case "CoNA":
                        CoNA = fvalue;
                        break;
                    case "CoContactDept":
                        CoContactDept = fvalue;
                        break;
                    case "CoEmail":
                        CoEmail = fvalue;
                        break;
                    case "CoFax":
                        CoFax = fvalue;
                        break;
                    case "CoHttp":
                        CoHttp = fvalue;
                        break;
                    case "CoMonitorTel":
                        CoMonitorTel = fvalue;
                        break;
                    case "CoZC":
                        CoZC = fvalue;
                        break;
                    case "SysStartPage":
                        SysStartPage = fvalue;
                        break;
                    //---------------

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 用户名密码正确否
        /// </summary>
        /// <param name="fperson_id">登录名</param>
        /// <param name="fpass">密码</param>
        /// <returns></returns>
        public bool BllUserPassOk(string fperson_id, string fpass)
        {
            string pwd = Security.StrToEncrypt("MD5", fpass);
            string sql = "SELECT count(1) FROM wwf_person where fperson_id='" + fperson_id + "' AND (fpass='" + pwd + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn, sql);
        }

        //add by wjz 20151227 修改登录认证方式 ▽
        public bool BllUserPassOkParams(string fperson_id, string fpass)
        {
            string pwd = Security.StrToEncrypt("MD5", fpass);
            string uspName = "UserLogin";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExistsParam(yunLis.wwfbll.WWFInit.strDBConn, uspName, fperson_id, pwd);
        }
        //add by wjz 20151227 修改登录认证方式 △

        /// <summary>
        /// 取得用启组织列表，有可能一个用户在多个岗位
        /// </summary>
        /// <param name="fperson_id"></param>
        /// <returns></returns>
        public DataTable BllUserOrg(string fperson_id)
        {
            string sql = "SELECT t.*,(SELECT fname  FROM wwf_dept WHERE (fdept_id = t.fdept_id)) AS fdept_name,(SELECT fname  FROM WWF_POSITION  WHERE (fposition_id = t.fposition_id)) AS fposition_name FROM wwf_org t where fperson_id='" + fperson_id + "' AND (ftype='person')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        ///  设置登录信息
        /// </summary>
        /// <param name="fperson_id"></param>
        public void BllSetLoginValue(DataTable dtOrg, string forg_id)
        {
            DataRow[] rowCurr = dtOrg.Select("forg_id='" + forg_id + "'");
            if (rowCurr.Length > 0)
            {
                foreach (DataRow dr in rowCurr)
                {
                    strPersonID = dr["fperson_id"].ToString();
                    strPersonName = dr["fname"].ToString();
                    strPositionID = dr["fposition_id"].ToString();
                    strPositionName = dr["fposition_name"].ToString();
                    strDeptID = dr["fdept_id"].ToString();
                    strDeptName = dr["fdept_name"].ToString();
                    strOrgID = dr["forg_id"].ToString();
                }
            }
        }

        /// <summary>
        /// 根据组织ID取得功能列表 启用的  只读数据集
        /// </summary>
        /// <param name="forg_id"></param>
        /// <returns></returns>
        public DataTable BllOrgFuncDT(string forg_id)
        {
            // string sql = "SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 INNER JOIN  wwf_org_func t2 ON t1.ffunc_id = t2.ffunc_id where t2.forg_id='" + forg_id + "' and t1.fuse_flag=1 order by t1.forder_by";

            string sql = "SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 ,  wwf_org_func t2 where t1.ffunc_id = t2.ffunc_id and  t1.fuse_flag=1 and t2.forg_id='" + forg_id + "' order by t1.forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        public DataTable BllOrgFuncDT(string forg_id, string fp_id)
        {
            //  string sql = "SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 INNER JOIN  wwf_org_func t2 ON t1.ffunc_id = t2.ffunc_id where t2.forg_id='" + forg_id + "' and t1.fuse_flag=1 and  t1.fp_id='" + fp_id + "' order by t1.forder_by";

            string sql = "SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 ,  wwf_org_func t2 where t1.ffunc_id = t2.ffunc_id and  t1.fuse_flag=1 and t2.forg_id='" + forg_id + "' and t1.fp_id='" + fp_id + "' order by t1.forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }
        public DataTable BllOrgFuncDTDesc(string forg_id, string fp_id)
        {
            //  string sql = "SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 INNER JOIN  wwf_org_func t2 ON t1.ffunc_id = t2.ffunc_id where t2.forg_id='" + forg_id + "' and t1.fuse_flag=1 and  t1.fp_id='" + fp_id + "' order by t1.forder_by";

            string sql = "SELECT t2.forg_id, t1.ffunc_id, t2.ftool_flag, t2.fgrzm_flag, t1.fp_id, t1.fname, t1.fname_e, t1.ficon, t1.ffunc_winform, t1.ffunc_web, t1.forder_by, t1.fuse_flag, t1.fhelp, t1.fcj_flag, t1.fcj_name, t1.fcj_zp  FROM wwf_func t1 ,  wwf_org_func t2 where t1.ffunc_id = t2.ffunc_id and  t1.fuse_flag=1 and t2.forg_id='" + forg_id + "' and t1.fp_id='" + fp_id + "' order by t1.forder_by desc";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        public DataTable BllFuncListByID(string ffunc_id)
        {
            string sql = "SELECT * FROM wwf_func WHERE (ffunc_id = '" + ffunc_id + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql).Tables[0];
        }

        /// <summary>
        /// 取得功能帮助
        /// </summary>
        /// <param name="strid"></param>
        /// <returns></returns>
        public string BllFuncHelp(string strid)
        {
            object obj = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn, "SELECT FHELP FROM WWF_FUNC WHERE (FFUNC_ID = '" + strid + "')");
            if (obj != null)
                return (String)obj;
            else
                return "";
        }


        //==============================================
        /// <summary>
        /// 推送消息地址
        /// </summary>
        public static string sendurl { get; set; }

        /// <summary>
        /// 推送医院ID
        /// </summary>
        public static string sendappId { get; set; }

        /// <summary>
        /// 自动推送:默认不自动推送
        /// </summary>
        public static bool sendauto = false;

        /// <summary>
        /// 读卡url
        /// </summary>
        public static string readurl { get; set; }

        /// <summary>
        /// 读卡医院ID
        /// </summary>
        public static string readappId { get; set; }

        /// <summary>
        /// 读卡医院Key
        /// </summary>
        public static string readappkey { get; set; }

        /// <summary>
        /// 终端号
        /// </summary>
        public static string term_id { get; set; }
        public static string strDeptID
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_strDeptID))
                {
                    using (LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
                    {
                        _strDeptID = lis.全局参数.Where(c => c.参数名称 == "strDeptID").FirstOrDefault().参数值;
                    }

                        
                    BllSet系统参数();
                    BllSetSysInfo();
                }
                return _strDeptID;
            }
            set { _strDeptID = value; }
        }




        //==============================================
        /// <summary>
        /// 设置全局参数
        /// 2019年11月12日 yfh 添加 暂时先添加这两个，以后用到再说
        /// </summary>
        public static void BllSet系统参数()
        {
            try
            {
                DataTable dt = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, "SELECT * FROM 全局参数").Tables[0];
                string fname = "";//参数名称
                string fvalue = "";//参数值
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    fname = dt.Rows[i]["参数名称"].ToString();
                    fvalue = dt.Rows[i]["参数值"].ToString();
                    switch (fname)
                    {
                        case "sendurl":
                            sendurl = fvalue;
                            break;
                        case "sendappId":
                            sendappId = fvalue;
                            break;
                        case "sendauto":
                            sendauto = fvalue.Equals("true");
                            break;
                        case "readurl":
                            readurl = fvalue;
                            break;
                        case "readappId":
                            readappId = fvalue;
                            break;
                        case "readappkey":
                            readappkey = fvalue;
                            break;
                        case "term_id":
                            term_id = fvalue;
                            break;
                        case "HisDBName":
                            Properties.Settings.Default.HisDBName = fvalue;
                            break;
                        //case "strDeptID":
                        //    strDeptID = HIS.COMM.zdInfo.ModelUserInfo.科室编码;
                            //break;
                        default:
                            break;
                    }
                }
            }
            catch
            {
                //出现异常默认龙家圈医院地址
                sendurl = "http://192.168.10.171:1811/ehc-portal-push/sms/send";
                sendappId = "1DNUJKJV00000100007F0000F0308269";
                //读卡默认
                readurl = "http://192.168.10.171:1811/ehcService/gateway.do";
                readappId = "1DOB630TT0500100007F0000CC065635";
                readappkey = "1DOB630TT0510100007F0000D6D2E81F";
                term_id = "371300210001";
                //默认龙家圈
                Properties.Settings.Default.HisDBName = "HIS.CHIS2721";
                _strDeptID = "208";
            }
        }
    }
}