using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ww.wwf.com;
using yunLis.dao;
namespace yunLis.wwfbll
{
    /// <summary>
    /// 
    /// </summary>
    public class OrgFuncBLL : DAOWWF
    {
        public OrgFuncBLL()
        {
            // 
            // TODO: 在此处添加构造函数逻辑
            //	
        }


        /// <summary>
        /// 所有功能列表
        /// </summary>
        /// <returns></returns>
        public DataTable BllDTAll()
        {
            string sql = "SELECT *  FROM wwf_org ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 根据ID取得类别
        /// </summary>
        /// <param name="forg_id"></param>
        /// <returns></returns>
        public string Bllftype(string forg_id)
        {
            string sql = "SELECT ftype FROM wwf_org WHERE (forg_id = '" + forg_id + "')";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).ToString();
        }
        /// <summary>
        /// 已分功能
        /// </summary>
        /// <param name="forg_id"></param>
        /// <returns></returns>
        public DataTable BllFuncOK(string forg_id)
        {
            string sql = "SELECT t1.ffunc_id, t1.fp_id, t1.fname FROM wwf_func t1,wwf_org_func t2 where t1.ffunc_id = t2.ffunc_id and t2.forg_id = '" + forg_id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 删除一条数据 已分权限
        /// </summary>
        /// <param name="forg_id"></param>
        /// <param name="ffunc_id"></param>
        /// <returns></returns>
        public int BllOkFuncDelete(string forg_id, string ffunc_id)
        {
            string sql = "delete FROM wwf_org_func where forg_id='" + forg_id + "' and ffunc_id='" + ffunc_id + "'";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql);
        }
        /// <summary>
        /// 增加权限
        /// </summary>
        /// <param name="forg_id"></param>
        /// <param name="ffunc_id"></param>
        public void BllOkFuncInsert(string forg_id, string ffunc_id)
        {
            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn,"SELECT COUNT(1) FROM wwf_org_func WHERE (forg_id = '" + forg_id + "') AND (ffunc_id = '" + ffunc_id + "')"))
            { }
            else
            {
                string sql = "INSERT INTO wwf_org_func(fid, forg_id, ffunc_id) VALUES ('" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "', '" + forg_id + "', '" + ffunc_id + "')";
                yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql);
            }
        }

       /// <summary>
       /// 已分岗位
       /// </summary>
       /// <returns></returns>
        public DataTable BllPositionOK(string forg_id)
        {
            string sql = "SELECT *  FROM wwf_org where fp_id='" + forg_id + "' and ftype='position' ORDER BY forder_by";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 可分岗位
        /// </summary>
        /// <returns></returns>
        public DataTable BllPositionNO()
        {
            string sql = "SELECT * FROM WWF_POSITION WHERE (fuse_flag = 1)";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }

        /// <summary>
        /// 已分人员
        /// </summary>
        /// <returns></returns>
        public DataTable BllPersonOK(string forg_id)
        {
           // string sql = "SELECT forg_id, '(' + fperson_id + ')' + fname AS PersonName FROM wwf_org WHERE (fp_id = '" + forg_id + "') AND (ftype = 'person') ORDER BY fname";
            string sql = "SELECT forg_id, (select fshow_name from wwf_person t1 where t1.fperson_id=t.fperson_id) AS PersonName FROM wwf_org t WHERE (fp_id = '" + forg_id + "') AND (ftype = 'person') ORDER BY fname";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,sql).Tables[0];
        }
        /// <summary>
        /// 分岗位
        /// </summary>
        /// <param name="fposition_id"></param>
        public void BllFP(string fposition_id,string fp_id)
        {
            DataTable dtCurr = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,"SELECT * FROM WWF_POSITION WHERE (fposition_id = '" + fposition_id + "')").Tables[0];
            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn, "SELECT COUNT(1) FROM wwf_org WHERE (ftype = 'position') AND (fposition_id = '" + fposition_id + "') AND (fp_id = '" + fp_id + "')")) { }
            else
            {
                StringBuilder strSqlOrg = new StringBuilder();
                strSqlOrg.Append("insert into wwf_org(");
                strSqlOrg.Append("forg_id,ftype,fp_id,fname,fposition_id,fdept_id,fuse_flag,fremark");
                strSqlOrg.Append(")");
                strSqlOrg.Append(" values (");
                strSqlOrg.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");               
                strSqlOrg.Append("'position',");
                strSqlOrg.Append("'" + fp_id + "',");
                strSqlOrg.Append("'" + dtCurr.Rows[0]["fname"].ToString() + "',");
                strSqlOrg.Append("'" + dtCurr.Rows[0]["fposition_id"].ToString() + "',");
                strSqlOrg.Append("'" + fp_id + "',");    
                strSqlOrg.Append("'1',");
                strSqlOrg.Append("''");
                strSqlOrg.Append(")");

                yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSqlOrg.ToString());
            }
        }
        /// <summary>
        /// 分人员
        /// </summary>
        /// <param name="fperson_id"></param>
        public void BllFPPerson(string fperson_id, string fp_id)
        {
            string strDeptID = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn,"SELECT fdept_id FROM wwf_org WHERE (forg_id = '" + fp_id + "')").ToString();
            string strPositionID = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySqlString(yunLis.wwfbll.WWFInit.strDBConn,"SELECT fposition_id FROM wwf_org WHERE (forg_id = '" + fp_id + "')").ToString();

            DataTable dtCurr = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn,"SELECT * FROM wwf_person WHERE (fperson_id = '" + fperson_id + "')").Tables[0];
            if (yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn, "SELECT COUNT(1) FROM wwf_org WHERE (ftype = 'person') AND (fperson_id = '" + fperson_id + "') AND (fp_id = '" + fp_id + "')")) { }
            else
            {
                StringBuilder strSqlOrg = new StringBuilder();
                strSqlOrg.Append("insert into wwf_org(");
                strSqlOrg.Append("forg_id,ftype,fp_id,fname,fperson_id,fposition_id,fdept_id,fuse_flag,fremark");
                strSqlOrg.Append(")");
                strSqlOrg.Append(" values (");
                strSqlOrg.Append("'" + WEISHENG.COMM.Helper.GuidHelper.DbGuid() + "',");
                strSqlOrg.Append("'person',");
                strSqlOrg.Append("'" + fp_id + "',");
                strSqlOrg.Append("'" + dtCurr.Rows[0]["fname"].ToString() + "',");
                strSqlOrg.Append("'" + dtCurr.Rows[0]["fperson_id"].ToString() + "',");
                strSqlOrg.Append("'" + strPositionID + "',");
                strSqlOrg.Append("'" + strDeptID + "',");
                strSqlOrg.Append("'1',");
                strSqlOrg.Append("''");
                strSqlOrg.Append(")");

                yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn,strSqlOrg.ToString());
            }
        }
       /// <summary>
        /// 取消岗位
       /// </summary>
       /// <param name="forg_id"></param>
        public void BllQX(string forg_id)
        {
            yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(yunLis.wwfbll.WWFInit.strDBConn, "DELETE FROM wwf_org WHERE forg_id = '" + forg_id + "'");
          
        }
        /// <summary>
        /// 取消用户
        /// </summary>
        /// <param name="forg_id"></param>
        public bool BllQXPerson(string forg_id)
        {

            string sql1 = "delete FROM wwf_org WHERE (forg_id = '" + forg_id + "') ";
            string sql2 = "delete FROM wwf_org_func WHERE (forg_id = '" + forg_id + "') ";
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryTransaction2(yunLis.wwfbll.WWFInit.strDBConn,sql1, sql2);
        }
        /// <summary>
        /// 要取消的岗位下有人员否
        /// </summary>
        /// <param name="forg_id"></param>
        /// <returns></returns>
        public bool BllQXPersonOK(string forg_id)
        {
            return yunLis.wwfbll.WWFInit.wwfRemotingDao.DbRecordExists(yunLis.wwfbll.WWFInit.strDBConn, "SELECT count(1) FROM wwf_org where fp_id = '" + forg_id + "'");
        }

       
    }
}