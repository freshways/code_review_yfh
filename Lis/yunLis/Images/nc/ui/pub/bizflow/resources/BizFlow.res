CloseBox=nc/ui/pub/bizflow/resources/closebox.gif
ToolbarMenu=nc/ui/pub/bizflow/resources/ToolbarMenu.gif

#
# bizflow toolbar definition
#subflowTool blockTool Save 
bizflowtoolbar=selectTool routeTool textTool transitionTool selfTransitionTool - EditProperties Delete
selectToolImage=nc/ui/pub/bizflow/resources/select.gif
routeToolImage=nc/ui/pub/bizflow/resources/routeactivity.gif
subflowToolImage=nc/ui/pub/bizflow/resources/subflowTool.gif
blockToolImage=nc/ui/pub/bizflow/resources/blockTool.gif
textToolImage=nc/ui/pub/bizflow/resources/text.gif
transitionToolImage=nc/ui/pub/bizflow/resources/transition.gif
selfTransitionToolImage=nc/ui/pub/bizflow/resources/selfroutedtransition.gif
SaveImage=nc/ui/pub/bizflow/resources/save.gif
EditPropertiesImage=nc/ui/pub/bizflow/resources/properties.gif
DeleteImage=nc/ui/pub/bizflow/resources/delete.gif


# definition of SetSelfRouting popup menu item
SetSelfRoutingImage=nc/ui/pub/bizflow/resources/selfroutedtransition.gif

# definition of SetNoRouting popup menu item
SetNoRoutingImage=nc/ui/pub/bizflow/resources/transition.gif

# definition of FormatLineStyle popup menu item
FormatLineStyleImage=nc/ui/pub/bizflow/resources/linestyle.gif

# definition of FormatLineOrthogonal popup menu item
FormatLineOrthogonalImage=nc/ui/pub/bizflow/resources/lineorthogonal.gif

# definition of FormatLineBezier popup menu item
FormatLineBezierImage=nc/ui/pub/bizflow/resources/linebezier.gif

# definition of FormatLineQuadratic popup menu item
FormatLineQuadraticImage=nc/ui/pub/bizflow/resources/linequadratic.gif

# definition of FormatLineWidth popup menu item
FormatLineWidthImage=nc/ui/pub/bizflow/resources/linewidth.gif

# definition of FormatLineWidthMostThin popup menu item
FormatLineWidthMostThinImage=nc/ui/pub/bizflow/resources/mostthin.gif

# definition of FormatLineWidthThin popup menu item
FormatLineWidthThinImage=nc/ui/pub/bizflow/resources/thin.gif

# definition of FormatLineWidthMiddle popup menu item
FormatLineWidthMiddleImage=nc/ui/pub/bizflow/resources/middle.gif

# definition of FormatLineWidthThick popup menu item
FormatLineWidthThickImage=nc/ui/pub/bizflow/resources/thick.gif

# definition of FormatLineWidthThickest popup menu item
FormatLineWidthThickestImage=nc/ui/pub/bizflow/resources/thickest.gif

# definition of FormatLineColor popup menu item
FormatLineColorImage=nc/ui/pub/bizflow/resources/linecolor.gif

# definition of FormatLineColorRed popup menu item
FormatLineColorRedImage=nc/ui/pub/bizflow/resources/red.gif

# definition of FormatLineColorBlue popup menu item
FormatLineColorBlueImage=nc/ui/pub/bizflow/resources/blue.gif

# definition of FormatLineColorYellow popup menu item
FormatLineColorYellowImage=nc/ui/pub/bizflow/resources/yellow.gif

# definition of FormatLineColorGreen popup menu item
FormatLineColorGreenImage=nc/ui/pub/bizflow/resources/green.gif

# definition of FormatLineColorBlack popup menu item
FormatLineColorBlackImage=nc/ui/pub/bizflow/resources/black.gif

# definition of FormatLineColorCyan popup menu item
FormatLineColorCyanImage=nc/ui/pub/bizflow/resources/cyan.gif

# definition of FormatLineColorOrange popup menu item
FormatLineColorOrangeImage=nc/ui/pub/bizflow/resources/orange.gif


routeIconImage=nc/ui/pub/bizflow/resources/route.gif
funcStepIconImage=nc/ui/pub/bizflow/resources/china.gif
subflowIconImage=nc/ui/pub/bizflow/resources/subflowTreeIcon.gif
blockIconImage=nc/ui/pub/bizflow/resources/blockTreeIcon.gif
