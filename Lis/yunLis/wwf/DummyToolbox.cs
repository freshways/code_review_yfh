using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using yunLis.wwfbll;
using System.Reflection;
namespace yunLis.wwf
{
    public partial class DummyToolbox : ToolWindow
    {
        string str快捷id = "483ba746c0cd4a57946bcb462f92c88f";
        public static string str系统功能id = "";
        public static string str流程id = "";
        public static string str流程步骤id = "";
        LoginBLL bll = new LoginBLL();//组织功能
        string thisStrUserOrgId = "";//"61590ec8451a4fc2aedf4139d9b37c10";
        DockContent formFunDockContent = null;
        Form formFunForm = new Form();
        public DummyToolbox()
        {
            InitializeComponent();
        }

        private void DummyToolbox_Load(object sender, EventArgs e)
        {
            thisStrUserOrgId = LoginBLL.strOrgID;//"61590ec8451a4fc2aedf4139d9b37c10";   
            //GetxPanderList(LoginBLL.pidBusiness);
            GetxPanderListTree(LoginBLL.pidSys);
            GetxPanderList_new(LoginBLL.pidBusiness);
           

          
                formFunDockContent = this.GetFormByFormNameDockContent("yunLis.InitForm");
                if (formFunDockContent != null)
                {
                    //fff.Text = fname;
                    //fff.Show();
                    formFunDockContent.Text = "初始页";
                    formFunDockContent.TabText = "初始页"; 
                    formFunDockContent.ToolTipText = "初始页"; 
                    this.formFunDockContent.Show(MainForm.dockPanel);
                }
           
             
        }
        /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetxPanderListTree(string strp)
        {
            try
            {
                this.zaSuiteTreeView1.ZADataTable = bll.BllOrgFuncDT(thisStrUserOrgId); ;//装载数据集
                this.zaSuiteTreeView1.ZATreeViewRootValue = strp;//树 RootId　根节点值
                this.zaSuiteTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.zaSuiteTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.zaSuiteTreeView1.ZAKeyFieldName = "ffunc_id";//主键字段名称
                this.zaSuiteTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.zaSuiteTreeView1.SelectedNode = zaSuiteTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        /// <summary>
        /// 树选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void zaSuiteTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable dtthislist = bll.BllFuncListByID(e.Node.Name);                
                string fpluggableUnit = dtthislist.Rows[0]["fcj_flag"].ToString();
                string fpluggableUnitAssembly = dtthislist.Rows[0]["fcj_zp"].ToString();
                string fwinform = dtthislist.Rows[0]["ffunc_winform"].ToString();
                string fname = dtthislist.Rows[0]["fname"].ToString();
                string fopen_mode = dtthislist.Rows[0]["fopen_mode"].ToString();

                str系统功能id = dtthislist.Rows[0]["ffunc_id"].ToString();

                //SysBaseForm.strFuncP1 = dtthislist.Rows[0]["p1"].ToString();
                //SysBaseForm.strFuncP2 = dtthislist.Rows[0]["p2"].ToString();
                //SysBaseForm.strFuncP3 = dtthislist.Rows[0]["p3"].ToString();
                //SysBaseForm.strFuncP4 = dtthislist.Rows[0]["p4"].ToString();
                //SysBaseForm.strFuncP5 = dtthislist.Rows[0]["p5"].ToString();

                #region 流程设置
                string strflow_flag = "0";// dtthislist.Rows[0]["flow_flag"].ToString();               
                if (dtthislist.Rows[0]["flow_flag"] != null)
                {
                    strflow_flag = dtthislist.Rows[0]["flow_flag"].ToString();
                }
                if (strflow_flag.Equals("1"))
                {
                    string strflow_id = "";// dtthislist.Rows[0]["flow_id"].ToString();
                    string strfstep_id = "";// dtthislist.Rows[0]["fstep_id"].ToString();
                    if (dtthislist.Rows[0]["flow_id"] != null)
                    {
                        strflow_id = dtthislist.Rows[0]["flow_id"].ToString();
                    }
                    if (dtthislist.Rows[0]["fstep_id"] != null)
                    {
                        strfstep_id = dtthislist.Rows[0]["fstep_id"].ToString();
                    }
                    /*
                    try
                    {
                        string str表单 = WWFInit.wwfRemotingDao.DbExecuteScalarBySql(WWFInit.strDBConn, "SELECT (SELECT top 1 z1.fform FROM wwf_flowfunc z1 where z1.ffunc_id='1') as 表单  FROM wwf_flow t where t.flow_id='" + strflow_id + "'");
                        fwinform = str表单;
                    }
                    catch
                    {
                        WWMessage.MessageShowWarning("流程设置有误。");
                    }*/
                    str流程id = strflow_id;
                    str流程步骤id = strfstep_id;
                }
                #endregion

                if (fwinform == "" || fwinform == null) { }
                else
                {                    
                    if (fpluggableUnit == "1")
                    {
                        if (fopen_mode == "1")
                        {
                            formFunForm = GetFormByFormNameForm(fwinform, fpluggableUnitAssembly);
                            if (formFunForm != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunForm.Text = fname;                               
                                formFunForm.Show();
                            }
                        }
                        else
                        {
                            formFunDockContent = this.GetFormByFormNameDockContent(fwinform, fpluggableUnitAssembly);
                            if (formFunDockContent != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunDockContent.Text = fname;
                                formFunDockContent.TabText = fname;
                                formFunDockContent.ToolTipText = fname;
                               this.formFunDockContent.Show(MainForm.dockPanel);
                            }
                        }
                    }
                    else
                    {
                         if (fopen_mode == "1")
                        {
                            formFunForm = GetFormByFormNameForm(fwinform);
                            if (formFunForm != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunForm.Text = fname;                               
                                formFunForm.Show();
                            }
                        }
                        else
                        {
                            if (DocOK(fname) == false)
                            {
                                formFunDockContent = this.GetFormByFormNameDockContent(fwinform);
                                if (formFunDockContent != null)
                                {
                                    //fff.Text = fname;
                                    //fff.Show();
                                    formFunDockContent.Text = fname;
                                    formFunDockContent.TabText = fname;
                                    formFunDockContent.ToolTipText = fname;
                                    this.formFunDockContent.Show(MainForm.dockPanel);
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private DockContent GetFormByFormNameDockContent(string strFormName)
        {
            DockContent form = null;
            try
            {
                Type type = Type.GetType(strFormName);
                if (type!=null)
                form = (DockContent)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError("窗体有误，请联系管理员；\n当前窗体名为：" + strFormName + "\n" + ex.ToString());
            }
            return form;
        }
        private Form GetFormByFormNameForm(string strFormName)
        {
            Form form = null;
            try
            {
                Type type = Type.GetType(strFormName);
                form = (Form)Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError("窗体有误，请联系管理员；\n当前窗体名为：" + strFormName + "\n" + ex.ToString());
            }
            return form;
        }

        private DockContent GetFormByFormNameDockContent(string strFormName, string strFpluggableUnitAssembly)
        {
            DockContent form = null;
            try
            {
                Assembly assm = Assembly.LoadFrom(strFpluggableUnitAssembly);
                Type TypeToLoad = assm.GetType(strFormName);
                object obj;
                obj = Activator.CreateInstance(TypeToLoad);
                form = (DockContent)obj;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError("窗体有误，请联系管理员；\n当前窗体名为：" + strFormName + "\n" + ex.ToString());
            }
            return form;
        }
        private Form GetFormByFormNameForm(string strFormName, string strFpluggableUnitAssembly)
        {
            Form form = null;
            try
            {
                Assembly assm = Assembly.LoadFrom(strFpluggableUnitAssembly);
                Type TypeToLoad = assm.GetType(strFormName);
                object obj;
                obj = Activator.CreateInstance(TypeToLoad);
                form = (Form)obj;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError("窗体有误，请联系管理员；\n当前窗体名为：" + strFormName + "\n" + ex.ToString());
            }
            return form;
        }

        public void GetxPanderList不用(string strParentId)
        {
            /*
            xPanderListMain.Controls.Clear();
            string strNodeName = "";//主键ID
            string strNodeText = "";//显示名称     
            string strficon = "";//图标

            DataTable dt = this.bll.BllOrgFuncDT(thisStrUserOrgId, strParentId);
            int intCount = dt.Rows.Count;
            DataTable dtChild = new DataTable();

            // MessageBox.Show(intCount.ToString());
            int xPanderHeight = 50;//高度
            for (int i = 0; i < intCount; i++)
            {
                //-------------------------
                
               
                strNodeName = dt.Rows[i]["ffunc_id"].ToString();//功能ID        
                strNodeText = dt.Rows[i]["fname"].ToString();//名称  
                dtChild = bll.BllOrgFuncDT(thisStrUserOrgId, strNodeName);
                int intCountChild = dtChild.Rows.Count;
                string strNodeNameChild, strNodeTextChild, strNodeWinFormChild;
                              switch (intCountChild)
                {
                    case 1:
                        xPanderHeight = 50;
                        break;
                    case 2:
                        xPanderHeight = 75;
                        break;
                    case 3:
                        xPanderHeight = 95;
                        break;
                    case 4:
                        xPanderHeight = 120;
                        break;
                    case 5:
                        xPanderHeight = 140;
                        break;
                    case 6:
                        xPanderHeight = 160;
                        break;
                    case 7:
                        xPanderHeight = 185;
                        break;
                    case 8:
                        xPanderHeight = 205;
                        break;
                    case 9:
                        xPanderHeight = 230;
                        break;
                    case 10:
                        xPanderHeight = 255;
                        break;
                    default:
                        xPanderHeight = 50;
                        break;
                }
                if (intCountChild > 10)
                {
                    xPanderHeight = 400;
                }
                if (intCountChild > 15)
                {
                    xPanderHeight = 500;
                }
                XPExplorerBar.Expando expando1 = new XPExplorerBar.Expando();

               // expando1.AutoLayout = true;
                expando1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
                // expando1.Collapsed = false;
                expando1.Dock = System.Windows.Forms.DockStyle.Top;
                expando1.ExpandedHeight = xPanderHeight;
                expando1.Font = new System.Drawing.Font("宋体", 8.25F);
                expando1.Location = new System.Drawing.Point(0, 0);
                expando1.Name = strNodeName;
               // expando1.Size = new System.Drawing.Size(210, xPanderHeight);
                expando1.TabIndex = 0;
                expando1.Text = strNodeText;
                expando1.Collapsed = true;
                if (i == 0||i==1||i==2)
                  //  if (i == 0 || i == 1 || i == 2 || i == 3)
                {
                    expando1.Collapsed = false;
                }
                xPanderListMain.Controls.Add(expando1);

               
                for (int ii = 0; ii < intCountChild; ii++)
                {
                    strNodeNameChild = dtChild.Rows[ii]["ffunc_id"].ToString();//功能ID        
                    strNodeTextChild = dtChild.Rows[ii]["fname"].ToString();//名称  
                    strNodeWinFormChild = dtChild.Rows[ii]["ffunc_winform"].ToString();//打开窗口                 
                    strficon = dtChild.Rows[ii]["ficon"].ToString();//图标

                    System.Windows.Forms.LinkLabel LinkLabel1 = new LinkLabel();
                    LinkLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    LinkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
                    LinkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(93)))), ((int)(((byte)(198)))));
                    LinkLabel1.Location = new System.Drawing.Point(43, 35);
                    LinkLabel1.Name = strNodeNameChild;
                    LinkLabel1.Size = new System.Drawing.Size(100, 22);
                    LinkLabel1.TabIndex = ii;
                    LinkLabel1.Dock = DockStyle.Top;
                    LinkLabel1.TabStop = true;
                    LinkLabel1.Font = new System.Drawing.Font("宋体", 9F);
                    LinkLabel1.Text = strNodeTextChild;


                    LinkLabel1.Tag = strNodeWinFormChild;
                    LinkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                    LinkLabel1.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(93)))), ((int)(((byte)(198)))));

                    LinkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
                    expando1.Controls.Add(LinkLabel1);

                    //------------------------------
                }
               

            }*/
        }

        public void GetxPanderList_new(string strParentId)
        {
            xpPanelGroup1.Controls.Clear();
            string strNodeName = "";//主键ID
            string strNodeText = "";//显示名称     
            string strficon = "";//图标

            DataTable dt = this.bll.BllOrgFuncDT(thisStrUserOrgId, strParentId);
            int intCount = dt.Rows.Count;
            DataTable dtChild = new DataTable();

            // MessageBox.Show(intCount.ToString());
            int xPanderHeight = 70;//高度
            int xPanderHeight初始 = 70; 
            for (int i = 0; i < intCount; i++)
            {
                //-------------------------

                strNodeName = dt.Rows[i]["ffunc_id"].ToString();//功能ID    
                
                    strNodeText = dt.Rows[i]["fname"].ToString();//名称  
                    dtChild = bll.BllOrgFuncDT(thisStrUserOrgId, strNodeName);
                    int intCountChild = dtChild.Rows.Count;
                    string strNodeNameChild, strNodeTextChild, strNodeWinFormChild;
                    switch (intCountChild)
                    {
                        case 1:
                            xPanderHeight = xPanderHeight初始;
                            break;
                        case 2:
                            xPanderHeight = xPanderHeight初始 + 30;
                            break;
                        case 3:
                            xPanderHeight = xPanderHeight初始 + 50;
                            break;
                        case 4:
                            xPanderHeight = xPanderHeight初始 + 70;
                            break;
                        case 5:
                            xPanderHeight = xPanderHeight初始 + 90;
                            break;
                        case 6:
                            xPanderHeight = xPanderHeight初始 + 110;
                            break;
                        case 7:
                            xPanderHeight = xPanderHeight初始 + 130;
                            break;
                        case 8:
                            xPanderHeight = xPanderHeight初始 + 150;
                            break;
                        case 9:
                            xPanderHeight = xPanderHeight初始 + 170;
                            break;
                        case 10:
                            xPanderHeight = xPanderHeight初始 + 210;
                            break;
                        default:
                            xPanderHeight = xPanderHeight初始;
                            break;
                    }
                    if (intCountChild > 10)
                    {
                        xPanderHeight = 510;
                    }
                    if (intCountChild > 15)
                    {
                        xPanderHeight = 610;
                    }
                    if (strNodeName.Equals("0"))
                    {
                        xPanderHeight = 300;                
                    }
                    UIComponents.XPPanel expando1 = new UIComponents.XPPanel();


               

                    // XPExplorerBar.Expando expando1 = new XPExplorerBar.Expando();

                    // expando1.AutoLayout = true;
                    expando1.BackColor = System.Drawing.Color.Transparent;
                    expando1.XPPanelStyle = UIComponents.XPPanelStyle.Custom;
                    expando1.ForeColor = System.Drawing.SystemColors.WindowText;
                    expando1.HorzAlignment = System.Drawing.StringAlignment.Near;
                    expando1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
                    expando1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
                    
                     expando1.Dock = System.Windows.Forms.DockStyle.Top;
                      expando1.ExpandedHeight = xPanderHeight;
                    expando1.Size = new System.Drawing.Size(180, xPanderHeight);
                   
                    expando1.Location = new System.Drawing.Point(0, 0);
                    expando1.Margin = new Padding(6, 6, 0, 0);
                    expando1.Name = strNodeName;
                   
                    expando1.TabIndex = 0;
                    expando1.Text = strNodeText;
                    expando1.Caption = strNodeText;
                    expando1.CaptionGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
                    if (i > 2)
                    {
                        expando1.PanelState = UIComponents.XPPanelState.Collapsed;
                    }
                   
                    if (strNodeName.Equals("0"))
                    {
                       // xPanderHeight = 300;
                        expando1.Controls.Add(zaSuiteTreeView1);
                    }
                    xpPanelGroup1.Controls.Add(expando1);
                    for (int ii = 0; ii < intCountChild; ii++)
                    {
                         
                            strNodeNameChild = dtChild.Rows[ii]["ffunc_id"].ToString();//功能ID        
                            strNodeTextChild = dtChild.Rows[ii]["fname"].ToString();//名称  
                            strNodeWinFormChild = dtChild.Rows[ii]["ffunc_winform"].ToString();//打开窗口                 
                            strficon = dtChild.Rows[ii]["ficon"].ToString();//图标

                            System.Windows.Forms.LinkLabel LinkLabel1 = new LinkLabel();
                            LinkLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                            LinkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
                            LinkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(93)))), ((int)(((byte)(198)))));
                            LinkLabel1.Location = new System.Drawing.Point(43, 35);
                            LinkLabel1.Name = strNodeNameChild;
                            LinkLabel1.Size = new System.Drawing.Size(100, 22);
                            LinkLabel1.TabIndex = ii;
                            LinkLabel1.Margin = new Padding(6, 6, 0, 0);
                            LinkLabel1.Dock = DockStyle.Top;
                            LinkLabel1.TabStop = true;
                            LinkLabel1.Font = new System.Drawing.Font("宋体", 11F);
                            LinkLabel1.Text = "  " + strNodeTextChild;
                            LinkLabel1.Image = global::yunLis.Properties.Resources.func;
                            LinkLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;


                            LinkLabel1.Tag = strNodeWinFormChild;
                            LinkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                            LinkLabel1.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(93)))), ((int)(((byte)(198)))));

                            LinkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
                            expando1.Controls.Add(LinkLabel1);

                            /*
                 
                            System.Windows.Forms.LinkLabel l_1 = new LinkLabel();
                            l_1.AutoSize = true;
                            l_1.Dock = System.Windows.Forms.DockStyle.Top;
                         // l_1.Image = global::yunLis.Properties.Resources._new;
                          //  l_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                            l_1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
                            l_1.Location = new System.Drawing.Point(2, 35);
                            l_1.Name = strNodeWinFormChild;
                            l_1.Tag = strNodeWinFormChild;
                           // l_1.Padding = new System.Windows.Forms.Padding(22, 5, 0, 5);
                            l_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
                            l_1.Size = new System.Drawing.Size(83, 23);
                            l_1.TabIndex = 0;
                            l_1.TabStop = true;
                            l_1.Text = strNodeTextChild;
                            l_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                            l_1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
                            expando1.Controls.Add(l_1);
                             *  */
                       
                        //------------------------------
                    }

              
            }
        }

        private void LinkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                LinkLabel control = (LinkLabel)sender;
                string strNodeName = "";//主键ID
                // string strNodeText = "";//显示名称
                string strNodeTag = "";//打开页

                strNodeName = control.Name;
                strNodeTag = control.Tag.ToString();

               
                DataTable dtthislist = bll.BllFuncListByID(strNodeName);
                //string opentype = dtthislist.Rows[0]["fopen_page"].ToString();
                string fpluggableUnit = dtthislist.Rows[0]["fcj_flag"].ToString();
                string fpluggableUnitAssembly = dtthislist.Rows[0]["fcj_zp"].ToString();
                string fwinform = dtthislist.Rows[0]["ffunc_winform"].ToString();
                string fname = dtthislist.Rows[0]["fname"].ToString();
                string fopen_mode = dtthislist.Rows[0]["fopen_mode"].ToString();
                //SysBaseForm.strFuncP1 = dtthislist.Rows[0]["p1"].ToString();
                //SysBaseForm.strFuncP2 = dtthislist.Rows[0]["p2"].ToString();
                //SysBaseForm.strFuncP3 = dtthislist.Rows[0]["p3"].ToString();
                //SysBaseForm.strFuncP4 = dtthislist.Rows[0]["p4"].ToString();
                //SysBaseForm.strFuncP5 = dtthislist.Rows[0]["p5"].ToString();
                if (fwinform == "" || fwinform == null) { }
                else
                {
                    if (fpluggableUnit == "1")
                    {
                        if (fopen_mode == "1")
                        {
                            formFunForm = GetFormByFormNameForm(fwinform, fpluggableUnitAssembly);
                            if (formFunForm != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunForm.Text = fname;
                                formFunForm.Show();
                            }
                        }
                        else
                        {
                            formFunDockContent = this.GetFormByFormNameDockContent(fwinform, fpluggableUnitAssembly);
                            if (formFunDockContent != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunDockContent.Text = fname;
                                formFunDockContent.TabText = fname;
                                formFunDockContent.ToolTipText = fname;
                                this.formFunDockContent.Show(MainForm.dockPanel);
                            }
                        }
                    }
                    else
                    {
                        if (fopen_mode == "1")
                        {
                            formFunForm = GetFormByFormNameForm(fwinform);
                            if (formFunForm != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunForm.Text = fname;
                                formFunForm.Show();
                            }
                        }
                        else
                        {
                            if (DocOK(fname) == false)
                            {
                                formFunDockContent = this.GetFormByFormNameDockContent(fwinform);
                                if (formFunDockContent != null)
                                {
                                    //fff.Text = fname;
                                    //fff.Show();
                                    formFunDockContent.Text = fname;
                                    formFunDockContent.TabText = fname;
                                    formFunDockContent.ToolTipText = fname;
                                    this.formFunDockContent.Show(MainForm.dockPanel);
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        /// <summary>
        /// 查找窗口打开否
        /// </summary>
        /// <param name="fname"></param>
        private bool DocOK(string fname)
        {
            bool bl = false;
            foreach (IDockContent content in MainForm.dockPanel.Documents)
            {
                if (content.DockHandler.TabText == fname)
                {
                    content.DockHandler.Show();
                    bl = true;
                }
            }
            return bl;
        }
        
        private IDockContent FindDocument(string text)
        {
            if (MainForm.dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                foreach (Form form in MdiChildren)
                    if (form.Text == text)
                        return form as IDockContent;

                return null;
            }
            else
            {
                foreach (IDockContent content in MainForm.dockPanel.Documents)
                    if (content.DockHandler.TabText == text)
                        return content;

                return null;
            }
        }
        private bool VisibleForm(TabPage tabpage, Form form)
        {
            bool b = true;
            try
            {

                form.Location = new Point(0, 0);
                form.Dock = DockStyle.Fill;
                //form.Dock = DockStyle.Left;
                //form.WindowState = FormWindowState.Maximized;
                form.TopLevel = false;
                form.TopMost = false;
                form.ControlBox = false;
                form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                form.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
                form.Size = tabpage.ClientSize;
                form.Parent = tabpage;
                form.Visible = true;

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());

            }
            return b;
        }

        private void zaSuiteTreeView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable dtthislist = bll.BllFuncListByID(this.zaSuiteTreeView1.SelectedNode.Name);
                string fpluggableUnit = dtthislist.Rows[0]["fcj_flag"].ToString();
                string fpluggableUnitAssembly = dtthislist.Rows[0]["fcj_zp"].ToString();
                string fwinform = dtthislist.Rows[0]["ffunc_winform"].ToString();
                string fname = dtthislist.Rows[0]["fname"].ToString();
                string fopen_mode = dtthislist.Rows[0]["fopen_mode"].ToString();
                if (fwinform == "" || fwinform == null) { }
                else
                {
                    if (fpluggableUnit == "1")
                    {
                        if (fopen_mode == "1")
                        {
                            formFunForm = GetFormByFormNameForm(fwinform, fpluggableUnitAssembly);
                            if (formFunForm != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunForm.Text = fname;
                                formFunForm.Show();
                            }
                        }
                        else
                        {
                            formFunDockContent = this.GetFormByFormNameDockContent(fwinform, fpluggableUnitAssembly);
                            if (formFunDockContent != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunDockContent.Text = fname;
                                formFunDockContent.TabText = fname;
                                formFunDockContent.ToolTipText = fname;
                                this.formFunDockContent.Show(MainForm.dockPanel);
                            }
                        }
                    }
                    else
                    {
                        if (fopen_mode == "1")
                        {
                            formFunForm = GetFormByFormNameForm(fwinform);
                            if (formFunForm != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunForm.Text = fname;
                                formFunForm.Show();
                            }
                        }
                        else
                        {
                            formFunDockContent = this.GetFormByFormNameDockContent(fwinform);
                            if (formFunDockContent != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunDockContent.Text = fname;
                                formFunDockContent.TabText = fname;
                                formFunDockContent.ToolTipText = fname;
                                this.formFunDockContent.Show(MainForm.dockPanel);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void 打开ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable dtthislist = bll.BllFuncListByID(this.zaSuiteTreeView1.SelectedNode.Name);
                string fpluggableUnit = dtthislist.Rows[0]["fcj_flag"].ToString();
                string fpluggableUnitAssembly = dtthislist.Rows[0]["fcj_zp"].ToString();
                string fwinform = dtthislist.Rows[0]["ffunc_winform"].ToString();
                string fname = dtthislist.Rows[0]["fname"].ToString();
                string fopen_mode = dtthislist.Rows[0]["fopen_mode"].ToString();
                if (fwinform == "" || fwinform == null) { }
                else
                {
                    if (fpluggableUnit == "1")
                    {
                        if (fopen_mode == "1")
                        {
                            formFunForm = GetFormByFormNameForm(fwinform, fpluggableUnitAssembly);
                            if (formFunForm != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunForm.Text = fname;
                                formFunForm.Show();
                            }
                        }
                        else
                        {
                            formFunDockContent = this.GetFormByFormNameDockContent(fwinform, fpluggableUnitAssembly);
                            if (formFunDockContent != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunDockContent.Text = fname;
                                formFunDockContent.TabText = fname;
                                formFunDockContent.ToolTipText = fname;
                                this.formFunDockContent.Show(MainForm.dockPanel);
                            }
                        }
                    }
                    else
                    {
                        if (fopen_mode == "1")
                        {
                            formFunForm = GetFormByFormNameForm(fwinform);
                            if (formFunForm != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunForm.Text = fname;
                                formFunForm.Show();
                            }
                        }
                        else
                        {
                            formFunDockContent = this.GetFormByFormNameDockContent(fwinform);
                            if (formFunDockContent != null)
                            {
                                //fff.Text = fname;
                                //fff.Show();
                                formFunDockContent.Text = fname;
                                formFunDockContent.TabText = fname;
                                formFunDockContent.ToolTipText = fname;
                                this.formFunDockContent.Show(MainForm.dockPanel);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
      
    }
}

/*
 if (FindDocument("检验申请") == null)
                t111 = new t1();
        
         
            
            if (MainForm.dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                
                t111.MdiParent = this;
                t111.Show();
            }
            else
                t111.Show(MainForm.dockPanel);  
 */