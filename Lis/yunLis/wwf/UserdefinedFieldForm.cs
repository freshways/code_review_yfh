﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;
namespace yunLis.wwf
{
    //public partial class UserdefinedFieldForm : SysBaseForm
    public partial class UserdefinedFieldForm : SysBaseForm
    {
        UserdefinedBLL bll = new UserdefinedBLL();
        string strTypeID = "";//类型ID
        DataTable dtCurrColumns = new DataTable();
        public UserdefinedFieldForm()
        {
            InitializeComponent();
           
        }

        private void UserdefinedFieldForm_Load(object sender, EventArgs e)
        {
           // ShowLoginForm();
            GetTree("0");
           // this.richTextBoxfquery_sql.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_columnsBindingSource, "fquery_sql", true));
           // this.richTextBoxfformula.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_columnsBindingSource, "fformula", true));
        }
        /*
        /// <summary>
        /// 打开登录Form
        /// </summary>
        private void ShowLoginForm()
        {
            try
            {
                this.Hide();
                LoginForm frmlogin = new LoginForm();
                if (frmlogin.ShowDialog() == DialogResult.OK)
                {
                    frmlogin.Dispose();
                    this.Show();
                }
                else
                {
                    frmlogin.Dispose();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }*/
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllTypeDT(2);
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "ftable_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void labelfquery_sql_Click(object sender, EventArgs e)
        {

        }

        private void labelfformula_Click(object sender, EventArgs e)
        {

        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                this.strTypeID = e.Node.Name.ToString();
                GetColumns(strTypeID);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (strTypeID == "" || strTypeID == null)
                { }
                else
                {
                    if (this.bll.BllColumnsAdd(this.strTypeID) > 0)
                        GetColumns(strTypeID);
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void GetColumns(string fp_id)
        {
            try
            {
                if (dtCurrColumns != null)
                    dtCurrColumns.Clear();
                dtCurrColumns = this.bll.BllColumnsByftable_id(fp_id);
                //this.DataGridViewObject.DataSource = dtCurrColumns;
                this.wwf_columnsBindingSource.DataSource = dtCurrColumns;
                
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.wwf_columnsBindingSource.EndEdit();
                for (int i = 0; i < this.dtCurrColumns.Rows.Count; i++)
                {
                    this.bll.BllColumnsUpdate(this.dtCurrColumns.Rows[i]).ToString();
                }
                GetColumns(this.strTypeID);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void butRef_Click(object sender, EventArgs e)
        {
            try
            {
                GetTree("0");
                GetColumns(this.strTypeID);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void DataGridViewObject_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.wwf_columnsBindingSource.EndEdit();
                string fcolumns_id = this.DataGridViewObject.CurrentRow.Cells["fcolumns_id"].Value.ToString();
                if (yunLis.wwfbll.WWMessage.MessageDialogResult("真要删除些列? 记录删除后将不可恢复!"))
                {
                    if (this.bll.BllColumnsDel(fcolumns_id) > 0)
                    {

                        GetColumns(this.strTypeID);
                    }
                }
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void buttonExcel_Click(object sender, EventArgs e)
        {
            BllExcelOut();
        }
        /// <summary>
        /// 导出Excel
        /// </summary>
        private void BllExcelOut()
        {
            try
            {
                //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 ▽
                //ExcelHelper excelhelp = new ExcelHelper("", "");
                //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 △
                this.Validate();
                this.DataGridViewObject.EndEdit();
                if (this.DataGridViewObject.Rows.Count > 0)
                {
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 ▽
                    //int intTop = 1;
                    //int intLeft = 1;
                    //excelhelp.GridViewToExcel(this.DataGridViewObject, "", intTop, intLeft);
                    ExcelHelper.DataGridViewToExcel(this.DataGridViewObject);
                    //changed by wjz 20150520 将旧的导出方式替换为NPOI的实现方式 △
                }
                else
                {
                    WWMessage.MessageShowWarning("暂无记录,不可导出!");
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }
       
    }
}