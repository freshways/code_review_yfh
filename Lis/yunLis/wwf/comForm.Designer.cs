﻿namespace ww.lis.wwf
{
    partial class comForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(comForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BindingNavigatorObject = new System.Windows.Forms.BindingNavigator(this.components);
            this.bAdd = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bDelete = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bSave = new System.Windows.Forms.ToolStripButton();
            this.bPrint = new System.Windows.Forms.ToolStripButton();
            this.bQuery = new System.Windows.Forms.ToolStripButton();
            this.bUpdate = new System.Windows.Forms.ToolStripButton();
            this.bExcel = new System.Windows.Forms.ToolStripButton();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BindingSourceObject = new System.Windows.Forms.BindingSource(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelTool = new System.Windows.Forms.Panel();
            this.butUpdate = new System.Windows.Forms.Button();
            this.butOut = new System.Windows.Forms.Button();
            this.butRef = new System.Windows.Forms.Button();
            this.butPrint = new System.Windows.Forms.Button();
            this.butQuery = new System.Windows.Forms.Button();
            this.butSave = new System.Windows.Forms.Button();
            this.butDel = new System.Windows.Forms.Button();
            this.butAdd = new System.Windows.Forms.Button();
            this.TreeViewObject = new ww.wwf.control.WWControlLib.WWTreeView();
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorObject)).BeginInit();
            this.BindingNavigatorObject.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSourceObject)).BeginInit();
            this.panelTool.SuspendLayout();
            this.SuspendLayout();
            // 
            // BindingNavigatorObject
            // 
            this.BindingNavigatorObject.AddNewItem = this.bAdd;
            this.BindingNavigatorObject.CountItem = this.bindingNavigatorCountItem;
            this.BindingNavigatorObject.DeleteItem = this.bDelete;
            this.BindingNavigatorObject.Enabled = false;
            this.BindingNavigatorObject.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.BindingNavigatorObject.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bAdd,
            this.bDelete,
            this.bSave,
            this.bPrint,
            this.bQuery,
            this.bUpdate,
            this.bExcel});
            this.BindingNavigatorObject.Location = new System.Drawing.Point(0, 0);
            this.BindingNavigatorObject.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.BindingNavigatorObject.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.BindingNavigatorObject.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.BindingNavigatorObject.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.BindingNavigatorObject.Name = "BindingNavigatorObject";
            this.BindingNavigatorObject.PositionItem = this.bindingNavigatorPositionItem;
            this.BindingNavigatorObject.Size = new System.Drawing.Size(846, 25);
            this.BindingNavigatorObject.TabIndex = 87;
            this.BindingNavigatorObject.Text = "bindingNavigator1";
            // 
            // bAdd
            // 
            this.bAdd.Image = ((System.Drawing.Image)(resources.GetObject("bAdd.Image")));
            this.bAdd.Name = "bAdd";
            this.bAdd.RightToLeftAutoMirrorImage = true;
            this.bAdd.Size = new System.Drawing.Size(64, 22);
            this.bAdd.Text = "新 增  ";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(32, 22);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bDelete
            // 
            this.bDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.bDelete.Image = ((System.Drawing.Image)(resources.GetObject("bDelete.Image")));
            this.bDelete.Name = "bDelete";
            this.bDelete.RightToLeftAutoMirrorImage = true;
            this.bDelete.Size = new System.Drawing.Size(64, 22);
            this.bDelete.Text = "删 除  ";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "移到第一条记录";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "移到上一条记录";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("宋体", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "移到下一条记录";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "移到最后一条记录";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bSave
            // 
            this.bSave.Image = ((System.Drawing.Image)(resources.GetObject("bSave.Image")));
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(64, 22);
            this.bSave.Text = "保 存  ";
            // 
            // bPrint
            // 
            this.bPrint.Image = ((System.Drawing.Image)(resources.GetObject("bPrint.Image")));
            this.bPrint.Name = "bPrint";
            this.bPrint.Size = new System.Drawing.Size(60, 22);
            this.bPrint.Text = "打 印 ";
            // 
            // bQuery
            // 
            this.bQuery.Image = global::yunLis.Properties.Resources.func;
            this.bQuery.Name = "bQuery";
            this.bQuery.Size = new System.Drawing.Size(60, 22);
            this.bQuery.Text = "查 询 ";
            // 
            // bUpdate
            // 
            this.bUpdate.Image = ((System.Drawing.Image)(resources.GetObject("bUpdate.Image")));
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(64, 22);
            this.bUpdate.Text = "修 改  ";
            // 
            // bExcel
            // 
            this.bExcel.Image = ((System.Drawing.Image)(resources.GetObject("bExcel.Image")));
            this.bExcel.Name = "bExcel";
            this.bExcel.Size = new System.Drawing.Size(81, 22);
            this.bExcel.Text = "导出Excel";
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(210, 25);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(636, 270);
            this.DataGridViewObject.TabIndex = 88;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Column7";
            this.Column7.Name = "Column7";
            // 
            // BindingSourceObject
            // 
            this.BindingSourceObject.DataMember = "wwf_func";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(210, 25);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 270);
            this.splitter1.TabIndex = 89;
            this.splitter1.TabStop = false;
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.butUpdate);
            this.panelTool.Controls.Add(this.butOut);
            this.panelTool.Controls.Add(this.butRef);
            this.panelTool.Controls.Add(this.butPrint);
            this.panelTool.Controls.Add(this.butQuery);
            this.panelTool.Controls.Add(this.butSave);
            this.panelTool.Controls.Add(this.butDel);
            this.panelTool.Controls.Add(this.butAdd);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTool.Location = new System.Drawing.Point(0, 295);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(846, 27);
            this.panelTool.TabIndex = 90;
            // 
            // butUpdate
            // 
            this.butUpdate.Image = ((System.Drawing.Image)(resources.GetObject("butUpdate.Image")));
            this.butUpdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butUpdate.Location = new System.Drawing.Point(605, 3);
            this.butUpdate.Name = "butUpdate";
            this.butUpdate.Size = new System.Drawing.Size(80, 23);
            this.butUpdate.TabIndex = 7;
            this.butUpdate.Text = "   修改(&U)";
            this.butUpdate.UseVisualStyleBackColor = true;
            // 
            // butOut
            // 
            this.butOut.Image = ((System.Drawing.Image)(resources.GetObject("butOut.Image")));
            this.butOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butOut.Location = new System.Drawing.Point(523, 3);
            this.butOut.Name = "butOut";
            this.butOut.Size = new System.Drawing.Size(80, 23);
            this.butOut.TabIndex = 6;
            this.butOut.Text = "   导出(&O)";
            this.butOut.UseVisualStyleBackColor = true;
            // 
            // butRef
            // 
            this.butRef.Image = ((System.Drawing.Image)(resources.GetObject("butRef.Image")));
            this.butRef.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butRef.Location = new System.Drawing.Point(441, 3);
            this.butRef.Name = "butRef";
            this.butRef.Size = new System.Drawing.Size(80, 23);
            this.butRef.TabIndex = 5;
            this.butRef.Text = "   刷新(&R)";
            this.butRef.UseVisualStyleBackColor = true;
            // 
            // butPrint
            // 
            this.butPrint.Image = ((System.Drawing.Image)(resources.GetObject("butPrint.Image")));
            this.butPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butPrint.Location = new System.Drawing.Point(359, 3);
            this.butPrint.Name = "butPrint";
            this.butPrint.Size = new System.Drawing.Size(80, 23);
            this.butPrint.TabIndex = 4;
            this.butPrint.Text = "   打印(&P)";
            this.butPrint.UseVisualStyleBackColor = true;
            // 
            // butQuery
            // 
            this.butQuery.Image = ((System.Drawing.Image)(resources.GetObject("butQuery.Image")));
            this.butQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butQuery.Location = new System.Drawing.Point(277, 3);
            this.butQuery.Name = "butQuery";
            this.butQuery.Size = new System.Drawing.Size(80, 23);
            this.butQuery.TabIndex = 3;
            this.butQuery.Text = "   查询(&Q)";
            this.butQuery.UseVisualStyleBackColor = true;
            // 
            // butSave
            // 
            this.butSave.Image = ((System.Drawing.Image)(resources.GetObject("butSave.Image")));
            this.butSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.Location = new System.Drawing.Point(195, 3);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(80, 23);
            this.butSave.TabIndex = 2;
            this.butSave.Text = "   保存(&S)";
            this.butSave.UseVisualStyleBackColor = true;
            // 
            // butDel
            // 
            this.butDel.Image = ((System.Drawing.Image)(resources.GetObject("butDel.Image")));
            this.butDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butDel.Location = new System.Drawing.Point(113, 3);
            this.butDel.Name = "butDel";
            this.butDel.Size = new System.Drawing.Size(80, 23);
            this.butDel.TabIndex = 1;
            this.butDel.Text = "   删除(&A)";
            this.butDel.UseVisualStyleBackColor = true;
            // 
            // butAdd
            // 
            this.butAdd.Image = ((System.Drawing.Image)(resources.GetObject("butAdd.Image")));
            this.butAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butAdd.Location = new System.Drawing.Point(31, 3);
            this.butAdd.Name = "butAdd";
            this.butAdd.Size = new System.Drawing.Size(80, 23);
            this.butAdd.TabIndex = 0;
            this.butAdd.Text = "   新增(&A)";
            this.butAdd.UseVisualStyleBackColor = true;
            // 
            // TreeViewObject
            // 
            this.TreeViewObject.Dock = System.Windows.Forms.DockStyle.Left;
            this.TreeViewObject.Location = new System.Drawing.Point(0, 25);
            this.TreeViewObject.Name = "TreeViewObject";
            this.TreeViewObject.Size = new System.Drawing.Size(210, 270);
            this.TreeViewObject.TabIndex = 86;
            this.TreeViewObject.ZADataTable = null;
            this.TreeViewObject.ZADisplayFieldName = "";
            this.TreeViewObject.ZAKeyFieldName = "";
            this.TreeViewObject.ZAParentFieldName = "";
            this.TreeViewObject.ZAToolTipTextName = "";
            this.TreeViewObject.ZATreeViewRootValue = "";
            // 
            // comForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(846, 322);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.TreeViewObject);
            this.Controls.Add(this.BindingNavigatorObject);
            this.Controls.Add(this.panelTool);
            this.Name = "comForm";
            this.Text = "comForm";
            ((System.ComponentModel.ISupportInitialize)(this.BindingNavigatorObject)).EndInit();
            this.BindingNavigatorObject.ResumeLayout(false);
            this.BindingNavigatorObject.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSourceObject)).EndInit();
            this.panelTool.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ww.wwf.control.WWControlLib.WWTreeView TreeViewObject;
        private System.Windows.Forms.BindingNavigator BindingNavigatorObject;
        private System.Windows.Forms.ToolStripButton bAdd;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bDelete;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton bSave;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.BindingSource BindingSourceObject;
        private System.Windows.Forms.ToolStripButton bPrint;
        private System.Windows.Forms.ToolStripButton bQuery;
        private System.Windows.Forms.ToolStripButton bUpdate;
        private System.Windows.Forms.ToolStripButton bExcel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butAdd;
        private System.Windows.Forms.Button butDel;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butPrint;
        private System.Windows.Forms.Button butQuery;
        private System.Windows.Forms.Button butOut;
        private System.Windows.Forms.Button butRef;
        private System.Windows.Forms.Button butUpdate;
    }
}