namespace ww.form.wwf.print
{
    partial class ReportPrint
    {
        #region  用户定义
        /// <summary>
        /// 报表路径
        /// </summary>
        public string ReportPath
        {
            /*
            get
            {
                return this.rptViewer.LocalReport.ReportPath;
            }
            set
            {
                this.rptViewer.LocalReport.ReportPath = value;
            }*/
            get
            {
                return this.rptViewer.LocalReport.ReportEmbeddedResource;
            }
            set
            {
                this.rptViewer.LocalReport.ReportEmbeddedResource = value;
            }
        }

        private string m_ReportName = string.Empty;
        /// <summary>
        /// 报表名称
        /// </summary>
        public string ReportName
        {
            get
            {
                return this.m_ReportName;
            }
            set
            {
                this.m_ReportName = value;
            }
        }

        /// <summary>
        /// DataSource of the Main Report
        /// </summary>
        private object m_MainDataSet = null;
        /// <summary>
        /// 报表数据集  主
        /// </summary>
        public object MainDataSet
        {
            get
            {
                return this.m_MainDataSet;
            }
            set
            {
                this.m_MainDataSet = value;
            }
        }

        /// <summary>
        /// DataSource of the DrillThrough Report
        /// </summary>
        private object m_DrillDataSet = null;
        /// <summary>
        /// 报表数据集  子
        /// </summary>
        public object DrillDataSet
        {
            get
            {
                return this.m_DrillDataSet;
            }
            set
            {
                this.m_DrillDataSet = value;
            }
        }
        /// <summary>
        /// Data Source Name of the Main Report
        /// </summary>
        private string m_MainDataSourceName = string.Empty;
        /// <summary>
        /// 主数据源名
        /// </summary>
        public string MainDataSourceName
        {
            get
            {
                return this.m_MainDataSourceName;
            }
            set
            {
                this.m_MainDataSourceName = value;
            }
        }

        /// <summary>
        /// Data Source Name of the DrillThrough Report
        /// </summary>
        private string m_DrillDataSourceName = string.Empty;
        /// <summary>
        /// 子数据源名
        /// </summary>
        public string DrillDataSourceName
        {
            get
            {
                return this.m_DrillDataSourceName;
            }
            set
            {
                this.m_DrillDataSourceName = value;
            }
        }



        /// <summary>
        /// Data Source Name of the Main Report
        /// </summary>
        private string m_MainDataSourceName2 = string.Empty;
        public string MainDataSourceName2
        {
            get
            {
                return this.m_MainDataSourceName2;
            }
            set
            {
                this.m_MainDataSourceName2 = value;
            }
        }
        #endregion 
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportPrint));
            this.rptViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rptViewer
            // 
            this.rptViewer.AutoScroll = true;
            this.rptViewer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rptViewer.DocumentMapCollapsed = true;
            this.rptViewer.LocalReport.ReportEmbeddedResource = "RDLCPrint.rptWuLiao.rdlc";
            this.rptViewer.Location = new System.Drawing.Point(142, 12);
            this.rptViewer.Name = "rptViewer";
            this.rptViewer.ShowToolBar = false;
            this.rptViewer.Size = new System.Drawing.Size(105, 21);
            this.rptViewer.TabIndex = 1;
            this.rptViewer.Visible = false;
            this.rptViewer.Drillthrough += new Microsoft.Reporting.WinForms.DrillthroughEventHandler(this.rptViewer_Drillthrough);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(251, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "系统正在打印... ...";
            // 
            // ReportPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 103);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rptViewer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportPrint";
            this.ShowIcon = false;
            this.Text = "打印";
            this.Load += new System.EventHandler(this.ReportPrint_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rptViewer;
        private System.Windows.Forms.Label label1;

    }
}