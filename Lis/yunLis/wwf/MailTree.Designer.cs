﻿namespace yunLis.wwf
{
    partial class MailTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailTree));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.zaSuiteTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "1.gif");
            this.imageList.Images.SetKeyName(1, "2.gif");
            // 
            // zaSuiteTreeView1
            // 
            this.zaSuiteTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zaSuiteTreeView1.ImageIndex = 0;
            this.zaSuiteTreeView1.ImageList = this.imageList;
            this.zaSuiteTreeView1.Location = new System.Drawing.Point(0, 0);
            this.zaSuiteTreeView1.Name = "zaSuiteTreeView1";
            this.zaSuiteTreeView1.SelectedImageIndex = 1;
            this.zaSuiteTreeView1.Size = new System.Drawing.Size(227, 216);
            this.zaSuiteTreeView1.TabIndex = 85;
            this.zaSuiteTreeView1.ZADataTable = null;
            this.zaSuiteTreeView1.ZADisplayFieldName = "";
            this.zaSuiteTreeView1.ZAKeyFieldName = "";
            this.zaSuiteTreeView1.ZAParentFieldName = "";
            this.zaSuiteTreeView1.ZAToolTipTextName = "";
            this.zaSuiteTreeView1.ZATreeViewRootValue = "";
            // 
            // MailTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.zaSuiteTreeView1);
            this.Name = "MailTree";
            this.Size = new System.Drawing.Size(227, 216);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private ww.wwf.control.WWControlLib.WWTreeView zaSuiteTreeView1;
    }
}
