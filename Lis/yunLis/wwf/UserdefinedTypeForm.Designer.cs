﻿namespace yunLis.wwf
{
    partial class UserdefinedTypeForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserdefinedTypeForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.wwTreeView1 = new ww.wwf.control.WWControlLib.WWTreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.panelTool = new System.Windows.Forms.Panel();
            this.butRef = new System.Windows.Forms.Button();
            this.butSave = new System.Windows.Forms.Button();
            this.butAdd = new System.Windows.Forms.Button();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.bindingSourceType = new System.Windows.Forms.BindingSource(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.fuse_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ftype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ftemplate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fexcel_left = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fexcel_top = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fsum_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ftable_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelTool.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceType)).BeginInit();
            this.SuspendLayout();
            // 
            // wwTreeView1
            // 
            this.wwTreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wwTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wwTreeView1.ImageIndex = 0;
            this.wwTreeView1.ImageList = this.imageListTree;
            this.wwTreeView1.Location = new System.Drawing.Point(0, 0);
            this.wwTreeView1.Name = "wwTreeView1";
            this.wwTreeView1.SelectedImageIndex = 1;
            this.wwTreeView1.Size = new System.Drawing.Size(200, 338);
            this.wwTreeView1.TabIndex = 116;
            this.wwTreeView1.ZADataTable = null;
            this.wwTreeView1.ZADisplayFieldName = "";
            this.wwTreeView1.ZAKeyFieldName = "";
            this.wwTreeView1.ZAParentFieldName = "";
            this.wwTreeView1.ZAToolTipTextName = "";
            this.wwTreeView1.ZATreeViewRootValue = "";
            this.wwTreeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.wwTreeView1_AfterSelect);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTree.Images.SetKeyName(0, "12.gif");
            this.imageListTree.Images.SetKeyName(1, "27.gif");
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.butRef);
            this.panelTool.Controls.Add(this.butSave);
            this.panelTool.Controls.Add(this.butAdd);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTool.Location = new System.Drawing.Point(0, 338);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(707, 27);
            this.panelTool.TabIndex = 117;
            // 
            // butRef
            // 
            this.butRef.Image = ((System.Drawing.Image)(resources.GetObject("butRef.Image")));
            this.butRef.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butRef.Location = new System.Drawing.Point(183, 3);
            this.butRef.Name = "butRef";
            this.butRef.Size = new System.Drawing.Size(80, 23);
            this.butRef.TabIndex = 5;
            this.butRef.Text = "   刷新(&R)";
            this.butRef.UseVisualStyleBackColor = true;
            this.butRef.Click += new System.EventHandler(this.butRef_Click);
            // 
            // butSave
            // 
            this.butSave.Image = ((System.Drawing.Image)(resources.GetObject("butSave.Image")));
            this.butSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.Location = new System.Drawing.Point(97, 3);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(80, 23);
            this.butSave.TabIndex = 2;
            this.butSave.Text = "   保存(&S)";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butAdd
            // 
            this.butAdd.Image = ((System.Drawing.Image)(resources.GetObject("butAdd.Image")));
            this.butAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butAdd.Location = new System.Drawing.Point(11, 3);
            this.butAdd.Name = "butAdd";
            this.butAdd.Size = new System.Drawing.Size(80, 23);
            this.butAdd.TabIndex = 0;
            this.butAdd.Text = "   新增(&A)";
            this.butAdd.UseVisualStyleBackColor = true;
            this.butAdd.Click += new System.EventHandler(this.butAdd_Click);
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AutoGenerateColumns = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.ColumnHeadersHeight = 21;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fuse_flag,
            this.ftype,
            this.fname,
            this.forder_by,
            this.ftemplate,
            this.fexcel_left,
            this.fexcel_top,
            this.fsum_flag,
            this.fremark,
            this.fcode,
            this.ftable_id});
            this.DataGridViewObject.DataSource = this.bindingSourceType;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(200, 0);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 35;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(507, 338);
            this.DataGridViewObject.TabIndex = 123;
            // 
            // bindingSourceType
            // 
            this.bindingSourceType.PositionChanged += new System.EventHandler(this.bindingSourceType_PositionChanged);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(200, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 338);
            this.splitter1.TabIndex = 124;
            this.splitter1.TabStop = false;
            // 
            // fuse_flag
            // 
            this.fuse_flag.DataPropertyName = "fuse_flag";
            this.fuse_flag.FalseValue = "0";
            this.fuse_flag.HeaderText = "启";
            this.fuse_flag.IndeterminateValue = "0";
            this.fuse_flag.Name = "fuse_flag";
            this.fuse_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_flag.TrueValue = "1";
            this.fuse_flag.Width = 40;
            // 
            // ftype
            // 
            this.ftype.DataPropertyName = "ftype";
            this.ftype.HeaderText = "类型";
            this.ftype.Name = "ftype";
            this.ftype.Width = 60;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.Width = 150;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.Width = 60;
            // 
            // ftemplate
            // 
            this.ftemplate.DataPropertyName = "ftemplate";
            this.ftemplate.HeaderText = "导出模板";
            this.ftemplate.Name = "ftemplate";
            this.ftemplate.Width = 200;
            // 
            // fexcel_left
            // 
            this.fexcel_left.DataPropertyName = "fexcel_left";
            this.fexcel_left.HeaderText = "Excel左";
            this.fexcel_left.Name = "fexcel_left";
            this.fexcel_left.Width = 60;
            // 
            // fexcel_top
            // 
            this.fexcel_top.DataPropertyName = "fexcel_top";
            this.fexcel_top.HeaderText = "Excel上";
            this.fexcel_top.Name = "fexcel_top";
            this.fexcel_top.Width = 60;
            // 
            // fsum_flag
            // 
            this.fsum_flag.DataPropertyName = "fsum_flag";
            this.fsum_flag.FalseValue = "0";
            this.fsum_flag.HeaderText = "合计";
            this.fsum_flag.IndeterminateValue = "0";
            this.fsum_flag.Name = "fsum_flag";
            this.fsum_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fsum_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fsum_flag.TrueValue = "1";
            this.fsum_flag.Width = 40;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            // 
            // fcode
            // 
            this.fcode.DataPropertyName = "fcode";
            this.fcode.HeaderText = "编号";
            this.fcode.Name = "fcode";
            this.fcode.Visible = false;
            // 
            // ftable_id
            // 
            this.ftable_id.DataPropertyName = "ftable_id";
            this.ftable_id.HeaderText = "ftable_id";
            this.ftable_id.Name = "ftable_id";
            // 
            // UserdefinedTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 365);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.wwTreeView1);
            this.Controls.Add(this.panelTool);
            this.Name = "UserdefinedTypeForm";
            this.Text = "用户自定义类型";
            this.Load += new System.EventHandler(this.UserdefinedTypeForm_Load);
            this.panelTool.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ww.wwf.control.WWControlLib.WWTreeView wwTreeView1;
        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butRef;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butAdd;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.BindingSource bindingSourceType;
        private System.Windows.Forms.ImageList imageListTree;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftype;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftemplate;
        private System.Windows.Forms.DataGridViewTextBoxColumn fexcel_left;
        private System.Windows.Forms.DataGridViewTextBoxColumn fexcel_top;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fsum_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        private System.Windows.Forms.DataGridViewTextBoxColumn fcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftable_id;
    }
}