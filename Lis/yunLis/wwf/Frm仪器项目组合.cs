﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ww.lis.lisbll.sam;
using ww.wwf.wwfbll;

namespace ww.form.wwf
{
    public partial class Frm仪器项目组合 : ww.form.wwf.SysBaseForm
    {
        public Frm仪器项目组合()
        {
            InitializeComponent();
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView2.AutoGenerateColumns = false;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            //如果当前焦点在仪器控件上，则禁止控件的操作
            if ((keyData == (Keys.Down) || keyData == (Keys.Up))
                && (this.ActiveControl == this.cboInstr)
                )
            {
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void Frm仪器项目组合_Load(object sender, EventArgs e)
        {
            InitInstr();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            //InitInstr();
            cboInstr_SelectedIndexChanged(null, null);
        }

        void InitInstr()
        {
            try
            {
                cboInstr.SelectedIndexChanged -= cboInstr_SelectedIndexChanged;

                InstrBLL bllInstr = new InstrBLL();
                DataTable dtInstr = bllInstr.BllInstrDTByUseAndGroupID(1, ww.wwf.wwfbll.LoginBLL.strDeptID_New);//仪器列表
                this.cboInstr.ValueMember = "finstr_id";
                this.cboInstr.DisplayMember = "ShowName";
                this.cboInstr.DataSource = dtInstr;

                string strInstrID = ww.form.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    DataRow[] drInstr = dtInstr.Select("finstr_id='" + strInstrID + "'");
                    if (drInstr.Length > 0)
                    {
                        this.cboInstr.SelectedValue = strInstrID;
                    }
                }

                cboInstr_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
            finally
            {
                cboInstr.SelectedIndexChanged -= cboInstr_SelectedIndexChanged;
                cboInstr.SelectedIndexChanged += cboInstr_SelectedIndexChanged;
            }
        }

        private void cboInstr_SelectedIndexChanged(object sender, EventArgs e)
        {
            object obj = cboInstr.SelectedValue;
            if(obj==null || string.IsNullOrWhiteSpace(obj.ToString()))
            {
                this.dataGridView1.Rows.Clear();
                this.dataGridView2.Rows.Clear();
                return;
            }

            ShowInstrGroup(obj.ToString());
        }

        void ShowInstrGroup(string instrid)
        {
            try
            {
                string strSql = "select * from SAM_INSTR_GROUPS where finstrid=@instrid order by orderby";
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("@instrid", instrid));
                //更新组合列表数据
                DataTable data = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, strSql, paramlist.ToArray()).Tables[0];
                this.dataGridView1.DataSource = data;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning(ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ShowGroupItems();
        }

        private void dataGridView1_DataSourceChanged(object sender, EventArgs e)
        {
            if(dataGridView1.Rows.Count ==0)
            {
                DataTable datasource = this.dataGridView2.DataSource as DataTable;
                if (datasource != null)
                {
                    datasource.Clear();
                }
                //this.dataGridView2.Rows.Clear();
                return;
            }
            ShowGroupItems();
        }

        void ShowGroupItems()
        {
            try
            {
                string groupid = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
                ShowGroupItemsInfo(groupid);
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        void ShowGroupItemsInfo(string groupid)
        {
            string sql = @"SELECT AA.[groupitemid],AA.[groupid],AA.[fitemid],BB.fitem_code,BB.fname,BB.funit_name,BB.fref
  FROM [dbo].[SAM_INSTR_GROUPS_REL] AA
  join dbo.SAM_ITEM BB ON AA.fitemid = BB.fitem_id
where AA.groupid=@groupid and BB.fuse_if=1";
            List<SqlParameter> paramlist = new List<SqlParameter>();
            paramlist.Add(new SqlParameter("@groupid", groupid));

            DataTable data = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql, paramlist.ToArray()).Tables[0];
            this.dataGridView2.DataSource = data;
        }

        private void btnAddGroup_Click(object sender, EventArgs e)
        {
            try
            {
                string instrid = cboInstr.SelectedValue.ToString();
                if(string.IsNullOrWhiteSpace(instrid))
                {
                    WWMessage.MessageShowError("请先选中仪器");
                    return;
                }

                Frm仪器项目组合Edit frm = new Frm仪器项目组合Edit(true, "", instrid);
                if(frm.ShowDialog()==DialogResult.OK)
                {
                    cboInstr_SelectedIndexChanged(null, null);
                }
            }
            catch(Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void btnEditGroup_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.Rows.Count == 0)
                {
                    WWMessage.MessageShowInformation("请先查询出需要修改的组合，然后再点击“修改组合”按钮。");
                    return;
                }
                string groupid = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                if (string.IsNullOrWhiteSpace(groupid))
                {
                    WWMessage.MessageShowError("组合ID是空的。");
                    return;
                }

                Frm仪器项目组合Edit frm = new Frm仪器项目组合Edit(true, groupid,"");
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    cboInstr_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void btnDelGroup_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (dataGridView1.Rows.Count == 0)
                {
                    WWMessage.MessageShowInformation("请先查询出需要删除的组合，然后再点击“删除组合”按钮。");
                    return;
                }
                string groupname = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                if (MessageBox.Show("删除后无法恢复,确定要删除【" + groupname + "】组合吗？确认删除请选择“是”，否则请点击“否”。", "确认",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
                != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }
                
                string groupid = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                string sql = "delete dbo.[SAM_INSTR_GROUPS] where groupid=@groupid; delete dbo.[SAM_INSTR_GROUPS_REL] where groupid=@groupid;";
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("@groupid", groupid));

                int ret = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql, paramlist.ToArray());
                if(ret > 0)
                {
                    WWMessage.MessageShowInformation("删除成功");
                    cboInstr_SelectedIndexChanged(null, null);
                }
                else
                {
                    WWMessage.MessageShowInformation("删除失败");
                }
            }
            catch(Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }

        }

        private void btnDelItem_Click(object sender, EventArgs e)
        {
            try
            {
                if(dataGridView2.Rows.Count == 0)
                {
                    WWMessage.MessageShowInformation("请先查询出需要删除的项目，然后再点击“删除项目”按钮。");
                    return;
                }
                string groupname = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                string itemname = dataGridView2.CurrentRow.Cells[4].Value.ToString();
                if (MessageBox.Show("删除后无法恢复,确定要删除【" + groupname + "】组合里的【"+itemname+"】项目吗？确认删除请选择“是”，否则请点击“否”。", "确认",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
                != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }

                string groupitemid = dataGridView2.CurrentRow.Cells[0].Value.ToString();
                string sql = "delete dbo.[SAM_INSTR_GROUPS_REL] where groupitemid=@groupitemid;";
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("@groupitemid", groupitemid));

                int ret = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql, paramlist.ToArray());
                if (ret > 0)
                {
                    WWMessage.MessageShowInformation("删除成功");
                    string groupid = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    ShowGroupItemsInfo(groupid);
                }
                else
                {
                    WWMessage.MessageShowInformation("删除失败");
                }
            }
            catch(Exception ex)
            {
                WWMessage.MessageShowError(ex.Message.ToString());
            }
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            try
            {
                if(dataGridView1.Rows.Count ==0)
                {
                    WWMessage.MessageShowInformation("请先查询出需要添加项目的组合，然后再点击“添加项目”按钮。");
                    return;
                }
                string instrid = cboInstr.SelectedValue.ToString();
                string groupid = dataGridView1.CurrentRow.Cells[0].Value.ToString();

                Frm仪器项目组合ItemEdit frm = new Frm仪器项目组合ItemEdit(groupid, instrid);
                if(frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ShowGroupItems();
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}
