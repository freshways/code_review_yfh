﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;
namespace yunLis.wwf
{
    public partial class UserdefinedQueryDemoForm : Form
    {
        /// <summary>
        /// 表ID
        /// </summary>
        private string strTable_id = "c9d865964653460381ae9e02e72b0082";
        /// <summary>
        /// 原始值 数据表
        /// </summary>
        private DataTable dtYSValue = null;
        /// <summary>
        /// 据表ID取出的列值
        /// </summary>
      //  private DataTable dtColumns = null;
        /// <summary>
        /// 据表ID取出的表
        /// </summary>
     //   private DataTable dtTable = null;
        /// <summary>
        /// 自定义业务规则
        /// </summary>
        private UserdefinedBLL bll = new UserdefinedBLL();
        public UserdefinedQueryDemoForm()
        {
            InitializeComponent();
        }

        private UserdefinedResult r;
        public UserdefinedQueryDemoForm(UserdefinedResult r)
            : this()
        {
            this.r = r;

        }

        private void butOK_Click(object sender, EventArgs e)
        {
            this.dtYSValue = this.bll.BllCreatYSDataTableBase(this.strTable_id);
            BllSetdtYSValue();
   
            r.ChangeTextString(this.strTable_id);
            r.ChangeTextDataTable(this.dtYSValue);
            this.Close();
        }
        /// <summary>
        /// 设置原始值
        /// </summary>
        public void BllSetdtYSValue()
        {
            
            for (int iv = 0; iv < 5; iv++)
            {
                DataRow drTT = dtYSValue.NewRow();
                for (int i = 0; i < this.dtYSValue.Columns.Count; i++)
                {
                    
                    string str = this.dtYSValue.Columns[i].ColumnName.ToString();
                    if (str == "fid")
                        drTT[str] = iv.ToString();
                    else
                        drTT[str] = iv + 2.1234567;
                  
                }
                this.dtYSValue.Rows.Add(drTT);
            }
           
            this.dataGridView1.DataSource = this.dtYSValue;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.dtYSValue = this.bll.BllCreatYSDataTableBase(this.strTable_id);
            BllSetdtYSValue();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.dtYSValue = this.bll.BllCreatYSDataTableBase("");
            BllSetdtYSValue();

            r.ChangeTextString("");
            r.ChangeTextDataTable(null);
            this.Close();
        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                this.wwTreeView1.ZADataTable = this.bll.BllTypeDT(2);
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "ftable_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        
        private void UserdefinedQueryDemoForm_Load(object sender, EventArgs e)
        {
            GetTree("0");
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.strTable_id = e.Node.Name.ToString();
            this.dtYSValue = this.bll.BllCreatYSDataTableBase(this.strTable_id);
            BllSetdtYSValue();
        }
        //--------------------

    }
}