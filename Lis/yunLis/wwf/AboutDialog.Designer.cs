namespace yunLis.wwf
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutDialog));
            this.buttonOK = new System.Windows.Forms.Button();
            this.Copyright = new System.Windows.Forms.Label();
            this.sysVar = new System.Windows.Forms.Label();
            this.CoTel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CoContact = new System.Windows.Forms.Label();
            this.CoHttp = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CoEmail = new System.Windows.Forms.Label();
            this.CoAdd = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.CoZC = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelSysName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonOK.Location = new System.Drawing.Point(331, 30);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "确认(&C)";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // Copyright
            // 
            this.Copyright.AutoSize = true;
            this.Copyright.BackColor = System.Drawing.Color.Transparent;
            this.Copyright.Location = new System.Drawing.Point(200, 22);
            this.Copyright.Name = "Copyright";
            this.Copyright.Size = new System.Drawing.Size(59, 12);
            this.Copyright.TabIndex = 9;
            this.Copyright.Text = "Copyright";
            // 
            // sysVar
            // 
            this.sysVar.AutoSize = true;
            this.sysVar.BackColor = System.Drawing.Color.Transparent;
            this.sysVar.Location = new System.Drawing.Point(13, 22);
            this.sysVar.Name = "sysVar";
            this.sysVar.Size = new System.Drawing.Size(41, 12);
            this.sysVar.TabIndex = 10;
            this.sysVar.Text = "sysVar";
            // 
            // CoTel
            // 
            this.CoTel.AutoSize = true;
            this.CoTel.BackColor = System.Drawing.Color.Transparent;
            this.CoTel.Location = new System.Drawing.Point(62, 60);
            this.CoTel.Name = "CoTel";
            this.CoTel.Size = new System.Drawing.Size(35, 12);
            this.CoTel.TabIndex = 11;
            this.CoTel.Text = "CoTel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(15, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 16;
            this.label1.Text = "联 系:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(15, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 15;
            this.label2.Text = "电 话:";
            // 
            // CoContact
            // 
            this.CoContact.AutoSize = true;
            this.CoContact.BackColor = System.Drawing.Color.Transparent;
            this.CoContact.Location = new System.Drawing.Point(62, 44);
            this.CoContact.Name = "CoContact";
            this.CoContact.Size = new System.Drawing.Size(59, 12);
            this.CoContact.TabIndex = 12;
            this.CoContact.Text = "CoContact";
            // 
            // CoHttp
            // 
            this.CoHttp.AutoSize = true;
            this.CoHttp.BackColor = System.Drawing.Color.Transparent;
            this.CoHttp.Location = new System.Drawing.Point(62, 124);
            this.CoHttp.Name = "CoHttp";
            this.CoHttp.Size = new System.Drawing.Size(41, 12);
            this.CoHttp.TabIndex = 20;
            this.CoHttp.TabStop = true;
            this.CoHttp.Text = "CoHttp";
            this.CoHttp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(15, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 21;
            this.label6.Text = "网 址:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(15, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 25;
            this.label7.Text = "邮 箱:";
            // 
            // CoEmail
            // 
            this.CoEmail.AutoSize = true;
            this.CoEmail.BackColor = System.Drawing.Color.Transparent;
            this.CoEmail.Location = new System.Drawing.Point(62, 76);
            this.CoEmail.Name = "CoEmail";
            this.CoEmail.Size = new System.Drawing.Size(47, 12);
            this.CoEmail.TabIndex = 26;
            this.CoEmail.Text = "CoEmail";
            // 
            // CoAdd
            // 
            this.CoAdd.AutoSize = true;
            this.CoAdd.BackColor = System.Drawing.Color.Transparent;
            this.CoAdd.Location = new System.Drawing.Point(62, 92);
            this.CoAdd.Name = "CoAdd";
            this.CoAdd.Size = new System.Drawing.Size(35, 12);
            this.CoAdd.TabIndex = 28;
            this.CoAdd.Text = "CoAdd";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(15, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 27;
            this.label9.Text = "地 址:";
            // 
            // CoZC
            // 
            this.CoZC.AutoSize = true;
            this.CoZC.BackColor = System.Drawing.Color.Transparent;
            this.CoZC.Location = new System.Drawing.Point(62, 108);
            this.CoZC.Name = "CoZC";
            this.CoZC.Size = new System.Drawing.Size(29, 12);
            this.CoZC.TabIndex = 30;
            this.CoZC.Text = "CoZC";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(15, 108);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 29;
            this.label10.Text = "邮 编:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(15, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(383, 24);
            this.label8.TabIndex = 31;
            this.label8.Text = "警告:本软件受版权法及国级公约保护，未经合法授权而擅自复制本软件\r\n的全部或部份，将承担严厉的法律责任。";
            // 
            // labelSysName
            // 
            this.labelSysName.AutoSize = true;
            this.labelSysName.BackColor = System.Drawing.Color.Transparent;
            this.labelSysName.Font = new System.Drawing.Font("黑体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelSysName.ForeColor = System.Drawing.SystemColors.Window;
            this.labelSysName.Location = new System.Drawing.Point(69, 19);
            this.labelSysName.Name = "labelSysName";
            this.labelSysName.Size = new System.Drawing.Size(90, 21);
            this.labelSysName.TabIndex = 0;
            this.labelSysName.Text = "XXX系统";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.labelSysName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(419, 72);
            this.panel1.TabIndex = 34;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.CoContact);
            this.panel3.Controls.Add(this.CoZC);
            this.panel3.Controls.Add(this.Copyright);
            this.panel3.Controls.Add(this.CoTel);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.sysVar);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.CoAdd);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.CoHttp);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.CoEmail);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 72);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(419, 150);
            this.panel3.TabIndex = 35;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.Controls.Add(this.buttonOK);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 222);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(419, 66);
            this.panel2.TabIndex = 36;
            // 
            // AboutDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.ClientSize = new System.Drawing.Size(419, 288);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "关于";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AboutDialog_Load);
            this.Click += new System.EventHandler(this.AboutDialog_Click);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label Copyright;
        private System.Windows.Forms.Label sysVar;
        private System.Windows.Forms.Label CoTel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label CoContact;
        private System.Windows.Forms.LinkLabel CoHttp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label CoEmail;
        private System.Windows.Forms.Label CoAdd;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label CoZC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelSysName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
    }
}