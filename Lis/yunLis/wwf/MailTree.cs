﻿/*
 * Proje	: 
 *
 * Hazırlayan   : Muhammed ŞAHİN
 * eMail        : muhammed.sahin@gmail.com
 *
 * Açıklama	:	
 *	
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;
namespace yunLis.wwf
{
    public partial class MailTree : UserControl
    {
        LoginBLL bll = new LoginBLL();//组织功能
        DataTable dtFunc = new DataTable();
        string strPID = "";

        public MailTree(string strid)
        {
            InitializeComponent();
            this.strPID = strid;
            try
            {
                dtFunc = bll.BllOrgFuncDT(LoginBLL.strOrgID); //装载数据集
                GetxPanderListTree(strPID);
                WWMessage.MessageShowWarning(dtFunc.Rows.Count.ToString());
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void treeMailBox_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                dtFunc = bll.BllOrgFuncDT(LoginBLL.strOrgID); //装载数据集
                GetxPanderListTree(strPID);
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

       /// <summary>
        /// 列出系统功能树
        /// </summary>
        public void GetxPanderListTree(string strp)
        {
            try
            {
                this.zaSuiteTreeView1.ZADataTable = dtFunc;//装载数据集
                this.zaSuiteTreeView1.ZATreeViewRootValue = strp;//树 RootId　根节点值
                this.zaSuiteTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.zaSuiteTreeView1.ZADisplayFieldName = "fname";//显示字段名称
                this.zaSuiteTreeView1.ZAKeyFieldName = "ffunc_id";//主键字段名称
                this.zaSuiteTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.zaSuiteTreeView1.SelectedNode = zaSuiteTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
    }
}
