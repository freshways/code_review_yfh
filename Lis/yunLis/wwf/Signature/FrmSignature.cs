﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.wwf.Signature
{
    public partial class FrmSignature : ww.form.wwf.SysBaseForm
    {
        public FrmSignature()
        {
            InitializeComponent();
        }

        OrgFuncUserBLL bll = new OrgFuncUserBLL();
        FuncBLL bllFunc = new FuncBLL();

        HisBll.Users hisuserbll = new HisBll.Users();

        string orgid = "";
        string strOKFuncID = "";

        private void FrmSignature_Load(object sender, EventArgs e)
        {
            GetTreeOrg("-1");
        }

        public void GetTreeOrg(string pid)
        {
            #region new code
            try
            {
                DataTable dt = hisuserbll.GetUserListInCommDept(LoginBLL.strDeptName_New);
                this.DataGridViewObject.DataSource = dt;
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError("获取人员列表时出现异常。异常信息：\r\n" + ex.ToString());
            }
            #endregion
        }

        //选中子节点 
        public void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                if (node.Checked)
                {
                    this.bll.BllOkFuncInsert(orgid, node.Name.ToString());
                }
                if (node.Nodes.Count > 0)
                {
                    this.CheckAllChildNodes(node, nodeChecked);
                   
                }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //orgid = this.treeView1.SelectedNode.Name;
            orgid = e.Node.Name;
            try
            {
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.DataGridViewObject.CurrentRow == null)
            {
                MessageBox.Show("没有选择任何行！");
                return;
            }

            string dlid = this.DataGridViewObject.CurrentRow.Cells["fperson_id"].Value.ToString();
            string username = this.DataGridViewObject.CurrentRow.Cells["fname"].Value.ToString();
            ww.form.sqz.FrmUploadSQZ frm = new sqz.FrmUploadSQZ(dlid, username);
            frm.ShowDialog();
        }
    }
}