﻿namespace yunLis.wwf
{
    partial class LoginForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelC = new System.Windows.Forms.Label();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SysLoginUserName = new System.Windows.Forms.TextBox();
            this.SysLoginPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SysLoginCancel = new System.Windows.Forms.Button();
            this.SysLoginLogin = new System.Windows.Forms.Button();
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.forg_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fdept_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fposition_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelSysName = new System.Windows.Forms.Label();
            this.bindingSourceObject = new System.Windows.Forms.BindingSource(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceObject)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.buttonUpdate);
            this.panel3.Controls.Add(this.labelC);
            this.panel3.Controls.Add(this.buttonHelp);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.SysLoginUserName);
            this.panel3.Controls.Add(this.SysLoginPassword);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.SysLoginCancel);
            this.panel3.Controls.Add(this.SysLoginLogin);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 72);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(389, 175);
            this.panel3.TabIndex = 16;
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.BackColor = System.Drawing.Color.Transparent;
            this.labelC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelC.ForeColor = System.Drawing.Color.Gray;
            this.labelC.Location = new System.Drawing.Point(271, 125);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(45, 20);
            this.labelC.TabIndex = 2;
            this.labelC.Text = "版本:";
            // 
            // buttonHelp
            // 
            this.buttonHelp.BackColor = System.Drawing.Color.Transparent;
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonHelp.ForeColor = System.Drawing.Color.Transparent;
            this.buttonHelp.Image = ((System.Drawing.Image)(resources.GetObject("buttonHelp.Image")));
            this.buttonHelp.Location = new System.Drawing.Point(332, 84);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(23, 20);
            this.buttonHelp.TabIndex = 8;
            this.buttonHelp.UseVisualStyleBackColor = false;
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("黑体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(129, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "LIS";
            // 
            // SysLoginUserName
            // 
            this.SysLoginUserName.Location = new System.Drawing.Point(132, 21);
            this.SysLoginUserName.Name = "SysLoginUserName";
            this.SysLoginUserName.Size = new System.Drawing.Size(200, 21);
            this.SysLoginUserName.TabIndex = 1;
            this.SysLoginUserName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SysLoginUserName_KeyDown);
            // 
            // SysLoginPassword
            // 
            this.SysLoginPassword.Location = new System.Drawing.Point(132, 50);
            this.SysLoginPassword.Name = "SysLoginPassword";
            this.SysLoginPassword.Size = new System.Drawing.Size(200, 21);
            this.SysLoginPassword.TabIndex = 2;
            this.SysLoginPassword.UseSystemPasswordChar = true;
            this.SysLoginPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SysLoginPassword_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(77, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "用户名:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(77, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "密  码:";
            // 
            // SysLoginCancel
            // 
            this.SysLoginCancel.BackColor = System.Drawing.Color.Transparent;
            this.SysLoginCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SysLoginCancel.Location = new System.Drawing.Point(236, 79);
            this.SysLoginCancel.Name = "SysLoginCancel";
            this.SysLoginCancel.Size = new System.Drawing.Size(80, 30);
            this.SysLoginCancel.TabIndex = 4;
            this.SysLoginCancel.Text = "取消(&C)";
            this.SysLoginCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SysLoginCancel.UseVisualStyleBackColor = false;
            this.SysLoginCancel.Click += new System.EventHandler(this.SysLoginCancel_Click);
            // 
            // SysLoginLogin
            // 
            this.SysLoginLogin.BackColor = System.Drawing.Color.Transparent;
            this.SysLoginLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SysLoginLogin.Location = new System.Drawing.Point(132, 79);
            this.SysLoginLogin.Name = "SysLoginLogin";
            this.SysLoginLogin.Size = new System.Drawing.Size(80, 30);
            this.SysLoginLogin.TabIndex = 3;
            this.SysLoginLogin.Text = "确定(&O)";
            this.SysLoginLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SysLoginLogin.UseVisualStyleBackColor = false;
            this.SysLoginLogin.Click += new System.EventHandler(this.SysLoginLogin_Click);
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.AllowUserToDeleteRows = false;
            this.DataGridViewObject.AllowUserToOrderColumns = true;
            this.DataGridViewObject.AllowUserToResizeRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.forg_id,
            this.fdept_name,
            this.fposition_name,
            this.fname});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(0, 247);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            this.DataGridViewObject.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowHeadersWidth = 20;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(389, 30);
            this.DataGridViewObject.TabIndex = 17;
            this.DataGridViewObject.Visible = false;
            this.DataGridViewObject.Click += new System.EventHandler(this.DataGridViewObject_Click);
            this.DataGridViewObject.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridViewObject_KeyDown);
            // 
            // forg_id
            // 
            this.forg_id.DataPropertyName = "forg_id";
            this.forg_id.HeaderText = "组织ID";
            this.forg_id.Name = "forg_id";
            this.forg_id.ReadOnly = true;
            this.forg_id.Visible = false;
            // 
            // fdept_name
            // 
            this.fdept_name.DataPropertyName = "fdept_name";
            this.fdept_name.HeaderText = "部门";
            this.fdept_name.Name = "fdept_name";
            this.fdept_name.ReadOnly = true;
            this.fdept_name.Width = 150;
            // 
            // fposition_name
            // 
            this.fposition_name.DataPropertyName = "fposition_name";
            this.fposition_name.HeaderText = "岗位";
            this.fposition_name.Name = "fposition_name";
            this.fposition_name.ReadOnly = true;
            this.fposition_name.Width = 120;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "姓名";
            this.fname.Name = "fname";
            this.fname.ReadOnly = true;
            this.fname.Width = 120;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.labelSysName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(389, 72);
            this.panel1.TabIndex = 15;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.Transparent;
            this.buttonUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdate.ForeColor = System.Drawing.Color.Transparent;
            this.buttonUpdate.Image = global::yunLis.Properties.Resources.refresh;
            this.buttonUpdate.Location = new System.Drawing.Point(359, 84);
            this.buttonUpdate.Margin = new System.Windows.Forms.Padding(1);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(23, 20);
            this.buttonUpdate.TabIndex = 1;
            this.buttonUpdate.UseVisualStyleBackColor = false;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelSysName
            // 
            this.labelSysName.AutoSize = true;
            this.labelSysName.BackColor = System.Drawing.Color.Transparent;
            this.labelSysName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelSysName.Font = new System.Drawing.Font("黑体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelSysName.ForeColor = System.Drawing.Color.White;
            this.labelSysName.Location = new System.Drawing.Point(66, 22);
            this.labelSysName.Name = "labelSysName";
            this.labelSysName.Size = new System.Drawing.Size(93, 19);
            this.labelSysName.TabIndex = 0;
            this.labelSysName.Text = "产品名称";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(389, 277);
            this.ControlBox = false;
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "登录";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginForm_KeyDown);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceObject)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SysLoginCancel;
        private System.Windows.Forms.Button SysLoginLogin;
        private System.Windows.Forms.TextBox SysLoginUserName;
        private System.Windows.Forms.TextBox SysLoginPassword;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelSysName;
        private System.Windows.Forms.DataGridView DataGridViewObject;
        private System.Windows.Forms.BindingSource bindingSourceObject;
        private System.Windows.Forms.DataGridViewTextBoxColumn forg_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fdept_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fposition_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonHelp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Button buttonUpdate;
    }
}