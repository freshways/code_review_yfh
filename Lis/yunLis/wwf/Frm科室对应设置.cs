﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ww.form.wwf
{
    public partial class Frm科室对应设置 : Form
    {
        bool m_isAdd = false;
        string m_id = string.Empty;
        public Frm科室对应设置(bool isAdd, string id)
        {
            InitializeComponent();
            m_isAdd = isAdd;
            m_id = id;
        }

        private void Frm科室对应设置_Load(object sender, EventArgs e)
        {
            //绑定数据源
            BindSourceForDept();

            //显示旧信息
            if(m_isAdd)
            { }
            else
            {
                sleSourceDept.Properties.ReadOnly = true;
                cboType.Enabled = false;
                GetInfoByID();
            }
        }

        void BindSourceForDept()
        {
            try
            {
                string sql = @"SELECT  AA.[科室编码], AA.[科室名称],bb.分院名称 所属分院
  FROM [dbo].[GY科室设置] AA
	left outer join dbo.GY分院字典 BB ON AA.分院编码=BB.分院编码
  where 是否禁用=0 and 科室编码 < 8000";
                DataTable data = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, sql).Tables[0];

                sleSourceDept.Properties.ValueMember = "科室编码";
                sleSourceDept.Properties.DisplayMember = "科室名称";

                sleDestDept.Properties.ValueMember = "科室编码";
                sleDestDept.Properties.DisplayMember = "科室名称";

                sleDestDept.Properties.DataSource = data;
                sleSourceDept.Properties.DataSource = data;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void GetInfoByID()
        {
            try
            {
                string sql = "select * from dbo.GY科室转换关系 where ID=@id";
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("@id", m_id));
                DataTable data = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, sql, paramlist.ToArray()).Tables[0];
                if(data.Rows.Count > 0)
                {
                    cboType.Text = data.Rows[0]["类型"].ToString();
                    sleSourceDept.EditValue = Convert.ToInt32(data.Rows[0]["源科室"].ToString());
                    sleDestDept.EditValue = Convert.ToInt32(data.Rows[0]["目标科室"].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string type = cboType.Text;
            object sourceDept = sleSourceDept.EditValue;
            object destDept = sleDestDept.EditValue;

            if(string.IsNullOrWhiteSpace(type) 
                || sourceDept ==null || destDept == null 
                || string.IsNullOrWhiteSpace(sourceDept.ToString())
                || string.IsNullOrWhiteSpace(destDept.ToString()))
            {
                MessageBox.Show("请检查信息是否填写完整。");
                return;
            }

            string sql = string.Empty;
            List<SqlParameter> paramlist = new List<SqlParameter>();
            if(m_isAdd)
            {
                sql = @"MERGE [dbo].[GY科室转换关系] AS Target
    USING (SELECT @type, @source,@dest) AS source([类型],[源科室],[目标科室])
    ON (Target.[源科室] = source.[源科室] and Target.类型=source.[类型])
	WHEN NOT MATCHED  THEN	
	    insert([类型],[源科室],[目标科室]) 
		values(source.[类型],source.[源科室],source.[目标科室]);";
                paramlist.Add(new SqlParameter("@type", type));
                paramlist.Add(new SqlParameter("@source", sourceDept.ToString()));
                paramlist.Add(new SqlParameter("@dest", destDept.ToString()));
            }
            else
            {
                sql = "update dbo.GY科室转换关系 set 目标科室=@dest where ID=@id";
                paramlist.Add(new SqlParameter("@id", m_id));
                paramlist.Add(new SqlParameter("@dest", destDept.ToString()));
            }

            int ret = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strCHisDBConn, sql, paramlist.ToArray());
            if (ret > 0)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                MessageBox.Show("编辑失败,请确认需转换的科室是否已经做了设置。");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
