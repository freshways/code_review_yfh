﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;
using System.Collections;
namespace ww.form.wwf
{
    public partial class DeptForm : SysBaseForm
    {
        DeptBll bll = new DeptBll();
        DataTable dtFunc = null;
        string strFuncID = "";//上级ID

        DataTable dt部门 = new DataTable();

        public DeptForm()
        {
            InitializeComponent();
        }

        private void DeptForm_Load(object sender, EventArgs e)
        {
            GetTree("-1");
        }
        /// <summary>
        /// 列出系统部门树
        /// </summary>
        public void GetTree(string pid)
        {
            try
            {
                dt部门 = this.bll.BllDeptDT();
                this.wwTreeView1.ZADataTable = dt部门;
                this.wwTreeView1.ZATreeViewRootValue = pid;//树 RootId　根节点值
                this.wwTreeView1.ZAParentFieldName = "fp_id";//上级字段名称
                this.wwTreeView1.ZADisplayFieldName = "fname_show";//显示字段名称
                this.wwTreeView1.ZAKeyFieldName = "fdept_id";//主键字段名称
                this.wwTreeView1.ZATreeViewShow();//显示树        
                try
                {
                    this.wwTreeView1.SelectedNode = wwTreeView1.Nodes[0].Nodes[0];
                }
                catch { }

            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void wwTreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                strFuncID = e.Node.Name.ToString();
                GetFuncDec();
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void GetFuncDec()
        {
            dtFunc = this.bll.BllDeptDTByID(strFuncID);
            wwf_deptBindingSource.DataSource = dtFunc;
        }



        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            if (this.strFuncID == "" || this.strFuncID == null)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowWarning("请选择上级部门后重试！");
            }
            else
            {
                if (dtFunc != null)
                    dtFunc.Clear();
                DataRow dr = null;
                dr = dtFunc.NewRow();
                string guid = this.bll.DbGuid();
                dr["fdept_id"] = guid;
                dr["fp_id"] = this.strFuncID;
                dr["fname"] = "";
                dr["forder_by"] = "0";
                dr["fuse_flag"] = "1";
                dtFunc.Rows.Add(dr);

                this.wwf_deptBindingSource.DataSource = dtFunc;
                toolStripButtonS.Enabled = true;
            }
        }

        private void toolStripButtonS_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.wwf_deptBindingSource.EndEdit();
                if (strFuncID == "" || strFuncID == null)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("未选择可修改的部门！");
                    return;
                }
                if (this.dtFunc != null)
                {
                    if (this.bll.BllAdd(this.dtFunc) > 0)
                    {
                        GetFuncDec();
                        GetTree("-1");
                    }
                    toolStripButtonS.Enabled = false;
                }
            }
            catch
            {

                ww.wwf.wwfbll.WWMessage.MessageShowError("请新增部门后再保存！");
            }
        }

        private void toolStripButtonR_Click(object sender, EventArgs e)
        {
            GetTree("-1");
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.wwf_deptBindingSource.EndEdit();
                if (strFuncID == "" || strFuncID == null)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("未选择可修改的部门！");
                    return;
                }
                if (this.dtFunc != null)
                {
                    if (this.bll.BllUpdate(this.dtFunc, strFuncID) > 0)
                    {
                        GetFuncDec();
                        GetTree("-1");
                    }
                    else
                    {
                        WWMessage.MessageShowWarning("修改失败，可能是编码重复，请新录入编码后重试。");
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.strFuncID == "" || this.strFuncID == null)
                {
                    ww.wwf.wwfbll.WWMessage.MessageShowWarning("请选择要删除的部门后重试！");
                }
                else
                {
                    if (strFuncID == "kfz" || strFuncID == "jyk")//开发组 检验科
                    {
                        ww.wwf.wwfbll.WWMessage.MessageShowWarning("此部门不可删除！");
                    }
                    else
                    {
                        if (this.bll.BllChildExists(this.strFuncID))
                        {
                            ww.wwf.wwfbll.WWMessage.MessageShowWarning("此部门存在下级，请先删除其下级后重试！");
                        }
                        else
                        {
                            if (ww.wwf.wwfbll.WWMessage.MessageDialogResult("确认要删除此部门？部门删除后将不可恢复！"))
                            {
                                if (this.bll.BllDelete(this.strFuncID) > 0)
                                {
                                    GetFuncDec();
                                    GetTree("-1");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void toolStripButtonhelp_code_Click(object sender, EventArgs e)
        {
            try
            {
                string sqlHelp = "";
                IList lissql = new ArrayList();

                for (int i = 0; i < dt部门.Rows.Count; i++)
                {
                    sqlHelp = "";
                    try
                    {
                        sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(dt部门.Rows[i]["fname"].ToString().Trim());
                    }
                    catch { }

                    lissql.Add("UPDATE WWF_DEPT SET fremark = '" + sqlHelp + "'  WHERE (fdept_id = '" + dt部门.Rows[i]["fdept_id"].ToString() + "')");
                }
                if (lissql.Count > 0)
                {
                    string strRR = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbTransaction(ww.wwf.wwfbll.WWFInit.strDBConn, lissql);
                    if (strRR.Equals("true"))
                    {
                        MessageBox.Show("生成拼音码成功。");
                    }
                    GetTree("-1");
                }
               
            }
            catch (Exception ex)
            {
                ww.wwf.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
    }
}
/*
 * 
 *  try
            {
                Cursor = Cursors.WaitCursor;
                this.Validate();
                this.bindingSourceObject.EndEdit();
                this.rulesObject.ZADataSetUpdate(this.m_dsObject);

            }
            catch (Exception ex)
            {
                this.zaSaveLog("", ex.ToString());
            }
            finally
            {
                LoadDataSet();
                Cursor = Cursors.Arrow;

            }
   /// <summary>
        /// 新增一行 
       /// </summary>
       /// <param name="ds"></param>
       /// <param name="strKey"></param>    
        public void zaAddRow(DataSet ds, string strKey)
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt = ds.Tables[0];
            dr = dt.NewRow();
            dr[strKey] = zaStrGUID();
            dt.Rows.Add(dr);
        }
 */