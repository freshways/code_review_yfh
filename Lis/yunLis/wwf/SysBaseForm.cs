using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace yunLis.wwf
{
    public partial class SysBaseForm : Form
    {
        public SysBaseForm()
        {
            InitializeComponent();
        }

        private void SysBaseForm_Load(object sender, EventArgs e)
        {

        }

        private void option1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (MainForm.dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
                    ActiveMdiChild.Close();
                else if (MainForm.dockPanel.ActiveDocument != null)
                    MainForm.dockPanel.ActiveDocument.DockHandler.Close();
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

      
      
    }
}