﻿namespace ww.form.wwf
{
    partial class PersonPassForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fperson_idLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fname_eLabel;
            System.Windows.Forms.Label fpassLabel;
            System.Windows.Forms.Label forder_byLabel;
            System.Windows.Forms.Label fuse_flagLabel;
            System.Windows.Forms.Label fremarkLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonPassForm));
            this.wwfDataSet = new wwfDataSet();
            this.wwf_personBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fperson_idTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fname_eTextBox = new System.Windows.Forms.TextBox();
            this.fpassTextBox = new System.Windows.Forms.TextBox();
            this.forder_byTextBox = new System.Windows.Forms.TextBox();
            this.fuse_flagCheckBox = new System.Windows.Forms.CheckBox();
            this.fremarkTextBox = new System.Windows.Forms.TextBox();
            this.panelTool = new System.Windows.Forms.Panel();
            this.butSave = new System.Windows.Forms.Button();
            this.butReturn = new System.Windows.Forms.Button();
            fperson_idLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fname_eLabel = new System.Windows.Forms.Label();
            fpassLabel = new System.Windows.Forms.Label();
            forder_byLabel = new System.Windows.Forms.Label();
            fuse_flagLabel = new System.Windows.Forms.Label();
            fremarkLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_personBindingSource)).BeginInit();
            this.panelTool.SuspendLayout();
            this.SuspendLayout();
            // 
            // fperson_idLabel
            // 
            fperson_idLabel.AutoSize = true;
            fperson_idLabel.Location = new System.Drawing.Point(19, 16);
            fperson_idLabel.Name = "fperson_idLabel";
            fperson_idLabel.Size = new System.Drawing.Size(47, 12);
            fperson_idLabel.TabIndex = 1;
            fperson_idLabel.Text = "登录名:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(31, 74);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(35, 12);
            fnameLabel.TabIndex = 4;
            fnameLabel.Text = "姓名:";
            // 
            // fname_eLabel
            // 
            fname_eLabel.AutoSize = true;
            fname_eLabel.Location = new System.Drawing.Point(19, 103);
            fname_eLabel.Name = "fname_eLabel";
            fname_eLabel.Size = new System.Drawing.Size(47, 12);
            fname_eLabel.TabIndex = 6;
            fname_eLabel.Text = "英文名:";
            // 
            // fpassLabel
            // 
            fpassLabel.AutoSize = true;
            fpassLabel.Location = new System.Drawing.Point(31, 45);
            fpassLabel.Name = "fpassLabel";
            fpassLabel.Size = new System.Drawing.Size(35, 12);
            fpassLabel.TabIndex = 8;
            fpassLabel.Text = "密码:";
            // 
            // forder_byLabel
            // 
            forder_byLabel.AutoSize = true;
            forder_byLabel.Location = new System.Drawing.Point(31, 132);
            forder_byLabel.Name = "forder_byLabel";
            forder_byLabel.Size = new System.Drawing.Size(35, 12);
            forder_byLabel.TabIndex = 10;
            forder_byLabel.Text = "排序:";
            // 
            // fuse_flagLabel
            // 
            fuse_flagLabel.AutoSize = true;
            fuse_flagLabel.Location = new System.Drawing.Point(31, 161);
            fuse_flagLabel.Name = "fuse_flagLabel";
            fuse_flagLabel.Size = new System.Drawing.Size(35, 12);
            fuse_flagLabel.TabIndex = 12;
            fuse_flagLabel.Text = "启用:";
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(31, 190);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(35, 12);
            fremarkLabel.TabIndex = 14;
            fremarkLabel.Text = "备注:";
            // 
            // wwfDataSet
            // 
            this.wwfDataSet.DataSetName = "wwfDataSet";
            this.wwfDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // wwf_personBindingSource
            // 
            this.wwf_personBindingSource.DataMember = "wwf_person";
            this.wwf_personBindingSource.DataSource = this.wwfDataSet;
            // 
            // fperson_idTextBox
            // 
            this.fperson_idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fperson_id", true));
            this.fperson_idTextBox.Location = new System.Drawing.Point(72, 12);
            this.fperson_idTextBox.Name = "fperson_idTextBox";
            this.fperson_idTextBox.ReadOnly = true;
            this.fperson_idTextBox.Size = new System.Drawing.Size(242, 21);
            this.fperson_idTextBox.TabIndex = 1;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fname", true));
            this.fnameTextBox.Location = new System.Drawing.Point(72, 70);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.ReadOnly = true;
            this.fnameTextBox.Size = new System.Drawing.Size(242, 21);
            this.fnameTextBox.TabIndex = 3;
            // 
            // fname_eTextBox
            // 
            this.fname_eTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fname_e", true));
            this.fname_eTextBox.Location = new System.Drawing.Point(72, 99);
            this.fname_eTextBox.Name = "fname_eTextBox";
            this.fname_eTextBox.ReadOnly = true;
            this.fname_eTextBox.Size = new System.Drawing.Size(242, 21);
            this.fname_eTextBox.TabIndex = 4;
            // 
            // fpassTextBox
            // 
            this.fpassTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fpass", true));
            this.fpassTextBox.Location = new System.Drawing.Point(72, 41);
            this.fpassTextBox.Name = "fpassTextBox";
            this.fpassTextBox.Size = new System.Drawing.Size(242, 21);
            this.fpassTextBox.TabIndex = 2;
            this.fpassTextBox.UseSystemPasswordChar = true;
            // 
            // forder_byTextBox
            // 
            this.forder_byTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "forder_by", true));
            this.forder_byTextBox.Location = new System.Drawing.Point(72, 128);
            this.forder_byTextBox.Name = "forder_byTextBox";
            this.forder_byTextBox.ReadOnly = true;
            this.forder_byTextBox.Size = new System.Drawing.Size(242, 21);
            this.forder_byTextBox.TabIndex = 5;
            this.forder_byTextBox.Text = "0";
            // 
            // fuse_flagCheckBox
            // 
            this.fuse_flagCheckBox.Checked = true;
            this.fuse_flagCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fuse_flagCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.wwf_personBindingSource, "fuse_flag", true));
            this.fuse_flagCheckBox.Enabled = false;
            this.fuse_flagCheckBox.Location = new System.Drawing.Point(72, 155);
            this.fuse_flagCheckBox.Name = "fuse_flagCheckBox";
            this.fuse_flagCheckBox.Size = new System.Drawing.Size(246, 24);
            this.fuse_flagCheckBox.TabIndex = 6;
            // 
            // fremarkTextBox
            // 
            this.fremarkTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fremark", true));
            this.fremarkTextBox.Location = new System.Drawing.Point(72, 187);
            this.fremarkTextBox.Multiline = true;
            this.fremarkTextBox.Name = "fremarkTextBox";
            this.fremarkTextBox.ReadOnly = true;
            this.fremarkTextBox.Size = new System.Drawing.Size(242, 63);
            this.fremarkTextBox.TabIndex = 7;
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.butSave);
            this.panelTool.Controls.Add(this.butReturn);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTool.Location = new System.Drawing.Point(0, 256);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(340, 46);
            this.panelTool.TabIndex = 116;
            // 
            // butSave
            // 
            this.butSave.Image = ((System.Drawing.Image)(resources.GetObject("butSave.Image")));
            this.butSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.Location = new System.Drawing.Point(72, 6);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(80, 23);
            this.butSave.TabIndex = 8;
            this.butSave.Text = "   保存(&S)";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butReturn
            // 
            this.butReturn.Image = ((System.Drawing.Image)(resources.GetObject("butReturn.Image")));
            this.butReturn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butReturn.Location = new System.Drawing.Point(178, 6);
            this.butReturn.Name = "butReturn";
            this.butReturn.Size = new System.Drawing.Size(80, 23);
            this.butReturn.TabIndex = 9;
            this.butReturn.Text = "   返回(&R)";
            this.butReturn.UseVisualStyleBackColor = true;
            this.butReturn.Click += new System.EventHandler(this.butReturn_Click);
            // 
            // PersonPassForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 302);
            this.Controls.Add(this.panelTool);
            this.Controls.Add(fremarkLabel);
            this.Controls.Add(this.fremarkTextBox);
            this.Controls.Add(fuse_flagLabel);
            this.Controls.Add(this.fuse_flagCheckBox);
            this.Controls.Add(forder_byLabel);
            this.Controls.Add(this.forder_byTextBox);
            this.Controls.Add(fpassLabel);
            this.Controls.Add(this.fpassTextBox);
            this.Controls.Add(fname_eLabel);
            this.Controls.Add(this.fname_eTextBox);
            this.Controls.Add(fnameLabel);
            this.Controls.Add(this.fnameTextBox);
            this.Controls.Add(fperson_idLabel);
            this.Controls.Add(this.fperson_idTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PersonPassForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "密码修改";
            this.Load += new System.EventHandler(this.PersonPassForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_personBindingSource)).EndInit();
            this.panelTool.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ww.form.wwf.wwfDataSet wwfDataSet;
        private System.Windows.Forms.BindingSource wwf_personBindingSource;
        private System.Windows.Forms.TextBox fperson_idTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fname_eTextBox;
        private System.Windows.Forms.TextBox fpassTextBox;
        private System.Windows.Forms.TextBox forder_byTextBox;
        private System.Windows.Forms.CheckBox fuse_flagCheckBox;
        private System.Windows.Forms.TextBox fremarkTextBox;
        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butReturn;
    }
}