﻿namespace yunLis.wwf
{
    partial class PersonAddForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fperson_idLabel;
            System.Windows.Forms.Label fdept_idLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label fname_eLabel;
            System.Windows.Forms.Label fpassLabel;
            System.Windows.Forms.Label forder_byLabel;
            System.Windows.Forms.Label fuse_flagLabel;
            System.Windows.Forms.Label fremarkLabel;
            System.Windows.Forms.Label ftypeLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonAddForm));
            this.fperson_idTextBox = new System.Windows.Forms.TextBox();
            this.wwf_personBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wwfDataSet = new yunLis.wwf.wwfDataSet();
            this.fdept_idTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.fname_eTextBox = new System.Windows.Forms.TextBox();
            this.fpassTextBox = new System.Windows.Forms.TextBox();
            this.forder_byTextBox = new System.Windows.Forms.TextBox();
            this.fuse_flagCheckBox = new System.Windows.Forms.CheckBox();
            this.fremarkTextBox = new System.Windows.Forms.TextBox();
            this.panelTool = new System.Windows.Forms.Panel();
            this.butSave = new System.Windows.Forms.Button();
            this.butReturn = new System.Windows.Forms.Button();
            this.ftypeTextBox = new System.Windows.Forms.TextBox();
            fperson_idLabel = new System.Windows.Forms.Label();
            fdept_idLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            fname_eLabel = new System.Windows.Forms.Label();
            fpassLabel = new System.Windows.Forms.Label();
            forder_byLabel = new System.Windows.Forms.Label();
            fuse_flagLabel = new System.Windows.Forms.Label();
            fremarkLabel = new System.Windows.Forms.Label();
            ftypeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_personBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).BeginInit();
            this.panelTool.SuspendLayout();
            this.SuspendLayout();
            // 
            // fperson_idLabel
            // 
            fperson_idLabel.AutoSize = true;
            fperson_idLabel.Location = new System.Drawing.Point(19, 38);
            fperson_idLabel.Name = "fperson_idLabel";
            fperson_idLabel.Size = new System.Drawing.Size(47, 12);
            fperson_idLabel.TabIndex = 1;
            fperson_idLabel.Text = "登录名:";
            // 
            // fdept_idLabel
            // 
            fdept_idLabel.AutoSize = true;
            fdept_idLabel.Location = new System.Drawing.Point(31, 9);
            fdept_idLabel.Name = "fdept_idLabel";
            fdept_idLabel.Size = new System.Drawing.Size(35, 12);
            fdept_idLabel.TabIndex = 2;
            fdept_idLabel.Text = "部门:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(31, 96);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(35, 12);
            fnameLabel.TabIndex = 4;
            fnameLabel.Text = "姓名:";
            // 
            // fname_eLabel
            // 
            fname_eLabel.AutoSize = true;
            fname_eLabel.Location = new System.Drawing.Point(19, 125);
            fname_eLabel.Name = "fname_eLabel";
            fname_eLabel.Size = new System.Drawing.Size(47, 12);
            fname_eLabel.TabIndex = 6;
            fname_eLabel.Text = "英文名:";
            // 
            // fpassLabel
            // 
            fpassLabel.AutoSize = true;
            fpassLabel.Location = new System.Drawing.Point(31, 67);
            fpassLabel.Name = "fpassLabel";
            fpassLabel.Size = new System.Drawing.Size(35, 12);
            fpassLabel.TabIndex = 8;
            fpassLabel.Text = "密码:";
            // 
            // forder_byLabel
            // 
            forder_byLabel.AutoSize = true;
            forder_byLabel.Location = new System.Drawing.Point(31, 154);
            forder_byLabel.Name = "forder_byLabel";
            forder_byLabel.Size = new System.Drawing.Size(35, 12);
            forder_byLabel.TabIndex = 10;
            forder_byLabel.Text = "排序:";
            // 
            // fuse_flagLabel
            // 
            fuse_flagLabel.AutoSize = true;
            fuse_flagLabel.Location = new System.Drawing.Point(31, 183);
            fuse_flagLabel.Name = "fuse_flagLabel";
            fuse_flagLabel.Size = new System.Drawing.Size(35, 12);
            fuse_flagLabel.TabIndex = 12;
            fuse_flagLabel.Text = "启用:";
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(31, 240);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(35, 12);
            fremarkLabel.TabIndex = 14;
            fremarkLabel.Text = "备注:";
            // 
            // ftypeLabel
            // 
            ftypeLabel.AutoSize = true;
            ftypeLabel.Location = new System.Drawing.Point(31, 210);
            ftypeLabel.Name = "ftypeLabel";
            ftypeLabel.Size = new System.Drawing.Size(35, 12);
            ftypeLabel.TabIndex = 116;
            ftypeLabel.Text = "类型:";
            // 
            // fperson_idTextBox
            // 
            this.fperson_idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fperson_id", true));
            this.fperson_idTextBox.Location = new System.Drawing.Point(72, 34);
            this.fperson_idTextBox.Name = "fperson_idTextBox";
            this.fperson_idTextBox.Size = new System.Drawing.Size(242, 21);
            this.fperson_idTextBox.TabIndex = 1;
            // 
            // wwf_personBindingSource
            // 
            this.wwf_personBindingSource.DataMember = "wwf_person";
            this.wwf_personBindingSource.DataSource = this.wwfDataSet;
            // 
            // wwfDataSet
            // 
            this.wwfDataSet.DataSetName = "wwfDataSet";
            this.wwfDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fdept_idTextBox
            // 
            this.fdept_idTextBox.Location = new System.Drawing.Point(72, 5);
            this.fdept_idTextBox.Name = "fdept_idTextBox";
            this.fdept_idTextBox.ReadOnly = true;
            this.fdept_idTextBox.Size = new System.Drawing.Size(242, 21);
            this.fdept_idTextBox.TabIndex = 0;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fname", true));
            this.fnameTextBox.Location = new System.Drawing.Point(72, 92);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(242, 21);
            this.fnameTextBox.TabIndex = 3;
            // 
            // fname_eTextBox
            // 
            this.fname_eTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fname_e", true));
            this.fname_eTextBox.Location = new System.Drawing.Point(72, 121);
            this.fname_eTextBox.Name = "fname_eTextBox";
            this.fname_eTextBox.Size = new System.Drawing.Size(242, 21);
            this.fname_eTextBox.TabIndex = 4;
            // 
            // fpassTextBox
            // 
            this.fpassTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fpass", true));
            this.fpassTextBox.Location = new System.Drawing.Point(72, 63);
            this.fpassTextBox.Name = "fpassTextBox";
            this.fpassTextBox.Size = new System.Drawing.Size(242, 21);
            this.fpassTextBox.TabIndex = 2;
            this.fpassTextBox.UseSystemPasswordChar = true;
            // 
            // forder_byTextBox
            // 
            this.forder_byTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "forder_by", true));
            this.forder_byTextBox.Location = new System.Drawing.Point(72, 150);
            this.forder_byTextBox.Name = "forder_byTextBox";
            this.forder_byTextBox.Size = new System.Drawing.Size(242, 21);
            this.forder_byTextBox.TabIndex = 5;
            this.forder_byTextBox.Text = "0";
            // 
            // fuse_flagCheckBox
            // 
            this.fuse_flagCheckBox.Checked = true;
            this.fuse_flagCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fuse_flagCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.wwf_personBindingSource, "fuse_flag", true));
            this.fuse_flagCheckBox.Location = new System.Drawing.Point(72, 177);
            this.fuse_flagCheckBox.Name = "fuse_flagCheckBox";
            this.fuse_flagCheckBox.Size = new System.Drawing.Size(246, 24);
            this.fuse_flagCheckBox.TabIndex = 6;
            // 
            // fremarkTextBox
            // 
            this.fremarkTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "fremark", true));
            this.fremarkTextBox.Location = new System.Drawing.Point(72, 237);
            this.fremarkTextBox.Multiline = true;
            this.fremarkTextBox.Name = "fremarkTextBox";
            this.fremarkTextBox.Size = new System.Drawing.Size(242, 63);
            this.fremarkTextBox.TabIndex = 7;
            // 
            // panelTool
            // 
            this.panelTool.Controls.Add(this.butSave);
            this.panelTool.Controls.Add(this.butReturn);
            this.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTool.Location = new System.Drawing.Point(0, 320);
            this.panelTool.Name = "panelTool";
            this.panelTool.Size = new System.Drawing.Size(340, 46);
            this.panelTool.TabIndex = 116;
            // 
            // butSave
            // 
            this.butSave.Image = ((System.Drawing.Image)(resources.GetObject("butSave.Image")));
            this.butSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butSave.Location = new System.Drawing.Point(72, 6);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(80, 23);
            this.butSave.TabIndex = 8;
            this.butSave.Text = "   保存(&S)";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butReturn
            // 
            this.butReturn.Image = ((System.Drawing.Image)(resources.GetObject("butReturn.Image")));
            this.butReturn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butReturn.Location = new System.Drawing.Point(178, 6);
            this.butReturn.Name = "butReturn";
            this.butReturn.Size = new System.Drawing.Size(80, 23);
            this.butReturn.TabIndex = 9;
            this.butReturn.Text = "   返回(&R)";
            this.butReturn.UseVisualStyleBackColor = true;
            this.butReturn.Click += new System.EventHandler(this.butReturn_Click);
            // 
            // ftypeTextBox
            // 
            this.ftypeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_personBindingSource, "ftype", true));
            this.ftypeTextBox.Location = new System.Drawing.Point(72, 207);
            this.ftypeTextBox.Name = "ftypeTextBox";
            this.ftypeTextBox.Size = new System.Drawing.Size(242, 21);
            this.ftypeTextBox.TabIndex = 117;
            // 
            // PersonAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 366);
            this.Controls.Add(ftypeLabel);
            this.Controls.Add(this.ftypeTextBox);
            this.Controls.Add(this.panelTool);
            this.Controls.Add(fremarkLabel);
            this.Controls.Add(this.fremarkTextBox);
            this.Controls.Add(fuse_flagLabel);
            this.Controls.Add(this.fuse_flagCheckBox);
            this.Controls.Add(forder_byLabel);
            this.Controls.Add(this.forder_byTextBox);
            this.Controls.Add(fpassLabel);
            this.Controls.Add(this.fpassTextBox);
            this.Controls.Add(fname_eLabel);
            this.Controls.Add(this.fname_eTextBox);
            this.Controls.Add(fnameLabel);
            this.Controls.Add(this.fnameTextBox);
            this.Controls.Add(fdept_idLabel);
            this.Controls.Add(this.fdept_idTextBox);
            this.Controls.Add(fperson_idLabel);
            this.Controls.Add(this.fperson_idTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PersonAddForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "用户";
            this.Load += new System.EventHandler(this.PersonAddForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wwf_personBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).EndInit();
            this.panelTool.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private yunLis.wwf.wwfDataSet wwfDataSet;
        private System.Windows.Forms.BindingSource wwf_personBindingSource;
        private System.Windows.Forms.TextBox fperson_idTextBox;
        private System.Windows.Forms.TextBox fdept_idTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox fname_eTextBox;
        private System.Windows.Forms.TextBox fpassTextBox;
        private System.Windows.Forms.TextBox forder_byTextBox;
        private System.Windows.Forms.CheckBox fuse_flagCheckBox;
        private System.Windows.Forms.TextBox fremarkTextBox;
        private System.Windows.Forms.Panel panelTool;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butReturn;
        private System.Windows.Forms.TextBox ftypeTextBox;
    }
}