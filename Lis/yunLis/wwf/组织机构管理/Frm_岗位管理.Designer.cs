﻿namespace yunLis.wwf
{
    partial class Frm_岗位管理
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label fposition_idLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label forder_byLabel;
            System.Windows.Forms.Label fremarkLabel;
            System.Windows.Forms.Label fuse_flagLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_岗位管理));
            this.DataGridViewObject = new System.Windows.Forms.DataGridView();
            this.fposition_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fuse_flag = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.forder_by = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fremark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wwf_positionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wwfDataSet = new yunLis.wwf.wwfDataSet();
            this.fposition_idTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.forder_byTextBox = new System.Windows.Forms.TextBox();
            this.fremarkTextBox = new System.Windows.Forms.TextBox();
            this.fuse_flagCheckBox = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bN = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonS = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonR = new System.Windows.Forms.ToolStripButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            fposition_idLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            forder_byLabel = new System.Windows.Forms.Label();
            fremarkLabel = new System.Windows.Forms.Label();
            fuse_flagLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_positionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).BeginInit();
            this.bN.SuspendLayout();
            this.SuspendLayout();
            // 
            // fposition_idLabel
            // 
            fposition_idLabel.AutoSize = true;
            fposition_idLabel.Location = new System.Drawing.Point(13, 17);
            fposition_idLabel.Name = "fposition_idLabel";
            fposition_idLabel.Size = new System.Drawing.Size(35, 12);
            fposition_idLabel.TabIndex = 118;
            fposition_idLabel.Text = "编号:";
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Location = new System.Drawing.Point(13, 44);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(35, 12);
            fnameLabel.TabIndex = 119;
            fnameLabel.Text = "名称:";
            // 
            // forder_byLabel
            // 
            forder_byLabel.AutoSize = true;
            forder_byLabel.Location = new System.Drawing.Point(13, 71);
            forder_byLabel.Name = "forder_byLabel";
            forder_byLabel.Size = new System.Drawing.Size(35, 12);
            forder_byLabel.TabIndex = 120;
            forder_byLabel.Text = "排序:";
            // 
            // fremarkLabel
            // 
            fremarkLabel.AutoSize = true;
            fremarkLabel.Location = new System.Drawing.Point(13, 128);
            fremarkLabel.Name = "fremarkLabel";
            fremarkLabel.Size = new System.Drawing.Size(35, 12);
            fremarkLabel.TabIndex = 122;
            fremarkLabel.Text = "备注:";
            // 
            // fuse_flagLabel
            // 
            fuse_flagLabel.AutoSize = true;
            fuse_flagLabel.Location = new System.Drawing.Point(13, 100);
            fuse_flagLabel.Name = "fuse_flagLabel";
            fuse_flagLabel.Size = new System.Drawing.Size(35, 12);
            fuse_flagLabel.TabIndex = 123;
            fuse_flagLabel.Text = "启用:";
            // 
            // DataGridViewObject
            // 
            this.DataGridViewObject.AllowUserToAddRows = false;
            this.DataGridViewObject.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DataGridViewObject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewObject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fposition_id,
            this.fname,
            this.fuse_flag,
            this.forder_by,
            this.fremark});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridViewObject.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewObject.EnableHeadersVisualStyles = false;
            this.DataGridViewObject.Location = new System.Drawing.Point(0, 30);
            this.DataGridViewObject.MultiSelect = false;
            this.DataGridViewObject.Name = "DataGridViewObject";
            this.DataGridViewObject.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridViewObject.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewObject.RowHeadersVisible = false;
            this.DataGridViewObject.RowTemplate.Height = 23;
            this.DataGridViewObject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewObject.Size = new System.Drawing.Size(316, 319);
            this.DataGridViewObject.TabIndex = 117;
            // 
            // fposition_id
            // 
            this.fposition_id.DataPropertyName = "fposition_id";
            this.fposition_id.HeaderText = "编号";
            this.fposition_id.Name = "fposition_id";
            this.fposition_id.ReadOnly = true;
            this.fposition_id.Width = 65;
            // 
            // fname
            // 
            this.fname.DataPropertyName = "fname";
            this.fname.HeaderText = "名称";
            this.fname.Name = "fname";
            this.fname.ReadOnly = true;
            // 
            // fuse_flag
            // 
            this.fuse_flag.DataPropertyName = "fuse_flag";
            this.fuse_flag.HeaderText = "启用";
            this.fuse_flag.Name = "fuse_flag";
            this.fuse_flag.ReadOnly = true;
            this.fuse_flag.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fuse_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fuse_flag.Width = 65;
            // 
            // forder_by
            // 
            this.forder_by.DataPropertyName = "forder_by";
            this.forder_by.HeaderText = "排序";
            this.forder_by.Name = "forder_by";
            this.forder_by.ReadOnly = true;
            this.forder_by.Width = 65;
            // 
            // fremark
            // 
            this.fremark.DataPropertyName = "fremark";
            this.fremark.HeaderText = "备注";
            this.fremark.Name = "fremark";
            this.fremark.ReadOnly = true;
            this.fremark.Width = 200;
            // 
            // wwf_positionBindingSource
            // 
            this.wwf_positionBindingSource.DataMember = "wwf_position";
            this.wwf_positionBindingSource.DataSource = this.wwfDataSet;
            this.wwf_positionBindingSource.PositionChanged += new System.EventHandler(this.wwf_positionBindingSource_PositionChanged);
            // 
            // wwfDataSet
            // 
            this.wwfDataSet.DataSetName = "wwfDataSet";
            this.wwfDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fposition_idTextBox
            // 
            this.fposition_idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_positionBindingSource, "fposition_id", true));
            this.fposition_idTextBox.Location = new System.Drawing.Point(54, 14);
            this.fposition_idTextBox.Name = "fposition_idTextBox";
            this.fposition_idTextBox.Size = new System.Drawing.Size(191, 21);
            this.fposition_idTextBox.TabIndex = 119;
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_positionBindingSource, "fname", true));
            this.fnameTextBox.Location = new System.Drawing.Point(54, 41);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(191, 21);
            this.fnameTextBox.TabIndex = 120;
            // 
            // forder_byTextBox
            // 
            this.forder_byTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_positionBindingSource, "forder_by", true));
            this.forder_byTextBox.Location = new System.Drawing.Point(54, 68);
            this.forder_byTextBox.Name = "forder_byTextBox";
            this.forder_byTextBox.Size = new System.Drawing.Size(191, 21);
            this.forder_byTextBox.TabIndex = 121;
            // 
            // fremarkTextBox
            // 
            this.fremarkTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.wwf_positionBindingSource, "fremark", true));
            this.fremarkTextBox.Location = new System.Drawing.Point(54, 125);
            this.fremarkTextBox.Multiline = true;
            this.fremarkTextBox.Name = "fremarkTextBox";
            this.fremarkTextBox.Size = new System.Drawing.Size(191, 114);
            this.fremarkTextBox.TabIndex = 123;
            // 
            // fuse_flagCheckBox
            // 
            this.fuse_flagCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.wwf_positionBindingSource, "fuse_flag", true));
            this.fuse_flagCheckBox.Location = new System.Drawing.Point(54, 95);
            this.fuse_flagCheckBox.Name = "fuse_flagCheckBox";
            this.fuse_flagCheckBox.Size = new System.Drawing.Size(195, 24);
            this.fuse_flagCheckBox.TabIndex = 124;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.fposition_idTextBox);
            this.panel1.Controls.Add(fposition_idLabel);
            this.panel1.Controls.Add(forder_byLabel);
            this.panel1.Controls.Add(fuse_flagLabel);
            this.panel1.Controls.Add(this.fremarkTextBox);
            this.panel1.Controls.Add(this.fnameTextBox);
            this.panel1.Controls.Add(this.forder_byTextBox);
            this.panel1.Controls.Add(this.fuse_flagCheckBox);
            this.panel1.Controls.Add(fremarkLabel);
            this.panel1.Controls.Add(fnameLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(316, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 319);
            this.panel1.TabIndex = 126;
            // 
            // bN
            // 
            this.bN.AddNewItem = null;
            this.bN.AutoSize = false;
            this.bN.BindingSource = this.wwf_positionBindingSource;
            this.bN.CountItem = this.bindingNavigatorCountItem;
            this.bN.DeleteItem = null;
            this.bN.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.toolStripButtonAdd,
            this.toolStripButtonUpdate,
            this.toolStripButtonS,
            this.toolStripButtonDel,
            this.toolStripSeparator3,
            this.toolStripButtonR});
            this.bN.Location = new System.Drawing.Point(0, 0);
            this.bN.MoveFirstItem = null;
            this.bN.MoveLastItem = null;
            this.bN.MoveNextItem = null;
            this.bN.MovePreviousItem = null;
            this.bN.Name = "bN";
            this.bN.PositionItem = this.bindingNavigatorPositionItem;
            this.bN.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bN.Size = new System.Drawing.Size(616, 30);
            this.bN.TabIndex = 127;
            this.bN.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 27);
            this.bindingNavigatorCountItem.Text = "/ {0}";
            this.bindingNavigatorCountItem.ToolTipText = "总项数";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "位置";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "当前位置";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonAdd.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonAdd.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonAdd.Text = "新增(&A)  ";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonUpdate.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonUpdate.Text = "修改(&U)  ";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripButtonS
            // 
            this.toolStripButtonS.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonS.Image")));
            this.toolStripButtonS.Name = "toolStripButtonS";
            this.toolStripButtonS.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonS.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonS.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonS.Text = "保存(&S)  ";
            this.toolStripButtonS.Click += new System.EventHandler(this.toolStripButtonS_Click);
            // 
            // toolStripButtonDel
            // 
            this.toolStripButtonDel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDel.Image")));
            this.toolStripButtonDel.Name = "toolStripButtonDel";
            this.toolStripButtonDel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonDel.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonDel.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonDel.Text = "删除(&D)  ";
            this.toolStripButtonDel.Click += new System.EventHandler(this.toolStripButtonDel_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // toolStripButtonR
            // 
            this.toolStripButtonR.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonR.Image")));
            this.toolStripButtonR.Name = "toolStripButtonR";
            this.toolStripButtonR.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripButtonR.RightToLeftAutoMirrorImage = true;
            this.toolStripButtonR.Size = new System.Drawing.Size(79, 27);
            this.toolStripButtonR.Text = "刷新(&R)  ";
            this.toolStripButtonR.Click += new System.EventHandler(this.toolStripButtonR_Click);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(313, 30);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 319);
            this.splitter1.TabIndex = 128;
            this.splitter1.TabStop = false;
            // 
            // PositionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 349);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.DataGridViewObject);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bN);
            this.Name = "PositionForm";
            this.Text = "PositionForm";
            this.Load += new System.EventHandler(this.PositionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwf_positionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wwfDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bN)).EndInit();
            this.bN.ResumeLayout(false);
            this.bN.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewObject;
        private yunLis.wwf.wwfDataSet wwfDataSet;
        private System.Windows.Forms.BindingSource wwf_positionBindingSource;
        private System.Windows.Forms.TextBox fposition_idTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox forder_byTextBox;
        private System.Windows.Forms.TextBox fremarkTextBox;
        private System.Windows.Forms.CheckBox fuse_flagCheckBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fposition_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fuse_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn forder_by;
        private System.Windows.Forms.DataGridViewTextBoxColumn fremark;
        protected System.Windows.Forms.BindingNavigator bN;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        private System.Windows.Forms.ToolStripButton toolStripButtonS;
        private System.Windows.Forms.ToolStripButton toolStripButtonDel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        protected System.Windows.Forms.ToolStripButton toolStripButtonR;
        private System.Windows.Forms.Splitter splitter1;
    }
}