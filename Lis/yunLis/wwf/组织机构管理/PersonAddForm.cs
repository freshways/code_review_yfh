﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;

namespace yunLis.wwf
{
    public partial class PersonAddForm : Form
    {
        DeptBll bllDept = new DeptBll();
        PersonBLL bll = new PersonBLL();
        DataTable dtPerson = new DataTable();      
        string strDeptID = "";//部门ID
        string strPersonID = "";//人员ID
        int intEdit = 0;//0为新增，1为修改
        public PersonAddForm(string fdept_id, string fperson_id,int fedit)
        {
            InitializeComponent();            
            strDeptID = fdept_id;
            strPersonID = fperson_id;
            intEdit = fedit;
        }

        private void PersonAddForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.fdept_idTextBox.Text = bllDept.BllDeptNameByID(strDeptID);
                

                if (intEdit == 0)
                {
                    dtPerson = this.bll.BllDTByID("-2");
                    wwf_personBindingSource.DataSource = dtPerson;
                    this.fpassTextBox.Enabled = true;
                    this.fperson_idTextBox.Enabled = true;
                    AddNewRow();
                }
                else if (intEdit == 1)
                {
                    //修改记录
                    dtPerson = this.bll.BllDTByID(strPersonID);
                    wwf_personBindingSource.DataSource = dtPerson;
                    this.fpassTextBox.Enabled = false;
                    this.fperson_idTextBox.Enabled = false;
                }
            
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }

        private void SaveUpdate()
        {
            try
            {               
                if (this.fnameTextBox.Text == "" || this.fnameTextBox.Text == null)
                {
                    yunLis.wwfbll.WWMessage.MessageShowWarning("姓名不能为空，请填写后重试！");
                    return;
                }                
                else
                {                   
                    dtPerson.Rows[0]["fname"] = this.fnameTextBox.Text;
                   // dtPerson.Rows[0]["fpass"] = ww.wwf.com.Security.StrToEncrypt("MD5", this.fpassTextBox.Text);
                    dtPerson.Rows[0]["fname_e"] = this.fname_eTextBox.Text;
                    dtPerson.Rows[0]["forder_by"] = this.forder_byTextBox.Text;
                    dtPerson.Rows[0]["ftype"] = this.ftypeTextBox.Text;                    
                    if (fuse_flagCheckBox.Checked)
                        dtPerson.Rows[0]["fuse_flag"] = "1";
                    else
                        dtPerson.Rows[0]["fuse_flag"] = "0";
                    dtPerson.Rows[0]["fremark"] = this.fremarkTextBox.Text;
                    dtPerson.Rows[0]["fshow_name"] = "(" + dtPerson.Rows[0]["fperson_id"].ToString() + ")" + this.fnameTextBox.Text;
                    this.Validate();
                    this.wwf_personBindingSource.EndEdit();
                    if (this.bll.BllUpdate(dtPerson) > 0)
                    {
                        yunLis.wwfbll.WWMessage.MessageShowInformation("用户修改成功！");
                        this.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
        private void SaveAdd()
        {
            try
            {
                if (this.fperson_idTextBox.Text == "" || this.fperson_idTextBox.Text == null)
                {
                    yunLis.wwfbll.WWMessage.MessageShowWarning("登录名不能为空，请填写后重试！");
                    return;
                }
                if (this.fnameTextBox.Text == "" || this.fnameTextBox.Text == null)
                {
                    yunLis.wwfbll.WWMessage.MessageShowWarning("姓名不能为空，请填写后重试！");
                    return;
                }
                if (this.bll.BllPersonExists(this.fperson_idTextBox.Text))
                {
                    yunLis.wwfbll.WWMessage.MessageShowWarning("登录名为" + this.fperson_idTextBox.Text + "的用户已经存在！");
                    return;
                }
                else
                {

                    dtPerson.Rows[0]["fperson_id"] = this.fperson_idTextBox.Text;
                    dtPerson.Rows[0]["fdept_id"] = this.strDeptID;
                    dtPerson.Rows[0]["fname"] = this.fnameTextBox.Text;
                    dtPerson.Rows[0]["fpass"] = ww.wwf.com.Security.StrToEncrypt("MD5", this.fpassTextBox.Text);
                    dtPerson.Rows[0]["fname_e"] = this.fname_eTextBox.Text;
                    dtPerson.Rows[0]["forder_by"] = this.forder_byTextBox.Text;
                    dtPerson.Rows[0]["ftype"] = this.ftypeTextBox.Text;


                    dtPerson.Rows[0]["fhelp_code"] = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(fnameTextBox.Text);

                   // sqlHelp = ww.wwf.com.Spell.MakeSpellCodeFirstLetterOnly(DataGridViewObject.Rows[i].Cells["fname"].Value.ToString().Trim());


                    if (fuse_flagCheckBox.Checked)
                        dtPerson.Rows[0]["fuse_flag"] = "1";
                    else
                        dtPerson.Rows[0]["fuse_flag"] = "0";
                    dtPerson.Rows[0]["fremark"] = this.fremarkTextBox.Text;
                    dtPerson.Rows[0]["fshow_name"] = "(" + this.fperson_idTextBox.Text + ")" + this.fnameTextBox.Text;

                    this.Validate();
                    this.wwf_personBindingSource.EndEdit();
                    if (this.bll.BllAdd(dtPerson) > 0)
                    {
                        yunLis.wwfbll.WWMessage.MessageShowInformation("用户增加成功！");
                        AddNewRow();
                    }
                }
            }
            catch(Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError("请新增功能后再保存！"+ex.Message.ToString());
            }
        }
        private void AddNewRow()
        {
            if (dtPerson != null)
                dtPerson.Clear();
            DataRow dr = null;
            dr = dtPerson.NewRow();
            dr["fdept_id"] = this.strDeptID;
            dr["forder_by"] = "0";
            dr["fuse_flag"] = "1";

           
            if (strDeptID == "jyk")
            {
                dr["ftype"] = "检验";
            }
            else if (strDeptID == "kfz")
            {
                dr["ftype"] = "开发";
            }
            else
            {

                dr["ftype"] = "医生";
            }

            if (this.bllDept.Bllfp_id(strDeptID) == "jyk")
                dr["ftype"] = "检验";

            dtPerson.Rows.Add(dr);
        }
        private void butSave_Click(object sender, EventArgs e)
        {
            if (intEdit == 0)
            {
                SaveAdd();
            }
            else
            {
                SaveUpdate();
            }
        }

        private void butReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}