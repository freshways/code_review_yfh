﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace yunLis.wwf
{
    public partial class DefaultInstSettingForm : Form
    {
        private string cfgPath;
        public DefaultInstSettingForm(string filePath)
        {
            InitializeComponent();
            cfgPath = filePath;
        }

        private void btn关闭_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            try
            {
                string instrID = comboBox仪器.SelectedValue.ToString();
                string instrName = comboBox仪器.Text;
                StreamWriter writer = new StreamWriter(cfgPath, false);
                writer.WriteLine(instrID);
                writer.Close();

                label默认仪器.Text = instrID + " " + instrName;

                //将默认仪器id存入静态类
                yunLis.wwf.Customization.DefaultInstrument.SetInstrID(instrID);
            }
            catch(Exception ex)
            {
                MessageBox.Show("保存默认设备信息时出现异常。\n" + ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void DefaultInstSettingForm_Load(object sender, EventArgs e)
        {
            try
            {
                //加载设备列表
                yunLis.lisbll.sam.InstrBLL bllInstr = new yunLis.lisbll.sam.InstrBLL();
                DataTable dtInstr = bllInstr.BllInstrDTByUseAndGroupID(1, yunLis.wwfbll.LoginBLL.strDeptID);//仪器列表
                this.comboBox仪器.DataSource = dtInstr;

                //20150610 begin  add by wjz 添加本机默认仪器功能
                string strInstrID = yunLis.wwf.Customization.DefaultInstrument.GetInstrID();
                if (!(string.IsNullOrWhiteSpace(strInstrID)))
                {
                    this.comboBox仪器.SelectedValue = strInstrID;
                }
                //20150610 end    add by wjz 添加本机默认仪器功能

                //从文件中读取默认仪器id
                if (File.Exists(cfgPath))
                {
                    StreamReader reader = new StreamReader(cfgPath);
                    string instrID = reader.ReadLine();
                    reader.Close();

                    DataRow[] drs = dtInstr.Select("finstr_id='" + instrID + "'");
                    if (drs.Length > 0)
                    {
                        this.comboBox仪器.SelectedValue = instrID;
                        this.label默认仪器.Text = instrID + " " + drs[0]["fname"].ToString();
                    }
                }
            }
            catch(Exception ex)
            {
                //暂时不用做任何处理
                MessageBox.Show("默认设备设置画面加载时出现异常。\n" + ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
    }
}
