﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ww.wwf.wwfbll;

namespace ww.form.wwf
{
    public partial class Frm仪器项目组合Edit : Form
    {
        bool m_isAdd = true;
        string m_groupid = string.Empty;
        string m_instrid = string.Empty;
        public Frm仪器项目组合Edit(bool isAdd, string groupid, string instrid)
        {
            InitializeComponent();

            m_isAdd = isAdd;
            m_groupid = groupid;
            m_instrid = instrid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(textBox1.Text.Trim()))
            {
                WWMessage.MessageShowWarning("请填写组合名称（不要与其他组合名称重名）。");
                return;
            }
            try
            {
                string sql = "";
                List<SqlParameter> paramlist = new List<SqlParameter>();
                if (m_isAdd)
                {
                    sql = "INSERT INTO [dbo].[SAM_INSTR_GROUPS]([groupid],[finstrid],[groupname]) VALUES(@groupid,@instrid,@groupname)";
                    paramlist.Add(new SqlParameter("@groupid", Guid.NewGuid().ToString().Replace("-", "")));
                    paramlist.Add(new SqlParameter("@instrid", m_instrid));
                    paramlist.Add(new SqlParameter("@groupname", textBox1.Text.Trim()));
                }
                else
                {
                    sql = "update [dbo].[SAM_INSTR_GROUPS] set groupname=@groupname where groupid=@groupid";
                    paramlist.Add(new SqlParameter("@groupid", m_groupid));
                    paramlist.Add(new SqlParameter("@groupname", textBox1.Text.Trim()));
                }
                int ret = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(ww.wwf.wwfbll.WWFInit.strDBConn, sql, paramlist.ToArray());
                if (ret > 0)
                {
                    WWMessage.MessageShowInformation("修改成功");
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    WWMessage.MessageShowInformation("修改失败");
                }
            }
            catch(Exception ex)
            {
                WWMessage.MessageShowWarning(ex.Message);
            }
        }

        private void Frm仪器项目组合Edit_Load(object sender, EventArgs e)
        {
            GetGroupName();
        }

        void GetGroupName()
        {
            try
            {
                string sql = "select groupname from dbo.sam_instr_groups where groupid=@groupid";
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("@groupid", m_groupid));
                string groupname = ww.wwf.wwfbll.WWFInit.wwfRemotingDao.DbExecuteScalarBySql(ww.wwf.wwfbll.WWFInit.strDBConn, sql, paramlist.ToArray());
                textBox1.Text = groupname;
            }
            catch(Exception ex)
            {
                WWMessage.MessageShowWarning("获取组合旧名称时失败,原因："+ex.Message);
            }
        }
    }
}
