﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using yunLis.wwfbll;
namespace yunLis.wwf
{
    public partial class NumForm : SysBaseForm
    {
        NumBLL bll = new NumBLL();
        public NumForm()
        {
            InitializeComponent();
        }

        private void NumForm_Load(object sender, EventArgs e)
        {
            try
            {
                bindingSource1.DataSource = this.bll.BllDT();
                this.DataGridViewObject.AutoGenerateColumns = false;
                this.DataGridViewObject.DataSource = bindingSource1;
            }
            catch (Exception ex)
            {
                yunLis.wwfbll.WWMessage.MessageShowError(ex.ToString());
            }
        }
    }
}