﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using yunLis.lisbll.sam;
using yunLis.wwfbll;

namespace yunLis.wwf
{
    public partial class FrmSignChecker : Form
    {
        private DataTable m_dtUser = null;
        public FrmSignChecker()
        {
            InitializeComponent();

            InitDoctorInfo();
            //this.comboBoxChecker.DataSource = dt;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                string id = this.comboBoxChecker.SelectedValue.ToString();
                string temp = this.comboBoxChecker.Text;
                string name = temp.Replace(id + "|", "");

                LoginBLL.strCheckID = id;
                LoginBLL.strCheckName = name;

                this.DialogResult = DialogResult.OK;
            }
            catch
            {
                MessageBox.Show("请选择审核医师。");
            }
        }

        private void InitDoctorInfo()
        {
            try
            {
                DataSet ds = WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(WWFInit.strDBConn,
                    "SELECT '' fperson_id,'' fname union all SELECT [fperson_id],[fname] FROM [WWF_PERSON] WHERE fuse_flag = 1 order by fname ");
                m_dtUser = ds.Tables[0];
            }
            catch
            {
                m_dtUser = new DataTable();
                m_dtUser.Columns.Add("fperson_id");
                m_dtUser.Columns.Add("fname");

                DataRow drSpace = m_dtUser.NewRow();
                drSpace["fperson_id"] = "";
                drSpace["fname"] = "";
                m_dtUser.Rows.Add(drSpace);
            }
        }

        private void FrmSignChecker_Load(object sender, EventArgs e)
        {
            try
            {
                
                //DataRow dr = dtLisUser.NewRow();
                //dr["USER_CODE"] = "";
                //dr["UESR_NAME"] = "";
                //dtLisUser.Rows.Add(dr);
                this.comboBoxChecker.DataSource = m_dtUser;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            
        }
    }
}
