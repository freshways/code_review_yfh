﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace yunLis.sqz
{
    public partial class FrmUploadSQZ : Form
    {
        string _usercode;
        public FrmUploadSQZ(string userid, string username)
        {
            InitializeComponent();

            _usercode = userid;

            textBox1.Text = username;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        string imagefile = "";
        private void btnUpload_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(imagefile))
            {
                MessageBox.Show("没有选择手签照", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                byte[] sqzImage = ImageToArray(imagefile);
                SaveSQZ(_usercode, sqzImage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.DialogResult = DialogResult.OK;
        }

        private void FrmUploadSQZ_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                pictureBox1.Image.Dispose();
                pictureBox1.Image = null;
            }

        }

        public void SaveSQZ(string usercode, byte[] sqzImage)
        {
            //using (SqlConnection con = new SqlConnection(ClassDBConnstring.SConnHISDb))
            //{
            //    con.Open();
            //    string sql = "update [pubUser] set [手签照片] = @photo  where 用户编码=" + _s用户编码;
            //    SqlParameter param = new SqlParameter();
            //    param = new SqlParameter("@photo", SqlDbType.Image);
            //    param.Value = image;
            //    SqlCommand commd = new SqlCommand(sql, con);
            //    commd.Parameters.Add(param);
            //    try
            //    {
            //        commd.ExecuteNonQuery();
            //    }
            //    catch (Exception ex)
            //    {
            //        string sErr = ex.Message;
            //        MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}

            try
            {
                string sql = @"MERGE [dbo].[SQZ] AS target
    USING (SELECT @usercode, @image) AS source ([usercode],[手签照片])
    ON (target.[usercode] = source.[usercode])
    WHEN MATCHED THEN 
        UPDATE SET [手签照片] = source.[手签照片]
	WHEN NOT MATCHED THEN	
	    INSERT ([usercode], [手签照片])
	    VALUES (source.[usercode], source.[手签照片]);";

                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@usercode", usercode));

                SqlParameter paramTemp = new SqlParameter("@image", SqlDbType.Binary);
                paramTemp.Value = sqzImage;
                paramList.Add(paramTemp);

                yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteNonQueryBySqlString(HIS.COMM.DBConnHelper.SConnHISDb, sql, paramList.ToArray());
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static byte[] ImageToArray(string path)
        {
            FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] bufferPhoto = new byte[stream.Length];
            stream.Read(bufferPhoto, 0, Convert.ToInt32(stream.Length));
            stream.Flush();
            stream.Close();
            return bufferPhoto;
        }

        private void btnSC_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "选择图片";
            openFileDialog.Filter = "jpg文件(*.jpg)|*.jpg";
            DialogResult dialogResult = openFileDialog.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                //HIS.COMM.Class医生.ysImageSaveToDb(HIS.COMM.Class医生.PhotoToArray(openFileDialog.FileName), _s用户编码);

                if (File.Exists(openFileDialog.FileName))
                {
                    FileInfo fileInfo = new FileInfo(openFileDialog.FileName);
                    if (fileInfo.Length > 4096)
                    {
                        MessageBox.Show("手签照大小不允许超过4K.", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        imagefile = openFileDialog.FileName;
                        pictureBox1.Image = Image.FromFile(openFileDialog.FileName);
                    }
                }
            }
        }

        private void FrmUploadSQZ_Load(object sender, EventArgs e)
        {
            try
            {
                string sql = "SELECT [usercode],[手签照片] FROM [dbo].[SQZ] where usercode=@usercode";
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@usercode", _usercode));
                DataSet ds = yunLis.wwfbll.WWFInit.wwfRemotingDao.DbExecuteDataSetBySqlString(yunLis.wwfbll.WWFInit.strDBConn, sql, paramList.ToArray());
                if (ds == null || ds.Tables.Count==0)
                {
                    MessageBox.Show("连接数据库可能查询异常", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["手签照片"] != null && ds.Tables[0].Rows[0]["手签照片"].ToString() != "")
                    {
                        try
                        {
                            byte[] imagebyte = (byte[])ds.Tables[0].Rows[0]["手签照片"];
                            MemoryStream ms = new MemoryStream(imagebyte);
                            this.pictureBox1.Image = System.Drawing.Image.FromStream(ms, true);
                        }
                        catch
                        {

                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        
    }
}
