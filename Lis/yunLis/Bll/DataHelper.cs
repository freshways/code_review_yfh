﻿using HIS.Model;
using HIS.Model.Pojo.LIS;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WEISHENG.COMM;
using yunLis.Pojo;

namespace yunLis.Bll
{
    public class DataHelper
    {
        /// <summary>
        ///根据设备id查询所有计算项目 
        /// </summary>
        /// <param name="str仪器id"></param>
        /// <returns></returns>
        public static List<PojoCalItem> BllGetCaluatedItems(string str仪器id)
        {
            using (LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {
                string sql = "SELECT b.fitem_id as fitem_id,a.fitem_code FItem, a.fname as fitem_name,a.funit_name,a.fprint_num,a.fref,a.fjx_formula " +
                    "FROM SAM_ITEM a, SAM_ITEM_IO b " +
                    "WHERE a.fitem_id = b.fitem_id " +
                    "and b.finstr_id='" + str仪器id + "' and a.fjx_if=1 ";
                return lis.Database.SqlQuery<PojoCalItem>(sql).ToList();
            }
        }

        /// <summary>
        /// 仪器设备结果
        /// </summary>
        /// <returns></returns>
        public static List<Pojo设备结果明细> dt_Lis_Ins_Result_Detail(string str结果id, string str仪器id)
        {
            using (LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {

                string sql = "SELECT b.fitem_id as fitem_id,c.fname as fitem_name,c.funit_name,c.fprint_num,c.fref,a.*  " +
                    "FROM Lis_Ins_Result_Detail a,SAM_ITEM_IO b,SAM_ITEM c " +
                    "WHERE a.FItem = b.fcode and b.fitem_id = c.fitem_id " +
                    "and b.finstr_id='" + str仪器id + "' and (FTaskID = '" + str结果id + "') " +
                    " order by convert(int,c.fprint_num)";
                return lis.Database.SqlQuery<Pojo设备结果明细>(sql).ToList();
            }
        }


        /// <summary>
        /// 获得检验结果
        /// </summary>
        public static List<PojoSam_Jy_Result> Get_SAM_JY_RESULT(string strWhere)
        {

            using (LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("select t.*,'' as fitem_jgref ");
                strSql.Append(" FROM SAM_JY_RESULT t");
                if (strWhere.Trim() != "")
                {
                    strSql.Append(" where " + strWhere);
                }
                strSql.Append(" order by (SELECT top 1 convert(int,z.fprint_num)  FROM   SAM_ITEM z WHERE (z.fitem_id = t.fitem_id))");
                return lis.Database.SqlQuery<PojoSam_Jy_Result>(strSql.ToString()).ToList();
            }
        }

        public static Pojo报告单摘要 getModel报告摘要(string fjy_id)
        {
            using (LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {
                string sql1 = $@"SELECT
	                            * 
                            FROM
	                            (
	                            SELECT
		                            fhz_name 姓名,
		                            fhz_age 年龄,
		                            fjy_yb_code 样本号,
		                            fhz_zyh 住院号,
		                            fhz_bed 床号,
		                            fapply_id 条码,
		                            fremark 备注,
		                            fjy_date 检验日期,
		                            fapply_time 送检日期,
		                            freport_time 报告时间,
		                            fjy_instr 化验设备ID,
 		                            (select top 1 fcode from SAM_REPORT report where report.finstr_id=t.fjy_instr) 报告样式,
		                            ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '病人类别' AND z.fcode= t.fhz_type_id ) ) AS 病人类别,
		                            ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '性别' AND z.fcode= t.fhz_sex ) ) AS 性别,
		                            ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '年龄单位' AND z.fcode= t.fhz_age_unit ) ) AS 年龄单位,
		                            ( SELECT TOP 1 z.fname FROM WWF_DEPT z WHERE ( z.fdept_id= t.fhz_dept ) ) AS 科室,
		                            ( SELECT TOP 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE ( z.fsample_type_id= t.fjy_yb_type ) ) AS 样本类型,
		                            ( SELECT TOP 1 z.fname FROM WWF_PERSON z WHERE ( z.fperson_id= t.fjy_user_id ) ) AS 检验医师,
		                            ( SELECT TOP 1 z.fname FROM WWF_PERSON z WHERE ( z.fperson_id= t.fapply_user_id ) ) AS 申请医师,
		                            ( SELECT TOP 1 z.fname FROM SAM_DISEASE z WHERE z.fcode = t.fjy_lczd ) AS 疾病名称,
		                            ( SELECT TOP 1 z.fname FROM WWF_PERSON Z WHERE ( Z.fperson_id= t.fchenk_user_id ) ) 审核者,
		                            ( SELECT ins.fname FROM SAM_INSTR ins WHERE ins.finstr_id = t.fjy_instr ) 检验设备,
		                            ( SELECT app.fitem_group FROM SAM_APPLY app WHERE app.fapply_id = t.fapply_id ) 化验项目,
		                            t.fjy_id,
		                            fprint_zt 打印状态
                            FROM
	                            SAM_JY t 
                            WHERE
	                            t.fjy_zt= '已审核' 
	                            ) a 
                            WHERE	    
	                           fjy_id= '{fjy_id}'
                            ORDER BY
	                            住院号 ASC,检验日期,
	                            CAST ( 样本号 AS INT ) ASC";
                //string sql1 = $@"SELECT	 fjy_id,                        
                //             '' 报表名称,
                //             fhz_name 姓名,
                //             fjy_yb_code 样本号,
                //             fhz_zyh 住院号,
                //             fhz_bed 床号,
                //             fapply_id 条码,
                //             fremark 备注,
                //             fjy_date 检验日期,
                //             fapply_time 送检日期,
                //             freport_time 报告时间,
                //             ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '病人类别' AND z.fcode= t.fhz_type_id ) ) AS 病人类别,
                //             ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '性别' AND z.fcode= t.fhz_sex ) ) AS 性别,
                //             ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '年龄单位' AND z.fcode= t.fhz_age_unit ) ) AS 年龄单位,
                //             ( SELECT TOP 1 z.fname FROM WWF_DEPT z WHERE ( z.fdept_id= t.fhz_dept ) ) AS 科室,
                //             ( SELECT TOP 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE ( z.fsample_type_id= t.fjy_yb_type ) ) AS 样本类型,
                //             ( SELECT TOP 1 z.fname FROM WWF_PERSON z WHERE ( z.fperson_id= t.fjy_user_id ) ) AS 检验医师,
                //             ( SELECT TOP 1 z.fname FROM WWF_PERSON z WHERE ( z.fperson_id= t.fapply_user_id ) ) AS 申请医师,
                //             ( SELECT TOP 1 z.fname FROM SAM_DISEASE z WHERE z.fcode = t.fjy_lczd ) AS 疾病名称,
                //             ( SELECT TOP 1 z.fname FROM WWF_PERSON Z WHERE ( Z.fperson_id= t.fchenk_user_id ) ) 审核者  
                //            FROM
                //             SAM_JY t 
                //            WHERE
                //             t.fjy_id= '{fjy_id}'";
                var item表头 = lis.Database.SqlQuery<Pojo.Pojo报告单摘要>(sql1).FirstOrDefault();
                if (item表头 == null)
                {
                    msgHelper.ShowInformation("报告表头信息为空！");
                }
                return item表头;
            }
        }

        public static List<Pojo报告单摘要> getList报告摘要(string sqlWhere, string sqlOrderby = "住院号 ASC,检验日期, CAST (样本号 AS INT) ASC")
        {
            using (LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {

                /// <summary>
                /// 获得数据列表
                /// </summary>
                //public DataTable get_dtSAM_JY(string strWhere)
                //{
                //    StringBuilder strSql = new StringBuilder();
                //    strSql.Append($@"select (SELECT top 1 z.fname FROM SAM_TYPE z 
                //                    WHERE (z.ftype = '病人类别' and z.fcode=t.fhz_type_id)) as 类型,     o   病人类别     k
                //                    (SELECT top 1 z.fname FROM SAM_TYPE z 
                //                    WHERE (z.ftype = '性别' and z.fcode=t.fhz_sex)) as 性别,      ok
                //                    (SELECT top 1 z.fname FROM SAM_TYPE z WHERE (z.ftype = '年龄单位' and z.fcode=t.fhz_age_unit)) as 年龄单位,   ok
                //                    (SELECT top 1 z.fname FROM WWF_DEPT z WHERE (z.fdept_id=t.fhz_dept)) as 科室,          ok
                //                    (SELECT top 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE (z.fsample_type_id=t.fjy_yb_type)) as 样本,   o  样本类型   k
                //                    (SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fjy_user_id)) as 检验医师,    ok
                //                    (SELECT top 1 z.fname FROM WWF_PERSON z WHERE (z.fperson_id=t.fapply_user_id)) as 申请医师,  ok
                //                    (select top 1 z.fname from SAM_DISEASE z where z.fcode = t.fjy_lczd) as 疾病名称,  ok
                //                    (select top 1 z.fname from WWF_PERSON Z WHERE (Z.fperson_id=t.fchenk_user_id)) 审核者    ok
                //,t.* ");
                //    strSql.Append(" FROM SAM_JY t");
                //    if (strWhere.Trim() != "")
                //    {
                //        strSql.Append(" where " + strWhere);
                //    }
                //    return SqlHelper.ExecuteDataTable(strSql.ToString());
                //}

                string sql = $@"SELECT
	                            * 
                            FROM
	                            (
	                            SELECT
		                            fhz_name 姓名,
		                            fhz_age 年龄,
		                            fjy_yb_code 样本号,
		                            fhz_zyh 住院号,
		                            fhz_bed 床号,
		                            fapply_id 条码,
		                            fremark 备注,
		                            fjy_date 检验日期,
		                            fapply_time 送检日期,
		                            freport_time 报告时间,
		                            fjy_instr 化验设备ID,
 		                            (select top 1 fcode from SAM_REPORT report where report.finstr_id=t.fjy_instr) 报告样式,
		                            ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '病人类别' AND z.fcode= t.fhz_type_id ) ) AS 病人类别,
		                            ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '性别' AND z.fcode= t.fhz_sex ) ) AS 性别,
		                            ( SELECT TOP 1 z.fname FROM SAM_TYPE z WHERE ( z.ftype = '年龄单位' AND z.fcode= t.fhz_age_unit ) ) AS 年龄单位,
		                            ( SELECT TOP 1 z.fname FROM WWF_DEPT z WHERE ( z.fdept_id= t.fhz_dept ) ) AS 科室,
		                            ( SELECT TOP 1 z.fname FROM SAM_SAMPLE_TYPE z WHERE ( z.fsample_type_id= t.fjy_yb_type ) ) AS 样本类型,
		                            ( SELECT TOP 1 z.fname FROM WWF_PERSON z WHERE ( z.fperson_id= t.fjy_user_id ) ) AS 检验医师,
		                            ( SELECT TOP 1 z.fname FROM WWF_PERSON z WHERE ( z.fperson_id= t.fapply_user_id ) ) AS 申请医师,
		                            ( SELECT TOP 1 z.fname FROM SAM_DISEASE z WHERE z.fcode = t.fjy_lczd ) AS 疾病名称,
		                            ( SELECT TOP 1 z.fname FROM WWF_PERSON Z WHERE ( Z.fperson_id= t.fchenk_user_id ) ) 审核者,
		                            ( SELECT ins.fname FROM SAM_INSTR ins WHERE ins.finstr_id = t.fjy_instr ) 检验设备,
		                            ( SELECT app.fitem_group FROM SAM_APPLY app WHERE app.fapply_id = t.fapply_id ) 化验项目,
                                    fjy_zt 化验状态,
		                            t.*,
		                            fprint_zt 打印状态
                            FROM
	                            SAM_JY t 
	                            ) a 
                            WHERE
	                            1 = 1 and
	                            {sqlWhere}
                            ORDER BY
	                            {sqlOrderby}";

                var list摘要 = lis.Database.SqlQuery<Pojo.Pojo报告单摘要>(sql).ToList();
                return list摘要;
            }
        }




        public static List<Pojo报告单内容> get报告内容(string fjy_id)
        {
            using (LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {
                //string sql = "select fitem_code 检验编码,fitem_name 检验名称,fitem_unit 单位,fitem_ref 参考值,fitem_badge 升降,fvalue 检验值,forder_by 排序  FROM SAM_JY_RESULT t where t.fjy_id='" + item.fjy_id + "'  order by cast(forder_by as int) ";
                //this.gc化验明细.DataSource = lis.Database.SqlQuery<Pojo.Pojo报告单内容Form>(sql).ToList();

                string sql2 = $@"SELECT
	                            ROW_NUMBER ( ) OVER ( ORDER BY CAST ( forder_by AS INT ) ) AS 序号 ,
	                            fitem_code 项目代码,
	                            fitem_name 项目名称,
	                            fvalue 结果,
	                            fitem_unit 单位,
	                            fitem_badge 标记,
	                            fitem_ref 参考值 
                            FROM
	                            SAM_JY_RESULT t 
                            WHERE
	                            t.fjy_id= '{fjy_id}' 
                            ORDER BY
	                            CAST ( forder_by AS INT )";

                var list报告内容 = lis.Database.SqlQuery<Pojo报告单内容>(sql2).ToList();
                if (list报告内容.Count() == 0)
                {
                    msgHelper.ShowInformation("报告内容为空！");
                }
                return list报告内容;
            }
        }





        public static List<Pojo比较值> GetList比较值(string strfjy_id)
        {
            using (LISEntities lis = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {
                string strSql = "SELECT '' as 上一次,'' as 上二次,'' as 上三次,fitem_id AS 项目id, fitem_name AS 项目, fvalue AS 项目值 FROM    SAM_JY_RESULT WHERE     (fjy_id = '" + strfjy_id + "')  ORDER BY fjy_id, forder_by";
                return lis.Database.SqlQuery<Pojo比较值>(strSql).ToList();
            }
        }
    }
}
