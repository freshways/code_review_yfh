﻿using HIS.Model;
using HIS.Model.Pojo.LIS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yunLis.wwfbll;

namespace yunLis.Bll
{
    public class StyleHelper
    {
        public static string Set小数格式(string strfitem_id, string value, List<SAM_ITEM> listSam_item)
        {
            //小数位数
            //fcode	fhelp_code	fname
            //1	    NULL	保持原始值
            //2	    NULL	0 位
            //3	    NULL	1 位
            //4	    NULL	2 位
            //5	    NULL	3 位
            //6	    NULL	4 位
            //7	    NULL	5 位
            //8	    NULL	6 位

            //结果类型
            //ftype	fcode	fhelp_code	fname
            //结果类型	1	NULL	数值型
            //结果类型	2	NULL	文字型
            //结果类型	3	NULL	阴阳性型
            //结果类型	4	NULL	数值文字混合型
            string strfitem_value = "";
            var item = listSam_item.Where(c => c.fitem_id == strfitem_id).FirstOrDefault();
            string s小数位数 = item.fvalue_dd.ToString();
            //↓ 2016-10-10 11:42:28 yufh 调整系数  添加调整系数计算 
            string strfxs = item.fxs.ToString();
            if (strfxs != "1")
                value = (Convert.ToDecimal(value) * Convert.ToDecimal(strfxs)).ToString();
            //↑ 2016-10-10 11:42:28 yufh 如果项目维护了调整系数且不等于1则参与计算，配合小数位使用

            if (s小数位数 == "2")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 0).ToString();
            }
            else if (s小数位数 == "3")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 1).ToString("0.0");
            }
            else if (s小数位数 == "4")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 2).ToString("0.00");
            }
            else if (s小数位数 == "5")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 3).ToString("0.000");
            }
            else if (s小数位数 == "6")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 4).ToString("0.0000");
            }
            else if (s小数位数 == "7")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 5).ToString("0.00000");
            }
            else if (s小数位数 == "8")
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 6).ToString("0.000000");
            }
            else if (s小数位数 == "1")
            {
                strfitem_value = value;
            }
            else
            {
                strfitem_value = Math.Round(Convert.ToDecimal(value), 2).ToString("0.00");
            }
            return strfitem_value;
        }

        public static string bll公式计算值ByDataTable(string str公式, List<PojoSam_Jy_Result> dtable)
        {
            string strR = "";
            try
            {
                string strgs_新 = str公式;
                foreach (var item in dtable)
                {
                    string strItemCode = "";
                    string strItemValue = "";
                    if (item.fitem_code != null)
                    {
                        strItemCode = "[" + item.fitem_code + "]";
                    }
                    if (item.fvalue != null)
                    {
                        strItemValue = item.fvalue;
                    }
                    strgs_新 = DbIndexOfStrRep(strgs_新, strItemCode, strItemValue);
                }

                strgs_新 = strgs_新.Replace("[", "").Trim();
                strgs_新 = strgs_新.Replace("]", "").Trim();

                strR = yunLis.wwfbll.FormulaGenerator.EvaluationResult(strgs_新);
                try
                {
                    double dou1 = Convert.ToDouble(strR.ToString());
                    strR = Math.Round(dou1, 2, MidpointRounding.AwayFromZero).ToString();
                }
                catch
                {
                    strR = "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strR;
        }
        /// <summary>
        /// 字符串替换
        /// </summary>
        /// <param name="str字符串"></param>
        /// <param name="str包含字符"></param>
        /// <param name="str新值"></param>
        /// <returns></returns>
        public static string DbIndexOfStrRep(string str字符串, string str包含字符, string str新值)
        {
            if (str字符串.IndexOf(str包含字符) > -1)
            {
                str字符串 = str字符串.Replace(str包含字符, str新值).Trim();
            }
            return str字符串;
        }
        public static void SplitItems(string formula, List<string> items)
        {
            int beg = -1;

            for (int index = 0; index < formula.Length; index++)
            {
                if (formula[index] == '[')
                {
                    beg = index;
                }
                else if (formula[index] == ']')
                {
                    //end = index;
                    if (beg != -1 && index > beg)
                    {
                        string sub = formula.Substring(beg + 1, index - beg - 1);
                        items.Add(sub);
                        beg = -1;
                    }
                }
                else
                {

                }
            }
        }
    }
}
