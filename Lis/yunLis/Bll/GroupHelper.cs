﻿using HIS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yunLis.Bll
{
    public class GroupHelper
    {


        public static List<SAM_ITEM> get组合内容(string groupID)
        {

            using (LISEntities lisEntities = new LISEntities(EF6Helper.GetLISEf6Conn(HIS.COMM.DBConnHelper.sLISConnString)))
            {
                string sql = $@"select * from SAM_ITEM where fitem_id in
                                (
                                select fitem_id from SAM_ITEM_GROUP_REL where fgroup_id='{groupID}'
                                )";
                return lisEntities.Database.SqlQuery<SAM_ITEM>(sql).ToList();
            }


        }
    }
}
