﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;


namespace HIS.Model.Helper
{
    /// <summary>
    /// 使用原型模式创建新的实例对象
    /// 不产生新的内存地址，方便界面绑定
    /// excludes列表指定跳过的字段，一般用于ef6提交时的主键
    /// </summary>
    public static class modelHelper
    {
        /// <summary>
        /// 清理实体信息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <param name="忽略清单{"",""}"></param>
        /// <returns></returns>
        public static bool clearContext<T>(T model, params string[] excludes)
        {
            List<string> list_excludes = excludes.ToList();
            if (model == null)
            {
                return false;
            }
            System.Reflection.PropertyInfo[] properties = model.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            if (properties.Length <= 0)
            {
                return false;
            }
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                if (list_excludes.Contains(property.Name))
                {
                    continue;
                }
                string value2 = null;
                if (!property.PropertyType.IsGenericType)
                {
                    //非泛型
                    property.SetValue(model, string.IsNullOrEmpty(value2) ? null : Convert.ChangeType(value2, property.PropertyType), null);
                }
                else
                {
                    //泛型Nullable<>
                    Type genericTypeDefinition = property.PropertyType.GetGenericTypeDefinition();
                    if (genericTypeDefinition == typeof(Nullable<>))
                    {
                        property.SetValue(model, string.IsNullOrEmpty(value2) ? null : Convert.ChangeType(value2, Nullable.GetUnderlyingType(property.PropertyType)), null);
                    }
                }
            }
            return true;
        }


        /// <summary>
        /// 不产生新的实例复制数据，方便界面双向绑定
        /// excludes列表指定跳过的字段，一般用于ef6提交时的主键
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns> 
        public static bool copyInstanceValue<T>(T source, T target, params string[] excludes)
        {
            List<string> list_excludes = excludes.ToList();
            var ParentType = typeof(T);
            var Properties = ParentType.GetProperties();
            foreach (var Propertie in Properties)
            {
                if (Propertie.CanRead && Propertie.CanWrite)
                {
                    if (list_excludes.Contains(Propertie.Name))
                    {
                        continue;
                    }
                    Propertie.SetValue(target, Propertie.GetValue(source, null), null);
                }
            }
            return true;
        }


        public static T ConvertTo<T>(this IConvertible convertibleValue)
        {
            if (null == convertibleValue)
            {
                return default(T);
            }

            if (!typeof(T).IsGenericType)
            {
                return (T)Convert.ChangeType(convertibleValue, typeof(T));
            }
            else
            {
                Type genericTypeDefinition = typeof(T).GetGenericTypeDefinition();
                if (genericTypeDefinition == typeof(Nullable<>))
                {
                    return (T)Convert.ChangeType(convertibleValue, Nullable.GetUnderlyingType(typeof(T)));
                }
            }
            throw new InvalidCastException(string.Format("Invalid cast from type \"{0}\" to type \"{1}\".", convertibleValue.GetType().FullName, typeof(T).FullName));

        }

        /// <summary>
        /// 不同类型实体数据复制
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool copyInstanceValue<TSource, TTarget>(TSource source, TTarget target)
        {
            var typeSource = typeof(TSource);
            var PropertyInfoSource = typeSource.GetProperties();

            var typeTarget = typeof(TTarget);
            var PropertyInfosTarget = typeTarget.GetProperties();
            try
            {
                foreach (var itemSource in PropertyInfoSource)
                {
                    var objSourceValue = itemSource.GetValue(source, null);
                    if (objSourceValue == null)
                    {
                        break;
                    }
                    foreach (var itemTarget in PropertyInfosTarget)
                    {
                        if (itemSource.Name == itemTarget.Name)// && itemSource.PropertyType == itemTarget.PropertyType
                        {
                            itemTarget.SetValue(target, Convert.ChangeType(objSourceValue, itemTarget.PropertyType), null);                         
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 支持自动数据类型转换的映射方法,产生新的实例返回，界面绑定的时候需要注意
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <param name="s"></param>
        /// <returns></returns>
        public static D Mapper<S, D>(S s)
        {
            D d = Activator.CreateInstance<D>();//产生新的内存地址
            try
            {
                var sType = s.GetType();
                var dType = typeof(D);
                foreach (PropertyInfo sP in sType.GetProperties())
                {
                    foreach (PropertyInfo dP in dType.GetProperties())
                    {
                        if (dP.Name == sP.Name)
                        {
                            try
                            {
                                dP.SetValue(d, sP.GetValue(s));
                                break;
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return d;
        }




        /// <summary>
        /// Clones the specified list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="List">The list.</param>
        /// <returns>List{``0}.</returns>
        public static List<T> ListClone<T>(object List)
        {
            using (Stream objectStream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(objectStream, List);
                objectStream.Seek(0, SeekOrigin.Begin);
                return formatter.Deserialize(objectStream) as List<T>;
            }
        }

        /// <summary>
        /// 不用加Serializable的深复制方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T DeepCopy<T>(T obj)
        {
            object retval;
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializer xml = new XmlSerializer(typeof(T));
                xml.Serialize(ms, obj);
                ms.Seek(0, SeekOrigin.Begin);
                retval = xml.Deserialize(ms);
                ms.Close();
            }
            return (T)retval;
        }

        public static string toStringWithNewline<T>(T model)
        {
            StringBuilder sb = new StringBuilder();
            var ParentType = typeof(T);
            var Properties = ParentType.GetProperties();
            foreach (var Propertie in Properties)
            {
                if (Propertie.CanRead && Propertie.CanWrite)
                {
                    if (Propertie.Name == "IsChanged" || Propertie.Name == "createTime" || Propertie.Name == "ID")
                    {
                        continue;
                    }
                    sb.Append(string.Format("{0}:{1}\r\n", Propertie.Name.ToString(), Propertie.GetValue(model, null)));
                }
            }
            return sb.ToString();
        }


    }
}
