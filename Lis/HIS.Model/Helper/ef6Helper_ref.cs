﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Helper
{

    /// <summary>
        /// EF——对象操作-辅助类
        /// </summary>
        /// <typeparam name="M"></typeparam>
    public class ef6Helper<M> where M : DbContext, new()//new限定类必须有无参构造函数，DbContext限定类型
    {
        /// <summary>
                /// DBEntities
                /// </summary>
        private M sd;
        /// <summary>
                /// 实例化DBEntities
                /// </summary>
                /// <returns></returns>
        private M GetDBEntities()
        {
            if (sd == null)
            {
                sd = new M();
            }
            return sd;
        }
        /// <summary>
                /// 添加数据到对象
                /// </summary>
                /// <typeparam name="T">对象</typeparam>
                /// <param name="t">对象名</param>
                /// <returns>true/false</returns>
        public bool Add<T>(T t) where T : class
        {
            M sde = GetDBEntities();
            sde.Set<T>().Add(t);
            return sde.SaveChanges() > 0;
        }
        /// <summary>
                /// 根据条件查询对象并返回对象集合
                /// </summary>
                /// <typeparam name="T">对象</typeparam>
                /// <param name="p">条件</param>
                /// <returns>对象集合list</returns>
        public List<T> GetList<T>(Expression<Func<T, bool>> p) where T : class
        {
            M sde = GetDBEntities();
            return sde.Set<T>().Where(p).ToList();
        }
        /// <summary>
                /// 根据条件查询对象
                /// </summary>
                /// <typeparam name="T">对象</typeparam>
                /// <param name="p">条件</param>
                /// <returns>对象</returns>
        public T GetNow<T>(Expression<Func<T, bool>> p) where T : class
        {
            M sde = GetDBEntities();
            return sde.Set<T>().Where(p).FirstOrDefault();
        }
        /// <summary>
                /// 修改对象
                /// </summary>
                /// <typeparam name="T">对象</typeparam>
                /// <param name="t">对象名称</param>
                /// <returns>true/false</returns>
        public bool Update<T>(T t) where T : class
        {
            M sde = GetDBEntities();
            var ent = sde.Entry<T>(t);
            ent.State = EntityState.Unchanged;
            PropertyInfo[] pi = t.GetType().GetProperties();
            for (int i = 0; i < pi.Length; i++)
            {
                object obj = pi[i].GetValue(t);
                if (obj != null)
                {
                    ent.Property(pi[i].Name).IsModified = true;
                }
            }
            return sde.SaveChanges() > 0;
        }
        /// <summary>
                /// 删除对象
                /// </summary>
                /// <typeparam name="T">对象</typeparam>
                /// <param name="t">对象名</param>
                /// <returns>true/false</returns>
        public bool Delete<T>(T t) where T : class
        {
            M sde = GetDBEntities();
            var ent = sde.Entry<T>(t);
            ent.State = EntityState.Deleted;
            return sde.SaveChanges() > 0;
        }

    }
}
