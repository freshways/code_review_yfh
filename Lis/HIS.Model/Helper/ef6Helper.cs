﻿using System.Data.Common;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace HIS.Model
{
    /// <summary>
    /// EF6基础类
    /// </summary>
    public class EF6Helper
    {
        public static DbConnection GetDbEf6Conn(string _connectionString)
        {
            //Next: 迁移到HIS.COMM
            string providerName = "System.Data.SqlClient";
            SqlConnectionStringBuilder sqlbulider = new SqlConnectionStringBuilder(_connectionString);
            sqlbulider.PersistSecurityInfo = true;
            sqlbulider.IntegratedSecurity = false;
            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();
            entityBuilder.Provider = providerName;
            entityBuilder.ProviderConnectionString = sqlbulider.ToString();
            entityBuilder.Metadata = @"res://*/DB.csdl|res://*/DB.ssdl|res://*/DB.msl";
            EntityConnection entityConnection = new EntityConnection(entityBuilder.ToString());
            return entityConnection;
        }

        public static DbConnection GetLISEf6Conn(string _connectionString)
        {
            //Next: 迁移到HIS.COMM
            string providerName = "System.Data.SqlClient";
            SqlConnectionStringBuilder sqlbulider = new SqlConnectionStringBuilder(_connectionString);
            sqlbulider.PersistSecurityInfo = true;
            sqlbulider.IntegratedSecurity = false;
            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();
            entityBuilder.Provider = providerName;
            entityBuilder.ProviderConnectionString = sqlbulider.ToString();
            entityBuilder.Metadata = @"res://*/LISEntities.csdl|res://*/LISEntities.ssdl|res://*/LISEntities.msl";
            EntityConnection entityConnection = new EntityConnection(entityBuilder.ToString());
            return entityConnection;
        }
        public static DbConnection GetPluginDbEf6Conn(string _connectionString)
        {
            string providerName = "System.Data.SqlClient";
            SqlConnectionStringBuilder sqlbulider = new SqlConnectionStringBuilder(_connectionString);
            sqlbulider.ConnectTimeout = 120;
            sqlbulider.PersistSecurityInfo = true;
            sqlbulider.IntegratedSecurity = false;
            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();
            entityBuilder.Provider = providerName;
            entityBuilder.ProviderConnectionString = sqlbulider.ToString();
            entityBuilder.Metadata = @"res://*/Plugins.csdl|res://*/Plugins.ssdl|res://*/Plugins.msl";
            EntityConnection entityConnection = new EntityConnection(entityBuilder.ToString());
            return entityConnection;
        }



    }
}
