﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Helper
{
    public class timeHelper
    {
        public static string getServerDateTimeStr()
        {
            string currTime = HIS.Model.Dal.SqlHelper.ExecuteScalar("Select CONVERT(varchar(100), GETDATE(), 120)").ToString();
            return currTime;
        }
        public static DateTime getServerDateTime()
        {
            string currTime = HIS.Model.Dal.SqlHelper.ExecuteScalar("Select CONVERT(varchar(100), GETDATE(), 120)").ToString();
            return Convert.ToDateTime(currTime);
        }

        /// <summary>
        /// 使用数据库方法返回服务器当前日期
        /// </summary>
        /// <returns></returns>
        public static string getServerDateStr()
        {
            string currTime = HIS.Model.Dal.SqlHelper.ExecuteScalar("Select CONVERT(varchar(100), GETDATE(), 23)").ToString();
            return currTime;
        }

    }
}
