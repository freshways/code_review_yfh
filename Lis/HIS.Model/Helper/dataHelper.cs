﻿namespace HIS.Model
{
    /// <summary>
    /// 通用数据类
    /// 主要用途
    /// 1、全局chis，实现引用部分的数据通道
    /// 2、常用数据集，实现数据缓存，减轻数据库压力
    /// </summary>
    public class dataHelper
    {
        private static dataHelper _Singleton = null;        
        public static string ConnHisString = "";//赋值后，与sqlHelper同步
        public static PluginsEntities plugDB;

        public static dataHelper CreateInstance(string _his_conn, string plug_conn_string)
        {          
            if (_Singleton == null)
            {
                _Singleton = new dataHelper(_his_conn, plug_conn_string);
            }
            return _Singleton;
        }
        dataHelper(string his_conn, string plug_conn_string)
        {
            plugDB = new HIS.Model.PluginsEntities(HIS.Model.EF6Helper.GetPluginDbEf6Conn(plug_conn_string));
            ConnHisString = his_conn;
        }
    }
}
