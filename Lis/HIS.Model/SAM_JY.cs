//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class SAM_JY
    {
        public string fjy_id { get; set; }
        public string fjy_date { get; set; }
        public string fjy_instr { get; set; }
        public string fjy_yb_code { get; set; }
        public string fjy_yb_tm { get; set; }
        public string fjy_yb_type { get; set; }
        public string fjy_sf_type { get; set; }
        public Nullable<double> fjy_f { get; set; }
        public string fjy_f_ok { get; set; }
        public string fjy_zt { get; set; }
        public string fjy_md { get; set; }
        public string fjy_lczd { get; set; }
        public string fhz_id { get; set; }
        public string fhz_type_id { get; set; }
        public string fhz_zyh { get; set; }
        public string fhz_name { get; set; }
        public string fhz_sex { get; set; }
        public Nullable<double> fhz_age { get; set; }
        public string fhz_age_unit { get; set; }
        public string fhz_age_date { get; set; }
        public string fhz_bq { get; set; }
        public string fhz_dept { get; set; }
        public string fhz_room { get; set; }
        public string fhz_bed { get; set; }
        public string fhz_volk { get; set; }
        public string fhz_hy { get; set; }
        public string fhz_job { get; set; }
        public string fhz_gms { get; set; }
        public string fhz_add { get; set; }
        public string fhz_tel { get; set; }
        public string fapply_id { get; set; }
        public string fapply_user_id { get; set; }
        public string fapply_dept_id { get; set; }
        public string fapply_time { get; set; }
        public string fsampling_user_id { get; set; }
        public string fsampling_time { get; set; }
        public string fjy_user_id { get; set; }
        public string fchenk_user_id { get; set; }
        public string fcheck_time { get; set; }
        public string freport_time { get; set; }
        public string fprint_time { get; set; }
        public string fprint_zt { get; set; }
        public Nullable<int> fprint_count { get; set; }
        public string fsys_user { get; set; }
        public string fsys_time { get; set; }
        public string fsys_dept { get; set; }
        public string fremark { get; set; }
        public string finstr_result_id { get; set; }
        public string f1 { get; set; }
        public string f2 { get; set; }
        public string f3 { get; set; }
        public string f4 { get; set; }
        public string f5 { get; set; }
        public string f6 { get; set; }
        public string f7 { get; set; }
        public string f8 { get; set; }
        public string f9 { get; set; }
        public string f10 { get; set; }
        public string sfzh { get; set; }
    }
}
