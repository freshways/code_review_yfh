﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HIS.Model
{

    public partial class yb_费用结算
    {
        decimal _其他医保支付;
        decimal _sum报销;

        [NotMapped]
        public decimal 其他医保支付//住院发票使用，统筹之外报销金额
        {
            set { _其他医保支付 = value; }
            get
            {
                return Convert.ToDecimal(救助金支付) +
                        Convert.ToDecimal(公务员补助支付) +
                        Convert.ToDecimal(财政支出) +
                        Convert.ToDecimal(大病支付费用) +
                        Convert.ToDecimal(民政救助支出) +
                        Convert.ToDecimal(民政特大救助支出) +
                        Convert.ToDecimal(商业补充保险支出) +
                        Convert.ToDecimal(医疗机构减免) +
                        Convert.ToDecimal(特优群体补充保障支付) +
                        Convert.ToDecimal(建国前老党员补充保障支付);
            }
        }

        [NotMapped]
        public decimal sum报销//住院发票使用，统筹之外报销金额
        {
            set { _sum报销 = value; }
            get
            {
                return Convert.ToDecimal(统筹支付金额) +
                     Convert.ToDecimal(大病支付费用) +
                     Convert.ToDecimal(民政救助支出) +
                     Convert.ToDecimal(民政特大救助支出) +
                     Convert.ToDecimal(商业补充保险支出) +
                     Convert.ToDecimal(医疗机构减免) +
                     Convert.ToDecimal(特优群体补充保障支付) +
                     Convert.ToDecimal(建国前老党员补充保障支付);
            }
        }

    }
}
