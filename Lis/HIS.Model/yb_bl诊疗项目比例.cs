//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class yb_bl诊疗项目比例
    {
        public bool IsChanged { get; set; }
        public string 诊疗项目编码 { get; set; }
        public string 项目名称 { get; set; }
        public string 收费项目等级 { get; set; }
        public string 单位 { get; set; }
        public string 职工住院自付比例 { get; set; }
        public string 职工门诊自付比例 { get; set; }
        public string 居民住院自付比例 { get; set; }
        public string 居民门诊自付比例 { get; set; }
        public string 离休自付比例 { get; set; }
        public string 最高限价 { get; set; }
        public string 一级医院限价 { get; set; }
        public string 二级医院限价 { get; set; }
        public string 三级医院限价 { get; set; }
        public string 备注 { get; set; }
        public string 项目内涵 { get; set; }
        public string 除外内容 { get; set; }
        public Nullable<System.DateTime> createTime { get; set; }
        public int ID { get; set; }
    }
}
