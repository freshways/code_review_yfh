//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class yunRis_医院设置
    {
        public bool IsChanged { get; set; }
        public decimal IID { get; set; }
        public string 医院编码 { get; set; }
        public string 医院名称 { get; set; }
        public string 备注 { get; set; }
        public string 医院等级 { get; set; }
        public Nullable<decimal> 排序 { get; set; }
        public string WebServerURL { get; set; }
        public string 数据中心编码 { get; set; }
        public string 能否接收会诊_ { get; set; }
        public string 是否自动审核 { get; set; }
        public string 报告提交自动审核时间 { get; set; }
        public string CSHYSBM { get; set; }
        public string CSHYSXM { get; set; }
    }
}
