//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class GY收费小项
    {
        public bool IsChanged { get; set; }
        public int ID { get; set; }
        public int 收费编码 { get; set; }
        public string 收费名称 { get; set; }
        public string 单位 { get; set; }
        public decimal 单价 { get; set; }
        public Nullable<int> 归并编码 { get; set; }
        public string 归并名称 { get; set; }
        public string 拼音代码 { get; set; }
        public bool 是否禁用 { get; set; }
        public int 单位编码 { get; set; }
        public string 新合编码 { get; set; }
        public Nullable<bool> 是否报销 { get; set; }
        public Nullable<decimal> 新合门诊报销比例12 { get; set; }
        public Nullable<int> 执行科室编码 { get; set; }
        public Nullable<int> 输液贴打印标志 { get; set; }
        public Nullable<bool> PACS检查项 { get; set; }
        public Nullable<bool> LIS检查项 { get; set; }
        public Nullable<bool> 可否刷次 { get; set; }
        public string remark { get; set; }
        public string 首页上报编码 { get; set; }
        public string 医保编码 { get; set; }
        public string 医保名称 { get; set; }
        public Nullable<System.DateTime> 上传时间 { get; set; }
        public string 医保项目类别 { get; set; }
        public Nullable<System.DateTime> 对照时间 { get; set; }
        public string 标准编码 { get; set; }
        public string 标准名称 { get; set; }
    }
}
