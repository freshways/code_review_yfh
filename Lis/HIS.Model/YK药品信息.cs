//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class YK药品信息
    {
        public bool IsChanged { get; set; }
        public decimal ID { get; set; }
        public string 招标编码 { get; set; }
        public decimal 药品序号 { get; set; }
        public string 药品名称 { get; set; }
        public string 药库规格 { get; set; }
        public string 药房规格 { get; set; }
        public string 药库单位 { get; set; }
        public string 药房单位 { get; set; }
        public string 药品产地 { get; set; }
        public int 包装转换 { get; set; }
        public Nullable<bool> 是否禁用 { get; set; }
        public string 拼音代码 { get; set; }
        public Nullable<decimal> 药库高储警戒 { get; set; }
        public Nullable<decimal> 药库低储警戒 { get; set; }
        public string 药品说明 { get; set; }
        public Nullable<decimal> 药品剂量 { get; set; }
        public string 剂量单位 { get; set; }
        public string 新合编码 { get; set; }
        public string 医保编码 { get; set; }
        public string 基药编码 { get; set; }
        public string 基药名称 { get; set; }
        public string 特殊药品 { get; set; }
        public string 财务分类 { get; set; }
        public string 药理分类 { get; set; }
        public string 药品剂型 { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public int 单位编码 { get; set; }
        public Nullable<int> 财务分类编码 { get; set; }
        public string 默认频次 { get; set; }
        public string 默认用法 { get; set; }
        public string remark { get; set; }
        public string 批准文号 { get; set; }
        public string 国标YPID { get; set; }
        public string 药品本位码 { get; set; }
        public string 基本库YPID4 { get; set; }
        public string 国标药品名称 { get; set; }
        public string 抗菌药物使用级别代码 { get; set; }
        public string 抗菌药物使用级别 { get; set; }
        public Nullable<decimal> 抗菌药物DDD值 { get; set; }
        public Nullable<bool> 是否基药 { get; set; }
    }
}
