//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class ZJ原料信息
    {
        public bool IsChanged { get; set; }
        public decimal id { get; set; }
        public string create_name { get; set; }
        public string create_by { get; set; }
        public Nullable<System.DateTime> create_date { get; set; }
        public string update_name { get; set; }
        public string update_by { get; set; }
        public Nullable<System.DateTime> update_date { get; set; }
        public string sys_org_code { get; set; }
        public string sys_company_code { get; set; }
        public string bpm_status { get; set; }
        public Nullable<int> 原料序号 { get; set; }
        public string 原料名称 { get; set; }
        public string 原料规格 { get; set; }
        public string 原料单位 { get; set; }
        public string 原料产地 { get; set; }
        public Nullable<int> 包装转换 { get; set; }
        public Nullable<bool> 是否禁用 { get; set; }
        public string 拼音代码 { get; set; }
        public Nullable<decimal> 高储警戒 { get; set; }
        public string 原料说明 { get; set; }
        public Nullable<decimal> 原料剂量 { get; set; }
        public string 剂量单位 { get; set; }
        public string 原料剂型 { get; set; }
        public Nullable<int> 单位编码 { get; set; }
        public string 分类名称 { get; set; }
    }
}
