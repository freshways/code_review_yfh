﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class PojoOutpatientDispensingContent : MF处方明细
    {
        public decimal 库存数量 { get; set; }
    }
}
