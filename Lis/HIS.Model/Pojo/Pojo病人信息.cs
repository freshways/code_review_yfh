﻿using PropertyChanged;
using System;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class Pojo病人信息 : ZY病人信息
    {
        private string _s年龄;
        private decimal _总费用;
        private decimal _预交款;
        private decimal _欠款;
        private string _主治医生姓名;
        private string _病区名称;
        private string _科室名称;
        private int _住院天数;

        public string 房间号 { get; set; }

        public string S年龄 { get => _s年龄; set => _s年龄 = value; }
        public string 病区名称 { get => _病区名称; set => _病区名称 = value; }
        public string 科室名称 { get => _科室名称; set => _科室名称 = value; }
        public decimal 总费用 { get => _总费用; set => _总费用 = value; }
        public decimal 预交款 { get => _预交款; set => _预交款 = value; }
        public decimal 欠款 { get => _欠款; set => _欠款 = value; }
        public string 主治医生姓名 { get => _主治医生姓名; set => _主治医生姓名 = value; }
        public int 床位编码 { get; set; }
        public int? 显示顺序 { get; set; }        

        public int 住院天数
        {
            get
            {
                return _住院天数;
            }
            set { _住院天数 = value; }
        }

      
    }
}
