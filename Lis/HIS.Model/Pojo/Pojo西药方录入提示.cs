﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class Pojo西药方录入提示 : YF库存信息
    {
        public Nullable<decimal> 药品剂量 { get; set; }
        public string 剂量单位 { get; set; }

        public string 默认频次 { get; set; }
        public string 默认用法 { get; set; }

        public string 拼音代码 { get; set; }
        public string 财务分类 { get; set; }
    }
}
