﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class Query药房库存提示Model
    {
        public Nullable<int> 药房编码 { get; set; }
        public string 财务分类 { get; set; }
        public Nullable<decimal> 药品序号 { get; set; }
        public string 药品名称 { get; set; }
        public string 药房规格 { get; set; }
        public string 药房单位 { get; set; }
        public string 药品产地 { get; set; }
        public string 药品批号 { get; set; }
        public Nullable<System.DateTime> 药品效期 { get; set; }
        public Nullable<decimal> 库存数量 { get; set; }
        public decimal 进价 { get; set; }
        public decimal 医院零售价 { get; set; }
        public decimal 卫生室零售价 { get; set; }
        public decimal YPID { get; set; }
        public string 默认频次 { get; set; }
        public string 默认用法 { get; set; }
        public decimal 药品剂量 { get; set; }
        public string 剂量单位 { get; set; }
        public string 拼音代码 { get; set; }
    }
}
