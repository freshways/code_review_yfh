﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
     [AddINotifyPropertyChangedInterface]
    public class PojoYK入库摘要 : YK入库摘要
    {
        private string _入库方式名称;
        private string _业务单位名称;

        public string 入库方式名称
        {
            get
            {
                return _入库方式名称;
            }

            set
            {
                _入库方式名称 = value;
            }
        }

        public string 业务单位名称
        {
            get
            {
                return _业务单位名称;
            }

            set
            {
                _业务单位名称 = value;
            }
        }
    }


}

