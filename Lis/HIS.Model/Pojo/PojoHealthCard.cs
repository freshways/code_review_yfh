﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    public class PojoHealthCard
    {
        /// <summary>
        /// 李涛
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 男
        /// </summary>
        public string gender { get; set; }
        /// <summary>
        /// 汉族
        /// </summary>
        public string nation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string birthday { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string idNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string phone1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string phone2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string qrCodeText { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string openId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string idType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string phid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string patid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string healthCardId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
    }
}
