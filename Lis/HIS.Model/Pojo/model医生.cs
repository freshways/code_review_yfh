﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.Model.Pojo
{
    public class model医生
    {
        private int _医生编码;
        private string _医生姓名;
        private string _拼音代码;
        private string _医生助记码;
        private int _科室编码;
        private string _科室名称;

        public int 医生编码
        {
            get
            {
                return _医生编码;
            }

            set
            {
                _医生编码 = value;
            }
        }

        public string 医生姓名
        {
            get
            {
                return _医生姓名;
            }

            set
            {
                _医生姓名 = value;
            }
        }

        public string 拼音代码
        {
            get
            {
                return _拼音代码;
            }

            set
            {
                _拼音代码 = value;
            }
        }

        public string 医生助记码
        {
            get
            {
                return _医生助记码;
            }

            set
            {
                _医生助记码 = value;
            }
        }

        public int 科室编码
        {
            get
            {
                return _科室编码;
            }

            set
            {
                _科室编码 = value;
            }
        }

        public string 科室名称
        {
            get
            {
                return _科室名称;
            }

            set
            {
                _科室名称 = value;
            }
        }
    }
}
