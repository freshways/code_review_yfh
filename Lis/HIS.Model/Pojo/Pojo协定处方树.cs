﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class Pojo协定处方树
    {
        public int KeyFieldName { get; set; }
        public int ParentFieldName { get; set; }
        public string 处方类型 { get; set; }
        public string 处方名称 { get; set; }
    }
}
