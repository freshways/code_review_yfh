﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    public class Pojo输液记录单
    {
        public int 组别 { get; set; }
        public string 医嘱内容 { get; set; }
        public string 医嘱用法 { get; set; }
        public string 医嘱频次 { get; set; }
        public string 门诊或住院号 { get; set; }
        public string 病人姓名 { get; set; }
        public string 床位名称 { get; set; }

        public int 横线标记 { get; set; }
    }
}
