﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
     [AddINotifyPropertyChangedInterface]
    public class model单位信息
    {
        private string _s医保单位编码;
        public string S医保单位编码
        {
            get
            {
                return _s医保单位编码;
            }

            set
            {
                _s医保单位编码 = value;
            }
        }


        private string _s组织机构代码 = "";
        public string S组织机构代码
        {
            get { return _s组织机构代码; }
            set { _s组织机构代码 = value; }
        }


        private string dwmc = "";
        public string sDwmc
        {
            get
            {
                return dwmc.Trim();
            }
            set
            {
                dwmc = value;
            }
        }

        private string _s单位级别;

        public string s单位级别
        {
            get { return _s单位级别; }
            set { _s单位级别 = value; }
        }
        private int dwid = 0;
        public int iDwid
        {
            get
            {
                return dwid;
            }
            set
            {
                dwid = value;
            }
        }
        private string _sRegion = "";//单位区划，设计时考虑医保接口区分沂水,在调用住院居民报销时使用

        public string sRegion
        {
            get { return _sRegion; }
            set { _sRegion = value; }
        }



        /// <summary>
        /// 行政区划
        /// </summary>
        private string xzqh = "";
        public string sXzqh
        {
            get
            {
                return xzqh.Trim();
            }
            set
            {
                xzqh = value;
            }
        }

        public string WxAppID
        {
            get
            {
                return _sWxAppID;
            }

            set
            {
                _sWxAppID = value;
            }
        }

        public string Mch_ID
        {
            get
            {
                return _Mch_ID;
            }

            set
            {
                _Mch_ID = value;
            }
        }

        private string _sWxAppID;


        private string _Mch_ID;
    }
}
