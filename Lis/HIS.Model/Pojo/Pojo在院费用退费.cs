﻿using PropertyChanged;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class Pojo在院费用退费 : ZY在院费用
    {
        private decimal _退费数量;
        private string _退费原因;
        public decimal 退费数量 { get => _退费数量; set => _退费数量 = value; }
        public string 退费原因 { get => _退费原因; set => _退费原因 = value; }
    }
}
