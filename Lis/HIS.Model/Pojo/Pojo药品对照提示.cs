﻿namespace HIS.Model.Pojo
{
    public class Pojo药品对照提示
    {
        private string _医保编码;
        private string _医保名称;
        private string _拼音助记码;
        private string _收费类别;
        private string _类别;
        private int _类别编码;
        private string _规格;
        private string _单位;


        public string 拼音助记码
        {
            get
            {
                return _拼音助记码;
            }

            set
            {
                _拼音助记码 = value;
            }
        }

        public string 收费类别
        {
            get
            {
                return _收费类别;
            }

            set
            {
                _收费类别 = value;
            }
        }



        public string 类别
        {
            get
            {
                return _类别;
            }

            set
            {
                _类别 = value;
            }
        }

        public int 类别编码 { get => _类别编码; set => _类别编码 = value; }
        public string 医保编码 { get => _医保编码; set => _医保编码 = value; }
        public string 医保名称 { get => _医保名称; set => _医保名称 = value; }
        public string 规格 { get => _规格; set => _规格 = value; }
        public string 单位 { get => _单位; set => _单位 = value; }
    }
}
