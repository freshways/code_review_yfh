﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class Pojo医嘱提示
    {
        public Nullable<decimal> 医嘱项目编码 { get; set; }
        public Nullable<int> 药房编码 { get; set; }
        public string 医嘱名称 { get; set; }
        public string 包装规格 { get; set; }
        public string 包装单位 { get; set; }
        public Nullable<decimal> 单价 { get; set; }
        public Nullable<decimal> 库存数量 { get; set; }
        public string 医嘱项目类型 { get; set; }
        public string 默认频次 { get; set; }
        public string 默认用法 { get; set; }
        public Nullable<decimal> 默认医嘱剂量 { get; set; }
        public string 剂量单位 { get; set; }
        public string 财务分类 { get; set; }
        public string 拼音代码 { get; set; }
        public string 职工住院自付比例 { get; set; }
        public string 职工门诊自付比例 { get; set; }
        public string 居民住院自付比例 { get; set; }
        public string 居民门诊自付比例 { get; set; }
        public string 离休自付比例 { get; set; }
        public string 一级医院限价 { get; set; }
        public string 二级医院限价 { get; set; }
        public string 三级医院限价 { get; set; }
        public string 最高限价 { get; set; }
        public string 使用限制和备注 { get; set; }
    }
}
