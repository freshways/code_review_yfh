﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
     [AddINotifyPropertyChangedInterface]
    public class PojoCipherPrescription
    {
        public int ParentFieldName { get; set; }
        public int KeyFieldName { get; set; }
        public string 处方名称 { get; set; }
        public string 处方类型 { get; set; }

    }
}
