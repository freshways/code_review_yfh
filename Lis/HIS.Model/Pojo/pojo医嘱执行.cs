﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
     [AddINotifyPropertyChangedInterface]
    public class Pojo医嘱执行:YS住院医嘱
    {
        public Nullable<decimal> 执行数量 { get; set; }
        public Nullable<int> 长期医嘱只执行一次 { get; set; }
        public Nullable<bool> PACS检查项 { get; set; }
        public Nullable<bool> LIS检查项 { get; set; }
        public Nullable<int> 执行科室编码 { get; set; }
        //public decimal lisID { get; set; }
        public bool 选择 { get; set; }
        public bool 是否当日入院 { get; set; }


    }
}
