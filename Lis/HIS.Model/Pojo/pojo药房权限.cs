﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class Pojo药房权限
    {
        public int 科室编码 { get; set; }
        public string 科室名称 { get; set; }
    }
}
