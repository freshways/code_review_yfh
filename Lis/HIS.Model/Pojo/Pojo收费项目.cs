﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class Pojo收费项目 : GY收费小项
    {
        public string 医保编码 { get; set; }
        public string 医保名称 { get; set; }
    }
}
