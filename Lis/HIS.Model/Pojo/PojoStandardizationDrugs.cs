﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class PojoStandardizationDrugs
    {
        public string YPID { get; set; }
        public string 品种名_中文_ { get; set; }
        public string 批准文号 { get; set; }
    }
}
