﻿using PropertyChanged;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class PojoOutpatientDispensing : MF处方摘要
    {
        public string 科室名称 { get; set; }
        public string 病人姓名 { get; set; }
        public string 处方录入 { get; set; }
        public string 诊治医生 { get; set; }
        public string 收款人 { get; set; }

        public string 性别 { get; set; }

        public int 年龄 { get; set; }

        public string 年龄单位 { get; set; }

        public string 临床诊断 { get; set; }

    }
}
