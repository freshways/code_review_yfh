﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.Model.Pojo
{
    public class model药库入库提示
    {
        private string _药品序号;
        private string _药品名称;
        private string _药库规格;
        private string _药库单位;
        private string _库存数量;
        private string _医院零售价;
        private string _卫生室零售价;
        private string _药品产地;
        private string _药品批号;
        private string _拼音代码;
        private string _进价;
        private string _加价率;
        private string _包装转换;
        private string _YPID;

        public string 药品序号
        {
            get
            {
                return _药品序号;
            }

            set
            {
                _药品序号 = value;
            }
        }

        public string 药品名称
        {
            get
            {
                return _药品名称;
            }

            set
            {
                _药品名称 = value;
            }
        }

        public string 药库规格
        {
            get
            {
                return _药库规格;
            }

            set
            {
                _药库规格 = value;
            }
        }

        public string 药库单位
        {
            get
            {
                return _药库单位;
            }

            set
            {
                _药库单位 = value;
            }
        }

        public string 库存数量
        {
            get
            {
                return _库存数量;
            }

            set
            {
                _库存数量 = value;
            }
        }

        public string 医院零售价
        {
            get
            {
                return _医院零售价;
            }

            set
            {
                _医院零售价 = value;
            }
        }

        public string 卫生室零售价
        {
            get
            {
                return _卫生室零售价;
            }

            set
            {
                _卫生室零售价 = value;
            }
        }

        public string 药品产地
        {
            get
            {
                return _药品产地;
            }

            set
            {
                _药品产地 = value;
            }
        }

        public string 药品批号
        {
            get
            {
                return _药品批号;
            }

            set
            {
                _药品批号 = value;
            }
        }

        public string 拼音代码
        {
            get
            {
                return _拼音代码;
            }

            set
            {
                _拼音代码 = value;
            }
        }

        public string 进价
        {
            get
            {
                return _进价;
            }

            set
            {
                _进价 = value;
            }
        }

        public string 加价率
        {
            get
            {
                return _加价率;
            }

            set
            {
                _加价率 = value;
            }
        }

        public string 包装转换
        {
            get
            {
                return _包装转换;
            }

            set
            {
                _包装转换 = value;
            }
        }

        public string YPID
        {
            get
            {
                return _YPID;
            }

            set
            {
                _YPID = value;
            }
        }
    }
}
