﻿using PropertyChanged;

namespace HIS.Model.Pojo
{
    [AddINotifyPropertyChangedInterface]
    public class PojoExaminationGroup
    {
        public int ParentFieldName { get; set; }
        public int KeyFieldName { get; set; }
        public string 套餐名称 { get; set; }

        public int 套餐编号 { get; set; }
        public int 显示顺序 { get; set; }



    }
}
