﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo.LIS
{
    /// <summary>
    /// 增加收费名称和收费单价的  化验项目类  
    /// 1、用于项目组合维护和调用时，显示费用，产生费用
    /// 2、扫码时记费
    /// </summary>
    public class PojoItem_GroupRel_SF : SAM_ITEM
    {
        public decimal 收费单价 { get; set; }
        public string 收费名称 { get; set; }
        public string fid { get; set; }
        public string fitem_id { get; set; }
        public string fgroup_id { get; set; }
    }
}
