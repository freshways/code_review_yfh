﻿using System;

namespace HIS.Model.Pojo.LIS
{
    [Serializable]
    public class Pojo条码打印
    {
        public string HIS申请单号 { get; set; }
        public string 打印状态 { get; set; }
        public string 病人类别 { get; set; }
        public string 病人姓名 { get; set; }
        public string 年龄 { get; set; }
        public string 年龄单位 { get; set; }
        public string 性别 { get; set; }
        public string 病人ID { get; set; }
        public string 床号 { get; set; }
        public string 住院号或门诊号 { get; set; }
        public string 检查项目 { get; set; }
        public string HIS记录ID { get; set; }
        public string 申请医生 { get; set; }
        public string 分类名称 { get; set; }
        public string 申请日期 { get; set; }
        public string 申请科室 { get; set; }
        public string 身份证号 { get; set; }

        public string 条码打印组合号 { get; set; }
    }
}
