﻿using System;

namespace HIS.Model.Pojo.LIS
{
    [Serializable]
    public class Pojo化验结果
    {
        public string 病人类别 { get; set; }
        public string 姓名 { get; set; }
        public string 性别 { get; set; }
        public string 年龄 { get; set; }
        public string 年龄单位 { get; set; }
        public string 科室 { get; set; }
        public string 床号 { get; set; }
        public string 申请医师 { get; set; }
        public string 检验者 { get; set; }
        public string 审核者 { get; set; }
        public string 化验项目 { get; set; }
        public string 条码 { get; set; }
        public string 结果ID { get; set; }
        public string 打印状态 { get; set; }
        public string 检验设备 { get; set; }
        public DateTime 检验日期 { get; set; }
        public DateTime 送检日期 { get; set; }
        public DateTime 报告日期 { get; set; }


    }
}
