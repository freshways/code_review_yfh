﻿namespace HIS.Model.Pojo
{
    public class Pojo诊疗对照提示
    {
        private string _诊疗项目编码;
        private string _项目名称;
        private string _拼音助记码;
        private string _收费类别;
        private string _收费项目等级;
        private string _类别;
        private int _类别编码;

        public string 诊疗项目编码
        {
            get
            {
                return _诊疗项目编码;
            }

            set
            {
                _诊疗项目编码 = value;
            }
        }

        public string 项目名称
        {
            get
            {
                return _项目名称;
            }

            set
            {
                _项目名称 = value;
            }
        }

        public string 拼音助记码
        {
            get
            {
                return _拼音助记码;
            }

            set
            {
                _拼音助记码 = value;
            }
        }

        public string 收费类别
        {
            get
            {
                return _收费类别;
            }

            set
            {
                _收费类别 = value;
            }
        }

        public string 收费项目等级
        {
            get
            {
                return _收费项目等级;
            }

            set
            {
                _收费项目等级 = value;
            }
        }

        public string 类别
        {
            get
            {
                return _类别;
            }

            set
            {
                _类别 = value;
            }
        }

        public int 类别编码 { get => _类别编码; set => _类别编码 = value; }
    }
}
