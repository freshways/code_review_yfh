﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.Model.Pojo
{
     [AddINotifyPropertyChangedInterface]
    public class Pojo医嘱打印 : YS住院医嘱
    {
        public byte[] 打印医生手签 { get; set; }
        public byte[] 打印护士手签 { get; set; }

        public string 打印开嘱日期 { get; set; }
        public string 打印开嘱时间 { get; set; }
        public string 打印医嘱内涵 { get; set; }
        public string 打印剂量数量 { get; set; }

        public string 打印剂量单位 { get; set; }
        public string 打印组别符号 { get; set; }
        public string 打印用法 { get; set; }
        public string 打印频次 { get; set; }
        public string 打印执行时间 { get; set; }

        public string 打印停嘱日期 { get; set; }
        public string 打印停嘱时间 { get; set; }

        public byte[] 打印停嘱医师签名 { get; set; }
        public byte[] 打印停嘱护士签名 { get; set; }

        public string 打印开嘱医生点点 { get; set; }
        public string 打印开嘱护士点点 { get; set; }
    }


    //续打是否显示支持，下列字段==true是，显示对应的信息
    //public bool 开嘱时间晚于续打时间 { get; set; }
    //public bool 执行时间晚于续打时间 { get; set; }
    //public bool 停嘱时间晚于续打时间 { get; set; }
}

