//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class SAM_REPORT
    {
        public string fsetting_id { get; set; }
        public string ftype_id { get; set; }
        public string finstr_id { get; set; }
        public string fcode { get; set; }
        public string fname { get; set; }
        public string fprinter_name { get; set; }
        public string fpaper_name { get; set; }
        public Nullable<double> fpage_width { get; set; }
        public Nullable<double> fpage_height { get; set; }
        public Nullable<double> fmargin_top { get; set; }
        public Nullable<double> fmargin_bottom { get; set; }
        public Nullable<double> fmargin_left { get; set; }
        public Nullable<double> fmargin_right { get; set; }
        public string forientation { get; set; }
        public Nullable<int> fuse_if { get; set; }
        public string forder_by { get; set; }
        public Nullable<int> frows { get; set; }
        public Nullable<int> fone_count { get; set; }
        public string fy_mess { get; set; }
        public string fry_mess { get; set; }
        public string fhight_mess { get; set; }
        public string flow_mess { get; set; }
        public string ferror_mess { get; set; }
        public string f1 { get; set; }
        public string f2 { get; set; }
        public string f3 { get; set; }
        public string f4 { get; set; }
        public string f5 { get; set; }
        public string f6 { get; set; }
        public string f7 { get; set; }
        public string f8 { get; set; }
        public string f9 { get; set; }
        public string f10 { get; set; }
        public string fremark { get; set; }
    }
}
