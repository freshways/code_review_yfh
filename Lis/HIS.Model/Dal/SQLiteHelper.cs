﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Configuration;
using System.Collections;

namespace WEISHENG.COMM.Db
{
    /// <summary>
    /// 
    /// </summary>
    public class SQLiteHelper
    {
        /// <summary>
        /// 连接字符串
        /// </summary>

        private static string _connectionString = @"Data Source=|DataDirectory|\jbb.db";

        public static string ConnectionString
        {
            get { return SQLiteHelper._connectionString; }
            set { SQLiteHelper._connectionString = value; }
        }
        //public static String connectionString = @"Data Source=|DataDirectory|\jbb.db";
        //<add   key="connectionString"   value="Data Source=|DataDirectory|\client967841181571626333.db"/>


        #region ExecuteNonQuery

        /// <summary>
        /// 执行数据库操作(新增、更新或删除)
        /// </summary>
        /// <param name="cmd">SqlCommand对象</param>
        /// <returns>所受影响的行数</returns>
        public static int ExecuteNonQuery(SQLiteCommand cmd)
        {
            int result = 0;
            using (SQLiteConnection con = new SQLiteConnection(_connectionString))
            {
                SQLiteTransaction trans = null;
                PrepareCommand(cmd, con, ref trans, true, cmd.CommandType, cmd.CommandText);
                try
                {
                    result = cmd.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
            return result;
        }

        /// <summary>
        /// 执行数据库操作(新增、更新或删除)
        /// </summary>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型-默认Text</param>
        /// <returns>所受影响的行数</returns>
        public static int ExecuteNonQuery(string commandText)
        {
            int result = 0;
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");
            SQLiteCommand cmd = new SQLiteCommand();
            using (SQLiteConnection con = new SQLiteConnection(_connectionString))
            {
                SQLiteTransaction trans = null;
                PrepareCommand(cmd, con, ref trans, true, CommandType.Text, commandText);
                try
                {
                    result = cmd.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
            return result;
        }

        /// <summary>
        /// 执行数据库操作(新增、更新或删除)
        /// </summary>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型-默认Text</param>
        /// <returns>所受影响的行数</returns>
        public static bool ExecuteNonQuery(ArrayList commandList)
        {
            bool result = true;
            using (SQLiteConnection con = new SQLiteConnection(_connectionString))
            {
                con.Open();
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = con;
                SQLiteTransaction trans = con.BeginTransaction();
                cmd.Transaction = trans;
                try
                {
                    for (int i = 0; i < commandList.Count; i++)
                    {
                        string str = commandList[i].ToString();
                        if (str.Trim().Length > 1)
                        {
                            cmd.CommandText = str;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    trans.Commit();
                    return result;
                }
                catch (Exception ex)
                {
                    result = false;
                    trans.Rollback();
                    throw ex;
                }
                return result;
            }
        }

        /// <summary>
        /// 执行数据库操作(新增、更新或删除)
        /// </summary>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型</param>
        /// <param name="cmdParms">SQL参数对象</param>
        /// <returns>所受影响的行数</returns>
        public static int ExecuteNonQuery(string commandText, params SQLiteParameter[] cmdParms)
        {
            int result = 0;
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");

            SQLiteCommand cmd = new SQLiteCommand();
            using (SQLiteConnection con = new SQLiteConnection(_connectionString))
            {
                SQLiteTransaction trans = null;
                PrepareCommand(cmd, con, ref trans, true, CommandType.Text, commandText, cmdParms);
                try
                {
                    result = cmd.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
            return result;
        }

        #endregion

        #region ExecuteScalar

        public static object ExecuteScalar(string commandText)
        {
            object result = 0;
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");
            SQLiteCommand cmd = new SQLiteCommand();
            using (SQLiteConnection con = new SQLiteConnection(_connectionString))
            {
                SQLiteTransaction trans = null;
                PrepareCommand(cmd, con, ref trans, true, CommandType.Text, commandText);
                try
                {
                    result = cmd.ExecuteScalar();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
            return result;
        }

        /// <summary>
        /// 执行数据库操作(新增、更新或删除)同时返回执行后查询所得的第1行第1列数据
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="cmd">SqlCommand对象</param>
        /// <returns>查询所得的第1行第1列数据</returns>
        public static object ExecuteScalar(string connectionString, SQLiteCommand cmd)
        {
            object result = 0;
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException("connectionString");
            using (SQLiteConnection con = new SQLiteConnection(connectionString))
            {
                SQLiteTransaction trans = null;
                PrepareCommand(cmd, con, ref trans, true, cmd.CommandType, cmd.CommandText);
                try
                {
                    result = cmd.ExecuteScalar();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
            return result;
        }

        /// <summary>
        /// 执行数据库操作(新增、更新或删除)同时返回执行后查询所得的第1行第1列数据
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型</param>
        /// <returns>查询所得的第1行第1列数据</returns>
        public static object ExecuteScalar(string connectionString, string commandText, CommandType commandType)
        {
            object result = 0;
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException("connectionString");
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");
            SQLiteCommand cmd = new SQLiteCommand();
            using (SQLiteConnection con = new SQLiteConnection(connectionString))
            {
                SQLiteTransaction trans = null;
                PrepareCommand(cmd, con, ref trans, true, commandType, commandText);
                try
                {
                    result = cmd.ExecuteScalar();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
            return result;
        }

        /// <summary>
        /// 执行数据库操作(新增、更新或删除)同时返回执行后查询所得的第1行第1列数据
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型</param>
        /// <param name="cmdParms">SQL参数对象</param>
        /// <returns>查询所得的第1行第1列数据</returns>
        public static object ExecuteScalar(string connectionString, string commandText, CommandType commandType, params SQLiteParameter[] cmdParms)
        {
            object result = 0;
            if (connectionString == null || connectionString.Length == 0)
                throw new ArgumentNullException("connectionString");
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");

            SQLiteCommand cmd = new SQLiteCommand();
            using (SQLiteConnection con = new SQLiteConnection(connectionString))
            {
                SQLiteTransaction trans = null;
                PrepareCommand(cmd, con, ref trans, true, commandType, commandText);
                try
                {
                    result = cmd.ExecuteScalar();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
            return result;
        }
        #endregion

        #region ExecuteReader

        /// <summary>
        /// 执行数据库查询，返回SqlDataReader对象
        /// </summary>
        /// <param name="cmd">SqlCommand对象</param>
        /// <returns>SqlDataReader对象</returns>
        public static DbDataReader ExecuteReader(SQLiteCommand cmd)
        {
            DbDataReader reader = null;
            if (_connectionString == null || _connectionString.Length == 0)
                throw new ArgumentNullException("connectionString");

            SQLiteConnection con = new SQLiteConnection(_connectionString);
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, cmd.CommandType, cmd.CommandText);
            try
            {
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 执行数据库查询，返回SqlDataReader对象
        /// </summary>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型-默认sql</param>
        /// <returns>SqlDataReader对象</returns>
        public static DbDataReader ExecuteReader(string commandText)
        {
            DbDataReader reader = null;
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");

            SQLiteConnection con = new SQLiteConnection(_connectionString);
            SQLiteCommand cmd = new SQLiteCommand();
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, CommandType.Text, commandText);
            try
            {
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }

        /// <summary>
        /// 执行数据库查询，返回SqlDataReader对象
        /// </summary>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型</param>
        /// <param name="cmdParms">SQL参数对象</param>
        /// <returns>SqlDataReader对象</returns>
        public static DbDataReader ExecuteReader(string commandText, params SQLiteParameter[] cmdParms)
        {
            DbDataReader reader = null;
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");

            SQLiteConnection con = new SQLiteConnection(_connectionString);
            SQLiteCommand cmd = new SQLiteCommand();
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, CommandType.Text, commandText, cmdParms);
            try
            {
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reader;
        }
        #endregion

        #region ExecuteDataSet
        /// <summary>
        /// 执行数据库查询，返回DataSet对象
        /// </summary>
        /// <param name="cmd">SqlCommand对象</param>
        /// <returns>DataSet对象</returns>
        public static DataSet ExecuteDataSet(SQLiteCommand cmd)
        {
            DataSet ds = new DataSet();
            SQLiteConnection con = new SQLiteConnection(_connectionString);
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, cmd.CommandType, cmd.CommandText);
            try
            {
                SQLiteDataAdapter sda = new SQLiteDataAdapter(cmd);
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmd.Connection != null)
                {
                    if (cmd.Connection.State == ConnectionState.Open)
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return ds;
        }

        /// <summary>
        /// 执行数据库查询，返回DataSet对象
        /// </summary>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型</param>
        /// <returns>DataSet对象</returns>
        public static DataSet ExecuteDataSet(string commandText)
        {
            if (_connectionString == null || _connectionString.Length == 0)
                throw new ArgumentNullException("connectionString");
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");
            DataSet ds = new DataSet();
            SQLiteConnection con = new SQLiteConnection(_connectionString);
            SQLiteCommand cmd = new SQLiteCommand();
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, CommandType.Text, commandText);
            try
            {
                SQLiteDataAdapter sda = new SQLiteDataAdapter(cmd);
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null)
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            return ds;
        }

        /// <summary>
        /// 执行数据库查询，返回DataSet对象
        /// </summary>
        /// <param name="commandText">执行语句或存储过程名</param>
        /// <param name="commandType">执行类型</param>
        /// <param name="cmdParms">SQL参数对象</param>
        /// <returns>DataSet对象</returns>
        public static DataSet ExecuteDataSet(string commandText, CommandType commandType, params SQLiteParameter[] cmdParms)
        {
            if (_connectionString == null || _connectionString.Length == 0)
                throw new ArgumentNullException("connectionString");
            if (commandText == null || commandText.Length == 0)
                throw new ArgumentNullException("commandText");
            DataSet ds = new DataSet();
            SQLiteConnection con = new SQLiteConnection(_connectionString);
            SQLiteCommand cmd = new SQLiteCommand();
            SQLiteTransaction trans = null;
            PrepareCommand(cmd, con, ref trans, false, commandType, commandText, cmdParms);
            try
            {
                SQLiteDataAdapter sda = new SQLiteDataAdapter(cmd);
                sda.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null)
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            return ds;
        }
        #endregion

        /// <summary>
        /// 通用分页查询方法
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="strColumns">查询字段名</param>
        /// <param name="strWhere">where条件</param>
        /// <param name="strOrder">排序条件</param>
        /// <param name="pageSize">每页数据数量</param>
        /// <param name="currentIndex">当前页数</param>
        /// <param name="recordOut">数据总量</param>
        /// <returns>DataTable数据表</returns>
        public static DataTable SelectPaging(string tableName, string strColumns, string strWhere, string strOrder,
            int pageSize, int currentIndex, out int recordOut)
        {
            DataTable dt = new DataTable();
            recordOut = Convert.ToInt32(ExecuteScalar(_connectionString, "select count(*) from " + tableName, CommandType.Text));
            string pagingTemplate = "select {0} from {1} where {2} order by {3} limit {4} offset {5} ";
            int offsetCount = (currentIndex - 1) * pageSize;
            string commandText = String.Format(pagingTemplate, strColumns, tableName, strWhere, strOrder, pageSize.ToString(), offsetCount.ToString());
            using (DbDataReader reader = ExecuteReader(commandText))
            {
                if (reader != null)
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }

        /// <summary>
        /// 预处理Command对象,数据库链接,事务,需要执行的对象,参数等的初始化
        /// </summary>
        /// <param name="cmd">Command对象</param>
        /// <param name="conn">Connection对象</param>
        /// <param name="trans">Transcation对象</param>
        /// <param name="useTrans">是否使用事务</param>
        /// <param name="cmdType">SQL字符串执行类型</param>
        /// <param name="cmdText">SQL Text</param>
        /// <param name="cmdParms">SQLiteParameters to use in the command</param>
        private static void PrepareCommand(SQLiteCommand cmd, SQLiteConnection conn, ref SQLiteTransaction trans, bool useTrans,
            CommandType cmdType, string cmdText, params SQLiteParameter[] cmdParms)
        {

            if (conn.State != ConnectionState.Open)
                conn.Open();

            cmd.Connection = conn;
            cmd.CommandText = cmdText;

            if (useTrans)
            {
                trans = conn.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
            }

            cmd.CommandType = cmdType;

            if (cmdParms != null)
            {
                foreach (SQLiteParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }


        #region ExecutedataAdapter 废弃的方法

        /*
         * 本来想通过dataAdapter直接更新数据来提高效率，经过反复测试，还是直接拼sql执行最快
         * 可能是我写的有问题，怎么优化还是不如硬编码来的快
         * 最后决定放弃这个方法，换种思路来实现大数据量的传输
         * 思路：1.下载数据不超过1000条，在用到数据时进行下载（即：本地查询返回空的时候，通过远程连接服务器查询）
         *          2.既然原创服务器连接查询了，就要优化传输，压缩数据
         */

        /// <summary>
        /// 用SQLiteDataAdapter更新数据
        /// </summary>
        /// <param name="dt">datatable</param>
        /// <returns></returns>
        public static bool test(DataTable dt)
        {

            //对于大量的插入操作,可以利用一个空的DataTable加入要插入的行,达到一定数量提交后清空该表就行了,
            //实现起来并不算复杂:
            DateTime begin = DateTime.Now;
            using (SQLiteConnection conn = new SQLiteConnection(_connectionString))
            {
                try
                {

                    conn.Open();

                    SQLiteDataAdapter sd = new SQLiteDataAdapter();
                    SQLiteTransaction trans = conn.BeginTransaction();
                    sd.SelectCommand = new SQLiteCommand("select c_owner,c_style,c_color,c_size,c_code,c_quantity,c_name,c_sizegroupid,Delivery_Price,c_version from BASIC_QUANTITY where 1=0 ", conn);
                    sd.InsertCommand = new SQLiteCommand("replace into BASIC_QUANTITY(c_owner,c_style,c_color,c_size,c_code,c_quantity,c_name,c_sizegroupid,Delivery_Price,c_version) "
                                    + " values (@c_owner,@c_style,@c_color,@c_size,@c_code,@c_quantity,@c_name,@c_sizegroupid,@Delivery_Price,@c_version);", conn);

                    sd.InsertCommand.Parameters.Add("@c_owner", DbType.Int64, 18, "c_owner");
                    sd.InsertCommand.Parameters.Add("@c_style", DbType.String, 50, "c_style");
                    sd.InsertCommand.Parameters.Add("@c_color", DbType.String, 50, "c_color");
                    sd.InsertCommand.Parameters.Add("@c_size", DbType.String, 50, "c_size");
                    sd.InsertCommand.Parameters.Add("@c_code", DbType.String, 50, "c_code");
                    sd.InsertCommand.Parameters.Add("@c_quantity", DbType.Double, 18, "c_quantity");
                    sd.InsertCommand.Parameters.Add("@c_name", DbType.String, 100, "c_name");
                    sd.InsertCommand.Parameters.Add("@c_sizegroupid", DbType.String, 50, "c_sizegroupid");
                    sd.InsertCommand.Parameters.Add("@Delivery_Price", DbType.Double, 18, "Delivery_Price");
                    sd.InsertCommand.Parameters.Add("@c_version", DbType.Int32, 10, "c_version");
                    sd.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    sd.UpdateBatchSize = 1;

                    DataSet dataset = new DataSet();
                    sd.Fill(dataset);
                    Random r = new Random(1000);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //object[] row = { "DEVID" + i, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), r.Next(1, 1000) };
                        object[] row = { dt.Rows[i]["c_owner"].ToString(), dt.Rows[i]["c_style"].ToString(), dt.Rows[i]["c_color"].ToString(), dt.Rows[i]["c_size"].ToString(),
                            dt.Rows[i]["c_code"].ToString(),dt.Rows[i]["c_quantity"].ToString(),dt.Rows[i]["c_name"].ToString(),dt.Rows[i]["c_sizegroupid"].ToString(),
                            dt.Rows[i]["Delivery_Price"].ToString(),i+1};
                        dataset.Tables[0].Rows.Add(row);
                        if (i % 300 == 0)
                        {
                            sd.Update(dataset.Tables[0]);
                            dataset.Tables[0].Clear();
                        }
                    }
                    sd.Update(dataset.Tables[0]);
                    dataset.Tables[0].Clear();
                    sd.Dispose();
                    dataset.Dispose();
                    trans.Commit();
                    conn.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    throw ex;
                    return false;
                }
            }
        }

        /// <summary>
        /// 用SQLiteDataAdapter更新数据
        /// </summary>
        /// <param name="strColomns">字段名</param>
        /// <param name="countent">带@的字段作为替换值变量</param>
        /// <param name="strTableName">表名</param>
        /// <param name="dt">datatable</param>
        /// <returns></returns>
        public static bool ExecutedataAdapter(String strColomns, String countent, String strTableName, DataTable dt)
        {
            String selTex = "select {0} from {1} where 1=0 ";
            selTex = String.Format(selTex, strColomns, strTableName);
            string InsTex = "replace into {0} ({1}) values ({2})";
            InsTex = string.Format(InsTex, strTableName, strColomns, countent);
            //对于大量的插入操作,可以利用一个空的DataTable加入要插入的行,达到一定数量提交后清空该表就行了,
            //实现起来并不算复杂:
            DateTime begin = DateTime.Now;
            using (SQLiteConnection conn = new SQLiteConnection(_connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter sd = new SQLiteDataAdapter();
                    SQLiteTransaction trans = conn.BeginTransaction();
                    sd.SelectCommand = new SQLiteCommand(selTex, conn);
                    sd.InsertCommand = new SQLiteCommand(InsTex, conn);
                    String[] strcon = strColomns.Split(new char[] { ',' });
                    for (int i = 0; i < strcon.Length; i++)
                    {
                        sd.InsertCommand.Parameters.Add("@" + strcon[i].ToString(), DbType.AnsiString, 100, strcon[i].ToString());
                    }
                    sd.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
                    sd.UpdateBatchSize = 1;

                    DataSet dataset = new DataSet();
                    sd.Fill(dataset);
                    Random r = new Random(1000);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        object[] row = { dt.Rows[i]["c_owner"].ToString(), dt.Rows[i]["c_id"].ToString(), dt.Rows[i]["c_vip_id"].ToString(), dt.Rows[i]["c_type"].ToString() };
                        //dataset.Tables[0].Rows.Add(dt.Rows[i].ItemArray);
                        dataset.Tables[0].Rows.Add(row);
                        if (i % 300 == 0)
                        {
                            sd.Update(dataset.Tables[0]);
                            dataset.Tables[0].Clear();
                        }
                    }
                    sd.Update(dataset.Tables[0]);
                    dataset.Tables[0].Clear();
                    sd.Dispose();
                    dataset.Dispose();
                    trans.Commit();
                    conn.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    return false;
                }
            }
        }

        #endregion

    }
}
