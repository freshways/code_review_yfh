//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class YS住院医嘱历史
    {
        public bool IsChanged { get; set; }
        public decimal ID { get; set; }
        public Nullable<decimal> ZYID { get; set; }
        public string 医嘱类型 { get; set; }
        public Nullable<int> 组别 { get; set; }
        public string 项目类型 { get; set; }
        public Nullable<int> 项目编码 { get; set; }
        public string 项目名称 { get; set; }
        public Nullable<decimal> 剂量数量 { get; set; }
        public string 剂量单位 { get; set; }
        public Nullable<decimal> 医嘱数量 { get; set; }
        public string 频次名称 { get; set; }
        public string 用法名称 { get; set; }
        public Nullable<int> 开嘱医生编码 { get; set; }
        public Nullable<System.DateTime> 开嘱时间 { get; set; }
        public Nullable<int> 开嘱执行者编码 { get; set; }
        public Nullable<int> 开嘱核对者编码 { get; set; }
        public Nullable<System.DateTime> 执行时间 { get; set; }
        public Nullable<int> 停嘱医生编码 { get; set; }
        public Nullable<int> 停嘱执行者编码 { get; set; }
        public Nullable<int> 停嘱核对者编码 { get; set; }
        public Nullable<System.DateTime> 停嘱时间 { get; set; }
        public Nullable<decimal> 单日总量 { get; set; }
        public Nullable<decimal> 频次数量 { get; set; }
        public Nullable<System.DateTime> createTime { get; set; }
        public Nullable<int> 临床路径ID { get; set; }
        public string 组别符号 { get; set; }
        public Nullable<decimal> 库存数量 { get; set; }
        public string 药房规格 { get; set; }
        public string 药房单位 { get; set; }
        public Nullable<int> 药房编码 { get; set; }
        public Nullable<int> 更新标志 { get; set; }
        public Nullable<decimal> 单次数量 { get; set; }
        public string 财务分类 { get; set; }
        public Nullable<int> 页号 { get; set; }
        public Nullable<int> 行号 { get; set; }
        public Nullable<int> 已打印标志 { get; set; }
        public Nullable<int> 子嘱ID { get; set; }
        public string 医嘱内涵 { get; set; }
        public string 医嘱备注 { get; set; }
        public string 停嘱原因 { get; set; }
        public string 作废原因 { get; set; }
        public Nullable<decimal> 在院医嘱ID { get; set; }
        public string 医嘱套餐编码 { get; set; }
        public string 用药状态 { get; set; }
        public string 医嘱验证标记 { get; set; }
        public string pacs申请单号 { get; set; }
        public Nullable<bool> 会诊 { get; set; }
        public Nullable<bool> 出院带药 { get; set; }
        public Nullable<bool> 自备 { get; set; }
        public Nullable<bool> 不打印 { get; set; }
        public Nullable<bool> 持续 { get; set; }
        public Nullable<bool> 紧急 { get; set; }
        public Nullable<bool> 抢救 { get; set; }
        public string lis申请单号 { get; set; }
    }
}
