//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class ZJ成品入库摘要
    {
        public bool IsChanged { get; set; }
        public decimal ID { get; set; }
        public int 入库库方式 { get; set; }
        public decimal 入库单号 { get; set; }
        public Nullable<int> 业务单位编码 { get; set; }
        public Nullable<System.DateTime> 入库日期 { get; set; }
        public decimal 入库成本金额 { get; set; }
        public int 单位编码 { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string 入库操作员姓名 { get; set; }
        public Nullable<int> visible { get; set; }
    }
}
