//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class YF盘点摘要
    {
        public bool IsChanged { get; set; }
        public decimal ID { get; set; }
        public Nullable<int> 药房编码 { get; set; }
        public int 单据号 { get; set; }
        public Nullable<decimal> 新进价金额 { get; set; }
        public Nullable<decimal> 老进价金额 { get; set; }
        public Nullable<decimal> 老零售价金额 { get; set; }
        public Nullable<decimal> 新零售价金额 { get; set; }
        public Nullable<System.DateTime> 盘点时间 { get; set; }
        public string 备注 { get; set; }
        public string 操作员 { get; set; }
        public Nullable<int> 单位编码 { get; set; }
        public Nullable<System.DateTime> createTime { get; set; }
        public Nullable<int> visible { get; set; }
    }
}
