//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class ZY预交款
    {
        public bool IsChanged { get; set; }
        public decimal ID { get; set; }
        public Nullable<decimal> ZYID { get; set; }
        public Nullable<System.DateTime> 交款时间 { get; set; }
        public Nullable<decimal> 交款额 { get; set; }
        public Nullable<decimal> 收据号 { get; set; }
        public Nullable<System.DateTime> 结账时间 { get; set; }
        public Nullable<System.DateTime> 汇总时间 { get; set; }
        public Nullable<int> 收款人编码 { get; set; }
        public Nullable<int> 汇总人编码 { get; set; }
        public int 单位编码 { get; set; }
        public int 分院编码 { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<System.DateTime> 月结时间 { get; set; }
        public string 第三方订单编号 { get; set; }
        public string 付款方式 { get; set; }
        public string channelType { get; set; }
        public string tradeMode { get; set; }
        public string traceNo { get; set; }
        public string outChargeNO { get; set; }
        public string 单据状态 { get; set; }
        public string 备注 { get; set; }
        public string 订单号 { get; set; }
    }
}
