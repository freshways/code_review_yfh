//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class SAM_ITEM_VALUE
    {
        public string fvalue_id { get; set; }
        public string fitem_id { get; set; }
        public string fvalue { get; set; }
        public string fvalue_flag { get; set; }
        public string forder_by { get; set; }
    }
}
