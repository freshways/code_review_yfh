//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HIS.Model
{
    using PropertyChanged;  
    using System;
    using System.Collections.Generic;
    
    [AddINotifyPropertyChangedInterface]
    public partial class PubStationPaperSetting
    {
        public bool IsChanged { get; set; }
        public int ID { get; set; }
        public string StationID { get; set; }
        public string StationName { get; set; }
        public string PrinterName { get; set; }
        public string PaperName { get; set; }
        public int PaperWidth { get; set; }
        public int PaperHeight { get; set; }
        public int MarginTop { get; set; }
        public int MarginBottom { get; set; }
        public int MarginLeft { get; set; }
        public int MarginRight { get; set; }
        public bool IsPreView { get; set; }
        public string Remark { get; set; }
        public bool CanPrint { get; set; }
        public string InvokeModuleName { get; set; }
        public bool Landscape { get; set; }
    }
}
