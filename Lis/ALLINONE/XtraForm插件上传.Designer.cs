﻿using DevExpress.XtraEditors;
using DevExpress.XtraLayout;

namespace ALLINONE
{
    partial class XtraForm插件上传
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm插件上传));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEdit插件包类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.checkEdit默认版本 = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton选择压缩文件 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit压缩包位置 = new DevExpress.XtraEditors.TextEdit();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.simpleButton上传 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton主程序选择 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit插件位置 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit插件包类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit默认版本.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit压缩包位置.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit插件位置.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.comboBoxEdit插件包类型);
            this.layoutControl1.Controls.Add(this.checkEdit默认版本);
            this.layoutControl1.Controls.Add(this.simpleButton选择压缩文件);
            this.layoutControl1.Controls.Add(this.textEdit压缩包位置);
            this.layoutControl1.Controls.Add(this.richTextBox1);
            this.layoutControl1.Controls.Add(this.simpleButton上传);
            this.layoutControl1.Controls.Add(this.simpleButton主程序选择);
            this.layoutControl1.Controls.Add(this.textEdit插件位置);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1670, 324, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1010, 498);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // comboBoxEdit插件包类型
            // 
            this.comboBoxEdit插件包类型.EditValue = "正式包";
            this.comboBoxEdit插件包类型.Location = new System.Drawing.Point(717, 12);
            this.comboBoxEdit插件包类型.Name = "comboBoxEdit插件包类型";
            this.comboBoxEdit插件包类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit插件包类型.Properties.Items.AddRange(new object[] {
            "正式包",
            "测试包",
            "基础包",
            "升级工具"});
            this.comboBoxEdit插件包类型.Size = new System.Drawing.Size(77, 20);
            this.comboBoxEdit插件包类型.StyleController = this.layoutControl1;
            this.comboBoxEdit插件包类型.TabIndex = 12;
            // 
            // checkEdit默认版本
            // 
            this.checkEdit默认版本.Location = new System.Drawing.Point(798, 12);
            this.checkEdit默认版本.Name = "checkEdit默认版本";
            this.checkEdit默认版本.Properties.Caption = "默认版本";
            this.checkEdit默认版本.Size = new System.Drawing.Size(70, 19);
            this.checkEdit默认版本.StyleController = this.layoutControl1;
            this.checkEdit默认版本.TabIndex = 11;
            // 
            // simpleButton选择压缩文件
            // 
            this.simpleButton选择压缩文件.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton选择压缩文件.ImageOptions.Image")));
            this.simpleButton选择压缩文件.Location = new System.Drawing.Point(580, 12);
            this.simpleButton选择压缩文件.Name = "simpleButton选择压缩文件";
            this.simpleButton选择压缩文件.Size = new System.Drawing.Size(100, 22);
            this.simpleButton选择压缩文件.StyleController = this.layoutControl1;
            this.simpleButton选择压缩文件.TabIndex = 6;
            this.simpleButton选择压缩文件.Text = "选择压缩文件";
            this.simpleButton选择压缩文件.Click += new System.EventHandler(this.simpleButton选择压缩文件_Click);
            // 
            // textEdit压缩包位置
            // 
            this.textEdit压缩包位置.Location = new System.Drawing.Point(440, 12);
            this.textEdit压缩包位置.Name = "textEdit压缩包位置";
            this.textEdit压缩包位置.Size = new System.Drawing.Size(136, 20);
            this.textEdit压缩包位置.StyleController = this.layoutControl1;
            this.textEdit压缩包位置.TabIndex = 10;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 38);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(986, 448);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "插件详情";
            // 
            // simpleButton上传
            // 
            this.simpleButton上传.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton上传.ImageOptions.Image")));
            this.simpleButton上传.Location = new System.Drawing.Point(920, 12);
            this.simpleButton上传.Name = "simpleButton上传";
            this.simpleButton上传.Size = new System.Drawing.Size(78, 22);
            this.simpleButton上传.StyleController = this.layoutControl1;
            this.simpleButton上传.TabIndex = 6;
            this.simpleButton上传.Text = "上传";
            this.simpleButton上传.Click += new System.EventHandler(this.simpleButton上传_Click);
            // 
            // simpleButton主程序选择
            // 
            this.simpleButton主程序选择.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton主程序选择.ImageOptions.Image")));
            this.simpleButton主程序选择.Location = new System.Drawing.Point(267, 12);
            this.simpleButton主程序选择.Name = "simpleButton主程序选择";
            this.simpleButton主程序选择.Size = new System.Drawing.Size(78, 22);
            this.simpleButton主程序选择.StyleController = this.layoutControl1;
            this.simpleButton主程序选择.TabIndex = 5;
            this.simpleButton主程序选择.Text = "插件信息";
            this.simpleButton主程序选择.Click += new System.EventHandler(this.simpleButton选择_Click);
            // 
            // textEdit插件位置
            // 
            this.textEdit插件位置.Location = new System.Drawing.Point(103, 12);
            this.textEdit插件位置.Name = "textEdit插件位置";
            this.textEdit插件位置.Size = new System.Drawing.Size(160, 20);
            this.textEdit插件位置.StyleController = this.layoutControl1;
            this.textEdit插件位置.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem6});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1010, 498);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit插件位置;
            this.layoutControlItem1.CustomizationFormText = "插件位置:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(255, 26);
            this.layoutControlItem1.Text = "插件主程序位置:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(88, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.richTextBox1;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(990, 452);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton上传;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(908, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(82, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit压缩包位置;
            this.layoutControlItem4.CustomizationFormText = "插件压缩包位置:";
            this.layoutControlItem4.Location = new System.Drawing.Point(337, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(231, 26);
            this.layoutControlItem4.Text = "插件压缩包位置:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(88, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton主程序选择;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(255, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(82, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton选择压缩文件;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(568, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(104, 26);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(860, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(48, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.checkEdit默认版本;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(786, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(74, 26);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.comboBoxEdit插件包类型;
            this.layoutControlItem8.Location = new System.Drawing.Point(672, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(114, 26);
            this.layoutControlItem8.Text = "类型:";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(28, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // XtraForm插件上传
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 498);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraForm插件上传";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "XtraForm插件上传";
            this.Load += new System.EventHandler(this.XtraForm插件上传_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit插件包类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit默认版本.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit压缩包位置.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit插件位置.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private LayoutControl layoutControl1;
        private LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private SimpleButton simpleButton上传;
        private SimpleButton simpleButton主程序选择;
        private TextEdit textEdit插件位置;
        private LayoutControlItem layoutControlItem1;
        private LayoutControlItem layoutControlItem2;
        private LayoutControlItem layoutControlItem3;
        private LayoutControlItem layoutControlItem5;
        private TextEdit textEdit压缩包位置;
        private LayoutControlItem layoutControlItem4;
        private SimpleButton simpleButton选择压缩文件;
        private LayoutControlItem layoutControlItem6;
        private EmptySpaceItem emptySpaceItem1;
        private CheckEdit checkEdit默认版本;
        private LayoutControlItem layoutControlItem7;
        private ComboBoxEdit comboBoxEdit插件包类型;
        private LayoutControlItem layoutControlItem8;
    }
}