﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;

namespace ALLINONE
{
    partial class XtraForm插件仓库
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm插件仓库));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton下载到默认位置 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton更新包 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton新增包 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton配置 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton关闭 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton下载 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl插件下载 = new DevExpress.XtraGrid.GridControl();
            this.gridView插件下载 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl插件下载)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView插件下载)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton下载到默认位置);
            this.layoutControl1.Controls.Add(this.simpleButton更新包);
            this.layoutControl1.Controls.Add(this.simpleButton新增包);
            this.layoutControl1.Controls.Add(this.simpleButton配置);
            this.layoutControl1.Controls.Add(this.simpleButton刷新);
            this.layoutControl1.Controls.Add(this.simpleButton关闭);
            this.layoutControl1.Controls.Add(this.simpleButton下载);
            this.layoutControl1.Controls.Add(this.gridControl插件下载);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(893, 221, 448, 507);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(753, 322);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton下载到默认位置
            // 
            this.simpleButton下载到默认位置.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton下载到默认位置.ImageOptions.Image")));
            this.simpleButton下载到默认位置.Location = new System.Drawing.Point(571, 288);
            this.simpleButton下载到默认位置.Name = "simpleButton下载到默认位置";
            this.simpleButton下载到默认位置.Size = new System.Drawing.Size(112, 22);
            this.simpleButton下载到默认位置.StyleController = this.layoutControl1;
            this.simpleButton下载到默认位置.TabIndex = 10;
            this.simpleButton下载到默认位置.Text = "下载到默认位置";
            this.simpleButton下载到默认位置.Click += new System.EventHandler(this.simpleButton下载到默认位置_Click);
            // 
            // simpleButton更新包
            // 
            this.simpleButton更新包.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton更新包.ImageOptions.Image")));
            this.simpleButton更新包.Location = new System.Drawing.Point(331, 288);
            this.simpleButton更新包.Name = "simpleButton更新包";
            this.simpleButton更新包.Size = new System.Drawing.Size(104, 22);
            this.simpleButton更新包.StyleController = this.layoutControl1;
            this.simpleButton更新包.TabIndex = 9;
            this.simpleButton更新包.Text = "更当前新包";
            this.simpleButton更新包.Click += new System.EventHandler(this.simpleButton更新包_Click);
            // 
            // simpleButton新增包
            // 
            this.simpleButton新增包.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton新增包.ImageOptions.Image")));
            this.simpleButton新增包.Location = new System.Drawing.Point(439, 288);
            this.simpleButton新增包.Name = "simpleButton新增包";
            this.simpleButton新增包.Size = new System.Drawing.Size(63, 22);
            this.simpleButton新增包.StyleController = this.layoutControl1;
            this.simpleButton新增包.TabIndex = 8;
            this.simpleButton新增包.Text = "新增包";
            this.simpleButton新增包.Click += new System.EventHandler(this.simpleButton上传_Click);
            // 
            // simpleButton配置
            // 
            this.simpleButton配置.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton配置.ImageOptions.Image")));
            this.simpleButton配置.Location = new System.Drawing.Point(12, 288);
            this.simpleButton配置.Name = "simpleButton配置";
            this.simpleButton配置.Size = new System.Drawing.Size(57, 22);
            this.simpleButton配置.StyleController = this.layoutControl1;
            this.simpleButton配置.TabIndex = 8;
            this.simpleButton配置.Text = "配置";
            this.simpleButton配置.Click += new System.EventHandler(this.simpleButton配置_Click);
            // 
            // simpleButton刷新
            // 
            this.simpleButton刷新.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton刷新.ImageOptions.Image")));
            this.simpleButton刷新.Location = new System.Drawing.Point(253, 288);
            this.simpleButton刷新.Name = "simpleButton刷新";
            this.simpleButton刷新.Size = new System.Drawing.Size(74, 22);
            this.simpleButton刷新.StyleController = this.layoutControl1;
            this.simpleButton刷新.TabIndex = 7;
            this.simpleButton刷新.Text = "刷新";
            this.simpleButton刷新.Click += new System.EventHandler(this.simpleButton刷新_Click);
            // 
            // simpleButton关闭
            // 
            this.simpleButton关闭.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton关闭.ImageOptions.Image")));
            this.simpleButton关闭.Location = new System.Drawing.Point(687, 288);
            this.simpleButton关闭.Name = "simpleButton关闭";
            this.simpleButton关闭.Size = new System.Drawing.Size(54, 22);
            this.simpleButton关闭.StyleController = this.layoutControl1;
            this.simpleButton关闭.TabIndex = 6;
            this.simpleButton关闭.Text = "关闭";
            this.simpleButton关闭.Click += new System.EventHandler(this.simpleButton关闭_Click);
            // 
            // simpleButton下载
            // 
            this.simpleButton下载.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton下载.ImageOptions.Image")));
            this.simpleButton下载.Location = new System.Drawing.Point(506, 288);
            this.simpleButton下载.Name = "simpleButton下载";
            this.simpleButton下载.Size = new System.Drawing.Size(61, 22);
            this.simpleButton下载.StyleController = this.layoutControl1;
            this.simpleButton下载.TabIndex = 5;
            this.simpleButton下载.Text = "下载...";
            this.simpleButton下载.Click += new System.EventHandler(this.simpleButton下载_Click);
            // 
            // gridControl插件下载
            // 
            this.gridControl插件下载.Location = new System.Drawing.Point(12, 12);
            this.gridControl插件下载.MainView = this.gridView插件下载;
            this.gridControl插件下载.Name = "gridControl插件下载";
            this.gridControl插件下载.Size = new System.Drawing.Size(729, 272);
            this.gridControl插件下载.TabIndex = 4;
            this.gridControl插件下载.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView插件下载});
            // 
            // gridView插件下载
            // 
            this.gridView插件下载.GridControl = this.gridControl插件下载;
            this.gridView插件下载.Name = "gridView插件下载";
            this.gridView插件下载.OptionsBehavior.Editable = false;
            this.gridView插件下载.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView插件下载.OptionsView.ShowGroupPanel = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem4,
            this.layoutControlItem9,
            this.layoutControlItem6});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(753, 322);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(61, 276);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(180, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl插件下载;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(733, 276);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton下载;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(494, 276);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(56, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton关闭;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(675, 276);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(58, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButton新增包;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(427, 276);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(56, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(67, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.simpleButton更新包;
            this.layoutControlItem8.Location = new System.Drawing.Point(319, 276);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(68, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(108, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton刷新;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(241, 276);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(56, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(78, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton下载到默认位置;
            this.layoutControlItem9.Location = new System.Drawing.Point(559, 276);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(116, 26);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton配置;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 276);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(56, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(61, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // XtraForm插件仓库
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 322);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraForm插件仓库";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "插件仓库";
            this.Load += new System.EventHandler(this.XtraForm模块列表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl插件下载)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView插件下载)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private LayoutControl layoutControl1;
        private SimpleButton simpleButton刷新;
        private SimpleButton simpleButton关闭;
        private SimpleButton simpleButton下载;
        private GridControl gridControl插件下载;
        private GridView gridView插件下载;
        private LayoutControlGroup layoutControlGroup1;
        private EmptySpaceItem emptySpaceItem2;
        private LayoutControlItem layoutControlItem1;
        private LayoutControlItem layoutControlItem2;
        private LayoutControlItem layoutControlItem3;
        private LayoutControlItem layoutControlItem4;
        private SimpleButton simpleButton配置;
        private LayoutControlItem layoutControlItem6;
        private SimpleButton simpleButton新增包;
        private LayoutControlItem layoutControlItem7;
        private SimpleButton simpleButton更新包;
        private LayoutControlItem layoutControlItem8;
        private SimpleButton simpleButton下载到默认位置;
        private LayoutControlItem layoutControlItem9;
    }
}