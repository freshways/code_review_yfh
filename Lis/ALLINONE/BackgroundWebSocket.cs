﻿using HIS.COMM.ProgressBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ALLINONE
{
    class BackgroundWebSocket
    {

        public readonly BackgroundWorker bw;
        public BackgroundWebSocket()
        {
            bw = BackgroundWorkerExtension.InitNewBackgroundWorker(bw_DoWork, bw_ProgressChanged, bw_localRunWorkerCompleted);
            //    bw.RunWorkerCompleted += bw_localRunWorkerCompleted;//多事件委托，顺序执行调用界面和进度界面的事件。
        }
        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            if (bw.IsBusy)
            {

            }
            var ce = e.Argument as CustomerDoWorkEventArgs;
            var test = ce.ExecParameter;
            BackgroundWorker _cBw = ce.ExecBackgroundWorker;          
                     
        }
        void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //var userState = e.UserState;
            ////lblMessage.Text = "bw_ProgressChanged正在处理" + e.ProgressPercentage;
            //progressBar1.Value = e.ProgressPercentage;
            //lsRes.Items.Insert(0, userState);
        }

        void bw_localRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
          
            if (e.Cancelled)
            {
                MessageBox.Show("取消成功");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("执行出错 " + e.Error);
            }
            else
            {
                var res = e.Result;
            }      
        }
    }
}
