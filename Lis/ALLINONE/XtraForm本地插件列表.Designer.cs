﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;

namespace ALLINONE
{
    partial class XtraForm本地插件列表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm本地插件列表));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton卸载插件 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl摘要信息 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton关闭 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton远程插件 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl本地插件列表 = new DevExpress.XtraGrid.GridControl();
            this.gridView本地插件列表 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col插件名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col功能说明 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col维护团队 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col插件更新日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col插件文件名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl本地插件列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView本地插件列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton卸载插件);
            this.layoutControl1.Controls.Add(this.labelControl摘要信息);
            this.layoutControl1.Controls.Add(this.simpleButton刷新);
            this.layoutControl1.Controls.Add(this.simpleButton关闭);
            this.layoutControl1.Controls.Add(this.simpleButton远程插件);
            this.layoutControl1.Controls.Add(this.gridControl本地插件列表);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1014, 194, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(771, 391);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton卸载插件
            // 
            this.simpleButton卸载插件.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton卸载插件.ImageOptions.Image")));
            this.simpleButton卸载插件.Location = new System.Drawing.Point(542, 359);
            this.simpleButton卸载插件.Name = "simpleButton卸载插件";
            this.simpleButton卸载插件.Size = new System.Drawing.Size(78, 22);
            this.simpleButton卸载插件.StyleController = this.layoutControl1;
            this.simpleButton卸载插件.TabIndex = 9;
            this.simpleButton卸载插件.Text = "卸载插件";
            this.simpleButton卸载插件.Click += new System.EventHandler(this.simpleButton卸载插件_Click);
            // 
            // labelControl摘要信息
            // 
            this.labelControl摘要信息.Location = new System.Drawing.Point(11, 359);
            this.labelControl摘要信息.Name = "labelControl摘要信息";
            this.labelControl摘要信息.Size = new System.Drawing.Size(37, 14);
            this.labelControl摘要信息.StyleController = this.layoutControl1;
            this.labelControl摘要信息.TabIndex = 8;
            this.labelControl摘要信息.Visible = false;
            // 
            // simpleButton刷新
            // 
            this.simpleButton刷新.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton刷新.ImageOptions.Image")));
            this.simpleButton刷新.Location = new System.Drawing.Point(482, 359);
            this.simpleButton刷新.Name = "simpleButton刷新";
            this.simpleButton刷新.Size = new System.Drawing.Size(56, 22);
            this.simpleButton刷新.StyleController = this.layoutControl1;
            this.simpleButton刷新.TabIndex = 7;
            this.simpleButton刷新.Text = "刷新";
            this.simpleButton刷新.Click += new System.EventHandler(this.simpleButton刷新_Click);
            // 
            // simpleButton关闭
            // 
            this.simpleButton关闭.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton关闭.ImageOptions.Image")));
            this.simpleButton关闭.Location = new System.Drawing.Point(706, 359);
            this.simpleButton关闭.Name = "simpleButton关闭";
            this.simpleButton关闭.Size = new System.Drawing.Size(54, 22);
            this.simpleButton关闭.StyleController = this.layoutControl1;
            this.simpleButton关闭.TabIndex = 6;
            this.simpleButton关闭.Text = "关闭";
            this.simpleButton关闭.Click += new System.EventHandler(this.simpleButton关闭_Click);
            // 
            // simpleButton远程插件
            // 
            this.simpleButton远程插件.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton远程插件.ImageOptions.Image")));
            this.simpleButton远程插件.Location = new System.Drawing.Point(624, 359);
            this.simpleButton远程插件.Name = "simpleButton远程插件";
            this.simpleButton远程插件.Size = new System.Drawing.Size(78, 22);
            this.simpleButton远程插件.StyleController = this.layoutControl1;
            this.simpleButton远程插件.TabIndex = 5;
            this.simpleButton远程插件.Text = "插件仓库";
            this.simpleButton远程插件.Click += new System.EventHandler(this.simpleButton下载_Click);
            // 
            // gridControl本地插件列表
            // 
            this.gridControl本地插件列表.Location = new System.Drawing.Point(11, 10);
            this.gridControl本地插件列表.MainView = this.gridView本地插件列表;
            this.gridControl本地插件列表.Name = "gridControl本地插件列表";
            this.gridControl本地插件列表.Size = new System.Drawing.Size(749, 345);
            this.gridControl本地插件列表.TabIndex = 4;
            this.gridControl本地插件列表.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView本地插件列表});
            // 
            // gridView本地插件列表
            // 
            this.gridView本地插件列表.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col插件名称,
            this.col功能说明,
            this.col维护团队,
            this.col插件更新日期,
            this.colCreateTime,
            this.col插件文件名});
            this.gridView本地插件列表.GridControl = this.gridControl本地插件列表;
            this.gridView本地插件列表.Name = "gridView本地插件列表";
            this.gridView本地插件列表.OptionsBehavior.Editable = false;
            this.gridView本地插件列表.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView本地插件列表.OptionsView.ShowGroupPanel = false;
            this.gridView本地插件列表.DoubleClick += new System.EventHandler(this.gridView本地插件列表_DoubleClick);
            // 
            // col插件名称
            // 
            this.col插件名称.Caption = "插件名称";
            this.col插件名称.FieldName = "插件名称";
            this.col插件名称.MinWidth = 22;
            this.col插件名称.Name = "col插件名称";
            this.col插件名称.Visible = true;
            this.col插件名称.VisibleIndex = 1;
            this.col插件名称.Width = 264;
            // 
            // col功能说明
            // 
            this.col功能说明.Caption = "功能说明";
            this.col功能说明.FieldName = "功能说明";
            this.col功能说明.MinWidth = 22;
            this.col功能说明.Name = "col功能说明";
            this.col功能说明.Visible = true;
            this.col功能说明.VisibleIndex = 2;
            this.col功能说明.Width = 192;
            // 
            // col维护团队
            // 
            this.col维护团队.Caption = "维护团队";
            this.col维护团队.FieldName = "维护团队";
            this.col维护团队.MinWidth = 22;
            this.col维护团队.Name = "col维护团队";
            this.col维护团队.Visible = true;
            this.col维护团队.VisibleIndex = 3;
            this.col维护团队.Width = 189;
            // 
            // col插件更新日期
            // 
            this.col插件更新日期.Caption = "插件更新日期";
            this.col插件更新日期.FieldName = "插件更新日期";
            this.col插件更新日期.MinWidth = 22;
            this.col插件更新日期.Name = "col插件更新日期";
            this.col插件更新日期.Visible = true;
            this.col插件更新日期.VisibleIndex = 4;
            this.col插件更新日期.Width = 273;
            // 
            // colCreateTime
            // 
            this.colCreateTime.Caption = "CreateTime";
            this.colCreateTime.FieldName = "createTime";
            this.colCreateTime.MinWidth = 22;
            this.colCreateTime.Name = "colCreateTime";
            this.colCreateTime.Visible = true;
            this.colCreateTime.VisibleIndex = 5;
            this.colCreateTime.Width = 201;
            // 
            // col插件文件名
            // 
            this.col插件文件名.Caption = "插件文件名";
            this.col插件文件名.FieldName = "插件文件名";
            this.col插件文件名.MinWidth = 22;
            this.col插件文件名.Name = "col插件文件名";
            this.col插件文件名.Visible = true;
            this.col插件文件名.VisibleIndex = 0;
            this.col插件文件名.Width = 225;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(771, 391);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(41, 349);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(430, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl本地插件列表;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(753, 349);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton远程插件;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(613, 349);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton关闭;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(695, 349);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(58, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(58, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton刷新;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(471, 349);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(60, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(60, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(60, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelControl摘要信息;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 349);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(41, 26);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton卸载插件;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(531, 349);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // XtraForm本地插件列表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 391);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraForm本地插件列表";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "本地插件管理";
            this.Load += new System.EventHandler(this.XtraForm模块列表_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl本地插件列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView本地插件列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private LayoutControl layoutControl1;
        private SimpleButton simpleButton刷新;
        private SimpleButton simpleButton关闭;
        private SimpleButton simpleButton远程插件;
        private GridControl gridControl本地插件列表;
        private GridView gridView本地插件列表;
        private LayoutControlGroup layoutControlGroup1;
        private EmptySpaceItem emptySpaceItem2;
        private LayoutControlItem layoutControlItem1;
        private LayoutControlItem layoutControlItem2;
        private LayoutControlItem layoutControlItem3;
        private LayoutControlItem layoutControlItem4;
        private LabelControl labelControl摘要信息;
        private LayoutControlItem layoutControlItem5;
        private SimpleButton simpleButton卸载插件;
        private LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Columns.GridColumn col插件名称;
        private DevExpress.XtraGrid.Columns.GridColumn col功能说明;
        private DevExpress.XtraGrid.Columns.GridColumn col维护团队;
        private DevExpress.XtraGrid.Columns.GridColumn col插件更新日期;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateTime;
        private DevExpress.XtraGrid.Columns.GridColumn col插件文件名;
    }
}