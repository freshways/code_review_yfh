﻿using DevExpress.XtraBars.Helpers;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraTabbedMdi;
using HIS.COMM.Helper;
using HIS.Model;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WEISHENG.COMM;
using WEISHENG.COMM.PluginsAttribute;
using msgBalloonHelper = HIS.COMM.msgBalloonHelper;



namespace ALLINONE
{
    public partial class FormMain : DevExpress.XtraEditors.XtraForm
    {
        private static string vpnName = "vpnAllinone";
        string s界面配置文件 = "mainForm.xml";
        System.Media.SoundPlayer pl声音提示 = new System.Media.SoundPlayer("ringin.wav");
        NavBarLoader _NavBarLoader;

        public FormMain()
        {
            DevExpress.Skins.SkinManager.EnableMdiFormSkins();
            InitializeComponent();
            InitSkins();
        }

        void InitSkins()
        {
            SkinHelper.InitSkinPopupMenu(barSubItem界面);
        }



        private void barSubItem退出系统_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            WEISHENG.COMM.LogHelper.Info(this.Text, "事件信息", "用户" + HIS.COMM.zdInfo.ModelUserInfo.用户名 + "退出");
            this.Close();
        }



        private void FormMain_Load(object sender, EventArgs e)
        {
            try
            {
                _NavBarLoader = new NavBarLoader(imageCollection16);
                if (HIS.COMM.zdInfo.Model站点信息.可否医保签到 == true)
                {
                    try
                    {
                        LogHelper.Info("初始化ybjk中的log4net成功", WEISHENG.COMM.enumLog.ybjk);
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error("错误日志", ex);
                    }
                }

                labelControl医院标题.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc + "信息平台";
                pictureEdit公告_Click(null, null);
                DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(HIS.COMM.XtraWaitForm));
                _NavBarLoader._Openurl2 += v打开窗体2;
                try
                {
                    _NavBarLoader.v加载菜单(HIS.COMM.zdInfo.ModelUserInfo.角色名称, HIS.COMM.zdInfo.ModelUserInfo.用户名, navBarControlLeft);
                }
                catch (Exception ex)
                {
                    WEISHENG.COMM.LogHelper.Error(null, ex);
                    MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                barStaticItem单位_ItemClick(null, null);
                navBarControlLeft.ActiveGroup = navBarControlLeft.Groups[LayoutHelper.Load("FormMain", "navBarControlLeft")];
                //HIS.COMM.msgHelper.BalloonShow("读取用户站点默认配置成功");
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
            finally
            {
                barStaticItem软件版本.Caption = "主程序版本:" + zdInfo.sAppVersion;
                barStaticItem登陆用户.Caption = "登录用户:" + HIS.COMM.zdInfo.ModelUserInfo.用户名 + ";用户角色:" + HIS.COMM.zdInfo.ModelUserInfo.角色名称 + ";科室:" + HIS.COMM.zdInfo.ModelUserInfo.科室名称;
                barStaticItem单位.Caption = "单位:【" + HIS.COMM.zdInfo.Model单位信息.sDwmc + "】";
                barStaticItem登录站点.Caption = "登录站点:" + HIS.COMM.zdInfo.Model站点信息.站点名称 + ";标识:" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + ";";
                try
                {
                    toolStripMenuItem工作提示.Checked = Convert.ToBoolean(WEISHENG.COMM.configHelper.getKeyValue("工作提示"));
                }
                catch (Exception)
                {
                    toolStripMenuItem工作提示.Checked = false;
                    WEISHENG.COMM.configHelper.setKeyValue("工作提示", "False");
                }
                gx工作提示(toolStripMenuItem工作提示.Checked);
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }
        }

        enum openType
        {
            ShowNormalDialog,
            ShowMaximizedDialog,
            SelectedPage
        }

        void openFormWithArgs(string assemblyName, string formName, object[] args, string titleName, openType openType)
        {
            try
            {
                Form childrenForm = (Form)CacheManager.Instance.CreateInstance(assemblyName, formName, args);
                childrenForm.Text = titleName;
                switch (openType)
                {
                    case openType.ShowNormalDialog:
                        childrenForm.StartPosition = FormStartPosition.CenterScreen;
                        childrenForm.Size = new System.Drawing.Size(818, 497);
                        childrenForm.ShowDialog();
                        break;
                    case openType.ShowMaximizedDialog:
                        childrenForm.StartPosition = FormStartPosition.CenterScreen;
                        childrenForm.WindowState = FormWindowState.Maximized;
                        childrenForm.ShowDialog();
                        break;
                    case openType.SelectedPage:
                    default:
                        int itemCount = 0;
                        itemCount = xtraTabbedMdiManager1.Pages.Count;
                        foreach (XtraMdiTabPage itemPage in xtraTabbedMdiManager1.Pages)
                        {
                            if (titleName == itemPage.Text)
                            {
                                xtraTabbedMdiManager1.SelectedPage = itemPage;
                                return;
                            }
                        }
                        childrenForm.MdiParent = this;
                        xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages[itemCount];
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        void v打开窗体2(pubQx pubqx)
        {
            try
            {
                int itemCount = 0;
                itemCount = xtraTabbedMdiManager1.Pages.Count;
                foreach (XtraMdiTabPage itemPage in xtraTabbedMdiManager1.Pages)
                {
                    if (pubqx.模块名 == itemPage.Text)
                    {
                        xtraTabbedMdiManager1.SelectedPage = itemPage;
                        return;
                    }
                }
                if (pubqx.菜单类型 == "WebForm")
                {
                    pubqx.传递参数 = HIS.COMM.Helper.UriHelper.MakeUri(pubqx.传递参数);
                }
                object[] _obj参数 = new object[1];
                _obj参数[0] = pubqx.传递参数 == null ? "" : pubqx.传递参数;
                Form childrenForm = (Form)CacheManager.Instance.CreateInstance(pubqx.程序集名称, pubqx.程序集调用类地址, _obj参数);
                childrenForm.MdiParent = this;
                childrenForm.Text = pubqx.功能名称;
                childrenForm.Show();
                xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages[itemCount];
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage));
                    }
                }
                msgBalloonHelper.BalloonShow(stringBuilder.ToString());
            }
            catch (Exception ex)
            {
                string errorMsg = "错误：";
                if (ex.InnerException == null)
                    errorMsg += ex.Message + "，";
                else if (ex.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.Message + "，";
                else if (ex.InnerException.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.InnerException.Message;
                msgBalloonHelper.ShowInformation(errorMsg);
            }
        }






        private void OpenLink(Form itemForm, string itemHeader)
        {
            int itemCount = 0;
            itemCount = xtraTabbedMdiManager1.Pages.Count;
            foreach (XtraMdiTabPage itemPage in xtraTabbedMdiManager1.Pages)
            {
                if (itemHeader == itemPage.Text)
                {
                    xtraTabbedMdiManager1.SelectedPage = itemPage;
                    return;
                }
            }
            itemForm.MdiParent = this;
            itemForm.Text = itemHeader;
            itemForm.Show();
            xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages[itemCount];
        }

        private void barButtonItem短信发送_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //ALLINONE.Comm.XtraForm短信发送 frmDxfs = new HIS.COMM.XtraForm短信发送();
            //OpenLink(frmDxfs, "短信发送");
        }

        private void barButtonItem帮助_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }


        private void timer_Tick(object sender, EventArgs e)
        {
            barStaticItem时间.Caption = "时间:" + Convert.ToString(DateTime.Now);
            if (HIS.COMM.SmartAlert.AlertInfo.context != null)
            {
                this.alertControl提示.Show(this.FindForm(), HIS.COMM.SmartAlert.AlertInfo.context.Title, HIS.COMM.SmartAlert.AlertInfo.context.Info,
                    imageCollection16.Images[0]);
                HIS.COMM.SmartAlert.AlertInfo.context = null;
            }

        }



        private void barButtonItem自动升级_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (System.IO.File.Exists(zdInfo.upgrade_tools_name) == false)
            {
                MessageBox.Show("升级工具丢失，请联系软件维护人员处理故障", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Process.Start(zdInfo.upgrade_tools_name);
        }

        private void barButtonItem页面设置_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PageSetupDialog pageSetupDialog1 = new PageSetupDialog();
            pageSetupDialog1.Document = this.pDoc;
            pageSetupDialog1.ShowDialog();
        }

        private void barButtonItem打印机设置_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            printDialog.Document = this.pDoc;
            printDialog.ShowDialog();
        }


        private void barButtonItem默认打印机选择_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Comm.XtraForm默认打印机选择 mrdyjxz = new HIS.COMM.XtraForm默认打印机选择();
            //mrdyjxz.ShowDialog();
        }



        private void barButtonItem信息_ItemDoubleClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (HIS.COMM.zdInfo.ModelUserInfo.用户名 == "Admin")
            {
                HIS.COMM.XtraForm操作验证 lg = new HIS.COMM.XtraForm操作验证();
                if (lg.ShowDialog() == DialogResult.OK)
                {
                    HIS.COMM.zdInfo.ModelUserInfo.用户名 = "SupperAdmin";
                    FormMain_Load(null, null);
                }
            }
        }
        private void navBarItemVPN拨号_LinkClicked(object sender, NavBarLinkEventArgs e)
        {

        }

        private void cshVpnInfo()
        {
            //DataTable dt = SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISPubDb, CommandType.Text,
            //    "select vpnServer,vpnUser,vpnPd,defaultGateway from gy站点设置 where mac地址='" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "'").Tables[0];
            //if (dt.Rows.Count > 0)
            //{
            //    zdInfo.vpnInfo.vpnServerIp = dt.Rows[0]["vpnServer"].ToString();
            //    zdInfo.vpnInfo.passWord = dt.Rows[0]["VpnPd"].ToString();
            //    zdInfo.vpnInfo.user = dt.Rows[0]["VpnUser"].ToString();
            //    zdInfo.vpnInfo.defaultGateway = dt.Rows[0]["DefaultGateway"].ToString();
            //}
            //else
            //{
            //    zdInfo.vpnInfo.vpnServerIp = "未定义";
            //}
        }


        private void barStaticItem登陆用户_ItemDoubleClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //XtraMessageBox.Show(zdInfo.vpnInfo.vpnServerIp);
            //XtraMessageBox.Show(zdInfo.vpnInfo.defaultGateway);
        }


        private void timer特殊任务_Tick(object sender, EventArgs e)
        {

        }


        private void barStaticItem登录站点_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            timer工作提示_Tick(null, null);
        }

        private void FormMain_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                //this.Hide();
                this.notifyIcon托盘.Visible = true;
            }
        }

        private void notifyIcon托盘_Click(object sender, EventArgs e)
        {
            try
            {
                pl声音提示.Stop();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void timer工作提示_Tick(object sender, EventArgs e)
        {
            try
            {
                backgroundWorker工作提示.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                msgBalloonHelper.BalloonShow(ex.Message);
            }
        }

        private void barStaticItem单位_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        }


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                HIS.COMM.SmartAlert.Class医嘱执行提示.getInfo();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
            }
            finally
            {
                timer工作提示.Enabled = true;
            }
        }


        private void barButtonItem调查问卷_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string sIP = db_connectHelper.get连接串IP(HIS.COMM.DBConnHelper.SConnHISDb);
            string sDbName = db_connectHelper.get连接串dbName(HIS.COMM.DBConnHelper.SConnHISDb);
            this.alertControl提示.Show(this.FindForm(), "标题", sIP + "|" + sDbName, imageCollection16.Images[0]);
        }





        private void barButtonItem连接VPN_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //cshVpnInfo();
            //if (zdInfo.vpnInfo.vpnServerIp == "未定义")
            //{
            //    XtraMessageBox.Show("服务器未分配此站点的VPN账户，不能继续拨号", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
            //VPNHelper vpn = new VPNHelper();
            //vpn.CreateOrUpdateVPN(vpnName, zdInfo.vpnInfo.vpnServerIp);
            //vpn.TryConnectVPN(vpnName, zdInfo.vpnInfo.user, zdInfo.vpnInfo.passWord);
            //dosHelper dosRun = new dosHelper();
            //dosRun.runDosCMD("route delete 192.168.10.0");
            //dosRun.runDosCMD("route add 192.168.10.0 mask 255.255.255.0 " + zdInfo.vpnInfo.defaultGateway + " metric 20");
        }

        private void barButtonItem计算器_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Diagnostics.Process.Start("calc.exe");

            //var browser = new BrowserForm("http://localhost:8075/WebReport/ReportServer?reportlet=GettingStarted.cpt");
            //var browser = new BrowserForm();
            //browser.SUrlTarget = "http://localhost:8075/WebReport/ReportServer?reportlet=GettingStarted.cpt";
            //browser.ShowDialog();
            //using (BrowserForm brow=new BrowserForm() { SUrlTarget= "http://localhost:8075/WebReport/ReportServer?reportlet=GettingStarted.cpt" })
            //{
            //    brow.ShowDialog();
            //}
        }

        private void barButtonItem断开VPN_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            VPNHelper vpn = new VPNHelper();
            vpn.TryDisConnectVPN(vpnName);
            vpn.TryDeleteVPN(vpnName);
        }

        private void barButtonItem问题反馈_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            //MessageBox.Show(timer工作提示.Enabled.ToString());
            //System.Diagnostics.Process.Start("IEXPLORE.EXE", "http://192.168.10.100:82/btnet");
            //ServiceReferenceXnhBusiness.ServiceXnhBusinessClient busi = new ServiceReferenceXnhBusiness.ServiceXnhBusinessClient();

            //DataTable dttt = Db.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnXnhDbString,CommandType.Text,"select top 10 * from 报销费用").Tables[0];
            //dttt.TableName = "aa";
            //busi.XNH_InsertFYMXTable(dttt);
        }

        private void navBarItem影像传输_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            barButtonItem屏幕截图_ItemClick(null, null);
        }

        public Bitmap bmp = new Bitmap(Screen.AllScreens[0].Bounds.Width, Screen.AllScreens[0].Bounds.Height);
        private void barButtonItem屏幕截图_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            //Graphics g = Graphics.FromImage(bmp);
            //g.CopyFromScreen(0, 0, 0, 0, new Size(Screen.AllScreens[0].Bounds.Width, Screen.AllScreens[0].Bounds.Height));

            //business.Jcjg.frmPACS截屏 f = new business.Jcjg.frmPACS截屏(bmp);
            //f.Show();
        }

        private void 屏幕截图PToolStripMenuItem_Click(object sender, EventArgs e)
        {
            barButtonItem屏幕截图_ItemClick(null, null);
        }

        private void notifyIcon托盘_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Maximized;
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {

            try
            {
                if (XtraMessageBox.Show("是否退出系统？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    e.Cancel = true;
                }
                LayoutHelper.Save("FormMain", "navBarControlLeft", navBarControlLeft.ActiveGroup.Name);
                //HIS.COMM.msgHelper.BalloonShow("用户默认配置保存成功");
                if (HIS.COMM.Helper.WebSocketHelper.WebSocketHelperShared.ws_client != null)
                {
                    HIS.COMM.Helper.WebSocketHelper.WebSocketHelperShared.ws_client.Close();
                }
                navBarControlLeft.SaveToXml(s界面配置文件);
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }


        private void toolStripMenuItem工作提示_CheckedChanged(object sender, EventArgs e)
        {
            WEISHENG.COMM.configHelper.setKeyValue("工作提示", toolStripMenuItem工作提示.Checked.ToString());
            gx工作提示(toolStripMenuItem工作提示.Checked);

        }
        private void gx工作提示(bool _bool)
        {
            timer工作提示.Enabled = _bool;
            if (_bool)
            {
                backgroundWorker工作提示.RunWorkerAsync();
            }
        }


        private void pictureEdit公告_Click(object sender, EventArgs e)
        {
            try
            {
                HIS.COMM.XtraForm通知公告列表 tzgg = new HIS.COMM.XtraForm通知公告列表(HIS.COMM.DBConnHelper.SConnHISDb);
                OpenLink(tzgg, "通知公告");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void toolStripMenuItem关闭当前窗口_Click(object sender, EventArgs e)
        {
            if (xtraTabbedMdiManager1.SelectedPage.Text != "通知公告")
                xtraTabbedMdiManager1.SelectedPage.MdiChild.Close();
        }

        private void toolStripMenuItem关闭其他窗口_Click(object sender, EventArgs e)
        {
            ArrayList pages = new ArrayList(xtraTabbedMdiManager1.Pages);
            foreach (var item in pages)
            {
                var page = item as XtraMdiTabPage;
                if (page != xtraTabbedMdiManager1.SelectedPage)
                {
                    if (page.Text != "通知公告")
                        page.MdiChild.Close();
                }
            }
        }

        private void xtraTabbedMdiManager1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            BaseTabHitInfo hint = xtraTabbedMdiManager1.CalcHitInfo(e.Location);
            if (hint.IsValid && (hint.Page != null))
            {
                if (xtraTabbedMdiManager1.SelectedPage.MdiChild != null)
                {
                    Point p = xtraTabbedMdiManager1.SelectedPage.MdiChild.PointToScreen(e.Location);
                    menuStripCloseForm.Show(p);
                }
            }
        }

        private void 关闭全部窗口ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ArrayList pages = new ArrayList(xtraTabbedMdiManager1.Pages);
            foreach (var item in pages)
            {
                var page = item as XtraMdiTabPage;
                if (page.Text != "通知公告")
                    page.MdiChild.Close();
            }
        }

        private void barButtonItem单点登录电子病历_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //string FilePath = "D:\\EMR\\MandalaT DoqLei\\LordInterop.exe";
            //string[] args = new string[] { "http://192.168.10.152/YS21", "guest1", "0000", "Hospital", "46953", "Name:饶上仁|Sex:男|Birthday:1958-03-18|Age:55岁|HIS内部标识:495571840-0201312127350" };
            //ProcessStartInfo psi = new ProcessStartInfo(FilePath);
            //psi.Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\"", args);
            //Process.Start(psi);
        }

        private void pictureEdit知识库_Click(object sender, EventArgs e)
        {
            try
            {
                if (HIS.COMM.baseInfo.En站点认证方式 == HIS.COMM.Class认证方式.en认证方式.每站点Ukey && WEISHENG.COMM.dzgHelper.IshaveDzg("3DA9733D") == false)
                {
                    MessageBox.Show("获取授权异常，请联系技术人员", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string sConn = HIS.COMM.ClassPubArgument.s知识库连接();
                HIS.COMM.XtraForm通知公告列表 tzgg = new HIS.COMM.XtraForm通知公告列表(sConn);
                OpenLink(tzgg, "知识库");
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void panelControl标题_DoubleClick(object sender, EventArgs e)
        {
            panelControl标题.Visible = false;
            barCheckItem标题栏.Checked = false;
        }

        private void barCheckItem标题栏_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (barCheckItem标题栏.Checked)
            {
                panelControl标题.Visible = true;
            }
            else
            {
                panelControl标题.Visible = false;
            }
        }

        private void barButtonItem日积月累_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //FrmKnowledge tempKnowledge = new FrmKnowledge();
            //tempKnowledge.ShowDialog();
        }

        private void pictureEdit知识库_EditValueChanged(object sender, EventArgs e)
        {

        }


        private void barStaticItem登录站点_ItemDoubleClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //HIS.COMM.XtraForm操作验证 lg = new HIS.COMM.XtraForm操作验证();
            //if (lg.ShowDialog() != DialogResult.OK)
            //{
            //    return;
            //}
            XtraForm站点信息更新 zdxx = new XtraForm站点信息更新();
            if (zdxx.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                barStaticItem登录站点.Caption = "登录站点:" + HIS.COMM.zdInfo.Model站点信息.站点名称 + ";标识:" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + ";";
            }
        }

        private void barButtonItem注销_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Diagnostics.Process.Start("ALLINONE.EXE");
            Application.Exit();
        }

        private void barButtonItem修改密码_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm密码修改().ShowDialog();
        }

        private void barButtonItem已缓存插件_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string _sCacheInfo = CacheManager.Instance.getCacheDllsInfo();
            if (_sCacheInfo.Length == 0) return;
            MessageBox.Show(_sCacheInfo);
        }

        private void barButtonItem插件管理_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using (new WEISHENG.COMM.AutoWaitCursor(this))
                {
                    XtraForm本地插件列表 frm = new XtraForm本地插件列表();
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alertControl提示_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
        {
            //try
            //{              
            //    if (!string.IsNullOrEmpty(HIS.COMM.SmartAlert.AlertInfo.Url))
            //    {
            //        HIS.Browser.Helper.Show.MaximizedShow(HIS.COMM.SmartAlert.AlertInfo.Url);       
            //    }
            //}
            //catch (Exception ex)
            //{
            //    WEISHENG.COMM.msgHelper.ShowInformation(ex.Message);
            //}
        }

        private void barButtonItem制作二维码_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            XtraForm制作二维码 dialog = new XtraForm制作二维码();
            dialog.ShowDialog();
        }

        private void barSubItem帮助_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //this.alertControl提示.Show(this.FindForm(), "标题", "aa.result", imageCollection提示.Images[0]);         
            //XtraMessageBox.Show(zdInfo.UserInfo.userKsbm, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //System.Diagnostics.Process.Start("IEXPLORE.EXE", "http://192.168.10.131,1433/allInOne/readme.mht");
            //ALLINONE.Comm.XtraForm版本说明 bbsm = new HIS.COMM.XtraForm版本说明();
            //bbsm.ShowDialog(); 
        }

        private void barButtonItem关于_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new AboutWindow().ShowDialog();
        }

        private void xtraTabbedMdiManager1_SelectedPageChanged(object sender, EventArgs e)
        {
            string _sTitle = "";
            try
            {
                if (xtraTabbedMdiManager1.SelectedPage == null)
                {
                    _sTitle = string.Format("{0}信息平台", HIS.COMM.zdInfo.Model单位信息.sDwmc);
                    return;
                }

                //1、根据模块（winform）属性设置窗口大小等             
                var currForm = (Form)xtraTabbedMdiManager1.SelectedPage.MdiChild;
                var moduleInfo = PlugInfoHelper.getClassInfo(currForm);
                if (moduleInfo != null)
                {
                    //todo:配置到个性化设置里面，写死到代码里面太笨了
                    //if (moduleInfo.全屏打开)
                    //{
                    //    this.dockPanel功能导航.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
                    //    //this.navBarControlLeft.OptionsNavPane.NavPaneState = NavPaneState.Collapsed;
                    //}
                }


                //2、根据程序集更新title显示内容
                PluginAssemblyInfoAttribute AssemblyAttribute = PlugInfoHelper.getAssemblyInfo(currForm.GetType().Assembly);
                if (AssemblyAttribute == null)
                {
                    _sTitle = string.Format("{0}信息平台", HIS.COMM.zdInfo.Model单位信息.sDwmc);
                    return;
                }
                if (!string.IsNullOrEmpty(AssemblyAttribute.插件名称))
                {
                    string _sVer = currForm.GetType().Assembly.GetName().Version.ToString();
                    _sTitle = string.Format("{0}信息平台{1}模块【{2}】", HIS.COMM.zdInfo.Model单位信息.sDwmc, AssemblyAttribute.插件名称, _sVer);
                }
                this.labelControl医院标题.Tag = AssemblyAttribute;
            }
            finally
            {
                this.labelControl医院标题.Text = _sTitle;
            }


        }

        private void label我的网盘_Click(object sender, EventArgs e)
        {
            try
            {
                //var form = HIS.Browser.Helper.Show.ShowNormal("我的网盘", HIS.COMM.ClassPubArgument.S医院公用网盘登录地址());
                //OpenLink(form, "我的网盘");
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void labelControl医院标题_DoubleClick(object sender, EventArgs e)
        {
            PluginAssemblyInfoAttribute pluginAssemblyInfo = new PluginAssemblyInfoAttribute();
            if (labelControl医院标题.Text.Contains("模块"))
            {
                pluginAssemblyInfo = (PluginAssemblyInfoAttribute)labelControl医院标题.Tag;

            }
            new Form模块版本说明(pluginAssemblyInfo).ShowDialog();
        }

        private void barCheckItemTitle_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            panelControl标题.Visible = !barCheckItemTitle.Checked;

        }

        private void label医保规范_Click(object sender, EventArgs e)
        {
            try
            {
                HIS.COMM.XtraForm标准规范 tzgg = new HIS.COMM.XtraForm标准规范();
                OpenLink(tzgg, "医保规范");
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void barButtonItem日志_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                object[] _obj参数 = new object[1];
                _obj参数[0] = HIS.COMM.Helper.UriHelper.MakeUri("{localfile}index.html");
                openFormWithArgs("HIS.Browser", "FormShow", _obj参数, "升级日志", openType.ShowMaximizedDialog);
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void barButtonItem移动设备_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                new XtraForm我的移动端().ShowDialog();
            }
            catch (Exception ex)
            {
                HIS.COMM.msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void barButtonItem站点信息_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                object[] _obj参数 = new object[1];
                _obj参数[0] = HIS.COMM.Helper.UriHelper.MakeUri("{localfile}jsInteractionDemo.html");
                openFormWithArgs("HIS.Browser", "FormShow", _obj参数, "站点信息", openType.ShowNormalDialog);
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
