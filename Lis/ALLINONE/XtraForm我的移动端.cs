﻿using HIS.COMM.Helper.WebSocketHelper;
using System;

namespace ALLINONE
{
    public partial class XtraForm我的移动端 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm我的移动端()
        {
            InitializeComponent();
        }

        private void XtraForm我的移动端_Load(object sender, EventArgs e)
        {
            refresh();
        }

        private void simpleButton删除_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }
            var item = gridView1.GetFocusedRow() as socketBind;
            MobileClient.deleteWxPcUser(item.openid, "his");
            refresh();
        }

        void refresh()
        {
            var binds = MobileClient.getListWxPcUser($"{HIS.COMM.zdInfo.Model单位信息.iDwid}_{ HIS.COMM.zdInfo.ModelUserInfo.用户编码}");
            gridControl1.DataSource = binds;
            gridView1.PopulateColumns();
            gridView1.BestFitColumns();
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}