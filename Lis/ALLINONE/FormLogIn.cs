﻿using DevExpress.XtraEditors;
using HIS.COMM;
using HIS.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;


namespace ALLINONE
{
    public partial class FormLogIn : DevExpress.XtraEditors.XtraForm
    {


        #region 圆角
        //[DllImport("gdi32.dll")]
        //public static extern int CreateRoundRectRgn(int x1, int y1, int x2, int y2, int x3, int y3);

        //[DllImport("user32.dll")]
        //public static extern int SetWindowRgn(IntPtr hwnd, int hRgn, Boolean bRedraw);

        //[DllImport("gdi32.dll", EntryPoint = "DeleteObject", CharSet = CharSet.Ansi)]
        //public static extern int DeleteObject(int hObject);

        ///// <summary>
        ///// 设置窗体的圆角矩形
        ///// </summary>
        ///// <param name="form">需要设置的窗体</param>
        ///// <param name="rgnRadius">圆角矩形的半径</param>
        //private static void SetFormRoundRectRgn(Form form, int rgnRadius)
        //{
        //    int hRgn = 0;
        //    hRgn = CreateRoundRectRgn(0, 0, form.Width + 1, form.Height + 1, rgnRadius, rgnRadius);
        //    SetWindowRgn(form.Handle, hRgn, true);
        //    DeleteObject(hRgn);
        //}
        #endregion
        public FormLogIn()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            WEISHENG.COMM.LogHelper.Info("系统登录", "用户取消登录");
            this.DialogResult = DialogResult.Cancel;
        }

        IList<pubUser> users = null;
        private void FormLogIn_Load(object sender, EventArgs e)
        {
            try
            {
                labelControl标题信息.ForeColor = Color.FromArgb(69, 173, 224);

                //checkFolderStruct.v文件列表写到文件();
                //0、运行环境监测、创建快捷方式、清理指定文件
                configHelper.MergeConfigFiles();
                WEISHENG.COMM.link_iconHelper.CreateDesktopLnk("医院信息平台2020版");
                WEISHENG.COMM.Helper.environmentHelper.initEnvironment();

                //1、初始化mac地址等硬件信息
                WEISHENG.COMM.zdInfo.init_zdInfo(Application.ProductVersion, WEISHENG.COMM.appList.enAppName.AllInOne框架, "allinone.exe.config");


                //2、初始化数据库连接
                if (HIS.COMM.baseInfo.bSet数据库连接(false) == false)//当站点为注册时，只返回hispub数据连接
                {
                    //return;
                }

                //3、单位信息,放到静态路的构造函数里面了
                //zdInfo.Model单位信息 = HIS.COMM.BLL.Get_model单位信息.Action(WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress);

                //4、监测站点注册，返回站点信息
                var item站点 = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)).GY站点设置.Where(c => c.Mac地址 == WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress).FirstOrDefault();
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    if (item站点 == null)
                    {
                        item站点 = new GY站点设置()
                        {
                            Mac地址 = WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress,
                            站点名称 = "未定义",
                            当前版本 = WEISHENG.COMM.zdInfo.sAppVersion,
                            单位编码 = HIS.COMM.zdInfo.Model单位信息.iDwid,
                            是否禁用 = true,
                            限定版本 = "1.0.0.0",
                            登陆次数 = 0
                        };
                        chis.GY站点设置.Attach(item站点);
                        chis.Entry(item站点).State = EntityState.Added;
                    }
                    else
                    {
                        item站点.登陆次数++;
                        item站点.当前版本 = WEISHENG.COMM.zdInfo.sAppVersion;
                        item站点.最后登陆时间 = DateTime.Now;
                    }
                    chis.SaveChanges();
                }

                if (item站点.是否禁用 == true)
                {
                    MessageBox.Show($"{item站点.Mac地址}当前站点被禁用");//log4net还没有初始化，所以不能用msgHelper
                    Application.Exit();
                }

                zdInfo.Model站点信息 = item站点;
                //todo:  5、自动升级工具，怎样替换 ICSharpCode.SharpZipLib.dll ，直接从数据库下载，不用压缩组件吧。
                upgradeHelper.check更新自动升级组件();
                upgradeHelper.check主程序();
                //Next:后期考虑卫生室医保定点编码赋值问题
                if(zdInfo.Model站点信息.分院编码!=1)
                {
                    zdInfo.Model单位信息.S医保单位编码 = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("分院医保编码"+ zdInfo.Model站点信息.分院编码.ToString(), "", "").ToString();
                }
                //todo:插件是否需要升级          
                vRefreshForm();
            }
            catch (Exception ex)
            {
                string errorMsg = "错误：";
                if (ex.InnerException == null)
                    errorMsg += ex.Message + "，";
                else if (ex.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.Message + "，";
                else if (ex.InnerException.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.InnerException.Message;
                MessageBox.Show(errorMsg, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void vRefreshForm()
        {
            try
            {
                if (users != null)
                {
                    users.Clear();
                }
                users = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)).pubUsers.Where(c => c.是否禁用 == false).ToList();
                searchLookUpEditUser.Properties.DataSource = users;
                searchLookUpEditUser.Properties.DisplayMember = "用户名";
                searchLookUpEditUser.Properties.ValueMember = "用户编码";
                searchLookUpEditUser.EditValue = Convert.ToInt16(WEISHENG.COMM.configHelper.getKeyValue("最后登录人员编码", ""));
                labelControl标题信息.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc + "信息平台";
                labelControl版本号.Text = "  版本：" + WEISHENG.COMM.zdInfo.sAppVersion;
                int ileft = (this.Width - labelControl标题信息.Width + 465) / 2;  //重置图标及系统名称位置，使其能根据文本长短居中
                labelControl标题信息.Location = new System.Drawing.Point(ileft + 5, labelControl标题信息.Location.Y);
            }
            catch (Exception ex)
            {
                string errorMsg = "错误：";
                if (ex.InnerException == null)
                    errorMsg += ex.Message + "，";
                else if (ex.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.Message + "，";
                else if (ex.InnerException.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.InnerException.Message;
                WEISHENG.COMM.msgHelper.ShowInformation(errorMsg);
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                pubUser modelUser = null;
                if (searchLookUpEditUser.Properties.View.FocusedRowHandle < 0)//当未触发下拉列表时，先别扭的实现获取model
                {
                    var userCode = Convert.ToInt32(searchLookUpEditUser.EditValue);
                    modelUser = users.Where(c => c.用户编码 == userCode).FirstOrDefault();
                }
                else
                {
                    modelUser = searchLookUpEditUser.Properties.View.GetFocusedRow() as pubUser;
                }
                if (modelUser.角色名称 == "")
                {
                    if (modelUser.用户名 == "Admin")
                    {
                        modelUser.角色名称 = "Admin";
                    }
                    else
                    {
                        XtraMessageBox.Show("用户【" + modelUser.用户名 + "】未分配角色权限，请选择其他用户登录", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                if (modelUser.是否禁用 == true)
                {
                    XtraMessageBox.Show("【" + modelUser.用户名 + "】已经被禁用，请选择其他用户登录", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                if (textEditMM.Text == modelUser.用户密码 || (textEditMM.Text == " " && zdInfo.Model单位信息.sDwmc.Contains("测试单位")))
                {
                    //5、科室、数据权限
                    zdInfo.ModelUserInfo = modelUser;
                    int _ksbm = Convert.ToInt32(modelUser.科室编码);
                    zdInfo.Model科室信息 = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)).GY科室设置.Where(c => c.科室编码 == _ksbm).FirstOrDefault();
                    if (HIS.COMM.zdInfo.Model科室信息 == null)
                    {
                        msgBalloonHelper.ShowInformation("获取科室信息失败,请核对用户设置");
                        return;
                    }

                    zdInfo.s科室数据权限 = HIS.COMM.baseInfo.s获取全部科室数据权限(HIS.COMM.DBConnHelper.SConnHISDb);

                    WEISHENG.COMM.LogHelper.Debug(modelUser.用户名 + "登录成功");
                    WEISHENG.COMM.configHelper.setKeyValue("最后登录人员编码", HIS.COMM.zdInfo.ModelUserInfo.用户编码.ToString());

                    //6、各种软件运行参数初始化
                    HIS.COMM.baseInfo.bGet软件设置状态();

                    //7、log4net初始化
                    try
                    {
                        //初始化log4net的时候，如果没有实例化并赋值  zdInfo.ModelUserInfo.用户名，会导致log4net的后台表pub_log插入数据异常
                        WEISHENG.COMM.LogHelper.Init(HIS.COMM.ClassPubArgument.getArgumentValue("SentryDSN", ""),
                           HIS.COMM.ClassPubArgument.getArgumentValue("log4net记录方式", "file|db"),
                           HIS.COMM.ClassPubArgument.getArgumentValue("log4net记录级别", "info"),
                           HIS.COMM.DBConnHelper.SConnHISDb, WEISHENG.COMM.appList.enAppName.AllInOne框架.ToString(),
                           WEISHENG.COMM.zdInfo.sAppVersion, HIS.COMM.zdInfo.ModelUserInfo.用户名,
                           WEISHENG.COMM.zdInfo.ModelHardInfo.IpAddress,
                           WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress);
                    }
                    catch (Exception ex)
                    {
                        WEISHENG.COMM.LogHelper.Error("错误日志", ex);
                    }
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    XtraMessageBox.Show("密码错误", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.searchLookUpEditUser.Focus();
                }

            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void textEditMM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnOk_Click(null, null);
            }
        }


        private void searchLookUpEditUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }



        private void FormLogIn_New_SizeChanged(object sender, EventArgs e)
        {
            //SetFormRoundRectRgn(this, 23);
        }

        private void labelVer_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                WEISHENG.COMM.msgHelper.ShowInformation(WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress);
                WEISHENG.COMM.zdInfo.init_zdInfo(Application.ProductVersion, WEISHENG.COMM.appList.enAppName.AllInOne框架, "allinone.exe.config");
                HIS.COMM.baseInfo.bSet数据库连接(true);
                vRefreshForm();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void labelControl版本号_Click(object sender, EventArgs e)
        {
            try
            {
                XtraForm操作验证 lg = new XtraForm操作验证();
                if (lg.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                using (new WEISHENG.COMM.AutoWaitCursor(this))
                {
                    XtraForm本地插件列表 frm = new XtraForm本地插件列表();
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
