﻿using HIS.Model;
using System;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using WEISHENG.COMM;

namespace ALLINONE
{
    public partial class XtraForm本地插件列表 : DevExpress.XtraEditors.XtraForm
    {
        //IList<PUB插件站点列表Model> localModels = new List<PUB插件站点列表Model>();
        //IList<PUB插件站点列表Model> RemoteModels = new List<PUB插件站点列表Model>();
        //PUB插件站点列表BLL bll = new PUB插件站点列表BLL();


        public XtraForm本地插件列表()
        {
            InitializeComponent();
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            try
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(false);
                DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(HIS.COMM.XtraWaitForm));

                CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
                //1、更新  插件信息 ==>遍历程序集 ==> 服务器上记录全部可用插件
                //2、更新  pubQxnew2 ==>遍历类标记属性 ==> 在权限中显示可用的功能
                //以上操作也可以移动到  自动升级工具里面完成 
                //也可以建一个公共的库，或者共享机制，可以向指定单位推送指定的插件
                string outString = "";
                PluginHelper plug = new PluginHelper();
                plug.LoadLocalPlugins_ToDb_DelOldFile(out outString);//检索plugins下面的插件,并更新到数据库           

                gridControl本地插件列表.DataSource = chis.plug_站点插件.Where(c => c.站点标识 == WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress).ToList();
                WEISHENG.COMM.frmGridCustomize.RegisterGrid(gridView本地插件列表);
                labelControl摘要信息.Text = outString;
                labelControl摘要信息.Visible = true;
                gridView本地插件列表.RefreshData();
            }
            catch (DbEntityValidationException dbEx)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage));
                    }
                }
                HIS.COMM.msgBalloonHelper.ShowInformation(stringBuilder.ToString());
            }
            catch (Exception ex)
            {
                string errorMsg = "错误：";
                if (ex.InnerException == null)
                    errorMsg += ex.Message + "，";
                else if (ex.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.Message + "，";
                else if (ex.InnerException.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.InnerException.Message;
                HIS.COMM.msgBalloonHelper.ShowInformation(errorMsg);
            }
            finally
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(false);
            }
        }

        private void XtraForm模块列表_Load(object sender, EventArgs e)
        {


        }




        private void simpleButton关闭_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void simpleButton下载_Click(object sender, EventArgs e)
        {
            XtraForm插件仓库 frm = new XtraForm插件仓库();
            frm.ShowDialog();
        }

        private void simpleButton卸载插件_Click(object sender, EventArgs e)
        {
            try
            {
                int _row = gridView本地插件列表.FocusedRowHandle;
                if (_row < 0)
                {
                    return;
                }
                var model = gridView本地插件列表.GetFocusedRow() as plug_站点插件;
                if (!msgHelper.AskQuestion($"是否卸载全部角色的【{model.插件名称}】权限？"))
                {
                    return;
                }
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    chis.Database.ExecuteSqlCommand($"delete pubjsqx where 键ID in (select 键ID from pubqx where [程序集名称] = '{model.插件文件名}')");
                    chis.Database.ExecuteSqlCommand($"delete pubqx where[程序集名称] = '{model.插件文件名}'");
                }
                string _pathPix = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "plugins");
                string _sKey = _pathPix + @"\" + model.插件文件名 + @"\" + model.插件文件名 + ".dll";
                string _pluginsFolder = _pathPix + @"\" + model.插件文件名;
                CacheManager.Instance.UnLoad(_sKey);
                WEISHENG.COMM.FileDirtoryHelper.DeleteDir(_pluginsFolder);//删除本地插件             
                msgHelper.ShowInformation("卸载插件成功");
                simpleButton刷新.PerformClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n请手工删除组件");
            }
        }

        private void gridView本地插件列表_DoubleClick(object sender, EventArgs e)
        {
            int _row = gridView本地插件列表.FocusedRowHandle;
            if (_row < 0)
            {
                return;
            }
            var model = gridView本地插件列表.GetFocusedRow() as plug_站点插件;


            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                var list = chis.pubQxes.Where(c => c.程序集名称 == model.插件文件名).ToList();
                HIS.COMM.frmBrow show = new HIS.COMM.frmBrow();
                show.ShowData(list, false, true);
                show.ShowDialog();
            }




        }
    }
}