﻿using DevExpress.XtraEditors;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using WEISHENG.COMM;
using WEISHENG.COMM.Utilities;


namespace ALLINONE
{
    internal static class Program
    {
        /// <summary>
        /// 非托管dll路径设置思路
        /// 软件的dll查找优先级是root,system32和path,所以，使用代码控制path，或者调用底层API来实现。
        /// 医保接口调用未实现，身份证读卡器没有测试环境
        /// 静态构造方法，在Main之前执行
        /// </summary>
        static Program()
        {
            //自定义加载dll的委托,解析程序集失败，会加载对应的程序集  判断了CefSharp的子目录

            //AppUtil.Set64Or32BitDllDir();
            //使用代码控制path
            WEISHENG.COMM.configHelper.SetProbingPath(@"plugins;support\ExtDlls;plugins\HIS.Browser;support\DevExpress18.1;");
            //底层API来实现
            AppDomain.CurrentDomain.AssemblyResolve += WEISHENG.COMM.configHelper.OnResolveAssembly; 
        }

        [STAThread]
        private static void Main(string[] Args)
        {
            /**
             * 当前用户是管理员的时候，直接启动应用程序
             * 如果不是管理员，则使用启动对象启动程序，以确保使用管理员身份运行
             */
            //获得当前登录的Windows用户标示
            System.Security.Principal.WindowsIdentity identity = System.Security.Principal.WindowsIdentity.GetCurrent();
            //创建Windows用户主题
            Application.EnableVisualStyles();
            System.Security.Principal.WindowsPrincipal principal = new System.Security.Principal.WindowsPrincipal(identity);
            //判断当前登录用户是否为管理员
            //if (principal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator))
            //{

            BindExceptionHandler();//绑定程序中的异常处理
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.LookAndFeel.UserLookAndFeel.Default.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "Blue";
            try
            {
                GetConfig();
                bool isRuned;
                System.Threading.Mutex mutex = new System.Threading.Mutex(true, "ALLINONE", out isRuned);
                if (isRuned)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    FormLogIn lg = new FormLogIn();
                    if (lg.ShowDialog() == DialogResult.OK)
                    {
                        Application.Run(new FormMain());
                    }
                    mutex.ReleaseMutex();
                }
                else
                {
                    XtraMessageBox.Show("程序已启动!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ee)
            {
                XtraMessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //}
            //else
            //{
            //    //创建启动对象
            //    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //    //设置运行文件
            //    startInfo.FileName = System.Windows.Forms.Application.ExecutablePath;
            //    //设置启动参数
            //    startInfo.Arguments = String.Join(" ", Args);
            //    //设置启动动作,确保以管理员身份运行
            //    startInfo.Verb = "runas";
            //    //如果不是管理员，则启动UAC
            //    System.Diagnostics.Process.Start(startInfo);
            //    //退出
            //    System.Windows.Forms.Application.Exit();
            //}
        }

        ///<summary>        
        ///解析程序集失败，会加载对应的程序集，成功解决了cef发布的子目录访问问题。
        ///也可以在config文件中指定，这样可能会更好，但是需要研究一下怎么在启动的时候更新config文件
        ///这个地方也可以做一个记录，看看那些dll文件没有加载成功
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static Assembly OnResolveAssembly(object sender, ResolveEventArgs args)
        {

            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = new AssemblyName(args.Name);
#if DEBUG
            Console.WriteLine("激活OnResolveAssembly:" + args.Name);
#endif

            var assemblyAllName = assemblyName.Name + ".dll";
            //加载CefSharp相关库
            string appPath = Application.StartupPath;
            const string CefLibDir = @"plugins\HIS.Browser"; //cef目录名称
            string cef_root = Path.Combine(appPath, CefLibDir);
            if (args.Name.StartsWith("CefSharp"))
            {
                string assemblyPath = Path.Combine(Application.StartupPath, CefLibDir, assemblyAllName);
                if (File.Exists(assemblyPath))
                {
                    return Assembly.LoadFile(assemblyPath);
                }
                else
                {
                    return null;
                }
            }
            //判断程序集的区域性
            if (!assemblyName.CultureInfo.Equals(CultureInfo.InvariantCulture))
            {
                assemblyAllName = string.Format(@"{0}\{1}", assemblyName.CultureInfo, assemblyAllName);
            }

            using (Stream stream = executingAssembly.GetManifestResourceStream(assemblyAllName))
            {
                if (stream == null)
                {
                    Console.WriteLine("通过OnResolveAssembly读取未果:" + args.Name);
                    return null;
                }

                var assemblyRawBytes = new byte[stream.Length];
                stream.Read(assemblyRawBytes, 0, assemblyRawBytes.Length);
                Console.WriteLine("通过OnResolveAssembly读取成功:" + args.Name);
                return Assembly.Load(assemblyRawBytes);
            }
        }

        /// <summary>
        /// 绑定程序中的异常处理
        /// </summary>
        private static void BindExceptionHandler()
        {
            //设置应用程序处理异常方式：ThreadException处理
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            //处理UI线程异常
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            //处理未捕获的异常
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        /// <summary>
        /// 处理UI线程异常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            LogHelper.Error(null, e.Exception as Exception);
            MessageBox.Show(e.Exception.Message);
        }

        /// <summary>
        /// 处理未捕获的异常
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogHelper.Error(null, e.ExceptionObject as Exception);
        }



        public static void GetConfig()
        {
            try
            {
                // 2012.12.10 Pcsky 修正bug：加入忽略大小写判断的代码
                // 如： BaseSystemInfo.AllowUserRegister = allowUserRegister.Equals(false.ToString(), StringComparison.CurrentCultureIgnoreCase) ? false : true;

                // 读取客户端配置文件
                BaseSystemInfo.ConfigurationFrom = ConfigurationCategory.UserConfig;
                UserConfigHelper.GetConfig();

                //// 这里应该读取服务上的配置信息，不只是读取本地的配置信息
                //DotNetService dotNetService = new DotNetService();
                //string allowUserRegister = dotNetService.ParameterService.GetServiceConfig(BaseSystemInfo.UserInfo, "AllowUserRegister");
                //BaseSystemInfo.AllowUserRegister = allowUserRegister.Equals(false.ToString(), StringComparison.CurrentCultureIgnoreCase);
                //// 密码强度检查
                //string checkPasswordStrength = dotNetService.ParameterService.GetServiceConfig(BaseSystemInfo.UserInfo, "CheckPasswordStrength");
                //BaseSystemInfo.CheckPasswordStrength = checkPasswordStrength.Equals(false.ToString(), StringComparison.CurrentCultureIgnoreCase);
                //// 密码错误锁定次数
                //string passwordErrorLockLimit = dotNetService.ParameterService.GetServiceConfig(BaseSystemInfo.UserInfo, "PasswordErrorLockLimit");
                //if (!string.IsNullOrEmpty(passwordErrorLockLimit))
                //{
                //    BaseSystemInfo.PasswordErrorLockLimit = int.Parse(passwordErrorLockLimit);
                //}
                //string passwordErrorLockCycle = dotNetService.ParameterService.GetServiceConfig(BaseSystemInfo.UserInfo, "PasswordErrorLockCycle");
                //if (!string.IsNullOrEmpty(passwordErrorLockCycle))
                //{
                //    BaseSystemInfo.PasswordErrorLockCycle = int.Parse(passwordErrorLockCycle);
                //}

                //string useTableColumnPermission = dotNetService.ParameterService.GetServiceConfig(BaseSystemInfo.UserInfo, "UseTableColumnPermission");
                //BaseSystemInfo.UseTableColumnPermission = useTableColumnPermission.Equals(true.ToString(), StringComparison.CurrentCultureIgnoreCase);
                //string usePermissionScope = dotNetService.ParameterService.GetServiceConfig(BaseSystemInfo.UserInfo, "UsePermissionScope");
                //BaseSystemInfo.UsePermissionScope = usePermissionScope.Equals(true.ToString(), StringComparison.CurrentCultureIgnoreCase);

                //if (dotNetService.ParameterService is ICommunicationObject)
                //{
                //    ((ICommunicationObject)dotNetService.ParameterService).Close();
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }




}