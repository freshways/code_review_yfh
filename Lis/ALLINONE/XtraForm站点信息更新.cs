﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ALLINONE
{
    public partial class XtraForm站点信息更新 : DevExpress.XtraEditors.XtraForm
    {
        private string _s站点信息;


        public XtraForm站点信息更新()
        {
            InitializeComponent();
            textEdit站点名称.Text = HIS.COMM.zdInfo.Model站点信息.站点名称;
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            _s站点信息 = textEdit站点名称.Text.Trim();
            if (_s站点信息.Length == 0)
            {
                MessageBox.Show("请填写站点信息");
                return;
            }
            string _sql = "update GY站点设置 set 站点名称='" +
                _s站点信息 + "' where Mac地址='" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "'";
            HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.DBConnHelper.SConnHISPubDb, CommandType.Text, _sql);
            HIS.COMM.zdInfo.Model站点信息.站点名称 = _s站点信息;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}