﻿using WEISHENG.COMM.PluginsAttribute;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ALLINONE
{
    public partial class Form模块版本说明 : Form
    {
        public Form模块版本说明(PluginAssemblyInfoAttribute pluginAssemblyInfo)
        {
            InitializeComponent();
            var temp = HIS.Model.dataHelper.plugDB.plug_插件版本库.Where(c => c.插件名称 == pluginAssemblyInfo.插件名称)
               .Select(a => new
               {
                   a.插件名称,
                   a.版本号,
                   a.版本说明,
                   a.ID
               })
               .OrderByDescending(c => c.ID).ToList();
            if (temp.Count == 0)
            {
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.Append(temp.First().插件名称);
            foreach (var item in temp)
            {
                sb.Append("\r\n" + item.版本号 + ": " + item.版本说明);
            }

            richTextBox1.Text = sb.ToString();


        }

        private void AboutWindow_Load(object sender, EventArgs e)
        {

        }
    }
}
