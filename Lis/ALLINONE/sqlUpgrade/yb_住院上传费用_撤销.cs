﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALLINONE.sqlUpgrade
{
    public class yb_住院上传费用_撤销
    {
        public static bool bCreateTable()
        {
            string sql = @"
if not exists (select * from sysobjects where id = object_id('yb_住院上传费用_撤销') and OBJECTPROPERTY(id, 'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[yb_住院上传费用_撤销] (
[Id] numeric(18) NOT NULL IDENTITY(1,1) NOT FOR REPLICATION ,
[金额] numeric(10,2) NULL ,
[自理金额] numeric(12,2) NULL ,
[自费金额] numeric(12,2) NULL ,
[超限价自付金额] numeric(10,2) NULL ,
[收费类别] varchar(10) NULL ,
[收费项目等级] varchar(10) NULL ,
[全额自费标志] varchar(10) NULL ,
[自理比例] numeric(10,2) NULL ,
[限价] numeric(10,2) NULL ,
[说明信息] varchar(10) NULL ,
[交易流水号] nvarchar(50) NULL ,
[费用ID] int NULL ,
[ZYID] int NULL ,
[上传操作时间] datetime NULL ,
[撤销操作时间] datetime NULL ,
[门诊或住院流水号] varchar(50) NULL 
)
ALTER TABLE [dbo].[yb_住院上传费用_撤销] ADD PRIMARY KEY ([Id])
end
";
            HIS.Model.Dal.DbHelperSqlServer.ExecuteSql(sql);
            return true;
        }
    }
}
