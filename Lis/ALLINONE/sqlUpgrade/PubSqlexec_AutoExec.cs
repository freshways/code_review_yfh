﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WEISHENG.COMM;

namespace ALLINONE.sqlUpgrade
{
    public static class PubSqlexec_AutoExec
    {
        public static bool AutoExec()
        {
            try
            {
                string sqlGetSqlData =
                        "SELECT  [ID]" + "\r\n" +
                        "      ,[sqlName]" + "\r\n" +
                        "      ,[sqlContext]" + "\r\n" +
                        "      ,[readMe]" + "\r\n" +
                        "      ,[exeTime]" + "\r\n" +
                        "      ,[isSuccessRun]" + "\r\n" +
                        "      ,[createTime]" + "\r\n" +
                        "  FROM  [pubSqlExec]" + "\r\n" +
                    "  where exeTime is null";
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, sqlGetSqlData).Tables[0];
                if (dt.Rows.Count == 0)
                {
                    return false;
                }
                foreach (DataRow item in dt.Rows)
                {
                    String exesql = item["sqlContext"].ToString();
                    string iid = item["id"].ToString();
                    try
                    {
                        int iRe = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, exesql);
                        WEISHENG.COMM.LogHelper.Info("HIS.COMM", "tbPubSqlExec", string.Format("执行'{0}',影响行数{1},id={0}", iid, iRe.ToString()));
                        string sqlupdate = string.Format("update pubSqlExec set exeTime=getdate(),isSuccessRun=1 where id={0}", iid);
                        HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, sqlupdate);
                    }
                    catch (Exception ex)
                    {
                        WEISHENG.COMM.LogHelper.Info("HIS.COMM", "tbPubSqlExec", ex.Message);
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                msgHelper.ShowInformation(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 判断当天是否需要自动记费
        /// </summary>
        /// <returns></returns>
        private static bool needCharging()
        {
            string SQL = "select count(ID) from [ZY在院费用] where [记费人编码]=-99 and 记费时间=CONVERT(varchar(50),GETDATE(),23)";
            int iRe = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL));
            return iRe == 0;
        }


        public static bool bCreateTable_pubSqlExec()
        {
            string sqlCreateTable = @"if not exists (select * from sysobjects where id = object_id('pubSqlExec') and OBJECTPROPERTY(id, 'IsUserTable') = 1)
                                        BEGIN
                                        CREATE TABLE [dbo].[pubSqlExec] (
                                        [ID] int NOT NULL IDENTITY(1,1) ,
                                        [sqlName] varchar(50) NULL ,
                                        [sqlContext] varchar(500) NULL ,
                                        [readMe] varchar(500) NULL ,
                                        [exeTime] datetime NULL ,
                                        [isSuccessRun] bit NULL ,
                                        [guid] varchar(100) NULL ,
                                        [createTime] datetime NULL DEFAULT (getdate()) 
                                        )
                                        ALTER TABLE [dbo].[pubSqlExec] ADD PRIMARY KEY ([ID])
                                        ALTER TABLE [dbo].[pubSqlExec] ADD CONSTRAINT [guidOnly] UNIQUE ([guid])
                                        end";
            HIS.Model.Dal.DbHelperSqlServer.ExecuteSql(sqlCreateTable);
            return true;
        }


        /// <summary>
        /// 增加要自动执行的语句
        /// </summary>
        /// <returns></returns>
        public static bool bAddTodoSqlExec()
        {
            string sqlToExec = @"
                   if NOT exists(select * from pubSqlExec where guid='CF58EB24-A36E-4D6D-A68A-6D5988CDCE66')   
INSERT INTO [pubSqlExec] (
	[sqlName],
	[sqlContext],
	[readMe],
	[exeTime],
	[isSuccessRun],
	[guid]
)
VALUES
	(
		'增加医保上传功能',
		'INSERT INTO pubQxNew2 ([模块名], [键ID], [父ID], [命名空间], [窗口名], [图标索引], [显示顺序], [是否显示], [传递参数], [GUID]) VALUES (''住院医保费用上传'', ''10002'', ''10000'',''HIS.YBJK'', ''XtraForm住院医保费用上传'', ''1'', ''1'', ''1'', NULL, ''80721A4F-46BC-4362-A78F-D7217BACD1C0'');',
		NULL,
		NULL,
		NULL,
		'CF58EB24-A36E-4D6D-A68A-6D5988CDCE66'
	);
if not exists(select * from pubsqlexec where guid='E051B8C4-3802-4073-99BC-98D599CE51E5')   
insert into [pubsqlexec] (
	[sqlname],
	[sqlcontext],
	[readme],
	[exetime],
	[issuccessrun],
	[guid]
)
values
	(
		'增加主键',
		'alter table [dbo].[pubqxnew2] add constraint [jianid] unique ([键id] asc)',
		null,
		null,
		null,
		'E051B8C4-3802-4073-99BC-98D599CE51E5'
	);

if not exists(select * from pubsqlexec where guid='6358A17F-A5FF-4279-A7AF-FD3531563C57')   
insert into [pubsqlexec] (
	[sqlname],
	[sqlcontext],
	[readme],
	[exetime],
	[issuccessrun],
	[guid]
)
values
	(
		'增加医保接口父菜单',
		'insert into pubqxnew2 ([模块名], [键id], [父id],[命名空间], [窗口名], [图标索引], [显示顺序], [是否显示], [传递参数], [guid]) values (''医保目录'', ''10001'', ''10000'',  ''his.ybjk'', ''xtraform医保目录'', ''1'', ''1'', ''1'', null, ''672d0eec-5646-40bf-bbd7-47f40a7c81b5'');',
		null,
		null,
		null,
		'6358A17F-A5FF-4279-A7AF-FD3531563C57'
	);

if not exists(select * from pubsqlexec where guid='22255770-5D8E-4A25-A394-C8BD274BAE4D')   
insert into [pubsqlexec] (
	[sqlname],
	[sqlcontext],
	[readme],
	[exetime],
	[issuccessrun],
	[guid]
)
values
	(
		'增加医保目录浏览',
		'insert into pubqxnew2 ([模块名], [键id], [父id], [命名空间], [窗口名], [图标索引], [显示顺序], [是否显示], [传递参数], [guid]) values (''医保接口'', ''10000'', ''0'',  ''his.ybjk'', null, null, null, null, null, ''ff605bc0-b79b-4183-94cc-68339654a81e'');',
		null,
		null,
		null,
		'22255770-5D8E-4A25-A394-C8BD274BAE4D'
	);
                    ";
            int i = HIS.Model.Dal.DbHelperSqlServer.ExecuteSql(sqlToExec);




            return true;
        }


    }
}
