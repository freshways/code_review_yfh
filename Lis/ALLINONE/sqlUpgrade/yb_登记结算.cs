﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALLINONE.sqlUpgrade
{
    class yb_登记结算
    {
        public static bool b建表()
        {
            string sqlTableCreate = @"
if not exists (select * from sysobjects where id = object_id('yb_住院登记结算') and OBJECTPROPERTY(id, 'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[yb_住院登记结算] (
[病人姓名] varchar(20) NULL ,
[Ic卡号] varchar(50) NULL ,
[单位编码] varchar(50) NULL ,
[个人社保编号] varchar(50) NULL ,
[医疗人员类别] varchar(50) NULL ,
[身份证号] varchar(50) NULL ,
[住院疾病编码] varchar(50) NULL ,
[医保登记流水号] varchar(100) NULL DEFAULT ('未定义') ,
[医保结算流水号] varchar(100) NULL ,
[操作时间] datetime NULL DEFAULT (getdate()) ,
[入院日期] datetime NULL ,
[出院日期] datetime NULL ,
[住院号] varchar(50) NOT NULL ,
[医保结算时间] datetime NULL ,
[结算状态] varchar(50) NULL ,
[总费用] numeric(18,2) NULL DEFAULT ((0)) ,
[统筹支付] numeric(18,2) NULL ,
[公务员补助] numeric(18,2) NULL DEFAULT ((0)) ,
[政府补助] numeric(18,2) NULL DEFAULT ((0)) ,
[救助金] numeric(18,2) NULL DEFAULT ((0)) ,
[报销金额] numeric(18,2) NULL DEFAULT ((0)) ,
[作废标志] varchar(10) NOT NULL DEFAULT ('0') ,
[登记经办人] varchar(20) NULL ,
[结算经办人] varchar(20) NULL ,
[科室] varchar(30) NULL ,
[Id] numeric(18) NOT NULL IDENTITY(1,1) NOT FOR REPLICATION ,
[hosId] varchar(50) NOT NULL ,
[结算次数] int NOT NULL DEFAULT ((0)) ,
[登记次数] int NOT NULL DEFAULT ((0)) ,
[人员类别] varchar(10) NULL ,
[登记医院流水号] varchar(30) NULL ,
[出院诊断编码] varchar(50) NULL ,
[出院副诊断编码] varchar(50) NULL ,
[医生编码] varchar(20) NULL ,
[医生姓名] varchar(20) NULL ,
[结账时间] datetime NULL ,
[汇总时间] datetime NULL ,
[月结时间] datetime NULL ,
[汇总人] varchar(20) NULL ,
[月结人] varchar(20) NULL ,
[个人帐户支出] numeric(8,2) NULL ,
[个人现金支付] numeric(8,2) NULL ,
[个人自费金额] numeric(8,2) NULL ,
[分档自理金额] numeric(8,2) NULL ,
[超过封顶线个人自付金额] numeric(8,2) NULL ,
[本次进入统筹金额] numeric(8,2) NULL ,
[卡片类型] varchar(2) NULL ,
[无卡交易] bit NULL ,
[联系人姓名] varchar(10) NULL ,
[联系人亲属关系] varchar(10) NULL ,
[联系电话] varchar(30) NULL ,
[入院医疗类别] varchar(50) NULL ,
[报销医疗类别] varchar(50) NULL ,
[内部单位编码] varchar(20) NULL DEFAULT ((1)) ,
[原交易日期] varchar(50) NULL ,
[银行消费流水号] varchar(50) NULL ,
[银行交易索引号] varchar(100) NULL ,
[银行参考号] varchar(100) NULL ,
[银行三代卡社保号] varchar(50) NULL ,
[银行POS流水号] varchar(50) NULL ,
[银行终端号] varchar(50) NULL ,
[银行商户号] varchar(50) NULL ,
[救助金额] numeric(10,2) NULL ,
[救助金封顶上金额] numeric(10,2) NULL ,
[起付线] numeric(10,2) NULL ,
[起付线支付] numeric(10,2) NULL ,
[年度] varchar(10) NULL ,
[本年住院次数累计] int NULL ,
[交易时间] varchar(16) NULL ,
[行政区划代码] varchar(20) NULL ,
[领款人姓名] varchar(20) NULL ,
[领款人与患者关系] varchar(20) NULL ,
[领款人联系电话] varchar(50) NULL ,
[ZYID] varchar(20) NULL ,
[业务周期号] varchar(50) NULL ,
[医院交易流水号] varchar(70) NULL ,
[结算单据号] varchar(22) NULL ,
[银行账号] varchar(50) NULL ,
[银行退款成功] bit NULL DEFAULT ((0)) ,
[超限价自付费用] numeric(12,2) NULL 
)

ALTER TABLE [dbo].[yb_住院登记结算] ADD PRIMARY KEY ([Id])
end";
            HIS.Model.Dal.DbHelperSqlServer.ExecuteSql(sqlTableCreate);
            return true;

        }
        public static bool dataTranslate()
        {
            string sqlDataTranslate = @"
SET IDENTITY_INSERT [dbo].[yb_住院登记结算] ON

INSERT INTO [yb_住院登记结算] (
	[病人姓名],
	[Ic卡号],
	[单位编码],
	[个人社保编号],
	[医疗人员类别],
	[身份证号],
	[住院疾病编码],
	[医保登记流水号],
	[医保结算流水号],
	[操作时间],
	[入院日期],
	[出院日期],
	[住院号],
	[医保结算时间],
	[结算状态],
	[总费用],
	[统筹支付],
	[公务员补助],
	[政府补助],
	[救助金],
	[报销金额],
	[作废标志],
	[登记经办人],
	[结算经办人],
	[科室],
	[Id],
	[hosId],
	[结算次数],
	[登记次数],
	[人员类别],
	[登记医院流水号],
	[出院诊断编码],
	[出院副诊断编码],
	[医生编码],
	[医生姓名],
	[结账时间],
	[汇总时间],
	[月结时间],
	[汇总人],
	[月结人],
	[个人帐户支出],
	[个人现金支付],
	[个人自费金额],
	[分档自理金额],
	[超过封顶线个人自付金额],
	[本次进入统筹金额],
	[卡片类型],
	[无卡交易],
	[联系人姓名],
	[联系人亲属关系],
	[联系电话],
	[入院医疗类别],
	[报销医疗类别],
	[内部单位编码],
	[原交易日期],
	[银行消费流水号],
	[银行交易索引号],
	[银行参考号],
	[银行三代卡社保号],
	[银行POS流水号],
	[银行终端号],
	[银行商户号],
	[救助金额],
	[救助金封顶上金额],
	[起付线],
	[起付线支付],
	[年度],
	[本年住院次数累计],
	[交易时间],
	[行政区划代码],
	[领款人姓名],
	[领款人与患者关系],
	[领款人联系电话],
	[ZYID],
	[业务周期号],
	[医院交易流水号],
	[结算单据号],
	[银行账号],
	[银行退款成功],
	[超限价自付费用]
)
select [病人姓名],
	[Ic卡号],
	[单位编码],
	[个人社保编号],
	[医疗人员类别],
	[身份证号],
	[住院疾病编码],
	[医保登记流水号],
	[医保结算流水号],
	[操作时间],
	[入院日期],
	[出院日期],
	[住院号],
	[医保结算时间],
	[结算状态],
	[总费用],
	[统筹支付],
	[公务员补助],
	[政府补助],
	[救助金],
	[报销金额],
	[作废标志],
	[登记经办人],
	[结算经办人],
	[科室],
	[Id],
	[hosId],
	[结算次数],
	[登记次数],
	[人员类别],
	[登记医院流水号],
	[出院诊断编码],
	[出院副诊断编码],
	[医生编码],
	[医生姓名],
	[结账时间],
	[汇总时间],
	[月结时间],
	[汇总人],
	[月结人],
	[个人帐户支出],
	[个人现金支付],
	[个人自费金额],
	[分档自理金额],
	[超过封顶线个人自付金额],
	[本次进入统筹金额],
	[卡片类型],
	[无卡交易],
	[联系人姓名],
	[联系人亲属关系],
	[联系电话],
	[入院医疗类别],
	[报销医疗类别],
	[内部单位编码],
	[原交易日期],
	[银行消费流水号],
	[银行交易索引号],
	[银行参考号],
	[银行三代卡社保号],
	[银行POS流水号],
	[银行终端号],
	[银行商户号],
	[救助金额],
	[救助金封顶上金额],
	[起付线],
	[起付线支付],
	[年度],
	[本年住院次数累计],
	[交易时间],
	[行政区划代码],
	[领款人姓名],
	[领款人与患者关系],
	[领款人联系电话],
	[ZYID],
	[业务周期号],
	[医院交易流水号],
	[结算单据号],
	[银行账号],
	[银行退款成功],
	[超限价自付费用]
from ybjk.dbo.tbhisybdj
where 医保登记流水号 not IN
(select 医保登记流水号 from [yb_住院登记结算])

SET IDENTITY_INSERT [dbo].[yb_住院登记结算] OFF

";
            HIS.Model.Dal.DbHelperSqlServer.ExecuteSql(sqlDataTranslate);
            return true;
        }

        public static bool b更新tbhisybdj的zyid()
        {
            string sql = @"
                    update aa set aa.zyid=bb.zyid
                    from ybjk.dbo.tbhisybdj aa 
                    left OUTER join zy病人信息 bb 
                    on aa.住院号=case when bb.住院次数>1 then convert(varchar(10),bb.住院号码)+'-'+convert(varchar(2),bb.住院次数)
                    else bb.住院号码 end
                    where aa.结算状态<>1 and 作废标志=0 and 医保登记流水号<>'未定义' and bb.zyid is not null AND aa.zyid is null
                    ";
            HIS.Model.Dal.DbHelperSqlServer.ExecuteSql(sql);
            return true;
        }
    }
}
