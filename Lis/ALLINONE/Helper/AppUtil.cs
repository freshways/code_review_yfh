﻿//#define ALLOW_REMOVE_DLL_DIRS

namespace ALLINONE
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Runtime.InteropServices;

    public static partial class AppUtil
    {
        //当前 exe 路径和目录============= begin===========
        public static string AppExePath { get; } = Assembly.GetEntryAssembly().Location;
        public static string AppExeDir { get; } = Path.GetDirectoryName(AppExePath) + Path.DirectorySeparatorChar;
        public static string AppExePath1 { get; } = Path.GetFullPath(Assembly.GetEntryAssembly().CodeBase.Substring(8));
        public static string AppExeDir1 { get; } = AppDomain.CurrentDomain.BaseDirectory;
        public static string AppExeDir2 { get; } = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
        static AppUtil()
        {
            System.Diagnostics.Debug.Assert(AppExePath == AppExePath1);
            System.Diagnostics.Debug.Assert(AppExeDir == AppExeDir1);
            System.Diagnostics.Debug.Assert(AppExeDir1 == AppExeDir2);
        }
        //当前 exe 路径和目录============= end============


        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern bool SetDllDirectory(string dir);

        [DefaultDllImportSearchPaths(DllImportSearchPath.UseDllDirectoryForDependencies)]
        public static void Set64Or32BitDllDir(string x64DirName = @"dlls\x64", string x86DirName = @"dlls\x86")
        {
            var dir = IntPtr.Size == 8 ? x64DirName : x86DirName;
            if (!Path.IsPathRooted(dir)) dir = AppExeDir + dir;
            if (!SetDllDirectory(dir))
                throw new System.ComponentModel.Win32Exception(nameof(SetDllDirectory));
        }






        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern bool SetDefaultDllDirectories(int flags = 0x1E00);
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr AddDllDirectory(string dir);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern bool RemoveDllDirectory(IntPtr cookie);

        public static Dictionary<string, IntPtr> DllDirs { get; } = new Dictionary<string, IntPtr>();


        public static readonly string[] x64DefaultDllDirs = new[] { @"dlls\x64" };
        public static readonly string[] x86DefaultDllDirs = new[] { @"dlls\x86" };

        public static void Set64Or32BitDllDirs(IEnumerable<string> x64DirNames, IEnumerable<string> x86DirNames)
        {
            if (null == x64DirNames && null == x86DirNames)
                throw new ArgumentNullException($"Must set at least one of {nameof(x64DirNames)} or {nameof(x86DirNames)}");

            if (!SetDefaultDllDirectories())
                throw new System.ComponentModel.Win32Exception(nameof(SetDefaultDllDirectories));

            AddDllDirs(IntPtr.Size == 8 ? x64DirNames ?? x64DefaultDllDirs : x86DirNames ?? x86DefaultDllDirs);
        }

        public static void AddDllDirs(IEnumerable<string> dirNames)
        {
            foreach (var dn in dirNames)
            {
                var dir = Path.IsPathRooted(dn) ? dn : AppExeDir + dn;

                if (!DllDirs.ContainsKey(dir))
                    DllDirs[dir] =

                AddDllDirectory(dir);
            }
        }
        public static void AddDllDirs(params string[] dirNames) => AddDllDirs(dirNames);


        public static void RemoveDllDirs(IEnumerable<string> dirNames)
        {
            foreach (var dn in dirNames)
            {
                var dir = Path.IsPathRooted(dn) ? dn : AppExeDir + dn;
                if (DllDirs.TryGetValue(dir, out IntPtr cookie))
                    RemoveDllDirectory(cookie);
            }
        }
        public static void RemoveDllDirs(params string[] dirNames) => RemoveDllDirs(dirNames);

    }

    /// <summary>
    /// 检测 dll 程序集是否可加载到当前进程
    /// </summary>
    public static partial class ReflectionX
    {
        private static readonly ProcessorArchitecture CurrentProcessorArchitecture = IntPtr.Size == 8 ? ProcessorArchitecture.Amd64 : ProcessorArchitecture.X86;
        public static AssemblyName GetLoadableAssemblyName(this string dllPath)
        {
            try
            {
                var an = AssemblyName.GetAssemblyName(dllPath);
                switch (an.ProcessorArchitecture)
                {
                    case ProcessorArchitecture.MSIL: return an;
                    case ProcessorArchitecture.Amd64:
                    case ProcessorArchitecture.X86: return CurrentProcessorArchitecture == an.ProcessorArchitecture ? an : null;
                }
            }
            catch { }
            return null;
        }
    }
}