﻿using HIS.COMM;
using HIS.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using WEISHENG.COMM;
using WEISHENG.COMM.PluginsAttribute;

namespace ALLINONE
{
    public class PluginHelper
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public static string getFileVersion(string filename)
        {
            FileVersionInfo myFileVersion = FileVersionInfo.GetVersionInfo(filename);
            return myFileVersion.FileVersion;
        }



        public void LoadLocalPlugins_ToDb_DelOldFile(out string Elapsed)
        {
            using (WEISHENG.COMM.AutoStopwatch watch = new WEISHENG.COMM.AutoStopwatch())
            {
                try
                {
                    string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "plugins");
                    var theFolder = new DirectoryInfo(path);
                    var plugins = theFolder.GetDirectories();
                    List<plug_全局插件> list全局信息 = chis.plug_全局插件.ToList();
                    List<pubQx> list全局PubQx = chis.pubQxes.ToList();
                    chis.Database.ExecuteSqlCommand($"delete plug_站点插件 where 站点标识='{WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress}'");
                    foreach (var plugin in plugins)
                    {
                        string dllFileName = $@"{plugin.FullName}\{plugin.Name}.dll";
                        try
                        {
                            //加载程序集
                            Assembly assembly = CacheManager.Instance.Load(dllFileName, CacheManager.enLoadType.LoadFile);
                            if (assembly != null)
                            {
                                PluginAssemblyInfoAttribute assemblyInfo = PlugInfoHelper.getAssemblyInfo(assembly);
                                if (assemblyInfo != null)
                                {
                                    var plug_全局插件 = list全局信息.Where(c => c.插件GUID == Guid.Parse(assemblyInfo.插件GUID)).FirstOrDefault();
                                    if (plug_全局插件 == null)
                                    {
                                        plug_全局插件 = new HIS.Model.plug_全局插件()
                                        {
                                            插件名称 = assemblyInfo.插件名称,
                                            功能说明 = assemblyInfo.功能说明,                                            
                                            维护团队 = assemblyInfo.维护团队,
                                            联系方式 = assemblyInfo.联系方式,
                                            插件文件名 = plugin.Name,
                                            插件授权 = assemblyInfo.插件授权,
                                            插件更新日期 = assemblyInfo.插件更新日期,
                                            插件是否有效 = assemblyInfo.插件是否有效,
                                            插件GUID = System.Guid.Parse(assemblyInfo.插件GUID),
                                            插件网址 = assemblyInfo.插件网址,
                                            插件运行目录 = assemblyInfo.插件运行目录,
                                            createTime = DateTime.Now,
                                        };
                                        chis.plug_全局插件.Attach(plug_全局插件);
                                        chis.Entry(plug_全局插件).State = System.Data.Entity.EntityState.Added;
                                    }
                                    else
                                    {
                                        plug_全局插件.插件名称 = assemblyInfo.插件名称;
                                        plug_全局插件.功能说明 = assemblyInfo.功能说明;                                        
                                        plug_全局插件.维护团队 = assemblyInfo.维护团队;
                                        plug_全局插件.联系方式 = assemblyInfo.联系方式;
                                        plug_全局插件.插件文件名 = plugin.Name;
                                        plug_全局插件.插件授权 = assemblyInfo.插件授权;
                                        plug_全局插件.插件更新日期 = assemblyInfo.插件更新日期;
                                        plug_全局插件.插件是否有效 = assemblyInfo.插件是否有效;
                                        plug_全局插件.插件网址 = assemblyInfo.插件网址;
                                        plug_全局插件.插件运行目录 = assemblyInfo.插件运行目录;
                                        plug_全局插件.createTime = DateTime.Now;

                                        chis.plug_全局插件.Attach(plug_全局插件);
                                        chis.Entry(plug_全局插件).State = System.Data.Entity.EntityState.Modified;
                                    }


                                    var plug_本机插件 = new HIS.Model.plug_站点插件()
                                    {
                                        插件名称 = assemblyInfo.插件名称,
                                        功能说明 = assemblyInfo.功能说明,
                                        插件版本 = assemblyInfo.插件版本,
                                        维护团队 = assemblyInfo.维护团队,
                                        联系方式 = assemblyInfo.联系方式,
                                        插件文件名 = plugin.Name,
                                        插件授权 = assemblyInfo.插件授权,
                                        插件更新日期 = assemblyInfo.插件更新日期,
                                        插件是否有效 = assemblyInfo.插件是否有效,
                                        插件GUID = System.Guid.Parse(assemblyInfo.插件GUID),
                                        createTime = DateTime.Now,
                                        站点标识 = WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress
                                    };
                                    chis.plug_站点插件.Attach(plug_本机插件);
                                    chis.Entry(plug_本机插件).State = System.Data.Entity.EntityState.Added;


                                    //获取自定义插件
                                    var typesClassInfoes = PlugInfoHelper.getClassInfo(assembly);
                                    foreach (var classInfoMarkAttribute in typesClassInfoes)
                                    {
                                        var item_qx = list全局PubQx.Where(c => c.键ID == classInfoMarkAttribute.键ID).FirstOrDefault();
                                        if (item_qx == null)
                                        {
                                            item_qx = new pubQx()
                                            {
                                                功能名称 = classInfoMarkAttribute.功能名称,
                                                键ID = classInfoMarkAttribute.键ID,
                                                父ID = classInfoMarkAttribute.父ID,
                                                程序集名称 = plugin.Name,
                                                程序集调用类地址 = classInfoMarkAttribute.程序集调用类地址,
                                                图标索引 = classInfoMarkAttribute.图标索引,
                                                显示顺序 = classInfoMarkAttribute.显示顺序,
                                                是否显示 = classInfoMarkAttribute.是否显示,
                                                传递参数 = classInfoMarkAttribute.传递参数,
                                                GUID = classInfoMarkAttribute.GUID,
                                                菜单类型 = classInfoMarkAttribute.菜单类型,
                                                图标名称 = classInfoMarkAttribute.图标名称,
                                                全屏打开 = classInfoMarkAttribute.全屏打开,
                                                createTime = DateTime.Now
                                            };
                                            chis.pubQxes.Attach(item_qx);
                                            chis.Entry(item_qx).State = System.Data.Entity.EntityState.Added;
                                        }
                                        else
                                        {
                                            item_qx.功能名称 = classInfoMarkAttribute.功能名称;
                                            item_qx.键ID = classInfoMarkAttribute.键ID;
                                            item_qx.父ID = classInfoMarkAttribute.父ID;
                                            item_qx.程序集名称 = plugin.Name;
                                            item_qx.程序集调用类地址 = classInfoMarkAttribute.程序集调用类地址;
                                            item_qx.图标索引 = classInfoMarkAttribute.图标索引;
                                            item_qx.显示顺序 = classInfoMarkAttribute.显示顺序;
                                            item_qx.是否显示 = classInfoMarkAttribute.是否显示;
                                            item_qx.传递参数 = classInfoMarkAttribute.传递参数;
                                            item_qx.菜单类型 = classInfoMarkAttribute.菜单类型;
                                            item_qx.图标名称 = classInfoMarkAttribute.图标名称;
                                            item_qx.全屏打开 = classInfoMarkAttribute.全屏打开;
                                            item_qx.createTime = DateTime.Now;
                                            chis.pubQxes.Attach(item_qx);
                                            chis.Entry(item_qx).State = System.Data.Entity.EntityState.Modified;
                                        }
                                    }
                                    vDelFile(dllFileName);//删除原来程序目录下面的同名插件                                
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            msgHelper.ShowInformation(plugin.Name + ex.Message);
                        }
                    }
                    chis.SaveChanges();
                    HIS.COMM.msgBalloonHelper.BalloonShow("刷新插件数据成功");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    Elapsed = "加载耗时:" + watch.Elapsed.ToString();
                }
            }
        }



        /// <summary>
        /// 删除老版本的文件，确保只引用插件模式
        /// </summary>
        /// <param name="_s"></param>
        private void vDelFile(string _s)
        {
            string _ss = _s.Replace("plugins\\", "").Replace(".dll", "");
            string _delFileName = "";
            string[] array = _ss.Split('\\');
            List<string> list = new List<string>();
            foreach (string item in array)
            {
                if (!list.Contains(item))
                {
                    list.Add(item);
                    _delFileName += item + "\\";
                }
            }
            _delFileName += ".dll";
            _delFileName = _delFileName.Replace("\\.dll", ".dll");
            WEISHENG.COMM.Utilities.FileUtil.DeleteFile(_delFileName);
        }

    }
}
