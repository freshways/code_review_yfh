﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraNavBar;
using DevExpress.XtraTabbedMdi;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList.Columns;
using DevExpress.Utils;
using HIS.Model;

namespace ALLINONE
{
    public class NavBarLoader
    {
        List<pubQx> list_pubQx;
        public ImageCollection ImList;

        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public delegate void OpenUrl2(pubQx _qx);
        public event OpenUrl2 _Openurl2;


        public NavBarLoader(ImageCollection _imageList)
        {
            ImList = _imageList;
        }


        public void v加载菜单(string _s角色名称, string _s用户名, NavBarControl _nvbar被加载)
        {


            if (_s用户名.ToUpper() == "ADMIN")
            {
                list_pubQx = chis.pubQxes.Where(i => i.ID > 0).OrderBy(c => c.显示顺序).ToList();
            }
            else
            {
                string sql = @"	 select * from pubQx where [键ID] in
										 (
										 select [键ID] from pubJsQx where 权限=1 and [角色名称]='" + HIS.COMM.zdInfo.ModelUserInfo.角色名称 + "')";
                list_pubQx = chis.Database.SqlQuery<pubQx>(sql).OrderBy(c => c.显示顺序).ToList();
            }
            List<pubQx> qx0 = list_pubQx.Where(i => i.父ID == "0").ToList();
            foreach (var dr in qx0) this.CreateNavBarGroup(_nvbar被加载, dr);
        }

        /// <summary>
        /// 创建导航组件按钮(包括创建按钮组(NavBarGroup)和按钮(BarItem)
        /// </summary>
        /// <param name="navBar">NavBarControl对象</param>
        /// <param name="qx0">模块名称</param>
        protected void CreateNavBarGroup(NavBarControl navBar, pubQx qx0)
        {
            //注册group展开事件
            navBar.MouseClick += new MouseEventHandler(this.OnNavBar_MouseClick);

            NavBarGroup group = navBar.Groups.Add();//增加一个Button组.
            group.GroupStyle = NavBarGroupStyle.ControlContainer;
            group.Caption = qx0.功能名称 == string.Empty ? "Unknown Name" : qx0.功能名称;
            group.Name = qx0.功能名称;

            //group.ImageOptions.SmallImageIndex = Convert.ToInt16(qx0.图标索引);
            //group.ImageOptions.SmallImageIndex = Convert.ToInt16(qx0.图标索引);
            //group.LargeImage = ImList.Images[18];// (Bitmap)ALLINONE.Properties.Resources.ResourceManager.GetObject(caption["图标索引"].ToString());



            TreeList tv = new TreeList();
            tv.StateImageList = ImList;

            tv.BeginUnboundLoad();
            this.InitTreeList(tv);

            TreeListColumn col = tv.Columns.Add();
            col.Visible = true;
            col.Fixed = FixedStyle.Left;
            //递归增加菜单            
            CreateGroupTreeView(group, list_pubQx, qx0.键ID, tv, null);
        }


        //group 菜单点击事件
        protected void OnNavBar_MouseClick(object sender, MouseEventArgs e)
        {
            NavBarControl nav = (sender as NavBarControl);//取到NavBarControl对象引用
            NavBarHitInfo hit = nav.CalcHitInfo(e.Location);//计算点击区域的对象
            if (hit.InGroup && hit.InGroupCaption)//点击导航分组按钮
            {
                try
                {
                    nav.ActiveGroup = hit.Group; //立即设置为激活的组                    
                    string moduleName = hit.Group.Caption.ToString();//取组按钮的标题(模块的名称)
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        private void CreateGroupTreeView(NavBarGroup group, List<pubQx> listQx, string parentModuleNO, TreeList tv, TreeListNode perNode)
        {

            foreach (var _qx in listQx)
            {
                if (_qx.父ID == parentModuleNO)
                {
                    TreeListNode moduleRoot = tv.AppendNode(new object[] { _qx.功能名称 }, perNode == null ? null : perNode);
                    moduleRoot.StateImageIndex = ImList.Images.Keys.IndexOf(_qx.图标名称) == -1 ? 0 : ImList.Images.Keys.IndexOf(_qx.图标名称);
                    moduleRoot.Tag = _qx;
                    CreateGroupTreeView(group, listQx, _qx.键ID, tv, moduleRoot);
                }
            }
            tv.ExpandAll();//扩展所有结点
            tv.EndUnboundLoad();
            group.ControlContainer.Controls.Add(tv);
        }

        /// <summary>
        /// 初始化树的显示样式
        /// </summary>
        /// <param name="treeList">树组件</param>
        private void InitTreeList(TreeList treeList)
        {
            treeList.BackColor = Color.Transparent;
            treeList.BorderStyle = BorderStyles.NoBorder;
            treeList.Dock = DockStyle.Fill;
            treeList.MouseClick += new MouseEventHandler(tv_MouseClick);

            treeList.Appearance.Empty.BackColor = Color.Transparent;
            treeList.Appearance.Row.BackColor = Color.Transparent;

            //树视图其它设置
            treeList.OptionsView.ShowCheckBoxes = false;
            treeList.OptionsView.ShowColumns = false;
            treeList.OptionsView.ShowIndicator = false;
            treeList.OptionsView.ShowHorzLines = false;
            treeList.OptionsView.ShowVertLines = true;
            treeList.OptionsView.ShowRoot = true;

            //不显示菜单
            treeList.OptionsMenu.EnableColumnMenu = false;
            treeList.OptionsMenu.EnableFooterMenu = false;

            //操作行为属性设置
            treeList.OptionsDragAndDrop.DragNodesMode = DragNodesMode.None;
            treeList.OptionsBehavior.Editable = false;
            treeList.OptionsBehavior.AllowExpandOnDblClick = false;
            treeList.OptionsBehavior.AutoChangeParent = false;
            treeList.OptionsBehavior.AutoNodeHeight = false;
            treeList.OptionsBehavior.Editable = false;
            treeList.OptionsBehavior.KeepSelectedOnClick = true;
            treeList.OptionsBehavior.PopulateServiceColumns = false;
            treeList.OptionsBehavior.ResizeNodes = true;
            treeList.OptionsBehavior.ShowEditorOnMouseUp = false;
            treeList.OptionsBehavior.ShowToolTips = false;
            treeList.OptionsBehavior.SmartMouseHover = true;
            //treeList.OptionsBehavior.DragNodes = false;
            //treeList.OptionsBehavior.UseTabKey = false;
            //treeList.OptionsBehavior.CanCloneNodesOnDrop = false;
            //treeList.OptionsBehavior.AutoFocusNewNode = false;
            //treeList.OptionsBehavior.DragNodes = true;
            //treeList.OptionsBehavior.MoveOnEdit = false;

            //显示样式
            treeList.Appearance.FocusedCell.BackColor = Color.FromArgb(239, 235, 156);
            treeList.Appearance.FocusedCell.Options.UseBackColor = true;
            treeList.Appearance.FocusedRow.BackColor = Color.FromArgb(230, 230, 250);
            treeList.Appearance.FocusedRow.Options.UseBackColor = true;
            treeList.Appearance.HideSelectionRow.BackColor = Color.FromArgb(212, 212, 223);
            treeList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            treeList.Appearance.HorzLine.BackColor = Color.Silver;
            treeList.Appearance.HorzLine.Options.UseBackColor = true;
            treeList.Appearance.VertLine.BackColor = Color.Silver;
            treeList.Appearance.VertLine.Options.UseBackColor = true;

            //treeList.OptionsBehavior.AllowExpandOnDblClick = true;
        }

        /// <summary>
        /// TreeNode.Click事件，判断条件可以在细化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tv_MouseClick(object sender, MouseEventArgs e)
        {
            if ((sender as TreeList).FocusedNode != null)
            {
                TreeListNode node = (sender as TreeList).FocusedNode;
                pubQx _qx = (pubQx)node.Tag;

                if (_qx == null)
                {
                    return;
                }

                if (node.HasChildren)
                {
                    if (node.Expanded)
                    {
                        node.Expanded = false;
                    }
                    else
                    {
                        node.Expanded = true;
                        //node.ExpandAll();
                    }
                    return;
                }
                node.Selected = true;
                _Openurl2(_qx);
            }
        }


    }

}
