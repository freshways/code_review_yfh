﻿using HIS.Model;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using WEISHENG.COMM;
using WEISHENG.COMM.PluginsAttribute;

namespace ALLINONE
{
    public class upgradeHelper
    {
        public static bool check_plug_byGUID(Assembly assembly)
        {
            try
            {
                PluginAssemblyInfoAttribute assemblyInfo = WEISHENG.COMM.PluginsAttribute.PlugInfoHelper.getAssemblyInfo(assembly);
                Version vLocal = new Version(assemblyInfo.插件版本);
                var remote_ver = dataHelper.plugDB.plug_全局插件.Where(c => c.插件GUID == Guid.Parse(assemblyInfo.插件GUID)).FirstOrDefault();
                Version vServer = new Version(remote_ver.插件限定版本);
                if (vLocal < vServer)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                msgHelper.ShowInformation(ex.Message);
                return false;
            }
        }

        public static void check主程序()
        {
            var ver限定最低版本 = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("主程序限定版本", "1.0.0.1", "");
            var var当前版本 = new Version(Application.ProductVersion);
            if (var当前版本 < new Version(ver限定最低版本))
            {
                msgHelper.ShowInformation($"主程序版本不符合要求，请下载包含主程序【{ver限定最低版本}】的升级包。");

                if (System.IO.File.Exists(zdInfo.upgrade_tools_name) == false)
                {
                    MessageBox.Show("升级工具丢失，请联系软件维护人员处理故障", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                System.Diagnostics.Process.Start(zdInfo.upgrade_tools_name);
                Application.Exit();
            }
        }
        public static bool check更新自动升级组件()
        {
            //升级自动升级工具
            //0.1 升級工具丟失，自動下載
            if (System.IO.File.Exists(zdInfo.upgrade_tools_name) == false)
            {
                WEISHENG.COMM.msgHelper.ShowInformation("升级工具丢失，将尝试从插件库中自动下载");
                if (downloadUpgradeToolsWithoutZip() == false)
                {
                    msgHelper.ShowInformation("下载失败");
                    return false;
                }
            }

            //0.2 判断版本自動下載
            Version vLocal = new Version(PluginHelper.getFileVersion(zdInfo.upgrade_tools_name));
            var item_ver = HIS.COMM.Helper.EnvironmentHelper.UpgradeToolsLimitVersion();
            string _rVer = item_ver == null ? "2.0.0.0" : item_ver;
            Version vServer = new Version(_rVer);
            if (vLocal < vServer)
            {
                //下载升级工具，更新站点信息
                downloadUpgradeToolsWithoutZip();
            }
            return true;

        }

        /// <summary>
        /// 从插件库下载最新升级工具
        /// </summary>
        /// <returns></returns>
        public static bool download_upgrade_tools()
        {
            try
            {
                var item2 = HIS.Model.dataHelper.plugDB.plug_插件版本库.Where(c => c.插件包类型 == "升级工具").Select(c => new { c.ID }).OrderByDescending(c => c.ID).FirstOrDefault();
                if (item2 == null)
                {
                    WEISHENG.COMM.msgHelper.ShowInformation("从插件库中未检索到自动升级工具,自动下载升级工具失败");
                    return false;
                }
                bDownloadPlugins(item2.ID);
                return true;
            }
            catch (Exception ex)
            {
                WEISHENG.COMM.msgHelper.ShowInformation(ex.InnerException.Message);
                LogHelper.Info(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 从专门的数据表下载升级文件，不使用解压缩，避免无法更新ICSharpCode.SharpZipLib.dll文件
        /// </summary>
        /// <returns></returns>
        public static bool downloadUpgradeToolsWithoutZip()
        {
            try
            {
                var tools = HIS.Model.dataHelper.plugDB.plugAutomaticUpgradeTools.ToList();
                if (tools == null)
                {
                    WEISHENG.COMM.msgHelper.ShowInformation("从插件库中未检索到自动升级工具,自动下载升级工具失败");
                    return false;
                }
                foreach (var item in tools)
                {
                    string fullFileName = "";
                    if (item.FileName.Contains("dll"))
                    {
                        fullFileName = Application.StartupPath + @"\support\ExtDlls\" + item.FileName;                      
                    }
                    else
                    {
                        fullFileName = Application.StartupPath +@"\"+ item.FileName;
                    }
                    if (File.Exists(fullFileName))
                    {
                        File.Delete(fullFileName);
                    }
                    byte[] mybyte = item.Content;
                    FileStream myfs = new FileStream(fullFileName, FileMode.CreateNew);
                    BinaryWriter writefile = new BinaryWriter(myfs);
                    writefile.Write(mybyte, 0, mybyte.Length);
                    writefile.Close();
                    myfs.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                WEISHENG.COMM.msgHelper.ShowInformation(ex.InnerException.Message);
                LogHelper.Info(ex.Message);
                return false;
            }
        }


        /// <summary>
        /// 下载指定ID的升级ZIP文件并解压缩
        /// </summary>
        /// <param name="_sID"></param>
        /// <returns></returns>
        public static bool bDownloadPlugins(int _sID)
        {
            try
            {
                var currModel = HIS.Model.dataHelper.plugDB.plug_插件版本库.Where(c => c.ID == _sID).FirstOrDefault();
                string downLoadPath = "";
                string downLoadFileName = "temp" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".zip";
                downLoadPath = Environment.GetEnvironmentVariable("Temp") + "\\" + "_" + downLoadFileName + "_" + "y" + "_" + "x" + "_" + "m" + "_" + "\\";
                if (System.IO.Directory.Exists(downLoadPath)) System.IO.Directory.Delete(downLoadPath, true);
                System.IO.Directory.CreateDirectory(downLoadPath);

                string fullFileName = downLoadPath + "\\" + downLoadFileName;
                byte[] mybyte = currModel.插件包;
                FileStream myfs = new FileStream(fullFileName, FileMode.CreateNew);
                BinaryWriter writefile = new BinaryWriter(myfs);
                writefile.Write(mybyte, 0, mybyte.Length);
                writefile.Close();
                myfs.Close();

                System.Threading.Thread.Sleep(3000);
                //1.1解压缩&复制文件
                if (System.IO.Path.GetExtension(fullFileName) == ".zip")//从其获取扩展名的路径字符串
                {
                    zipHelper.UnZip(fullFileName, downLoadPath);
                    System.IO.File.Delete(fullFileName);
                }
                //todo:灵活配置下载路径，从数据库中读取
                WEISHENG.COMM.FileDirtoryHelper.CopyFile(downLoadPath, zdInfo.PathMain);

                System.IO.Directory.Delete(downLoadPath, true);
                //todo:从内存卸载，实现不用重启，就可以更新插件

                //更新站点设置中的当前版本信息              
                WEISHENG.COMM.msgHelper.ShowInformation("插件下载成功");
                return true;
            }
            catch (Exception ex)
            {
                msgHelper.ShowInformation(ex.Message);
                LogHelper.Info(ex.Message);
                return false;
            }
        }
    }
}
