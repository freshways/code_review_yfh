﻿//-----------------------------------------------------------------
// All Rights Reserved , Copyright (C) 2013 , Hairihan TECH, Ltd.
//-----------------------------------------------------------------

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using WEISHENG.COMM;

namespace ALLINONE
{
    /// <summary>
    /// CacheManager
    /// Assembly 缓存服务
    ///
    /// 修改纪录
    ///
    ///		2008.06.05 版本：1.0 JiRiGaLa 创建。
    ///
    /// <author>
    ///		<name>JiRiGaLa</name>
    ///		<date>2008.06.05</date>
    /// </author>
    /// </summary>
    public class CacheManager
    {
        private CacheManager()
        {
        }

        private Hashtable ObjectCacheStore = new Hashtable();

        private static CacheManager instance = null;
        private static object locker = new Object();

        public static CacheManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (locker)
                    {
                        if (instance == null)
                        {
                            instance = new CacheManager();
                        }
                    }
                }
                return instance;
            }
        }

        /// <summary>
        /// 获得一个窗体
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="formName">窗体名</param>
        /// <returns>窗体</returns>
        public Form GetForm(string assemblyName, string formName)
        {
            Type type = this.GetType(assemblyName, formName);
            return (Form)Activator.CreateInstance(type);
        }

        public Type GetType(string assemblyName, string name)
        {
            Assembly assembly = this.Load(assemblyName, enLoadType.Load);
            Type type = assembly.GetType(assemblyName + "." + name, true, false);
            return type;
        }

        public string getCacheDllsInfo()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in CacheManager.Instance.ObjectCacheStore.Keys)
            {
                string _assemblyname = item.ToString();
                Assembly _assembly = (Assembly)ObjectCacheStore[_assemblyname];
                AssemblyName _assemblyInfo = _assembly.GetName();
                sb.Append("已缓存：" + _assemblyInfo.Name + " " + _assemblyInfo.Version + "\r\n");
            }
            return sb.ToString();
        }

        public enum enLoadType
        {
            LoadFile,
            Load,
            LoadFrom
        }

        /// <summary>
        /// 加载 Assembly
        /// </summary>
        /// <param name="assemblyName">命名空间</param>
        /// <returns>Assembly</returns>
        public Assembly Load(string assemblyName, enLoadType _enLoadType)
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\plugins\" + assemblyName + @"\";

            Assembly assembly = null;
            if (this.ObjectCacheStore.ContainsKey(assemblyName))
            {
                assembly = (Assembly)this.ObjectCacheStore[assemblyName];
            }
            else
            {
                switch (_enLoadType)
                {
                    case enLoadType.LoadFile:
                    case enLoadType.Load:
                        string _assemblyFileName = "";
                        if (assemblyName.Contains(".dll") == false)
                        {
                            _assemblyFileName = path + assemblyName + ".dll";
                        }
                        else
                        {
                            _assemblyFileName = assemblyName;
                        }
                        //条件编译
                        //#if DEBUG
                        //    MessageBox.Show("LoadFile");
                        try
                        {
                            if (!File.Exists(_assemblyFileName))
                            {
                                HIS.COMM.msgBalloonHelper.BalloonShow($"插件【{_assemblyFileName}】文件缺失，请联系信息管理员。");
                                return null;
                            }
                            assembly = Assembly.LoadFile(_assemblyFileName);//从文件读取，可以调试
                        }
                        catch (Exception ex)
                        {
                          HIS.COMM.msgBalloonHelper.BalloonShow($"{_assemblyFileName}:{ ex.Message}");
                        }

                        //#else
                        //MessageBox.Show("LoadAssemblyFromBytes");
                        //assembly = LoadAssemblyFromBytes(_assemblyFileName);//从内存中读取，支持卸载，无法调试
                        //#endif

                        break;

                    case enLoadType.LoadFrom:
                        assembly = Assembly.LoadFrom(assemblyName);
                        break;
                }

                this.ObjectCacheStore.Add(assemblyName, assembly);
            }
            return assembly;
        }

        /// <summary>
        /// 使用 Assembly.LoadFile 加载程序集后 ,被加载的文件就会被锁定，之后就不能对其执行转移、删除等操作
        //为了解决次问题，我们可以先读取成字节流，然后转换成Assembly
        /// </summary>
        /// <param name="fileFullName"></param>
        /// <returns></returns>
        public static Assembly LoadAssemblyFromBytes(string fileFullName)

        {
            byte[] b = File.ReadAllBytes(fileFullName);
            Assembly asm = Assembly.Load(b);
            return asm;
        }

        /// <summary>
        /// 从内存中卸载Dll文件
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="_enLoadType"></param>
        /// <returns></returns>
        public bool UnLoad(string assemblyName)
        {
            try
            {
                this.ObjectCacheStore.Remove(assemblyName);
                return true;
            }
            catch (Exception ex)
            {
                msgHelper.ShowInformation(ex.Message);
                return false;
            }
        }

        public void Add(string key, object storeObject)
        {
            if (!this.ObjectCacheStore.ContainsKey(key))
            {
                this.ObjectCacheStore.Add(key, storeObject);
            }
        }

        public object Retrieve(string key)
        {
            return this.ObjectCacheStore[key];
        }

        public object CreateInstance(string assemblyName, string className, object[] arg)
        {
            // 这里用了缓存技术，若已经被创建过就不反复创建了
            string _Assembly = assemblyName;
            Assembly assembly = CacheManager.Instance.Load(_Assembly, enLoadType.LoadFile);
            Type type = assembly.GetType(assemblyName + "." + className, true, false);
            if (arg[0].ToString() == "")
            {
                return Activator.CreateInstance(type);
            }
            else
            {
                return Activator.CreateInstance(type, arg);//直接传递参数给构造函数，创建实体类
            }
        }
    }
}