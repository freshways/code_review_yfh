﻿using DevExpress.XtraEditors;
using System;
using System.Collections;
using System.Configuration;
using System.Windows.Forms;
using System.Xml;
using WEISHENG.COMM;

namespace ALLINONE
{
    public class configHelper
    {
        public static bool MergeConfigFiles()
        {
            try
            {
                scanXML("allinone.cfg", "allinone.exe.config");
                //scanXML("upListDo.config", "allinone.exe.config");
                return true;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }


        private static bool scanXML(string _sFromFileName, string _sToFileName)
        {
            try
            {
                if (!System.IO.File.Exists(_sFromFileName))
                {
                    return true;//没有这个from文件,视为已经整合
                }
                ExeConfigurationFileMap map = new ExeConfigurationFileMap();
                map.ExeConfigFilename = _sToFileName;
                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                try
                {
                    //如果已经包含连接信息，不再循环写入
                    string name = config.AppSettings.Settings["connHisPub"].Value;
                    return false;                    
                }
                catch (Exception ex)
                {
                    msgHelper.ShowInformation(ex.Message);
                }
                XmlDocument m_oDom = new XmlDocument();
                m_oDom.Load(_sFromFileName);
                ArrayList headersubList = new ArrayList();
                //获取当前XML文档的根 一级
                XmlNode oNode = m_oDom.DocumentElement;
                //获取根节点的所有子节点列表
                XmlNodeList oList = oNode.ChildNodes;
                //标记当前节点
                XmlNode oCurrentNode;
                //遍历所有二级节点
                for (int i = 0; i < oList.Count; i++)
                {
                    //二级
                    oCurrentNode = oList[i];
                    //检测当前节点的名称，节点的值是否与已知匹配
                    if (oCurrentNode.Name.ToLower().Equals("add"))
                    {
                        string _sKeyName = oCurrentNode.Attributes["key"].Value;
                        string _sValue = oCurrentNode.Attributes["value"].Value;
                        ExeConfigurationFileMap map2 = new ExeConfigurationFileMap();
                        map2.ExeConfigFilename = _sToFileName;
                        Configuration config2 = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                        try
                        {
                            string name = config2.AppSettings.Settings[_sKeyName].Value;
                        }
                        catch (Exception ex)
                        {
                            config2.AppSettings.Settings.Add(oCurrentNode.Attributes["key"].Value, oCurrentNode.Attributes["value"].Value);
                            config2.Save(ConfigurationSaveMode.Modified);
                            msgHelper.ShowInformation(ex.Message);
                        }
                        //如果包含连接字符串，定义数据库连接方式
                        if (_sKeyName == "connHisDb")
                        {
                            WEISHENG.COMM.configHelper.setKeyValue("allinone.exe.config", "数据库连接获取方式", WEISHENG.COMM.ClassEnum.en数据库连接获取方式.本地配置文件.ToString());
                        }
                    }
                }
                //todo:在将来的某个版本删除无用的allinone.cfg版本
                //WEISHENG.COMM.Utilities.FileUtil.DeleteFile(_sFromFileName);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void AccessAppSettings()
        {
            //获取Configuration对象
            Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //根据Key读取<add>元素的Value
            string name = config.AppSettings.Settings["name"].Value;
            //写入<add>元素的Value
            config.AppSettings.Settings["name"].Value = "xiao";
            //增加<add>元素
            config.AppSettings.Settings.Add("url", "http://www.baidu.com");
            //删除<add>元素
            config.AppSettings.Settings.Remove("name");
            //一定要记得保存，写不带参数的config.Save()也可以
            config.Save(ConfigurationSaveMode.Modified);
            //刷新，否则程序读取的还是之前的值（可能已装入内存）
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }


    }
}
