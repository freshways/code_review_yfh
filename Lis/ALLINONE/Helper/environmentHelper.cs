﻿using System.Collections;
using System.IO;
using System.Reflection;
using System.Text;
using WEISHENG.COMM;

namespace ALLINONE
{
    public class environmentHelper
    {
        /// <summary>
        /// 初始化软件运行环境相关
        /// 1、合并配置文件
        /// 2、创建快捷方式
        /// 3、运行目录及插件、基础文件监测
        /// 4、删除制定列表的文件
        /// </summary>
        /// <returns></returns>
        public static bool initEnvironment()
        {
            configHelper.MergeConfigFiles();
            WEISHENG.COMM.link_iconHelper.CreateDesktopLnk("医院信息平台2020版");
            environmentHelper.checkEnvironment();
            ArrayList delList = new ArrayList();
            delList.Add("HIS.MAIN.exe");
            delList.Add("ICSharpCode.SharpZipLib.dll");
            delList.Add("ICSharpCode.SharpZipLib.xml");
            FileDirtoryHelper.delUselessFiles(delList);
            return true;
        }


        public static string checkEnvironment()
        {
            string _sResult = string.Empty;
            //1、检查plugins目录
            FileDirtoryHelper.folderCheckAndCreate(WEISHENG.COMM.zdInfo.PathPlugins);
            _sResult += WEISHENG.COMM.stringHelper.MakePrettyLine("本地安装插件");
            //2、医保接口
            string _sRe = string.Empty;
            if (!isFilesOk(getYbjkFiles(), enCheckType.医保接口基础文件, out _sRe))
            {

            }
            _sResult += WEISHENG.COMM.stringHelper.MakePrettyLine(enCheckType.医保接口基础文件.ToString()) + _sRe;
            //3、身份证读卡
            if (!isFilesOk(getSfzReadCardFiles(), enCheckType.身份证读卡基础文件, out _sRe))
            {

            }
            //4、遍历插件
            //_sResult += WEISHENG.COMM.StringHelper.MakePrettyLine(enCheckType.身份证读卡基础文件.ToString()) + _sRe;
            //ArrayList dlls = new ArrayList(Directory.GetFiles(WEISHENG.COMM.zdInfo.PathPlugins, "*.dll", SearchOption.AllDirectories));
            //if (dlls.Count == 0)
            //{
            //    _sResult = "插件目录为空，请下载功能插件！";
            //}
            //else
            //{               
            //    foreach (var item in dlls)
            //    {
            //        _sResult += item.ToString() + "\r\n";
            //    }
            //}
            return _sResult;
        }

        public enum enCheckType
        {
            医保接口基础文件,
            身份证读卡基础文件
        }

        public static void v文件列表写到文件()
        {
            WEISHENG.COMM.iniHelper iniControl = new WEISHENG.COMM.iniHelper();
            string pathPlugins = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "plugins");
            iniControl.StrFilePath = pathPlugins + "\\license.ini";
            ArrayList dlls = new ArrayList(Directory.GetFiles(pathPlugins, "*.*", SearchOption.AllDirectories));
            //string M_str_sqlcon = "server=47.104.70.225;port=3310;user id=query1;password=query1;database=temp";
            foreach (var item in dlls)
            {
                //MySqlHelper.ExecuteDataset(M_str_sqlcon, "insert into fileName (fileName) VALUES('"+ item.ToString().Replace(@"\",@"\\") + "')");
                //iniControl.WriteValue("LICENSE", item.ToString(), item.ToString());
            }
        }
        //if (System.IO.File.Exists(zdInfo.sConfigFileName) == true)
        //{
        //    XML.XMLControl.SetConfigValue(zdInfo.sConfigFileName, "xlType", comboBoxEdit1.Text);
        //}

        //his.comm不在plugins目录下面，单独设置
        //string hiscomm = path.Replace(@"\plugins", "");
        //private bool del

        public static ArrayList getSfzReadCardFiles()
        {
            ArrayList needExistFiles = new ArrayList();
            needExistFiles.Add("sdtapi.dll");
            needExistFiles.Add("termb.dll");
            needExistFiles.Add("UnPack.dll");
            return needExistFiles;
        }

        public static ArrayList getYbjkFiles()
        {
            ArrayList needExistFiles = new ArrayList();
            needExistFiles.Add("VerifyLicAuthLayer.dll");
            needExistFiles.Add("Card.dll");
            needExistFiles.Add("Card.ini");
            needExistFiles.Add("Eapagent.dll");
            needExistFiles.Add("eapagent.ini");
            needExistFiles.Add("enhisif.dll");
            needExistFiles.Add("FT_ET99_API.dll");
            needExistFiles.Add("License.ini");
            needExistFiles.Add("LySb.dll");
            needExistFiles.Add("PrintFile.dll");
            //needExistFiles.Add("Reserve.log");
            needExistFiles.Add("SiInterface.ini");
            //needExistFiles.Add("SiInterface_old.dll");
            needExistFiles.Add("sldll.dll");
            needExistFiles.Add("SQLITE.dll");
            needExistFiles.Add("SS728M01.dll");
            needExistFiles.Add("test.exe");
            needExistFiles.Add("VerifyLicAuth.dll");
            needExistFiles.Add("VerifyLicAuthLayer.dll");
            return needExistFiles;
        }
        public static bool isFilesOk(ArrayList needExistFiles, enCheckType _checkType, out string _sResult)
        {
            StringBuilder _sb = new StringBuilder();
            bool re = true;

            foreach (var item in needExistFiles)
            {
                if (!System.IO.File.Exists(item.ToString()))
                {
                    _sb.Append(string.Format("{0}{1}缺失", _checkType, item.ToString()) + "\r\n");
                    re = false;
                }
            }
            _sResult = _sb.Length == 0 ? string.Format("{0}完整", _checkType) : _sb.ToString();
            return re;

        }

    }
}
