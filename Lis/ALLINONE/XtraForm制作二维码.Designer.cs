﻿namespace ALLINONE
{
    partial class XtraForm制作二维码
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm制作二维码));
            this.picture1 = new DevExpress.XtraEditors.PictureEdit();
            this.simpleButton微信扫码 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label提示 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton保存 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.picture1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // picture1
            // 
            this.picture1.Location = new System.Drawing.Point(78, 69);
            this.picture1.Name = "picture1";
            this.picture1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.picture1.Properties.ZoomPercent = 50;
            this.picture1.Size = new System.Drawing.Size(421, 329);
            this.picture1.TabIndex = 11;
            // 
            // simpleButton微信扫码
            // 
            this.simpleButton微信扫码.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton微信扫码.Image")));
            this.simpleButton微信扫码.Location = new System.Drawing.Point(382, 31);
            this.simpleButton微信扫码.Name = "simpleButton微信扫码";
            this.simpleButton微信扫码.Size = new System.Drawing.Size(52, 22);
            this.simpleButton微信扫码.TabIndex = 12;
            this.simpleButton微信扫码.Text = "制作";
            this.simpleButton微信扫码.Click += new System.EventHandler(this.simpleButton微信扫码_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(132, 33);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(244, 20);
            this.textEdit1.TabIndex = 13;
            // 
            // label提示
            // 
            this.label提示.Location = new System.Drawing.Point(78, 36);
            this.label提示.Name = "label提示";
            this.label提示.Size = new System.Drawing.Size(48, 14);
            this.label提示.TabIndex = 14;
            this.label提示.Text = "身份证号";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(486, 31);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(52, 22);
            this.simpleButton1.TabIndex = 15;
            this.simpleButton1.Text = "清空";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton保存
            // 
            this.simpleButton保存.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton保存.Image")));
            this.simpleButton保存.Location = new System.Drawing.Point(434, 31);
            this.simpleButton保存.Name = "simpleButton保存";
            this.simpleButton保存.Size = new System.Drawing.Size(52, 22);
            this.simpleButton保存.TabIndex = 16;
            this.simpleButton保存.Text = "保存";
            this.simpleButton保存.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // XtraForm制作二维码
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 443);
            this.Controls.Add(this.simpleButton保存);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.label提示);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.simpleButton微信扫码);
            this.Controls.Add(this.picture1);
            this.Name = "XtraForm制作二维码";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "制作预约二维码";
            ((System.ComponentModel.ISupportInitialize)(this.picture1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit picture1;
        private DevExpress.XtraEditors.SimpleButton simpleButton微信扫码;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl label提示;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton保存;
    }
}