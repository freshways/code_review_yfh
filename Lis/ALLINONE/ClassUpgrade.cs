﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace HIS.MAIN.business
{
    class ClassUpgrade
    {
        public static bool b医嘱临床路径改造()
        {
            try
            {
                string SQL =
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱') and name = '医嘱套餐编码')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱 add 医嘱套餐编码 varchar(50) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱历史') and name = '医嘱套餐编码')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱历史 add 医嘱套餐编码 varchar(50) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱') and name = '用药状态')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱 add 用药状态 varchar(5) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱历史') and name = '用药状态')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱历史 add 用药状态 varchar(5) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱') and name = '医嘱验证标记')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱 add 医嘱验证标记 varchar(5) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱历史') and name = '医嘱验证标记')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱历史 add 医嘱验证标记 varchar(5) NULL;" + "\r\n" +
            "  end" +
                "" + "\r\n" +
           "if not exists" + "\r\n" +
           "     (select 1" + "\r\n" +
           "      from   syscolumns" + "\r\n" +
           "      where  id = object_id('YS住院医嘱') and name = '医嘱验证标记')" + "\r\n" +
           "  begin" + "\r\n" +
           "alter table YS住院医嘱 add 医嘱验证标记 varchar(5) NULL;" + "\r\n" +
           "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static bool b升级通知公告增加附件内容()
        {
            try
            {
                string SQL = "alter table dbo.pubnoteic add 附件内容 image,附件文件名 varchar(30)";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



        public static bool b报表中心统计报表()
        {
            try
            {
                string SQL =
                    "ALTER VIEW [dbo].[ZY出院费用View]" + "\r\n" +
                    "AS" + "\r\n" +
                    "  SELECT cc.科室, aa.id, aa.ZYID, 项目编码, 费用编码, bb.财务分类 费用名称, 金额, 数量, 单价," + "\r\n" +
                    "         进价, 已发药标记, 0 YPID, 药房编码, 医生编码, 记费人编码, 发药人编码, 记费时间, 发药时间," + "\r\n" +
                    "         aa.汇总时间, aa.月结时间, aa.单位编码, aa.分院编码, aa.CreateTime, 医嘱ID" + "\r\n" +
                    "  FROM   ZY出院费用 aa" + "\r\n" +
                    "         LEFT OUTER JOIN YK药品信息 bb ON aa.费用编码 = bb.药品序号" + "\r\n" +
                    "         LEFT OUTER JOIN ZY病人信息 cc ON aa.ZYID = cc.ZYID" + "\r\n" +
                    "  WHERE  ypid <> 0" + "\r\n" +
                    "  UNION ALL" + "\r\n" +
                    "  SELECT cc.科室, aa.id, aa.ZYID, 项目编码, 费用编码, 费用名称, 金额, 数量, 单价," + "\r\n" +
                    "         进价, 已发药标记, YPID, 药房编码, 医生编码, 记费人编码, 发药人编码, 记费时间, 发药时间," + "\r\n" +
                    "         aa.汇总时间, aa.月结时间, aa.单位编码, aa.分院编码, aa.CreateTime, 医嘱ID" + "\r\n" +
                    "  FROM   ZY出院费用 aa LEFT OUTER JOIN ZY病人信息 cc ON aa.ZYID = cc.ZYID" + "\r\n" +
                    "  WHERE  ypid = 0";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                string SQL1 =
                    "ALTER VIEW [dbo].[ZY在院费用View]" + "\r\n" +
                    "AS" + "\r\n" +
                    "  select cc.科室, aa.id, aa.ZYID, 项目编码, 费用编码, bb.财务分类 费用名称, 金额, 数量, 单价," + "\r\n" +
                    "         进价, 已发药标记, 0 YPID, 药房编码, 医生编码, 记费人编码, 发药人编码, 记费时间, 发药时间," + "\r\n" +
                    "         aa.汇总时间, aa.月结时间, aa.单位编码, aa.分院编码, aa.CreateTime, 医嘱ID" + "\r\n" +
                    "  from   ZY在院费用 aa" + "\r\n" +
                    "         left outer join YK药品信息 bb on aa.费用编码 = bb.药品序号" + "\r\n" +
                    "         left outer join ZY病人信息 cc on aa.zyid = cc.zyid" + "\r\n" +
                    "  where  ypid <> 0" + "\r\n" +
                    "  union all" + "\r\n" +
                    "  select cc.科室, aa.id, aa.ZYID, 项目编码, 费用编码, 费用名称, 金额, 数量, 单价," + "\r\n" +
                    "         进价, 已发药标记, YPID, 药房编码, 医生编码, 记费人编码, 发药人编码, 记费时间, 发药时间," + "\r\n" +
                    "         aa.汇总时间, aa.月结时间, aa.单位编码, aa.分院编码, aa.CreateTime, 医嘱ID" + "\r\n" +
                    "  from   ZY在院费用 aa left outer join ZY病人信息 cc on aa.zyid = cc.zyid" + "\r\n" +
                    "  where  ypid = 0";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL1);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b药典是否禁用问题()
        {
            try
            {
                string SQL = "update YK药品信息 set 是否禁用=0 where 是否禁用 is null";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool bHL自动记费设置()
        {
            try
            {
                string SQL =
                    "CREATE TABLE [dbo].[HL自动记费设置](" + "\r\n" +
                    "	[组别] [varchar](50) NULL," + "\r\n" +
                    "	[费用编码] [varchar](50) NULL," + "\r\n" +
                    "	[费用名称] [varchar](50) NULL," + "\r\n" +
                    "	[数量] [numeric](18, 2) NULL," + "\r\n" +
                    "	[药品序号] [int] NULL," + "\r\n" +
                    "	[病区编码] [varchar](50) NULL," + "\r\n" +
                    "	[ID] [int] IDENTITY(1,1) NOT NULL," + "\r\n" +
                    " CONSTRAINT [PK_HL自动记费设置] PRIMARY KEY CLUSTERED " + "\r\n" +
                    "(" + "\r\n" +
                    "	[ID] ASC" + "\r\n" +
                    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
                    ") ON [PRIMARY]";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b_vw全部病人修改()
        {
            try
            {
                string SQL =
                 "ALTER VIEW [dbo].[vw全部病人]" + "\r\n" +
                 "AS" + "\r\n" +
                 "  SELECT zyid 子zyid, zyid 父zyid, 病区, 0 子病人, 住院号码, 病人姓名, 已出院标记, 在院状态, 病床" + "\r\n" +
                 "  FROM   zy病人信息" + "\r\n" +
                 "  UNION ALL" + "\r\n" +
                 "  SELECT 子zyid, 父zyid, bb.病区, 1 子病人, aa.住院号码, aa.病人姓名, bb.已出院标记, bb.在院状态, bb.病床" + "\r\n" +
                 "  FROM   zy子病人信息 aa LEFT OUTER JOIN [ZY病人信息] bb ON aa.[父ZYID] = bb.ZYID";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b_vw全部病人()
        {
            try
            {
                string SQL =
                 "CREATE VIEW [dbo].[vw全部病人]" + "\r\n" +
                 "AS" + "\r\n" +
                 "  SELECT zyid 子zyid, zyid 父zyid, 病区, 0 子病人, 住院号码, 病人姓名 FROM zy病人信息" + "\r\n" +
                 "  UNION ALL" + "\r\n" +
                 "  SELECT 子zyid, 父zyid, bb.病区, 1 子病人, aa.住院号码, aa.病人姓名" + "\r\n" +
                 "  FROM   zy子病人信息 aa LEFT OUTER JOIN [ZY病人信息] bb ON aa.[父ZYID] = bb.ZYID";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool bSQL子病人信息()
        {
            try
            {
                string SQL =
                   "CREATE TABLE [dbo].[ZY子病人信息](" + "\r\n" +
                   "  [ID]           [numeric](18, 0)" + "\r\n" +
                   "      IDENTITY (" + "\r\n" +
                   "                1," + "\r\n" +
                   "                1)" + "\r\n" +
                   "      NOT NULL," + "\r\n" +
                   "  [子ZYID]      [numeric](18, 0) NOT NULL," + "\r\n" +
                   "  [住院号码] [nvarchar](20) NOT NULL," + "\r\n" +
                   "  [医疗证号] [nvarchar](50) NULL," + "\r\n" +
                   "  [病人姓名] [nvarchar](10) NOT NULL," + "\r\n" +
                   "  [性别]       [nvarchar](50) NULL," + "\r\n" +
                   "  [出生日期] [datetime] NULL," + "\r\n" +
                   "  [身份证号] [nvarchar](20) NULL," + "\r\n" +
                   "  [父ZYID]      [numeric](18, 0) NOT NULL," + "\r\n" +
                   "  [createTime]   [datetime] NULL," + "\r\n" +
                   "  CONSTRAINT [PK_ZY子病人信息] PRIMARY KEY" + "\r\n" +
                   "    CLUSTERED" + "\r\n" +
                   "    ([ID] ASC)" + "\r\n" +
                   "    WITH (PAD_INDEX = OFF," + "\r\n" +
                   "          STATISTICS_NORECOMPUTE = OFF," + "\r\n" +
                   "          IGNORE_DUP_KEY = OFF," + "\r\n" +
                   "          ALLOW_ROW_LOCKS = ON," + "\r\n" +
                   "          ALLOW_PAGE_LOCKS = ON)" + "\r\n" +
                   "    ON [PRIMARY])" + "\r\n" +
                   "ON [PRIMARY]" + "\r\n" +
                   "" + "\r\n" +
                   "ALTER TABLE [dbo].[ZY子病人信息]  WITH CHECK ADD  CONSTRAINT [FK_ZY子病人信息_ZY病人信息] FOREIGN KEY([父ZYID])" + "\r\n" +
                   "REFERENCES [dbo].[ZY病人信息] ([ZYID])" + "\r\n" +
                   "" + "\r\n" +
                   "ALTER TABLE [dbo].[ZY子病人信息] CHECK CONSTRAINT [FK_ZY子病人信息_ZY病人信息]" + "\r\n" +
                   "" + "\r\n" +
                   "ALTER TABLE [dbo].[ZY子病人信息] ADD  CONSTRAINT [DF_ZY子病人信息_createTime]  DEFAULT (getdate()) FOR [createTime]";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region 权限更新
        public static bool bPACS条码188()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = 'PACS条码'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 188" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 188" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b护理体温单187()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '自动记费设置'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 187" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 187" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b护理自动记费186()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '自动记费设置'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 186" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 186" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b删除权限117三卡合一()
        {
            try
            {
                string SQL_权限 =
                    "delete pubQxNew where 键id=117 delete pubJsQxNew where 键id=117";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b诊治记录查询185()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '诊治记录查询'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 95 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 185" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 185" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b接诊记录接口184()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '接诊记录接口'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 95 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 184" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 184" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b护理输液记录183()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '输液记录'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 183" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 183" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b护理打印医嘱182()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '医嘱打印'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 182" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 182" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool b药房调拨确认权限181()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '调拨确认'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 147 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 181" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 181" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b药房调拨申请权限180()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '调拨申请'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 147 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 180" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 180" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b管理员维护_协定处方权限()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '协定处方'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 39 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "--select @NewId= max(键ID)+1 from pubqxNew" + "\r\n" +
                    "set @NewId  = 178" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 178" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b历史库存功能权限()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '历史库存'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 149 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "--select @NewId= max(键ID)+1 from pubqxNew" + "\r\n" +
                    "set @NewId  = 177" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 177" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        #endregion
        //alter table YS住院医嘱历史 alter column 医嘱内涵 varchar(200)

        public static bool b长期医嘱只执行一次()
        {
            try
            {
                string SQL_权限 = "update [YS医嘱用法] set 长期医嘱只执行一次=0 where [长期医嘱只执行一次] is null";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b输液记录打印标志()
        {
            try
            {
                string SQL_权限 = "alter table ys医嘱频次 add 输液记录打印标志 int";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b医嘱续打时间点()
        {
            try
            {
                string SQL =
                 "alter table zy病人信息 add 长期医嘱续打时间点 datetime,临时医嘱续打时间点 datetime";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool b药房调拨()
        {
            try
            {
                string SQL = "alter table YF入库摘要 add 调拨药房编码 int,确认时间 datetime";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool b升级出院结算和取消结算()
        {
            try
            {
                string SQL =
            "CREATE TABLE [dbo].[YS住院医嘱历史](" + "\r\n" +
            "	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL," + "\r\n" +
            "	[ZYID] [numeric](18, 0) NULL," + "\r\n" +
            "	[医嘱类型] [nvarchar](10) NULL," + "\r\n" +
            "	[组别] [int] NULL," + "\r\n" +
            "	[项目类型] [nvarchar](10) NULL," + "\r\n" +
            "	[项目编码] [int] NULL," + "\r\n" +
            "	[项目名称] [nvarchar](50) NULL," + "\r\n" +
            "	[剂量数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[剂量单位] [nvarchar](10) NULL," + "\r\n" +
            "	[医嘱数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次名称] [nvarchar](50) NULL," + "\r\n" +
            "	[用法名称] [nvarchar](50) NULL," + "\r\n" +
            "	[开嘱医生编码] [int] NULL," + "\r\n" +
            "	[开嘱时间] [datetime] NULL," + "\r\n" +
            "	[开嘱执行者编码] [int] NULL," + "\r\n" +
            "	[开嘱核对者编码] [int] NULL," + "\r\n" +
            "	[执行时间] [datetime] NULL," + "\r\n" +
            "	[停嘱医生编码] [int] NULL," + "\r\n" +
            "	[停嘱执行者编码] [int] NULL," + "\r\n" +
            "	[停嘱核对者编码] [int] NULL," + "\r\n" +
            "	[停嘱时间] [datetime] NULL," + "\r\n" +
            "	[单日总量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[createTime] [datetime] NULL," + "\r\n" +
            "	[临床路径ID] [int] NULL," + "\r\n" +
            "	[组别符号] [nvarchar](6) NULL," + "\r\n" +
            "	[库存数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[药房规格] [varchar](50) NULL," + "\r\n" +
            "	[药房单位] [varchar](10) NULL," + "\r\n" +
            "	[药房编码] [int] NULL," + "\r\n" +
            "	[更新标志] [int] NULL," + "\r\n" +
            "	[单次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[财务分类] [varchar](20) NULL," + "\r\n" +
            "	[页号] [int] NULL," + "\r\n" +
            "	[行号] [int] NULL," + "\r\n" +
            "	[已打印标志] [int] NULL," + "\r\n" +
            "	[子嘱ID] [int] NULL," + "\r\n" +
            "	[医嘱内涵] [varchar](200) NULL," + "\r\n" +
            "	[医嘱备注] [varchar](50) NULL," + "\r\n" +
            "	[停嘱原因] [varchar](50) NULL," + "\r\n" +
            "	[作废原因] [varchar](50) NULL," + "\r\n" +
            "	在院医嘱ID numeric(18,0)" + "\r\n" +
            " CONSTRAINT [PK_YS住院医嘱历史] PRIMARY KEY CLUSTERED " + "\r\n" +
            "(" + "\r\n" +
            "	[ID] ASC" + "\r\n" +
            ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
            ") ON [PRIMARY]" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "CREATE TABLE [dbo].[YS住院子医嘱历史](" + "\r\n" +
            "	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL," + "\r\n" +
            "	[子嘱ID] [int] NULL," + "\r\n" +
            "	[ZYID] [numeric](18, 0) NULL," + "\r\n" +
            "	[医嘱类型] [nvarchar](10) NULL," + "\r\n" +
            "	[组别] [int] NULL," + "\r\n" +
            "	[项目类型] [nvarchar](10) NULL," + "\r\n" +
            "	[项目编码] [int] NULL," + "\r\n" +
            "	[项目名称] [nvarchar](50) NULL," + "\r\n" +
            "	[剂量数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[剂量单位] [nvarchar](10) NULL," + "\r\n" +
            "	[医嘱数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次名称] [nvarchar](50) NULL," + "\r\n" +
            "	[用法名称] [nvarchar](50) NULL," + "\r\n" +
            "	[开嘱医生编码] [int] NULL," + "\r\n" +
            "	[开嘱时间] [datetime] NULL," + "\r\n" +
            "	[开嘱执行者编码] [int] NULL," + "\r\n" +
            "	[开嘱核对者编码] [int] NULL," + "\r\n" +
            "	[执行时间] [datetime] NULL," + "\r\n" +
            "	[停嘱医生编码] [int] NULL," + "\r\n" +
            "	[停嘱执行者编码] [int] NULL," + "\r\n" +
            "	[停嘱核对者编码] [int] NULL," + "\r\n" +
            "	[停嘱时间] [datetime] NULL," + "\r\n" +
            "	[单日总量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[createTime] [datetime] NULL," + "\r\n" +
            "	[临床路径ID] [int] NULL," + "\r\n" +
            "	[组别符号] [nvarchar](6) NULL," + "\r\n" +
            "	[库存数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[药房规格] [varchar](50) NULL," + "\r\n" +
            "	[药房单位] [varchar](10) NULL," + "\r\n" +
            "	[药房编码] [int] NULL," + "\r\n" +
            "	[更新标志] [int] NULL," + "\r\n" +
            "	[单次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[财务分类] [varchar](20) NULL," + "\r\n" +
            "	[页号] [int] NULL," + "\r\n" +
            "	[行号] [int] NULL," + "\r\n" +
            "	[已打印标志] [int] NULL," + "\r\n" +
            "	[医嘱ID] [numeric](18, 0) NOT NULL," + "\r\n" +
            "	在院子医嘱ID numeric(18,0)" + "\r\n" +
            " CONSTRAINT [PK_YS住院子医嘱历史] PRIMARY KEY CLUSTERED " + "\r\n" +
            "(" + "\r\n" +
            "	[ID] ASC" + "\r\n" +
            ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
            ") ON [PRIMARY]" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "CREATE TABLE [dbo].[HL执行医嘱历史](" + "\r\n" +
            "	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL," + "\r\n" +
            "	[医嘱ID] [numeric](18, 0) NULL," + "\r\n" +
            "	[ZYID] [numeric](18, 0) NULL," + "\r\n" +
            "	[项目类型] [nvarchar](10) NULL," + "\r\n" +
            "	[项目编码] [int] NULL," + "\r\n" +
            "	[项目名称] [nvarchar](50) NULL," + "\r\n" +
            "	[医嘱数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次名称] [nvarchar](50) NULL," + "\r\n" +
            "	[用法名称] [nvarchar](50) NULL," + "\r\n" +
            "	[单日总量] [numeric](10, 2) NULL," + "\r\n" +
            "	[执行时间] [datetime] NULL," + "\r\n" +
            "	[执行数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[YPID] [numeric](18, 4) NULL," + "\r\n" +
            "	[执行者编码] [int] NULL," + "\r\n" +
            "	[createTime] [datetime] NULL," + "\r\n" +
            "	[组别] [int] NULL," + "\r\n" +
            "	[医嘱类型] [nvarchar](10) NULL," + "\r\n" +
            "	[库存数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[药房规格] [varchar](50) NULL," + "\r\n" +
            "	[药房单位] [varchar](10) NULL," + "\r\n" +
            "	[药房编码] [int] NULL," + "\r\n" +
            "	[剂量数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[剂量单位] [varchar](10) NULL," + "\r\n" +
            "	[单次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[财务分类] [varchar](20) NULL," + "\r\n" +
            "	在院ID numeric(18,0)" + "\r\n" +
            " CONSTRAINT [PK_HL医嘱执行历史] PRIMARY KEY CLUSTERED " + "\r\n" +
            "(" + "\r\n" +
            "	[ID] ASC" + "\r\n" +
            ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
            ") ON [PRIMARY]" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "alter table zy出院费用 add 在院费用ID numeric(18,0)";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool b协定处方功能升级()
        {
            try
            {
                string SQL =
                  "alter TABLE [GY协定处方] add 处方类型 varchar(20)";
                string SQL2 =
                "update [GY协定处方] set 处方类型='个人' where 处方类型 is null";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL2);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public static bool b更新医嘱内涵()
        {
            try
            {
                string SQL =
                 "  update ys住院医嘱 set 医嘱内涵=项目名称 where 医嘱内涵 is null";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static bool b医嘱处理增加字段()
        {
            try
            {
                string SQL =
                 " alter table [YS住院医嘱] add 医嘱内涵 varchar(50),医嘱备注 varchar(50),停嘱原因 varchar(50),作废原因 varchar(50)";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static bool b病历索引加ZYID()
        {
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text,
                    "alter table dbo.emr病历索引 add ZYID numeric(18,0)");
            }
            catch (Exception ex)
            {
                //throw;
            }
            return true;
        }

        public static bool b子医嘱约束问题()
        {
            string SQL =
    "" + "\r\n" +
    "ALTER TABLE dbo.YS住院子医嘱 ADD CONSTRAINT" + "\r\n" +
    "	PK_YS住院子医嘱 PRIMARY KEY CLUSTERED " + "\r\n" +
    "	(" + "\r\n" +
    "	ID" + "\r\n" +
    "	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]" + "\r\n" +
    "" + "\r\n" +
    "alter table YS住院子医嘱 add  医嘱ID numeric(18,0)" + "\r\n" +
    "" + "\r\n" +
    "ALTER TABLE dbo.YS住院子医嘱 ADD CONSTRAINT" + "\r\n" +
    "	FK_YS住院子医嘱_YS住院医嘱 FOREIGN KEY" + "\r\n" +
    "	(" + "\r\n" +
    "	医嘱ID" + "\r\n" +
    "	) REFERENCES dbo.YS住院医嘱" + "\r\n" +
    "	(" + "\r\n" +
    "	ID" + "\r\n" +
    "	) ON UPDATE  CASCADE " + "\r\n" +
    "	 ON DELETE  CASCADE " + "\r\n" +
    "" + "\r\n" +
    "----删除子医嘱孤立数据" + "\r\n" +
    "delete aa from  [dbo].[YS住院子医嘱] aa left outer join " + "\r\n" +
    "ys住院医嘱 bb on aa.子嘱ID=bb.子嘱ID and aa.医嘱类型=bb.医嘱类型 and aa.ZYID=bb.ZYID" + "\r\n" +
    "where bb.ID is not null and bb.停嘱时间 is not null and aa.停嘱时间 is null" + "\r\n" +
    "" + "\r\n" +
    "" + "\r\n" +
    "----更新停嘱时间" + "\r\n" +
    "update bb set bb.停嘱时间=aa.停嘱时间 from [dbo].[YS住院子医嘱] aa left outer join " + "\r\n" +
    "ys住院医嘱 bb on aa.子嘱ID=bb.子嘱ID and aa.医嘱类型=bb.医嘱类型 and aa.ZYID=bb.ZYID" + "\r\n" +
    "where bb.ID is not null and bb.停嘱时间 is not null and aa.停嘱时间 is null" + "\r\n" +
    "" + "\r\n" +
    "----更新子表医嘱ＩＤ" + "\r\n" +
    "update aa set aa.医嘱ID=bb.id from" + "\r\n" +
    "[dbo].[YS住院子医嘱] aa left outer join ys住院医嘱 bb " + "\r\n" +
    "on aa.子嘱ID=bb.子嘱ID and aa.医嘱类型=bb.医嘱类型 and aa.ZYID=bb.ZYID" + "\r\n" +
    "where bb.ID is not null and aa.医嘱ID is null" + "\r\n" +
    "";


            //    string SQL =
            //"update aa" + "\r\n" +
            //"set    aa.停嘱时间 = bb.停嘱时间" + "\r\n" +
            //"from   YS住院子医嘱 aa left outer join YS住院医嘱 bb on aa.医嘱ID = bb.ID" + "\r\n" +
            //"where  aa.停嘱时间 is null and bb.停嘱时间 is not null";
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
            }
            catch (Exception ex)
            {

                //throw;
            }
            return true;

        }
        public static bool b收款整合居民报销()
        {
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, "alter table [MF门诊摘要] add 医保结算流水号 varchar(50)");
            }
            catch (Exception ex)
            {

                //throw;
            }
            return true;
        }
        public static bool b医嘱剂量打印显示()
        {
            string SQL =
        "CREATE function [dbo].[ClearZero](@inValue varchar(50))" + "\r\n" +
        "  returns varchar(50)" + "\r\n" +
        "as" + "\r\n" +
        "  begin" + "\r\n" +
        "    declare @returnValue varchar(20)" + "\r\n" +
        "" + "\r\n" +
        "    if (@inValue = '')" + "\r\n" +
        "      set @returnValue = '' --空的时候为空" + "\r\n" +
        "    else" + "\r\n" +
        "      if (charindex('.', @inValue) = '0')" + "\r\n" +
        "        set @returnValue = @inValue --针对不含小数点的" + "\r\n" +
        "      else" + "\r\n" +
        "        if (substring(reverse(@inValue), patindex('%[^0]%', reverse(@inValue)), 1) = '.')" + "\r\n" +
        "          set @returnValue = left(@inValue, len(@inValue) - patindex('%[^0]%', reverse(@inValue))) --针对小数点后全是0的" + "\r\n" +
        "        else" + "\r\n" +
        "          set @returnValue = left(@inValue, len(@inValue) - patindex('%[^0]%.%', reverse(@inValue)) + 1) --其他任何情形" + "\r\n" +
        "" + "\r\n" +
        "    return @returnValue" + "\r\n" +
        "  end";

            return true;
        }



        public static bool b修改EMR模板类型列()//解决截断二进制字符串问题
        {
            try
            {
                string SQL =
                   "alter table [emr模板索引] alter column	[模板文件名] [nvarchar](100)" + "\r\n" +
                   "alter table [emr模板索引] alter column	[模板编码] [nvarchar](100) ";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool b增加病历类型术前讨论()
        {
            try
            {
                string SQL =
                   "declare @iCount int" + "\r\n" +
                   "select   @iCount=count(*)" + "\r\n" +
                   "from     [emr模板类型] where [类型编码]='Z1'" + "\r\n" +
                   "if  @iCount=0" + "\r\n" +
                   "begin" + "\r\n" +
                   "insert into [emr模板类型]([类型编码], [类型名称], CATALOG_TYPE, [图标索引], [被选图标索引], ONE_FILE_FLAG," + "\r\n" +
                   "              [用户类型], [模块类型], [排列顺序], [是否禁用])" + "\r\n" +
                   "VALUES      (N'Z1', N'术前讨论', N'2', 4, 5, 0, N'1', N'0', 11, '0')" + "\r\n" +
                   "end";
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool b统计报表改造()
        {
            string SQL工作量统计完善 =
                    "ALTER VIEW [dbo].[MF门诊明细View]" + "\r\n" +
                    "AS" + "\r\n" +
                    "  select   aa.ID, aa.收费编码, dd.财务分类 收费名称, SUM(cc.零售价金额) 金额, null 归并编码," + "\r\n" +
                    "           null 归并名称, aa.MZID, aa.单位编码, aa.CreateTIme" + "\r\n" +
                    "  from     MF门诊明细 aa" + "\r\n" +
                    "           left outer join MF处方摘要 bb on aa.MZID = bb.MZID" + "\r\n" +
                    "           left outer join MF处方明细 cc on bb.CFID = cc.CFID" + "\r\n" +
                    "           left outer join yk药品信息 dd on cc.药品序号 = dd.药品序号" + "\r\n" +
                    "  where    aa.收费编码 in (select 收费编码" + "\r\n" +
                    "                               from   GY收费小项" + "\r\n" +
                    "                               where  收费名称 = '西药费') and dd.财务分类 <> '中药费'" + "\r\n" +
                    "  group by aa.ID, aa.收费编码, dd.财务分类, aa.MZID, aa.单位编码, aa.CreateTIme" + "\r\n" +
                    "  union all" + "\r\n" +
                    "  SELECT ID, 收费编码, 收费名称, 金额, 归并编码, 归并名称, MZID, 单位编码, CreateTIme" + "\r\n" +
                    "  FROM   MF门诊明细" + "\r\n" +
                    "  where  收费编码 not in (select 收费编码" + "\r\n" +
                    "                              from   GY收费小项" + "\r\n" +
                    "                              where  收费名称 = '西药费')";
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL工作量统计完善);
            }
            catch (Exception ex)
            {
                return false;
            }

            string SQL_报表中心门诊发药权限 =
                "declare @Name varchar(20)" + "\r\n" +
                "set @Name   = '门诊发药统计'" + "\r\n" +
                "" + "\r\n" +
                "declare @父Id int" + "\r\n" +
                "set @父Id  = 49 --从前台查到的Tag" + "\r\n" +
                "" + "\r\n" +
                "declare" + "\r\n" +
                "  @NewId  int," + "\r\n" +
                "  @ishave int" + "\r\n" +
                "--select @NewId= max(键ID)+1 from pubqxNew" + "\r\n" +
                "set @NewId  = 176" + "\r\n" +
                "select @ishave = count(*)" + "\r\n" +
                "from pubqxnew" + "\r\n" +
                "where [键ID] = 176" + "\r\n" +
                "" + "\r\n" +
                "if @ishave = 0" + "\r\n" +
                "  begin" + "\r\n" +
                "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                "  end";
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_报表中心门诊发药权限);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool b曼荼罗改造1()
        {
            string SQL0 =
                 "alter table [ZY病人信息] add 医嘱出院时间 datetime,入区时间 datetime";
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL0);
            }
            catch (Exception)
            {
            }
            string SQL1 =
                  "alter table [ZY病人信息] add 住院次数 int default 1";
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL1);
            }
            catch (Exception)
            {
            }
            string SQL2 =
                "update [ZY病人信息] set 住院次数=1 where 住院次数 is null";
            //"update [ZY病人信息] set 入区时间=入院时间 where 入区时间 is null" + "\r\n" +
            //"update [ZY病人信息] set 医嘱出院时间=出院时间 where 医嘱出院时间 is null";
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL2);
            }
            catch (Exception)
            {
            }
            string SQL3 = "alter table [ZY病人信息] add 联系人亲属关系 varchar(10), 联系人姓名 varchar(20)";
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL3);
            }
            catch (Exception)
            {
            }

            return true;
        }
        /// <summary>
        /// 20140125升级药房库存预警数据库修改
        /// </summary>
        /// <returns></returns>
        public static bool b升级药房库存预警SQL()
        {
            string SQL_YF库存预警 =
              "if object_id(N'YF库存预警', N'U') is null" + "\r\n" +
              "begin" + "\r\n" +
              "  CREATE TABLE [dbo].[YF库存预警] (" + "\r\n" +
              "  [ID] numeric(18, 0) IDENTITY(1, 1) NOT NULL," + "\r\n" +
              "  [药房编码] int NULL," + "\r\n" +
              "  [药品序号] numeric(10, 0) NULL ," + "\r\n" +
              "  [高储] numeric(10, 2) NULL default 0," + "\r\n" +
              "  [低储] numeric(10, 2) NULL default 0," + "\r\n" +
              "  [单位编码] int NULL," + "\r\n" +
              "  [CreateTime] datetime NULL default getdate())" + "\r\n" +
              "  ON [PRIMARY]" + "\r\n" +
              "  WITH (DATA_COMPRESSION = NONE);" + "\r\n" +
              "end";
            string SQL_权限 =
                 "declare @Name varchar(20)" + "\r\n" +
                 "set @Name   = '库存预警'" + "\r\n" +
                 "" + "\r\n" +
                 "declare @父Id int" + "\r\n" +
                 "set @父Id  = 149 --从前台查到的Tag" + "\r\n" +
                 "" + "\r\n" +
                 "declare" + "\r\n" +
                 "  @NewId  int," + "\r\n" +
                 "  @ishave int" + "\r\n" +
                 "--select @NewId= max(键ID)+1 from pubqxNew" + "\r\n" +
                 "set @NewId  = 175" + "\r\n" +
                 "select @ishave = count(*)" + "\r\n" +
                 "from pubqxnew" + "\r\n" +
                 "where [键ID] = 175" + "\r\n" +
                 "" + "\r\n" +
                 "if @ishave = 0" + "\r\n" +
                 "  begin" + "\r\n" +
                 "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                 "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                 "  end";
            try
            {
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_YF库存预警);
                tykClass.Db.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }




    }
}
