﻿using System;
using System.Text;
using System.Windows.Forms;
using WEISHENG.COMM;
using WEISHENG.COMM.Helper;

namespace ALLINONE
{
    public partial class AboutWindow : Form
    {
        public AboutWindow()
        {
            InitializeComponent();
        }

        private void AboutWindow_Load(object sender, EventArgs e)
        {
            label1.Text = "计算机名：" + WEISHENG.COMM.zdInfo.ModelHardInfo.ComputerName +
                "\r\n内存:" + WEISHENG.COMM.zdInfo.ModelHardInfo.TotalPhysicalMemory +
                "\r\n操作系统：" + WEISHENG.COMM.zdInfo.ModelHardInfo.SystemType;
            label2.Text = "nMAC地址:" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress +
                "\r\nIP地址:" + WEISHENG.COMM.zdInfo.ModelHardInfo.IpAddress;

            if (WEISHENG.COMM.zdInfo.ModelHardInfo.PPPoEIP != "")
            {
                label2.Text += "\r\n" + WEISHENG.COMM.zdInfo.ModelHardInfo.PPPoEIP;
            }
            richTextBox1.Text = environmentHelper.checkEnvironment();
            richTextBox1.Text += stringHelper.MakePrettyLine(".NET框架版本") + WEISHENG.COMM.Computer.GetDotVersion();

       
        
            StringBuilder sb = new StringBuilder();
          
            sb.Append("\r\nMAC地址:" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress);
            sb.Append("\r\nIP地址: " + WEISHENG.COMM.zdInfo.ModelHardInfo.IpAddress);
            richTextBox1.Text += WEISHENG.COMM.stringHelper.MakePrettyLine("其他信息") + sb.ToString();
        }
    }
}
