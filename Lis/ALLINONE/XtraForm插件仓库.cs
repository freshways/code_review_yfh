﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WEISHENG.COMM;
using HIS.Model;

namespace ALLINONE
{
    public partial class XtraForm插件仓库 : DevExpress.XtraEditors.XtraForm
    {
        //IList<PUB站点模块列表Model> models = new List<PUB站点模块列表Model>();
        public XtraForm插件仓库()
        {

            InitializeComponent();
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            try
            {
                var models = HIS.Model.dataHelper.plugDB.plug_插件版本库.Select(c => new
                { c.ID, c.插件包类型, c.文件名, c.版本号, c.版本说明, c.插件GUID, c.createTime }
                ).ToList();
                gridControl插件下载.DataSource = models;
                WEISHENG.COMM.frmGridCustomize.RegisterGrid(gridView插件下载);
            }
            catch (Exception ex)
            {
                WEISHENG.COMM.msgHelper.ShowInformation(ex.Message);
            }
        }

        private void XtraForm模块列表_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void simpleButton关闭_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void simpleButton下载_Click(object sender, EventArgs e)
        {
            if (gridView插件下载.FocusedRowHandle < 0)
            {
                return;
            }
            var currModel = Convert.ToInt32(gridView插件下载.GetFocusedRowCellDisplayText("ID"));
            Int32 id = Convert.ToInt32(currModel);

            var download_item = HIS.Model.dataHelper.plugDB.plug_插件版本库.Where(c => c.ID == id).FirstOrDefault();
            try
            {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                dialog.Description = "请选择文件夹";

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if (string.IsNullOrEmpty(dialog.SelectedPath))
                    {
                        MessageBox.Show(this, "文件夹路径不能为空", "提示");
                        return;
                    }
                }


                string downLoadPath = dialog.SelectedPath;
                //string downLoadFileName = "temp" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".zip";
                //downLoadPath = Environment.GetEnvironmentVariable("Temp") + "\\" + "_" + downLoadFileName + "_" + "y" + "_" + "x" + "_" + "m" + "_" + "\\";
                //if (System.IO.Directory.Exists(downLoadPath)) System.IO.Directory.Delete(downLoadPath, true);
                //System.IO.Directory.CreateDirectory(downLoadPath);

                string fullFileName = downLoadPath + "\\" + download_item.文件名;
                byte[] mybyte = download_item.插件包;
                FileStream myfs = new FileStream(fullFileName, FileMode.CreateNew);
                BinaryWriter writefile = new BinaryWriter(myfs);
                writefile.Write(mybyte, 0, mybyte.Length);
                writefile.Close();
                myfs.Close();

                //1、清除主程序运行        
                //Process[] allProcess = Process.GetProcesses();//关闭已经打开的要升级的软件
                //foreach (Process p in allProcess)
                //{
                //    if (p.ProcessName.ToLower() + ".exe" == sMainApp.ToLower())
                //    {
                //        for (int i = 0; i < p.Threads.Count; i++)
                //            p.Threads[i].Dispose();
                //        p.Kill();
                //        //break;
                //    }
                //}
                //System.Threading.Thread.Sleep(3000);

                ////1.1解压缩&复制文件
                //if (System.IO.Path.GetExtension(fullFileName) == ".zip")//从其获取扩展名的路径字符串
                //{
                //    ZipClass.UnZip(fullFileName, downLoadPath, 1, FrmUpdate_SetProgress);
                //    System.IO.File.Delete(fullFileName);
                //}
                ////todo:灵活配置下载路径，从数据库中读取
                //WEISHENG.COMM.fileControl.CopyFile(downLoadPath, Directory.GetCurrentDirectory() + @"\plugins\");

                //System.IO.Directory.Delete(downLoadPath, true);
                ////todo:从内存卸载，实现不用重启，就可以更新插件
                XtraMessageBox.Show("插件下载成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void FrmUpdate_SetProgress(int maximum, string msg)
        {
        }
        private void simpleButton配置_Click(object sender, EventArgs e)
        {
            HIS.COMM.XtraForm操作验证 lg = new HIS.COMM.XtraForm操作验证();
            if (lg.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            HIS.COMM.baseInfo.bResetPluginsConn(true);//重新配置插件仓库地址
        }

        private void simpleButton上传_Click(object sender, EventArgs e)
        {
            HIS.COMM.XtraForm操作验证 lg = new HIS.COMM.XtraForm操作验证();
            if (lg.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            XtraForm插件上传 frm = new XtraForm插件上传(null);
            frm.ShowDialog();
        }

        private void simpleButton更新包_Click(object sender, EventArgs e)
        {
            if (gridView插件下载.FocusedRowHandle < 0)
            {
                return;
            }
            var currModel = gridView插件下载.GetFocusedRow() as HIS.Model.plug_插件版本库;
            XtraForm插件上传 frm = new XtraForm插件上传(currModel);
            frm.ShowDialog();
        }

        private void simpleButton下载到默认位置_Click(object sender, EventArgs e)
        {
            if (gridView插件下载.FocusedRowHandle < 0)
            {
                return;
            }
            var currModel = gridView插件下载.GetFocusedRow() as HIS.Model.plug_插件版本库;
            try
            {
                string downLoadPath = "";
                string downLoadFileName = "temp" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".zip";
                downLoadPath = Environment.GetEnvironmentVariable("Temp") + "\\" + "_" + downLoadFileName + "_" + "y" + "_" + "x" + "_" + "m" + "_" + "\\";
                if (System.IO.Directory.Exists(downLoadPath)) System.IO.Directory.Delete(downLoadPath, true);
                System.IO.Directory.CreateDirectory(downLoadPath);

                string fullFileName = downLoadPath + "\\" + downLoadFileName;
                byte[] mybyte = currModel.插件包;
                FileStream myfs = new FileStream(fullFileName, FileMode.CreateNew);
                BinaryWriter writefile = new BinaryWriter(myfs);
                writefile.Write(mybyte, 0, mybyte.Length);
                writefile.Close();
                myfs.Close();

                //1、清除主程序运行        
                //Process[] allProcess = Process.GetProcesses();//关闭已经打开的要升级的软件
                //foreach (Process p in allProcess)
                //{
                //    if (p.ProcessName.ToLower() + ".exe" == sMainApp.ToLower())
                //    {
                //        for (int i = 0; i < p.Threads.Count; i++)
                //            p.Threads[i].Dispose();
                //        p.Kill();
                //        //break;
                //    }
                //}
                System.Threading.Thread.Sleep(3000);
                //1.1解压缩&复制文件
                if (System.IO.Path.GetExtension(fullFileName) == ".zip")//从其获取扩展名的路径字符串
                {
                    zipHelper.UnZip(fullFileName, downLoadPath, 1, FrmUpdate_SetProgress);
                    System.IO.File.Delete(fullFileName);
                }
                //todo:灵活配置下载路径，从数据库中读取
                WEISHENG.COMM.FileDirtoryHelper.CopyFile(downLoadPath, Directory.GetCurrentDirectory() + @"\plugins\");

                System.IO.Directory.Delete(downLoadPath, true);
                //todo:从内存卸载，实现不用重启，就可以更新插件
                XtraMessageBox.Show("插件下载成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}