﻿namespace ALLINONE
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)

        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            DevExpress.Utils.Animation.PushTransition pushTransition1 = new DevExpress.Utils.Animation.PushTransition();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel工作提醒 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanel功能导航 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.navBarControlLeft = new DevExpress.XtraNavBar.NavBarControl();
            this.imageCollection16 = new DevExpress.Utils.ImageCollection(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.statusBar = new DevExpress.XtraBars.Bar();
            this.barStaticItem软件版本 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem时间 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem单位 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem登录站点 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem登陆用户 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem信息 = new DevExpress.XtraBars.BarButtonItem();
            this.barMain = new DevExpress.XtraBars.Bar();
            this.barSubItem系统 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem系统锁定 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem修改密码 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem打印机设置 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem页面设置 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem默认打印机选择 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem短信发送 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem注销 = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItemTitle = new DevExpress.XtraBars.BarCheckItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barDockingMenuItem1 = new DevExpress.XtraBars.BarDockingMenuItem();
            this.barWorkspaceMenuItem1 = new DevExpress.XtraBars.BarWorkspaceMenuItem();
            this.workspaceManager1 = new DevExpress.Utils.WorkspaceManager();
            this.skinBarSubItem1 = new DevExpress.XtraBars.SkinBarSubItem();
            this.barSubItem工具 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem连接VPN = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem断开VPN = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem计算器 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem屏幕截图 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem日积月累 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem已缓存插件 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem插件管理 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem制作二维码 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem自动升级 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem帮助 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem日志 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem移动设备 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem站点信息 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem关于 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem调查问卷 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem问题反馈 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem退出系统 = new DevExpress.XtraBars.BarSubItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem日期 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem界面 = new DevExpress.XtraBars.BarSubItem();
            this.barCheckItem标题栏 = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem单点登录电子病历 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem我的网盘 = new DevExpress.XtraBars.BarButtonItem();
            this.skinDropDownButtonItem1 = new DevExpress.XtraBars.SkinDropDownButtonItem();
            this.navBarGroup数据检测工具 = new DevExpress.XtraNavBar.NavBarGroup();
            this.mFile = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.timer时钟 = new System.Windows.Forms.Timer(this.components);
            this.pDoc = new System.Drawing.Printing.PrintDocument();
            this.timer特殊任务 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon托盘 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip右键菜单 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.屏幕截图PToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem工作提示 = new System.Windows.Forms.ToolStripMenuItem();
            this.timer工作提示 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker工作提示 = new System.ComponentModel.BackgroundWorker();
            this.panelControl标题 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.label医保规范 = new System.Windows.Forms.Label();
            this.pictureEdit知识库 = new DevExpress.XtraEditors.PictureEdit();
            this.label知识库 = new System.Windows.Forms.Label();
            this.labelControl医院标题 = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureEdit公告 = new DevExpress.XtraEditors.PictureEdit();
            this.label通知公告 = new System.Windows.Forms.Label();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.label我的网盘 = new System.Windows.Forms.Label();
            this.labelsp = new System.Windows.Forms.Label();
            this.mainMenu = new DevExpress.XtraBars.Bar();
            this.menuStripCloseForm = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem关闭当前窗口 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem关闭其他窗口 = new System.Windows.Forms.ToolStripMenuItem();
            this.关闭全部窗口ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alertControl提示 = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            this.imageCollection32 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel工作提醒.SuspendLayout();
            this.dockPanel功能导航.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControlLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.contextMenuStrip右键菜单.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl标题)).BeginInit();
            this.panelControl标题.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit知识库.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit公告.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            this.menuStripCloseForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection32)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel工作提醒});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel功能导航});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraEditors.PanelControl",
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel工作提醒
            // 
            this.dockPanel工作提醒.Controls.Add(this.dockPanel2_Container);
            this.dockPanel工作提醒.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel工作提醒.ID = new System.Guid("db2e8a08-491f-4458-8faa-12a8521e651b");
            this.dockPanel工作提醒.Location = new System.Drawing.Point(4, 23);
            this.dockPanel工作提醒.Name = "dockPanel工作提醒";
            this.dockPanel工作提醒.OriginalSize = new System.Drawing.Size(152, 470);
            this.dockPanel工作提醒.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel工作提醒.SavedIndex = 1;
            this.dockPanel工作提醒.SavedParent = this.dockPanel功能导航;
            this.dockPanel工作提醒.SavedSizeFactor = 1D;
            this.dockPanel工作提醒.SavedTabbed = true;
            this.dockPanel工作提醒.Size = new System.Drawing.Size(172, 470);
            this.dockPanel工作提醒.Text = "工作提醒";
            this.dockPanel工作提醒.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(172, 470);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // dockPanel功能导航
            // 
            this.dockPanel功能导航.Controls.Add(this.dockPanel1_Container);
            this.dockPanel功能导航.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel功能导航.FloatSize = new System.Drawing.Size(200, 516);
            this.dockPanel功能导航.ID = new System.Guid("9aade670-6cb8-4481-bc13-e013c0e2b568");
            this.dockPanel功能导航.Location = new System.Drawing.Point(0, 64);
            this.dockPanel功能导航.Name = "dockPanel功能导航";
            this.dockPanel功能导航.Options.ShowCloseButton = false;
            this.dockPanel功能导航.OriginalSize = new System.Drawing.Size(181, 200);
            this.dockPanel功能导航.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel功能导航.SavedIndex = 0;
            this.dockPanel功能导航.SavedSizeFactor = 1D;
            this.dockPanel功能导航.Size = new System.Drawing.Size(181, 519);
            this.dockPanel功能导航.Text = "功能导航";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.navBarControlLeft);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(172, 492);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // navBarControlLeft
            // 
            this.navBarControlLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControlLeft.LargeImages = this.imageCollection16;
            this.navBarControlLeft.Location = new System.Drawing.Point(0, 0);
            this.navBarControlLeft.Name = "navBarControlLeft";
            this.navBarControlLeft.OptionsNavPane.ExpandedWidth = 172;
            this.navBarControlLeft.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            this.navBarControlLeft.Size = new System.Drawing.Size(172, 492);
            this.navBarControlLeft.TabIndex = 18;
            // 
            // imageCollection16
            // 
            this.imageCollection16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection16.ImageStream")));
            this.imageCollection16.Images.SetKeyName(0, "门诊处方模板.png");
            this.imageCollection16.Images.SetKeyName(1, "门诊费用模板.png");
            this.imageCollection16.Images.SetKeyName(2, "门诊医生个人工作量.png");
            this.imageCollection16.Images.SetKeyName(3, "门诊诊疗.png");
            this.imageCollection16.Images.SetKeyName(4, "手术室.png");
            this.imageCollection16.Images.SetKeyName(5, "医嘱打印.png");
            this.imageCollection16.Images.SetKeyName(6, "住院医生个人工作量.png");
            this.imageCollection16.Images.SetKeyName(7, "住院医嘱模板.png");
            this.imageCollection16.Images.SetKeyName(8, "住院诊疗.png");
            this.imageCollection16.Images.SetKeyName(9, "统计.png");
            this.imageCollection16.Images.SetKeyName(10, "医技确费.png");
            this.imageCollection16.Images.SetKeyName(11, "医技项目明细查询.png");
            this.imageCollection16.Images.SetKeyName(12, "费用上传.png");
            this.imageCollection16.Images.SetKeyName(13, "基础数据匹配.png");
            this.imageCollection16.Images.SetKeyName(14, "目录匹配.png");
            this.imageCollection16.Images.SetKeyName(15, "医保操作.png");
            this.imageCollection16.Images.SetKeyName(16, "医保对账.png");
            this.imageCollection16.Images.SetKeyName(17, "财务付款.png");
            this.imageCollection16.Images.SetKeyName(18, "采购计划.png");
            this.imageCollection16.Images.SetKeyName(19, "采购入库对比表.png");
            this.imageCollection16.Images.SetKeyName(20, "供应商维护.png");
            this.imageCollection16.Images.SetKeyName(21, "库存查询.png");
            this.imageCollection16.Images.SetKeyName(22, "库存上下限.png");
            this.imageCollection16.Images.SetKeyName(23, "库位管理.png");
            this.imageCollection16.Images.SetKeyName(24, "盘点单审核.png");
            this.imageCollection16.Images.SetKeyName(25, "生产厂家.png");
            this.imageCollection16.Images.SetKeyName(26, "往来科室设置.png");
            this.imageCollection16.Images.SetKeyName(27, "新药入库统计.png");
            this.imageCollection16.Images.SetKeyName(28, "药剂科室设置.png");
            this.imageCollection16.Images.SetKeyName(29, "药库汇总账本.png");
            this.imageCollection16.Images.SetKeyName(30, "药库进销存统计.png");
            this.imageCollection16.Images.SetKeyName(31, "药库流水账.png");
            this.imageCollection16.Images.SetKeyName(32, "药库明细查询.png");
            this.imageCollection16.Images.SetKeyName(33, "药库入库.png");
            this.imageCollection16.Images.SetKeyName(34, "药库有效期预警.png");
            this.imageCollection16.Images.SetKeyName(35, "药理分类维护.png");
            this.imageCollection16.Images.SetKeyName(36, "药品参数设置.png");
            this.imageCollection16.Images.SetKeyName(37, "药品出库.png");
            this.imageCollection16.Images.SetKeyName(38, "药品剂型维护.png");
            this.imageCollection16.Images.SetKeyName(39, "药品类型维护.png");
            this.imageCollection16.Images.SetKeyName(40, "药品盘点.png");
            this.imageCollection16.Images.SetKeyName(41, "药品调价.png");
            this.imageCollection16.Images.SetKeyName(42, "药品月结.png");
            this.imageCollection16.Images.SetKeyName(43, "库存查询.png");
            this.imageCollection16.Images.SetKeyName(44, "库存上下限.png");
            this.imageCollection16.Images.SetKeyName(45, "库位管理.png");
            this.imageCollection16.Images.SetKeyName(46, "领药申请.png");
            this.imageCollection16.Images.SetKeyName(47, "门诊发药.png");
            this.imageCollection16.Images.SetKeyName(48, "门诊配药.png");
            this.imageCollection16.Images.SetKeyName(49, "盘点单审核.png");
            this.imageCollection16.Images.SetKeyName(50, "往来科室设置.png");
            this.imageCollection16.Images.SetKeyName(51, "往来科室维护.png");
            this.imageCollection16.Images.SetKeyName(52, "药房拆零设置.png");
            this.imageCollection16.Images.SetKeyName(53, "药房出库.png");
            this.imageCollection16.Images.SetKeyName(54, "药房发药统计.png");
            this.imageCollection16.Images.SetKeyName(55, "药房汇总账本.png");
            this.imageCollection16.Images.SetKeyName(56, "药房进销存统计.png");
            this.imageCollection16.Images.SetKeyName(57, "药房流水账.png");
            this.imageCollection16.Images.SetKeyName(58, "药房明细账查询.png");
            this.imageCollection16.Images.SetKeyName(59, "药房盘点.png");
            this.imageCollection16.Images.SetKeyName(60, "药房入库.png");
            this.imageCollection16.Images.SetKeyName(61, "药房有效期预警.png");
            this.imageCollection16.Images.SetKeyName(62, "药房月结.png");
            this.imageCollection16.Images.SetKeyName(63, "住院发药.png");
            this.imageCollection16.Images.SetKeyName(64, "采购计划.png");
            this.imageCollection16.Images.SetKeyName(65, "查询统计.png");
            this.imageCollection16.Images.SetKeyName(66, "供应商维护.png");
            this.imageCollection16.Images.SetKeyName(67, "库存查询.png");
            this.imageCollection16.Images.SetKeyName(68, "库存上下限.png");
            this.imageCollection16.Images.SetKeyName(69, "库位管理.png");
            this.imageCollection16.Images.SetKeyName(70, "盘点单审核.png");
            this.imageCollection16.Images.SetKeyName(71, "生产厂家维护.png");
            this.imageCollection16.Images.SetKeyName(72, "往来科室设置.png");
            this.imageCollection16.Images.SetKeyName(73, "物资参数设置.png");
            this.imageCollection16.Images.SetKeyName(74, "物资出库.png");
            this.imageCollection16.Images.SetKeyName(75, "物资付款.png");
            this.imageCollection16.Images.SetKeyName(76, "物资汇总账本.png");
            this.imageCollection16.Images.SetKeyName(77, "物资进销存统计.png");
            this.imageCollection16.Images.SetKeyName(78, "物资科室设置.png");
            this.imageCollection16.Images.SetKeyName(79, "物资类型维护.png");
            this.imageCollection16.Images.SetKeyName(80, "物资流水账.png");
            this.imageCollection16.Images.SetKeyName(81, "物资明细查询.png");
            this.imageCollection16.Images.SetKeyName(82, "物资盘点.png");
            this.imageCollection16.Images.SetKeyName(83, "物资入库.png");
            this.imageCollection16.Images.SetKeyName(84, "物资效期预警.png");
            this.imageCollection16.Images.SetKeyName(85, "物资月结.png");
            this.imageCollection16.Images.SetKeyName(86, "出院结算.png");
            this.imageCollection16.Images.SetKeyName(87, "处方查询.png");
            this.imageCollection16.Images.SetKeyName(88, "费用清单.png");
            this.imageCollection16.Images.SetKeyName(89, "个人交款.png");
            this.imageCollection16.Images.SetKeyName(90, "挂号类型.png");
            this.imageCollection16.Images.SetKeyName(91, "结算查询.png");
            this.imageCollection16.Images.SetKeyName(92, "门诊挂号.png");
            this.imageCollection16.Images.SetKeyName(93, "门诊收费.png");
            this.imageCollection16.Images.SetKeyName(94, "票据分配.png");
            this.imageCollection16.Images.SetKeyName(95, "入院登记.png");
            this.imageCollection16.Images.SetKeyName(96, "退费消息.png");
            this.imageCollection16.Images.SetKeyName(97, "医生排班.png");
            this.imageCollection16.Images.SetKeyName(98, "预交金.png");
            this.imageCollection16.Images.SetKeyName(99, "统计.png");
            this.imageCollection16.Images.SetKeyName(100, "药品开方医生统计.png");
            this.imageCollection16.Images.SetKeyName(101, "报表管理.png");
            this.imageCollection16.Images.SetKeyName(102, "本院材料字典.png");
            this.imageCollection16.Images.SetKeyName(103, "本院收费项目.png");
            this.imageCollection16.Images.SetKeyName(104, "本院统计分类.png");
            this.imageCollection16.Images.SetKeyName(105, "本院药品字典.png");
            this.imageCollection16.Images.SetKeyName(106, "本院支付方式.png");
            this.imageCollection16.Images.SetKeyName(107, "病区维护.png");
            this.imageCollection16.Images.SetKeyName(108, "查看业务消息.png");
            this.imageCollection16.Images.SetKeyName(109, "机构维护.png");
            this.imageCollection16.Images.SetKeyName(110, "机构注册码.png");
            this.imageCollection16.Images.SetKeyName(111, "角色权限.png");
            this.imageCollection16.Images.SetKeyName(112, "科室维护.png");
            this.imageCollection16.Images.SetKeyName(113, "频次维护.png");
            this.imageCollection16.Images.SetKeyName(114, "人员维护.png");
            this.imageCollection16.Images.SetKeyName(115, "说明性医嘱维护.png");
            this.imageCollection16.Images.SetKeyName(116, "系统菜单.png");
            this.imageCollection16.Images.SetKeyName(117, "系统参数.png");
            this.imageCollection16.Images.SetKeyName(118, "项目大类.png");
            this.imageCollection16.Images.SetKeyName(119, "消息订阅.png");
            this.imageCollection16.Images.SetKeyName(120, "消息类型设置.png");
            this.imageCollection16.Images.SetKeyName(121, "一键订阅消息.png");
            this.imageCollection16.Images.SetKeyName(122, "用法维护.png");
            this.imageCollection16.Images.SetKeyName(123, "用户维护.png");
            this.imageCollection16.Images.SetKeyName(124, "支付方式.png");
            this.imageCollection16.Images.SetKeyName(125, "执行单配置.png");
            this.imageCollection16.Images.SetKeyName(126, "中心材料字典.png");
            this.imageCollection16.Images.SetKeyName(127, "中心收费项目.png");
            this.imageCollection16.Images.SetKeyName(128, "中心药品字典.png");
            this.imageCollection16.Images.SetKeyName(129, "嘱托维护.png");
            this.imageCollection16.Images.SetKeyName(130, "组合项目.png");
            this.imageCollection16.Images.SetKeyName(131, "会员信息管理.png");
            this.imageCollection16.Images.SetKeyName(132, "礼品兑换.png");
            this.imageCollection16.Images.SetKeyName(133, "礼品管理.png");
            this.imageCollection16.Images.SetKeyName(134, "优惠方案管理.png");
            this.imageCollection16.Images.SetKeyName(135, "优惠明细查询.png");
            this.imageCollection16.Images.SetKeyName(136, "账户类型管理.png");
            this.imageCollection16.Images.SetKeyName(137, "采血站.png");
            this.imageCollection16.Images.SetKeyName(138, "床位维护.png");
            this.imageCollection16.Images.SetKeyName(139, "床位一览.png");
            this.imageCollection16.Images.SetKeyName(140, "费用记账.png");
            this.imageCollection16.Images.SetKeyName(141, "费用清单.png");
            this.imageCollection16.Images.SetKeyName(142, "排队分诊台.png");
            this.imageCollection16.Images.SetKeyName(143, "输液室.png");
            this.imageCollection16.Images.SetKeyName(144, "药品统领.png");
            this.imageCollection16.Images.SetKeyName(145, "医嘱发送.png");
            this.imageCollection16.Images.SetKeyName(146, "医嘱费用核对.png");
            this.imageCollection16.Images.SetKeyName(147, "医嘱申请单.png");
            this.imageCollection16.Images.SetKeyName(148, "医嘱执行单.png");
            this.imageCollection16.Images.SetKeyName(149, "医嘱转抄.png");
            this.imageCollection16.Images.SetKeyName(150, "账单模板.png");
            this.imageCollection16.Images.SetKeyName(151, "病历模板分类维护.png");
            this.imageCollection16.Images.SetKeyName(152, "病历模板维护.png");
            this.imageCollection16.Images.SetKeyName(153, "病历树节点维护.png");
            this.imageCollection16.Images.SetKeyName(154, "病历数据源维护.png");
            this.imageCollection16.Images.SetKeyName(155, "病历知识库维护.png");
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.statusBar,
            this.barMain});
            this.barManager1.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("系统菜单", new System.Guid("974a83cc-76f7-4a5b-b32f-76653e572055")),
            new DevExpress.XtraBars.BarManagerCategory("状态栏", new System.Guid("360fc0ca-65d8-44fa-b6f8-339af8dcc262"))});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageCollection16;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem系统,
            this.barSubItem退出系统,
            this.barStaticItem单位,
            this.barButtonItem系统锁定,
            this.barButtonItem短信发送,
            this.barSubItem工具,
            this.barStaticItem登陆用户,
            this.barButtonItem信息,
            this.barButtonItem日期,
            this.barSubItem界面,
            this.barStaticItem软件版本,
            this.barStaticItem时间,
            this.barButtonItem自动升级,
            this.barButtonItem打印机设置,
            this.barButtonItem页面设置,
            this.barButtonItem默认打印机选择,
            this.barStaticItem登录站点,
            this.barButtonItem调查问卷,
            this.barButtonItem连接VPN,
            this.barButtonItem断开VPN,
            this.barButtonItem计算器,
            this.barButtonItem问题反馈,
            this.barButtonItem屏幕截图,
            this.barButtonItem1,
            this.barSubItem2,
            this.barButtonItem单点登录电子病历,
            this.barCheckItem标题栏,
            this.barButtonItem日积月累,
            this.barButtonItem注销,
            this.barButtonItem修改密码,
            this.barButtonItem已缓存插件,
            this.barButtonItem插件管理,
            this.barButtonItem制作二维码,
            this.barButtonItem4,
            this.barSubItem帮助,
            this.barButtonItem关于,
            this.barButtonItem我的网盘,
            this.barSubItem3,
            this.skinDropDownButtonItem1,
            this.barDockingMenuItem1,
            this.barWorkspaceMenuItem1,
            this.barCheckItemTitle,
            this.skinBarSubItem1,
            this.barButtonItem日志,
            this.barButtonItem移动设备,
            this.barButtonItem站点信息});
            this.barManager1.MainMenu = this.barMain;
            this.barManager1.MaxItemId = 72;
            this.barManager1.StatusBar = this.statusBar;
            // 
            // statusBar
            // 
            this.statusBar.BarName = "statusBar";
            this.statusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.statusBar.DockCol = 0;
            this.statusBar.DockRow = 0;
            this.statusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.statusBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem软件版本),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem时间),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem单位, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem登录站点),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem登陆用户),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.barButtonItem信息, false)});
            this.statusBar.OptionsBar.AllowQuickCustomization = false;
            this.statusBar.OptionsBar.DrawDragBorder = false;
            this.statusBar.OptionsBar.UseWholeRow = true;
            this.statusBar.Text = "statusBar";
            // 
            // barStaticItem软件版本
            // 
            this.barStaticItem软件版本.Caption = "软件版本:";
            this.barStaticItem软件版本.Id = 30;
            this.barStaticItem软件版本.Name = "barStaticItem软件版本";
            // 
            // barStaticItem时间
            // 
            this.barStaticItem时间.Caption = "时间:";
            this.barStaticItem时间.Id = 33;
            this.barStaticItem时间.Name = "barStaticItem时间";
            // 
            // barStaticItem单位
            // 
            this.barStaticItem单位.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem单位.Caption = "单位:";
            this.barStaticItem单位.CategoryGuid = new System.Guid("360fc0ca-65d8-44fa-b6f8-339af8dcc262");
            this.barStaticItem单位.Id = 10;
            this.barStaticItem单位.Name = "barStaticItem单位";
            this.barStaticItem单位.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barStaticItem单位_ItemClick);
            // 
            // barStaticItem登录站点
            // 
            this.barStaticItem登录站点.Caption = "登录站点:";
            this.barStaticItem登录站点.Id = 38;
            this.barStaticItem登录站点.Name = "barStaticItem登录站点";
            this.barStaticItem登录站点.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barStaticItem登录站点_ItemClick);
            this.barStaticItem登录站点.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barStaticItem登录站点_ItemDoubleClick);
            // 
            // barStaticItem登陆用户
            // 
            this.barStaticItem登陆用户.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem登陆用户.Caption = "登陆用户:";
            this.barStaticItem登陆用户.CategoryGuid = new System.Guid("360fc0ca-65d8-44fa-b6f8-339af8dcc262");
            this.barStaticItem登陆用户.Id = 22;
            this.barStaticItem登陆用户.Name = "barStaticItem登陆用户";
            this.barStaticItem登陆用户.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barStaticItem登陆用户_ItemDoubleClick);
            // 
            // barButtonItem信息
            // 
            this.barButtonItem信息.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem信息.Caption = "您尚有未读信息0条";
            this.barButtonItem信息.CategoryGuid = new System.Guid("360fc0ca-65d8-44fa-b6f8-339af8dcc262");
            this.barButtonItem信息.Id = 23;
            this.barButtonItem信息.Name = "barButtonItem信息";
            this.barButtonItem信息.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem信息_ItemDoubleClick);
            // 
            // barMain
            // 
            this.barMain.BarName = "mainMenu";
            this.barMain.DockCol = 0;
            this.barMain.DockRow = 0;
            this.barMain.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMain.FloatLocation = new System.Drawing.Point(393, 152);
            this.barMain.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItem系统, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barCheckItemTitle, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItem3, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItem工具, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem自动升级, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItem帮助, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem调查问卷, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem问题反馈, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItem退出系统, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barMain.OptionsBar.MultiLine = true;
            this.barMain.OptionsBar.UseWholeRow = true;
            this.barMain.Text = "mainMenu";
            // 
            // barSubItem系统
            // 
            this.barSubItem系统.Caption = "系统";
            this.barSubItem系统.CategoryGuid = new System.Guid("974a83cc-76f7-4a5b-b32f-76653e572055");
            this.barSubItem系统.Id = 3;
            this.barSubItem系统.ImageOptions.ImageIndex = 28;
            this.barSubItem系统.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem系统锁定),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem修改密码),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem打印机设置),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem页面设置),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem默认打印机选择),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem短信发送),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem注销)});
            this.barSubItem系统.Name = "barSubItem系统";
            // 
            // barButtonItem系统锁定
            // 
            this.barButtonItem系统锁定.Caption = "系统锁定";
            this.barButtonItem系统锁定.Id = 16;
            this.barButtonItem系统锁定.Name = "barButtonItem系统锁定";
            this.barButtonItem系统锁定.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem修改密码
            // 
            this.barButtonItem修改密码.Caption = "修改密码";
            this.barButtonItem修改密码.Id = 52;
            this.barButtonItem修改密码.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem修改密码.ImageOptions.Image")));
            this.barButtonItem修改密码.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem修改密码.ImageOptions.LargeImage")));
            this.barButtonItem修改密码.Name = "barButtonItem修改密码";
            this.barButtonItem修改密码.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem修改密码_ItemClick);
            // 
            // barButtonItem打印机设置
            // 
            this.barButtonItem打印机设置.Caption = "打印机设置";
            this.barButtonItem打印机设置.Id = 35;
            this.barButtonItem打印机设置.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem打印机设置.ImageOptions.Image")));
            this.barButtonItem打印机设置.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem打印机设置.ImageOptions.LargeImage")));
            this.barButtonItem打印机设置.Name = "barButtonItem打印机设置";
            this.barButtonItem打印机设置.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem打印机设置_ItemClick);
            // 
            // barButtonItem页面设置
            // 
            this.barButtonItem页面设置.Caption = "页面设置";
            this.barButtonItem页面设置.Id = 36;
            this.barButtonItem页面设置.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem页面设置.ImageOptions.Image")));
            this.barButtonItem页面设置.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem页面设置.ImageOptions.LargeImage")));
            this.barButtonItem页面设置.Name = "barButtonItem页面设置";
            this.barButtonItem页面设置.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem页面设置_ItemClick);
            // 
            // barButtonItem默认打印机选择
            // 
            this.barButtonItem默认打印机选择.Caption = "默认打印机选择";
            this.barButtonItem默认打印机选择.Id = 37;
            this.barButtonItem默认打印机选择.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem默认打印机选择.ImageOptions.Image")));
            this.barButtonItem默认打印机选择.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem默认打印机选择.ImageOptions.LargeImage")));
            this.barButtonItem默认打印机选择.Name = "barButtonItem默认打印机选择";
            this.barButtonItem默认打印机选择.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem默认打印机选择_ItemClick);
            // 
            // barButtonItem短信发送
            // 
            this.barButtonItem短信发送.Caption = "短信发送";
            this.barButtonItem短信发送.Id = 17;
            this.barButtonItem短信发送.Name = "barButtonItem短信发送";
            this.barButtonItem短信发送.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem短信发送.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem短信发送_ItemClick);
            // 
            // barButtonItem注销
            // 
            this.barButtonItem注销.Caption = "注销";
            this.barButtonItem注销.Id = 51;
            this.barButtonItem注销.Name = "barButtonItem注销";
            this.barButtonItem注销.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem注销.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem注销_ItemClick);
            // 
            // barCheckItemTitle
            // 
            this.barCheckItemTitle.Caption = "标题栏";
            this.barCheckItemTitle.Id = 67;
            this.barCheckItemTitle.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCheckItemTitle.ImageOptions.Image")));
            this.barCheckItemTitle.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barCheckItemTitle.ImageOptions.LargeImage")));
            this.barCheckItemTitle.Name = "barCheckItemTitle";
            this.barCheckItemTitle.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemTitle_CheckedChanged);
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "布局";
            this.barSubItem3.Id = 63;
            this.barSubItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem3.ImageOptions.Image")));
            this.barSubItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem3.ImageOptions.LargeImage")));
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barDockingMenuItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barWorkspaceMenuItem1),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.skinBarSubItem1, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barDockingMenuItem1
            // 
            this.barDockingMenuItem1.Caption = "保存样式";
            this.barDockingMenuItem1.Id = 65;
            this.barDockingMenuItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barDockingMenuItem1.ImageOptions.Image")));
            this.barDockingMenuItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barDockingMenuItem1.ImageOptions.LargeImage")));
            this.barDockingMenuItem1.Name = "barDockingMenuItem1";
            // 
            // barWorkspaceMenuItem1
            // 
            this.barWorkspaceMenuItem1.Caption = "我的样式";
            this.barWorkspaceMenuItem1.Id = 66;
            this.barWorkspaceMenuItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barWorkspaceMenuItem1.ImageOptions.Image")));
            this.barWorkspaceMenuItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barWorkspaceMenuItem1.ImageOptions.LargeImage")));
            this.barWorkspaceMenuItem1.Name = "barWorkspaceMenuItem1";
            this.barWorkspaceMenuItem1.WorkspaceManager = this.workspaceManager1;
            // 
            // workspaceManager1
            // 
            this.workspaceManager1.TargetControl = this;
            this.workspaceManager1.TransitionType = pushTransition1;
            // 
            // skinBarSubItem1
            // 
            this.skinBarSubItem1.Caption = "样式风格";
            this.skinBarSubItem1.Id = 68;
            this.skinBarSubItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("skinBarSubItem1.ImageOptions.Image")));
            this.skinBarSubItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("skinBarSubItem1.ImageOptions.LargeImage")));
            this.skinBarSubItem1.Name = "skinBarSubItem1";
            // 
            // barSubItem工具
            // 
            this.barSubItem工具.Caption = "常用工具";
            this.barSubItem工具.CategoryGuid = new System.Guid("974a83cc-76f7-4a5b-b32f-76653e572055");
            this.barSubItem工具.Id = 18;
            this.barSubItem工具.ImageOptions.ImageIndex = 118;
            this.barSubItem工具.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem连接VPN),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem断开VPN),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem计算器),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem屏幕截图),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem日积月累),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem已缓存插件, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem插件管理),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem制作二维码, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barSubItem工具.Name = "barSubItem工具";
            // 
            // barButtonItem连接VPN
            // 
            this.barButtonItem连接VPN.Caption = "连接VPN网络";
            this.barButtonItem连接VPN.Id = 41;
            this.barButtonItem连接VPN.Name = "barButtonItem连接VPN";
            this.barButtonItem连接VPN.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem连接VPN.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem连接VPN_ItemClick);
            // 
            // barButtonItem断开VPN
            // 
            this.barButtonItem断开VPN.Caption = "断开VPN网络";
            this.barButtonItem断开VPN.Id = 42;
            this.barButtonItem断开VPN.Name = "barButtonItem断开VPN";
            this.barButtonItem断开VPN.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem断开VPN.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem断开VPN_ItemClick);
            // 
            // barButtonItem计算器
            // 
            this.barButtonItem计算器.Caption = "计算器";
            this.barButtonItem计算器.Id = 43;
            this.barButtonItem计算器.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem计算器.ImageOptions.Image")));
            this.barButtonItem计算器.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem计算器.ImageOptions.LargeImage")));
            this.barButtonItem计算器.Name = "barButtonItem计算器";
            this.barButtonItem计算器.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem计算器_ItemClick);
            // 
            // barButtonItem屏幕截图
            // 
            this.barButtonItem屏幕截图.Caption = "屏幕截图&P";
            this.barButtonItem屏幕截图.Id = 45;
            this.barButtonItem屏幕截图.Name = "barButtonItem屏幕截图";
            this.barButtonItem屏幕截图.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem屏幕截图.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem屏幕截图_ItemClick);
            // 
            // barButtonItem日积月累
            // 
            this.barButtonItem日积月累.Caption = "日积月累";
            this.barButtonItem日积月累.Id = 50;
            this.barButtonItem日积月累.Name = "barButtonItem日积月累";
            this.barButtonItem日积月累.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem日积月累.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem日积月累_ItemClick);
            // 
            // barButtonItem已缓存插件
            // 
            this.barButtonItem已缓存插件.Caption = "已缓存插件";
            this.barButtonItem已缓存插件.Id = 53;
            this.barButtonItem已缓存插件.ImageIndexDisabled = 6;
            this.barButtonItem已缓存插件.ImageOptions.DisabledImageIndex = 6;
            this.barButtonItem已缓存插件.ImageOptions.ImageIndex = 8;
            this.barButtonItem已缓存插件.Name = "barButtonItem已缓存插件";
            this.barButtonItem已缓存插件.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem已缓存插件_ItemClick);
            // 
            // barButtonItem插件管理
            // 
            this.barButtonItem插件管理.Caption = "插件管理";
            this.barButtonItem插件管理.Id = 54;
            this.barButtonItem插件管理.ImageOptions.ImageIndex = 2;
            this.barButtonItem插件管理.Name = "barButtonItem插件管理";
            this.barButtonItem插件管理.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem插件管理_ItemClick);
            // 
            // barButtonItem制作二维码
            // 
            this.barButtonItem制作二维码.Caption = "制作二维码";
            this.barButtonItem制作二维码.Id = 55;
            this.barButtonItem制作二维码.ImageOptions.ImageIndex = 1;
            this.barButtonItem制作二维码.Name = "barButtonItem制作二维码";
            this.barButtonItem制作二维码.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem制作二维码_ItemClick);
            // 
            // barButtonItem自动升级
            // 
            this.barButtonItem自动升级.Caption = "自动升级";
            this.barButtonItem自动升级.CategoryGuid = new System.Guid("974a83cc-76f7-4a5b-b32f-76653e572055");
            this.barButtonItem自动升级.Id = 34;
            this.barButtonItem自动升级.ImageOptions.ImageIndex = 27;
            this.barButtonItem自动升级.Name = "barButtonItem自动升级";
            this.barButtonItem自动升级.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem自动升级_ItemClick);
            // 
            // barSubItem帮助
            // 
            this.barSubItem帮助.Caption = "帮助";
            this.barSubItem帮助.CategoryGuid = new System.Guid("974a83cc-76f7-4a5b-b32f-76653e572055");
            this.barSubItem帮助.Id = 59;
            this.barSubItem帮助.ImageOptions.ImageIndex = 14;
            this.barSubItem帮助.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem日志, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem移动设备, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem站点信息),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem关于)});
            this.barSubItem帮助.Name = "barSubItem帮助";
            this.barSubItem帮助.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSubItem帮助_ItemClick);
            // 
            // barButtonItem日志
            // 
            this.barButtonItem日志.Caption = "更新日志";
            this.barButtonItem日志.Id = 69;
            this.barButtonItem日志.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem日志.ImageOptions.Image")));
            this.barButtonItem日志.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem日志.ImageOptions.LargeImage")));
            this.barButtonItem日志.Name = "barButtonItem日志";
            this.barButtonItem日志.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem日志_ItemClick);
            // 
            // barButtonItem移动设备
            // 
            this.barButtonItem移动设备.Caption = "移动设备";
            this.barButtonItem移动设备.Id = 70;
            this.barButtonItem移动设备.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem移动设备.ImageOptions.Image")));
            this.barButtonItem移动设备.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem移动设备.ImageOptions.LargeImage")));
            this.barButtonItem移动设备.Name = "barButtonItem移动设备";
            this.barButtonItem移动设备.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem移动设备_ItemClick);
            // 
            // barButtonItem站点信息
            // 
            this.barButtonItem站点信息.Caption = "站点信息";
            this.barButtonItem站点信息.Id = 71;
            this.barButtonItem站点信息.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem站点信息.ImageOptions.Image")));
            this.barButtonItem站点信息.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem站点信息.ImageOptions.LargeImage")));
            this.barButtonItem站点信息.Name = "barButtonItem站点信息";
            this.barButtonItem站点信息.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem站点信息_ItemClick);
            // 
            // barButtonItem关于
            // 
            this.barButtonItem关于.Caption = "关于";
            this.barButtonItem关于.Id = 60;
            this.barButtonItem关于.ImageOptions.ImageIndex = 0;
            this.barButtonItem关于.Name = "barButtonItem关于";
            this.barButtonItem关于.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem关于_ItemClick);
            // 
            // barButtonItem调查问卷
            // 
            this.barButtonItem调查问卷.Caption = "调查问卷";
            this.barButtonItem调查问卷.Id = 40;
            this.barButtonItem调查问卷.ImageOptions.ImageIndex = 2;
            this.barButtonItem调查问卷.Name = "barButtonItem调查问卷";
            this.barButtonItem调查问卷.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem调查问卷.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem调查问卷_ItemClick);
            // 
            // barButtonItem问题反馈
            // 
            this.barButtonItem问题反馈.Caption = "问题反馈";
            this.barButtonItem问题反馈.Id = 44;
            this.barButtonItem问题反馈.ImageOptions.ImageIndex = 37;
            this.barButtonItem问题反馈.Name = "barButtonItem问题反馈";
            this.barButtonItem问题反馈.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem问题反馈.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem问题反馈_ItemClick);
            // 
            // barSubItem退出系统
            // 
            this.barSubItem退出系统.Caption = "退出系统";
            this.barSubItem退出系统.CategoryGuid = new System.Guid("974a83cc-76f7-4a5b-b32f-76653e572055");
            this.barSubItem退出系统.Id = 9;
            this.barSubItem退出系统.ImageOptions.ImageIndex = 147;
            this.barSubItem退出系统.Name = "barSubItem退出系统";
            this.barSubItem退出系统.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSubItem退出系统_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1062, 27);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 583);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1062, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 27);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 556);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1062, 27);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 556);
            // 
            // barButtonItem日期
            // 
            this.barButtonItem日期.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem日期.Caption = "日期:";
            this.barButtonItem日期.CategoryGuid = new System.Guid("360fc0ca-65d8-44fa-b6f8-339af8dcc262");
            this.barButtonItem日期.Id = 24;
            this.barButtonItem日期.Name = "barButtonItem日期";
            // 
            // barSubItem界面
            // 
            this.barSubItem界面.Caption = "界面";
            this.barSubItem界面.Id = 28;
            this.barSubItem界面.ImageOptions.ImageIndex = 6;
            this.barSubItem界面.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItem标题栏)});
            this.barSubItem界面.Name = "barSubItem界面";
            // 
            // barCheckItem标题栏
            // 
            this.barCheckItem标题栏.BindableChecked = true;
            this.barCheckItem标题栏.Caption = "标题栏";
            this.barCheckItem标题栏.Checked = true;
            this.barCheckItem标题栏.Id = 49;
            this.barCheckItem标题栏.Name = "barCheckItem标题栏";
            this.barCheckItem标题栏.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItem标题栏_CheckedChanged);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 46;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "单点登录";
            this.barSubItem2.Id = 47;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem单点登录电子病历, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem单点登录电子病历
            // 
            this.barButtonItem单点登录电子病历.Caption = "电子病历";
            this.barButtonItem单点登录电子病历.Id = 48;
            this.barButtonItem单点登录电子病历.Name = "barButtonItem单点登录电子病历";
            this.barButtonItem单点登录电子病历.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem单点登录电子病历_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.CategoryGuid = new System.Guid("974a83cc-76f7-4a5b-b32f-76653e572055");
            this.barButtonItem4.Id = 57;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem我的网盘
            // 
            this.barButtonItem我的网盘.Id = 62;
            this.barButtonItem我的网盘.Name = "barButtonItem我的网盘";
            // 
            // skinDropDownButtonItem1
            // 
            this.skinDropDownButtonItem1.Id = 64;
            this.skinDropDownButtonItem1.Name = "skinDropDownButtonItem1";
            // 
            // navBarGroup数据检测工具
            // 
            this.navBarGroup数据检测工具.Name = "navBarGroup数据检测工具";
            // 
            // mFile
            // 
            this.mFile.Caption = "&File";
            this.mFile.CategoryGuid = new System.Guid("01b4aa47-f56a-49b3-845b-a6dde012966a");
            this.mFile.Id = 1;
            this.mFile.Name = "mFile";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "&File";
            this.barSubItem1.CategoryGuid = new System.Guid("01b4aa47-f56a-49b3-845b-a6dde012966a");
            this.barSubItem1.Id = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.SelectedPageChanged += new System.EventHandler(this.xtraTabbedMdiManager1_SelectedPageChanged);
            this.xtraTabbedMdiManager1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.xtraTabbedMdiManager1_MouseUp);
            // 
            // timer时钟
            // 
            this.timer时钟.Enabled = true;
            this.timer时钟.Interval = 1000;
            this.timer时钟.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // timer特殊任务
            // 
            this.timer特殊任务.Enabled = true;
            this.timer特殊任务.Interval = 60000;
            this.timer特殊任务.Tick += new System.EventHandler(this.timer特殊任务_Tick);
            // 
            // notifyIcon托盘
            // 
            this.notifyIcon托盘.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon托盘.BalloonTipText = "点击鼠标恢复工作界面。";
            this.notifyIcon托盘.ContextMenuStrip = this.contextMenuStrip右键菜单;
            this.notifyIcon托盘.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon托盘.Icon")));
            this.notifyIcon托盘.Text = "allInOne";
            this.notifyIcon托盘.Visible = true;
            this.notifyIcon托盘.Click += new System.EventHandler(this.notifyIcon托盘_Click);
            this.notifyIcon托盘.DoubleClick += new System.EventHandler(this.notifyIcon托盘_DoubleClick);
            // 
            // contextMenuStrip右键菜单
            // 
            this.contextMenuStrip右键菜单.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip右键菜单.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.屏幕截图PToolStripMenuItem,
            this.toolStripMenuItem工作提示});
            this.contextMenuStrip右键菜单.Name = "contextMenuStrip1";
            this.contextMenuStrip右键菜单.ShowCheckMargin = true;
            this.contextMenuStrip右键菜单.ShowImageMargin = false;
            this.contextMenuStrip右键菜单.Size = new System.Drawing.Size(144, 52);
            // 
            // 屏幕截图PToolStripMenuItem
            // 
            this.屏幕截图PToolStripMenuItem.Name = "屏幕截图PToolStripMenuItem";
            this.屏幕截图PToolStripMenuItem.Size = new System.Drawing.Size(143, 24);
            this.屏幕截图PToolStripMenuItem.Text = "影像传输&P";
            this.屏幕截图PToolStripMenuItem.Visible = false;
            this.屏幕截图PToolStripMenuItem.Click += new System.EventHandler(this.屏幕截图PToolStripMenuItem_Click);
            // 
            // toolStripMenuItem工作提示
            // 
            this.toolStripMenuItem工作提示.Checked = true;
            this.toolStripMenuItem工作提示.CheckOnClick = true;
            this.toolStripMenuItem工作提示.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem工作提示.Name = "toolStripMenuItem工作提示";
            this.toolStripMenuItem工作提示.Size = new System.Drawing.Size(143, 24);
            this.toolStripMenuItem工作提示.Text = "工作提示";
            this.toolStripMenuItem工作提示.CheckedChanged += new System.EventHandler(this.toolStripMenuItem工作提示_CheckedChanged);
            // 
            // timer工作提示
            // 
            this.timer工作提示.Enabled = true;
            this.timer工作提示.Interval = 40000;
            this.timer工作提示.Tick += new System.EventHandler(this.timer工作提示_Tick);
            // 
            // backgroundWorker工作提示
            // 
            this.backgroundWorker工作提示.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // panelControl标题
            // 
            this.panelControl标题.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(165)))), ((int)(((byte)(214)))));
            this.panelControl标题.Appearance.Options.UseBackColor = true;
            this.panelControl标题.Appearance.Options.UseImage = true;
            this.panelControl标题.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControl标题.ContentImage")));
            this.panelControl标题.ContentImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.panelControl标题.Controls.Add(this.pictureEdit1);
            this.panelControl标题.Controls.Add(this.label医保规范);
            this.panelControl标题.Controls.Add(this.pictureEdit知识库);
            this.panelControl标题.Controls.Add(this.label知识库);
            this.panelControl标题.Controls.Add(this.labelControl医院标题);
            this.panelControl标题.Controls.Add(this.pictureBox1);
            this.panelControl标题.Controls.Add(this.pictureEdit公告);
            this.panelControl标题.Controls.Add(this.label通知公告);
            this.panelControl标题.Controls.Add(this.pictureEdit2);
            this.panelControl标题.Controls.Add(this.label我的网盘);
            this.panelControl标题.Controls.Add(this.labelsp);
            this.panelControl标题.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl标题.Location = new System.Drawing.Point(0, 27);
            this.panelControl标题.Name = "panelControl标题";
            this.panelControl标题.Size = new System.Drawing.Size(1062, 37);
            this.panelControl标题.TabIndex = 8;
            this.panelControl标题.DoubleClick += new System.EventHandler(this.panelControl标题_DoubleClick);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(747, 2);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(21, 33);
            this.pictureEdit1.TabIndex = 32;
            // 
            // label医保规范
            // 
            this.label医保规范.BackColor = System.Drawing.Color.Transparent;
            this.label医保规范.Dock = System.Windows.Forms.DockStyle.Right;
            this.label医保规范.Location = new System.Drawing.Point(768, 2);
            this.label医保规范.Name = "label医保规范";
            this.label医保规范.Size = new System.Drawing.Size(65, 33);
            this.label医保规范.TabIndex = 31;
            this.label医保规范.Text = "医保规范";
            this.label医保规范.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label医保规范.Click += new System.EventHandler(this.label医保规范_Click);
            // 
            // pictureEdit知识库
            // 
            this.pictureEdit知识库.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit知识库.EditValue = ((object)(resources.GetObject("pictureEdit知识库.EditValue")));
            this.pictureEdit知识库.Location = new System.Drawing.Point(833, 2);
            this.pictureEdit知识库.Name = "pictureEdit知识库";
            this.pictureEdit知识库.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit知识库.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit知识库.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit知识库.Size = new System.Drawing.Size(21, 33);
            this.pictureEdit知识库.TabIndex = 30;
            this.pictureEdit知识库.EditValueChanged += new System.EventHandler(this.pictureEdit知识库_EditValueChanged);
            this.pictureEdit知识库.Click += new System.EventHandler(this.pictureEdit知识库_Click);
            // 
            // label知识库
            // 
            this.label知识库.BackColor = System.Drawing.Color.Transparent;
            this.label知识库.Dock = System.Windows.Forms.DockStyle.Right;
            this.label知识库.Location = new System.Drawing.Point(854, 2);
            this.label知识库.Name = "label知识库";
            this.label知识库.Size = new System.Drawing.Size(44, 33);
            this.label知识库.TabIndex = 29;
            this.label知识库.Text = "知识库";
            this.label知识库.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label知识库.Click += new System.EventHandler(this.pictureEdit知识库_Click);
            // 
            // labelControl医院标题
            // 
            this.labelControl医院标题.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl医院标题.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl医院标题.Appearance.Options.UseFont = true;
            this.labelControl医院标题.Appearance.Options.UseForeColor = true;
            this.labelControl医院标题.Location = new System.Drawing.Point(62, 6);
            this.labelControl医院标题.Name = "labelControl医院标题";
            this.labelControl医院标题.Size = new System.Drawing.Size(133, 22);
            this.labelControl医院标题.TabIndex = 27;
            this.labelControl医院标题.Text = "|医院信息管理系统";
            this.labelControl医院标题.DoubleClick += new System.EventHandler(this.labelControl医院标题_DoubleClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // pictureEdit公告
            // 
            this.pictureEdit公告.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit公告.EditValue = ((object)(resources.GetObject("pictureEdit公告.EditValue")));
            this.pictureEdit公告.Location = new System.Drawing.Point(898, 2);
            this.pictureEdit公告.Name = "pictureEdit公告";
            this.pictureEdit公告.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit公告.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit公告.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit公告.Size = new System.Drawing.Size(22, 33);
            this.pictureEdit公告.TabIndex = 2;
            this.pictureEdit公告.Click += new System.EventHandler(this.pictureEdit公告_Click);
            // 
            // label通知公告
            // 
            this.label通知公告.BackColor = System.Drawing.Color.Transparent;
            this.label通知公告.Dock = System.Windows.Forms.DockStyle.Right;
            this.label通知公告.Location = new System.Drawing.Point(920, 2);
            this.label通知公告.Name = "label通知公告";
            this.label通知公告.Size = new System.Drawing.Size(31, 33);
            this.label通知公告.TabIndex = 1;
            this.label通知公告.Text = "公告";
            this.label通知公告.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label通知公告.Click += new System.EventHandler(this.pictureEdit公告_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(951, 2);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Size = new System.Drawing.Size(22, 33);
            this.pictureEdit2.TabIndex = 5;
            this.pictureEdit2.Click += new System.EventHandler(this.label我的网盘_Click);
            // 
            // label我的网盘
            // 
            this.label我的网盘.BackColor = System.Drawing.Color.Transparent;
            this.label我的网盘.Dock = System.Windows.Forms.DockStyle.Right;
            this.label我的网盘.Location = new System.Drawing.Point(973, 2);
            this.label我的网盘.Name = "label我的网盘";
            this.label我的网盘.Size = new System.Drawing.Size(56, 33);
            this.label我的网盘.TabIndex = 4;
            this.label我的网盘.Text = "我的网盘";
            this.label我的网盘.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label我的网盘.Click += new System.EventHandler(this.label我的网盘_Click);
            // 
            // labelsp
            // 
            this.labelsp.BackColor = System.Drawing.Color.Transparent;
            this.labelsp.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelsp.Location = new System.Drawing.Point(1029, 2);
            this.labelsp.Name = "labelsp";
            this.labelsp.Size = new System.Drawing.Size(31, 33);
            this.labelsp.TabIndex = 3;
            this.labelsp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mainMenu
            // 
            this.mainMenu.BarName = "mainMenu";
            this.mainMenu.DockCol = 0;
            this.mainMenu.DockRow = 0;
            this.mainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.mainMenu.OptionsBar.MultiLine = true;
            this.mainMenu.OptionsBar.UseWholeRow = true;
            this.mainMenu.Text = "mainMenu";
            // 
            // menuStripCloseForm
            // 
            this.menuStripCloseForm.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripCloseForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem关闭当前窗口,
            this.toolStripMenuItem关闭其他窗口,
            this.关闭全部窗口ToolStripMenuItem});
            this.menuStripCloseForm.Name = "menuStripCloseForm";
            this.menuStripCloseForm.Size = new System.Drawing.Size(167, 82);
            // 
            // toolStripMenuItem关闭当前窗口
            // 
            this.toolStripMenuItem关闭当前窗口.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem关闭当前窗口.Image")));
            this.toolStripMenuItem关闭当前窗口.Name = "toolStripMenuItem关闭当前窗口";
            this.toolStripMenuItem关闭当前窗口.Size = new System.Drawing.Size(166, 26);
            this.toolStripMenuItem关闭当前窗口.Text = "关闭当前窗口";
            this.toolStripMenuItem关闭当前窗口.Click += new System.EventHandler(this.toolStripMenuItem关闭当前窗口_Click);
            // 
            // toolStripMenuItem关闭其他窗口
            // 
            this.toolStripMenuItem关闭其他窗口.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem关闭其他窗口.Image")));
            this.toolStripMenuItem关闭其他窗口.Name = "toolStripMenuItem关闭其他窗口";
            this.toolStripMenuItem关闭其他窗口.Size = new System.Drawing.Size(166, 26);
            this.toolStripMenuItem关闭其他窗口.Text = "关闭其他窗口";
            this.toolStripMenuItem关闭其他窗口.Click += new System.EventHandler(this.toolStripMenuItem关闭其他窗口_Click);
            // 
            // 关闭全部窗口ToolStripMenuItem
            // 
            this.关闭全部窗口ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("关闭全部窗口ToolStripMenuItem.Image")));
            this.关闭全部窗口ToolStripMenuItem.Name = "关闭全部窗口ToolStripMenuItem";
            this.关闭全部窗口ToolStripMenuItem.Size = new System.Drawing.Size(166, 26);
            this.关闭全部窗口ToolStripMenuItem.Text = "关闭全部窗口";
            this.关闭全部窗口ToolStripMenuItem.Click += new System.EventHandler(this.关闭全部窗口ToolStripMenuItem_Click);
            // 
            // alertControl提示
            // 
            this.alertControl提示.AppearanceCaption.Image = ((System.Drawing.Image)(resources.GetObject("alertControl提示.AppearanceCaption.Image")));
            this.alertControl提示.AppearanceCaption.Options.UseImage = true;
            this.alertControl提示.AutoHeight = true;
            this.alertControl提示.Images = this.imageCollection16;
            this.alertControl提示.AlertClick += new DevExpress.XtraBars.Alerter.AlertClickEventHandler(this.alertControl提示_AlertClick);
            // 
            // imageCollection32
            // 
            this.imageCollection32.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection32.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection32.ImageStream")));
            this.imageCollection32.Images.SetKeyName(0, "病历模板分类维护.png");
            this.imageCollection32.Images.SetKeyName(1, "病历模板维护.png");
            this.imageCollection32.Images.SetKeyName(2, "病历树节点维护.png");
            this.imageCollection32.Images.SetKeyName(3, "病历数据源维护.png");
            this.imageCollection32.Images.SetKeyName(4, "病历知识库维护.png");
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 613);
            this.Controls.Add(this.dockPanel功能导航);
            this.Controls.Add(this.panelControl标题);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "yunHis";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.SizeChanged += new System.EventHandler(this.FormMain_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel工作提醒.ResumeLayout(false);
            this.dockPanel功能导航.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControlLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.contextMenuStrip右键菜单.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl标题)).EndInit();
            this.panelControl标题.ResumeLayout(false);
            this.panelControl标题.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit知识库.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit公告.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            this.menuStripCloseForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection32)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.BarSubItem mFile;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private System.Windows.Forms.Timer timer时钟;
        private System.Drawing.Printing.PrintDocument pDoc;
        private System.Windows.Forms.Timer timer特殊任务;
        private System.Windows.Forms.NotifyIcon notifyIcon托盘;
        private System.Windows.Forms.Timer timer工作提示;
        private System.ComponentModel.BackgroundWorker backgroundWorker工作提示;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup数据检测工具;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip右键菜单;
        private System.Windows.Forms.ToolStripMenuItem 屏幕截图PToolStripMenuItem;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel功能导航;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem工作提示;
        private DevExpress.XtraEditors.PanelControl panelControl标题;
        private DevExpress.XtraEditors.LabelControl labelControl医院标题;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit公告;
        private System.Windows.Forms.Label label通知公告;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private System.Windows.Forms.Label label我的网盘;
        private System.Windows.Forms.Label labelsp;
        private DevExpress.XtraBars.Bar mainMenu;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar statusBar;
        private DevExpress.XtraBars.BarStaticItem barStaticItem软件版本;
        private DevExpress.XtraBars.BarStaticItem barStaticItem时间;
        private DevExpress.XtraBars.BarStaticItem barStaticItem单位;
        private DevExpress.XtraBars.BarStaticItem barStaticItem登录站点;
        private DevExpress.XtraBars.BarStaticItem barStaticItem登陆用户;
        private DevExpress.XtraBars.BarButtonItem barButtonItem信息;
        private DevExpress.XtraBars.Bar barMain;
        private DevExpress.XtraBars.BarSubItem barSubItem系统;
        private DevExpress.XtraBars.BarButtonItem barButtonItem系统锁定;
        private DevExpress.XtraBars.BarButtonItem barButtonItem短信发送;
        private DevExpress.XtraBars.BarButtonItem barButtonItem打印机设置;
        private DevExpress.XtraBars.BarButtonItem barButtonItem页面设置;
        private DevExpress.XtraBars.BarButtonItem barButtonItem默认打印机选择;
        private DevExpress.XtraBars.BarSubItem barSubItem界面;
        private DevExpress.XtraBars.BarSubItem barSubItem工具;
        private DevExpress.XtraBars.BarButtonItem barButtonItem连接VPN;
        private DevExpress.XtraBars.BarButtonItem barButtonItem断开VPN;
        private DevExpress.XtraBars.BarButtonItem barButtonItem计算器;
        private DevExpress.XtraBars.BarButtonItem barButtonItem屏幕截图;
        private DevExpress.XtraBars.BarButtonItem barButtonItem自动升级;
        private DevExpress.XtraBars.BarButtonItem barButtonItem调查问卷;
        private DevExpress.XtraBars.BarButtonItem barButtonItem问题反馈;
        private DevExpress.XtraBars.BarSubItem barSubItem退出系统;
        private DevExpress.XtraBars.BarButtonItem barButtonItem日期;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private System.Windows.Forms.ContextMenuStrip menuStripCloseForm;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem关闭当前窗口;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem关闭其他窗口;
        private System.Windows.Forms.ToolStripMenuItem 关闭全部窗口ToolStripMenuItem;
        private DevExpress.XtraBars.Alerter.AlertControl alertControl提示;
        private DevExpress.Utils.ImageCollection imageCollection16;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem单点登录电子病历;
        private DevExpress.XtraEditors.PictureEdit pictureEdit知识库;
        private System.Windows.Forms.Label label知识库;
        private DevExpress.XtraBars.BarCheckItem barCheckItem标题栏;
        private DevExpress.XtraNavBar.NavBarControl navBarControlLeft;
        private DevExpress.XtraBars.BarButtonItem barButtonItem日积月累;
        private DevExpress.XtraBars.BarButtonItem barButtonItem注销;
        private DevExpress.XtraBars.BarButtonItem barButtonItem修改密码;
        private DevExpress.XtraBars.BarButtonItem barButtonItem已缓存插件;
        private DevExpress.XtraBars.BarButtonItem barButtonItem插件管理;
        private DevExpress.XtraBars.BarButtonItem barButtonItem制作二维码;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarSubItem barSubItem帮助;
        private DevExpress.XtraBars.BarButtonItem barButtonItem关于;
        private DevExpress.XtraBars.BarButtonItem barButtonItem我的网盘;
        private DevExpress.Utils.ImageCollection imageCollection32;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel工作提醒;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.SkinDropDownButtonItem skinDropDownButtonItem1;
        private DevExpress.XtraBars.BarDockingMenuItem barDockingMenuItem1;
        private DevExpress.XtraBars.BarWorkspaceMenuItem barWorkspaceMenuItem1;
        private DevExpress.Utils.WorkspaceManager workspaceManager1;
        private DevExpress.XtraBars.BarCheckItem barCheckItemTitle;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.Label label医保规范;
        private DevExpress.XtraBars.BarButtonItem barButtonItem日志;
        private DevExpress.XtraBars.BarButtonItem barButtonItem移动设备;
        private DevExpress.XtraBars.BarButtonItem barButtonItem站点信息;
    }
}