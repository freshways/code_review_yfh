﻿using DevExpress.XtraEditors;

namespace ALLINONE
{
    partial class FormLogIn
    {
        /// <summary>
        /// 必需的设计器变量。
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogIn));
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.textEditMM = new DevExpress.XtraEditors.TextEdit();
            this.searchLookUpEditUser = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col用户编号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col拼音代码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用户名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用户密码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col角色名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col科室名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col单位编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col是否禁用 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl版本号 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl标题信息 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit记住模块 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit记住密码 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit记住模块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit记住密码.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.simpleButtonCancel.Appearance.Options.UseBackColor = true;
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.Appearance.Options.UseForeColor = true;
            this.simpleButtonCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("simpleButtonCancel.BackgroundImage")));
            this.simpleButtonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.simpleButtonCancel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(728, 218);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(110, 42);
            this.simpleButtonCancel.TabIndex = 8;
            this.simpleButtonCancel.TabStop = false;
            this.simpleButtonCancel.Text = "取  消";
            this.simpleButtonCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnOk.Appearance.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnOk.Appearance.Options.UseBackColor = true;
            this.btnOk.Appearance.Options.UseFont = true;
            this.btnOk.Appearance.Options.UseForeColor = true;
            this.btnOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOk.BackgroundImage")));
            this.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOk.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnOk.Location = new System.Drawing.Point(585, 218);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(110, 42);
            this.btnOk.TabIndex = 7;
            this.btnOk.TabStop = false;
            this.btnOk.Text = "登  录";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // textEditMM
            // 
            this.textEditMM.EditValue = "";
            this.textEditMM.ImeMode = System.Windows.Forms.ImeMode.Close;
            this.textEditMM.Location = new System.Drawing.Point(598, 141);
            this.textEditMM.Name = "textEditMM";
            this.textEditMM.Properties.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.textEditMM.Properties.Appearance.Options.UseForeColor = true;
            this.textEditMM.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditMM.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEditMM.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEditMM.Properties.AutoHeight = false;
            this.textEditMM.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEditMM.Properties.PasswordChar = '*';
            this.textEditMM.Size = new System.Drawing.Size(231, 32);
            this.textEditMM.TabIndex = 13;
            this.textEditMM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textEditMM_KeyDown);
            // 
            // searchLookUpEditUser
            // 
            this.searchLookUpEditUser.ImeMode = System.Windows.Forms.ImeMode.Close;
            this.searchLookUpEditUser.Location = new System.Drawing.Point(597, 81);
            this.searchLookUpEditUser.Name = "searchLookUpEditUser";
            this.searchLookUpEditUser.Properties.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.searchLookUpEditUser.Properties.Appearance.Options.UseForeColor = true;
            this.searchLookUpEditUser.Properties.Appearance.Options.UseTextOptions = true;
            this.searchLookUpEditUser.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.searchLookUpEditUser.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.searchLookUpEditUser.Properties.AutoHeight = false;
            this.searchLookUpEditUser.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.searchLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEditUser.Properties.NullText = "";
            this.searchLookUpEditUser.Properties.PopupView = this.gridView1;
            this.searchLookUpEditUser.Size = new System.Drawing.Size(232, 33);
            this.searchLookUpEditUser.TabIndex = 12;
            this.searchLookUpEditUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchLookUpEditUser_KeyDown);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col用户编号,
            this.col拼音代码,
            this.col用户名,
            this.col用户密码,
            this.col角色名称,
            this.col科室名称,
            this.col单位编码,
            this.col是否禁用,
            this.colID});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // col用户编号
            // 
            this.col用户编号.FieldName = "用户编码";
            this.col用户编号.Name = "col用户编号";
            this.col用户编号.Visible = true;
            this.col用户编号.VisibleIndex = 0;
            // 
            // col拼音代码
            // 
            this.col拼音代码.FieldName = "拼音代码";
            this.col拼音代码.Name = "col拼音代码";
            this.col拼音代码.Visible = true;
            this.col拼音代码.VisibleIndex = 1;
            // 
            // col用户名
            // 
            this.col用户名.FieldName = "用户名";
            this.col用户名.Name = "col用户名";
            this.col用户名.Visible = true;
            this.col用户名.VisibleIndex = 2;
            // 
            // col用户密码
            // 
            this.col用户密码.FieldName = "用户密码";
            this.col用户密码.Name = "col用户密码";
            // 
            // col角色名称
            // 
            this.col角色名称.FieldName = "角色名称";
            this.col角色名称.Name = "col角色名称";
            this.col角色名称.Visible = true;
            this.col角色名称.VisibleIndex = 3;
            // 
            // col科室名称
            // 
            this.col科室名称.FieldName = "科室名称";
            this.col科室名称.Name = "col科室名称";
            this.col科室名称.Visible = true;
            this.col科室名称.VisibleIndex = 4;
            // 
            // col单位编码
            // 
            this.col单位编码.FieldName = "单位编码";
            this.col单位编码.Name = "col单位编码";
            // 
            // col是否禁用
            // 
            this.col是否禁用.FieldName = "是否禁用";
            this.col是否禁用.Name = "col是否禁用";
            this.col是否禁用.Visible = true;
            this.col是否禁用.VisibleIndex = 5;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // labelControl单位名称
            // 
            this.labelControl版本号.Appearance.Options.UseTextOptions = true;
            this.labelControl版本号.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl版本号.Location = new System.Drawing.Point(796, 277);
            this.labelControl版本号.Name = "labelControl单位名称";
            this.labelControl版本号.Size = new System.Drawing.Size(11, 14);
            this.labelControl版本号.TabIndex = 18;
            this.labelControl版本号.Text = "@";
            this.labelControl版本号.Click += new System.EventHandler(this.labelControl版本号_Click);
            // 
            // labelControl标题信息
            // 
            this.labelControl标题信息.Appearance.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl标题信息.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelControl标题信息.Appearance.Options.UseFont = true;
            this.labelControl标题信息.Appearance.Options.UseForeColor = true;
            this.labelControl标题信息.Appearance.Options.UseTextOptions = true;
            this.labelControl标题信息.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl标题信息.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl标题信息.Location = new System.Drawing.Point(634, 42);
            this.labelControl标题信息.Name = "labelControl标题信息";
            this.labelControl标题信息.Size = new System.Drawing.Size(152, 26);
            this.labelControl标题信息.TabIndex = 20;
            this.labelControl标题信息.Text = "医院管理信息平台";
            // 
            // checkEdit记住模块
            // 
            this.checkEdit记住模块.Location = new System.Drawing.Point(569, 193);
            this.checkEdit记住模块.Name = "checkEdit记住模块";
            this.checkEdit记住模块.Properties.Caption = "记住模块";
            this.checkEdit记住模块.Size = new System.Drawing.Size(75, 19);
            this.checkEdit记住模块.TabIndex = 21;
            // 
            // checkEdit记住密码
            // 
            this.checkEdit记住密码.Location = new System.Drawing.Point(778, 193);
            this.checkEdit记住密码.Name = "checkEdit记住密码";
            this.checkEdit记住密码.Properties.Caption = "记住密码";
            this.checkEdit记住密码.Size = new System.Drawing.Size(75, 19);
            this.checkEdit记住密码.TabIndex = 22;
            // 
            // FormLogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(940, 309);
            this.Controls.Add(this.checkEdit记住密码);
            this.Controls.Add(this.checkEdit记住模块);
            this.Controls.Add(this.labelControl标题信息);
            this.Controls.Add(this.labelControl版本号);
            this.Controls.Add(this.searchLookUpEditUser);
            this.Controls.Add(this.textEditMM);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.simpleButtonCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLogIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "医院管理信息平台";
            this.Load += new System.EventHandler(this.FormLogIn_Load);
            this.SizeChanged += new System.EventHandler(this.FormLogIn_New_SizeChanged);
            this.DoubleClick += new System.EventHandler(this.labelVer_DoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this.textEditMM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit记住模块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit记住密码.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraEditors.TextEdit textEditMM;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn col用户编号;
        private DevExpress.XtraGrid.Columns.GridColumn col拼音代码;
        private DevExpress.XtraGrid.Columns.GridColumn col用户名;
        private DevExpress.XtraGrid.Columns.GridColumn col用户密码;
        private DevExpress.XtraGrid.Columns.GridColumn col角色名称;
        private DevExpress.XtraGrid.Columns.GridColumn col科室名称;
        private DevExpress.XtraGrid.Columns.GridColumn col单位编码;
        private DevExpress.XtraGrid.Columns.GridColumn col是否禁用;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraEditors.LabelControl labelControl版本号;
        private DevExpress.XtraEditors.LabelControl labelControl标题信息;
        private CheckEdit checkEdit记住模块;
        private CheckEdit checkEdit记住密码;
    }
}

