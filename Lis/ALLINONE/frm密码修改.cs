﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ALLINONE
{
    public partial class frm密码修改 : DevExpress.XtraEditors.XtraForm
    {

        public frm密码修改()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if(txtMM1.Text.Trim()=="")
            {
                XtraMessageBox.Show("密码不允许为空，请重新输入！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (txtMM1.Text != txtMM2.Text)
            {
                XtraMessageBox.Show("两次录入不一致", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                int temp = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text,
                    "update pubUser set 用户密码='" + txtMM1.Text + "' where 单位编码=" + HIS.COMM.zdInfo.Model科室信息.单位编码 + " and 用户编码=" + HIS.COMM.zdInfo.ModelUserInfo.用户编码.ToString());
                if (temp == 1)
                {
                    WEISHENG.COMM.LogHelper.Info(this.Text, "密码修改成功", "用户" + HIS.COMM.zdInfo.ModelUserInfo.用户名 );
                    XtraMessageBox.Show("密码修改成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    WEISHENG.COMM.LogHelper.Info(this.Text, "密码修改保存失败", "用户" + HIS.COMM.zdInfo.ModelUserInfo.用户名);
                    XtraMessageBox.Show("密码修改保存失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }



        private void btnCanel_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("您确认取消本次密码修改操作吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }


    }
}