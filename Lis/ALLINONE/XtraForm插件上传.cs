﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using HIS.Model;
using WEISHENG.COMM.PluginsAttribute;

namespace ALLINONE
{
    public partial class XtraForm插件上传 : DevExpress.XtraEditors.XtraForm
    {
        OpenFileDialog op = new OpenFileDialog();
        string _s压缩包位置;
        string _s压缩包文件名;
        string _s插件包文件名;
        PluginAssemblyInfoAttribute _InfoMark = new PluginAssemblyInfoAttribute();
        plug_插件版本库 plug_item ;
        public XtraForm插件上传(plug_插件版本库 item)
        {
            InitializeComponent();
            plug_item = item;
            if (plug_item != null)
            {
                richTextBox1.Text = WEISHENG.COMM.stringHelper.toStringWithNewline<plug_插件版本库>(plug_item);
            }
        }

        private void simpleButton选择_Click(object sender, EventArgs e)
        {
            op.InitialDirectory = @"C:\Users\xx\Desktop\自动升级组件包";
            op.RestoreDirectory = true;
            op.Filter = "插件包(*.dll)|*.dll|所有文件(*.*)|*.*";
            if (op.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            textEdit插件位置.Text = op.SafeFileName;
            _s插件包文件名 = op.SafeFileName;
            string dllFileName = op.FileName;
            Assembly assembly = CacheManager.Instance.Load(dllFileName, CacheManager.enLoadType.LoadFile);
            _InfoMark = PlugInfoHelper.getAssemblyInfo(assembly);
            richTextBox1.Text = WEISHENG.COMM.stringHelper.toStringWithNewline<PluginAssemblyInfoAttribute>(_InfoMark);
        }

        private void simpleButton上传_Click(object sender, EventArgs e)
        {
            try
            {
                //if (string.IsNullOrEmpty(_InfoMark.插件名称))
                //{
                //    WEISHENG.COMM.Msg.ShowInformation("插件名称获取失败");
                //    return;
                //}
                //if (string.IsNullOrEmpty(_InfoMark.插件GUID))
                //{
                //    WEISHENG.COMM.Msg.ShowInformation("插件GUID获取失败");
                //    return;
                //}

                string strFileName = _s压缩包文件名;
                FileInfo myfile = new FileInfo(_s压缩包位置);
                FileStream mystream = myfile.OpenRead();
                byte[] mybyte = new byte[myfile.Length];
                mystream.Read(mybyte, 0, Convert.ToInt32(mystream.Length));
                if (comboBoxEdit插件包类型.Text == "升级工具")
                {
                    plug_item = new plug_插件版本库();
                    plug_item.插件名称 = "升级工具";
                    plug_item.版本号 = "";
                    plug_item.文件名 = textEdit压缩包位置.Text;
                    plug_item.版本说明 = "升级工具";
                    plug_item.插件包 = mybyte;
                    plug_item.createTime = DateTime.Now;
                    plug_item.插件GUID = "";
                    plug_item.插件包类型 = comboBoxEdit插件包类型.Text;
                    dataHelper.plugDB.plug_插件版本库.Attach(plug_item);
                    dataHelper.plugDB.Entry(plug_item).State = System.Data.Entity.EntityState.Added;
                }
                else
                {
                    if (plug_item == null)
                    {
                        plug_item = new HIS.Model.plug_插件版本库()
                        {
                            插件名称 = _InfoMark.插件名称,
                            版本号 = _InfoMark.插件版本,
                            文件名 = textEdit压缩包位置.Text,
                            版本说明 = _InfoMark.功能说明,
                            插件包 = mybyte,
                            createTime = DateTime.Now,
                            插件GUID = _InfoMark.插件GUID,
                            插件包类型 = comboBoxEdit插件包类型.Text
                        };
                        dataHelper.plugDB.plug_插件版本库.Attach(plug_item);
                        dataHelper.plugDB.Entry(plug_item).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        plug_item.插件名称 = _InfoMark.插件名称;
                        plug_item.版本号 = _InfoMark.插件版本;
                        plug_item.文件名 = textEdit压缩包位置.Text;
                        plug_item.版本说明 = _InfoMark.功能说明;
                        plug_item.插件包 = mybyte;
                        plug_item.createTime = DateTime.Now;
                        plug_item.插件GUID = _InfoMark.插件GUID;
                        plug_item.插件包类型 = comboBoxEdit插件包类型.Text;
                        dataHelper.plugDB.plug_插件版本库.Attach(plug_item);
                        dataHelper.plugDB.Entry(plug_item).State = System.Data.Entity.EntityState.Modified;
                    }
                }
                
                
                var type = dataHelper.plugDB.SaveChanges();
                MessageBox.Show("上传插件文件成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void simpleButton选择压缩文件_Click(object sender, EventArgs e)
        {
            op.InitialDirectory = @"C:\Users\xx\Desktop\自动升级组件包";
            op.RestoreDirectory = true;
            op.Filter = "压缩包(*.zip)|*.zip|所有文件(*.*)|*.*";
            if (op.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            textEdit压缩包位置.Text = op.SafeFileName;
            _s压缩包位置 = op.FileName;
            _s压缩包文件名 = op.SafeFileName;
        }

        private void XtraForm插件上传_Load(object sender, EventArgs e)
        {

        }
    }
}