﻿using DevExpress.Data;
using HIS.Model;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WEISHENG.COMM.PluginsAttribute;

namespace HIS.COMM
{
    
    public partial class XtraForm标准规范 : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public XtraForm标准规范()
        {
            InitializeComponent();
        }

        private void simpleButton关闭_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void XtraForm药品报销比例_Load(object sender, EventArgs e)
        {
            //HIS.Model.CommData.chis.yb_bl医疗服务设施.ToList();
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            switch (comboBoxEdit数据类型.Text)
            {
                case "ICD-10":
                    gridControl1.DataSource = chis.emr_pub_icd10.Select(c => new {c.ID, c.ICD10, c.ICD10Name, c.ICD11_章节_flag, c.ICD11_flag })
                        .Where(c => c.ICD11_章节_flag == null).ToList();
                    gridView1.PopulateColumns();
                    gridView1.Columns["ICD10"].Caption = "ICD10编码";
                    gridView1.Columns["ICD10Name"].Caption = "疾病名称";
                    gridView1.BestFitColumns();
                    break;

                case "ICD-11":
                    gridControl1.DataSource = chis.emr_pub_icd10.Select(c => new {c.ID, c.ICD10, c.ICD10Name, c.ICD11_章节_flag, c.ICD11_flag })
                        .Where(c => c.ICD11_flag == true).ToList();
                    gridView1.PopulateColumns();
                    gridView1.Columns["ICD10"].Caption = "ICD11编码";
                    gridView1.Columns["ICD10Name"].Caption = "疾病名称";
                    gridView1.BestFitColumns();
                    break;

                case "《山东省住院病案首页2018年修订版》数据字典":
                    gridControl1.DataSource = chis.emr数据字典.ToList();
                    gridView1.PopulateColumns();
                    gridView1.BestFitColumns();
                    break;

                case "医保数据字典":
                    gridControl1.DataSource = chis.yb_数据字典.ToList();
                    gridView1.PopulateColumns();
                    gridView1.BestFitColumns();
                    break;

                case "医保医疗服务设施目录":
                    gridControl1.DataSource = chis.yb_bl医疗服务设施.ToList();
                    gridView1.PopulateColumns();
                    gridView1.BestFitColumns();
                    break;

                case "医保诊疗项目目录":
                    gridControl1.DataSource = chis.yb_bl诊疗项目比例.ToList();
                    gridView1.PopulateColumns();
                    gridView1.BestFitColumns();
                    break;

                case "医保药品目录":
                    gridControl1.DataSource = chis.yb_bl药品比例2020.ToList();
                    gridView1.PopulateColumns();
                    gridView1.BestFitColumns();
                    break;

                default:
                    break;
            }

            for (int i = 0; i < gridView1.Columns.Count; i++)
            {
                if (gridView1.Columns[i].FieldName == "IsChanged" || gridView1.Columns[i].FieldName == "createTime" )
                {
                    gridView1.Columns[i].Visible = false;

                }

                if (gridView1.Columns[i].FieldName == "ID")
                {
                    gridView1.Columns[i].VisibleIndex = 0;
                    gridView1.Columns[i].SummaryItem.FieldName = "ID";
                    gridView1.Columns[i].SummaryItem.SummaryType = SummaryItemType.Count;
                }
            }

        }

        private void simpleButton导出_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "导出Excel";
            saveFileDialog.Filter = "Excel文件(*.xls)|*.xls";
            DialogResult dialogResult = saveFileDialog.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions();
                gridControl1.ExportToXls(saveFileDialog.FileName);
                DevExpress.XtraEditors.XtraMessageBox.Show("保存成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}