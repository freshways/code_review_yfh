﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HIS.Model;
using HIS.Model.Helper;
using WEISHENG.COMM;

namespace HIS.COMM
{
    public partial class XtraForm医嘱项目扩展信息 : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        string s项目类型;
        int? i项目编码;
        public XtraForm医嘱项目扩展信息(string _s项目类型, int? _i项目编码)
        {
            s项目类型 = _s项目类型;
            i项目编码 = _i项目编码;
            InitializeComponent();
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void XtraForm医嘱项目扩展信息_Load(object sender, EventArgs e)
        {
            string _s医保信息 = "";
            string _s药品信息 = "";
            switch (s项目类型)
            {
                case "药品项目":
                    var dz = chis.yb_药品对照.Where(c => c.药品序号 == i项目编码.ToString()).FirstOrDefault();
                    if (dz == null)
                    {
                        _s医保信息 = "获取药品对照信息失败";
                    }
                    else
                    {
                        var ybxx = chis.yb_bl药品比例.Where(c => c.药品编码 == dz.医保编码).FirstOrDefault();
                        if (ybxx != null)
                        {
                            _s医保信息 = stringHelper.toStringWithNewline(ybxx);
                        }
                        else
                        {
                            _s医保信息 = "获取医保比例信息失败";
                        }

                    }
                    break;
                case "医技项目":
                    var dzyj = chis.yb_诊疗对照.Where(c => c.诊疗编码 == "A" + i项目编码.ToString()).FirstOrDefault();
                    if (dzyj == null)
                    {
                        _s医保信息 = "获取医技项目对照信息失败";
                    }
                    else
                    {
                        var ybxx = chis.yb_bl诊疗项目比例.Where(c => c.诊疗项目编码 == dzyj.医保编码).FirstOrDefault();
                        if (ybxx != null)
                        {
                            _s医保信息 = stringHelper.toStringWithNewline(ybxx);
                        }
                        else
                        {
                            _s医保信息 = "获取医保比例信息失败";
                        }

                    }
                    break;
            }

            richEditControl扩展信息.Text = string.Format("{0}\r\n{1} \r\n{2}\r\n【暂无】",
                stringHelper.MakePrettyLine("医保报销信息"),
                _s医保信息,
                stringHelper.MakePrettyLine("其他相关信息"));
            ;
            //WEISHENG.COMM.stringHelper.toStringWithNewline(tt);
        }
    }
}