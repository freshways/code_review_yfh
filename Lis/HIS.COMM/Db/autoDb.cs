﻿using System;
using System.Windows.Forms;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace HIS.COMMON.Db
{
        
    /// <summary>
    /// 通用数据维护，将更新自动传回数据库
    /// </summary>
    public class autoDb
    {
        /// <summary>
        /// 构造一棵树型表结构
        /// </summary>
        /// <returns></returns>
        private DataTable CreateTreeListTable()
        {

            DataTable dt = new DataTable();

            DataColumn dcOID = new DataColumn("KeyFieldName", Type.GetType("System.Int32"));

            DataColumn dcParentOID = new DataColumn("ParentFieldName", Type.GetType("System.Int32"));

            DataColumn dcNodeName = new DataColumn("NodeName", Type.GetType("System.String"));

            DataColumn dcNodeCode = new DataColumn("NodeCode", Type.GetType("System.String"));

            DataColumn dcOthers = new DataColumn("Others", Type.GetType("System.String"));

            dt.Columns.Add(dcOID);

            dt.Columns.Add(dcParentOID);

            dt.Columns.Add(dcNodeName);

            dt.Columns.Add(dcNodeCode);

            dt.Columns.Add(dcOthers);

            //以上代码完成了DataTable的构架，但是里面是没有任何数据的

            DataRow dr1 = dt.NewRow();

            dr1["KeyFieldName"] = 1;

            dr1["ParentFieldName"] = 0;// DBNull.Value;

            dr1["NodeName"] = "根节1";

            dr1["NodeCode"] = "根节点编码";

            dr1["Others"] = "其他";

            dt.Rows.Add(dr1);

            DataRow dr2 = dt.NewRow();

            dr2["KeyFieldName"] = 2;

            dr2["ParentFieldName"] = 1;

            dr2["NodeName"] = "节点子节点名称";

            dr2["NodeCode"] = "节点子节点编码";

            dr2["Others"] = "其他";

            dt.Rows.Add(dr2);

            DataRow dr3 = dt.NewRow();

            dr3["KeyFieldName"] = 3;

            dr3["ParentFieldName"] = 1;

            dr3["NodeName"] = "节点子节点名称";

            dr3["NodeCode"] = "节点子节点编码";

            dr3["Others"] = "其他";

            dt.Rows.Add(dr3);

            DataRow dr4 = dt.NewRow();

            dr4["KeyFieldName"] = 4;

            dr4["ParentFieldName"] = 0;// DBNull.Value;

            dr4["NodeName"] = "根节2";

            dr4["NodeCode"] = "根节点编码";

            dr4["Others"] = "其他";

            dt.Rows.Add(dr4);

            DataRow dr5 = dt.NewRow();

            dr5["KeyFieldName"] = 5;

            dr5["ParentFieldName"] = 4;

            dr5["NodeName"] = "节点子节点名称";

            dr5["NodeCode"] = "节点子节点编码";

            dr5["Others"] = "其他";

            dt.Rows.Add(dr5);

            DataRow dr6 = dt.NewRow();

            dr6["KeyFieldName"] = 6;

            dr6["ParentFieldName"] = 5;

            dr6["NodeName"] = "节点子节点名称";

            dr6["NodeCode"] = "节点子节点编码";

            dr6["Others"] = "其他";

            dt.Rows.Add(dr6);

            return dt;

        }
        /// <summary>
        /// 执行DataTable中的查询返回新的DataTable
        /// </summary>
        /// <param name="dt">源数据DataTable</param>
        /// <param name="condition">查询条件</param>
        /// <returns></returns>
        public static DataTable GetNewDataTable(DataTable dt, string condition)
        {
            DataTable newdt = new DataTable();
            newdt = dt.Clone();
            DataRow[] dr = dt.Select(condition);
            for (int i = 0; i < dr.Length; i++)
            {
                newdt.ImportRow((DataRow)dr[i]);
            }
            return newdt;//返回的查询结果
        }

        //根据传入的数据返回指定表名称的ds
        public static string ConString = "";

        public static DataSet dsGetData(string sqlstr,string tablename,string ConString)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(ConString))
                {

                    SqlDataAdapter da = new SqlDataAdapter(sqlstr, con);
                    da.Fill(ds, tablename);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
                return ds;
        }



        //根据传入的ds和sql语句保存数据改动，要求表中必须包括主键
       public static void dsUpData(DataSet ds,string table,string sqlstr,string ConString)
        {
            System.Data.SqlClient.SqlConnection sqlcon = new System.Data.SqlClient.SqlConnection(ConString);
            System.Data.SqlClient.SqlTransaction sqltran;
            sqlcon.Open();
            //将事务绑定到连接对像
            sqltran = sqlcon.BeginTransaction();
            try
            {
                DataSetUpdate(ds, sqlcon,table,sqlstr,sqltran);
                sqltran.Commit();
             //   MessageBox.Show("数据保存成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Data.SqlClient.SqlException sqlex)
            {
                sqltran.Rollback();
                MessageBox.Show(sqlex.Message);
            }
            catch (Exception ex)
            {
                sqltran.Rollback();
                MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlcon.Close();
            }
        }

        private static void DataSetUpdate(DataSet ds, SqlConnection sqlconnect, string tablename, string sqlstr, SqlTransaction sqltrans)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sqlstr, sqlconnect);
                SqlCommandBuilder cmdBuilder = new SqlCommandBuilder(adapter);
                //创建SqlDataAdapter 对像的Command对像，并将连接对像及事务对像绑定到Command对像上
                adapter.DeleteCommand = new SqlCommand("", sqlconnect, sqltrans);
                adapter.InsertCommand = new SqlCommand("", sqlconnect, sqltrans);
                adapter.UpdateCommand = new SqlCommand("", sqlconnect, sqltrans);
                adapter.SelectCommand = new SqlCommand(sqlstr, sqlconnect, sqltrans);
                //使用GetDeleteCommand将相对应的SQLCOMMAND对像传入
                adapter.DeleteCommand = cmdBuilder.GetDeleteCommand();
                adapter.InsertCommand = cmdBuilder.GetInsertCommand();
                adapter.UpdateCommand = cmdBuilder.GetUpdateCommand();
                //cmdBuilder.RefreshSchema();
                int val = adapter.Update(ds, tablename);
                ds.Tables[tablename].AcceptChanges();
            }
            catch
            {
                throw;
            }
        }


//        01.using (OracleConnection con = new OracleConnection(Properties.Settings.Default.ConnectionString))  
//02.            {  
//03.                con.Open();  
//04.                using (OracleCommand com = con.CreateCommand())  
//05.                {  
//06.                    com.CommandText = "select * from salgrade";  
//07.                    OrclDataSet ds = new OrclDataSet();  
//08.                      
//09.                    OracleDataAdapter adapter = new OracleDataAdapter(com);  
//10.                    <STRONG>OracleCommandBuilder builder = new OracleCommandBuilder(adapter);</STRONG>  
//11.                    adapter.Fill(ds,"salgrade");  
//12.                    DataTable table = ds.Tables["salgrade"];  
//13.                    //修改   
//14.                    //DataRow dr = table.Rows.Find("7777");   
//15.                    //dr.BeginEdit();   
//16.                    //dr["id"] = 626;   
//17.                    //dr["grade"] = 636;   
//18.                    //dr["test"] = "646";   
//19.                    //dr.EndEdit();   
//20.                    //增加   
//21.                    //DataRow dr1 = table.NewRow();   
//22.                    //dr1["id"] = 7777;   
//23.                    //dr1["grade"] = 7777;   
//24.                    //dr1["test"] = "77777";   
//25.                    //table.Rows.Add(dr1);   
//26.                    //删除   
//27.                    //DataRow dr2 = table.NewRow();   
//28.                    //dr2["id"] = 7777;   
//29.                    //table.Rows.Remove(dr2);   
//30.                      
//31.                    adapter.Update(table);  
//32.  
//33.                }  
//34.            }
        
        
        
        /// <summary>
          /// 比较两个DataTable数据（结构相同）mail：yx_007@163.com
          /// </summary>
          /// <param name="dtDest">来自数据库的DataTable</param>
          /// <param name="dtSrc">来自文件的DataTable</param>
          /// <param name="dtRetAdd">新增数据（dt2中的数据）</param>
          /// <param name="dtRetDel">删除的数据（dt2中的数据）</param>
          /// <param name="keyFields">关键字段名</param>
          public static void CompareDt(DataTable dtSrc, DataTable dtDest, out DataTable dtRetAdd, out DataTable dtRetDel, string keyFields)
         {
             dtRetDel = dtSrc.Clone();
             dtRetAdd = dtRetDel.Clone();
             StringBuilder sSrc = new StringBuilder();
             StringBuilder sDest = new StringBuilder();
             string[] sFields = keyFields.Split(',');//列名数组
             int iSrcCount = dtSrc.Rows.Count;
             int iDestCount = dtDest.Rows.Count;
             int iSrc = 0;
             int iDest = 0;
             int result = 0;
             bool isRun = true;
             dtSrc.Select("", keyFields);
             dtDest.Select("", keyFields);
             while (isRun)
             {
                 if (iSrcCount == 0)//
                 {
                     dtRetDel = dtDest;
                     isRun = false;
                     continue;
                 }
                 if (iDestCount == 0)
                 {
                     dtRetAdd = dtSrc;
                     isRun = false;
                     continue;
                 }
                 sSrc.Length = 0;
                 sDest.Length = 0;
                 for (result = 0; result < sFields.Length; result++)
                 {
                     sSrc.Append(dtSrc.Rows[iSrc][sFields[result]]).Append(",");
                     sDest.Append(dtDest.Rows[iDest][sFields[result]]).Append(",");
                 }
                 result = string.Compare(sSrc.ToString(), sDest.ToString(), true);
                 switch (result)
                 {
                    ////src表小则新增src表
                    case -1:
                        dtRetAdd.Rows.Add(dtSrc.Rows[iSrc].ItemArray);
                        iSrc++;
                        break;
                    ////相同同时向下移动
                    case 0:
                        iSrc++;
                        iDest++;
                        break;
                    ////src表大则删除dest表
                    case 1:
                        dtRetDel.Rows.Add(dtDest.Rows[iDest].ItemArray);
                        iDest++;
                        break;
                }
                 //检查是否已经移动到最后一条
                 if ((iSrc == iSrcCount - 1) && (iDest < iDestCount - 1))
                 {
                     for (result = iDest; result < iDestCount; result++)
                     {
                         dtRetDel.Rows.Add(dtDest.Rows[result].ItemArray);
                     }
                     isRun = false;
                     continue;
                 }
                 if ((iSrc < iSrcCount - 1) && (iDest == iDestCount - 1))
                 {
                     for (result = iSrc; result < iSrcCount; result++)
                     {
                         dtRetAdd.Rows.Add(dtSrc.Rows[result].ItemArray);
                     }
                     isRun = false;
                     continue;
                 }
                 if ((iSrc == iSrcCount - 1) && (iDest == iDestCount - 1))
                 {
                     isRun = false;
                     continue;
                 }
             }
         }
 
   }
}
