﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;


namespace HIS.COMMON.Db
{
  public  class oraHelper
    {
        public static DataTable GetInfo(string sql,string ConnectionString)
        {
            DataTable table = null;
           
            OracleConnection conn = new OracleConnection(ConnectionString);
            try
            {
                conn.Open();//打开连接
                OracleCommand cmd = conn.CreateCommand();
                //cmd.CommandText=sql;//在这儿写sql语句
                //OracleDataReader odr=cmd.ExecuteReader();//创建一个OracleDateReader对象
                //创建DataAdapter对象
                OracleDataAdapter da = new OracleDataAdapter(sql, conn);
                //封存数据
                DataSet ds = new DataSet();
                da.Fill(ds);
                table = ds.Tables[0];
                //释放DataAdapter
                da.Dispose();
             
            }
            catch (Exception ex)
            {
                //如果有错误，输出错误信息
            }
            finally
            {
                conn.Close(); //关闭连接
            }
            return table;

        }
    }
}



//        [转帖]使用asp.net访问Oracle的方法汇总
//方法一：通过System.Data.OracleClient(需要安装Oracle客户端并配置tnsnames.ora)

//string strcnn = "User ID=lportal;Password=lportal;Data Source=zhbrserverORCL;";

//System.Data.OracleClient.OracleConnection cnn = new System.Data.OracleClient.OracleConnection(strcnn);

//cnn.Open();

//MessageBox.Show(cnn.State.ToString());

//cnn.Close();

//方法二：通过System.Data.OracleClient(需要安装Oracle客户端不需配置tnsnames.ora)

//string strcnn = "User ID=lportal;Password=lportal;Data Source=(DESCRIPTION = (ADDRESS_LIST= (ADDRESS = (PROTOCOL = TCP)(HOST = zhbrserver)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = orcl)))";

//System.Data.OracleClient.OracleConnection cnn = new System.Data.OracleClient.OracleConnection(strcnn);

//cnn.Open();

//MessageBox.Show(cnn.State.ToString());

//cnn.Close();

//方法三：通过System.Data.OleDb和Oracle公司的驱动

//string strcnn = "Provider=OraOLEDB.Oracle.1;User ID=lportal;Password=lportal;Data Source=(DESCRIPTION = (ADDRESS_LIST= (ADDRESS = (PROTOCOL = TCP)(HOST = zhbrserver)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = orcl)))";

//System.Data.OleDb.OleDbConnection cnn = new System.Data.OleDb.OleDbConnection(strcnn);

//cnn.Open();

//MessageBox.Show(cnn.State.ToString());

//cnn.Close();

//方法四：通过System.Data.OleDb和微软公司的Oracle驱动

//string strcnn = 
//    "Provider=MSDAORA.1;User ID=system;Password=orcl;Data Source=(DESCRIPTION = (ADDRESS_LIST= (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.10.119)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = orcl)))";

//System.Data.OleDb.OleDbConnection cnn = new System.Data.OleDb.OleDbConnection(strcnn);

//cnn.Open();

//MessageBox.Show(cnn.State.ToString());

//cnn.Close();

//备注：

//1、   XP操作系统已经安装了微软公司的Oracle驱动C:"Program Files"Common Files"System"Ole DB"msdaora.dll

//2、   该驱动需要Oracle客户端的三个文件(oraocixe10.dll、oci.dll、ociw32.dll)方在System32下即可
