﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WEISHENG.COMM
{
    public static class zdInfo2单例模式用不到
    {
        public static zdInfo2站点实例 zd工作信息;

        public static void init()
        {
            zd工作信息 = zdInfo2站点实例.GetInstance();//调用单例模式，在第一次引用时触发，当然，静态类的静态变量不存在实例化的问题。
            zd工作信息.HardInfo = new Computer();
            zd工作信息.Model站点信息 = new HIS.Model.GY站点设置() { 站点名称 = "test" };
            //zdInfo2 test = zdInfo2.GetInstance();
            //zdInfo2 zd = zdInfo2.GetInstance();
            //zd.HardInfo.CpuID = "0000";
        }
    }

    public  class Class工作信息_非静态
    {
        public  zdInfo2站点实例 zd工作信息 = zdInfo2站点实例.GetInstance();//调用单例模式，在第一次引用时触发
        public  zdInfo2站点实例 zd工作信息_;
        public static void init()
        {
            zdInfo2站点实例 zd = zdInfo2站点实例.GetInstance();
            //string s9 = zd.Model站点信息.站点名称;
            //zd工作信息.HardInfo = new Computer();
            //zd工作信息.Model站点信息 = new Model.GY站点设置Model() { 站点名称 = "test" };
            //zdInfo2 test = zdInfo2.GetInstance();
            //zdInfo2 zd = zdInfo2.GetInstance();
            //zd.HardInfo.CpuID = "0000";
        }
    }

}
