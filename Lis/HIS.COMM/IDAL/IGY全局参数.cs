﻿// litao@Copy Right 2006-2008
// 文件： GY全局参数.cs
// 项目名称：项目管理 
// 创建时间：2018-05-26
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.COMM.Model;

namespace HIS.COMM.IDAL
{
    /// <summary>
    /// 数据层 dbo.GY全局参数 接口。

    /// </summary>
    public interface IGY全局参数
    {
		#region 基本方法
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="gy全局参数">GY全局参数实体</param>
		/// <returns>新插入记录的编号</returns>
		int Insert(GY全局参数Model gy全局参数);
				
		/// <summary>
		/// 向数据表GY全局参数更新一条记录。

		/// </summary>
		/// <param name="gy全局参数">gy全局参数</param>
		/// <returns>影响的行数</returns>
		int Update(GY全局参数Model gy全局参数);
		
		
		/// <summary>
		/// 删除数据表GY全局参数中的一条记录

		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		int Delete(int id);

		
        /// <summary>
		/// 得到 gy全局参数 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>gy全局参数 数据实体</returns>
		GY全局参数Model GetGY全局参数(int id);
		
		
		/// <summary>
		/// 得到数据表GY全局参数所有记录

		/// </summary>
		/// <returns>数据实体</returns>
		IList<GY全局参数Model> GetGY全局参数All();
			
		/// <summary>
		/// 得到数据表GY全局参数符合查询条件的记录

		/// </summary>
		/// <param name="sqlWhere">查询条件</param>
		/// <returns>数据实体</returns>
		IList<GY全局参数Model> GetGY全局参数All(string sqlWhere);
			
		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		bool IsExist(int id);

        #endregion
    }
}

