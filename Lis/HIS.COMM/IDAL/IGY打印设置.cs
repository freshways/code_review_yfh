﻿// litao@Copy Right 2006-2008
// 文件： GY打印设置.cs
// 项目名称：项目管理 
// 创建时间：2018-05-26
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.COMM.Model;

namespace HIS.COMM.IDAL
{
    /// <summary>
    /// 数据层 dbo.GY打印设置 接口。

    /// </summary>
    public interface IGY打印设置
    {
		#region 基本方法
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="gy打印设置">GY打印设置实体</param>
		/// <returns>新插入记录的编号</returns>
		int Insert(GY打印设置Model gy打印设置);
				
		/// <summary>
		/// 向数据表GY打印设置更新一条记录。

		/// </summary>
		/// <param name="gy打印设置">gy打印设置</param>
		/// <returns>影响的行数</returns>
		int Update(GY打印设置Model gy打印设置);
		
		
		/// <summary>
		/// 删除数据表GY打印设置中的一条记录

		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		int Delete(int id);

		
        /// <summary>
		/// 得到 gy打印设置 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>gy打印设置 数据实体</returns>
		GY打印设置Model GetGY打印设置(int id);
		
		
		/// <summary>
		/// 得到数据表GY打印设置所有记录

		/// </summary>
		/// <returns>数据实体</returns>
		IList<GY打印设置Model> GetGY打印设置All();
			
		/// <summary>
		/// 得到数据表GY打印设置符合查询条件的记录

		/// </summary>
		/// <param name="sqlWhere">查询条件</param>
		/// <returns>数据实体</returns>
		IList<GY打印设置Model> GetGY打印设置All(string sqlWhere);
			
		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		bool IsExist(int id);

        #endregion
    }
}

