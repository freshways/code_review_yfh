﻿// litao@Copy Right 2006-2008
// 文件： GY收费小项.cs
// 项目名称：项目管理 
// 创建时间：2018-05-26
// 
// ===================================================================
using System;
using System.Collections.Generic;
using HIS.COMM.Model;

namespace HIS.COMM.IDAL
{
    /// <summary>
    /// 数据层 dbo.GY收费小项 接口。

    /// </summary>
    public interface IGY收费小项
    {
		#region 基本方法
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="gy收费小项">GY收费小项实体</param>
		/// <returns>新插入记录的编号</returns>
		int Insert(GY收费小项Model gy收费小项);
				
		/// <summary>
		/// 向数据表GY收费小项更新一条记录。

		/// </summary>
		/// <param name="gy收费小项">gy收费小项</param>
		/// <returns>影响的行数</returns>
		int Update(GY收费小项Model gy收费小项);
		
		
		/// <summary>
		/// 删除数据表GY收费小项中的一条记录

		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		int Delete(int id);

		
        /// <summary>
		/// 得到 gy收费小项 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>gy收费小项 数据实体</returns>
		GY收费小项Model GetGY收费小项(int id);
		
		
		/// <summary>
		/// 得到数据表GY收费小项所有记录

		/// </summary>
		/// <returns>数据实体</returns>
		IList<GY收费小项Model> GetGY收费小项All();
			
		/// <summary>
		/// 得到数据表GY收费小项符合查询条件的记录

		/// </summary>
		/// <param name="sqlWhere">查询条件</param>
		/// <returns>数据实体</returns>
		IList<GY收费小项Model> GetGY收费小项All(string sqlWhere);
			
		
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		bool IsExist(int id);

        #endregion
    }
}

