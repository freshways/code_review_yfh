﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace HIS.COMM.SmartAlert
{
    public class Class医嘱执行提示
    {
        public static void getInfo()
        {
            StringBuilder showText = new StringBuilder();
            StringBuilder showTextTemp = new StringBuilder();
            Boolean b显示医嘱提示 = false;

            DataTable dt本科室病人 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text,
                        " select aa.入院时间,bb.科室名称 病区,aa.病床,aa.病人姓名,aa.住院号码,ZYID " +
                        "from ZY病人信息 aa left outer join gy科室设置 bb on aa.病区=bb.科室编码 where bb.科室编码 in (" +
                        HIS.COMM.zdInfo.s科室数据权限 + ") and aa.已出院标记=0 " + " and aa.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码).Tables[0];

            foreach (DataRow dr in dt本科室病人.Rows)
            {
                string currZYID = "";
                string currName = "";
                string currBc = "";//病床
                string currBq = "";
                string currZYH = "";
                Boolean b是否当日入院 = false;//是否当日入院
                Boolean b发现未执行医嘱 = false;
                currZYID = dr["zyid"].ToString();
                currName = dr["病人姓名"].ToString();
                currBq = dr["病区"].ToString();
                currZYH = dr["住院号码"].ToString();
                currBc = dr["病床"].ToString();

                if (Convert.ToDateTime(dr["入院时间"].ToString()).ToShortDateString() == DateTime.Now.ToShortDateString())
                {
                    b是否当日入院 = true;
                }
                else
                {
                    b是否当日入院 = false;
                }
                string SQL_HL执行医嘱 = string.Format(
                    "SELECT   ID, ZYID, 停嘱医生编码, 停嘱执行者编码, 停嘱时间, 停嘱核对者编码, 剂量单位," + "\r\n" +
                    "         剂量数量, 单次数量, 医嘱类型, 单日总量, 开嘱医生编码, 开嘱执行者编码, 开嘱时间," + "\r\n" +
                    "         开嘱核对者编码, 执行时间, 用法名称, 组别, 项目名称, 项目类型, 项目编码," + "\r\n" +
                    "         频次名称, 频次数量, 库存数量, 药房规格, 药房单位, 药房编码, 财务分类,0 选择,0.00 执行数量" + "\r\n" +
                    "FROM     YS住院医嘱" + "\r\n" +
                    "WHERE    (ZYID = {0}) AND (项目编码 > 0)" + "\r\n" +
                    "ORDER BY 组别, 开嘱时间", currZYID);


                DataTable HL执行医嘱 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb,
                    CommandType.Text, SQL_HL执行医嘱).Tables[0];

                for (int i = 0; i < HL执行医嘱.Rows.Count; i++)
                {
                    string s医嘱类型 = HL执行医嘱.Rows[i]["医嘱类型"].ToString().Trim();
                    string s执行时间 = HL执行医嘱.Rows[i]["执行时间"].ToString();
                    string s停嘱时间 = HL执行医嘱.Rows[i]["停嘱时间"].ToString();

                    if (s医嘱类型 == "临时医嘱")
                    {
                        if (s停嘱时间 != "")
                        {
                            HL执行医嘱.Rows[i]["选择"] = false;
                        }
                        else
                        {
                            if (s执行时间 != "")
                            {
                                HL执行医嘱.Rows[i]["选择"] = false;
                            }
                            else
                            {
                                HL执行医嘱.Rows[i]["选择"] = true;
                                HL执行医嘱.Rows[i]["执行数量"] = HL执行医嘱.Rows[i]["单日总量"];
                            }
                        }
                    }

                    if (s医嘱类型 == "长期医嘱")
                    {
                        if (s停嘱时间 != "")
                        {
                            HL执行医嘱.Rows[i]["选择"] = false;

                        }
                        else
                        {

                            if (s执行时间 == "")
                            {
                                HL执行医嘱.Rows[i]["选择"] = true;
                                HL执行医嘱.Rows[i]["执行数量"] = HL执行医嘱.Rows[i]["单日总量"];
                            }
                            else if (Convert.ToDateTime(s执行时间).Date >= DateTime.Now.Date)
                            {
                                HL执行医嘱.Rows[i]["选择"] = false;
                            }
                            else
                            {
                                HL执行医嘱.Rows[i]["选择"] = true;
                                HL执行医嘱.Rows[i]["执行数量"] = HL执行医嘱.Rows[i]["单日总量"];
                            }
                        }

                    }
                }

                string iLsCount = HL执行医嘱.Compute("count(医嘱类型)", "医嘱类型='临时医嘱' and 选择=true").ToString();
                if (iLsCount != "0")
                {
                    showTextTemp.Append("临时医嘱" + iLsCount + "条未执行；\r\n");
                    b发现未执行医嘱 = true;
                }
                //只有当日入院病人提示长期医嘱
                string iCqCount = HL执行医嘱.Compute("count(医嘱类型)", "医嘱类型='长期医嘱' and 选择=true").ToString();
                if (iCqCount != "0")
                {
                    if (b是否当日入院)
                    {
                        showTextTemp.Append("长期医嘱" + iCqCount + "条未执行");
                        b发现未执行医嘱 = true;
                    }
                }
                if (b发现未执行医嘱)
                {
                    string ryString = b是否当日入院 == true ? "当日入院" : "";
                    showText.Append("\r\n" + ryString + "【" + currBq + "|" + currBc + "】" + currZYH + currName + ":" + showTextTemp.ToString());
                    b显示医嘱提示 = true;
                }
                showTextTemp.Clear();
                b发现未执行医嘱 = false;
            }
            if (b显示医嘱提示)
            {
                HIS.COMM.SmartAlert.AlertInfo.context = new COMM.SmartAlert.alertContext()
                {
                    Info = showText.ToString(),
                    Title = "执行医嘱提示"
                };

            }
        }
    }
}
