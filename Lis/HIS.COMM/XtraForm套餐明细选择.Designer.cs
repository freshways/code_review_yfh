﻿namespace HIS.COMM
{
    partial class XtraForm套餐明细选择
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm套餐明细选择));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit费用数量 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton确定 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.searchLookUpEdit1 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col收费编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col收费名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col单位 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col单价 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col归并名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col拼音代码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col是否禁用 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col单位编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col归并编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col新合编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit费用数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit费用数量);
            this.layoutControl1.Controls.Add(this.simpleButton确定);
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(526, 274, 250, 350);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(478, 120);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit费用数量
            // 
            this.textEdit费用数量.EditValue = "1";
            this.textEdit费用数量.Location = new System.Drawing.Point(43, 62);
            this.textEdit费用数量.Name = "textEdit费用数量";
            this.textEdit费用数量.Size = new System.Drawing.Size(423, 20);
            this.textEdit费用数量.StyleController = this.layoutControl1;
            this.textEdit费用数量.TabIndex = 7;
            // 
            // simpleButton确定
            // 
            this.simpleButton确定.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton确定.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton确定.Image")));
            this.simpleButton确定.Location = new System.Drawing.Point(273, 86);
            this.simpleButton确定.Name = "simpleButton确定";
            this.simpleButton确定.Size = new System.Drawing.Size(94, 22);
            this.simpleButton确定.StyleController = this.layoutControl1;
            this.simpleButton确定.TabIndex = 6;
            this.simpleButton确定.Text = "确定";
            this.simpleButton确定.Click += new System.EventHandler(this.simpleButton确定_Click);
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton取消.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton取消.Image")));
            this.simpleButton取消.Location = new System.Drawing.Point(371, 86);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(95, 22);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 5;
            this.simpleButton取消.Text = "取消";
            // 
            // searchLookUpEdit1
            // 
            this.searchLookUpEdit1.Location = new System.Drawing.Point(43, 38);
            this.searchLookUpEdit1.Name = "searchLookUpEdit1";
            this.searchLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit1.Properties.NullText = "请选择...";
            this.searchLookUpEdit1.Properties.View = this.searchLookUpEdit1View;
            this.searchLookUpEdit1.Size = new System.Drawing.Size(423, 20);
            this.searchLookUpEdit1.StyleController = this.layoutControl1;
            this.searchLookUpEdit1.TabIndex = 4;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.col收费编码,
            this.col收费名称,
            this.col单位,
            this.col单价,
            this.col归并名称,
            this.col拼音代码,
            this.col是否禁用,
            this.col单位编码,
            this.col归并编码,
            this.col新合编码});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // col收费编码
            // 
            this.col收费编码.FieldName = "收费编码";
            this.col收费编码.Name = "col收费编码";
            this.col收费编码.Visible = true;
            this.col收费编码.VisibleIndex = 0;
            // 
            // col收费名称
            // 
            this.col收费名称.FieldName = "收费名称";
            this.col收费名称.Name = "col收费名称";
            this.col收费名称.Visible = true;
            this.col收费名称.VisibleIndex = 1;
            // 
            // col单位
            // 
            this.col单位.FieldName = "单位";
            this.col单位.Name = "col单位";
            this.col单位.Visible = true;
            this.col单位.VisibleIndex = 2;
            // 
            // col单价
            // 
            this.col单价.FieldName = "单价";
            this.col单价.Name = "col单价";
            this.col单价.Visible = true;
            this.col单价.VisibleIndex = 3;
            // 
            // col归并名称
            // 
            this.col归并名称.FieldName = "归并名称";
            this.col归并名称.Name = "col归并名称";
            // 
            // col拼音代码
            // 
            this.col拼音代码.FieldName = "拼音代码";
            this.col拼音代码.Name = "col拼音代码";
            this.col拼音代码.Visible = true;
            this.col拼音代码.VisibleIndex = 5;
            // 
            // col是否禁用
            // 
            this.col是否禁用.FieldName = "是否禁用";
            this.col是否禁用.Name = "col是否禁用";
            // 
            // col单位编码
            // 
            this.col单位编码.FieldName = "单位编码";
            this.col单位编码.Name = "col单位编码";
            // 
            // col归并编码
            // 
            this.col归并编码.FieldName = "归并编码";
            this.col归并编码.Name = "col归并编码";
            // 
            // col新合编码
            // 
            this.col新合编码.Caption = "新合编码";
            this.col新合编码.FieldName = "新合编码";
            this.col新合编码.Name = "col新合编码";
            this.col新合编码.Visible = true;
            this.col新合编码.VisibleIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(478, 120);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(458, 26);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 74);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(261, 26);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.searchLookUpEdit1;
            this.layoutControlItem1.CustomizationFormText = "选择:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(458, 24);
            this.layoutControlItem1.Text = "费用:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(28, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton取消;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(359, 74);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(99, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(99, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(99, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton确定;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(261, 74);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(98, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(98, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(98, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit费用数量;
            this.layoutControlItem4.CustomizationFormText = "数量:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(458, 24);
            this.layoutControlItem4.Text = "数量:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(28, 14);
            // 
            // XtraForm套餐明细选择
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 120);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraForm套餐明细选择";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "费用选择";
            this.Load += new System.EventHandler(this.XtraForm套餐明细选择_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit费用数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton确定;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn col收费编码;
        private DevExpress.XtraGrid.Columns.GridColumn col收费名称;
        private DevExpress.XtraGrid.Columns.GridColumn col单位;
        private DevExpress.XtraGrid.Columns.GridColumn col单价;
        private DevExpress.XtraGrid.Columns.GridColumn col归并名称;
        private DevExpress.XtraGrid.Columns.GridColumn col拼音代码;
        private DevExpress.XtraGrid.Columns.GridColumn col是否禁用;
        private DevExpress.XtraGrid.Columns.GridColumn col单位编码;
        private DevExpress.XtraGrid.Columns.GridColumn col归并编码;
        private DevExpress.XtraEditors.TextEdit textEdit费用数量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn col新合编码;
    }
}