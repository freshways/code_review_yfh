﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using System.Diagnostics;
using DevExpress.Data;
using HIS.Model;
using System.Linq;
using LinqKit;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace HIS.COMM
{
    public partial class XtraFormICD10 : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        private bool b可否筛选 = false;
        public emr_pub_icd10 selected_icd_info = new emr_pub_icd10();
        List<emr_pub_icd10> list_icd = new List<emr_pub_icd10>();
        string curr_bigKind = "A";
        public XtraFormICD10()
        {
            InitializeComponent();
        }

        private void simpleButton退出_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void V刷新数据()
        {

            if (treeList分类.FocusedNode.Id == 0) //"我的常用"
            {
                list_icd.Clear();
                var list_我的常用 = chis.emr_collect_icd10.Where(c => c.用户编码 == HIS.COMM.zdInfo.ModelUserInfo.用户编码).ToList();
                foreach (var item in list_我的常用)
                {
                    emr_pub_icd10 icd = new emr_pub_icd10();
                    icd = HIS.Model.Helper.modelHelper.Mapper<emr_collect_icd10, emr_pub_icd10>(item);
                    list_icd.Add(icd);
                }
            }
            else
            {
                //选择大分类显示数据
                if (treeList分类.FocusedNode.Level == 0)
                {
                    var model大类 = chis.emr_pub_icd10大分类.Where(c => c.BigKindCode == treeList分类.FocusedNode.Tag.ToString()).FirstOrDefault();
                    curr_bigKind = model大类.BigKindCode;
                    labelControl编码依据.Text = $"编码依据：{model大类.编码依据}";

                    var list_item_byBigKind = chis.emr_pub_icd10.Where(c => c.BigKindCode == model大类.BigKindCode && c.disabled != true).OrderBy(c => c.ID).Skip(5).Take(20);
                    list_icd.Clear();
                    list_icd.AddRange(list_item_byBigKind);
                    //return;
                }
                else
                {
                    //选择中分类显示数据
                    int iKind;
                    int.TryParse(treeList分类.FocusedNode.Tag.ToString(), out iKind);
                    var list_item = chis.emr_pub_icd10.Where(c => c.KindID == iKind && c.disabled != true).OrderBy(c => c.ID).Skip(5).Take(20);
                    list_icd.Clear();
                    list_icd.AddRange(list_item);
                }
            }
            refreshGrid();
        }

        private void treeList分类_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            try
            {
                if (b可否筛选 == false)
                {
                    return;
                }
                V刷新数据();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
        private void refreshGrid()
        {
            gridView1.RefreshData();
            gridView1.BestFitMaxRowCount = 100;
            gridView1.BestFitColumns();
        }
        private void XtraFormICD10_Load(object sender, EventArgs e)
        {
            try
            {
                var list_ICD大类 = chis.emr_pub_icd10大分类.Where(c => c.disabled != true).ToList();
                var list_ICD中类 = chis.emr_pub_icd10中分类.Where(c => c.disabled != true).ToList();
                TreeListNode nodeICD我的常用 = this.treeList分类.AppendNode(new object[] { "我的常用" }, null);

                foreach (var item大类 in list_ICD大类)
                {
                    TreeListNode nodeICD大类 = this.treeList分类.AppendNode(new object[] { item大类.BigKindName }, null);
                    nodeICD大类.ImageIndex = 4;
                    nodeICD大类.SelectImageIndex = 4;
                    nodeICD大类.Tag = item大类.BigKindCode;
                    var list_level_0 = list_ICD中类.Where(c => c.theLevel == 0 && c.BigKindCode == item大类.BigKindCode && c.disabled != true).ToList();
                    if (list_level_0.Count > 0)
                    {
                        foreach (var item_level_0 in list_level_0)
                        {
                            TreeListNode nodeICD中类_0 = this.treeList分类.AppendNode(new object[] { item_level_0.KindName }, nodeICD大类);
                            nodeICD中类_0.ImageIndex = 1;
                            nodeICD中类_0.SelectImageIndex = 0;
                            nodeICD中类_0.Tag = item_level_0.ID.ToString();

                            var item_level_1 = list_ICD中类.Where(c => c.theLevel == 1 && c.ParentID == item_level_0.ID && c.disabled != true).ToList();
                            if (item_level_1.Count > 0)
                            {
                                foreach (var row_1 in item_level_1)
                                {
                                    TreeListNode nodeICD中类_1 = this.treeList分类.AppendNode(new object[] { row_1.KindName.ToString() }, nodeICD中类_0);
                                    nodeICD中类_1.ImageIndex = 1;
                                    nodeICD中类_1.SelectImageIndex = 0;
                                    nodeICD中类_1.Tag = row_1.ID.ToString();

                                    var rows2 = list_ICD中类.Where(c => c.theLevel == 2 && c.ParentID == row_1.ID && c.disabled != true).ToList();
                                    if (rows2.Count > 0)
                                    {
                                        foreach (var row_2 in rows2)
                                        {
                                            TreeListNode nodeICD中类_2 = this.treeList分类.AppendNode(new object[] { row_2.KindName.ToString() }, nodeICD中类_1);
                                            nodeICD中类_2.ImageIndex = 1;
                                            nodeICD中类_2.SelectImageIndex = 0;
                                            nodeICD中类_2.Tag = row_2.ID.ToString();

                                            var rows3 = list_ICD中类.Where(c => c.theLevel == 3 && c.ParentID == row_2.ID && c.disabled != true).ToList();
                                            if (rows3.Count > 0)
                                            {
                                                foreach (var row_3 in rows3)
                                                {
                                                    TreeListNode nodeICD中类_3 = this.treeList分类.AppendNode(new object[] { row_3.KindName.ToString() }, nodeICD中类_2);
                                                    nodeICD中类_3.ImageIndex = 1;
                                                    nodeICD中类_0.SelectImageIndex = 0;
                                                    nodeICD中类_3.Tag = row_3.ID.ToString();
                                                }
                                            }

                                        }
                                    }

                                }
                            }

                        }
                    }

                }
                gridControl1.DataSource = list_icd;
                this.gridView1.Columns["ICD10Name"].SummaryItem.FieldName = "ICD10Name";
                this.gridView1.Columns["ICD10Name"].SummaryItem.SummaryType = SummaryItemType.Count;

                treeList分类.SetFocusedNode(nodeICD我的常用);
                V刷新数据();
                refreshGrid();
                b可否筛选 = true;
            }
            catch (Exception ex)
            {

                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void simpleButton查询_Click(object sender, EventArgs e)
        {
            try
            {
                var where = PredicateBuilder.New<emr_pub_icd10>();

                if (textEdit编码.Text != "")
                {
                    where = where.And(c => c.ICD10.Contains(textEdit编码.Text.ToUpper()));
                }
                if (textEdit名称.Text != "")
                {
                    where = where.And(c => c.ICD10Name.Contains(textEdit名称.Text));
                }
                if (textEdit拼音.Text != "")
                {
                    where = where.And(c => c.ICD10HeadPY.Contains(textEdit拼音.Text));
                }
                if (!string.IsNullOrEmpty(curr_bigKind))
                {
                    where = where.And(c => c.BigKindCode == curr_bigKind && c.ICD11_章节_flag == false && c.disabled != true);
                }
             
                var list_item = chis.emr_pub_icd10.AsExpandable().Where(where).ToList();
                list_icd.Clear();
                list_icd.AddRange(list_item);
                refreshGrid();
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                {
                    MessageBox.Show("请选择病种", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                selected_icd_info = gridView1.GetFocusedRow() as emr_pub_icd10;
                barButtonItem添加_ItemClick(null, null);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }
            if (e.Button == MouseButtons.Right && ModifierKeys == Keys.None)
            {
                selected_icd_info = gridView1.GetFocusedRow() as emr_pub_icd10;
                Point p = new Point(Cursor.Position.X, Cursor.Position.Y);
                popupMenu常用疾病维护.ShowPopup(p);
                //if (treeList分类.FocusedNode.Id == 0)//我的常用
                //{
                //    barButtonItem添加.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                //    barButtonItem删除.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                //}
                //else
                //{
                //    barButtonItem添加.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                //    barButtonItem删除.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                //}
            }
        }

        private void barButtonItem添加_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            try
            {
                selected_icd_info = gridView1.GetFocusedRow() as emr_pub_icd10;
                var is_have = chis.emr_collect_icd10.Where(c => c.ICD10Name == selected_icd_info.ICD10Name && c.用户编码 == HIS.COMM.zdInfo.ModelUserInfo.用户编码).FirstOrDefault();
                if (is_have != null)
                {
                    return;
                }
                emr_collect_icd10 icd_collect = new emr_collect_icd10();
                icd_collect = HIS.Model.Helper.modelHelper.Mapper<emr_pub_icd10, emr_collect_icd10>(selected_icd_info);
                icd_collect.用户编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码;

                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
                {
                    chis.emr_collect_icd10.Attach(icd_collect);
                    chis.Entry(icd_collect).State = System.Data.Entity.EntityState.Added;
                    chis.SaveChanges();
                }
                HIS.COMM.msgBalloonHelper.BalloonShow($"【{icd_collect.ICD10Name }】成功添加到我的常用疾病");
            }
            catch (DbEntityValidationException dbEx)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage));
                    }
                }
                msgBalloonHelper.ShowInformation(stringBuilder.ToString());
            }
            catch (Exception ex)
            {
                string errorMsg = "错误：";
                if (ex.InnerException == null)
                    errorMsg += ex.Message + "，";
                else if (ex.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.Message + "，";
                else if (ex.InnerException.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.InnerException.Message;

                HIS.COMM.msgBalloonHelper.ShowInformation(errorMsg);
                this.DialogResult = DialogResult.OK;
            }

        }

        private void barButtonItem删除_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            selected_icd_info = gridView1.GetFocusedRow() as emr_pub_icd10;
            var collectItem = chis.emr_collect_icd10.Where(c => c.ICD10Name == selected_icd_info.ICD10Name).FirstOrDefault();
            if (collectItem == null)
            {
                return;
            }
            list_icd.Remove(selected_icd_info);

            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
            {
                chis.emr_collect_icd10.Attach(collectItem);
                chis.Entry(collectItem).State = System.Data.Entity.EntityState.Deleted;
                chis.SaveChanges();
            }
            refreshGrid();
            HIS.COMM.msgBalloonHelper.BalloonShow(collectItem.ICD10Name + "从我的常用疾病删除成功");
        }
    }
}