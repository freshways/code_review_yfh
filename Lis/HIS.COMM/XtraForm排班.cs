﻿using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using System;
using System.Data;
using System.Data.SqlClient;
using WEISHENG.COMM.PluginsAttribute;

namespace HIS.COMM
{
    [ClassInfoMark(GUID = "FF8F8CA4-13CA-4E73-813D-E3C96C5F626E", 键ID = "FF8F8CA4-13CA-4E73-813D-E3C96C5F626E", 父ID = "478FF7C6-E3F2-4FFD-91E6-992FD573746C",
功能名称 = "人员排班", 程序集名称 = "HIS.COMM", 程序集调用类地址 = "XtraForm排班",
传递参数 = "", 显示顺序 = 100,
菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public partial class XtraForm排班 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm排班()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 角色
        /// </summary>
        public string 角色 { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        public string 星期开始时间 { get; set; }

        /// <summary>
        /// 分院编码
        /// </summary>
        public string 科室 { get; set; }
        /// <summary>
        /// 更新登陆者
        /// </summary>
        public string 当前用户 { get; set; }

        /// <summary>
        /// 医院名称
        /// </summary>
        public string 医院名称
        { get; set; }
        DataTable Patient;
        DataTable PatientCopy;
        bool 角色flag = false;
        /// <summary>
        /// 画面初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XtraForm排班_Load(object sender, EventArgs e)
        {
            医院名称 = HIS.COMM.zdInfo.Model单位信息.sDwmc;
            DataTable dt员工 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb , CommandType.Text,
            " SELECT 角色名称,科室名称,用户名 FROM pubUser where 是否禁用 = '0' AND 用户编码 = '" + HIS.COMM.zdInfo.ModelUserInfo.用户编码 + "';").Tables[0];
            Getjs();
            if (dt员工.Rows[0]["用户名"].ToString().Equals("Admin"))
            {
                this.lookUpEdit角色选择.Properties.ReadOnly = false;
                this.lookUpEdit科室选择.Properties.ReadOnly = false;
                this.lookUpEdit角色选择.Text = dt员工.Rows[0]["角色名称"].ToString();
                this.lookUpEdit科室选择.Text = dt员工.Rows[0]["科室名称"].ToString();
            }
            else
            {
                this.lookUpEdit角色选择.Text = dt员工.Rows[0]["角色名称"].ToString();
                this.lookUpEdit科室选择.Text = dt员工.Rows[0]["科室名称"].ToString();
                this.lookUpEdit角色选择.Properties.ReadOnly = true;
                this.lookUpEdit科室选择.Properties.ReadOnly = true;
            }
            this.dta测量日期.EditValue = DateTime.Now;
            //角色 = dt员工.Rows[0]["角色名称"].ToString();
            //科室 = dt员工.Rows[0]["科室名称"].ToString();
            当前用户 = dt员工.Rows[0]["用户名"].ToString();
            GetInformion();
            this.lookUpEdit科室选择.EditValueChanged += new System.EventHandler(this.lookUpEdit科室选择_EditValueChanged);
            this.lookUpEdit角色选择.EditValueChanged += new System.EventHandler(this.lookUpEdit角色选择_EditValueChanged);
        }

        /// <summary>
        /// 获取角色信息
        /// </summary>
        private void Getjs()
        {
            DataTable dt角色 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb , CommandType.Text,
            " SELECT 角色名称,角色编号 FROM pubJs;").Tables[0];
            lookUpEdit角色选择.Properties.DataSource = dt角色;
            lookUpEdit角色选择.Properties.DisplayMember = "角色名称";
            lookUpEdit角色选择.Properties.ValueMember = "角色编号";
            DataTable dt科室 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text,
            " SELECT 科室编码,科室名称 FROM GY科室设置 where 是否禁用 = '0';").Tables[0];
            lookUpEdit科室选择.Properties.DataSource = dt科室;
            lookUpEdit科室选择.Properties.DisplayMember = "科室名称";
            lookUpEdit科室选择.Properties.ValueMember = "科室编码";
        }
        /// <summary>
        /// 查询数据库
        /// </summary>
        private void GetInformion()
        {
            try
            {
                string strwhere = "";
                角色 = this.lookUpEdit角色选择.Text;
                科室 = this.lookUpEdit科室选择.Text;
                if ("病历审核".Equals(角色) || "大夫".Equals(角色))
                {
                    角色 = "大夫"; strwhere += "and ( js.[角色名称] = '大夫' OR js.[角色名称] = '病历审核')";
                }
                else
                {
                    strwhere += " and js.[角色名称] = '" + 角色 + "'";
                }
                if (!string.IsNullOrEmpty(this.txt员工姓名.Text))
                {
                    strwhere += "AND js.[用户名] like '%" + this.txt员工姓名.Text + "%'";
                }
                //获取当前日期周一日期
                星期开始时间 = getWeekUpOfDate(this.dta测量日期.DateTime, DayOfWeek.Monday, 0).ToShortDateString();
                string SQL当前排班 = "SELECT" +
                "  js.[用户编码] 员工编号," +
                "  js.[用户名] 员工姓名," +
                "  ISNULL(ps.星期开始时间, '" + 星期开始时间 + "') 星期开始时间," +
                "  ps.[星期一] ," +
                "  ps.[星期二] ," +
                "  ps.[星期三] ," +
                "  ps.[星期四] ," +
                "  ps.[星期五] ," +
                "  ps.[星期六] ," +
                "  ps.[星期日] ," +
                "  ps.[作废标记] " +
                "  FROM" +
                "  pubUser js" +
                "  LEFT JOIN YS排班 ps ON js.[用户编码] = ps.[员工编号]" +
                "  AND ps.[星期开始时间] = '" + 星期开始时间 + "'" +
                "  WHERE" +
                " js.[是否禁用] = '0'" +
                " AND js.[科室名称] ='" + 科室 + "'" + strwhere;
                Patient = new DataTable();
                Patient = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL当前排班).Tables[0];
                PatientCopy = Patient.Copy();
                this.grd排班信息.DataSource = Patient;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 比较数据
        /// </summary>
        /// <param name="dt1">基础数据表</param>
        /// <param name="dt2"></param>
        /// <returns></returns>
        private static DataTable IsSame(DataTable dt1, DataTable dt2)
        {
            DataTable dt3 = new DataTable();
            dt3.Merge(dt1);
            dt3.AcceptChanges();
            dt3.Merge(dt2);
            DataTable dt4 = dt3.GetChanges();
            return dt4;
        }

        /// <summary>
        /// 获取周一的信息
        /// </summary>
        /// <param name="dt">时间</param>
        /// <param name="weekday">星期几</param>
        /// <param name="Number"></param>
        /// <returns></returns>
        public DateTime getWeekUpOfDate(DateTime dt, DayOfWeek weekday, int Number)
        {
            int wd1 = (int)weekday;
            int wd2 = (int)dt.DayOfWeek;
            return wd2 == wd1 ? dt.AddDays(7 * Number) : dt.AddDays(7 * Number - wd2 + wd1);
        }

        /// <summary>
        /// 角色选择变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lookUpEdit角色选择_EditValueChanged(object sender, EventArgs e)
        {
            GetInformion();
        }

        /// <summary>
        /// 保存按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn保存_Click(object sender, EventArgs e)
        {
            //修改数据保存
            GetDate();
        }
        /// <summary>
        /// 获取修改的信息
        /// </summary>
        private void GetDate()
        {
            string msgAlert = string.Empty;
            Model排班信息 model;
            DataTable PatientDf = IsSame(PatientCopy, Patient);
            if (PatientDf == null || PatientDf.Rows.Count < 1) return;
            SqlTransaction tran = HIS.Model.Dal.SqlHelper.BeginTransaction();
            bool flg = false;
            for (int i = 0; i < PatientDf.Rows.Count; i++)
            {
                model = new Model排班信息();
                var rows = PatientDf.Rows[i];
                model.员工编号 = PatientDf.Rows[i]["员工编号"].ToString();
                model.员工姓名 = string.IsNullOrEmpty(rows["员工姓名"].ToString()) ? null : rows["员工姓名"].ToString();
                model.星期开始时间 = getWeekUpOfDate(Convert.ToDateTime(rows["星期开始时间"].ToString()), DayOfWeek.Monday, 0).ToShortDateString();
                model.星期一 = string.IsNullOrEmpty(rows["星期一"].ToString()) ? "" : rows["星期一"].ToString();
                model.星期二 = string.IsNullOrEmpty(rows["星期二"].ToString()) ? "" : rows["星期二"].ToString();
                model.星期三 = string.IsNullOrEmpty(rows["星期三"].ToString()) ? "" : rows["星期三"].ToString();
                model.星期四 = string.IsNullOrEmpty(rows["星期四"].ToString()) ? "" : rows["星期四"].ToString();
                model.星期五 = string.IsNullOrEmpty(rows["星期五"].ToString()) ? "" : rows["星期五"].ToString();
                model.星期六 = string.IsNullOrEmpty(rows["星期六"].ToString()) ? "" : rows["星期六"].ToString();
                model.星期日 = string.IsNullOrEmpty(rows["星期日"].ToString()) ? "" : rows["星期日"].ToString();
                model.作废标记 = string.IsNullOrEmpty(rows["作废标记"].ToString()) ? "" : rows["作废标记"].ToString();
                if (!string.IsNullOrEmpty(msgAlert)) { XtraMessageBox.Show(msgAlert); HIS.Model.Dal.SqlHelper.RollbackTransaction(tran); return; }
                flg = InsertPatient(model, tran);
                if (!flg) break;
            }
            if (flg)
            {
                HIS.Model.Dal.SqlHelper.CommitTransaction(tran);
                XtraMessageBox.Show("数据保存成功");
                GetInformion();
            }
            else
            {
                HIS.Model.Dal.SqlHelper.RollbackTransaction(tran);
                XtraMessageBox.Show("数据保存失败");
            }
        }

        /// <summary>
        /// 取消按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn取消_Click(object sender, EventArgs e)
        {
            //画面关闭
            this.Close();
        }

        /// <summary>
        /// 插入数据库
        /// </summary>
        private bool InsertPatient(Model排班信息 model, SqlTransaction tran)
        {

            bool result = false;
            try
            {
                string SQLInsert =
                " IF EXISTS (" +
                " SELECT" +
                " *" +
                " FROM" +
                " YS排班" +
                " WHERE" +
                " 员工编号 = '" + model.员工编号 + "'" +
                " AND 星期开始时间 = '" + model.星期开始时间 + "'" +
                " )" +
                " BEGIN" +
                " UPDATE YS排班" +
                " SET [星期一] = '" + model.星期一 + "'" +
                " ,[星期二] = '" + model.星期二 + "'" +
                " ,[星期三] = '" + model.星期三 + "'" +
                " ,[星期四] = '" + model.星期四 + "'" +
                " ,[星期五] = '" + model.星期五 + "'" +
                " ,[星期六] = '" + model.星期六 + "'" +
                " ,[星期日] = '" + model.星期日 + "'" +
                " ,[作废标记] = '" + model.作废标记 + "'" +
                " ,[更新时间] = '" + DateTime.Now.ToString() + "'" +
                " ,[更新者] = '" + 当前用户 + "'" +
                " WHERE" +
                " 员工编号 = '" + model.员工编号 + "'" +
                " AND 星期开始时间 = '" + model.星期开始时间 + "'" +
                " END" +
                " ELSE" +
                " BEGIN" +
                " INSERT INTO YS排班 (" +
                " 员工编号," +
                " 员工姓名," +
                " 星期开始时间," +
                " 星期一," +
                " 星期二," +
                " 星期三," +
                " 星期四," +
                " 星期五," +
                " 星期六," +
                " 星期日," +
                " 作废标记," +
                " [更新时间]," +
                " [更新者]," +
                " [登录时间]," +
                " [登录人]" +
                " )" +
                " VALUES" +
                " (" +
                " '" + model.员工编号 + "'," +
                " '" + model.员工姓名 + "'," +
                " '" + model.星期开始时间 + "'," +
                " '" + model.星期一 + "'," +
                " '" + model.星期二 + "'," +
                " '" + model.星期三 + "'," +
                " '" + model.星期四 + "'," +
                " '" + model.星期五 + "'," +
                " '" + model.星期六 + "'," +
                " '" + model.星期日 + "'," +
                " '" + model.作废标记 + "'," +
                "'" + DateTime.Now.ToString() + "'," +
                "'" + 当前用户 + "'," +
                "'" + DateTime.Now.ToString() + "'," +
                "'" + 当前用户 + "'" +
                " )" +
                " END ";
                result = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(tran, CommandType.Text, SQLInsert) > 0;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return false;
            }
            return result;
        }

        /// <summary>
        /// 查询按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn查询_Click(object sender, EventArgs e)
        {
            this.GetInformion();
        }

        /// <summary>
        /// 打印按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (this.grdv排班信息.RowCount > 0)
            {
                PrintingSystem print = new DevExpress.XtraPrinting.PrintingSystem();
                PrintableComponentLink link = new PrintableComponentLink(print);
                print.Links.Add(link);
                this.grdv排班信息.Columns["星期开始时间"].VisibleIndex = -1;
                this.grdv排班信息.Columns["作废标记"].VisibleIndex = -1;
                link.Component = this.grd排班信息;//这里可以是可打印的部件
                //打印标题
                string _PrintHeader = 医院名称 + this.lookUpEdit科室选择.Text + Convert.ToDateTime(Patient.Rows[0]["星期开始时间"].ToString()).ToShortDateString() + "～" + getWeekUpOfDate(Convert.ToDateTime(Patient.Rows[0]["星期开始时间"].ToString()), DayOfWeek.Sunday, 1).ToShortDateString() + "排班信息";
                PageHeaderFooter phf = link.PageHeaderFooter as PageHeaderFooter;
                phf.Header.Content.Clear();
                phf.Header.Content.AddRange(new string[] { "", _PrintHeader, "" });
                phf.Header.Font = new System.Drawing.Font("宋体", 14, System.Drawing.FontStyle.Bold);
                phf.Header.LineAlignment = BrickAlignment.Center;
                link.CreateDocument(); //建立文档
                print.PreviewFormEx.Show();//进行预览
                this.grdv排班信息.Columns["星期开始时间"].VisibleIndex = 2;
                this.grdv排班信息.Columns["作废标记"].VisibleIndex = 10;
            }
            else
            {
                XtraMessageBox.Show("请选择要打印的内容");
            }
        }

        /// <summary>
        /// 科室选择修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lookUpEdit科室选择_EditValueChanged(object sender, EventArgs e)
        {
            //查询数据
            this.GetInformion();
        }

    }
}