﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using HIS.Model;

namespace HIS.COMM
{
    public class classBusiness
    {
        //public static Boolean MedicinesInventoryMonitoring(int pharmacyCode, decimal dypid, string s操作时间, string s操作摘要, string s操作明细, string s模块,
        // decimal d操作前, decimal d操作数, decimal d操作后)
        //{
        //    var medicine = new YP库存监控()
        //    {
        //        操作时间 = Convert.ToDateTime(s操作时间),
        //        YPID = dypid,
        //        操作摘要 = s操作摘要,
        //        操作明细 = s操作明细,
        //        操作前库存数 = d操作前,
        //        业务数 = d操作数,
        //        操作后库存数 = d操作后,
        //        模块名 = s模块,
        //        操作员名 = HIS.COMM.zdInfo.ModelUserInfo.用户名,
        //        药房编码 = pharmacyCode,
        //        createTime = DateTime.Now,
        //    };
        //    chis.YP库存监控.Attach(medicine);
        //    chis.Entry(medicine).State = System.Data.Entity.EntityState.Added;
        //    return true;
        //}
        public static Boolean yp库存监控(SqlTransaction tconn, decimal dypid, string s操作时间, string s操作摘要, string s操作明细, string s模块, string s操作员名,
            decimal d操作前, decimal d操作数, decimal d操作后)
        {
            Boolean re = false;
            try
            {
                string sqlCmd =
                    "insert into YP库存监控 (操作员名,ypid,操作时间, 操作摘要, 操作明细, 操作前库存数, 业务数, 操作后库存数, 模块名) values " +
                    " ('" + s操作员名 + "'," + dypid.ToString() + ",'" + s操作时间 + "','" + s操作摘要 + "','" + s操作明细 + "'," + d操作前.ToString() + "," + d操作数.ToString() + ","
                    + d操作后.ToString() + ",'" + s模块 + "')";
                if (HIS.Model.Dal.SqlHelper.ExecuteNonQuery(tconn, CommandType.Text, sqlCmd) == 1)
                {
                    re = true;
                }
                else
                {
                    re = false;
                }

                return re;
            }
            catch (Exception ee)
            {
                return re;
                throw ee;
            }
        }
        public static Boolean yp库存监控(SqlTransaction tconn, string s药房编码, decimal dypid, string s操作时间, string s操作摘要, string s操作明细, string s模块, string s操作员名,
                   decimal d操作前, decimal d操作数, decimal d操作后)
        {
            Boolean re = false;
            try
            {
                string sqlCmd =
                    "insert into YP库存监控 (操作员名,药房编码,ypid,操作时间, 操作摘要, 操作明细, 操作前库存数, 业务数, 操作后库存数, 模块名) values " +
                    " ('" + s操作员名 + "'," + s药房编码 + "," + dypid.ToString() + ",'" + s操作时间 + "','" + s操作摘要 + "','" + s操作明细 + "'," + d操作前.ToString() + "," + d操作数.ToString() + "," + d操作后.ToString() + ",'" + s模块 + "')";
                if (HIS.Model.Dal.SqlHelper.ExecuteNonQuery(tconn, CommandType.Text, sqlCmd) == 1)
                {
                    re = true;
                }
                else
                {
                    re = false;
                }

                return re;
            }
            catch (Exception ee)
            {
                return re;
                throw ee;
            }
        }
        public static Boolean yp结账(SqlTransaction tconn, string s模块名称, string s操作员名, string s结账时间, string s操作摘要)
        {
            Boolean re = false;
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" insert into  YP库存监控 (操作时间, YPID, 操作摘要, 操作明细, 操作前库存数, 业务数, 操作后库存数, 模块名, 操作员名, 药房编码) ");
                sb.Append(" select '" + s结账时间 + "' 操作时间, YPID,'" + s操作摘要 + "' 操作摘要,药品名称+'|'+药房规格+'|'+药房单位+'|'+药品产地+'|'+药品批号+'|'+convert(varchar(20),药品效期,102) 操作明细,");
                sb.Append(" 库存数量 操作前库存数,库存数量 业务数,库存数量 操作后库存数,'" + s模块名称 + "' 模块名,'" + s操作员名 + "' 操作员名,药房编码");
                sb.Append(" from YF库存信息 ");
                sb.Append(" where 库存数量<>0");
                Int32 iCount = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(tconn, CommandType.Text, sb.ToString());
                if (iCount == 0) { re = false; } else { re = true; }
                return re;
            }
            catch (Exception ex)
            {
                return re;
                //throw;
            }

        }

        /// <summary>
        /// 同步hyserver目录对照到  his门诊接口
        /// </summary>
        /// <returns></returns>
        public static string syncHyserverToMzjk()
        {
            string re = "成功";
            string sqlCmd;
            try
            {
                string localid = HIS.COMM.zdInfo.Model科室信息.单位编码.ToString();
                // string xzqh = WEISHENG.COMM.zdInfo.sXzqh;
                string hisDbName = "chis27" + localid.Substring(1, localid.Length - 1);
                string hyDbName = "hyhis2014_27" + localid.Substring(1, localid.Length - 1);

                sqlCmd =
                "declare @localid varchar(20) " +
                " set @localid='" + localid + "' " +
                " exec(' " +
                "     select '+@localid+' localid,*,1 grade into ##tmp2011 from " +
                "     OPENDATASOURCE(  " +
                "     ''SQLOLEDB'',  " +
                "    ''Data Source=192.168.10.121,1433;User ID=linkUser;Password=xxhjs321'')." + hyDbName + ".dbo.报销项目 where LEN(编码)<5000'" +
                "    ) " +
                " ; " +
                " with xnhdy as " +
                " ( " +
                "     SELECT " + localid + " 单位编码,编号 农合编码,名称,B.value as HIS编码 " +
                "     FROM( " +
                "         SELECT localID,编号,名称, 别名, 规格, 单位, 单价, 单价可改, 类型, 封顶, 是否报销, 备注,grade, value = CONVERT(xml, " +
                "                 '<root><v>' + REPLACE(substring(编码,1,len(编码)-1), ';', '</v><v>') + '</v></root>') " +
                "         FROM ##tmp2011 where  len(编码)>0 " +
                "     )A " +
                "    OUTER APPLY( " +
                "        SELECT value = N.v.value('.', 'varchar(100)') " +
                "        FROM A.[value].nodes('/root/v') N(v) " +
                "    )B " +
                " ) " +
                " select * into ##tmpdygx from xnhdy  where HIS编码<>'' " +

                //" --插入新的目录对应关系 " +
                " insert into tb目录对应 (单位编码, 农合编码, 农合名称, HIS编码) " +
                " select * from ##tmpdygx " +
                " where HIS编码 not in  " +
                " ( " +
                " select HIS编码 from tb目录对应 where 单位编码=" + localid + " " +
                " ) " +
                //" --删除对应关系 " +
                " delete from tb目录对应 where 单位编码=" + localid + " and HIS编码 not in " +
                " ( " +
                " select HIS编码 from ##tmpdygx " +
                " ) " +
                " drop table ##tmp2011 " +
                " drop table ##tmpdygx " +
                //" --更新his名称 " +
                " ; " +
                " with aa as " +
                " ( " +
                " select * from " +
                " OPENDATASOURCE(  " +
                " 'SQLOLEDB',  " +
                " 'Data Source=192.168.10.131;User ID=sa;Password=wsxxh@123')." + hisDbName + ".dbo.ypzl " +
                " ) " +
                " update " +
                " bb set bb.his名称=aa.名称 " +
                " from aa left outer join tb目录对应 bb on aa.编码=bb.his编码  " +
                " where bb.农合编码 is not null and bb.单位编码=('1'+substring(@localid,3,2)) and isnull(aa.名称,'')<>isnull(bb.his名称,'') " +

                " update [tb目录对应] " +
                " set HIS编码='A'+HIS编码 " +
                " where  农合编码 in " +
                " (  select 编号  " +
                " from 报销项目  " +
                " where 类型 not in (1,2,3,22,21)  " +
                " ) and  left(ISNULL(his编码,''),1)<>'A'";
                int iCount = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnXnhDbString, CommandType.Text, sqlCmd);
            }
            catch (Exception ex)
            {
                re = ex.Message;
                return re;
                //throw;
            }
            WEISHENG.COMM.LogHelper.Info("同步目录对应到门诊接口", "信息", re);
            return re;
        }


        public static int get当日一般诊疗费记录数(HIS.Model.MF门诊摘要 _mzPerson)
        {
            string sqlCmd =
                       "select count(*) from MF门诊明细 where MZID in " +
                       " ( " +
                       " select MZID from MF门诊摘要 where  作废标记=0 and  [医卡通账户号]='" + _mzPerson.医卡通账户号 + "' and 病人姓名='" + _mzPerson.病人姓名 + "' " +
                       " and 录入时间 between '" + DateTime.Now.ToShortDateString() + "' and '" + DateTime.Now.ToShortDateString() + " 23:59:59'  " +
                       " ) and 收费编码 in (15317,15318,15319,115317,115318,115319)";
            int iCount = Convert.ToInt32(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, sqlCmd));
            return iCount;
        }
        public static bool b一般诊疗费可否录入(int i当前收费编号, HIS.Model.MF门诊摘要 _mzPerson, DataTable dt, ref decimal _dec数量)
        {
            //15317	一般诊疗费(非注射型)
            //15318	一般诊疗费(肌注型)
            //15319	一般诊疗费(静滴型)          
            if ((i当前收费编号 == 15317 || i当前收费编号 == 15318 || i当前收费编号 == 15319 || i当前收费编号 == 115317 || i当前收费编号 == 115318 || i当前收费编号 == 115319))
            {
                int iCount = get当日一般诊疗费记录数(_mzPerson);
                if (iCount > 0)
                {
                    XtraMessageBox.Show("此病人在本单位当天已经发生一般诊疗费费用，不能再次录入", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                Int16 iCount2 = Convert.ToInt16(dt.Compute("count(费用序号)", "费用序号 in (15317,15318,15319,115317,115318,115319)"));
                if (iCount2 > 0)
                {
                    XtraMessageBox.Show("此病人在本张单据已经录入一般诊疗费，不能重复录入", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                _dec数量 = 1;
                return true;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="i当前收费编号"></param>
        /// <param name="_s医疗证号"></param>
        /// <param name="_s病人姓名"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool b一般诊疗费录入(int i当前收费编号, string _s医疗证号, string _s病人姓名, DataTable dt, ref decimal _dec数量)
        {
            //15317	一般诊疗费(非注射型)
            //15318	一般诊疗费(肌注型)
            //15319	一般诊疗费(静滴型)
            if ((i当前收费编号 == 15317 || i当前收费编号 == 15318 || i当前收费编号 == 15319 || i当前收费编号 == 115317 || i当前收费编号 == 115318 || i当前收费编号 == 115319))
            {

                string sqlCmd =
                    "select count(*) from MF门诊明细 where MZID in " +
                    " ( " +
                    " select MZID from MF门诊摘要 where  作废标记=0 and  医疗证号='" + _s医疗证号 + "' and 病人姓名='" + _s病人姓名 + "' " +
                    " and 录入时间 between '" + DateTime.Now.ToShortDateString() + "' and '" + DateTime.Now.ToShortDateString() + " 23:59:59'  " +
                    " ) and 收费编码 in (15317,15318,15319,115317,115318,115319)";
                int iCount = Convert.ToInt32(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, sqlCmd));
                if (iCount > 0)
                {
                    XtraMessageBox.Show("此病人在本单位当天已经发生一般诊疗费费用，不能再次录入", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                Int16 iCount2 = Convert.ToInt16(dt.Compute("count(费用序号)", "费用序号 in (15317,15318,15319,115317,115318,115319)"));
                if (iCount2 > 0)
                {
                    XtraMessageBox.Show("此病人在本张单据已经录入一般诊疗费，不能重复录入", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                _dec数量 = 1;
                return true;
            }
            else
            {
                return true;
            }
        }
    }
}
