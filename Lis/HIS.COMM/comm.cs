﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using HIS.Model;

namespace HIS.COMM
{
    public class comm
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
        /// <summary>
        /// 获取指定药品序号和数量的最早可用药品库存的YPID
        /// </summary>
        /// <param name="ypxh"></param>
        /// <param name="ypsl"></param>
        /// <returns></returns>
        public static bool getYPIDWith药品序号(string ypxh, string ypsl, string s药房编码, drug _drug)
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                "select ypid,药品产地,药房规格,药房单位,医院零售价,进价,药品名称,药品批号,库存数量 from yf库存信息 where ypid in (select isnull(min(ypid),0) from YF库存信息 where 药房编码=" +
                s药房编码 + " and 药品序号=" + ypxh + " and 库存数量>=" + ypsl + ") and 药房编码=" + s药房编码).Tables[0];
            if (dt.Rows.Count == 1)
            {
                _drug.S药品序号 = ypxh;
                _drug.Sypid = dt.Rows[0]["YPID"].ToString();
                _drug.S药房单位 = dt.Rows[0]["药房单位"].ToString();
                _drug.S药房规格 = dt.Rows[0]["药房规格"].ToString();
                _drug.S药品产地 = dt.Rows[0]["药品产地"].ToString();
                _drug.Dec医院零售价 = Convert.ToDecimal(dt.Rows[0]["医院零售价"]);
                _drug.Dec进价 = Convert.ToDecimal(dt.Rows[0]["进价"]);
                _drug.S药品名称 = dt.Rows[0]["药品名称"].ToString();
                _drug.S药品批号 = dt.Rows[0]["药品批号"].ToString();
                _drug.dec库存数量 = Convert.ToDecimal(dt.Rows[0]["库存数量"]);
                return true;
            }
            else
            {
                return false;
            }
        }
        public static HIS.Model.YF库存信息 GetYF库存信息(int _yfCode, decimal _useNumber, decimal _ypCode)
        {

            var item = chis.YF库存信息.Where(c => c.药房编码 == _yfCode && c.药品序号 == _ypCode && c.库存数量 >= _useNumber).OrderBy(c => c.CreateTime).FirstOrDefault();
            return item;

        }
        public static HIS.Model.YF库存信息 getYPIDWith药品序号(decimal? ypxh, decimal? ypsl, int s药房编码)
        {
            using (HIS.Model.CHISEntities chis = new HIS.Model.CHISEntities(HIS.Model.EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                var yf_item = chis.YF库存信息.Where(c => c.药房编码 == s药房编码 && c.药品序号 == ypxh && c.库存数量 >= ypsl).OrderBy(c => c.YPID)
                    .FirstOrDefault();
                return yf_item;
            }
        }

        ///// <summary>
        ///// 获取指定药品序号和数量的最早可用药品库存的YPID
        ///// </summary>
        ///// <param name="ypxh"></param>
        ///// <param name="ypsl"></param>
        ///// <returns></returns>
        //public static bool getYPIDWith药品序号(string ypxh, string ypsl, string s药房编码, ref drug[] _drug)
        //{
        //    DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(ClassDBConnstring.SConnHISDb, CommandType.Text,
        //        "select ypid,药品产地,药房规格,药房单位,医院零售价,进价,药品名称,药品批号,库存数量 from yf库存信息 where ypid in (select isnull(min(ypid),0) from YF库存信息 where 药房编码=" +
        //        s药房编码 + " and 药品序号=" + ypxh + " and 库存数量>=" + ypsl + ") and 药房编码=" + s药房编码).Tables[0];
        //    if (dt.Rows.Count == 1)
        //    {
        //        _drug.Sypid = dt.Rows[0]["YPID"].ToString();
        //        _drug.S药房单位 = dt.Rows[0]["药房单位"].ToString();
        //        _drug.S药房规格 = dt.Rows[0]["药房规格"].ToString();
        //        _drug.S药品产地 = dt.Rows[0]["药品产地"].ToString();
        //        _drug.Dec医院零售价 = Convert.ToDecimal(dt.Rows[0]["医院零售价"]);
        //        _drug.Dec进价 = Convert.ToDecimal(dt.Rows[0]["进价"]);
        //        _drug.S药品名称 = dt.Rows[0]["药品名称"].ToString();
        //        _drug.S药品批号 = dt.Rows[0]["药品批号"].ToString();
        //        _drug.dec库存数量 = Convert.ToDecimal(dt.Rows[0]["库存数量"]);
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public static string get财务分类with药品序号(string s药品序号)
        {
            return HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text,
                "select isnull(财务分类,'') from YK药品信息 where 药品序号=" + s药品序号).ToString();
        }

        public static string get药品产地WithYPID(string sYpid, string s药房编码)
        {
            return HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text,
                "select 药品产地 from YF库存信息 where 药房编码=" + s药房编码 + " and YPID=" + sYpid).ToString();
        }
        /// <summary>
        /// 获取指定药品编码的财务分类编码
        /// </summary>
        /// <param name="ypxh"></param>
        /// <returns></returns>
        public static string get药品财务归并编码With药品序号(string ypxh)
        {
            string s财务分类名 = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select 财务分类 from yk药品信息 where 药品序号='" + ypxh + "'").ToString();
            string s收费归并编码 = get归并编码(s财务分类名);
            return s收费归并编码;
        }
        /// <summary>
        /// 根据收费项目名称获取收费归并编码
        /// </summary>
        /// <param name="sfmc"></param>
        /// <returns></returns>
        public static string get归并编码(string sfmc)
        {
            if (sfmc == "卫生材料" || sfmc == "疫苗")
            {
                sfmc = "西药费";
            }
            string xmbm = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select 归并编码 from GY收费小项 where 收费名称='" + sfmc + "'").ToString();
            return xmbm;
        }
        /// <summary>
        /// 根据收费名称获取收费收费编码
        /// </summary>
        /// <param name="sfmc"></param>
        /// <returns></returns>
        public static string get收费小项编码(string sfmc)
        {
            string xmbm = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text,
                "select 收费编码 from GY收费小项 where 收费名称='" + sfmc + "'").ToString();
            return xmbm;
        }

        /// <summary>
        /// 根据收费编码获取收费归并编码
        /// </summary>
        /// <param name="sfbm"></param>
        /// <returns></returns>
        public static string get归并编码(int sfbm)
        {
            string xmbm = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select 归并编码 from GY收费小项 where 收费编码=" + sfbm).ToString();
            return xmbm;
        }


        public static string get收费名称By收费编码(int sfbm)
        {
            string xmbm = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select 收费名称 from GY收费小项 where 收费编码=" + sfbm).ToString();
            return xmbm;
        }
        /// <summary>
        /// 根据小项编码获取归并大项名称
        /// </summary>
        /// <param name="sfbm"></param>
        /// <returns></returns>
        public static string get归并名称(int sfbm)
        {
            string xmbm = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select 归并名称 from GY收费小项 where 收费编码=" + sfbm).ToString();
            return xmbm;
        }

        public static string get单价with收费编码(string sfbm)
        {
            string dj = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select 单价 from GY收费小项 where 收费编码=" + sfbm).ToString();
            return dj;
        }

        public static HIS.Model.GY收费小项 get收费小项By收费编码(Int32? sfbm)
        {
            return HIS.COMM.BLL.CacheData.Gy收费小项.Where(c => c.收费编码 == sfbm).FirstOrDefault();
        }

    }


}
