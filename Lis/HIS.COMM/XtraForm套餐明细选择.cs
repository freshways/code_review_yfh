﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HIS.COMM
{
    public partial class XtraForm套餐明细选择 : DevExpress.XtraEditors.XtraForm
    {
        private string s费用类型;
        public DataRow reRow;
        public decimal tcFysl;
        public XtraForm套餐明细选择(string _s费用类型)
        {
            InitializeComponent();
            s费用类型 = _s费用类型;
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            reRow = searchLookUpEdit1View.GetDataRow(searchLookUpEdit1View.FocusedRowHandle);
            tcFysl = Convert.ToDecimal(textEdit费用数量.Text);
        }

        private void XtraForm套餐明细选择_Load(object sender, EventArgs e)
        {
            try
            {
                string SQL = "";
                switch (s费用类型)
                {
                    case "检查":
                    case "检查套餐":
                        SQL =
                            "SELECT ID, 单价, 单位, 单位编码, 归并名称, 归并编码, 执行科室编码, 拼音代码, 收费名称, 收费编码," + "\r\n" +
                            "       新合编码, 是否报销, 是否禁用, 新合编码" + "\r\n" +
                            "FROM   GY收费小项" + "\r\n" +
                            "WHERE  (单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + ") AND (是否禁用 = 0)";
                        break;
                    case "药品":
                    case "药品套餐":
                        SQL =
                           "select 药品序号 收费编码,药品名称+'/'+药房规格 收费名称,药房单位 单位,0 单价,拼音代码,1 是否药品 " + "\r\n" +
                           "from [YK药品信息] where [是否禁用]=0";
                        break;
                }

                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL).Tables[0];
                searchLookUpEdit1.Properties.DataSource = dt;
                searchLookUpEdit1.Properties.PopulateViewColumns();
                searchLookUpEdit1.Properties.View.BestFitColumns();
                searchLookUpEdit1.Properties.View.Columns["收费名称"].Width = 150;
                searchLookUpEdit1.Properties.DisplayMember = "收费名称";
                searchLookUpEdit1.Properties.ValueMember = "收费编码";
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }
    }
}