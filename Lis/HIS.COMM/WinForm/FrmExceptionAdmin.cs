﻿//--------------------------------------------------------------------
// All Rights Reserved , Copyright (C) 2013 , Hairihan TECH, Ltd. 
//--------------------------------------------------------------------

using System;
using System.Data;
using System.ServiceModel;
using System.Windows.Forms;

namespace WEISHENG.COMM.WinForm
{
    /// <summary>
    /// FrmExceptionAdmin.cs
    /// 异常信息管理-管理异常信息窗体
    ///		
    /// 修改记录
    /// 
    ///     2010.09.21 版本：2.0 JiRiGaLa  按继承基础类窗体的方式改进好代码。
    ///     2007.08.20 版本：1.2 JiRiGaLa  Instance 实例调用模式，加快运行速度。
    ///     2007.06.14 版本：1.1 JiRiGaLa  进行调试修改细节上的错误改进。
    ///     2007.06.07 版本：1.0 JiRiGaLa  异常信息管理功能页面编写。
    ///		
    /// <author>
    ///		<name>JiRiGaLa</name>
    ///		<date>2010.09.21</date>
    /// </author> 
    /// </summary> 
    public partial class FrmExceptionAdmin : BaseForm
    {
        /// <summary>
        /// 异常 DataTable
        /// </summary>
        private DataTable ExceptionList = new DataTable(BaseExceptionEntity.TableName);

        /// <summary>
        /// 异常信息主键
        /// </summary>
        public new string EntityId
        {
            get
            {
                DataRow dr = BaseInterfaceLogic.GetFocusedDataRow(this.grvException);
                if (dr != null)
                {
                    return dr[BaseExceptionEntity.FieldId].ToString();
                }
                return null;
            }
        }

        public FrmExceptionAdmin()
        {
            InitializeComponent();
        }

        #region public override void SetControlState() 设置控件状态
        /// <summary>
        /// 设置控件状态
        /// </summary>
        public override void SetControlState()
        {
            this.btnSelectAll.Enabled = false;
            this.btnInvertSelect.Enabled = false;
            this.btnExport.Enabled = false;
            this.btnClearException.Enabled = false;
            this.btnBatchDelete.Enabled = false;
            if (this.ExceptionList.DefaultView.Count > 0)
            {
                this.btnSelectAll.Enabled = this.PermissionExport || this.PermissionExport;
                this.btnInvertSelect.Enabled = this.PermissionExport || this.PermissionExport;
                this.btnExport.Enabled = this.PermissionExport;
                this.btnClearException.Enabled = this.PermissionDelete;
                this.btnBatchDelete.Enabled = this.PermissionDelete;
            }
            // 判断编辑权限
            if (!this.PermissionDelete)
            {
                // 只读属性设置
                // this.grvException.Columns["colSelected"].ReadOnly = !this.PermissionDelete;
                this.btnSelectAll.Enabled = false;
                this.btnInvertSelect.Enabled = false;
            }
        }
        #endregion

        #region public override void GetPermissions() 获得页面的权限
        /// <summary>
        /// 获得页面的权限
        /// </summary>
        public override void GetPermissions()
        {
            // 这个是范围权限，对哪些（模块）有模块访问的权限？
            // 获取操作权限
            this.PermissionDelete = this.IsAuthorized(Permissions.ExceptionAdminDelete);
            this.PermissionExport = this.IsAuthorized(Permissions.ExceptionAdminExport);
        }
        #endregion

        #region public override void GetList() 绑定屏幕数据
        /// <summary>
        /// 绑定屏幕数据
        /// </summary>
        public override void GetList()
        {
            // 权限信息
            DotNetService dotNetService = new DotNetService();
            this.ExceptionList = dotNetService.ExceptionService.GetDataTable(UserInfo);
            if (dotNetService.ExceptionService is ICommunicationObject)
            {
                ((ICommunicationObject)dotNetService.ExceptionService).Close();
            }
            // this.grdExceptionAdmin.AutoGenerateColumns = false;

            BaseInterfaceLogic.GridViewColumnsToUpper(this.grvException);
            // 2012.05.23 Pcsky 按创建时间倒序排列，方便查看最新的异常
            this.ExceptionList.DefaultView.Sort = BaseExceptionEntity.FieldCreateOn + " DESC";
            DataTableAddColumn(this.ExceptionList);
            this.grdException.DataSource = this.ExceptionList.DefaultView;
            // this.grdExceptionAdmin.Refresh();
            
            // 设置按钮状态
            this.SetControlState();
        }
        #endregion

        #region private void DataTableAddColumn(DataTable dt) 往DataTable里面添加一列
        /// <summary>
        /// 往DataTable里面添加一列
        /// </summary>
        private void DataTableAddColumn(DataTable dt)
        {
            if (!dt.Columns.Contains(BaseBusinessLogic.SelectedColumn))
            {
                BaseInterfaceLogic.DataTableAddColumn(dt, BaseBusinessLogic.SelectedColumn, typeof(bool));
            }            
            // 设置表主键
            DataColumn[] primaryKey = new DataColumn[] { dt.Columns[BaseExceptionEntity.FieldId] };
            dt.PrimaryKey = primaryKey;
            // 未必选中状态
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ExceptionList.Rows[i][BaseBusinessLogic.SelectedColumn] = false;
            }
            dt.AcceptChanges();
        }
        #endregion

        #region public override void FormOnLoad() 加载窗体
        /// <summary>
        /// 加载窗体
        /// </summary>
        public override void FormOnLoad()
        {
            // 表格显示序号的处理部分
            // this.DataGridViewOnLoad(grdExceptionAdmin);
            // 绑定屏幕数据
            this.GetList();
        }
        #endregion

        private void grdExceptionAdmin_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            // 判断是否有删除权限
            if (!this.PermissionDelete)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(AppMessage.MSG0015, AppMessage.MSG0000, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                else
                {
                    DotNetService dotNetService = new DotNetService();
                    dotNetService.ExceptionService.Delete(this.UserInfo, this.EntityId);
                    if (dotNetService.ExceptionService is ICommunicationObject)
                    {
                        ((ICommunicationObject)dotNetService.ExceptionService).Close();
                    }
                }
            }
        }

        private void grdException_DoubleClick(object sender, EventArgs e)
        {
            DataRow dr = BaseInterfaceLogic.GetFocusedDataRow(this.grvException);
            if (dr != null)
            {
				BaseExceptionEntity exceptionEntity = BaseEntity.Create<BaseExceptionEntity>(dr);
                FrmException frmException = new FrmException(exceptionEntity);
                frmException.ShowDialog();
            }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ExceptionList.Rows.Count; i++)
            {
                ExceptionList.Rows[i][BaseBusinessLogic.SelectedColumn] = true;
            }
        }

        private void btnInvertSelect_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ExceptionList.Rows.Count; i++)
            {
                if (ExceptionList.Rows[i][BaseBusinessLogic.SelectedColumn].ToString() == true.ToString())
                {
                    ExceptionList.Rows[i][BaseBusinessLogic.SelectedColumn] = false;
                }
                else
                {
                    ExceptionList.Rows[i][BaseBusinessLogic.SelectedColumn] = true;
                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            // 导出Excel
            //string exportFileName = this.Text + ".csv";
            string exportFileName = this.Text + ".xls";
			this.ExportExcel(this.grvException, @"\Modules\Export\", exportFileName);
        }

        #region private string[] GetSelecteIds() 获得已被选择的部门主键数组
        /// <summary>
        /// 获得已被选择的部门主键数组
        /// </summary>
        /// <returns>主键数组</returns>
        private string[] GetSelecteIds()
        {
            return BaseInterfaceLogic.GetSelecteIds(this.ExceptionList.DefaultView, BaseExceptionEntity.FieldId, BaseBusinessLogic.SelectedColumn, true);
        }
        #endregion

        #region private bool CheckInputBatchDelete() 检查删除选择项的有效性
        /// <summary>
        /// 检查删除选择项的有效性
        /// </summary>
        /// <returns>有效</returns>
        private bool CheckInputBatchDelete()
        {
            return BaseInterfaceLogic.CheckInputSelectAnyOne(this.ExceptionList, BaseBusinessLogic.SelectedColumn);
        }
        #endregion

        #region public override int BatchDelete() 批量删除
        /// <summary>
        /// 批量删除
        /// </summary>
        public override int BatchDelete()
        {
            DotNetService dotNetService = new DotNetService();
            int result = dotNetService.ExceptionService.BatchDelete(this.UserInfo, this.GetSelecteIds());
            if (dotNetService.ExceptionService is ICommunicationObject)
            {
                ((ICommunicationObject)dotNetService.ExceptionService).Close();
            }
            // 绑定数据
            this.GetList();
            return result;
        }
        #endregion

        private void btnBatchDelete_Click(object sender, EventArgs e)
        {
            if (this.CheckInputBatchDelete())
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show(AppMessage.MSG0015, AppMessage.MSG0000, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                {
                    this.BatchDelete();
                }
            }
        }

        #region private void ClearException() 清除日志
        /// <summary>
        /// 清除日志
        /// </summary>
        private void ClearException()
        {
            // 设置鼠标繁忙状态，并保留原先的状态
            Cursor holdCursor = this.Cursor;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                DotNetService dotNetService = new DotNetService();
                dotNetService.ExceptionService.Truncate(this.UserInfo);
                if (dotNetService.ExceptionService is ICommunicationObject)
                {
                    ((ICommunicationObject)dotNetService.ExceptionService).Close();
                }
                // 重新加载
                this.FormOnLoad();
                // 显示提示信息
                DevExpress.XtraEditors.XtraMessageBox.Show(AppMessage.MSG0238, AppMessage.MSG0000, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                this.ProcessException(ex);
            }
            finally
            {
                // 设置鼠标默认状态，原来的光标状态
                this.Cursor = holdCursor;
            }
        }
        #endregion

        private void btnClearException_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show(AppMessage.MSG0239, AppMessage.MSG0000, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                this.ClearException();                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grvException_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            // 显示表格序号
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }
    }
}