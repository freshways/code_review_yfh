﻿//--------------------------------------------------------------------
// All Rights Reserved , Copyright (C) 2013 , Hairihan TECH, Ltd. 
//--------------------------------------------------------------------

using System.Windows.Forms;

using DevExpress.XtraSplashScreen;
using DevExpress.XtraWaitForm;

namespace WEISHENG.COMM.WinForm
{
	public class LoadingUtil
	{
		public static void StopLoading()
		{
			SplashScreenManager.CloseForm(false);
		}

		public static void ShowWaitLoading(Form frm)
		{
			//SplashScreenManager.ShowForm(frm, typeof(WaitForm1), true, true, false);
		}

		public static void ShowWaitLoading()
		{
			SplashScreenManager.ShowForm(null, typeof(WaitForm), true, true, false);
		}

		public static void ShowStartLoading()
		{
			SplashScreenManager.ShowForm(typeof(SplashScreen), true, true);
		}

		public static DialogResult ShowInformationMessage(string statusMessage, string appMessage)
		{
			return ShowMessage(statusMessage, appMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public static DialogResult ShowStopMessage(string statusMessage, string appMessage)
		{
			return ShowMessage(statusMessage, appMessage, MessageBoxButtons.OK, MessageBoxIcon.Stop);
		}

		public static DialogResult ShowQuestionMessage(string statusMessage, string appMessage)
		{
			return ShowMessage(statusMessage, appMessage, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
		}

		private static DialogResult ShowMessage(string statusMessage, string appMessage, MessageBoxButtons mbb, MessageBoxIcon mbi)
		{
			StopLoading();
			return DevExpress.XtraEditors.XtraMessageBox.Show(statusMessage, appMessage, mbb, mbi);
		}
	}
}
