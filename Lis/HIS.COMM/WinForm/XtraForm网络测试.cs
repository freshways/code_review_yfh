﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Net.NetworkInformation;

namespace WEISHENG.COMM.WinForm
{
    public partial class XtraForm网络测试 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm网络测试()
        {
            InitializeComponent();
        }

        public void simpleButton测试_Click(object sender, EventArgs e)
        {
            this.lst_PingResult.Items.Clear();
            string sTarget = "";// tykClass.Xml.XMLControl.GetConfigValue(WEISHENG.COMM.zdInfo.sConfigFileName, "目标服务器");
            if (sTarget == "")
            {
                switch (WEISHENG.COMM.zdInfo.enAppName)
                {
                    case appList.enAppName.AllInOne框架:
                        sTarget = "146.209.1.115|银行服务器,146.209.1.118|银行FTP服务器,192.168.100.120|FTP服务器," +
                            "10.117.225.11|医保主服务器,10.117.225.7|医保备用服务器";
                        if (WEISHENG.COMM.zdInfo.servers_主数据库.Length >= 8)
                        {
                            sTarget += "," + WEISHENG.COMM.zdInfo.servers_主数据库 + "|接口数据库";
                        }
                        if (WEISHENG.COMM.zdInfo.servers_主软件升级.Length >= 8)
                        {
                            sTarget += "," + WEISHENG.COMM.zdInfo.servers_主软件升级 + "|软件升级主服务器";
                        }
                        if (WEISHENG.COMM.zdInfo.servers_主数据库.Length >= 8)
                        {
                            sTarget += "," + WEISHENG.COMM.zdInfo.servers_辅软件升级 + "|软件升级备用服务器";
                        }
                        configHelper.setKeyValue("目标服务器", sTarget);
                        sTarget = "146.209.1.115|银行服务器,146.209.1.118|银行FTP服务器,192.168.100.120|FTP服务器," +
                            "10.117.193.10|医保主服务器,10.117.225.7|医保备用服务器";
                        break;
                    default:
                        break;
                }
            }
            string[] sArray1 = sTarget.Split(',');
            for (int i = 0; i < sArray1.Length; i++)
            {
                string sT = sArray1[i];
                string sT_ip = stringHelper.mySplit(sT, 0);
                string sT_Name = stringHelper.mySplit(sT, 1);
                ping(sT_ip, sT_Name);
            }

        }
        private void ping(string ipStr, string server说明)
        {
            string sRe = "";
            try
            {
                Ping pingSender = new Ping();
                //Ping 选项设置
                PingOptions options = new PingOptions();
                options.DontFragment = true;
                //测试数据
                string data = "test data abcabc";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                //设置超时时间
                int timeout = 120;
                //调用同步 send 方法发送数据,将返回结果保存至PingReply实例
                PingReply reply = pingSender.Send(ipStr.Trim(), timeout, buffer, options);

                if (reply.Status == IPStatus.Success)
                {
                    sRe = "网络通畅---> 答复的主机地址：" + server说明 + "|" + reply.Address.ToString() + "|往返时间：" +
                        reply.RoundtripTime + "|生存时间（TTL）：" + reply.Options.Ttl + "|是否控制数据包的分段：" +
                        reply.Options.DontFragment + "|缓冲区大小：" + reply.Buffer.Length;
                }
                else
                    sRe = "网络不通---> 答复的主机地址：" + server说明 + "|" + ipStr + "|" + reply.Status.ToString();
            }
            catch (Exception ex)
            {
                sRe = ex.Message;
                //   throw;
            }
            lst_PingResult.Items.Add(sRe);
            LogHelper.Info(this.Text + sRe);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                configHelper.setKeyValue("目标服务器",
                          "146.209.1.115|银行服务器,146.209.1.118|银行FTP服务器,192.168.100.120|FTP服务器,10.117.225.11|医保主服务器,10.117.225.7|医保备用服务器");
                XtraMessageBox.Show("保存配置成功");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}