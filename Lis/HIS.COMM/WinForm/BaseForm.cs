﻿//--------------------------------------------------------------------
// All Rights Reserved , Copyright (C) 2013 , Hairihan TECH, Ltd. 
//--------------------------------------------------------------------

using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using WEISHENG.COMM.Utilities;
using WEISHENG.COMM.Utilities.DbUtilities;

namespace WEISHENG.COMM.WinForm
{
    //using DotNet.Business;
    //using WEISHENG.COMM.Utilities;

    /// <summary>
    /// BaseForm
    /// 
    /// 修改纪录
    /// 
    ///     2013.05.07 版本：3.0 JiRiGaLa 修改基础窗体类Form 为 DevExpress.XtraEditors.XtraForm。
    ///     2013.04.22 版本：2.0 JiRiGaLa 增加了 partial 关键字，方便别人扩展，可以不影响本程序增加自己的方法属性。
    ///     2011.12.15 版本：1.3 zgl      修改public DataTable GetUserScope(string permissionScopeCode)方法，适应数据集权限的实施。
    ///     2011.04.05 版本：1.2 24 3 8   增加DataGridViewOnLoad方法。
    ///		2008.04.21 版本：1.1 JiRiGaLa 用户重新登录，扮演时的错误进行修正。
    ///		2008.01.11 版本：1.0 JiRiGaLa 创建。
    ///		
    /// <author>
    ///		<name>JiRiGaLa</name>
    ///		<date>2013.05.07</date>
    /// </author> 
    /// </summary>
    public partial class BaseForm : DevExpress.XtraEditors.XtraForm
    {
        private string entityId = string.Empty;
        /// <summary>
        /// 实体主键
        /// </summary>
        public virtual string EntityId
        {
            get
            {
                return this.entityId;
            }
            set
            {
                this.entityId = value;
            }
        }

        /// <summary>
        /// 只按对话框方式显示窗体
        /// </summary>
        public bool ShowDialogOnly = false;

        /// <summary>
        /// 是否记录窗体日志
        /// </summary>
        public bool RecordFormLog = false;

        /// <summary>
        /// 是否加在用户配置参数（表格）
        /// </summary>
        public bool LoadUserParameters = true;

        /// <summary>
        /// 访问权限
        /// </summary>
        public bool PermissionAccess = false;

        /// <summary>
        /// 新增权限
        /// </summary>
        public bool PermissionAdd = false;

        /// <summary>
        /// 编辑权限
        /// </summary>
        public bool PermissionEdit = false;

        /// <summary>
        /// 删除权限
        /// </summary>
        public bool PermissionDelete = false;

        /// <summary>
        /// 导入权限
        /// </summary>
        public bool PermissionImport = false;

        /// <summary>
        /// 导出权限
        /// </summary>
        public bool PermissionExport = false;

        /// <summary>
        /// 查询权限
        /// </summary>
        public bool PermissionSearch = false;


        public BaseForm()
        {
            this.Visible = false;
            if (!this.DesignMode)
            {
                // 必须放在初始化组件之前
                // this.GetIcon(); 
            }
            InitializeComponent();
        }

        public virtual void GetIcon()
        {
            if (System.IO.File.Exists(BaseSystemInfo.AppIco))
            {
                // XP 系统下这个程序会出错
                this.Icon = new Icon(BaseSystemInfo.AppIco);
            }

            if (this.BackgroundImage != null)
            {
                string file = "Resources\\Head.bmp";
                file = Path.Combine(Application.StartupPath, file);

                if (System.IO.File.Exists(file))
                {
                    this.BackgroundImage = Image.FromFile(file);
                }
            }
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.SuspendLayout();
            // 
            // BaseForm
            // 
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(694, 450);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "BaseForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_FormClosed);
            this.Load += new System.EventHandler(this.Form_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_KeyDown);
            this.ResumeLayout(false);
        }

        #region public virtual void GetPermissions() 获得页面的权限
        /// <summary>
        /// 获得页面的权限
        /// </summary>
        public virtual void GetPermissions()
        {
        }
        #endregion

        #region public virtual void SetControlState() 设置控件状态
        /// <summary>
        /// 设置控件状态
        /// </summary>
        public virtual void SetControlState()
        {
        }
        #endregion

        #region public virtual void SetControlState(bool enabled) 设置按钮状态
        /// <summary>
        /// 设置控件状态
        /// </summary>
        /// <param name="enabled">有效</param>
        public virtual void SetControlState(bool enabled)
        {
        }
        #endregion

        #region public virtual void SetHelp() 设置帮助
        /// <summary>
        /// 设置帮助
        /// </summary>
        public virtual void SetHelp()
        {
        }
        #endregion

        #region public virtual void ShowEntity() 显示内容
        /// <summary>
        /// 显示内容
        /// </summary>
        public virtual void ShowEntity()
        {
        }
        #endregion

        #region public virtual void GetList() 获得列表数据
        /// <summary>
        /// 获得列表数据
        /// </summary>
        public virtual void GetList()
        {
        }
        #endregion

        #region public virtual bool CheckInput() 检查输入的有效性
        /// <summary>
        /// 检查输入的有效性
        /// </summary>
        /// <returns>有效</returns>
        public virtual bool CheckInput()
        {
            return true;
        }
        #endregion

        #region public virtual bool SaveEntity() 保存
        /// <summary>
        /// 保存
        /// </summary>
        /// <returns>保存成功</returns>
        public virtual bool SaveEntity()
        {
            return true;
        }
        #endregion

        #region public virtual int BatchDelete() 批量删除
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <returns>影响行数</returns>
        public virtual int BatchDelete()
        {
            return 0;
        }
        #endregion

        #region public virtual bool CheckInputDelete() 检查批量删除
        /// <summary>
        /// 检查批量删除
        /// </summary>
        /// <returns>允许删除</returns>
        public virtual bool CheckInputDelete()
        {
            return true;
        }
        #endregion

        public void Comment(string formName = null, string entityId = null)
        {
            //if (string.IsNullOrEmpty(formName))
            //{
            //    formName = this.Name;
            //}
            //if (string.IsNullOrEmpty(entityId))
            //{
            //    entityId = this.EntityId;
            //}
            //bool commnets = false;
            //List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            //parameters.Add(new KeyValuePair<string, object>(BaseCommentEntity.FieldCategoryCode, formName));
            //parameters.Add(new KeyValuePair<string, object>(BaseCommentEntity.FieldObjectId, entityId));
            //parameters.Add(new KeyValuePair<string, object>(BaseCommentEntity.FieldDeletionStateCode, 0));
            //commnets = DbLogic.Exists(this.UserCenterDbHelper, BaseCommentEntity.TableName, parameters);
            //// 若有记录显示列表页面，若没记录直接显示添加页面
            //if (commnets)
            //{
            //    FrmCommnets frmCommnets = new FrmCommnets(formName, entityId);
            //    frmCommnets.ShowDialog();
            //}
            //else
            //{
            //    FrmCommentAdd frmCommentAdd = new FrmCommentAdd(formName, entityId);
            //    frmCommentAdd.ShowDialog();
            //}
        }

        public void WriteException(Exception ex)
        {
            // 在本地记录异常
            FileUtil.WriteException(UserInfo, ex);
        }

        /// <summary>
        /// 处理异常信息
        /// </summary>
        /// <param name="ex">异常</param>
        public void ProcessException(Exception ex)
        {
            this.WriteException(ex);
            // 显示异常页面
            FrmException frmException = new FrmException(ex);
            frmException.ShowDialog();
        }

        /// <summary>
        /// 是否退出应用程序
        /// </summary>
        public bool ExitApplication = false;

        /// <summary>
        /// 窗体已经加载完毕
        /// </summary>
        public bool FormLoaded = false;

        /// <summary>
        /// 是否忙碌状态
        /// </summary>
        public bool Busyness = false;

        /// <summary>
        /// 数据发生过变化
        /// </summary>
        public bool Changed = false;

        /// <summary>
        /// 当前日志主键
        /// </summary>
        public string LogId = string.Empty;

        /// <summary>
        /// 设置按钮的可用状态
        /// </summary>
        /// <param name="setTop">置顶</param>
        /// <param name="setUp">上移</param>
        /// <param name="setDown">下移</param>
        /// <param name="setBottom">置底</param>
        /// <param name="add">添加</param>
        /// <param name="edit">编辑</param>
        /// <param name="batchDelete">批量删除</param>
        /// <param name="batchSave">批量保存</param>
        public delegate void SetControlStateEventHandler(bool setTop, bool setUp, bool setDown, bool setBottom, bool add, bool edit, bool batchDelete, bool batchSave);

        /// <summary>
        /// 当前用户信息
        /// 这里表示是只读的
        /// </summary>
        public BaseUserInfo UserInfo
        {
            get
            {
                return BaseSystemInfo.UserInfo;
            }
        }

        #region public void Localization(Form form) 多语言国际化加载
        /// <summary>
        /// 多语言国际化加载
        /// </summary>
        public void Localization(System.Windows.Forms.Form form)
        {
            // BaseInterfaceLogic.SetLanguageResource(form);
        }
        #endregion

        #region public void LoadUserParameter(Form form) 客户端页面配置加载
        /// <summary>
        /// 客户端页面配置加载
        /// </summary>
        public void LoadUserParameter(System.Windows.Forms.Form form)
        {
            // BaseInterfaceLogic.LoadDataGridViewColumnWidth(form);
        }
        #endregion

        public virtual void Form_KeyDown(object sender, KeyEventArgs e)
        {
            // 按键事件
            if (e.KeyCode == Keys.F5)
            {
                // F5刷新，重新加载窗体
                this.FormOnLoad();
            }
            else
            {
                // 按了回车按钮处理光标焦点
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
                {
                    if ((this.ActiveControl is DevExpress.XtraEditors.TextEdit)
                        || (this.ActiveControl is DevExpress.XtraEditors.ImageComboBoxEdit)
                        || (this.ActiveControl is DevExpress.XtraEditors.CheckEdit))
                    {
                        SendKeys.Send("{TAB}");
                    }

                    //if ((this.ActiveControl is TextBox) || (this.ActiveControl is ComboBox) || (this.ActiveControl is CheckBox))
                    //{
                    //    if ((this.ActiveControl is TextBox) && ((TextBox)this.ActiveControl).Multiline)
                    //    {
                    //        return;
                    //    }
                    //    SendKeys.Send("{TAB}");
                    //}
                }
            }
            // 打印界面的快捷方式
            if (e.KeyCode == Keys.F10)
            {
                Image iA = new Bitmap(this.Width, this.Height);
                Graphics g = Graphics.FromImage(iA);
                g.CopyFromScreen(new Point(this.Location.X, this.Location.Y), new Point(0, 0), new Size(this.Width, this.Height));
                Clipboard.SetImage(iA);
                FrmPrint frmPrint = new FrmPrint();
                frmPrint.ShowDialog();
            }
        }

        #region public virtual void FormOnLoad() 加载窗体
        /// <summary>
        /// 加载窗体
        /// </summary>
        public virtual void FormOnLoad()
        {
        }
        #endregion

        public virtual void Form_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                LoadingUtil.ShowWaitLoading();

                // 设置鼠标繁忙状态，并保留原先的状态
                Cursor holdCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;
                this.FormLoaded = false;
                try
                {
                    //// 是否记录访问日志
                    //if (BaseSystemInfo.RecordLog)
                    //{
                    //    // 已经登录了系统了，才记录日志
                    //    if (BaseSystemInfo.UserIsLogOn)
                    //    {
                    //        // 调用服务事件
                    //        if (this.RecordFormLog)
                    //        {
                    //            // 调用服务事件
                    //            // this.LogId = DotNetService.Instance.LogService.WriteLog(UserInfo, this.Name, this.Text, "FormLoad");
                    //            DotNetService dotNetService = new DotNetService();
                    //            dotNetService.LogService.WriteLog(UserInfo, this.Name, AppMessage.GetMessage(this.Name), "FormLoad",  AppMessage.LoadWindow);
                    //            if (dotNetService.LogService is ICommunicationObject)
                    //            {
                    //                ((ICommunicationObject)dotNetService.LogService).Close();
                    //            }
                    //        }
                    //    }
                    //}

                    // 必须放在初始化组件之前
                    this.GetIcon();
                    // 获得页面的权限
                    this.GetPermissions();
                    // 加载窗体
                    this.FormOnLoad();
                    // 设置按钮状态
                    this.SetControlState();
                    if (BaseSystemInfo.MultiLanguage)
                    {
                        // 多语言国际化加载
                        if (ResourceManagerWrapper.Instance.GetLanguages() != null)
                        {
                            this.Localization(this);
                        }
                    }
                    if (this.LoadUserParameters)
                    {
                        // 客户端页面配置加载
                        this.LoadUserParameter(this);
                    }
                    // 设置帮助
                    this.SetHelp();
                }
                catch (Exception ex)
                {
                    this.ProcessException(ex);
                }
                finally
                {
                    this.FormLoaded = true;
                    // 设置鼠标默认状态，原来的光标状态
                    this.Cursor = holdCursor;
                }

                LoadingUtil.StopLoading();
                this.Visible = true;
            }
        }

        //public bool IsAuthorized(PermissionItem ppItem)
        //{
        //    return IsAuthorized(ppItem.Code, ppItem.Name);
        //}

        #region public bool IsAuthorized(string permissionCode, string permissionItemName = string.Empty) 是否有相应的权限
        /// <summary>
        /// 是否有相应的权限
        /// </summary>
        /// <param name="permissionCode">权限编号</param>
        /// <param name="permissionItemName">权限名称</param>
        /// <returns>有权限</returns>
        //public bool IsAuthorized(string permissionCode, string permissionItemName = null)
        //{
        //    // 默认为了安全起见、设置为无权限比较好
        //    bool result = false;
        //    // 先判断用户是否超级管理员，若是超级管理员，就不用判断操作权限了（这个是有点儿C/S的特色）

        //    // 若不使用操作权限项定义，那就所有操作权限都是不用生效了
        //    bool fromCache = false;

        //    // 若是调试状态，从服务器上获取权限
        //    #if (!DEBUG)

        //        // 加强安全验证防止未授权匿名调用
        //        if (UserInfo.IsAdministrator)
        //        {
        //            return true;
        //        }

        //        fromCache = true;
        //    #endif

        //    if (fromCache)
        //    {
        //        // 这里也可以优化一下，没必要遍历所有的操作权限列表
        //        int count = ClientPermissionCache.Instance.UserModuleList.Count(entity => !string.IsNullOrEmpty(entity.Code) && entity.Code.Equals(permissionCode, StringComparison.OrdinalIgnoreCase));
        //        return count > 0;
        //    }

        //    // 虽然这样读取权限效率低一些，但是会及时性高一些，例如这个时候正好权限被调整了
        //    // 这里是在服务器上进行权限判断，远程进行权限判断（B/S的都用这个方法直接判断权限）
        //    if (!result)
        //    {
        //        DotNetService dotNetService = new DotNetService();
        //        result = dotNetService.PermissionService.IsAuthorizedByUser(this.UserInfo, this.UserInfo.Id, permissionCode, permissionItemName);
        //        if (dotNetService.PermissionService is ICommunicationObject)
        //        {
        //            ((ICommunicationObject)dotNetService.PermissionService).Close();
        //        }
        //    }
        //    return result;
        //}
        #endregion

        /// <summary>
        /// 获取组织机构权限域数据
        /// </summary>
        //public DataTable GetOrganizeDataTableScope(string permissionScopeCode = "Resource.ManagePermission", bool isInnerOrganize = true)
        //{
        //    // 获取部门数据，不启用权限域
        //    DataTable dt = new DataTable(BaseOrganizeEntity.TableName);
        //    if (UserInfo.IsAdministrator
        //        || !BaseSystemInfo.UsePermissionScope
        //        || String.IsNullOrEmpty(permissionScopeCode))
        //    {
        //        dt = ClientPermissionCache.Instance.GetOrganizeDT(UserInfo).Copy();
        //        if (isInnerOrganize)
        //        {
        //            BaseBusinessLogic.SetFilter(dt, BaseOrganizeEntity.FieldIsInnerOrganize, "1");
        //            BaseInterfaceLogic.CheckTreeParentId(dt, BaseOrganizeEntity.FieldId, BaseOrganizeEntity.FieldParentId);
        //        }
        //        dt.DefaultView.Sort = BaseOrganizeEntity.FieldSortCode;
        //    }
        //    else
        //    {
        //        DotNetService dotNetService = new DotNetService();
        //        dt = dotNetService.PermissionService.GetOrganizeDTByPermissionScope(UserInfo, UserInfo.Id, permissionScopeCode);
        //        if (dotNetService.PermissionService is ICommunicationObject)
        //        {
        //            ((ICommunicationObject)dotNetService.PermissionService).Close();
        //        }
        //        if (isInnerOrganize)
        //        {
        //            BaseBusinessLogic.SetFilter(dt, BaseOrganizeEntity.FieldIsInnerOrganize, "1");
        //            BaseInterfaceLogic.CheckTreeParentId(dt, BaseOrganizeEntity.FieldId, BaseOrganizeEntity.FieldParentId);
        //        }
        //        dt.DefaultView.Sort = BaseOrganizeEntity.FieldSortCode;
        //    }
        //    return dt;
        //}

        /// <summary>
        /// 获取用户权限域数据
        /// </summary>
        //public DataTable GetUserDataTableScope(string permissionScopeCode = "Resource.ManagePermission", bool showRole = false)
        //{
        //// 是否有用户管理权限，若有用户管理权限就有所有的用户类表，这个应该是内置的操作权限
        //bool userAdmin = false;
        //userAdmin = this.IsAuthorized(Permissions.UserAdmin);
        //DataTable dt = new DataTable(BaseUserEntity.TableName);
        //// 获取用户数据
        //DotNetService dotNetService = new DotNetService();
        //if (userAdmin
        //    || this.UserInfo.IsAdministrator
        //    || !BaseSystemInfo.UsePermissionScope
        //    || (String.IsNullOrEmpty(permissionScopeCode)))
        //{
        //    dt = dotNetService.UserService.GetDataTable(UserInfo, showRole);
        //    if (dotNetService.UserService is ICommunicationObject)
        //    {
        //        ((ICommunicationObject)dotNetService.UserService).Close();
        //    }
        //}
        //else
        //{
        //    dt = dotNetService.PermissionService.GetUserDataTableByPermissionScope(UserInfo, UserInfo.Id, permissionScopeCode);
        //    if (dotNetService.PermissionService is ICommunicationObject)
        //    {
        //        ((ICommunicationObject)dotNetService.PermissionService).Close();
        //    }
        //}
        //dt.TableName = BaseUserEntity.TableName;
        //return dt;
        //}

        /// <summary>
        /// 获取用户权限域数据
        /// </summary>
        //public DataTable GetUserDataTableScopeByPage(string permissionScopeCode, string search, bool showRole, bool userAllInformation, out int recordCount, int pageIndex = 0, int pageSize = 20, string sort = "SortCode")
        //{
        //    // 是否有用户管理权限，若有用户管理权限就有所有的用户类表，这个应该是内置的操作权限
        //    bool userAdmin = false;
        //    userAdmin = this.IsAuthorized(Permissions.UserAdmin);
        //    DataTable dt = new DataTable(BaseUserEntity.TableName);
        //    // 获取用户数据
        //    DotNetService dotNetService = new DotNetService();
        //    if (userAdmin
        //        || this.UserInfo.IsAdministrator
        //        || !BaseSystemInfo.UsePermissionScope)
        //    {
        //        permissionScopeCode = string.Empty;
        //    }
        //    dt = dotNetService.UserService.SearchByPage(UserInfo, permissionScopeCode, search, string.Empty, null, showRole, userAllInformation, out recordCount, pageIndex, pageSize, sort);
        //    if (dotNetService.PermissionService is ICommunicationObject)
        //    {
        //        ((ICommunicationObject)dotNetService.PermissionService).Close();
        //    }
        //    dt.TableName = BaseUserEntity.TableName;
        //    return dt;
        //}

        /// <summary>
        /// 获取用户权限域数据
        /// </summary>
        //public List<BaseUserEntity> GetUserListScope(string permissionScopeCode = "Resource.ManagePermission")
        //{
        //    // 是否有用户管理权限，若有用户管理权限就有所有的用户类表，这个应该是内置的操作权限
        //    bool userAdmin = false;
        //    userAdmin = this.IsAuthorized(Permissions.UserAdmin);
        //    List<BaseUserEntity> entityList = new List<BaseUserEntity>();
        //    // 获取用户数据
        //    DotNetService dotNetService = new DotNetService();
        //    if (userAdmin
        //        || this.UserInfo.IsAdministrator
        //        || !BaseSystemInfo.UsePermissionScope
        //        || (String.IsNullOrEmpty(permissionScopeCode)))
        //    {
        //        entityList = dotNetService.UserService.GetList(UserInfo);
        //        if (dotNetService.UserService is ICommunicationObject)
        //        {
        //            ((ICommunicationObject)dotNetService.UserService).Close();
        //        }
        //    }
        //    else
        //    {
        //        entityList = dotNetService.PermissionService.GetUserListByPermissionScope(UserInfo, UserInfo.Id, permissionScopeCode);
        //        if (dotNetService.PermissionService is ICommunicationObject)
        //        {
        //            ((ICommunicationObject)dotNetService.PermissionService).Close();
        //        }
        //    }
        //    return entityList;
        //}

        /// <summary>
        /// 获取用户权限域数据
        /// </summary>
        //public DataTable GetRoleDataTableScope(string permissionScopeCode = "Resource.ManagePermission")
        //{
        //    // 获取部门数据
        //    DataTable result = new DataTable(BaseOrganizeEntity.TableName);
        //    DotNetService dotNetService = new DotNetService();
        //    if ((UserInfo.IsAdministrator)
        //        || !BaseSystemInfo.UsePermissionScope
        //        || (String.IsNullOrEmpty(permissionScopeCode)))
        //    {
        //        result = dotNetService.RoleService.GetDataTable(UserInfo);
        //        if (dotNetService.RoleService is ICommunicationObject)
        //        {
        //            ((ICommunicationObject)dotNetService.RoleService).Close();
        //        }
        //    }
        //    else
        //    {
        //        result = dotNetService.PermissionService.GetRoleDTByPermissionScope(UserInfo, UserInfo.Id, permissionScopeCode);
        //        if (dotNetService.PermissionService is ICommunicationObject)
        //        {
        //            ((ICommunicationObject)dotNetService.PermissionService).Close();
        //        }
        //    }
        //    return result;
        //}

        /// <summary>
        /// 获取模块菜单限域数据
        /// </summary>
        //public DataTable GetModuleDataTableScope(string permissionScopeCode = "Resource.ManagePermission")
        //{
        //DotNetService dotNetService = new DotNetService();
        //// 获取部门数据
        //if (UserInfo.IsAdministrator
        //    || !BaseSystemInfo.UsePermissionScope
        //    || String.IsNullOrEmpty(permissionScopeCode))
        //{
        //    DataTable dtModule = dotNetService.ModuleService.GetDataTable(UserInfo);
        //    if (dotNetService.ModuleService is ICommunicationObject)
        //    {
        //        ((ICommunicationObject)dotNetService.ModuleService).Close();
        //    }
        //    // 这里需要只把有效的模块显示出来
        //    // BaseBusinessLogic.SetFilter(dtModule, BaseModuleEntity.FieldEnabled, "1");
        //    // 未被打上删除标标志的
        //    // BaseBusinessLogic.SetFilter(dtModule, BaseModuleEntity.FieldDeletionStateCode, "0");
        //    return dtModule;
        //}
        //else
        //{
        //    DataTable dt = dotNetService.PermissionService.GetModuleDTByPermissionScope(UserInfo, UserInfo.Id, permissionScopeCode);
        //    if (dotNetService.PermissionService is ICommunicationObject)
        //    {
        //        ((ICommunicationObject)dotNetService.PermissionService).Close();
        //    }
        //    BaseInterfaceLogic.CheckTreeParentId(dt, BaseModuleEntity.FieldId, BaseModuleEntity.FieldParentId);
        //    return dt;
        //}
        //}

        /// <summary>
        /// 获取授权范围数据 （按道理，应该是在某个数据区域上起作用）
        /// </summary>
        //public DataTable GetPermissionDataTableScop(string permissionScopeCode = "Resource.ManagePermission")
        //{
        //    // 获取部门数据
        //    DataTable dtPermission = new DataTable(BaseModuleEntity.TableName);
        //    DotNetService dotNetService = new DotNetService();
        //    if (UserInfo.IsAdministrator
        //        || !BaseSystemInfo.UsePermissionScope
        //        || String.IsNullOrEmpty(permissionScopeCode))
        //    {
        //        dtPermission = dotNetService.ModuleService.GetDataTable(UserInfo);
        //        if (dotNetService.PermissionService is ICommunicationObject)
        //        {
        //            ((ICommunicationObject)dotNetService.PermissionService).Close();
        //        }
        //        // 这里需要只把有效的模块显示出来
        //        // BaseBusinessLogic.SetFilter(dtPermission, BaseModuleEntity.FieldEnabled, "1");
        //        // 未被打上删除标标志的
        //        // BaseBusinessLogic.SetFilter(dtPermission, BaseModuleEntity.FieldDeletionStateCode, "0");

        //    }
        //    else
        //    {
        //        dtPermission = dotNetService.PermissionService.GetPermissionDTByPermissionScope(UserInfo, UserInfo.Id, permissionScopeCode);
        //        if (dotNetService.PermissionService is ICommunicationObject)
        //        {
        //            ((ICommunicationObject)dotNetService.PermissionService).Close();
        //        }
        //        BaseInterfaceLogic.CheckTreeParentId(dtPermission, BaseModuleEntity.FieldId, BaseModuleEntity.FieldParentId);
        //    }
        //    return dtPermission;
        //}

        #region public bool ModuleIsVisible(string formName) 模块是否可见
        /// <summary>
        /// 模块是否可见
        /// </summary>
        /// <param name="formName">模块编号</param>
        /// <returns>有权限</returns>
        //public bool ModuleIsVisible(string formName)
        //{
        //    bool result = false;
        //    foreach (var entity in ClientPermissionCache.Instance.ModuleList)
        //    {
        //        if (entity.Enabled == 1
        //            && entity.DeletionStateCode == 0
        //            && (!string.IsNullOrEmpty(entity.Code) && entity.Code.Equals(formName)) || (!string.IsNullOrEmpty(entity.FormName) && entity.FormName.Equals(formName)))
        //        {
        //            result = (entity.Enabled == 1);
        //            break;
        //        }
        //    }
        //    // 模块是否可见;
        //    return result;
        //}
        #endregion

        #region private bool FileExist(string fileName) 检查文件是否存在
        /// <summary>
        /// 检查文件是否存在
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <returns>是否存在</returns>
        private bool FileExist(string fileName)
        {
            if (System.IO.File.Exists(fileName))
            {
                string targetFileName = System.IO.Path.GetFileName(fileName);
                if (DevExpress.XtraEditors.XtraMessageBox.Show(AppMessage.Format(AppMessage.MSG0236, targetFileName), AppMessage.MSG0000, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    System.IO.File.Delete(fileName);
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        private SaveFileDialog GetShowFileDialog(string directory, string fileName)
        {
            string directoryName = BaseSystemInfo.StartupPath + directory;
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
            // 这里显示选择文件的对话框，可以取消导出可以确认导出，可以修改文件名。
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = fileName;
            fileName = BaseSystemInfo.StartupPath + directory + fileName;
            saveFileDialog.InitialDirectory = directoryName;
            //saveFileDialog.Filter = "导出数据文件(*.csv)|*.csv|所有文件|*.*";
            saveFileDialog.Filter = "Excel(*.xls)|*.xls|所有文件|*.*";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.Title = "导出数据文件";
            return saveFileDialog;
        }

        private bool ValidateExport(SaveFileDialog saveFileDialog, out string fileName)
        {
            fileName = saveFileDialog.FileName;

            // 2012.04.02 Pcsky 增加 判断文件是否被打开
            //if (BaseExportExcel.CheckIsOpened(fileName))
            //{
            //    LoadingUtil.ShowInformationMessage("Excel文件已经打开,请关闭后重试!", "提示信息");
            //    return false;
            //}

            if (System.IO.File.Exists(fileName))
            {
                System.IO.File.Delete(fileName);
            }
            return true;
        }

        #region private void ExportExcel(System.Data.DataTable dt, Dictionary<string,string> fieldList, string directory, string fileName) 导出Excel
        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="dataGridView">表格控件</param>
        /// <param name="dt">数据表格</param>
        /// <param name="fieldList">数据表字段名-说明对应列表</param>
        /// <param name="directory">目录</param>
        /// <param name="fileName">文件名</param>
        public void ExportExcel(System.Data.DataTable dt, System.Collections.Generic.Dictionary<string, string> fieldList, string directory, string fileName)
        {
            MyExportExcel(directory, fileName, (fiName) =>
            {
                // BaseExportCSV.ExportCSV(dataGridView, dataView, fileName);
                // 2012.04.02 Pcsky 增加新的导出Excel方法，非Com+方式，改用.Net控件
                //BaseExportExcel.ExportXlsByNPOI(dt, fieldList, fiName);
            });
        }
        #endregion

        public void ExportExcel(DataGridView grd, string directory, string fileName)
        {
            ExportExcel(grd, (DataView)(grd.DataSource), directory, fileName);
        }

        #region private void ExportExcel(DataGridView grd, DataView dataView, string directory, string fileName) 导出Excel
        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="dataGridView">表格控件</param>
        /// <param name="dataView">数据表格</param>
        /// <param name="directory">目录</param>
        /// <param name="fileName">文件名</param>
        public void ExportExcel(DataGridView grd, DataView dataView, string directory, string fileName)
        {
            MyExportExcel(directory, fileName, (fiName) =>
            {
                // BaseExportCSV.ExportCSV(dataGridView, dataView, fileName);
                // 2012.04.02 Pcsky 增加新的导出Excel方法，非Com+方式，改用.Net控件
                //BaseExportExcel.ExportXlsByNPOI(grd, dataView, fiName);
            });
        }
        #endregion

        public void ExportExcel(DevExpress.XtraGrid.Views.Grid.GridView grv, string directory, string fileName)
        {
            ExportExcel(grv, (DataView)(grv.DataSource), directory, fileName);
        }

        #region private void ExportExcel(DevExpress.XtraGrid.Views.Grid.GridView grv, DataView dataView, string directory, string fileName) 导出Excel
        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <param name="dataGridView">表格控件</param>
        /// <param name="dataView">数据表格</param>
        /// <param name="directory">目录</param>
        /// <param name="fileName">文件名</param>
        public void ExportExcel(DevExpress.XtraGrid.Views.Grid.GridView grv, DataView dataView, string directory, string fileName)
        {
            MyExportExcel(directory, fileName, (fiName) =>
            {
                // BaseExportCSV.ExportCSV(dataGridView, dataView, fileName);
                // 2012.04.02 Pcsky 增加新的导出Excel方法，非Com+方式，改用.Net控件
                //BaseExportExcel.ExportXlsByNPOI(grv, dataView, fiName);
            });
        }
        #endregion

        private delegate void ExportExcelFun(string fiName);
        private void MyExportExcel(string directory, string fileName, ExportExcelFun fun)
        {
            SaveFileDialog saveFileDialog = GetShowFileDialog(directory, fileName);
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor holdCursor = BeginWait();
                if (!ValidateExport(saveFileDialog, out fileName)) { EndWait(holdCursor); return; }
                fun(fileName);
                EndWait(holdCursor);
                Process.Start(fileName);
            }
        }

        public void Print(DevExpress.XtraGrid.Views.Grid.GridView dop)
        {
            dop.PrintDialog();
        }

        #region private void FormOnClosed() 关闭窗体
        /// <summary>
        /// 关闭窗体
        /// </summary>
        private void FormOnClosed()
        {
            //if (!this.DesignMode)
            //{
            //    // 是否记录访问日志，已经登录了系统了，才记录日志
            //    if (BaseSystemInfo.RecordLog && BaseSystemInfo.UserIsLogOn)
            //    {
            //        // 保存列宽
            //        //BaseInterfaceLogic.SaveDataGridViewColumnWidth(this);
            //        // 调用服务事件
            //        if (this.RecordFormLog)
            //        {
            //            DotNetService dotNetService = new DotNetService();
            //            dotNetService.LogService.WriteExit(UserInfo, this.LogId);
            //            if (dotNetService.LogService is ICommunicationObject)
            //            {
            //                ((ICommunicationObject)dotNetService.LogService).Close();
            //            }
            //        }
            //    }
            //}
        }
        #endregion

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!this.DesignMode)
            {
                // 设置鼠标繁忙状态，并保留原先的状态
                Cursor holdCursor = this.Cursor;
                this.Cursor = Cursors.WaitCursor;
                try
                {
                    this.FormOnClosed();
                }
                catch (Exception ex)
                {
                    this.ProcessException(ex);
                }
                finally
                {
                    // 设置鼠标默认状态，原来的光标状态
                    this.Cursor = holdCursor;
                }
            }
        }

        public void grv_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            // 显示表格序号
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        #region private void DataGridViewOnLoad(object sender, DataGridViewRowPostPaintEventArgs e) DGV加载
        /// <summary>
        /// DataGridView加载
        /// </summary>
        public void DataGridViewOnLoad(DataGridView grd)
        {
            grd.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.DataGridView_RowPostPaint);
        }

        public void DataGridView_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            //序号右对齐
            //Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
            //    e.RowBounds.Location.Y,
            //    (sender as DataGridView).RowHeadersWidth - 4,
            //    e.RowBounds.Height);

            //TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
            //    (sender as DataGridView).RowHeadersDefaultCellStyle.Font,
            //    rectangle,
            //    (sender as DataGridView).RowHeadersDefaultCellStyle.ForeColor,
            //    TextFormatFlags.VerticalCenter | TextFormatFlags.Right);

            // 序号居中
            // 定义一个画笔，颜色用行标题的前景色填充
            SolidBrush solidBrush = new SolidBrush((sender as DataGridView).RowHeadersDefaultCellStyle.ForeColor);
            //得到当前行的行号
            int rowIndex = e.RowIndex + 1;
            //DataGridView的RowHeadersWidth 为了算中间位置
            int rowHeadersWidth = (sender as DataGridView).RowHeadersWidth;
            //根据宽度与显示的字符数计算中间位置
            int rowHeadersX = (rowHeadersWidth - rowIndex.ToString().Length * 6) / 2;
            int rowHeadersY = e.RowBounds.Location.Y + 4;
            e.Graphics.DrawString((rowIndex).ToString(System.Globalization.CultureInfo.CurrentUICulture), (sender as DataGridView).DefaultCellStyle.Font, solidBrush, rowHeadersX, rowHeadersY);
        }
        #endregion

        /// <summary>
        /// 业务数据库部分
        /// </summary>
        private IDbHelper dbHelper = null;

        /// <summary>
        /// 业务数据库部分
        /// </summary>
        protected IDbHelper DbHelper
        {
            get
            {
                if (dbHelper == null)
                {
                    // 当前数据库连接对象
                    dbHelper = DbHelperFactory.GetHelper(BaseSystemInfo.BusinessDbType, BaseSystemInfo.BusinessDbConnection);
                }
                return dbHelper;
            }
        }

        /// <summary>
        /// 工作流数据库部分
        /// </summary>
        private IDbHelper userCenterDbHelper = null;

        /// <summary>
        /// 用户中心数据库部分
        /// </summary>
        protected IDbHelper UserCenterDbHelper
        {
            get
            {
                if (userCenterDbHelper == null)
                {
                    // 当前数据库连接对象
                    userCenterDbHelper = DbHelperFactory.GetHelper(BaseSystemInfo.UserCenterDbType, BaseSystemInfo.UserCenterDbConnection);
                }
                return userCenterDbHelper;
            }
        }

        /// <summary>
        /// 工作流数据库部分
        /// </summary>
        private IDbHelper workFlowDbHelper = null;

        /// <summary>
        /// 工作流数据库部分
        /// </summary>
        protected IDbHelper WorkFlowDbHelper
        {
            get
            {
                if (workFlowDbHelper == null)
                {
                    // 当前数据库连接对象
                    workFlowDbHelper = DbHelperFactory.GetHelper(BaseSystemInfo.WorkFlowDbType, BaseSystemInfo.WorkFlowDbConnection);
                }
                return workFlowDbHelper;
            }
        }

        protected Cursor BeginWait()
        {
            // 设置鼠标繁忙状态，并保留原先的状态
            Cursor holdCursor = this.Cursor;
            this.Cursor = Cursors.WaitCursor;
            return holdCursor;
        }

        protected void EndWait(Cursor holdCursor)
        {
            // 设置鼠标默认状态，原来的光标状态
            this.Cursor = holdCursor;
        }
    }
}