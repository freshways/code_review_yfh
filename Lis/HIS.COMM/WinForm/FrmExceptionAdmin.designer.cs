﻿namespace WEISHENG.COMM.WinForm
{
    partial class FrmExceptionAdmin
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的主键

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用主键编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBatchDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnInvertSelect = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnClearException = new DevExpress.XtraEditors.SimpleButton();
            this.btnExport = new DevExpress.XtraEditors.SimpleButton();
            this.grdException = new DevExpress.XtraGrid.GridControl();
            this.grvException = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThreadName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFormattedMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateBy = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdException)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvException)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBatchDelete
            // 
            this.btnBatchDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatchDelete.Location = new System.Drawing.Point(597, 504);
            this.btnBatchDelete.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnBatchDelete.Name = "btnBatchDelete";
            this.btnBatchDelete.Size = new System.Drawing.Size(74, 23);
            this.btnBatchDelete.TabIndex = 4;
            this.btnBatchDelete.Text = "删除(&D)";
            this.btnBatchDelete.Click += new System.EventHandler(this.btnBatchDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(783, 504);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(74, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "关闭";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnInvertSelect
            // 
            this.btnInvertSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInvertSelect.Location = new System.Drawing.Point(92, 504);
            this.btnInvertSelect.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnInvertSelect.Name = "btnInvertSelect";
            this.btnInvertSelect.Size = new System.Drawing.Size(78, 23);
            this.btnInvertSelect.TabIndex = 2;
            this.btnInvertSelect.Text = "反选";
            this.btnInvertSelect.Click += new System.EventHandler(this.btnInvertSelect_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectAll.Location = new System.Drawing.Point(6, 504);
            this.btnSelectAll.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(78, 23);
            this.btnSelectAll.TabIndex = 1;
            this.btnSelectAll.Text = "全选";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnClearException
            // 
            this.btnClearException.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearException.Location = new System.Drawing.Point(679, 504);
            this.btnClearException.Name = "btnClearException";
            this.btnClearException.Size = new System.Drawing.Size(97, 23);
            this.btnClearException.TabIndex = 5;
            this.btnClearException.Text = "全部清除(&C)";
            this.btnClearException.Click += new System.EventHandler(this.btnClearException_Click);
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(745, 11);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(111, 23);
            this.btnExport.TabIndex = 3;
            this.btnExport.Text = "导出Excel...";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // grdException
            // 
            this.grdException.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdException.Location = new System.Drawing.Point(6, 40);
            this.grdException.MainView = this.grvException;
            this.grdException.Name = "grdException";
            this.grdException.Size = new System.Drawing.Size(849, 458);
            this.grdException.TabIndex = 15;
            this.grdException.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvException});
            this.grdException.DoubleClick += new System.EventHandler(this.grdException_DoubleClick);
            // 
            // grvException
            // 
            this.grvException.Appearance.EvenRow.BackColor = System.Drawing.Color.LemonChiffon;
            this.grvException.Appearance.EvenRow.Options.UseBackColor = true;
            this.grvException.Appearance.FocusedRow.BackColor = System.Drawing.Color.SkyBlue;
            this.grvException.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grvException.Appearance.Row.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvException.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvException.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSelected,
            this.colCreateOn,
            this.colMessage,
            this.colThreadName,
            this.colFormattedMessage,
            this.colCreateBy});
            this.grvException.GridControl = this.grdException;
            this.grvException.IndicatorWidth = 40;
            this.grvException.Name = "grvException";
            this.grvException.OptionsView.ColumnAutoWidth = false;
            this.grvException.OptionsView.EnableAppearanceEvenRow = true;
            this.grvException.OptionsView.EnableAppearanceOddRow = true;
            this.grvException.OptionsView.ShowGroupPanel = false;
            this.grvException.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvException_CustomDrawRowIndicator);
            // 
            // colSelected
            // 
            this.colSelected.AppearanceHeader.Options.UseTextOptions = true;
            this.colSelected.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSelected.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSelected.Caption = "选择";
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colSelected.Visible = true;
            this.colSelected.VisibleIndex = 0;
            this.colSelected.Width = 35;
            // 
            // colCreateOn
            // 
            this.colCreateOn.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreateOn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreateOn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCreateOn.Caption = "发生时间";
            this.colCreateOn.FieldName = "CreateOn";
            this.colCreateOn.Name = "colCreateOn";
            this.colCreateOn.OptionsColumn.ReadOnly = true;
            this.colCreateOn.Visible = true;
            this.colCreateOn.VisibleIndex = 1;
            this.colCreateOn.Width = 120;
            // 
            // colMessage
            // 
            this.colMessage.AppearanceHeader.Options.UseTextOptions = true;
            this.colMessage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMessage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMessage.Caption = "异常信息";
            this.colMessage.FieldName = "Message";
            this.colMessage.Name = "colMessage";
            this.colMessage.OptionsColumn.ReadOnly = true;
            this.colMessage.Visible = true;
            this.colMessage.VisibleIndex = 2;
            this.colMessage.Width = 400;
            // 
            // colThreadName
            // 
            this.colThreadName.AppearanceHeader.Options.UseTextOptions = true;
            this.colThreadName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colThreadName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colThreadName.Caption = "异常信息来源";
            this.colThreadName.FieldName = "ThreadName";
            this.colThreadName.Name = "colThreadName";
            this.colThreadName.OptionsColumn.ReadOnly = true;
            this.colThreadName.Visible = true;
            this.colThreadName.VisibleIndex = 3;
            this.colThreadName.Width = 200;
            // 
            // colFormattedMessage
            // 
            this.colFormattedMessage.AppearanceHeader.Options.UseTextOptions = true;
            this.colFormattedMessage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFormattedMessage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFormattedMessage.Caption = "异常信息描述";
            this.colFormattedMessage.FieldName = "FormattedMessage";
            this.colFormattedMessage.Name = "colFormattedMessage";
            this.colFormattedMessage.OptionsColumn.ReadOnly = true;
            this.colFormattedMessage.Visible = true;
            this.colFormattedMessage.VisibleIndex = 4;
            this.colFormattedMessage.Width = 800;
            // 
            // colCreateBy
            // 
            this.colCreateBy.AppearanceHeader.Options.UseTextOptions = true;
            this.colCreateBy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCreateBy.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCreateBy.Caption = "创建者";
            this.colCreateBy.FieldName = "CreateBy";
            this.colCreateBy.Name = "colCreateBy";
            this.colCreateBy.OptionsColumn.ReadOnly = true;
            this.colCreateBy.Visible = true;
            this.colCreateBy.VisibleIndex = 5;
            this.colCreateBy.Width = 120;
            // 
            // FrmExceptionAdmin
            // 
            this.Appearance.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(134)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(866, 531);
            this.Controls.Add(this.grdException);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnClearException);
            this.Controls.Add(this.btnInvertSelect);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnBatchDelete);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "FrmExceptionAdmin";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Text = "系统异常情况记录";
            ((System.ComponentModel.ISupportInitialize)(this.grdException)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvException)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnBatchDelete;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnInvertSelect;
        private DevExpress.XtraEditors.SimpleButton btnSelectAll;
        private DevExpress.XtraEditors.SimpleButton btnClearException;
        private DevExpress.XtraEditors.SimpleButton btnExport;
        private DevExpress.XtraGrid.GridControl grdException;
        private DevExpress.XtraGrid.Views.Grid.GridView grvException;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateOn;
        private DevExpress.XtraGrid.Columns.GridColumn colMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colThreadName;
        private DevExpress.XtraGrid.Columns.GridColumn colFormattedMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateBy;
    }
}