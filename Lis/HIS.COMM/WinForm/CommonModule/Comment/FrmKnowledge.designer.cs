﻿namespace WEISHENG.COMM.WinForm.CommonModule.Comment
{
    partial class FrmKnowledge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmKnowledge));
            this.txtContents = new DevExpress.XtraEditors.MemoEdit();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.lblDoYouKnow = new System.Windows.Forms.Label();
            this.chkShowKnowledge = new DevExpress.XtraEditors.CheckEdit();
            this.btnPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContents.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowKnowledge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtContents
            // 
            this.txtContents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContents.Enabled = false;
            this.txtContents.Location = new System.Drawing.Point(11, 45);
            this.txtContents.Name = "txtContents";
            this.txtContents.Properties.MaxLength = 200;
            this.txtContents.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtContents.Size = new System.Drawing.Size(582, 188);
            this.txtContents.TabIndex = 1;
            this.txtContents.UseOptimizedRendering = true;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(505, 239);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(87, 27);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "关闭";
            this.btnClose.Click += new System.EventHandler(this.btnColse_Click);
            // 
            // lblDoYouKnow
            // 
            this.lblDoYouKnow.Location = new System.Drawing.Point(74, 9);
            this.lblDoYouKnow.Name = "lblDoYouKnow";
            this.lblDoYouKnow.Size = new System.Drawing.Size(122, 20);
            this.lblDoYouKnow.TabIndex = 0;
            this.lblDoYouKnow.Text = "你知道吗？";
            this.lblDoYouKnow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkShowKnowledge
            // 
            this.chkShowKnowledge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkShowKnowledge.EditValue = true;
            this.chkShowKnowledge.Location = new System.Drawing.Point(9, 247);
            this.chkShowKnowledge.Name = "chkShowKnowledge";
            this.chkShowKnowledge.Properties.Caption = "启动时显示提示";
            this.chkShowKnowledge.Size = new System.Drawing.Size(110, 19);
            this.chkShowKnowledge.TabIndex = 3;
            this.chkShowKnowledge.CheckedChanged += new System.EventHandler(this.chkShowKnowledge_CheckedChanged);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevious.Enabled = false;
            this.btnPrevious.Location = new System.Drawing.Point(319, 239);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(87, 27);
            this.btnPrevious.TabIndex = 2;
            this.btnPrevious.Text = "上一条(&U)";
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(412, 239);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(87, 27);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "下一条(&D)";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(23, 1);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(45, 38);
            this.pictureEdit1.TabIndex = 4;
            // 
            // FrmKnowledge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(605, 279);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.lblDoYouKnow);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.txtContents);
            this.Controls.Add(this.chkShowKnowledge);
            this.Controls.Add(this.btnClose);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmKnowledge";
            this.Text = "日积月累";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmKnowledge_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.txtContents.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowKnowledge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit txtContents;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.CheckEdit chkShowKnowledge;
        private DevExpress.XtraEditors.SimpleButton btnPrevious;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private System.Windows.Forms.Label lblDoYouKnow;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
    }
}