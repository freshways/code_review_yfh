﻿//--------------------------------------------------------------------
// All Rights Reserved , Copyright (C) 2013 , Hairihan TECH, Ltd. 
//--------------------------------------------------------------------

using System;
using System.Data;
using System.Windows.Forms;
using WEISHENG.COMM.Business;
using WEISHENG.COMM.Utilities;

namespace WEISHENG.COMM.WinForm.CommonModule.Comment
{
    /// <summary>
    /// FrmKnowledge.cs
    /// 日积月累
    ///	
    /// 修改记录
    /// 
    ///     2013.05.12 版本：2.0 JiRiGaLa  支持 Keys.PageUp、Keys.PageDown。
    ///     2012.09.03 版本：1.0 JiRiGaLa  显示功能页面编写。
    ///		
    /// <author>
    ///		<name>JiRiGaLa</name>
    ///		<date>2012.09.03</date>
    /// </author> 
    /// </summary>
    public partial class FrmKnowledge : BaseForm
    {
        public FrmKnowledge()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 日积月累的知识库
        /// </summary>
        DataTable dtKnowledge = null;

        /// <summary>
        /// 当前显示第几条
        /// </summary>
        int CurrentIndex = 0;

        #region public override void ShowEntity() 显示内容
        /// <summary>
        /// 显示内容
        /// </summary>
        public override void ShowEntity()
        {
            // 显示信息
			BaseCommentEntity commentEntity = BaseEntity.Create<BaseCommentEntity>(dtKnowledge.Rows[this.CurrentIndex]);
            this.txtContents.Text = commentEntity.Contents;
        }
        #endregion

        public override void SetControlState()
        {
            if (this.dtKnowledge != null && this.dtKnowledge.Rows.Count > 0)
            {
                this.btnNext.Enabled = true;
                if (this.CurrentIndex == this.dtKnowledge.Rows.Count - 1)
                {
                    this.btnNext.Enabled = false;
                }
                this.btnPrevious.Enabled = true;
                if (this.CurrentIndex == 0)
                {
                    this.btnPrevious.Enabled = false;
                }
            }
        }

        #region public override void FormOnLoad() 加载窗体
        /// <summary>
        /// 加载窗体
        /// </summary>
        public override void FormOnLoad()
        {
            // 获取数据
            SQLBuilder sqlBuilder = new SQLBuilder(this.UserCenterDbHelper);
            sqlBuilder.BeginSelect("BaseKnowledge");
            // 只获取前200个数据就可以了，减小网络传递数据的网络带宽。
            sqlBuilder.SelectTop(200);
            if (!string.IsNullOrEmpty(this.EntityId))
            {
                sqlBuilder.SetWhere(BaseCommentEntity.FieldId, this.EntityId);
            }
            // 这里是为了每次显示的数据都是乱序的，数据测循序是被打乱的。
            sqlBuilder.SetOrderByRandom();
            dtKnowledge = sqlBuilder.EndSelect();
            if (dtKnowledge.Rows.Count > 1)
            {
                this.CurrentIndex = new Random().Next(0, dtKnowledge.Rows.Count - 1);

                // 显示实体
                this.ShowEntity();
            }

            // 显示日积月累
            //string showKnowledge = DotNetService.Instance.ParameterService.GetParameter(BaseSystemInfo.UserInfo, "User", this.UserInfo.Id, "ShowKnowledg");
            //if (!string.IsNullOrEmpty(showKnowledge))
            //{
            //    this.chkShowKnowledge.Checked = showKnowledge.Equals(true.ToString());
            //}
        }
        #endregion

        private void FrmKnowledge_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp)
            {
                this.btnPrevious.Focus();
                this.btnPrevious.PerformClick();
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                this.btnNext.Focus();
                this.btnNext.PerformClick();
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (this.CurrentIndex > 0)
            {
                this.CurrentIndex--;
                this.ShowEntity();
            }
            this.SetControlState();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (this.CurrentIndex < this.dtKnowledge.Rows.Count -1)
            {
                this.CurrentIndex++;
                this.ShowEntity();
            }
            this.SetControlState();
        }

        private void chkShowKnowledge_CheckedChanged(object sender, EventArgs e)
        {
            if (this.FormLoaded)
            {
                //DotNetService.Instance.ParameterService.SetParameter(BaseSystemInfo.UserInfo, "User", this.UserInfo.Id, "ShowKnowledg", this.chkShowKnowledge.Checked.ToString());
            }
        }

        private void btnColse_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
