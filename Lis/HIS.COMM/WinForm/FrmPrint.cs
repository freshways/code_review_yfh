﻿//--------------------------------------------------------------------
// All Rights Reserved , Copyright (C) 2013 , Hairihan TECH, Ltd. 
//--------------------------------------------------------------------

using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace WEISHENG.COMM.WinForm
{
    /// <summary>
    /// FormPrint
    /// 窗体打印
    /// 
    /// 修改纪录
    ///
    ///     2007.07.31 版本：1.5 JinGangBo  新增获得本地IP地址与通过域名获得IP地址
    ///     2007.07.30 版本：1.4 JinGangBo  新增设置打印按钮功能。
    ///     2007.07.27 版本：1.3 JinGangBo  实现打印设置，修改横向打印，新增了焦点和ＥＳＣ，界面排布修改
    ///     2007.07.27 版本：1.2 JinGangBo  实现横向打印和纵向打印，打印预览最大化，及自动调及100%，及直接打印。
    ///     2007.07.26 版本：1.1 JinGangBo  代码修改与整理
    ///     2007.07.26 版本：1.0 JinGangBo  使用2种方法进行了窗体打印
    ///     
    /// <author>
    ///		<name>JinGangBo</name>
    ///		<date>2007.07.26</date>
    /// </author>
    /// </summary>
    public partial class FrmPrint : Form
    {
        public FrmPrint()
        {
            InitializeComponent();
        }

        private Bitmap memoryImageOne;
        private Image  memoryImageTwo;

        //新建打印设置
        PrintDocument myPrintDocumentSet = new PrintDocument();
        
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern long BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

        private void GetPrint_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyValue)
            {
                case 118:
                    // 点击了F7按钮
                    this.PrintPageOne();
                    break;
                case 119:
                    // 点击了F8按钮
                    this.PrintPageTwo();
                    break;
                case 67:
                    this.Close();
                    break;  
                case 27:
                    this.Close();
                    break;
            }           
        }

        #region private void PrintPageOne() 打印预览方法一
        /// <summary>
        /// 打印预览
        /// </summary>
        private void PrintPageOne()
        {
            //设置为忙碌状态
            this.Cursor = Cursors.WaitCursor;
            //创建当前屏幕的DC对象
            Graphics mygraphics = this.CreateGraphics();
            Size s = this.Size;
            //创建以当前活动页大小为标准的位图对象 
            memoryImageOne = new Bitmap(s.Width, s.Height, mygraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImageOne);
            //得到屏幕DC
            IntPtr dc1 = mygraphics.GetHdc();
            //得到位图的DC 
            IntPtr dc2 = memoryGraphics.GetHdc();
            BitBlt(dc2, 0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height, dc1, 0, 0, 13369376);
            //释放DC 
            mygraphics.ReleaseHdc(dc1);
            memoryGraphics.ReleaseHdc(dc2);
            //新建打印预览窗体
            PrintPreviewDialog myPrintPreviewDialogOne = new PrintPreviewDialog();
            //新建打印对象
            PrintDocument myPrintDocumentOne = new PrintDocument();
            //新建打印设置
            PageSetupDialog myPageSetupDialogOne = new PageSetupDialog();
            //新建打印输出
            myPrintDocumentOne.PrintPage += new PrintPageEventHandler(myPrintDocumentOne_PrintPage);
            //获取打印预览
            myPrintPreviewDialogOne.Document = myPrintDocumentOne;
            //将预揽调制100%
            myPrintPreviewDialogOne.PrintPreviewControl.Zoom = 1.0;
            //将预览窗体最大化
            ((System.Windows.Forms.Form)myPrintPreviewDialogOne).WindowState = FormWindowState.Maximized;
            //打开打印预览窗口
            myPrintPreviewDialogOne.ShowDialog();
            //设置鼠标默认状态
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region private void PrintPageTwo() 打印预览方法二
        /// <summary>
        /// 打印预览
        /// </summary>
        private void PrintPageTwo()
        {
            //设置为忙碌状态
            this.Cursor = Cursors.WaitCursor;
            //获得ALT+PRINT热键
            //SendKeys.SendWait("%{PRTSC}");
            //新建打印预览窗体 
            PrintPreviewDialog myPrintPreviewDialogTwo = new PrintPreviewDialog();
            //新建打印对象
            PrintDocument myPrintDocumentTwo = new PrintDocument();
            //新建打印输出 
            myPrintDocumentTwo.PrintPage += new PrintPageEventHandler(myPrintDocumentTwo_PrintPage);
            //新建打印设置
            PageSetupDialog myPageSetupDialogTwo = new PageSetupDialog();
            //获取打印预览
            myPrintPreviewDialogTwo.Document = myPrintDocumentTwo;
            //将预揽调制100%
            myPrintPreviewDialogTwo.PrintPreviewControl.Zoom = 1.0;
            //将预览窗体最大化
            ((System.Windows.Forms.Form)myPrintPreviewDialogTwo).WindowState = FormWindowState.Maximized;
            //打开打印预览窗口
            myPrintPreviewDialogTwo.ShowDialog();           
            //设置鼠标默认状态
            this.Cursor = Cursors.Default;
        }
        #endregion            

        #region private void PrintPageImmediacy() 直接打印
        /// <summary>
        /// 直接打印
        /// </summary>
        private void PrintPageImmediacy()
        {
            //设置为忙碌状态
            this.Cursor = Cursors.WaitCursor;
            //获得ALT+PRINT热键
            //SendKeys.SendWait("%{PRTSC}");
            //新建打印预览窗体 
            PrintPreviewDialog myPrintPreviewDialogTwo = new PrintPreviewDialog();
            //新建打印对象
            PrintDocument myPrintDocumentTwo = new PrintDocument();
            //新建打印输出 
            myPrintDocumentTwo.PrintPage += new PrintPageEventHandler(myPrintDocumentTwo_PrintPage);
            //新建打印设置
            PageSetupDialog myPageSetupDialogTwo = new PageSetupDialog();
            //获取打印预览
            myPrintPreviewDialogTwo.Document = myPrintDocumentTwo;
            //将预揽调制100%
            myPrintPreviewDialogTwo.PrintPreviewControl.Zoom = 1.0;
            //直接打印
            myPrintDocumentTwo.Print();
            //设置鼠标默认状态
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region private void PrintSet() 打印设置
        /// <summary>
        /// 打印设置
        /// </summary>
        private void PrintSet()
        {
            //设置为忙碌状态
            this.Cursor = Cursors.WaitCursor;
            //获得ALT+PRINT热键
            //SendKeys.SendWait("%{PRTSC}");
            //新建打印输出 
            myPrintDocumentSet.PrintPage += new PrintPageEventHandler(myPrintDocumentTwo_PrintPage);
            //新建打印设置
            PageSetupDialog myPageSetupDialog = new PageSetupDialog();
            //在打印设置中填充图象            
            myPageSetupDialog.Document = myPrintDocumentSet;
            myPageSetupDialog.PageSettings.Landscape = true;
            myPageSetupDialog.ShowDialog(this);
             //保存设置
            myPrintDocumentSet.DefaultPageSettings = myPageSetupDialog.PageSettings;
            myPrintDocumentSet.PrinterSettings = myPageSetupDialog.PrinterSettings;
            //设置鼠标默认状态
            this.Cursor = Cursors.Default;
        }
        #endregion 

        private void myPrintDocumentOne_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (radiobtnvertinal.Checked == true)
            {
                //绘制打印预览方法一
                e.Graphics.DrawImage(memoryImageOne, 0, 0);
            }
            else 
            {
                //图片旋转９０
                memoryImageOne.RotateFlip(RotateFlipType.Rotate90FlipXY);
                e.Graphics.DrawImage(memoryImageOne, 0, 0);
            }
        }

        private void myPrintDocumentTwo_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //从剪切板中获得图片
            memoryImageTwo = Clipboard.GetImage();
            if (radiobtnvertinal.Checked == true)
            {
                //绘制打印预览方法一
                e.Graphics.DrawImage(memoryImageTwo, 0, 0);
            }
            else
            {
                //图片旋转９０
                memoryImageTwo.RotateFlip(RotateFlipType.Rotate90FlipXY);
                e.Graphics.DrawImage(memoryImageTwo, 0, 0);
            }          
        }

        private void btnPirntSet_Click(object sender, EventArgs e)
        {
            this.PrintSet();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            myPrintDocumentSet.Print();
        }     

        private void btnPrintNow_Click(object sender, EventArgs e)
        {
            this.PrintPageImmediacy();
        }

        private void btnPrintPageOne_Click_1(object sender, EventArgs e)
        {
            this.PrintPageOne();
        }

        private void btnPrintPageTwo_Click_1(object sender, EventArgs e)
        {
            this.PrintPageTwo();
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormPrint_Load(object sender, EventArgs e)
        {
            btnCancel.Focus();
        }

        private void BtnLocalIP_Click(object sender, EventArgs e)
        {
            //取得本地的机器名
            string strHostName = System.Net.Dns.GetHostName();
            //根据字符串型的主机名称,得到IP地址
            //IP获得过时写法[否决的]
            //System.Net.IPHostEntry myHostinfo = System.Net.Dns.GetHostByName(strHostName);
　　　　　　//IP最新获得方法　　
            System.Net.IPHostEntry myHostinfo = System.Net.Dns.GetHostEntry(strHostName);
            System.Net.IPAddress myIpAddress = myHostinfo.AddressList[0];
            DevExpress.XtraEditors.XtraMessageBox.Show(myIpAddress.ToString());
        }

        private void btnFieldIP_Click(object sender, EventArgs e)
        {
            //域名IP获得过时写法[否决的]
            //string strIP = System.Net.Dns.GetHostByName("www.sina.com.cn").AddressList[0].ToString();
            string strIP = System.Net.Dns.GetHostEntry("www.sina.com.cn").AddressList[0].ToString();
            DevExpress.XtraEditors.XtraMessageBox.Show(strIP);
        }
    }
}