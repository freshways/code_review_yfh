﻿using DevExpress.XtraEditors;
using HIS.COMM.Helper;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WEISHENG.COMM;

namespace HIS.COMM
{
    public partial class XtraForm病人选择2 : DevExpress.XtraEditors.XtraForm
    {
        public Pojo病人信息 pojo选择病人;
        public XtraForm病人选择2(ClassEnum.en病人选择类型 _en选择类型, string _s住院号)
        {
            InitializeComponent();
            if (string.IsNullOrWhiteSpace(HIS.COMM.zdInfo.s科室数据权限))
            {
                msgHelper.ShowInformation("用户未设置科室数据权限");
                return;
            }
            switch (_en选择类型)
            {

                case ClassEnum.en病人选择类型.区分分院单病人:
                    gridControl1.DataSource = HIS.COMM.BLL.bll病人选择.get区分科室权限含子病人单病人(HIS.COMM.zdInfo.s科室数据权限,
                        HIS.COMM.zdInfo.Model站点信息.分院编码, _s住院号, ClassEnum.en在院状态.正常);
                    break;
                case ClassEnum.en病人选择类型.区分分院单病人含出院:
                    gridControl1.DataSource = HIS.COMM.BLL.bll病人选择.get区分科室权限含子病人单病人(HIS.COMM.zdInfo.s科室数据权限,
                        HIS.COMM.zdInfo.Model站点信息.分院编码, _s住院号, ClassEnum.en在院状态.未定义);
                    break;

                case ClassEnum.en病人选择类型.区分科室权限单病人:
                    break;
                case ClassEnum.en病人选择类型.区分科室权限含子病人:
                    gridControl1.DataSource = HIS.COMM.BLL.bll病人选择.get区分科室权限含子病人(HIS.COMM.zdInfo.s科室数据权限, HIS.COMM.zdInfo.Model站点信息.分院编码);
                    break;
                case ClassEnum.en病人选择类型.出院申请单病人:
                    break;
                default:
                    break;
            }
            HIS.COMM.frmGridCustomize.RegisterGrid(gridView1);
            gridView1.BestFitColumns();
        }

        public XtraForm病人选择2(ClassEnum.en病人选择类型 _en选择类型)
        {
            InitializeComponent();
            if (string.IsNullOrWhiteSpace(HIS.COMM.zdInfo.s科室数据权限))
            {
                msgHelper.ShowInformation("用户未设置科室数据权限");                
                return;
            }
            switch (_en选择类型)
            {
                case ClassEnum.en病人选择类型.区分分院:
                    gridControl1.DataSource = HIS.COMM.BLL.bll病人选择.get区分科室权限含子病人(HIS.COMM.zdInfo.s科室数据权限, HIS.COMM.zdInfo.Model站点信息.分院编码);
                    break;
                case ClassEnum.en病人选择类型.区分分院单病人含出院:
                    msgHelper.ShowInformation("未传递住院号码");
                    break;
                case ClassEnum.en病人选择类型.区分科室权限:
                case ClassEnum.en病人选择类型.区分科室权限含子病人:
                    gridControl1.DataSource = HIS.COMM.BLL.bll病人选择.get区分科室权限含子病人(HIS.COMM.zdInfo.s科室数据权限, HIS.COMM.zdInfo.Model站点信息.分院编码);
                    break;

                case ClassEnum.en病人选择类型.出院申请:
                    gridControl1.DataSource = HIS.COMM.BLL.bll病人选择.Get出院申请病人(HIS.COMM.zdInfo.Model站点信息.分院编码);
                    break;
                case ClassEnum.en病人选择类型.出院申请单病人:
                    break;
                default:
                    break;
            }
            HIS.COMM.frmGridCustomize.RegisterGrid(gridView1);
            gridView1.BestFitColumns();
        }

        public XtraForm病人选择2(List<Pojo病人信息> pojos)
        {
            InitializeComponent();
            gridControl1.DataSource = pojos;
            HIS.COMM.frmGridCustomize.RegisterGrid(gridView1);
            gridView1.BestFitColumns();
        }


        private void v刷新数据(ClassEnum.en病人选择类型 _en选择类型, Pojo病人信息 _person)
        {
            try
            {
                using (var chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    //基础通用部分    按个人数据权限，按分院编码，包含子病人列表
                    var query0 = (chis.ZY子病人信息.Where(c => c.分院编码 == HIS.COMM.zdInfo.Model站点信息.分院编码).Join(chis.ZY病人信息, c => c.父ZYID, c => c.ZYID, (a, list) => new
                    {
                        a.住院号码,
                        a.出生日期,
                        a.医疗证号,
                        a.子ZYID,
                        a.性别,
                        a.父ZYID,
                        a.病人姓名,
                        a.身份证号,
                        list.分院编码,
                        list.在院状态,
                        list.已出院标记,
                        list.住院次数,
                        list.病床,
                        list.主治医生编码,
                        list.疾病编码,
                        list.疾病名称,
                        list.ZYID,
                        list.科室,
                        list.病区
                    })).Concat(chis.ZY病人信息.Where(c => c.分院编码 == HIS.COMM.zdInfo.Model站点信息.分院编码).Select(a => new
                    {
                        a.住院号码,
                        a.出生日期,
                        a.医疗证号,
                        子ZYID = a.ZYID,
                        a.性别,
                        父ZYID = a.ZYID,
                        a.病人姓名,
                        a.身份证号,
                        a.分院编码,
                        a.在院状态,
                        a.已出院标记,
                        a.住院次数,
                        a.病床,
                        a.主治医生编码,
                        a.疾病编码,
                        a.疾病名称,
                        a.ZYID,
                        a.科室,
                        a.病区
                    })).Join(chis.pub用户数据权限.Where(c => c.用户编码 == 0), c => c.病区, c => c.科室编码, (a, list) => new
                    {
                        a.住院号码,
                        a.出生日期,
                        a.医疗证号,
                        a.子ZYID,
                        a.性别,
                        a.父ZYID,
                        a.病人姓名,
                        a.身份证号,
                        a.分院编码,
                        a.在院状态,
                        a.已出院标记,
                        a.住院次数,
                        a.病床,
                        a.主治医生编码,
                        a.疾病编码,
                        a.疾病名称,
                        a.ZYID,
                        a.科室,
                        a.病区
                    });



                    var query = query0.Join(chis.GY科室设置, c => c.病区, c => c.科室编码, (a, list) => new
                    {
                        a.子ZYID,
                        a.父ZYID,
                        病区编码 = a.病区,
                        a.分院编码,
                        a.在院状态,
                        a.已出院标记,
                        a.住院号码,
                        a.住院次数,
                        a.病床,
                        a.性别,
                        a.出生日期,
                        a.主治医生编码,
                        a.疾病编码,
                        a.疾病名称,
                        a.ZYID,
                        a.病人姓名,
                        病区名称 = list.科室名称,
                        a.科室
                    })
                           .Join(chis.GY科室设置, c => c.科室, c => c.科室编码, (a, list) => new
                           {
                               a.子ZYID,
                               a.父ZYID,
                               a.病区编码,
                               a.分院编码,
                               a.在院状态,
                               a.已出院标记,
                               a.住院号码,
                               a.住院次数,
                               a.主治医生编码,
                               a.出生日期,
                               a.疾病编码,
                               a.疾病名称,
                               a.ZYID,
                               a.病人姓名,
                               a.病区名称,
                               科室名称 = list.科室名称
                           })
                         .GroupJoin(chis.ZY在院费用, c => c.ZYID, s => s.ZYID, (a, list) => new
                         {
                             a.子ZYID,
                             a.父ZYID,
                             a.病区编码,
                             a.分院编码,
                             a.在院状态,
                             a.已出院标记,
                             a.住院号码,
                             a.住院次数,
                             a.出生日期,
                             a.主治医生编码,
                             a.疾病编码,
                             a.疾病名称,
                             a.病区名称,
                             a.科室名称,
                             a.ZYID,
                             a.病人姓名,
                             总费用 = list.Sum(c => c.金额)
                         })
                         .GroupJoin(chis.ZY预交款, c => c.ZYID, c => c.ZYID, (a, list) => new
                         {
                             a.子ZYID,
                             a.父ZYID,
                             a.病区编码,
                             a.分院编码,
                             a.在院状态,
                             a.已出院标记,
                             a.住院号码,
                             a.住院次数,
                             a.出生日期,
                             a.主治医生编码,
                             a.疾病编码,
                             a.疾病名称,
                             a.病区名称,
                             a.科室名称,
                             a.ZYID,
                             a.病人姓名,
                             a.总费用,
                             预交款 = list.Sum(c => c.交款额),
                             欠款 = a.总费用 - list.Sum(c => c.交款额)
                         })
                         .GroupJoin(chis.pubUsers, c => c.主治医生编码, c => c.用户编码, (a, list) => new
                         {
                             a.子ZYID,
                             a.父ZYID,
                             a.病区编码,
                             a.分院编码,
                             a.在院状态,
                             a.已出院标记,
                             a.住院号码,
                             a.住院次数,
                             a.出生日期,
                             a.疾病编码,
                             a.疾病名称,
                             a.病区名称,
                             a.科室名称,
                             a.ZYID,
                             a.病人姓名,
                             a.总费用,
                             a.预交款,
                             a.欠款,
                             主治医师姓名 = list.FirstOrDefault().用户名
                         });


                    switch (_en选择类型)
                    {
                        case HIS.COMM.ClassEnum.en病人选择类型.区分分院单病人含出院:
                            //sqlWhere = "  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.住院号码= " + _person.s住院号码 + " and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                            var query_dbr_cy = query.Where(c => c.住院号码 == _person.住院号码);
                            break;
                        case HIS.COMM.ClassEnum.en病人选择类型.区分分院:
                            query = query.Where(c => c.已出院标记 == false && c.在院状态 == "正常");
                            break;
                        case HIS.COMM.ClassEnum.en病人选择类型.区分科室权限:
                            //默认构造，不需要特殊处理
                            //sqlWhere = " (br.已出院标记 = 0) and  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                            break;
                        case HIS.COMM.ClassEnum.en病人选择类型.区分科室权限单病人:
                            //sqlWhere = " (br.已出院标记 = 0) and  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.住院号码= " + _person.s住院号码 +
                            //    " and br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                            query = query.Where(c => c.住院号码 == _person.住院号码);
                            break;
                        case HIS.COMM.ClassEnum.en病人选择类型.区分分院单病人:
                            query = query.Where(c => c.已出院标记 == false && c.在院状态 == "正常" && c.住院号码 == _person.住院号码);
                            //sqlWhere = " (br.已出院标记 = 0) and br.在院状态='正常' and br.住院号码= " + _person.s住院号码 + " and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                            break;

                        case HIS.COMM.ClassEnum.en病人选择类型.区分科室权限含子病人:
                            //sqlWhere =
                            //" (br.已出院标记 = 0) and  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码 +
                            //" union all SELECT zbr.子ZYID, zbr.住院号码, zbr.病人姓名,br.疾病名称,br.[疾病编码],br.入院时间," + "\r\n" +
                            //"       bq.科室名称 病区名称, br.病床 病床名称, zbr.性别," + "\r\n" +
                            //"       DATEDIFF(year, zbr.出生日期, getdate()) 年龄,  br.单位编码," + "\r\n" +
                            //"       br.在院状态, br.主治医生编码, br.[病区] 病区编码," + "\r\n" +
                            //"       br.科室 科室编码, br.[家庭地址], isnull(预交款, 0) 预交款," + "\r\n" +
                            //"       isnull(fy.总费用, 0) 总费用, isnull(fy.[总费用], 0) - isnull(yjk.预交款, 0) 欠款, br.[分院编码]," + "\r\n" +
                            //"       ys.用户名 主治医生姓名, ks.科室名称, br.医保类型,zbr.父ZYID" + "\r\n" +
                            //"FROM   zy子病人信息 zbr" + "\r\n" +
                            //"       left outer join ZY病人信息 AS br on zbr.[父ZYID] = br.ZYID" + "\r\n" +
                            //"       LEFT OUTER JOIN (select   zyid, ISNULL(SUM(交款额), 0) AS 预交款" + "\r\n" +
                            //"                        from     ZY预交款" + "\r\n" +
                            //"                        group by zyid) AS yjk" + "\r\n" +
                            //"         ON br.ZYID = yjk.ZYID" + "\r\n" +
                            //"       left outer join gy科室设置 bq on br.病区 = bq.科室编码" + "\r\n" +
                            //"       left outer join gy科室设置 ks on br.科室 = ks.科室编码" + "\r\n" +
                            //"       left outer join (select   zyid, isnull(sum(金额), 0) 总费用" + "\r\n" +
                            //"                        from     [ZY在院费用]" + "\r\n" +
                            //"                        group by zyid) fy" + "\r\n" +
                            //"         on br.zyid = fy.zyid" + "\r\n" +
                            //"       left outer join pubuser ys on br.主治医生编码 = ys.[用户编码]" + "\r\n" +
                            //" where  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                            var query2 = query.Where(c => c.已出院标记 == false && c.在院状态 == "正常");
                            Console.Write(query2.ToString());
                            var querylist2 = (from item in query2.ToList()
                                              select new Pojo病人信息
                                              {
                                                  住院号码 = item.住院号码,
                                                  住院次数 = Convert.ToInt32(item.住院次数),
                                                  年龄 = Convert.ToByte(AgeHelper.GetAgeByBirthdate(Convert.ToDateTime(item.出生日期))),
                                                  疾病编码 = item.疾病编码,
                                                  疾病名称 = item.疾病名称,
                                                  病区名称 = item.病区名称,
                                                  科室名称 = item.科室名称,
                                                  ZYID = item.ZYID,
                                                  病人姓名 = item.病人姓名,
                                                  总费用 = Convert.ToDecimal(item.总费用),
                                                  预交款 = Convert.ToDecimal(item.预交款),
                                                  欠款 = Convert.ToDecimal(item.欠款),
                                                  主治医生姓名 = item.主治医师姓名
                                              });
                            this.gridControl1.DataSource = querylist2;
                            HIS.COMM.frmGridCustomize.RegisterGrid(gridView1);
                            gridView1.BestFitColumns();
                            return;
                        case HIS.COMM.ClassEnum.en病人选择类型.出院申请:
                            query = query.Where(c => c.已出院标记 == false && c.在院状态 == "出院申请");
                            //sqlWhere = "  (br.已出院标记 = 0)  and (br.在院状态 = '出院申请') AND br.分院编码 = " + HIS.COMM.zdInfo.Model站点信息.分院编码;
                            break;
                        case HIS.COMM.ClassEnum.en病人选择类型.出院申请单病人:
                            //sqlWhere = "  (br.已出院标记 = 0)  and (br.在院状态 = '出院申请') AND br.分院编码 = " + HIS.COMM.zdInfo.Model站点信息.分院编码 + " and br.住院号码= " + _person.s住院号码;
                            query = query.Where(c => c.已出院标记 == false && c.在院状态 == "出院申请" && c.住院号码 == _person.住院号码);
                            break;
                    }

                    var querylist = (from item in query.ToList()
                                     select new Pojo病人信息
                                     {
                                         住院号码 = item.住院号码,
                                         住院次数 = Convert.ToInt32(item.住院次数),
                                         年龄 = Convert.ToByte(AgeHelper.GetAgeByBirthdate(Convert.ToDateTime(item.出生日期))),
                                         疾病编码 = item.疾病编码,
                                         疾病名称 = item.疾病名称,
                                         病区名称 = item.病区名称,
                                         科室名称 = item.科室名称,
                                         ZYID = item.ZYID,
                                         病人姓名 = item.病人姓名,
                                         总费用 = Convert.ToDecimal(item.总费用),
                                         预交款 = Convert.ToDecimal(item.预交款),
                                         欠款 = Convert.ToDecimal(item.欠款),
                                         主治医生姓名 = item.主治医师姓名
                                     });


                    HIS.COMM.frmGridCustomize.RegisterGrid(gridView1);
                    gridView1.BestFitColumns();
                }
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                {
                    return;
                }
                if (gridView1.DataRowCount == 0)
                {
                    return;
                }
                else
                {
                    pojo选择病人 = gridView1.GetFocusedRow() as Pojo病人信息;


                    if (Convert.ToDecimal(pojo选择病人.欠款) >= baseInfo.dec医院欠款下限)
                    {
                        MessageBox.Show("【" + pojo选择病人.病人姓名 + "】的当前欠款【" + pojo选择病人.欠款 + "】，" +
                            "\r\n大于医院设定的欠款下限【" + baseInfo.dec医院欠款下限.ToString() + "】。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                simpleButton确定_Click(null, null);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            simpleButton确定_Click(null, null);
        }

        private void XtraForm病人选择_Load(object sender, EventArgs e)
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                chis.Database.ExecuteSqlCommand("update zy病人信息 set 在院状态 = '已出院' where 已出院标记 = 1 and 在院状态<>'已出院'");
                chis.SaveChanges();
            }            
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {

        }

        private void XtraForm病人选择_Activated(object sender, EventArgs e)
        {
            //if (gridView1.DataRowCount == 1)
            //{
            //    gridView1.FocusedRowHandle = 0;
            //    simpleButton确定_Click(null, null);
            //}
        }


    }

}