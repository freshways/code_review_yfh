﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM
{
    public class Person门诊病人 : HIS.COMM.person医卡通
    {
        private int _i床位编码 = 0;

        public int i床位编码
        {
            get { return _i床位编码; }
            set { _i床位编码 = value; }
        }

        private string s处方号;
        public string S处方号
        {
            get { return s处方号; }
            set { s处方号 = value; }
        }


        private string s临床诊断;
        public string S临床诊断
        {
            get { return s临床诊断; }
            set { s临床诊断 = value; }
        }

        private string s临床诊断ICD码;
        public string S临床诊断ICD码
        {
            get { return s临床诊断ICD码; }
            set { s临床诊断ICD码 = value; }
        }

        private decimal dec总金额;

        public decimal Dec总金额
        {
            get { return Math.Round(dec总金额, 2); }
            set { dec总金额 = value; }
        }

        private DateTime dtime日期;

        public DateTime Dtime日期
        {
            get { return dtime日期; }
            set { dtime日期 = value; }
        }

    }
}
