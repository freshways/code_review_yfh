﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.IO;
using WEISHENG.COMM;
using HIS.Model;

namespace HIS.COMM
{
    public partial class XtraForm通知公告查看 : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        private string sConn = "";
        pubnoteic _pubnoteic;
        public XtraForm通知公告查看(string _sConn)
        {
            InitializeComponent();
            sConn = _sConn;
        }
        public XtraForm通知公告查看(pubnoteic pubnoteic)
        {
            InitializeComponent();
            _pubnoteic = pubnoteic;
        }
        public string colID;
        private void XtraForm通知公告查看_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(sConn))
                {
                    var noteic = chis.pubnoteics.Where(c => c.通知GUID == _pubnoteic.通知GUID).FirstOrDefault();
                    labelc标题.Text += noteic.title;
                    labelC程度.Text += noteic.ctype;
                    labelC时间.Text += noteic.createdate.ToString();
                    richEditControl1.HtmlText = noteic.content;
                    if (string.IsNullOrEmpty(noteic.附件文件名))
                    {
                        layoutControlItem下载附件.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }
                }
                else
                {
                    string RESULT = WEISHENG.COMM.Helper.db_connectHelper.get连接串IP(sConn);
                    if (WEISHENG.COMM.netHelper.IsInNetOk(RESULT) == false)
                    {
                        MessageBox.Show("通知公告服务器【" + RESULT + "】无法正常访问", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    string strSql = "";
                    if (!string.IsNullOrEmpty(colID))
                    {
                        strSql = "select [iid], [ctype], [title], [content], [createuser], [createdate],附件文件名  from pubnoteic where iid='" + colID + "'";
                    }
                    else
                    {
                        strSql = "select top 1 [iid], [ctype], [title], [content], [createuser], [createdate],附件文件名  from pubnoteic order by iid desc";
                    }
                    DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(sConn, CommandType.Text, strSql).Tables[0];
                    labelc标题.Text += dt.Rows[0]["title"].ToString();
                    labelC程度.Text += dt.Rows[0]["ctype"].ToString();
                    labelC时间.Text += dt.Rows[0]["createdate"].ToString();
                    richEditControl1.HtmlText = dt.Rows[0]["content"].ToString();
                    if (dt.Rows[0]["附件文件名"].ToString() == "")
                    {
                        layoutControlItem下载附件.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void simpleButton下载附件_Click(object sender, EventArgs e)
        {
            try
            {
                string path = "";
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
                folderBrowserDialog.ShowNewFolderButton = false;
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    path = folderBrowserDialog.SelectedPath;
                }
                else
                {
                    return;
                }

                byte[] bt附件内容 = null;
                string s附件名 = "";
                string sqlCmd = "select 附件内容,附件文件名 from pubnoteic where iid='" + colID + "'";
                SqlConnection mySQLconnection = new SqlConnection(sConn);
                mySQLconnection.Open();
                SqlCommand mycommand = new SqlCommand(sqlCmd, mySQLconnection);
                SqlDataReader mySQLreader = mycommand.ExecuteReader();
                if (mySQLreader.HasRows == false)
                {
                    //sError = "获取新的升级组件失败!";
                    //return false;
                }
                if (mySQLreader.Read())
                {
                    bt附件内容 = (byte[])mySQLreader[0];
                    s附件名 = (string)mySQLreader[1];
                }
                mySQLreader.Close();
                mySQLconnection.Close();
                string tempPathFile = path + "\\" + s附件名;
                FileStream myfs = new FileStream(tempPathFile, FileMode.CreateNew);
                BinaryWriter writefile = new BinaryWriter(myfs);
                writefile.Write(bt附件内容, 0, bt附件内容.Length);
                writefile.Close();
                myfs.Close();
                XtraMessageBox.Show("下载成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {

        }
    }
}