﻿using DevExpress.XtraEditors;
using System;
using System.Windows.Forms;

namespace HIS.COMM
{
    public partial class XtraForm操作验证 : DevExpress.XtraEditors.XtraForm
    {

        public XtraForm操作验证()
        {
            InitializeComponent();
        }
        private void btnYes_Click(object sender, EventArgs e)
        {
            if (txtMM1.Text != HIS.COMM.zdInfo.Model单位信息.iDwid.ToString())
            {
                XtraMessageBox.Show("请录入正确的验证一级密码", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (txtMM2.Text != DateTime.Now.ToShortDateString().Replace("-", "").Replace("/", ""))
            {
                XtraMessageBox.Show("请录入正确的验证二级密码", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            XtraMessageBox.Show("密码验证通过，请谨慎操作", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
        }
        private void btnCanel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}