﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HIS.Model;
using HIS.Model.Pojo;

namespace WEISHENG.COMM
{
    public class zdInfo2站点实例
    {
        private static zdInfo2站点实例 instance;
        private static readonly object syncRoot = new object();
        private  Computer _HardInfo;
        private  GY站点设置 _Model站点信息;

        private zdInfo2站点实例() 
        {
           
        }
        public static zdInfo2站点实例 GetInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                    {
                        instance = new zdInfo2站点实例();
                    }
                }
            }
            return instance;
        }
        public  Computer HardInfo
        {
            get
            {
                return _HardInfo;
            }
            set
            {
                _HardInfo = value;
            }
        }
        public GY站点设置 Model站点信息
        {
            get
            {
                return _Model站点信息;
            }

            set
            {
                _Model站点信息 = value;
            }
        }

        public void init()
        {
            HardInfo = new Computer();
        }
    }



    //class Singleton懒汉式
    //{
    //    private static Singleton懒汉式 instance;
    //    private static readonly object syncRoot = new object();//程序运行时创建一个静态只读的进程辅助对象
    //    private Singleton懒汉式()
    //    {
    //    }
    //    public static Singleton懒汉式 GetInstance()
    //    {
    //        if (instance == null)
    //        {
    //            lock (syncRoot)//在同一时刻加了锁的那部分程序，只有一个线程可以进入
    //            {
    //                if (instance == null)
    //                {
    //                    instance = new Singleton懒汉式();
    //                }
    //            }
    //        }
    //        return instance;
    //    }
    //}
}
