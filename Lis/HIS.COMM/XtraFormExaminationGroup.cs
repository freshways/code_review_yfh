﻿using DevExpress.XtraTreeList;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace HIS.COMM
{

    public partial class XtraFormExaminationGroup : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public List<PojoExaminationGroup> selectedGroup = new List<PojoExaminationGroup>();
        List<GY套餐明细> ExaminationGroupContentes;

        public XtraFormExaminationGroup()
        {
            InitializeComponent();
        }
        private void XtraForm套餐选择_Load(object sender, EventArgs e)
        {
            try
            {
                string sql = $@"select 编号 KeyFieldName, -1 ParentFieldName,套餐名称,编号 套餐编号,isnull(显示顺序,999) 显示顺序
                                from   gy套餐摘要
                                where 创建人 = '{HIS.COMM.zdInfo.ModelUserInfo.用户名}' and 数据类型 = '个人'
                                union all
                                select 编号 KeyFieldName, -2 ParentFieldName,套餐名称,编号,isnull(显示顺序,999) 显示顺序
                                from   gy套餐摘要
                                where 科室编码 = '{HIS.COMM.zdInfo.Model科室信息.科室编码}' and 数据类型 = '科室'
                                union all
                                select 编号 KeyFieldName, -3 ParentFieldName,套餐名称,编号,isnull(显示顺序,999) 显示顺序
                                from   gy套餐摘要
                                where 数据类型 = '全院'";
                PojoExaminationGroup item1 = new PojoExaminationGroup()
                {
                    KeyFieldName = -1,
                    ParentFieldName = 0,
                    套餐名称 = "本人",
                    套餐编号 = -1
                };
                PojoExaminationGroup item2 = new PojoExaminationGroup()
                {
                    KeyFieldName = -2,
                    ParentFieldName = 0,
                    套餐名称 = "本科",
                    套餐编号 = -1
                };
                PojoExaminationGroup item3 = new PojoExaminationGroup()
                {
                    KeyFieldName = -3,
                    ParentFieldName = 0,
                    套餐名称 = "全院",
                    套餐编号 = -1
                };
                List<HIS.Model.Pojo.PojoExaminationGroup> listGroups = new List<PojoExaminationGroup>();
                listGroups.Add(item1);
                listGroups.Add(item2);
                listGroups.Add(item3);

                var list1 = chis.Database.SqlQuery<PojoExaminationGroup>(sql).OrderBy(c => c.显示顺序).ToList();
                listGroups.AddRange(list1);
                treeListExaminationGroup.KeyFieldName = "KeyFieldName";
                treeListExaminationGroup.ParentFieldName = "ParentFieldName";
                treeListExaminationGroup.ExpandAll();
                treeListExaminationGroup.DataSource = listGroups;
                treeListExaminationGroup.Columns["套餐编号"].Visible = false;
                treeListExaminationGroup.Columns["显示顺序"].Visible = false;
                treeListExaminationGroup.ExpandAll();

                this.searchControlFilter.Client = this.treeListExaminationGroup;
                treeListExaminationGroup.OptionsBehavior.EnableFiltering = true;
                //过滤模式，枚举1.Default 2.Extended（推荐） 3.Smart（推荐） 4.Standard
                treeListExaminationGroup.OptionsFilter.FilterMode = FilterMode.Smart;
                treeListExaminationGroup.FilterNode += treeList1_FilterNode;
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }

        }
        private void treeList1_FilterNode(object sender, DevExpress.XtraTreeList.FilterNodeEventArgs e)
        {
            if (treeListExaminationGroup.DataSource == null) return;
            string NodeText = e.Node.GetDisplayText("套餐名称");//参数填写FieldName
            if (string.IsNullOrWhiteSpace(NodeText)) return;
            bool IsVisible = NodeText.ToUpper().IndexOf(searchControlFilter.Text.ToUpper()) >= 0;
            if (IsVisible)
            {
                DevExpress.XtraTreeList.Nodes.TreeListNode Node = e.Node.ParentNode;
                while (Node != null)
                {
                    if (!Node.Visible)
                    {
                        Node.Visible = true;
                        Node = Node.ParentNode;
                    }
                    else
                        break;
                }
            }
            e.Node.Visible = IsVisible;
            e.Handled = true;
        }
        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void treeListExaminationGroup_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            var model = treeListExaminationGroup.GetDataRecordByNode(e.Node) as PojoExaminationGroup;
            refreshContent(model.套餐编号);
        }
        void refreshContent(int bh)
        {
            ExaminationGroupContentes = chis.GY套餐明细.Where(c => c.套餐编号 == bh).ToList();
            gridControlExaminationGroup.DataSource = ExaminationGroupContentes;
            gridViewExaminationGroup.PopulateColumns();
            gridViewExaminationGroup.Columns["IsChanged"].Visible = false;
            gridViewExaminationGroup.Columns["ID"].Visible = false;
            gridViewExaminationGroup.Columns["是否药品"].Visible = false;
            gridViewExaminationGroup.Columns["createTime"].Visible = false;
            gridViewExaminationGroup.BestFitColumns();
        }
        private void treeListExaminationGroup_AfterCheckNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            try
            {
                selectedGroup.Clear();
                if (e.Node.ParentNode == null)
                {
                    return;
                }
                var treeListNodes = e.Node.ParentNode.Nodes.Where(c => c.Checked == true).ToList();
                if (treeListNodes.Count == 0)
                {
                    return;
                }
                foreach (var item in treeListNodes)
                {
                    var model = treeListExaminationGroup.GetDataRecordByNode(item) as PojoExaminationGroup;
                    selectedGroup.Add(model);
                }
                var item2 = treeListExaminationGroup.GetDataRecordByNode(treeListNodes.LastOrDefault()) as PojoExaminationGroup;
                refreshContent(item2.套餐编号);
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }

        }

   
    }
}