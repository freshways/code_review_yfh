﻿namespace HIS.COMM
{
    partial class XtraForm短信发送
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton接收RPT = new DevExpress.XtraEditors.SimpleButton();
            this.txtSMTime = new DevExpress.XtraEditors.TextEdit();
            this.txtSrcID = new DevExpress.XtraEditors.TextEdit();
            this.textSmId = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton接收 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton发送 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton释放 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton初始化 = new DevExpress.XtraEditors.SimpleButton();
            this.textContext = new DevExpress.XtraEditors.MemoEdit();
            this.textMobile = new DevExpress.XtraEditors.TextEdit();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSMTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSrcID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSmId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textContext.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton接收RPT);
            this.layoutControl1.Controls.Add(this.txtSMTime);
            this.layoutControl1.Controls.Add(this.txtSrcID);
            this.layoutControl1.Controls.Add(this.textSmId);
            this.layoutControl1.Controls.Add(this.simpleButton接收);
            this.layoutControl1.Controls.Add(this.simpleButton发送);
            this.layoutControl1.Controls.Add(this.simpleButton释放);
            this.layoutControl1.Controls.Add(this.simpleButton初始化);
            this.layoutControl1.Controls.Add(this.textContext);
            this.layoutControl1.Controls.Add(this.textMobile);
            this.layoutControl1.Controls.Add(this.dataLayoutControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(325, 158, 250, 350);
            this.layoutControl1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(593, 444);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton接收RPT
            // 
            this.simpleButton接收RPT.Location = new System.Drawing.Point(508, 313);
            this.simpleButton接收RPT.Name = "simpleButton接收RPT";
            this.simpleButton接收RPT.Size = new System.Drawing.Size(57, 22);
            this.simpleButton接收RPT.StyleController = this.layoutControl1;
            this.simpleButton接收RPT.TabIndex = 14;
            this.simpleButton接收RPT.Text = "接收RPT";
            this.simpleButton接收RPT.Click += new System.EventHandler(this.simpleButton接收RPT_Click);
            // 
            // txtSMTime
            // 
            this.txtSMTime.Location = new System.Drawing.Point(402, 37);
            this.txtSMTime.Name = "txtSMTime";
            this.txtSMTime.Size = new System.Drawing.Size(179, 21);
            this.txtSMTime.StyleController = this.layoutControl1;
            this.txtSMTime.TabIndex = 13;
            // 
            // txtSrcID
            // 
            this.txtSrcID.EditValue = "10";
            this.txtSrcID.Location = new System.Drawing.Point(296, 37);
            this.txtSrcID.Name = "txtSrcID";
            this.txtSrcID.Size = new System.Drawing.Size(50, 21);
            this.txtSrcID.StyleController = this.layoutControl1;
            this.txtSrcID.TabIndex = 12;
            // 
            // textSmId
            // 
            this.textSmId.EditValue = "10";
            this.textSmId.Location = new System.Drawing.Point(67, 37);
            this.textSmId.Name = "textSmId";
            this.textSmId.Size = new System.Drawing.Size(110, 21);
            this.textSmId.StyleController = this.layoutControl1;
            this.textSmId.TabIndex = 11;
            // 
            // simpleButton接收
            // 
            this.simpleButton接收.Location = new System.Drawing.Point(453, 313);
            this.simpleButton接收.Name = "simpleButton接收";
            this.simpleButton接收.Size = new System.Drawing.Size(51, 22);
            this.simpleButton接收.StyleController = this.layoutControl1;
            this.simpleButton接收.TabIndex = 10;
            this.simpleButton接收.Text = "接收SM";
            this.simpleButton接收.Click += new System.EventHandler(this.simpleButton接收_Click);
            // 
            // simpleButton发送
            // 
            this.simpleButton发送.Location = new System.Drawing.Point(414, 313);
            this.simpleButton发送.Name = "simpleButton发送";
            this.simpleButton发送.Size = new System.Drawing.Size(35, 22);
            this.simpleButton发送.StyleController = this.layoutControl1;
            this.simpleButton发送.TabIndex = 9;
            this.simpleButton发送.Text = "发送";
            this.simpleButton发送.Click += new System.EventHandler(this.simpleButton发送_Click);
            // 
            // simpleButton释放
            // 
            this.simpleButton释放.Location = new System.Drawing.Point(375, 313);
            this.simpleButton释放.Name = "simpleButton释放";
            this.simpleButton释放.Size = new System.Drawing.Size(35, 22);
            this.simpleButton释放.StyleController = this.layoutControl1;
            this.simpleButton释放.TabIndex = 8;
            this.simpleButton释放.Text = "释放";
            this.simpleButton释放.Click += new System.EventHandler(this.simpleButton释放_Click);
            // 
            // simpleButton初始化
            // 
            this.simpleButton初始化.Location = new System.Drawing.Point(324, 313);
            this.simpleButton初始化.Name = "simpleButton初始化";
            this.simpleButton初始化.Size = new System.Drawing.Size(47, 22);
            this.simpleButton初始化.StyleController = this.layoutControl1;
            this.simpleButton初始化.TabIndex = 7;
            this.simpleButton初始化.Text = "初始化";
            this.simpleButton初始化.Click += new System.EventHandler(this.simpleButton初始化_Click);
            // 
            // textContext
            // 
            this.textContext.Location = new System.Drawing.Point(64, 62);
            this.textContext.Name = "textContext";
            this.textContext.Size = new System.Drawing.Size(517, 212);
            this.textContext.StyleController = this.layoutControl1;
            this.textContext.TabIndex = 6;
            // 
            // textMobile
            // 
            this.textMobile.EditValue = "";
            this.textMobile.Location = new System.Drawing.Point(76, 12);
            this.textMobile.Name = "textMobile";
            this.textMobile.Size = new System.Drawing.Size(505, 21);
            this.textMobile.StyleController = this.layoutControl1;
            this.textMobile.TabIndex = 5;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Location = new System.Drawing.Point(12, 339);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(627, 178, 250, 350);
            this.dataLayoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(569, 93);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(569, 93);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(593, 444);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 266);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(573, 35);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(557, 301);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(16, 26);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataLayoutControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 327);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(573, 97);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textMobile;
            this.layoutControlItem2.CustomizationFormText = "发送手机号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(573, 25);
            this.layoutControlItem2.Text = "发送手机号";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textContext;
            this.layoutControlItem3.CustomizationFormText = "短信内容";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(573, 216);
            this.layoutControlItem3.Text = "短信内容";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(48, 14);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 301);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(312, 26);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton初始化;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(312, 301);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(51, 26);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton释放;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(363, 301);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(39, 26);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton发送;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(402, 301);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(39, 26);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButton接收;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(441, 301);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(55, 26);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textSmId;
            this.layoutControlItem8.CustomizationFormText = "短信smID";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(169, 25);
            this.layoutControlItem8.Text = "短信smID";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(51, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtSrcID;
            this.layoutControlItem9.CustomizationFormText = "手机上显示尾号srcID";
            this.layoutControlItem9.Location = new System.Drawing.Point(169, 25);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(169, 25);
            this.layoutControlItem9.Text = "手机上显示尾号srcID";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(111, 14);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtSMTime;
            this.layoutControlItem10.CustomizationFormText = "发送时间";
            this.layoutControlItem10.Location = new System.Drawing.Point(338, 25);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(235, 25);
            this.layoutControlItem10.Text = "发送时间";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton接收RPT;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(496, 301);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(61, 26);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // XtraForm短信发送
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 444);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraForm短信发送";
            this.Text = "短信发送";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSMTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSrcID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textSmId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textContext.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton发送;
        private DevExpress.XtraEditors.SimpleButton simpleButton释放;
        private DevExpress.XtraEditors.SimpleButton simpleButton初始化;
        private DevExpress.XtraEditors.MemoEdit textContext;
        private DevExpress.XtraEditors.TextEdit textMobile;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton simpleButton接收;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.TextEdit txtSrcID;
        private DevExpress.XtraEditors.TextEdit textSmId;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txtSMTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton simpleButton接收RPT;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
    }
}