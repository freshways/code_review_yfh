﻿namespace HIS.COMM
{
    partial class XtraForm套餐选择
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.radioGroup数据范围 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl权限信息 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit套餐数量 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton确定 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.searchLookUpEdit套餐选择 = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col编号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col套餐名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col套餐类型 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col拼音代码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col备注 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col科室名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl套餐内容 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col收费编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col收费名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col单价 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col选择 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col数量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cmb套餐类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup数据范围.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit套餐数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit套餐选择.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl套餐内容)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb套餐类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.radioGroup数据范围);
            this.layoutControl1.Controls.Add(this.labelControl权限信息);
            this.layoutControl1.Controls.Add(this.spinEdit套餐数量);
            this.layoutControl1.Controls.Add(this.simpleButton确定);
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.searchLookUpEdit套餐选择);
            this.layoutControl1.Controls.Add(this.gridControl套餐内容);
            this.layoutControl1.Controls.Add(this.cmb套餐类型);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(207, 245, 250, 350);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(699, 399);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // radioGroup数据范围
            // 
            this.radioGroup数据范围.Location = new System.Drawing.Point(509, 362);
            this.radioGroup数据范围.Name = "radioGroup数据范围";
            this.radioGroup数据范围.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "本科"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "全院"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "个人")});
            this.radioGroup数据范围.Size = new System.Drawing.Size(178, 25);
            this.radioGroup数据范围.StyleController = this.layoutControl1;
            this.radioGroup数据范围.TabIndex = 11;
            this.radioGroup数据范围.SelectedIndexChanged += new System.EventHandler(this.radioGroup套餐权限_SelectedIndexChanged);
            // 
            // labelControl权限信息
            // 
            this.labelControl权限信息.Location = new System.Drawing.Point(12, 365);
            this.labelControl权限信息.Name = "labelControl权限信息";
            this.labelControl权限信息.Size = new System.Drawing.Size(112, 14);
            this.labelControl权限信息.StyleController = this.layoutControl1;
            this.labelControl权限信息.TabIndex = 10;
            this.labelControl权限信息.Text = "库存选择:套餐权限：";
            // 
            // spinEdit套餐数量
            // 
            this.spinEdit套餐数量.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit套餐数量.Location = new System.Drawing.Point(455, 364);
            this.spinEdit套餐数量.Name = "spinEdit套餐数量";
            this.spinEdit套餐数量.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit套餐数量.Size = new System.Drawing.Size(50, 20);
            this.spinEdit套餐数量.StyleController = this.layoutControl1;
            this.spinEdit套餐数量.TabIndex = 8;
            this.spinEdit套餐数量.EditValueChanged += new System.EventHandler(this.spinEdit套餐数量_EditValueChanged);
            // 
            // simpleButton确定
            // 
            this.simpleButton确定.Image = global::HIS.COMM.Properties.Resources.AcceptInvitation;
            this.simpleButton确定.Location = new System.Drawing.Point(540, 12);
            this.simpleButton确定.Name = "simpleButton确定";
            this.simpleButton确定.Size = new System.Drawing.Size(72, 22);
            this.simpleButton确定.StyleController = this.layoutControl1;
            this.simpleButton确定.TabIndex = 7;
            this.simpleButton确定.Text = "确定";
            this.simpleButton确定.Click += new System.EventHandler(this.simpleButton确定_Click);
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton取消.Image = global::HIS.COMM.Properties.Resources.Undo;
            this.simpleButton取消.Location = new System.Drawing.Point(616, 12);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(71, 22);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 6;
            this.simpleButton取消.Text = "取消";
            // 
            // searchLookUpEdit套餐选择
            // 
            this.searchLookUpEdit套餐选择.Location = new System.Drawing.Point(236, 12);
            this.searchLookUpEdit套餐选择.Name = "searchLookUpEdit套餐选择";
            this.searchLookUpEdit套餐选择.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.searchLookUpEdit套餐选择.Properties.DisplayMember = "套餐名称";
            this.searchLookUpEdit套餐选择.Properties.NullText = "请选择...";
            this.searchLookUpEdit套餐选择.Properties.PopupFormSize = new System.Drawing.Size(600, 300);
            this.searchLookUpEdit套餐选择.Properties.ValueMember = "编号";
            this.searchLookUpEdit套餐选择.Properties.View = this.searchLookUpEdit1View;
            this.searchLookUpEdit套餐选择.Size = new System.Drawing.Size(274, 20);
            this.searchLookUpEdit套餐选择.StyleController = this.layoutControl1;
            this.searchLookUpEdit套餐选择.TabIndex = 5;
            this.searchLookUpEdit套餐选择.TextChanged += new System.EventHandler(this.searchLookUpEdit1_TextChanged);
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.col编号,
            this.col套餐名称,
            this.col套餐类型,
            this.col创建人,
            this.col拼音代码,
            this.col创建时间,
            this.col备注,
            this.col科室名称});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            // 
            // col编号
            // 
            this.col编号.FieldName = "编号";
            this.col编号.Name = "col编号";
            this.col编号.Width = 147;
            // 
            // col套餐名称
            // 
            this.col套餐名称.FieldName = "套餐名称";
            this.col套餐名称.Name = "col套餐名称";
            this.col套餐名称.Width = 486;
            // 
            // col套餐类型
            // 
            this.col套餐类型.FieldName = "套餐类型";
            this.col套餐类型.Name = "col套餐类型";
            this.col套餐类型.Width = 86;
            // 
            // col创建人
            // 
            this.col创建人.FieldName = "创建人";
            this.col创建人.Name = "col创建人";
            this.col创建人.Width = 56;
            // 
            // col拼音代码
            // 
            this.col拼音代码.FieldName = "拼音代码";
            this.col拼音代码.Name = "col拼音代码";
            this.col拼音代码.Width = 68;
            // 
            // col创建时间
            // 
            this.col创建时间.FieldName = "创建时间";
            this.col创建时间.Name = "col创建时间";
            // 
            // col备注
            // 
            this.col备注.FieldName = "备注";
            this.col备注.Name = "col备注";
            this.col备注.Width = 70;
            // 
            // col科室名称
            // 
            this.col科室名称.Caption = "科室名称";
            this.col科室名称.FieldName = "科室名称";
            this.col科室名称.Name = "col科室名称";
            this.col科室名称.Width = 77;
            // 
            // gridControl套餐内容
            // 
            this.gridControl套餐内容.Location = new System.Drawing.Point(12, 38);
            this.gridControl套餐内容.MainView = this.gridView1;
            this.gridControl套餐内容.Name = "gridControl套餐内容";
            this.gridControl套餐内容.Size = new System.Drawing.Size(675, 320);
            this.gridControl套餐内容.TabIndex = 4;
            this.gridControl套餐内容.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col收费编码,
            this.col收费名称,
            this.col单价,
            this.col选择,
            this.col数量});
            this.gridView1.GridControl = this.gridControl套餐内容;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // col收费编码
            // 
            this.col收费编码.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.col收费编码.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.col收费编码.AppearanceCell.Options.UseBackColor = true;
            this.col收费编码.FieldName = "收费编码";
            this.col收费编码.Name = "col收费编码";
            this.col收费编码.OptionsColumn.ReadOnly = true;
            this.col收费编码.Visible = true;
            this.col收费编码.VisibleIndex = 1;
            this.col收费编码.Width = 116;
            // 
            // col收费名称
            // 
            this.col收费名称.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.col收费名称.AppearanceCell.Options.UseBackColor = true;
            this.col收费名称.FieldName = "收费名称";
            this.col收费名称.Name = "col收费名称";
            this.col收费名称.OptionsColumn.ReadOnly = true;
            this.col收费名称.Visible = true;
            this.col收费名称.VisibleIndex = 2;
            this.col收费名称.Width = 444;
            // 
            // col单价
            // 
            this.col单价.FieldName = "单价";
            this.col单价.Name = "col单价";
            this.col单价.Visible = true;
            this.col单价.VisibleIndex = 3;
            this.col单价.Width = 151;
            // 
            // col选择
            // 
            this.col选择.FieldName = "选择";
            this.col选择.Name = "col选择";
            this.col选择.Visible = true;
            this.col选择.VisibleIndex = 0;
            this.col选择.Width = 55;
            // 
            // col数量
            // 
            this.col数量.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.col数量.AppearanceCell.Options.UseBackColor = true;
            this.col数量.FieldName = "数量";
            this.col数量.Name = "col数量";
            this.col数量.OptionsColumn.ReadOnly = true;
            this.col数量.Visible = true;
            this.col数量.VisibleIndex = 4;
            this.col数量.Width = 62;
            // 
            // cmb套餐类型
            // 
            this.cmb套餐类型.Location = new System.Drawing.Point(67, 12);
            this.cmb套餐类型.Name = "cmb套餐类型";
            this.cmb套餐类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmb套餐类型.Properties.NullText = "请选择...";
            this.cmb套餐类型.Properties.PopupSizeable = true;
            this.cmb套餐类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmb套餐类型.Size = new System.Drawing.Size(110, 20);
            this.cmb套餐类型.StyleController = this.layoutControl1;
            this.cmb套餐类型.TabIndex = 9;
            this.cmb套餐类型.SelectedIndexChanged += new System.EventHandler(this.cmb套餐类型_SelectedIndexChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.emptySpaceItem2,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(699, 399);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(502, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(26, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl套餐内容;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(679, 324);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.searchLookUpEdit套餐选择;
            this.layoutControlItem2.CustomizationFormText = "套餐选择:";
            this.layoutControlItem2.Location = new System.Drawing.Point(169, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(333, 26);
            this.layoutControlItem2.Text = "套餐选择:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton取消;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(604, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(75, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton确定;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(528, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(76, 26);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cmb套餐类型;
            this.layoutControlItem6.CustomizationFormText = "套餐类型:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(169, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "套餐类型:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(52, 14);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(116, 350);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(272, 29);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.labelControl权限信息;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 350);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 5, 2);
            this.layoutControlItem7.Size = new System.Drawing.Size(116, 29);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.radioGroup数据范围;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(497, 350);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(182, 0);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(182, 29);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(182, 29);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.spinEdit套餐数量;
            this.layoutControlItem5.CustomizationFormText = "套餐数量:";
            this.layoutControlItem5.Location = new System.Drawing.Point(388, 350);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 4, 2);
            this.layoutControlItem5.Size = new System.Drawing.Size(109, 29);
            this.layoutControlItem5.Text = "套餐数量:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(52, 14);
            // 
            // XtraForm套餐选择
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 399);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraForm套餐选择";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "套餐选择";
            this.Load += new System.EventHandler(this.XtraForm套餐选择_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup数据范围.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit套餐数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit套餐选择.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl套餐内容)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmb套餐类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton确定;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraEditors.SearchLookUpEdit searchLookUpEdit套餐选择;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraGrid.GridControl gridControl套餐内容;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn col收费编码;
        private DevExpress.XtraGrid.Columns.GridColumn col收费名称;
        private DevExpress.XtraGrid.Columns.GridColumn col单价;
        private DevExpress.XtraGrid.Columns.GridColumn col选择;
        private DevExpress.XtraGrid.Columns.GridColumn col数量;
        private DevExpress.XtraEditors.SpinEdit spinEdit套餐数量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn col编号;
        private DevExpress.XtraGrid.Columns.GridColumn col套餐名称;
        private DevExpress.XtraGrid.Columns.GridColumn col套餐类型;
        private DevExpress.XtraGrid.Columns.GridColumn col创建人;
        private DevExpress.XtraGrid.Columns.GridColumn col拼音代码;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn col备注;
        private DevExpress.XtraGrid.Columns.GridColumn col科室名称;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.ComboBoxEdit cmb套餐类型;
        private DevExpress.XtraEditors.LabelControl labelControl权限信息;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.RadioGroup radioGroup数据范围;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
    }
}