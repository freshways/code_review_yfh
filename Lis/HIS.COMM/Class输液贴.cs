﻿using DevExpress.XtraEditors;
using HIS.COMM.Helper;
using HIS.COMM.Report;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.COMM
{
    public class Class输液贴
    {
        public void show住院输液贴(Boolean b是否预览, string _sZYID, string _s医嘱类型, string _s病区编码, string _s开始时间, string _s截止时间)
        {
            WEISHENG.COMM.LogHelper.Info("Class输液贴", ClassEnum.logLevel.INFO.ToString(), "show住院输液贴");
            #region 获取组别SQL
            string SQL可用组别 = "SELECT distinct 执行时间,cc.zyid,cc.病人姓名,cc.病床, aa.频次名称 频次,dd.频次次数, [用法名称] 用法, [组别], [医嘱类型]" + "\r\n" +
             "FROM   [HL执行医嘱] aa left outer join [YK药品信息] bb on aa.[项目编码] = bb.[药品序号] left outer join zy病人信息 cc on aa.zyid=cc.zyid left outer join YS医嘱频次 dd on aa.频次名称 = dd.频次名称 " + "\r\n";
            string sql可用组别where = "";
            if (_sZYID == "")
            {
                sql可用组别where +=
                     "where cc.病区 = " + _s病区编码 + " and  aa.执行时间 between '" +
                     _s开始时间 + "' and '" + _s截止时间 + "' " + "\r\n";
                if (_s医嘱类型 != "")
                {
                    sql可用组别where += " and aa.医嘱类型='" + _s医嘱类型 + "'";
                }
            }
            else
            {
                sql可用组别where =
                        "where aa.zyid=" + _sZYID + " and 执行时间 between '" + _s开始时间 + "' and '" + _s截止时间 + "' " + "\r\n";
                if (_s医嘱类型 != "")
                {
                    sql可用组别where += " and aa.医嘱类型='" + _s医嘱类型 + "'";
                }
            }
            sql可用组别where += " and aa.[项目类型] = '药品项目' and bb.[财务分类] <> '卫生材料' and aa.[用法名称] in " + "\r\n" +
                 "(" + "\r\n" +
                 "  select 用法名称 from   ys医嘱用法 where  输液贴打印标志 = 1" + "\r\n" +
                 ") order by cc.病床,组别";

            SQL可用组别 = SQL可用组别 + sql可用组别where;
            #endregion

            DataTable dt组别 = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL可用组别).Tables[0];
            if (dt组别.Rows.Count == 0)
            {
                XtraMessageBox.Show("当前处方不包含输液贴输出内容", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            #region 创建打印临时table
            DataTable dt打印 = new DataTable("Test");
            //医嘱套餐编码            
            DataColumn dtc = new DataColumn("表头", typeof(string));
            dt打印.Columns.Add(dtc);
            //项目编码
            dtc = new DataColumn("频次用法", typeof(string));
            dt打印.Columns.Add(dtc);
            //项目名称
            dtc = new DataColumn("内容", typeof(string));
            dt打印.Columns.Add(dtc);
            #endregion

            #region 组建打印内容table
            for (int i = 0; i < dt组别.Rows.Count; i++)
            {
                string s表头 = dt组别.Rows[i]["病床"].ToString() + "    " + dt组别.Rows[i]["病人姓名"].ToString() + "    " + dt组别.Rows[i]["医嘱类型"].ToString();

                string s频次用法 = DateTime.Now.Date.ToShortDateString() + "      " + dt组别.Rows[i]["频次"].ToString() + "       " +
                    dt组别.Rows[i]["用法"].ToString() + "       " + dt组别.Rows[i]["组别"].ToString() + "组";
                string SQL输液贴内容 =
                    "select   aa.财务分类, aa.项目名称 药品名称, aa.剂量数量, aa.剂量单位, aa.组别, aa.用法名称 用法," + "\r\n" +
                    "         aa.频次名称 频次" + "\r\n" +
                    "from     hl执行医嘱 aa" + "\r\n" +
                    "where 执行时间 ='" + dt组别.Rows[i]["执行时间"].ToString() + "' and 组别=" + dt组别.Rows[i]["组别"].ToString() + " and 医嘱类型='" + dt组别.Rows[i]["医嘱类型"].ToString() + "' and zyid = " + dt组别.Rows[i]["zyid"].ToString() + " and aa.用法名称 in (select 用法名称" + "\r\n" +
                    "                                              from   ys医嘱用法" + "\r\n" +
                    "                                              where  输液贴打印标志 = 1) and aa.财务分类 <> '卫生材料'" + "\r\n" +
                    "order by aa.id";
                DataTable dt输液贴内容 = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                    CommandType.Text, SQL输液贴内容).Tables[0];
                string syt内容 = "";
                if (dt输液贴内容.Rows.Count > 0)
                {
                    foreach (DataRow row in dt输液贴内容.Rows)
                    {
                        string syt药品 = row["药品名称"].ToString().Trim() + "    ";
                        string syt数量剂量 = row["剂量数量"].ToString().Trim().Replace(".00", "") + row["剂量单位"].ToString().Trim();
                        syt内容 += syt药品 + syt数量剂量 + "\r\n";
                    }
                    //把组织好的打印内容，填入打印临时table
                    int Row频次次数 = 1;
                    if (Convert.ToString(dt组别.Rows[i]["频次次数"]) != "")
                    {
                        Row频次次数 = Convert.ToInt32(dt组别.Rows[i]["频次次数"]);
                    }
                    for (int i频次 = 0; i频次 < Row频次次数; i频次++)
                    {
                        DataRow dr = dt打印.NewRow();
                        dr["表头"] = s表头;
                        dr["频次用法"] = s频次用法;
                        dr["内容"] = syt内容;
                        dt打印.Rows.Add(dr);
                    }
                }
            }
            #endregion

            XtraReport输液贴 mzsyt = new XtraReport输液贴(dt打印);
            if (b是否预览)
            {
                xtraReportShow.show(mzsyt, "门诊输液贴", true, DBConnHelper.SConnHISDb);
            }
            else
            {
                xtraReportShow.printToPaper(mzsyt, "门诊输液贴", DBConnHelper.SConnHISDb);
            }
        }
        public void show门诊输液贴(bool b是否预览, Person门诊病人 person)
        {
            try
            {
                WEISHENG.COMM.LogHelper.Info("Class输液贴", ClassEnum.logLevel.INFO.ToString(), "show门诊输液贴");
                string sCFID = person.sCFID;// Convert.ToDecimal(gridView摘要.GetDataRow(gridView摘要.FocusedRowHandle)["处方号"]);
                string s病人姓名 = person.S姓名;// gridView摘要.GetDataRow(gridView摘要.FocusedRowHandle)["病人姓名"].ToString();
                string s科室名称 = person.S科室;// gridView摘要.GetDataRow(gridView摘要.FocusedRowHandle)["科室名称"].ToString();

                string SQL输液贴表头 =
                    "select aa.收费名称" + "\r\n" +
                    "from   MF门诊明细 aa left outer join gy收费小项 bb on aa.收费编码 = bb.收费编码" + "\r\n" +
                    "where  mzid in (select mzid" + "\r\n" +
                    "                from   MF处方摘要" + "\r\n" +
                    "                where  CFID = " + sCFID.ToString() + ") and bb.输液贴打印标志 = 1";

                DataTable dt输液贴表头 = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL输液贴表头).Tables[0];
                string s输液贴表头 = "";
                if (dt输液贴表头.Rows.Count > 0)
                {
                    foreach (DataRow row in dt输液贴表头.Rows)
                    {
                        s输液贴表头 += row[0].ToString().Substring(0, 1) + "、";
                    }
                    s输液贴表头 = "(" + s输液贴表头.Substring(0, s输液贴表头.Length - 1) + ")";
                }
                string SQL可用组别 =
                   "select   distinct aa.[组别],aa.用法,aa.频次,pc.频次次数 " + "\r\n" +
                   "from     MF处方明细 aa left outer join YK药品信息 bb on aa.药品序号 = bb.药品序号 left outer join YS医嘱频次 pc on aa.频次 = pc.频次名称 " + "\r\n" +
                   "where    cfid = " + sCFID.ToString() + " and aa.用法 in (select 用法名称" + "\r\n" +
                   "                                                from   ys医嘱用法" + "\r\n" +
                   "                                                where  输液贴打印标志 = 1)" + "\r\n" +
                   "and bb.[财务分类]<>'卫生材料'";
                DataTable dt组别 = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL可用组别).Tables[0];

                if (dt组别.Rows.Count == 0)
                {
                    XtraMessageBox.Show("当前处方不包含输液贴输出内容", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                #region 创建打印临时table
                DataTable dt打印 = new DataTable("Test");
                //医嘱套餐编码            
                DataColumn dtc = new DataColumn("表头", typeof(string));
                dt打印.Columns.Add(dtc);
                //项目编码
                dtc = new DataColumn("频次用法", typeof(string));
                dt打印.Columns.Add(dtc);
                //项目名称
                dtc = new DataColumn("内容", typeof(string));
                dt打印.Columns.Add(dtc);
                #endregion
                for (int i = 0; i < dt组别.Rows.Count; i++)
                {
                    string s频次用法 = DateTime.Now.Date.ToShortDateString() + "      " + dt组别.Rows[i]["频次"].ToString() + "       " +
                        dt组别.Rows[i]["用法"].ToString() + "       " + dt组别.Rows[i]["组别"].ToString() + "组";
                    string SQL输液贴内容 =
                        "select   bb.财务分类, aa.药品名称, aa.剂量数量, aa.剂量单位, aa.组别, aa.用法, aa.频次" + "\r\n" +
                        "from     MF处方明细 aa left outer join YK药品信息 bb on aa.药品序号 = bb.药品序号" + "\r\n" +
                        "where  aa.频次='" + dt组别.Rows[i]["频次"].ToString() + "' and aa.组别='" + dt组别.Rows[i][0].ToString() + "' and  cfid = " + sCFID.ToString() + " and aa.用法 in (select 用法名称" + "\r\n" +
                        "                                                from   ys医嘱用法" + "\r\n" +
                        "                                                where  输液贴打印标志 = 1)" + "\r\n" +
                        " and bb.财务分类<>'卫生材料' order by aa.id";
                    DataTable dt输液贴内容 = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL输液贴内容).Tables[0];
                    string syt内容 = "";



                    if (dt输液贴内容.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt输液贴内容.Rows)
                        {
                            string syt药品 = row["药品名称"].ToString().Trim() + "    ";
                            string syt数量剂量 = row["剂量数量"].ToString().Trim().Replace(".00", "") + row["剂量单位"].ToString().Trim();
                            //Int32 i补齐 = Convert.ToInt32(25) - (Convert.ToInt32(syt药品.Length) + Convert.ToInt32(syt数量剂量.Length));
                            //syt药品 = syt药品.PadRight(i补齐, '　');                          
                            syt内容 += syt药品 + syt数量剂量 + "\r\n";
                        }
                        //把组织好的打印内容，填入打印临时table
                        int Row频次次数 = 1;
                        if (Convert.ToString(dt组别.Rows[i]["频次次数"]) != "")
                        {
                            Row频次次数 = Convert.ToInt32(dt组别.Rows[i]["频次次数"]);
                        }
                        for (int i频次 = 0; i频次 < Row频次次数; i频次++)
                        {
                            //把组织好的打印内容，填入打印临时table
                            DataRow dr = dt打印.NewRow();
                            dr["表头"] = s病人姓名 + s输液贴表头 + "      " + s科室名称;
                            dr["频次用法"] = s频次用法;
                            dr["内容"] = syt内容;
                            dt打印.Rows.Add(dr);
                        }

                    }

                }
                XtraReport输液贴 xtraReport = new XtraReport输液贴(dt打印);

                var printParam = new HIS.COMM.Report.PrintParams { invokeModuleName = "门诊发药" };
                printParam.paperName = "门诊输液贴";
                printParam.width = 800;
                printParam.height = 600;
                printParam.marginTop = 6;
                printParam.marginLeft = 2;
                printParam.marginRight = 0;
                printParam.marginBottom = 0;
                printParam.isPreview = b是否预览;
                ReportHelper2.Print(xtraReport, printParam);

                //if (b是否预览)
                //{
                //    xtraReportShow.show(mzsyt, "门诊输液贴", true, ClassDBConnstring.SConnHISDb);
                //}
                //else
                //{
                //    xtraReportShow.printToPaper(mzsyt, "门诊输液贴", ClassDBConnstring.SConnHISDb);
                //}

            }
            catch (Exception ee)
            {
                XtraMessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
