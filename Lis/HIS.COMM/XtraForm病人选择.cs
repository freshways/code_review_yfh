﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HIS.COMM
{
    public partial class XtraForm病人选择 : DevExpress.XtraEditors.XtraForm
    {
        public emr病人 person = new emr病人();
        public XtraForm病人选择(ClassEnum.en病人选择类型 _en选择类型)
        {
            InitializeComponent();
            v刷新数据(_en选择类型, null);
        }
        public XtraForm病人选择(ClassEnum.en病人选择类型 _en选择类型, Person住院病人 _person)
        {
            InitializeComponent();
            person.i床位编码 = _person.i床位编码;//分配床位用到
            v刷新数据(_en选择类型, _person);
        }
        private void v刷新数据(ClassEnum.en病人选择类型 _en选择类型, Person住院病人 _person)
        {
            string sqlCmd =
            "SELECT br.ZYID, br.住院号码, br.病人姓名,br.疾病名称,br.[疾病编码], br.入院时间," + "\r\n" +
            "       bq.科室名称 病区名称,br.病床 病床名称,  br.性别, dbo.fn_getage(出生日期) 年龄, br.单位编码," + "\r\n" +
            "       br.在院状态, br.主治医生编码, br.[病区] 病区编码, br.科室 科室编码," + "\r\n" +
            "       br.[家庭地址], isnull(预交款, 0) 预交款, isnull(fy.总费用, 0) 总费用," + "\r\n" +
            "       isnull(fy.[总费用], 0) - isnull(yjk.预交款, 0) 欠款, br.[分院编码],ys.用户名 主治医生姓名,ks.科室名称,br.医保类型,0 父ZYID  " + "\r\n" +
            "FROM   ZY病人信息 AS br" + "\r\n" +
            "       LEFT OUTER JOIN (select   zyid, ISNULL(SUM(交款额), 0) AS 预交款" + "\r\n" +
            "                        from     ZY预交款" + "\r\n" +
            "                        group by zyid) AS yjk" + "\r\n" +
            "         ON br.ZYID = yjk.ZYID" + "\r\n" +
            "       left outer join gy科室设置 bq on br.病区 = bq.科室编码" + "\r\n" +
            "       left outer join gy科室设置 ks on br.科室 = ks.科室编码" + "\r\n" +
            "       left outer join (select   zyid, isnull(sum(金额), 0) 总费用" + "\r\n" +
            "                        from     [ZY在院费用]" + "\r\n" +
            "                        group by zyid) fy" + "\r\n" +
            "         on br.zyid = fy.zyid  left outer join pubuser ys on br.主治医生编码=ys.[用户编码] " + "\r\n" +
            " WHERE   ";
            string sqlWhere = "";

            switch (_en选择类型)
            {
                case HIS.COMM.ClassEnum.en病人选择类型.区分分院单病人含出院:
                    sqlWhere = "  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.住院号码= " + _person.s住院号码 + " and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                    break;
                case HIS.COMM.ClassEnum.en病人选择类型.区分分院:
                    sqlWhere = " (br.已出院标记 = 0) and  br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                    break;
                case HIS.COMM.ClassEnum.en病人选择类型.区分科室权限:
                    sqlWhere = " (br.已出院标记 = 0) and  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                    break;
                case HIS.COMM.ClassEnum.en病人选择类型.区分科室权限单病人:
                    sqlWhere = " (br.已出院标记 = 0) and  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.住院号码= " + _person.s住院号码 +
                        " and br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                    break;
                case HIS.COMM.ClassEnum.en病人选择类型.区分分院单病人:
                    sqlWhere = " (br.已出院标记 = 0) and br.在院状态='正常' and br.住院号码= " + _person.s住院号码 + " and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                    break;
                case HIS.COMM.ClassEnum.en病人选择类型.本病区未分配床位病人:
                    sqlWhere = " (br.已出院标记 = 0) and br.在院状态='正常' and  br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码 +
                        " and br.ZYID not in (select isnull(ZYID,0) from ZY床位设置 where 病区=" + _person.S病区编码 +
                        ") and br.病区=" + _person.S病区编码;
                    break;
                case HIS.COMM.ClassEnum.en病人选择类型.区分科室权限含子病人:
                    sqlWhere =
                    " (br.已出院标记 = 0) and  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码 +
                    " union all SELECT zbr.子ZYID, zbr.住院号码, zbr.病人姓名,br.疾病名称,br.[疾病编码],br.入院时间," + "\r\n" +
                    "       bq.科室名称 病区名称, br.病床 病床名称, zbr.性别," + "\r\n" +
                    "       DATEDIFF(year, zbr.出生日期, getdate()) 年龄,  br.单位编码," + "\r\n" +
                    "       br.在院状态, br.主治医生编码, br.[病区] 病区编码," + "\r\n" +
                    "       br.科室 科室编码, br.[家庭地址], isnull(预交款, 0) 预交款," + "\r\n" +
                    "       isnull(fy.总费用, 0) 总费用, isnull(fy.[总费用], 0) - isnull(yjk.预交款, 0) 欠款, br.[分院编码]," + "\r\n" +
                    "       ys.用户名 主治医生姓名, ks.科室名称, br.医保类型,zbr.父ZYID" + "\r\n" +
                    "FROM   zy子病人信息 zbr" + "\r\n" +
                    "       left outer join ZY病人信息 AS br on zbr.[父ZYID] = br.ZYID" + "\r\n" +
                    "       LEFT OUTER JOIN (select   zyid, ISNULL(SUM(交款额), 0) AS 预交款" + "\r\n" +
                    "                        from     ZY预交款" + "\r\n" +
                    "                        group by zyid) AS yjk" + "\r\n" +
                    "         ON br.ZYID = yjk.ZYID" + "\r\n" +
                    "       left outer join gy科室设置 bq on br.病区 = bq.科室编码" + "\r\n" +
                    "       left outer join gy科室设置 ks on br.科室 = ks.科室编码" + "\r\n" +
                    "       left outer join (select   zyid, isnull(sum(金额), 0) 总费用" + "\r\n" +
                    "                        from     [ZY在院费用]" + "\r\n" +
                    "                        group by zyid) fy" + "\r\n" +
                    "         on br.zyid = fy.zyid" + "\r\n" +
                    "       left outer join pubuser ys on br.主治医生编码 = ys.[用户编码]" + "\r\n" +
                    " where  br.病区 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") and br.在院状态='正常' and br.分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码;
                    break;
                case HIS.COMM.ClassEnum.en病人选择类型.出院申请:
                    sqlWhere = "  (br.已出院标记 = 0)  and (br.在院状态 = '出院申请') AND br.分院编码 = " + HIS.COMM.zdInfo.Model站点信息.分院编码;
                    break;
                case HIS.COMM.ClassEnum.en病人选择类型.出院申请单病人:
                    sqlWhere = "  (br.已出院标记 = 0)  and (br.在院状态 = '出院申请') AND br.分院编码 = " + HIS.COMM.zdInfo.Model站点信息.分院编码 + " and br.住院号码= " + _person.s住院号码;
                    break;
                default:
                    break;
            }
            try
            {
                sqlCmd += sqlWhere;
                this.gridControl1.DataSource = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, sqlCmd).Tables[0];
                gridView1.PopulateColumns();
                try
                {
                    gridView1.Columns["ZYID"].Visible = false;
                    gridView1.Columns["单位编码"].Visible = false;
                    //gridView1.Columns["入院时间"].Visible = false;
                    gridView1.Columns["病区编码"].Visible = false;
                    gridView1.Columns["科室编码"].Visible = false;
                    gridView1.Columns["分院编码"].Visible = false;
                    gridView1.Columns["家庭地址"].Visible = false;
                    gridView1.Columns["主治医生编码"].Visible = false;
                    gridView1.Columns["科室名称"].Visible = false;
                    gridView1.Columns["医保类型"].Visible = false;
                    gridView1.Columns["父ZYID"].Visible = false;
                    gridView1.Columns["疾病名称"].Visible = false;
                    gridView1.Columns["疾病编码"].Visible = false;
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                gridView1.BestFitColumns();               
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridView1.FocusedRowHandle < 0)
                {
                    return;
                }
                if (gridView1.DataRowCount == 0)
                {
                    return;
                }
                else
                {
                    DataRow row = gridView1.GetDataRow(gridView1.FocusedRowHandle);
                    person.S姓名 = row["病人姓名"].ToString();
                    person.s主治医生编码 = row["主治医生编码"].ToString();
                    person.sZYID = row["ZYID"].ToString();
                    if (row["父ZYID"].ToString() == "0")
                    {
                        person.sZYID父 = person.sZYID;
                    }
                    else
                    {
                        person.sZYID父 = row["父ZYID"].ToString();
                    }
                    person.s住院号码 = row["住院号码"].ToString();
                    person.d病人欠款 = Convert.ToDecimal(row["欠款"]);
                    person.d当前欠款 = person.d病人欠款;
                    person.d欠款下限 =baseInfo.dec医院欠款下限;

                    if (person.d当前欠款 >= person.d欠款下限)
                    {
                        MessageBox.Show("【" + person.S姓名 + "】的当前欠款【" + person.d当前欠款 + "】，" +
                            "\r\n大于医院设定的欠款下限【" + person.d欠款下限 + "】。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    person.S性别 = row["性别"].ToString();
                    person.S床位名称 = row["病床名称"].ToString();
                    person.s病区名称 = row["病区名称"].ToString();
                    person.S科室名称 = row["科室名称"].ToString();
                    person.s入院时间 = row["入院时间"].ToString();
                    person.dec预交款总额 = Convert.ToDecimal(row["预交款"]);
                    person.S病区编码 = row["病区编码"].ToString();
                    person.S病区编码 = row["病区编码"].ToString();
                    person.s科室编码 = row["科室编码"].ToString();
                    person.S年龄 = row["年龄"].ToString();
                    person.S家庭地址 = row["家庭地址"].ToString();
                    person.S医保类型 = row["医保类型"].ToString() == "" ? "现金自费" : row["医保类型"].ToString();
                    person.s主治医生姓名 = row["主治医生姓名"].ToString();
                    person.s疾病名称 = row["疾病名称"].ToString();
                    person.s疾病编码 = row["疾病编码"].ToString();
                    this.DialogResult = DialogResult.OK;
                    //this.Close();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                simpleButton确定_Click(null, null);
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            simpleButton确定_Click(null, null);
        }

        private void XtraForm病人选择_Load(object sender, EventArgs e)
        {
            //    if (gridView1.DataRowCount == 1)
            //    {
            //        simpleButton确定_Click(null, null);
            //    }
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {

        }

        private void XtraForm病人选择_Activated(object sender, EventArgs e)
        {
            if (gridView1.DataRowCount == 1)
            {
                gridView1.FocusedRowHandle = 0;
                simpleButton确定_Click(null, null);
            }
        }


    }
}