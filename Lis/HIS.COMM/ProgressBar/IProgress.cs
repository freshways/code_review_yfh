﻿using HIS.COMM.ProcessBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HIS.COMM.ProgressBar
{
    public interface IProgress
    {
        void ShowProcessBar();
    }
}
