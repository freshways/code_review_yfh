﻿using HIS.COMM.ProcessBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HIS.COMM.ProgressBar
{
    public class Show
    {
        //进度弹窗
        private frmProcessBar fromProgress = null;
        //进度委托事件
        private delegate bool IncreaseHandle(int nValue);
        //要展示进度的业务的线程
        Thread progressBusinessThread = null;
        private int iTotal = 3000;

        private void ShowProcessBar()
        {
            fromProgress = new frmProcessBar(true, true);
            fromProgress.Title = "业务进度";
            fromProgress.ProgressThread = progressBusinessThread;
            fromProgress.setMaxValue(iTotal);
            // Init increase event
            fromProgress.TopMost = true;
            fromProgress.ShowDialog();
            fromProgress = null;
        }

        /// <summary>
        /// 处理业务，并显示进度的方法
        /// </summary>
        private void ThreadWorker()
        {
            iTotal = 40;
            //1、在业务方法中调用中显示进度界面
            MethodInvoker mi = new MethodInvoker(ShowProcessBar);
            mi.BeginInvoke(null, null);
            //处理业务代码
            Thread.Sleep(1000);

            for (int i = 0; i < iTotal - 10; i++)
            {
                Thread.Sleep(800);
                fromProgress.Content = "处方上传进度" + i;
                this.fromProgress.Invoke(new IncreaseHandle(fromProgress.Increase),
                    new object[] { i });
            }

            fromProgress.Content = "正在保存数据";
            this.fromProgress.Invoke(new IncreaseHandle(fromProgress.Increase),
               new object[] { iTotal - 5 });

            Thread.Sleep(3000);
            this.fromProgress.Invoke(new IncreaseHandle(fromProgress.Increase),
                new object[] { iTotal });
        }
        public Show()
        {
            progressBusinessThread = new Thread(new ThreadStart(ThreadWorker));
            progressBusinessThread.Start();
        }
    }
}
