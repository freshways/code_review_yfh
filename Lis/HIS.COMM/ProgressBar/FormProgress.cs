﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using WEISHENG.COMM;

namespace HIS.COMM.ProgressBar
{
    public partial class FormProgress : Form
    {
        public readonly BackgroundWorker bw;
        //DoWorkEventHandler doAction;

        int maxValue = 100;

        public FormProgress(DoWorkEventHandler doAction, RunWorkerCompletedEventHandler runWorkerCompletedEvent, int _maxValue)
        {
            InitializeComponent();
            maxValue = _maxValue;
            progressBar1.Maximum = maxValue;
            bw = BackgroundWorkerExtension.InitNewBackgroundWorker(doAction, bw_ProgressChanged, runWorkerCompletedEvent);
            bw.RunWorkerCompleted += bw_localRunWorkerCompleted;//多事件委托，顺序执行调用界面和进度界面的事件。
        }



        void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var userState = e.UserState;
            //lblMessage.Text = "bw_ProgressChanged正在处理" + e.ProgressPercentage;
            progressBar1.Value = e.ProgressPercentage;
            lsRes.Items.Insert(0, userState);
        }

        void bw_localRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnCancel.Enabled = false;
            if (e.Cancelled)
            {
                MessageBox.Show("取消成功");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("执行出错 " + e.Error.Message);
            }
            else
            {
                msgHelper.ShowInformation($"上传成功{e.Result}");
            }
          
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            bw.CancelAsyncExt();
            this.Close();
        }
    }
}
