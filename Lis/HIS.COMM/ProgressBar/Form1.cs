﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using HIS.COMM.ProgressBar;

namespace HIS.COMM.ProcessBar
{
    public partial class Form1 : Form
    {
        //进度弹窗
        private frmProcessBar fromProgress = null;
        //进度委托事件
        private delegate bool IncreaseHandle(int nValue);
        //要展示进度的业务的线程
        Thread progressBusinessThread = null;
        private int iTotal = 30;
        public Form1()
        {
            InitializeComponent();
        }

        private void ShowProcessBar()
        {
            fromProgress = new frmProcessBar(true, true);
            fromProgress.Title = "业务进度";
            fromProgress.ProgressThread = progressBusinessThread;
            fromProgress.setMaxValue(iTotal);
            // Init increase event
            fromProgress.TopMost = true;
            fromProgress.ShowDialog();
            fromProgress = null;
        }
        //处理业务，并显示进度的方法
        private void ThreadWorker()
        {
            iTotal = 40;
            //1、在业务方法中调用中显示进度界面
            MethodInvoker mi = new MethodInvoker(ShowProcessBar);
            this.BeginInvoke(mi);
            //处理业务代码
            Thread.Sleep(1000);//Sleep a while to show window

            for (int i = 0; i < iTotal - 10; i++)
            {
                fromProgress.Content = "进度文本" + i;
                this.Invoke(new IncreaseHandle(fromProgress.Increase),
                    new object[] { i });
            }

            fromProgress.Content = "正在保存数据" ;
            this.Invoke(new IncreaseHandle(fromProgress.Increase),
               new object[] { iTotal-5 });

            Thread.Sleep(3000);
            this.Invoke(new IncreaseHandle(fromProgress.Increase),
                new object[] { iTotal });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBusinessThread = new Thread(new ThreadStart(ThreadWorker));         
            progressBusinessThread.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Show show = new Show();
            
        }
    }
}