using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using HIS.Model;
using System.Linq;
using WEISHENG.COMM;

namespace HIS.COMM
{
    public class djh
    {

        public static string make门诊预交款单据号()
        {
            return HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text,
                "select isnull(max(收据号),0)+1 from [MF预交款] where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid.ToString() + " and 分院编码=" + HIS.COMM.zdInfo.Model站点信息.分院编码).ToString();
        }
        /// <summary>
        /// 考虑药房、药库库存生成YPID
        /// </summary>
        /// <returns></returns>
        public static decimal makeYPID()
        {
            decimal yfYPID = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select (case when max(ypid) is null then convert(numeric(10),convert(varchar(10),getdate(),112))+0.0001 else max(ypid)+0.0001 end) from yf库存信息 where floor(ypid)=convert(varchar(10),getdate(),112) "));
            decimal ykYPID = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select (case when max(ypid) is null then convert(numeric(10),convert(varchar(10),getdate(),112))+0.0001 else max(ypid)+0.0001 end) from yk库存信息 where floor(ypid)=convert(varchar(10),getdate(),112) "));

            if (ykYPID >= yfYPID)
            {
                return ykYPID;
            }
            else
            {
                return yfYPID;
            }
        }
        /// <summary>
        /// 获取套餐号
        /// </summary>
        /// <param name="connString"></param>
        /// <returns></returns>
        public static string makeTch(string connString)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(connString, CommandType.Text,
                "select isnull(max(编号),0)+1 from gy套餐摘要"));
        }

        /// <summary>
        /// 生成新的临床路径编号
        /// </summary>
        /// <param name="connString"></param>
        /// <returns></returns>
        public static string makeLjbh(string connString)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(connString, CommandType.Text,
                "select isnull(max(路径编号),0)+1 from ys路径摘要"));
        }
        /// <summary>
        /// 药库出库单号总生成
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        public static string sMakeYK出库单号(SqlConnection con)
        {
            string serverDate = HIS.Model.Helper.timeHelper.getServerDateStr();
            Int64 zsbf = Convert.ToInt32(serverDate.Replace("-", ""));//整数部分

            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(con, CommandType.Text,
    "select isnull(MAX(出库单号)," + (zsbf * 1000).ToString() + ")+1  from yk出库摘要 where 出库单号 between " + (zsbf * 1000).ToString() + " and " + zsbf.ToString() + "999 and 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid));
            //return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(con, CommandType.Text,
            //    "select isnull(MAX(出库单号),0)+1  from yk出库摘要 where 单位编码=" + WEISHENG.COMM.zdInfo.Dwid));
        }

        public static string Ckdh(string connString)
        {
            string serverDate = HIS.Model.Helper.timeHelper.getServerDateStr();
            Int64 zsbf = Convert.ToInt32(serverDate.Replace("-", ""));//整数部分

            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(connString, CommandType.Text,
    "select isnull(MAX(出库单号)," + (zsbf * 1000).ToString() + ")+1  from yk出库摘要 where 出库单号 between " + (zsbf * 1000).ToString() + " and " + zsbf.ToString() + "999 and 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid));
        }


        /// <summary>
        /// 药库出库单号分生成
        /// </summary>
        /// <param name="con"></param>
        /// <param name="ksxh"></param>
        /// <returns></returns>
        public static string CkdhMz(SqlConnection con, string ksxh)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(con, CommandType.Text,
                "select isnull(MAX(出库单号分),0)+1 from yk出库摘要 where 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and   科室编码=" + ksxh));

        }
        public static string Tjdh(SqlConnection con)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(con, CommandType.Text,
               " SELECT ISNULL(MAX(tjdh), 0) + 1 AS tjdh FROM yk_yptj"));

        }

        /// <summary>
        /// 药房盘点单据号
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string sYfPddjh(string conn)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select isnull(max(单据号),0)+1 from yf盘点摘要 where 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid));
        }

        /// <summary>
        /// 药库盘点单据号生成
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string sYkPddjh(string conn)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select isnull(max(单据号),0)+1 from yk盘点摘要 where 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid));
        }
        /// <summary>
        /// 药房入库单号生成
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string sMake药房入库单号(SqlConnection conn)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select isnull(max(入库单号),0)+1 from yf入库摘要 where 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid));
        }
        /// <summary>
        /// 药库入库单号
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string YkRkdh(string conn)
        {
            string serverDate = HIS.Model.Helper.timeHelper.getServerDateStr();
            Int64 zsbf = Convert.ToInt32(serverDate.Replace("-", ""));//整数部分

            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
    "select isnull(MAX(入库单号)," + (zsbf * 1000).ToString() + ")+1  from yk入库摘要 where 入库单号 between " + (zsbf * 1000).ToString() + " and " + zsbf.ToString() + "999 and 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid));

            //return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
            //    "select isnull(MAX(入库单号),0)+1 from yk入库摘要 where 单位编码=" + WEISHENG.COMM.zdInfo.Dwid));
        }

        /// <summary>
        /// 返回yth数据库中的卫生室调价单号
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string WssTjdh(SqlConnection conn)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select isnull(MAX(单据号),0)+1 from yp调价单 where wsy_id=" + HIS.COMM.zdInfo.Model单位信息.iDwid));
        }

        public static string WssTjdh(string conn)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select isnull(MAX(单据号),0)+1 from yp调价单 where wsy_id=" + HIS.COMM.zdInfo.Model单位信息.iDwid));
        }

        /// <summary>
        /// 药房调价单号计算
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string yfTjdh(string conn)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select isnull(MAX(单据号),0)+1 from yf调价单 "));
        }


        /// <summary>
        /// 药库调价单计算
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string ykTjdh(string conn)
        {
            return Convert.ToString(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select isnull(MAX(单据号),0)+1 from yk调价单 "));
        }


        /// <summary>
        /// 获取处方号
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string GetCfh(SqlConnection conn)
        {
            string temp = HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text, "select 参数值 from gy全局参数 where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and  参数名称='cur_cfh'").ToString().Trim();
            string cfh = temp.Substring(0, temp.IndexOf("&")).Trim() == DateTime.Now.ToShortDateString().Trim() ? temp.Substring(0, temp.IndexOf("&")) + "&" + Convert.ToString(Convert.ToInt64(temp.Substring(temp.IndexOf("&") + 1, temp.Length - 1 - temp.IndexOf("&"))) + 1) : DateTime.Now.ToShortDateString() + "&1";
            HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update gy全局参数 set 参数值=' " + cfh + "' where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and 参数名称='cur_cfh'");
            return cfh;
        }
        /// <summary>
        /// 生成住院预交款收据号
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string ZyYjkSjh()
        {
            string temp = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, "select isnull(max(收据号),0)+1 from zy预交款 where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid).ToString().Trim();

            return temp;

        }

        /// <summary>
        /// 返回某操作员的下一个可用住院发票号
        /// </summary>
        /// <param name="czyId"></param>
        /// <returns></returns>
        public static string FphmZyGetNext(string czyId)
        {
            string temp = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, " select MAX(convert(bigint,参数值))+1 newzyfph  from gy全局参数 where 参数名称='当前住院发票号' and  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid).ToString().Trim();

            return temp;
        }







        /// <summary>
        /// 保存当前发票号到门诊票据设置
        /// </summary>
        /// <param name="gy票据设置"></param>
        /// <returns></returns>
        public static Boolean SaveSettings(HIS.Model.GY票据设置 gy票据设置)
        {
            try
            {
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    if (chis.MF发票使用记录.Any(c => c.发票号码 == gy票据设置.当前号码))
                    {
                        msgHelper.ShowInformation("设置发票号已经被使用，不能保存,发票号码：" + gy票据设置.当前号码);
                        return false;
                    }
                    chis.GY票据设置.Attach(gy票据设置);
                    chis.Entry(gy票据设置).Property(c => c.当前号码).IsModified = true;
                    chis.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 门诊发票号码设置是否合法性检查
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string s门诊发票号验证(string conn)//获取处方及发票号(比较发票值)
        {
            try
            {
                Int16 validity = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text, "select COUNT(*) from GY票据设置 where Mac地址='" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "' and 票据类型='门诊发票' and 当前号码 between 起始号码 and 截止号码 and 是否当前号段=1").ToString().Trim());

                if (validity != 1)
                {
                    return "本机门诊发票设置合法性检查未通过，请联系技术人员合理设置发票！";
                }
                else
                {
                    return "通过";
                }

            }
            catch (Exception ee)
            {

                throw ee;
            }

        }

        /// <summary>
        /// 住院发票号码设置是否合法性检查
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string s住院发票号验证(string conn)//获取处方及发票号(比较发票值)
        {
            try
            {
                Int16 validity = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text, "select COUNT(*) from GY票据设置 where Mac地址='" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "' and 票据类型='住院发票' and 当前号码 between 起始号码 and 截止号码 and 是否当前号段=1").ToString().Trim());

                if (validity != 1)
                {
                    return "本机住院发票设置合法性检查未通过，请联系技术人员合理设置发票！";
                }
                else
                {
                    return "通过";
                }

            }
            catch (Exception ee)
            {

                throw ee;
            }

        }



        /// <summary>
        /// 获取门诊发票号
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static bool get门诊发票号和打印格式(ref string _sFph, ref Class发票结构.en门诊发票打印模式 _enFp)//获取处方及发票号(比较发票值)
        {
            Boolean re = false;
            try
            {
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                    "select 当前号码 ,isnull(发票格式,'老格式_摘要') 发票格式 from GY票据设置 where Mac地址='" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress +
                    "' and 是否当前号段=1 and 票据类型='门诊发票'").Tables[0];
                _sFph = dt.Rows[0]["当前号码"].ToString();
                _enFp = (Class发票结构.en门诊发票打印模式)Enum.Parse(typeof(Class发票结构.en门诊发票打印模式), dt.Rows[0]["发票格式"].ToString());
                re = true;
            }
            catch (Exception ex)
            {
                re = false;
            }
            return re;
        }

        /// <summary>
        /// 返回某操作员的下一个可用门诊发票号
        /// </summary>
        /// <param name="czyId"></param>
        /// <returns></returns>
        public static string FphmMzGetNext(string czyId)
        {
            string temp = HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, " select MAX(convert(int,住院号码))+1 newzyhm  from zy病人信息 where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid).ToString().Trim();

            return temp;
        }

        //public static string GetCFID(SqlTransaction conn)
        //{

        //    string serverDate = WEISHENG.COMM.dateTimeHelper.getServerDateStr(ClassDBConnstring.SConnHISDb);// HIS.COMM.getServerDate();
        //    int i整数部分 = Convert.ToInt32(serverDate.Replace("-", ""));//整数部分


        //    string s全局参数当前值 = HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
        //        "select isnull(max(参数值),20010101.0001) from gy全局参数 where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid +
        //        " and  参数名称='cur_CFID' and left(ltrim(rtrim(参数值)),8)='" + i整数部分.ToString() + "'").ToString().Trim();

        //    Decimal dec小数部分;//小数部分


        //    if (s全局参数当前值.Substring(0, s全局参数当前值.IndexOf(".")).Trim() == serverDate.Replace("-", ""))
        //    {
        //        dec小数部分 = Convert.ToDecimal(s全局参数当前值) - i整数部分 + Convert.ToDecimal(0.0001); //(Convert.ToInt16(temp.Substring(temp.IndexOf(".") + 1, temp.Length - 1 - temp.IndexOf(".")))+1)/1000; 
        //    }
        //    else
        //    {
        //        dec小数部分 = Convert.ToDecimal("0.0001");
        //    }

        //    decimal dec处方摘要最大值计算 = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
        //        "select isnull(max(CFID),0)+0.0001 from mf处方摘要 where 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and floor(cfid)=" + i整数部分.ToString()));
        //    string s日期和全局参数计算值 = Convert.ToString(i整数部分 + dec小数部分);

        //    string resultCFID;
        //    if (Convert.ToDecimal(s日期和全局参数计算值) >= Convert.ToDecimal(dec处方摘要最大值计算))
        //    {
        //        resultCFID = s日期和全局参数计算值;
        //    }
        //    else
        //    {
        //        resultCFID = Convert.ToString(dec处方摘要最大值计算);
        //    }

        //    HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update gy全局参数 set 参数值='" + resultCFID + "' where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and 参数名称='cur_CFID'");
        //    return resultCFID;
        //}
        public static string GetCFID()
        {

            string serverDate = HIS.Model.Helper.timeHelper.getServerDateStr();
            int i整数部分 = Convert.ToInt32(serverDate.Replace("-", ""));//整数部分


            string s全局参数当前值 = HIS.Model.Dal.SqlHelper.ExecuteScalar("select isnull(max(参数值),20010101.0001) from gy全局参数 where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid +
                " and  参数名称='cur_CFID' and left(ltrim(rtrim(参数值)),8)='" + i整数部分.ToString() + "'").ToString().Trim();

            Decimal dec小数部分;//小数部分


            if (s全局参数当前值.Substring(0, s全局参数当前值.IndexOf(".")).Trim() == serverDate.Replace("-", ""))
            {
                dec小数部分 = Convert.ToDecimal(s全局参数当前值) - i整数部分 + Convert.ToDecimal(0.0001); //(Convert.ToInt16(temp.Substring(temp.IndexOf(".") + 1, temp.Length - 1 - temp.IndexOf(".")))+1)/1000; 
            }
            else
            {
                dec小数部分 = Convert.ToDecimal("0.0001");
            }

            decimal dec处方摘要最大值计算 = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(
                "select isnull(max(CFID),0)+0.0001 from mf处方摘要 where 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and floor(cfid)=" + i整数部分.ToString()));
            string s日期和全局参数计算值 = Convert.ToString(i整数部分 + dec小数部分);

            string resultCFID;
            if (Convert.ToDecimal(s日期和全局参数计算值) >= Convert.ToDecimal(dec处方摘要最大值计算))
            {
                resultCFID = s日期和全局参数计算值;
            }
            else
            {
                resultCFID = Convert.ToString(dec处方摘要最大值计算);
            }

            HIS.Model.Dal.SqlHelper.ExecuteScalar("update gy全局参数 set 参数值='" + resultCFID + "' where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and 参数名称='cur_CFID'");
            return resultCFID;
        }


        /// <summary>
        /// 生成MZID
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static string GetMZID(SqlConnection conn)
        {

            int i整数部分 = Convert.ToInt32(HIS.Model.Helper.timeHelper.getServerDateStr().Replace("-", ""));
            string _s全家参数当前值 = HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select isnull(max(参数值),20010101.0001) from gy全局参数 where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid +
                " and  参数名称='cur_MZID' and left(参数值,8)='" + i整数部分.ToString() + "'").ToString().Trim();
            string _s全局参数计算MZID = "";
            Decimal _dec全局参数小数部分计算;

            if (_s全家参数当前值.Substring(0, _s全家参数当前值.IndexOf(".")).Trim() == i整数部分.ToString())//日期时间格式设置不同会有问题！！！DateTime.Now.ToShortDateString().Replace("-", "")
            {
                _dec全局参数小数部分计算 = Convert.ToDecimal(_s全家参数当前值) - i整数部分 + Convert.ToDecimal(0.0001);
            }
            else
            {
                _dec全局参数小数部分计算 = Convert.ToDecimal("0.0001");
            }


            _s全局参数计算MZID = Convert.ToString(i整数部分 + _dec全局参数小数部分计算);

            decimal dec门诊摘要计算 = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
              "select isnull(max(MZID),0)+0.0001 from mf门诊摘要 where 单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and floor(MZID)=" + i整数部分.ToString()));

            string resultMZID;
            if (Convert.ToDecimal(_s全局参数计算MZID) >= Convert.ToDecimal(dec门诊摘要计算))
            {
                resultMZID = _s全局参数计算MZID;
            }
            else
            {
                resultMZID = Convert.ToString(dec门诊摘要计算);
            }

            HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update gy全局参数 set 参数值='" + resultMZID + "' where  单位编码=" + HIS.COMM.zdInfo.Model单位信息.iDwid + " and 参数名称='cur_MZID'");
            WEISHENG.COMM.LogHelper.Info("调试信息", "调试信息", "生成新值|" + resultMZID + "|参数ID" + _s全局参数计算MZID + "|摘要计算" + dec门诊摘要计算);
            return resultMZID;
        }


        public static string GetMZID(string conn)
        {
            using (SqlConnection _conn = new SqlConnection(conn))
            {
                return GetMZID(_conn);
            }
        }


        public static string vaildMZID(SqlConnection conn, string _sMZID)
        {
            int iCount = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(conn, CommandType.Text,
                "select count(mzid) from mf门诊摘要 where mzid=" + _sMZID));
            if (iCount == 0)
            {
                return _sMZID;
                WEISHENG.COMM.LogHelper.Info("MZID验证", "调试信息", _sMZID + "验证通过");
            }
            string _NewMZID = GetMZID(conn);
            WEISHENG.COMM.LogHelper.Info("MZID验证", "调试信息", _sMZID + "|调试冲突，获取新值|" + _NewMZID);
            return _NewMZID;
        }
    }
}
