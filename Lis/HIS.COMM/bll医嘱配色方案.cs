﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM
{
    public class bll医嘱配色方案
    {
        public static void gridView医嘱处理_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                if (e.RowHandle >= 0)
                {
                    string s子嘱ID = view.GetRowCellDisplayText(e.RowHandle, view.Columns["子嘱ID"]).ToString();
                    if (s子嘱ID != "")
                    {
                        e.Appearance.BackColor2 = Color.BurlyWood;
                    }
                    string s停嘱时间 = view.GetRowCellDisplayText(e.RowHandle, view.Columns["停嘱时间"]).ToString();
                    if (s停嘱时间 != "")
                    {
                        e.Appearance.BackColor2 = Color.Red;
                    }
                    if (Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["自备"])))
                    {
                        e.Appearance.BackColor2 = Color.Green;
                    }
                    if (Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["出院带药"])))
                    {
                        e.Appearance.BackColor2 = Color.Aqua;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        public static void gridView执行医嘱_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            try
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
                if (e.RowHandle >= 0)
                {
                    string s子嘱ID = view.GetRowCellDisplayText(e.RowHandle, view.Columns["子嘱ID"]).ToString();
                    if (s子嘱ID != "")
                    {
                        e.Appearance.BackColor2 = Color.BurlyWood;
                    }
                    string s长期医嘱只执行一次 = view.GetRowCellDisplayText(e.RowHandle, view.Columns["长期医嘱只执行一次"]).ToString();//长期医嘱只执行一次
                    string s医嘱类型 = view.GetRowCellDisplayText(e.RowHandle, view.Columns["医嘱类型"]).ToString();
                    if (s长期医嘱只执行一次 == "1" && s医嘱类型 == "长期医嘱")
                    {
                        e.Appearance.BackColor2 = Color.Brown;
                    }
                    string s停嘱时间 = view.GetRowCellDisplayText(e.RowHandle, view.Columns["停嘱时间"]).ToString();
                    if (s停嘱时间 != "")
                    {
                        e.Appearance.BackColor2 = Color.Red;
                    }

                    if (Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["自备"])))
                    {
                        e.Appearance.BackColor2 = Color.Green;
                    }

                    if (Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["出院带药"])))
                    {
                        e.Appearance.BackColor2 = Color.DarkBlue;
                    }

                    if (Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, view.Columns["出院带药"])))
                    {
                        e.Appearance.BackColor2 = Color.Aqua;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }
    }
}
