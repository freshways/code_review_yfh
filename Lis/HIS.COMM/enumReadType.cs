﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM
{
    public enum enumReadType
    {
        电子健康卡 = 0,
        电子医保码 = 1,
        医保卡 = 2,
        医保无卡 = 3,
        身份证读卡 = 4,
        院内条码 = 5,
        我的移动端 = 6,
        其他 = 7
    }
}
