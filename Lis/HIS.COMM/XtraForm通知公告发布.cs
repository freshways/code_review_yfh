﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace HIS.COMM
{
    public partial class XtraForm通知公告发布 : DevExpress.XtraEditors.XtraForm
    {
        private string sConn;
        public XtraForm通知公告发布(string _sConn){
            InitializeComponent();
            if (_sConn.Trim() == "")
            {
                sConn = DBConnHelper.SConnHISDb;
            }
            else
            {
                sConn = _sConn;
            }
        }

        private void XtraForm通知公告_Load(object sender, EventArgs e)
        {

            BindGrid();
        }

        private void BindGrid()
        {
            using (SqlConnection con = new SqlConnection(sConn))
            {
                string strSql = "select * , convert(bit,0) as bolchk from pubnoteic";
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(con, CommandType.Text, strSql).Tables[0];
                gridControl1.DataSource = dt;
                //gridControl1.DataMember = dt.ToString();
            }
        }

        private void simpleButton新增_Click(object sender, EventArgs e)
        {
            XtraForm通知公告Edit edit = new XtraForm通知公告Edit(sConn);
            edit.ShowDialog();
            BindGrid();
        }

        private void simpleButton编辑_Click(object sender, EventArgs e)
        {

        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {

            object colID = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["iid"]);
            XtraForm通知公告Edit frm = new XtraForm通知公告Edit(sConn);
            frm.colID = colID.ToString();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                BindGrid();
            }
        }
        string DelLastComma(string origin)
        {
            if (origin.IndexOf(",") == -1)
            {
                return origin;
            }
            return origin.Substring(0, origin.LastIndexOf(","));
        }

        private void simpleButton删除_Click(object sender, EventArgs e)
        {
            string strSysCodes = "";
            gridView1.FocusedColumn = gridView1.Columns["iid"];
            for (int r = 0; r < gridView1.RowCount; r++)
            {
                object check = gridView1.GetRowCellValue(r, gridView1.Columns["bolchk"]);

                if (check != null && check != DBNull.Value)
                {
                    bool val = Convert.ToBoolean(check);
                    if (val)
                    {
                        strSysCodes += gridView1.GetRowCellValue(r, gridView1.Columns["iid"]) + ",";
                    }
                }
            }

            strSysCodes = DelLastComma(strSysCodes);

            if (strSysCodes != null && strSysCodes != "")
            {
                if (XtraMessageBox.Show("确定要删除所选择的信息吗？", "his", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    string strSql = "delete pubnoteic where iid in ( " + strSysCodes + ")";
                    using (SqlConnection con = new SqlConnection(sConn))
                    {

                        HIS.Model.Dal.SqlHelper.ExecuteNonQuery(con, CommandType.Text, strSql);
                        XtraMessageBox.Show("删除成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        BindGrid();
                    }
                }
            }
            else
            {
                XtraMessageBox.Show("请点击下面列表中的数据，选择将要删除的行！", "his", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

            object colID = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["iid"]);
            XtraForm通知公告Edit frm = new XtraForm通知公告Edit(sConn);
            frm.colID = colID.ToString();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                BindGrid();
            }
        }

    }
}