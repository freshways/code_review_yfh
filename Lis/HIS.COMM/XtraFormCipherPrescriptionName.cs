﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HIS.Model;

namespace HIS.COMM
{
    public partial class XtraFormCipherPrescriptionName : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        private List<GY协定处方> list协定内容;
        private string s打开方式;

        public XtraFormCipherPrescriptionName(List<GY协定处方> list协定内容, string _s打开方式)
        {
            InitializeComponent();
            this.list协定内容 = list协定内容;
            s打开方式 = _s打开方式;
        }

        private void XtraForm医嘱组别维护_Load(object sender, EventArgs e)
        {
            radioGroup类型_SelectedIndexChanged(null, null);
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                var item = chis.GY协定处方.Where(c => c.处方名称 == textEdit协定处方名称.Text).FirstOrDefault();
                if (item != null)
                {
                    MessageBox.Show("已经存在同名的协定处方", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                string s处方类型 = "";
                switch (radioGroup类型.SelectedIndex)
                {
                    case 0:
                        s处方类型 = "个人";
                        break;
                    case 1:
                        s处方类型 = "科室";
                        break;
                    case 2:
                        if (HIS.COMM.zdInfo.ModelUserInfo.用户名.ToUpper() != "ADMIN")
                        {
                            MessageBox.Show("建立全院协定处方，请使用管理员账户登录。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        s处方类型 = "全院";
                        break;
                }
                foreach (var i in list协定内容)
                {
                    i.处方类型 = s处方类型;
                    i.处方名称 = textEdit协定处方名称.Text;
                    i.拼音代码 = textEdit拼音代码.Text;
                    i.创建人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码;
                    i.创建人姓名 = HIS.COMM.zdInfo.ModelUserInfo.用户名;
                    i.创建科室编码 = HIS.COMM.zdInfo.Model科室信息.科室编码;
                    i.创建科室名称 = HIS.COMM.zdInfo.Model科室信息.科室名称;
                    i.单位编码 = HIS.COMM.zdInfo.Model单位信息.iDwid;
                }
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
                {
                    chis.GY协定处方.AddRange(list协定内容);
                    chis.SaveChanges();
                }
                msgBalloonHelper.BalloonShow("协定处方保存成功");
                this.DialogResult = DialogResult.OK;
            }
            catch (DbEntityValidationException dbEx)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage));
                    }
                }
                msgBalloonHelper.ShowInformation(stringBuilder.ToString());
            }
            catch (Exception ex)
            {
                string errorMsg = "错误：";
                if (ex.InnerException == null)
                    errorMsg += ex.Message + "，";
                else if (ex.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.Message + "，";
                else if (ex.InnerException.InnerException.InnerException == null)
                    errorMsg += ex.InnerException.InnerException.Message;
                msgBalloonHelper.ShowInformation(errorMsg);
            }
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void textEdit协定处方名称_EditValueChanged(object sender, EventArgs e)
        {
            textEdit拼音代码.Text = WEISHENG.COMM.pinyinHelper.Get拼音首字符(textEdit协定处方名称.Text);
        }

        private void radioGroup类型_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (radioGroup类型.SelectedIndex)
            {
                case 0:
                    textEdit协定处方名称.Text = HIS.COMM.zdInfo.ModelUserInfo.用户名 + "_";
                    break;
                case 1:
                    textEdit协定处方名称.Text = HIS.COMM.zdInfo.ModelUserInfo.科室名称 + "_";
                    break;
                case 2:
                    if (HIS.COMM.zdInfo.ModelUserInfo.用户名.ToUpper() != "ADMIN")
                    {
                        MessageBox.Show("建立全院协定处方，请使用管理员账户登录。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    textEdit协定处方名称.Text = "全院_";
                    break;
            }
            textEdit协定处方名称.Focus();
        }
    }
}