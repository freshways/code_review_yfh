﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HIS.Model;

namespace HIS.COMM
{
    public partial class XtraForm科室选择 : DevExpress.XtraEditors.XtraForm
    {

        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
        public List<GY科室设置> selectedPower = new List<GY科室设置>();
        public XtraForm科室选择()
        {
            InitializeComponent();
            try
            {
                this.gridControl1.DataSource = chis.GY科室设置.Where(c => c.是否禁用 == false).ToList();
                gridView1.BestFitColumns();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            selectedPower.Clear();
            foreach (int i in gridView1.GetSelectedRows())
            {
                var row = gridView1.GetRow(i) as GY科室设置;
                selectedPower.Add(row);
            }
            this.DialogResult = DialogResult.OK;
        }


    }
}