﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HIS.COMM
{
    public class Class科室选择
    {
        public static string sGET科室名称(string _s科室编码)
        {
            if (_s科室编码 == "未定义" || string.IsNullOrEmpty(_s科室编码))
            {
                return "未定义";
            }
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 科室名称 from GY科室设置 where 科室编码=" + _s科室编码).Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "未定义";
            }
            else
            {
                try
                {
                    return Convert.ToString(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return "未定义";
                }
            }
        }
        public static System.Data.DataTable dt住院科室()
        {
            string SQL =
            "SELECT ID, 科室名称, 科室编码, 拼音代码, 单位编码, 是否禁用" + "\r\n" +
            "FROM   dbo.gy科室设置" + "\r\n" +
            "where  单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码 + " and 是否住院科室 = 1 and isnull([是否禁用], 0) = 0";

            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, System.Data.CommandType.Text, SQL).Tables[0];
        }
        public static System.Data.DataTable dt病区列表()
        {
            string SQL =
            "SELECT ID, 科室名称, 科室编码, 拼音代码, 单位编码, 是否禁用 FROM dbo.gy科室设置" + "\r\n" +
            "where 单位编码=" + HIS.COMM.zdInfo.Model科室信息.单位编码 + " and 是否病区科室=1";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, System.Data.CommandType.Text, SQL).Tables[0];
        }
    }
}
