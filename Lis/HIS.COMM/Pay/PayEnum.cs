﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.Pay
{
    /// <summary>
    /// 支付类型，用于 SF支付信息 业务相关
    /// </summary>
    public enum PaymentType
    {
        现金 = 1,
        医保结算 = 2,
        微信 = 3,
        支付宝 = 4,
        一码付 = 5,//易联众一码付
        预充值账户 = 6//马站医院这种存在门诊预付费的情况
    }

    public enum BusinessType
    {
        门诊收款 = 1,
        门诊预交款 = 2,
        住院结算 = 3,
        住院预交款 = 4,
        村卫生室 = 5,
        疫苗代收 = 6,
        其他 = 7
    }
}
