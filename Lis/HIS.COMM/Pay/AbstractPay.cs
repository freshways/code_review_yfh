﻿using HIS.COMM.InterFace;
using HIS.Model;
using System;
using System.Data.Entity;
using System.Text;
using WEISHENG.COMM;

namespace HIS.COMM.Pay
{
    public abstract class AbstractPay
    {
        HIS.Model.SF支付信息 sf支付信息 = new SF支付信息();
        public AbstractPay()
        {
            sf支付信息.UUID = Guid.NewGuid().ToString("N");
            sf支付信息.支付时间 = DateTime.Now;
            sf支付信息.作废标志 = false;
            sf支付信息.分院编码 = HIS.COMM.zdInfo.Model站点信息.分院编码;
            sf支付信息.单位编码 = HIS.COMM.zdInfo.Model站点信息.单位编码;
            sf支付信息.收款人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码;
            sf支付信息.CreateTime = DateTime.Now;
            sf支付信息.更新时间 = DateTime.Now;
        }

        public SF支付信息 Sf支付信息 { get => sf支付信息; set => sf支付信息 = value; }


        protected bool SavePayInfo()
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                try
                {
                    chis.SF支付信息.Attach(Sf支付信息);
                    chis.Entry(Sf支付信息).State = System.Data.Entity.EntityState.Added;
                    chis.SaveChanges();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                                validationErrors.Entry.Entity.GetType().FullName,
                                validationError.PropertyName,
                                validationError.ErrorMessage));
                        }
                    }
                    msgBalloonHelper.ShowInformation(stringBuilder.ToString());
                    return false;
                }
                catch (Exception ex)
                {
                    string errorMsg = "错误：";
                    if (ex.InnerException == null)
                        errorMsg += ex.Message + "，";
                    else if (ex.InnerException.InnerException == null)
                        errorMsg += ex.InnerException.Message + "，";
                    else if (ex.InnerException.InnerException.InnerException == null)
                        errorMsg += ex.InnerException.InnerException.Message;
                    msgBalloonHelper.ShowInformation(errorMsg);
                    return false;
                }

            }
        }

        /// <summary>
        /// 设置支付信息
        /// </summary>
        /// <param name="businessType">业务类型</param>
        /// <param name="_settlementID">结算ID</param>
        /// <param name="_payMoney">支付金额</param>
        /// <returns></returns>
        public bool SetPayInfo(BusinessType businessType, decimal _settlementID, decimal _payMoney,en支付渠道 _en支付渠道)
        {
            if (_en支付渠道 == en支付渠道.门诊医生线上)
            {
                Sf支付信息.是否诊间结算 = true;
            }
            else
            {
                Sf支付信息.是否诊间结算 = false;
            }
            this.Sf支付信息.业务类型 = businessType.ToString();
            this.Sf支付信息.结算ID = _settlementID;
            this.Sf支付信息.支付金额 = _payMoney;
            this.Sf支付信息.支付渠道 = _en支付渠道.ToString();
            return true;
        }

        /// <summary>
        /// 撤销支付信息，采用数据库事物处理的方式
        /// </summary>
        /// <returns></returns>
        public bool RevocationPayInfo(CHISEntities chis)
        {
            Sf支付信息.作废标志 = true;
            chis.SF支付信息.Attach(Sf支付信息);
            chis.Entry(Sf支付信息).Property(c => c.作废标志).IsModified = true;
            return true;
        }

        /// <summary>
        /// 撤销支付信息，直接保存
        /// </summary>
        /// <returns></returns>
        public bool RevocationSavePayInfo()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Sf支付信息.支付类型))
                {
                    msgHelper.ShowInformation("支付信息未设置");
                    return false;
                }
                if (Sf支付信息.支付金额 == 0)
                {
                    return false;
                }
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {

                    try
                    {
                        Sf支付信息.作废标志 = true;
                        chis.SF支付信息.Attach(Sf支付信息);
                        chis.Entry(Sf支付信息).Property(c => c.作废标志).IsModified = true;
                        chis.SaveChanges();
                        HIS.COMM.msgBalloonHelper.BalloonShow("支付信息更新成功");
                        return true;
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                                    validationErrors.Entry.Entity.GetType().FullName,
                                    validationError.PropertyName,
                                    validationError.ErrorMessage));
                            }
                        }
                        msgBalloonHelper.ShowInformation(stringBuilder.ToString());
                        return false;
                    }
                    catch (Exception ex)
                    {
                        string errorMsg = "错误：";
                        if (ex.InnerException == null)
                            errorMsg += ex.Message + "，";
                        else if (ex.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.Message + "，";
                        else if (ex.InnerException.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.InnerException.Message;
                        msgBalloonHelper.ShowInformation(errorMsg);
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                msgHelper.ShowInformation(ex.Message);
                return false;
            }

        }
    }
}
