﻿using PropertyChanged;
using System;
using System.Text.RegularExpressions;

namespace HIS.COMM.Pay
{
    /// <summary>
    /// 身份认证抽象类，用于在多种身份认证之间共享通用信息，绑定到业务界面
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public abstract class AbstractPerson
    {
        string _姓名;
        string _身份证号;
        string _性别;
        string _民族;
        string _出生日期;
        string _单位名称;
        string _个人编号;
        string _人员类别;
        string _在院状态;
        string _年龄;
        string _年龄单位;
        string _余额;
        decimal _dec账户号;
        string _精准扶贫标志 = "0";
        string _民政人员标志 = "0";
        string _GUID;

        public string 姓名 { get => _姓名; set => _姓名 = value; }
        public string 身份证号
        {
            get => _身份证号;
            set
            {
                _身份证号 = value;
                if (value == null)
                {
                    return;
                }
                if (HIS.COMM.SfzhHelper.CheckIDCard(value))
                {
                    string _s年龄;
                    string _s年龄单位;
                    HIS.COMM.Helper.AgeHelper.GetOutAgeAndUnitBySFZH(value, out _s年龄, out _s年龄单位);
                    _年龄 = _s年龄;
                    _年龄单位 = _s年龄单位;
                    _出生日期 = Helper.AgeHelper.GetBirthdayBySFZH(value);
                }
            }
        }
        public string 性别
        {
            get
            {
                if (_性别 == null)
                {
                    return _性别;
                }
                if (Regex.IsMatch(_性别, @"[\u4e00-\u9fbb]+$"))//如果已经是汉字，不处理
                {
                    return _性别;
                }
                return _性别 == "1" ? "男" : "女";
            }
            set => _性别 = value;
        }
        public string 民族 { get => _民族; set => _民族 = value; }
        public string 出生日期 { get => _出生日期; set => _出生日期 = value; }
        public string 单位名称 { get => _单位名称; set => _单位名称 = value; }
        public string 个人编号 { get => _个人编号; set => _个人编号 = value; }
        public string 人员类别 { get => _人员类别; set => _人员类别 = value; }
        public string 在院状态 { get => _在院状态; set => _在院状态 = value; }
        public string 年龄 { get => _年龄; set => _年龄 = value; }
        public string 年龄单位 { get => _年龄单位; set => _年龄单位 = value; }
        public string 余额 { get => _余额; set => _余额 = value; }
        public string GUID
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_GUID))
                {
                    _GUID = Guid.NewGuid().ToString("N");
                }
                return _GUID;
            }
            set => _GUID = value;
        }

        public decimal Dec账户号 { get => _dec账户号; set => _dec账户号 = value; }
        public string 精准扶贫标志 { get => _精准扶贫标志; set => _精准扶贫标志 = value; }
        public string 民政人员标志 { get => _民政人员标志; set => _民政人员标志 = value; }
    }
}
