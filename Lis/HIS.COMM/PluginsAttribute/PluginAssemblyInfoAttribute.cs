﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEISHENG.COMM.PluginsAttribute
{

    /// <summary>
    /// 标注程序集信息
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly)]
    public class PluginAssemblyInfoAttribute : System.Attribute
    {

        public PluginAssemblyInfoAttribute() { }

        public PluginAssemblyInfoAttribute(string pluginName, string version, string author, string webpage,
            bool loadWhenStart, int index, string guid, string Tel, string _s详细说明,
            string s运行目录 = "", string s插件授权 = "")
        {
            _Name = pluginName;
            _Version = version;
            _Author = author;
            _Webpage = webpage;
            _LoadWhenStart = loadWhenStart;
            _Index = index;
            _GUID = guid;
            _Tel = Tel;
            this._s详细说明 = _s详细说明;
            _s运行目录 = s运行目录;
            _s插件授权 = s插件授权;
        }

        private string _s插件授权;

        private string _s运行目录;
        private string _s详细说明;

        private string _Tel;

        public string Tel
        {
            get { return _Tel; }
            set { _Tel = value; }
        }

        private string _GUID;

        public string GUID
        {
            get { return _GUID; }
            set { _GUID = value; }
        }

        public string pluginName
        {
            get
            {
                return _Name;
            }
        }

        public string Version
        {
            get
            {
                return _Version;
            }
        }

        public string Author
        {
            get
            {
                return _Author;
            }
        }

        public string Webpage
        {
            get
            {
                return _Webpage;
            }
        }

        public bool LoadWhenStart
        {
            get
            {
                return _LoadWhenStart;
            }
        }

        /// <summary>
        /// 用来存储一些有用的信息
        /// </summary>
        public object Tag
        {
            get
            {
                return _Tag;
            }
            set
            {
                _Tag = value;
            }
        }

        public int Index
        {
            get
            {
                return _Index;
            }
            set
            {
                _Index = value;
            }
        }

        public string S详细说明
        {
            get
            {
                return _s详细说明;
            }

            set
            {
                _s详细说明 = value;
            }
        }

        public string S运行目录
        {
            get
            {
                return _s运行目录;
            }

            set
            {
                _s运行目录 = value;
            }
        }

        public string S插件授权
        {
            get
            {
                return _s插件授权;
            }

            set
            {
                _s插件授权 = value;
            }
        }

        private string _Name = "";
        private string _Version = "";
        private string _Author = "";
        private string _Webpage = "";
        private object _Tag = null;
        private int _Index = 0;

        private bool _LoadWhenStart = true;
    }
}
