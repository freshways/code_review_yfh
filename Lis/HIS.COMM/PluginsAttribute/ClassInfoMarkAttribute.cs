﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEISHENG.COMM.PluginsAttribute
{
    //用 AttributeUsage 属性标记来限制本属性的应用范围
    [AttributeUsage(AttributeTargets.Class)]
    public class ClassInfoMarkAttribute : System.Attribute
    {
        //特性类的命名参数，可以指定默认值
        public string _s图标名称;
        public int _i显示顺序;
         
        public ClassInfoMarkAttribute(string s父ID, string s键ID, string GUID, string assemblyName, string formClassName, bool b是否全屏)//特性类的定位参数，对类型有要求
        {
            HIS.Model.pubQxNew2 pubQxNew2 = new HIS.Model.pubQxNew2();
            pubQxNew2.父ID = Convert.ToInt32(s父ID);
            pubQxNew2.键ID = Convert.ToInt32(s键ID);
            pubQxNew2.guid = GUID;
            pubQxNew2.模块名 = assemblyName;//程序集名称
            pubQxNew2.窗口名 = formClassName;//命名空间 - 程序集名 + 类名
            pubQxNew2.全屏打开 = b是否全屏;
            pubQxNew2.图标名称 = _s图标名称;
            pubQxNew2.显示顺序 = _i显示顺序;
            //pubQxNew2.图标索引
        }
        public ClassInfoMarkAttribute()
        {

        }
        public ClassInfoMarkAttribute(HIS.Model.pubQxNew2 _pubQxNew2)
        {
        }
    }
}
