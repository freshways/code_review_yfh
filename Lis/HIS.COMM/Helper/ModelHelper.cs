﻿using HIS.Model;
using System.Collections.Generic;
using System.Linq;

namespace HIS.COMM.Helper
{
    public class ModelHelper
    {
        public static List<T> getMode<T>(string sql)
        {
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
            {
                List<T> list = chis.Database.SqlQuery<T>(sql).ToList();
                return list;
            }
        }
    }
}
