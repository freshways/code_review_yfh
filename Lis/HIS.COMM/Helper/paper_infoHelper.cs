﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace HIS.COMM.Helper
{
    public class paper_infoHelper
    {
        public static Boolean b设置纸张(string conString, string dymk, string dwbm, int width, int height, int top, int bottom, int left, int right)
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(conString, CommandType.Text,
                "SELECT top 1 打印机名称,纸张名称, 纸张宽度, 纸张高度, 上页边距, 下页边距, 左页边距, 右页边距, 是否直接打印, 是否打印预览 FROM GY打印设置 where 单位编码=" + dwbm + " and 打印模块='" + dymk + "'").Tables[0];
            if (dt.Rows.Count == 1)
            {
                string SQL =
                   "UPDATE [GY打印设置] " + "\r\n" +
                   "SET  " + "\r\n" +
                   "[纸张宽度] = " + width.ToString() + "," + "\r\n" +
                   "[纸张高度] = " + height.ToString() + ", [上页边距] = " + top.ToString() + ", [下页边距] = " + bottom.ToString() + ", " + "\r\n" +
                   "[左页边距] = " + left.ToString() + ", [右页边距] = " + right.ToString() + "\r\n" +
                   "where 单位编码=" + dwbm + " and 打印模块='" + dymk + "'";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text, SQL);
            }
            else
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
                    "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
                       " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注],[单位编码]) " +
                       "  VALUES ('" + dymk + "','Printer','Pjdy'," + width.ToString() + "," + height.ToString() + "," + top + "," + bottom + "," + left + "," + right + ",0,1,'1'," + dwbm + ")");
            }
            return true;
        }
        public static Boolean b新增纸张(string conString, string dymk, string dwbm, int width, int height, int top, int bottom, int left, int right)
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(conString, CommandType.Text,
                "SELECT top 1 打印机名称,纸张名称, 纸张宽度, 纸张高度, 上页边距, 下页边距, 左页边距, 右页边距, 是否直接打印, 是否打印预览 FROM GY打印设置 where 单位编码=" + dwbm + " and 打印模块='" + dymk + "'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
                "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
                " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注],[单位编码]) " +
                "  VALUES ('" + dymk + "','Printer','Pjdy'," + width.ToString() + "," + height.ToString() + "," + top + "," + bottom + "," + left + "," + right + ",0,1,'1'," + dwbm + ")");
            }
            return true;
        }

        public static Boolean setPaper_oracle(string conString, string dymk, string dwbm, int width, int height, int top, int bottom, int left, int right)
        {
            //DataTable dt = WEISHENG.COMM.Db.OracleOdpHelper.ExecuteDataset(conString, CommandType.Text,
            //    "SELECT 打印机名称,纸张名称, 纸张宽度, 纸张高度, 上页边距, 下页边距, 左页边距, 右页边距, 是否直接打印, 是否打印预览 " +
            //    "FROM pub_paper_set where 单位编码=" + dwbm + " and 打印模块='" + dymk + "' and mac地址='" + WEISHENG.COMM.zdInfo.HardInfo.MacAddress + "'").Tables[0];
            //if (dt.Rows.Count == 1)
            //{
            //}
            //else
            //{
            //    WEISHENG.COMM.Db.OracleOdpHelper.ExecuteNonQuery(conString, CommandType.Text,
            //        "INSERT INTO pub_paper_set (mac地址,站点名称,打印模块 ,打印机名称 ,纸张名称,纸张宽度,纸张高度 " +
            //           " ,上页边距,下页边距,左页边距,右页边距,是否直接打印,是否打印预览,备注,单位编码) " +
            //           "  VALUES ('" + WEISHENG.COMM.zdInfo.HardInfo.MacAddress + "','" + WEISHENG.COMM.zdInfo.s站点名称 + "','" +
            //           dymk + "','Printer','Pjdy'," + width.ToString() + "," + height.ToString() + "," + top + "," + bottom + "," + left + "," + right + ",0,1,'1'," + dwbm + ")");
            //}
            return true;
        }
        //public static Boolean getPaper(string conString, string dymk)
        //{
        //    DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(conString, CommandType.Text,
        //        "SELECT 打印机名称,纸张名称, 纸张宽度, 纸张高度, 上页边距, 下页边距, 左页边距, 右页边距, 是否直接打印, 是否打印预览 FROM GY打印设置 where 打印模块='" + dymk + "'").Tables[0];

        //    if (dt.Rows.Count == 1)
        //    {
        //        dyjmc = dt.Rows[0]["打印机名称"].ToString();
        //        zzmc = dt.Rows[0]["纸张名称"].ToString();
        //        zzkd = Convert.ToInt16(dt.Rows[0]["纸张宽度"]);
        //        zzgd = Convert.ToInt16(dt.Rows[0]["纸张高度"]);
        //        ybj_top = Convert.ToInt16(dt.Rows[0]["上页边距"]);
        //        ybj_bottom = Convert.ToInt16(dt.Rows[0]["下页边距"]);
        //        ybj_left = Convert.ToInt16(dt.Rows[0]["左页边距"]);
        //        ybj_right = Convert.ToInt16(dt.Rows[0]["右页边距"]);
        //        sfzjdy = Convert.ToBoolean(dt.Rows[0]["是否直接打印"]);
        //        sfdyyl = Convert.ToBoolean(dt.Rows[0]["是否打印预览"]);
        //    }
        //    else
        //    {
        //        if (dymk == "住院发票")
        //        {
        //            HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
        //                   "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
        //                      " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注]) " +
        //                      "  VALUES ('" + dymk + "','Printer','Pjdy',3760,1400,40,40,40,40,40,1,'1')");

        //            dyjmc = "Printer";
        //            zzmc = "Pjdy";
        //            zzkd = 3760;
        //            zzgd = 1400;
        //            ybj_top = 40;
        //            ybj_bottom = 40;
        //            ybj_left = 40;
        //            ybj_right = 40;
        //            sfzjdy = false;
        //            sfdyyl = true;

        //        }
        //        else if (dymk == "门诊发票")
        //        {
        //            HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
        //                   "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
        //                      " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注]) " +
        //                      "  VALUES ('" + dymk + "','Printer','Pjdy',2400,932,0,0,0,0,0,1,'1')");

        //            dyjmc = "Printer";
        //            zzmc = "Pjdy";
        //            zzkd = 2400;
        //            zzgd = 932;
        //            ybj_top = 40;
        //            ybj_bottom = 40;
        //            ybj_left = 40;
        //            ybj_right = 40;
        //            sfzjdy = false;
        //            sfdyyl = true;

        //        }
        //        else
        //        {
        //            HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
        //                "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
        //                   " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注]) " +
        //                   "  VALUES ('" + dymk + "','Printer','Pjdy',2100,1200,40,40,40,40,0,1,'1')");

        //            dyjmc = "Printer";
        //            zzmc = "Pjdy";
        //            zzkd = 2100;
        //            zzgd = 1200;
        //            ybj_top = 40;
        //            ybj_bottom = 40;
        //            ybj_left = 40;
        //            ybj_right = 40;
        //            sfzjdy = false;
        //            sfdyyl = true;
        //        }
        //    }
        //    return true;
        //}



        public static Boolean getPaper(string conString, string dymk, string dwbm)
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(conString, CommandType.Text,
                "SELECT top 1 打印机名称,纸张名称, 纸张宽度, 纸张高度, 上页边距, 下页边距, 左页边距, 右页边距, 是否直接打印, 是否打印预览 FROM GY打印设置 where 单位编码=" + dwbm + " and 打印模块='" + dymk + "'").Tables[0];

            if (dt.Rows.Count == 1)
            {
                dyjmc = dt.Rows[0]["打印机名称"].ToString();
                zzmc = dt.Rows[0]["纸张名称"].ToString();
                zzkd = Convert.ToInt16(dt.Rows[0]["纸张宽度"]);
                zzgd = Convert.ToInt16(dt.Rows[0]["纸张高度"]);
                ybj_top = Convert.ToInt16(dt.Rows[0]["上页边距"]);
                ybj_bottom = Convert.ToInt16(dt.Rows[0]["下页边距"]);
                ybj_left = Convert.ToInt16(dt.Rows[0]["左页边距"]);
                ybj_right = Convert.ToInt16(dt.Rows[0]["右页边距"]);
                sfzjdy = Convert.ToBoolean(dt.Rows[0]["是否直接打印"]);
                sfdyyl = Convert.ToBoolean(dt.Rows[0]["是否打印预览"]);
            }
            else
            {
                if (dymk == "住院发票")
                {
                    HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
                           "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
                              " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注],单位编码) " +
                              "  VALUES ('" + dymk + "','Printer','Pjdy',3760,1400,40,40,40,40,40,1,'1'," + dwbm + ")");

                    dyjmc = "Printer";
                    zzmc = "Pjdy";
                    zzkd = 3760;
                    zzgd = 1400;
                    ybj_top = 40;
                    ybj_bottom = 40;
                    ybj_left = 40;
                    ybj_right = 40;
                    sfzjdy = false;
                    sfdyyl = true;

                }
                else if (dymk == "门诊发票")
                {
                    HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
                           "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
                              " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注],单位编码) " +
                              "  VALUES ('" + dymk + "','Printer','Pjdy',2400,932,0,0,0,0,0,1,'1'," + dwbm + ")");

                    dyjmc = "Printer";
                    zzmc = "Pjdy";
                    zzkd = 2400;
                    zzgd = 932;
                    ybj_top = 40;
                    ybj_bottom = 40;
                    ybj_left = 40;
                    ybj_right = 40;
                    sfzjdy = false;
                    sfdyyl = true;

                }
                else
                {
                    HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
                        "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
                           " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注],[单位编码]) " +
                           "  VALUES ('" + dymk + "','Printer','Pjdy',2100,1200,40,40,40,40,0,1,'1'," + dwbm + ")");

                    dyjmc = "Printer";
                    zzmc = "Pjdy";
                    zzkd = 2100;
                    zzgd = 1200;
                    ybj_top = 40;
                    ybj_bottom = 40;
                    ybj_left = 40;
                    ybj_right = 40;
                    sfzjdy = false;
                    sfdyyl = true;
                }

                //HIS.Model.Dal.SqlHelper.ExecuteNonQuery(conString, CommandType.Text,
                //    "INSERT INTO [GY打印设置] ([打印模块] ,[打印机名称] ,[纸张名称],[纸张宽度],[纸张高度] " +
                //       " ,[上页边距],[下页边距],[左页边距],[右页边距],[是否直接打印],[是否打印预览],[备注],[单位编码]) " +
                //       "  VALUES ('" + dymk + "','Printer','Pjdy',2100,1200,20,20,20,20,0,1,'1',"+dwbm+")");

                //dyjmc = "Printer";
                //zzmc = "Pjdy";
                //zzkd = 2100;
                //zzgd = 1200;
                //ybj_top = 20;
                //ybj_bottom = 20;
                //ybj_left = 20;
                //ybj_right = 20;
                //sfzjdy = false;
                //sfdyyl = true;

            }

            return true;

        }

        /// <summary>
        /// 打印机名称
        /// </summary>
        private static string dyjmc = "";

        public static string Dyjmc
        {
            get { return paper_infoHelper.dyjmc; }
            set { paper_infoHelper.dyjmc = value; }
        }
        /// <summary>
        /// 是否打印预览
        /// </summary>
        private static Boolean sfdyyl = true;

        public static Boolean Sfdyyl
        {
            get { return paper_infoHelper.sfdyyl; }
            set { paper_infoHelper.sfdyyl = value; }
        }
        /// <summary>
        /// 是否直接打印
        /// </summary>
        private static Boolean sfzjdy = false;

        public static Boolean Sfzjdy
        {
            get { return paper_infoHelper.sfzjdy; }
            set { paper_infoHelper.sfzjdy = value; }
        }
        /// <summary>
        /// 右页边距
        /// </summary>
        private static int ybj_right = 0;

        public static int i右边距
        {
            get { return paper_infoHelper.ybj_right; }
            set { paper_infoHelper.ybj_right = value; }
        }
        /// <summary>
        /// 左页边距
        /// </summary>
        private static int ybj_left = 0;

        public static int i左边距
        {
            get { return paper_infoHelper.ybj_left; }
            set { paper_infoHelper.ybj_left = value; }
        }
        /// <summary>
        /// 下页边距
        /// </summary>
        private static int ybj_bottom = 0;

        public static int i下边距
        {
            get { return paper_infoHelper.ybj_bottom; }
            set { paper_infoHelper.ybj_bottom = value; }
        }
        /// <summary>
        /// 上页边距
        /// </summary>
        private static int ybj_top = 0;

        public static int i上边距
        {
            get { return paper_infoHelper.ybj_top; }
            set { paper_infoHelper.ybj_top = value; }
        }
        /// <summary>
        /// 纸张宽度
        /// </summary>
        private static int zzkd = 0;

        public static int i纸张宽度
        {
            get { return paper_infoHelper.zzkd; }
            set { paper_infoHelper.zzkd = value; }
        }

        /// <summary>
        /// 纸张高度
        /// </summary>
        private static int zzgd = 0;

        public static int i纸张高度
        {
            get { return paper_infoHelper.zzgd; }
            set { paper_infoHelper.zzgd = value; }
        }
        /// <summary>
        /// 纸张名称
        /// </summary>
        private static string zzmc = "";
        public static string Zzmc
        {
            get
            {
                return zzmc.Trim();
            }
            set
            {
                zzmc = value;
            }
        }    //注册单位
    }
}
