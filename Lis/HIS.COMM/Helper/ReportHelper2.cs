﻿using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraReports.UI;
using HIS.COMM.Report;
using HIS.Model;
using System;
using System.Drawing.Printing;
using System.Linq;

namespace HIS.COMM.Helper
{
    /// <summary>
    /// 通用打印帮助类
    /// 打印控制优先级
    /// 代码>配置.PubStationPaperSettings>配置.PubCommonPaperSettings
    /// </summary>
    public class ReportHelper2
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        //private void btnPrint_Click(object sender, EventArgs e)
        //{
        //    //this.chartControl1.ShowPrintPreview(DevExpress.XtraCharts.Printing.PrintSizeMode.Zoom);
        //    DevExpress.XtraPrintingLinks.CompositeLink compositeLink = new DevExpress.XtraPrintingLinks.CompositeLink();
        //    DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem();

        //    compositeLink.PrintingSystem = ps;
        //    compositeLink.Landscape = true;
        //    compositeLink.PaperKind = System.Drawing.Printing.PaperKind.A4;

        //    DevExpress.XtraPrinting.PrintableComponentLink link = new DevExpress.XtraPrinting.PrintableComponentLink(ps);
        //    ps.PageSettings.Landscape = true;
        //    link.Component = this.chartControl1;
        //    compositeLink.Links.Add(link);

        //    link.CreateDocument();  //建立文档
        //    ps.PreviewFormEx.Show();//进行预览
        //}

        //public static void test(XtraReport report)
        //{


        //    report.ShowPreviewDialog();
        //    //PrintableComponentLink link = new PrintableComponentLink(new PrintingSystem());
        //    //link.Margins = margins;
        //    //link.Component = report;
        //}


        /// <summary>
        /// 打印功能
        /// </summary>
        /// <param name="panel">打印控件</param>
        /// <param name="title">标题</param>
        /// <param name="pageSize">纸张类型</param>
        /// <param name="margins">边距</param>
        protected virtual void Print(IPrintable panel, string title = null, PaperKind pageSize = PaperKind.A4, System.Drawing.Printing.Margins margins = null)
        {
            PrintingSystem ps = new PrintingSystem();
            CompositeLink link = new CompositeLink(ps);
            PrintableComponentLink printableLink = new PrintableComponentLink() { Component = panel };

            ps.Links.Add(link);
            link.Links.Add(printableLink);
            link.Landscape = true;//横向
            link.PaperKind = pageSize;//设置纸张大小
            if (margins == null)
                margins = new System.Drawing.Printing.Margins(10, 10, 50, 50);
            link.Margins = margins;
            //判断是否有标题，有则设置
            if (!string.IsNullOrEmpty(title))
            {
                PageHeaderFooter phf = link.PageHeaderFooter as PageHeaderFooter;
                phf.Header.Content.Clear();
                phf.Header.Content.AddRange(new string[] { "", title, "" });
                phf.Header.Font = new System.Drawing.Font("宋体", 14, System.Drawing.FontStyle.Bold);
                phf.Header.LineAlignment = BrickAlignment.Center;
                phf.Footer.Content.AddRange(new string[] { "", String.Format("打印时间: {0:g}", DateTime.Now), "" });
            }
            link.CreateDocument(); //建立文档
            ps.PreviewRibbonFormEx.Show();//进行预览
        }


        /// <summary>
        /// 代码>配置.PubStationPaperSettings>配置.PubCommonPaperSettings
        /// </summary>
        /// <param name="report"></param>
        /// <param name="printParam"></param>
        /// <returns></returns>
        public static bool Print(XtraReport report, PrintParams printParam)
        {
            try
            {
                //1、刷新设置数据
                var paperStationSettings = chis.PubStationPaperSettings.Where(c => c.StationID == WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress &&
                c.PaperName == printParam.paperName && c.InvokeModuleName == printParam.invokeModuleName).FirstOrDefault();
                var paperCommonSettings = chis.PubCommonPaperSettings.Where(c => c.PaperName == printParam.paperName).FirstOrDefault();
                if (paperCommonSettings != null)
                {
                    chis.Entry(paperCommonSettings).Reload();//强制刷新数据
                }
                if (paperStationSettings != null)
                {
                    chis.Entry(paperStationSettings).Reload();
                }
                //2、自动添加默认纸张大小
                if (paperCommonSettings == null && paperStationSettings == null)
                {
                    var newPaperName = new HIS.Model.PubCommonPaperSetting()
                    {
                        PaperName = printParam.paperName,
                        PaperWidth = printParam.width,
                        PaperHeight = printParam.height,
                        MarginTop = printParam.marginTop,
                        MarginBottom = printParam.marginBottom,
                        MarginLeft = printParam.marginLeft,
                        MarginRight = printParam.marginRight,
                        IsPreView = true,
                        Remark = "自动添加默认大小",
                        CanPrint = true,
                    };
                    using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
                    {
                        chis.PubCommonPaperSettings.Attach(newPaperName);
                        chis.Entry(newPaperName).State = System.Data.Entity.EntityState.Added;
                        chis.SaveChanges();
                        msgBalloonHelper.BalloonShow("获取纸张定义失败，已经自动添加");                      
                    }
                     paperCommonSettings = chis.PubCommonPaperSettings.Where(c => c.PaperName == printParam.paperName).FirstOrDefault();            
                }

                //3、配置打印参数  预览，打印机名，可否打印
                printParam.isPreview = paperStationSettings != null ? paperStationSettings.IsPreView : paperCommonSettings.IsPreView;
                printParam.printName = paperStationSettings != null ? paperStationSettings.PrinterName : null;
                printParam.canPrint = paperStationSettings != null ? paperStationSettings.CanPrint : paperCommonSettings.CanPrint;
                printParam.width = paperStationSettings != null ? paperStationSettings.PaperWidth : paperCommonSettings.PaperWidth;
                printParam.height = paperStationSettings != null ? paperStationSettings.PaperHeight : paperCommonSettings.PaperHeight;
                printParam.marginTop = paperStationSettings != null ? paperStationSettings.MarginTop : paperCommonSettings.MarginTop;
                printParam.marginBottom = paperStationSettings != null ? paperStationSettings.MarginBottom : paperCommonSettings.MarginBottom;
                printParam.marginLeft = paperStationSettings != null ? paperStationSettings.MarginLeft : paperCommonSettings.MarginLeft;
                printParam.marginRight = paperStationSettings != null ? paperStationSettings.MarginRight : paperCommonSettings.MarginRight;
                printParam.Landscape = paperStationSettings != null ? paperStationSettings.Landscape : paperCommonSettings.Landscape;
                report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
                report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
                System.Drawing.Printing.Margins margins = new Margins(printParam.marginLeft, printParam.marginRight, printParam.marginTop, printParam.marginBottom);
                report.Margins = margins;
                report.PageWidth = printParam.width;
                report.PageHeight = printParam.height;
                report.Landscape = printParam.Landscape;
                report.Tag = printParam;
                report.CreateDocument();
                report.PrintingSystem.ShowMarginsWarning = false;
                report.PrintingSystem.ShowPrintStatusDialog = false;
                if (printParam.isPreview)
                {                 
                    new Report.XtraFormCommonPrintPreview(report, printParam).ShowDialog();
                }
                else
                {
                    //如果没有指定打印机名称，直接默认打印；指定打印机时按照指定打印机去打印
                    if (string.IsNullOrEmpty(printParam.printName)) report.Print();
                    else
                    {//指定打印机时有可能会出现找不到打印机的情况【网络打印机】，如果出现异常，抓取并给提示
                        try
                        {
                            report.Print(printParam.printName);
                        }
                        catch (Exception ex)
                        {
                            msgBalloonHelper.BalloonShow("指定打印机失败，将用默认打印机！\n" + ex.Message, "提示");
                            new Report.XtraFormCommonPrintPreview(report, printParam).ShowDialog();
                            //report.Print();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
                return false;
            }
        }
    }
}
