﻿using HIS.Model;
using System;
using System.Linq;

namespace HIS.COMM.Helper
{
    /// <summary>
    /// 软件界面个性化自定义设置
    /// todo:把表格自定义设置保存到这里?
    /// todo:区分按照个人和按照站点标志保存设置
    /// </summary>
    public class LayoutHelper
    {

        public static void Save(string formName, string itemName, string itemValue)
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
            {
                PubFormLayout pubFormLayout = new PubFormLayout()
                {
                    SiteID = HIS.COMM.zdInfo.Model站点信息.Mac地址,
                    FormName = formName,
                    ItemName = itemName,
                    ItemValue = itemValue,
                    UserID = HIS.COMM.zdInfo.ModelUserInfo.用户编码,
                    createTime = DateTime.Now
                };
                var item = chis.PubFormLayouts.Where(c => c.SiteID == pubFormLayout.SiteID
                && c.FormName == formName && c.ItemName == itemName && c.UserID == pubFormLayout.UserID).FirstOrDefault();
                if (item == null)
                {
                    chis.PubFormLayouts.Attach(pubFormLayout);
                    chis.Entry(pubFormLayout).State = System.Data.Entity.EntityState.Added;
                }
                else
                {
                    if (item.ItemValue == pubFormLayout.ItemValue)
                    {
                        return;
                    }
                    item.ItemValue = pubFormLayout.ItemValue;

                }
                chis.SaveChanges();
            }
        }

        public static string Load(string formName, string itemName)
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                var item = chis.PubFormLayouts.Where(c => c.SiteID == HIS.COMM.zdInfo.Model站点信息.Mac地址 && c.UserID == HIS.COMM.zdInfo.ModelUserInfo.用户编码
                         && c.ItemName == itemName && c.FormName == formName).FirstOrDefault();
                if (item == null)
                {
                    return "0";
                }
                else
                {
                    chis.Entry(item).Reload();
                    return item.ItemValue;
                }
            }
        }
    }

}

