﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.Helper
{
    /// <summary>
    /// 医嘱格式 
    /// </summary>
    public static class MedicalAdviceFormatHelper
    {
      
        /// <summary>
        /// 去掉小数点后末尾0
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string numberMasker(string param)
        {
            if (param.Split('.').Length == 2)
            {
                string result = param.Split('.')[0];
                string strChar = param.Split('.')[1];
                strChar.ToArray();
                for (int i = strChar.Length - 1; i >= 0; i--)
                {
                    if (strChar[i] == 0 || strChar[i] == '0')
                    {
                        strChar = strChar.Remove(i);
                    }
                    else
                    {
                        break;
                    }
                }
                if (!string.IsNullOrEmpty(strChar.ToString()))
                {
                    return result + "." + strChar.ToString();
                }
                else
                {
                    return result;
                }
            }
            return param;
        }
    }
}
