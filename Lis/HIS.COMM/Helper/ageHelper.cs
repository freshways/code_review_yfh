﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM.Helper
{
    public class AgeHelper
    {
        /// <summary>
        /// 通过身份证号获取性别
        /// </summary>
        /// <param name="_sfzh"></param>
        /// <returns></returns>
        public static string GetGenderBySFZH(string _sfzh)
        {
            try
            {
                string str = _sfzh.Trim();
                string s = "";
                if (str.Length == 0x12)
                {
                    s = str.Substring(14, 3);
                }
                if (str.Length == 15)
                {
                    s = str.Substring(12, 3);
                }
                if ((int.Parse(s) % 2) == 0)
                {
                    return "女";
                }
                return "男";
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// 在控件验证 textBox_IdentityCard 的 Validated事件中定义身份证号码的合法性并根据身份证号码得到生日和性别
        /// </summary>
        public static string GetBirthdayBySFZH(string _sfzh)
        {
            try
            {
                string identityCard = _sfzh.Trim();//获取得到输入的身份证号码

                if (identityCard.Length != 15 && identityCard.Length != 18)//身份证号码只能为15位或18位其它不合法
                {

                    return "";
                }

                string birthday = "";
                string sex = "";
                if (identityCard.Length == 18)//处理18位的身份证号码从号码中得到生日和性别代码
                {
                    birthday = identityCard.Substring(6, 4) + "-" + identityCard.Substring(10, 2) + "-" + identityCard.Substring(12, 2);
                    sex = identityCard.Substring(14, 3);
                }
                if (identityCard.Length == 15)
                {
                    birthday = "19" + identityCard.Substring(6, 2) + "-" + identityCard.Substring(8, 2) + "-" + identityCard.Substring(10, 2);
                    sex = identityCard.Substring(12, 3);
                }
                return birthday;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string GetAgeAndUnitBySFZH(string _sfzh)
        {
            var birthday = GetBirthdayBySFZH(_sfzh);
            return GetAgeAndUnitByBirthday(Convert.ToDateTime(birthday));
        }

        public static string GetAgeAndUnitByBirthday(DateTime dtBirthday, DateTime sometime)
        {
            string _s年龄;
            string _s年龄单位;
            AgeHelper.GetOutAgeAndUnitByBirthday(dtBirthday, DateTime.Now, out _s年龄, out _s年龄单位);
            return _s年龄 + _s年龄单位;
        }

        public static string GetAgeAndUnitByBirthday(DateTime dtBirthday)
        {
            string _s年龄;
            string _s年龄单位;
            AgeHelper.GetOutAgeAndUnitByBirthday(dtBirthday, DateTime.Now, out _s年龄, out _s年龄单位);
            return _s年龄 + _s年龄单位;
        }

        /// <summary>
        /// 通过生日获取年龄
        /// </summary>
        /// <param name="birthdate"></param>
        /// <returns></returns>
        public static int GetAgeByBirthdate(DateTime birthdate)
        {
            string _s年龄;
            string _s年龄单位;
            AgeHelper.GetOutAgeAndUnitByBirthday(birthdate, DateTime.Now, out _s年龄, out _s年龄单位);
            return Convert.ToInt32(_s年龄);
        }





        public static string GetOutAgeAndUnitByBirthday(DateTime dtBirthday, DateTime sometime, out string s年龄, out string s年龄单位)
        {
            string strAge = string.Empty;                         // 年龄的字符串表示
            int intYear = 0;                                    // 岁
            int intMonth = 0;                                    // 月
            int intDay = 0;                                    // 天
            s年龄 = "";
            s年龄单位 = "";
            //// 如果没有设定出生日期, 返回空
            //if (DataType.DateTime_IsNull(ref dtBirthday) == true)
            //{
            //    return "";
            //}

            // 计算天数
            intDay = sometime.Day - dtBirthday.Day;
            if (intDay < 0)
            {
                sometime = sometime.AddMonths(-1);
                intDay += DateTime.DaysInMonth(sometime.Year, sometime.Month);
            }

            // 计算月数
            intMonth = sometime.Month - dtBirthday.Month;
            if (intMonth < 0)
            {
                intMonth += 12;
                sometime = sometime.AddYears(-1);
            }

            // 计算年数
            intYear = sometime.Year - dtBirthday.Year;

            // 格式化年龄输出
            if (intYear >= 1)                                            // 年份输出
            {
                s年龄单位 = "岁";
                s年龄 = intYear.ToString();
                strAge = s年龄 + s年龄单位;
            }

            if (intMonth > 0 && intYear <= 5)                           // 五岁以下可以输出月数
            {
                s年龄单位 = "月";
                s年龄 = intMonth.ToString();
                strAge += s年龄 + s年龄单位;
            }

            if (intDay >= 0 && intYear < 1)                              // 一岁以下可以输出天数
            {
                if (strAge.Length == 0 || intDay > 0)
                {
                    s年龄单位 = "日";
                    s年龄 = intDay.ToString();
                    strAge += s年龄 + s年龄单位;
                }
            }

            return strAge;
        }
        /// <summary>
        /// 通过身份证号返回年龄和年龄单位，用于门诊处方等处，在获取人员信息后转换数据
        /// </summary>
        /// <param name="_sfzh"></param>
        /// <param name="s年龄"></param>
        /// <param name="s年龄单位"></param>
        /// <returns></returns>
        public static string GetOutAgeAndUnitBySFZH(string _sfzh, out string s年龄, out string s年龄单位)
        {
            if (!RegexHelper.IsSFZH(_sfzh))
            {
                msgBalloonHelper.ShowInformation($"{_sfzh}不符合身份证号规范,请核对");
            }
            string _s出生日期 = GetBirthdayBySFZH(_sfzh);
            s年龄 = "";
            s年龄单位 = "";
            return GetOutAgeAndUnitByBirthday(Convert.ToDateTime(_s出生日期), DateTime.Now, out s年龄, out s年龄单位);
        }
    }
}
