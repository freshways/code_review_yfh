﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;


namespace HIS.COMM
{
    public class SfzhHelper
    {
        public static bool saveToDb(string _sconn, HIS.COMM.PersonBase _person, string _s数据源)
        {
            return SaveToDb(_sconn, WEISHENG.COMM.ImageHelper.ImageToBytes(_person.Image), _person.S身份证号, _person.S姓名, _person.S性别, _person.S民族, _person.s出生日期, _person.S家庭地址, _person.S有效期起始, _person.S有效期终止, _person.S民族编码, _s数据源);
        }
        public static bool SaveToDb(string connString, byte[] image, string sfzh, string name, string sex, string nation, string birthday, string address, string validStart, string validEnd, string nationcode, string Qt1)
        {
            if (connString == "" || connString == null)
            {
                return false;
            }
            using (SqlConnection con = new SqlConnection(connString))
            {
                con.Open();
                string sqlCmdIshave = "select count(*) from pubSfzhInfo where cardid='" + sfzh + "'";
                int ishave = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(con, CommandType.Text, sqlCmdIshave));

                string sql = "";
                if (ishave == 0)
                {
                    sql = "insert into pubSfzhInfo (cardid,picture,name, sex, nation, birthday, address,  validStart, validEnd, nationcode,  Qt1) values ('" +
                                   sfzh + "',@photo," + "'" + name +
                                   "','" + sex + "','" +
                                   nation + "','" +
                                   birthday + "','" +
                                   address + "','" +
                                   validStart + "'," +
                                   validEnd + ",'" +
                                   nationcode + "','" +
                                   Qt1 + "')";
                }
                else if (ishave == 1)
                {
                    sql = "update pubSfzhInfo set picture= @photo where cardid='" + sfzh + "'";
                }
                else
                {
                    return false;
                }



                SqlParameter param = new SqlParameter();
                param = new SqlParameter("@photo", SqlDbType.Image);
                param.Value = image;
                SqlCommand commd = new SqlCommand(sql, con);
                commd.Parameters.Add(param);
                try
                {
                    commd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

            }

        }

        public static Image GetFile(string path)
        {
            MemoryStream stream = ReadFile(path);
            return stream == null ? null : Image.FromStream(stream);
        }
        private static MemoryStream ReadFile(string path)
        {
            if (!File.Exists(path))
                return null;

            using (FileStream file = new FileStream(path, FileMode.Open))
            {
                byte[] b = new byte[file.Length];
                file.Read(b, 0, b.Length);

                MemoryStream stream = new MemoryStream(b);
                return stream;
            }
        }


        public static byte[] PhotoToArray(string path)
        {
            FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] bufferPhoto = new byte[stream.Length];
            stream.Read(bufferPhoto, 0, Convert.ToInt32(stream.Length));
            stream.Flush();
            stream.Close();
            return bufferPhoto;
        }

        public static System.Drawing.Image getSfzhImage(string conn, string sfzh)
        {
            if (conn == null || conn == "")
            {
                return Properties.Resources.rytxwcj;
            }
            System.Drawing.Image image = null;
            try
            {
                if (sfzh.Length != 15 && sfzh.Length != 18)
                {
                    return image;
                }
                string strSQL = "select top 1  picture from pubSfzhInfo where cardid='" + sfzh + "'";
                System.Data.SqlClient.SqlDataReader reader = HIS.Model.Dal.SqlHelper.ExecuteReader(conn, CommandType.Text,
                    strSQL);
                reader.Read();
                if (reader.HasRows)
                {
                    MemoryStream ms = new MemoryStream((byte[])reader["picture"]);
                    image = System.Drawing.Image.FromStream(ms, true);
                    string base64string = Convert.ToBase64String((byte[])reader["picture"]);
                }
                else
                {
                    image = Properties.Resources.rytxwcj;
                }
                return image;
            }
            catch (Exception ee)
            {
                return image;
            }

        }

        public static byte[] getSfzhBye(string conn, string sfzh)
        {
            try
            {
                if (conn == null || conn == "")
                {
                    return ConvertImage((Image)Properties.Resources.rytxwcj);
                }
                if (sfzh.Length != 15 && sfzh.Length != 18)
                {
                    return ConvertImage((Image)Properties.Resources.rytxwcj);
                }
                string strSQL = "select top 1 picture from pubSfzhInfo where cardid='" + sfzh + "'";
                System.Data.SqlClient.SqlDataReader reader = HIS.Model.Dal.SqlHelper.ExecuteReader(conn, CommandType.Text,
                    strSQL);
                reader.Read();
                if (reader.HasRows)
                {
                    return (byte[])reader["picture"];
                }
                else
                {
                    return ConvertImage((Image)Properties.Resources.rytxwcj);
                }
            }
            catch (Exception ee)
            {
                return ConvertImage((Image)Properties.Resources.rytxwcj);
            }

        }


        public static System.Drawing.Image getSfzhImage(byte[] sfzhImage)
        {
            if (sfzhImage == null)
            {
                return Properties.Resources.rytxwcj;
            }
            else
            {
                MemoryStream ms = new MemoryStream(sfzhImage);
                return System.Drawing.Image.FromStream(ms, true);
            }
        }


        public static byte[] ConvertImage(Image image)
        {
            FileStream fs = new FileStream("imagetemp", FileMode.Create, FileAccess.Write, FileShare.None);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, (object)image);
            fs.Close();
            fs = new FileStream("imagetemp", FileMode.Open, FileAccess.Read, FileShare.None);
            byte[] bytes = new byte[fs.Length];
            fs.Read(bytes, 0, (int)fs.Length);
            fs.Close();
            return bytes;
        }

        /// <summary> 
        /// 验证身份证合理性 
        /// </summary> 
        /// <param name="Id"></param> 
        /// <returns></returns> 
        public static bool CheckIDCard(string idNumber)
        {
            if (idNumber.Length == 18)
            {
                bool check = CheckIDCard18(idNumber);
                return check;
            }
            else if (idNumber.Length == 15)
            {
                bool check = CheckIDCard15(idNumber);
                return check;
            }
            else
            {
                return false;
            }
        }


        /// <summary> 
        /// 18位身份证号码验证 
        /// </summary> 
        private static bool CheckIDCard18(string idNumber)
        {
            long n = 0;
            if (long.TryParse(idNumber.Remove(17), out n) == false
                || n < Math.Pow(10, 16) || long.TryParse(idNumber.Replace('x', '0').Replace('X', '0'), out n) == false)
            {
                return false;//数字验证 
            }
            string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
            if (address.IndexOf(idNumber.Remove(2)) == -1)
            {
                return false;//省份验证 
            }
            string birth = idNumber.Substring(6, 8).Insert(6, "-").Insert(4, "-");
            DateTime time = new DateTime();
            if (DateTime.TryParse(birth, out time) == false)
            {
                return false;//生日验证 
            }
            string[] arrVarifyCode = ("1,0,x,9,8,7,6,5,4,3,2").Split(',');
            string[] Wi = ("7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2").Split(',');
            char[] Ai = idNumber.Remove(17).ToCharArray();
            int sum = 0;
            for (int i = 0; i < 17; i++)
            {
                sum += int.Parse(Wi[i]) * int.Parse(Ai[i].ToString());
            }
            int y = -1;
            Math.DivRem(sum, 11, out y);
            if (arrVarifyCode[y] != idNumber.Substring(17, 1).ToLower())
            {
                return false;//校验码验证 
            }
            return true;//符合GB11643-1999标准 
        }


        /// <summary> 
        /// 16位身份证号码验证 
        /// </summary> 
        private static bool CheckIDCard15(string idNumber)
        {
            long n = 0;
            if (long.TryParse(idNumber, out n) == false || n < Math.Pow(10, 14))
            {
                return false;//数字验证 
            }
            string address = "11x22x35x44x53x12x23x36x45x54x13x31x37x46x61x14x32x41x50x62x15x33x42x51x63x21x34x43x52x64x65x71x81x82x91";
            if (address.IndexOf(idNumber.Remove(2)) == -1)
            {
                return false;//省份验证 
            }
            string birth = idNumber.Substring(6, 6).Insert(4, "-").Insert(2, "-");
            DateTime time = new DateTime();
            if (DateTime.TryParse(birth, out time) == false)
            {
                return false;//生日验证 
            }
            return true;
        }
    }

}

