﻿using WEISHENG.COMM;

namespace HIS.COMM
{
    public class msgBalloonHelper : WEISHENG.COMM.msgHelper
    {
        public static void BalloonShow(string _info, string _title)
        {
            HIS.COMM.SmartAlert.AlertInfo.context = new HIS.COMM.SmartAlert.alertContext()
            {
                Info = _info,
                Title = _title
            };
        }
        public static void BalloonShow(string _info)
        {
            LogHelper.Info(_info);
            BalloonShow(_info, "提示");
        }
    }
}
