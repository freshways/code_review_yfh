﻿/*
 源码己托管:http://git.oschina.net/kuiyu/dotnetcodes
 */
using System;
using System.Text.RegularExpressions;

namespace HIS.COMM.Helper
{
    /// <summary>
    /// 操作正则表达式的公共类
    /// </summary>    
    public class RegexHelper
    {
        /// <summary>
        /// 是否为数字
        /// </summary>
        public static string strIsNumeric = "^[\\+\\-]?[0-9]*\\.?[0-9]+$";
        /// <summary>
        /// 电话号码
        /// </summary>
        public static string strPhone = @"(^(\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$|(1(([35][0-9])|(47)|[8][01236789]))\d{8}$";
        /// <summary>
        /// 身份证号
        /// </summary>
        public static string strSFZH = @"^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$|^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$";
        public static bool IsSFZH(string subjectString)
        {
            bool foundMatch = false;
            try
            {
                foundMatch = Regex.IsMatch(subjectString, strSFZH);
            }
            catch (ArgumentException ex)
            {
                // Syntax error in the regular expression
            }
            return foundMatch;

        }
        #region 验证输入字符串是否与模式字符串匹配
        /// <summary>
        /// 验证输入字符串是否与模式字符串匹配，匹配返回true
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="pattern">模式字符串</param>        
        public static bool IsMatch(string input, string pattern)
        {
            return IsMatch(input, pattern, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// 验证输入字符串是否与模式字符串匹配，匹配返回true
        /// </summary>
        /// <param name="input">输入的字符串</param>
        /// <param name="pattern">模式字符串</param>
        /// <param name="options">筛选条件</param>
        public static bool IsMatch(string input, string pattern, RegexOptions options)
        {
            return Regex.IsMatch(input, pattern, options);
        }


        #endregion
    }
}

#region 正则资料
//只能输入数字："^[0-9]*$"。

//      只能输入n位的数字："^\d{n}$"。

//　　只能输入至少n位的数字："^\d{n,}$"。

//　　只能输入m ~n位的数字：。"^\d{m,n}$"

//　　只能输入零和非零开头的数字："^(0|[1-9][0-9]*)$"。

//　　只能输入有两位小数的正实数："^[0-9]+(.[0-9]{2})?$"。

//　　只能输入有1 ~3位小数的正实数："^[0-9]+(.[0-9]{1,3})?$"。

//　　只能输入非零的正整数："^\+?[1-9][0-9]*$"。

//　　只能输入非零的负整数："^\-[1-9][]0-9"*$。

//　　只能输入长度为3的字符："^.{3}$"。

//　　只能输入由26个英文字母组成的字符串："^[A-Za-z]+$"。

//　　只能输入由26个大写英文字母组成的字符串："^[A-Z]+$"。

//　　只能输入由26个小写英文字母组成的字符串："^[a-z]+$"。

//　　只能输入由数字和26个英文字母组成的字符串："^[A-Za-z0-9]+$"。

//　　只能输入由数字、26个英文字母或者下划线组成的字符串："^\w+$"。

//　　验证用户密码："^[a-zA-Z]\w{5,17}$"正确格式为：以字母开头，长度在6 ~18之间，只能包含字符、数字和下划线。

//　　验证是否含有^%&’,;=?$\"等字符："[^%&’,;=?$\x22]+"。

//　　只能输入汉字："^[\u4e00-\u9fa5]{0,}$"

//　　验证Email地址："^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"。

//　　验证InternetURL："^http://([\w-]+\.)+[\w-]+(/[\w-./?%&=]*)?$"。

//　　验证电话号码："^(\(\d{3,4}-)|\d{3.4}-)?\d{7,8}$"正确格式为："XXX-XXXXXXX"、"XXXX-XXXXXXXX"、"XXX-XXXXXXX"、"XXX-XXXXXXXX"、"XXXXXXX"和"XXXXXXXX"。


//验证身份证号(15位或18位数字)："^\d{15}|\d{18}$"。

//　　验证一年的12个月："^(0?[1-9]|1[0-2])$"正确格式为："01"～"09"和"1"～"12"。

//　　验证一个月的31天："^((0?[1-9])|((1|2)[0-9])|30|31)$"正确格式为;"01"～"09"和"1"～"31"。

//　　利用正则表达式限制网页表单里的文本框输入内容：

//　　用正则表达式限制只能输入中文：onkeyup="value=value.replace(/[^\u4E00-\u9FA5]/g,’’)" onbeforepaste="clipboardData.setData(’text’,clipboardData.getData(’text’).replace(/[^\u4E00-\u9FA5]/g,’’))"

//　　用正则表达式限制只能输入全角字符： onkeyup="value=value.replace(/[^\uFF00-\uFFFF]/g,’’)" onbeforepaste="clipboardData.setData(’text’,clipboardData.getData(’text’).replace(/[^\uFF00-\uFFFF]/g,’’))"

//　　用正则表达式限制只能输入数字：onkeyup="value=value.replace(/[^\d]/g,’’) "onbeforepaste="clipboardData.setData(’text’,clipboardData.getData(’text’).replace(/[^\d]/g,’’))"

//　　用正则表达式限制只能输入数字和英文：onkeyup="value=value.replace(/[\W]/g,’’) "onbeforepaste="clipboardData.setData(’text’,clipboardData.getData(’text’).replace(/[^\d]/g,’’))"
#endregion
