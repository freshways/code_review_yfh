﻿//
// ================================================= 
//   基于DevExpress通用表格打印
//    
//    设计: 陈炎和 2011.02
//==================================================
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.Data;
using System.Windows.Forms;

namespace HIS.COMM
{

    /// <summary>
    /// 报表每个单元打印前事件
    /// </summary>
    /// <param name="sender">发生事件的对象</param>
    /// <param name="source">来源:1:页头 2:表头 3:表格 4:表脚 5:页脚 6:条码</param>
    /// <param name="PageIndex">当前页号</param>
    /// <param name="RowIndex">当前行号</param>
    /// <param name="Caption">标题</param>
    /// <param name="Field">绑定字段</param>
    /// <param name="Value">打印前值</param>
    /// <param name="e"></param>
    public delegate void XReportUnitAfterPrint(object sender, int source, int PageIndex, int RowIndex, string Caption, string Field, ref string Value, EventArgs e);


    /// <summary>
    /// 通用报表
    /// </summary>
    public class TableXReport : DevExpress.XtraReports.UI.XtraReport
    {

        /// <summary>
        /// 报表主体设置
        /// </summary>
        private struct ReportMain
        {
            /// <summary>
            /// 打印机名
            /// </summary>
            public string printerName;
            /// <summary>
            /// 纸张大小
            /// </summary>
            public Size pageSize;
            /// <summary>
            /// 页边距
            /// </summary>
            public System.Drawing.Printing.Margins Margins;
            /// <summary>
            /// 获取或设置纸张类型
            /// </summary>
            public System.Drawing.Printing.PaperKind PaperKind;
            /// <summary>
            /// 获取或设置每行高度（以0.1毫米为单位）
            /// </summary>
            public int DetailHeight;
            /// <summary>
            /// 获取或设置每页行数
            /// </summary>
            public int DetailRows;
            /// <summary>
            /// 获取或设置打印开始页
            /// </summary>
            public int BeginPage;
            /// <summary>
            /// 获取或设置打印结束页
            /// </summary>
            public int EndPage;
            /// <summary>
            /// 获取或设置打印方向
            /// </summary>
            public bool Landscape;
            /// <summary>
            /// 获取或设置表格线的类型
            ///   1:三栏表,2:有边框的三栏,4:完全网格
            /// </summary>
            public int GridLine;
            /// <summary>
            /// 表格对齐方式
            /// </summary>
            public System.Drawing.ContentAlignment Align;
        }
        /// <summary>
        /// 报表标题
        /// </summary>
        private struct ReportTitle
        {
            /// <summary>
            /// 标题内空
            /// </summary>
            public string Caption;
            /// <summary>
            /// 页头或页脚
            /// </summary>
            public bool Header;
            /// <summary>
            /// 字体(宋体加粗四号)
            /// </summary>
            public Font Font;
            /// <summary>
            /// 对齐方式
            /// </summary>
            public DevExpress.XtraPrinting.TextAlignment TextAlignment;
            /// <summary>
            ///  标题的高度和宽度，若不定义便检测Caption,否则自换行
            /// </summary>
            public System.Drawing.Size size;
        }
        /// <summary>
        /// 报表各字段格式
        /// </summary>
        private struct ReportField
        {
            /// <summary>
            /// 名称
            /// </summary>
            public string Name;
            /// <summary>
            /// 表格头标题
            /// </summary>
            public string Caption;
            /// <summary>
            /// 标题字体(宋体、小四)
            /// </summary>
            public Font HeaderFont;
            /// <summary>
            /// 绑定字段
            /// </summary>
            public string Field;
            /// <summary>
            /// 对齐方式
            /// </summary>
            public DevExpress.XtraPrinting.TextAlignment TextAlignment;
            /// <summary>
            /// 表格字体(宋体、小四)
            /// </summary>
            public Font DetailFont;
            /// <summary>
            /// 自动换行
            /// </summary>
            public bool WordWrap;
            /// <summary>
            /// 表格脚内容
            /// </summary>
            public string Bottom;
            /// <summary>
            /// 是否合计
            /// </summary>
            public bool Sum;
            /// <summary>
            /// 格式化字符串
            /// </summary>
            public string FormatString;
            /// <summary>
            /// 标题的高度和宽度，若不定义便检测Caption,否则自换行并居中
            /// </summary>
            public Size HeaderSize;
        }

        private struct ReportBarcode
        {
            /// <summary>
            /// 条码内容(A-Z,0-9)
            /// </summary>
            public string Text;
            /// <summary>
            /// 起点
            /// </summary>
            public Point pint;
            /// <summary>
            /// 大小
            /// </summary>
            public Size size;
            /// <summary>
            /// 编码格式
            /// </summary>
            public DevExpress.XtraPrinting.BarCode.BarCodeGeneratorBase Symbology;
            /// <summary>
            /// 是否显示文字
            /// </summary>
            public bool showText;
        }


        private ReportMain m_rpm = new ReportMain();
        private ArrayList m_rpt = new ArrayList();
        private ArrayList m_rpf = new ArrayList();
        private ReportBarcode m_rpb = new ReportBarcode();
        private TopMarginBand topMarginBand1;
        private DetailBand detailBand1;
        private BottomMarginBand bottomMarginBand1;
        private int m_rowindex;
        /// <summary>
        /// 定义报表主体同时创建报表(必须先定义SetReportTitle和SetReportFiled)
        /// </summary>
        /// <param name="DetailHeight">明细每行高度</param>
        /// <param name="DetailRows">每页行数</param>
        /// <param name="GridLine">表格线的类型 1:三栏表,2:有边框的三栏,4:完全网格</param>
        /// <param name="Align">表格对齐方向</param>
        /// <param name="Landscape">横向还是纵向打印</param>
        /// <param name="Margins">页的边距尺寸</param>
        /// <param name="PaperKind">标准的纸张大小</param>
        /// <param name="pageSize">纸张大小</param>
        /// <param name="FromPage">起始页</param>
        /// <param name="ToPage">结束页</param>
        /// <param name="printerName">打印机名</param>        
        public void SetReportMain(int DetailHeight, int DetailRows, int GridLine, System.Drawing.ContentAlignment Align, bool Landscape, System.Drawing.Printing.Margins Margins,
            System.Drawing.Printing.PaperKind PaperKind, Size pageSize, int FromPage, int ToPage, string printerName)
        {
            m_rpm.DetailHeight = DetailHeight;
            m_rpm.DetailRows = DetailRows;
            m_rpm.Align = Align;
            m_rpm.Landscape = Landscape;
            m_rpm.Margins = Margins;
            m_rpm.PaperKind = PaperKind;
            m_rpm.pageSize = pageSize;
            m_rpm.BeginPage = FromPage;
            m_rpm.EndPage = ToPage;
            m_rpm.printerName = printerName;
            m_rpm.GridLine = GridLine;
            base.Bands.Clear();
            this.SetReport(m_rpm, (ReportTitle[])m_rpt.ToArray(typeof(ReportTitle)), (ReportField[])m_rpf.ToArray(typeof(ReportField)), m_rpb);
        }
        /// <summary>
        /// 定义报表标题
        /// </summary>
        /// <param name="Caption">标题内容</param>
        /// <param name="Header">页头或页脚</param>
        /// <param name="Font">字体(宋体加粗四号)</param>
        /// <param name="TextAlignment">对齐方式</param>
        /// <param name="size"> 标题的高度和宽度，若不定义便检测Caption,否则自换行</param>
        public void SetReportTitle(string Caption, bool Header, Font Font, DevExpress.XtraPrinting.TextAlignment TextAlignment, System.Drawing.Size size)
        {
            ReportTitle rpt = new ReportTitle();
            rpt.Caption = Caption;
            rpt.Header = Header;
            rpt.TextAlignment = TextAlignment == 0 ? DevExpress.XtraPrinting.TextAlignment.TopCenter : TextAlignment;
            rpt.Font = Font == null ? new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134))) : Font;
            rpt.size = size;
            m_rpt.Add(rpt);
        }
        /// <summary>
        /// 定义报表字段
        /// </summary>
        /// <param name="Caption">表格头标题</param>
        /// <param name="HeaderFont">标题字体(宋体、小四)</param>
        /// <param name="Field">绑定字段</param>
        /// <param name="TextAlignment">对齐方式</param>
        /// <param name="DetailFont"> 表格字体(宋体、小四)</param>
        /// <param name="WordWrap">自动换行</param>
        /// <param name="Bottom">表格脚内容</param>
        /// <param name="Sum">是否合计</param>
        /// <param name="FormatString">格式化字符串</param>
        /// <param name="HeaderSize">标题的高度和宽度，若不定义便检测Caption,否则自换行并居中</param>
        public void SetReportField(string Caption, Font HeaderFont, string Field, DevExpress.XtraPrinting.TextAlignment TextAlignment, Font DetailFont
                        , bool WordWrap, string Bottom, bool Sum, string FormatString, Size HeaderSize)
        {
            ReportField rpf = new ReportField();
            rpf.Caption = Caption;
            rpf.HeaderFont = HeaderFont;
            rpf.Field = Field;
            rpf.TextAlignment = TextAlignment;
            rpf.DetailFont = DetailFont;
            rpf.WordWrap = WordWrap;
            rpf.Bottom = Bottom;
            rpf.Sum = Sum;
            rpf.FormatString = FormatString;
            rpf.HeaderSize = HeaderSize;
            m_rpf.Add(rpf);
        }
        /// <summary>
        /// 定义条码
        /// </summary>
        /// <param name="Text">条码内容(A-Z,0-9)</param>
        /// <param name="pint">起点</param>
        /// <param name="size">大小</param>
        /// <param name="Symbology">编码格式</param>
        /// <param name="showText">是否显示文字</param>
        public void SetReportBarCode(string Text, Point pint, Size size, DevExpress.XtraPrinting.BarCode.BarCodeGeneratorBase Symbology, bool showText)
        {
            m_rpb.Text = Text;
            m_rpb.pint = pint;
            m_rpb.size = size;
            m_rpb.Symbology = Symbology == null ? new DevExpress.XtraPrinting.BarCode.Code128Generator() : Symbology;
            m_rpb.showText = showText;
        }


        private const int c_LineWidth = 1;//预留边线的宽度
        /// <summary>
        /// 报表每个单元打印前事件
        /// </summary>
        public event XReportUnitAfterPrint UnitAfterPrint;
        //报表头、报表尾整个报表中只会加载一次，页头、页尾每一页都会加载一次
        //本通用报表只有页头，页尾和表体三部份

        private ReportMain rpMain;
        /// <summary>
        /// 初始化通用表格打印
        /// </summary>
        public TableXReport()
        {
            this.ReportUnit = ReportUnit.TenthsOfAMillimeter; //使用0.1毫米计量单位
            this.Dpi = 254F;
            base.BeforePrint += new System.Drawing.Printing.PrintEventHandler(XReport_BeforePrint);
            base.AfterPrint += new EventHandler(XReport_AfterPrint);

        }
        // 设置标准格式报表
        private void SetReport(ReportMain rpm, ReportTitle[] rpt, ReportField[] rpf, ReportBarcode rpb)
        {
            rpMain = rpm;
            //初始化变量
            if (rpMain.Align == 0) rpMain.Align = ContentAlignment.TopCenter;
            for (int i = 0; i < rpt.Length; i++)
            {
                if (rpt[i].TextAlignment == 0) rpt[i].TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
                if (rpt[i].Font == null) rpt[i].Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            }
            for (int i = 0; i < rpf.Length; i++)
            {
                if (rpf[i].TextAlignment == 0) rpf[i].TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
                if (rpf[i].HeaderFont == null) rpf[i].HeaderFont = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                if (rpf[i].DetailFont == null) rpf[i].DetailFont = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                if (rpf[i].HeaderSize.IsEmpty)
                {
                    SizeF size = MeasureString(rpf[i].Caption, rpf[i].HeaderFont);
                    rpf[i].HeaderSize = new Size((int)size.Width + (i == 0 ? 3 : 2) * c_LineWidth, (int)size.Height + 30); //宽度增加2个线位，防止自动换行                  
                }
            }

            if (rpm.Margins != null) this.Margins = rpm.Margins;
            if (!rpm.pageSize.IsEmpty)
            {
                this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                this.PageSize = rpm.pageSize;
            }
            else
                this.PaperKind = rpm.PaperKind;
            this.Landscape = rpm.Landscape;
            if (rpm.printerName != string.Empty) this.PrinterName = rpm.printerName;


            DetailBand detail = new DetailBand();
            PageHeaderBand pageHeader = new PageHeaderBand();
            PageFooterBand pageFooter = new PageFooterBand();
            pageHeader.Dpi = 254F;  //用于0.1毫米的计量
            detail.Dpi = 254F;
            pageFooter.Dpi = 254F;
            XRPanel TitleHeader = CreateTitleHeader(rpt, true);
            XRPanel TitleFooter = CreateTitleHeader(rpt, false);
            XRTable TableHeader = CreateTableHeader(rpf, rpMain.GridLine == 4);
            XRTable TableFooter = CreateTableFooter(rpf, rpm.DetailHeight, rpMain.GridLine == 4);
            XRTable TableDetail = CreateTableDetail(rpf, rpm.DetailHeight, rpMain.GridLine == 4);
            XRBarCode BarCode = createBarCode(rpb);
            //校正表格的对齐方式
            switch (rpMain.Align)
            {
                case ContentAlignment.TopLeft:
                    TableHeader.Left = c_LineWidth;
                    break;
                case ContentAlignment.TopRight:
                    TableHeader.Left = (this.PageWidth - (this.Margins.Left + this.Margins.Right)) - TableHeader.Width - c_LineWidth;
                    break;
                default:  //中间
                    TableHeader.Left = (this.PageWidth - (this.Margins.Left + this.Margins.Right) - TableHeader.Width) / 2;
                    break;
            }
            TableDetail.Left = TableHeader.Left;
            TableFooter.Left = TableHeader.Left;
            //页头 
            TableHeader.Top = c_LineWidth + TitleHeader.Top + TitleHeader.Height;
            if (rpb.Text != null && rpb.Text != string.Empty)  //加入条码
            {
                pageHeader.Controls.Add(BarCode);
                if (TableHeader.Top + TableHeader.Height < BarCode.Height) TableHeader.Top = BarCode.Height - TableHeader.Height;
            }
            pageHeader.Controls.Add(TitleHeader);
            pageHeader.Controls.Add(TableHeader);
            pageHeader.Height = TableHeader.Top + TableHeader.Height;
            //表格头二条线
            if (rpm.GridLine == 1 || rpm.GridLine == 2)
            {
                pageHeader.Controls.Add(CreateLine(TableHeader.Left, TableHeader.Top, TableHeader.Width, TableHeader.Height, DevExpress.XtraPrinting.BorderSide.Top));
                pageHeader.Controls.Add(CreateLine(TableHeader.Left, TableHeader.Top, TableHeader.Width, TableHeader.Height, DevExpress.XtraPrinting.BorderSide.Bottom));
                //两侧线
                if (rpm.GridLine == 2)
                {
                    pageHeader.Controls.Add(CreateLine(TableHeader.Left, TableHeader.Top, TableHeader.Width, TableHeader.Height, DevExpress.XtraPrinting.BorderSide.Left));
                    pageHeader.Controls.Add(CreateLine(TableHeader.Left, TableHeader.Top, TableHeader.Width, TableHeader.Height, DevExpress.XtraPrinting.BorderSide.Right));
                }
            }
            //带区
            detail.Height = rpm.DetailHeight;
            detail.Controls.Add(TableDetail);
            //两侧线
            if (rpm.GridLine == 2)
            {
                detail.Controls.Add(CreateLine(TableDetail.Left, TableDetail.Top, TableDetail.Width, TableDetail.Height, DevExpress.XtraPrinting.BorderSide.Left));
                detail.Controls.Add(CreateLine(TableDetail.Left, TableDetail.Top, TableDetail.Width, TableDetail.Height, DevExpress.XtraPrinting.BorderSide.Right));
            }
            //页脚
            if (rpm.GridLine == 1 || rpm.GridLine == 2) //一条线
            {
                pageFooter.Controls.Add(CreateLine(TableDetail.Left, 0, TableDetail.Width, 0, DevExpress.XtraPrinting.BorderSide.Top));
                TableFooter.Top = c_LineWidth;
                TitleFooter.Top = c_LineWidth;
            }
            else
            {
                TableFooter.Top = 0;
                TitleFooter.Top = 0;
            }
            if (TableFooter.Rows[0].Cells.Count > 0)
            {
                TitleFooter.Top = TableFooter.Height + TableFooter.Top;
                pageFooter.Controls.Add(TableFooter);
            }
            pageFooter.Controls.Add(TitleFooter);
            if (rpm.DetailRows > 0)
            {
                //计算余下的空白
                if (pageHeader.Height + detail.Height * rpm.DetailRows + this.Margins.Top + this.Margins.Bottom > this.PageHeight)
                    throw new Exception("PageHeight不够大,请重新定义纸张大小，或减少DetailRows");
                pageFooter.Height = this.PageHeight - pageHeader.Height - detail.Height * rpm.DetailRows - this.Margins.Top - this.Margins.Bottom - 1; //必须减1         
            }
            else
                pageFooter.Height = TitleFooter.Top + TitleFooter.Height;
            Bands.AddRange(new DevExpress.XtraReports.UI.Band[] { detail, pageHeader, pageFooter });
            //this.Bands[BandKind.PageHeader].Controls.Add(tableHeader); 另一种加入方法

        }

        // 创建表线(从指定距形中创建边线,线在边框外)
        //由于线条有一个宽度,起点坐标要向左移一个宽度，水平线还要增长2个宽度,上水平线向上移一个宽度
        private XRLine CreateLine(int x, int y, int w, int h, DevExpress.XtraPrinting.BorderSide BorderSide)
        {
            XRLine xrline = new XRLine();
            xrline.Dpi = 254F;
            xrline.LineDirection = (BorderSide == DevExpress.XtraPrinting.BorderSide.Left || BorderSide == DevExpress.XtraPrinting.BorderSide.Right) ? LineDirection.Vertical : LineDirection.Horizontal;
            xrline.LineWidth = c_LineWidth; //线宽
            if (xrline.LineDirection == LineDirection.Vertical) //垂直
            {
                xrline.Width = c_LineWidth;
                //当LineWidth少于5时,Width或Height会自动校正为5，必须手工校正坐标
                xrline.Left = x - (xrline.Width - c_LineWidth) / 2 + (BorderSide == DevExpress.XtraPrinting.BorderSide.Right ? w : -c_LineWidth);
                xrline.Height = h;
                xrline.Top = y;
            }
            else //水平
            {
                xrline.Height = c_LineWidth;
                //当LineWidth少于5时,Width或Height会自动校正为5，必须手工校正坐标
                xrline.Top = y - (xrline.Height - c_LineWidth) / 2 + (BorderSide == DevExpress.XtraPrinting.BorderSide.Bottom ? h : -c_LineWidth);
                xrline.Width = w + 2 * c_LineWidth; //水平线加一个宽度
                xrline.Left = x - c_LineWidth;
            }
            return xrline;
        }

        #region 创建表格的各个功能单元
        // 创建表格头
        private XRTable CreateTableHeader(ReportField[] rpf, bool border)
        {
            System.Drawing.Size tablesize = new Size();
            XRTableRow headerRow = new XRTableRow();
            headerRow.Dpi = 254F;
            foreach (ReportField obj in rpf)
            {
                XRTableCell headerCell = new XRTableCell();
                headerCell.Dpi = 254F;
                headerCell.Font = obj.HeaderFont;
                headerCell.Width = obj.HeaderSize.Width;
                if (border)   //上下边和侧边
                    if (obj.Equals(rpf[0])) headerCell.Borders = (DevExpress.XtraPrinting.BorderSide)(DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom);
                    else headerCell.Borders = (DevExpress.XtraPrinting.BorderSide)(DevExpress.XtraPrinting.BorderSide.Bottom | DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right);
                headerCell.BorderWidth = c_LineWidth;
                if (tablesize.Height < obj.HeaderSize.Height) tablesize.Height = obj.HeaderSize.Height;
                headerCell.WordWrap = true;  //自动换行
                headerCell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                headerCell.CanGrow = false;
                headerCell.Text = obj.Caption;
                headerCell.Tag = obj.Field;
                //headerCell.BackColor = Color.Red;
                headerCell.PrintOnPage += new PrintOnPageEventHandler(headerCell_PrintOnPage);
                if (obj.Name != null && obj.Name != string.Empty) headerCell.Name = "header_" + obj.Name;
                headerRow.Cells.Add(headerCell);
                tablesize.Width = tablesize.Width + headerCell.Width;
            }
            headerRow.Size = tablesize;
            XRTable tableHeader = new XRTable();
            tableHeader.Dpi = 254F;
            tableHeader.Size = tablesize;
            tableHeader.Rows.Add(headerRow);
            return tableHeader;
        }
        // 创建表格
        private XRTable CreateTableDetail(ReportField[] rpf, int rowHeight, bool border)
        {
            System.Drawing.Size tablesize = new Size();
            tablesize.Height = rowHeight;
            XRTableRow detailRow = new XRTableRow();
            detailRow.Dpi = 254F;
            foreach (ReportField obj in rpf)
            {
                XRTableCell detailCell = new XRTableCell();
                detailCell.Dpi = 254F;
                detailCell.Font = obj.DetailFont;
                detailCell.Width = obj.HeaderSize.Width;
                if (obj.FormatString == null || obj.FormatString == string.Empty)
                    detailCell.DataBindings.Add("Text", null, obj.Field);
                else
                    detailCell.DataBindings.Add("Text", null, obj.Field, obj.FormatString);
                detailCell.Tag = obj.Caption;
                detailCell.TextAlignment = obj.TextAlignment;
                detailCell.WordWrap = obj.WordWrap;
                //detailCell.BackColor = Color.Blue;
                detailCell.CanGrow = false;
                if (border)   //侧边和下边
                    if (obj.Equals(rpf[0])) detailCell.Borders = (DevExpress.XtraPrinting.BorderSide)(DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom);
                    else detailCell.Borders = (DevExpress.XtraPrinting.BorderSide)(DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom);
                detailCell.BorderWidth = c_LineWidth;
                detailCell.PrintOnPage += new PrintOnPageEventHandler(detailCell_PrintOnPage);
                if (obj.Name != null && obj.Name != string.Empty) detailCell.Name = "detail_" + obj.Name;
                detailRow.Cells.Add(detailCell);
                tablesize.Width = tablesize.Width + detailCell.Width;// +LineCell.Width;
            }
            detailRow.Size = tablesize;
            XRTable tableDetail = new XRTable();
            tableDetail.Dpi = 254F;
            tableDetail.Size = tablesize;
            tableDetail.Rows.Add(detailRow);
            tableDetail.PrintOnPage += new PrintOnPageEventHandler(tableDetail_PrintOnPage);
            return tableDetail;
        }

        // 创建表格脚(若Rows[0].Cells.Count 表示无表格脚)
        private XRTable CreateTableFooter(ReportField[] rpf, int rowHeight, bool border)
        {
            System.Drawing.Size tablesize = new Size();
            tablesize.Height = rowHeight;
            XRTableRow FooterRow = new XRTableRow();
            FooterRow.Dpi = 254F;
            bool isfooter = false;
            foreach (ReportField obj in rpf)
            {
                if (obj.Sum || (obj.Bottom != null && obj.Bottom != string.Empty))
                {
                    isfooter = true;
                    break;
                }
            }
            if (isfooter)
                foreach (ReportField obj in rpf)
                {
                    XRTableCell FooterCell = new XRTableCell();
                    FooterCell.Dpi = 254F;
                    FooterCell.Font = obj.HeaderFont;
                    FooterCell.Width = obj.HeaderSize.Width;
                    FooterCell.Tag = obj.Caption;
                    FooterCell.CanGrow = false;
                    if (obj.Sum)  //合计
                    {
                        if (obj.FormatString == null || obj.FormatString == string.Empty)
                            FooterCell.DataBindings.Add("Text", null, obj.Field);
                        else
                            FooterCell.DataBindings.Add("Text", null, obj.Field, obj.FormatString);
                        DevExpress.XtraReports.UI.XRSummary xrSummary = new DevExpress.XtraReports.UI.XRSummary();
                        xrSummary.Running = DevExpress.XtraReports.UI.SummaryRunning.Page;
                        FooterCell.Summary = xrSummary;
                    }
                    else
                        FooterCell.Text = obj.Bottom;
                    if (border)   //侧边和下边
                        if (obj.Equals(rpf[0])) FooterCell.Borders = (DevExpress.XtraPrinting.BorderSide)(DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom);
                        else FooterCell.Borders = (DevExpress.XtraPrinting.BorderSide)(DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom);
                    FooterCell.BorderWidth = c_LineWidth;
                    FooterCell.TextAlignment = obj.TextAlignment;
                    FooterCell.WordWrap = obj.WordWrap;
                    //FooterCell.BackColor = Color.Yellow;
                    FooterCell.PrintOnPage += new PrintOnPageEventHandler(FooterCell_PrintOnPage);
                    if (obj.Name != null && obj.Name != string.Empty) FooterCell.Name = "footer_" + obj.Name;
                    FooterRow.Cells.Add(FooterCell);
                    tablesize.Width = tablesize.Width + FooterCell.Width;// +LineCell.Width;

                }
            XRTable tableFooter = new XRTable();
            tableFooter.Dpi = 254F;
            FooterRow.Size = tablesize;
            tableFooter.Size = tablesize;
            tableFooter.Rows.Add(FooterRow);
            return tableFooter;
        }
        // 创建页头或页脚标题 (Rows.Count =0表示无标题) 
        private XRPanel CreateTitleHeader(ReportTitle[] rpt, bool Header)
        {
            XRPanel xrpHeader = new XRPanel();
            xrpHeader.Dpi = 254F;
            System.Drawing.Size size = new Size();
            foreach (ReportTitle obj in rpt)
            {
                if (obj.Header == Header)
                {
                    XRLabel xrt = new XRLabel();
                    xrt.Dpi = 254F;
                    xrt.Text = obj.Caption;
                    xrt.Tag = Header;
                    xrt.TextAlignment = obj.TextAlignment;
                    xrt.WordWrap = true;
                    xrt.CanGrow = false;
                    xrt.Font = obj.Font;
                    if (obj.size.IsEmpty)
                    {
                        xrt.Width = (this.PageWidth - (this.Margins.Left + this.Margins.Right));
                        xrt.Height = (int)MeasureString(obj.Caption, obj.Font, xrt.Width).Height + 1;
                    }
                    else
                    {
                        xrt.Width = obj.size.Width;
                        xrt.Height = obj.size.Height;
                    }
                    xrt.PrintOnPage += new PrintOnPageEventHandler(title_PrintOnPage);
                    xrt.Top = size.Height;
                    size.Width = xrt.Width;
                    size.Height = size.Height + xrt.Height; ;
                    xrpHeader.Controls.Add(xrt);
                }
            }
            xrpHeader.Size = size;
            return xrpHeader;
        }

        // 创建条码
        private XRBarCode createBarCode(ReportBarcode rpb)
        {
            XRBarCode xrbar = new XRBarCode();
            xrbar.AutoModule = true;
            xrbar.Dpi = 254F;
            xrbar.Location = rpb.pint.IsEmpty ? new Point(0, 0) : rpb.pint;
            xrbar.Module = 5.08F;
            xrbar.Size = rpb.size.IsEmpty ? new Size(this.PageWidth - this.Margins.Left - this.Margins.Right, 150) : rpb.size;
            xrbar.Text = rpb.Text;
            xrbar.ShowText = rpb.showText;
            xrbar.Symbology = rpb.Symbology;
            xrbar.PrintOnPage += new PrintOnPageEventHandler(xrbar_PrintOnPage);
            return xrbar;
        }


        #endregion

        #region   //处理表格各个事件

        //每页单元格事件
        void tableDetail_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            m_rowindex++;
        }

        void headerCell_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            if (UnitAfterPrint == null) return;
            XRTableCell cell = (XRTableCell)sender;
            string value = cell.Text;
            UnitAfterPrint(sender, 2, e.PageIndex, m_rowindex, cell.Text, cell.Tag.ToString(), ref value, new EventArgs());
            cell.Text = value;
        }

        //处理表格单元事件
        void detailCell_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            if (UnitAfterPrint == null) return;
            XRTableCell cell = (XRTableCell)sender;
            string field = "";
            string value = cell.Text;
            if (cell.DataBindings.Count > 0) field = cell.DataBindings[0].DataMember;
            UnitAfterPrint(sender, 3, e.PageIndex, m_rowindex, cell.Tag.ToString(), field, ref value, new EventArgs());
            cell.Text = value;
        }

        //处理标题单元事件
        void title_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            if (UnitAfterPrint == null) return;
            XRLabel xrl = (XRLabel)sender;
            string value = xrl.Text;
            UnitAfterPrint(sender, Convert.ToBoolean(xrl.Tag) ? 1 : 5, e.PageIndex, m_rowindex, xrl.Text, "", ref value, new EventArgs());
            xrl.Text = value;
        }
        //表格脚事件
        void FooterCell_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            if (UnitAfterPrint == null) return;
            XRTableCell cell = (XRTableCell)sender;
            string field = "";
            string value = cell.Text;
            if (cell.DataBindings.Count > 0) field = cell.DataBindings[0].PropertyName;
            UnitAfterPrint(sender, 4, e.PageIndex, m_rowindex, cell.Tag.ToString(), field, ref value, new EventArgs());
            cell.Text = value;
        }
        //条码事件
        void xrbar_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            if (UnitAfterPrint == null) return;
            XRBarCode barcode = (XRBarCode)sender;
            string value = barcode.Text;
            UnitAfterPrint(sender, 6, e.PageIndex, m_rowindex, barcode.Text, "", ref value, new EventArgs());
            barcode.Text = value;
        }
        #endregion

        void XReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            m_rowindex = 0;
        }
        void XReport_AfterPrint(object sender, EventArgs e)
        {
            for (int i = this.PrintingSystem.Pages.Count; i > 0; i--)
            {
                if ((i < m_rpm.BeginPage && m_rpm.BeginPage > 0) || (i > m_rpm.EndPage && m_rpm.EndPage > 0))
                    this.PrintingSystem.Pages.Remove(this.PrintingSystem.Pages[1]); //移去指定的页码;
            }
        }
        //测量字符的打印范围(单行)
        private SizeF MeasureString(string text, Font font)
        {
            return MeasureString(text, font, 10000);
        }
        //测量字符的打印范围(多行)
        private SizeF MeasureString(string text, Font font, int width)
        {
            //Graphics fg = CreateGraphics();
            //fg.PageUnit = GraphicsUnit.Pixel;
            //fg.MeasureString();
            SizeF size = DevExpress.XtraPrinting.BrickGraphics.MeasureString(text, font, width, null, GraphicsUnit.Millimeter);
            size.Width = size.Width * 10;
            size.Height = size.Height * 10;
            return size;
        }

        private void InitializeComponent()
        {
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.detailBand1 = new DevExpress.XtraReports.UI.DetailBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // detailBand1
            // 
            this.detailBand1.Name = "detailBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // TableXReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand1,
            this.detailBand1,
            this.bottomMarginBand1});
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }


    }
}
