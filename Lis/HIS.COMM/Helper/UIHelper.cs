﻿using DevExpress.XtraEditors;
using System.Drawing;

namespace HIS.COMM.Helper
{
    public class UIHelper
    {
        /// <summary>
        /// 设置指定控件的颜色和只读状态
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="bReadOnly"></param>
        public static void setSpecial(object obj, bool bReadOnly)
        {
            var item = (TextEdit)obj;
            if (item == null)
            {
                return;
            }
            if (bReadOnly)
            {
                item.ForeColor = System.Drawing.Color.Blue;
                item.Font = new Font(new System.Drawing.FontFamily("Tahoma"), 9, FontStyle.Bold);
                item.BackColor = System.Drawing.Color.FromArgb(245, 245, 247);
                item.Properties.ReadOnly = true;
            }
            else
            {
                item.ForeColor = System.Drawing.Color.Black;
                item.Font = new Font(new System.Drawing.FontFamily("Tahoma"), 9, FontStyle.Regular);
                item.BackColor = System.Drawing.Color.Transparent;
                item.Properties.ReadOnly = false;
            }
        }
    }
}
