﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HIS.COMM
{
    [Obsolete("应该用新的 'WEISHENG.COMM.logHelper' 替换", true)]
    public static class Log
    {
        static ClassEnum.logLevel logLevelSet = ClassPubArgument.getLogLevel();
        public static bool logWrite(string moduleName, string eventType, string eventInfo, System.Data.SqlClient.SqlTransaction _connT)
        {
            if (logLevelSet == ClassEnum.logLevel.OFF)
            {
                return false;
            }
            bool value;
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(_connT, CommandType.Text,
                "INSERT INTO pub_Log " +
               "([appName]" +
                ",[appVer]" +
                ",[moduleName]" +
                ",[eventType]" +
                ",[evenInfo]" +
                ",[localTime]" +
                ",[dwName]" +
                ",[userName]" +
                ",[IP]" +
                ",[Mac]" +
                ",[computerName]" +
                ",[otherinfo],dwid)" +
                "VALUES" +
                "('" + WEISHENG.COMM.zdInfo.enAppName.ToString() + "','" + WEISHENG.COMM.zdInfo.sAppVersion + "','" +
               moduleName + "','" + eventType + "','" +
               eventInfo + "','" + DateTime.Now.ToString() + "','" +
               HIS.COMM.zdInfo.Model单位信息.sDwmc + "','" +
               HIS.COMM.zdInfo.ModelUserInfo.用户名 + "','" +
               WEISHENG.COMM.zdInfo.ModelHardInfo.IpAddress + "','" +
               WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "','" +
               WEISHENG.COMM.zdInfo.ModelHardInfo.ComputerName + "'," +
               "''," + HIS.COMM.zdInfo.Model科室信息.单位编码 + ")");
                value = true;
            }
            catch (Exception ex)
            {
                try
                {
                    WEISHENG.COMM.LogHelper.Info("log\\err", "【" + eventType + "|" + eventInfo + "】〖" + ex.Message + "〗");
                    //WEISHENG.COMM.zdInfo.sError = ex.Message;
                }
                catch (Exception ex2)
                {

                }
                value = false;
            }
            return value;
        }

        public static bool logInfo(string moduleName, string eventInfo)
        {
            return logWrite(moduleName, ClassEnum.logLevel.INFO, eventInfo);
        }
        public static bool logWrite(string moduleName, ClassEnum.logLevel eventType, string eventInfo)
        {
            if (eventType >= logLevelSet)
            {
                return logWrite(moduleName, eventType.ToString(), eventInfo);
            }
            return false;
        }
        public static bool logWrite(string moduleName, string eventType, string eventInfo)
        {
            if (logLevelSet == ClassEnum.logLevel.OFF)
            {
                return false;
            }
            string _moduleName = sqlSecurity.ReplaceSQLChar(moduleName);
            string _eventType = sqlSecurity.ReplaceSQLChar(eventType);
            string _eventInfo = sqlSecurity.ReplaceSQLChar(eventInfo);
            string sql = "INSERT INTO pub_Log " +
               "([appName]" +
                ",[appVer]" +
                ",[moduleName]" +
                ",[eventType]" +
                ",[evenInfo]" +
                ",[localTime]" +
                ",[dwName]" +
                ",[userName]" +
                ",[IP]" +
                ",[Mac]" +
                ",[computerName]" +
                ",[otherinfo],dwid)" +
                "VALUES" +
                "('" + WEISHENG.COMM.zdInfo.enAppName.ToString() + "','" + WEISHENG.COMM.zdInfo.sAppVersion + "','" +
               _moduleName + "','" + _eventType + "','" +
               _eventInfo + "','" + DateTime.Now.ToString() + "','" +
               HIS.COMM.zdInfo.Model单位信息.sDwmc + "','" +
               HIS.COMM.zdInfo.ModelUserInfo.用户名 + "','" +
               WEISHENG.COMM.zdInfo.ModelHardInfo.IpAddress + "','" +
               WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "','" +
               WEISHENG.COMM.zdInfo.ModelHardInfo.ComputerName + "'," +
               "''," + HIS.COMM.zdInfo.Model科室信息.单位编码 + ")";
            bool value;
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, sql);
                value = true;
            }
            catch (Exception ex)
            {
                try
                {
                    WEISHENG.COMM.LogHelper.Info("log\\err", "【" + eventType + "|" + eventInfo + "】〖" + ex.Message + "〗");
                    //WEISHENG.COMM.zdInfo.sError = ex.Message;
                }
                catch (Exception ex2)
                {

                }
                value = false;
            }
            return true;
        }
    }
}
