﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors;
using System.Drawing.Printing;
using DevExpress.Data;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using HIS.Model;
using System.Linq;

namespace HIS.COMM
{
    public class xtraReportShow
    {
       static  CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public static bool printToPaper(XtraReport report, string paperName, Boolean canPrint, string conHisString, string dwbm, string PrintName = "")//2017年10月31日 yufh添加参数，打印机名称，主要处理一台电脑多个业务的情况，默认给空这样不影响其他调用
        {
            try
            {
                var paperSetup = chis.GY打印设置.Where(c => c.MAC地址 == WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress && c.纸张名称 == paperName).FirstOrDefault();
                paperInfo.getPaper(conHisString, paperName, dwbm);

                report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
                report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义

                report.PageWidth = Convert.ToInt16(paperSetup.纸张宽度);
                report.PageHeight = Convert.ToInt16(paperSetup.纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);

            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            //如果没有指定打印机名称，直接默认打印；指定打印机时按照指定打印机去打印
            if (string.IsNullOrEmpty(PrintName))
                report.Print();
            else
            {//指定打印机时有可能会出现找不到打印机的情况，如果出现异常，抓取并给提示
                try
                {
                    report.Print(PrintName);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("指定打印机失败，将用默认打印机！\n" + ex.Message, "提示");
                    report.Print();
                }
            }
            return true;
        }

        public static bool printToPaper(System.Data.DataSet ds, XtraReport report, string paperName)
        {
            report.DataSource = null;

            report.DataSource = ds.Tables[0];

            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(HIS.COMM.DBConnHelper.SConnHISDb, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);

            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("获取纸张设置失败，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.PrintDirect, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.ExportFile, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.SendFile, CommandVisibility.None);
            report.Print();

            return true;
        }
        public static bool printToPaper(System.Data.DataSet ds, XtraReport report, string paperName, string conHisString)
        {
            report.DataSource = null;

            report.DataSource = ds.Tables[0];

            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);

            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("获取纸张设置失败，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.PrintDirect, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.ExportFile, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.SendFile, CommandVisibility.None);
            report.Print();

            return true;
        }
        public static bool printToPaper(System.Data.DataTable dt, XtraReport report, string paperName, string conHisString)
        {
            report.DataSource = null;

            report.DataSource = dt;

            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);

            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("获取纸张设置失败，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.PrintDirect, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.ExportFile, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.SendFile, CommandVisibility.None);
            report.Print();

            return true;
        }

        public static bool show(System.Data.DataSet ds, XtraReport report, string paperName, Boolean canPrint, string conHisString)
        {
            report.DataSource = null;

            report.DataSource = ds.Tables[0];

            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, Convert.ToString(HIS.COMM.zdInfo.Model科室信息.单位编码));
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("获取纸张设置失败，使用默认纸张设置。", "提示");
            }

            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            return true;
        }


        public static bool show(DataTable dt, XtraReport report, Boolean canPrint)
        {
            report.DataSource = dt;
            //try
            //{
            //    //边距
            //    for (int i = 0; i < report.Bands.Count; i++)
            //    {
            //        if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
            //            report.Bands[i].HeightF = Convert.ToInt16(paperInfo.Ybj_top);

            //        if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
            //            report.Bands[i].HeightF = Convert.ToInt16(paperInfo.Ybj_bottom);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //如果有错误，则取默认打印机的默认纸张
            //    PrintDocument pd = new PrintDocument();
            //    report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
            //    report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
            //    XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。" + ex.Message, "提示");
            //}
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }

        public static bool show(DataTable dt, XtraReport report, Boolean canPrint, System.Drawing.Printing.PaperKind _paperKind)
        {
            report.DataSource = dt;
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = _paperKind; //纸张类型自定义
            try
            {
                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }
            }
            catch (Exception ex)
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。" + ex.Message, "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }
        public static bool show(DataTable dt, XtraReport report, string paperName, Boolean canPrint, string conHisString)
        {
            report.DataSource = null;
            report.DataMember = null;
            report.DataAdapter = null;

            report.DataSource = dt;
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch (Exception ex)
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。" + ex.Message, "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }

        public static bool show(DataView dv, XtraReport report, string paperName, Boolean canPrint, string conHisString)
        {
            dv.RowFilter = "卡片类型='三代卡'";
            report.DataSource = dv;
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch (Exception ex)
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。" + ex.Message, "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }


        public static bool show(XtraReport report, string paperName, Boolean canPrint, string conHisString, bool b英寸转换)
        {
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(Convert.ToDecimal(paperInfo.i纸张宽度) / 2.54M);
                report.PageHeight = Convert.ToInt16(Convert.ToDecimal(paperInfo.i纸张高度) / 2.54M);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(Convert.ToDecimal(paperInfo.i上边距) / 2.54M);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(Convert.ToDecimal(paperInfo.i下边距) / 2.54M);
                }
                report.Margins.Left = Convert.ToInt16(Convert.ToDecimal(paperInfo.i左边距) / 2.54M);
                report.Margins.Right = Convert.ToInt16(Convert.ToDecimal(paperInfo.i右边距) / 2.54M);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码

            return true;
        }

        /// <summary>
        /// 报表打印预览，不传入数据集
        /// </summary>
        /// <param name="report"></param>
        /// <param name="paperName"></param>
        /// <param name="canPrint"></param>
        /// <param name="conHisString"></param>
        /// <returns></returns>
        /// 
        public static bool show(XtraReport report, string paperName, Boolean canPrint, string _connString)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm 2.54转换
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(HIS.COMM.DBConnHelper.SConnHISDb, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码
            return true;
        }
        public static bool show(XtraReport report, string paperName, Boolean canPrint)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm 2.54转换
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(HIS.COMM.DBConnHelper.SConnHISDb, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }



            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码

            return true;
        }
        //public static bool show(DataTable dt, XtraReport report, int _iWidth, int _iHeight, int _iTop, int _iBottom, int _iLeft, int _iRight, Boolean canPrint)
        //{
        //    report.DataSource = null;
        //    report.DataMember = null;
        //    report.DataAdapter = null;

        //    report.DataSource = dt;

        //    report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
        //    report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
        //    try
        //    {
        //        report.PageWidth = _iWidth;
        //        report.PageHeight = _iHeight;
        //        //边距
        //        for (int i = 0; i < report.Bands.Count; i++)
        //        {
        //            if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
        //                report.Bands[i].HeightF = _iTop;

        //            if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
        //                report.Bands[i].HeightF = _iBottom;
        //        }
        //        report.Margins.Left = _iLeft;
        //        report.Margins.Right = _iRight;
        //        report.Margins.Top = _iTop;
        //        report.Margins.Bottom = _iBottom;
        //    }
        //    catch
        //    {
        //        //如果有错误，则取默认打印机的默认纸张
        //        PrintDocument pd = new PrintDocument();
        //        report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
        //        report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
        //        XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
        //    }



        //    report.CreateDocument();//没有这句，下面的没用
        //    report.PrintingSystem.ShowMarginsWarning = false;
        //    report.PrintingSystem.ShowPrintStatusDialog = false;
        //    if (canPrint)
        //    {
        //        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);
        //    }
        //    else
        //    {
        //        report.PrintingSystem.SetCommandVisibility
        //            (
        //            new PrintingSystemCommand[]
        //            {
        //                PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
        //            }, CommandVisibility.None
        //            );
        //        report.PrintingSystem.SetCommandVisibility
        //            (
        //            new PrintingSystemCommand[]
        //            {
        //                PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
        //            }, CommandVisibility.None
        //            );
        //        report.PrintingSystem.SetCommandVisibility
        //              (
        //              new PrintingSystemCommand[]
        //            {
        //                PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
        //            }, CommandVisibility.None
        //              );
        //        report.PrintingSystem.SetCommandVisibility
        //           (
        //           new PrintingSystemCommand[]
        //            {
        //                PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
        //            }, CommandVisibility.None
        //           );
        //    }
        //    report.ShowPreviewDialog();//软件暂停，等用户响应
        //    // report.ShowPreview();//直接显示报表后继续运行下面的代码
        //    return true;
        //}
        public static bool printToPaper(XtraReport report, int _iWidth, int _iHeight, int _iTop, int _iBottom, int _iLeft, int _iRight)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                report.PageWidth = _iWidth;
                report.PageHeight = _iHeight;
                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = _iTop;

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = _iBottom;
                }
                report.Margins.Left = _iLeft;
                report.Margins.Right = _iRight;
                report.Margins.Top = _iTop;
                report.Margins.Bottom = _iBottom;
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }

            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            report.Print();
            return true;
        }
        public static bool show(XtraReport report, int _iWidth, int _iHeight, int _iTop, int _iBottom, int _iLeft, int _iRight, Boolean canPrint)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                report.PageWidth = _iWidth;
                report.PageHeight = _iHeight;
                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = _iTop;

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = _iBottom;
                }
                report.Margins.Left = _iLeft;
                report.Margins.Right = _iRight;
                report.Margins.Top = _iTop;
                report.Margins.Bottom = _iBottom;
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }



            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);
            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码
            return true;
        }

        /// <summary>
        /// 直接打印输出
        /// </summary>
        /// <param name="report"></param>
        /// <param name="paperName"></param>
        /// <param name="conHisString"></param>
        /// <returns></returns>
        public static bool printToPaper(XtraReport report, string paperName, string conHisString)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }

            //PrintingSystem ps = report.PrintingSystem;
            //PrintingSystemCommand.DocumentMap
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.PrintDirect, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.ExportFile, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.SendFile, CommandVisibility.None);
            //2016-05-20 15:05:24 yufh 添加：判断是否有设置打印机名称，如果有按照指定打印机名称打印
            if (paperInfo.Dyjmc == "Printer" || string.IsNullOrEmpty(paperInfo.Dyjmc))
                report.Print();
            else
                report.Print(paperInfo.Dyjmc);


            //if (canPrint)
            //{
            //    report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            //}
            //else
            //{
            //    report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            //}
            //report.ShowPreviewDialog();
            //    report.ShowPreview();

            return true;
        }
        public static bool printToPaper(XtraReport report, string paperName)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(HIS.COMM.DBConnHelper.SConnHISDb, paperName, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }

            //PrintingSystem ps = report.PrintingSystem;
            //PrintingSystemCommand.DocumentMap
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.PrintDirect, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.ExportFile, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.SendFile, CommandVisibility.None);
            //2016-05-20 15:05:24 yufh 添加：判断是否有设置打印机名称，如果有按照指定打印机名称打印
            if (paperInfo.Dyjmc == "Printer" || string.IsNullOrEmpty(paperInfo.Dyjmc))
                report.Print();
            else
                report.Print(paperInfo.Dyjmc);


            //if (canPrint)
            //{
            //    report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            //}
            //else
            //{
            //    report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            //}
            //report.ShowPreviewDialog();
            //    report.ShowPreview();

            return true;
        }


        /// <summary>
        /// 报表打印预览，不传入数据集
        /// </summary>
        /// <param name="report"></param>
        /// <param name="paperName"></param>
        /// <param name="canPrint"></param>
        /// <param name="conHisString"></param>
        /// <returns></returns>
        public static bool show(XtraReport report, string paperName, Boolean canPrint, string conHisString, string dwbm)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, dwbm);
                report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }



            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (canPrint)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码

            return true;
        }

        public static XtraReport setReport(XtraReport _report, string paperName, Boolean canPrint, string conHisString, string dwbm)
        {
            //_report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            //_report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paperInfo.getPaper(conHisString, paperName, dwbm);
                _report.PageWidth = Convert.ToInt16(paperInfo.i纸张宽度);
                _report.PageHeight = Convert.ToInt16(paperInfo.i纸张高度);

                //边距
                for (int i = 0; i < _report.Bands.Count; i++)
                {
                    if (_report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        _report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i上边距);

                    if (_report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        _report.Bands[i].HeightF = Convert.ToInt16(paperInfo.i下边距);
                }

                _report.Margins.Left = Convert.ToInt16(paperInfo.i左边距);
                _report.Margins.Right = Convert.ToInt16(paperInfo.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                _report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                _report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }



            _report.CreateDocument();//没有这句，下面的没用
            _report.PrintingSystem.ShowMarginsWarning = false;
            _report.PrintingSystem.ShowPrintStatusDialog = false;
            //if (canPrint)
            //{
            //    _report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            //}
            //else
            //{
            //    _report.PrintingSystem.SetCommandVisibility
            //        (
            //        new PrintingSystemCommand[]
            //        {
            //            PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
            //        }, CommandVisibility.None
            //        );
            //    _report.PrintingSystem.SetCommandVisibility
            //        (
            //        new PrintingSystemCommand[]
            //        {
            //            PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
            //        }, CommandVisibility.None
            //        );
            //    _report.PrintingSystem.SetCommandVisibility
            //          (
            //          new PrintingSystemCommand[]
            //        {
            //            PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
            //        }, CommandVisibility.None
            //          );
            //    _report.PrintingSystem.SetCommandVisibility
            //       (
            //       new PrintingSystemCommand[]
            //        {
            //            PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
            //        }, CommandVisibility.None
            //       );
            //}
            return _report;
            //report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码

            //return true;
        }

    }


}


