﻿namespace HIS.COMM.Helper.WebSocketHelper
{
    /// <summary>
    /// 发送到服务端的请求格式
    /// </summary>
    public class WebsocketContext
    {
        public string toUser { get; set; }
        public string fromUser { get; set; }
        public enWebsocketCommType type { get; set; }
        //99自定义状态下的请求信息
        public string requestData { get; set; }
        //99自定义状态下的返回信息
        public string responseData { get; set; }
        public string appId { get; set; }
        public string timeStamp { get; set; }
        public string appName { get; set; }
        //websocket服务器返回的状态消息
        public string message { get; set; }
    }


}
