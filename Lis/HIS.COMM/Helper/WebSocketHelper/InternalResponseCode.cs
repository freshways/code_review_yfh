﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.Helper.WebSocketHelper
{
    /// <summary>
    /// websocket内部数据通道99，成功标记
    /// </summary>
    public enum InternalResponseCode
    {
        ok = 0,
        error = -1
    }
}
