﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using WebSocketSharp;
using WEISHENG.COMM;
using ErrorEventArgs = WebSocketSharp.ErrorEventArgs;
using HIS.Model;
using System.Linq;

namespace HIS.COMM.Helper.WebSocketHelper
{
    public class WebSocketHelperShared
    {
        public static WebSocket ws_client;
        //新起线程，发送心跳包专用
        public static Thread tr_Heart = new Thread(new ThreadStart(SendHeart));
        public static bool isBusy = false;
        static enWebsocketClinetType enWebsocketClinetType = enWebsocketClinetType.医保后台服务;
        public static void CreateInstance(EventHandler<MessageEventArgs> onMessage, enWebsocketClinetType enWebsocketClinetType = enWebsocketClinetType.医保后台服务)
        {
            if (ws_client == null || ws_client.ReadyState == WebSocketState.Closed)
            {
                string wsUrl = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("Websocket服务地址", "ws://192.168.10.27:8089/socketServer/", "");
                if (wsUrl == "")
                {
                    return;
                }
                ws_client = new WebSocket($"{wsUrl}{Comm.getWebsocketClientName(enWebsocketClinetType)}"); // 服务器接口
                ws_client.OnMessage += onMessage; // 信息事件
                ws_client.OnClose += ws_OnClose; // 关闭事件
                ws_client.OnError += ws_OnError;
                ws_client.Connect(); // 链接  
                tr_Heart.IsBackground = true;
                tr_Heart.Start();
            }
        }



        private static void ws_OnError(object sender, ErrorEventArgs e)
        {
            LogHelper.Info(e.Message);
            MessageBox.Show(e.Message);
        }

        public static void SendMsg(string msg)
        {
            if (ws_client != null && ws_client.ReadyState == WebSocketState.Open)
            {
                try
                {
                    LogHelper.Info(msg);
                    ws_client.Send(msg);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }



        /// <summary>
        /// 服务器接收消息--此处处理接受消息、解析内容等业务逻辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private static void ws_OnMessage(object sender, MessageEventArgs e)
        //{
        //    Console.Write("接收消息：" + e.Data + "\r\n");
        //}

        /// <summary>
        /// 与服务器连接中断--处理连接意外中断
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ws_OnClose(object sender, CloseEventArgs e)
        {
            Comm.updateOnlineStatus(enClientBusyStatus.free, enClientOnlineStatus.offline, enWebsocketClinetType);
        }


        /// <summary>
        /// 发送心跳包
        /// </summary>
        public static void SendHeart()
        {
            //可定义无限循环
            while (true)
            {
                Comm.updateOnlineStatus(enClientBusyStatus.free, enClientOnlineStatus.online, enWebsocketClinetType.医保后台服务);
                SendMsg(@"{""type"":21,""message"":"" 心跳包 ""}");
                tr_Heart.Join(1 * 60 * 2 * 1000 - 3000);
            }
        }


    }


    /// <summary>
    /// 多线程安全Log记录工具20180314
    /// </summary>
    public class LogHelper2
    {
        //为了使用DBGView进行在线调试：
        //System.Diagnostics.Debug.WriteLine("Debug模式可见")
        //System.Diagnostics.Trace.WriteLine("Debug、Release都可见");

        private static Thread WriteThread;
        private static readonly Queue<string> MsgQueue;

        private static readonly string FilePath;

        private static Boolean autoResetEventFlag = false;
        private static AutoResetEvent aEvent = new AutoResetEvent(false);
        private static bool flag = true;
        public static bool LogFlag = true;

        static LogHelper2()
        {
            FilePath = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "App_Log\\";
            WriteThread = new Thread(WriteMsg);
            MsgQueue = new Queue<string>();
            WriteThread.Start();
        }

        public static void LogInfo(string msg)
        {
            Monitor.Enter(MsgQueue);
            MsgQueue.Enqueue(string.Format("{0} {1} {2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "Info", msg));
            Monitor.Exit(MsgQueue);
            if (autoResetEventFlag)
            {
                aEvent.Set();
            }
        }
        public static void LogError(string msg)
        {
            Monitor.Enter(MsgQueue);
            MsgQueue.Enqueue(string.Format("{0} {1} {2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "Error", msg));
            Monitor.Exit(MsgQueue);
            if (autoResetEventFlag)
            {
                aEvent.Set();
            }
        }
        public static void LogWarn(string msg)
        {
            Monitor.Enter(MsgQueue);
            MsgQueue.Enqueue(string.Format("{0} {1} {2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "Warn", msg));
            Monitor.Exit(MsgQueue);
            if (autoResetEventFlag)
            {
                aEvent.Set();
            }
        }

        /// <summary>
        /// ExitThread是退出日志记录线程的方法，一旦退出，无法开启，一般在程序关闭时执行
        /// </summary>
        public static void ExitThread()
        {
            flag = false;
            aEvent.Set();//恢复线程执行
        }
        private static void WriteMsg()
        {
            while (flag)
            {
                //进行记录
                if (LogFlag)
                {
                    autoResetEventFlag = false;
                    if (!Directory.Exists(FilePath))
                    {
                        Directory.CreateDirectory(FilePath);
                    }
                    string fileName = FilePath + DateTime.Now.ToString("yyyy-MM-dd") + "-Dic.log";
                    var logStreamWriter = new StreamWriter(fileName, true);
                    while (MsgQueue.Count > 0)
                    {
                        Monitor.Enter(MsgQueue);
                        string msg = MsgQueue.Dequeue();
                        Monitor.Exit(MsgQueue);
                        logStreamWriter.WriteLine(msg);
                        if (GetFileSize(fileName) > 1024 * 5)
                        {
                            logStreamWriter.Flush();
                            logStreamWriter.Close();
                            CopyToBak(fileName);
                            logStreamWriter = new StreamWriter(fileName, false);
                            logStreamWriter.Write("");
                            logStreamWriter.Flush();
                            logStreamWriter.Close();
                            logStreamWriter = new StreamWriter(fileName, true);
                        }
                        //下面用于DbgView.exe工具进行在线调试
                        System.Diagnostics.Debug.WriteLine("BS_Debug:" + msg);
                        System.Diagnostics.Trace.WriteLine("BS_Release:" + msg);
                    }
                    logStreamWriter.Flush();
                    logStreamWriter.Close();
                    autoResetEventFlag = true;
                    aEvent.WaitOne();
                }
                else
                {
                    autoResetEventFlag = true;
                    aEvent.WaitOne();
                }
            }
        }
        private static long GetFileSize(string fileName)
        {
            long strRe = 0;
            if (File.Exists(fileName))
            {
                var myFs = new FileInfo(fileName);
                strRe = myFs.Length / 1024;
                //Console.WriteLine(strRe);
            }
            return strRe;
        }
        private static void CopyToBak(string sFileName)
        {
            int fileCount = 0;
            string sBakName = "";
            do
            {
                fileCount++;
                sBakName = sFileName + "." + fileCount + ".BAK";
            }
            while (File.Exists(sBakName));
            File.Copy(sFileName, sBakName);
        }
    }
}
