﻿namespace HIS.COMM.Helper.WebSocketHelper
{
    /// <summary>
    /// 99通用websocket接口的数据通道内容格式,是对message的第二次封装
    /// </summary>
    public class InternalData
    {
        /// <summary>
        /// ok,error状态
        /// </summary>
        public InternalResponseCode responseCode { get; set; }

        /// <summary>
        /// ok,error状态对应描述
        /// </summary>
        public string reponseStatus { get; set; }

        /// <summary>
        /// 医保结算、医保撤销、无卡查询等操作类型
        /// </summary>
        public enInternalType messageType { get; set; }

        /// <summary>
        /// 序列化后的业务数据内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 1、数据交换的标识
        /// 2、YbServer的taskID
        /// </summary>
        public string GUID { get; set; }

    }

}
