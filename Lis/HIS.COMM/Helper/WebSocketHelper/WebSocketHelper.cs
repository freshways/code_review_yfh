﻿using System;
using System.Threading;
using System.Windows.Forms;
using WebSocketSharp;
using WEISHENG.COMM;

namespace HIS.COMM.Helper.WebSocketHelper
{
    public class WebSocketHelper
    {
        public WebSocket ws_client;
        //新起线程，发送心跳包专用
        public WebSocketHelper(EventHandler<MessageEventArgs> onMessage)
        {
            if (ws_client == null || ws_client.ReadyState == WebSocketState.Closed)
            {
                string wsUrl = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("Websocket服务地址", "ws://192.168.10.27:8089/socketServer/", "");
                ws_client = new WebSocket($"{wsUrl}{Comm.getWebsocketClientName(enWebsocketClinetType.医保查询客户端)}"); // 服务器接口
                ws_client.OnMessage += onMessage; // 信息事件
                ws_client.OnClose += ws_OnClose; // 关闭事件
                ws_client.OnError += ws_OnError;
                ws_client.Connect(); // 链接   
                Comm.updateOnlineStatus(enClientBusyStatus.free, enClientOnlineStatus.online, enWebsocketClinetType.医保查询客户端);
            }
        }

        public WebSocketHelper(EventHandler<MessageEventArgs> onMessage, string wsUrl)
        {
            if (ws_client == null || ws_client.ReadyState == WebSocketState.Closed)
            {
                ws_client = new WebSocket(wsUrl); // 服务器接口
                ws_client.OnMessage += onMessage; // 信息事件
                ws_client.OnClose += ws_OnClose; // 关闭事件
                ws_client.OnError += ws_OnError;
                ws_client.Connect(); // 链接                  
            }
        }

        public void SendMsg(string msg)
        {
            if (ws_client != null && ws_client.ReadyState == WebSocketState.Open)
            {
                LogHelper.Info("发送:" + msg);
                ws_client.Send(msg);
            }
        }

        private void ws_OnError(object sender, ErrorEventArgs e)
        {
            MessageBox.Show(e.Message);
        }




        /// <summary>
        /// 服务器接收消息--此处处理接受消息、解析内容等业务逻辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private static void ws_OnMessage(object sender, MessageEventArgs e)
        //{
        //    Console.Write("接收消息：" + e.Data + "\r\n");
        //}

        /// <summary>
        /// 与服务器连接中断--处理连接意外中断
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ws_OnClose(object sender, CloseEventArgs e)
        {
            Comm.updateOnlineStatus(enClientBusyStatus.free, enClientOnlineStatus.offline, enWebsocketClinetType.医保查询客户端);
        }


    }
}
