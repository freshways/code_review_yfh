﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.Helper.WebSocketHelper
{

    /// <summary>
    ///99， websocket的内部操作类型枚举
    /// </summary>
    public enum enInternalType
    {
        医保无卡查询 = 11,
        医保码查询 = 12,
        //医保登记 = 13,
        //医保费用上传 = 14,
        //医保预结算 = 15,
        医保结算 = 16,
        结算撤销 = 17,
        //登记撤销 = 18,

        绑定OpenID = 21,
        移动端扫码 = 22,
        未定义 = -1,
        //lis化验
        //public static final String LIS_HY="31";
        ////pacs检验
        //public static final String PACS_JC="41";
    }
}
