﻿using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;

namespace HIS.COMM.Helper.WebSocketHelper
{
    public class MobileClient
    {
        static string url = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("后端共享服务地址", "http://192.168.10.27:8090", "用于我的移动端");
        public static List<socketBind> getListWxPcUser(string _id, string bindType = "his")
        {
            //测试我的移动端是否在线
            var client = new RestSharp.RestClient(url);
            var requestGet = new RestRequest("/auth/getListWxPcUser/{pcId}/{bindType}", Method.GET);
            requestGet.AddUrlSegment("pcId", _id);
            requestGet.AddUrlSegment("bindType", bindType);
            IRestResponse response = client.Execute(requestGet);
            var contentGet = response.Content;
            var item = JsonConvert.DeserializeObject<restAPIReturn>(contentGet);
            if (item.code != 200 || item == null || item.result == null)
            {               
                return null;
            }
            return item.result;
        }

        public static bool deleteWxPcUser(string openid, string bindType = "his")
        {
            var client = new RestSharp.RestClient(url);
            var requestGet = new RestRequest("/auth/deleteWxPcUser/{openid}/{bindType}", Method.DELETE);
            requestGet.AddUrlSegment("openid", openid);
            requestGet.AddUrlSegment("bindType", bindType);
            IRestResponse response = client.Execute(requestGet);
            var contentGet = response.Content;
            var item = JsonConvert.DeserializeObject<restAPIReturn>(contentGet);
            if (item.code != 200 || item == null || item.result == null)
            {
                msgBalloonHelper.BalloonShow("获取我的移动端设置失败");
                return false;
            }
            return true;
        }

    }
}
