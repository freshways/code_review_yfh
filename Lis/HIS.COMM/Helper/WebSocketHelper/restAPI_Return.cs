﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.Helper.WebSocketHelper
{
    /// <summary>
    /// 我的移动端   返回的信息
    /// </summary>
    public class socketBind
    {
        /// <summary>
        /// 
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string phoneNumber { get; set; }
        /// <summary>
        /// 李涛
        /// </summary>
        public string username { get; set; }
    }

    /// <summary>
    /// 约定的API返回格式
    /// </summary>
    public class restAPIReturn
    {
        /// <summary>
        /// 
        /// </summary>
        public bool success { get; set; }
        /// <summary>
        /// 操作成功！
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<socketBind> result { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string timestamp { get; set; }
    }
}
