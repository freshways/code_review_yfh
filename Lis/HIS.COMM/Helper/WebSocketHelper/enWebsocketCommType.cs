﻿namespace HIS.COMM.Helper.WebSocketHelper
{
    /// <summary>
    /// websocket后台type类型枚举
    /// </summary>
    public enum enWebsocketCommType
    {
        通用 = 99,
        移动端检测 = 61,//我的移动端测试
        HEART_BEAT = 21, //心跳类型
        AUTH = 1,
        消息发送成功 = 200,
        消息发送用户未找到 = 404
    }

}



