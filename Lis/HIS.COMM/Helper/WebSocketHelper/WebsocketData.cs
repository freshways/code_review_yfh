﻿using System;

namespace HIS.COMM.Helper.WebSocketHelper
{
    /// <summary>
    /// 99通用websocket接口的数据通道内容格式
    /// </summary>
    public class WebsocketData
    {
        public enInternalType requestType { get; set; }
        public string conntent { get; set; }
 
    }

}
