﻿namespace HIS.COMM.Helper.WebSocketHelper
{
    /// <summary>
    /// sockect服务端返回的数据格式
    /// </summary>
    public class ReponseContext
    {
        public string toUser { get; set; }
        public string fromUser { get; set; }
        public enWebsocketType type { get; set; }
        public string message { get; set; }
        public string appId { get; set; }
        public string timeStamp { get; set; }
        public string appName { get; set; }

        public string code;//404,200，用于表示消息发送状态
    }


}
