﻿using HIS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEISHENG.COMM;

namespace HIS.COMM.Helper.WebSocketHelper
{
    public class Comm
    {
        /// <summary>
        /// websocket中的数据交换  ClientID
        /// 如果是运行在服务端的使用 ybServer + MAC
        /// 否则使用 单位编码 + 用户编码
        /// </summary>
        /// <param name="enWebsocketClinetType"></param>
        /// <returns></returns>
        public static string getWebsocketClientName(enWebsocketClinetType enWebsocketClinetType)
        {
            if (enWebsocketClinetType == enWebsocketClinetType.医保后台服务)
            {
                return "ybServer" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress;
            }
            else
            {
                return $"{HIS.COMM.zdInfo.Model单位信息.iDwid}_{HIS.COMM.zdInfo.ModelUserInfo.用户编码}";
            }
        }


        /// <summary>
        /// 更新医保服务器正在执行的任务
        /// </summary>
        /// <param name="taskID"></param>
        public static void updateYBServerDoingTask(string taskID)
        {
            try
            {
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    string clientID = getWebsocketClientName(enWebsocketClinetType.医保后台服务);
                    pub_WebsocketClient websocketClient = chis.pub_WebsocketClient.Where(c => c.clientID == clientID).FirstOrDefault();
                    if (websocketClient == null)
                    {
                        pub_WebsocketClient _websocketClient = new pub_WebsocketClient();
                        _websocketClient.clientID = clientID;
                        _websocketClient.clientType = enWebsocketClinetType.医保后台服务.ToString();
                        _websocketClient.isBusy = true;
                        _websocketClient.isOnline = true;
                        _websocketClient.taskID = taskID;
                        _websocketClient.updateTime = DateTime.Now;
                        _websocketClient.createTime = DateTime.Now;
                        chis.pub_WebsocketClient.Add(_websocketClient);
                        chis.SaveChanges();
                        return;
                    }
                    websocketClient.updateTime = DateTime.Now;
                    websocketClient.taskID = taskID;
                    chis.pub_WebsocketClient.Attach(websocketClient);
                    
                    chis.Entry(websocketClient).Property(c => c.updateTime).IsModified = true;
                    chis.Entry(websocketClient).Property(c => c.taskID).IsModified = true;
                    chis.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                msgHelper.ShowInformation(ex.Message);
            }
        }

        /// <summary>
        /// 更新websocket客户端状态
        /// </summary>
        /// <param name="enClientBusyStatuses"></param>
        /// <param name="enClientOnlineStatus"></param>
        /// <param name="enWebsocketClinetType"></param>
        public static void updateOnlineStatus(enClientBusyStatus enClientBusyStatuses, enClientOnlineStatus enClientOnlineStatus,
            enWebsocketClinetType enWebsocketClinetType = enWebsocketClinetType.医保查询客户端)
        {
            try
            {
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    string clientID = getWebsocketClientName(enWebsocketClinetType);
                    pub_WebsocketClient websocketClient = chis.pub_WebsocketClient.Where(c => c.clientID == clientID).FirstOrDefault();
                    if (websocketClient == null)
                    {
                        pub_WebsocketClient _websocketClient = new pub_WebsocketClient();
                        _websocketClient.clientID = clientID;
                        _websocketClient.clientType = enWebsocketClinetType.ToString();
                        _websocketClient.isBusy = false;
                        _websocketClient.isOnline = true;
                        _websocketClient.taskID = "";
                        _websocketClient.updateTime = DateTime.Now;
                        _websocketClient.createTime = DateTime.Now;
                        chis.pub_WebsocketClient.Add(_websocketClient);
                        chis.SaveChanges();
                        return;
                    }
                    websocketClient.updateTime = DateTime.Now;
                    chis.pub_WebsocketClient.Attach(websocketClient);
                    if (enClientBusyStatuses == enClientBusyStatus.busy)
                    {
                        websocketClient.isBusy = true;
                    }
                    else
                    {
                        websocketClient.isBusy = false;
                    }
                    if (enClientOnlineStatus == enClientOnlineStatus.online)
                    {
                        websocketClient.isOnline = true;
                    }
                    else
                    {
                        websocketClient.isOnline = false;
                    }
                    chis.Entry(websocketClient).Property(c => c.updateTime).IsModified = true;
                    chis.Entry(websocketClient).Property(c => c.isBusy).IsModified = true;
                    chis.Entry(websocketClient).Property(c => c.isOnline).IsModified = true;
                    chis.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                msgHelper.ShowInformation(ex.Message);
            }
        }

        /// <summary>
        /// 更新挂死的客户端状态
        /// </summary>
        public static void updateDieClientStatus()
        {
            //更新不在线的clinet
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                string sql = "update pub_WebsocketClient set isOnline = 0 ,taskID = '',isBusy= 0 where isOnline = 1 and updateTime<DATEADD(minute,-10,getdate())";
                chis.Database.ExecuteSqlCommand(sql);
            }
        }
    }

    public enum enClientBusyStatus
    {
        busy = 2,
        free = 3,
    }
    public enum enClientOnlineStatus
    {
        online = 1,
        offline = 9
    }

    public enum enWebsocketClinetType
    {
        医保后台服务 = 1,
        医保查询客户端 = 2
    }
}
