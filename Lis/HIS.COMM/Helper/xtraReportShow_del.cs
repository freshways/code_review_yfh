﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors;
using System.Drawing.Printing;
using DevExpress.Data;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;


namespace HIS.COMM.Helper
{
    public class xtraReportShow_del
    {

        public static bool printToPaper(XtraReport report, string zzmc, Boolean kfdy, string conHisString, string dwbm)
        {

            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, dwbm);
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);

            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;

            report.Print();

            return true;
        }


        public static bool printToPaper(System.Data.DataSet ds, XtraReport report, string zzmc, string conHisString)
        {
            report.DataSource = null;

            report.DataSource = ds.Tables[0];

            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, HIS.COMM.zdInfo.Model单位信息.iDwid.ToString());
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);

            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("获取纸张设置失败，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.PrintDirect, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.ExportFile, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.SendFile, CommandVisibility.None);
            report.Print();

            return true;
        }
        public static bool printToPaper(System.Data.DataTable dt, XtraReport report, string zzmc, string conHisString)
        {
            report.DataSource = null;

            report.DataSource = dt;

            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, HIS.COMM.zdInfo.Model单位信息.iDwid.ToString());
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);

            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("获取纸张设置失败，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.PrintDirect, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.ExportFile, CommandVisibility.None);
            report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.SendFile, CommandVisibility.None);
            report.Print();

            return true;
        }

        public static bool show(System.Data.DataSet ds, XtraReport report, string zzmc, Boolean kfdy, string conHisString)
        {
            report.DataSource = null;

            report.DataSource = ds.Tables[0];

            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, Convert.ToString(HIS.COMM.zdInfo.Model单位信息.iDwid));
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("获取纸张设置失败，使用默认纸张设置。", "提示");
            }

            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }


        public static bool show(DataTable dt, XtraReport report, Boolean kfdy)
        {
            report.DataSource = dt;
            //try
            //{
            //    //边距
            //    for (int i = 0; i < report.Bands.Count; i++)
            //    {
            //        if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
            //            report.Bands[i].HeightF = Convert.ToInt16(paperInfo.Ybj_top);

            //        if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
            //            report.Bands[i].HeightF = Convert.ToInt16(paperInfo.Ybj_bottom);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //如果有错误，则取默认打印机的默认纸张
            //    PrintDocument pd = new PrintDocument();
            //    report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
            //    report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
            //    XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。" + ex.Message, "提示");
            //}
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }

        public static bool show(DataTable dt, XtraReport report, Boolean kfdy, System.Drawing.Printing.PaperKind _paperKind)
        {
            report.DataSource = dt;
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = _paperKind; //纸张类型自定义
            try
            {
                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }
            }
            catch (Exception ex)
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。" + ex.Message, "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }
        public static bool show(DataTable dt, XtraReport report, string zzmc, Boolean kfdy, string conHisString)
        {
            report.DataSource = null;
            report.DataMember = null;
            report.DataAdapter = null;

            report.DataSource = dt;
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, HIS.COMM.zdInfo.Model单位信息.iDwid.ToString());
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);
            }
            catch (Exception ex)
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。" + ex.Message, "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }

        public static bool show(DataView dv, XtraReport report, string zzmc, Boolean kfdy, string conHisString)
        {
            dv.RowFilter = "卡片类型='三代卡'";
            report.DataSource = dv;
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, HIS.COMM.zdInfo.Model单位信息.iDwid.ToString());
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);
            }
            catch (Exception ex)
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。" + ex.Message, "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应

            return true;
        }


        public static bool show(XtraReport report, string zzmc, Boolean kfdy, string conHisString, bool b英寸转换)
        {
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, HIS.COMM.zdInfo.Model单位信息.iDwid.ToString());
                report.PageWidth = Convert.ToInt16(Convert.ToDecimal(paper_infoHelper.i纸张宽度) / 2.54M);
                report.PageHeight = Convert.ToInt16(Convert.ToDecimal(paper_infoHelper.i纸张高度) / 2.54M);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(Convert.ToDecimal(paper_infoHelper.i上边距) / 2.54M);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(Convert.ToDecimal(paper_infoHelper.i下边距) / 2.54M);
                }
                report.Margins.Left = Convert.ToInt16(Convert.ToDecimal(paper_infoHelper.i左边距) / 2.54M);
                report.Margins.Right = Convert.ToInt16(Convert.ToDecimal(paper_infoHelper.i右边距) / 2.54M);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }
            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码

            return true;
        }

        /// <summary>
        /// 报表打印预览，不传入数据集
        /// </summary>
        /// <param name="report"></param>
        /// <param name="zzmc"></param>
        /// <param name="kfdy"></param>
        /// <param name="conHisString"></param>
        /// <returns></returns>
        /// 
        public static bool show(XtraReport report, string zzmc, Boolean kfdy, string conHisString)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm 2.54转换
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, HIS.COMM.zdInfo.Model单位信息.iDwid.ToString());
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }



            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码

            return true;
        }
        //public static bool show(DataTable dt, XtraReport report, int _iWidth, int _iHeight, int _iTop, int _iBottom, int _iLeft, int _iRight, Boolean kfdy)
        //{
        //    report.DataSource = null;
        //    report.DataMember = null;
        //    report.DataAdapter = null;

        //    report.DataSource = dt;

        //    report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
        //    report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
        //    try
        //    {
        //        report.PageWidth = _iWidth;
        //        report.PageHeight = _iHeight;
        //        //边距
        //        for (int i = 0; i < report.Bands.Count; i++)
        //        {
        //            if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
        //                report.Bands[i].HeightF = _iTop;

        //            if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
        //                report.Bands[i].HeightF = _iBottom;
        //        }
        //        report.Margins.Left = _iLeft;
        //        report.Margins.Right = _iRight;
        //        report.Margins.Top = _iTop;
        //        report.Margins.Bottom = _iBottom;
        //    }
        //    catch
        //    {
        //        //如果有错误，则取默认打印机的默认纸张
        //        PrintDocument pd = new PrintDocument();
        //        report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
        //        report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
        //        XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
        //    }



        //    report.CreateDocument();//没有这句，下面的没用
        //    report.PrintingSystem.ShowMarginsWarning = false;
        //    report.PrintingSystem.ShowPrintStatusDialog = false;
        //    if (kfdy)
        //    {
        //        report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);
        //    }
        //    else
        //    {
        //        report.PrintingSystem.SetCommandVisibility
        //            (
        //            new PrintingSystemCommand[]
        //            {
        //                PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
        //            }, CommandVisibility.None
        //            );
        //        report.PrintingSystem.SetCommandVisibility
        //            (
        //            new PrintingSystemCommand[]
        //            {
        //                PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
        //            }, CommandVisibility.None
        //            );
        //        report.PrintingSystem.SetCommandVisibility
        //              (
        //              new PrintingSystemCommand[]
        //            {
        //                PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
        //            }, CommandVisibility.None
        //              );
        //        report.PrintingSystem.SetCommandVisibility
        //           (
        //           new PrintingSystemCommand[]
        //            {
        //                PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
        //            }, CommandVisibility.None
        //           );
        //    }
        //    report.ShowPreviewDialog();//软件暂停，等用户响应
        //    // report.ShowPreview();//直接显示报表后继续运行下面的代码
        //    return true;
        //}
        public static bool printToPaper(XtraReport report, int _iWidth, int _iHeight, int _iTop, int _iBottom, int _iLeft, int _iRight)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                report.PageWidth = _iWidth;
                report.PageHeight = _iHeight;
                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = _iTop;

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = _iBottom;
                }
                report.Margins.Left = _iLeft;
                report.Margins.Right = _iRight;
                report.Margins.Top = _iTop;
                report.Margins.Bottom = _iBottom;
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }

            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            report.Print();
            return true;
        }
        public static bool show(XtraReport report, int _iWidth, int _iHeight, int _iTop, int _iBottom, int _iLeft, int _iRight, Boolean kfdy)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                report.PageWidth = _iWidth;
                report.PageHeight = _iHeight;
                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = _iTop;

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = _iBottom;
                }
                report.Margins.Left = _iLeft;
                report.Margins.Right = _iRight;
                report.Margins.Top = _iTop;
                report.Margins.Bottom = _iBottom;
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }



            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);
            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码
            return true;
        }

        /// <summary>
        /// 直接打印输出
        /// </summary>
        /// <param name="report"></param>
        /// <param name="zzmc"></param>
        /// <param name="conHisString"></param>
        /// <returns></returns>
        public static bool printToPaper(XtraReport report, string zzmc, string conHisString)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, HIS.COMM.zdInfo.Model单位信息.iDwid.ToString());
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }

            //PrintingSystem ps = report.PrintingSystem;
            //PrintingSystemCommand.DocumentMap
            report.CreateDocument();
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.PrintDirect, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.ExportFile, CommandVisibility.None);
            //report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.SendFile, CommandVisibility.None);
            report.Print();


            //if (kfdy)
            //{
            //    report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            //}
            //else
            //{
            //    report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.None);
            //}
            //report.ShowPreviewDialog();
            //    report.ShowPreview();

            return true;
        }


        /// <summary>
        /// 报表打印预览，不传入数据集
        /// </summary>
        /// <param name="report"></param>
        /// <param name="zzmc"></param>
        /// <param name="kfdy"></param>
        /// <param name="conHisString"></param>
        /// <returns></returns>
        public static bool show(XtraReport report, string zzmc, Boolean kfdy, string conHisString, string dwbm)
        {
            report.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter; //设置单位为十分之一mm
            report.PaperKind = System.Drawing.Printing.PaperKind.Custom; //纸张类型自定义
            try
            {
                paper_infoHelper.getPaper(conHisString, zzmc, dwbm);
                report.PageWidth = Convert.ToInt16(paper_infoHelper.i纸张宽度);
                report.PageHeight = Convert.ToInt16(paper_infoHelper.i纸张高度);

                //边距
                for (int i = 0; i < report.Bands.Count; i++)
                {
                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.TopMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i上边距);

                    if (report.Bands[i].GetType().ToString() == "DevExpress.XtraReports.UI.BottomMarginBand")
                        report.Bands[i].HeightF = Convert.ToInt16(paper_infoHelper.i下边距);
                }

                report.Margins.Left = Convert.ToInt16(paper_infoHelper.i左边距);
                report.Margins.Right = Convert.ToInt16(paper_infoHelper.i右边距);
            }
            catch
            {
                //如果有错误，则取默认打印机的默认纸张
                PrintDocument pd = new PrintDocument();
                report.PageHeight = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Height / 0.394);
                report.PageWidth = Convert.ToInt16(pd.DefaultPageSettings.PaperSize.Width / 0.394);
                XtraMessageBox.Show("打印页面设置不正确，使用默认纸张设置。", "提示");
            }



            report.CreateDocument();//没有这句，下面的没用
            report.PrintingSystem.ShowMarginsWarning = false;
            report.PrintingSystem.ShowPrintStatusDialog = false;
            if (kfdy)
            {
                report.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Print, CommandVisibility.All);

            }
            else
            {
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                    (
                    new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Save
                    }, CommandVisibility.None
                    );
                report.PrintingSystem.SetCommandVisibility
                      (
                      new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.ExportFile
                    }, CommandVisibility.None
                      );
                report.PrintingSystem.SetCommandVisibility
                   (
                   new PrintingSystemCommand[]
                    {
                        PrintingSystemCommand.PrintDirect,PrintingSystemCommand.SendFile
                    }, CommandVisibility.None
                   );
            }
            report.ShowPreviewDialog();//软件暂停，等用户响应
            // report.ShowPreview();//直接显示报表后继续运行下面的代码

            return true;
        }



    }


}


//[原创]DevExpress Report 打印边距越界问题
//      DevExpress  Report Print的时候，出现这样的问题：one or more margins are set outside the printable area of the page 。

//      要忽略这个提示，方法为：report.PrintingSystem.ShowMarginsWarning = false;  

//但是需要注意的是，PrintingSystem在report 文档产生后会重新生成。XtraReport.CreateDocument 方法优先级高于设置PrintingSystem的属性。这样处理才是正确的：

//report.CreateDocument();

//report.PrintingSystem.ShowMarginsWarning = false;  




//2.1 获取默认工具栏

// DevExpress.XtraBars.Bar bar = Report.PrintingSystem.PreviewFormEx.PrintBarManager.Bars[0];



//PrintingSystem:当前Report的默认打印组件，用于提供默认的打印预览对话框和打印功能。

//可通过PrintingSystem.PreviewForEx.PrintBarManager获取设置打印预览窗口的工具栏和菜单。



//获取DevExpress.XtraBars.Bar 之后就可以添加或删除新的BarItem和Menu。



//2.2 添加新的按钮

//bar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;

//DevExpress.XtraBars.BarButtonItem printItem = new DevExpress.XtraBars.BarButtonItem(

//printingSystem.PreviewFormEx.PrintBarManager, "打印", 1);

//bar.AddItem(printItem);





//2.3 隐藏不需要的按钮



//事例：隐藏打印按钮

//PrintingSystem.SetCommandVisibility(new PrintingSystemCommand[]{
//PrintingSystemCommand.PrintDirect,PrintingSystemCommand.Print}, CommandVisibility.None);




//SetCommandVisibility（）方法：设置菜单和工具栏按键的显示及隐藏。

//方法原形：

//public void SetCommandVisibility(

//   PrintingSystemCommand[] commands, 

//   CommandVisibility visibility

//);

//Parameters

//commands

//DevExpress.XtraPrinting.PrintingSystemCommand

//指定可在打印系统的预览文件执行的命令。(详细说明请点击链接参见帮助文档)

//visibility

//CommandVisibility.

//指定用于打印系统命令的能见度标准。(详细说明请点击链接参见帮助文档)



//All

//这两个命令的工具栏项目和菜单项是可见的。

//Menu

//已过时;使用CommandVisibility全部代替。这两个命令的工具栏项目和菜单项是可见的。

//None

// 这两个命令的工具栏按钮和菜单项是看不见的。

//Toolbar

//已过时;使用CommandVisibility全部代替。这两个命令的工具栏项目和菜单项是可见的。
