﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HIS.COMM.Helper
{
    public class ClipboardHelper
    {
        public static string action()
        {
            try
            {
                IDataObject iData = Clipboard.GetDataObject();
                if (iData.GetDataPresent(DataFormats.Text))
                {
                    return (String)iData.GetData(DataFormats.Text);
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                msgBalloonHelper.BalloonShow(ex.Message);
                return "";
            }
        }
    }
}
