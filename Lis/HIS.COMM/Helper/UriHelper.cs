﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.Helper
{
    public class UriHelper
    {
        public static string MakeUri(string _uri)
        {
            if (_uri.Contains("{prefix_reportserver}"))
            {
                var page = _uri.Replace("{prefix_reportserver}", HIS.COMM.ClassPubArgument.S医院ReportServer内部登录地址());
                return page.ToString();
            }
            if (_uri.Contains("{localfile}"))
            {
                var page = new Uri(_uri.Replace("{localfile}", $"file:///{AppDomain.CurrentDomain.BaseDirectory}plugins/His.Browser/HTML/"));        
                return page.ToString();
            }
            return new Uri(_uri).ToString();
        }
    }
}
