﻿using HIS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace HIS.COMM.Helper
{
    public class HealthCardHelper
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public static HIS.Model.Pojo.PojoHealthCard GetHealthCardByQrCode(string qrcode)
        {
            var staticCode = "";// GetStaticCode(qrcode);
            var url = HIS.COMM.Helper.EnvironmentHelper.HealthCardInvokeURL();
            url += "?qrCodeText=" + qrcode;
            var json = WEISHENG.COMM.WxAPI.Utils.HttpPost(url, staticCode);
            try
            {
                JObject jObject = JsonConvert.DeserializeObject(json) as JObject;
                var test = WEISHENG.COMM.Helper.JsonHelper.DeserializeJSON<HIS.Model.Pojo.PojoHealthCard>(jObject["rsp"]["card"].ToString());
                return test;
            }
            catch (Exception ex)
            {
                HIS.COMM.msgBalloonHelper.ShowInformation(ex.Message);
                return null;
            }

        }
        static string GetStaticCode(string qrcode)
        {          
            return WEISHENG.COMM.stringHelper.mySplit(qrcode, ':', 0);
        }
    }
}
