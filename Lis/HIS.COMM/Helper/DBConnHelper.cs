﻿using System.Configuration;

namespace HIS.COMM
{
    public class DBConnHelper
    {
        public static string DbType = ConfigurationManager.AppSettings.Get("DbType");
        private static string _sConnSfzh;

        public static string SConnSfzh
        {
            get { return DBConnHelper._sConnSfzh; }
            set { DBConnHelper._sConnSfzh = value; }
        }

        private static string _sConnHisDb = "";

        public static string SConnHISDb
        {
            get { return DBConnHelper._sConnHisDb; }
            set { DBConnHelper._sConnHisDb = value; }
        }
        private static string _sConnHISPubDb = "";

        public static string SConnHISPubDb
        {
            get { return DBConnHelper._sConnHISPubDb; }
            set { DBConnHelper._sConnHISPubDb = value; }
        }
        private static string _sConnYBJK = "";

        public static string SConnYBJK
        {
            get { return DBConnHelper._sConnYBJK; }
            set { DBConnHelper._sConnYBJK = value; }
        }
        private static string _sConnXnhDbString;

        public static string SConnXnhDbString
        {
            get { return DBConnHelper._sConnXnhDbString; }
            set { DBConnHelper._sConnXnhDbString = value; }
        }

        private static string _sConnYthString = "";

        public static string SConnYthString
        {
            get { return DBConnHelper._sConnYthString; }
            set { DBConnHelper._sConnYthString = value; }
        }

        private static string _sLISConn = "";
        public static string sLISConnString
        {
            get
            {
                if (_sLISConn == "")
                {
                    _sLISConn = code.DecryptStr(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS数据库连接", "", "连接LIS数据库的字符串"), code.PubKey2);
                }
                return _sLISConn;
            }
            set { _sLISConn = value; }
        }



        private static string _sConnPlugins;
        public static string SConnPlugins
        {
            get
            {
                return _sConnPlugins;
            }

            set
            {
                _sConnPlugins = value;
            }
        }


    }
}
