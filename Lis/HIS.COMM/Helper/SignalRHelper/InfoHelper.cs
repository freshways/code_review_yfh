﻿using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserCenter.Repository.SignalRObjectes.Models;

namespace HIS.COMM.Helper.SignalRHelper
{
    public static class InfoHelper
    {
        static HubConnection HubConn;
        static string token = "";
        public static HubConnection init()
        {
            try
            {
                getToken();
                setHub();
                return HubConn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async static Task SendToGroup(string groupName, UserMessageContent msg)
        {
          
            await HubConn.InvokeAsync("SendToGroup",
                msg
                , groupName);
        }



        public static async Task SendToPerson(string sfzh, UserMessageContent msg)
        {            
            await HubConn.InvokeAsync("SendToUser",
                msg
                , sfzh);
        }

        public static async Task SendToAll(UserMessageContent context)
        {           
            await HubConn.InvokeAsync("SendToAll",
                context
                );
        }







        static void setHub()
        {
            var url = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("消息推送地址", "http://192.168.10.143:5003/chatHub2", "人脸识别、危机值管理等消息推送服务地址");
            HubConn = new HubConnectionBuilder()
                 .WithUrl(url, options =>             
                 {
                 options.Headers.Add("groupId", "testGroupID");
                 options.Headers.Add("mac", "macabcd");
                 options.AccessTokenProvider = () =>
                 Task.FromResult(token);
                 })
            .WithAutomaticReconnect()//自动连接
            .Build();
            HubConn.ServerTimeout = TimeSpan.FromMinutes(4);
            HubConn.KeepAliveInterval = TimeSpan.FromMinutes(2);
            HubConn.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await HubConn.StartAsync();
            };
        }
        static void getToken()
        {
            if (string.IsNullOrWhiteSpace(HIS.COMM.zdInfo.ModelUserInfo.身份证号))
            {
                throw new Exception("身份证号不能为空");
            }
            appInfo _appInfo = new appInfo();
            _appInfo.appID = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("jwtAppID", "doctor2703", "消息推送appid信息");
            _appInfo.appSecret = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("jwtAppSecret", "jkgly@163.com@2703", "消息推送appid信息");
            _appInfo.appUserName = HIS.COMM.zdInfo.ModelUserInfo.用户名;
            _appInfo.appUserSFZH = HIS.COMM.zdInfo.ModelUserInfo.身份证号;
            _appInfo.groupName = GetGroupName();

            var url = HIS.COMM.Helper.EnvironmentHelper.CommStringGet("JWT授权获取地址", "http://192.168.10.143:5003/api/authentication/GetToken", "jwtToken获取地址");
            var client = new RestSharp.RestClient(url);
            var requestGet = new RestRequest(Method.POST);
            requestGet.AddParameter("application/json", JsonConvert.SerializeObject(_appInfo), ParameterType.RequestBody);
            IRestResponse response = client.Execute(requestGet);
            var contentGet = response.Content;

            var item = JsonConvert.DeserializeObject<tokenInfo>(contentGet);
            token = item.access_token;
        }

        static string GetGroupName()
        {
            return HIS.COMM.zdInfo.Model单位信息.iDwid + "_" + HIS.COMM.zdInfo.ModelUserInfo.科室编码;
        }
    }



    public class appInfo
    {
        public string appID { get; set; }
        public string appSecret { get; set; }

        public string appUserName { get; set; }

        public string appUserPassword { get; set; }

        public string appUserSFZH { get; set; }
        public string groupName { get; set; }
    }

    public class tokenInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string token_type { get; set; }
    }
}
