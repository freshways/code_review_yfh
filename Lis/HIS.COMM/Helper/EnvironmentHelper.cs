﻿using HIS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using WEISHENG.COMM;

namespace HIS.COMM.Helper
{
    /// <summary>
    /// 软件运行参数统一入口
    /// 支持表 gy全局参数,
    /// </summary>
    public static class EnvironmentHelper
    {

        public static List<GY全局参数> cacheSettinges;
        static EnvironmentHelper()
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                cacheSettinges = chis.GY全局参数.ToList();
            }
        }



        /// <summary>
        /// 住院病人退费必须说明退费原因
        /// </summary>
        /// <returns></returns>
        public static bool IsMustDeclareInpatientRefundFeeReason()
        {
            return CommBoolSetting("住院病人退费必须说明退费原因", "true", "用于严格退费操作");
        }
        /// <summary>
        /// 启用医卡通门诊余额控制
        /// </summary>
        /// <returns></returns>
        public static bool IsOneCardBalanceControl()
        {
            return CommBoolSetting("启用医卡通门诊余额控制", "false", "区分发卡充值余额控制和只发卡，不充值");
        }


        /// <summary>
        /// 升级工具限定版本
        /// </summary>
        /// <returns></returns>
        public static string UpgradeToolsLimitVersion()
        {
            return CommStringGet("升级工具限定版本", "2.0.0.1", "限定的自动升级工具最低版本号");
        }

        /// <summary>
        /// 出院结算住院天数计算方法
        /// </summary>
        /// <returns></returns>
        public static string CountHospitalizationDays()
        {
            return CommStringGet("出院结算住院天数计算方法", "最小天数", "（最小天数，最大天数）最小时，用住院日期整数相减，不考虑小时因素。最大时，超过一个24小时单位，就增加一天。");
        }

        /// <summary>
        /// 门诊发票是否打印药品产地
        /// </summary>
        /// <returns></returns>
        public static bool IsOutpatientInvoicePrintDrugOrigin()
        {
            return CommBoolSetting("门诊发票是否打印药品产地", "false", "门诊发票打印药品产地控制");
        }
        /// <summary>
        /// 医保编码模式,用来控制医保上传费用的医院编码,医院名称以及目录对应时候的医院编码名称
        /// </summary>
        /// <returns></returns>
        public static string YbUploadType()
        {
            return CommStringGet("医保编码上传模式", "医院内码", "医院内码,上传医院内部的收费编码和收费名称|医保标准码,上传医保标准的编码");
        }

        /// <summary>
        /// 收费项目维护模式,自由模式可以自由添加,标准目录模式,只能从标准库中增加
        /// </summary>
        /// <returns></returns>
        public static string ChargeItemMaintenanceType()
        {
            return CommStringGet("收费项目维护模式", "自由模式", "自由模式|标准目录模式");
        }


        /// <summary>
        /// 临时医嘱默认频次
        /// </summary>
        /// <returns></returns>
        public static string TemporaryMedicalOrderDefaultFrequency()
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                var MedicalAdviceFrequency = chis.YS医嘱频次.Where(c => c.频次名称 == "st").FirstOrDefault();
                if (MedicalAdviceFrequency == null)
                {
                    chis.YS医嘱频次.Add(new YS医嘱频次() { 频次名称 = "st", 频次次数 = 1, 频次简写 = "st", 拼音代码 = "st" });
                }
                return CommStringGet("临时医嘱默认频次", "st", "临时医嘱全部默认为st或者立即执行");
            }
        }
        //临时医嘱默认频次


        /// <summary>
        /// 病案首页默认地址
        /// </summary>
        /// <returns></returns>
        public static string MedicalRecordHomeDefaultAddress()
        {
            return CommStringGet("病案首页默认地址", "|||", "病案首页上默认显示的地址邮编，省|市|县|邮编号,不包含区划级别后缀");
        }

        /// <summary>
        /// 医嘱打印参数
        /// </summary>
        /// <returns></returns>
        public static string MedicalOrderSignaturePrintParam()
        {
            return CommStringGet("医嘱手签打印参数", "", "医嘱打印显示那些手签 开嘱医生|停嘱医生|开嘱执行|停嘱执行");
            //item.执行时间 = null;//执行
        }

        /// <summary>
        /// 电子健康卡调用地址
        /// </summary>
        /// <returns></returns>
        public static string MedicalOrderTimePrintParam()
        {
            return CommStringGet("医嘱时间打印参数", "", "医嘱打印显示那些手签 执行时间|||");
        }
        public static string HealthCardInvokeURL()
        {
            return CommStringGet("电子健康卡调用地址", "http://10.118.143.100:2033/api/jkgly/getHealthCardByCode", "电子健康卡调用地址，支持读卡扫码");
        }

        /// <summary>
        /// 药库入库单是否显示差价
        /// </summary>
        /// <returns></returns>
        public static bool IsYkglStockInDisplayPriceSpread()
        {
            return CommBoolSetting("药库入库单是否显示差价", "false", "药库入库单是否显示差价");
        }

        /// <summary>
        /// 门诊收款打印预览开关
        /// </summary>
        /// <returns></returns>
        public static bool IsOutpatientPaymentPreview()
        {
            return CommBoolGet("门诊收款打印预览开关", "false", "门诊收款打印预览开关");
        }

        /// <summary>
        /// 获取设置的全部读卡方式
        /// </summary>
        /// <returns></returns>
        ///电子健康卡 = 0,
        //电子医保码 = 1,
        //医保卡 = 2,
        //医保无卡 = 3,
        //身份证读卡 = 4,
        //院内条码 = 5,
        //我的移动终端 = 6,
        //其他 = 7
        public static List<enumReadType> GetReadInfoType()
        {
            List<enumReadType> enumReadTypes = new List<enumReadType>();
            var str = CommStringGet("支持的读卡类型", "医保卡|医保无卡|身份证读卡|", "");
            var arrayList = stringHelper.GetSubStringList(str, '|');
            foreach (var item in arrayList)
            {
                enumReadType enumReadType = (enumReadType)Enum.Parse(typeof(enumReadType), item.ToString());
                enumReadTypes.Add(enumReadType);
            }
            return enumReadTypes;
        }


        static bool? _b启动医保后台服务 = null;

        public static bool b启动医保后台服务
        {
            get
            {
                if (_b启动医保后台服务 == null)
                {
                    _b启动医保后台服务 = CommBoolGet("启动医保后台服务", "false", "");
                }
                return (bool)_b启动医保后台服务;
            }
            set
            {
                _b启动医保后台服务 = value;
            }
        }

        #region 底层支持
        static bool CommBoolSetting(string parameterName, string defaultValue, string remark)
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                var setting = cacheSettinges.Where(c => c.参数名称 == parameterName).FirstOrDefault();
                if (setting == null)
                {
                    chis.GY全局参数.Add(new GY全局参数() { 参数名称 = parameterName, 参数值 = defaultValue, 备注 = remark, updateTime = DateTime.Now });
                    chis.SaveChanges();
                    return Convert.ToBoolean(defaultValue);
                }
                else
                {
                    return Convert.ToBoolean(setting.参数值);
                }
            }
        }
        public static bool CommBoolGet(string parameterName, string defaultValue, string remark)
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                var setting = cacheSettinges.Where(c => c.参数名称 == parameterName).FirstOrDefault();
                if (setting == null)
                {
                    chis.GY全局参数.Add(new GY全局参数() { 参数名称 = parameterName, 参数值 = defaultValue, 备注 = remark, updateTime = DateTime.Now });
                    chis.SaveChanges();
                    cacheSettinges = chis.GY全局参数.ToList();
                    return Convert.ToBoolean(defaultValue);
                }
                else
                {
                    return Convert.ToBoolean(setting.参数值);
                }
            }
        }

        public static string CommStringGet(string parameterName, string defaultValue, string remark, bool useCache = true)
        {
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
            {
                if (useCache)
                {
                    var setting = cacheSettinges.Where(c => c.参数名称 == parameterName).FirstOrDefault();
                    if (setting == null)
                    {
                        chis.GY全局参数.Add(new GY全局参数() { 参数名称 = parameterName, 参数值 = defaultValue, 备注 = remark, updateTime = DateTime.Now });
                        chis.SaveChanges();
                        return defaultValue;
                    }
                    else
                    {
                        return setting.参数值;
                    }
                }
                else
                {
                    var setting = chis.GY全局参数.Where(c => c.参数名称 == parameterName).FirstOrDefault();
                    chis.Entry(setting).Reload();
                    if (setting == null)
                    {
                        chis.GY全局参数.Add(new GY全局参数() { 参数名称 = parameterName, 参数值 = defaultValue, 备注 = remark, updateTime = DateTime.Now });
                        chis.SaveChanges();
                        return defaultValue;
                    }
                    else
                    {
                        return setting.参数值;
                    }
                }
                
            }
        }

        public static bool UpdateValue(string parameterName, string defaultValue, string remark = null)
        {

            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
            {
                var setting = cacheSettinges.Where(c => c.参数名称 == parameterName).FirstOrDefault();
                if (setting == null)
                {
                    chis.GY全局参数.Add(new GY全局参数() { 参数名称 = parameterName, 参数值 = defaultValue, 备注 = remark, updateTime = DateTime.Now });
                }
                else
                {
                    setting.参数值 = defaultValue;
                    chis.GY全局参数.Attach(setting);
                    chis.Entry(setting).Property(c => c.参数值).IsModified = true;
                    if (!string.IsNullOrWhiteSpace(remark))
                    {
                        setting.备注 = remark;
                        chis.Entry(setting).Property(c => c.备注).IsModified = true;
                    }
                }
                chis.SaveChanges();
            }
            return true;
        }
        #endregion

    }
}
