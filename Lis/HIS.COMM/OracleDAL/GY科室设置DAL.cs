﻿// litao@Copy Right 2006-2016
// 文件： GY科室设置.cs
// 创建时间：2018-06-13
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Text;
using HIS.COMM.Model;
using HIS.COMM.IDAL;

namespace HIS.COMM.OracleDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.GY科室设置.
    /// </summary>
    public partial class GY科室设置DAL : IGY科室设置
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM GY科室设置 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM GY科室设置 WHERE ID=:ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM GY科室设置 WHERE ID=:ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM GY科室设置 WHERE ID=:ID";
		private static readonly string SQL_INSERTGY科室设置 = "INSERT INTO GY科室设置 (科室编码,科室名称,拼音代码,科室类型,是否单设药房,是否住院科室,是否病区科室,是否科室库存,是否禁用,单位编码,ID,YTHID,分院编码,是否固定资产管理,GUID) VALUES (:科室编码,:科室名称,:拼音代码,:科室类型,:是否单设药房,:是否住院科室,:是否病区科室,:是否科室库存,:是否禁用,:单位编码,:ID,:YTHID,:分院编码,:是否固定资产管理,:GUID)";
		private static readonly string SQL_UPDATE_GY科室设置_BY_PR = "UPDATE GY科室设置 SET 科室编码=:科室编码,科室名称=:科室名称,拼音代码=:拼音代码,科室类型=:科室类型,是否单设药房=:是否单设药房,是否住院科室=:是否住院科室,是否病区科室=:是否病区科室,是否科室库存=:是否科室库存,是否禁用=:是否禁用,单位编码=:单位编码,YTHID=:YTHID,分院编码=:分院编码,是否固定资产管理=:是否固定资产管理 WHERE ID=:ID";
		private static readonly string PARM_PRM_GY科室设置 = ":ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public GY科室设置DAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="gy科室设置Model">GY科室设置实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(GY科室设置Model gy科室设置Model)
		{
			
            OracleParameter[] _param = GetInsertParameters();
			
			_param[0].Value = gy科室设置Model.科室编码;
			_param[1].Value = gy科室设置Model.科室名称;
			_param[2].Value = gy科室设置Model.拼音代码;
			_param[3].Value = gy科室设置Model.科室类型;
			_param[4].Value = gy科室设置Model.是否单设药房;
			_param[5].Value = gy科室设置Model.是否住院科室;
			_param[6].Value = gy科室设置Model.是否病区科室;
			_param[7].Value = gy科室设置Model.是否科室库存;
			_param[8].Value = gy科室设置Model.是否禁用;
			_param[9].Value = gy科室设置Model.单位编码;
			_param[10].Value = gy科室设置Model.ID;
			_param[11].Value = gy科室设置Model.YTHID;
			_param[12].Value = gy科室设置Model.分院编码;
			_param[13].Value = gy科室设置Model.是否固定资产管理;
			_param[14].Value = gy科室设置Model.GUID;
			
			return DbHelperOra.ExecuteSql(SQL_INSERTGY科室设置,_param);
			
		}
		/// <summary>
		/// 向数据表GY科室设置更新一条记录。

		/// </summary>
		/// <param name="gy科室设置Model">gy科室设置Model</param>
		/// <returns>影响的行数</returns>
		public int Update(GY科室设置Model gy科室设置Model)
		{
            OracleParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=gy科室设置Model.科室编码;
			_param[1].Value=gy科室设置Model.科室名称;
			_param[2].Value=gy科室设置Model.拼音代码;
			_param[3].Value=gy科室设置Model.科室类型;
			_param[4].Value=gy科室设置Model.是否单设药房;
			_param[5].Value=gy科室设置Model.是否住院科室;
			_param[6].Value=gy科室设置Model.是否病区科室;
			_param[7].Value=gy科室设置Model.是否科室库存;
			_param[8].Value=gy科室设置Model.是否禁用;
			_param[9].Value=gy科室设置Model.单位编码;
			_param[10].Value=gy科室设置Model.ID;
			_param[11].Value=gy科室设置Model.YTHID;
			_param[12].Value=gy科室设置Model.分院编码;
			_param[13].Value=gy科室设置Model.是否固定资产管理;
			_param[14].Value=gy科室设置Model.GUID;
			
			return DbHelperOra.ExecuteSql(SQL_UPDATE_GY科室设置_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表GY科室设置中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			OracleParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return DbHelperOra.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  gy科室设置 数据实体1
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>gy科室设置 数据实体</returns>
		private GY科室设置Model GetModelFromDr(DataRow row)
		{
			GY科室设置Model Obj = new GY科室设置Model();
			if(row!=null)
			{
				Obj.科室编码      = (( row["科室编码"])==DBNull.Value)?0:Convert.ToInt32( row["科室编码"]);
				Obj.科室名称      =  row["科室名称"].ToString();
				Obj.拼音代码      =  row["拼音代码"].ToString();
				Obj.科室类型      =  row["科室类型"].ToString();
				Obj.是否单设药房    =  row["是否单设药房"] == DBNull.Value ? false : true;
				Obj.是否住院科室    =  row["是否住院科室"] == DBNull.Value ? false : true;
				Obj.是否病区科室    =  row["是否病区科室"] == DBNull.Value ? false : true;
				Obj.是否科室库存    =  row["是否科室库存"] == DBNull.Value ? false : true;
				Obj.是否禁用      =  row["是否禁用"] == DBNull.Value ? false : true;
				Obj.单位编码      = (( row["单位编码"])==DBNull.Value)?0:Convert.ToInt32( row["单位编码"]);
				Obj.ID        = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.YTHID     = (( row["YTHID"])==DBNull.Value)?0:Convert.ToInt32( row["YTHID"]);
				Obj.分院编码      = (( row["分院编码"])==DBNull.Value)?0:Convert.ToInt32( row["分院编码"]);
				Obj.是否固定资产管理  =  row["是否固定资产管理"] == DBNull.Value ? false : true;
				Obj.GUID      = (Guid) row["GUID"];
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  gy科室设置 数据实体2
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>gy科室设置 数据实体</returns>
		private GY科室设置Model GetModelFromDr(IDataReader dr)
		{
			GY科室设置Model Obj = new GY科室设置Model();
			
				Obj.科室编码     = (( dr["科室编码"])==DBNull.Value)?0:Convert.ToInt32( dr["科室编码"]);
				Obj.科室名称     =  dr["科室名称"].ToString();
				Obj.拼音代码     =  dr["拼音代码"].ToString();
				Obj.科室类型     =  dr["科室类型"].ToString();
				Obj.是否单设药房   =  dr["是否单设药房"] == DBNull.Value ? false : true;
				Obj.是否住院科室   =  dr["是否住院科室"] == DBNull.Value ? false : true;
				Obj.是否病区科室   =  dr["是否病区科室"] == DBNull.Value ? false : true;
				Obj.是否科室库存   =  dr["是否科室库存"] == DBNull.Value ? false : true;
				Obj.是否禁用     =  dr["是否禁用"] == DBNull.Value ? false : true;
				Obj.单位编码     = (( dr["单位编码"])==DBNull.Value)?0:Convert.ToInt32( dr["单位编码"]);
				Obj.ID       = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.YTHID    = (( dr["YTHID"])==DBNull.Value)?0:Convert.ToInt32( dr["YTHID"]);
				Obj.分院编码     = (( dr["分院编码"])==DBNull.Value)?0:Convert.ToInt32( dr["分院编码"]);
				Obj.是否固定资产管理 =  dr["是否固定资产管理"] == DBNull.Value ? false : true;
				Obj.GUID     = (Guid) dr["GUID"];
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个GY科室设置对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>GY科室设置对象</returns>
		public GY科室设置Model GetGY科室设置 (int id)
		{
			GY科室设置Model _obj=null;			
			OracleParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表GY科室设置所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY科室设置Model> GetGY科室设置All()
		{			
			return GetGY科室设置All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表GY科室设置所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY科室设置Model> GetGY科室设置All(string sqlWhere)
		{
			IList<GY科室设置Model> list=new List<GY科室设置Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere.Replace("'","''");
			}
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			OracleParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(DbHelperOra.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetPRMParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(PARM_PRM_GY科室设置);

            if (parms == null) {
                parms = new OracleParameter[] {						
					new OracleParameter(":ID",OracleDbType.Int32)
				};
                DbHelperOra.CacheParameters(PARM_PRM_GY科室设置, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetInsertParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_INSERTGY科室设置);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":科室编码",OracleDbType.Int32),
					new OracleParameter(":科室名称",OracleDbType.Varchar2),
					new OracleParameter(":拼音代码",OracleDbType.Varchar2),
					new OracleParameter(":科室类型",OracleDbType.Varchar2),
					new OracleParameter(":是否单设药房",OracleDbType.Int16),
					new OracleParameter(":是否住院科室",OracleDbType.Int16),
					new OracleParameter(":是否病区科室",OracleDbType.Int16),
					new OracleParameter(":是否科室库存",OracleDbType.Int16),
					new OracleParameter(":是否禁用",OracleDbType.Int16),
					new OracleParameter(":单位编码",OracleDbType.Int32),
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":YTHID",OracleDbType.Int32),
					new OracleParameter(":分院编码",OracleDbType.Int32),
					new OracleParameter(":是否固定资产管理",OracleDbType.Int16),
					new OracleParameter(":GUID",OracleDbType.Varchar2)
					};
                DbHelperOra.CacheParameters(SQL_INSERTGY科室设置, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetUpdateParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_UPDATE_GY科室设置_BY_PR);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":科室编码",OracleDbType.Int32),
					new OracleParameter(":科室名称",OracleDbType.Varchar2),
					new OracleParameter(":拼音代码",OracleDbType.Varchar2),
					new OracleParameter(":科室类型",OracleDbType.Varchar2),
					new OracleParameter(":是否单设药房",OracleDbType.Int16),
					new OracleParameter(":是否住院科室",OracleDbType.Int16),
					new OracleParameter(":是否病区科室",OracleDbType.Int16),
					new OracleParameter(":是否科室库存",OracleDbType.Int16),
					new OracleParameter(":是否禁用",OracleDbType.Int16),
					new OracleParameter(":单位编码",OracleDbType.Int32),
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":YTHID",OracleDbType.Int32),
					new OracleParameter(":分院编码",OracleDbType.Int32),
					new OracleParameter(":是否固定资产管理",OracleDbType.Int16),
					new OracleParameter(":GUID",OracleDbType.Varchar2)
					};
                DbHelperOra.CacheParameters(SQL_UPDATE_GY科室设置_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

