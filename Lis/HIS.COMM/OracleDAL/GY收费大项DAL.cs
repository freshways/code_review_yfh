﻿// litao@Copy Right 2006-2016
// 文件： GY收费大项.cs
// 创建时间：2018-05-26
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Text;
using HIS.COMM.Model;
using HIS.COMM.IDAL;

namespace HIS.COMM.OracleDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.GY收费大项.
    /// </summary>
    public partial class GY收费大项DAL : IGY收费大项
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM GY收费大项 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM GY收费大项 WHERE ID=:ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM GY收费大项 WHERE ID=:ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM GY收费大项 WHERE ID=:ID";
		private static readonly string SQL_INSERTGY收费大项 = "INSERT INTO GY收费大项 (ID,项目编码,项目名称,拼音代码,单位编码) VALUES (:ID,:项目编码,:项目名称,:拼音代码,:单位编码)";
		private static readonly string SQL_UPDATE_GY收费大项_BY_PR = "UPDATE GY收费大项 SET ,项目编码=:项目编码,项目名称=:项目名称,拼音代码=:拼音代码,单位编码=:单位编码 WHERE ID=:ID";
		private static readonly string PARM_PRM_GY收费大项 = ":ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public GY收费大项DAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。

		/// </summary>
		/// <param name="gy收费大项Model">GY收费大项实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(GY收费大项Model gy收费大项Model)
		{
			
            OracleParameter[] _param = GetInsertParameters();
			
			_param[0].Value = gy收费大项Model.ID;
			_param[1].Value = gy收费大项Model.项目编码;
			_param[2].Value = gy收费大项Model.项目名称;
			_param[3].Value = gy收费大项Model.拼音代码;
			_param[4].Value = gy收费大项Model.单位编码;
			
			return DbHelperOra.ExecuteSql(SQL_INSERTGY收费大项,_param);
			
		}
		/// <summary>
		/// 向数据表GY收费大项更新一条记录。

		/// </summary>
		/// <param name="gy收费大项Model">gy收费大项Model</param>
		/// <returns>影响的行数</returns>
		public int Update(GY收费大项Model gy收费大项Model)
		{
            OracleParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=gy收费大项Model.ID;
			_param[1].Value=gy收费大项Model.项目编码;
			_param[2].Value=gy收费大项Model.项目名称;
			_param[3].Value=gy收费大项Model.拼音代码;
			_param[4].Value=gy收费大项Model.单位编码;
			
			return DbHelperOra.ExecuteSql(SQL_UPDATE_GY收费大项_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表GY收费大项中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			OracleParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return DbHelperOra.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  gy收费大项 数据实体1
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>gy收费大项 数据实体</returns>
		private GY收费大项Model GetModelFromDr(DataRow row)
		{
			GY收费大项Model Obj = new GY收费大项Model();
			if(row!=null)
			{
				Obj.ID    = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.项目编码  = (( row["项目编码"])==DBNull.Value)?0:Convert.ToInt32( row["项目编码"]);
				Obj.项目名称  =  row["项目名称"].ToString();
				Obj.拼音代码  =  row["拼音代码"].ToString();
				Obj.单位编码  = (( row["单位编码"])==DBNull.Value)?0:Convert.ToInt32( row["单位编码"]);
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  gy收费大项 数据实体2
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>gy收费大项 数据实体</returns>
		private GY收费大项Model GetModelFromDr(IDataReader dr)
		{
			GY收费大项Model Obj = new GY收费大项Model();
			
				Obj.ID   = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.项目编码 = (( dr["项目编码"])==DBNull.Value)?0:Convert.ToInt32( dr["项目编码"]);
				Obj.项目名称 =  dr["项目名称"].ToString();
				Obj.拼音代码 =  dr["拼音代码"].ToString();
				Obj.单位编码 = (( dr["单位编码"])==DBNull.Value)?0:Convert.ToInt32( dr["单位编码"]);
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个GY收费大项对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>GY收费大项对象</returns>
		public GY收费大项Model GetGY收费大项 (int id)
		{
			GY收费大项Model _obj=null;			
			OracleParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表GY收费大项所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY收费大项Model> GetGY收费大项All()
		{			
			return GetGY收费大项All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表GY收费大项所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY收费大项Model> GetGY收费大项All(string sqlWhere)
		{
			IList<GY收费大项Model> list=new List<GY收费大项Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere.Replace("'","''");
			}
			using(OracleDataReader dr=DbHelperOra.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			OracleParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(DbHelperOra.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetPRMParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(PARM_PRM_GY收费大项);

            if (parms == null) {
                parms = new OracleParameter[] {						
					new OracleParameter(":ID",OracleDbType.Int32)
				};
                DbHelperOra.CacheParameters(PARM_PRM_GY收费大项, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetInsertParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_INSERTGY收费大项);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":项目编码",OracleDbType.Int32),
					new OracleParameter(":项目名称",OracleDbType.Varchar2),
					new OracleParameter(":拼音代码",OracleDbType.Varchar2),
					new OracleParameter(":单位编码",OracleDbType.Int32)
					};
                DbHelperOra.CacheParameters(SQL_INSERTGY收费大项, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static OracleParameter[] GetUpdateParameters() {
            OracleParameter[] parms = DbHelperOra.GetCachedParameters(SQL_UPDATE_GY收费大项_BY_PR);

            if (parms == null) {
                parms = new OracleParameter[] {	
					new OracleParameter(":ID",OracleDbType.Int32),
					new OracleParameter(":项目编码",OracleDbType.Int32),
					new OracleParameter(":项目名称",OracleDbType.Varchar2),
					new OracleParameter(":拼音代码",OracleDbType.Varchar2),
					new OracleParameter(":单位编码",OracleDbType.Int32)
					};
                DbHelperOra.CacheParameters(SQL_UPDATE_GY收费大项_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

