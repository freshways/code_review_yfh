﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace HIS.COMM
{
    public class DbControl
    {
        public static string killprocess(string connstring, string dbname)
        {
            string resultStr="";
            try
            {
                string sql = "";
                if (dbname == "")
                {
                    sql =
                   "USE master " +


                   //" declare @t_count int " +
                                    //" set @t_count=1 " +
                                    //" while exists(SELECT * FROM ::::fn_trace_geteventinfo(@t_count)) " +
                                    //" begin " +
                                    //" exec sp_trace_setstatus @t_count,0 " +
                                    //" set @t_count=@t_count+1 " +
                                    //" end " +


                   "  iF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_addaliases]') AND  " +
                   "  OBJECTPROPERTY(id,N'IsProcedure') = 1) " +
                   "  DROP PROCEDURE [dbo].[sp_addaliases] " +


                    " EXEC dbo.sp_executesql @statement =  " +
                   "  N'CREATE   proc   [dbo].[sp_addaliases]    " +
                 //  "  @dbname   varchar(200)  " +
                    " as        " +
                    " declare   @sql     nvarchar(500)     " +
                    " declare   @spid   nvarchar(20)   " +
                    " declare   #tb   cursor   for    " +
                    " select   spid=cast(spid   as   varchar(20))   from   master..sysprocesses    " +
                    " open   #tb    " +
                   "  fetch   next   from   #tb   into   @spid    " +
                   "  while   @@fetch_status=0    " +
                   "  begin        " +
                   "  exec(''kill   ''+@spid)    " +
                   "  fetch   next   from   #tb   into   @spid    " +
                   "  end        " +
                   "  close   #tb  " +
                   "  deallocate   #tb' " +


                  "   EXEC dbo.sp_executesql @temp=N'[sp_addaliases] '  " +


                   "  iF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_addaliases]') AND  " +
                   "  OBJECTPROPERTY(id,N'IsProcedure') = 1) " +
                   "  DROP PROCEDURE [dbo].[sp_addaliases]";



                }
                else
                {
                    sql =
                   "USE master " +


                   //" declare @t_count int " +
                                    //" set @t_count=1 " +
                                    //" while exists(SELECT * FROM ::::fn_trace_geteventinfo(@t_count)) " +
                                    //" begin " +
                                    //" exec sp_trace_setstatus @t_count,0 " +
                                    //" set @t_count=@t_count+1 " +
                                    //" end " +


                   "  iF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_addaliases]') AND  " +
                   "  OBJECTPROPERTY(id,N'IsProcedure') = 1) " +
                   "  DROP PROCEDURE [dbo].[sp_addaliases] " +


                    " EXEC dbo.sp_executesql @statement =  " +
                    "  N'CREATE   proc   [dbo].[sp_addaliases]    " +
                    "  @dbname   varchar(200)  " +
                    " as        " +
                    " declare   @sql     nvarchar(500)     " +
                    " declare   @spid   nvarchar(20)   " +
                    " declare   #tb   cursor   for    " +
                    " select   spid=cast(spid   as   varchar(20))   from   master..sysprocesses   where   dbid=db_id " + " (@dbname)    " +
                    " open   #tb    " +
                    "  fetch   next   from   #tb   into   @spid    " +
                    "  while   @@fetch_status=0    " +
                    "  begin        " +
                    "  exec(''kill   ''+@spid)    " +
                    "  fetch   next   from   #tb   into   @spid    " +
                    "  end        " +
                    "  close   #tb  " +
                    "  deallocate   #tb' " +
                    "   EXEC dbo.sp_executesql @temp=N'[sp_addaliases] " + (dbname) + "'  " +
                    "  iF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_addaliases]') AND  " +
                    "  OBJECTPROPERTY(id,N'IsProcedure') = 1) " +
                    "  DROP PROCEDURE [dbo].[sp_addaliases]";
                }
                WEISHENG.COMM.Db.SqlHelper.ExecuteNonQuery(connstring, CommandType.Text, sql);

            }
            catch (Exception ex)
            {
                resultStr = ex.Message;
                //WEISHENG.COMM.Log.LogToFile("log\\err", ex.Message);
                //throw;
            }
            return resultStr;
        }
    }
}


        //USE master
        //go
        
        //declare @t_count int
        //set @t_count=1
        //while exists(SELECT * FROM ::::fn_trace_geteventinfo(@t_count))
        //begin
        //exec sp_trace_setstatus @t_count,0
        //set @t_count=@t_count+1
        //end
        //go
        
        // iF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_addaliasess]') AND 
        // OBJECTPROPERTY(id,N'IsProcedure') = 1)
        // DROP PROCEDURE [dbo].[sp_addaliasess]
        //go
        
        // EXEC dbo.sp_executesql @statement = 
        // N'CREATE   proc   [dbo].[sp_addaliasess]   
        // as       
        // declare   @sql     nvarchar(500)       
        // declare   @spid   nvarchar(20)   
        // declare   #tb   cursor   for   
        // select   spid=cast(spid   as   varchar(20))   from   master..sysprocesses   where (db_name(dbid) like ''AIS%'') or  (db_name(dbid) like ''xnh%'')
        // open   #tb   
        // fetch   next   from   #tb   into   @spid   
        // while   @@fetch_status=0   
        // begin       
        // exec(''kill   ''+@spid)   
        // fetch   next   from   #tb   into   @spid   
        // end       
        // close   #tb   
        // deallocate   #tb'
        //go
        
        //EXEC dbo.sp_executesql @temp=N'[sp_addaliasess]' 
        //go
        
        // iF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_addaliasess]') AND 
        // OBJECTPROPERTY(id,N'IsProcedure') = 1)
        // DROP PROCEDURE [dbo].[sp_addaliasess]
        //go
        
        //Close;




        //                                                  USE master
        //go
        
        //declare @t_count int
        //set @t_count=1
        //while exists(SELECT * FROM ::::fn_trace_geteventinfo(@t_count))
        //begin
        //exec sp_trace_setstatus @t_count,0
        //set @t_count=@t_count+1
        //end
        //go
        
        // iF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_addaliases]') AND 
        // OBJECTPROPERTY(id,N'IsProcedure') = 1)
        // DROP PROCEDURE [dbo].[sp_addaliases]
        //go
        
        // EXEC dbo.sp_executesql @statement = 
        // N'CREATE   proc   [dbo].[sp_addaliases]   
        // @dbname   varchar(200) 
        // as       
        // declare   @sql     nvarchar(500)       
        // declare   @spid   nvarchar(20)   
        // declare   #tb   cursor   for   
        // select   spid=cast(spid   as   varchar(20))   from   master..sysprocesses   where   dbid=db_id
        // (@dbname)   
        // open   #tb   
        // fetch   next   from   #tb   into   @spid   
        // while   @@fetch_status=0   
        // begin       
        // exec(''kill   ''+@spid)   
        // fetch   next   from   #tb   into   @spid   
        // end       
        // close   #tb   
        // deallocate   #tb'
        //go
        
        // EXEC dbo.sp_executesql @temp=N'[sp_addaliases] '' + QuotedStr(dbname) + ''' 
        //go
        
        // iF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp_addaliases]') AND 
        // OBJECTPROPERTY(id,N'IsProcedure') = 1)
        // DROP PROCEDURE [dbo].[sp_addaliases]
        //go
        
        //Close;