﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WEISHENG.COMM;

namespace HIS.COMM
{

    public class personYB : HIS.COMM.PersonBase
    {

        //#region 新农合调用
        private string _s诊治医生;

        public string S诊治医生
        {
            get { return _s诊治医生; }
            set { _s诊治医生 = value; }
        }
        private string _s疾病诊断;

        public string S疾病诊断
        {
            get { return _s疾病诊断; }
            set { _s疾病诊断 = value; }
        }
        private decimal dec剔除金额;

        public decimal Dec剔除金额
        {
            get { return dec剔除金额; }
            set { dec剔除金额 = value; }
        }
        private string _s单据号;

        public string S单据号
        {
            get { return _s单据号; }
            set { _s单据号 = value; }
        }

        private decimal dec普通费用 = 0;

        public decimal Dec普通应报费用
        {
            get { return dec普通费用; }
            set { dec普通费用 = value; }
        }

        private decimal dec报销金额 = 0;

        public decimal Dec报销金额
        {
            get { return dec报销金额; }
            set { dec报销金额 = value; }
        }
        private decimal dec病人自负 = 0;

        public decimal Dec病人自负
        {
            get { return dec病人自负; }
            set { dec病人自负 = value; }
        }
        private decimal dec处方金额 = 0;

        public decimal Dec处方金额
        {
            get { return dec处方金额; }
            set { dec处方金额 = value; }
        }

        private bool b发生一般诊疗费 = false;

        public bool B发生一般诊疗费
        {
            get { return b发生一般诊疗费; }
            set { b发生一般诊疗费 = value; }
        }

        private decimal dec一般诊疗费 = 0;

        public decimal Dec一般诊疗费
        {
            get { return dec一般诊疗费; }
            set { dec一般诊疗费 = value; }
        }


        private decimal dec本年已报;

        public decimal Dec本年已报
        {
            get { return dec本年已报; }
            set { dec本年已报 = value; }
        }
        private string _个人编号;

        public string 个人编号
        {
            get { return _个人编号; }
            set { _个人编号 = value; }
        }

        private decimal xnh_dec_Bnyb;//本年已报金额
        public decimal Xnh_dec_Bnyb
        {
            get
            {
                return xnh_dec_Bnyb;
            }

            set
            {
                xnh_dec_Bnyb = value;
            }
        }
        private string s新农合家庭序号 = "-1";//序号


        public string S新农合家庭序号
        {
            get
            {
                return s新农合家庭序号;
            }

            set
            {
                s新农合家庭序号 = value;
            }
        }


        private string _s新农合医疗证号;

        public string S新农合医疗证号
        {
            get { return _s新农合医疗证号; }
            set { _s新农合医疗证号 = value; }
        }
        //#endregion


        public _stru领款人 Stru领款人 = new _stru领款人();

        ClassEnmu单据状态.en门诊单据 _en门诊单据状态;

        public ClassEnmu单据状态.en门诊单据 En门诊单据状态
        {
            get { return _en门诊单据状态; }
            set { _en门诊单据状态 = value; }
        }

        #region 新农合门诊收费相关
        //private string _s诊治医生;

        //public string S诊治医生
        //{
        //    get { return _s诊治医生; }
        //    set { _s诊治医生 = value; }
        //}

        #endregion




        private bool b无卡交易;

        public bool B无卡交易
        {
            get { return b无卡交易; }
            set { b无卡交易 = value; }
        }


        //门诊收款用到的变量
        private string _sMzsf_Fphm;

        public string s门诊发票号
        {
            get { return _sMzsf_Fphm; }
            set { _sMzsf_Fphm = value; }
        }
        private string _sMzsf_Mzid;

        public string sMZID
        {
            get { return _sMzsf_Mzid; }
            set { _sMzsf_Mzid = value; }
        }

        private string _sCFID;

        public string sCFID
        {
            get { return _sCFID; }
            set { _sCFID = value; }
        }

        private string _sMzsf_报销结果;

        public string sMzsf_报销结果
        {
            get { return _sMzsf_报销结果; }
            set { _sMzsf_报销结果 = value; }
        }
        private decimal _dec上传总费用;

        public decimal decMZ上传总费用
        {
            get { return _dec上传总费用; }
            set { _dec上传总费用 = value; }
        }

        private decimal _sMzsf_总费用 = 0;

        public decimal decMz处方总费用
        {
            get { return Math.Round(_sMzsf_总费用, 2, MidpointRounding.AwayFromZero); }
            set { _sMzsf_总费用 = value; }
        }

        //private string _sMzsf_应报费用 = "0";

        //public string sMzsf_应报费用
        //{
        //    get { return _sMzsf_应报费用; }
        //    set { _sMzsf_应报费用 = value; }
        //}
        //private decimal _sMzsf_医保刷卡金额 = 0;

        //public decimal dec医保刷卡金额
        //{
        //    get { return _sMzsf_医保刷卡金额; }
        //    set { _sMzsf_医保刷卡金额 = value; }
        //}
        //private string _sMzsf_新合报销金额 = "0";

        //public string s统筹支付  // sMzsf_新合报销金额
        //{
        //    get { return _sMzsf_新合报销金额; }
        //    set { _sMzsf_新合报销金额 = value; }
        //}
        private decimal _sMzsf_账户冲减金额 = 0;

        public decimal dec账户冲减金额
        {
            get { return _sMzsf_账户冲减金额; }
            set { _sMzsf_账户冲减金额 = value; }
        }
        //private string _sMzsf_收现金 = "0";

        //public string sMzsf_收现金
        //{
        //    get { return _sMzsf_收现金; }
        //    set { _sMzsf_收现金 = value; }
        //}
        private decimal _sMzsf_货币误差 = 0;

        public decimal dec货币误差
        {
            get { return _sMzsf_货币误差; }
            set { _sMzsf_货币误差 = value; }
        }
        public void init金额()
        {
            decMz处方总费用 = 0;
            dec应报费用 = 0;
            //dec医保刷卡金额 = 0;
            //reDec统筹支付 = 0;
            dec账户冲减金额 = 0;
            //sMzsf_收现金 = "0";
            dec货币误差 = 0;
        }




        public string s民族; //民族
        public string sIC卡号; //IC卡号(银行卡号)    医保中心手工报销支付账号
        public string s三代卡社保号;//sldll   银联个人账户，银行POS隔日到账

        public string s单位编码; //单位编码
        private string _s个人社保编号 = ""; //个人社保编号(分析可能写入芯片卡中了,eaagent.log显示开始的时候都是这个入口：931001|社保编号)

        public string s个人社保编号
        {
            get { return _s个人社保编号; }
            set { _s个人社保编号 = value; }
        }
        public string s人员类别编号; //医疗人员类别  本次升级重点
        public string s修改日期; //修改日期
        public string qt1; //其它1  临沂改为慢性病补助证号
        public string qt2; //其它2  临沂改为慢性病发证日期
        public string qt3; //其它3
        public string s住院状态; //在院状态 0：出院 1：在院
        public string s国籍;

        //读取个人账户累计信息
        //private string _sAccount = "";

        public string sAccount = "";//人账户累计信息Personaccout串
        //{
        //    get { return _sAccount; }
        //    set { _sAccount = value; }
        //}

        public decimal decAccount统筹支出累计;
        public decimal decAccount救助金支出累计;
        public decimal decAccount账户余额;//账户余额
        public decimal decAccount转出医院起付标准;//临沂暂时未用
        public decimal decAccount转出医院起付标准自付;//临沂暂时未用
        public decimal decAccount本年度住院次数;
        public decimal decAccount账户支出累计;
        public decimal decAccount门诊大病统筹支出累计;//临沂暂时未用
        public decimal decAccount门诊慢性病统筹支出累计;
        public decimal decAccount个人现金支出累计;
        public decimal decAccount乙类自理费用累计;
        public decimal decAccount公务员补助支出累计;
        public decimal decAccount个人自费费用累计;
        public decimal decAccount进入统筹费用累计;
        public decimal decAccount第一次住院医院等级;
        public decimal decAccount第一次住院起付标准自付;
        public decimal decAccount第二次住院医院等级;
        public decimal decAccount第二次住院起付标准自付;
        public decimal decAccount三次以上住院起付标准自付累计;
        public decimal decAccount门诊慢性病起付标准自付累计;



        private string s人员类别名称;//根据规则转化后的人员类别

        public string S人员类别名称
        {
            get { return s人员类别名称; }
            set { s人员类别名称 = value; }
        }

        //private decimal _decMz处方金额;//门诊处方金额=grzf+cj  

        //public decimal decMz处方金额
        //{
        //    get { return _decMz处方金额; }
        //    set { _decMz处方金额 = value; }
        //}

        private decimal _dec应报费用 = 0;

        public decimal dec应报费用
        {
            get { return _dec应报费用; }
            set { _dec应报费用 = value; }
        }

        private decimal _decMz账户冲减金额 = 0;//门诊账户冲减金额(刷卡金额)

        public decimal decMz医保卡银行账户冲减金额
        {
            get { return _decMz账户冲减金额; }
            set { _decMz账户冲减金额 = value; }
        }

        private decimal _decMz本地计算自负;

        public decimal decMz本地计算自负
        {
            get { return _decMz本地计算自负; }
            set { _decMz本地计算自负 = value; }
        }

        private decimal _decMz个人自负 = 0;//账户冲减时个人自付部分

        public decimal decMz医保返回个人自负
        {
            get { return _decMz个人自负; }
            set { _decMz个人自负 = value; }
        }
        private decimal _decMz冲减后余额 = 0;//计算账户冲减后余额

        public decimal decMz冲减后余额
        {
            get { return _decMz冲减后余额; }
            set { _decMz冲减后余额 = value; }
        }
        private string _sMz医保挂号流水号;//门诊医保登记（挂号）返回流水号


        public string sMz医保挂号流水号
        {
            get { return _sMz医保挂号流水号; }
            set { _sMz医保挂号流水号 = value; }
        }

        private string _s医院流水号;//医院his流水号

        public string S医院流水号
        {
            get { return _s医院流水号; }
            set { _s医院流水号 = value; }
        }
        //private string _sMz医院单据号;

        //public string sMz医院单据号
        //{
        //    get { return _sMz医院单据号; }
        //    set { _sMz医院单据号 = value; }
        //}
        private string _sMz登记次数 = "0";

        public string sMz登记次数
        {
            get { return _sMz登记次数; }
            set { _sMz登记次数 = value; }
        }
        private string _s门诊医保结算流水号;//医保结算流水号

        public string s门诊医保结算流水号
        {
            get { return _s门诊医保结算流水号; }
            set { _s门诊医保结算流水号 = value; }
        }
        private string _mzYbjsQxLsh;//医保结算取消流水号

        public string mzYbjsQxLsh
        {
            get { return _mzYbjsQxLsh; }
            set { _mzYbjsQxLsh = value; }
        }
        private string _mz银行消费流水号 = "";

        public string S银行消费流水号
        {
            get { return _mz银行消费流水号; }
            set { _mz银行消费流水号 = value; }
        }
        private DateTime _dtMz刷卡时间;//门诊刷卡时间

        public DateTime dtMz刷卡时间
        {
            get { return _dtMz刷卡时间; }
            set { _dtMz刷卡时间 = value; }
        }
        private string _S交易索引号;//sldll改造增加 20140716

        public string S交易索引号
        {
            get { return _S交易索引号; }
            set { _S交易索引号 = value; }
        }
        private string _SPOS流水号 = "";//sldll改造增加 20140716,三代卡银行消费时返回

        public string SPOS流水号
        {
            get { return _SPOS流水号; }
            set { _SPOS流水号 = value; }
        }
        private string _S参考号;//sldll改造增加 20140716,隔日退货时用到

        public string S参考号
        {
            get { return _S参考号; }
            set { _S参考号 = value; }
        }
        private string _S银行余额;//sldll改造增加 20140716

        public string S银行余额
        {
            get { return _S银行余额; }
            set { _S银行余额 = value; }
        }
        private string _S终端号;//sldll改造增加 20140716

        public string S终端号
        {
            get { return _S终端号; }
            set { _S终端号 = value; }
        }
        private string _S商户号;//sldll改造增加 20140716

        public string S商户号
        {
            get { return _S商户号; }
            set { _S商户号 = value; }
        }
        private string _S原交易日期;//sldll改造增加 20140922

        public string S原交易日期
        {
            get { return _S原交易日期; }
            set { _S原交易日期 = value; }
        }

        //-------------从返回字符串中读取的
        //private decimal _reFymxZfje;//自费金额

        //public decimal reFymxZfje
        //{
        //    get { return _reFymxZfje; }
        //    set { _reFymxZfje = value; }
        //}
        //private decimal _reFymxZlje;//自理金额

        //public decimal reDec自理金额
        //{
        //    get { return _reFymxZlje; }
        //    set { _reFymxZlje = value; }
        //}
        //private Boolean _reSccg;//上传成功

        //public Boolean reB上传成功
        //{
        //    get { return _reSccg; }
        //    set { _reSccg = value; }
        //}
        //private string _reJylsh;//交易流水号

        //public string reS交易流水号
        //{
        //    get { return _reJylsh; }
        //    set { _reJylsh = value; }
        //}
        private string _reZyYbghLsh;//住院医保挂号流水号

        public string reS住院医保挂号流水号
        {
            get { return _reZyYbghLsh; }
            set { _reZyYbghLsh = value; }
        }


        private string _s住院结算使用账户标志 = "0";

        public string S住院结算使用账户标志
        {
            get { return _s住院结算使用账户标志; }
            set { _s住院结算使用账户标志 = value; }
        }
        //住院上传

        private string _sMZ流水号;

        public string SMZ医院流水号
        {
            get { return _sMZ流水号; }
            set { _sMZ流水号 = value; }
        }

        private string _sZy医院流水号;//住院费用上传流水号，由zy+住院号+登记次数   //住院或门诊流水号(33103)

        public string sZy医院流水号
        {
            get { return _sZy医院流水号; }
            set { _sZy医院流水号 = value; }
        }
        private string _sZy住院号;//住院号

        public string sZy住院号
        {
            get { return _sZy住院号; }
            set { _sZy住院号 = value; }
        }
        private string _sZy单据号;

        public string sZy单据号
        {
            get { return _sZy单据号; }
            set { _sZy单据号 = value; }
        }
        private string _sZy医疗类别编码;//医疗类别，用于住院报销业务   11普通门诊 14 账户顶现金 15 门诊慢性病21普通住院 44 生育住院

        public string sZy医疗类别编码
        {
            get { return _sZy医疗类别编码; }
            set { _sZy医疗类别编码 = value; }
        }
        private string _zyYllbName;

        public string s医疗类别名称
        {
            get { return _zyYllbName; }
            set { _zyYllbName = value; }
        }
        private string _sZy科室;//住院科室

        public string sZy科室
        {
            get { return _sZy科室; }
            set { _sZy科室 = value; }
        }

        private DateTime _dtt医保结算时间;

        public DateTime dtt医保结算时间
        {
            get { return _dtt医保结算时间; }
            set { _dtt医保结算时间 = value; }
        }

        private DateTime _zyRyrq;//入院日期

        public DateTime dtt入院日期
        {
            get { return _zyRyrq; }
            set { _zyRyrq = value; }
        }

        private DateTime _zyCyrq;//出院日期

        public DateTime dtt出院日期
        {
            get { return _zyCyrq; }
            set { _zyCyrq = value; }
        }
        private string _zyRyzdName;//入院诊断名

        public string s入院诊断名称
        {
            get { return _zyRyzdName; }
            set { _zyRyzdName = value; }
        }
        private string _zyCyzdName;//出院诊断名

        public string s出院诊断名称
        {
            get { return _zyCyzdName; }
            set { _zyCyzdName = value; }
        }
        private string _sZy出院诊断疾病编码;//出院诊断疾病编码

        public string sZy出院诊断疾病编码
        {
            get { return _sZy出院诊断疾病编码; }
            set { _sZy出院诊断疾病编码 = value; }
        }
        private string _sZy出院辅诊断编码;//出院辅诊断编码

        public string sZy出院辅诊断编码
        {
            get { return _sZy出院辅诊断编码; }
            set { _sZy出院辅诊断编码 = value; }
        }
        private string _sZy出院原因;//出院原因 

        public string sZy出院原因
        {
            get { return _sZy出院原因; }
            set { _sZy出院原因 = value; }
        }



        //门诊&住院都返回数据
        private decimal _reDec统筹支付 = 0;

        public decimal reDec统筹支付
        {
            get { return _reDec统筹支付; }
            set { _reDec统筹支付 = value; }
        }

        #region 住院报销返回数据

        private string _sReZy交易时间;

        public string SReZy交易时间
        {
            get { return _sReZy交易时间; }
            set { _sReZy交易时间 = value; }
        }
        private decimal _dec大病合规费用;

        public decimal decReZy大病合规费用
        {
            get { return _dec大病合规费用; }
            set { _dec大病合规费用 = value; }
        }
        private decimal _decReZy大病报销_原救助金支出金额;//救助金支出金额

        private decimal _decReZy本年进入大额费用;

        public decimal decReZy本年进入大额费用
        {
            get { return _decReZy本年进入大额费用; }
            set { _decReZy本年进入大额费用 = value; }
        }

        private decimal _decReZy本年救助金支出累计;

        public decimal decReZy本年救助金支出累计
        {
            get { return _decReZy本年救助金支出累计; }
            set { _decReZy本年救助金支出累计 = value; }
        }

        public decimal decReZy大病报销_原救助金支出金额
        {
            get { return _decReZy大病报销_原救助金支出金额; }
            set { _decReZy大病报销_原救助金支出金额 = value; }
        }
        public decimal decReZy优抚对象民政补助; //涉军政府补助(优抚对象民政补助)
        private decimal _decReZy个人现金支付金额; //个人现金支付

        public decimal decReZy个人现金支付金额
        {
            get { return _decReZy个人现金支付金额; }
            set { _decReZy个人现金支付金额 = value; }
        }
        private decimal _reZy公务员补助;

        public decimal decReZy公务员补助
        {
            get { return _reZy公务员补助; }
            set { _reZy公务员补助 = value; }
        }
        private decimal _decReZy分档自理金额;

        public decimal decReZy分档自理金额
        {
            get { return _decReZy分档自理金额; }
            set { _decReZy分档自理金额 = value; }
        }

        private decimal _decReZy起伏线支付;

        //public decimal DecReZy起付线支付
        //{
        //    get { return _decReZy起伏线支付; }
        //    set { _decReZy起伏线支付 = value; }
        //}
        private decimal _decReZy起付线 = 0;

        public decimal decReZy起付线
        {
            get { return _decReZy起付线; }
            set { _decReZy起付线 = value; }
        }
        private int _iReZy本年住院次数累计;//本年住院次数   

        public int iReZy本年住院次数累计
        {
            get { return _iReZy本年住院次数累计; }
            set { _iReZy本年住院次数累计 = value; }
        }
        private string sReZy年度;

        public string SReZy年度
        {
            get { return sReZy年度; }
            set { sReZy年度 = value; }
        }
        private string _sReZy行政区划;

        public string SReZy行政区划
        {
            get { return _sReZy行政区划; }
            set { _sReZy行政区划 = value; }
        }

        private decimal _dec超限价自付费用 = 0;

        private decimal _decReZy起付线支付;

        public decimal decReZy起付线支付
        {
            get { return _decReZy起付线支付; }
            set { _decReZy起付线支付 = value; }
        }
        private decimal _decReZy救助金额;

        public decimal decReZy救助金额
        {
            get { return _decReZy救助金额; }
            set { _decReZy救助金额 = value; }
        }

        private decimal _decReZy救助金封顶上金额;

        public decimal decReZy救助金封顶上金额
        {
            get { return _decReZy救助金封顶上金额; }
            set { _decReZy救助金封顶上金额 = value; }
        }

        private decimal _decReZy个人账户支出 = 0;//个人账户支出

        public decimal decReZy个人账户支出
        {
            get { return _decReZy个人账户支出; }
            set { _decReZy个人账户支出 = value; }
        }
        public decimal decReZy医疗费总额 = 0;//医疗费总额
        private string _sReZy医保结算流水号 = "";//住院结算流水号

        public string sReZy医保结算流水号
        {
            get { return _sReZy医保结算流水号; }
            set { _sReZy医保结算流水号 = value; }
        }

        public decimal Dec超限价自付费用
        {
            get
            {
                return _dec超限价自付费用;
            }

            set
            {
                _dec超限价自付费用 = value;
            }
        }

        public decimal decReZy个人自费金额;//         个人自费金额  
        public decimal decReZy超过封顶线个人自付金额;//超过封顶线个人自付金额
        public decimal decReZy本次进入统筹金额 = 0;//本次进入统筹金额   
        public decimal decReZy中心支付;//中心支付
        public decimal decReZy跨区住院未审批个人负担金额;
        #endregion


        public void get人员类别名称(string ylrylb)
        {
            switch (ylrylb)
            {

                case "11":
                    s人员类别名称 = "城镇职工|在职";
                    break;
                case "12":
                    s人员类别名称 = "城镇职工|在职公务员";
                    break;
                case "21":
                    s人员类别名称 = "城镇职工|退休";
                    break;
                case "22":
                    s人员类别名称 = "城镇职工|退休公务员";
                    break;
                case "31":
                    s人员类别名称 = "城镇职工|离休";
                    break;
                case "32":
                    s人员类别名称 = "城镇职工|离休异地安置";
                    break;
                case "33"://
                    s人员类别名称 = "城镇职工|二等乙级以上革命伤残军人";
                    break;
                case "34"://
                    s人员类别名称 = "城镇职工|二等乙级以上革命伤残军人异地安置";
                    break;
                case "91"://
                    s人员类别名称 = "城镇职工|其它";
                    break;
                default:
                    s人员类别名称 = "城乡居民";
                    break;
            }
            string s医保类型 = WEISHENG.COMM.stringHelper.mySplit(S人员类别名称, 0);
            En医保类型 = (HIS.COMM.ClassEnum.en收款类型)Enum.Parse(typeof(HIS.COMM.ClassEnum.en收款类型), s医保类型);
        }

        public bool getInfoFromAccount()
        {
            bool re = true;
            try
            {
                //lmh cichuyaogai
                string temp = sAccount.Replace("||", "|0|").Replace("||", "|0|").Replace("||", "|0|").Replace("||", "|0|");

                decAccount账户余额 = Convert.ToDecimal(stringHelper.mySplit(temp, 5));
                decAccount统筹支出累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 3));
                decAccount救助金支出累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 4));
                decAccount转出医院起付标准 = Convert.ToDecimal(stringHelper.mySplit(temp, 6));
                decAccount转出医院起付标准自付 = Convert.ToDecimal(stringHelper.mySplit(temp, 7));

                decAccount本年度住院次数 = Convert.ToDecimal(stringHelper.mySplit(temp, 8));
                decAccount账户支出累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 9));
                decAccount门诊大病统筹支出累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 10));
                decAccount门诊慢性病统筹支出累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 11));
                decAccount个人现金支出累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 12));
                decAccount乙类自理费用累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 13));
                decAccount公务员补助支出累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 14));
                decAccount个人自费费用累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 15));
                decAccount进入统筹费用累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 16));
                decAccount第一次住院医院等级 = Convert.ToDecimal(stringHelper.mySplit(temp, 17));
                decAccount第一次住院起付标准自付 = Convert.ToDecimal(stringHelper.mySplit(temp, 18));
                decAccount第二次住院医院等级 = Convert.ToDecimal(stringHelper.mySplit(temp, 19));
                decAccount第二次住院起付标准自付 = Convert.ToDecimal(stringHelper.mySplit(temp, 20));
                decAccount三次以上住院起付标准自付累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 21));
                decAccount门诊慢性病起付标准自付累计 = Convert.ToDecimal(stringHelper.mySplit(temp, 22));
            }
            catch (Exception ex)
            {
                sbErrInfo.Append("\r\n转换账户信息出错：" + ex.Message);
                re = false;
                //   ybBusiness.logInterfaceInfotoDB("读取账户余额失败", "getZhyeFromAccount", ex.ToString());
            }
            return re;
        }



        /// <summary>
        /// 门诊账户冲减算法
        /// </summary>
        /// <param name="_zhcjje"></param>
        public void v门诊账户冲减算法(decimal _zhcjje)
        {
            decMz医保卡银行账户冲减金额 = _zhcjje;
            decMz冲减后余额 = decAccount账户余额 - _zhcjje;
        }

        public void v更新冲减后余额(ref personYB _person)
        {
            if (_person.B无卡交易)
            {
                decMz冲减后余额 = decAccount账户余额 - reDec统筹支付;
            }
            else
            {
                decMz冲减后余额 = decAccount账户余额 - decMz医保卡银行账户冲减金额;
            }
        }


        //新利驱动从读卡器读取信息
        public string updateDay;
        public string others1;
        public string others2;
        public string others3;
        public string outCardType = "";  //2014年07月，O=老卡;   P=新卡（三代卡，支持银联功能）




        public DataTable dt上传费用 = new DataTable("dt上传费用");
        public personYB()
        {
            //dt费用明细.Columns.Add("药品序号", Type.GetType("System.Decimal"));
            //dt费用明细.Columns.Add("费用序号", Type.GetType("System.Int32"));
            //dt费用明细.Columns.Add("费用名称", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("规格", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("单位", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("数量", Type.GetType("System.Decimal"));
            //dt费用明细.Columns.Add("单价", Type.GetType("System.Decimal"));
            //dt费用明细.Columns.Add("金额", Type.GetType("System.Decimal"));
            //dt费用明细.Columns.Add("YPID", Type.GetType("System.Decimal"));
            //dt费用明细.Columns.Add("进价", Type.GetType("System.Decimal"));
            //dt费用明细.Columns.Add("产地", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("批号", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("是否报销", Type.GetType("System.Int32"));
            //dt费用明细.Columns.Add("组别", Type.GetType("System.Int32"));
            //dt费用明细.Columns.Add("进价金额", Type.GetType("System.Decimal"));
            //dt费用明细.Columns["进价金额"].Expression = "数量*进价";
            //dt费用明细.Columns.Add("医保编码", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("医保名称", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("交易流水号", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("自理金额", Type.GetType("System.Decimal"));
            //dt费用明细.Columns.Add("自费金额", Type.GetType("System.Decimal"));
            //dt费用明细.Columns.Add("是否上传", Type.GetType("System.Int32"));
            //dt费用明细.Columns.Add("YB_项目大类", Type.GetType("System.String"));
            //dt费用明细.Columns.Add("YB_项目收费类别代码", Type.GetType("System.String"));
            //dt费用明细.Constraints.Add(new global::System.Data.UniqueConstraint("Constraint1", new global::System.Data.DataColumn[] {
            //                   dt费用明细.Columns["组别"],
            //                   dt费用明细.Columns["费用序号"],
            //                   dt费用明细.Columns["YPID"]}, true));            
        }

    }


    //todo:困扰了好几天的问题，用下面这种方式可以方便分层。
    public struct _stru领款人
    {
        public string _s姓名;
        public string _s电话;
        public string _s亲属关系;
    }



}
