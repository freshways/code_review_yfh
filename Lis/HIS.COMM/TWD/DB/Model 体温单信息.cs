﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM.TWD
{
    /// <summary>
    /// 体温单实体类
    /// </summary>
    [Serializable]
    public class Model体温单信息
    {
        public string ZYID { get; set; }
        public string 病人姓名 { get; set; }
        public string 性别 { get; set; }
        public string 年龄 { get; set; }
        public string 病床 { get; set; }
        public string 科室名称 { get; set; }
        public string 测量日期 { get; set; }
        public string 测量时间 { get; set; }
        public string 体温 { get; set; }
        public string 物理降温 { get; set; }
        public string 脉搏 { get; set; }
        public string 呼吸 { get; set; }
        public string 大便 { get; set; }
        public string 小便 { get; set; }
        public string 液入量 { get; set; }
        public string 血压AM { get; set; }
        public string 呕吐量 { get; set; }
        public string 体重 { get; set; }
        public string 痰量 { get; set; }
        public string 引流 { get; set; }
        public string 标注文字 { get; set; }
    }
}
