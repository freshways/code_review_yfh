using System;

namespace HIS.COMM.TWD
{
	/// <summary>
	/// 允许键盘输入指定的字符
	/// </summary>
	public abstract class PermitKey
	{
		/// <summary>
		/// 允许输入小数
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void PermitPID(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			try
			{
				byte kc=Convert.ToByte(e.KeyChar);
				if ((kc>=48 && kc<=57) || kc==120 || kc==8 || kc==13)
					e.Handled=false;
				else
					e.Handled=true;
			}
			catch (System.Exception ex)
			{
				/*throw ex;*/
			}
		}

		/// <summary>
		/// 允许输入小数
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void PermitPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			try
			{
				byte kc=Convert.ToByte(e.KeyChar);
				if ((kc>=48 && kc<=57) || kc==47 || kc==8 || kc==13)
					e.Handled=false;
				else
					e.Handled=true;
			}
			catch (System.Exception ex)
			{
				/*throw ex;*/
			}
		}
		/// <summary>
		/// 允许输入小数
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void PermitFloat(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			try
			{
				byte kc=Convert.ToByte(e.KeyChar);
				if ((kc>=48 && kc<=57) || kc==46 || kc==8 || kc==13)
					e.Handled=false;
				else
					e.Handled=true;
			}
			catch (System.Exception ex)
			{
				/*throw ex;*/
			}
		}
		/// <summary>
		/// 允许输入数字和字母
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void PermitDigitAndChar(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			try
			{
				byte kc=Convert.ToByte(e.KeyChar);
				if ((kc>=48 && kc<=57) || (kc>=65 && kc<=92) || (kc>=97 && kc<=122) || kc==8 || kc==8)
					e.Handled=false;
				else
					e.Handled=true;
			}
			catch (System.Exception ex)
			{
				/*throw ex;*/
			}
		}
		/// <summary>
		/// 允许输入整数
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void v允许输入整数(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			try
			{
				byte kc=Convert.ToByte(e.KeyChar);
				if ((kc>=48 && kc<=57) || kc==8 || kc==8)
					e.Handled=false;
				else
					e.Handled=true;
			}
			catch (System.Exception ex)
			{
				/*throw ex;*/
			}
		}
		/// <summary>
		/// 允许输入大写、小写字母
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void v允许数据字母(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			try
			{
				byte kc=Convert.ToByte(e.KeyChar);
				if ((kc>=65 && kc<=92) || (kc>=97 && kc<=122) || kc==8 || kc==8)
					e.Handled=false;
				else
					e.Handled=true;
			}
			catch (System.Exception ex)
			{
				/*throw ex;*/
			}
		}
	}
}
