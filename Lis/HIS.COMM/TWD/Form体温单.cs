﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace HIS.COMM.TWD
{
    public partial class Form体温单 : Form
    {
        private XtraForm批量体温 parent;
        public Form体温单()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 医院名称
        /// </summary>
        public string 医院名称
        { get; set; }
        /// <summary>
        /// 更新登陆者
        /// </summary>
        public string 当前用户 { get; set; }
        /// <summary>
        /// 住院号
        /// </summary>
        public string sZYID { get; set; }

        /// <summary>
        /// 在院状态
        /// </summary>
        //public string 在院状态 { get; set; }

        public bool CanEdit { get; set; }
        private void Form1_Load(object sender, EventArgs e)
        {
            医院名称 = HIS.COMM.zdInfo.Model单位信息.sDwmc;
            uC_BPT1.MainTitle = 医院名称;
            uC_BPT1.s当前用户 = 当前用户;
            uC_BPT1.ZYID = sZYID;
            uC_BPT1.CanEdit = CanEdit;
            uC_BPT1.Load();
        }
    }
}