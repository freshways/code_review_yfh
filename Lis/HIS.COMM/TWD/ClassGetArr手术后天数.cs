﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HIS.COMM.TWD
{
    public static class ClassGet手术分娩后天数
    {
        public static DateTime getDtt分娩日期(DataTable dt三测数据)
        {
            DateTime dtt = DateTime.Parse("1900-01-01");
            foreach (DataRow row in dt三测数据.Rows)
            {
                if (row["标注文字"].ToString().Contains("分娩"))
                {
                    dtt = DateTime.Parse(row["测量日期"].ToString());
                }
            }
            return dtt;
        }

        public static ArrayList getArr手术日期(DateTime dtt表格开始日期, DataTable dt三测数据)
        {
            //查找手术日期
            ArrayList arr手术日期 = new ArrayList();
            foreach (DataRow row in dt三测数据.Rows)
            {
                if (row["标注文字"].ToString().Contains("手术"))
                {
                    arr手术日期.Add(row["测量日期"].ToString());
                }
            }
            //分析显示内容
            ArrayList arr显示术后天数 = new ArrayList();
            int i手术次数 = 1;
            bool b是否手术当天 = false;
            foreach (var _s手术日期 in arr手术日期)
            {
                DateTime _dtt表格当前日期 = dtt表格开始日期;
                for (int i = 0; i < 7; i++)//循环表格天数
                {
                    TimeSpan ts手术后天数 = _dtt表格当前日期.Date - DateTime.Parse(_s手术日期.ToString()).Date;
                    //string _s术后天数 = "";
                    if (i手术次数 == 1)//分析第一次手术时，创建数组
                    {
                        if (ts手术后天数.Days >= 0)
                        {
                            arr显示术后天数.Add(ts手术后天数.Days.ToString());
                        }
                        else
                        {
                            arr显示术后天数.Add("");
                        }
                    }
                    else//多次手术时，更新数组
                    {
                        arr显示术后天数[i] = _s多次手术显示(i手术次数, DateTime.Parse(_s手术日期.ToString()), _dtt表格当前日期, i, arr显示术后天数[i].ToString());
                    }

                    _dtt表格当前日期 = _dtt表格当前日期.AddDays(1);
                }
                i手术次数++;
            }

            return arr显示术后天数;
        }
        private static string _s多次手术显示(int _i手术次数, DateTime dtt手术日期, DateTime dtt表格第几天, int _i表格第几天, string _s当前术后显示)
        {
            string re = "";
            //bool b是否手术当天 = false;
            //if (dtt表格第几天.Date == dtt手术日期.Date)//判断是否手术当天
            //{
            //    b是否手术当天 = true;
            //}
            //else
            //{
            //    b是否手术当天 = false;
            //}
            TimeSpan ts本次术后第几天 = dtt表格第几天 - dtt手术日期;
            int _i本次术后第几天 = ts本次术后第几天.Days;

            switch (_i手术次数)
            {
                case 2:
                    switch (_i本次术后第几天)
                    {
                        case -1:
                        case -2:
                        case -3:
                        case -4:
                        case -5:
                        case -6:
                            re = _s当前术后显示;
                            break;
                        case 0:
                            re = _s当前术后显示 + "(" + _i手术次数.ToString() + ")";//list.Add("2(2)");
                            break;
                        default:
                            re = _i本次术后第几天.ToString() + "/" + _s当前术后显示; //list.Add("1/3");
                            break;
                    }
                    break;
                case 3:
                default:
                    break;
            }
            return re;
            //list.Add("0");
            //list.Add("1");
            //list.Add("2(2)");
            //list.Add("1/3");
            //list.Add("2/4");
            //list.Add("3/5");
            //list.Add("4/6");
        }
    }
}
