﻿namespace HIS.COMM.TWD
{
    partial class XtraForm批量体温
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm批量体温));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton查询病人 = new DevExpress.XtraEditors.SimpleButton();
            this.txt住院号 = new DevExpress.XtraEditors.ButtonEdit();
            this.lookUpEdit病区选择 = new DevExpress.XtraEditors.LookUpEdit();
            this.btn取消 = new DevExpress.XtraEditors.SimpleButton();
            this.grd病人信息 = new DevExpress.XtraGrid.GridControl();
            this.grdv病人信息 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ZYID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.住院号码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.病人姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.年龄 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.病床 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.测量日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit测量日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox测量时间 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.体温 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.物理降温 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.脉搏 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.呼吸 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.大便 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.小便 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.液入量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.血压AM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.呕吐量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.体重 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.痰量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.引流 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.标注文字 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit事件设置 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.在院状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.com测量时间 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dta测量日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt住院号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit病区选择.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd病人信息)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdv病人信息)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit测量日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit测量日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox测量时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit事件设置)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.com测量时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dta测量日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dta测量日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.simpleButton查询病人);
            this.layoutControl2.Controls.Add(this.txt住院号);
            this.layoutControl2.Controls.Add(this.lookUpEdit病区选择);
            this.layoutControl2.Controls.Add(this.btn取消);
            this.layoutControl2.Controls.Add(this.grd病人信息);
            this.layoutControl2.Controls.Add(this.btn保存);
            this.layoutControl2.Controls.Add(this.com测量时间);
            this.layoutControl2.Controls.Add(this.dta测量日期);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(-830, 191, 650, 645);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1060, 482);
            this.layoutControl2.TabIndex = 10;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // simpleButton查询病人
            // 
            this.simpleButton查询病人.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton查询病人.ImageOptions.Image")));
            this.simpleButton查询病人.Location = new System.Drawing.Point(860, 12);
            this.simpleButton查询病人.Name = "simpleButton查询病人";
            this.simpleButton查询病人.Size = new System.Drawing.Size(76, 22);
            this.simpleButton查询病人.StyleController = this.layoutControl2;
            this.simpleButton查询病人.TabIndex = 10;
            this.simpleButton查询病人.Text = "查询";
            this.simpleButton查询病人.Click += new System.EventHandler(this.simpleButton查询病人_Click);
            // 
            // txt住院号
            // 
            this.txt住院号.Location = new System.Drawing.Point(249, 12);
            this.txt住院号.Name = "txt住院号";
            this.txt住院号.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "选择", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.txt住院号.Size = new System.Drawing.Size(132, 21);
            this.txt住院号.StyleController = this.layoutControl2;
            this.txt住院号.TabIndex = 9;
            this.txt住院号.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            this.txt住院号.EditValueChanged += new System.EventHandler(this.txt住院号_EditValueChanged);
            // 
            // lookUpEdit病区选择
            // 
            this.lookUpEdit病区选择.Location = new System.Drawing.Point(67, 12);
            this.lookUpEdit病区选择.Name = "lookUpEdit病区选择";
            this.lookUpEdit病区选择.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit病区选择.Properties.NullText = "请选择...";
            this.lookUpEdit病区选择.Size = new System.Drawing.Size(123, 20);
            this.lookUpEdit病区选择.StyleController = this.layoutControl2;
            this.lookUpEdit病区选择.TabIndex = 8;
            this.lookUpEdit病区选择.EditValueChanged += new System.EventHandler(this.lookUpEdit病区选择_EditValueChanged);
            // 
            // btn取消
            // 
            this.btn取消.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn取消.ImageOptions.Image")));
            this.btn取消.Location = new System.Drawing.Point(996, 12);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(52, 22);
            this.btn取消.StyleController = this.layoutControl2;
            this.btn取消.TabIndex = 6;
            this.btn取消.Text = "取消";
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // grd病人信息
            // 
            this.grd病人信息.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grd病人信息.Location = new System.Drawing.Point(12, 38);
            this.grd病人信息.MainView = this.grdv病人信息;
            this.grd病人信息.Name = "grd病人信息";
            this.grd病人信息.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemButtonEdit事件设置,
            this.repositoryItemDateEdit测量日期,
            this.repositoryItemComboBox测量时间});
            this.grd病人信息.Size = new System.Drawing.Size(1036, 432);
            this.grd病人信息.TabIndex = 7;
            this.grd病人信息.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdv病人信息});
            this.grd病人信息.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.grd病人信息_MouseDoubleClick);
            // 
            // grdv病人信息
            // 
            this.grdv病人信息.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ZYID,
            this.住院号码,
            this.病人姓名,
            this.性别,
            this.年龄,
            this.病床,
            this.测量日期,
            this.时间,
            this.体温,
            this.物理降温,
            this.脉搏,
            this.呼吸,
            this.大便,
            this.小便,
            this.液入量,
            this.血压AM,
            this.呕吐量,
            this.体重,
            this.痰量,
            this.引流,
            this.标注文字,
            this.在院状态});
            this.grdv病人信息.GridControl = this.grd病人信息;
            this.grdv病人信息.Name = "grdv病人信息";
            this.grdv病人信息.OptionsView.ShowGroupPanel = false;
            this.grdv病人信息.FocusedColumnChanged += new DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventHandler(this.grdv病人信息_FocusedColumnChanged);
            // 
            // ZYID
            // 
            this.ZYID.Caption = "ZYID";
            this.ZYID.FieldName = "ZYID";
            this.ZYID.Name = "ZYID";
            this.ZYID.OptionsColumn.AllowEdit = false;
            this.ZYID.OptionsColumn.ReadOnly = true;
            this.ZYID.Width = 42;
            // 
            // 住院号码
            // 
            this.住院号码.Caption = "住院号码";
            this.住院号码.FieldName = "住院号码";
            this.住院号码.Name = "住院号码";
            this.住院号码.OptionsColumn.AllowEdit = false;
            this.住院号码.Visible = true;
            this.住院号码.VisibleIndex = 0;
            this.住院号码.Width = 42;
            // 
            // 病人姓名
            // 
            this.病人姓名.Caption = "病人姓名";
            this.病人姓名.FieldName = "病人姓名";
            this.病人姓名.Name = "病人姓名";
            this.病人姓名.OptionsColumn.AllowEdit = false;
            this.病人姓名.Visible = true;
            this.病人姓名.VisibleIndex = 1;
            this.病人姓名.Width = 42;
            // 
            // 性别
            // 
            this.性别.Caption = "性别";
            this.性别.FieldName = "性别";
            this.性别.Name = "性别";
            this.性别.OptionsColumn.AllowEdit = false;
            this.性别.Visible = true;
            this.性别.VisibleIndex = 2;
            this.性别.Width = 42;
            // 
            // 年龄
            // 
            this.年龄.Caption = "年龄";
            this.年龄.FieldName = "年龄";
            this.年龄.Name = "年龄";
            this.年龄.OptionsColumn.AllowEdit = false;
            this.年龄.Visible = true;
            this.年龄.VisibleIndex = 3;
            this.年龄.Width = 42;
            // 
            // 病床
            // 
            this.病床.Caption = "病床";
            this.病床.FieldName = "病床";
            this.病床.Name = "病床";
            this.病床.OptionsColumn.AllowEdit = false;
            this.病床.Visible = true;
            this.病床.VisibleIndex = 4;
            this.病床.Width = 42;
            // 
            // 测量日期
            // 
            this.测量日期.Caption = "测量日期";
            this.测量日期.ColumnEdit = this.repositoryItemDateEdit测量日期;
            this.测量日期.FieldName = "测量日期";
            this.测量日期.Name = "测量日期";
            this.测量日期.Visible = true;
            this.测量日期.VisibleIndex = 5;
            this.测量日期.Width = 42;
            // 
            // repositoryItemDateEdit测量日期
            // 
            this.repositoryItemDateEdit测量日期.AutoHeight = false;
            this.repositoryItemDateEdit测量日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit测量日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit测量日期.Name = "repositoryItemDateEdit测量日期";
            // 
            // 时间
            // 
            this.时间.Caption = "时间";
            this.时间.ColumnEdit = this.repositoryItemComboBox测量时间;
            this.时间.FieldName = "测量时间";
            this.时间.Name = "时间";
            this.时间.Visible = true;
            this.时间.VisibleIndex = 6;
            this.时间.Width = 42;
            // 
            // repositoryItemComboBox测量时间
            // 
            this.repositoryItemComboBox测量时间.AutoHeight = false;
            this.repositoryItemComboBox测量时间.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox测量时间.Items.AddRange(new object[] {
            "03",
            "07",
            "11",
            "15",
            "19",
            "23"});
            this.repositoryItemComboBox测量时间.Name = "repositoryItemComboBox测量时间";
            // 
            // 体温
            // 
            this.体温.Caption = "体温";
            this.体温.FieldName = "体温___";
            this.体温.Name = "体温";
            this.体温.Visible = true;
            this.体温.VisibleIndex = 7;
            this.体温.Width = 42;
            // 
            // 物理降温
            // 
            this.物理降温.Caption = "物理降温";
            this.物理降温.FieldName = "物理降温___";
            this.物理降温.Name = "物理降温";
            this.物理降温.Visible = true;
            this.物理降温.VisibleIndex = 8;
            this.物理降温.Width = 112;
            // 
            // 脉搏
            // 
            this.脉搏.Caption = "脉搏";
            this.脉搏.FieldName = "脉搏_次_分_";
            this.脉搏.Name = "脉搏";
            this.脉搏.Visible = true;
            this.脉搏.VisibleIndex = 9;
            this.脉搏.Width = 35;
            // 
            // 呼吸
            // 
            this.呼吸.Caption = "呼吸";
            this.呼吸.FieldName = "呼吸_次_分_";
            this.呼吸.Name = "呼吸";
            this.呼吸.Visible = true;
            this.呼吸.VisibleIndex = 10;
            this.呼吸.Width = 35;
            // 
            // 大便
            // 
            this.大便.Caption = "大便";
            this.大便.FieldName = "大便_次_";
            this.大便.Name = "大便";
            this.大便.Visible = true;
            this.大便.VisibleIndex = 11;
            this.大便.Width = 35;
            // 
            // 小便
            // 
            this.小便.Caption = "小便";
            this.小便.FieldName = "小便_ml_";
            this.小便.Name = "小便";
            this.小便.Visible = true;
            this.小便.VisibleIndex = 12;
            this.小便.Width = 35;
            // 
            // 液入量
            // 
            this.液入量.Caption = "液入量";
            this.液入量.FieldName = "液入量_ml_";
            this.液入量.Name = "液入量";
            this.液入量.Visible = true;
            this.液入量.VisibleIndex = 13;
            this.液入量.Width = 46;
            // 
            // 血压AM
            // 
            this.血压AM.Caption = "血压AM";
            this.血压AM.FieldName = "血压AM_mmHg_";
            this.血压AM.Name = "血压AM";
            this.血压AM.Visible = true;
            this.血压AM.VisibleIndex = 14;
            this.血压AM.Width = 63;
            // 
            // 呕吐量
            // 
            this.呕吐量.Caption = "呕吐量";
            this.呕吐量.FieldName = "呕吐量_ml_";
            this.呕吐量.Name = "呕吐量";
            this.呕吐量.Visible = true;
            this.呕吐量.VisibleIndex = 15;
            this.呕吐量.Width = 28;
            // 
            // 体重
            // 
            this.体重.Caption = "体重";
            this.体重.FieldName = "体重_Kg_";
            this.体重.Name = "体重";
            this.体重.Visible = true;
            this.体重.VisibleIndex = 16;
            this.体重.Width = 28;
            // 
            // 痰量
            // 
            this.痰量.Caption = "痰量";
            this.痰量.FieldName = "痰量_ml_";
            this.痰量.Name = "痰量";
            this.痰量.Visible = true;
            this.痰量.VisibleIndex = 17;
            this.痰量.Width = 28;
            // 
            // 引流
            // 
            this.引流.Caption = "引流";
            this.引流.FieldName = "引流量_ml_";
            this.引流.Name = "引流";
            this.引流.Visible = true;
            this.引流.VisibleIndex = 18;
            this.引流.Width = 28;
            // 
            // 标注文字
            // 
            this.标注文字.Caption = "标注文字";
            this.标注文字.ColumnEdit = this.repositoryItemButtonEdit事件设置;
            this.标注文字.FieldName = "标注文字";
            this.标注文字.Name = "标注文字";
            this.标注文字.Visible = true;
            this.标注文字.VisibleIndex = 19;
            this.标注文字.Width = 34;
            // 
            // repositoryItemButtonEdit事件设置
            // 
            this.repositoryItemButtonEdit事件设置.AutoHeight = false;
            this.repositoryItemButtonEdit事件设置.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.repositoryItemButtonEdit事件设置.Name = "repositoryItemButtonEdit事件设置";
            this.repositoryItemButtonEdit事件设置.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit事件设置_ButtonClick);
            // 
            // 在院状态
            // 
            this.在院状态.Caption = "在院状态";
            this.在院状态.FieldName = "在院状态";
            this.在院状态.Name = "在院状态";
            this.在院状态.OptionsColumn.AllowEdit = false;
            this.在院状态.Width = 91;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.DisplayFormat.FormatString = "t";
            this.repositoryItemTimeEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit1.EditFormat.FormatString = "t";
            this.repositoryItemTimeEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // btn保存
            // 
            this.btn保存.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.ImageOptions.Image")));
            this.btn保存.Location = new System.Drawing.Point(940, 12);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(52, 22);
            this.btn保存.StyleController = this.layoutControl2;
            this.btn保存.TabIndex = 6;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // com测量时间
            // 
            this.com测量时间.Location = new System.Drawing.Point(632, 12);
            this.com测量时间.Name = "com测量时间";
            this.com测量时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.com测量时间.Properties.Items.AddRange(new object[] {
            "03",
            "07",
            "11",
            "15",
            "19",
            "23"});
            this.com测量时间.Size = new System.Drawing.Size(117, 20);
            this.com测量时间.StyleController = this.layoutControl2;
            this.com测量时间.TabIndex = 4;
            // 
            // dta测量日期
            // 
            this.dta测量日期.EditValue = null;
            this.dta测量日期.Location = new System.Drawing.Point(440, 12);
            this.dta测量日期.Name = "dta测量日期";
            this.dta测量日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dta测量日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dta测量日期.Size = new System.Drawing.Size(133, 20);
            this.dta测量日期.StyleController = this.layoutControl2;
            this.dta测量日期.TabIndex = 2;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.emptySpaceItem3,
            this.layoutControlItem2,
            this.layoutControlItem9});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1060, 482);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lookUpEdit病区选择;
            this.layoutControlItem1.CustomizationFormText = "病区:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(182, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "病区:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.grd病人信息;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1040, 436);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btn取消;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(984, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(56, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(56, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(56, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btn保存;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(928, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(56, 26);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(56, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(56, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dta测量日期;
            this.layoutControlItem3.CustomizationFormText = "测量日期:";
            this.layoutControlItem3.Location = new System.Drawing.Point(373, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(192, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "测量日期:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.com测量时间;
            this.layoutControlItem5.CustomizationFormText = "测量时间:";
            this.layoutControlItem5.Location = new System.Drawing.Point(565, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(176, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(176, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(176, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "测量时间:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(52, 14);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(741, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(107, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt住院号;
            this.layoutControlItem2.CustomizationFormText = "住院号:";
            this.layoutControlItem2.Location = new System.Drawing.Point(182, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(191, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "住院号:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton查询病人;
            this.layoutControlItem9.Location = new System.Drawing.Point(848, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Location = new System.Drawing.Point(-164, 188);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(180, 120);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(180, 120);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem1.Location = new System.Drawing.Point(816, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem2";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // XtraForm批量体温
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1060, 482);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraForm批量体温";
            this.Text = "批量体温设置";
            this.Load += new System.EventHandler(this.XtraForm批量体温_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt住院号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit病区选择.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd病人信息)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdv病人信息)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit测量日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit测量日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox测量时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit事件设置)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.com测量时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dta测量日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dta测量日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.DateEdit dta测量日期;
        private DevExpress.XtraEditors.ComboBoxEdit com测量时间;
        private DevExpress.XtraGrid.GridControl grd病人信息;
        private DevExpress.XtraGrid.Views.Grid.GridView grdv病人信息;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn取消;
        private DevExpress.XtraGrid.Columns.GridColumn 住院号码;
        private DevExpress.XtraGrid.Columns.GridColumn 病人姓名;
        private DevExpress.XtraGrid.Columns.GridColumn 性别;
        private DevExpress.XtraGrid.Columns.GridColumn 年龄;
        private DevExpress.XtraGrid.Columns.GridColumn 病床;
        private DevExpress.XtraGrid.Columns.GridColumn 日期;
        private DevExpress.XtraGrid.Columns.GridColumn 时间;
        private DevExpress.XtraGrid.Columns.GridColumn 体温;
        private DevExpress.XtraGrid.Columns.GridColumn 脉搏;
        private DevExpress.XtraGrid.Columns.GridColumn 呼吸;
        private DevExpress.XtraGrid.Columns.GridColumn 大便;
        private DevExpress.XtraGrid.Columns.GridColumn 小便;
        private DevExpress.XtraGrid.Columns.GridColumn 液入量;
        private DevExpress.XtraGrid.Columns.GridColumn 血压AM;
        private DevExpress.XtraGrid.Columns.GridColumn 呕吐量;
        private DevExpress.XtraGrid.Columns.GridColumn 体重;
        private DevExpress.XtraGrid.Columns.GridColumn 痰量;
        private DevExpress.XtraGrid.Columns.GridColumn 标注文字;
        private DevExpress.XtraGrid.Columns.GridColumn 物理降温;
        private DevExpress.XtraGrid.Columns.GridColumn 引流;
        private DevExpress.XtraGrid.Columns.GridColumn ZYID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit事件设置;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit病区选择;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit测量日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox测量时间;
        private DevExpress.XtraGrid.Columns.GridColumn 在院状态;
        private DevExpress.XtraEditors.ButtonEdit txt住院号;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraGrid.Columns.GridColumn 测量日期;
        private DevExpress.XtraEditors.SimpleButton simpleButton查询病人;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}