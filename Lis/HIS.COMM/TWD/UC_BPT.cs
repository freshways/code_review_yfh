using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


namespace HIS.COMM.TWD
{
    /// <summary>
    /// UC_BPT 的摘要说明。
    /// </summary>
    public class UC_BPT : System.Windows.Forms.UserControl
    {
        private System.Windows.Forms.Panel panelBorder;
        private System.Windows.Forms.Panel panelDraw;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonPrew;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Label labelPage;
        private System.Windows.Forms.TextBox txt住院号;
        private System.Windows.Forms.TextBox txt年龄;
        private System.Windows.Forms.TextBox txt性别;
        private System.Windows.Forms.TextBox txt姓名;
        private System.Windows.Forms.TextBox txt床位;
        private System.Windows.Forms.TextBox txt住院日期;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Button buttonPrintAll;
        private Label label1;
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.Container components = null;

        public UC_BPT()
        {
            // 该调用是 Windows.Forms 窗体设计器所必需的。
            InitializeComponent();

            // TODO: 在 InitializeComponent 调用后添加任何初始化

        }

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码
        /// <summary>
        /// 设计器支持所需的方法 - 不要使用代码编辑器
        /// 修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBorder = new System.Windows.Forms.Panel();
            this.panelDraw = new System.Windows.Forms.Panel();
            this.txt年龄 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt住院号 = new System.Windows.Forms.TextBox();
            this.txt性别 = new System.Windows.Forms.TextBox();
            this.txt姓名 = new System.Windows.Forms.TextBox();
            this.txt床位 = new System.Windows.Forms.TextBox();
            this.txt住院日期 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPage = new System.Windows.Forms.Label();
            this.buttonPrintAll = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrew = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.panelBorder.SuspendLayout();
            this.panelDraw.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBorder
            // 
            this.panelBorder.BackColor = System.Drawing.SystemColors.Window;
            this.panelBorder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBorder.Controls.Add(this.panelDraw);
            this.panelBorder.Location = new System.Drawing.Point(30, 30);
            this.panelBorder.Name = "panelBorder";
            this.panelBorder.Size = new System.Drawing.Size(800, 1170);
            this.panelBorder.TabIndex = 0;
            // 
            // panelDraw
            // 
            this.panelDraw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelDraw.Controls.Add(this.txt年龄);
            this.panelDraw.Controls.Add(this.label1);
            this.panelDraw.Controls.Add(this.txt住院号);
            this.panelDraw.Controls.Add(this.txt性别);
            this.panelDraw.Controls.Add(this.txt姓名);
            this.panelDraw.Controls.Add(this.txt床位);
            this.panelDraw.Controls.Add(this.txt住院日期);
            this.panelDraw.Controls.Add(this.label8);
            this.panelDraw.Controls.Add(this.label6);
            this.panelDraw.Controls.Add(this.label5);
            this.panelDraw.Controls.Add(this.label4);
            this.panelDraw.Controls.Add(this.label3);
            this.panelDraw.Controls.Add(this.label2);
            this.panelDraw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDraw.Location = new System.Drawing.Point(0, 0);
            this.panelDraw.Name = "panelDraw";
            this.panelDraw.Size = new System.Drawing.Size(798, 1168);
            this.panelDraw.TabIndex = 0;
            this.panelDraw.Paint += new System.Windows.Forms.PaintEventHandler(this.panelDraw_Paint);
            this.panelDraw.DoubleClick += new System.EventHandler(this.panelDraw_DoubleClick);
            // 
            // txt年龄
            // 
            this.txt年龄.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt年龄.Location = new System.Drawing.Point(534, 112);
            this.txt年龄.Name = "txt年龄";
            this.txt年龄.ReadOnly = true;
            this.txt年龄.Size = new System.Drawing.Size(25, 14);
            this.txt年龄.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(553, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 16);
            this.label1.TabIndex = 25;
            this.label1.Text = "岁";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt住院号
            // 
            this.txt住院号.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt住院号.Location = new System.Drawing.Point(650, 112);
            this.txt住院号.Name = "txt住院号";
            this.txt住院号.ReadOnly = true;
            this.txt住院号.Size = new System.Drawing.Size(96, 14);
            this.txt住院号.TabIndex = 11;
            // 
            // txt性别
            // 
            this.txt性别.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt性别.Location = new System.Drawing.Point(446, 112);
            this.txt性别.Name = "txt性别";
            this.txt性别.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(48, 14);
            this.txt性别.TabIndex = 16;
            // 
            // txt姓名
            // 
            this.txt姓名.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt姓名.Location = new System.Drawing.Point(342, 112);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(66, 14);
            this.txt姓名.TabIndex = 14;
            // 
            // txt床位
            // 
            this.txt床位.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt床位.Location = new System.Drawing.Point(236, 112);
            this.txt床位.Name = "txt床位";
            this.txt床位.ReadOnly = true;
            this.txt床位.Size = new System.Drawing.Size(62, 14);
            this.txt床位.TabIndex = 13;
            // 
            // txt住院日期
            // 
            this.txt住院日期.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt住院日期.Location = new System.Drawing.Point(103, 112);
            this.txt住院日期.Name = "txt住院日期";
            this.txt住院日期.ReadOnly = true;
            this.txt住院日期.Size = new System.Drawing.Size(81, 14);
            this.txt住院日期.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(596, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 16);
            this.label8.TabIndex = 24;
            this.label8.Text = "住院号号";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(502, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 16);
            this.label6.TabIndex = 22;
            this.label6.Text = "年龄";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(414, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "性别";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(304, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 16);
            this.label4.TabIndex = 19;
            this.label4.Text = "姓名";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(204, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "床位";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(43, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "住院日期";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPage
            // 
            this.labelPage.AutoSize = true;
            this.labelPage.BackColor = System.Drawing.SystemColors.Control;
            this.labelPage.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelPage.Location = new System.Drawing.Point(478, 6);
            this.labelPage.Name = "labelPage";
            this.labelPage.Size = new System.Drawing.Size(0, 14);
            this.labelPage.TabIndex = 18;
            this.labelPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonPrintAll
            // 
            this.buttonPrintAll.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPrintAll.Location = new System.Drawing.Point(349, 3);
            this.buttonPrintAll.Name = "buttonPrintAll";
            this.buttonPrintAll.Size = new System.Drawing.Size(75, 23);
            this.buttonPrintAll.TabIndex = 19;
            this.buttonPrintAll.Text = "全部打印";
            this.buttonPrintAll.UseVisualStyleBackColor = false;
            this.buttonPrintAll.Click += new System.EventHandler(this.buttonS_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPrint.Location = new System.Drawing.Point(253, 3);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(75, 23);
            this.buttonPrint.TabIndex = 16;
            this.buttonPrint.Text = "打印";
            this.buttonPrint.UseVisualStyleBackColor = false;
            this.buttonPrint.Click += new System.EventHandler(this.buttonS_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.BackColor = System.Drawing.SystemColors.Control;
            this.buttonNext.Location = new System.Drawing.Point(155, 3);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 15;
            this.buttonNext.Text = "下一页";
            this.buttonNext.UseVisualStyleBackColor = false;
            this.buttonNext.Click += new System.EventHandler(this.buttonS_Click);
            // 
            // buttonPrew
            // 
            this.buttonPrew.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPrew.Location = new System.Drawing.Point(61, 3);
            this.buttonPrew.Name = "buttonPrew";
            this.buttonPrew.Size = new System.Drawing.Size(75, 23);
            this.buttonPrew.TabIndex = 14;
            this.buttonPrew.Text = "上一页";
            this.buttonPrew.UseVisualStyleBackColor = false;
            this.buttonPrew.Click += new System.EventHandler(this.buttonS_Click);
            // 
            // UC_BPT
            // 
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 30);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Controls.Add(this.labelPage);
            this.Controls.Add(this.panelBorder);
            this.Controls.Add(this.buttonPrintAll);
            this.Controls.Add(this.buttonPrew);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.buttonPrint);
            this.Name = "UC_BPT";
            this.Size = new System.Drawing.Size(860, 1250);
            this.SizeChanged += new System.EventHandler(this.UC_BPT_SizeChanged);
            this.panelBorder.ResumeLayout(false);
            this.panelDraw.ResumeLayout(false);
            this.panelDraw.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        #region 自定义属性
        private bool canEdit = false;
        public bool CanEdit
        {
            get { return canEdit; }
            set { canEdit = value; }
        }
        private string _sZYID = "";
        /// <summary>
        /// 病案号
        /// </summary>
        public string ZYID
        {
            get { return _sZYID; }
            set { _sZYID = value; }
        }
        private string pageType = "MR";
        /// <summary>
        /// 纸张格式
        /// MR：病历纸，A4
        /// </summary>
        public string PageType
        {
            get { return pageType; }
            set { pageType = value; }
        }
        private string mainTitle = "";
        /// <summary>
        /// 主标题
        /// </summary>
        public string MainTitle
        {
            get { return mainTitle; }
            set { mainTitle = value; }
        }
        private string secondTitle = "体 温 记 录 单";
        /// <summary>
        /// 副标题
        /// </summary>
        public string SecondTitle
        {
            get { return secondTitle; }
            set { secondTitle = value; }
        }
        private Point startPoint = new Point(50, 130);
        /// <summary>
        /// 起点
        /// </summary>
        public Point StartPoint
        {
            get { return startPoint; }
            set { startPoint = value; }
        }
        private int bottomLimitValue = 1070;//1110;
        /// <summary>
        /// 向下延伸的边界值(就是最后一条行线的Y值不能超过350)
        /// </summary>
        public int BottomLimitValue
        {
            get { return this.bottomLimitValue; }
            set { this.bottomLimitValue = value; }
        }

        private int rightLimitValue = 750;
        /// <summary>
        /// 向右延伸的边界值（最后一条列线的X值不能超过600）
        /// </summary>
        public int RightLimitValue
        {
            get { return this.rightLimitValue; }
            set { this.rightLimitValue = value; }
        }

        /// <summary>
        /// 数据文件路径
        /// </summary>
        private string bpt_File = @"";
        public string BPT_File
        {
            get { return this.bpt_File; }
            set { this.bpt_File = value; }
        }

        /// <summary>
        /// 数据文件路径
        /// </summary>
        private DataTable dt病人信息 = new DataTable();
        public DataTable dt病人基本信息
        {
            get { return this.dt病人信息; }
            set { this.dt病人信息 = value; }
        }

        private DataSet _ds体温单信息 = new DataSet();
        public DataSet ds体温单信息
        {
            get { return this._ds体温单信息; }
            set { this._ds体温单信息 = value; }
        }
        public string s当前用户 { get; set; }

        #endregion

        #region 常量定义
        /// <summary>
        /// 横线间隔数区间步长
        /// </summary>
        const float yStep = 6.72f;
        /// <summary>
        /// 顶部、底部横线间隔数区间步长
        /// </summary>
        const float yStepHead = 18;
        /// <summary>
        /// 竖线间隔数区间步长
        /// </summary>
        const float xStep = 15.0f;
        /// <summary>
        /// 左侧竖线间隔数区间步长
        /// </summary>
        const float xStepHead = 70;
        /// <summary>
        /// 横线间隔去间数
        /// </summary>
        const int xNum = 86;
        /// <summary>
        /// 顶部横线间隔去间数
        /// </summary>
        const int xNumHead = 2;
        /// <summary>
        /// 底部横线间隔去间数
        /// </summary>
        const int xNumBottom = 14;
        /// <summary>
        /// 竖线间隔数区间数
        /// </summary>
        const int yNum = 42;
        #endregion

        private int pageNo = -1, pageNum = 0, ope手术后rNo = -1, oper分娩后No1 = -1;//
        private DataSet dataSet;
        private string strMsg = string.Empty;
        private string strQuery = string.Empty;
        #region 系统事件

        /// <summary>
        /// 画面初期化
        /// </summary>
        public void Load()
        {
            setPageSize();
            if (!string.IsNullOrEmpty(_sZYID))
            {
                strMsg = string.Empty;
                try
                {
                    DataTable dt病人信息 = this.GetDt病人信息ByZYID(_sZYID);
                    if (dt病人信息.Rows.Count == 1)
                    {
                        DataRow dataRow = dt病人信息.Rows[0];
                        txt住院日期.Text = dataRow["住院日期"].ToString();
                        txt床位.Text = dataRow["病床"].ToString();
                        txt姓名.Text = dataRow["病人姓名"].ToString();
                        txt性别.Text = dataRow["性别"].ToString();
                        txt年龄.Text = dataRow["年龄"].ToString();
                        txt住院号.Text = dataRow["住院号码"].ToString();
                    }
                }
                catch (System.Exception ex)
                {
                    strMsg = ex.Message;
                }
                finally
                {
                    if (strMsg != string.Empty)
                    {
                        MessageBox.Show(strMsg, "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                loadBPT_File();
            }
        }


        /// <summary>
        /// 设置打印大小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UC_BPT_SizeChanged(object sender, System.EventArgs e)
        {
            setPageSize();
        }


        /// <summary>
        /// 按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonS_Click(object sender, System.EventArgs e)
        {
            //构造打印对话框
            PrintDialog printDialog = new PrintDialog();
            DialogResult Result;
            Graphics graphics = panelDraw.CreateGraphics();
            switch ((sender as Button).Name)
            {
                case "buttonPrew":
                    if (pageNo > 0)
                    {
                        pageNo -= 1;
                        labelPage.Text = string.Format("第 {0} 页，共 {1} 页", pageNo + 1, pageNum);
                        drawAll(graphics);
                    }
                    break;
                case "buttonNext":
                    if (pageNo < pageNum - 1)
                    {
                        pageNo += 1;
                        labelPage.Text = string.Format("第 {0} 页，共 {1} 页", pageNo + 1, pageNum);
                        drawAll(graphics);
                    }
                    break;
                case "buttonPrint":
                    //设置Document属性
                    printDialog.Document = this.printDocument1;
                    //显示对话框
                    Result = printDialog.ShowDialog();
                    if (Result == DialogResult.OK)
                        printPageOne();
                    break;
                case "buttonPrintAll":
                    //设置Document属性
                    printDialog.Document = this.printDocument1;
                    //显示对话框
                    Result = printDialog.ShowDialog();
                    if (Result == DialogResult.OK)
                    {
                        for (int i = 0; i < pageNum; i++)
                        {
                            pageNo = i;
                            printPageOne();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        #region 位图
        protected Bitmap memoryImage;
        [DllImport("gdi32.dll")]
        private static extern bool BitBlt(
        IntPtr hdcDest, // 目标 DC的句柄
        int nXDest,
        int nYDest,
        int nWidth,
        int nHeight,
        IntPtr hdcSrc,// 源DC的句柄
        int nXSrc,
        int nYSrc,
        System.Int32 dwRop// 光栅的处理数值
        );
        #endregion
        #region 矢量图
        //private static System.Drawing.Imaging.Metafile mf;
        const int WM_PRINT = 0x0317;
        const int PRF_CHECKVISIBLE = 0x00000001,
        PRF_NONCLIENT = 0x00000002,
        PRF_CLIENT = 0x00000004,
        PRF_ERASEBKGND = 0x00000008,
        PRF_CHILDREN = 0x00000010;
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(HandleRef hWnd, int msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        static extern bool OpenClipboard(IntPtr hWndNewOwner);
        [DllImport("user32.dll")]
        static extern bool EmptyClipboard();
        [DllImport("user32.dll")]
        static extern IntPtr SetClipboardData(uint uFormat, IntPtr hMem);
        [DllImport("user32.dll")]
        static extern bool CloseClipboard();
        [DllImport("gdi32.dll")]
        static extern IntPtr CopyEnhMetaFile(IntPtr hemfSrc, IntPtr hNULL);
        [DllImport("gdi32.dll")]
        static extern bool DeleteEnhMetaFile(IntPtr hemf);
        public static void DrawControl(Control control, Graphics g)
        {
            if (!control.Created)
                control.CreateControl();
            IntPtr hDc = g.GetHdc();
            SendMessage(new HandleRef(control, control.Handle), WM_PRINT, (int)hDc,
            (int)(PRF_CHILDREN | PRF_CLIENT | PRF_ERASEBKGND | PRF_NONCLIENT));
            g.ReleaseHdc(hDc);
        }
        static public bool PutEnhMetafileOnClipboard(IntPtr hWnd, Metafile mf)
        {
            bool bResult = false;
            IntPtr hEMF, hEMF2;
            hEMF = mf.GetHenhmetafile(); // invalidates mf
            if (!hEMF.Equals(new IntPtr(0)))
            {
                hEMF2 = CopyEnhMetaFile(hEMF, new IntPtr(0));
                if (!hEMF2.Equals(new IntPtr(0)))
                {
                    if (OpenClipboard(hWnd))
                    {
                        if (EmptyClipboard())
                        {
                            IntPtr hRes = SetClipboardData(14 /*CF_ENHMETAFILE*/, hEMF2);
                            bResult = hRes.Equals(hEMF2);
                            CloseClipboard();
                        }
                    }
                }
                DeleteEnhMetaFile(hEMF);
            }
            return bResult;
        }
        private void CaptureScreen()
        {
            #region 位图
            /*Graphics mygraphics = exRTB.CreateGraphics();
Size s = exRTB.Size;
memoryImage = new Bitmap(s.Width, s.Height, mygraphics);
Graphics memoryGraphics = Graphics.FromImage(memoryImage);
IntPtr dc1 = mygraphics.GetHdc();
IntPtr dc2 = memoryGraphics.GetHdc();
BitBlt(dc2, 0,0, exRTB.Width-5, exRTB.Height-5, dc1, 0, 0, 13369376);
mygraphics.ReleaseHdc(dc1);
memoryGraphics.ReleaseHdc(dc2);*/
            #endregion
            #region 矢量图
            /*Graphics g1 = exRTB.CreateGraphics();
IntPtr hdc = g1.GetHdc();
mf = new Metafile(hdc, new Rectangle(exRTB.Location.X, exRTB.Location.Y, exRTB.Width, exRTB.Height), MetafileFrameUnit.Pixel, EmfType.EmfOnly);
g1.ReleaseHdc(hdc);
g1.Dispose();*/
            #endregion
        }
        #endregion

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            #region 原始打印功能
            drawAll(e.Graphics);
            StringFormat sf = null;
            for (int i = 0; i < this.panelDraw.Controls.Count; i++)
            {
                string typeName = panelDraw.Controls[i].GetType().Name;
                switch (typeName)
                {
                    case "TextBox":
                        TextBox textBox = panelDraw.Controls[i] as TextBox;
                        e.Graphics.DrawString(textBox.Text, textBox.Font, Brushes.Black, textBox.Location);
                        break;
                    case "Label":
                        sf = new StringFormat();
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Center;
                        Label label = panelDraw.Controls[i] as Label;
                        e.Graphics.DrawString(label.Text, label.Font, Brushes.Black, new Rectangle(label.Location, new Size(label.Width, label.Height)), sf);
                        break;
                    default:
                        break;
                }
            }
            #endregion
        }

        private void panelDraw_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            drawAll(graphics);
        }


        /// <summary>
        /// 双击显示编辑页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panelDraw_DoubleClick(object sender, System.EventArgs e)
        {
            if (canEdit)
            {
                //返回dateset信息
                InputFrm inputFrm = new InputFrm();
                inputFrm.Tag = this;
                if (inputFrm.ShowDialog() == DialogResult.OK)
                {
                    loadBPT_File();
                    Graphics graphics = panelDraw.CreateGraphics();
                    drawAll(graphics);
                }
            }
            else
            {
                MessageBox.Show("当前体温单数据不能修改");
            }
        }
        #endregion

        #region 自定义方法

        /// <summary>
        /// 画面打印
        /// </summary>
        /// <returns></returns>
        private bool printPageOne()
        {
            try
            {
                if (dataSet.Tables.Count > 1)
                {
                    printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printDocument1_PrintPage);
                    printDocument1.Print();
                    return true;
                }
                else
                    return false;
            }
            catch (System.Exception ex)
            {
                printDocument1.PrintPage -= new System.Drawing.Printing.PrintPageEventHandler(printDocument1_PrintPage);
                if (ex.Message == "操作已被用户取消。")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        /// <summary>
        /// 画面信息读取
        /// </summary>
        /// <param name="bptFile"></param>
        private void loadBPT_File()
        {
            _ds体温单信息 = this.GetDs体温单信息ByZYID(_sZYID);
            if (_ds体温单信息 != null)
            {
                dataSet = new DataSet();
                try
                {
                    dataSet = _ds体温单信息.Copy();
                    if (dataSet.Tables.Count == 3)
                    {
                        if (dataSet.Tables["病人信息"] != null)
                        {
                            dataSet.Tables["病人信息"].PrimaryKey = new DataColumn[] { dataSet.Tables["病人信息"].Columns["Patient_Mrno"] };
                        }
                        if (dataSet.Tables["三测数据"] != null)
                        {
                            dataSet.Tables["三测数据"].PrimaryKey = new DataColumn[] { dataSet.Tables["三测数据"].Columns["测量日期"], dataSet.Tables["三测数据"].Columns["测量时间"] };
                            dataSet.Tables["三测数据"].DefaultView.Sort = "测量日期,测量时间";
                        }
                        if (dataSet.Tables["底栏数据"] != null)
                        {
                            dataSet.Tables["底栏数据"].PrimaryKey = new DataColumn[] { dataSet.Tables["底栏数据"].Columns["测量日期"] };
                            dataSet.Tables["底栏数据"].DefaultView.Sort = "测量日期";
                        }
                        DataTable dt测量天数 = this.dt测量天数();
                        if (dt测量天数.Rows.Count > 0)
                        {
                            int days = (DateTime.Parse(dt测量天数.Rows[dt测量天数.Rows.Count - 1]["测量日期"].ToString()) - DateTime.Parse(dt测量天数.Rows[0]["测量日期"].ToString())).Days + 1;
                            if (days % 7 == 0)
                                pageNum = days / 7;
                            else
                                pageNum = (days / 7) + 1;
                            pageNo = pageNum - 1;
                        }
                    }
                    else
                    {
                        pageNo = -1;
                        pageNum = 0;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    labelPage.Text = string.Format("第 {0} 页，共 {1} 页", pageNo + 1, pageNum);
                }
            }
        }


        /// <summary>
        /// 天数计算
        /// </summary>
        /// <returns></returns>
        private DataTable dt测量天数()
        {
            DataTable dtDay = new DataTable();
            dtDay.Columns.Add("测量日期");
            dtDay.PrimaryKey = new DataColumn[] { dtDay.Columns["测量日期"] };
            if (dataSet.Tables["三测数据"] != null)
            {
                for (int i = 0; i < dataSet.Tables["三测数据"].Rows.Count; i++)
                {
                    string 测量日期 = dataSet.Tables["三测数据"].Rows[i]["测量日期"].ToString();
                    if (!dtDay.Rows.Contains(new object[] { 测量日期 }))
                        dtDay.Rows.Add(new object[] { 测量日期 });
                }
                dtDay.DefaultView.Sort = "测量日期";
            }
            return dtDay;
        }

        /// <summary>
        /// 画所有
        /// </summary>
        /// <param name="graphics"></param>
        private void drawAll(Graphics graphics)
        {
            graphics.Clear(Color.White);
            drawBase(graphics);
            if (_ds体温单信息 != null && _ds体温单信息.Tables.Count == 3)
            {
                drawBPT(pageNo, graphics);
            }
        }

        /// <summary>
        /// 画面值画图
        /// </summary>
        /// <param name="pageNo">页数</param>
        /// <param name="graphics"></param>
        private void drawBPT(int pageNo, Graphics graphics)
        {
            //pageNo = 0;
            Rectangle rect = Rectangle.Empty;
            StringFormat sf = new StringFormat();
            PointF[] bptPoints0 = new PointF[3], bptPoints1 = new PointF[3];

            #region 表头、手术、分娩日期
            DateTime dtt表格第一天 = DateTime.Parse(txt住院日期.Text).AddDays(pageNo * 7);//当前页的第一天
            DateTime dtt表头日期 = dtt表格第一天;//循环表格第几天
            ArrayList arr手术日期 = ClassGet手术分娩后天数.getArr手术日期(dtt表格第一天, ds体温单信息.Tables["三测数据"]);//获取手术后天数显示数组
            DateTime dtt分娩日期 = ClassGet手术分娩后天数.getDtt分娩日期(ds体温单信息.Tables["三测数据"]);//获取分娩日期


            for (int i = 0; i < 7; i++)
            {
                PointF pointf = PointF.Empty;
                pointf = new PointF(StartPoint.X + 2 + xStepHead + (i % 7) * (xStep * 6) + 8 + 40, StartPoint.Y + 3);
                StringFormat sf1 = new StringFormat();
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Near;
                string s界面表头日期 = "";
                if (DateTime.Parse(dtt表头日期.ToString()).Date == DateTime.Parse(txt住院日期.Text).Date)//第一天
                {
                    s界面表头日期 = dtt表头日期.ToString("yyyy-MM-dd");
                }
                else if (DateTime.Parse(dtt表头日期.ToString()).Year > dtt表头日期.AddDays(-1).Year)//跨年份
                {
                    s界面表头日期 = dtt表头日期.ToString("yyyy-MM-dd");
                }
                else if (DateTime.Parse(dtt表头日期.ToString()).Month > dtt表头日期.AddDays(-1).Month)//跨月份
                {
                    s界面表头日期 = dtt表头日期.ToString("yyyy-MM-dd").Substring(5, 5);
                }
                else
                {
                    s界面表头日期 = dtt表头日期.ToString("yyyy-MM-dd").Substring(8, 2);
                }
                graphics.DrawString(s界面表头日期, new Font("宋体", 10), Brushes.Black, pointf, sf);//表头住院日期


                pointf.X = startPoint.X + xStepHead + ((i) % 7) * 6 * xStep;
                pointf.Y = startPoint.Y + (xNumHead + 11) * yStepHead + xNum * yStep + 40;
                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                string _s无底栏数据部分住院天数 = (pageNo * 7 + i).ToString();
                graphics.DrawString(_s无底栏数据部分住院天数, new Font("宋体", 10), Brushes.Black, rect, sf);//无底栏数据部分表尾住院天数



                if (arr手术日期.Count > 0)
                {
                    pointf.X = startPoint.X + xStepHead + (i) * 6 * xStep;// + i手术次数 * 8;
                    pointf.Y = startPoint.Y + (xNumHead + 9) * yStepHead + xNum * yStep + 40;
                    rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                    graphics.DrawString(arr手术日期[i].ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//手术后日期
                }
                if (dtt分娩日期.Date != DateTime.Parse("1900-01-01"))
                {
                    TimeSpan ts分娩后天数 = dtt表头日期.Date - dtt分娩日期.Date;
                    if (ts分娩后天数.Days >= 0)
                    {
                        pointf.X = startPoint.X + xStepHead + (i) * 6 * xStep;
                        pointf.Y = startPoint.Y + (xNumHead + 10) * yStepHead + xNum * yStep + 40;
                        rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                        graphics.DrawString(ts分娩后天数.Days.ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//分娩后日期
                    }
                }
                dtt表头日期 = dtt表头日期.AddDays(1);

            }
            #endregion

            if (dataSet.Tables.Contains("三测数据"))
            {

                DataTable dt三测数据 = dataSet.Tables["三测数据"];

                int k = 0;
                DateTime dtt当前测量日期 = new DateTime();
                for (int i = 0; i < dt三测数据.Rows.Count; i++)
                {
                    TimeSpan ts首次测量日期到本次测量天数 = DateTime.Parse(dt三测数据.Rows[i]["测量日期"].ToString()) - DateTime.Parse(dt三测数据.Rows[0]["测量日期"].ToString());
                    TimeSpan ts入院日期到本次测量天数 = DateTime.Parse(dt三测数据.Rows[i]["测量日期"].ToString()) - DateTime.Parse(txt住院日期.Text);
                    //if (dt三测数据.Rows[i]["标注文字"].ToString().IndexOf("手术") > -1) ope手术后rNo = ts首次测量日期到本次测量天数.Days;
                    //if (dt三测数据.Rows[i]["标注文字"].ToString().IndexOf("分娩") > -1) oper分娩后No1 = ts首次测量日期到本次测量天数.Days;
                    if ((ts首次测量日期到本次测量天数.Days >= pageNo * 7) && (ts首次测量日期到本次测量天数.Days < (pageNo + 1) * 7))
                    {
                        strMsg = dt三测数据.Rows[i]["测量日期"].ToString() + " - " + dt三测数据.Rows[i]["测量时间"].ToString() + " - " + ts首次测量日期到本次测量天数.Days + " - " + i.ToString() + "\r\n";
                        PointF pointf = PointF.Empty;


                        #region draw三测数据
                        int xn = int.Parse(dt三测数据.Rows[i]["测量时间"].ToString());

                        if (!(dt三测数据.Rows[i]["脉搏(次/分)"] == null || dt三测数据.Rows[i]["脉搏(次/分)"].ToString() == ""))
                        {
                            pointf.X = StartPoint.X + 2 + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * (xStep * 6) + (xn / 4) * xStep + 1.3f;
                            pointf.Y = (float)(StartPoint.Y - 66 + yStepHead * xNumHead + ((210.0f - float.Parse(dt三测数据.Rows[i]["脉搏(次/分)"].ToString())) / 2) * yStep) - 8.5f;
                            graphics.DrawString("●", new Font("楷体", 10), Brushes.Red, pointf);
                            //if (bptPoints0[1].IsEmpty)
                            //{
                            //    bptPoints0[1] = new PointF(pointf.X + 6.5f, pointf.Y + 8.5f);
                            //}
                            //else
                            //{
                            //    bptPoints1[1] = new PointF(pointf.X + 6.5f, pointf.Y + 8.5f);
                            //    graphics.DrawLine(new Pen(Color.Red, 1.5f), bptPoints0[1], bptPoints1[1]);
                            //    bptPoints0[1] = bptPoints1[1];
                            //}
                            //节点处断开
                            if (bptPoints0[1].IsEmpty)
                            {
                                bptPoints0[1] = new PointF(pointf.X + 6.5f + 6, pointf.Y + 8.5f);
                            }
                            else
                            {
                                bptPoints1[1] = new PointF(pointf.X + 6.5f - 1, pointf.Y + 8.5f);
                                graphics.DrawLine(new Pen(Color.Red, 1.5f), bptPoints0[1], bptPoints1[1]);
                                bptPoints0[1] = new PointF(pointf.X + 6.5f + 6, pointf.Y + 8f - 2);
                            }
                            //↓节点处断开，已还原
                            //if (bptPoints0[1].IsEmpty)
                            //{
                            //    bptPoints0[1] = new PointF(pointf.X + 6.5f + 6, pointf.Y + 8.5f);
                            //}
                            //else
                            //{
                            //    bptPoints1[1] = new PointF(pointf.X + 6.5f - 1, pointf.Y + 8.5f);
                            //    graphics.DrawLine(new Pen(Color.Red, 1.5f), bptPoints0[1], bptPoints1[1]);
                            //    bptPoints0[1] = new PointF(pointf.X + 6.5f + 6, pointf.Y + 8f - 2);
                            //}
                        }

                        if (!(dt三测数据.Rows[i]["体温(℃)"] == null || dt三测数据.Rows[i]["体温(℃)"].ToString() == ""))
                        {
                            pointf.X = StartPoint.X + 2 + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * (xStep * 6) + (xn / 4) * xStep + 0.8f;
                            pointf.Y = (float)(StartPoint.Y + 35 + yStepHead * xNumHead + ((42.0f - float.Parse(dt三测数据.Rows[i]["体温(℃)"].ToString())) / 0.1) * yStep) - 8f;
                            graphics.DrawString("×", new Font("楷体", 10, FontStyle.Bold), Brushes.Blue, pointf);
                            int i偏移 = 2;
                            if (Environment.OSVersion.ToString().Replace("Microsoft Windows NT 5.1", "") != Environment.OSVersion.ToString()) //XP系统
                            {
                                i偏移 = 0;
                            }
                            if (bptPoints0[0].IsEmpty)
                            {
                                bptPoints0[0] = new PointF(pointf.X + 6.5f + i偏移, pointf.Y + 8f - i偏移);
                            }
                            else
                            {
                                bptPoints1[0] = new PointF(pointf.X + 6.5f + i偏移, pointf.Y + 8f - i偏移);
                                graphics.DrawLine(new Pen(Color.Blue, 1.5f), bptPoints0[0], bptPoints1[0]);
                                bptPoints0[0] = bptPoints1[0];
                            }

                            ////↓节点处断开，已还原
                            //if (bptPoints0[0].IsEmpty)
                            //{
                            //    bptPoints0[0] = new PointF(pointf.X + 6.5f + 6, pointf.Y + 8f - 2);
                            //}
                            //else
                            //{
                            //    bptPoints1[0] = new PointF(pointf.X + 6.5f - 1, pointf.Y + 8f - 2);
                            //    graphics.DrawLine(new Pen(Color.Blue, 1.5f), bptPoints0[0], bptPoints1[0]);
                            //    bptPoints0[0] = new PointF(pointf.X + 6.5f + 6, pointf.Y + 8f - 2);
                            //}


                            if (!(dt三测数据.Rows[i]["物理降温(℃)"] == null || dt三测数据.Rows[i]["物理降温(℃)"].ToString() == ""))
                            {
                                pointf.X = StartPoint.X + 2 + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * (xStep * 6) + (xn / 4) * xStep - 1.5f;
                                pointf.Y = (float)(StartPoint.Y + 35 + yStepHead * xNumHead + ((42.0f - float.Parse(dt三测数据.Rows[i]["物理降温(℃)"].ToString())) / 0.1) * yStep) - 7f;
                                graphics.DrawString("○", new Font("楷体", 9, FontStyle.Bold), Brushes.Red, pointf);
                                pointf.X += xStep / 2 + 1; pointf.Y += 9f;
                                Pen pen = new Pen(Color.Red, 2f);
                                pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                                graphics.DrawLine(pen, bptPoints0[0], pointf);
                            }
                        }


                        //画
                        if (!(dt三测数据.Rows[i]["呼吸(次/分)"] == null || dt三测数据.Rows[i]["呼吸(次/分)"].ToString() == ""))
                        {
                            pointf.X = StartPoint.X + 1 + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * (xStep * 6) + (xn / 4) * xStep + 1.3f;
                            //  if (!adta.Equals(dtMeasure.Rows[i]["测量日期"].ToString())) k = 0;
                            //adta = dtMeasure.Rows[i]["测量日期"].ToString();
                            if (k % 2 == 0) { pointf.Y = startPoint.Y + (xNumHead + 0) * yStepHead + xNum * yStep + 7; } else { pointf.Y = startPoint.Y + (xNumHead + 0) * yStepHead + xNum * yStep + 20; }

                            graphics.DrawString(dt三测数据.Rows[i]["呼吸(次/分)"].ToString(), new Font("宋体", 9), Brushes.Blue, pointf);//呼吸
                            k = k + 1;
                        }
                        for (int j = 0; j < dt三测数据.Rows[i]["标注文字"].ToString().Length; j++)
                        {
                            pointf.X = StartPoint.X + 2 + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * (xStep * 6) + (xn / 4) * xStep - 1.0f;
                            pointf.Y = startPoint.Y + yStepHead * xNumHead + j * yStep * 2 + 35;
                            if (dt三测数据.Rows[i]["标注文字"].ToString()[j] == '-' ||
                            dt三测数据.Rows[i]["标注文字"].ToString()[j] == '－' ||
                            dt三测数据.Rows[i]["标注文字"].ToString()[j] == '—')
                                graphics.DrawString("│", new Font("楷体", 9), Brushes.Red, pointf);
                            else
                                graphics.DrawString(dt三测数据.Rows[i]["标注文字"].ToString()[j].ToString(), new Font("楷体", 9/*, FontStyle.Bold*/), Brushes.Red, pointf);
                        }

                        #endregion

                        #region draw底栏数据
                        if (dataSet.Tables.Contains("底栏数据"))
                        {
                            dataSet.Tables["底栏数据"].DefaultView.RowFilter = string.Format("测量日期 like '{0}'", dt三测数据.Rows[i]["测量日期"].ToString());
                            DataRow dRow = dataSet.Tables["底栏数据"].Rows.Find(new object[] { dt三测数据.Rows[i]["测量日期"].ToString() });
                            if (dRow != null)
                            {
                                sf.Alignment = StringAlignment.Center;
                                sf.LineAlignment = StringAlignment.Center;
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                pointf.Y = startPoint.Y + (xNumHead + 0) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString(dRow["大便(次)"].ToString(), new Font("宋体", 10), Brushes.Blue, rect, sf);//大便次数
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                pointf.Y = startPoint.Y + (xNumHead + 1) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString(dRow["小便(ml)"].ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//尿量
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                pointf.Y = startPoint.Y + (xNumHead + 2) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString(dRow["痰量(ml)"].ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//痰量
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                pointf.Y = startPoint.Y + (xNumHead + 3) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString(dRow["引流量(ml)"].ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//引流量
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                pointf.Y = startPoint.Y + (xNumHead + 4) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString(dRow["呕吐量(ml)"].ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//呕吐量
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                pointf.Y = startPoint.Y + (xNumHead + 5) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString("", new Font("宋体", 10), Brushes.Black, rect, sf);//总量
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                pointf.Y = startPoint.Y + (xNumHead + 6) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString(dRow["液入量(ml)"].ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//入量
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep + 2;
                                pointf.Y = startPoint.Y + (xNumHead + 7) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString(dRow["血压AM(mmHg)"].ToString(), new Font("宋体", 8), Brushes.Black, rect);//血压
                                pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                pointf.Y = startPoint.Y + (xNumHead + 8) * yStepHead + xNum * yStep + 40;
                                rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                graphics.DrawString(dRow["体重(Kg)"].ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//体重
                                                                                                                            //if (ope手术后rNo > -1 && ts首次测量日期到本次测量天数.Days - ope手术后rNo >= 0 && ts首次测量日期到本次测量天数.Days - ope手术后rNo < 11)
                                                                                                                            //{
                                                                                                                            //    pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                                                                                                            //    pointf.Y = startPoint.Y + (xNumHead + 9) * yStepHead + xNum * yStep + 40;
                                                                                                                            //    rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                                                                                                            //    // pointf = new PointF(StartPoint.X + xStepHead + (timeSpan.Days % 7) * (xStep * 6), startPoint.Y + (xNumHead + 9) * yStepHead + xNum * yStep + 40);
                                                                                                                            //    graphics.DrawString((ts首次测量日期到本次测量天数.Days - ope手术后rNo).ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//手术后日期
                                                                                                                            //}
                                                                                                                            //if (oper分娩后No1 > -1 && ts首次测量日期到本次测量天数.Days - oper分娩后No1 > 0 && ts首次测量日期到本次测量天数.Days - oper分娩后No1 < 15)
                                                                                                                            //{
                                                                                                                            //    pointf.X = startPoint.X + xStepHead + (ts首次测量日期到本次测量天数.Days % 7) * 6 * xStep;
                                                                                                                            //    pointf.Y = startPoint.Y + (xNumHead + 10) * yStepHead + xNum * yStep + 40;
                                                                                                                            //    rect = new Rectangle(new Point((int)pointf.X, (int)pointf.Y), new Size((int)(6 * xStep), (int)(yStepHead)));
                                                                                                                            //    //pointf = new PointF(StartPoint.X + xStepHead + (timeSpan.Days % 7) * (xStep * 6), startPoint.Y + (xNumHead + 10) * yStepHead + xNum * yStep + 40);
                                                                                                                            //    graphics.DrawString((ts首次测量日期到本次测量天数.Days - oper分娩后No1).ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);//分娩后日期
                                                                                                                            //}

                            }
                        }
                        #endregion
                    }
                    rect = new Rectangle(new Point(startPoint.X, bottomLimitValue), new Size(rightLimitValue - startPoint.X, 19));
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    graphics.DrawString(string.Format("第 {0} 周", pageNo + 1), new Font("宋体", 10), Brushes.Black, rect, sf);
                }
            }

        }

        private string AddData(DataRow dRow)
        {
            string reust = "";
            int Pee = 0;
            int circumference = 0;
            int liquid_out = 0;
            int pressurep = 0;
            if (!string.IsNullOrEmpty(dRow["小便(ml)"].ToString()))
            {
                Pee = int.Parse(dRow["小便(ml)"].ToString());
            }
            if (!string.IsNullOrEmpty(dRow["痰量(ml)"].ToString()))
            {
                circumference = int.Parse(dRow["痰量(ml)"].ToString());
            }
            if (!string.IsNullOrEmpty(dRow["引流量(ml)"].ToString()))
            {
                liquid_out = int.Parse(dRow["引流量(ml)"].ToString());
            }
            if (!string.IsNullOrEmpty(dRow["呕吐量(ml)"].ToString()))
            {
                pressurep = int.Parse(dRow["呕吐量(ml)"].ToString());
            }
            //总量
            reust = (Pee + circumference + liquid_out + pressurep).ToString();
            return reust;

        }
        /// <summary>
        /// 画基础表格
        /// </summary>
        /// <param name="graphics"></param>
        private void drawBase(Graphics graphics)
        {
            StringFormat sf;
            Rectangle rect;
            sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            //医院名称
            rect = new Rectangle(new Point(startPoint.X, startPoint.Y - 105), new Size(rightLimitValue - startPoint.X, 40));
            graphics.DrawString(mainTitle, new Font("宋体", 23, System.Drawing.FontStyle.Bold), Brushes.Black, rect, sf);
            //体温单记录
            rect = new Rectangle(new Point(startPoint.X, startPoint.Y - 60), new Size(rightLimitValue - startPoint.X, 35));
            graphics.DrawString(secondTitle, new Font("宋体", 22, System.Drawing.FontStyle.Bold), Brushes.Black, rect, sf);
            rect = new Rectangle(new Point(startPoint.X - 4, startPoint.Y), new Size(rightLimitValue - startPoint.X + 6, bottomLimitValue - startPoint.Y - 25));
            graphics.DrawRectangle(new Pen(Brushes.Black, 1.0f), rect);
            string[] drawString = new string[] { "住院日期", "脉搏  体温" };
            for (int i = 0; i < xNumHead; i++)
            {
                PointF pointF0 = new PointF(startPoint.X - 4, startPoint.Y + (i + 1) * yStepHead);
                PointF pointF1 = new PointF(rightLimitValue, startPoint.Y + (i + 1) * yStepHead);

                if (i == 1)
                {
                    pointF0 = new PointF(startPoint.X - 4, startPoint.Y + (i + 1) * yStepHead + 7);
                    pointF1 = new PointF(rightLimitValue, startPoint.Y + (i + 1) * yStepHead + 7);
                }
                Pen pen = new Pen(Brushes.Black, 1.0f);
                graphics.DrawLine(pen, pointF0, pointF1);
                //
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;

                rect = new Rectangle(new Point(startPoint.X + 2, (int)(startPoint.Y + i * yStepHead) + 2), new Size((int)(xStepHead) + 5, (int)yStepHead));
                if (i == 1)
                    rect = new Rectangle(new Point(startPoint.X + 2, (int)(startPoint.Y + i * yStepHead) + 6), new Size((int)(xStepHead) + 5, (int)yStepHead));
                graphics.DrawString(drawString[i], new Font("宋体", 9), Brushes.Black, rect, sf);
            }
            for (int i = 0; i < xNum; i++)
            {
                PointF pointF0 = new PointF(startPoint.X + xStepHead + 2, startPoint.Y + i * yStep + xNumHead * yStepHead);
                PointF pointF1 = new PointF(rightLimitValue + 2, startPoint.Y + i * yStep + xNumHead * yStepHead);
                if (i % 2 == 0)
                {
                    pointF0 = new PointF();
                    pointF1 = new PointF();
                }
                Pen pen = new Pen(Brushes.Black, 1.0f);
                if (i % 5 == 0 && i != 55)
                {
                    pen = new Pen(Brushes.Black, 2.0f);
                    if (i / 5 == 10)
                    {
                        pen = new Pen(Brushes.Red, 2.0f);
                    }
                }
                if (i == 55 || i == 87)
                {
                    pen = new Pen(Brushes.Red, 2.0f);
                }
                if (i > 0)
                {
                    graphics.DrawLine(pen, pointF0, pointF1);
                }
                if (i % 10 == 0 && i / 10 < 15)
                {//体温
                    sf.Alignment = StringAlignment.Far;
                    sf.LineAlignment = StringAlignment.Center;
                    int tmp = (42 - i / 10);
                    if (i == 0)
                    {
                        rect = new Rectangle(new Point((int)(startPoint.X + xStepHead - 40), (int)(startPoint.Y + i * yStep + xNumHead * yStepHead + 10)), new Size(34, 16));
                        graphics.DrawString("℃", new Font("宋体", 8), Brushes.Red, rect, sf);
                        rect = new Rectangle(new Point((int)(startPoint.X + xStepHead - 20), (int)(startPoint.Y + i * yStep + xNumHead * yStepHead + 26)), new Size(20, 11));
                        graphics.DrawString(tmp.ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);
                    }
                    else
                    {
                        rect = new Rectangle(new Point((int)(startPoint.X + xStepHead - 20), (int)(startPoint.Y + i * yStep + xNumHead * yStepHead + 28)), new Size(20, 11));
                        graphics.DrawString(tmp.ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);
                    }
                }
                if (i % 5 == 0 && i / 10 < 18)
                {//脉搏
                    sf.Alignment = StringAlignment.Far;
                    sf.LineAlignment = StringAlignment.Center;
                    int tmp = (180 - i / 5 * 10);
                    if (i == 0)
                    {
                        rect = new Rectangle(new Point((int)(startPoint.X + xStepHead - 72), (int)(startPoint.Y + i * yStep + xNumHead * yStepHead + 10)), new Size(34, 16));
                        graphics.DrawString("次/分", new Font("宋体", 8), Brushes.Red, rect, sf);
                    }
                    if (tmp > 10)
                    {
                        rect = new Rectangle(new Point((int)(startPoint.X), (int)(startPoint.Y + i * yStep + xNumHead * yStepHead + 28)), new Size(30, 11));
                        graphics.DrawString(tmp.ToString(), new Font("宋体", 10), Brushes.Black, rect, sf);
                    }
                }
            }
            for (int i = 0; i < xNumBottom; i++)
            {
                //底部栏画横线
                PointF pointF0 = new PointF(startPoint.X - 4, startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead);
                PointF pointF1 = new PointF(rightLimitValue, startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead);

                if (i == 0)
                {//底部栏画横线
                    pointF0 = new PointF(startPoint.X - 4, startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead + 7);
                    pointF1 = new PointF(rightLimitValue, startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead + 7);
                }
                if (i == 1)
                {
                    pointF0 = new PointF();
                    pointF1 = new PointF();
                }
                if (4 <= i && i <= 7)
                {
                    pointF0 = new PointF(startPoint.X + xStepHead - 55, startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead);
                    pointF1 = new PointF(rightLimitValue, startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead);
                }

                Pen pen = new Pen(Brushes.Black, 1.0f);
                graphics.DrawLine(pen, pointF0, pointF1);
                drawString = new string[] { "呼  吸  数", "", "大便  次数", "   尿  量ml", "出 痰  量ml", "   引流量ml", "量 呕吐量ml", "   总  量ml", "入    量ml", "血  压mmHg  ", "体   重Kg", "手术后天数", "分娩后天数", "住院天  数" };
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;
                //底部栏项目名称
                rect = new Rectangle(new Point((int)(startPoint.X - 4), (int)(startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead) + 2), new Size((int)(xStepHead) + 5, (int)yStepHead));
                //底部栏项目名称
                if (i == 0)
                {
                    rect = new Rectangle(new Point((int)(startPoint.X - 4), (int)(startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead) + 12), new Size((int)(xStepHead) + 5, (int)yStepHead));
                }
                if (3 <= i && i <= 7)
                {
                    rect = new Rectangle(new Point((int)(startPoint.X - 4), (int)(startPoint.Y + i * yStepHead + xNum * yStep + xNumHead * yStepHead) + 2), new Size((int)(xStepHead) + 5, (int)yStepHead));
                    graphics.DrawString(drawString[i], new Font("宋体", 9), Brushes.Black, rect, sf);
                }
                else { graphics.DrawString(drawString[i], new Font("宋体", 9), Brushes.Black, rect, sf); }

            }
            //底栏项目名称竖线
            graphics.DrawLine(new Pen(Brushes.Black, 1.0f),
            new PointF(startPoint.X + xStepHead - 55, startPoint.Y + 3 * yStepHead + xNum * yStep + xNumHead * yStepHead),
            new PointF(startPoint.X + xStepHead - 55, startPoint.Y + 5 * yStepHead + xNum * yStep + xNumHead * yStepHead + 54));
            for (int i = 0; i < yNum; i++)
            {
                //竖线的开始位置
                PointF pointF0 = new PointF(startPoint.X + xStepHead + i * xStep + 2, startPoint.Y + (xNumHead - 1) * yStepHead);
                //竖线的结束位置
                PointF pointF1 = new PointF(startPoint.X + xStepHead + i * xStep + 2, bottomLimitValue - xNumBottom * yStepHead + 12);
                Pen pen = new Pen(Brushes.Black, 1.0f);
                if (i % 6 == 0)
                {
                    //粗竖线的开始位置
                    pointF0 = new PointF(startPoint.X + xStepHead + i * xStep + 2, startPoint.Y);
                    //粗竖线的结束位置
                    pointF1 = new PointF(startPoint.X + xStepHead + i * xStep + 2, bottomLimitValue - 25);

                    if (i == 0)
                    {
                        pen = new Pen(Brushes.Black, 1.0f);
                    }
                    else
                    {
                        pen = new Pen(Brushes.Red, 1.0f);
                    }
                }
                graphics.DrawLine(pen, pointF0, pointF1);
                //画时间
                int hours = (i + 1) % 6 * 4;
                if (hours == 0)
                {
                    hours = 24;
                }
                hours -= 1;
                pointF0 = new PointF(startPoint.X + 2 + xStepHead + i * xStep, startPoint.Y + (xNumHead - 1) * yStepHead + 7);
                if (i % 6 == 0 || i % 6 == 4 || i % 6 == 5)
                {
                    graphics.DrawString(hours.ToString(), new Font("宋体", 9), Brushes.Red, pointF0);
                }
                else
                {
                    graphics.DrawString(hours.ToString(), new Font("宋体", 9), Brushes.Black, pointF0);
                }
            }
            //画体温脉搏中的竖线
            graphics.DrawLine(new Pen(Brushes.Black, 1.0f),
            new PointF(startPoint.X + 30, startPoint.Y + xNumHead * yStepHead - 17),
            new PointF(startPoint.X + 30, bottomLimitValue - xNumBottom * yStepHead - 18));
            //画text下划线
            for (int i = 0; i < this.panelDraw.Controls.Count; i++)
            {
                string typeName = panelDraw.Controls[i].GetType().Name;
                if (typeName == "TextBox")
                {
                    TextBox textBox = panelDraw.Controls[i] as TextBox;
                    graphics.DrawLine(new Pen(Brushes.Black, 1.0f), new Point(textBox.Location.X, textBox.Location.Y + textBox.Height + 1), new Point(textBox.Location.X + textBox.Width, textBox.Location.Y + textBox.Height + 1));
                }
            }
        }


        /// <summary>
        /// 设置纸张尺寸
        /// </summary>
        private void setPageSize()
        {
            if (pageType == "MR")
            {//纸张【病历纸：210*275】
                panelBorder.Size = new Size(800, 1080);
                StartPoint = new Point(50, 130);
                RightLimitValue = 750;
                BottomLimitValue = 1020;
            }
            else if (pageType == "A4")
            {//纸张【A4纸：210*297】
                panelBorder.Size = new Size(800, 1170);
                this.StartPoint = new Point(50, 130);
                this.RightLimitValue = 750;
                this.BottomLimitValue = 1120;
            }
            if (this.Width <= panelBorder.Width)
            {
                this.panelBorder.Location = new Point(0, 30);
            }
            else
            {
                this.panelBorder.Location = new Point((this.Width - panelBorder.Width) / 2, 30/*(this.Height-panelBorder.Height)/2*/);
            }
            this.buttonPrew.Location = new Point(this.panelBorder.Location.X + 30, this.buttonPrew.Location.Y);
            this.buttonNext.Location = new Point(this.panelBorder.Location.X + 130, this.buttonNext.Location.Y);
            this.buttonPrint.Location = new Point(this.panelBorder.Location.X + 230, this.buttonPrint.Location.Y);
            this.buttonPrintAll.Location = new Point(this.panelBorder.Location.X + 330, this.buttonPrintAll.Location.Y);
            this.labelPage.Location = new Point(this.panelBorder.Location.X + 430, this.labelPage.Location.Y);
        }


        /// <summary>
        /// 根据住院号获取病人基本信息
        /// </summary>
        /// <param name="Id">住院号</param>
        /// <returns>病人基本信息</returns>
        public DataTable GetDt病人信息ByZYID(string Id)
        {
            var result = new DataTable();
            try
            {
                string SQLPatient
                = "SELECT" +
                " Z.住院号码 ," +
                "Z.病人姓名," +
                "Z.性别," +
                "datediff(" +
                " YEAR," +
                " Z.出生日期," +
                " getdate()" +
                ") 年龄," +
                "Z.病床," +
               " Convert(varchar(100),入院时间,23) 住院日期," +
                "G.科室名称" +
                " FROM" +
                " ZY病人信息 Z" +
                " LEFT JOIN GY科室设置 G ON Z.科室 = G.科室编码" +
                " WHERE" +
                " Z.ZYID = " + Id;
                result = HIS.Model.Dal.SqlHelper.ExecuteDataTable(CommandType.Text, SQLPatient);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return result;
        }

        private DataSet GetDs体温单信息ByZYID(string ZYID)
        {
            DataSet result = new DataSet();

            try
            {
                //查询病人信息
                string SQLPatient
                = "SELECT" +
                " Z.病人姓名  ," +
                " Z.病床  ," +
                " G.科室名称  ," +
                " Z.住院号码 " +
                " FROM" +
                " ZY病人信息 Z" +
                " LEFT JOIN GY科室设置 G ON Z.科室 = G.科室编码" +
                " WHERE" +
                " Z.ZYID = " + ZYID;

                DataTable Patient = new DataTable();
                Patient = HIS.Model.Dal.SqlHelper.ExecuteDataTable(CommandType.Text, SQLPatient);
                Patient.TableName = "病人信息";
                result.Tables.Add(Patient);

                //查询体温呼吸
                string SQLMeasure
                = "SELECT " +
                " [体温(℃)] ," +
                " [物理降温(℃)] ," +
                " [脉搏(次/分)] ," +
                " [呼吸(次/分)] ," +
                " [标注文字] ," +
                " CONVERT(varchar(10), [测量日期], 120 ) 测量日期," +
                " [测量时间] " +
                " FROM" +
                " HL体温单三测数据" +
                " WHERE" +
                " 作废标志 = '0' AND" +
                " ZYID = " + ZYID + " order by [测量日期],测量时间";
                DataTable Measure = new DataTable();
                Measure = HIS.Model.Dal.SqlHelper.ExecuteDataTable(CommandType.Text, SQLMeasure);
                Measure.TableName = "三测数据";
                result.Tables.Add(Measure);

                //查询大便等信息
                string SQLCMeasure
                = " SELECT " +
                " [小便(ml)] ," +
                " [大便(次)] ," +
                " [液入量(ml)] ," +
                " [血压AM(mmHg)] ," +
                " [呕吐量(ml)] ," +
                " [体重(Kg)] ," +
                " [痰量(ml)] ," +
                " [引流量(ml)] ," +
                " CONVERT(varchar(10), [测量日期], 120 ) 测量日期" +
                " FROM" +
                " HL体温单底栏数据" +
                " WHERE" +
                " 作废标志 = '0' AND" +
                " ZYID = " + ZYID + " order by [测量日期]";
                DataTable CMeasure = new DataTable();
                CMeasure = HIS.Model.Dal.SqlHelper.ExecuteDataTable(CommandType.Text, SQLCMeasure);
                CMeasure.TableName = "底栏数据";
                result.Tables.Add(CMeasure);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return result;
        }
        #endregion
    }
}
