﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;


namespace HIS.COMM.TWD
{
    public partial class XtraForm事件设置 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm事件设置()
        {
            InitializeComponent();
        }
        private XtraForm批量体温 parent;

        /// <summary>
        ///确认按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn确认_Click(object sender, EventArgs e)
        {
            //设置父类
            parent = this.Tag as XtraForm批量体温;
            //string name = "";
            string name = this.com名称.Text + "--";
            DateTime time = DateTime.Parse(this.time时间.EditValue.ToString());
            string  strtime = WEISHENG.COMM.dateTimeHelper.dateToUpper(time);
            
            parent.标注文本 = name + strtime;
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn取消_Click(object sender, EventArgs e)
        {
            //画面关闭
            this.Close();
        }

    

        /// <summary>
        /// 画面项目初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XtraForm事件设置_Load(object sender, EventArgs e)
        {
            date日期.Text = DateTime.Now.ToShortDateString();
        }
        
    }
}