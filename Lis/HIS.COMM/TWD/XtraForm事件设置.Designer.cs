﻿namespace HIS.COMM.TWD
{
    partial class XtraForm事件设置
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.com名称 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.date日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.time时间 = new DevExpress.XtraEditors.TimeEdit();
            this.btn确认 = new DevExpress.XtraEditors.SimpleButton();
            this.btn取消 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.com名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.time时间.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(48, 37);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(36, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "名称：";
            // 
            // com名称
            // 
            this.com名称.EditValue = "入院";
            this.com名称.Location = new System.Drawing.Point(90, 34);
            this.com名称.Name = "com名称";
            this.com名称.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.com名称.Properties.Items.AddRange(new object[] {
            "入院",
            "手术",
            "分娩",
            "出院"});
            this.com名称.Size = new System.Drawing.Size(142, 20);
            this.com名称.TabIndex = 5;
            // 
            // date日期
            // 
            this.date日期.EditValue = new System.DateTime(2015, 6, 16, 18, 49, 47, 64);
            this.date日期.Location = new System.Drawing.Point(90, 87);
            this.date日期.Name = "date日期";
            this.date日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date日期.Size = new System.Drawing.Size(142, 20);
            this.date日期.TabIndex = 6;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(48, 90);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(36, 14);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "日期：";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(48, 150);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 14);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "时间：";
            // 
            // time时间
            // 
            this.time时间.EditValue = new System.DateTime(2015, 6, 16, 0, 0, 0, 0);
            this.time时间.Location = new System.Drawing.Point(90, 147);
            this.time时间.Name = "time时间";
            this.time时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.time时间.Properties.DisplayFormat.FormatString = "t";
            this.time时间.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time时间.Properties.EditFormat.FormatString = "t";
            this.time时间.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time时间.Size = new System.Drawing.Size(142, 20);
            this.time时间.TabIndex = 9;
            // 
            // btn确认
            // 
            this.btn确认.Location = new System.Drawing.Point(116, 186);
            this.btn确认.Name = "btn确认";
            this.btn确认.Size = new System.Drawing.Size(75, 23);
            this.btn确认.TabIndex = 10;
            this.btn确认.Text = "确定";
            this.btn确认.Click += new System.EventHandler(this.btn确认_Click);
            // 
            // btn取消
            // 
            this.btn取消.Location = new System.Drawing.Point(197, 186);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(75, 23);
            this.btn取消.TabIndex = 10;
            this.btn取消.Text = "取消";
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // XtraForm事件设置
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 228);
            this.Controls.Add(this.btn取消);
            this.Controls.Add(this.btn确认);
            this.Controls.Add(this.time时间);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.date日期);
            this.Controls.Add(this.com名称);
            this.Controls.Add(this.labelControl1);
            this.Name = "XtraForm事件设置";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "事件设置";
            this.Load += new System.EventHandler(this.XtraForm事件设置_Load);
            ((System.ComponentModel.ISupportInitialize)(this.com名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.time时间.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit com名称;
        private DevExpress.XtraEditors.DateEdit date日期;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TimeEdit time时间;
        private DevExpress.XtraEditors.SimpleButton btn确认;
        private DevExpress.XtraEditors.SimpleButton btn取消;
    }
}