using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml.Serialization;
using System.Text;
using Microsoft.VisualBasic;
using System.IO;
using System.Data.SqlClient;
using Comm = HIS.COMM;

namespace HIS.COMM.TWD
{
    /// <summary>
    /// InputFrm 的摘要说明。
    /// </summary>
    public class InputFrm : System.Windows.Forms.Form
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.Container components = null;

        public InputFrm()
        {
            //
            // Windows 窗体设计器支持所必需的
            //
            InitializeComponent();

            //
            // TODO: 在 InitializeComponent 调用后添加任何构造函数代码
            //
        }

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码
        /// <summary>
        /// 设计器支持所需的方法 - 不要使用代码编辑器修改
        /// 此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.grd三测数据 = new System.Windows.Forms.DataGrid();
            this.grd测量列表 = new System.Windows.Forms.DataGrid();
            this.grd底栏 = new System.Windows.Forms.DataGrid();
            this.buttonAppend三测 = new System.Windows.Forms.Button();
            this.buttonModify三测 = new System.Windows.Forms.Button();
            this.buttonDelete三测 = new System.Windows.Forms.Button();
            this.buttonCancel三测 = new System.Windows.Forms.Button();
            this.buttonSave三测 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Date测量日期 = new System.Windows.Forms.DateTimePicker();
            this.ComTime测量时间 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox三测 = new System.Windows.Forms.GroupBox();
            this.comboBox物理降温 = new System.Windows.Forms.ComboBox();
            this.textBox物理降温 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBox脉搏 = new System.Windows.Forms.ComboBox();
            this.comboBox呼吸 = new System.Windows.Forms.ComboBox();
            this.comboBox体温 = new System.Windows.Forms.ComboBox();
            this.textBoxMemo标注文字 = new System.Windows.Forms.TextBox();
            this.textBox脉搏 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox呼吸 = new System.Windows.Forms.TextBox();
            this.textBox体温 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox底栏 = new System.Windows.Forms.GroupBox();
            this.buttonCancel底栏 = new System.Windows.Forms.Button();
            this.buttonSave底栏 = new System.Windows.Forms.Button();
            this.buttonDelete底栏 = new System.Windows.Forms.Button();
            this.buttonModify底栏 = new System.Windows.Forms.Button();
            this.buttonAppend底栏 = new System.Windows.Forms.Button();
            this.comboBox大便 = new System.Windows.Forms.ComboBox();
            this.comboBox小便 = new System.Windows.Forms.ComboBox();
            this.textBox痰量 = new System.Windows.Forms.TextBox();
            this.textBox引流量 = new System.Windows.Forms.TextBox();
            this.textBox体重 = new System.Windows.Forms.TextBox();
            this.textBox呕吐量 = new System.Windows.Forms.TextBox();
            this.textBox液入量 = new System.Windows.Forms.TextBox();
            this.textBox血压 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox小便 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox大便 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.grd三测数据)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd测量列表)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd底栏)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox三测.SuspendLayout();
            this.groupBox底栏.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // grd三测数据
            // 
            this.grd三测数据.AllowSorting = false;
            this.grd三测数据.CaptionVisible = false;
            this.grd三测数据.DataMember = "";
            this.grd三测数据.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grd三测数据.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.grd三测数据.Location = new System.Drawing.Point(0, 0);
            this.grd三测数据.Name = "grd三测数据";
            this.grd三测数据.ParentRowsVisible = false;
            this.grd三测数据.ReadOnly = true;
            this.grd三测数据.RowHeadersVisible = false;
            this.grd三测数据.Size = new System.Drawing.Size(562, 192);
            this.grd三测数据.TabIndex = 2;
            this.grd三测数据.CurrentCellChanged += new System.EventHandler(this.dataGrid三测_CurrentCellChanged);
            this.grd三测数据.Click += new System.EventHandler(this.grd三测数据_Click);
            // 
            // grd测量列表
            // 
            this.grd测量列表.AllowSorting = false;
            this.grd测量列表.CaptionVisible = false;
            this.grd测量列表.DataMember = "";
            this.grd测量列表.Dock = System.Windows.Forms.DockStyle.Left;
            this.grd测量列表.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.grd测量列表.Location = new System.Drawing.Point(0, 0);
            this.grd测量列表.Name = "grd测量列表";
            this.grd测量列表.ParentRowsVisible = false;
            this.grd测量列表.ReadOnly = true;
            this.grd测量列表.RowHeadersVisible = false;
            this.grd测量列表.Size = new System.Drawing.Size(96, 192);
            this.grd测量列表.TabIndex = 0;
            this.grd测量列表.CurrentCellChanged += new System.EventHandler(this.dataGrid测量列表_CurrentCellChanged);
            // 
            // grd底栏
            // 
            this.grd底栏.CaptionVisible = false;
            this.grd底栏.DataMember = "";
            this.grd底栏.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.grd底栏.Location = new System.Drawing.Point(0, 128);
            this.grd底栏.Name = "grd底栏";
            this.grd底栏.ReadOnly = true;
            this.grd底栏.RowHeadersVisible = false;
            this.grd底栏.Size = new System.Drawing.Size(562, 64);
            this.grd底栏.TabIndex = 3;
            // 
            // buttonAppend三测
            // 
            this.buttonAppend三测.Location = new System.Drawing.Point(266, 99);
            this.buttonAppend三测.Name = "buttonAppend三测";
            this.buttonAppend三测.Size = new System.Drawing.Size(75, 23);
            this.buttonAppend三测.TabIndex = 0;
            this.buttonAppend三测.Text = "新增";
            this.buttonAppend三测.Click += new System.EventHandler(this.buttonAppend三测_Click);
            // 
            // buttonModify三测
            // 
            this.buttonModify三测.Location = new System.Drawing.Point(340, 99);
            this.buttonModify三测.Name = "buttonModify三测";
            this.buttonModify三测.Size = new System.Drawing.Size(75, 23);
            this.buttonModify三测.TabIndex = 1;
            this.buttonModify三测.Text = "修改";
            this.buttonModify三测.Click += new System.EventHandler(this.buttonModify三测_Click);
            // 
            // buttonDelete三测
            // 
            this.buttonDelete三测.Location = new System.Drawing.Point(413, 99);
            this.buttonDelete三测.Name = "buttonDelete三测";
            this.buttonDelete三测.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete三测.TabIndex = 2;
            this.buttonDelete三测.Text = "删除";
            this.buttonDelete三测.Click += new System.EventHandler(this.buttonDelete三测_Click);
            // 
            // buttonCancel三测
            // 
            this.buttonCancel三测.Enabled = false;
            this.buttonCancel三测.Location = new System.Drawing.Point(486, 99);
            this.buttonCancel三测.Name = "buttonCancel三测";
            this.buttonCancel三测.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel三测.TabIndex = 3;
            this.buttonCancel三测.Text = "取消";
            this.buttonCancel三测.Click += new System.EventHandler(this.buttonCancel三测_Click);
            // 
            // buttonSave三测
            // 
            this.buttonSave三测.Enabled = false;
            this.buttonSave三测.Location = new System.Drawing.Point(558, 99);
            this.buttonSave三测.Name = "buttonSave三测";
            this.buttonSave三测.Size = new System.Drawing.Size(75, 23);
            this.buttonSave三测.TabIndex = 4;
            this.buttonSave三测.Text = "保存";
            this.buttonSave三测.Click += new System.EventHandler(this.buttonSave三测_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Date测量日期);
            this.panel1.Controls.Add(this.ComTime测量时间);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.groupBox三测);
            this.panel1.Controls.Add(this.groupBox底栏);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 192);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(658, 311);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "测量日期：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Date测量日期
            // 
            this.Date测量日期.Location = new System.Drawing.Point(96, 11);
            this.Date测量日期.Name = "Date测量日期";
            this.Date测量日期.Size = new System.Drawing.Size(112, 21);
            this.Date测量日期.TabIndex = 0;
            this.Date测量日期.ValueChanged += new System.EventHandler(this.Date测量日期_ValueChanged);
            this.Date测量日期.LostFocus += new System.EventHandler(this.Dat测量日期_LostFocus);
            // 
            // ComTime测量时间
            // 
            this.ComTime测量时间.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComTime测量时间.Items.AddRange(new object[] {
            "03",
            "07",
            "11",
            "15",
            "19",
            "23"});
            this.ComTime测量时间.Location = new System.Drawing.Point(256, 11);
            this.ComTime测量时间.Name = "ComTime测量时间";
            this.ComTime测量时间.Size = new System.Drawing.Size(54, 20);
            this.ComTime测量时间.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "时间：";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox三测
            // 
            this.groupBox三测.Controls.Add(this.buttonCancel三测);
            this.groupBox三测.Controls.Add(this.buttonSave三测);
            this.groupBox三测.Controls.Add(this.comboBox物理降温);
            this.groupBox三测.Controls.Add(this.buttonDelete三测);
            this.groupBox三测.Controls.Add(this.textBox物理降温);
            this.groupBox三测.Controls.Add(this.label13);
            this.groupBox三测.Controls.Add(this.buttonModify三测);
            this.groupBox三测.Controls.Add(this.comboBox脉搏);
            this.groupBox三测.Controls.Add(this.comboBox呼吸);
            this.groupBox三测.Controls.Add(this.buttonAppend三测);
            this.groupBox三测.Controls.Add(this.comboBox体温);
            this.groupBox三测.Controls.Add(this.textBoxMemo标注文字);
            this.groupBox三测.Controls.Add(this.textBox脉搏);
            this.groupBox三测.Controls.Add(this.label6);
            this.groupBox三测.Controls.Add(this.label4);
            this.groupBox三测.Controls.Add(this.textBox呼吸);
            this.groupBox三测.Controls.Add(this.textBox体温);
            this.groupBox三测.Controls.Add(this.label5);
            this.groupBox三测.Controls.Add(this.label3);
            this.groupBox三测.Location = new System.Drawing.Point(13, 37);
            this.groupBox三测.Name = "groupBox三测";
            this.groupBox三测.Size = new System.Drawing.Size(633, 136);
            this.groupBox三测.TabIndex = 5;
            this.groupBox三测.TabStop = false;
            this.groupBox三测.Text = "【体温、脉搏、呼吸】数据";
            // 
            // comboBox物理降温
            // 
            this.comboBox物理降温.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox物理降温.Enabled = false;
            this.comboBox物理降温.Items.AddRange(new object[] {
            "腋温×",
            "肛温○",
            "口温●"});
            this.comboBox物理降温.Location = new System.Drawing.Point(112, 51);
            this.comboBox物理降温.Name = "comboBox物理降温";
            this.comboBox物理降温.Size = new System.Drawing.Size(60, 20);
            this.comboBox物理降温.TabIndex = 2;
            this.comboBox物理降温.Visible = false;
            // 
            // textBox物理降温
            // 
            this.textBox物理降温.Location = new System.Drawing.Point(112, 51);
            this.textBox物理降温.MaxLength = 4;
            this.textBox物理降温.Name = "textBox物理降温";
            this.textBox物理降温.ReadOnly = true;
            this.textBox物理降温.Size = new System.Drawing.Size(110, 21);
            this.textBox物理降温.TabIndex = 3;
            this.textBox物理降温.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 12);
            this.label13.TabIndex = 13;
            this.label13.Text = "物理降温(℃)：";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBox脉搏
            // 
            this.comboBox脉搏.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox脉搏.Enabled = false;
            this.comboBox脉搏.Items.AddRange(new object[] {
            "脉搏●",
            "心率○"});
            this.comboBox脉搏.Location = new System.Drawing.Point(112, 77);
            this.comboBox脉搏.Name = "comboBox脉搏";
            this.comboBox脉搏.Size = new System.Drawing.Size(60, 20);
            this.comboBox脉搏.TabIndex = 4;
            this.comboBox脉搏.Visible = false;
            // 
            // comboBox呼吸
            // 
            this.comboBox呼吸.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox呼吸.Enabled = false;
            this.comboBox呼吸.Items.AddRange(new object[] {
            "正常●",
            "辅助Ａ"});
            this.comboBox呼吸.Location = new System.Drawing.Point(112, 103);
            this.comboBox呼吸.Name = "comboBox呼吸";
            this.comboBox呼吸.Size = new System.Drawing.Size(60, 20);
            this.comboBox呼吸.TabIndex = 6;
            this.comboBox呼吸.Visible = false;
            // 
            // comboBox体温
            // 
            this.comboBox体温.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox体温.Enabled = false;
            this.comboBox体温.Items.AddRange(new object[] {
            "腋温×",
            "肛温○",
            "口温●"});
            this.comboBox体温.Location = new System.Drawing.Point(112, 25);
            this.comboBox体温.Name = "comboBox体温";
            this.comboBox体温.Size = new System.Drawing.Size(60, 20);
            this.comboBox体温.TabIndex = 0;
            this.comboBox体温.Visible = false;
            // 
            // textBoxMemo标注文字
            // 
            this.textBoxMemo标注文字.Location = new System.Drawing.Point(343, 22);
            this.textBoxMemo标注文字.MaxLength = 20;
            this.textBoxMemo标注文字.Multiline = true;
            this.textBoxMemo标注文字.Name = "textBoxMemo标注文字";
            this.textBoxMemo标注文字.ReadOnly = true;
            this.textBoxMemo标注文字.Size = new System.Drawing.Size(269, 67);
            this.textBoxMemo标注文字.TabIndex = 8;
            // 
            // textBox脉搏
            // 
            this.textBox脉搏.Location = new System.Drawing.Point(112, 77);
            this.textBox脉搏.MaxLength = 3;
            this.textBox脉搏.Name = "textBox脉搏";
            this.textBox脉搏.ReadOnly = true;
            this.textBox脉搏.Size = new System.Drawing.Size(110, 21);
            this.textBox脉搏.TabIndex = 5;
            this.textBox脉搏.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(288, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "标注文字：";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "脉搏(次/分)：";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox呼吸
            // 
            this.textBox呼吸.Location = new System.Drawing.Point(112, 103);
            this.textBox呼吸.MaxLength = 2;
            this.textBox呼吸.Name = "textBox呼吸";
            this.textBox呼吸.ReadOnly = true;
            this.textBox呼吸.Size = new System.Drawing.Size(110, 21);
            this.textBox呼吸.TabIndex = 7;
            this.textBox呼吸.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox体温
            // 
            this.textBox体温.Location = new System.Drawing.Point(112, 25);
            this.textBox体温.MaxLength = 4;
            this.textBox体温.Name = "textBox体温";
            this.textBox体温.ReadOnly = true;
            this.textBox体温.Size = new System.Drawing.Size(110, 21);
            this.textBox体温.TabIndex = 1;
            this.textBox体温.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "呼吸(次/分)：";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "体温(℃)：";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox底栏
            // 
            this.groupBox底栏.Controls.Add(this.buttonCancel底栏);
            this.groupBox底栏.Controls.Add(this.buttonSave底栏);
            this.groupBox底栏.Controls.Add(this.buttonDelete底栏);
            this.groupBox底栏.Controls.Add(this.buttonModify底栏);
            this.groupBox底栏.Controls.Add(this.buttonAppend底栏);
            this.groupBox底栏.Controls.Add(this.comboBox大便);
            this.groupBox底栏.Controls.Add(this.comboBox小便);
            this.groupBox底栏.Controls.Add(this.textBox痰量);
            this.groupBox底栏.Controls.Add(this.textBox引流量);
            this.groupBox底栏.Controls.Add(this.textBox体重);
            this.groupBox底栏.Controls.Add(this.textBox呕吐量);
            this.groupBox底栏.Controls.Add(this.textBox液入量);
            this.groupBox底栏.Controls.Add(this.textBox血压);
            this.groupBox底栏.Controls.Add(this.label15);
            this.groupBox底栏.Controls.Add(this.textBox小便);
            this.groupBox底栏.Controls.Add(this.label9);
            this.groupBox底栏.Controls.Add(this.textBox大便);
            this.groupBox底栏.Controls.Add(this.label8);
            this.groupBox底栏.Controls.Add(this.label7);
            this.groupBox底栏.Controls.Add(this.label10);
            this.groupBox底栏.Controls.Add(this.label12);
            this.groupBox底栏.Controls.Add(this.label11);
            this.groupBox底栏.Controls.Add(this.label16);
            this.groupBox底栏.Location = new System.Drawing.Point(13, 179);
            this.groupBox底栏.Name = "groupBox底栏";
            this.groupBox底栏.Size = new System.Drawing.Size(633, 128);
            this.groupBox底栏.TabIndex = 6;
            this.groupBox底栏.TabStop = false;
            this.groupBox底栏.Text = "【底栏】数据";
            // 
            // buttonCancel底栏
            // 
            this.buttonCancel底栏.Enabled = false;
            this.buttonCancel底栏.Location = new System.Drawing.Point(485, 101);
            this.buttonCancel底栏.Name = "buttonCancel底栏";
            this.buttonCancel底栏.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel底栏.TabIndex = 20;
            this.buttonCancel底栏.Text = "取消";
            this.buttonCancel底栏.Click += new System.EventHandler(this.buttonCancel底栏_Click);
            // 
            // buttonSave底栏
            // 
            this.buttonSave底栏.Enabled = false;
            this.buttonSave底栏.Location = new System.Drawing.Point(557, 101);
            this.buttonSave底栏.Name = "buttonSave底栏";
            this.buttonSave底栏.Size = new System.Drawing.Size(75, 23);
            this.buttonSave底栏.TabIndex = 21;
            this.buttonSave底栏.Text = "保存";
            this.buttonSave底栏.Click += new System.EventHandler(this.buttonSave底栏_Click);
            // 
            // buttonDelete底栏
            // 
            this.buttonDelete底栏.Location = new System.Drawing.Point(412, 101);
            this.buttonDelete底栏.Name = "buttonDelete底栏";
            this.buttonDelete底栏.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete底栏.TabIndex = 19;
            this.buttonDelete底栏.Text = "删除";
            this.buttonDelete底栏.Click += new System.EventHandler(this.buttonDelete底栏_Click);
            // 
            // buttonModify底栏
            // 
            this.buttonModify底栏.Location = new System.Drawing.Point(339, 101);
            this.buttonModify底栏.Name = "buttonModify底栏";
            this.buttonModify底栏.Size = new System.Drawing.Size(75, 23);
            this.buttonModify底栏.TabIndex = 18;
            this.buttonModify底栏.Text = "修改";
            this.buttonModify底栏.Click += new System.EventHandler(this.buttonModify底栏_Click);
            // 
            // buttonAppend底栏
            // 
            this.buttonAppend底栏.Location = new System.Drawing.Point(265, 101);
            this.buttonAppend底栏.Name = "buttonAppend底栏";
            this.buttonAppend底栏.Size = new System.Drawing.Size(75, 23);
            this.buttonAppend底栏.TabIndex = 17;
            this.buttonAppend底栏.Text = "新增";
            this.buttonAppend底栏.Click += new System.EventHandler(this.buttonAppend底栏_Click);
            // 
            // comboBox大便
            // 
            this.comboBox大便.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox大便.Enabled = false;
            this.comboBox大便.Items.AddRange(new object[] {
            "正常",
            "灌肠E",
            "失禁*",
            "假肛*"});
            this.comboBox大便.Location = new System.Drawing.Point(502, 50);
            this.comboBox大便.Name = "comboBox大便";
            this.comboBox大便.Size = new System.Drawing.Size(60, 20);
            this.comboBox大便.TabIndex = 2;
            this.comboBox大便.Visible = false;
            // 
            // comboBox小便
            // 
            this.comboBox小便.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox小便.Enabled = false;
            this.comboBox小便.Items.AddRange(new object[] {
            "正常+",
            "未解-",
            "失禁*"});
            this.comboBox小便.Location = new System.Drawing.Point(112, 23);
            this.comboBox小便.Name = "comboBox小便";
            this.comboBox小便.Size = new System.Drawing.Size(60, 20);
            this.comboBox小便.TabIndex = 0;
            this.comboBox小便.Visible = false;
            // 
            // textBox痰量
            // 
            this.textBox痰量.Location = new System.Drawing.Point(112, 50);
            this.textBox痰量.MaxLength = 4;
            this.textBox痰量.Name = "textBox痰量";
            this.textBox痰量.ReadOnly = true;
            this.textBox痰量.Size = new System.Drawing.Size(110, 21);
            this.textBox痰量.TabIndex = 2;
            this.textBox痰量.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox引流量
            // 
            this.textBox引流量.Location = new System.Drawing.Point(304, 20);
            this.textBox引流量.MaxLength = 4;
            this.textBox引流量.Name = "textBox引流量";
            this.textBox引流量.ReadOnly = true;
            this.textBox引流量.Size = new System.Drawing.Size(110, 21);
            this.textBox引流量.TabIndex = 3;
            this.textBox引流量.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox体重
            // 
            this.textBox体重.Location = new System.Drawing.Point(504, 20);
            this.textBox体重.MaxLength = 4;
            this.textBox体重.Name = "textBox体重";
            this.textBox体重.ReadOnly = true;
            this.textBox体重.Size = new System.Drawing.Size(110, 21);
            this.textBox体重.TabIndex = 5;
            this.textBox体重.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox呕吐量
            // 
            this.textBox呕吐量.Location = new System.Drawing.Point(304, 46);
            this.textBox呕吐量.MaxLength = 5;
            this.textBox呕吐量.Name = "textBox呕吐量";
            this.textBox呕吐量.ReadOnly = true;
            this.textBox呕吐量.Size = new System.Drawing.Size(110, 21);
            this.textBox呕吐量.TabIndex = 4;
            this.textBox呕吐量.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox液入量
            // 
            this.textBox液入量.Location = new System.Drawing.Point(331, 76);
            this.textBox液入量.MaxLength = 4;
            this.textBox液入量.Name = "textBox液入量";
            this.textBox液入量.ReadOnly = true;
            this.textBox液入量.Size = new System.Drawing.Size(110, 21);
            this.textBox液入量.TabIndex = 7;
            this.textBox液入量.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox血压
            // 
            this.textBox血压.Location = new System.Drawing.Point(126, 76);
            this.textBox血压.MaxLength = 9999;
            this.textBox血压.Name = "textBox血压";
            this.textBox血压.ReadOnly = true;
            this.textBox血压.Size = new System.Drawing.Size(110, 21);
            this.textBox血压.TabIndex = 8;
            this.textBox血压.TextChanged += new System.EventHandler(this.textBoxBlood_PressureA_TextChanged);
            this.textBox血压.GotFocus += new System.EventHandler(this.textBoxBlood_PressureA_GotFocus);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(255, 79);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 12);
            this.label15.TabIndex = 15;
            this.label15.Text = "液入量(ml)：";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox小便
            // 
            this.textBox小便.Location = new System.Drawing.Point(112, 23);
            this.textBox小便.MaxLength = 4;
            this.textBox小便.Name = "textBox小便";
            this.textBox小便.ReadOnly = true;
            this.textBox小便.Size = new System.Drawing.Size(110, 21);
            this.textBox小便.TabIndex = 1;
            this.textBox小便.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(228, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 11;
            this.label9.Text = "引流量(ml)：";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox大便
            // 
            this.textBox大便.Location = new System.Drawing.Point(502, 50);
            this.textBox大便.MaxLength = 2;
            this.textBox大便.Name = "textBox大便";
            this.textBox大便.ReadOnly = true;
            this.textBox大便.Size = new System.Drawing.Size(110, 21);
            this.textBox大便.TabIndex = 6;
            this.textBox大便.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 12);
            this.label8.TabIndex = 10;
            this.label8.Text = "血压(mmHg)：";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(48, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "小便(ml)：";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(48, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "痰量(ml)：";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(438, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 12;
            this.label12.Text = "大便(次)：";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(227, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 12);
            this.label11.TabIndex = 13;
            this.label11.Text = "呕吐量(ml)：";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(438, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 12);
            this.label16.TabIndex = 16;
            this.label16.Text = "体重(Kg)：";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.grd测量列表);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(658, 192);
            this.panel3.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.grd三测数据);
            this.panel4.Controls.Add(this.grd底栏);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(96, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(562, 192);
            this.panel4.TabIndex = 1;
            // 
            // InputFrm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
            this.ClientSize = new System.Drawing.Size(658, 503);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "InputFrm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "体温记录单数据录入";
            this.Closed += new System.EventHandler(this.InputFrm_Closed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InputFrm_FormClosing);
            this.Load += new System.EventHandler(this.InputFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grd三测数据)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd测量列表)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd底栏)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox三测.ResumeLayout(false);
            this.groupBox三测.PerformLayout();
            this.groupBox底栏.ResumeLayout(false);
            this.groupBox底栏.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        #region 控件定义       
        private System.Windows.Forms.DataGrid grd测量列表;
        private System.Windows.Forms.DataGrid grd三测数据;
        private System.Windows.Forms.Button buttonAppend三测;
        private System.Windows.Forms.Button buttonModify三测;
        private System.Windows.Forms.Button buttonDelete三测;
        private System.Windows.Forms.Button buttonCancel三测;
        private System.Windows.Forms.DataGrid grd底栏;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker Date测量日期;
        private System.Windows.Forms.ComboBox ComTime测量时间;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox三测;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxMemo标注文字;
        private System.Windows.Forms.TextBox textBox脉搏;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox呼吸;
        private System.Windows.Forms.TextBox textBox体温;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox底栏;
        private System.Windows.Forms.TextBox textBox痰量;
        private System.Windows.Forms.TextBox textBox引流量;
        private System.Windows.Forms.TextBox textBox体重;
        private System.Windows.Forms.TextBox textBox呕吐量;
        private System.Windows.Forms.TextBox textBox血压;
        private System.Windows.Forms.TextBox textBox液入量;
        private System.Windows.Forms.TextBox textBox大便;
        private System.Windows.Forms.TextBox textBox小便;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox comboBox物理降温;
        private System.Windows.Forms.TextBox textBox物理降温;
        private System.Windows.Forms.ComboBox comboBox脉搏;
        private System.Windows.Forms.ComboBox comboBox呼吸;
        private System.Windows.Forms.ComboBox comboBox体温;
        private System.Windows.Forms.ComboBox comboBox小便;
        private System.Windows.Forms.ComboBox comboBox大便;
        private Button buttonCancel底栏;
        private Button buttonSave底栏;
        private Button buttonDelete底栏;
        private Button buttonModify底栏;
        private Button buttonAppend底栏;
        private System.Windows.Forms.Button buttonSave三测;
        #endregion

        /// <summary>
        /// 自定义变量
        /// </summary>



        Model体温单信息 _model体温单数据;
        /// <summary>
        /// 体温信息表初始化
        /// </summary>
        /// <returns></returns>
        private static DataTable createDt三测数据()
        {
            DataTable dt = new DataTable("三测数据");
            dt.Columns.Add("体温(℃)");
            dt.Columns.Add("物理降温(℃)");
            dt.Columns.Add("脉搏(次/分)");
            dt.Columns.Add("呼吸(次/分)");
            dt.Columns.Add("标注文字");
            dt.Columns.Add("测量日期");
            dt.Columns.Add("测量时间");
            dt.PrimaryKey = new DataColumn[] { dt.Columns["测量日期"], dt.Columns["测量时间"] };
            return dt;
        }
        private static DataTable createDt测量日期()
        {
            DataTable dt测量日期 = new DataTable("测量日期");
            dt测量日期.Columns.Add("测量日期");
            dt测量日期.PrimaryKey = new DataColumn[] { dt测量日期.Columns["测量日期"] };
            return dt测量日期;
        }

        /// <summary>
        /// 底栏项目
        /// </summary>
        /// <returns></returns>
        private static DataTable createDt底栏数据()
        {
            DataTable dt = new DataTable("底栏数据");
            dt.Columns.Add("小便(ml)");
            dt.Columns.Add("大便(次)");
            dt.Columns.Add("液入量(ml)");
            dt.Columns.Add("血压AM(mmHg)");
            dt.Columns.Add("呕吐量(ml)");
            dt.Columns.Add("体重(Kg)");
            dt.Columns.Add("痰量(ml)");
            dt.Columns.Add("引流量(ml)");
            dt.Columns.Add("测量日期");
            dt.PrimaryKey = new DataColumn[] { dt.Columns["测量日期"] };
            return dt;
        }

        /// <summary>
        /// 病人信息初始化
        /// </summary>
        /// <returns></returns>
        private static DataTable createDt病人信息()
        {
            DataTable dt = new DataTable("病人信息");
            dt.Columns.Add("Patient_Name");
            dt.Columns.Add("Patient_Dept");
            dt.Columns.Add("Patient_Bed");
            dt.Columns.Add("Patient_Mrno");
            dt.PrimaryKey = new DataColumn[] { dt.Columns["Patient_Mrno"] };
            return dt;
        }



        private void enableOper底栏TextBox(bool enable)
        {
            foreach (Control ctrl in groupBox底栏.Controls)
            {
                if (ctrl.GetType().Name == "TextBox")
                    (ctrl as TextBox).ReadOnly = !enable;
                else if (ctrl.GetType().Name == "ComboBox")
                    (ctrl as ComboBox).Enabled = enable;
            }
        }
        /// <summary>
        /// 文本框下拉框制御
        /// </summary>
        /// <param name="enable"></param>
        private void enableOper三测TextBox(bool enable)
        {
            foreach (Control ctrl in groupBox三测.Controls)
            {
                if (ctrl.GetType().Name == "TextBox")
                    (ctrl as TextBox).ReadOnly = !enable;
                else if (ctrl.GetType().Name == "ComboBox")
                    (ctrl as ComboBox).Enabled = enable;
            }
        }
        private void enableOper底栏Buttons(bool enable)
        {
            buttonAppend底栏.Enabled = !enable;
            buttonModify底栏.Enabled = !enable;
            buttonDelete底栏.Enabled = !enable;
            buttonCancel底栏.Enabled = enable;
            buttonSave底栏.Enabled = enable;
        }
        /// <summary>
        /// 画面按钮制御
        /// </summary>
        /// <param name="enable"></param>
        private void enableOper三测Buttons(bool enable)
        {
            buttonAppend三测.Enabled = !enable;
            buttonModify三测.Enabled = !enable;
            buttonDelete三测.Enabled = !enable;
            buttonCancel三测.Enabled = enable;
            buttonSave三测.Enabled = enable;
        }

        /// <summary>
        /// 画面项目清空
        /// </summary>
        /// <param name="groupBox"></param>
        private void emptyOperCtrls(GroupBox groupBox)
        {
            foreach (Control ctrl in groupBox.Controls)
            {
                if (ctrl.GetType().Name == "TextBox")
                    (ctrl as TextBox).Text = string.Empty;
                else if (ctrl.GetType().Name == "DateTimePicker")
                    (ctrl as DateTimePicker).Value = DateTime.MinValue.AddYears(1899);
                else if (ctrl.GetType().Name == "ComboBox")
                {
                    (ctrl as ComboBox).SelectedIndex = -1;
                }
            }
        }
        private enum en三测Oper
        {
            初始 = -1,
            增加 = 0,
            修改 = 1,
            删除 = 2
        }
        private enum en底栏Oper
        {
            初始 = -1,
            增加 = 0,
            修改 = 1,
            删除 = 2
        }
        private DataSet dataSet体温单信息 = new DataSet();
        private en三测Oper oper三测 = en三测Oper.初始;
        private en底栏Oper oper底栏 = en底栏Oper.初始;
        private string msgAlert = string.Empty;
        private bool saved = false;
        private UC_BPT uc_bpt体温单;
        private string _s病人信息 = string.Empty;
        public string _s当前用户 { get; set; }
        private string _s测量日期;
        private string _s测量时间;

        /// <summary>
        /// 获取病人信息
        /// </summary>
        /// <param name="mrId">住院号</param>
        private void vGet_s病人信息ByZYID(string mrId)
        {
            if (mrId == "" || mrId == string.Empty)
            {
                _s病人信息 = "张三" + ";" + "神经内一科" + ";" + "10" + ";" + "20098888";
            }
            else
            {
                DataTable dt病人信息 = uc_bpt体温单.GetDt病人信息ByZYID(mrId);
                _s病人信息 = dt病人信息.Rows[0]["病人姓名"].ToString() + ";" + dt病人信息.Rows[0]["住院日期"].ToString() + ";" + dt病人信息.Rows[0]["病床"].ToString() + ";" + mrId;
            }

        }

        /// <summary>
        /// 画面初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputFrm_Load(object sender, System.EventArgs e)
        {
            //设置父类
            uc_bpt体温单 = this.Tag as UC_BPT;//在调用录入界面时，把体温单作为tag传入进来
            //根据档案号获取病人信息
            vGet_s病人信息ByZYID(uc_bpt体温单.ZYID);
            //设置日期的最小值
            Date测量日期.Value = DateTime.Now;
            this._s当前用户 = uc_bpt体温单.s当前用户;
            //获取数据体k
            if (uc_bpt体温单.ds体温单信息.Tables.Count == 3)
            {
                try
                {
                    dataSet体温单信息 = uc_bpt体温单.ds体温单信息.Copy();//从体温单中复制数据，不是从数据库读取
                    checkDs体温单结构();
                    v更新测量列表();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }



        private void v显示底栏数据FromDt()
        {
            if (dataSet体温单信息.Tables["底栏数据"].Rows.Count == 0)
            {
                return;
            }
            DataRow dRow = dataSet体温单信息.Tables["底栏数据"].Rows.Find(new object[] { Date测量日期.Value.ToShortDateString() });
            if (dRow == null)
            {
                emptyOperCtrls(groupBox底栏);
                return;
            }
            textBox小便.Text = dRow["小便(ml)"].ToString();
            textBox大便.Text = dRow["大便(次)"].ToString();
            textBox液入量.Text = dRow["液入量(ml)"].ToString();
            textBox血压.Text = dRow["血压AM(mmHg)"].ToString();
            textBox体重.Text = dRow["体重(Kg)"].ToString();
            textBox痰量.Text = dRow["痰量(ml)"].ToString();
            textBox引流量.Text = dRow["引流量(ml)"].ToString();
            textBox呕吐量.Text = dRow["呕吐量(ml)"].ToString();
        }

        /// <summary>
        /// datatime设置
        /// </summary>
        private void v计算测量时间()
        {
            DateTime dt = DateTime.Now;
            Date测量日期.Value = dt;
            if (((dt.Hour == 3 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 3) && (dt.Hour < 7 || (dt.Hour == 7 && dt.Minute == 0 && dt.Second == 0)))
            {
                ComTime测量时间.Text = "07";
            }
            else if (((dt.Hour == 7 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 7) && (dt.Hour < 11 || (dt.Hour == 11 && dt.Minute == 0 && dt.Second == 0)))
            {
                ComTime测量时间.Text = "11";
            }
            else if (((dt.Hour == 11 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 11) && (dt.Hour < 15 || (dt.Hour == 15 && dt.Minute == 0 && dt.Second == 0)))
            {
                ComTime测量时间.Text = "15";
            }
            else if (((dt.Hour == 15 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 15) && (dt.Hour < 19 || (dt.Hour == 19 && dt.Minute == 0 && dt.Second == 0)))
            {
                ComTime测量时间.Text = "19";
            }
            else if (((dt.Hour == 19 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 19) && (dt.Hour < 23 || (dt.Hour == 23 && dt.Minute == 0 && dt.Second == 0)))
            {
                ComTime测量时间.Text = "23";
            }
            else
            {
                ComTime测量时间.Text = "03";
            }
        }



        /// <summary>
        /// 保存数据
        /// </summary>
        private Model体温单信息 UpdateDataModel()
        {

            Model体温单信息 model = new Model体温单信息();
            model.ZYID = uc_bpt体温单.ZYID;
            model.测量日期 = Date测量日期.Value.ToString("yyyy-MM-dd");
            model.测量时间 = ComTime测量时间.Text;
            model.体温 = string.IsNullOrEmpty(textBox体温.Text) ? "" : textBox体温.Text;
            model.物理降温 = string.IsNullOrEmpty(textBox物理降温.Text) ? "" : textBox物理降温.Text;
            model.脉搏 = string.IsNullOrEmpty(textBox脉搏.Text) ? "" : textBox脉搏.Text;
            model.呼吸 = string.IsNullOrEmpty(textBox呼吸.Text) ? "" : textBox呼吸.Text;
            model.大便 = string.IsNullOrEmpty(textBox大便.Text) ? "" : textBox大便.Text;
            model.小便 = string.IsNullOrEmpty(textBox小便.Text) ? "" : textBox小便.Text;
            model.液入量 = string.IsNullOrEmpty(textBox液入量.Text) ? "" : textBox液入量.Text;
            model.血压AM = string.IsNullOrEmpty(textBox血压.Text) ? "" : textBox血压.Text;
            model.呕吐量 = string.IsNullOrEmpty(textBox呕吐量.Text) ? "" : textBox呕吐量.Text;
            model.体重 = string.IsNullOrEmpty(textBox体重.Text) ? "" : textBox体重.Text;
            model.痰量 = string.IsNullOrEmpty(textBox痰量.Text) ? "" : textBox痰量.Text;
            model.引流 = string.IsNullOrEmpty(textBox引流量.Text) ? "" : textBox引流量.Text;
            model.标注文字 = textBoxMemo标注文字.Text;

            return model;
        }



        /// <summary>
        /// 保存其他信息
        /// </summary>
        private bool save底栏ToDB(Model体温单信息 _model)
        {
            bool bRe = true;
            try
            {
                //处理界面数据
                if (dataSet体温单信息.Tables["底栏数据"].Rows.Contains(new Object[] { Date测量日期.Value.ToString("yyyy-MM-dd") }) == false)
                {
                    dataSet体温单信息.Tables["底栏数据"].Rows.Add(new Object[] {  textBox小便.Text, textBox大便.Text, textBox液入量.Text,
                                                      textBox血压.Text, textBox呕吐量.Text, textBox体重.Text, textBox痰量.Text,
                                                      textBox引流量.Text, Date测量日期.Value.ToString("yyyy-MM-dd") });
                }
                else
                {
                    DataRow dRow = dataSet体温单信息.Tables["底栏数据"].Rows.Find(new object[] { Date测量日期.Value.ToString("yyyy-MM-dd") });
                    dRow.ItemArray = new Object[] {  textBox小便.Text, textBox大便.Text, textBox液入量.Text,
                                                      textBox血压.Text, textBox呕吐量.Text, textBox体重.Text, textBox痰量.Text,
                                                      textBox引流量.Text, Date测量日期.Value.ToString("yyyy-MM-dd")  };
                    dataSet体温单信息.Tables["底栏数据"].Rows.Find(new Object[] { Date测量日期.Value.ToString("yyyy-MM-dd") }).ItemArray = dRow.ItemArray;
                }

                //处理数据库

                SqlTransaction tran = HIS.Model.Dal.SqlHelper.BeginTransaction();
                bool flg = bUpdateOrInsert底栏dataToDB(_model, tran);
                if (flg)
                {
                    HIS.Model.Dal.SqlHelper.CommitTransaction(tran);
                    msgAlert += "【修改：底栏】修改成功！";
                    bRe = true;
                }
                else
                {
                    HIS.Model.Dal.SqlHelper.RollbackTransaction(tran);
                    msgAlert += "数据保存失败";
                    bRe = false;
                }

                return bRe;
            }
            catch (Exception ex)
            {
                msgAlert += ex.Message;
                return bRe;
            }
        }

        /// <summary>
        /// 更新体温单数据
        /// </summary>
        private bool save三测ToDB(Model体温单信息 _model)
        {
            DialogResult dlgResult = MessageBox.Show("您是否要保存当前【三测数据】数据修改？", "提示信息", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlgResult != DialogResult.Yes)
            {
                return false;
            }
            //处理界面数据
            if (dataSet体温单信息.Tables["三测数据"].Rows.Contains(new Object[] { Date测量日期.Value.ToString("yyyy-MM-dd"), ComTime测量时间.Text }) == false)
            {
                dataSet体温单信息.Tables["三测数据"].Rows.Add(new Object[] {textBox体温.Text, textBox物理降温.Text, textBox脉搏.Text,textBox呼吸.Text, textBoxMemo标注文字.Text,
                                         Date测量日期.Value.ToString("yyyy-MM-dd"), ComTime测量时间.Text });
            }
            else
            {
                DataRow dRow = dataSet体温单信息.Tables["三测数据"].Rows.Find(new object[] { Date测量日期.Value.ToString("yyyy-MM-dd"), ComTime测量时间.Text });
                dRow.ItemArray = new Object[] { textBox体温.Text, textBox物理降温.Text, textBox脉搏.Text,textBox呼吸.Text, textBoxMemo标注文字.Text,
                                                    Date测量日期.Value.ToString("yyyy-MM-dd"), ComTime测量时间.Text };
                dataSet体温单信息.Tables["三测数据"].Rows.Find(new Object[] { Date测量日期.Value.ToString("yyyy-MM-dd"), ComTime测量时间.Text }).ItemArray = dRow.ItemArray;
            }
            //更新数据库
            SqlTransaction tran = HIS.Model.Dal.SqlHelper.BeginTransaction();
            bool flg = false;
            flg = bUpdateOrInsertDB三测data(_model, tran);//更新数据库
            if (flg)
            {
                HIS.Model.Dal.SqlHelper.CommitTransaction(tran);
                MessageBox.Show("三测数据保存成功");
                return true;
            }
            else
            {
                HIS.Model.Dal.SqlHelper.RollbackTransaction(tran);
                MessageBox.Show("数据保存失败");
                return false;
            }
        }


        /// <summary>
        /// 初期化dataSet
        /// </summary>
        private void checkDs体温单结构()
        {

            if (dataSet体温单信息.Tables["病人信息"] == null)
            {
                dataSet体温单信息.Tables.Add(createDt病人信息());
            }
            if (dataSet体温单信息.Tables["三测数据"] == null)
            {
                dataSet体温单信息.Tables.Add(createDt三测数据());
            }
            if (dataSet体温单信息.Tables["底栏数据"] == null)
            {
                dataSet体温单信息.Tables.Add(createDt底栏数据());
            }
            if (dataSet体温单信息.Tables["测量日期"] == null)
            {
                dataSet体温单信息.Tables.Add(createDt测量日期());
            }
            if (dataSet体温单信息.Tables["病人信息"].PrimaryKey.Length == 0)
            {
                dataSet体温单信息.Tables["病人信息"].PrimaryKey = new DataColumn[] { dataSet体温单信息.Tables["病人信息"].Columns["Patient_Mrno"] };
            }
            if (dataSet体温单信息.Tables["三测数据"].PrimaryKey.Length == 0)
            {
                dataSet体温单信息.Tables["三测数据"].PrimaryKey = new DataColumn[] { dataSet体温单信息.Tables["三测数据"].Columns["测量日期"], dataSet体温单信息.Tables["三测数据"].Columns["测量时间"] };
            }
            if (dataSet体温单信息.Tables["底栏数据"].PrimaryKey.Length == 0)
            {
                dataSet体温单信息.Tables["底栏数据"].PrimaryKey = new DataColumn[] { dataSet体温单信息.Tables["底栏数据"].Columns["测量日期"] };
            }
            if (dataSet体温单信息.Tables["测量日期"].PrimaryKey.Length == 0)
            {
                dataSet体温单信息.Tables["测量日期"].PrimaryKey = new DataColumn[] { dataSet体温单信息.Tables["测量日期"].Columns["测量日期"] };
            }
        }

        /// <summary>
        /// 事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxS_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            PermitKey.v允许输入整数(sender, e);
        }

        /// <summary>
        /// 事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxT_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            PermitKey.PermitFloat(sender, e);
        }

        /// <summary>
        /// 日期选择改变时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid测量列表_CurrentCellChanged(object sender, System.EventArgs e)
        {
            v定位测量日期();
        }

        private void v定位测量日期()
        {
            if (grd测量列表.CurrentRowIndex > -1)
            {
                v更新三测底栏数据条件();
                if (grd三测数据.VisibleRowCount == 0)
                {
                    return;
                }
                grd三测数据.CurrentRowIndex = grd三测数据.VisibleRowCount - 1;
            }
        }
        /// <summary>
        /// 读取数据
        /// </summary>
        private void v更新测量列表()
        {
            for (int i = 0; i < dataSet体温单信息.Tables["三测数据"].Rows.Count; i++)
            {
                string 测量日期 = dataSet体温单信息.Tables["三测数据"].Rows[i]["测量日期"].ToString();
                //string _s测量时间 = dataSet体温单信息.Tables["三测数据"].Rows[i]["测量时间"].ToString();
                if (!dataSet体温单信息.Tables["测量日期"].Rows.Contains(new object[] { 测量日期 }))
                    dataSet体温单信息.Tables["测量日期"].Rows.Add(new object[] { 测量日期 });
            }
            for (int i = 0; i < dataSet体温单信息.Tables["底栏数据"].Rows.Count; i++)
            {
                string 测量日期 = dataSet体温单信息.Tables["底栏数据"].Rows[i]["测量日期"].ToString();
                if (!dataSet体温单信息.Tables["测量日期"].Rows.Contains(new object[] { 测量日期 }))
                    dataSet体温单信息.Tables["测量日期"].Rows.Add(new object[] { 测量日期 });
            }
            dataSet体温单信息.Tables["测量日期"].DefaultView.Sort = "测量日期";

            grd测量列表.DataSource = dataSet体温单信息.Tables["测量日期"];
            if (dataSet体温单信息.Tables["测量日期"].Rows.Count == 1)//当只有一行数据时，无法触发换号事件，所以手动触发一次
            {
                v定位测量日期();
            }
            grd测量列表.CurrentRowIndex = dataSet体温单信息.Tables["测量日期"].Rows.Count - 1;
        }


        /// <summary>
        /// 获取体温单信息
        /// </summary>
        private void v更新三测底栏数据条件()
        {
            dataSet体温单信息.Tables["三测数据"].DefaultView.Sort = "测量日期,测量时间";
            dataSet体温单信息.Tables["三测数据"].DefaultView.RowFilter = string.Format("测量日期 like '{0}'", grd测量列表[grd测量列表.CurrentRowIndex, 0].ToString());
            grd三测数据.DataSource = dataSet体温单信息.Tables["三测数据"];
            v更新三测文本框数据显示();
            dataSet体温单信息.Tables["底栏数据"].DefaultView.Sort = "测量日期";
            dataSet体温单信息.Tables["底栏数据"].DefaultView.RowFilter = string.Format("测量日期 like '{0}'", grd测量列表[grd测量列表.CurrentRowIndex, 0].ToString());
            grd底栏.DataSource = dataSet体温单信息.Tables["底栏数据"];

        }


        /// <summary>
        /// 画面项目初期化
        /// </summary>
        private void v清空三测底栏数据绑定()
        {
            try
            {
                for (int i = 0; i < groupBox三测.Controls.Count; i++)
                {
                    Type ctrlType = groupBox三测.Controls[i].GetType();
                    if (ctrlType.Name == "TextBox")
                    {
                        (groupBox三测.Controls[i] as TextBox).Text = string.Empty;
                    }
                    else if (ctrlType.Name == "DateTimePicker")
                    {
                        (groupBox三测.Controls[i] as DateTimePicker).Value = DateTime.MinValue.AddYears(1899);
                    }
                }
                for (int i = 0; i < groupBox底栏.Controls.Count; i++)
                {
                    Type ctrlType = groupBox底栏.Controls[i].GetType();
                    if (ctrlType.Name == "TextBox")
                    {
                        (groupBox底栏.Controls[i] as TextBox).Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 画面控件赋值
        /// </summary>
        private void v更新三测文本框数据显示()
        {
            if (dataSet体温单信息.Tables["三测数据"].Rows.Count == 0)
            {
                return;
            }

            DataRow dRow = dataSet体温单信息.Tables["三测数据"].Rows.Find(new object[] { grd三测数据[grd三测数据.CurrentRowIndex, 5].ToString(), grd三测数据[grd三测数据.CurrentRowIndex, 6].ToString() });
            if (dRow == null)
            {
                emptyOperCtrls(groupBox三测);
            }

            textBox体温.Text = dRow["体温(℃)"].ToString();
            textBox物理降温.Text = dRow["物理降温(℃)"].ToString();
            textBox脉搏.Text = dRow["脉搏(次/分)"].ToString();
            textBox呼吸.Text = dRow["呼吸(次/分)"].ToString();
            textBoxMemo标注文字.Text = dRow["标注文字"].ToString();

            Date测量日期.Value = Convert.ToDateTime(grd测量列表[grd测量列表.CurrentRowIndex, 0].ToString());
            if (grd三测数据.CurrentRowIndex > -1 && grd三测数据[grd三测数据.CurrentRowIndex, 6].ToString().Length > 0)
            {
                ComTime测量时间.Text = grd三测数据[grd三测数据.CurrentRowIndex, 6].ToString();
            }
            else
            {
                ComTime测量时间.Text = string.Empty;
            }


        }

        /// <summary>
        /// grid选择行改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid三测_CurrentCellChanged(object sender, System.EventArgs e)
        {
            v同步数据显示();
        }

        private void v同步数据显示()
        {
            try
            {
                v更新三测文本框数据显示();
                v显示底栏数据FromDt();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// 事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxS_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                System.Windows.Forms.SendKeys.Send("{TAB}");
            }
        }

        /// <summary>
        /// 画面关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputFrm_Closed(object sender, System.EventArgs e)
        {

            if (saved)
            {
                this.DialogResult = DialogResult.OK;
                uc_bpt体温单.ds体温单信息 = dataSet体温单信息.Copy();//将数据返回给体温单
            }
            else
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }






        private bool bUpdateOrInsert底栏dataToDB(Model体温单信息 model, SqlTransaction tran)
        {
            try
            {
                string SQLInsertTw = "IF EXISTS (" +
                " SELECT" +
                " *" +
                " FROM" +
                " HL体温单底栏数据" +
                " WHERE" +
                " ZYID ='" + model.ZYID + "'" +
                " AND 测量日期 = '" + model.测量日期 + "'" +
                " )" +
                " BEGIN" +
                " UPDATE HL体温单底栏数据" +
                " SET [小便(ml)] = '" + model.小便 + "'" +
                " ,[大便(次)] = '" + model.大便 + "'" +
                " ,[液入量(ml)] = '" + model.液入量 + "'" +
                " ,[血压AM(mmHg)] ='" + model.血压AM + "'" +
                " ,[体重(Kg)] = '" + model.体重 + "'" +
                " ,[痰量(ml)] = '" + model.痰量 + "'" +
                " ,[引流量(ml)] = '" + model.引流 + "'" +
                " ,[呕吐量(ml)] = '" + model.呕吐量 + "'" +
                " ,[作废标志] ='0'" +
                " ,[更新日期] = '" + DateTime.Now.ToString() + "'" +
                " ,[更新者] = '" + _s当前用户 + "'" +
                " WHERE" +
                " ZYID = '" + model.ZYID + "'" +
                " AND 测量日期 = '" + model.测量日期 + "'" +
                " END" +
                " ELSE" +
                " BEGIN" +
                " INSERT INTO HL体温单底栏数据 (" +
                " ZYID," +
                " 测量日期," +
                " [小便(ml)]," +
                " [大便(次)]," +
                " [液入量(ml)]," +
                " [血压AM(mmHg)]," +
                " [体重(Kg)]," +
                " [痰量(ml)]," +
                " [引流量(ml)]," +
                " [呕吐量(ml)]," +
                " [更新日期]," +
                " [更新者]," +
                " [登录时间]," +
                " [登录者],作废标志" +
                " )" +
                " VALUES" +
                " (" +
                " '" + model.ZYID + "'," +
                " '" + model.测量日期 + "'," +
                 " '" + model.小便 + "'," +
                " '" + model.大便 + "'," +
                 " '" + model.液入量 + "'," +
                 " '" + model.血压AM + "'," +
                 " '" + model.体重 + "'," +
                " '" + model.痰量 + "'," +
                " '" + model.引流 + "'," +
                 " '" + model.呕吐量 + "'," +
                 "'" + DateTime.Now.ToString() + "'," +
                  "'" + _s当前用户 + "'," +
                 "'" + DateTime.Now.ToString() + "'," +
                  "'" + _s当前用户 + "',0" +
                " ) " +
                " END ";
                return HIS.Model.Dal.SqlHelper.ExecuteNonQuery(tran, CommandType.Text, SQLInsertTw) > 0;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// 插入或更新数据库三测数据
        /// </summary>
        private bool bUpdateOrInsertDB三测data(Model体温单信息 model, SqlTransaction tran)
        {

            bool result = false;
            try
            {
                string SQLInsert =
                " IF EXISTS (" +
                " SELECT" +
                " *" +
                " FROM" +
                " HL体温单三测数据" +
                " WHERE" +
                " ZYID = '" + model.ZYID + "'" +
                " AND 测量日期 = '" + model.测量日期 + "'" +
                " AND 测量时间 = '" + model.测量时间 + "'" +
                " )" +
                " BEGIN" +
                " UPDATE HL体温单三测数据" +
                " SET [体温(℃)] = '" + model.体温 + "'" +
                " ,[物理降温(℃)] = '" + model.物理降温 + "'" +
                " ,[脉搏(次/分)] = '" + model.脉搏 + "'" +
                " ,[呼吸(次/分)] = '" + model.呼吸 + "'" +
                " ,[标注文字] = '" + model.标注文字 + "'" +
                " ,[作废标志] ='0'" +
                " ,[更新日期] = '" + DateTime.Now.ToString() + "'" +
                " ,[更新者] = '" + _s当前用户 + "'" +
                " WHERE" +
                " ZYID = '" + model.ZYID + "'" +
                " AND 测量日期 = '" + model.测量日期 + "'" +
                " AND 测量时间 = '" + model.测量时间 + "'" +
                " END" +
                " ELSE" +
                " BEGIN" +
                " INSERT INTO HL体温单三测数据 (" +
                " ZYID," +
                " 测量日期," +
                " 测量时间," +
                " [体温(℃)]," +
                " [物理降温(℃)]," +
                " [脉搏(次/分)]," +
                " [呼吸(次/分)]," +
                " [标注文字]," +
                " [更新日期]," +
                " [更新者]," +
                " [登录时间]," +
                " [登录者],作废标志" +
                " )" +
                " VALUES" +
                " (" +
                " '" + model.ZYID + "'," +
                " '" + model.测量日期 + "'," +
                " '" + model.测量时间 + "'," +
                " '" + model.体温 + "'," +
                " '" + model.物理降温 + "'," +
                " '" + model.脉搏 + "'," +
                " '" + model.呼吸 + "'," +
                " '" + model.标注文字 + "'," +
               "'" + DateTime.Now.ToString() + "'," +
                "'" + _s当前用户 + "'," +
               "'" + DateTime.Now.ToString() + "'," +
                "'" + _s当前用户 + "',0" +
                " )" +
                " END ";
                result = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(tran, CommandType.Text, SQLInsert) > 0;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return result;
        }
        private bool DelDB底栏数据(string _sDel测量日期)
        {
            bool b底栏更新记录大于0 = false;
            SqlTransaction tran = HIS.Model.Dal.SqlHelper.BeginTransaction();
            try
            {
                string SQLDEl血压 = " UPDATE HL体温单底栏数据" +
                  " SET 作废标志 = '1'" +
                  " WHERE" +
                  " ZYID = '" + uc_bpt体温单.ZYID + "'" +
                  " AND 测量日期 = '" + _sDel测量日期 + "'";
                b底栏更新记录大于0 = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(tran, CommandType.Text, SQLDEl血压) > 0;
                if (b底栏更新记录大于0) HIS.Model.Dal.SqlHelper.CommitTransaction(tran);
            }
            catch (Exception ex)
            {
                msgAlert += ex.Message;
                HIS.Model.Dal.SqlHelper.RollbackTransaction(tran);
                return false;
            }
            return b底栏更新记录大于0;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <returns></returns>
        private bool DelDB三测数据(string _sDel测量日期, string _sDel测量时间)
        {
            int i作废三测记录数 = 0;
            SqlTransaction tran = HIS.Model.Dal.SqlHelper.BeginTransaction();
            try
            {
                string SQLDEl三测 = " UPDATE HL体温单三测数据" +
                       " SET 作废标志 = '1'" +
                       " WHERE" +
                       " ZYID = '" + uc_bpt体温单.ZYID + "'" +
                       " AND 测量日期 = '" + _sDel测量日期 + "'" +
                       " AND 测量时间 = '" + _sDel测量时间 + "'";
                i作废三测记录数 = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(tran, CommandType.Text, SQLDEl三测);
                HIS.Model.Dal.SqlHelper.CommitTransaction(tran);
            }
            catch (Exception ex)
            {
                msgAlert += ex.Message;
                HIS.Model.Dal.SqlHelper.RollbackTransaction(tran);
                return false;
            }
            return i作废三测记录数 > 0;
        }

        private void textBoxBlood_PressureA_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxBlood_PressureA_GotFocus(object sender, EventArgs e)
        {
            if (textBox血压.Text.ToString().Trim() != "")
            {
                if (textBox血压.Text.ToString().Trim().LastIndexOf(" ") != 0)
                {
                    textBox血压.Text = textBox血压.Text.ToString().Trim() + " ";
                    textBox血压.SelectionStart = textBox血压.Text.Length - 1;
                }
            }
        }

        private void Date测量日期_ValueChanged(object sender, EventArgs e)
        {
            //loadOperPressure();
        }
        private void Dat测量日期_LostFocus(object sender, EventArgs e)
        {
            v显示底栏数据FromDt();
        }

        private void buttonAppend三测_Click(object sender, EventArgs e)
        {
            oper三测 = 0;
            enableOper三测TextBox(true);
            enableOper三测Buttons(true);
            emptyOperCtrls(groupBox三测);
            v计算测量时间();
            v显示底栏数据FromDt();
        }

        private void buttonCancel三测_Click(object sender, EventArgs e)
        {
            DialogResult dlgResult = MessageBox.Show("您是否要取消全部本次三测数据修改！", "提示信息", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlgResult != DialogResult.Yes)
            {
                return;
            }

            if (oper三测 == en三测Oper.修改 || oper三测 == en三测Oper.删除)
            {
                dataSet体温单信息.Tables["三测数据"].Clear();
                foreach (DataRow row in uc_bpt体温单.ds体温单信息.Tables["三测数据"].Rows)
                {
                    dataSet体温单信息.Tables["三测数据"].ImportRow(row);
                }
            }
            v更新测量列表();
            enableOper三测TextBox(false);
            enableOper三测Buttons(false);
            Date测量日期.Enabled = true;
            ComTime测量时间.Enabled = true;
            oper三测 = en三测Oper.初始;
        }

        private void buttonModify三测_Click(object sender, EventArgs e)
        {
            if (grd三测数据.VisibleRowCount > 0)
            {
                oper三测 = en三测Oper.修改;
                enableOper三测TextBox(true);
                enableOper三测Buttons(true);
                Date测量日期.Enabled = false;
                ComTime测量时间.Enabled = false;
            }
        }

        private void buttonDelete三测_Click(object sender, EventArgs e)
        {
            DialogResult dlgResult = MessageBox.Show("您是否要删除当前三测数据\r\n\r\n选择‘是’删除，‘否’取消！", "提示信息", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlgResult != DialogResult.Yes)
            {
                return;
            }
            oper三测 = en三测Oper.删除;
            string _sDel测量日期 = grd三测数据[grd三测数据.CurrentRowIndex, 5].ToString();
            string _sDel测量时间 = grd三测数据[grd三测数据.CurrentRowIndex, 6].ToString();


            //删除界面数据并标志到删除列表
            if (dataSet体温单信息.Tables["测量日期"].Rows.Count == 0) v清空三测底栏数据绑定();
            else
            {
                DataRow dRow = dataSet体温单信息.Tables["三测数据"].Rows.Find(new Object[] { _sDel测量日期, _sDel测量时间 });
                dataSet体温单信息.Tables["三测数据"].Rows.Remove(dRow);
                stuDelToDbList三测 stu = new stuDelToDbList三测();
                stu._sDel测量日期 = _sDel测量日期;
                stu._sDel测量时间 = _sDel测量时间;
                arrStuDel三测.Add(stu);
            }

            enableOper三测Buttons(true);
            Date测量日期.Enabled = false;
            ComTime测量时间.Enabled = false;
            emptyOperCtrls(groupBox三测);
        }

        struct stuDelToDbList三测
        {
            public string _sDel测量日期;
            public string _sDel测量时间;
        }
        struct stuDelToDbList底栏
        {
            public string _sDel测量日期;
        }


        ArrayList arrStuDel三测 = new ArrayList();
        ArrayList arrStuDel底栏 = new ArrayList();
        private void buttonSave三测_Click(object sender, EventArgs e)
        {
            msgAlert = string.Empty;
            try
            {
                if (textBox体温.Text.Trim().Length != 0 && (float.Parse(textBox体温.Text) > 42 || float.Parse(textBox体温.Text) < 32))
                {
                    msgAlert += "请检查【体温】数据是否正确！\r\n";
                }
                if (textBox物理降温.Text.Trim().Length != 0 && (float.Parse(textBox物理降温.Text) > 42 || float.Parse(textBox物理降温.Text) < 32))
                {
                    msgAlert += "请检查【体温】数据是否正确！\r\n";
                }
                if (textBox脉搏.Text.Trim().Length != 0 && (float.Parse(textBox脉搏.Text) > 160 || float.Parse(textBox脉搏.Text) < 40))
                {
                    msgAlert += "请检查【脉搏】数据是否正确！\r\n";
                }
                if (textBox呼吸.Text.Trim().Length != 0 && (float.Parse(textBox呼吸.Text) > 50 || float.Parse(textBox呼吸.Text) < 0))
                {
                    msgAlert += "请检查【呼吸】数据是否正确！\r\n";
                }
                //checkDs体温单结构();
                _model体温单数据 = UpdateDataModel();
                if (msgAlert == string.Empty)
                {
                    switch (oper三测)
                    {
                        case en三测Oper.修改:
                        case en三测Oper.增加:
                            if (save三测ToDB(_model体温单数据) == false)
                            {
                                return;
                            }
                            break;
                        case en三测Oper.删除:

                            foreach (var item in arrStuDel三测)
                            {
                                string _sDel日期 = ((stuDelToDbList三测)item)._sDel测量日期;
                                string _sDel时间 = ((stuDelToDbList三测)item)._sDel测量时间;

                                DelDB三测数据(_sDel日期, _sDel时间);

                            }
                            arrStuDel三测.Clear();

                            break;
                    }
                    v更新测量列表();
                    enableOper三测TextBox(false);
                    enableOper三测Buttons(false);
                    saved = true;
                    oper三测 = en三测Oper.初始;
                }
            }
            catch (System.Exception ex)
            {
                msgAlert += ex.Message + "\r\n";
            }
            finally
            {
                if (msgAlert.Length > 0)
                {
                    MessageBox.Show(msgAlert, "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void buttonAppend底栏_Click(object sender, EventArgs e)
        {
            oper底栏 = en底栏Oper.增加;
            enableOper底栏TextBox(true);
            enableOper底栏Buttons(true);
            emptyOperCtrls(groupBox底栏);
        }

        private void buttonCancel底栏_Click(object sender, EventArgs e)
        {
            if (oper底栏 == en底栏Oper.修改 || oper底栏 == en底栏Oper.删除)
            {
                dataSet体温单信息.Tables["底栏数据"].Clear();
                foreach (DataRow row in uc_bpt体温单.ds体温单信息.Tables["底栏数据"].Rows)
                {
                    dataSet体温单信息.Tables["底栏数据"].ImportRow(row);
                }
            }
            v更新测量列表();
            enableOper底栏TextBox(false);
            enableOper底栏Buttons(false);
            Date测量日期.Enabled = true;
            ComTime测量时间.Enabled = true;
            oper底栏 = en底栏Oper.初始;
        }

        private void buttonDelete底栏_Click(object sender, EventArgs e)
        {
            try
            {

                oper底栏 = en底栏Oper.删除;
                string _sDel测量日期 = grd测量列表[grd测量列表.CurrentRowIndex, 0].ToString();
                //删除界面数据并标志到删除列表
                if (dataSet体温单信息.Tables["测量日期"].Rows.Count == 0) v清空三测底栏数据绑定();
                else
                {
                    DataRow dRow = dataSet体温单信息.Tables["底栏数据"].Rows.Find(new Object[] { _sDel测量日期 });
                    dataSet体温单信息.Tables["底栏数据"].Rows.Remove(dRow);
                    stuDelToDbList底栏 stu = new stuDelToDbList底栏();
                    stu._sDel测量日期 = _sDel测量日期;
                    arrStuDel底栏.Add(stu);
                }

                enableOper底栏Buttons(true);
                Date测量日期.Enabled = false;
                ComTime测量时间.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonModify底栏_Click(object sender, EventArgs e)
        {
            try
            {
                oper底栏 = en底栏Oper.修改;
                enableOper底栏TextBox(true);
                enableOper底栏Buttons(true);
                Date测量日期.Enabled = false;
                ComTime测量时间.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void buttonSave底栏_Click(object sender, EventArgs e)
        {
            DialogResult dlgResult = MessageBox.Show("您是否要保存当前【底栏】数据修改", "提示信息", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlgResult != DialogResult.Yes)
            {
                return;
            }
            msgAlert = string.Empty;
            try
            {

                _model体温单数据 = UpdateDataModel();
                if (msgAlert == string.Empty)
                {
                    if (msgAlert == string.Empty)
                    {
                        switch (oper底栏)
                        {
                            case en底栏Oper.修改:
                            case en底栏Oper.增加:
                                if (save底栏ToDB(_model体温单数据) == false)
                                {
                                    return;
                                }
                                break;
                            case en底栏Oper.删除:
                                foreach (var item in arrStuDel底栏)
                                {
                                    string _sDel日期 = ((stuDelToDbList底栏)item)._sDel测量日期;
                                    DelDB底栏数据(_sDel日期);
                                }
                                arrStuDel底栏.Clear();
                                break;
                        }
                        v更新测量列表();
                        enableOper底栏TextBox(false);
                        enableOper底栏Buttons(false);
                        saved = true;
                        oper底栏 = en底栏Oper.初始;
                    }
                }
            }
            catch (System.Exception ex)
            {
                msgAlert += ex.Message + "\r\n";
            }
            finally
            {
                if (msgAlert.Length > 0)
                {
                    MessageBox.Show(msgAlert, "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void InputFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (oper三测 != en三测Oper.初始)
            {
                DialogResult dlgResult = MessageBox.Show("当前三测数据未保存，继续退出将取消当前数据操作！", "提示信息", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
            if (oper底栏 != en底栏Oper.初始)
            {
                DialogResult dlgResult = MessageBox.Show("当前底栏数据未保存，继续退出将取消当前数据操作！", "提示信息", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }

        }

        private void grd三测数据_Click(object sender, EventArgs e)
        {
            v同步数据显示();
        }


    }
}
