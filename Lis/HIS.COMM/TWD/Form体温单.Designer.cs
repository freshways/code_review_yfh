﻿namespace HIS.COMM.TWD
{
    partial class Form体温单
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form体温单));
            this.uC_BPT1 = new HIS.COMM.TWD.UC_BPT();
            this.SuspendLayout();
            // 
            // uC_BPT1
            // 
            this.uC_BPT1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uC_BPT1.AutoScroll = true;
            this.uC_BPT1.AutoScrollMargin = new System.Drawing.Size(0, 30);
            this.uC_BPT1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uC_BPT1.BottomLimitValue = 1020;
            this.uC_BPT1.BPT_File = "";
            this.uC_BPT1.CanEdit = false;
            this.uC_BPT1.Location = new System.Drawing.Point(4, 1);
            this.uC_BPT1.MainTitle = "Ｘ Ｘ Ｘ 人 民 医 院";
            this.uC_BPT1.Name = "uC_BPT1";
            this.uC_BPT1.ZYID = "";
            this.uC_BPT1.PageType = "MR";
            this.uC_BPT1.RightLimitValue = 750;
            this.uC_BPT1.SecondTitle = "体 温 记 录 单";
            this.uC_BPT1.Size = new System.Drawing.Size(864, 399);
            this.uC_BPT1.StartPoint = new System.Drawing.Point(50, 130);
            this.uC_BPT1.TabIndex = 0;
            this.uC_BPT1.s当前用户 = null;
            // 
            // Form体温单
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(868, 400);
            this.Controls.Add(this.uC_BPT1);
            this.Name = "Form体温单";
            this.Text = "体温单";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private UC_BPT uC_BPT1;




    }
}

