﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.Entity.SqlServer;
using static WEISHENG.COMM.LinqToDataTable;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.Generic;
using HIS.Model.Helper;
using HIS.COMM.Helper;

namespace HIS.COMM.TWD
{
    public partial class XtraForm批量体温 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm批量体温()
        {
            InitializeComponent();
        }
        DataTable tb_twd_info;


        /// <summary>
        /// 标注文字
        /// </summary>
        public string 标注文本 { get; set; }



        /// <summary>
        /// 画面初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XtraForm批量体温_Load(object sender, EventArgs e)
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text,
             " select aa.病区,bb.科室名称,COUNT(aa.ZYID) 病人数 " +
             " from ZY病人信息 aa left outer join GY科室设置 bb " +
             " on aa.病区=bb.科室编码 " +
             " where 已出院标记=0 and bb.科室编码 in  (" + HIS.COMM.zdInfo.s科室数据权限 + ")" +
             " group by aa.病区,bb.科室名称").Tables[0];
            lookUpEdit病区选择.Properties.DataSource = dt;
            lookUpEdit病区选择.Properties.DisplayMember = "科室名称";
            lookUpEdit病区选择.Properties.ValueMember = "病区";
            this.dta测量日期.DateTime = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            com测量时间.Text = this.selectDateTime(DateTime.Now);
            lookUpEdit病区选择.ItemIndex = 0;
        }


        /// <summary>
        /// 查询数据库
        /// </summary>
        private void GetInformion()
        {
            try
            {
                int i病区编码 = Convert.ToInt32(lookUpEdit病区选择.EditValue);
                if (string.IsNullOrEmpty(this.dta测量日期.Text) || string.IsNullOrEmpty(com测量时间.Text))
                {
                    WEISHENG.COMM.msgHelper.ShowInformation("请明确日期时间信息");
                }
                DateTime dtm测量日期 = string.IsNullOrEmpty(this.dta测量日期.Text) ? Convert.ToDateTime(DateTime.Now.ToShortDateString()) : Convert.ToDateTime(this.dta测量日期.Text);
                string s测量时间 = com测量时间.Text;

                //查询病区病人
                List<Model体温单信息_批量录入> models_twd_info = new List<Model体温单信息_批量录入>();
                using (HIS.Model.CHISEntities chis = new HIS.Model.CHISEntities(HIS.Model.EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    var models_brxx =
                        (from br in chis.ZY病人信息
                         where br.已出院标记 == false &&
                               br.在院状态 == "正常" &&
                               br.分院编码 == HIS.COMM.zdInfo.Model站点信息.分院编码 &&
                               br.病区 == i病区编码
                         select br).ToList();

                    var models_dl = (from dl in chis.HL体温单底栏数据
                                     where dl.测量日期 == dtm测量日期
                                     select dl).ToList();

                    var models_sc = (from sc in chis.HL体温单三测数据
                                     where sc.测量日期 == dtm测量日期 && sc.测量时间 == s测量时间
                                     select sc).ToList();

                    string tip_string_生成 = "";
                    string tip_string_标记 = "";
                    foreach (var item_brxx in models_brxx)
                    {
                        #region 校验入院事件,出院申请时，填写出院事件
                        DateTime cur入院日期 = Convert.ToDateTime(Convert.ToDateTime(item_brxx.入院时间).ToShortDateString());
                        string cur测量时间 = selectDateTime(Convert.ToDateTime(item_brxx.入院时间));
                        var model_sc入院事件 = chis.HL体温单三测数据.SingleOrDefault(c => c.测量日期 == cur入院日期 &&
                        c.测量时间 == cur测量时间 && c.ZYID == item_brxx.ZYID);
                        if (model_sc入院事件 == null)
                        {
                            HIS.Model.HL体温单三测数据 sc入院事件 = new HIS.Model.HL体温单三测数据
                            {
                                标注文字 = WEISHENG.COMM.dateTimeHelper.dateToUpper(Convert.ToDateTime(item_brxx.入院时间)),
                                测量日期 = Convert.ToDateTime(Convert.ToDateTime(item_brxx.入院时间).ToShortDateString()),
                                测量时间 = selectDateTime(Convert.ToDateTime(item_brxx.入院时间)),
                                更新日期 = DateTime.Now,
                                ZYID = item_brxx.ZYID,
                                createTime = DateTime.Now
                            };
                            chis.HL体温单三测数据.Add(sc入院事件);
                            chis.SaveChanges();
                            tip_string_生成 += item_brxx.病人姓名;
                        }
                        else
                        {
                            if (model_sc入院事件.标注文字 != null && !model_sc入院事件.标注文字.Contains("入院"))
                            {
                                model_sc入院事件.标注文字 = "入院--" + WEISHENG.COMM.dateTimeHelper.dateToUpper(Convert.ToDateTime(item_brxx.入院时间));
                                model_sc入院事件.更新日期 = DateTime.Now;
                                chis.SaveChanges();
                                tip_string_标记 += item_brxx.病人姓名 + ",";
                            }
                        }

                        #endregion

                        //查询三测和底栏信息
                        Model体温单信息_批量录入 model录入 = new Model体温单信息_批量录入
                        {
                            ZYID = item_brxx.ZYID,
                            病人姓名 = item_brxx.病人姓名,
                            住院号码 = item_brxx.住院号码,
                            性别 = item_brxx.性别,
                            年龄 = AgeHelper.GetAgeByBirthdate(Convert.ToDateTime(item_brxx.出生日期)).ToString(),
                            病床 = item_brxx.病床,
                            测量日期 = dtm测量日期,
                            测量时间 = s测量时间,
                            在院状态 = item_brxx.在院状态
                        };
                        var model_dl = models_dl.SingleOrDefault(c => c.ZYID == item_brxx.ZYID && c.测量日期 == dtm测量日期);
                        if (model_dl != null)
                        {
                            model录入.小便_ml_ = model_dl.小便_ml_;
                            model录入.大便_次_ = model_dl.大便_次_;
                            model录入.液入量_ml_ = model_dl.液入量_ml_;
                            model录入.血压AM_mmHg_ = model_dl.血压AM_mmHg_;
                            model录入.体重_Kg_ = model_dl.体重_Kg_;
                            model录入.痰量_ml_ = model_dl.痰量_ml_;
                            model录入.引流量_ml_ = model_dl.引流量_ml_;
                            model录入.呕吐量_ml_ = model_dl.呕吐量_ml_;
                            model录入.DLID = model_dl.ID;
                        }
                        var model_sc = models_sc.SingleOrDefault(c => c.ZYID == item_brxx.ZYID && c.测量日期 == dtm测量日期 && c.测量时间 == s测量时间);
                        if (model_sc != null)
                        {
                            model录入.体温___ = model_sc.体温___;
                            model录入.物理降温___ = model_sc.物理降温___;
                            model录入.脉搏_次_分_ = model_sc.脉搏_次_分_;
                            model录入.呼吸_次_分_ = model_sc.呼吸_次_分_;
                            model录入.标注文字 = model_sc.标注文字;
                            model录入.SCID = model_sc.ID;
                        }
                        models_twd_info.Add(model录入);
                    }

                    string balloon_info_1 = "";
                    string balloon_info_2 = "";

                    if (!string.IsNullOrEmpty(tip_string_生成))
                    {
                        balloon_info_1 = tip_string_生成 + "入院事件自动生成成功。\r\n";
                    }
                    if (!string.IsNullOrEmpty(tip_string_标记))
                    {
                        balloon_info_2 = tip_string_标记 + "入院事件标记成功。";
                    }
                    string balloon_info = balloon_info_1 + balloon_info_2;
                    if (!string.IsNullOrEmpty(balloon_info))
                    {
                        HIS.COMM.msgBalloonHelper.BalloonShow(balloon_info, "体温单");
                    }

                    tb_twd_info = models_twd_info.ToDataTable(rec => new object[] { models_twd_info });
                    tb_twd_info.AcceptChanges();

                    grd病人信息.DataSource = tb_twd_info;
                    grdv病人信息.BestFitColumns();

                }
                HIS.COMM.frmGridCustomize.RegisterGrid(grdv病人信息);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// 获取当前时间
        /// </summary>
        /// <returns></returns>
        private string selectDateTime(DateTime dt)
        {
            string Time = string.Empty;
            if (((dt.Hour == 3 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 3) && (dt.Hour < 7 || (dt.Hour == 7 && dt.Minute == 0 && dt.Second == 0)))
            {
                Time = "07";
            }
            else if (((dt.Hour == 7 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 7) && (dt.Hour < 11 || (dt.Hour == 11 && dt.Minute == 0 && dt.Second == 0)))
            {
                Time = "11";
            }
            else if (((dt.Hour == 11 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 11) && (dt.Hour < 15 || (dt.Hour == 15 && dt.Minute == 0 && dt.Second == 0)))
            {
                Time = "15";
            }
            else if (((dt.Hour == 15 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 15) && (dt.Hour < 19 || (dt.Hour == 19 && dt.Minute == 0 && dt.Second == 0)))
            {
                Time = "19";
            }
            else if (((dt.Hour == 19 && (dt.Minute > 0 || dt.Second > 0 || dt.Millisecond > 0)) || dt.Hour > 19) && (dt.Hour < 23 || (dt.Hour == 23 && dt.Minute == 0 && dt.Second == 0)))
            {
                Time = "23";
            }
            else
            {
                Time = "03";
            }
            return Time;
        }

        /// <summary>
        /// 保存按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn保存_Click(object sender, EventArgs e)
        {
            var diff2 = tb_twd_info.GetChanges();


            if (diff2.Rows.Count < 1)
            {
                WEISHENG.COMM.msgHelper.ShowInformation("当前数据未修改");
                return;
            }

            #region 校验数据
            //string msgAlert = String.Empty;
            //bool flg = false;
            //for (int i = 0; i < diff2.Rows.Count; i++)
            //{
            //    model = new Model体温单信息();
            //    var rows = diff2.Rows[i];
            //    model.ZYID = diff2.Rows[i]["ZYID"].ToString();
            //    model.测量日期 = string.IsNullOrEmpty(rows["测量日期"].ToString()) ? null : rows["测量日期"].ToString();
            //    model.测量时间 = string.IsNullOrEmpty(rows["测量时间"].ToString()) ? null : rows["测量时间"].ToString();
            //    model.体温 = string.IsNullOrEmpty(rows["体温(℃)"].ToString()) ? "" : rows["体温(℃)"].ToString();
            //    model.物理降温 = string.IsNullOrEmpty(rows["物理降温(℃)"].ToString()) ? "" : rows["物理降温(℃)"].ToString();
            //    model.脉搏 = string.IsNullOrEmpty(rows["脉搏(次/分)"].ToString()) ? "" : rows["脉搏(次/分)"].ToString();
            //    model.呼吸 = string.IsNullOrEmpty(rows["呼吸(次/分)"].ToString()) ? "" : rows["呼吸(次/分)"].ToString();
            //    model.大便 = string.IsNullOrEmpty(rows["大便(次)"].ToString()) ? "" : rows["大便(次)"].ToString();
            //    model.小便 = string.IsNullOrEmpty(rows["小便(ml)"].ToString()) ? "" : rows["小便(ml)"].ToString();
            //    model.液入量 = string.IsNullOrEmpty(rows["液入量(ml)"].ToString()) ? "" : rows["液入量(ml)"].ToString();
            //    model.血压AM = string.IsNullOrEmpty(rows["血压AM(mmHg)"].ToString()) ? "" : rows["血压AM(mmHg)"].ToString();
            //    model.呕吐量 = string.IsNullOrEmpty(rows["呕吐量(ml)"].ToString()) ? "" : rows["呕吐量(ml)"].ToString();
            //    model.体重 = string.IsNullOrEmpty(rows["体重(Kg)"].ToString()) ? "" : rows["体重(Kg)"].ToString();
            //    model.痰量 = string.IsNullOrEmpty(rows["痰量(ml)"].ToString()) ? "" : rows["痰量(ml)"].ToString();
            //    model.引流 = string.IsNullOrEmpty(rows["引流量(ml)"].ToString()) ? "" : rows["引流量(ml)"].ToString();
            //    model.标注文字 = string.IsNullOrEmpty(rows["标注文字"].ToString()) ? "" : rows["标注文字"].ToString();
            //    int time = 0;
            //    if (!int.TryParse(model.测量时间, out time))
            //    {
            //        msgAlert += "请检查" + rows["病人姓名"].ToString() + "【测量时间】数据是否正确！\r\n";
            //    }
            //    if (model.体温.Trim().Length != 0 && (float.Parse(model.体温) > 42 || float.Parse(model.体温) < 32))
            //    {
            //        msgAlert += "请检查" + rows["病人姓名"].ToString() + "【体温】数据是否正确！\r\n";
            //    }
            //    if (model.物理降温.Trim().Length != 0 && (float.Parse(model.物理降温) > 42 || float.Parse(model.物理降温) < 32))
            //    {
            //        msgAlert += "请检查" + rows["病人姓名"].ToString() + "【物理降温】数据是否正确！\r\n";
            //    }
            //    if (model.脉搏.Trim().Length != 0 && (float.Parse(model.脉搏) > 160 || float.Parse(model.脉搏) < 40))
            //    {
            //        msgAlert += "请检查" + rows["病人姓名"].ToString() + "【脉搏】数据是否正确！\r\n";
            //    }
            //    if (model.呼吸.Trim().Length != 0 && (float.Parse(model.呼吸) > 50 || float.Parse(model.呼吸) < 0))
            //    {
            //        msgAlert += "请检查" + rows["病人姓名"].ToString() + "【呼吸】数据是否正确！\r\n";
            //    }
            //    if (!"正常".Equals(rows["在院状态"].ToString()))
            //    {
            //        msgAlert += "请检查" + rows["病人姓名"].ToString() + "【在院状态】数据是否正确！\r\n";
            //    }
            //    if (!string.IsNullOrEmpty(msgAlert)) { XtraMessageBox.Show(msgAlert); return; }
            //}
            #endregion


            WEISHENG.COMM.Model2Datatable<HIS.Model.HL体温单三测数据> scData = new WEISHENG.COMM.Model2Datatable<HIS.Model.HL体温单三测数据>();
            WEISHENG.COMM.Model2Datatable<HIS.Model.HL体温单底栏数据> dlData = new WEISHENG.COMM.Model2Datatable<HIS.Model.HL体温单底栏数据>();

            var listScData = scData.FillModel(diff2, "SCID");
            var list_dl_data = dlData.FillModel(diff2, "DLID");
            using (HIS.Model.CHISEntities chis = new HIS.Model.CHISEntities(HIS.Model.EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                foreach (var item in listScData)
                {
                    item.createTime = DateTime.Now;
                    item.作废标志 = 0;
                    item.登录者 = HIS.COMM.zdInfo.ModelUserInfo.用户名;
                    item.登录时间 = DateTime.Now;
                    //item.更新日期= DateTime.Now;
                    //item.更新者 = HIS.COMM.zdInfo.ModelUserInfo.用户名;
                    if (item.ID == 0)
                    {
                        chis.HL体温单三测数据.Add(item);
                        continue;
                    }
                    var ishave = chis.HL体温单三测数据.Single(c => c.ID == item.ID);
                    if (ishave != null)
                    {
                        chis.Entry(ishave).CurrentValues.SetValues(item);
                    }
                    else
                    {
                        chis.HL体温单三测数据.Add(item);
                    }
                }
                foreach (var item_dl in list_dl_data)
                {
                    item_dl.createTime = DateTime.Now;
                    item_dl.作废标志 = 0;
                    item_dl.登录者 = HIS.COMM.zdInfo.ModelUserInfo.用户名;
                    item_dl.登录时间 = DateTime.Now;
                    //item_dl.更新日期= DateTime.Now;
                    //item_dl.更新者 = HIS.COMM.zdInfo.ModelUserInfo.用户名;

                    if (item_dl.ID == 0)
                    {
                        chis.HL体温单底栏数据.Add(item_dl);
                        continue;
                    }
                    var ishave = chis.HL体温单底栏数据.Single(c => c.ID == item_dl.ID);
                    if (ishave != null)
                    {
                        chis.Entry(ishave).CurrentValues.SetValues(item_dl);
                    }
                    else
                    {
                        chis.HL体温单底栏数据.Add(item_dl);
                    }
                }
                chis.SaveChanges();
                GetInformion();
                WEISHENG.COMM.msgHelper.ShowInformation("保存成功");
            }
            return;

        }



        /// <summary>
        /// 取消按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn取消_Click(object sender, EventArgs e)
        {
            //画面关闭
            this.Close();
        }


        /// <summary>
        /// 查询按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn查询记录_Click(object sender, EventArgs e)
        {
            string strwhere = "";
            //判断查询条件是否为空


            //条件查询
            GetInformionByCondition(strwhere);

        }

        /// <summary>
        /// 查询根据条件数据
        /// </summary>
        private void GetInformionByCondition(string strwhere)
        {
            //try
            //{
            //    string date = DateTime.Now.ToShortDateString();
            //    string time = this.selectDateTime();
            //    string SQL批量体温 = " SELECT" +
            //    " Z.住院号码," +
            //    " Z.ZYID," +
            //    " Z.病人姓名," +
            //    " Z.性别," +
            //    " datediff(" +
            //    " YEAR," +
            //    " Z.出生日期," +
            //    " getdate()" +
            //    " ) 年龄," +
            //    " Z.病床," +
            //    " G.科室名称," +
            //    " Z.在院状态," +
            //    " H.[测量日期] ," +
            //    " H.[测量时间] ," +
            //    " H.[体温(℃)]," +
            //    " H.[物理降温(℃)]," +
            //    " H.[脉搏(次/分)]," +
            //    " H.[呼吸(次/分)]," +
            //    " H2.[大便(次)]," +
            //    " H2.[小便(ml)]," +
            //    " H2.[液入量(ml)]," +
            //    " H2.[血压AM(mmHg)]," +
            //    " H2.[呕吐量(ml)]," +
            //    " H2.[体重(Kg)]," +
            //    " H2.[痰量(ml)]," +
            //    " H2.[引流量(ml)]," +
            //    " H.[标注文字]" +
            //    " FROM" +
            //    " ZY病人信息 Z" +
            //    " LEFT JOIN GY科室设置 G ON Z.科室 = G.科室编码" +
            //    " LEFT JOIN [HL体温单三测数据] H ON H.[ZYID] = z.[ZYID] and H.作废标志 = '0' " +
            //    " LEFT JOIN [HL体温单底栏数据] H2 ON H2.[ZYID] =z.[ZYID] AND H2.[测量日期] = H.[测量日期] and H2.作废标志 = '0' " +
            //    " WHERE" +
            //    " z.病区 = '" + int病区编码 + "'" +
            //    " AND z.分院编码 = '" + 分院编码 + "'";
            //    SQL批量体温 = SQL批量体温 + strwhere;
            //    Patient = new DataTable();
            //    Patient = HIS.Model.Dal.SqlHelper.ExecuteDataTable(CommandType.Text, SQL批量体温);
            //    if (Patient != null && Patient.Rows.Count > 1 && !"正常".Equals(Patient.Rows[0]["在院状态"].ToString()))
            //    {
            //        MessageBox.Show("该病人已出院或出院申请体温单数据不能修改");
            //        grdv病人信息.OptionsBehavior.Editable = false;
            //        //grd病人信息
            //    }
            //    grd病人信息.DataSource = Patient;
            //    HIS.COMM.frmGridCustomize.RegisterGrid(grdv病人信息);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}

        }

        /// <summary>
        /// 列表双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grd病人信息_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string zyid = grdv病人信息.GetFocusedRowCellValue("ZYID").ToString();
            string 在院状态 = grdv病人信息.GetFocusedRowCellValue("在院状态").ToString();
            Form体温单 fm = new Form体温单();
            fm.Tag = this;
            fm.sZYID = zyid;
            fm.医院名称 = HIS.COMM.zdInfo.Model单位信息.sDwmc;
            fm.当前用户 = HIS.COMM.zdInfo.ModelUserInfo.用户名;
            if (在院状态 == "正常")
            {
                fm.CanEdit = true;
            }
            else
            {
                fm.CanEdit = false;
            }
            fm.WindowState = FormWindowState.Maximized;
            fm.ShowDialog();
        }

        /// <summary>
        /// 事件添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void repositoryItemButtonEdit事件设置_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            XtraForm事件设置 fm = new XtraForm事件设置();
            fm.Tag = this;
            fm.ShowDialog();
            if (!string.IsNullOrEmpty(标注文本))
            {
                int i = this.grdv病人信息.GetSelectedRows()[0];
                grdv病人信息.SetRowCellValue(i, "标注文字", 标注文本);
            }
        }

        /// <summary>
        /// 病区选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">e</param>
        private void lookUpEdit病区选择_EditValueChanged(object sender, EventArgs e)
        {
            GetInformion();
        }

        /// <summary>
        /// 病人选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                HIS.COMM.XtraForm病人选择 cybrxz = new HIS.COMM.XtraForm病人选择(HIS.COMM.ClassEnum.en病人选择类型.区分科室权限含子病人);
                if (cybrxz.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                else
                {
                    this.txt住院号.Text = cybrxz.person.s住院号码;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt住院号_EditValueChanged(object sender, EventArgs e)
        {
            this.btn查询记录_Click(null, null);
        }


        private void grdv病人信息_FocusedColumnChanged(object sender, EventArgs e)
        {
            return;
            string SQL血压记录;
            DataTable table血压;
            if (grdv病人信息.FocusedColumn.Name == "体温")
            {
                SQL血压记录 = "select [小便(ml)],[大便(次)],[液入量(ml)],[血压AM(mmHg)],[体重(Kg)],[痰量(ml)],[引流量(ml)],[呕吐量(ml)] ";
                SQL血压记录 += " FROM [HL体温单底栏数据] where ZYID='" + grdv病人信息.GetFocusedRowCellValue("ZYID").ToString() + "' ";
                SQL血压记录 += " and 测量日期='" + DateTime.Parse(grdv病人信息.GetFocusedRowCellValue("测量日期").ToString()) + "' ";
                SQL血压记录 += " and  作废标志=0 ";
                table血压 = new DataTable();
                table血压 = HIS.Model.Dal.SqlHelper.ExecuteDataTable(CommandType.Text, SQL血压记录);
                if (table血压.Rows.Count > 0)
                {
                    grdv病人信息.CloseEditor();
                    grdv病人信息.SetFocusedRowCellValue("小便", table血压.Rows[0][0].ToString());
                    grdv病人信息.SetFocusedRowCellValue("大便", table血压.Rows[0][1].ToString());
                    grdv病人信息.SetFocusedRowCellValue("液入量", table血压.Rows[0][2].ToString());
                    grdv病人信息.SetFocusedRowCellValue("血压AM", table血压.Rows[0][3].ToString());
                    grdv病人信息.SetFocusedRowCellValue("呕吐量", table血压.Rows[0][7].ToString());
                    grdv病人信息.SetFocusedRowCellValue("体重", table血压.Rows[0][4].ToString());
                    grdv病人信息.SetFocusedRowCellValue("痰量", table血压.Rows[0][5].ToString());
                    grdv病人信息.SetFocusedRowCellValue("引流", table血压.Rows[0][6].ToString());
                    grdv病人信息.UpdateCurrentRow();
                    grdv病人信息.RefreshRow(grdv病人信息.FocusedRowHandle);
                }
            }
        }

        private void simpleButton查询病人_Click(object sender, EventArgs e)
        {
            GetInformion();
        }

    }
    public class Model体温单信息_批量录入
    {
        public decimal ZYID { get; set; }
        public System.DateTime 测量日期 { get; set; }
        public string 小便_ml_ { get; set; }
        public string 大便_次_ { get; set; }
        public string 液入量_ml_ { get; set; }
        public string 血压AM_mmHg_ { get; set; }
        public string 体重_Kg_ { get; set; }
        public string 痰量_ml_ { get; set; }
        public string 引流量_ml_ { get; set; }
        public string 呕吐量_ml_ { get; set; }

        public string 测量时间 { get; set; }
        public string 体温___ { get; set; }
        public string 物理降温___ { get; set; }
        public string 脉搏_次_分_ { get; set; }
        public string 呼吸_次_分_ { get; set; }
        public string 标注文字 { get; set; }

        public string 病人姓名 { get; set; }
        public string 性别 { get; set; }
        public string 年龄 { get; set; }
        public string 病床 { get; set; }
        public string 科室名称 { get; set; }
        public string 住院号码 { get; set; }
        public int DLID { get; set; }
        public int SCID { get; set; }
        public string 在院状态 { get; set; }

    }


}