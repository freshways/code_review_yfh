﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM
{
    public class Class版本升级执行
    {


        public static bool b初始化环境()
        {

            //Comm.paperInfo.setPaper(HIS.COMM.ClassDBConnstring.SConnHISDb, "住院发票_新格式", WEISHENG.COMM.zdInfo.GY科室信息.单位编码.ToString(), 2110, 1010, 0, 0, 0, 0);
            //Comm.paperInfo.setPaper(HIS.COMM.ClassDBConnstring.SConnHISDb, "门诊发票_新格式", WEISHENG.COMM.zdInfo.GY科室信息.单位编码.ToString(), 2110, 1270, 0, 0, 0, 0);
            //Comm.paperInfo.setPaper(HIS.COMM.ClassDBConnstring.SConnHISDb, "新农合门诊统筹汇总表", WEISHENG.COMM.zdInfo.Dwid.ToString(), 2970, 2100, 0, 0, 0, 0);
            //Comm.paperInfo.setPaper(HIS.COMM.ClassDBConnstring.SConnHISDb, "新农合门诊统筹明细表", WEISHENG.COMM.zdInfo.Dwid.ToString(), 2970, 2100, 0, 0, 0, 0);
            //TYK.Clasdongds.paperInfo.setPaper(HIS.COMM.ClassDBConnstring.SConnHISDb, "新农合住院明细表", WEISHENG.COMM.zdInfo.Dwid.ToString(), 2970, 2100, 0, 0, 0, 0);
            HIS.COMM.paperInfo.b新增纸张(HIS.COMM.DBConnHelper.SConnHISDb, "长期医嘱", HIS.COMM.zdInfo.Model科室信息.单位编码.ToString(), 2100, 2970, 0, 0, 0, 0);
            HIS.COMM.paperInfo.b新增纸张(HIS.COMM.DBConnHelper.SConnHISDb, "临时医嘱", HIS.COMM.zdInfo.Model科室信息.单位编码.ToString(), 2100, 2970, 0, 0, 0, 0);
            HIS.COMM.paperInfo.b设置纸张(HIS.COMM.DBConnHelper.SConnHISDb, "收款处汇总结账现金报表", HIS.COMM.zdInfo.Model科室信息.单位编码.ToString(), 2100, 1200, 30, 0, 0, 0);
            try
            {
                WEISHENG.COMM.dzgHelper.b初始化驱动();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public static bool b临时执行()
        {
            //HIS.COMM.ClassUpgrade.b德康导入新ICD疾病();
            HIS.COMM.ClassUpgrade.bPACSLIS接口改造();//20160129
            HIS.COMM.ClassUpgrade.b化验结果190();
            HIS.COMM.ClassUpgrade.b影像结果191();
            HIS.COMM.ClassUpgrade.b升级二次住院病人的EMR状态();//20151204
            HIS.COMM.ClassUpgrade.b修复体温单和自动记费重复键ID();
            HIS.COMM.ClassUpgrade.b日志服务器时间默认值();
            HIS.COMM.ClassUpgrade.b管理员维护39();
            HIS.COMM.ClassUpgrade.bPACS条码188();//PACS条码打印
            HIS.COMM.ClassUpgrade.b护理体温单187();//体温单
            HIS.COMM.ClassUpgrade.b医嘱临床路径改造();//20150511
            HIS.COMM.ClassUpgrade.b升级通知公告增加附件内容();//20150507
            HIS.COMM.ClassUpgrade.b报表中心统计报表();//20150504
            HIS.COMM.ClassUpgrade.b药典是否禁用问题();//20150502
            HIS.COMM.ClassUpgrade.bHL自动记费设置建表();//20150421
            HIS.COMM.ClassUpgrade.b护理自动记费186();//20150421
            HIS.COMM.ClassUpgrade.bSQL子病人信息();//20150412
            HIS.COMM.ClassUpgrade.b_vw全部病人();//20150419    
            HIS.COMM.ClassUpgrade.b_vw全部病人修改();//20150421    
            HIS.COMM.ClassUpgrade.b删除权限117三卡合一();//20150411
            HIS.COMM.ClassUpgrade.b输液记录打印标志();
            HIS.COMM.ClassUpgrade.b长期医嘱只执行一次();
            HIS.COMM.ClassUpgrade.b接诊记录接口184();//20150407
            HIS.COMM.ClassUpgrade.b诊治记录查询185();//20150407
            HIS.COMM.ClassUpgrade.b护理打印医嘱182();//20150309
            HIS.COMM.ClassUpgrade.b护理输液记录183();//20150309
            HIS.COMM.ClassUpgrade.b更新医嘱内涵();//20150308
            HIS.COMM.ClassUpgrade.b药房调拨确认权限181();//20150308
            HIS.COMM.ClassUpgrade.b药房调拨申请权限180();//20150308
            HIS.COMM.ClassUpgrade.b药房调拨();//20150308
            HIS.COMM.ClassUpgrade.b医嘱续打时间点();//20150308
            HIS.COMM.ClassUpgrade.b升级出院结算和取消结算();//20150227
            HIS.COMM.ClassUpgrade.b协定处方功能升级();//20150227
            HIS.COMM.ClassUpgrade.b医嘱处理增加字段();//20150227
            HIS.COMM.ClassUpgrade.b管理员维护_协定处方权限();//20150222
            HIS.COMM.ClassUpgrade.b病历索引加ZYID();
            HIS.COMM.ClassUpgrade.b收款整合居民报销();
            HIS.COMM.ClassUpgrade.b子医嘱约束问题();
            //HIS.COMM.ClassUpgrade.b历史库存功能权限();
            //HIS.COMM.ClassUpgrade.b增加病历类型术前讨论();
            HIS.COMM.ClassUpgrade.b升级药房库存预警SQL();
            //HIS.COMM.ClassUpgrade.b统计报表改造();//修改为代码sql，弃用原来的视图
            //HIS.COMM.ClassUpgrade.b曼荼罗改造1();
            return true;
        }
    }
}
