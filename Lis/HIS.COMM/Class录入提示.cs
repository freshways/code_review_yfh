﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using HIS.Model.Pojo;
using HIS.Model;

namespace HIS.COMM
{
    public static class Class录入提示
    {
      static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
        public static List<Pojo西药方录入提示> get西药方提示(string _s药房编码)
        {
            var sql =
                         "with dd" + "\r\n" +
                         "     as (select   YPID, SUM(处方总量) 处方总量" + "\r\n" +
                         "         from     MF处方明细" + "\r\n" +
                         "         where    CFID in" + "\r\n" +
                         "                    (select CFID" + "\r\n" +
                         "                     from   mf处方摘要 aa" + "\r\n" +
                         "                            left outer join mf门诊摘要 bb on aa.mzid = bb.mzid and aa.单位编码 = bb.单位编码" + "\r\n" +
                         "                     where  aa.单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 = " + _s药房编码 +
                         " and 已发药标记 = 0 and aa.作废标记 = 0 and aa.[开方时间] between GETDATE() - 1 and '" + DateTime.Now.ToShortDateString() + " 23:59:59')" + "\r\n" +
                         "         group by YPID)," + "\r\n" +
                         "     ee" + "\r\n" +
                         "     as (select   ypid, SUM(数量) 处方总量" + "\r\n" +
                         "         from     ZY在院费用" + "\r\n" +
                         "         where    YPID <> 0 and 已发药标记 = 0 and 单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and 药房编码 =  " + _s药房编码 + "\r\n" +
                         "         group by YPID)" + "\r\n" +
                         "SELECT   aa.药房编码, bb.财务分类," + "\r\n" +
                         "         aa.药品序号, aa.药品名称," + "\r\n" +
                         "         aa.药房规格, aa.药房单位," + "\r\n" +
                         "         aa.药品产地, aa.药品批号," + "\r\n" +
                         "         aa.药品效期, (aa.库存数量 - isnull(dd.处方总量, 0) - ISNULL(ee.处方总量, 0)) 库存数量," + "\r\n" +
                         "         isnull(aa.进价, 0) 进价, isnull(aa.医院零售价, 0) 医院零售价," + "\r\n" +
                         "         isnull(aa.卫生室零售价, 0) 卫生室零售价, aa.YPID," + "\r\n" +
                         "          bb.[默认频次]," + "\r\n" +
                         "         bb.默认用法, bb.药品剂量," + "\r\n" +
                         "         bb.剂量单位,bb.拼音代码" + "\r\n" +
                         "FROM     YF库存信息 AS aa" + "\r\n" +
                         "         LEFT OUTER JOIN YK药品信息 AS bb ON aa.药品序号 = bb.药品序号 and aa.单位编码 = bb.单位编码" + "\r\n" +
                         "         left outer join dd on aa.ypid = dd.ypid" + "\r\n" +
                         "         left outer join ee on aa.YPID = ee.YPID" + "\r\n" +
                         "where    aa.单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 =  " + _s药房编码 + " and 财务分类 <> '中药费' and (aa.库存数量 - isnull(dd.处方总量, 0)" + "\r\n" +
                         "         - ISNULL        (ee.处方总量, 0)) > 0" + "\r\n" +
                         "order by 药品名称, 药品效期";
            return chis.Database.SqlQuery<Pojo西药方录入提示>(sql).ToList();
        }
        public static List<Pojo中药方录入提示> get中药方提示(string _s药房编码)
        {
            var sql = "with dd" + "\r\n" +
                         "     as (select   YPID, SUM(处方总量) 处方总量" + "\r\n" +
                         "         from     MF处方明细" + "\r\n" +
                         "         where    CFID in" + "\r\n" +
                         "                    (select CFID" + "\r\n" +
                         "                     from   mf处方摘要 aa" + "\r\n" +
                         "                            left outer join mf门诊摘要 bb on aa.mzid = bb.mzid and aa.单位编码 = bb.单位编码" + "\r\n" +
                         "                     where  aa.单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 = " + _s药房编码 +
                         " and 已发药标记 = 0 and aa.作废标记 = 0 and aa.[开方时间] between GETDATE() - 1 and '" + DateTime.Now.ToShortDateString() + " 23:59:59')" + "\r\n" +
                         "         group by YPID)," + "\r\n" +
                         "     ee" + "\r\n" +
                         "     as (select   ypid, SUM(数量) 处方总量" + "\r\n" +
                         "         from     ZY在院费用" + "\r\n" +
                         "         where    YPID <> 0 and 已发药标记 = 0 and 单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and 药房编码 =  " + _s药房编码 + "\r\n" +
                         "         group by YPID)" + "\r\n" +
                         "SELECT   aa.药房编码, bb.财务分类," + "\r\n" +
                         "         aa.药品序号, aa.药品名称," + "\r\n" +
                         "         aa.药房规格, aa.药房单位," + "\r\n" +
                         "         aa.药品产地, aa.药品批号," + "\r\n" +
                         "         aa.药品效期, (aa.库存数量 - isnull(dd.处方总量, 0) - ISNULL(ee.处方总量, 0)) 库存数量," + "\r\n" +
                         "         isnull(aa.进价, 0) 进价, isnull(aa.医院零售价, 0) 医院零售价," + "\r\n" +
                         "         isnull(aa.卫生室零售价, 0) 卫生室零售价, aa.YPID," + "\r\n" +
                         "          bb.[默认频次]," + "\r\n" +
                         "         bb.默认用法, bb.药品剂量," + "\r\n" +
                         "         bb.剂量单位,bb.拼音代码" + "\r\n" +
                         "FROM     YF库存信息 AS aa" + "\r\n" +
                         "         LEFT OUTER JOIN YK药品信息 AS bb ON aa.药品序号 = bb.药品序号 and aa.单位编码 = bb.单位编码" + "\r\n" +
                         "         left outer join dd on aa.ypid = dd.ypid" + "\r\n" +
                         "         left outer join ee on aa.YPID = ee.YPID" + "\r\n" +
                         "where    aa.单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 =  " + _s药房编码 + " and 财务分类 = '中药费' and (aa.库存数量 - isnull(dd.处方总量, 0)" + "\r\n" +
                         "         - ISNULL        (ee.处方总量, 0)) > 0" + "\r\n" +
                         "order by 药品名称, 药品效期";
            return chis.Database.SqlQuery<Pojo中药方录入提示>(sql).ToList();
        }




        public static bool b是否科室库存(string _s药房编码)
        {
            string SQL = "select 是否科室库存 from GY科室设置 where 科室编码=" + _s药房编码;
            return Convert.ToBoolean(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, SQL));
        }



        public static DataTable dt费用录入提示(ClassCflx.enCflx _cflx, string _s药房编码)
        {
            return dtGet录入提示(_cflx, _s药房编码);
        }
        public static DataTable dt费用录入提示(ClassCflx.enCflx _cflx, string _s药房编码, DataTable _dt界面费用)
        {
            DataTable dt库存提示 = dtGet录入提示(_cflx, _s药房编码);
            if (_cflx == HIS.COMM.ClassCflx.enCflx.西药方 || _cflx == HIS.COMM.ClassCflx.enCflx.中药方)
            {
                foreach (DataRow row in _dt界面费用.Rows)
                {
                    string ypidls = row["YPID"].ToString();
                    if (ypidls != "")
                    {
                        if (dt库存提示.Select("YPID=" + ypidls).Length != 0)
                        {
                            DataRow r2 = dt库存提示.Select("YPID=" + ypidls)[0];
                            r2["库存数量"] = Convert.ToDecimal(r2["库存数量"]) - Convert.ToDecimal(row["数量"]);
                            dt库存提示.AcceptChanges();
                        }
                    }
                }
            }
            return dt库存提示;
        }

        private static DataTable dtGet录入提示(ClassCflx.enCflx _cflx, string _s药房编码)
        {
            string strQuery = "";
            switch (_cflx)
            {
                case HIS.COMM.ClassCflx.enCflx.西药方:
                    strQuery =
                        "with dd" + "\r\n" +
                        "     as (select   YPID, SUM(处方总量) 处方总量" + "\r\n" +
                        "         from     MF处方明细" + "\r\n" +
                        "         where    CFID in" + "\r\n" +
                        "                    (select CFID" + "\r\n" +
                        "                     from   mf处方摘要 aa" + "\r\n" +
                        "                            left outer join mf门诊摘要 bb on aa.mzid = bb.mzid and aa.单位编码 = bb.单位编码" + "\r\n" +
                        "                     where  aa.单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 = " + _s药房编码 +
                        " and 已发药标记 = 0 and aa.作废标记 = 0 and aa.[开方时间] between '" + DateTime.Now.ToShortDateString() + "' and '" + DateTime.Now.ToShortDateString() + " 23:59:59')" + "\r\n" +
                        "         group by YPID)," + "\r\n" +
                        "     ee" + "\r\n" +
                        "     as (select   ypid, SUM(数量) 处方总量" + "\r\n" +
                        "         from     ZY在院费用" + "\r\n" +
                        "         where    YPID <> 0 and 已发药标记 = 0 and 单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and 药房编码 =  " + _s药房编码 + "\r\n" +
                        "         group by YPID)" + "\r\n" +
                        "SELECT   aa.药房编码, bb.财务分类," + "\r\n" +
                        "         aa.药品序号, aa.药品名称," + "\r\n" +
                        "         aa.药房规格, aa.药房单位," + "\r\n" +
                        "         aa.药品产地, aa.药品批号," + "\r\n" +
                        "         aa.药品效期, (aa.库存数量 - isnull(dd.处方总量, 0) - ISNULL(ee.处方总量, 0)) 库存数量," + "\r\n" +
                        "         isnull(aa.进价, 0) 进价, isnull(aa.医院零售价, 0) 医院零售价," + "\r\n" +
                        "         isnull(aa.卫生室零售价, 0) 卫生室零售价, aa.YPID," + "\r\n" +
                        "          bb.[默认频次]," + "\r\n" +
                        "         bb.默认用法, bb.药品剂量," + "\r\n" +
                        "         bb.剂量单位,bb.拼音代码" + "\r\n" +
                        "FROM     YF库存信息 AS aa" + "\r\n" +
                        "         LEFT OUTER JOIN YK药品信息 AS bb ON aa.药品序号 = bb.药品序号 and aa.单位编码 = bb.单位编码" + "\r\n" +
                        "         left outer join dd on aa.ypid = dd.ypid" + "\r\n" +
                        "         left outer join ee on aa.YPID = ee.YPID" + "\r\n" +
                        "where    aa.单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 =  " + _s药房编码 + " and 财务分类 <> '中药费' and (aa.库存数量 - isnull(dd.处方总量, 0)" + "\r\n" +
                        "         - ISNULL        (ee.处方总量, 0)) > 0" + "\r\n" +
                        "order by 药品名称, 药品效期";
                    break;
                case HIS.COMM.ClassCflx.enCflx.中药方:
                    strQuery =// "usp划价提示_包括 " + WEISHENG.COMM.zdInfo.GY科室信息.单位编码.ToString() + "," + _s药房编码 + ",'中药费'";
                         "with dd" + "\r\n" +
                        "     as (select   YPID, SUM(处方总量) 处方总量" + "\r\n" +
                        "         from     MF中药方_处方明细" + "\r\n" +
                        "         where    CFID in" + "\r\n" +
                        "                    (select CFID" + "\r\n" +
                        "                     from   MF中药方_处方摘要 aa" + "\r\n" +
                        "                            left outer join mf门诊摘要 bb on aa.mzid = bb.mzid and aa.单位编码 = bb.单位编码" + "\r\n" +
                        "                     where  aa.单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 = " + _s药房编码 +
                        " and 已发药标记 = 0 and aa.作废标记 = 0 and aa.[开方时间] between '" + DateTime.Now.ToShortDateString() + "' and '" + DateTime.Now.ToShortDateString() + " 23:59:59')" + "\r\n" +
                        "         group by YPID)," + "\r\n" +
                        "     ee" + "\r\n" +
                        "     as (select   ypid, SUM(数量) 处方总量" + "\r\n" +
                        "         from     ZY在院费用" + "\r\n" +
                        "         where    YPID <> 0 and 已发药标记 = 0 and 单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and 药房编码 =  " + _s药房编码 + "\r\n" +
                        "         group by YPID)" + "\r\n" +
                        "SELECT   aa.药房编码, bb.财务分类," + "\r\n" +
                        "         aa.药品序号, aa.药品名称," + "\r\n" +
                        "         aa.药房规格, aa.药房单位," + "\r\n" +
                        "         aa.药品产地, aa.药品批号," + "\r\n" +
                        "         aa.药品效期, (aa.库存数量 - isnull(dd.处方总量, 0) - ISNULL(ee.处方总量, 0)) 库存数量," + "\r\n" +
                        "         isnull(aa.进价, 0) 进价, isnull(aa.医院零售价, 0) 医院零售价," + "\r\n" +
                        "         isnull(aa.卫生室零售价, 0) 卫生室零售价, aa.YPID," + "\r\n" +
                        "          bb.[默认频次]," + "\r\n" +
                        "         bb.默认用法, bb.药品剂量," + "\r\n" +
                        "         bb.剂量单位,bb.拼音代码" + "\r\n" +
                        "FROM     YF库存信息 AS aa" + "\r\n" +
                        "         LEFT OUTER JOIN YK药品信息 AS bb ON aa.药品序号 = bb.药品序号 and aa.单位编码 = bb.单位编码" + "\r\n" +
                        "         left outer join dd on aa.ypid = dd.ypid" + "\r\n" +
                        "         left outer join ee on aa.YPID = ee.YPID" + "\r\n" +
                        "where    aa.单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 =  " + _s药房编码 + " and 财务分类 = '中药费' and (aa.库存数量 - isnull(dd.处方总量, 0)" + "\r\n" +
                        "         - ISNULL        (ee.处方总量, 0)) > 0" + "\r\n" +
                        "order by 药品名称, 药品效期";
                    break;
                case HIS.COMM.ClassCflx.enCflx.检查治疗单:
                    strQuery =
                       "SELECT 收费编码,收费名称,单位,单价,  归并编码, 归并名称, 执行科室编码,PACS检查项,LIS检查项, 拼音代码,  " + "\r\n" +
                       "       新合编码, 是否报销" + "\r\n" +
                       "FROM   GY收费小项" + "\r\n" +
                       "WHERE  (单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + ") AND (是否禁用 = 0)";
                    break;
            }
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, strQuery).Tables[0];
        }


        public static DataTable dt住院医嘱提示(string _s药房编码)
        {

            string SQL =
                "select   aa.药品序号 项目编码, aa.药品名称 项目名称, aa.药房规格, aa.药房单位, aa.医院零售价 单价," + "\r\n" +
                "         sum(aa.库存数量) 库存数量, '药品项目' 类型, bb.[默认频次], bb.默认用法, bb.[药品剂量]," + "\r\n" +
                "         bb.剂量单位, bb.财务分类, bb.拼音代码, aa.药房编码" + "\r\n" +
                "from     YF库存信息 aa left outer join yk药品信息 bb on aa.药品序号 = bb.药品序号" + "\r\n" +
                "where    aa.库存数量 > 0 and aa.药房编码 = " + _s药房编码 + "\r\n" +
                "group by aa.药品序号, aa.药品名称, aa.药房规格, aa.药房单位, aa.医院零售价, bb.[默认频次]," + "\r\n" +
                "         bb.默认用法, bb.[药品剂量], bb.剂量单位, bb.财务分类, bb.拼音代码, aa.药房编码 " +
                "UNION ALL" + "\r\n" +
                "SELECT 项目编号 项目编码, 项目名称,  '' 药房规格,'' 药房单位,0 单价, -1 库存数量, '医嘱项目' 类型," + "\r\n" +
                "        '' [默认频次], '' 默认用法, 0 [药品剂量], '' 剂量单位, '' 财务分类, 拼音代码,0" + "\r\n" +
                "FROM   YS医嘱项目" + "\r\n" +
                "WHERE  是否禁用 = 0" + "\r\n" +
                "UNION ALL" + "\r\n" +
                "SELECT 收费编码 项目编码, 收费名称 项目名称,  '' 药房规格,单位, 单价, -1 库存数量, '医技项目' 类型," + "\r\n" +
                "      '' [默认频次], '' 默认用法, 0 [药品剂量], '' 剂量单位," + "\r\n" +
                "       '' 财务分类, 拼音代码,0" + "\r\n" +
                "FROM   GY收费小项" + "\r\n" +
                "WHERE  是否禁用 = 0";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL).Tables[0];
        }

        public static decimal get库存ByYPID(decimal decYPID, string _s药房编码)
        {
            string strQuery =
                              "with dd" + "\r\n" +
                              "     as (select   YPID, SUM(处方总量) 处方总量" + "\r\n" +
                              "         from     MF处方明细" + "\r\n" +
                              "         where    CFID in" + "\r\n" +
                              "                    (select CFID" + "\r\n" +
                              "                     from   mf处方摘要 aa" + "\r\n" +
                              "                            left outer join mf门诊摘要 bb on aa.mzid = bb.mzid and aa.单位编码 = bb.单位编码" + "\r\n" +
                              "                     where ypid=" + decYPID + " and aa.单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 = " + _s药房编码 +
                              " and 已发药标记 = 0 and aa.作废标记 = 0 and aa.[开方时间] between '" + DateTime.Now.ToShortDateString() + "' and '" + DateTime.Now.ToShortDateString() + " 23:59:59')" + "\r\n" +
                              "         group by YPID)," + "\r\n" +
                              "     ee" + "\r\n" +
                              "     as (select   ypid, SUM(数量) 处方总量" + "\r\n" +
                              "         from     ZY在院费用" + "\r\n" +
                              "         where  ypid=" + decYPID + " and 已发药标记 = 0 and 单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and 药房编码 =  " + _s药房编码 + "\r\n" +
                              "         group by YPID)" + "\r\n" +
                              "SELECT  aa.库存数量 - isnull(dd.处方总量, 0) - ISNULL(ee.处方总量, 0) 库存数量" + "\r\n" +
                              "FROM     YF库存信息 AS aa" + "\r\n" +
                              "         LEFT OUTER JOIN YK药品信息 AS bb ON aa.药品序号 = bb.药品序号 and aa.单位编码 = bb.单位编码" + "\r\n" +
                              "         left outer join dd on aa.ypid = dd.ypid" + "\r\n" +
                              "         left outer join ee on aa.YPID = ee.YPID" + "\r\n" +
                              "where    aa.单位编码 =  " + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and aa.药房编码 =  " + _s药房编码 + " and aa.ypid=" + decYPID;
            return Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, strQuery));
        }
        public static bool b剂量转换(string _s用法)
        {
            try
            {
                string SQL = "select isnull(门诊处方剂量转换,0) from [dbo].[YS医嘱用法] where 用法名称='" + _s用法 + "'";

                return Convert.ToBoolean(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, SQL));
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
