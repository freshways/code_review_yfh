﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.COMM
{
    public class Class记费
    {
        //https://www.cnblogs.com/multiplesoftware/archive/2011/08/27/2155268.html
        public DataTable dt费用摘要 = new DataTable("dt费用摘要");
        public DataTable dt费用明细 = new DataTable("dt费用明细");
        public DataTable dt住院记费 = new DataTable("dt住院记费");
        string s中药费小项编码 = HIS.COMM.comm.get收费小项编码("中药费");
        string s西药费小项编码 = HIS.COMM.comm.get收费小项编码("西药费");
        public Class记费()//构造函数
        {
            dt费用摘要.Columns.Add("序号", Type.GetType("System.Int32"));
            dt费用摘要.Columns.Add("收费名称", Type.GetType("System.String"));
            dt费用摘要.Columns.Add("收费金额", Type.GetType("System.Decimal"));
            dt费用摘要.Columns.Add("执行科室", Type.GetType("System.String"));
            dt费用摘要.Columns.Add("数量", Type.GetType("System.Decimal"));
            dt费用摘要.Columns.Add("单价", Type.GetType("System.Decimal"));

            DataColumn colNumber = new DataColumn();
            colNumber.AutoIncrement = true;//设置是否为自增列
            colNumber.AutoIncrementSeed = 1;//设置自增初始值
            colNumber.AutoIncrementStep = 1;//设置每次子增值
            colNumber.ColumnName = "ID";
            dt费用明细.Columns.Add(colNumber);
            dt费用明细.Columns.Add("剂量数量", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("剂量单位", Type.GetType("System.String"));
            dt费用明细.Columns.Add("频次", Type.GetType("System.String"));
            dt费用明细.Columns.Add("用法", Type.GetType("System.String"));
            dt费用明细.Columns.Add("药品序号", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("费用序号", Type.GetType("System.Int32"));
            dt费用明细.Columns.Add("费用名称", Type.GetType("System.String"));
            dt费用明细.Columns.Add("规格", Type.GetType("System.String"));
            dt费用明细.Columns.Add("单位", Type.GetType("System.String"));
            dt费用明细.Columns.Add("数量", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("单价", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("金额", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("YPID", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("进价", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("产地", Type.GetType("System.String"));
            dt费用明细.Columns.Add("批号", Type.GetType("System.String"));
            dt费用明细.Columns.Add("是否报销", Type.GetType("System.Int32"));
            dt费用明细.Columns.Add("组别", Type.GetType("System.Int32"));
            dt费用明细.Columns.Add("进价金额", Type.GetType("System.Decimal"));
            dt费用明细.Columns["进价金额"].Expression = "数量*进价";
            dt费用明细.Columns.Add("医保编码", Type.GetType("System.String"));
            dt费用明细.Columns.Add("医保名称", Type.GetType("System.String"));
            dt费用明细.Columns.Add("交易流水号", Type.GetType("System.String"));
            dt费用明细.Columns.Add("自理金额", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("自费金额", Type.GetType("System.Decimal"));
            dt费用明细.Columns.Add("是否上传", Type.GetType("System.Int32"));
            dt费用明细.Columns.Add("YB_项目大类", Type.GetType("System.String"));
            dt费用明细.Columns.Add("YB_项目收费类别代码", Type.GetType("System.String"));
            dt费用明细.Constraints.Add(new global::System.Data.UniqueConstraint("Constraint1", new global::System.Data.DataColumn[] {
                               dt费用明细.Columns["组别"],
                               dt费用明细.Columns["费用序号"],
                               dt费用明细.Columns["YPID"]}, true));

            dt住院记费.Columns.Add("项目编码", Type.GetType("System.Int64"));
            dt住院记费.Columns.Add("ZYID", Type.GetType("System.Int32"));
            dt住院记费.Columns.Add("费用编码", Type.GetType("System.Int64"));
            dt住院记费.Columns.Add("费用名称", Type.GetType("System.String"));
            dt住院记费.Columns.Add("数量", Type.GetType("System.Decimal"));
            dt住院记费.Columns.Add("单价", Type.GetType("System.Decimal"));
            dt住院记费.Columns.Add("金额", Type.GetType("System.Decimal"));
            dt住院记费.Columns.Add("药品序号", Type.GetType("System.Int64"));
            dt住院记费.Columns.Add("YPID", Type.GetType("System.Decimal"));
            dt住院记费.Columns.Add("进价", Type.GetType("System.Decimal"));
            dt住院记费.Columns.Add("规格", Type.GetType("System.String"));
            dt住院记费.Columns.Add("单位", Type.GetType("System.String"));
            dt住院记费.Columns.Add("产地", Type.GetType("System.String"));
            dt住院记费.Columns.Add("批号", Type.GetType("System.String"));
            dt住院记费.Columns.Add("医生编码", Type.GetType("System.Int32"));
            dt住院记费.Columns.Add("住院号码", Type.GetType("System.String"));
            dt住院记费.Columns.Add("病人姓名", Type.GetType("System.String"));
            dt住院记费.Columns.Add("医生", Type.GetType("System.String"));
            dt住院记费.Columns.Add("药房编码", Type.GetType("System.String"));
            dt住院记费.Columns.Add("组别", Type.GetType("System.Int32"));
        }

        public static bool b可否退费(string _currZYID, string _sFyxh, string _sYpid, decimal _dec总量, bool _b是否药品费用)
        {
            //decimal dec总量 = _dec总量; //Convert.ToDecimal(this.myText数量.Text) * Convert.ToInt16(this.spinEdit处方贴数.Text);
            //当检查收费项目小于3时冲突，所以增加bsfyp是否药品判断    到了，不能出现重复编码的收费项目，后续问题太多，所以最好就是费用编码的时候避免出现这种情况！！！
            if (_b是否药品费用 == true)//药品项目(1西药费2中药)
            {
                string sqlCmdT = "";
                if (HIS.COMM.zdInfo.ModelUserInfo.用户名 == "Admin")
                {
                    sqlCmdT = "select isnull(sum(数量),0) from ZY在院费用 where ZYID=" + _currZYID +
                    " and ypid=" + _sYpid;
                    decimal dYl = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, sqlCmdT));
                    if (dYl == 0)
                    {
                        XtraMessageBox.Show("未记账此费用，不能继续退费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    if (dYl < Math.Abs(_dec总量))
                    {
                        XtraMessageBox.Show("退费数量大于记费数量，不能继续退费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    return true;
                }
                else
                {
                    sqlCmdT = "select isnull(sum(数量),0) from ZY在院费用 where ZYID=" + _currZYID +
                       " and 记费时间 >=GETDATE()-1 and ypid=" + _sYpid;
                    decimal dYl = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, sqlCmdT));
                    if (dYl == 0)
                    {
                        XtraMessageBox.Show("24小时内未记账此费用，普通操作人员不能继续退费，请联系管理员退费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    if (dYl < Math.Abs(_dec总量))
                    {
                        XtraMessageBox.Show("退费数量大于24小时内记账数量，普通操作人员不能继续退费，请联系管理员退费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    return true;
                }
            }
            else
            {
                string sqlCmdT = "";
                if (HIS.COMM.zdInfo.ModelUserInfo.用户名 == "Admin")
                {
                    sqlCmdT = "select isnull(sum(数量),0) from ZY在院费用 where ZYID=" + _currZYID +
                    " and 费用编码=" + _sFyxh;
                    decimal dYl = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, sqlCmdT));
                    if (dYl == 0)
                    {
                        XtraMessageBox.Show("未记账此费用，不能继续退费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    if (dYl < Math.Abs(_dec总量))
                    {
                        XtraMessageBox.Show("退费数量大于记费数量，不能继续退费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    return true;
                }
                else
                {
                    sqlCmdT = "select isnull(sum(数量),0) from ZY在院费用 where ZYID=" + _currZYID +
                       " and 记费时间 >=GETDATE()-1 and 费用编码=" + _sFyxh;
                    decimal dYl = Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, sqlCmdT));
                    if (dYl == 0)
                    {
                        XtraMessageBox.Show("24小时内未记账此费用，普通操作人员不能继续退费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    if (dYl < Math.Abs(_dec总量))
                    {
                        XtraMessageBox.Show("退费数量大于24小时记账数量，普通操作人员不能继续退费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    return true;
                }
            }
        }
        public static bool b是否超出欠款限制(decimal _d金额, HIS.Model.Pojo.Pojo病人信息 person)
        {
            try
            {
                person.欠款 = person.欠款 + _d金额;
                if (person.欠款 >= baseInfo.dec医院欠款下限)
                {
                    WEISHENG.COMM.msgHelper.ShowInformation("【" + person.病人姓名 + "】的当前欠款【" + person.欠款 + "】，" + "\r\n大于医院设定的欠款下限【" + baseInfo.dec医院欠款下限 + "】。");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public void v重算合计()
        //{
        //    dt费用摘要.Clear();
        //    DataTable dtDistinct = dt费用明细.DefaultView.ToTable(true, "费用序号");
        //    foreach (DataRow row in dtDistinct.Rows)
        //    {
        //        int iCurr费用小项编号 = (int)row["费用序号"];
        //        string s费用名称 = dt费用明细.Select("费用序号=" + iCurr费用小项编号.ToString())[0]["费用名称"].ToString();
        //        Decimal dec金额 = Convert.IsDBNull(dt费用明细.Compute("sum(金额)", "费用序号=" + iCurr费用小项编号.ToString())) == true ?
        //            0 : Convert.ToDecimal(dt费用明细.Compute("sum(金额)", "费用序号=" + iCurr费用小项编号.ToString()));
        //        DataRow row摘要 = dt费用摘要.NewRow();
        //        row摘要["序号"] = iCurr费用小项编号;
        //        if (iCurr费用小项编号.ToString() == s西药费小项编码)
        //        {
        //            row摘要["收费名称"] = "西药费";
        //        }
        //        else
        //            if (iCurr费用小项编号.ToString() == s中药费小项编码)
        //        {
        //            row摘要["收费名称"] = "中药费";
        //        }
        //        else
        //        {
        //            row摘要["收费名称"] = s费用名称;
        //        }
        //        row摘要["收费金额"] = dec金额;
        //        dt费用摘要.Rows.Add(row摘要);
        //    }
        //}

        //todo：迁移到classybjk项目
        public void v重新对应目录(DataTable dt医保药品对应, ref personYB person)
        {
            //foreach (DataRow rowNew费用明细 in dt费用明细.Rows)
            //{
            //    string i当前收费编号 = rowNew费用明细["费用序号"].ToString();
            //    string s药品序号 = rowNew费用明细["药品序号"].ToString();
            //    string s医保类别编码 = "1";
            //    if (i当前收费编号 == s中药费小项编码 || i当前收费编号 == s西药费小项编码)
            //    {
            //        s医保类别编码 = "1";                                //s类别名称 = "西药费";
            //        string _s医保编码 = ClassYBJK.YBML.Class目录对应.get医保中心编码(s药品序号, person, dt医保药品对应);
            //        rowNew费用明细["医保编码"] = _s医保编码;
            //        rowNew费用明细["医保名称"] = ClassYBJK.YBML.Class目录对应.get医保中心名称(s药品序号, person, dt医保药品对应);
            //        string s项目大类 = ClassYBJK.YBML.Class目录对应.get医保中心医保大类编码(_s医保编码, person, dt医保药品对应);
            //        string s医保项目中类 = ClassYBJK.YBML.Class目录对应.get医保中心医保中类编码(_s医保编码, person, dt医保药品对应);
            //        rowNew费用明细["YB_项目大类"] = s项目大类;
            //        rowNew费用明细["YB_项目收费类别代码"] = s医保项目中类;
            //    }
            //    else
            //    {
            //        s医保类别编码 = "2";                                //s类别名称 = "诊疗项目";
            //        string _s医保编码 = ClassYBJK.YBML.Class目录对应.get医保中心编码("A" + i当前收费编号.ToString(), person, dt医保药品对应);
            //        rowNew费用明细["医保编码"] = _s医保编码;
            //        rowNew费用明细["医保名称"] = ClassYBJK.YBML.Class目录对应.get医保中心名称("A" + i当前收费编号.ToString(), person, dt医保药品对应);
            //        string s项目大类 = ClassYBJK.YBML.Class目录对应.get医保中心医保大类编码(_s医保编码, person, dt医保药品对应);
            //        string s医保项目中类 = ClassYBJK.YBML.Class目录对应.get医保中心医保中类编码(_s医保编码, person, dt医保药品对应);
            //        rowNew费用明细["YB_项目大类"] = s项目大类;
            //        rowNew费用明细["YB_项目收费类别代码"] = s医保项目中类;
            //    }
            //}
        }

        public void Hjje(DataRow row费用明细)
        {
            //int iCurr费用小项编号 = (int)row费用明细["费用序号"];
            //string s费用名称 = row费用明细["费用名称"].ToString();
            //Decimal dec金额 = Convert.IsDBNull(dt费用明细.Compute("sum(金额)", "费用序号=" + iCurr费用小项编号.ToString())) == true ?
            //    0 : Convert.ToDecimal(dt费用明细.Compute("sum(金额)", "费用序号=" + iCurr费用小项编号.ToString()));
            //if (dt费用摘要.Select("序号=" + iCurr费用小项编号.ToString()).Length == 0)
            //{
            //    DataRow row新增摘要 = dt费用摘要.NewRow();
            //    row新增摘要["序号"] = iCurr费用小项编号;

            //    if (iCurr费用小项编号.ToString() == s西药费小项编码)
            //    {
            //        row新增摘要["收费名称"] = "西药费";
            //    }
            //    else
            //        if (iCurr费用小项编号.ToString() == s中药费小项编码)
            //    {
            //        row新增摘要["收费名称"] = "中药费";
            //    }
            //    else
            //    {
            //        row新增摘要["收费名称"] = s费用名称;
            //    }
            //    row新增摘要["收费金额"] = dec金额;
            //    dt费用摘要.Rows.Add(row新增摘要);
            //}
            //else
            //{
            //    DataRow row更新摘要 = dt费用摘要.Select("序号=" + iCurr费用小项编号.ToString())[0];
            //    row更新摘要["收费金额"] = dec金额;
            //    dt费用摘要.AcceptChanges();
            //}
        }

        public void Hjje(int i收费项目, HIS.COMM.ClassCflx.enCflx enCflx, string _收费名称)
        {
            //Decimal dec金额 = Convert.IsDBNull(dt费用明细.Compute("sum(金额)", "费用序号=" + i收费项目.ToString())) == true ?
            //    0 : Convert.ToDecimal(dt费用明细.Compute("sum(金额)", "费用序号=" + i收费项目.ToString()));

            //if (dt费用摘要.Select("序号=" + i收费项目.ToString()).Length == 0)
            //{
            //    DataRow r2 = dt费用摘要.NewRow();
            //    r2["序号"] = i收费项目;
            //    switch (enCflx)
            //    {
            //        case HIS.COMM.ClassCflx.enCflx.西药方:
            //            r2["收费名称"] = "西药费";
            //            break;
            //        case HIS.COMM.ClassCflx.enCflx.中药方:
            //            r2["收费名称"] = "中药费";
            //            break;
            //        default:
            //            r2["收费名称"] = _收费名称;
            //            break;
            //    }
            //    r2["收费金额"] = dec金额;
            //    dt费用摘要.Rows.Add(r2);
            //}
            //else
            //{
            //    DataRow r2 = dt费用摘要.Select("序号=" + i收费项目.ToString())[0];
            //    r2["收费金额"] = dec金额;
            //    dt费用摘要.AcceptChanges();
            //}
        }
    }
}
