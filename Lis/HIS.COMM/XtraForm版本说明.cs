﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HIS.COMM
{
    public partial class XtraForm版本说明 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm版本说明()
        {
            InitializeComponent();
        }

        private void XtraForm版本说明_Load(object sender, EventArgs e)
        {
            richEditControl.LoadDocument(@"readme.doc");
        }
    }
}