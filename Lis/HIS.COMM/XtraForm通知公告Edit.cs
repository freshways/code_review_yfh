﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.IO;
using HISComm = HIS.COMM;

namespace HIS.COMM
{
    public partial class XtraForm通知公告Edit : DevExpress.XtraEditors.XtraForm
    {
        private string sConn;
        public XtraForm通知公告Edit(string _sConn)
        {
            InitializeComponent();
            sConn = _sConn;
        }

        public string colID;
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (textEdit标题.Text.Trim() == "")
                {
                    XtraMessageBox.Show("标题不能为空", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (richEditControl1.Text == "")
                {
                    XtraMessageBox.Show("内容不能为空", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(sConn))
                    {
                        string strAllFileName = openFileDialog1.FileName;
                        string strFileName = openFileDialog1.SafeFileName;

                        FileInfo myfile = null;
                        byte[] mybyte = null;
                        if (strAllFileName != "")
                        {
                            myfile = new FileInfo(strAllFileName);
                            FileStream mystream = myfile.OpenRead();
                            mybyte = new byte[myfile.Length];
                            mystream.Read(mybyte, 0, Convert.ToInt32(mystream.Length));
                        }

                        string a = richEditControl1.HtmlText;
                        con.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = con;
                        if (!string.IsNullOrEmpty(colID))
                        {
                            string s附件 = mybyte != null ? "附件文件名=@filename,附件内容=@file," : "";
                            cmd.CommandText = "update pubnoteic set " + s附件 + " ctype=@ctype ,title=@title ,content=@content ,createuser=@createuser where iid=@iid";

                            if (mybyte != null)
                            {
                                cmd.Parameters.Add("@filename", SqlDbType.VarChar);
                                cmd.Parameters["@filename"].Value = strFileName;
                                cmd.Parameters.Add("@file", SqlDbType.Binary);
                                cmd.Parameters["@file"].Value = mybyte;
                            }

                            cmd.Parameters.Add("@ctype", SqlDbType.VarChar);
                            cmd.Parameters["@ctype"].Value = comboBoxEdit1.Text;
                            cmd.Parameters.Add("@title", SqlDbType.VarChar);
                            cmd.Parameters["@title"].Value = textEdit标题.Text;

                            cmd.Parameters.Add("@content", SqlDbType.Text);
                            cmd.Parameters["@content"].Value = a;

                            cmd.Parameters.Add("@createuser", SqlDbType.VarChar);
                            cmd.Parameters["@createuser"].Value = "系统管理员";

                            cmd.Parameters.Add("@iid", SqlDbType.Int);
                            cmd.Parameters["@iid"].Value = colID;
                        }
                        else
                        {
                            string s附件1 = mybyte != null ? " 附件文件名,附件内容," : "";
                            string s附件2 = mybyte != null ? " @filename,@file," : "";
                            cmd.CommandText = "insert into pubnoteic(content,ctype ,title ," + s附件1 +
                                "createuser) values (@content,@ctype ,@title ," + s附件2 + "@createuser)";
                            if (mybyte != null)
                            {
                                cmd.Parameters.Add("@filename", SqlDbType.VarChar);
                                cmd.Parameters["@filename"].Value = strFileName;

                                cmd.Parameters.Add("@file", SqlDbType.Binary);
                                cmd.Parameters["@file"].Value = mybyte;
                            }
                            cmd.Parameters.Add("@ctype", SqlDbType.VarChar);
                            cmd.Parameters["@ctype"].Value = comboBoxEdit1.Text;
                            cmd.Parameters.Add("@title", SqlDbType.VarChar);
                            cmd.Parameters["@title"].Value = textEdit标题.Text;

                            cmd.Parameters.Add("@content", SqlDbType.Text);
                            cmd.Parameters["@content"].Value = a;

                            cmd.Parameters.Add("@createuser", SqlDbType.VarChar);
                            cmd.Parameters["@createuser"].Value = "系统管理员";
                        }
                        int t = (int)(cmd.ExecuteNonQuery());
                        if (t > 0)
                        {
                            XtraMessageBox.Show("保存成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        con.Close();
                        //XtraMessageBox.Show("保存成功!");
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void XtraForm通知公告Edit_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(colID))
            {
                using (SqlConnection con = new SqlConnection(sConn))
                {
                    string strSql = "select *  from pubnoteic where iid=" + colID;

                    DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(con, CommandType.Text, strSql).Tables[0];

                    textEdit标题.Text = dt.Rows[0]["title"].ToString();
                    richEditControl1.HtmlText = dt.Rows[0]["content"].ToString();
                    if (dt.Rows[0]["ctype"].ToString() == "一般")
                    {
                        comboBoxEdit1.SelectedIndex = 0;
                    }
                    else
                    {
                        comboBoxEdit1.SelectedIndex = 1;
                    }
                }
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void labelControl1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                XtraForm操作验证 lg = new XtraForm操作验证();
                if (lg.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                string sConn = HIS.COMM.ClassPubArgument.s知识库连接();
                using (SqlConnection con = new SqlConnection(sConn))
                {

                    string s公告内容 = richEditControl1.HtmlText;
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    string pubIId = HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + "." + colID;
                    int i内容 = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(sConn, CommandType.Text, "select count(*) from pubnoteic where iid='" + pubIId + "'"));
                    if (i内容 == 1)
                    {
                        cmd.CommandText = "update pubnoteic set ctype=@ctype ,title=@title ,content=@content ,createuser=@createuser where iid=@iid";
                        cmd.Parameters.Add("@ctype", SqlDbType.VarChar);
                        cmd.Parameters["@ctype"].Value = comboBoxEdit1.Text;
                        cmd.Parameters.Add("@title", SqlDbType.VarChar);
                        cmd.Parameters["@title"].Value = textEdit标题.Text;

                        cmd.Parameters.Add("@content", SqlDbType.Text);
                        cmd.Parameters["@content"].Value = s公告内容;

                        cmd.Parameters.Add("@createuser", SqlDbType.VarChar);
                        cmd.Parameters["@createuser"].Value = "acvSoft";

                        cmd.Parameters.Add("@iid", SqlDbType.VarChar);
                        cmd.Parameters["@iid"].Value = pubIId;
                    }
                    else
                    {
                        cmd.CommandText = "insert into pubnoteic(ctype ,title ,content ,createuser,iid) values (@ctype ,@title ,@content ,@createuser,@iid)";
                        cmd.Parameters.Add("@ctype", SqlDbType.VarChar);
                        cmd.Parameters["@ctype"].Value = comboBoxEdit1.Text;

                        cmd.Parameters.Add("@title", SqlDbType.VarChar);
                        cmd.Parameters["@title"].Value = textEdit标题.Text;

                        cmd.Parameters.Add("@content", SqlDbType.Text);
                        cmd.Parameters["@content"].Value = s公告内容;

                        cmd.Parameters.Add("@createuser", SqlDbType.VarChar);
                        cmd.Parameters["@createuser"].Value = "acvSoft";

                        cmd.Parameters.Add("@iid", SqlDbType.VarChar);
                        cmd.Parameters["@iid"].Value = pubIId;
                    }
                    int t = (int)(cmd.ExecuteNonQuery());
                    if (t > 0)
                    {
                        XtraMessageBox.Show("保存成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }

        private void simpleButton上传附件_Click(object sender, EventArgs e)
        {
            //OpenFileDialog op = new OpenFileDialog();
            //op.InitialDirectory = @"C:\Users\xx\Desktop\自动升级组件包";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Filter = "压缩包(*.zip)|*.zip|所有文件(*.*)|*.*";
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            labelControl文件名.Text = openFileDialog1.FileName;
            //labelSafeFileName.Text = op.SafeFileName;
        }
    }
}