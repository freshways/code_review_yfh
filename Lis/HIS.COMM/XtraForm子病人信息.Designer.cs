﻿namespace HIS.COMM
{
    partial class XtraForm子病人信息
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm子病人信息));
            this.treeList类别 = new DevExpress.XtraTreeList.TreeList();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton编辑 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton增加 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl内容 = new DevExpress.XtraGrid.GridControl();
            this.gridView内容 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton放弃 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton删除 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton保存 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.treeList类别)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl内容)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView内容)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // treeList类别
            // 
            this.treeList类别.Location = new System.Drawing.Point(12, 12);
            this.treeList类别.Name = "treeList类别";
            this.treeList类别.OptionsBehavior.Editable = false;
            this.treeList类别.Size = new System.Drawing.Size(112, 438);
            this.treeList类别.TabIndex = 3;
            this.treeList类别.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeList子病人信息_FocusedNodeChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton编辑);
            this.layoutControl1.Controls.Add(this.simpleButton增加);
            this.layoutControl1.Controls.Add(this.simpleButton刷新);
            this.layoutControl1.Controls.Add(this.gridControl内容);
            this.layoutControl1.Controls.Add(this.simpleButton放弃);
            this.layoutControl1.Controls.Add(this.treeList类别);
            this.layoutControl1.Controls.Add(this.simpleButton删除);
            this.layoutControl1.Controls.Add(this.simpleButton保存);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(126, 319, 250, 350);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(729, 462);
            this.layoutControl1.TabIndex = 8;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton编辑
            // 
            this.simpleButton编辑.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton编辑.Image")));
            this.simpleButton编辑.Location = new System.Drawing.Point(478, 428);
            this.simpleButton编辑.Name = "simpleButton编辑";
            this.simpleButton编辑.Size = new System.Drawing.Size(75, 22);
            this.simpleButton编辑.StyleController = this.layoutControl1;
            this.simpleButton编辑.TabIndex = 12;
            this.simpleButton编辑.Text = "编辑";
            this.simpleButton编辑.Click += new System.EventHandler(this.simpleButton编辑_Click);
            // 
            // simpleButton增加
            // 
            this.simpleButton增加.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton增加.Image")));
            this.simpleButton增加.Location = new System.Drawing.Point(317, 428);
            this.simpleButton增加.Name = "simpleButton增加";
            this.simpleButton增加.Size = new System.Drawing.Size(75, 22);
            this.simpleButton增加.StyleController = this.layoutControl1;
            this.simpleButton增加.TabIndex = 11;
            this.simpleButton增加.Text = "增加";
            this.simpleButton增加.Click += new System.EventHandler(this.simpleButton增加_Click);
            // 
            // simpleButton刷新
            // 
            this.simpleButton刷新.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton刷新.Image")));
            this.simpleButton刷新.Location = new System.Drawing.Point(238, 428);
            this.simpleButton刷新.Name = "simpleButton刷新";
            this.simpleButton刷新.Size = new System.Drawing.Size(75, 22);
            this.simpleButton刷新.StyleController = this.layoutControl1;
            this.simpleButton刷新.TabIndex = 10;
            this.simpleButton刷新.Text = "刷新";
            this.simpleButton刷新.Click += new System.EventHandler(this.simpleButton刷新_Click);
            // 
            // gridControl内容
            // 
            this.gridControl内容.Location = new System.Drawing.Point(133, 12);
            this.gridControl内容.MainView = this.gridView内容;
            this.gridControl内容.Name = "gridControl内容";
            this.gridControl内容.Size = new System.Drawing.Size(584, 412);
            this.gridControl内容.TabIndex = 9;
            this.gridControl内容.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView内容});
            // 
            // gridView内容
            // 
            this.gridView内容.GridControl = this.gridControl内容;
            this.gridView内容.Name = "gridView内容";
            this.gridView内容.OptionsBehavior.Editable = false;
            this.gridView内容.OptionsView.ShowFooter = true;
            this.gridView内容.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButton放弃
            // 
            this.simpleButton放弃.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton放弃.Image")));
            this.simpleButton放弃.Location = new System.Drawing.Point(639, 428);
            this.simpleButton放弃.Name = "simpleButton放弃";
            this.simpleButton放弃.Size = new System.Drawing.Size(78, 22);
            this.simpleButton放弃.StyleController = this.layoutControl1;
            this.simpleButton放弃.TabIndex = 8;
            this.simpleButton放弃.Text = "放弃";
            this.simpleButton放弃.Visible = false;
            this.simpleButton放弃.Click += new System.EventHandler(this.simpleButton放弃_Click);
            // 
            // simpleButton删除
            // 
            this.simpleButton删除.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton删除.Image")));
            this.simpleButton删除.Location = new System.Drawing.Point(396, 428);
            this.simpleButton删除.Name = "simpleButton删除";
            this.simpleButton删除.Size = new System.Drawing.Size(78, 22);
            this.simpleButton删除.StyleController = this.layoutControl1;
            this.simpleButton删除.TabIndex = 7;
            this.simpleButton删除.Text = "删除";
            this.simpleButton删除.Click += new System.EventHandler(this.simpleButton删除_Click);
            // 
            // simpleButton保存
            // 
            this.simpleButton保存.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton保存.Image")));
            this.simpleButton保存.Location = new System.Drawing.Point(557, 428);
            this.simpleButton保存.Name = "simpleButton保存";
            this.simpleButton保存.Size = new System.Drawing.Size(78, 22);
            this.simpleButton保存.StyleController = this.layoutControl1;
            this.simpleButton保存.TabIndex = 5;
            this.simpleButton保存.Text = "保存";
            this.simpleButton保存.Visible = false;
            this.simpleButton保存.Click += new System.EventHandler(this.simpleButton保存_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.splitterItem1,
            this.layoutControlItem7,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem3,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(729, 462);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.treeList类别;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(116, 442);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(116, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 442);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gridControl内容;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(121, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(588, 416);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(121, 416);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(105, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton刷新;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(226, 416);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(79, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(79, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(79, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton删除;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(384, 416);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButton保存;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(545, 416);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton放弃;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(627, 416);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton增加;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(305, 416);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(79, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(79, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(79, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.simpleButton编辑;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(466, 416);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(79, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(79, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(79, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // XtraForm子病人信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(729, 462);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraForm子病人信息";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "子病人信息";
            this.Load += new System.EventHandler(this.XtraForm子病人信息_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeList类别)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl内容)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView内容)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeList类别;
        private DevExpress.XtraEditors.SimpleButton simpleButton保存;
        private DevExpress.XtraEditors.SimpleButton simpleButton删除;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton simpleButton放弃;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraGrid.GridControl gridControl内容;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView内容;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton simpleButton刷新;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButton增加;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton simpleButton编辑;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
    }
}