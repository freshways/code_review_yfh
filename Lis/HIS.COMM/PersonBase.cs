﻿using HIS.COMM.Helper;
using HIS.Model.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM
{
    public class PersonBase
    {

        #region 医卡通
        private string _s信息来源;

        public string S信息来源
        {
            get { return _s信息来源; }
            set { _s信息来源 = value; }
        }


        private string _s账户状态;

        public string S账户状态
        {
            get { return _s账户状态; }
            set { _s账户状态 = value; }
        }

        private string _s发卡时间;

        public string S发卡时间
        {
            get { return _s发卡时间; }
            set { _s发卡时间 = value; }
        }
        private decimal _dec当前余额;

        public decimal Dec医卡通当前余额
        {
            get { return _dec当前余额; }
            set { _dec当前余额 = value; }
        }
        private string _s医卡通账户号 = "0";

        public string S医卡通账户号
        {
            get { return _s医卡通账户号; }
            set { _s医卡通账户号 = value; }
        }
        #endregion
        #region HIS门诊预付费发卡

        private System.Drawing.Image _image = null;

        public System.Drawing.Image Image
        {
            get { return _image; }
            set { _image = value; }
        }
        ClassEnum.en数据源 _en数据源;

        public ClassEnum.en数据源 en数据源
        {
            get { return _en数据源; }
            set { _en数据源 = value; }
        }

        #endregion

        //医保医生管理改造升级增加
        private string s医生编码;//处方医生编码
        public string S医生编码
        {
            get
            {
                return s医生编码;
            }

            set
            {
                s医生编码 = value;
            }
        }

        private string _s医生姓名;//医生姓名

        public string s医生姓名
        {
            get
            {
                try
                {
                    return _s医生姓名.Split('|')[1];
                }
                catch (Exception ex)
                {
                    return _s医生姓名;
                }
            }
            set { _s医生姓名 = value; }
        }

        private string s科室;
        public string S科室
        {
            get { return s科室; }
            set { s科室 = value; }
        }

        private string _s年龄单位;

        public string S年龄单位
        {
            get { return _s年龄单位; }
            set { _s年龄单位 = value; }
        }

        private HIS.COMM.ClassEnum.en收款类型 en医保类型;

        public HIS.COMM.ClassEnum.en收款类型 En医保类型
        {
            get { return en医保类型; }
            set { en医保类型 = value; }
        }


        private string _s家庭地址;

        public string S家庭地址
        {
            get { return _s家庭地址; }
            set { _s家庭地址 = value; }
        }

        private int _i年龄;

        public int I年龄
        {
            get { return DateTime.Now.Year - _dtime出生日期.Year; }//_i年龄;
            set { _i年龄 = value; }
        }

        private DateTime _dtime出生日期;

        public DateTime Dtime出生日期
        {
            get { return _dtime出生日期; }
            set { _dtime出生日期 = value; }
        }



        private string _s年龄 = "0";

        public string S年龄
        {
            get { return _s年龄; }
            set { _s年龄 = value; }
        }

        private string _s出生日期;

        public string s出生日期
        {
            get
            {

                return _s出生日期;
            }
            set
            {
                _s出生日期 = value;


            }
        }


        private string _s身份证号;

        public string S身份证号
        {
            get { return _s身份证号; }
            set
            {

                if (string.IsNullOrEmpty(value))
                {
                    return;
                }
                if (value.ToString().Substring(0, 1) == "9")
                {
                    return;
                }
                if (value.ToString().Length != 18)
                {
                    return;
                }
                _s身份证号 = value;
                AgeHelper.GetOutAgeAndUnitBySFZH(value, out _s年龄, out _s年龄单位);
                _i年龄 = Convert.ToInt16(_s年龄);
                _s出生日期 = AgeHelper.GetBirthdayBySFZH(value);
                _dtime出生日期 = Convert.ToDateTime(_s出生日期);
            }
        }
        private string _s性别; //性别 1：男  2：女

        public string S性别
        {
            get { return _s性别; }
            set
            {
                if (value == "1" || value == "男")
                {
                    _s性别 = "男";
                }
                else
                {
                    _s性别 = "女";
                }
            }
        }

        private string _openID = "";



        private string _s联系人姓名 = "";

        public string S联系人姓名
        {
            get { return _s联系人姓名; }
            set { _s联系人姓名 = value; }
        }


        private string _s联系人亲属关系 = "本人";

        public string S联系人亲属关系
        {
            get { return _s联系人亲属关系; }
            set { _s联系人亲属关系 = value; }
        }

        private string _s联系电话 = "";

        public string S联系电话
        {
            get { return _s联系电话; }
            set { _s联系电话 = value; }
        }




        private string _s病人姓名;

        public string S姓名
        {
            get { return _s病人姓名; }
            set { _s病人姓名 = value; }
        }




        private string _s医保类型;

        public string S医保类型
        {
            get { return _s医保类型; }
            set { _s医保类型 = value; }
        }

        private string _s医疗证号;

        public string S医疗证号
        {
            get { return _s医疗证号; }
            set { _s医疗证号 = value; }
        }

        private string _s婚姻状况;

        public string S婚姻状况
        {
            get { return _s婚姻状况; }
            set { _s婚姻状况 = value; }
        }

        private string _s职业;

        public string S职业
        {
            get { return _s职业; }
            set { _s职业 = value; }
        }

        private string _s工作单位;

        public string S工作单位
        {
            get { return _s工作单位; }
            set { _s工作单位 = value; }
        }

        //private string _s家庭地址;

        //public string S家庭地址
        //{
        //    get { return _s家庭地址; }
        //    set { _s家庭地址 = value; }
        //}

        private StringBuilder _sbErrInfo = new StringBuilder();
        public StringBuilder sbErrInfo
        {
            get { return _sbErrInfo; }
            set { _sbErrInfo = value; }
        }


        private string _s疾病名称;

        public string s疾病名称
        {
            get { return _s疾病名称; }
            set { _s疾病名称 = value; }
        }
        private string _s疾病编码;

        public string s疾病编码
        {
            get { return _s疾病编码; }
            set { _s疾病编码 = value; }
        }
        #region 身份证相关
        private string _s国籍;
        public string S国籍
        {
            get
            {
                return _s国籍;
            }

            set
            {
                _s国籍 = value;
            }
        }

        private string _s民族;
        public string S民族
        {
            get
            {
                return _s民族;
            }

            set
            {
                _s民族 = value;
            }
        }

        public string S发证机关
        {
            get
            {
                return _s发证机关;
            }

            set
            {
                _s发证机关 = value;
            }
        }

        public string S有效期起始
        {
            get
            {
                return _s有效期起始;
            }

            set
            {
                if (value == null)
                {
                    return;
                }
                _s有效期起始 = value.Replace('.', '-');
            }
        }

        public string S有效期终止
        {
            get
            {
                return _s有效期终止;
            }

            set
            {
                if (value == "长期")
                {
                    _s有效期终止 = "null";
                }
                else
                {
                    if (value == null)
                    {
                        return;
                    }
                    _s有效期终止 = value.Replace('.', '-');
                }

            }
        }

        private string _s发证机关;

        private string _s有效期起始;
        private string _s有效期终止;


        #endregion


        public string S民族编码 { get; set; }

        public string OpenID
        {
            get
            {
                return _openID;
            }

            set
            {
                _openID = value;
            }
        }
    }
}
