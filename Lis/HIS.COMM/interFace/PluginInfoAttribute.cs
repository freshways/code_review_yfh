﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM.interFace
{
    public class PluginInfoAttribute : System.Attribute
    {
        /// <summary>
        /// Deprecated. Do not use.
        /// </summary>
        public PluginInfoAttribute() { }

        public PluginInfoAttribute(string pluginName, string version, string author, string webpage, bool loadWhenStart, int index, string guid, string Tel,string s详细说明)
        {
            _Name = pluginName;
            _Version = version;
            _Author = author;
            _Webpage = webpage;
            _LoadWhenStart = loadWhenStart;
            _Index = index;
            _GUID = guid;
            _Tel = Tel;
            _s详细说明 = s详细说明;
        }
        private string _Tel;

        private string _s详细说明;

        public string S详细说明
        {
            get { return _s详细说明; }
            set { _s详细说明 = value; }
        }


        public string Tel
        {
            get { return _Tel; }
            set { _Tel = value; }
        }

        private string _GUID;

        public string GUID
        {
            get { return _GUID; }
            set { _GUID = value; }
        }

        public string pluginName
        {
            get
            {
                return _Name;
            }
        }

        public string Version
        {
            get
            {
                return _Version;
            }
        }

        public string Author
        {
            get
            {
                return _Author;
            }
        }

        public string Webpage
        {
            get
            {
                return _Webpage;
            }
        }

        public bool LoadWhenStart
        {
            get
            {
                return _LoadWhenStart;
            }
        }

        /// <summary>
        /// 用来存储一些有用的信息
        /// </summary>
        public object Tag
        {
            get
            {
                return _Tag;
            }
            set
            {
                _Tag = value;
            }
        }

        public int Index
        {
            get
            {
                return _Index;
            }
            set
            {
                _Index = value;
            }
        }

        private string _Name = "";
        private string _Version = "";
        private string _Author = "";
        private string _Webpage = "";
        private object _Tag = null;
        private int _Index = 0;

        private bool _LoadWhenStart = true;
    }
}
