﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.InterFace
{
    interface ISaveEntity<DbContext, T>
    {
        bool SaveEntity(DbContext chis, T model);
    }
    interface ISaveEntity
    {
        bool SaveEntity(DbContext chis);
    }
}
