﻿using HIS.Model;
using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WEISHENG.COMM;
using WEISHENG.COMM.PluginsAttribute;

namespace HIS.COMM
{
    [ClassInfoMark(GUID = "FF8F8CA4-13CA-4E73-813D-E3C96C5F626E", 键ID = "DDFCBFF3-3394-43EF-A977-DCEE714CF4B3", 父ID = "578FE7C6-E3F2-4FFD-91E6-992FD573746A",
功能名称 = "人员排班", 程序集名称 = "HIS.COMM", 程序集调用类地址 = "XtraForm排班",
传递参数 = "", 显示顺序 = 100,
菜单类型 = "WinForm", 全屏打开 = false, 图标名称 = "嘱托维护.png", 是否显示 = true)]
    public class Menu护理排班
    {

    }



    public class baseInfo
    {
        private static string _sXML = string.Empty;
        private static string _sReSetconnHisPub(WEISHENG.COMM.ClassEnum.en数据库连接获取方式 _en数据库连接方式, string _sConfigFileName, bool _bReConfig = false)
        {
            switch (_en数据库连接方式)
            {
                case WEISHENG.COMM.ClassEnum.en数据库连接获取方式.本地配置文件:
                    string connHisPub = string.Empty;
                    try
                    {
                        connHisPub = code.DecryptStr(configHelper.getKeyValue("connHisPub"), code.PubKey2);
                        if (connHisPub == "Key Error..." || _bReConfig)
                        {
                            connHisPub = WEISHENG.COMM.Helper.db_connectHelper.setConnSqlUI(connHisPub, "HisPub连接参数");
                            configHelper.setKeyValue("connHisPub", code.EncryptStr(connHisPub, code.PubKey2));
                        }
                        return connHisPub;
                    }
                    catch (Exception ee)
                    {
                        return "";
                    }

                case WEISHENG.COMM.ClassEnum.en数据库连接获取方式.默认服务器:
                case WEISHENG.COMM.ClassEnum.en数据库连接获取方式.Web服务器:
                default:
                    if (_sXML == string.Empty)
                    {
                        //_sXML = HIS.Web.WebAPI.allinoneGetConnString(WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress);
                    }
                    return code.DecryptStr(WEISHENG.COMM.XML.XmlFiles.GetNodeValue(_sXML, "connHisPub"), code.PubKey2);
            }
        }

        private static string _sReSetconnHis(WEISHENG.COMM.ClassEnum.en数据库连接获取方式 _en数据库连接方式, string _sConfigFileName, bool _bReConfig = false)
        {
            string reString = "";
            switch (_en数据库连接方式)
            {
                case WEISHENG.COMM.ClassEnum.en数据库连接获取方式.本地配置文件:
                    string connHisDb = string.Empty;
                    try
                    {
                        connHisDb = code.DecryptStr(WEISHENG.COMM.configHelper.getKeyValue("connHisDb"), code.PubKey2);
                        if (connHisDb == "Key Error..." || _bReConfig)
                        {
                            connHisDb = WEISHENG.COMM.Helper.db_connectHelper.setConnSqlUI(connHisDb, "His连接参数");
                            WEISHENG.COMM.configHelper.setKeyValue("connHisDb", code.EncryptStr(connHisDb, code.PubKey2));
                        }
                        reString = connHisDb;
                    }
                    catch (Exception ee)
                    {
                        reString = "";
                    }
                    break;
                case WEISHENG.COMM.ClassEnum.en数据库连接获取方式.默认服务器:
                case WEISHENG.COMM.ClassEnum.en数据库连接获取方式.Web服务器:
                    if (_sXML == string.Empty)
                    {
                        //_sXML = HIS.Web.WebAPI.allinoneGetConnString(WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress);
                    }
                    reString = code.DecryptStr(WEISHENG.COMM.XML.XmlFiles.GetNodeValue(_sXML, "connHisDb"), code.PubKey2);
                    break;
            }
            if (!reString.Contains("Timeout"))
            {
                reString += ";Connect Timeout=60";
            }
            return reString;

        }

        public static bool bResetPluginsConn(bool _bReConfig = false)
        {
            string connstring = string.Empty;
            try
            {
                connstring = HIS.COMM.DBConnHelper.SConnPlugins;
                if (connstring == "Key Error..." || _bReConfig)
                {
                    try
                    {
                        connstring = WEISHENG.COMM.Helper.db_connectHelper.setConnSqlUI(connstring, "插件仓库地址连接设置");
                    }
                    catch (Exception ex)
                    {
                        connstring = WEISHENG.COMM.Helper.db_connectHelper.setConnSqlUI("", "插件仓库地址连接设置");
                    }


                    var item = HIS.COMM.BLL.CacheData.Gy全局参数.Where(c => c.参数名称 == "插件仓库地址").FirstOrDefault();
                    if (item == null)
                    {
                        item = new GY全局参数();
                        item.参数值 = code.EncryptStr(connstring, code.PubKey2);
                        item.参数名称 = "插件仓库地址";
                        item.updateTime = DateTime.Now;
                        item.单位编码 = HIS.COMM.zdInfo.Model站点信息.单位编码;
                    }
                    else
                    {
                        item.参数值 = code.EncryptStr(connstring, code.PubKey2);
                    }


                    using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
                    {
                        chis.GY全局参数.Attach(item);
                        chis.Entry(item).State = item == null ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                        chis.SaveChanges();
                    }


                    msgBalloonHelper.ShowInformation("新插件仓库保存成功，软件重新打开生效。");
                }
                DBConnHelper.SConnPlugins = connstring;
                return true;
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
                return false;
            }

        }

        public static bool bSet数据库连接(bool _bReSetFlag)
        {
            try
            {
                DBConnHelper.SConnHISPubDb = _sReSetconnHisPub(WEISHENG.COMM.zdInfo.En数据库连接获取方式, WEISHENG.COMM.zdInfo.sConfigFileName, _bReSetFlag);
                DBConnHelper.SConnHISDb = _sReSetconnHis(WEISHENG.COMM.zdInfo.En数据库连接获取方式, WEISHENG.COMM.zdInfo.sConfigFileName, _bReSetFlag);
                DBConnHelper.SConnSfzh = ClassPubArgument.s身份证数据库连接();
                DBConnHelper.SConnPlugins = ClassPubArgument.s插件仓库地址();
                DBConnHelper.SConnYBJK = ClassPubArgument.s医保本地库连接();
                DBConnHelper.SConnYthString = ClassPubArgument.s一体化数据库连接();
                DBConnHelper.sLISConnString = code.DecryptStr(HIS.COMM.Helper.EnvironmentHelper.CommStringGet("LIS数据库连接", "", ""), code.PubKey2);
                if (HIS.COMM.zdInfo.Model单位信息.sDwmc == "测试单位" && HIS.COMM.zdInfo.Model单位信息.iDwid == 21233)
                {
                    DBConnHelper.sLISConnString = DBConnHelper.sLISConnString.Replace(WEISHENG.COMM.Helper.db_connectHelper.get连接串IP及端口(DBConnHelper.sLISConnString), "192.168.10.229,13201").Replace("CHIS", "LIS");
                    DBConnHelper.SConnPlugins = DBConnHelper.SConnPlugins.Replace(WEISHENG.COMM.Helper.db_connectHelper.get连接串IP(DBConnHelper.SConnPlugins), "192.168.10.229");
                }
                HIS.Model.Dal.SqlHelper.connectionString = DBConnHelper.SConnHISDb;
                HIS.Model.dataHelper.CreateInstance(DBConnHelper.SConnHISDb, DBConnHelper.SConnPlugins);//单例模式加载，初始化数据库连接
                return true;
            }
            catch (Exception ex)
            {
                WEISHENG.COMM.msgHelper.ShowInformation(ex.Message);
                return false;
            }
        }

        public static string s身份证照片临时文件名 = Application.StartupPath + "\\image.bmp";// "c:\\temp.bmp"; //System.Windows.Forms.Application.StartupPath + "\\temp.bmp";

        private static Class认证方式.en认证方式 _en站点认证方式;

        public static Class认证方式.en认证方式 En站点认证方式
        {
            get { return _en站点认证方式; }
            set { _en站点认证方式 = value; }
        }


        public static bool bGet软件设置状态()
        {
            try
            {
                baseInfo.EnPACS记费模式 = ClassPubArgument.enPACS记费模式();
                baseInfo.B启用医卡通 = ClassPubArgument.b启用医卡通();
                baseInfo.b启用医卡通门诊余额控制 = Helper.EnvironmentHelper.IsOneCardBalanceControl();

                baseInfo.Control是否新合门诊直报 = ClassPubArgument.b新农合门诊直报();
                baseInfo.Control床位管理是否显示空床 = ClassPubArgument.b床位管理是否显示空床();
                baseInfo.Control是否核查3次出院 = ClassPubArgument.b是否限定新合核查3次出院();
                baseInfo.Control入院登记是否必须录入家庭地址 = ClassPubArgument.b入院登记是否必须录入家庭地址();
                baseInfo.Control医嘱录入大夫可选 = ClassPubArgument.b医嘱录入界面大夫是否可选();
                baseInfo.B一般诊疗费当日不允许重复收取 = ClassPubArgument.b一般诊疗费当日不允许重复收取();
                baseInfo.B一般诊疗费自动记费 = ClassPubArgument.b一般诊疗费自动记费();

                baseInfo.B写检验申请单 = ClassPubArgument.b写检验申请单();
                baseInfo.B检验科确认费用 = ClassPubArgument.b检验科确认费用();

                baseInfo.bPACS医嘱验证开关 = ClassPubArgument.bPACS医嘱验证开关();
                baseInfo.b住院病人退费Ukey验证 = ClassPubArgument.b住院病人退费Ukey验证();
                baseInfo.S默认条码打印 = ClassPubArgument.s默认条码打印机名称();
                baseInfo.S默认腕带打印 = ClassPubArgument.s默认腕带打印机名称();
                //baseInfo.sPACS执行科室编码 = ClassPubArgument.sPACS执行科室编码();
                //baseInfo.sPACS执行科室名称 = Class科室选择.sGET科室名称(baseInfo.sPACS执行科室编码);



                baseInfo.B门诊收款集成医保报销 = ClassPubArgument.b门诊收款集成医保报销();
                baseInfo.B开启电子病历粘贴板 = ClassPubArgument.b开启电子病历粘贴板();
                baseInfo.dt病区权限 = baseInfo.dt获取病区权限();
                baseInfo.b分级诊疗开关 = ClassPubArgument.b分级诊疗开关();
                baseInfo.B开启签约服务 = ClassPubArgument.b签约服务();
                baseInfo.B开启传染病上报 = ClassPubArgument.b开启传染病上报();
                //baseInfo.B开启核三门诊收款直报 = ClassPubArgument.b开启核三门诊收款直报();
                baseInfo.B开启微信支付 = ClassPubArgument.b开启微信支付();
                baseInfo.B开启支付宝支付 = ClassPubArgument.b开启支付宝支付();
                baseInfo.dec医院欠款下限 = HIS.COMM.ClassPubArgument.dec医院欠款下限();
                baseInfo.B医嘱打印手签图片 = ClassPubArgument.b医嘱打印手签图片();
                baseInfo.B预交款凭条打印预览 = ClassPubArgument.b预交款凭条打印预览();
                baseInfo.B住院发票打印是否预览 = ClassPubArgument.b住院发票打印是否预览();

                baseInfo.B启用LIS插件 = Helper.EnvironmentHelper.CommBoolGet("启用LIS插件", "false", "护理执行写LIS申请");
                baseInfo.B启用RIS插件 = Helper.EnvironmentHelper.CommBoolGet("启用RIS插件", "false", "护理执行写");
                baseInfo.B启用LIS申请功能 = Helper.EnvironmentHelper.CommBoolGet("启用LIS申请功能", "false", "界面控制|启用后申请写SAM_APPLY，否则JY申请单摘要");
                if (B启用LIS插件)
                {
                    using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                    {
                        var LIS科室 = chis.GY科室设置.Where(c => c.分院编码 == HIS.COMM.zdInfo.Model站点信息.分院编码 && c.科室类型 == "LIS科室").FirstOrDefault();
                        if (LIS科室 != null)
                        {
                            baseInfo.sLIS执行科室编码 = LIS科室.科室编码.ToString();//ClassPubArgument.sLIS执行科室编码();
                            baseInfo.sLIS执行科室名称 = LIS科室.科室名称;// Class科室选择.sGET科室名称(baseInfo.sLIS执行科室编码);
                        }
                        else
                        {
                            msgBalloonHelper.BalloonShow("软件初始化未获取LIS科室");
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                string sErr = ex.Message;

                WEISHENG.COMM.LogHelper.Info("初始化BaseInfo", "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }
        public static string s获取全部科室数据权限(string conn)
        {
            string sRe = "";
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(conn, CommandType.Text,
                "select 科室编码 from pub用户数据权限 where 用户编码=" + HIS.COMM.zdInfo.ModelUserInfo.用户编码).Tables[0];
            if (dt.Rows.Count == 0)
            {
                sRe = "";
            }
            else
            {
                string reSu = "";
                foreach (DataRow row in dt.Rows)
                {
                    reSu = row[0].ToString() + "," + reSu;
                }
                sRe = reSu.Substring(0, reSu.Length - 1);
            }
            return sRe;
        }

        private static DataTable dt获取病区权限()
        {
            string SQL =
                "select bb.[科室编码] 病区编码, bb.[科室名称] " + "\r\n" +
                "from   pub用户数据权限 aa left outer join [GY科室设置] bb on aa.[科室编码] = bb.[科室编码] " + "\r\n" +
                "where  bb.[是否病区科室] = 1 and 用户编码 = " + HIS.COMM.zdInfo.ModelUserInfo.用户编码;
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL).Tables[0];
        }
        public static DataTable vDatatable获取药房权限(string _sConnHis, string _sConn站点设置)
        {
            string yfqx = HIS.Model.Dal.SqlHelper.ExecuteScalar(_sConn站点设置, CommandType.Text,
                "select 药房权限  from gy站点设置 where mac地址='" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "'").ToString();
            string sqlCmd = "";
            ArrayList al = new ArrayList(yfqx.Split('|'));
            for (int i = 0; i < al.Count; i++) //再增加3个元素
            {
                sqlCmd = al[i] + "," + sqlCmd;
            }
            sqlCmd = "select 科室名称,科室编码 from GY科室设置 where 是否单设药房=1 and 科室编码 in (" + sqlCmd.Substring(0, sqlCmd.Length - 1) + ")";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(_sConnHis, CommandType.Text, sqlCmd).Tables[0];
        }

        public static DataTable vDatatable获取分院列表(string sDwid)
        {
            string sqlCmd = "";
            sqlCmd = "select '1' 分院编码,'总院' 分院名称  union all  select 分院编码,分院名称  from tbHosItem where 医院编码='" + sDwid + "'";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnYBJK, CommandType.Text, sqlCmd).Tables[0];
        }




        private static bool _b开启电子病历粘贴板;

        public static bool B开启电子病历粘贴板
        {
            get { return baseInfo._b开启电子病历粘贴板; }
            set { baseInfo._b开启电子病历粘贴板 = value; }
        }
        private static bool _b门诊收款集成医保报销;

        public static bool B门诊收款集成医保报销
        {
            get { return baseInfo._b门诊收款集成医保报销; }
            set { baseInfo._b门诊收款集成医保报销 = value; }
        }


        private static string _sLIS执行科室名称;

        public static string sLIS执行科室名称
        {
            get { return baseInfo._sLIS执行科室名称; }
            set { baseInfo._sLIS执行科室名称 = value; }
        }



        private static string _sLIS执行科室编码;

        public static string sLIS执行科室编码
        {
            get { return baseInfo._sLIS执行科室编码; }
            set { baseInfo._sLIS执行科室编码 = value; }
        }
        private static string _s默认条码打印;

        public static string S默认条码打印
        {
            get { return baseInfo._s默认条码打印; }
            set { baseInfo._s默认条码打印 = value; }
        }

        private static string _s默认腕带打印;

        public static string S默认腕带打印
        {
            get { return baseInfo._s默认腕带打印; }
            set { baseInfo._s默认腕带打印 = value; }
        }


        private static bool _b住院病人退费Ukey验证;

        public static bool b住院病人退费Ukey验证
        {
            get { return baseInfo._b住院病人退费Ukey验证; }
            set { baseInfo._b住院病人退费Ukey验证 = value; }
        }
        private static bool _bPACS医嘱验证开关;

        public static bool bPACS医嘱验证开关
        {
            get { return baseInfo._bPACS医嘱验证开关; }
            set { baseInfo._bPACS医嘱验证开关 = value; }
        }

        private static bool _b一般诊疗费自动记费;

        public static bool B一般诊疗费自动记费
        {
            get { return baseInfo._b一般诊疗费自动记费; }
            set { baseInfo._b一般诊疗费自动记费 = value; }
        }

        private static bool _b一般诊疗费限制;

        public static bool B一般诊疗费当日不允许重复收取
        {
            get { return baseInfo._b一般诊疗费限制; }
            set { baseInfo._b一般诊疗费限制 = value; }
        }

        private static Boolean _Control医嘱录入大夫可选;

        public static Boolean Control医嘱录入大夫可选
        {
            get { return baseInfo._Control医嘱录入大夫可选; }
            set { baseInfo._Control医嘱录入大夫可选 = value; }
        }

        private static Boolean _Control床位管理是否显示空床;

        public static Boolean Control床位管理是否显示空床
        {
            get { return baseInfo._Control床位管理是否显示空床; }
            set { baseInfo._Control床位管理是否显示空床 = value; }
        }

        private static Boolean _Control是否核查3次出院;

        public static Boolean Control是否核查3次出院
        {
            get { return baseInfo._Control是否核查3次出院; }
            set { baseInfo._Control是否核查3次出院 = value; }
        }


        private static Boolean _Control是否新合门诊直报;

        public static Boolean Control是否新合门诊直报
        {
            get { return baseInfo._Control是否新合门诊直报; }
            set { baseInfo._Control是否新合门诊直报 = value; }
        }

        private static Boolean _control入院登记是否必须录入家庭地址;

        public static Boolean Control入院登记是否必须录入家庭地址
        {
            get { return baseInfo._control入院登记是否必须录入家庭地址; }
            set { baseInfo._control入院登记是否必须录入家庭地址 = value; }
        }

        private static Class发票结构.en门诊发票打印模式 _en门诊发票打印模式;
        public static Class发票结构.en门诊发票打印模式 en门诊发票打印模式
        {
            get { return baseInfo._en门诊发票打印模式; }
            set { baseInfo._en门诊发票打印模式 = value; }
        }
        private static Class发票结构.en住院发票打印模式 _en住院发票打印模式;

        public static Class发票结构.en住院发票打印模式 en住院发票打印模式
        {
            get { return baseInfo._en住院发票打印模式; }
            set { baseInfo._en住院发票打印模式 = value; }
        }

        private static bool _b写检验申请单;

        public static bool B写检验申请单
        {
            get { return baseInfo._b写检验申请单; }
            set { baseInfo._b写检验申请单 = value; }
        }

        private static bool _b检验科确认费用;

        public static bool B检验科确认费用
        {
            get { return baseInfo._b检验科确认费用; }
            set { baseInfo._b检验科确认费用 = value; }
        }

        private static bool _b分级诊疗开关;

        public static bool b分级诊疗开关
        {
            get { return baseInfo._b分级诊疗开关; }
            set { baseInfo._b分级诊疗开关 = value; }
        }

        private static ClassEnum.enPACS记费模式 _enPACS记费模式;

        private static bool _b启用医卡通;

        public static bool B启用医卡通
        {
            get { return baseInfo._b启用医卡通; }
            set { baseInfo._b启用医卡通 = value; }
        }

        private static bool _b启用医卡通门诊余额控制 = false;


        public static bool b启用医卡通门诊余额控制
        {
            get { return _b启用医卡通门诊余额控制; }
            //get { return Convert.ToBoolean(HIS.COMM.ClassPubArgument.getArgumentValue("启用医卡通门诊余额控制")); }
            set { _b启用医卡通门诊余额控制 = value; }
        }


        public static DataTable dt病区权限 { get; set; }
        public static bool B开启签约服务 { get; set; }

        public static bool B开启传染病上报 { get; set; }

        public static bool B开启核三门诊收款直报 { get; set; }

        public static bool B开启微信支付 { get; set; }

        public static bool B开启支付宝支付 { get; set; }
        public static decimal dec医院欠款下限 { get; private set; }

        public static bool B开启精准扶贫功能
        {
            get
            {
                return _b开启精准扶贫功能;
            }

            set
            {
                _b开启精准扶贫功能 = value;
            }
        }


        public static bool B预交款凭条打印预览
        {
            get
            {
                return b预交款凭条打印预览;
            }

            set
            {
                b预交款凭条打印预览 = value;
            }
        }

        public static bool B启用LIS申请功能 { get; set; }

        public static bool B启用LIS插件 { get; set; }
        public static bool B启用RIS插件 { get; set; }

        public static bool B住院发票打印是否预览
        {
            get
            {
                return _B住院发票打印是否预览;
            }

            set
            {
                _B住院发票打印是否预览 = value;
            }
        }

        public static ClassEnum.enPACS记费模式 EnPACS记费模式
        {
            get
            {
                return _enPACS记费模式;
            }

            set
            {
                _enPACS记费模式 = value;
            }
        }

        private static bool _b开启精准扶贫功能;
        public static bool B医嘱打印手签图片;

        private static bool b预交款凭条打印预览;
        private static Boolean _B住院发票打印是否预览;
    }
}
