﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;


namespace HIS.COMM
{
    public partial class XtraForm日志查询 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm日志查询()
        {
            InitializeComponent();
        }

        private void 日志查询_Load(object sender, EventArgs e)
        {
            dateEditKssj.EditValue = DateTime.Now.Date;
            dateEditJzsj.EditValue = DateTime.Now.Date.ToShortDateString() + " 23:59:59";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                   " select appver 版本号,modulename 模块名,eventtype 事件类型,eveninfo 事件信息,localtime 本机操作时间,username  用户名, ip, mac,computername 计算机名,otherinfo 其他信息,servertime  日志服务器时间 " +
                   " from pub_log where dwid='" + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() +
                   "' and localtime between '" + dateEditKssj.EditValue.ToString() + "' and '" + dateEditJzsj.EditValue.ToString() + "' " +
                   " order by servertime desc").Tables[0];
                gridControl1.DataSource = dt;
                gridView1.PopulateColumns();
                gridView1.BestFitColumns();
                gridView1.Columns["本机操作时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
                gridView1.Columns["日志服务器时间"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
                WEISHENG.COMM.LogHelper.Info(this.Text, "事件信息", "查询日志操作成功");
            }
            catch (Exception ex)
            {
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", ex.Message);
                MessageBox.Show(ex.Message);
                // throw ex;
            }

        }

        private void simpleButton导出_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "导出Excel";
            saveFileDialog.Filter = "Excel文件(*.xls)|*.xls";
            DialogResult dialogResult = saveFileDialog.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions();
                gridControl1.ExportToXls(saveFileDialog.FileName);
                DevExpress.XtraEditors.XtraMessageBox.Show("保存成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }
}