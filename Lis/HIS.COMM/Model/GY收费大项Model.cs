﻿// litao@Copy Right 2006-2016
// 文件： GY收费大项Model.cs
// 项目名称：HIS.COMM
// 创建时间：2018-05-26
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.COMM.Model
{
	/// <summary>
	///GY收费大项数据实体
	/// </summary>
	[DataContract]
	public partial class GY收费大项Model:INotifyPropertyChanged
	{
		#region 变量定义
		///<summary>
		///
		///</summary>
		private int _id = -1;
		///<summary>
		///
		///</summary>
		private int _项目编码;
		///<summary>
		///
		///</summary>
		private string _项目名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _拼音代码 = String.Empty;
		///<summary>
		///
		///</summary>
		private int _单位编码;
		#endregion
		
		#region 构造函数

		///<summary>
		///
		///</summary>
		public GY收费大项Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=项目编码></param>
		///<param name=项目名称></param>
		///<param name=拼音代码></param>
		///<param name=单位编码></param>
		public GY收费大项Model(int id,int 项目编码,string 项目名称,string 拼音代码,int 单位编码)
		{
			this._id = id;
			this._项目编码 = 项目编码;
			this._项目名称 = 项目名称;
			this._拼音代码 = 拼音代码;
			this._单位编码 = 单位编码;
		}
        
		#endregion
		
		#region 公共属性

		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "项目编码", IsRequired = false, Order = 1)]
		public int 项目编码
		{
			get {return _项目编码;}
			set {
            _项目编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("项目编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "项目名称", IsRequired = false, Order = 2)]
		public string 项目名称
		{
			get {return _项目名称;}
			set {
            _项目名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("项目名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "拼音代码", IsRequired = false, Order = 3)]
		public string 拼音代码
		{
			get {return _拼音代码;}
			set {
            _拼音代码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("拼音代码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单位编码", IsRequired = false, Order = 4)]
		public int 单位编码
		{
			get {return _单位编码;}
			set {
            _单位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单位编码"));
            }
		}
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
		#endregion
		
	}
}
