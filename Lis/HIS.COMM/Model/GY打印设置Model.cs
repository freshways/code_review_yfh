﻿// litao@Copy Right 2006-2016
// 文件： GY打印设置Model.cs
// 项目名称：HIS.COMM
// 创建时间：2018-05-26
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.COMM.Model
{
	/// <summary>
	///GY打印设置数据实体
	/// </summary>
	[DataContract]
	public partial class GY打印设置Model:INotifyPropertyChanged
	{
		#region 变量定义
		///<summary>
		///
		///</summary>
		private int _id = -1;
		///<summary>
		///
		///</summary>
		private string _mac地址 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _站点名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _打印模块 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _打印机名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _纸张名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private int _纸张宽度;
		///<summary>
		///
		///</summary>
		private int _纸张高度;
		///<summary>
		///
		///</summary>
		private int _上页边距;
		///<summary>
		///
		///</summary>
		private int _下页边距;
		///<summary>
		///
		///</summary>
		private int _左页边距;
		///<summary>
		///
		///</summary>
		private int _右页边距;
		///<summary>
		///
		///</summary>
		private bool _是否直接打印;
		///<summary>
		///
		///</summary>
		private bool _是否打印预览;
		///<summary>
		///
		///</summary>
		private string _备注 = String.Empty;
		///<summary>
		///
		///</summary>
		private int _单位编码;
		#endregion
		
		#region 构造函数

		///<summary>
		///
		///</summary>
		public GY打印设置Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=mac地址></param>
		///<param name=站点名称></param>
		///<param name=打印模块></param>
		///<param name=打印机名称></param>
		///<param name=纸张名称></param>
		///<param name=纸张宽度></param>
		///<param name=纸张高度></param>
		///<param name=上页边距></param>
		///<param name=下页边距></param>
		///<param name=左页边距></param>
		///<param name=右页边距></param>
		///<param name=是否直接打印></param>
		///<param name=是否打印预览></param>
		///<param name=备注></param>
		///<param name=单位编码></param>
		public GY打印设置Model(int id,string mac地址,string 站点名称,string 打印模块,string 打印机名称,string 纸张名称,int 纸张宽度,int 纸张高度,int 上页边距,int 下页边距,int 左页边距,int 右页边距,bool 是否直接打印,bool 是否打印预览,string 备注,int 单位编码)
		{
			this._id = id;
			this._mac地址 = mac地址;
			this._站点名称 = 站点名称;
			this._打印模块 = 打印模块;
			this._打印机名称 = 打印机名称;
			this._纸张名称 = 纸张名称;
			this._纸张宽度 = 纸张宽度;
			this._纸张高度 = 纸张高度;
			this._上页边距 = 上页边距;
			this._下页边距 = 下页边距;
			this._左页边距 = 左页边距;
			this._右页边距 = 右页边距;
			this._是否直接打印 = 是否直接打印;
			this._是否打印预览 = 是否打印预览;
			this._备注 = 备注;
			this._单位编码 = 单位编码;
		}
        
		#endregion
		
		#region 公共属性

		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "MAC地址", IsRequired = false, Order = 1)]
		public string MAC地址
		{
			get {return _mac地址;}
			set {
            _mac地址 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("MAC地址"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "站点名称", IsRequired = false, Order = 2)]
		public string 站点名称
		{
			get {return _站点名称;}
			set {
            _站点名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("站点名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "打印模块", IsRequired = false, Order = 3)]
		public string 打印模块
		{
			get {return _打印模块;}
			set {
            _打印模块 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("打印模块"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "打印机名称", IsRequired = false, Order = 4)]
		public string 打印机名称
		{
			get {return _打印机名称;}
			set {
            _打印机名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("打印机名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "纸张名称", IsRequired = false, Order = 5)]
		public string 纸张名称
		{
			get {return _纸张名称;}
			set {
            _纸张名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("纸张名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "纸张宽度", IsRequired = false, Order = 6)]
		public int 纸张宽度
		{
			get {return _纸张宽度;}
			set {
            _纸张宽度 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("纸张宽度"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "纸张高度", IsRequired = false, Order = 7)]
		public int 纸张高度
		{
			get {return _纸张高度;}
			set {
            _纸张高度 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("纸张高度"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "上页边距", IsRequired = false, Order = 8)]
		public int 上页边距
		{
			get {return _上页边距;}
			set {
            _上页边距 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("上页边距"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "下页边距", IsRequired = false, Order = 9)]
		public int 下页边距
		{
			get {return _下页边距;}
			set {
            _下页边距 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("下页边距"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "左页边距", IsRequired = false, Order = 10)]
		public int 左页边距
		{
			get {return _左页边距;}
			set {
            _左页边距 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("左页边距"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "右页边距", IsRequired = false, Order = 11)]
		public int 右页边距
		{
			get {return _右页边距;}
			set {
            _右页边距 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("右页边距"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否直接打印", IsRequired = false, Order = 12)]
		public bool 是否直接打印
		{
			get {return _是否直接打印;}
			set {
            _是否直接打印 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否直接打印"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否打印预览", IsRequired = false, Order = 13)]
		public bool 是否打印预览
		{
			get {return _是否打印预览;}
			set {
            _是否打印预览 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否打印预览"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "备注", IsRequired = false, Order = 14)]
		public string 备注
		{
			get {return _备注;}
			set {
            _备注 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("备注"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单位编码", IsRequired = false, Order = 15)]
		public int 单位编码
		{
			get {return _单位编码;}
			set {
            _单位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单位编码"));
            }
		}
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
		#endregion
		
	}
}
