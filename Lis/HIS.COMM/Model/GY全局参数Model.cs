﻿// litao@Copy Right 2006-2016
// 文件： GY全局参数Model.cs
// 项目名称：HIS.COMM
// 创建时间：2018-05-26
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.COMM.Model
{
	/// <summary>
	///GY全局参数数据实体
	/// </summary>
	[DataContract]
	public partial class GY全局参数Model:INotifyPropertyChanged
	{
		#region 变量定义
		///<summary>
		///
		///</summary>
		private int _id = -1;
		///<summary>
		///
		///</summary>
		private string _参数名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _参数值 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _备注 = String.Empty;
		///<summary>
		///
		///</summary>
		private int _单位编码;
		///<summary>
		///
		///</summary>
		private DateTime _updatetime = DateTime.Now;
		#endregion
		
		#region 构造函数

		///<summary>
		///
		///</summary>
		public GY全局参数Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=参数名称></param>
		///<param name=参数值></param>
		///<param name=备注></param>
		///<param name=单位编码></param>
		///<param name=updatetime></param>
		public GY全局参数Model(int id,string 参数名称,string 参数值,string 备注,int 单位编码,DateTime updatetime)
		{
			this._id = id;
			this._参数名称 = 参数名称;
			this._参数值 = 参数值;
			this._备注 = 备注;
			this._单位编码 = 单位编码;
			this._updatetime = updatetime;
		}
        
		#endregion
		
		#region 公共属性

		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "参数名称", IsRequired = false, Order = 1)]
		public string 参数名称
		{
			get {return _参数名称;}
			set {
            _参数名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("参数名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "参数值", IsRequired = false, Order = 2)]
		public string 参数值
		{
			get {return _参数值;}
			set {
            _参数值 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("参数值"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "备注", IsRequired = false, Order = 3)]
		public string 备注
		{
			get {return _备注;}
			set {
            _备注 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("备注"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单位编码", IsRequired = false, Order = 4)]
		public int 单位编码
		{
			get {return _单位编码;}
			set {
            _单位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "updateTime", IsRequired = false, Order = 5)]
		public DateTime updateTime
		{
			get {return _updatetime;}
			set {
            _updatetime = value;
            PropertyChanged(this, new PropertyChangedEventArgs("updateTime"));
            }
		}
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
		#endregion
		
	}
}
