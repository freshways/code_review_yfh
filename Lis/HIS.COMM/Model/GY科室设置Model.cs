﻿// litao@Copy Right 2006-2016
// 文件： GY科室设置Model.cs
// 项目名称：HIS.COMM
// 创建时间：2018-06-13
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.COMM.Model
{
	/// <summary>
	///GY科室设置数据实体
	/// </summary>
	[DataContract]
	public partial class GY科室设置Model:INotifyPropertyChanged
	{
	
		private int _科室编码 = 0;
		private string _科室名称 = String.Empty;
		private string _拼音代码 = String.Empty;
		private string _科室类型 = String.Empty;
		private bool _是否单设药房 = false;
		private bool _是否住院科室 = false;
		private bool _是否病区科室 = false;
		private bool _是否科室库存 = false;
		private bool _是否禁用 = false;
		private int _单位编码 = 0;
		private int _id = -1;
		private int _ythid = 0;
		private int _分院编码 = 0;
		private bool _是否固定资产管理 = false;
		private Guid _guid = Guid.Empty;
	
		///<summary>
		///
		///</summary>
		public GY科室设置Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=科室编码></param>
		///<param name=科室名称></param>
		///<param name=拼音代码></param>
		///<param name=科室类型></param>
		///<param name=是否单设药房></param>
		///<param name=是否住院科室></param>
		///<param name=是否病区科室></param>
		///<param name=是否科室库存></param>
		///<param name=是否禁用></param>
		///<param name=单位编码></param>
		///<param name=id></param>
		///<param name=ythid></param>
		///<param name=分院编码></param>
		///<param name=是否固定资产管理></param>
		///<param name=guid></param>
		public GY科室设置Model(int 科室编码,string 科室名称,string 拼音代码,string 科室类型,bool 是否单设药房,bool 是否住院科室,bool 是否病区科室,bool 是否科室库存,bool 是否禁用,int 单位编码,int id,int ythid,int 分院编码,bool 是否固定资产管理,Guid guid)
		{
			this._科室编码 = 科室编码;
			this._科室名称 = 科室名称;
			this._拼音代码 = 拼音代码;
			this._科室类型 = 科室类型;
			this._是否单设药房 = 是否单设药房;
			this._是否住院科室 = 是否住院科室;
			this._是否病区科室 = 是否病区科室;
			this._是否科室库存 = 是否科室库存;
			this._是否禁用 = 是否禁用;
			this._单位编码 = 单位编码;
			this._id = id;
			this._ythid = ythid;
			this._分院编码 = 分院编码;
			this._是否固定资产管理 = 是否固定资产管理;
			this._guid = guid;
		}
        
	
		
		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "科室编码", IsRequired = false, Order = 0)]
		public int 科室编码
		{
			get {return _科室编码;}
			set {
            _科室编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("科室编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "科室名称", IsRequired = false, Order = 1)]
		public string 科室名称
		{
			get {return _科室名称;}
			set {
            _科室名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("科室名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "拼音代码", IsRequired = false, Order = 2)]
		public string 拼音代码
		{
			get {return _拼音代码;}
			set {
            _拼音代码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("拼音代码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "科室类型", IsRequired = false, Order = 3)]
		public string 科室类型
		{
			get {return _科室类型;}
			set {
            _科室类型 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("科室类型"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否单设药房", IsRequired = false, Order = 4)]
		public bool 是否单设药房
		{
			get {return _是否单设药房;}
			set {
            _是否单设药房 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否单设药房"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否住院科室", IsRequired = false, Order = 5)]
		public bool 是否住院科室
		{
			get {return _是否住院科室;}
			set {
            _是否住院科室 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否住院科室"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否病区科室", IsRequired = false, Order = 6)]
		public bool 是否病区科室
		{
			get {return _是否病区科室;}
			set {
            _是否病区科室 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否病区科室"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否科室库存", IsRequired = false, Order = 7)]
		public bool 是否科室库存
		{
			get {return _是否科室库存;}
			set {
            _是否科室库存 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否科室库存"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否禁用", IsRequired = false, Order = 8)]
		public bool 是否禁用
		{
			get {return _是否禁用;}
			set {
            _是否禁用 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否禁用"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单位编码", IsRequired = false, Order = 9)]
		public int 单位编码
		{
			get {return _单位编码;}
			set {
            _单位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 10)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "YTHID", IsRequired = false, Order = 11)]
		public int YTHID
		{
			get {return _ythid;}
			set {
            _ythid = value;
            PropertyChanged(this, new PropertyChangedEventArgs("YTHID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "分院编码", IsRequired = false, Order = 12)]
		public int 分院编码
		{
			get {return _分院编码;}
			set {
            _分院编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("分院编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否固定资产管理", IsRequired = false, Order = 13)]
		public bool 是否固定资产管理
		{
			get {return _是否固定资产管理;}
			set {
            _是否固定资产管理 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否固定资产管理"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "GUID", IsRequired = false, Order = 14)]
		public Guid GUID
		{
			get {return _guid;}
			set {
            _guid = value;
            PropertyChanged(this, new PropertyChangedEventArgs("GUID"));
            }
		}
        
         public bool ClearContext()
		{
            _科室编码 = 0;
            _科室名称 = String.Empty;
            _拼音代码 = String.Empty;
            _科室类型 = String.Empty;
            _是否单设药房 = false;
            _是否住院科室 = false;
            _是否病区科室 = false;
            _是否科室库存 = false;
            _是否禁用 = false;
            _单位编码 = 0;
            _id = -1;
            _ythid = 0;
            _分院编码 = 0;
            _是否固定资产管理 = false;
            _guid = Guid.Empty;
    	    return true;
		}
     
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
	
		
	}
}
