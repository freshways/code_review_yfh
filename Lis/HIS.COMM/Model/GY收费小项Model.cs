﻿// litao@Copy Right 2006-2016
// 文件： GY收费小项Model.cs
// 项目名称：HIS.COMM
// 创建时间：2018-05-26
// ===================================================================
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace HIS.COMM.Model
{
	/// <summary>
	///GY收费小项数据实体
	/// </summary>
	[DataContract]
	public partial class GY收费小项Model:INotifyPropertyChanged
	{
		#region 变量定义
		///<summary>
		///
		///</summary>
		private int _id = -1;
		///<summary>
		///
		///</summary>
		private int _收费编码;
		///<summary>
		///
		///</summary>
		private string _收费名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _单位 = String.Empty;
		///<summary>
		///
		///</summary>
		private decimal _单价;
		///<summary>
		///
		///</summary>
		private int _归并编码;
		///<summary>
		///
		///</summary>
		private string _归并名称 = String.Empty;
		///<summary>
		///
		///</summary>
		private string _拼音代码 = String.Empty;
		///<summary>
		///
		///</summary>
		private bool _是否禁用;
		///<summary>
		///
		///</summary>
		private int _单位编码;
		///<summary>
		///
		///</summary>
		private string _新合编码 = String.Empty;
		///<summary>
		///
		///</summary>
		private bool _是否报销;
		///<summary>
		///
		///</summary>
		private decimal _新合门诊报销比例12;
		///<summary>
		///
		///</summary>
		private int _执行科室编码;
		///<summary>
		///
		///</summary>
		private int _输液贴打印标志;
		///<summary>
		///
		///</summary>
		private bool _pacs检查项;
		///<summary>
		///
		///</summary>
		private bool _lis检查项;
		#endregion
		
		#region 构造函数

		///<summary>
		///
		///</summary>
		public GY收费小项Model()
		{
		}
		///<summary>
		///
		///</summary>
		///<param name=id></param>
		///<param name=收费编码></param>
		///<param name=收费名称></param>
		///<param name=单位></param>
		///<param name=单价></param>
		///<param name=归并编码></param>
		///<param name=归并名称></param>
		///<param name=拼音代码></param>
		///<param name=是否禁用></param>
		///<param name=单位编码></param>
		///<param name=新合编码></param>
		///<param name=是否报销></param>
		///<param name=新合门诊报销比例12></param>
		///<param name=执行科室编码></param>
		///<param name=输液贴打印标志></param>
		///<param name=pacs检查项></param>
		///<param name=lis检查项></param>
		public GY收费小项Model(int id,int 收费编码,string 收费名称,string 单位,decimal 单价,int 归并编码,string 归并名称,string 拼音代码,bool 是否禁用,int 单位编码,string 新合编码,bool 是否报销,decimal 新合门诊报销比例12,int 执行科室编码,int 输液贴打印标志,bool pacs检查项,bool lis检查项)
		{
			this._id = id;
			this._收费编码 = 收费编码;
			this._收费名称 = 收费名称;
			this._单位 = 单位;
			this._单价 = 单价;
			this._归并编码 = 归并编码;
			this._归并名称 = 归并名称;
			this._拼音代码 = 拼音代码;
			this._是否禁用 = 是否禁用;
			this._单位编码 = 单位编码;
			this._新合编码 = 新合编码;
			this._是否报销 = 是否报销;
			this._新合门诊报销比例12 = 新合门诊报销比例12;
			this._执行科室编码 = 执行科室编码;
			this._输液贴打印标志 = 输液贴打印标志;
			this._pacs检查项 = pacs检查项;
			this._lis检查项 = lis检查项;
		}
        
		#endregion
		
		#region 公共属性

		
		///<summary>
		///
		///</summary>
		[DataMember(Name = "ID", IsRequired = false, Order = 0)]
		public int ID
		{
			get {return _id;}
			set {
            _id = value;
            PropertyChanged(this, new PropertyChangedEventArgs("ID"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "收费编码", IsRequired = false, Order = 1)]
		public int 收费编码
		{
			get {return _收费编码;}
			set {
            _收费编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("收费编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "收费名称", IsRequired = false, Order = 2)]
		public string 收费名称
		{
			get {return _收费名称;}
			set {
            _收费名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("收费名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单位", IsRequired = false, Order = 3)]
		public string 单位
		{
			get {return _单位;}
			set {
            _单位 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单位"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单价", IsRequired = false, Order = 4)]
		public decimal 单价
		{
			get {return _单价;}
			set {
            _单价 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单价"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "归并编码", IsRequired = false, Order = 5)]
		public int 归并编码
		{
			get {return _归并编码;}
			set {
            _归并编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("归并编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "归并名称", IsRequired = false, Order = 6)]
		public string 归并名称
		{
			get {return _归并名称;}
			set {
            _归并名称 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("归并名称"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "拼音代码", IsRequired = false, Order = 7)]
		public string 拼音代码
		{
			get {return _拼音代码;}
			set {
            _拼音代码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("拼音代码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否禁用", IsRequired = false, Order = 8)]
		public bool 是否禁用
		{
			get {return _是否禁用;}
			set {
            _是否禁用 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否禁用"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "单位编码", IsRequired = false, Order = 9)]
		public int 单位编码
		{
			get {return _单位编码;}
			set {
            _单位编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("单位编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "新合编码", IsRequired = false, Order = 10)]
		public string 新合编码
		{
			get {return _新合编码;}
			set {
            _新合编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("新合编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "是否报销", IsRequired = false, Order = 11)]
		public bool 是否报销
		{
			get {return _是否报销;}
			set {
            _是否报销 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("是否报销"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "新合门诊报销比例12", IsRequired = false, Order = 12)]
		public decimal 新合门诊报销比例12
		{
			get {return _新合门诊报销比例12;}
			set {
            _新合门诊报销比例12 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("新合门诊报销比例12"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "执行科室编码", IsRequired = false, Order = 13)]
		public int 执行科室编码
		{
			get {return _执行科室编码;}
			set {
            _执行科室编码 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("执行科室编码"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "输液贴打印标志", IsRequired = false, Order = 14)]
		public int 输液贴打印标志
		{
			get {return _输液贴打印标志;}
			set {
            _输液贴打印标志 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("输液贴打印标志"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "PACS检查项", IsRequired = false, Order = 15)]
		public bool PACS检查项
		{
			get {return _pacs检查项;}
			set {
            _pacs检查项 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("PACS检查项"));
            }
		}

		///<summary>
		///
		///</summary>
		[DataMember(Name = "LIS检查项", IsRequired = false, Order = 16)]
		public bool LIS检查项
		{
			get {return _lis检查项;}
			set {
            _lis检查项 = value;
            PropertyChanged(this, new PropertyChangedEventArgs("LIS检查项"));
            }
		}
	   public event PropertyChangedEventHandler PropertyChanged = delegate { };
		#endregion
		
	}
}
