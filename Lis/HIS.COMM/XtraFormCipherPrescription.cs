﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using System.Data.SqlClient;
using HIS.Model;
using HIS.Model.Pojo;
using System.Linq;

namespace HIS.COMM
{
    public partial class XtraFormCipherPrescription : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        List<PojoCipherPrescription> listCipherPrescriptionTree = new List<PojoCipherPrescription>();
        public List<GY协定处方> listPrescriptionContent = new List<GY协定处方>();
        string openType;

        public XtraFormCipherPrescription(string _s打开方式)
        {
            InitializeComponent();
            openType = _s打开方式;
        }


        private void XtraForm协定处方_Load(object sender, EventArgs e)
        {
            try
            {
                if (openType == "维护")
                {
                    layoutControlItem确认.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    layoutControlItem删除.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem保存.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                }
                else
                {
                    layoutControlItem确认.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem删除.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    layoutControlItem保存.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
                listCipherPrescriptionTree.Add(new PojoCipherPrescription() { KeyFieldName = -1, ParentFieldName = 0, 处方名称 = "本人", 处方类型 = "本人" });
                var models1 = chis.Database.SqlQuery<PojoCipherPrescription>(
                    $@"select distinct convert(int,max(id)) KeyFieldName, -1 ParentFieldName, 处方名称,处方类型 
                    from GY协定处方
                    where 处方类型 = '个人' and 创建人编码={HIS.COMM.zdInfo.ModelUserInfo.用户编码}
                    group by 处方名称, 处方类型"
                    ).ToList();
                listCipherPrescriptionTree.AddRange(models1);
                listCipherPrescriptionTree.Add(new PojoCipherPrescription() { KeyFieldName = -2, ParentFieldName = 0, 处方名称 = "本科", 处方类型 = "本科" });
                var models2 = chis.Database.SqlQuery<PojoCipherPrescription>(
                   $@"select distinct convert(int,max(id)) KeyFieldName, -2 ParentFieldName, 处方名称,处方类型 
                    from GY协定处方
                    where 处方类型 = '科室' and 创建科室编码={HIS.COMM.zdInfo.ModelUserInfo.科室编码}
                    group by 处方名称, 处方类型"
                   ).ToList();
                listCipherPrescriptionTree.AddRange(models2);
                listCipherPrescriptionTree.Add(new PojoCipherPrescription() { KeyFieldName = -3, ParentFieldName = 0, 处方名称 = "全院", 处方类型 = "全院" });
                var models3 = chis.Database.SqlQuery<PojoCipherPrescription>(
                   $@"select distinct convert(int,max(id)) KeyFieldName, -3 ParentFieldName, 处方名称,处方类型 
                    from GY协定处方
                    where 处方类型 = '全院'
                    group by 处方名称, 处方类型"
                   ).ToList();
                listCipherPrescriptionTree.AddRange(models3);
                treeListPrescription.DataSource = listCipherPrescriptionTree;
                treeListPrescription.KeyFieldName = "KeyFieldName";
                treeListPrescription.ParentFieldName = "ParentFieldName";
                treeListPrescription.Columns["处方类型"].Visible = false;
                treeListPrescription.ExpandAll();




                //listCipherPrescriptionTree = chis.Database.SqlQuery<PojoCipherPrescription>(SQL协定处方).ToList();                
                //treeListPrescription.DataSource = listCipherPrescriptionTree;
                //treeListPrescription.KeyFieldName = "KeyFieldName";
                //treeListPrescription.ParentFieldName = "ParentFieldName";
                //treeListPrescription.Columns["处方类型"].Visible = false;
                //treeListPrescription.ExpandAll();                
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        /// <summary>
        /// 选择某一节点时,该节点的子节点全部选择 取消某一节点时,该节点的子节点全部取消选择
        /// </summary>
        /// <param name="node"></param>
        /// <param name="state"></param>
        private void SetCheckedChildNodes(TreeListNode node, CheckState check)
        {
            for (int i = 0; i < node.Nodes.Count; i++)
            {
                node.Nodes[i].CheckState = check;
                SetCheckedChildNodes(node.Nodes[i], check);
            }
        }

        /// <summary>
        /// 某节点的子节点全部选择时,该节点选择 某节点的子节点未全部选择时,该节点不选择
        /// </summary>
        /// <param name="node"></param>
        /// <param name="check"></param>
        private void SetCheckedParentNodes(TreeListNode node, CheckState check)
        {
            if (node.ParentNode != null)
            {
                bool b = false;
                CheckState state;
                for (int i = 0; i < node.ParentNode.Nodes.Count; i++)
                {
                    state = (CheckState)node.ParentNode.Nodes[i].CheckState;
                    if (!check.Equals(state))
                    {
                        b = !b;
                        break;
                    }
                }
                node.ParentNode.CheckState = b ? CheckState.Indeterminate : check;
                SetCheckedParentNodes(node.ParentNode, check);
            }
        }




        private void simpleButton保存_Click(object sender, EventArgs e)
        {
            try
            {
                if (!chis.ChangeTracker.HasChanges())
                {
                    msgBalloonHelper.ShowInformation("未修改数据");
                    return;
                }
                chis.SaveChanges();
                XtraMessageBox.Show("修改保存成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ee)
            {
                msgBalloonHelper.ShowInformation(ee.Message);
            }
        }

        private void treeList协定处方_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            try
            {
                var item = treeListPrescription.GetDataRecordByNode(e.Node) as PojoCipherPrescription;
                if (item != null)
                {
                    listPrescriptionContent = chis.GY协定处方.Where(c => c.处方名称 == item.处方名称).ToList();
                    gridControlPrescription.DataSource = listPrescriptionContent;

                    gridViewPrescription.PopulateColumns();
                    gridViewPrescription.Columns["药品产地"].Visible = false;
                    gridViewPrescription.Columns["处方类型"].Visible = false;
                    gridViewPrescription.Columns["IsChanged"].Visible = false;
                    gridViewPrescription.Columns["ID"].Visible = false;
                    gridViewPrescription.Columns["处方名称"].Visible = false;
                    gridViewPrescription.Columns["拼音代码"].Visible = false;
                    gridViewPrescription.Columns["创建人编码"].Visible = false;
                    gridViewPrescription.Columns["创建人姓名"].Visible = false;
                    gridViewPrescription.Columns["创建科室编码"].Visible = false;
                    gridViewPrescription.Columns["创建科室名称"].Visible = false;
                    gridViewPrescription.Columns["CreateTime"].Visible = false;
                    gridViewPrescription.BestFitColumns();
                }
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void simpleButton删除_Click(object sender, EventArgs e)
        {
            int count = treeListPrescription.Selection.Count;
            if (count == 0) return;
            string msg = string.Format("{0}个协定处方将要被删除，是否继续?", count);
            if (XtraMessageBox.Show(msg, "删除提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                foreach (var item in treeListPrescription.Selection)
                {
                    var item1 = treeListPrescription.GetDataRecordByNode(item) as PojoCipherPrescription;
                    if (item1.处方类型 == "全院" && HIS.COMM.zdInfo.ModelUserInfo.用户名.ToUpper() != "ADMIN")
                    {
                        msgBalloonHelper.ShowInformation("请使用管理帐户登录删除全院协定处方");
                        continue;
                    }
                    var delItems = chis.GY协定处方.Where(c => c.处方名称 == item1.处方名称).ToList();
                    chis.GY协定处方.RemoveRange(delItems);
                }
                treeListPrescription.DeleteSelectedNodes();
            }

        }

        private void simpleButton放弃_Click(object sender, EventArgs e)
        {
            //if (ds协定处方.Tables["dt角色权限"].GetChanges() == null)
            //{
            //    XtraMessageBox.Show("当前角色权限未修改", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
            //ds协定处方.Tables["dt角色权限"].RejectChanges();
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            //XtraForm协定处方_Load(null, null);
        }

        private void simpleButtonAffirm_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
