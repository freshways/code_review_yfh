﻿namespace HIS.COMM
{
    partial class XtraFormExaminationGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.treeListExaminationGroup = new DevExpress.XtraTreeList.TreeList();
            this.simpleButton确定 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton取消 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExaminationGroup = new DevExpress.XtraGrid.GridControl();
            this.gridViewExaminationGroup = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col收费编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col收费名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col单价 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col选择 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col数量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.searchControlFilter = new DevExpress.XtraEditors.SearchControl();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListExaminationGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExaminationGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExaminationGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControlFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.searchControlFilter);
            this.layoutControl1.Controls.Add(this.treeListExaminationGroup);
            this.layoutControl1.Controls.Add(this.simpleButton确定);
            this.layoutControl1.Controls.Add(this.simpleButton取消);
            this.layoutControl1.Controls.Add(this.gridControlExaminationGroup);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(915, 194, 490, 417);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(699, 399);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // treeListExaminationGroup
            // 
            this.treeListExaminationGroup.Location = new System.Drawing.Point(12, 36);
            this.treeListExaminationGroup.Name = "treeListExaminationGroup";
            this.treeListExaminationGroup.OptionsBehavior.Editable = false;
            this.treeListExaminationGroup.OptionsView.ShowCheckBoxes = true;
            this.treeListExaminationGroup.Size = new System.Drawing.Size(191, 325);
            this.treeListExaminationGroup.TabIndex = 4;
            this.treeListExaminationGroup.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.treeListExaminationGroup_AfterCheckNode);
            this.treeListExaminationGroup.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListExaminationGroup_FocusedNodeChanged);
            // 
            // simpleButton确定
            // 
            this.simpleButton确定.ImageOptions.Image = global::HIS.COMM.Properties.Resources.AcceptInvitation;
            this.simpleButton确定.Location = new System.Drawing.Point(557, 365);
            this.simpleButton确定.Name = "simpleButton确定";
            this.simpleButton确定.Size = new System.Drawing.Size(59, 22);
            this.simpleButton确定.StyleController = this.layoutControl1;
            this.simpleButton确定.TabIndex = 7;
            this.simpleButton确定.Text = "确定";
            this.simpleButton确定.Click += new System.EventHandler(this.simpleButton确定_Click);
            // 
            // simpleButton取消
            // 
            this.simpleButton取消.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton取消.ImageOptions.Image = global::HIS.COMM.Properties.Resources.Undo;
            this.simpleButton取消.Location = new System.Drawing.Point(620, 365);
            this.simpleButton取消.Name = "simpleButton取消";
            this.simpleButton取消.Size = new System.Drawing.Size(67, 22);
            this.simpleButton取消.StyleController = this.layoutControl1;
            this.simpleButton取消.TabIndex = 6;
            this.simpleButton取消.Text = "取消";
            // 
            // gridControlExaminationGroup
            // 
            this.gridControlExaminationGroup.Location = new System.Drawing.Point(212, 12);
            this.gridControlExaminationGroup.MainView = this.gridViewExaminationGroup;
            this.gridControlExaminationGroup.Name = "gridControlExaminationGroup";
            this.gridControlExaminationGroup.Size = new System.Drawing.Size(475, 349);
            this.gridControlExaminationGroup.TabIndex = 4;
            this.gridControlExaminationGroup.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExaminationGroup});
            // 
            // gridViewExaminationGroup
            // 
            this.gridViewExaminationGroup.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col收费编码,
            this.col收费名称,
            this.col单价,
            this.col选择,
            this.col数量});
            this.gridViewExaminationGroup.GridControl = this.gridControlExaminationGroup;
            this.gridViewExaminationGroup.Name = "gridViewExaminationGroup";
            this.gridViewExaminationGroup.OptionsView.ShowFooter = true;
            this.gridViewExaminationGroup.OptionsView.ShowGroupPanel = false;
            // 
            // col收费编码
            // 
            this.col收费编码.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.col收费编码.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.col收费编码.AppearanceCell.Options.UseBackColor = true;
            this.col收费编码.FieldName = "收费编码";
            this.col收费编码.Name = "col收费编码";
            this.col收费编码.OptionsColumn.ReadOnly = true;
            this.col收费编码.Visible = true;
            this.col收费编码.VisibleIndex = 1;
            this.col收费编码.Width = 116;
            // 
            // col收费名称
            // 
            this.col收费名称.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.col收费名称.AppearanceCell.Options.UseBackColor = true;
            this.col收费名称.FieldName = "收费名称";
            this.col收费名称.Name = "col收费名称";
            this.col收费名称.OptionsColumn.ReadOnly = true;
            this.col收费名称.Visible = true;
            this.col收费名称.VisibleIndex = 2;
            this.col收费名称.Width = 444;
            // 
            // col单价
            // 
            this.col单价.FieldName = "单价";
            this.col单价.Name = "col单价";
            this.col单价.Visible = true;
            this.col单价.VisibleIndex = 3;
            this.col单价.Width = 151;
            // 
            // col选择
            // 
            this.col选择.FieldName = "选择";
            this.col选择.Name = "col选择";
            this.col选择.Visible = true;
            this.col选择.VisibleIndex = 0;
            this.col选择.Width = 55;
            // 
            // col数量
            // 
            this.col数量.AppearanceCell.BackColor = System.Drawing.Color.Silver;
            this.col数量.AppearanceCell.Options.UseBackColor = true;
            this.col数量.FieldName = "数量";
            this.col数量.Name = "col数量";
            this.col数量.OptionsColumn.ReadOnly = true;
            this.col数量.Visible = true;
            this.col数量.VisibleIndex = 4;
            this.col数量.Width = 62;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem2,
            this.layoutControlItem9,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.splitterItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(699, 399);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExaminationGroup;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(479, 353);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(105, 353);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(440, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.treeListExaminationGroup;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(195, 329);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton确定;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(545, 353);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(63, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(63, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(63, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton取消;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(608, 353);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(71, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(71, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(71, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(195, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 353);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 353);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(105, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // searchControlFilter
            // 
            this.searchControlFilter.Location = new System.Drawing.Point(43, 12);
            this.searchControlFilter.Name = "searchControlFilter";
            this.searchControlFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControlFilter.Size = new System.Drawing.Size(160, 20);
            this.searchControlFilter.StyleController = this.layoutControl1;
            this.searchControlFilter.TabIndex = 8;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.searchControlFilter;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem2.Text = "筛选:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(28, 14);
            // 
            // XtraFormExaminationGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 399);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraFormExaminationGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "选择";
            this.Load += new System.EventHandler(this.XtraForm套餐选择_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListExaminationGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExaminationGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExaminationGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchControlFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton确定;
        private DevExpress.XtraEditors.SimpleButton simpleButton取消;
        private DevExpress.XtraGrid.GridControl gridControlExaminationGroup;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewExaminationGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn col收费编码;
        private DevExpress.XtraGrid.Columns.GridColumn col收费名称;
        private DevExpress.XtraGrid.Columns.GridColumn col单价;
        private DevExpress.XtraGrid.Columns.GridColumn col选择;
        private DevExpress.XtraGrid.Columns.GridColumn col数量;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraTreeList.TreeList treeListExaminationGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SearchControl searchControlFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}