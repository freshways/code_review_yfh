﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HIS.COMM
{
    public partial class XtraForm药房发药起止时间 : DevExpress.XtraEditors.XtraForm
    {
        public XtraForm药房发药起止时间()
        {
            InitializeComponent();
        }

        private void XtraForm起止时间_Load(object sender, EventArgs e)
        {
            dateEdit开始时间.EditValue = DateTime.Now.ToShortDateString();
            dateEdit截止时间.EditValue = DateTime.Now.ToShortDateString() + " 23:59:59";
        }

        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void simpleButton取消_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}