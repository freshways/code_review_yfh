﻿using System;
using System.Data;

namespace HIS.COMM
{
    public class ClassPubArgument
    {

        public static int getArgumentIntValue(string argumentName)
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                            "select 参数值 from gy全局参数 where 参数名称='" + argumentName + "'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt16(dt.Rows[0][0]);
            }
        }
        public static string getArgumentValue(string argumentName, string _default)
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                            "select 参数值 from gy全局参数 where 参数名称='" + argumentName + "'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return _default;
            }
            else
            {
                return dt.Rows[0][0].ToString();
            }
        }
        public static string getArgumentValue(string argumentName)
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                            "select 参数值 from gy全局参数 where 参数名称='" + argumentName + "'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                return dt.Rows[0][0].ToString();
            }
        }

        public static string S医院ReportServer内部登录地址()
        {
            if (HIS.COMM.zdInfo.Model单位信息.sDwmc == "测试单位")
            {
                return "http://192.168.10.99:9088/fineReport/ReportServer?reportlet=";
            }
            return getArgumentValue("医院ReportServer内部登录地址", "");
        }
        public static string S医院公用网盘登录地址()
        {
            if (HIS.COMM.zdInfo.Model单位信息.sDwmc == "测试单位")
            {
                return "http://192.168.10.26:8081";
            }
            return getArgumentValue("医院公用网盘登录地址", "");
        }
        public static string S医院Web平台内部登录地址()
        {
            if (HIS.COMM.zdInfo.Model单位信息.sDwmc == "测试单位")
            {
                return "http://192.168.10.99:9088/platform";
            }
            return getArgumentValue("医院Web平台内部登录地址", "");
        }

        public static decimal dec医院欠款下限()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='欠款下限'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return 9999999999M;
            }
            else
            {
                return Convert.ToDecimal(dt.Rows[0][0]);
            }
        }

        public static bool b开启电子病历编辑独占窗口模式()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='开启电子病历编辑独占窗口模式'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return true;
                }
            }
        }
        public static bool b开启药库仓库权限设置()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='开启药库仓库权限设置'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        internal static bool b签约服务()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='开启签约服务'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static bool b开启电子病历粘贴板()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='开启电子病历粘贴板'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static bool bPACS退费验证()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='PACS退费验证'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static bool b收款窗口自动作废24小时前处方()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='收款窗口自动作废24小时前处方'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return true;
                }
            }
        }
        public static string s知识库连接()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='知识库连接'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return code.DecryptStr("BE9A08D4A358C410E5642AD6A88C66288168BD4E703BD3658C046DB0FBAE7492D3862A4EF9D59181957EE57088A53030FEE7F236EF4EBEF75FFF2455B32322A276E4A7F3A28F0E43291223A738C8C234708918DCC21027BD892C8AF9DD6D9B6DD8C608D8A9A1140C87294004F7FF3EE4B1EE897F378D61C9", code.PubKey2);
            }
            else
            {
                try
                {
                    return code.DecryptStr(Convert.ToString(Convert.ToString(dt.Rows[0][0])), code.PubKey2);
                }
                catch (Exception ex)
                {
                    return "未定义";
                }
            }
        }

        public static ClassEnum.logLevel getLogLevel()
        {
            try
            {
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                "select 参数值 from gy全局参数 where 参数名称='logLevel'").Tables[0];
                if (dt.Rows.Count == 0)
                {
                    return ClassEnum.logLevel.INFO;
                }
                else
                {
                    try
                    {
                        return (ClassEnum.logLevel)Enum.Parse(typeof(ClassEnum.logLevel), Convert.ToString(Convert.ToString(dt.Rows[0][0])));
                    }
                    catch (Exception ex)
                    {
                        return ClassEnum.logLevel.INFO;
                    }
                }
            }
            catch (Exception ex)
            {
                return ClassEnum.logLevel.INFO;
            }
        }
        public static string s身份证数据库连接()
        {
            try
            {
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                "select 参数值 from gy全局参数 where 参数名称='身份证数据库连接'").Tables[0];
                if (dt.Rows.Count == 0)
                {
                    return "";
                }
                else
                {
                    try
                    {
                        return code.DecryptStr(Convert.ToString(Convert.ToString(dt.Rows[0][0])), code.PubKey2);
                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string s一体化数据库连接()
        {
            try
            {
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                "select 参数值 from gy全局参数 where 参数名称='一体化数据库连接'").Tables[0];
                if (dt.Rows.Count == 0)
                {
                    return "";
                }
                else
                {
                    try
                    {
                        return code.DecryptStr(Convert.ToString(Convert.ToString(dt.Rows[0][0])), code.PubKey2);
                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static string s医保本地库连接()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                 "select 参数值 from gy全局参数 where 参数名称='医保本地库连接'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "";
            }
            else
            {
                try
                {
                    string _s = Convert.ToString(dt.Rows[0][0]);
                    if (_s == "")
                    {
                        return "";
                    }
                    else
                    {
                        return _s;
                    }
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }

        public static bool b读卡调试模式()
        {
            return HIS.COMM.Helper.EnvironmentHelper.CommBoolGet("读卡调试模式", "false", "条码读卡是否支持条码框界面双击从RichText获取值和Websocket显示调试信息");
        }

        public static bool b软件调试模式()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='软件调试模式'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static bool b电子病历只允许取消当天审核()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb,
                CommandType.Text, "select 参数值 from gy全局参数 where 参数名称='电子病历只允许取消当天审核'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return true;
                }
            }
        }
        public static bool b电子病历限制出院病人完成时限()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                        "select 参数值 from gy全局参数 where 参数名称='电子病历限制出院病人完成时限'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        internal static bool b医嘱打印手签图片()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='医嘱打印手签图片'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static bool b住院发票打印是否预览()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='住院发票打印是否预览'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static bool b预交款凭条打印预览()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='预交款凭条打印预览'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return true;
                }
            }
        }
        public static bool b门诊电子处方可否选择医生()
        {
            return HIS.COMM.Helper.EnvironmentHelper.CommBoolGet("门诊电子处方可否选择医生", "false", "");
        }

        public static string sPACS执行科室编码()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='PACS执行科室编码' and  备注='" + HIS.COMM.zdInfo.Model站点信息.分院编码 + "'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "未定义";
            }
            else
            {
                try
                {
                    return Convert.ToString(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return "未定义";
                }
            }
        }
        public static string sLIS执行科室编码()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='LIS执行科室编码' and  备注='" + HIS.COMM.zdInfo.Model站点信息.分院编码 + "'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "未定义";
            }
            else
            {
                try
                {
                    return Convert.ToString(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return "未定义";
                }
            }
        }
        public static string s医保单位编码()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='医保单位编码'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "未定义";
            }
            else
            {
                try
                {
                    return Convert.ToString(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return "未定义";
                }
            }
        }


        public static string s默认条码打印机名称()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='默认条码打印机名称'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "未定义";
            }
            else
            {
                try
                {
                    return Convert.ToString(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return "未定义";
                }
            }
        }

        public static string s默认腕带打印机名称()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='默认腕带打印机名称'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "未定义";
            }
            else
            {
                try
                {
                    return Convert.ToString(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return "未定义";
                }
            }
        }
        public static string s插件仓库地址()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='插件仓库地址'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "";
            }
            else
            {
                try
                {
                    string _s = Convert.ToString(dt.Rows[0][0]);
                    if (_s == "")
                    {
                        return "";
                    }
                    else
                    {
                        return code.DecryptStr(_s, code.PubKey2);
                    }
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        //public static string sLIS数据库连接()
        //{
        //    DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
        //                        "select 参数值 from gy全局参数 where 参数名称='LIS数据库连接'").Tables[0];
        //    if (dt.Rows.Count == 0)
        //    {
        //        return "";
        //    }
        //    else
        //    {
        //        try
        //        {
        //            string _s = Convert.ToString(dt.Rows[0][0]);
        //            if (_s == "")
        //            {
        //                return "";
        //            }
        //            else
        //            {
        //                return code.DecryptStr(_s, code.PubKey2);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return "";
        //        }

        //    }
        //}

        public static bool b分级诊疗开关()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='分级诊疗开关'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static bool b住院病人退费Ukey验证()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='住院病人退费Ukey验证'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static ClassEnum.enPACS记费模式 enPACS记费模式()
        {
            return (ClassEnum.enPACS记费模式)Enum.Parse(typeof(ClassEnum.enPACS记费模式), Helper.EnvironmentHelper.CommStringGet("PACS记费模式", "护理执行医嘱记费", ""));
        }

        public static bool bPACS医嘱验证开关()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                                "select 参数值 from gy全局参数 where 参数名称='PACS医嘱验证开关'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
        }
        public static bool b检验科确认费用()
        {
            return Helper.EnvironmentHelper.CommBoolGet("检验科确认费用", "false", "自动生成JY申请摘要等相关信息开关");
        }
        public static bool b写检验申请单()
        {
            return Helper.EnvironmentHelper.CommBoolGet("写检验申请单", "true", "自动生成JY申请摘要等相关信息开关");
        }
        public static bool b临时医嘱是否打印护理执行时间()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                             "select 参数值 from gy全局参数 where 参数名称='临时医嘱是否打印护理执行时间'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return true;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return true;
                }

            }
        }
        public static bool b曼荼罗临床路径接口()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                          "select 参数值 from gy全局参数 where 参数名称='曼荼罗临床路径接口'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
        }
        public static bool bFK_M()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                          "select 参数值 from gy全局参数 where 参数名称='FK_M'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                try
                {
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
        }
        /// <summary>
        /// 无;每站点Ukey;服务器Ukey
        /// </summary>
        /// <returns></returns>
        public static string get站点认证方式()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                          "select 参数值 from gy全局参数 where 参数名称='服务器ID2'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "无";
            }
            else
            {
                return code.DecryptStr(code.DecryptStr(dt.Rows[0][0].ToString(), code.PubKey2), code.PubKey2);
            }
        }
        public static string get电子处方默认医保类型()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                       "select 参数值 from gy全局参数 where 参数名称='电子处方默认医保类型'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "城乡居民";
            }
            else
            {
                return dt.Rows[0][0].ToString();
            }
        }
        public static string get门诊收款默认医保类型()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                       "select 参数值 from gy全局参数 where 参数名称='门诊收款默认医保类型'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "城乡居民";
            }
            else
            {
                return dt.Rows[0][0].ToString();
            }
        }

        public static bool b加密狗控制门诊作废()
        {
            bool bResult = true;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("加密狗控制门诊作废");
                if (sReturn != null)
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }
        public static bool b门诊收款集成医保报销()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("门诊收款集成医保报销");
                if (sReturn != null)
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }
        public static bool b一般诊疗费自动记费()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("一般诊疗费自动记费");
                if (sReturn != null)
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }

        public static bool b一般诊疗费当日不允许重复收取()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("一般诊疗费当日不允许重复收取");
                if (sReturn != null)
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }

        public static bool b是否同步上传住院登记()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("是否同步上传住院登记");
                if (sReturn != null)
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }

        public static bool b药典维护参照基药目录()
        {
            bool bResult = true;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("药典维护参照基药目录");
                if (sReturn != null)
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }
        public static bool b医嘱录入界面大夫是否可选()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("医嘱录入界面大夫是否可选");
                if (sReturn == null)
                {
                    bResult = false;
                }
                else
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }

        public static bool b入院登记是否必须录入家庭地址()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("入院登记是否必须录入家庭地址");
                if (sReturn == null)
                {
                    bResult = false;
                }
                else
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }

        public static bool b是否限定新合核查3次出院()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("是否限定新合核查3次出院");
                if (sReturn == null)
                {
                    bResult = false;
                }
                else
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }

        public static bool b床位管理是否显示空床()
        {
            bool bResult = true;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("床位管理是否显示空床");
                if (sReturn == null)
                {
                    bResult = false;
                }
                else
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }

        public static string get授权日期()
        {
            DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text,
                       "select 参数值 from gy全局参数 where 参数名称='服务器ID'").Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "2014-11-01";
            }
            else
            {
                return code.DecryptStr(code.DecryptStr(dt.Rows[0][0].ToString(), code.PubKey2), code.PubKey2);
            }
        }
        public static bool b启用医卡通()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("启用医卡通");
                if (sReturn == null)
                {
                    bResult = false;
                }
                else
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
                bResult = false;
            }
            return bResult;
        }
        //public static bool b启用医卡通门诊余额控制()
        //{
        //    bool bResult = false;
        //    try
        //    {
        //        string sReturn = ClassPubArgument.getArgumentValue("启用医卡通门诊余额控制");
        //        if (sReturn == null)
        //        {
        //            bResult = false;
        //        }
        //        else
        //        {
        //            bResult = Convert.ToBoolean(sReturn);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        bResult = false;
        //    }
        //    return bResult;
        //}

        public static bool get启用LIS接口()
        {
            bool b是否启用LIS接口 = false;
            try
            {
                string s是否启用LIS接口 = ClassPubArgument.getArgumentValue("是否启用LIS接口");
                if (s是否启用LIS接口 == null)
                {
                    b是否启用LIS接口 = false;
                }
                else
                {
                    b是否启用LIS接口 = Convert.ToBoolean(s是否启用LIS接口);
                }
            }
            catch (Exception ex)
            {
                b是否启用LIS接口 = false;
            }
            return b是否启用LIS接口;
        }


        public static bool b启用医嘱打印()
        {
            bool b医嘱打印 = false;
            try
            {
                string s医嘱打印 = ClassPubArgument.getArgumentValue("是否启用医嘱打印");
                if (s医嘱打印 == null)
                {
                    b医嘱打印 = false;
                }
                else
                {
                    b医嘱打印 = Convert.ToBoolean(s医嘱打印);
                }
            }
            catch (Exception ex)
            {
                b医嘱打印 = false;
            }
            return b医嘱打印;
        }



        public static bool b新农合门诊直报()
        {
            bool b是否启用新农合直报 = false;
            try
            {
                string s是否新农合直报 = ClassPubArgument.getArgumentValue("是否新合门诊直报");
                if (s是否新农合直报 == null)
                {
                    b是否启用新农合直报 = false;
                }
                else
                {
                    b是否启用新农合直报 = Convert.ToBoolean(s是否新农合直报);
                }
            }
            catch (Exception ex)
            {
                b是否启用新农合直报 = false;
            }
            return b是否启用新农合直报;
        }

        internal static bool b开启传染病上报()
        {
            bool bResult = false;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue("开启传染病上报");
                if (sReturn == null)
                {
                    bResult = false;
                }
                else
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
                bResult = false;
            }
            return bResult;
        }

        internal static bool b开启核三门诊收款直报()
        {
            return get参数设置("开启核三门诊收款直报", false);
        }

        private static bool get参数设置(string _s参数名称, bool _bDefault)
        {
            bool bResult = _bDefault;
            try
            {
                string sReturn = ClassPubArgument.getArgumentValue(_s参数名称);
                if (string.IsNullOrEmpty(sReturn))
                {
                    bResult = false;
                }
                else
                {
                    bResult = Convert.ToBoolean(sReturn);
                }
            }
            catch (Exception ex)
            {
                bResult = false;
            }
            return bResult;
        }

        internal static bool b开启支付宝支付()
        {
            return get参数设置("开启支付宝支付", false);
        }

        internal static bool b开启微信支付()
        {
            return get参数设置("开启微信支付", false);
        }
    }
}
