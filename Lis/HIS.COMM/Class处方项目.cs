﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HIS.COMM
{
    public class Class处方项目
    {
        public static DataTable dt医嘱频次()
        {
            string SQL医嘱频次 =
                        "SELECT [ID]" + "\r\n" +
                        "      ,[频次编码]" + "\r\n" +
                        "      ,[频次名称]" + "\r\n" +
                        "      ,[拼音代码]" + "\r\n" +
                        "      ,[频次次数]" + "\r\n" +
                        "FROM [dbo].[YS医嘱频次]";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL医嘱频次).Tables[0];
        }
        public static DataTable dt医嘱用法()
        {

            string SQL医嘱用法 =
                "SELECT  [ID]" + "\r\n" +
                "      ,[用法名称]" + "\r\n" +
                "      ,[拼音代码]" + "\r\n" +
                "  FROM [dbo].[YS医嘱用法]";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL医嘱用法).Tables[0];
        }
        public static DataTable dt药品剂量()
        {
            string SQL药品剂量 =
                "SELECT ID," + "\r\n" +
                "       剂量名称," + "\r\n" +
                "       拼音代码," + "\r\n" +
                "       createTime" + "\r\n" +
                "  FROM YK药品剂量";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL药品剂量).Tables[0];
        }
        public static DataTable dt医嘱类型()
        {
            string SQL医嘱类型 =
                     "SELECT [ID]" + "\r\n" +
                     "      ,[类型名称]" + "\r\n" +
                     "      ,[拼音代码]" + "\r\n" +
                     "  FROM [dbo].[YS医嘱类型]";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL医嘱类型).Tables[0];
        }


    }
}

