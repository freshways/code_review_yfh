﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.BLL
{

    public class IsUsedChargeItem
    {
        /// <summary>
        /// 某个收费项目是否没有发生业务可以修改删除
        /// </summary>
        /// <param name="fyxh"></param>
        /// <returns></returns>
        public static Boolean action(int fyxh)
        {
            int fyxhUseCount = Convert.ToInt32(HIS.Model.Dal.SqlHelper.ExecuteScalar(
                     "select SUM(费用次数) from " +
                      "  ( " +
                      "  select count(*) 费用次数 from mf门诊明细 where 收费编码=" + fyxh +
                      "  union all " +
                      "  select COUNT(*) from ZY出院费用 where YPID=0 and 费用编码=" + fyxh +
                      "  union all " +
                      "  select COUNT(*) from ZY在院费用 where YPID=0 and 费用编码=" + fyxh +
                      "  ) aa"));
            if (fyxhUseCount > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
