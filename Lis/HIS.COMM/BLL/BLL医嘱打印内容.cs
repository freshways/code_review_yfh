﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.BLL
{
    public class BLL医嘱打印内容
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
        /// <summary>
        /// 返回要打印的数据，控制打印格式，返回续打起始页
        /// </summary>
        /// <param name="zyid"></param>
        /// <param name="b是否续打"></param>
        /// <param name="_dateTime界面续打时间"></param>
        /// <returns></returns>
        public static List<Pojo医嘱打印> get医嘱打印内容(decimal zyid, bool b是否续打, DateTime _dateTime界面续打时间, out int out表格_表头尾_显示起始页, string _s医嘱类型)
        {
            var inpatient = chis.ZY病人信息.Where(c => c.ZYID == zyid).FirstOrDefault();
            var list全部医嘱 = new List<Pojo医嘱打印>();

            #region 获取医嘱内容
            if (Convert.ToBoolean(inpatient.已出院标记))
            {
                list全部医嘱 = (from a in chis.YS住院医嘱历史
                            where a.ZYID == zyid && a.医嘱类型 == _s医嘱类型
                            select new Pojo医嘱打印()
                            {
                                ID = a.ID,
                                ZYID = a.ZYID,
                                医嘱类型 = a.医嘱类型,
                                组别 = a.组别,
                                项目类型 = a.项目类型,
                                项目编码 = a.项目编码,
                                项目名称 = a.项目名称,
                                剂量数量 = a.剂量数量,
                                剂量单位 = a.剂量单位,
                                医嘱数量 = a.医嘱数量,
                                频次名称 = a.频次名称,
                                用法名称 = a.用法名称,
                                开嘱医生编码 = a.开嘱医生编码,
                                开嘱时间 = a.开嘱时间,
                                开嘱执行者编码 = a.开嘱执行者编码,
                                开嘱核对者编码 = a.开嘱核对者编码,
                                执行时间 = a.执行时间,
                                停嘱医生编码 = a.停嘱医生编码,
                                停嘱执行者编码 = a.停嘱执行者编码,
                                停嘱核对者编码 = a.停嘱核对者编码,
                                停嘱时间 = a.停嘱时间,
                                单日总量 = a.单日总量,
                                频次数量 = a.频次数量,
                                createTime = a.createTime,
                                临床路径ID = a.临床路径ID,
                                组别符号 = a.组别符号,
                                库存数量 = a.库存数量,
                                药房规格 = a.药房规格,
                                药房单位 = a.药房单位,
                                药房编码 = a.药房编码,
                                更新标志 = a.更新标志,
                                单次数量 = a.单次数量,
                                财务分类 = a.财务分类,
                                页号 = a.页号,
                                行号 = a.行号,
                                已打印标志 = a.已打印标志,
                                子嘱ID = a.子嘱ID,
                                医嘱内涵 = a.医嘱内涵,
                                医嘱备注 = a.医嘱备注,
                                停嘱原因 = a.停嘱原因,
                                作废原因 = a.作废原因,
                                //医嘱ID = a.医嘱ID,
                                医嘱套餐编码 = a.医嘱套餐编码,
                                用药状态 = a.用药状态,
                                医嘱验证标记 = a.医嘱验证标记,
                                pacs申请单号 = a.pacs申请单号,
                                不打印 = a.不打印
                            }
                         ).ToList();
            }
            else
            {
                list全部医嘱 = (from a in chis.YS住院医嘱
                            where a.ZYID == zyid && a.医嘱类型 == _s医嘱类型
                            select new Pojo医嘱打印()
                            {
                                ID = a.ID,
                                ZYID = a.ZYID,
                                医嘱类型 = a.医嘱类型,
                                组别 = a.组别,
                                项目类型 = a.项目类型,
                                项目编码 = a.项目编码,
                                项目名称 = a.项目名称,
                                剂量数量 = a.剂量数量,
                                剂量单位 = a.剂量单位,
                                医嘱数量 = a.医嘱数量,
                                频次名称 = a.频次名称,
                                用法名称 = a.用法名称,
                                开嘱医生编码 = a.开嘱医生编码,
                                开嘱时间 = a.开嘱时间,
                                开嘱执行者编码 = a.开嘱执行者编码,
                                开嘱核对者编码 = a.开嘱核对者编码,
                                执行时间 = a.执行时间,
                                停嘱医生编码 = a.停嘱医生编码,
                                停嘱执行者编码 = a.停嘱执行者编码,
                                停嘱核对者编码 = a.停嘱核对者编码,
                                停嘱时间 = a.停嘱时间,
                                单日总量 = a.单日总量,
                                频次数量 = a.频次数量,
                                createTime = a.createTime,
                                临床路径ID = a.临床路径ID,
                                组别符号 = a.组别符号,
                                库存数量 = a.库存数量,
                                药房规格 = a.药房规格,
                                药房单位 = a.药房单位,
                                药房编码 = a.药房编码,
                                更新标志 = a.更新标志,
                                单次数量 = a.单次数量,
                                财务分类 = a.财务分类,
                                页号 = a.页号,
                                行号 = a.行号,
                                已打印标志 = a.已打印标志,
                                子嘱ID = a.子嘱ID,
                                医嘱内涵 = a.医嘱内涵,
                                医嘱备注 = a.医嘱备注,
                                停嘱原因 = a.停嘱原因,
                                作废原因 = a.作废原因,
                                医嘱ID = a.医嘱ID,
                                医嘱套餐编码 = a.医嘱套餐编码,
                                用药状态 = a.用药状态,
                                医嘱验证标记 = a.医嘱验证标记,
                                pacs申请单号 = a.pacs申请单号,
                                不打印 = a.不打印
                            }
                          ).ToList();
            }
            list全部医嘱.RemoveAll(c => c.不打印 == true);
            #endregion

            #region 控制打印输出的时间和手签           
            //打印时间参数
            var printTimeParam = HIS.COMM.Helper.EnvironmentHelper.MedicalOrderTimePrintParam();
            //打印手签参数
            var printSignParam = HIS.COMM.Helper.EnvironmentHelper.MedicalOrderSignaturePrintParam();
            foreach (var item in list全部医嘱)
            {

                if (printSignParam.Contains("开嘱医生"))
                {
                    if (item.开嘱医生编码 != null)
                    {
                        item.打印医生手签 = CacheData.Pubuser.Where(c => c.用户编码 == item.开嘱医生编码).FirstOrDefault().手签照片;
                    }
                }
                if (printSignParam.Contains("开嘱执行"))
                {
                    if (item.开嘱执行者编码 != null)
                    {
                        item.打印护士手签 = CacheData.Pubuser.Where(c => c.用户编码 == item.开嘱执行者编码).FirstOrDefault().手签照片;
                    }
                }

                if (printSignParam.Contains("停嘱医生"))
                {
                    if (item.停嘱医生编码 != null)
                    {
                        item.打印停嘱医师签名 = CacheData.Pubuser.Where(c => c.用户编码 == item.停嘱医生编码).FirstOrDefault().手签照片;
                    }
                }
                if (printSignParam.Contains("停嘱执行"))
                {
                    if (item.停嘱执行者编码 != null)
                    {
                        item.打印停嘱护士签名 = CacheData.Pubuser.Where(c => c.用户编码 == item.停嘱执行者编码).FirstOrDefault().手签照片;
                    }
                }

                if (!printTimeParam.Contains("执行时间"))
                {
                    item.执行时间 = null;
                }
            }
            #endregion


            //2、通过判断上一行数据，控制本行是否显示为 ..
            var itemPre = new Pojo医嘱打印();
            int i医嘱行号 = 0;
            int i续打时间点后第一条医嘱 = 0;
            foreach (var item in list全部医嘱)
            {

                if (i医嘱行号 > 0)
                {
                    itemPre = list全部医嘱[i医嘱行号 - 1]; //获取前一行，用于比较上一行，控制是否显示为 ..
                    if (item.ID == 0)
                    {
                        break;
                    }
                    //长期、临时共有的
                    if (item.开嘱时间 == itemPre.开嘱时间 && !item.医嘱内涵.Contains("出院"))
                    {
                        item.打印开嘱日期 = "..";
                        item.打印开嘱时间 = "..";
                        item.打印开嘱护士点点 = "..";
                        item.打印开嘱医生点点 = "..";
                    }
                    else
                    {   //留空的情况1：当医嘱开嘱时间与上一行时间不同时，留出签字位置
                        item.打印开嘱日期 = Convert.ToDateTime(item.开嘱时间).ToString("yyyy-MM-dd");
                        item.打印开嘱时间 = Convert.ToDateTime(item.开嘱时间).ToString("HH:mm");
                        itemPre.打印开嘱医生点点 = "";
                        itemPre.打印开嘱护士点点 = "";
                    }

                    if (item.停嘱时间 != null)
                    {
                        item.打印停嘱日期 = Convert.ToDateTime(item.停嘱时间).ToString("yyyy-MM-dd").Substring(5, 5);
                        item.打印停嘱时间 = Convert.ToDateTime(item.停嘱时间).ToString("HH:mm");
                    }


                    if (item.执行时间 == itemPre.执行时间 && item.执行时间 != null)
                    {
                        item.打印执行时间 = "..";
                    }
                    else
                    {
                        if (item.执行时间 != null)
                        {
                            item.打印执行时间 = Convert.ToDateTime(item.执行时间).ToString("HH:mm");
                        }
                    }
                }
                else
                {//首行数据赋值中的时间处理
                    item.打印开嘱日期 = Convert.ToDateTime(item.开嘱时间).ToString("yyyy-MM-dd");
                    item.打印开嘱时间 = Convert.ToDateTime(item.开嘱时间).ToString("HH:mm");
                    if (item.执行时间 != null)
                    {
                        item.打印执行时间 = Convert.ToDateTime(item.执行时间).ToString("HH:mm");
                    }
                    if (item.停嘱时间 != null)
                    {
                        item.打印停嘱日期 = Convert.ToDateTime(item.停嘱时间).ToString("yyyy-MM-dd").Substring(5, 5);
                        item.打印停嘱时间 = Convert.ToDateTime(item.停嘱时间).ToString("HH:mm");
                    }
                }

                item.打印医嘱内涵 = item.医嘱内涵;

                item.打印剂量数量 = Helper.MedicalAdviceFormatHelper.numberMasker(item.剂量数量.ToString());
                item.打印剂量单位 = item.剂量单位;
                item.打印组别符号 = item.组别符号;
                item.打印用法 = item.用法名称;
                item.打印频次 = item.频次名称;
                if (_s医嘱类型 == "临时医嘱")
                {                    
                    if (HIS.COMM.Helper.EnvironmentHelper.CommBoolGet("临时医嘱非药品不打印频次", "false", "") && item.项目类型 != HIS.COMM.ClassEnum.en医嘱项目类型.药品项目.ToString())
                    {
                        item.打印频次 = "";
                    }
                    if (HIS.COMM.Helper.EnvironmentHelper.CommBoolGet("临时医嘱药品不打印频次", "false", "") && item.项目类型 == HIS.COMM.ClassEnum.en医嘱项目类型.药品项目.ToString())
                    {
                        item.打印频次 = "";
                    }                  
                }
                #region 续打控制                
                //1.1 根据开嘱时间和续打时间点比较，控制是否空白内容
                if (b是否续打)
                {
                    if (item.开嘱时间 >= _dateTime界面续打时间)
                    {
                        if (i续打时间点后第一条医嘱 == 0)
                        {
                            i续打时间点后第一条医嘱 = i医嘱行号; //获取开始出现续打内容的点，所在的页之后的新页，需要输出表头尾和表格
                        }
                    }
                    else
                    {
                        //早于续打时间点的内容
                        item.打印护士手签 = null;
                        item.打印医生手签 = null;
                        item.打印开嘱医生点点 = "";
                        item.打印开嘱护士点点 = "";
                        item.打印开嘱日期 = "";
                        item.打印开嘱时间 = "";
                        item.打印医嘱内涵 = "";
                        item.打印剂量数量 = "";
                        item.打印剂量单位 = "";
                        item.打印组别符号 = "";
                        item.打印用法 = "";
                        item.打印频次 = "";
                    }

                    //续打的时候可能出现在新行之前位置的情况1.2,1.3
                    //1.2临时医嘱独有，根据执行时间是否打印执行时间控制
                    if (item.执行时间 < _dateTime界面续打时间)
                    {
                        item.打印执行时间 = "";
                    }

                    //1.3长期医嘱独有，根据执行时间是否打印停嘱控制
                    if (item.停嘱时间 < _dateTime界面续打时间)
                    {
                        item.打印停嘱日期 = "";
                        item.打印停嘱时间 = "";
                    }
                }
                #endregion
                i医嘱行号++;
            }

            //3、通过判断医嘱组别结束符号，处理频次、用法，停嘱时间
            int ii = 0;
            foreach (var item in list全部医嘱)
            {
                ii++;
                if (item.组别符号 == "┘" || item.组别符号 == "]" || item.组别符号 == "")
                {

                }
                else
                {
                    item.打印频次 = "";
                    item.打印用法 = "";
                    item.打印停嘱日期 = "";
                    item.打印停嘱时间 = "";
                }
            }
            //todo:4、通过判断是否页首、页尾和末尾行，留出 点点位置为空， 用于手签

            int iii = 0;
            foreach (var item in list全部医嘱)
            {
                iii++;
                if (iii == list全部医嘱.Count) //手签留空的情况2：有内容的末尾行不点点
                {
                    item.打印开嘱护士点点 = "";
                    item.打印开嘱医生点点 = "";
                }
                if (iii % 25 == 0) //手签留空的情况3：页首不点点
                {
                    item.打印开嘱护士点点 = "";
                    item.打印开嘱医生点点 = "";
                }
                if (iii % 25 == 1) //手签留空的情况4：页首不点点
                {
                    item.打印开嘱护士点点 = "";
                    item.打印开嘱医生点点 = "";
                    //首行显示日期时间
                    if (!(b是否续打 && item.开嘱时间 < _dateTime界面续打时间))
                    {
                        item.打印开嘱日期 = Convert.ToDateTime(item.开嘱时间).ToString("yyyy-MM-dd");
                        item.打印开嘱时间 = Convert.ToDateTime(item.开嘱时间).ToString("HH:mm");
                    }

                }
            }

            //999、最后一页补齐空白行，使之全部==25行
            int row_count = list全部医嘱.Count;
            int need_add_rows_count = row_count % 25;
            if (need_add_rows_count > 0) need_add_rows_count = 25 - need_add_rows_count;
            for (int i = 0; i < need_add_rows_count; i++)
            {
                list全部医嘱.Add(new Pojo医嘱打印());
            }



            var i晚于续打时间点医嘱 = list全部医嘱.Count(c => c.开嘱时间 >= _dateTime界面续打时间);
            if (i晚于续打时间点医嘱 == 0)
            {
                out表格_表头尾_显示起始页 = 9999;//只有大于起始页才显示表格
            }
            else
            {
                out表格_表头尾_显示起始页 = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(i续打时间点后第一条医嘱 / 25))) + 1;
            }
            return list全部医嘱;
        }


        #region datatable分页
        /// <summary>
        /// 返回分页的页数
        /// </summary>
        /// <param name="rows_全部医嘱">总条数</param>
        /// <param name="i每页行书">每页显示多少条</param>
        /// <returns>如果 结尾为0：则返回1</returns>
        private static int all_rows_PageSize_Count(int rows_全部医嘱, int i每页行书)
        {
            int i总页数 = 0;
            if (rows_全部医嘱 % i每页行书 == 0) { i总页数 = rows_全部医嘱 / i每页行书; }
            else { i总页数 = (rows_全部医嘱 / i每页行书) + 1; }
            if (i总页数 == 0) { i总页数 += 1; }
            return i总页数;
        }

        private static int count_page_number(int _i续打起始行号)
        {
            //if (_i续打起始行号 % 25 > 0) { _b当前页是否无边框 = true; }//在续打状态下，最后一页，总是要打印边框
            return (_i续打起始行号 / 25) + 1; //计算当前应该打印的页码
        }

        /// <summary>
        /// DataTable分页
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="i页号">页索引,注意：从1开始</param>
        /// <param name="i每页行数">每页大小</param>
        /// <returns>分好页的DataTable数据</returns>              第1页        每页10条
        public List<Pojo医嘱打印> dt获取续打数据(List<Pojo医嘱打印> dt, int i页号, int i每页行数, bool bIsAll = false)
        {
            if (i页号 == 0) { return dt; }
            List<Pojo医嘱打印> newdt = dt.ToList();
            newdt.Clear();
            int i当页起始行 = (i页号 - 1) * i每页行数;
            int i当页终止行 = i当页起始行 + 25;
            int i页终止行 = i页号 * i每页行数;
            if (bIsAll)
                i页终止行 = dt.Count;

            if (i当页起始行 >= dt.Count)
            { return newdt; }

            if (i页终止行 > dt.Count)
            { i页终止行 = dt.Count; }
            for (int i当前行 = i当页起始行; i当前行 <= i页终止行 - 1; i当前行++)
            {
                //YS住院医嘱 newdr = newdt.NewRow();
                //YS住院医嘱 dr = dt.Rows[i当前行];
                //foreach (DataColumn column in dt.Columns)
                //{
                //    newdr[column.ColumnName] = dr[column.ColumnName];
                //}
                //string _str真正开嘱时间 = newdr["真正开嘱时间"].ToString();
                //if (string.IsNullOrEmpty(_str真正开嘱时间))
                //{
                //    newdr["开小于续"] = "否";
                //}
                //else
                //{
                //    if (Convert.ToDateTime(_str真正开嘱时间) < dateTime续打时间)
                //    {
                //        newdr["开小于续"] = "是";//白色不打印
                //    }
                //    else
                //    { newdr["开小于续"] = "否"; }
                //}

                //if (i当前行 < i当页终止行)
                //    newdr["已打印标志"] = 1;
                //string _str真正执行时间 = newdr["真正执行时间"].ToString();
                //if (string.IsNullOrEmpty(_str真正执行时间))
                //{
                //    newdr["执小于续"] = "否";//打印空
                //}
                //else
                //{
                //    if (Convert.ToDateTime(_str真正执行时间) < dateTime续打时间)
                //    {
                //        newdr["执小于续"] = "是";//不打印
                //    }
                //    else
                //    { newdr["执小于续"] = "否"; }//打印执行时间
                //}

                //newdt.Rows.Add(newdr);
            }
            return newdt;
        }

        #endregion
    }
}
