﻿using HIS.Model;
using HIS.Model.Pojo;
using System.Linq;

namespace HIS.COMM.BLL.DoctorWorkstation
{
    /// <summary>
    /// 同步医嘱数据
    /// </summary>
    public class SyncMedicalOrderID
    {
        /// <summary>
        /// 更新子医嘱医嘱ID
        /// </summary>
        /// <param name="pjInpatient"></param>
        public static void action(Pojo病人信息 pjInpatient)
        {

            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
            {

                var list子医嘱 = chis.YS住院子医嘱.Where(c => c.ZYID == pjInpatient.ZYID && (c.医嘱ID == 0 || c.医嘱ID == null)).ToList();
                foreach (var item子医嘱 in list子医嘱)
                {
                    var item父医嘱 = chis.YS住院医嘱.Where(c => c.ZYID == pjInpatient.ZYID && c.子嘱ID == item子医嘱.子嘱ID).FirstOrDefault();
                    if (item父医嘱 != null)
                    {
                        item子医嘱.医嘱ID = item父医嘱.ID;
                    }
                }
                chis.SaveChanges();
                //msgHelper.BalloonShow("更新医嘱ID成功");
            }

        }
    }
}
