﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.BLL.DoctorWorkstation
{
    public class MedicalAdviceHelper
    {
      static  CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public static bool NeedConvertDosage(Pojo医嘱提示 pojo)
        {

            if (string.IsNullOrEmpty(pojo.默认用法))
            {
                msgBalloonHelper.BalloonShow("药品默认用法未设置，请联系药学人员维护");
                return false;
            }
            if (string.IsNullOrEmpty(pojo.剂量单位))
            {
                msgBalloonHelper.BalloonShow("药品剂量单位未设置，请联系药学人员维护");
                return false;
            }
            if (pojo.默认医嘱剂量 == null)
            {
                msgBalloonHelper.BalloonShow("默认医嘱剂量未设置，请联系药学人员维护");
                return false;
            }
            if (string.IsNullOrEmpty(pojo.默认频次))
            {
                msgBalloonHelper.BalloonShow("默认频次未设置，请联系药学人员维护");
                return false;
            }
            var convertDosage = chis.YS医嘱用法.Where(c => c.用法名称 == pojo.默认用法).FirstOrDefault();
            return Convert.ToBoolean(convertDosage.convertDosage);
        }

        public static List<Pojo医嘱提示> GetMedicalAdviceHints(string _s药房编码)
        {
            string sql = string.Format(@"
                                        select   aa.药品序号 医嘱项目编码, aa.药品名称 医嘱名称, aa.药房规格 包装规格, aa.药房单位 包装单位, aa.医院零售价 单价,
			                                         sum(aa.库存数量) 库存数量, '药品项目' 医嘱项目类型, bb.[默认频次], bb.默认用法, bb.[药品剂量] 默认医嘱剂量,
			                                         bb.剂量单位, bb.财务分类, bb.拼音代码,bl.职工住院自付比例,bl.职工门诊自付比例,bl.居民住院自付比例,bl.居民门诊自付比例,bl.离休自付比例,
			 	                                         null 一级医院限价,null 二级医院限价,null 三级医院限价,null 最高限价
			                                         ,bl.备注 使用限制和备注,aa.药房编码
                                        from     YF库存信息 aa left outer join yk药品信息 bb on aa.药品序号 = bb.药品序号
			                                        left outer join yb_药品对照 dz on aa.药品序号=dz.药品序号
			                                        left outer join yb_bl药品比例 bl on dz.医保编码=bl.药品编码
                                        where    aa.库存数量 > 0 and aa.药房编码 =  {0}
                                        group by aa.药品序号, aa.药品名称, aa.药房规格, aa.药房单位, aa.医院零售价, bb.[默认频次],
			                                         bb.默认用法, bb.[药品剂量], bb.剂量单位, bb.财务分类, bb.拼音代码,bl.职工住院自付比例,bl.职工门诊自付比例,bl.居民住院自付比例,
                                                    bl.居民门诊自付比例,bl.离休自付比例,bl.备注,aa.药房编码
                                        union all			 
                                        SELECT sf.收费编码 项目编码, sf.收费名称 项目名称,  '' 药房规格,sf.单位, 单价, -1 库存数量, '医技项目' 类型,
		                                        '' [默认频次], '' 默认用法, 0 [药品剂量], '' 剂量单位,
		                                         '' 财务分类, sf.拼音代码,bl.职工住院自付比例,bl.职工门诊自付比例,bl.居民住院自付比例,bl.居民门诊自付比例,bl.离休自付比例,
		                                         bl.一级医院限价,bl.二级医院限价,bl.三级医院限价,最高限价
		                                         ,bl.备注+ '__内涵：'+bl.项目内涵+ '__除外内容：'+bl.除外内容 使用限制和备注,0
                                        FROM   GY收费小项 sf left outer join yb_诊疗对照 dz on sf.收费编码=replace(dz.诊疗编码,'A','')
                                        left outer join 
                                        (
                                        SELECT
	                                        createTime,
	                                        ID,
	                                        [备注],
	                                        [除外内容],
	                                        [单位],
	                                        [二级医院限价],
	                                        [居民门诊自付比例],
	                                        [居民住院自付比例],
	                                        [离休自付比例],
	                                        [三级医院限价],
	                                        [收费项目等级],
	                                        [项目名称],
	                                        [项目内涵],
	                                        [一级医院限价],
	                                        [诊疗项目编码],
	                                        [职工门诊自付比例],
	                                        [职工住院自付比例],
	                                        [最高限价] 
                                        FROM
	                                        dbo.[yb_bl诊疗项目比例] 
                                        UNION ALL
                                        SELECT
	                                        createTime,
	                                        ID,
	                                        [备注],
	                                        [除外内容],
	                                        '' 单位,
	                                        [二级医院限价],
	                                        [服务设施名称],
	                                        [居民门诊自付比例],
	                                        [居民住院自付比例],
	                                        [离休自付比例],
	                                        [三级医院限价],
	                                        '' 收费项目等级,
	                                        [项目内涵],
	                                        [一级医院限价],
	                                        [医疗服务设施编码],
	                                        [职工门诊自付比例],
	                                        [职工住院自付比例],
	                                        [最高限价] 
                                        FROM
	                                        dbo.[yb_bl医疗服务设施]
                                        )
                                        bl on dz.医保编码 = bl.诊疗项目编码
                                        WHERE  是否禁用 = 0
                                        union all			 
                                        SELECT 项目编号 项目编码, 项目名称,  '' 药房规格,'' 药房单位,0 单价, -1 库存数量, '医嘱项目' 类型,
			                                        '' [默认频次], '' 默认用法, 0 [药品剂量], '' 剂量单位, '' 财务分类, 拼音代码,
	                                        null 职工住院自付比例,null 职工门诊自付比例,null 居民住院自付比例,null 居民门诊自付比例,null 离休自付比例,
		                                         null 一级医院限价,null 二级医院限价,null 三级医院限价,null 最高限价,			'' 使用限制,0
                                        FROM   YS医嘱项目
                                        WHERE  是否禁用 = 0
                                        ", _s药房编码);
            return chis.Database.SqlQuery<Pojo医嘱提示>(sql).ToList();
        }
    }
}
