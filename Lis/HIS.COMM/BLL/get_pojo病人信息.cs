﻿using HIS.COMM.Helper;
using HIS.Model;
using HIS.Model.Helper;
using HIS.Model.Pojo;
using System;
using System.Linq;

namespace HIS.COMM.BLL
{
    public static class get_pojo病人信息
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public static Pojo病人信息 byZYID(decimal _zyid)
        {
            var zy病人信息 = chis.ZY病人信息.Where(c => c.ZYID == _zyid).FirstOrDefault();
            var ys = chis.pubUsers.Where(c => c.用户编码 == zy病人信息.主治医生编码).FirstOrDefault();
            var yjk = chis.ZY预交款.Where(c => c.ZYID == _zyid).Sum(c => c.交款额);
            var zfy = chis.ZY在院费用.Where(c => c.ZYID == _zyid).Sum(c => c.金额);
            Pojo病人信息 pojo = modelHelper.Mapper<ZY病人信息, Pojo病人信息>(zy病人信息);
            pojo.年龄 = Convert.ToByte(AgeHelper.GetAgeByBirthdate(Convert.ToDateTime(zy病人信息.出生日期)));
            pojo.总费用 = Convert.ToDecimal(zfy);
            pojo.预交款 = Convert.ToDecimal(yjk);
            pojo.欠款 = pojo.总费用 - pojo.预交款;
            pojo.主治医生姓名 = ys.用户名;
            pojo.病区名称 = CacheData.Gy科室设置.Where(c => c.科室编码 == Convert.ToInt32(zy病人信息.病区)).FirstOrDefault().科室名称;
            pojo.科室名称 = CacheData.Gy科室设置.Where(c => c.科室编码 == Convert.ToInt32(zy病人信息.科室)).FirstOrDefault().科室名称;
            return pojo;
        }
    }
}
