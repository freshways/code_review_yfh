﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HIS.COMM.BLL
{
    public class CacheData
    {

        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
        private static List<GY全局参数> _gy全局参数;
        private static List<GY收费小项> _gy收费小项;
        private static List<GY科室设置> _gy科室设置;
        private static List<pubUser> _pubuser;
        private static List<lis_HisLis项目对照> _lis_HisLis项目对照;

        private static List<yb_bl医疗服务设施> _yb_bl医疗服务设施;

        private static List<Pojo药房权限> _pojo药房权限s;

        public static List<Pojo药房权限> Pojo药房权限s(string _sMAC)
        {
           
            if (_pojo药房权限s == null)
            {
                var s药房权限 = chis.GY站点设置.Where(c => c.Mac地址 == _sMAC).FirstOrDefault().药房权限.Split('|');
                var filter = Array.ConvertAll<string, int>(s药房权限, c => int.Parse(c));
                _pojo药房权限s =
                    (from s in chis.GY科室设置
                     where filter.Contains(s.科室编码)
                     select new Pojo药房权限()
                     {
                         科室编码 = s.科室编码,
                         科室名称 = s.科室名称
                     }).ToList();
            }
            return _pojo药房权限s;
        }

        public static List<GY收费小项> Gy收费小项
        {
            get
            {
                if (_gy收费小项 == null)
                {
                    _gy收费小项 = chis.GY收费小项.ToList();
                }
                return _gy收费小项;
            }
            set => _gy收费小项 = value;
        }

        public static List<lis_HisLis项目对照> Lis_HisLis项目对照
        {
            get
            {
                if (_lis_HisLis项目对照 == null)
                {
                    _lis_HisLis项目对照 = chis.lis_HisLis项目对照.ToList();
                }
                return _lis_HisLis项目对照;
            }

            set => _lis_HisLis项目对照 = value;
        }

        public static List<yb_bl医疗服务设施> Yb_bl医疗服务设施
        {
            get
            {
                if (_yb_bl医疗服务设施 == null)
                {

                    _yb_bl医疗服务设施 = chis.yb_bl医疗服务设施.ToList();

                }
                return _yb_bl医疗服务设施;
            }

            set => _yb_bl医疗服务设施 = value;
        }

        public static List<GY全局参数> Gy全局参数
        {
            get
            {
                if (_gy全局参数 == null)
                {

                    _gy全局参数 = chis.GY全局参数.ToList();

                }
                return _gy全局参数;
            }
            set => _gy全局参数 = value;
        }
        public static List<GY科室设置> Gy科室设置
        {
            get
            {
                if (_gy科室设置 == null)
                {
                    _gy科室设置 = chis.GY科室设置.ToList();
                }
                return _gy科室设置;
            }
            set => _gy科室设置 = value;
        }

        public static List<pubUser> Pubuser
        {
            get
            {
                if (_pubuser == null)
                {
                    _pubuser = chis.pubUsers.ToList();
                }
                return _pubuser;
            }
            set => _pubuser = value;
        }
    }
}
