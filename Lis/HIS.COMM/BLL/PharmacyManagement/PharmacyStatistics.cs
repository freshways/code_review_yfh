﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.BLL.PharmacyManagement
{
    /// <summary>
    /// 药房统计，注意特殊判断的 药品序号<>0 and visible=1
    /// </summary>
    public class PharmacyStatistics
    {
        /// <summary>
        /// 药房入库摘要
        /// </summary>
        /// <param name="kssj"></param>
        /// <param name="jzsj"></param>
        /// <param name="_yfbmWhere"></param>
        /// <param name="_ypxhWhere"></param>
        /// <returns></returns>
        public static DataTable getEnteringWarehouse(string kssj, string jzsj, string _yfbmWhere, string _ypxhWhere)
        {
            string sql = $@"
                    select 药品序号,药品名称,[药房规格],[药房单位],sum(实发数量) 入库数量
                    from yf入库明细 aa left outer join yf入库摘要 bb on aa.入库单号=bb.入库单号 
                    where bb.确认时间 between '{kssj}' and '{jzsj}'  and 药品序号<>0 and visible=1  {_yfbmWhere} {_ypxhWhere}
                    group by 药品序号,药品名称,[药房规格],[药房单位]";
            return HIS.Model.Dal.SqlHelper.ExecuteDataTable(sql);
        }
        /// <summary>
        /// 药品药房入库明细
        /// </summary>
        /// <param name="kssj"></param>
        /// <param name="jzsj"></param>
        /// <param name="_yfbmWhere"></param>
        /// <param name="_ypxhWhere"></param>
        /// <returns></returns>
        public static DataTable getEnteringWarehouseDetail(string kssj, string jzsj, string _yfbmWhere, string _ypxhWhere)
        {
            string sql = $@"select aa.[入库单号], aa.[药品序号], aa.[药品名称], aa.[药房规格], aa.[药房单位], aa.[药品产地], aa.[药品批号], 
                        aa.[药品效期], aa.[实发数量], aa.[医院零售价], aa.[医院零售价金额], aa.[包装转换], aa.[YPID], 
                        bb.[确认时间],bb.[药库出库单号],bb.[操作员],bb.[入库方式]
                        from yf入库明细 aa left outer join yf入库摘要 bb on aa.入库单号=bb.入库单号 where bb.确认时间  between '{kssj}' and '{jzsj}'  and 药品序号<>0 and visible=1 {_yfbmWhere} {_ypxhWhere} ";
            return HIS.Model.Dal.SqlHelper.ExecuteDataTable(sql);

        }
        /// <summary>
        /// 药品用量摘要
        /// </summary>
        /// <param name="kssj"></param>
        /// <param name="jzsj"></param>
        /// <param name="_yfbmWhere"></param>
        /// <param name="_ypxhWhere"></param>
        /// <returns></returns>
        public static DataTable getDetail(string kssj, string jzsj, string _yfbmWhere, string _ypxhWhere)
        {
            string sql = $@"
                      select '在院病人' 病人类型,费用编码, 费用名称, 数量,  金额,记费时间,brxx.[病人姓名]
                      from   ZY在院费用 zy
                             left outer join ZY病人信息 brxx on zy.ZYID = brxx.ZYID    
                      where 数量<>0 and  YPID <> 0 and 发药时间 between '{kssj}' and '{jzsj}' and 费用编码<>0  {_yfbmWhere}  {_ypxhWhere} 
                      union all
                      select '出院病人' 病人类型,费用编码, 费用名称, 数量,  金额,记费时间,brxx.[病人姓名]
                      from   ZY出院费用 cy
                             left outer join ZY病人信息 brxx on cy.ZYID = brxx.ZYID
                      where 数量<>0 and  YPID <> 0 and 发药时间 between '{kssj}' and '{jzsj}'  and 费用编码<>0  {_yfbmWhere}  {_ypxhWhere} 
                      union all 
                      select '门诊病人' 病人类型,药品序号,药品名称,处方总量,aa.零售价金额 ,bb.[发药时间],mz.[病人姓名]
                      from [MF处方明细]  aa left outer join mf处方摘要 bb on aa.CFID=bb.CFID left outer join mf门诊摘要 mz on bb.MZID=mz.MZID
                      where bb.作废标记=0 and 发药时间 between '{kssj}' and '{jzsj}'  and 药品序号<>0  {_yfbmWhere}  {_ypxhWhere} ";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, sql).Tables[0];
        }

        /// <summary>
        /// 药品用量明细
        /// </summary>
        /// <param name="kssj"></param>
        /// <param name="jzsj"></param>
        /// <param name="_yfbmWhere"></param>
        /// <param name="_ypxhWhere"></param>
        /// <returns></returns>
        public static DataTable getSummary(string kssj, string jzsj, string _yfbmWhere, string _ypxhWhere)
        {
            string SQL = $@"select jg1.*,yp.药品名称 yk药品名称,yp.药房规格,yp.药房单位 
                    from (
                    select   费用编码 药品编码, 费用名称 药品名称, SUM(数量) 数量, SUM(金额) 金额
                    from     (
                                select 费用编码, 费用名称, 数量,  金额
                                from   ZY在院费用 zy
                                        left outer join ZY病人信息 brxx on zy.ZYID = brxx.ZYID                
                                where 数量<>0 and  YPID <> 0 and 发药时间 between '{kssj}' and '{jzsj}'  and 费用编码<>0  {_ypxhWhere} {_yfbmWhere}
                    union all
                                        select 费用编码, 费用名称, 数量, 金额
                                        from ZY出院费用 cy
                                                left outer join ZY病人信息 brxx on cy.ZYID = brxx.ZYID
                                        where 数量<>0 and YPID<> 0 and 发药时间 between '{kssj}' and '{jzsj}' and 费用编码<>0 { _ypxhWhere} {_yfbmWhere}
                    union all
                                            select 药品序号, 药品名称, 处方总量, aa.零售价金额 from[MF处方明细]  aa left outer join mf处方摘要 bb on aa.CFID = bb.CFID
                                            where 作废标记 = 0 and 发药时间 between '{kssj}' and '{jzsj}'  and 药品序号<>0   {_ypxhWhere} {_yfbmWhere}
                                        ) aa
                    group by 费用编码, 费用名称
                    ) jg1 left outer join yk药品信息 yp on jg1.药品编码 = yp.药品序号";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, SQL).Tables[0];
        }
    }
}
