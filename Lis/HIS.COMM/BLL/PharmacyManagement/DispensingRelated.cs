﻿using HIS.Model;
using HIS.Model.Helper;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WEISHENG.COMM;

namespace HIS.COMM.BLL.PharmacyManagement
{
    public class DispensingRelated
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
        public static List<PojoOutpatientDispensing> GetPojoOutpatientDispensingsByPharmacyCode(string prescriptionType, int pharmacyCode, string sfzh = "")
        {
            string SQL = "";
            string sqlWhere = sfzh == "" ? "" : " and bb.身份证号='" + sfzh + "'";
            string SQL_Western =
                    "select bb.性别,bb.年龄,bb.年龄单位,bb.临床诊断, aa.处方贴数,aa.处方类型,aa.医生编码, ee.科室名称,aa.CFID , aa.开方时间, bb.病人姓名, dd.用户名 处方录入, ee.[用户名] 诊治医生," + "\r\n" +
                    "       ff.[用户名] 收款人,aa.零售价金额,isnull(aa.医技业务号,0) 医技业务号,aa.MZID" + "\r\n" +
                    "from   mf处方摘要 aa" + "\r\n" +
                    "       left outer join mf门诊摘要 bb on aa.mzid = bb.mzid " + "\r\n" +
                    "       left outer join pubuser dd on bb.录入人编码 = dd.用户编码" + "\r\n" +
                    "       left outer join pubUser ee on bb.[医生编码] = ee.[用户编码]" + "\r\n" +
                    "       left outer join pubuser ff on bb.[收款人编码] = ff.[用户编码]" + "\r\n" +
                    "where   aa.药房编码 = " + pharmacyCode +
                    " and 已发药标记 = 0 and aa.作废标记 = 0 and  bb.作废标记 = 0 and bb.已收款标记 = 1" + sqlWhere;
            string sqlChinese = "select bb.性别,bb.年龄,bb.年龄单位,bb.临床诊断,aa.处方贴数,aa.处方类型,aa.医生编码, ee.科室名称,aa.CFID , aa.开方时间, bb.病人姓名, dd.用户名 录入人, ee.[用户名] 诊治医生," + "\r\n" +
                   "       ff.[用户名] 收款人,aa.零售价金额,isnull(aa.医技业务号,0) 医技业务号,aa.MZID" + "\r\n" +
                   "from   MF中药方_处方摘要 aa" + "\r\n" +
                   "       left outer join mf门诊摘要 bb on aa.mzid = bb.mzid " + "\r\n" +
                   "       left outer join pubuser dd on bb.录入人编码 = dd.用户编码" + "\r\n" +
                   "       left outer join pubUser ee on bb.[医生编码] = ee.[用户编码]" + "\r\n" +
                   "       left outer join pubuser ff on bb.[收款人编码] = ff.[用户编码]" + "\r\n" +
                   "where  aa.药房编码 = " + pharmacyCode +
                   " and 已发药标记 = 0 and aa.作废标记=0 and bb.作废标记 = 0 and bb.已收款标记 = 1" + sqlWhere;
            switch (prescriptionType)
            {
                case "西药方":
                    SQL = SQL_Western;
                    break;
                case "中药方":
                    SQL = sqlChinese;
                    break;
                case "全部":
                    SQL = SQL_Western + " union all " + sqlChinese;
                    break;
            }
            return chis.Database.SqlQuery<HIS.Model.Pojo.PojoOutpatientDispensing>(SQL).OrderBy(c => c.CFID).ToList();
        }

        /// <summary>
        /// 根据CFID获取处方摘要和病人信息
        /// </summary>
        /// <param name="prescriptionType"></param>
        /// <param name="cfid"></param>
        /// <returns></returns>
        public static PojoOutpatientDispensing GetPojoOutpatientDispensingsByCFID(decimal cfid)
        {
            decimal mzid;
            var westPrescription = chis.MF处方摘要.Where(c => c.CFID == cfid).FirstOrDefault();
            PojoOutpatientDispensing pojoOutpatientDispensing = new PojoOutpatientDispensing();
            if (westPrescription != null)
            {
                pojoOutpatientDispensing = modelHelper.Mapper<MF处方摘要, PojoOutpatientDispensing>(westPrescription);
                mzid = Convert.ToDecimal(westPrescription.MZID);
            }
            else
            {
                var chinesePrescription = chis.MF中药方_处方摘要.Where(c => c.CFID == cfid).FirstOrDefault();
                pojoOutpatientDispensing = modelHelper.Mapper<MF中药方_处方摘要, PojoOutpatientDispensing>(chinesePrescription);
                mzid = Convert.ToDecimal(chinesePrescription.MZID);
            }

            var mf门诊摘要 = chis.MF门诊摘要.Where(c => c.MZID == mzid).FirstOrDefault();
            pojoOutpatientDispensing.科室名称 = chis.pubUsers.Where(c => c.用户编码 == mf门诊摘要.医生编码).FirstOrDefault().科室名称;
            pojoOutpatientDispensing.病人姓名 = mf门诊摘要.病人姓名;
            pojoOutpatientDispensing.处方录入 = chis.pubUsers.Where(c => c.用户编码 == mf门诊摘要.录入人编码).FirstOrDefault().用户名;
            pojoOutpatientDispensing.诊治医生 = chis.pubUsers.Where(c => c.用户编码 == mf门诊摘要.医生编码).FirstOrDefault().用户名;
            pojoOutpatientDispensing.收款人 = chis.pubUsers.Where(c => c.用户编码 == mf门诊摘要.收款人编码).FirstOrDefault().用户名;
            pojoOutpatientDispensing.性别 = mf门诊摘要.性别;
            pojoOutpatientDispensing.年龄 = Convert.ToInt16(mf门诊摘要.年龄);
            pojoOutpatientDispensing.年龄单位 = mf门诊摘要.年龄单位;
            pojoOutpatientDispensing.临床诊断 = mf门诊摘要.临床诊断;
            return pojoOutpatientDispensing;
        }
        /// <summary>
        /// 获取某诊次处方摘要
        /// </summary>
        /// <param name="prescriptionType"></param>
        /// <param name="pharmacyCode"></param>
        /// <returns></returns>
        public static List<PojoOutpatientDispensing> GetPojoOutpatientDispensingsByMZID(string prescriptionType, decimal mzid)
        {
            string SQL = "";
            string SQL_Western =
                    "select bb.性别,bb.年龄,bb.年龄单位,bb.临床诊断,aa.处方贴数,aa.处方类型,aa.医生编码, ee.科室名称,aa.CFID , aa.开方时间, bb.病人姓名, dd.用户名 处方录入, ee.[用户名] 诊治医生," + "\r\n" +
                    "       ff.[用户名] 收款人,aa.零售价金额,isnull(aa.医技业务号,0) 医技业务号,aa.MZID" + "\r\n" +
                    "from   mf处方摘要 aa" + "\r\n" +
                    "       left outer join mf门诊摘要 bb on aa.mzid = bb.mzid " + "\r\n" +
                    "       left outer join pubuser dd on bb.录入人编码 = dd.用户编码" + "\r\n" +
                    "       left outer join pubUser ee on bb.[医生编码] = ee.[用户编码]" + "\r\n" +
                    "       left outer join pubuser ff on bb.[收款人编码] = ff.[用户编码]" + "\r\n" +
                    "where   aa.mzid = " + mzid +
                    " and aa.作废标记 = 0 ";
            string sqlChinese = "select bb.性别,bb.年龄,bb.年龄单位,bb.临床诊断,aa.处方贴数,aa.处方类型,aa.医生编码, ee.科室名称,aa.CFID , aa.开方时间, bb.病人姓名, dd.用户名 录入人, ee.[用户名] 诊治医生," + "\r\n" +
                   "       ff.[用户名] 收款人,aa.零售价金额,isnull(aa.医技业务号,0) 医技业务号,aa.MZID" + "\r\n" +
                   "from   MF中药方_处方摘要 aa" + "\r\n" +
                   "       left outer join mf门诊摘要 bb on aa.mzid = bb.mzid " + "\r\n" +
                   "       left outer join pubuser dd on bb.录入人编码 = dd.用户编码" + "\r\n" +
                   "       left outer join pubUser ee on bb.[医生编码] = ee.[用户编码]" + "\r\n" +
                   "       left outer join pubuser ff on bb.[收款人编码] = ff.[用户编码]" + "\r\n" +
                   "where  aa.mzid = " + mzid +
                   "  and aa.作废标记 = 0 ";
            switch (prescriptionType)
            {
                case "西药方":
                    SQL = SQL_Western;
                    break;
                case "中药方":
                    SQL = sqlChinese;
                    break;
                case "全部":
                    SQL = SQL_Western + " union all " + sqlChinese;
                    break;
            }
            return chis.Database.SqlQuery<HIS.Model.Pojo.PojoOutpatientDispensing>(SQL).ToList();
        }
        /// <summary>
        /// 获取中西药方发药内容
        /// </summary>
        /// <param name="pharmacyCode"></param>
        /// <param name="prescriptionID"></param>
        /// <returns></returns>
        public static List<PojoOutpatientDispensingContent> GetPojoOutpatientDispensingContentsByPharmacycodeAndPrescriptionID(int pharmacyCode, decimal prescriptionID)
        {

            string SQL =
          "SELECT   aa.药品序号, aa.药品名称, aa.药房规格, aa.药房单位, aa.药品产地, aa.药品批号, aa.处方总量," + "\r\n" +
          "         bb.库存数量, aa.零售价, aa.零售价金额, aa.YPID, aa.用法, aa.频次, aa.组别," + "\r\n" +
          "         aa.剂量数量, aa.剂量单位, aa.id" + "\r\n" +
          "FROM     MF处方明细 aa left outer join yf库存信息 bb on aa.ypid = bb.ypid" + "\r\n" +
          "where    bb.药房编码 = " + pharmacyCode + " and cfid = " + prescriptionID + "\r\n" +
          "  union all SELECT   aa.药品序号, aa.药品名称, aa.药房规格, aa.药房单位, aa.药品产地, aa.药品批号, aa.处方总量," + "\r\n" +
           "         bb.库存数量, aa.零售价, aa.零售价金额, aa.YPID, aa.用法, aa.频次, aa.组别," + "\r\n" +
           "         aa.剂量数量, aa.剂量单位, aa.id" + "\r\n" +
           "FROM    MF中药方_处方明细 aa left outer join yf库存信息 bb on aa.ypid = bb.ypid" + "\r\n" +
           "where     bb.药房编码 = " + pharmacyCode + " and cfid = " + prescriptionID;
            return chis.Database.SqlQuery<PojoOutpatientDispensingContent>(SQL).OrderBy(c => c.组别).ThenBy(c => c.ID).ToList();
        }

        /// <summary>
        /// 根据CFID获取门诊病人处方内容
        /// </summary>
        /// <param name="pharmacyCode"></param>
        /// <param name="prescriptionID"></param>
        /// <returns></returns>
        public static List<PojoOutpatientDispensingContent> GetPojoOutpatientPrescriptionContentsByPrescriptionID(decimal prescriptionID)
        {
            string SQL =
                 "SELECT   aa.药品序号, aa.药品名称, aa.药房规格, aa.药房单位, aa.药品产地, aa.药品批号, aa.处方总量," + "\r\n" +
                 "         bb.库存数量, aa.零售价, aa.零售价金额, aa.YPID, aa.用法, aa.频次, aa.组别," + "\r\n" +
                 "         aa.剂量数量, aa.剂量单位, aa.id" + "\r\n" +
                 "FROM     MF处方明细 aa left outer join yf库存信息 bb on aa.ypid = bb.ypid" + "\r\n" +
                 " left outer join MF处方摘要 cc on aa.cfid=cc.cfid  " +
                 "where  bb.药房编码=cc.药房编码 and  aa.cfid = " + prescriptionID + "\r\n" +
                 "  union all SELECT   aa.药品序号, aa.药品名称, aa.药房规格, aa.药房单位, aa.药品产地, aa.药品批号, aa.处方总量," + "\r\n" +
                  "         bb.库存数量, aa.零售价, aa.零售价金额, aa.YPID, aa.用法, aa.频次, aa.组别," + "\r\n" +
                  "         aa.剂量数量, aa.剂量单位, aa.id" + "\r\n" +
                  "FROM    MF中药方_处方明细 aa left outer join yf库存信息 bb on aa.ypid = bb.ypid" + "\r\n" +
                  " left outer join MF中药方_处方摘要 cc on aa.cfid=cc.cfid  " +
                  "where  bb.药房编码=cc.药房编码 and    aa.cfid = " + prescriptionID;
            return chis.Database.SqlQuery<PojoOutpatientDispensingContent>(SQL).ToList();
        }

        public static Boolean MedicinesInventoryMonitoring(CHISEntities chis, int pharmacyCode, decimal dypid, string s操作时间, string s操作摘要, string s操作明细, string s模块,
        decimal d操作前, decimal d操作数, decimal d操作后, string userName)
        {
            var medicine = new YP库存监控()
            {
                操作时间 = Convert.ToDateTime(s操作时间),
                YPID = dypid,
                操作摘要 = s操作摘要,
                操作明细 = s操作明细,
                操作前库存数 = d操作前,
                业务数 = d操作数,
                操作后库存数 = d操作后,
                模块名 = s模块,
                操作员名 = userName,
                药房编码 = pharmacyCode,
                createTime = DateTime.Now,
            };
            chis.YP库存监控.Attach(medicine);
            chis.Entry(medicine).State = System.Data.Entity.EntityState.Added;
            return true;
        }
        private static bool b是否可以发药(decimal cfid, bool b是否门诊预交款)
        {
            string SQL_XY =
                "select count(*)" + "\r\n" +
                "from   MF处方摘要 aa left outer join MF门诊摘要 bb on aa.mzid = bb.mzid" + "\r\n" +
                "where  aa.CFID = " + cfid + " and aa.已发药标记 = 0 and aa.作废标记 = 0 ";
            string SQL_ZY =
               "select count(*)" + "\r\n" +
               "from   MF中药方_处方摘要 aa left outer join MF门诊摘要 bb on aa.mzid = bb.mzid" + "\r\n" +
               "where  aa.CFID = " + cfid + " and aa.已发药标记 = 0 and aa.作废标记 = 0 ";
            if (b是否门诊预交款 == false)
            {
                SQL_XY += " and bb.已收款标记 = 1";
                SQL_ZY += " and bb.已收款标记 = 1";
            }
            int i_XY可发药记录 = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(SQL_XY));
            int i_ZY可发药记录 = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(SQL_ZY));
            if (i_XY可发药记录 == 1 || i_ZY可发药记录 == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool Dispensing(PojoOutpatientDispensing outpatientDispensing, List<PojoOutpatientDispensingContent> outpatientDispensingContents, int pharmacyCode)
        {
            var _sbInfo = new StringBuilder();
            if (b是否可以发药(Convert.ToDecimal(outpatientDispensing.CFID), HIS.COMM.baseInfo.b启用医卡通门诊余额控制) == false)
            {
                msgBalloonHelper.ShowInformation("当前处方已经作废或已经发药，请核对信息");
                return false;
            }
            string s发药时间 = HIS.Model.Helper.timeHelper.getServerDateTimeStr();


            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
            {
                using (var dbContextTransaction = chis.Database.BeginTransaction())
                {
                    try
                    {
                        chis.Database.Log = s => WEISHENG.COMM.LogHelper.LogToFile("ef6调试", s);
                        foreach (var prescriptionItem in outpatientDispensingContents)
                        {
                            var pharmacyInventory = chis.YF库存信息.Where(c => c.药房编码 == pharmacyCode && c.YPID == prescriptionItem.YPID).FirstOrDefault();
                            decimal d发药后数量 = Convert.ToDecimal(pharmacyInventory.库存数量) - Convert.ToDecimal(prescriptionItem.处方总量);
                            if (Convert.ToDecimal(prescriptionItem.处方总量) > Convert.ToDecimal(pharmacyInventory.库存数量))
                            {
                                throw new Exception("【" + pharmacyInventory.药品名称 + "】超出当前库存,发药操作取消。");
                            }
                            //1、修改库存数量
                            //chis.Entry(pharmacyInventory).State = System.Data.Entity.EntityState.Detached;
                            //chis.YF库存信息.Attach(pharmacyInventory);
                            pharmacyInventory.库存数量 = d发药后数量;
                            //chis.Entry(pharmacyInventory).Property(c => c.库存数量).IsModified = true;
                            //2、记录药品监控日志
                            if (MedicinesInventoryMonitoring(chis, pharmacyCode, Convert.ToDecimal(prescriptionItem.YPID),
                                s发药时间, "药房门诊发药", "病人姓名|" + outpatientDispensing.病人姓名 + "|处方ID|" + outpatientDispensing.CFID.ToString(),
                                "药房", Convert.ToDecimal(pharmacyInventory.库存数量), Convert.ToDecimal(prescriptionItem.处方总量), d发药后数量, HIS.COMM.zdInfo.ModelUserInfo.用户名) == false)
                            {
                                throw new Exception("记录库存监控数据失败" + Convert.ToString(prescriptionItem.YPID));
                            }
                        }
                        //3、修改处方摘要发药标记等
                        //3.1西药处方
                        var westernMedicinesPrescription = chis.MF处方摘要.Where(c => c.CFID == outpatientDispensing.CFID && c.作废标记 == false).FirstOrDefault();
                        var chineseMedicinesPrescription = chis.MF中药方_处方摘要.Where(c => c.CFID == outpatientDispensing.CFID && c.作废标记 == false).FirstOrDefault();
                        if (westernMedicinesPrescription != null && chineseMedicinesPrescription != null)
                        {
                            throw new Exception("中西药处方号重复,请联系信息科工作人员处理");
                        }
                        if (westernMedicinesPrescription != null)
                        {
                            westernMedicinesPrescription.已发药标记 = true;
                            westernMedicinesPrescription.发药时间 = Convert.ToDateTime(s发药时间);
                            westernMedicinesPrescription.发药人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码;
                            chis.Entry(westernMedicinesPrescription).Property(c => c.已发药标记).IsModified = true;
                            chis.Entry(westernMedicinesPrescription).Property(c => c.发药时间).IsModified = true;
                            chis.Entry(westernMedicinesPrescription).Property(c => c.发药人编码).IsModified = true;
                        }
                        //3.2中药处方
                        if (chineseMedicinesPrescription != null)
                        {
                            chineseMedicinesPrescription.已发药标记 = true;
                            chineseMedicinesPrescription.发药时间 = Convert.ToDateTime(s发药时间);
                            chineseMedicinesPrescription.发药人编码 = HIS.COMM.zdInfo.ModelUserInfo.用户编码;
                            chis.Entry(chineseMedicinesPrescription).Property(c => c.已发药标记).IsModified = true;
                            chis.Entry(chineseMedicinesPrescription).Property(c => c.发药时间).IsModified = true;
                            chis.Entry(chineseMedicinesPrescription).Property(c => c.发药人编码).IsModified = true;
                        }
                        {
                            //    if (dec医技业务号 > 100)//实行医卡通模式的单位
                            //    {
                            //        string SQL申请 =
                            //         "UPDATE [QR申请摘要]" + "\r\n" +
                            //         "SET    [执行科室编码] = " + HIS.COMM.zdInfo.Model科室信息.科室编码.ToString() + ", [执行科室名称] = '" + HIS.COMM.zdInfo.Model科室信息.科室名称 +
                            //         "', [执行时间] = getdate()," + "\r\n" +
                            //         "       [执行人编码] = " + HIS.COMM.zdInfo.ModelUserInfo.用户编码 + ", [执行人姓名] = '" + HIS.COMM.zdInfo.ModelUserInfo.用户名 + "'," + "\r\n" +
                            //         "       [执行单位编码] = " + HIS.COMM.zdInfo.Model单位信息.iDwid + ", [执行分院编码] = " + HIS.COMM.zdInfo.Model站点信息.分院编码 + "\r\n" +
                            //         "WHERE  医技业务号 = " + dec医技业务号.ToString();
                            //        if (HIS.COMM.baseInfo.b启用医卡通门诊余额控制)
                            //        {
                            //            //HIS.YKT.ClassYKTCard.get医卡通账户(mzPerson.S医卡通账户号, mzPerson);//防止在不刷新读卡，等待长时间后直接执行，产生余额信息不是当前余额的情况
                            //            //if (s病人姓名 != mzPerson.S姓名)
                            //            //{
                            //            //    connT.Rollback();
                            //            //    _sbInfo.Append("读取卡中姓名【" + mzPerson.S姓名 + "】与处方姓名【" + s病人姓名 + "】不符");
                            //            //    return false;
                            //            //}
                            //            //if (mzPerson.S医卡通账户号 != Math.Floor(dec医技业务号).ToString())
                            //            //{
                            //            //    connT.Rollback();
                            //            //    _sbInfo.Append("读取卡中账户号与作废信息不符");
                            //            //    return false;
                            //            //}
                            //            //decimal dec当前余额 = mzPerson.Dec医卡通当前余额;
                            //            //bool b支付成功 = HIS.YKT.ClassYKTCard.update账户支付(mzPerson, ref dec当前余额, dec零售价金额, dec医技业务号.ToString(), connT);
                            //            //if (b支付成功 == false)
                            //            //{
                            //            //    connT.Rollback();
                            //            //    _sbInfo.Append("账户余额更新失败，取消发药操作");
                            //            //    return false;
                            //            //}
                            //        }
                        }

                        chis.SaveChanges();
                        dbContextTransaction.Commit();
                        HIS.COMM.msgBalloonHelper.BalloonShow("数据保存成功");
                        return true;
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                                    validationErrors.Entry.Entity.GetType().FullName,
                                    validationError.PropertyName,
                                    validationError.ErrorMessage));
                            }
                        }
                        dbContextTransaction.Rollback();
                        HIS.COMM.msgBalloonHelper.BalloonShow(stringBuilder.ToString());
                        return false;
                    }
                    catch (Exception ex)
                    {
                        string errorMsg = "错误：";
                        if (ex.InnerException == null)
                            errorMsg += ex.Message + "，";
                        else if (ex.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.Message + "，";
                        else if (ex.InnerException.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.InnerException.Message;
                        dbContextTransaction.Rollback();
                        HIS.COMM.msgBalloonHelper.BalloonShow(errorMsg);
                        return false;
                    }
                }
            }
        }

        public static bool RefundMedicines(int pharmacyCode, decimal CFID)
        {
            var outpatientWesternPrescription = chis.MF处方摘要.Where(c => c.已发药标记 == true && c.作废标记 == false && c.CFID == CFID).FirstOrDefault();
            var outpatientChinesePrescription = chis.MF中药方_处方摘要.Where(c => c.已发药标记 == true && c.作废标记 == false && c.CFID == CFID).FirstOrDefault();
            decimal mzid = outpatientWesternPrescription == null ? Convert.ToDecimal(outpatientChinesePrescription.MZID) : Convert.ToDecimal(outpatientWesternPrescription.MZID);
            var outpatientFeeSummary = chis.MF门诊摘要.Where(c => c.MZID == mzid).FirstOrDefault();
            //0获取中西合并的药品处方内容 
            var prescriptionContent = GetPojoOutpatientDispensingContentsByPharmacycodeAndPrescriptionID(pharmacyCode, CFID);
            if (outpatientFeeSummary.结账时间 != null)
            {
                throw new Exception($"此处方已经于【{outpatientFeeSummary.结账时间}】结账");
            }

            if (prescriptionContent.Count == 0)
            {
                throw new Exception("未发现处方明细");
            }


            decimal dec医技业务号 = outpatientWesternPrescription == null ? Convert.ToDecimal(outpatientChinesePrescription.医技业务号) : Convert.ToDecimal(outpatientWesternPrescription.医技业务号);


            if (dec医技业务号 > 10000000)
            {
                //if (mzPerson.S医卡通账户号 == "0")
                //{
                //    XtraMessageBox.Show("医卡通病人请先读卡", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}
                //string SQL校验读卡信息 =
                //    "select 病人姓名, 医卡通账户号" + "\r\n" +
                //    "from   MF门诊摘要" + "\r\n" +
                //    "where  mzid in (select MZID" + "\r\n" +
                //    "                from   mf处方摘要" + "\r\n" +
                //    "                where  CFID = " + sCfid + ")";
                //DataTable dt检验 = HIS.Model.Dal.SqlHelper.ExecuteDataset(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL校验读卡信息).Tables[0];
                //string _s病人姓名 = dt检验.Rows[0]["病人姓名"].ToString();
                //string _s医卡通账户号 = dt检验.Rows[0]["医卡通账户号"].ToString();
                //if (mzPerson.S医卡通账户号 != _s医卡通账户号)
                //{
                //    XtraMessageBox.Show("读取卡中账户号与作废信息不符", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}
                //if (mzPerson.S姓名 != _s病人姓名)
                //{
                //    XtraMessageBox.Show("读取卡中姓名与作废信息不符", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}
                //string SQL医卡通是否结算 =
                //"select max(结算时间) from QR申请摘要 where 医技业务号 in " + "\r\n" +
                //"(" + "\r\n" +
                //"select 医技业务号 from MF处方摘要 where CFID=" + sCfid + "\r\n" +
                //")";

                //string s医卡通结算时间 = HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text, SQL医卡通是否结算).ToString();
                //if (s医卡通结算时间 != "")
                //{
                //    XtraMessageBox.Show("医卡通病人已经结算，不能继续取消发药", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}
            }

            //if (dec医技业务号 > 100 && HIS.COMM.baseInfo.b启用医卡通门诊余额控制)
            //{
            //    //bool b取消支付成功 = HIS.YKT.ClassYKTCard.update账户取消支付(mzPerson.S医卡通账户号, dec处方金额, dec医技业务号.ToString(), connT);
            //    //if (b取消支付成功 == false)
            //    //{
            //    //    connT.Rollback();
            //    //    XtraMessageBox.Show("账户余额更新失败，操作取消", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    //    return;
            //    //}
            //}

            string s退药时间 = HIS.Model.Helper.timeHelper.getServerDateTimeStr();

            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
            {
                chis.Database.Log = s => LogHelper.LogToFile("ef6调试", s);
                using (var dbContextTransaction = chis.Database.BeginTransaction())
                {
                    try
                    {
                        chis.Database.Log = s => WEISHENG.COMM.LogHelper.LogToFile("ef6调试", s);
                        foreach (var prescriptionItem in prescriptionContent)
                        {
                            var pharmacyInventory = chis.YF库存信息.Where(c => c.药房编码 == pharmacyCode && c.YPID == prescriptionItem.YPID).FirstOrDefault();
                            decimal afterRefundedNumber = Convert.ToDecimal(pharmacyInventory.库存数量) + Convert.ToDecimal(prescriptionItem.处方总量);
                            //1、修改库存数量
                            chis.Entry(pharmacyInventory).State = System.Data.Entity.EntityState.Detached;
                            chis.YF库存信息.Attach(pharmacyInventory);
                            pharmacyInventory.库存数量 = afterRefundedNumber;
                            chis.Entry(pharmacyInventory).Property(c => c.库存数量).IsModified = true;

                            //2、记录药品监控日志
                            if (MedicinesInventoryMonitoring(chis, pharmacyCode, Convert.ToDecimal(prescriptionItem.YPID),
                                s退药时间, "药房门诊病人取消发药", "病人姓名|" + outpatientFeeSummary.病人姓名 + "|处方ID|" + CFID,
                                "药房", Convert.ToDecimal(pharmacyInventory.库存数量), Convert.ToDecimal(prescriptionItem.处方总量), afterRefundedNumber, HIS.COMM.zdInfo.ModelUserInfo.用户名) == false)
                            {
                                throw new Exception("记录库存监控数据失败" + Convert.ToString(prescriptionItem.YPID));
                            }
                        }
                        //3、修改处方摘要发药标记等
                        //3.1西药处方                   
                        var westernMedicinesPrescription = chis.MF处方摘要.Where(c => c.CFID == CFID && c.作废标记 == false && c.已发药标记 == true).FirstOrDefault();
                        var chineseMedicinesPrescription = chis.MF中药方_处方摘要.Where(c => c.CFID == CFID && c.作废标记 == false && c.已发药标记 == true).FirstOrDefault();
                        if (westernMedicinesPrescription != null && chineseMedicinesPrescription != null)
                        {
                            throw new Exception("中西药处方号重复,请联系信息科工作人员处理");
                        }
                        if (westernMedicinesPrescription != null)
                        {
                            chis.Entry(westernMedicinesPrescription).State = System.Data.Entity.EntityState.Detached;
                            chis.MF处方摘要.Attach(westernMedicinesPrescription);
                            westernMedicinesPrescription.已发药标记 = false;
                            westernMedicinesPrescription.发药时间 = null;
                            westernMedicinesPrescription.发药人编码 = null;
                            chis.Entry(westernMedicinesPrescription).Property(c => c.已发药标记).IsModified = true;
                            chis.Entry(westernMedicinesPrescription).Property(c => c.发药时间).IsModified = true;
                            chis.Entry(westernMedicinesPrescription).Property(c => c.发药人编码).IsModified = true;
                        }
                        //3.2中药处方
                        if (chineseMedicinesPrescription != null)
                        {
                            chis.Entry(chineseMedicinesPrescription).State = System.Data.Entity.EntityState.Detached;
                            chis.MF中药方_处方摘要.Attach(chineseMedicinesPrescription);
                            chineseMedicinesPrescription.已发药标记 = false;
                            chineseMedicinesPrescription.发药时间 = null;
                            chineseMedicinesPrescription.发药人编码 = null;
                            chis.Entry(chineseMedicinesPrescription).Property(c => c.已发药标记).IsModified = true;
                            chis.Entry(chineseMedicinesPrescription).Property(c => c.发药时间).IsModified = true;
                            chis.Entry(chineseMedicinesPrescription).Property(c => c.发药人编码).IsModified = true;
                        }
                        chis.SaveChanges();
                        dbContextTransaction.Commit();
                        HIS.COMM.msgBalloonHelper.BalloonShow("数据保存成功");
                        return true;
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                stringBuilder.Append(string.Format("Class: {0}, Property: {1}, Error: {2}",
                                    validationErrors.Entry.Entity.GetType().FullName,
                                    validationError.PropertyName,
                                    validationError.ErrorMessage));
                            }
                        }
                        dbContextTransaction.Rollback();
                        HIS.COMM.msgBalloonHelper.BalloonShow(stringBuilder.ToString());
                        return false;
                    }
                    catch (Exception ex)
                    {
                        string errorMsg = "错误：";
                        if (ex.InnerException == null)
                            errorMsg += ex.Message + "，";
                        else if (ex.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.Message + "，";
                        else if (ex.InnerException.InnerException.InnerException == null)
                            errorMsg += ex.InnerException.InnerException.Message;
                        dbContextTransaction.Rollback();
                        HIS.COMM.msgBalloonHelper.BalloonShow(errorMsg);
                        return false;
                    }
                }
                chis.Database.Log = null;
            }

        }
    }
}
