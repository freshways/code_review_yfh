﻿using HIS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.BLL.PharmacyManagement
{
    public class Permission
    {
        public static List<GY科室设置> Get药房权限ByUserID()
        {
            string sql = string.Format(@"SELECT * FROM GY科室设置 where [是否单设药房]=1 and 科室编码 in 
                    (
                    select 科室编码 from [pub用户数据权限] where [用户编码]={0}
                    )", HIS.COMM.zdInfo.ModelUserInfo.用户编码);

            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
            return chis.Database.SqlQuery<GY科室设置>(sql).ToList();
        }
    }
}
