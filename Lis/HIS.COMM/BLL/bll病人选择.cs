﻿using HIS.COMM.Helper;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using static HIS.COMM.ClassEnum;

namespace HIS.COMM.BLL
{
    public class bll病人选择
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        static string sqlParent = @"
                SELECT br.出生日期,cw.房间号,br.ID,br.身份证号, br.ZYID, br.住院号码, br.病人姓名,br.疾病名称,br.[疾病编码], br.入院时间,
                        bq.科室名称 病区名称,br.病床,  br.性别, dbo.fn_getage(出生日期) S年龄, br.单位编码,
                        br.在院状态, br.主治医生编码, br.[病区] , br.科室 科室编码,
                        br.[家庭地址], isnull(预交款, 0) 预交款, isnull(fy.总费用, 0) 总费用,
                        isnull(fy.[总费用], 0) - isnull(yjk.预交款, 0) 欠款, br.[分院编码],ys.用户名 主治医生姓名,ks.科室名称,br.医保类型,0 父ZYID  
                FROM   ZY病人信息 AS br
                        LEFT OUTER JOIN (select   zyid, ISNULL(SUM(交款额), 0) AS 预交款
                                        from     ZY预交款
                                        group by zyid) AS yjk
                            ON br.ZYID = yjk.ZYID
                        left outer join gy科室设置 bq on br.病区 = bq.科室编码
                        left outer join gy科室设置 ks on br.科室 = ks.科室编码
                        left outer join (select   zyid, isnull(sum(金额), 0) 总费用
                                            from     [ZY在院费用]
                                            group by zyid
                                            union all
                                            select   zyid, isnull(sum(金额), 0) 总费用
                                            from     [ZY出院费用]
                                            group by zyid
                                        ) fy
                            on br.zyid = fy.zyid  left outer join pubuser ys on br.主治医生编码=ys.[用户编码] 
                            left outer join zy床位设置 cw on cw.床位名称=br.病床 and cw.病区=br.病区
                    WHERE  1=1 _where病区数据权限 _where在院状态 _where分院";
        static string sqlChild = @"  SELECT zbr.出生日期,cw.房间号,zbr.ID,zbr.身份证号, zbr.子ZYID, zbr.住院号码, zbr.病人姓名,br.疾病名称,br.[疾病编码],br.入院时间, 
				                             bq.科室名称 病区名称, br.病床 病床名称, zbr.性别, 
				                             dbo.fn_getage(zbr.出生日期) S年龄,  br.单位编码, 
				                             br.在院状态, br.主治医生编码, br.[病区] 病区编码, 
				                             br.科室 科室编码, br.[家庭地址], isnull(预交款, 0) 预交款, 
				                             isnull(fy.总费用, 0) 总费用, isnull(fy.[总费用], 0) - isnull(yjk.预交款, 0) 欠款, br.[分院编码], 
				                             ys.用户名 主治医生姓名, ks.科室名称, br.医保类型,zbr.父ZYID 
	                            FROM   zy子病人信息 zbr 
				                             left outer join ZY病人信息 AS br on zbr.[父ZYID] = br.ZYID 
				                             LEFT OUTER JOIN (select   zyid, ISNULL(SUM(交款额), 0) AS 预交款 
													                            from     ZY预交款 
													                            group by zyid) AS yjk 
					                             ON br.ZYID = yjk.ZYID 
				                             left outer join gy科室设置 bq on br.病区 = bq.科室编码 
				                             left outer join gy科室设置 ks on br.科室 = ks.科室编码 
				                             left outer join (select   zyid, isnull(sum(金额), 0) 总费用 
													                            from     [ZY在院费用] 
													                            group by zyid) fy 
					                             on br.zyid = fy.zyid 
				                             left outer join pubuser ys on br.主治医生编码 = ys.[用户编码] 
                                             left outer join zy床位设置 cw on cw.床位名称=br.病床 and cw.病区=br.病区
	                             where 1=1 _where病区数据权限 _where在院状态 _where分院";

        public static List<Pojo病人信息> get区分科室权限含子病人单病人(string _数据权限, int _分院编码, string inpatientNumber, en在院状态 _en在院状态)
        {
            try
            {
                string _s住院号 = "";
                string _s住院次数 = "";
                string sql = "";
                if (inpatientNumber.Contains("-"))
                {
                    _s住院号 = WEISHENG.COMM.stringHelper.mySplit(inpatientNumber, '-', 0);
                    _s住院次数 = WEISHENG.COMM.stringHelper.mySplit(inpatientNumber, '-', 1);
                    sql = $"{ sqlParent} and br.住院号码={ _s住院号} and br.住院次数={_s住院次数}  ";
                }
                else
                {
                    _s住院号 = inpatientNumber;
                    sql = $"{ sqlParent} and br.住院号码={ inpatientNumber} ";
                }


                sql = sql.Replace("_where分院", $" and br.分院编码={_分院编码}");
                sql = sql.Replace("_where病区数据权限", $"and br.病区 in ( {_数据权限} )");

                sql = sql.Replace("_where在院状态", _en在院状态 == en在院状态.未定义 ? "" : $"and br.在院状态='{ _en在院状态.ToString()}'");
                var pojos = chis.Database.SqlQuery<Pojo病人信息>(sql).ToList();
                return pojos;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {

                        var errInfo = string.Format("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                              validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// 显示在床头牌上的病人信息
        /// </summary>
        /// <param name="_病区编码"></param>
        /// <param name="_b显示空床"></param>
        /// <returns></returns>
        public static List<Pojo病人信息> Get病区病人(int _病区编码, bool _b显示空床, bool _b显示已出院申请)
        {
            try
            {
                string sql = sqlParent;
                sql = sql.Replace("_where分院", $" and br.分院编码={HIS.COMM.zdInfo.Model站点信息.分院编码}");
                sql = sql.Replace("_where病区数据权限", $"and br.病区 = {_病区编码}");
                sql = sql.Replace("_where在院状态", "");// and (br.在院状态='正常' or br.在院状态='取消出院结算')    改为完全根据床位设置显示，不考虑在院状态
                sql += " and br.已出院标记=0";
                sql += " order by 病床";
                var list病人信息 = chis.Database.SqlQuery<Pojo病人信息>(sql).ToList();

                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
                {

                    var list本病区床位 = chis.ZY床位设置.Where(c => c.病区 == _病区编码).ToList();
                    foreach (var itemBed in list本病区床位)
                    {
                        if (itemBed.ZYID == null)
                        {
                            if (_b显示空床)
                            {
                                list病人信息.Add(new Pojo病人信息() { 病床 = itemBed.床位名称, 床位编码 = itemBed.床位编码, 显示顺序 = itemBed.显示顺序 });
                            }
                        }
                        else
                        {
                            var itemPatient = list病人信息.Where(c => c.病床 == itemBed.床位名称).FirstOrDefault();
                            if (itemPatient != null)
                            {
                                itemPatient.床位编码 = itemBed.床位编码;
                                if (itemPatient.在院状态 != HIS.COMM.ClassEnum.en在院状态.正常.ToString())
                                {
                                    itemPatient.显示顺序 = -1;//非  正常 病人显示在前面
                                }
                                else
                                {
                                    itemPatient.显示顺序 = itemBed.显示顺序;
                                }
                            }
                            else
                            {
                                list病人信息.Add(new Pojo病人信息() { 病床 = itemBed.床位名称, 床位编码 = itemBed.床位编码, 显示顺序 = itemBed.显示顺序 });
                            }
                        }
                    }
                }
                List<Pojo病人信息> pojos = new List<Pojo病人信息>();

                var list正常 = list病人信息.Where(c => c.在院状态 != HIS.COMM.ClassEnum.en在院状态.出院申请.ToString()).OrderBy(c => c.显示顺序).ThenBy(c => c.病床).ToList();
                var list出院申请 = list病人信息.Where(c => c.在院状态 == HIS.COMM.ClassEnum.en在院状态.出院申请.ToString()).OrderBy(c => c.显示顺序).ThenBy(c => c.病床).ToList();

                if (_b显示已出院申请)
                {
                    pojos.AddRange(list出院申请);
                }
                pojos.AddRange(list正常);
                foreach (var item in pojos)
                {
                    string _s年龄;
                    string _s年龄单位;
                    if (string.IsNullOrEmpty(item.身份证号))
                    {
                        AgeHelper.GetOutAgeAndUnitByBirthday(Convert.ToDateTime(item.出生日期), DateTime.Now, out _s年龄, out _s年龄单位);
                    }
                    else
                    {
                        AgeHelper.GetOutAgeAndUnitBySFZH(item.身份证号, out _s年龄, out _s年龄单位);
                    }
                    item.S年龄 = _s年龄;
                }
                return pojos;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {

                        var errInfo = string.Format("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                              validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Pojo病人信息> get区分科室权限含子病人(string _数据权限, int _分院编码)
        {
            try
            {
                string sql = sqlParent + " union all " + sqlChild;
                sql = sql.Replace("_where分院", $" and br.分院编码={_分院编码}");
                sql = sql.Replace("_where病区数据权限", $"and br.病区 in ( {_数据权限} )");
                sql = sql.Replace("_where在院状态", "and br.在院状态='正常'");
                var pojos = chis.Database.SqlQuery<Pojo病人信息>(sql).ToList();
                return pojos;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {

                        var errInfo = string.Format("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                              validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<Pojo病人信息> get本病区未分配床位病人(int _分院编码, int _病区编码)
        {
            try
            {
                string sql = sqlParent;
                sql = sql.Replace("_where分院", $" and br.分院编码={_分院编码} ");
                sql = sql.Replace("_where病区数据权限", "");
                sql = sql.Replace("_where在院状态", "");//and (br.在院状态='正常' or br.在院状态='取消出院结算')
                sql += " and br.已出院标记=0";
                sql = sql + $"and br.ZYID not in (select isnull(ZYID,0) from ZY床位设置 ) and br.病区={_病区编码}";
                var pojos = chis.Database.SqlQuery<Pojo病人信息>(sql).ToList();
                return pojos;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {

                        var errInfo = string.Format("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                              validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {

                throw ex;
            }




        }

        public static List<Pojo病人信息> Get出院申请病人(int _分院编码)
        {
            try
            {
                //string sql = $@"SELECT br.ID, br.ZYID, br.住院号码, br.病人姓名,br.疾病名称,br.[疾病编码], br.入院时间,
                //                   bq.科室名称 病区名称,br.病床,  br.性别, dbo.fn_getage(出生日期) S年龄, br.单位编码,
                //                   br.在院状态, br.主治医生编码, br.[病区] , br.科室 科室编码,
                //                   br.[家庭地址], isnull(预交款, 0) 预交款, isnull(fy.总费用, 0) 总费用,
                //                   isnull(fy.[总费用], 0) - isnull(yjk.预交款, 0) 欠款, br.[分院编码],ys.用户名 主治医生姓名,ks.科室名称,br.医保类型,0 父ZYID  
                //            FROM   ZY病人信息 AS br
                //                   LEFT OUTER JOIN (select   zyid, ISNULL(SUM(交款额), 0) AS 预交款
                //                                    from     ZY预交款
                //                                    group by zyid) AS yjk
                //                     ON br.ZYID = yjk.ZYID
                //                   left outer join gy科室设置 bq on br.病区 = bq.科室编码
                //                   left outer join gy科室设置 ks on br.科室 = ks.科室编码
                //                   left outer join (select   zyid, isnull(sum(金额), 0) 总费用
                //                                    from     [ZY在院费用]
                //                                    group by zyid) fy
                //                     on br.zyid = fy.zyid  left outer join pubuser ys on br.主治医生编码=ys.[用户编码] 
                //             WHERE  (br.已出院标记 = 0) and br.在院状态='出院申请' and br.分院编码= {_分院编码}";
                string sql = sqlParent;
                sql = sql.Replace("_where分院", $" and br.分院编码={_分院编码} ");
                sql = sql.Replace("_where病区数据权限", "");
                sql = sql.Replace("_where在院状态", "and br.在院状态='出院申请'");
                var pojos = chis.Database.SqlQuery<Pojo病人信息>(sql).ToList();

                return pojos;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {

                        var errInfo = string.Format("Class: {0}, Property: {1}, Error: {2}", validationErrors.Entry.Entity.GetType().FullName,
                              validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
