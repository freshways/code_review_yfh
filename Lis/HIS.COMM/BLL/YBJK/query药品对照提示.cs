﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM.BLL.YBJK
{
    public class get药品对照提示
    {
        /// <summary>
        /// 获取全部诊疗对照提示,包括诊疗项目和服务设施
        /// </summary>
        /// <returns></returns>
        public static List<Pojo药品对照提示> GetAllItems()
        {
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
            string SQL_SELECT_诊疗提示_ALL =
                                            @"SELECT     诊疗项目编码 AS 医保编码, 项目名称 AS 
                                            医保名称,  '' AS 规格, '' AS 单位, 
                                            拼音助记码, '诊疗项目' AS 类别,2 类别编码 
                                            FROM         yb_诊疗项目目录
                                            union all
                                            SELECT     药品编码, aa.中文名称+'('+isnull(bb.代码名称,'')+')' 药品名称,  规格, 
                                            单位, 拼音助记码, '药品' AS 类别,1 类别编码 
                                            FROM yb_药品目录 aa left outer join 
                                            (
                                            select * from [yb_数据字典] where [类别名称]='药品剂型'
                                            ) bb on aa.[剂型]=bb.[代码值]";
            return chis.Database.SqlQuery<Pojo药品对照提示>(SQL_SELECT_诊疗提示_ALL).ToList();
        }

        /// <summary>
        /// 获取未匹配目录
        /// </summary>
        /// <returns></returns>
        //public static List<Pojo药品对照提示> GetUnMatchItems()
        //{
        //    CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
        //    string SQL_SELECT_诊疗提示_ALL = @"select * from 
        //                                        (
        //                                        select 诊疗项目编码,	项目名称,	拼音助记码, 收费类别,	收费项目等级,'诊疗项目' 类别,2 类别编码 
        //                                        from    yb_诊疗项目目录 
        //                                        union all
        //                                        select 医疗服务设施编码,	服务设施名称,	拼音助记码,	收费类别,	收费项目等级,'服务设施' 类别 ,3 类别编码
        //                                        from yb_医疗服务设施目录
        //                                        ) aa where aa.诊疗项目编码 not in 
        //                                        (
        //                                        select distinct 医保编码 from [GY收费小项] where 医保编码 is not null
        //                                        )";
        //    return chis.Database.SqlQuery<Pojo药品对照提示>(SQL_SELECT_诊疗提示_ALL).ToList();
        //}
    }
}
