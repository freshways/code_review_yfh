﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM.BLL.YBJK
{
    public class get诊疗对照提示
    {
        /// <summary>
        /// 获取全部诊疗对照提示,包括诊疗项目和服务设施
        /// </summary>
        /// <returns></returns>
        public static List<Pojo诊疗对照提示> GetAllItems()
        {
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
            string SQL_SELECT_诊疗提示_ALL = @"select 诊疗项目编码,	项目名称,	拼音助记码, 收费类别,	收费项目等级,'诊疗项目' 类别,2 类别编码
                                                from    yb_诊疗项目目录 
                                                union all
                                                select 医疗服务设施编码,	服务设施名称,	拼音助记码,	收费类别,	收费项目等级,'服务设施' 类别,3 类别编码
                                                from yb_医疗服务设施目录";
            return chis.Database.SqlQuery<Pojo诊疗对照提示>(SQL_SELECT_诊疗提示_ALL).ToList();
        }

        /// <summary>
        /// 获取未匹配目录
        /// </summary>
        /// <returns></returns>
        public static List<Pojo诊疗对照提示> GetUnMatchItems()
        {
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
            string SQL_SELECT_诊疗提示_ALL = @"select * from 
                                                (
                                                select 诊疗项目编码,	项目名称,	拼音助记码, 收费类别,	收费项目等级,'诊疗项目' 类别,2 类别编码 
                                                from    yb_诊疗项目目录 
                                                union all
                                                select 医疗服务设施编码,	服务设施名称,	拼音助记码,	收费类别,	收费项目等级,'服务设施' 类别 ,3 类别编码
                                                from yb_医疗服务设施目录
                                                ) aa where aa.诊疗项目编码 not in 
                                                (
                                                select distinct 医保编码 from [GY收费小项] where 医保编码 is not null
                                                )";
            return chis.Database.SqlQuery<Pojo诊疗对照提示>(SQL_SELECT_诊疗提示_ALL).ToList();
        }

        //public static List<Pojo诊疗对照提示> action()
        //{
        //    CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
        //    string SQL_SELECT_诊疗提示_ALL = @"select 诊疗项目编码,	项目名称,	拼音助记码, 收费类别,	收费项目等级,'诊疗项目' 类别 ,2 类别编码
        //                                        from    yb_诊疗项目目录 
        //                                        union all
        //                                        select 医疗服务设施编码,	服务设施名称,	拼音助记码,	收费类别,	收费项目等级,'服务设施' 类别 ,3 类别编码
        //                                        from yb_医疗服务设施目录";
        //    return chis.Database.SqlQuery<Pojo诊疗对照提示>(SQL_SELECT_诊疗提示_ALL).ToList();
        //}
    }
}
