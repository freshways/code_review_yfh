﻿using System;

namespace HIS.COMM.BLL.AdministratorMaintenance
{
    public class PriceSettings
    {
        /// <summary>
        /// 同步收费项目（大类、小类、已经发生的费用）
        /// </summary>
        /// <returns></returns>
        public static bool syncSFXM(string type)
        {
            try
            {
                //同步gy收费小项  归并名称
                string sql1 =
                     "update aa set aa.归并名称=bb.项目名称" + "\r\n" +
                    "from gy收费小项 aa left outer join [GY收费大项] bb on aa.[归并编码]=bb.[项目编码]" + "\r\n" +
                    "where isnull(aa.[归并名称],'')<>isnull(bb.[项目名称],'')";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sql1);

                //同步zy在院费用  项目编码
                string sql2 = " update aa set aa.项目编码=bb.归并编码 " +
                 " from ZY出院费用 aa left outer join GY收费小项 bb on substring(aa.费用名称,0,case when charindex('/',aa.费用名称,0)=0  " +
                 " then 100 else charindex('/',费用名称,0) end) =bb.收费名称 " +
                 " where aa.YPID=0 and aa.项目编码<>bb.归并编码 ";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sql2);

                //同步zy在院费用  项目编码
                string sql3 =
               " update aa set aa.项目编码=bb.归并编码 " +
               " from ZY在院费用 aa left outer join GY收费小项 bb on substring(aa.费用名称,0,case when charindex('/',aa.费用名称,0)=0  " +
               " then 100 else charindex('/',费用名称,0) end) =bb.收费名称 " +
               " where aa.YPID=0 and aa.项目编码<>bb.归并编码 ";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sql3);

                //如果新版标准和卫生局不一致，同步收费名称
                string sql4 =
                " update aa " +
               " set aa.收费名称=bb.收费名称,aa.归并编码=bb.归并编码,aa.归并名称=bb.归并名称 " +
               " from GY收费小项 aa left outer join hispub.dbo.GY收费小项 bb " +
               " on aa.收费编码=bb.收费编码 " +
               " where aa.收费编码>=10000 and aa.收费名称<>bb.收费名称 ";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sql4);

                // 处理门诊收费明细里面的小项编码
                string sql5 = " update bb set bb.收费编码=aa.收费编码 from GY收费小项 aa left outer join MF门诊明细 bb on aa.收费名称=bb.收费名称 where aa.收费编码<>bb.收费编码";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sql5);

                //   自动禁用老收费项目，暂时先不启用
                //   update GY收费小项 set 是否禁用=1 where 收费编码<10000 and 是否禁用=0

                return true;
            }
            catch (Exception ex)
            {
                HIS.COMM.msgBalloonHelper.ShowInformation(ex.Message);
                return false;
            }


        }


        public static bool syncSFXM()
        {
            try
            {
                //同步gy收费小项  归并名称
                string sql1 = @"update aa set aa.归并名称=bb.项目名称 from GY收费小项 aa left outer join GY收费大项 bb on aa.归并编码=bb.项目编码 where aa.归并名称<>bb.项目名称";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sql1);
                return true;
            }
            catch (Exception ex)
            {
                HIS.COMM.msgBalloonHelper.ShowInformation(ex.Message);
                return false;
            }


        }
        /// <summary>
        /// 同步收费项目价格设置到 床位自动记费设置
        /// </summary>
        /// <returns></returns>
        public static bool SyncBedsAutomaticallyRememberFeePrice()
        {

            try
            {
                string sql1 =
                        @"update aa 
                        set aa.单价=bb.单价,aa.金额=bb.单价*aa.数量
                        from [ZY床位自动计费] aa left outer join [GY收费小项] bb on aa.[收费编码]=bb.收费编码 and aa.单价<>bb.单价
                        where bb.id is not null";
                string sql2 = "update aa set aa.自动计费金额=(select sum(金额) from ZY床位自动计费 bb where bb.[床位编码]=aa.[床位编码]) from [ZY床位设置] aa";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sql1);
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sql2);
                return true;
            }
            catch (Exception ex)
            {
                HIS.COMM.msgBalloonHelper.ShowInformation(ex.Message);
                return false;
            }

        }
    }
}
