﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM.BLL
{
    public class Query医生
    {
        public static IList<model医生> action(string where)
        {
            string sql = string.Format(@"SELECT 用户编码 AS 医生编码,
                                        用户名 医生姓名,
                                        拼音代码,   
                                        拼音代码 + '|' + 用户名 医生助记码,
                                        convert(int, 科室编码) 科室编码,
                                        科室名称
                                        FROM pubUser
                                        WHERE  是否有处方权 = 1 AND 是否禁用 = 0");
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
            return chis.Database.SqlQuery<model医生>(sql).ToList();
        }
    }

}