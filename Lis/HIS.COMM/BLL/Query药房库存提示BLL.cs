﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM.BLL
{
    public  class Query药房库存提示BLL
    {

        public static IList<Query药房库存提示Model> Query药房库存提示Models(string _s药房编码, HIS.COMM.ClassCflx.enCflx _en处方类型)
        {

            string _s筛选条件 = "";
            switch (_en处方类型)
            {
                case COMM.ClassCflx.enCflx.西药方:
                    _s筛选条件 = " and 财务分类 <> '中药费'";
                    break;
                case COMM.ClassCflx.enCflx.中药方:
                    _s筛选条件 = " and 财务分类 = '中药费'";
                    break;
                default:
                    _s筛选条件 = " and 财务分类 <> '中药费'";
                    break;
            }
            string sql = string.Format(@"   with dd as (select   YPID, SUM(处方总量) 处方总量
						                                                from     MF处方明细
						                                                where    CFID in
												                                            (select CFID
												                                                from   mf处方摘要 aa
																                                            left outer join mf门诊摘要 bb on aa.mzid = bb.mzid and aa.单位编码 = bb.单位编码
												                                                where  aa.单位编码 = {0}  and aa.药房编码 = {1}
		                                                and 已发药标记 = 0 and aa.作废标记 = 0 and aa.[开方时间] between convert(datetime,convert(varchar(10),getdate(),120)) 
		                                                and convert(datetime,convert(varchar,getdate(),112),112)+1-1.0/3600/24)
						                                                group by YPID),
				                                                ee
				                                                as (select   ypid, SUM(数量) 处方总量
						                                                from     ZY在院费用
						                                                where    YPID <> 0 and 已发药标记 = 0 and 单位编码 ={0} and 药房编码 = {1}
						                                                group by YPID)
		                                            SELECT  aa.药房编码, bb.财务分类,
						                                                aa.药品序号, aa.药品名称,
						                                                aa.药房规格, aa.药房单位,
						                                                aa.药品产地, aa.药品批号,
						                                                aa.药品效期, (aa.库存数量 - isnull(dd.处方总量, 0) - ISNULL(ee.处方总量, 0)) 库存数量,
						                                                isnull(aa.进价, 0) 进价, isnull(aa.医院零售价, 0) 医院零售价,
						                                                isnull(aa.卫生室零售价, 0) 卫生室零售价, aa.YPID,
							                                            bb.[默认频次],
						                                                bb.默认用法, isnull(bb.药品剂量,0) 药品剂量,
						                                                bb.剂量单位,bb.拼音代码
		                                            FROM     YF库存信息 AS aa
						                                                LEFT OUTER JOIN YK药品信息 AS bb ON aa.药品序号 = bb.药品序号 and aa.单位编码 = bb.单位编码
						                                                left outer join dd on aa.ypid = dd.ypid
						                                                left outer join ee on aa.YPID = ee.YPID
		                                            where    aa.单位编码 =  {0}  and aa.药房编码 =  {1}  {2} and (aa.库存数量 - isnull(dd.处方总量, 0)
						                                                - ISNULL(ee.处方总量, 0))> 0
		                                            order by 药品名称, 药品效期; ", HIS.COMM.zdInfo.Model单位信息.iDwid, _s药房编码, _s筛选条件);

            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));

            return chis.Database.SqlQuery<Query药房库存提示Model>(sql).ToList();
        }
    }
}
