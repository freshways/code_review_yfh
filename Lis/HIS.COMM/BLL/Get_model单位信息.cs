﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Linq;

namespace HIS.COMM.BLL
{
    public class Get_model单位信息
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
        public static model单位信息 Action(string _mac)
        {
            model单位信息 model单位 = new model单位信息()
            {
                sDwmc = Helper.EnvironmentHelper.CommStringGet("医院名称", "", ""),
                iDwid = Convert.ToInt32(Helper.EnvironmentHelper.CommStringGet("医院编码", "0", "")),
                S组织机构代码 = Helper.EnvironmentHelper.CommStringGet("组织机构代码", "", ""),
                WxAppID = Helper.EnvironmentHelper.CommStringGet("微信AppID", "", ""),
                Mch_ID = Helper.EnvironmentHelper.CommStringGet("微信Mch_ID", "", ""),
                S医保单位编码 = Helper.EnvironmentHelper.CommStringGet("医保单位编码", "", ""),
            };
            if (chis.GY测试站点.Where(c => c.MAC == _mac).FirstOrDefault() != null)
            {
                model单位.sDwmc = "测试单位";
            }
            return model单位;
        }
    }
}
