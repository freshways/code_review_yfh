﻿using HIS.COMM;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace HIS.YKGL.BLL
{
    class bll新增品种
    {
         CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public YK药品信息 action(string _s条码信息,List<山东省直药品目录库20190702> list_SD_BWM,List<PojoStandardizationDrugs> list_GB_YPID)
        {
           
            decimal newID = chis.YK药品信息.Max(c => c.药品序号) + 1;

            YK药品信息 modelNew = new YK药品信息();
            modelNew.单位编码 = WEISHENG.COMM.zdInfo.Model单位信息.iDwid;
            modelNew.药品序号 = newID;
            modelNew.药品名称 = "新增品种";
            modelNew.药库规格 = "";
            modelNew.药房规格 = "";
            modelNew.药库单位 = "";
            modelNew.药房单位 = "";
            modelNew.药品产地 = "";
            modelNew.包装转换 = 1;
            modelNew.拼音代码 = "";
            modelNew.是否禁用 = false;
            modelNew.CreateTime = DateTime.Now;

            XtraForm品种编辑 dia = new XtraForm品种编辑(modelNew, _s条码信息, false, list_SD_BWM,list_GB_YPID );
            if (dia.ShowDialog() == DialogResult.OK)
            {

                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
                {
                    chis.YK药品信息.Attach(modelNew);
                    chis.Entry(modelNew).State = System.Data.Entity.EntityState.Added;
                    if (dia.model条码 != null)
                    {
                        chis.YK条码库.Attach(dia.model条码);//更新HIS的序号到条码库
                        chis.Entry(dia.model条码).State = System.Data.Entity.EntityState.Modified;
                    }
                    chis.SaveChanges();
                    WEISHENG.COMM.msgHelper.ShowInformation("新增成功");
                    return modelNew;                 
                }               
            }
            else
            {
                return null;
            }

        }
    }
}
