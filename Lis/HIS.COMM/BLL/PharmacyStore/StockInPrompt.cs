﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.BLL.PharmacyStore
{
    public class StockInPrompt
    {
        public static DataTable GetData(bool b启用仓库权限设置, string s仓库编码)
        {
            string str = "";
            if (b启用仓库权限设置)
            {
                str = "and aa.财务分类 = '" + s仓库编码 + "' ";
            }
            string SQL =
           @"with bb as
            (
            select * from yk库存信息 where YPID in
            (
	        select distinct ypid from (
				    select max(ypid) ypid from yk库存信息  group by 药品序号 
				    union all
				    select max(ypid) from yk库存信息 where 库存数量>0  group by 药品序号 
				    ) temp
            )
            )
            SELECT aa.药品序号, aa.药品名称, aa.药库规格,
                   aa.药库单位, bb.库存数量, isnull(bb.医院零售价, 0) 医院零售价,
                   isnull(bb.卫生室零售价, 0) 卫生室零售价, aa.药品产地, bb.药品批号,
                   aa.拼音代码, isnull(bb.进价, 0) 进价, isnull(cc.加价率, 1) 加价率,
                   aa.包装转换, isnull(bb.ypid, 0) YPID
            FROM   YK药品信息 aa
                   LEFT OUTER JOIN bb ON aa.药品序号 = bb.药品序号
                   left outer join YK财务分类 cc on aa.财务分类 = cc.财务分类名称
            where  isnull(aa.是否禁用, 0) = 0 " + str;
            return HIS.Model.Dal.SqlHelper.ExecuteDataTable(SQL);
            //"SELECT  aa.药品序号,aa.药品名称, aa.药库规格, aa.药库单位, bb.库存数量,  " +
            //           "isnull(bb.医院零售价,0) 医院零售价, isnull(bb.卫生室零售价,0) 卫生室零售价, aa.药品产地, bb.药品批号,  " +
            //           "aa.拼音代码, isnull(bb.进价,0) 进价,isnull(cc.加价率,1) 加价率,aa.包装转换,  isnull(bb.ypid,0) YPID " +
            //           "FROM         YK药品信息 aa LEFT OUTER JOIN " +
            //           "yk库存信息 bb ON aa.药品序号 = bb.药品序号 " +
            //           "left outer join YK财务分类 cc on aa.财务分类 =cc.财务分类名称 " +
            //           "where isnull(aa.是否禁用,0)=0 "
        }



        public static List<model药库入库提示> GetModels(string sqlWhere)
        {
            string sql = @"with bb as
                   (
                   select * from yk库存信息 where YPID in
                   (
                   select max(ypid) from yk库存信息 group by 药品序号 
                   )
                   )
                   SELECT aa.药品序号, aa.药品名称, aa.药库规格,
                           aa.药库单位, bb.库存数量, isnull(bb.医院零售价, 0) 医院零售价,
                           isnull(bb.卫生室零售价, 0) 卫生室零售价, aa.药品产地, bb.药品批号,
                           aa.拼音代码, isnull(bb.进价, 0) 进价, isnull(cc.加价率, 1) 加价率,
                           aa.包装转换, isnull(bb.ypid, 0) YPID
                   FROM   YK药品信息 aa
                           LEFT OUTER JOIN bb ON aa.药品序号 = bb.药品序号
                           left outer join YK财务分类 cc on aa.财务分类 = cc.财务分类名称
                   where  isnull(aa.是否禁用, 0) = 0 ";
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb));
            return chis.Database.SqlQuery<model药库入库提示>(sql).ToList();
        }
    }
}

