﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM.BLL
{
    /// <summary>
    /// 住院天数计算
    /// </summary>
    public class CountHospitalizationDays
    {
        public enum enCountDaysType
        {
            最小天数,
            最大天数
        }
        public static Int32 iCountDays(DateTime _sBeginDay, DateTime _sEndDay, enCountDaysType _enType)
        {

            int zyts = 1;
            if (Convert.ToDateTime(_sEndDay) < Convert.ToDateTime(_sBeginDay))
            {
                return zyts;
            }
            TimeSpan ts = Convert.ToDateTime(_sEndDay) - Convert.ToDateTime(_sBeginDay);
            if (ts.Days == 0)
            {
                zyts = 1;
            }
            else
            {
                int zsts = ts.Days;//整数天数
                zyts = zsts;
                if (_enType == enCountDaysType.最大天数)
                {
                    if (ts.Hours > 0)//如果超过一个24小时单位，就增加一天
                    {
                        zyts = zyts + 1;
                    }
                }
            }
            return zyts;
        }
    }
}
