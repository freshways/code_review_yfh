﻿using DevExpress.XtraEditors;
using HIS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HIS.COMM.BLL
{
    public class bll住院病人状态
    {

        public static bool b取消出院申请(decimal sZYID)
        {
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                try
                {
                    var zy病人信息 = chis.ZY病人信息.Where(c => c.ZYID == sZYID).FirstOrDefault();
                    if (zy病人信息.在院状态 != HIS.COMM.ClassEnum.en在院状态.出院申请.ToString() && zy病人信息.在院状态 != HIS.COMM.ClassEnum.en在院状态.取消出院结算.ToString())
                    {
                        XtraMessageBox.Show($"当前病人在院状态{zy病人信息.在院状态},您不能对此病人进行取消申请操作", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }                 
                    zy病人信息.在院状态 = HIS.COMM.ClassEnum.en在院状态.正常.ToString();
                    zy病人信息.出院申请时间 = null;
                    chis.ZY病人信息.Attach(zy病人信息);
                    chis.Entry(zy病人信息).Property(c => c.在院状态).IsModified = true;
                    chis.Entry(zy病人信息).Property(c => c.出院申请时间).IsModified = true;

                    //如果床位是空的，恢复到原床位，否则，提示重新分配床位
                    string tips = "取消出院申请操作成功。";
                    var zy床位设置 = chis.ZY床位设置.Where(c => c.床位名称 == zy病人信息.病床 && c.病区 == zy病人信息.病区).FirstOrDefault();

                    if (zy床位设置.ZYID == null)
                    {
                        zy床位设置.ZYID = zy病人信息.ZYID;
                        tips += $"\r\n自动重新分配到床位【{zy床位设置.床位名称}】成功。";
                        chis.ZY床位设置.Attach(zy床位设置);
                        chis.Entry(zy床位设置).Property(c => c.ZYID).IsModified = true;
                    }
                    else
                    {
                        tips += $"\r\n自动重新分配到床位【{zy床位设置.床位名称}】失败，请手工重新分配其他床位。";
                    }
                    chis.SaveChanges();
                    WEISHENG.COMM.LogHelper.Info("", "取消出院申请", sZYID + "|" + zy病人信息.病人姓名 + "|" + tips);
                    XtraMessageBox.Show(tips, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return true;
                }
                catch (Exception ee)
                {
                    XtraMessageBox.Show("取消出院申请操作失败" + ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return true;
                }
            }

        }
        public static bool b出院申请(decimal sZYID)
        {
            try
            {
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
                {
                    var item = chis.ZY病人信息.Where(c => c.ZYID == sZYID).FirstOrDefault();
                    decimal dec未发药金额 = HIS.COMM.Person住院病人.get未发药金额(sZYID.ToString());
                    if (dec未发药金额 != 0)
                    {
                        XtraMessageBox.Show("此病人还有未发药金额:" + dec未发药金额.ToString() + "\r\n请与药房工作人员核对后再出院申请!", "提示",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                    if (item.在院状态 == HIS.COMM.ClassEnum.en在院状态.正常.ToString() || item.在院状态 == HIS.COMM.ClassEnum.en在院状态.取消出院结算.ToString())
                    {
                               //清空病人床位，不在进行此病人自动计费
                        HIS.Model.Dal.SqlHelper.ExecuteNonQuery("update zy床位设置 set zyid=null where zyid=" + sZYID);

                        string sqlCmd = "update zy病人信息 set 在院状态='" + HIS.COMM.ClassEnum.en在院状态.出院申请.ToString() + "',出院申请时间=getdate() where zyid=" + sZYID;
                        int re = HIS.Model.Dal.SqlHelper.ExecuteNonQuery(sqlCmd);
                        WEISHENG.COMM.LogHelper.Info("", "申请出院", sZYID + "|" + item.病人姓名);

                        XtraMessageBox.Show("出院申请操作成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return true;

                    }
                    else
                    {
                        XtraMessageBox.Show("您不能对此病人进行此操作", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            catch (Exception ee)
            {
                XtraMessageBox.Show("出院申请操作失败" + ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }
    }
}
