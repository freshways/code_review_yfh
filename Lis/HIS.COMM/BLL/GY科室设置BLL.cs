﻿using System;
using System.Collections.Generic;
using HIS.COMM.Model;
using HIS.COMM.IDAL;
using HIS.COMM.DALFactory;

namespace HIS.COMM.BLL
{
    /// <summary>
    /// BLL Layer For dbo.GY科室设置.
    /// </summary>
    public partial class GY科室设置BLL
    {
        public static readonly IGY科室设置 dal = DataAccess.CreateGY科室设置();
        #region ----------构造函数----------
        /// <summary>
        /// 构造函数
        /// </summary>
        public GY科室设置BLL()
        {
        }
        #endregion

        #region ----------函数定义----------
        #region 添加更新删除
        /// <summary>
        /// 向数据库中插入一条新记录。
        /// </summary>
        /// <param name="gy科室设置">GY科室设置??</param>
        /// <returns>新插入记录的编号</returns>
        public int Insert(GY科室设置Model gy科室设置)
        {
            // Validate input
            if (gy科室设置 == null) return 0;
            // Use the dal to insert a new record 
            return dal.Insert(gy科室设置);
        }

        /// <summary>
        /// 向数据表GY科室设置更新一条记录。
        /// </summary>
        /// <param name="gy科室设置">gy科室设置</param>
        /// <returns>影响的行数</returns>
        public int Update(GY科室设置Model gy科室设置)
        {
            // Validate input
            if (gy科室设置 == null) return -1;
            // Use the dal to update a new record 
            return dal.Update(gy科室设置);
        }

        /// <summary>
        /// 删除数据表GY科室设置中的一条记录
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>影响的行数</returns>
        public int Delete(int id)
        {
            // Validate input
            if (id < 0) return 0;
            return dal.Delete(id);
        }
        #endregion

        /// <summary>
        /// 得到 gy科室设置 数据实体
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>gy科室设置 数据实体</returns>
        public GY科室设置Model GetGY科室设置(int id)
        {
            // Validate input
            if (id < 0) return null;

            // Use the dal to get a record 
            return dal.GetGY科室设置(id);
        }

        /// <summary>
        /// 得到数据表GY科室设置所有记录
        /// </summary>
        /// <returns>实体集</returns>
        public IList<GY科室设置Model> GetGY科室设置All()
        {
            return dal.GetGY科室设置All();
        }

        /// <summary>
        /// 得到数据表GY科室设置符合条件的所有记录
        /// </summary>
        /// <returns>实体集</returns>
        public IList<GY科室设置Model> GetGY科室设置All(string sqlWhere)
        {
            return dal.GetGY科室设置All(sqlWhere);
        }

        public IList<GY科室设置Model> Get药房权限ByUserID()
        {
            return new SqlServerDAL.GY科室设置DAL().Get药房权限ByUserID();
        }

        /// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
        public bool IsExist(int id)
        {
            return dal.IsExist(id);
        }

        #endregion		
    }
}

