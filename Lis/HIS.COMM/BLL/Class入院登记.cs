﻿using HIS.Model;
using System;
using System.Data;


namespace HIS.COMM.BLL
{
    public static class Class入院登记
    {
        /// <summary>
        /// 生成ZYID
        /// </summary>
        /// <returns></returns>
        public static Int32 makeNew_sZYID(string _dwid)
        {
            Int32 s父ZYID = Convert.ToInt32(HIS.Model.Dal.SqlHelper.ExecuteScalar("select ISNULL(MAX(zyid),0)+1 newzyid from zy病人信息 " ));
            Int32 s子ZYID = Convert.ToInt32(HIS.Model.Dal.SqlHelper.ExecuteScalar("select ISNULL(MAX(子zyid),0)+1 newzyid from zy子病人信息"));
            if (Convert.ToInt32(s父ZYID) >= Convert.ToInt32(s子ZYID))
            {
                return s父ZYID;
            }
            else
            {
                return s子ZYID;
            }

        }


        public static bool b住院号是否可用(ZY病人信息 _person住院病人)
        {
            string strQuery = "select count(*) from zy病人信息 where 住院次数=" + _person住院病人.住院次数 + " and 住院号码=" + _person住院病人.住院号码;
            int iRe = Convert.ToInt16(HIS.Model.Dal.SqlHelper.ExecuteScalar(strQuery));
            if (iRe == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
