﻿using System;
using System.Collections.Generic;
using HIS.COMM.Model;
using HIS.COMM.IDAL;
using HIS.COMM.DALFactory;

namespace HIS.COMM.BLL
{
    /// <summary>
    /// BLL Layer For dbo.GY全局参数.
    /// </summary>
    public partial class GY全局参数BLL
    {
		public static readonly IGY全局参数 dal = DataAccess.CreateGY全局参数();		
		#region ----------构造函数----------
		/// <summary>
		/// 构造函数
		/// </summary>
		public GY全局参数BLL()
		{
		}
		#endregion

        #region ----------函数定义----------
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="gy全局参数">GY全局参数??</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(GY全局参数Model gy全局参数)
		{
			// Validate input
			if (gy全局参数 == null) return 0;
			// Use the dal to insert a new record 
			return dal.Insert(gy全局参数);
		}
		
		/// <summary>
		/// 向数据表GY全局参数更新一条记录。
		/// </summary>
		/// <param name="gy全局参数">gy全局参数</param>
		/// <returns>影响的行数</returns>
		public int Update(GY全局参数Model gy全局参数)
		{
            // Validate input
			if (gy全局参数==null) return -1;
			// Use the dal to update a new record 
			return dal.Update(gy全局参数);
		}
		
		/// <summary>
		/// 删除数据表GY全局参数中的一条记录
		/// </summary>
	    /// <param name="id">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			// Validate input
			if(id<0)	return 0;
			return dal.Delete(id);
		}
		#endregion
		
        /// <summary>
		/// 得到 gy全局参数 数据实体
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>gy全局参数 数据实体</returns>
		public GY全局参数Model GetGY全局参数(int id)
		{
			// Validate input
			if(id<0)	return null;

			// Use the dal to get a record 
			return dal.GetGY全局参数(id);
		}
		
		/// <summary>
		/// 得到数据表GY全局参数所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<GY全局参数Model> GetGY全局参数All()
		{
			return dal.GetGY全局参数All();
		}
		
		/// <summary>
		/// 得到数据表GY全局参数符合条件的所有记录
		/// </summary>
		/// <returns>实体集</returns>
		public IList<GY全局参数Model> GetGY全局参数All(string sqlWhere)
		{
			return dal.GetGY全局参数All(sqlWhere);
		}				
		
		/// <summary>
        /// 检测是否存在根据主键
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{
			return dal.IsExist(id);
		}

        #endregion		
    }
}

