﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HIS.COMM
{
    public class Class发票结构
    {
           public enum b是否重印
        {
            是,
            否
        }
        public enum en门诊发票打印模式
        {
            老格式_摘要,//默认格式,老发票格式，打印摘要
            老格式_明细,
            新格式_明细,//药品打印明细，化验等打印明细
            新格式_摘要,//药品打印明细,化验打印摘要
            药店收据
        }
        public enum en住院发票打印模式
        {
            老格式,//默认
            新格式
        }

        public struct sct发票格式
        {
            public string s病人姓名;
            public string s打印日期;
            public string s流水号;
            public string s制单人;
            public string s收款人;
            public DataTable dtPrint;
            public string s报销信息;
            public Int64 i当前发票号;

            //新格式
            public string s医疗机构类型;
            public string s校验码;

            private ClassEnum.en收款类型 en医保类型;
            public ClassEnum.en收款类型 En医保类型
            {
                get { return en医保类型; }
                set { en医保类型 = value; }
            }
            // public string s医保类型;

            public string s社会保障号码;
            public string s合计大写;
            public string s合计小写;
            public string s医保统筹支付;
            public string s个人账户支付;
            public string s其他医保支付;
            public string s个人支付金额;
            public string s收款单位;
            public string s性别;
            public string s年月日;
            //住院发票独有
            public string s病历号;
            public string s住院时间_起;
            public string s住院时间_止;
            public string s住院天数;
            public string s住院号;
            public string s预缴金额;
            public string s补缴金额;
            public string s退费金额;

        }

    }
}
