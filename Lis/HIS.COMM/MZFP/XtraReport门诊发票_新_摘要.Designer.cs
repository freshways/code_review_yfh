﻿namespace HIS.COMM
{
    partial class XtraReport门诊发票_新_摘要
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.label姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.label流水号 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel社会保障号码 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医保类型 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel校验码 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医疗机构类型 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel合计大写 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel合计小写 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医保统筹支付 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel个人账户支付 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel其他医保支付 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel个人支付金额 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel年月日 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel收款人 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel收款单位 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel报销信息 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 58.42004F;
            this.Detail.MultiColumn.ColumnCount = 2;
            this.Detail.MultiColumn.ColumnSpacing = 1F;
            this.Detail.MultiColumn.ColumnWidth = 870F;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt门诊打印格式.收费金额")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(600.9792F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(254F, 58.42F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.WordWrap = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt门诊打印格式.数量")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(500.4375F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(100.5417F, 58.42F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "xrLabel2";
            this.xrLabel2.WordWrap = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt门诊打印格式.收费名称")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(48F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(452.4375F, 58.42F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.WordWrap = false;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // label姓名
            // 
            this.label姓名.BackColor = System.Drawing.Color.White;
            this.label姓名.BorderColor = System.Drawing.Color.Black;
            this.label姓名.CanGrow = false;
            this.label姓名.Dpi = 254F;
            this.label姓名.Font = new System.Drawing.Font("宋体", 9F);
            this.label姓名.ForeColor = System.Drawing.Color.Black;
            this.label姓名.LocationFloat = new DevExpress.Utils.PointFloat(153.4583F, 267.2108F);
            this.label姓名.Name = "label姓名";
            this.label姓名.SizeF = new System.Drawing.SizeF(391.429F, 45.56F);
            this.label姓名.StylePriority.UseFont = false;
            this.label姓名.Text = "姓名";
            this.label姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // label流水号
            // 
            this.label流水号.BackColor = System.Drawing.Color.White;
            this.label流水号.BorderColor = System.Drawing.Color.Black;
            this.label流水号.CanGrow = false;
            this.label流水号.Dpi = 254F;
            this.label流水号.Font = new System.Drawing.Font("宋体", 9F);
            this.label流水号.ForeColor = System.Drawing.Color.Black;
            this.label流水号.LocationFloat = new DevExpress.Utils.PointFloat(196.1666F, 201.375F);
            this.label流水号.Name = "label流水号";
            this.label流水号.SizeF = new System.Drawing.SizeF(350.52F, 35.56F);
            this.label流水号.StylePriority.UseFont = false;
            this.label流水号.Text = "流水号";
            this.label流水号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel社会保障号码,
            this.xrLabel医保类型,
            this.xrLabel校验码,
            this.xrLabel医疗机构类型,
            this.xrLabel性别,
            this.label流水号,
            this.label姓名});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 414.2083F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel社会保障号码
            // 
            this.xrLabel社会保障号码.BackColor = System.Drawing.Color.White;
            this.xrLabel社会保障号码.BorderColor = System.Drawing.Color.Black;
            this.xrLabel社会保障号码.CanGrow = false;
            this.xrLabel社会保障号码.Dpi = 254F;
            this.xrLabel社会保障号码.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel社会保障号码.ForeColor = System.Drawing.Color.Black;
            this.xrLabel社会保障号码.LocationFloat = new DevExpress.Utils.PointFloat(1275.666F, 267.2108F);
            this.xrLabel社会保障号码.Name = "xrLabel社会保障号码";
            this.xrLabel社会保障号码.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel社会保障号码.StylePriority.UseFont = false;
            this.xrLabel社会保障号码.Text = "社会保障号码";
            this.xrLabel社会保障号码.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel医保类型
            // 
            this.xrLabel医保类型.BackColor = System.Drawing.Color.White;
            this.xrLabel医保类型.BorderColor = System.Drawing.Color.Black;
            this.xrLabel医保类型.CanGrow = false;
            this.xrLabel医保类型.Dpi = 254F;
            this.xrLabel医保类型.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel医保类型.ForeColor = System.Drawing.Color.Black;
            this.xrLabel医保类型.LocationFloat = new DevExpress.Utils.PointFloat(823.2292F, 267.2108F);
            this.xrLabel医保类型.Name = "xrLabel医保类型";
            this.xrLabel医保类型.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel医保类型.StylePriority.UseFont = false;
            this.xrLabel医保类型.Text = "城镇职工";
            this.xrLabel医保类型.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel校验码
            // 
            this.xrLabel校验码.BackColor = System.Drawing.Color.White;
            this.xrLabel校验码.BorderColor = System.Drawing.Color.Black;
            this.xrLabel校验码.CanGrow = false;
            this.xrLabel校验码.Dpi = 254F;
            this.xrLabel校验码.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel校验码.ForeColor = System.Drawing.Color.Black;
            this.xrLabel校验码.LocationFloat = new DevExpress.Utils.PointFloat(1288.896F, 201.375F);
            this.xrLabel校验码.Name = "xrLabel校验码";
            this.xrLabel校验码.SizeF = new System.Drawing.SizeF(350.52F, 35.56F);
            this.xrLabel校验码.StylePriority.UseFont = false;
            this.xrLabel校验码.Text = "校验码";
            this.xrLabel校验码.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel医疗机构类型
            // 
            this.xrLabel医疗机构类型.BackColor = System.Drawing.Color.White;
            this.xrLabel医疗机构类型.BorderColor = System.Drawing.Color.Black;
            this.xrLabel医疗机构类型.CanGrow = false;
            this.xrLabel医疗机构类型.Dpi = 254F;
            this.xrLabel医疗机构类型.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel医疗机构类型.ForeColor = System.Drawing.Color.Black;
            this.xrLabel医疗机构类型.LocationFloat = new DevExpress.Utils.PointFloat(562.5616F, 201.375F);
            this.xrLabel医疗机构类型.Name = "xrLabel医疗机构类型";
            this.xrLabel医疗机构类型.SizeF = new System.Drawing.SizeF(350.52F, 35.56F);
            this.xrLabel医疗机构类型.StylePriority.UseFont = false;
            this.xrLabel医疗机构类型.Text = "医疗机构类型";
            this.xrLabel医疗机构类型.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.BackColor = System.Drawing.Color.White;
            this.xrLabel性别.BorderColor = System.Drawing.Color.Black;
            this.xrLabel性别.CanGrow = false;
            this.xrLabel性别.Dpi = 254F;
            this.xrLabel性别.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel性别.ForeColor = System.Drawing.Color.Black;
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(562.5616F, 267.2108F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(75.88254F, 45.56F);
            this.xrLabel性别.StylePriority.UseFont = false;
            this.xrLabel性别.Text = "性别";
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel合计大写
            // 
            this.xrLabel合计大写.BackColor = System.Drawing.Color.White;
            this.xrLabel合计大写.BorderColor = System.Drawing.Color.Black;
            this.xrLabel合计大写.CanGrow = false;
            this.xrLabel合计大写.Dpi = 254F;
            this.xrLabel合计大写.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel合计大写.ForeColor = System.Drawing.Color.Black;
            this.xrLabel合计大写.LocationFloat = new DevExpress.Utils.PointFloat(219.6041F, 5.291667F);
            this.xrLabel合计大写.Name = "xrLabel合计大写";
            this.xrLabel合计大写.SizeF = new System.Drawing.SizeF(491.2785F, 45.56001F);
            this.xrLabel合计大写.StylePriority.UseFont = false;
            this.xrLabel合计大写.Text = "合计大写";
            this.xrLabel合计大写.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel合计大写.Visible = false;
            // 
            // xrLabel合计小写
            // 
            this.xrLabel合计小写.BackColor = System.Drawing.Color.White;
            this.xrLabel合计小写.BorderColor = System.Drawing.Color.Black;
            this.xrLabel合计小写.CanGrow = false;
            this.xrLabel合计小写.Dpi = 254F;
            this.xrLabel合计小写.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel合计小写.ForeColor = System.Drawing.Color.Black;
            this.xrLabel合计小写.LocationFloat = new DevExpress.Utils.PointFloat(1314.979F, 5.291667F);
            this.xrLabel合计小写.Name = "xrLabel合计小写";
            this.xrLabel合计小写.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel合计小写.StylePriority.UseFont = false;
            this.xrLabel合计小写.Text = "合计小写";
            this.xrLabel合计小写.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel合计小写.Visible = false;
            // 
            // xrLabel医保统筹支付
            // 
            this.xrLabel医保统筹支付.BackColor = System.Drawing.Color.White;
            this.xrLabel医保统筹支付.BorderColor = System.Drawing.Color.Black;
            this.xrLabel医保统筹支付.CanGrow = false;
            this.xrLabel医保统筹支付.Dpi = 254F;
            this.xrLabel医保统筹支付.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel医保统筹支付.ForeColor = System.Drawing.Color.Black;
            this.xrLabel医保统筹支付.LocationFloat = new DevExpress.Utils.PointFloat(153.4583F, 60.85417F);
            this.xrLabel医保统筹支付.Name = "xrLabel医保统筹支付";
            this.xrLabel医保统筹支付.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel医保统筹支付.StylePriority.UseFont = false;
            this.xrLabel医保统筹支付.Text = "医保统筹支付";
            this.xrLabel医保统筹支付.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel个人账户支付
            // 
            this.xrLabel个人账户支付.BackColor = System.Drawing.Color.White;
            this.xrLabel个人账户支付.BorderColor = System.Drawing.Color.Black;
            this.xrLabel个人账户支付.CanGrow = false;
            this.xrLabel个人账户支付.Dpi = 254F;
            this.xrLabel个人账户支付.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel个人账户支付.ForeColor = System.Drawing.Color.Black;
            this.xrLabel个人账户支付.LocationFloat = new DevExpress.Utils.PointFloat(563.5625F, 60.85417F);
            this.xrLabel个人账户支付.Name = "xrLabel个人账户支付";
            this.xrLabel个人账户支付.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel个人账户支付.StylePriority.UseFont = false;
            this.xrLabel个人账户支付.Text = "个人账户支付";
            this.xrLabel个人账户支付.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel其他医保支付
            // 
            this.xrLabel其他医保支付.BackColor = System.Drawing.Color.White;
            this.xrLabel其他医保支付.BorderColor = System.Drawing.Color.Black;
            this.xrLabel其他医保支付.CanGrow = false;
            this.xrLabel其他医保支付.Dpi = 254F;
            this.xrLabel其他医保支付.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel其他医保支付.ForeColor = System.Drawing.Color.Black;
            this.xrLabel其他医保支付.LocationFloat = new DevExpress.Utils.PointFloat(899.5833F, 60.85417F);
            this.xrLabel其他医保支付.Name = "xrLabel其他医保支付";
            this.xrLabel其他医保支付.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel其他医保支付.StylePriority.UseFont = false;
            this.xrLabel其他医保支付.Text = "其他医保支付";
            this.xrLabel其他医保支付.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel个人支付金额
            // 
            this.xrLabel个人支付金额.BackColor = System.Drawing.Color.White;
            this.xrLabel个人支付金额.BorderColor = System.Drawing.Color.Black;
            this.xrLabel个人支付金额.CanGrow = false;
            this.xrLabel个人支付金额.Dpi = 254F;
            this.xrLabel个人支付金额.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel个人支付金额.ForeColor = System.Drawing.Color.Black;
            this.xrLabel个人支付金额.LocationFloat = new DevExpress.Utils.PointFloat(1314.979F, 60.85417F);
            this.xrLabel个人支付金额.Name = "xrLabel个人支付金额";
            this.xrLabel个人支付金额.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel个人支付金额.StylePriority.UseFont = false;
            this.xrLabel个人支付金额.Text = "个人支付金额";
            this.xrLabel个人支付金额.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel年月日
            // 
            this.xrLabel年月日.BackColor = System.Drawing.Color.White;
            this.xrLabel年月日.BorderColor = System.Drawing.Color.Black;
            this.xrLabel年月日.CanGrow = false;
            this.xrLabel年月日.Dpi = 254F;
            this.xrLabel年月日.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel年月日.ForeColor = System.Drawing.Color.Black;
            this.xrLabel年月日.LocationFloat = new DevExpress.Utils.PointFloat(1128.229F, 106.4142F);
            this.xrLabel年月日.Name = "xrLabel年月日";
            this.xrLabel年月日.SizeF = new System.Drawing.SizeF(335.1742F, 45.56001F);
            this.xrLabel年月日.StylePriority.UseFont = false;
            this.xrLabel年月日.Text = "年月日";
            this.xrLabel年月日.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel收款人
            // 
            this.xrLabel收款人.BackColor = System.Drawing.Color.White;
            this.xrLabel收款人.BorderColor = System.Drawing.Color.Black;
            this.xrLabel收款人.CanGrow = false;
            this.xrLabel收款人.Dpi = 254F;
            this.xrLabel收款人.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel收款人.ForeColor = System.Drawing.Color.Black;
            this.xrLabel收款人.LocationFloat = new DevExpress.Utils.PointFloat(780.5208F, 106.4142F);
            this.xrLabel收款人.Name = "xrLabel收款人";
            this.xrLabel收款人.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel收款人.StylePriority.UseFont = false;
            this.xrLabel收款人.Text = "收款人";
            this.xrLabel收款人.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel收款单位
            // 
            this.xrLabel收款单位.BackColor = System.Drawing.Color.White;
            this.xrLabel收款单位.BorderColor = System.Drawing.Color.Black;
            this.xrLabel收款单位.CanGrow = false;
            this.xrLabel收款单位.Dpi = 254F;
            this.xrLabel收款单位.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel收款单位.ForeColor = System.Drawing.Color.Black;
            this.xrLabel收款单位.LocationFloat = new DevExpress.Utils.PointFloat(24.99999F, 106.4142F);
            this.xrLabel收款单位.Name = "xrLabel收款单位";
            this.xrLabel收款单位.SizeF = new System.Drawing.SizeF(749.3826F, 45.56002F);
            this.xrLabel收款单位.StylePriority.UseFont = false;
            this.xrLabel收款单位.Text = "收款单位";
            this.xrLabel收款单位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel报销信息,
            this.xrLabel个人账户支付,
            this.xrLabel合计小写,
            this.xrLabel医保统筹支付,
            this.xrLabel合计大写,
            this.xrLabel其他医保支付,
            this.xrLabel个人支付金额,
            this.xrLabel年月日,
            this.xrLabel收款人,
            this.xrLabel收款单位});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 254F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel报销信息
            // 
            this.xrLabel报销信息.BackColor = System.Drawing.Color.White;
            this.xrLabel报销信息.BorderColor = System.Drawing.Color.Black;
            this.xrLabel报销信息.CanGrow = false;
            this.xrLabel报销信息.Dpi = 254F;
            this.xrLabel报销信息.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel报销信息.ForeColor = System.Drawing.Color.Black;
            this.xrLabel报销信息.LocationFloat = new DevExpress.Utils.PointFloat(153.4583F, 60F);
            this.xrLabel报销信息.Name = "xrLabel报销信息";
            this.xrLabel报销信息.SizeF = new System.Drawing.SizeF(1610.466F, 45.56001F);
            this.xrLabel报销信息.StylePriority.UseFont = false;
            this.xrLabel报销信息.Text = "报销信息";
            this.xrLabel报销信息.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // XtraReport门诊发票_新_摘要
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 1270;
            this.PageWidth = 1890;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 31.75F;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel label姓名;
        private DevExpress.XtraReports.UI.XRLabel label流水号;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel社会保障号码;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医保类型;
        private DevExpress.XtraReports.UI.XRLabel xrLabel校验码;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医疗机构类型;
        private DevExpress.XtraReports.UI.XRLabel xrLabel年月日;
        private DevExpress.XtraReports.UI.XRLabel xrLabel个人支付金额;
        private DevExpress.XtraReports.UI.XRLabel xrLabel其他医保支付;
        private DevExpress.XtraReports.UI.XRLabel xrLabel个人账户支付;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医保统筹支付;
        private DevExpress.XtraReports.UI.XRLabel xrLabel合计小写;
        private DevExpress.XtraReports.UI.XRLabel xrLabel合计大写;
        private DevExpress.XtraReports.UI.XRLabel xrLabel收款单位;
        private DevExpress.XtraReports.UI.XRLabel xrLabel收款人;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel报销信息;
    }
}
