﻿namespace HIS.COMM
{
    partial class XtraReport门诊发票_药店
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.label姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.label流水号 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel标题 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel社会保障号码 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医保类型 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel校验码 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医疗机构类型 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel收款单位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel合计大写 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel合计小写 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医保统筹支付 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel个人账户支付 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel其他医保支付 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel个人支付金额 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel年月日 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel收款人 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel报销信息 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 58.42F;
            this.Detail.MultiColumn.ColumnCount = 2;
            this.Detail.MultiColumn.ColumnSpacing = 1F;
            this.Detail.MultiColumn.ColumnWidth = 870F;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt门诊打印格式.收费金额")});
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(689.7709F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(232.9584F, 58.42F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrLabel3.WordWrap = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt门诊打印格式.数量", "{0:#}")});
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(554.8334F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(134.9375F, 58.42F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "xrLabel2";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrLabel2.WordWrap = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt门诊打印格式.收费名称")});
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(102.3959F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(452.4375F, 58.42F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "xrLabel1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel1.WordWrap = false;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // label姓名
            // 
            this.label姓名.BackColor = System.Drawing.Color.White;
            this.label姓名.BorderColor = System.Drawing.Color.Black;
            this.label姓名.CanGrow = false;
            this.label姓名.Dpi = 254F;
            this.label姓名.Font = new System.Drawing.Font("宋体", 9F);
            this.label姓名.ForeColor = System.Drawing.Color.Black;
            this.label姓名.LocationFloat = new DevExpress.Utils.PointFloat(1459.792F, 199.8775F);
            this.label姓名.Name = "label姓名";
            this.label姓名.SizeF = new System.Drawing.SizeF(141.8501F, 58.41998F);
            this.label姓名.StylePriority.UseFont = false;
            this.label姓名.StylePriority.UseTextAlignment = false;
            this.label姓名.Text = "姓名";
            this.label姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label流水号
            // 
            this.label流水号.BackColor = System.Drawing.Color.White;
            this.label流水号.BorderColor = System.Drawing.Color.Black;
            this.label流水号.CanGrow = false;
            this.label流水号.Dpi = 254F;
            this.label流水号.Font = new System.Drawing.Font("宋体", 9F);
            this.label流水号.ForeColor = System.Drawing.Color.Black;
            this.label流水号.LocationFloat = new DevExpress.Utils.PointFloat(848.1458F, 199.8775F);
            this.label流水号.Name = "label流水号";
            this.label流水号.SizeF = new System.Drawing.SizeF(350.52F, 58.42F);
            this.label流水号.StylePriority.UseFont = false;
            this.label流水号.StylePriority.UseTextAlignment = false;
            this.label流水号.Text = "流水号";
            this.label流水号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.xrTable2,
            this.xrTable1,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel标题,
            this.xrLabel社会保障号码,
            this.xrLabel医保类型,
            this.xrLabel校验码,
            this.xrLabel医疗机构类型,
            this.xrLabel性别,
            this.label流水号,
            this.label姓名,
            this.xrLabel收款单位});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 344.5208F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 3;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(92.97913F, 269.875F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1672.167F, 5.291656F);
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(922.7293F, 281.0208F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(820.3334F, 63.5F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "收费名称";
            this.xrTableCell4.Weight = 1.6819673508676565D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "数量";
            this.xrTableCell5.Weight = 0.50163938381134754D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "金额";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.86603893785797614D;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(102.3959F, 281.0208F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(820.3334F, 63.5F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "收费名称";
            this.xrTableCell1.Weight = 1.6819673508676565D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "数量";
            this.xrTableCell2.Weight = 0.50163938381134754D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "金额";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.86603893785797614D;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(1330.979F, 199.8775F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(124.3541F, 58.42F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "姓名:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(723.7917F, 199.8775F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(124.3541F, 58.42F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "单据号:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(79.6875F, 199.8775F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(164.0417F, 58.42F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "定点单位:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel标题
            // 
            this.xrLabel标题.Dpi = 254F;
            this.xrLabel标题.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel标题.LocationFloat = new DevExpress.Utils.PointFloat(102.3959F, 100.4359F);
            this.xrLabel标题.Name = "xrLabel标题";
            this.xrLabel标题.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel标题.SizeF = new System.Drawing.SizeF(1640.667F, 58.41999F);
            this.xrLabel标题.StylePriority.UseFont = false;
            this.xrLabel标题.StylePriority.UseTextAlignment = false;
            this.xrLabel标题.Text = "临沂市定点药店收费单据";
            this.xrLabel标题.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel社会保障号码
            // 
            this.xrLabel社会保障号码.BackColor = System.Drawing.Color.White;
            this.xrLabel社会保障号码.BorderColor = System.Drawing.Color.Black;
            this.xrLabel社会保障号码.CanGrow = false;
            this.xrLabel社会保障号码.Dpi = 254F;
            this.xrLabel社会保障号码.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel社会保障号码.ForeColor = System.Drawing.Color.Black;
            this.xrLabel社会保障号码.LocationFloat = new DevExpress.Utils.PointFloat(206.375F, 15.00001F);
            this.xrLabel社会保障号码.Name = "xrLabel社会保障号码";
            this.xrLabel社会保障号码.SizeF = new System.Drawing.SizeF(501.8618F, 45.56F);
            this.xrLabel社会保障号码.StylePriority.UseFont = false;
            this.xrLabel社会保障号码.Text = "社会保障号码";
            this.xrLabel社会保障号码.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel社会保障号码.Visible = false;
            // 
            // xrLabel医保类型
            // 
            this.xrLabel医保类型.BackColor = System.Drawing.Color.White;
            this.xrLabel医保类型.BorderColor = System.Drawing.Color.Black;
            this.xrLabel医保类型.CanGrow = false;
            this.xrLabel医保类型.Dpi = 254F;
            this.xrLabel医保类型.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel医保类型.ForeColor = System.Drawing.Color.Black;
            this.xrLabel医保类型.LocationFloat = new DevExpress.Utils.PointFloat(1654.18F, 25.00001F);
            this.xrLabel医保类型.Name = "xrLabel医保类型";
            this.xrLabel医保类型.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel医保类型.StylePriority.UseFont = false;
            this.xrLabel医保类型.Text = "城镇职工";
            this.xrLabel医保类型.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel医保类型.Visible = false;
            // 
            // xrLabel校验码
            // 
            this.xrLabel校验码.BackColor = System.Drawing.Color.White;
            this.xrLabel校验码.BorderColor = System.Drawing.Color.Black;
            this.xrLabel校验码.CanGrow = false;
            this.xrLabel校验码.Dpi = 254F;
            this.xrLabel校验码.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel校验码.ForeColor = System.Drawing.Color.Black;
            this.xrLabel校验码.LocationFloat = new DevExpress.Utils.PointFloat(841.7501F, 15.00001F);
            this.xrLabel校验码.Name = "xrLabel校验码";
            this.xrLabel校验码.SizeF = new System.Drawing.SizeF(350.52F, 35.56F);
            this.xrLabel校验码.StylePriority.UseFont = false;
            this.xrLabel校验码.Text = "校验码";
            this.xrLabel校验码.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel校验码.Visible = false;
            // 
            // xrLabel医疗机构类型
            // 
            this.xrLabel医疗机构类型.BackColor = System.Drawing.Color.White;
            this.xrLabel医疗机构类型.BorderColor = System.Drawing.Color.Black;
            this.xrLabel医疗机构类型.CanGrow = false;
            this.xrLabel医疗机构类型.Dpi = 254F;
            this.xrLabel医疗机构类型.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel医疗机构类型.ForeColor = System.Drawing.Color.Black;
            this.xrLabel医疗机构类型.LocationFloat = new DevExpress.Utils.PointFloat(1145.174F, 15.00001F);
            this.xrLabel医疗机构类型.Name = "xrLabel医疗机构类型";
            this.xrLabel医疗机构类型.SizeF = new System.Drawing.SizeF(350.52F, 35.56F);
            this.xrLabel医疗机构类型.StylePriority.UseFont = false;
            this.xrLabel医疗机构类型.Text = "医疗机构类型";
            this.xrLabel医疗机构类型.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel医疗机构类型.Visible = false;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.BackColor = System.Drawing.Color.White;
            this.xrLabel性别.BorderColor = System.Drawing.Color.Black;
            this.xrLabel性别.CanGrow = false;
            this.xrLabel性别.Dpi = 254F;
            this.xrLabel性别.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel性别.ForeColor = System.Drawing.Color.Black;
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(1528.762F, 15.00001F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(75.88254F, 45.56F);
            this.xrLabel性别.StylePriority.UseFont = false;
            this.xrLabel性别.Text = "性别";
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel性别.Visible = false;
            // 
            // xrLabel收款单位
            // 
            this.xrLabel收款单位.BackColor = System.Drawing.Color.White;
            this.xrLabel收款单位.BorderColor = System.Drawing.Color.Black;
            this.xrLabel收款单位.CanGrow = false;
            this.xrLabel收款单位.Dpi = 254F;
            this.xrLabel收款单位.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel收款单位.ForeColor = System.Drawing.Color.Black;
            this.xrLabel收款单位.LocationFloat = new DevExpress.Utils.PointFloat(243.7292F, 199.8775F);
            this.xrLabel收款单位.Name = "xrLabel收款单位";
            this.xrLabel收款单位.SizeF = new System.Drawing.SizeF(656.2916F, 58.41998F);
            this.xrLabel收款单位.StylePriority.UseFont = false;
            this.xrLabel收款单位.StylePriority.UseTextAlignment = false;
            this.xrLabel收款单位.Text = "收款单位";
            this.xrLabel收款单位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel合计大写
            // 
            this.xrLabel合计大写.BackColor = System.Drawing.Color.White;
            this.xrLabel合计大写.BorderColor = System.Drawing.Color.Black;
            this.xrLabel合计大写.CanGrow = false;
            this.xrLabel合计大写.Dpi = 254F;
            this.xrLabel合计大写.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel合计大写.ForeColor = System.Drawing.Color.Black;
            this.xrLabel合计大写.LocationFloat = new DevExpress.Utils.PointFloat(368.1458F, 195.7917F);
            this.xrLabel合计大写.Name = "xrLabel合计大写";
            this.xrLabel合计大写.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel合计大写.StylePriority.UseFont = false;
            this.xrLabel合计大写.Text = "合计大写";
            this.xrLabel合计大写.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel合计大写.Visible = false;
            // 
            // xrLabel合计小写
            // 
            this.xrLabel合计小写.BackColor = System.Drawing.Color.White;
            this.xrLabel合计小写.BorderColor = System.Drawing.Color.Black;
            this.xrLabel合计小写.CanGrow = false;
            this.xrLabel合计小写.Dpi = 254F;
            this.xrLabel合计小写.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel合计小写.ForeColor = System.Drawing.Color.Black;
            this.xrLabel合计小写.LocationFloat = new DevExpress.Utils.PointFloat(599.1798F, 195.7917F);
            this.xrLabel合计小写.Name = "xrLabel合计小写";
            this.xrLabel合计小写.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel合计小写.StylePriority.UseFont = false;
            this.xrLabel合计小写.Text = "合计小写";
            this.xrLabel合计小写.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel合计小写.Visible = false;
            // 
            // xrLabel医保统筹支付
            // 
            this.xrLabel医保统筹支付.BackColor = System.Drawing.Color.White;
            this.xrLabel医保统筹支付.BorderColor = System.Drawing.Color.Black;
            this.xrLabel医保统筹支付.CanGrow = false;
            this.xrLabel医保统筹支付.Dpi = 254F;
            this.xrLabel医保统筹支付.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel医保统筹支付.ForeColor = System.Drawing.Color.Black;
            this.xrLabel医保统筹支付.LocationFloat = new DevExpress.Utils.PointFloat(1195.82F, 183.44F);
            this.xrLabel医保统筹支付.Name = "xrLabel医保统筹支付";
            this.xrLabel医保统筹支付.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel医保统筹支付.StylePriority.UseFont = false;
            this.xrLabel医保统筹支付.Text = "医保统筹支付";
            this.xrLabel医保统筹支付.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel医保统筹支付.Visible = false;
            // 
            // xrLabel个人账户支付
            // 
            this.xrLabel个人账户支付.BackColor = System.Drawing.Color.White;
            this.xrLabel个人账户支付.BorderColor = System.Drawing.Color.Black;
            this.xrLabel个人账户支付.CanGrow = false;
            this.xrLabel个人账户支付.Dpi = 254F;
            this.xrLabel个人账户支付.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel个人账户支付.ForeColor = System.Drawing.Color.Black;
            this.xrLabel个人账户支付.LocationFloat = new DevExpress.Utils.PointFloat(810F, 195.7917F);
            this.xrLabel个人账户支付.Name = "xrLabel个人账户支付";
            this.xrLabel个人账户支付.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel个人账户支付.StylePriority.UseFont = false;
            this.xrLabel个人账户支付.Text = "个人账户支付";
            this.xrLabel个人账户支付.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel个人账户支付.Visible = false;
            // 
            // xrLabel其他医保支付
            // 
            this.xrLabel其他医保支付.BackColor = System.Drawing.Color.White;
            this.xrLabel其他医保支付.BorderColor = System.Drawing.Color.Black;
            this.xrLabel其他医保支付.CanGrow = false;
            this.xrLabel其他医保支付.Dpi = 254F;
            this.xrLabel其他医保支付.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel其他医保支付.ForeColor = System.Drawing.Color.Black;
            this.xrLabel其他医保支付.LocationFloat = new DevExpress.Utils.PointFloat(1646.084F, 195.7917F);
            this.xrLabel其他医保支付.Name = "xrLabel其他医保支付";
            this.xrLabel其他医保支付.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel其他医保支付.StylePriority.UseFont = false;
            this.xrLabel其他医保支付.Text = "其他医保支付";
            this.xrLabel其他医保支付.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel其他医保支付.Visible = false;
            // 
            // xrLabel个人支付金额
            // 
            this.xrLabel个人支付金额.BackColor = System.Drawing.Color.White;
            this.xrLabel个人支付金额.BorderColor = System.Drawing.Color.Black;
            this.xrLabel个人支付金额.CanGrow = false;
            this.xrLabel个人支付金额.Dpi = 254F;
            this.xrLabel个人支付金额.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel个人支付金额.ForeColor = System.Drawing.Color.Black;
            this.xrLabel个人支付金额.LocationFloat = new DevExpress.Utils.PointFloat(1403.783F, 183.44F);
            this.xrLabel个人支付金额.Name = "xrLabel个人支付金额";
            this.xrLabel个人支付金额.SizeF = new System.Drawing.SizeF(210.8201F, 45.56001F);
            this.xrLabel个人支付金额.StylePriority.UseFont = false;
            this.xrLabel个人支付金额.Text = "个人支付金额";
            this.xrLabel个人支付金额.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrLabel个人支付金额.Visible = false;
            // 
            // xrLabel年月日
            // 
            this.xrLabel年月日.BackColor = System.Drawing.Color.White;
            this.xrLabel年月日.BorderColor = System.Drawing.Color.Black;
            this.xrLabel年月日.CanGrow = false;
            this.xrLabel年月日.Dpi = 254F;
            this.xrLabel年月日.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel年月日.ForeColor = System.Drawing.Color.Black;
            this.xrLabel年月日.LocationFloat = new DevExpress.Utils.PointFloat(1195.82F, 80.6942F);
            this.xrLabel年月日.Name = "xrLabel年月日";
            this.xrLabel年月日.SizeF = new System.Drawing.SizeF(335.1742F, 58.42F);
            this.xrLabel年月日.StylePriority.UseFont = false;
            this.xrLabel年月日.StylePriority.UseTextAlignment = false;
            this.xrLabel年月日.Text = "年月日";
            this.xrLabel年月日.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel收款人
            // 
            this.xrLabel收款人.BackColor = System.Drawing.Color.White;
            this.xrLabel收款人.BorderColor = System.Drawing.Color.Black;
            this.xrLabel收款人.CanGrow = false;
            this.xrLabel收款人.Dpi = 254F;
            this.xrLabel收款人.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel收款人.ForeColor = System.Drawing.Color.Black;
            this.xrLabel收款人.LocationFloat = new DevExpress.Utils.PointFloat(381.375F, 80.6942F);
            this.xrLabel收款人.Name = "xrLabel收款人";
            this.xrLabel收款人.SizeF = new System.Drawing.SizeF(210.8201F, 58.42F);
            this.xrLabel收款人.StylePriority.UseFont = false;
            this.xrLabel收款人.StylePriority.UseTextAlignment = false;
            this.xrLabel收款人.Text = "收款人";
            this.xrLabel收款人.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.xrLabel9,
            this.xrLabel报销信息,
            this.xrLabel个人账户支付,
            this.xrLabel合计小写,
            this.xrLabel医保统筹支付,
            this.xrLabel合计大写,
            this.xrLabel其他医保支付,
            this.xrLabel个人支付金额,
            this.xrLabel年月日,
            this.xrLabel收款人,
            this.xrLabel8});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 254F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLine2
            // 
            this.xrLine2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LineWidth = 3;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(92.97913F, 71.70834F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(1672.167F, 5.291656F);
            this.xrLine2.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(1008.438F, 80.6942F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(164.0417F, 58.42F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "收款时间:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel报销信息
            // 
            this.xrLabel报销信息.BackColor = System.Drawing.Color.White;
            this.xrLabel报销信息.BorderColor = System.Drawing.Color.Black;
            this.xrLabel报销信息.CanGrow = false;
            this.xrLabel报销信息.Dpi = 254F;
            this.xrLabel报销信息.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel报销信息.ForeColor = System.Drawing.Color.Black;
            this.xrLabel报销信息.LocationFloat = new DevExpress.Utils.PointFloat(217.3333F, 25.00001F);
            this.xrLabel报销信息.Name = "xrLabel报销信息";
            this.xrLabel报销信息.SizeF = new System.Drawing.SizeF(1624.167F, 45.56001F);
            this.xrLabel报销信息.StylePriority.UseFont = false;
            this.xrLabel报销信息.Text = "报销信息";
            this.xrLabel报销信息.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("宋体", 9F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(190.4034F, 80.6942F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(164.0417F, 58.42F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "收款员:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // XtraReport门诊发票_药店
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 1270;
            this.PageWidth = 1890;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 31.75F;
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel label姓名;
        private DevExpress.XtraReports.UI.XRLabel label流水号;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel社会保障号码;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医保类型;
        private DevExpress.XtraReports.UI.XRLabel xrLabel校验码;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医疗机构类型;
        private DevExpress.XtraReports.UI.XRLabel xrLabel年月日;
        private DevExpress.XtraReports.UI.XRLabel xrLabel个人支付金额;
        private DevExpress.XtraReports.UI.XRLabel xrLabel其他医保支付;
        private DevExpress.XtraReports.UI.XRLabel xrLabel个人账户支付;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医保统筹支付;
        private DevExpress.XtraReports.UI.XRLabel xrLabel合计小写;
        private DevExpress.XtraReports.UI.XRLabel xrLabel合计大写;
        private DevExpress.XtraReports.UI.XRLabel xrLabel收款单位;
        private DevExpress.XtraReports.UI.XRLabel xrLabel收款人;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel报销信息;
        private DevExpress.XtraReports.UI.XRLabel xrLabel标题;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
    }
}
