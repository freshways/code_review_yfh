﻿namespace HIS.COMM
{
    partial class XtraReport门诊发票
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.jeCenterTop2 = new DevExpress.XtraReports.UI.XRLabel();
            this.jeCenterMiddle2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xmCenterMiddle2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xmCenterTop2 = new DevExpress.XtraReports.UI.XRLabel();
            this.jeLeftMiddle2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xmLeftMiddle2 = new DevExpress.XtraReports.UI.XRLabel();
            this.jeLeftTop2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xmLeftTop2 = new DevExpress.XtraReports.UI.XRLabel();
            this.TotalJeDxCenter = new DevExpress.XtraReports.UI.XRLabel();
            this.jeLeftTop = new DevExpress.XtraReports.UI.XRLabel();
            this.xmLeftTop = new DevExpress.XtraReports.UI.XRLabel();
            this.xmLeftMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.jeLeftMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.xmLeftBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.jeLeftBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.brxmCenter = new DevExpress.XtraReports.UI.XRLabel();
            this.dyrqLeftBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.zdLeftBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.xmCenterTop = new DevExpress.XtraReports.UI.XRLabel();
            this.jeCenterTop = new DevExpress.XtraReports.UI.XRLabel();
            this.xmCenterMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.jeCenterMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.xmCenterBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.jeCenterBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.dyrqCenter = new DevExpress.XtraReports.UI.XRLabel();
            this.TotalJeXxCenter = new DevExpress.XtraReports.UI.XRLabel();
            this.skrCenterBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.TotalJeXxLeft = new DevExpress.XtraReports.UI.XRLabel();
            this.jeRightTop = new DevExpress.XtraReports.UI.XRLabel();
            this.xmRightTop = new DevExpress.XtraReports.UI.XRLabel();
            this.xmRightMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.jeRightMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.xmRightBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.jeRightBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.skrRightTop = new DevExpress.XtraReports.UI.XRLabel();
            this.dyrqRightTop = new DevExpress.XtraReports.UI.XRLabel();
            this.skrRightMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.dyrqRightMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.skrRightBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.dyrqRightBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.brxmLeft = new DevExpress.XtraReports.UI.XRLabel();
            this.brxmRightTop = new DevExpress.XtraReports.UI.XRLabel();
            this.brxmRightMiddle = new DevExpress.XtraReports.UI.XRLabel();
            this.brxmRightBottom = new DevExpress.XtraReports.UI.XRLabel();
            this.LshCenter = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.jeCenterTop2,
            this.jeCenterMiddle2,
            this.xmCenterMiddle2,
            this.xmCenterTop2,
            this.jeLeftMiddle2,
            this.xmLeftMiddle2,
            this.jeLeftTop2,
            this.xmLeftTop2,
            this.TotalJeDxCenter,
            this.jeLeftTop,
            this.xmLeftTop,
            this.xmLeftMiddle,
            this.jeLeftMiddle,
            this.xmLeftBottom,
            this.jeLeftBottom,
            this.brxmCenter,
            this.dyrqLeftBottom,
            this.zdLeftBottom,
            this.xmCenterTop,
            this.jeCenterTop,
            this.xmCenterMiddle,
            this.jeCenterMiddle,
            this.xmCenterBottom,
            this.jeCenterBottom,
            this.dyrqCenter,
            this.TotalJeXxCenter,
            this.skrCenterBottom,
            this.TotalJeXxLeft,
            this.jeRightTop,
            this.xmRightTop,
            this.xmRightMiddle,
            this.jeRightMiddle,
            this.xmRightBottom,
            this.jeRightBottom,
            this.skrRightTop,
            this.dyrqRightTop,
            this.skrRightMiddle,
            this.dyrqRightMiddle,
            this.skrRightBottom,
            this.dyrqRightBottom,
            this.brxmLeft,
            this.brxmRightTop,
            this.brxmRightMiddle,
            this.brxmRightBottom,
            this.LshCenter});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 925F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // jeCenterTop2
            // 
            this.jeCenterTop2.BackColor = System.Drawing.Color.White;
            this.jeCenterTop2.BorderColor = System.Drawing.Color.Black;
            this.jeCenterTop2.CanGrow = false;
            this.jeCenterTop2.Dpi = 254F;
            this.jeCenterTop2.Font = new System.Drawing.Font("宋体", 9F);
            this.jeCenterTop2.ForeColor = System.Drawing.Color.Black;
            this.jeCenterTop2.LocationFloat = new DevExpress.Utils.PointFloat(1234.44F, 390.7133F);
            this.jeCenterTop2.Name = "jeCenterTop2";
            this.jeCenterTop2.SizeF = new System.Drawing.SizeF(182.88F, 45.56F);
            this.jeCenterTop2.StylePriority.UseFont = false;
            this.jeCenterTop2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeCenterMiddle2
            // 
            this.jeCenterMiddle2.BackColor = System.Drawing.Color.White;
            this.jeCenterMiddle2.BorderColor = System.Drawing.Color.Black;
            this.jeCenterMiddle2.CanGrow = false;
            this.jeCenterMiddle2.Dpi = 254F;
            this.jeCenterMiddle2.Font = new System.Drawing.Font("宋体", 9F);
            this.jeCenterMiddle2.ForeColor = System.Drawing.Color.Black;
            this.jeCenterMiddle2.LocationFloat = new DevExpress.Utils.PointFloat(1234.44F, 560.7892F);
            this.jeCenterMiddle2.Name = "jeCenterMiddle2";
            this.jeCenterMiddle2.SizeF = new System.Drawing.SizeF(182.88F, 46.14334F);
            this.jeCenterMiddle2.StylePriority.UseFont = false;
            this.jeCenterMiddle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmCenterMiddle2
            // 
            this.xmCenterMiddle2.BackColor = System.Drawing.Color.White;
            this.xmCenterMiddle2.BorderColor = System.Drawing.Color.Black;
            this.xmCenterMiddle2.CanGrow = false;
            this.xmCenterMiddle2.Dpi = 254F;
            this.xmCenterMiddle2.Font = new System.Drawing.Font("宋体", 9F);
            this.xmCenterMiddle2.ForeColor = System.Drawing.Color.Black;
            this.xmCenterMiddle2.LocationFloat = new DevExpress.Utils.PointFloat(718.8201F, 561.3725F);
            this.xmCenterMiddle2.Name = "xmCenterMiddle2";
            this.xmCenterMiddle2.SizeF = new System.Drawing.SizeF(508F, 45.56F);
            this.xmCenterMiddle2.StylePriority.UseFont = false;
            this.xmCenterMiddle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmCenterTop2
            // 
            this.xmCenterTop2.BackColor = System.Drawing.Color.White;
            this.xmCenterTop2.BorderColor = System.Drawing.Color.Black;
            this.xmCenterTop2.CanGrow = false;
            this.xmCenterTop2.Dpi = 254F;
            this.xmCenterTop2.Font = new System.Drawing.Font("宋体", 9F);
            this.xmCenterTop2.ForeColor = System.Drawing.Color.Black;
            this.xmCenterTop2.LocationFloat = new DevExpress.Utils.PointFloat(718.8201F, 390.7133F);
            this.xmCenterTop2.Name = "xmCenterTop2";
            this.xmCenterTop2.SizeF = new System.Drawing.SizeF(508F, 45.56F);
            this.xmCenterTop2.StylePriority.UseFont = false;
            this.xmCenterTop2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeLeftMiddle2
            // 
            this.jeLeftMiddle2.BackColor = System.Drawing.Color.White;
            this.jeLeftMiddle2.BorderColor = System.Drawing.Color.Black;
            this.jeLeftMiddle2.CanGrow = false;
            this.jeLeftMiddle2.Dpi = 254F;
            this.jeLeftMiddle2.Font = new System.Drawing.Font("宋体", 9F);
            this.jeLeftMiddle2.ForeColor = System.Drawing.Color.Black;
            this.jeLeftMiddle2.LocationFloat = new DevExpress.Utils.PointFloat(290.14F, 560.7891F);
            this.jeLeftMiddle2.Name = "jeLeftMiddle2";
            this.jeLeftMiddle2.SizeF = new System.Drawing.SizeF(127F, 46.14337F);
            this.jeLeftMiddle2.StylePriority.UseFont = false;
            this.jeLeftMiddle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmLeftMiddle2
            // 
            this.xmLeftMiddle2.BackColor = System.Drawing.Color.White;
            this.xmLeftMiddle2.BorderColor = System.Drawing.Color.Black;
            this.xmLeftMiddle2.CanGrow = false;
            this.xmLeftMiddle2.Dpi = 254F;
            this.xmLeftMiddle2.Font = new System.Drawing.Font("宋体", 9F);
            this.xmLeftMiddle2.ForeColor = System.Drawing.Color.Black;
            this.xmLeftMiddle2.LocationFloat = new DevExpress.Utils.PointFloat(77.5F, 561.3725F);
            this.xmLeftMiddle2.Name = "xmLeftMiddle2";
            this.xmLeftMiddle2.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.xmLeftMiddle2.StylePriority.UseFont = false;
            this.xmLeftMiddle2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeLeftTop2
            // 
            this.jeLeftTop2.BackColor = System.Drawing.Color.White;
            this.jeLeftTop2.BorderColor = System.Drawing.Color.Black;
            this.jeLeftTop2.CanGrow = false;
            this.jeLeftTop2.Dpi = 254F;
            this.jeLeftTop2.Font = new System.Drawing.Font("宋体", 9F);
            this.jeLeftTop2.ForeColor = System.Drawing.Color.Black;
            this.jeLeftTop2.LocationFloat = new DevExpress.Utils.PointFloat(290.1401F, 390.7133F);
            this.jeLeftTop2.Name = "jeLeftTop2";
            this.jeLeftTop2.SizeF = new System.Drawing.SizeF(127F, 45.56F);
            this.jeLeftTop2.StylePriority.UseFont = false;
            this.jeLeftTop2.Target = "jeLeftTop";
            this.jeLeftTop2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmLeftTop2
            // 
            this.xmLeftTop2.BackColor = System.Drawing.Color.White;
            this.xmLeftTop2.BorderColor = System.Drawing.Color.Black;
            this.xmLeftTop2.CanGrow = false;
            this.xmLeftTop2.Dpi = 254F;
            this.xmLeftTop2.Font = new System.Drawing.Font("宋体", 9F);
            this.xmLeftTop2.ForeColor = System.Drawing.Color.Black;
            this.xmLeftTop2.LocationFloat = new DevExpress.Utils.PointFloat(77.5F, 390.7133F);
            this.xmLeftTop2.Name = "xmLeftTop2";
            this.xmLeftTop2.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.xmLeftTop2.StylePriority.UseFont = false;
            this.xmLeftTop2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TotalJeDxCenter
            // 
            this.TotalJeDxCenter.BackColor = System.Drawing.Color.White;
            this.TotalJeDxCenter.BorderColor = System.Drawing.Color.Black;
            this.TotalJeDxCenter.CanGrow = false;
            this.TotalJeDxCenter.Dpi = 254F;
            this.TotalJeDxCenter.Font = new System.Drawing.Font("宋体", 9F);
            this.TotalJeDxCenter.ForeColor = System.Drawing.Color.Black;
            this.TotalJeDxCenter.LocationFloat = new DevExpress.Utils.PointFloat(647.3825F, 698.5F);
            this.TotalJeDxCenter.Name = "TotalJeDxCenter";
            this.TotalJeDxCenter.SizeF = new System.Drawing.SizeF(853.7576F, 104.14F);
            this.TotalJeDxCenter.StylePriority.UseFont = false;
            this.TotalJeDxCenter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeLeftTop
            // 
            this.jeLeftTop.BackColor = System.Drawing.Color.White;
            this.jeLeftTop.BorderColor = System.Drawing.Color.Black;
            this.jeLeftTop.CanGrow = false;
            this.jeLeftTop.Dpi = 254F;
            this.jeLeftTop.Font = new System.Drawing.Font("宋体", 9F);
            this.jeLeftTop.ForeColor = System.Drawing.Color.Black;
            this.jeLeftTop.LocationFloat = new DevExpress.Utils.PointFloat(300.7234F, 308.82F);
            this.jeLeftTop.Name = "jeLeftTop";
            this.jeLeftTop.SizeF = new System.Drawing.SizeF(127F, 51.01331F);
            this.jeLeftTop.StylePriority.UseFont = false;
            this.jeLeftTop.Target = "jeLeftTop";
            this.jeLeftTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmLeftTop
            // 
            this.xmLeftTop.BackColor = System.Drawing.Color.White;
            this.xmLeftTop.BorderColor = System.Drawing.Color.Black;
            this.xmLeftTop.CanGrow = false;
            this.xmLeftTop.Dpi = 254F;
            this.xmLeftTop.Font = new System.Drawing.Font("宋体", 9F);
            this.xmLeftTop.ForeColor = System.Drawing.Color.Black;
            this.xmLeftTop.LocationFloat = new DevExpress.Utils.PointFloat(77.5F, 308.82F);
            this.xmLeftTop.Name = "xmLeftTop";
            this.xmLeftTop.SizeF = new System.Drawing.SizeF(210.82F, 51.01331F);
            this.xmLeftTop.StylePriority.UseFont = false;
            this.xmLeftTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmLeftMiddle
            // 
            this.xmLeftMiddle.BackColor = System.Drawing.Color.White;
            this.xmLeftMiddle.BorderColor = System.Drawing.Color.Black;
            this.xmLeftMiddle.CanGrow = false;
            this.xmLeftMiddle.Dpi = 254F;
            this.xmLeftMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.xmLeftMiddle.ForeColor = System.Drawing.Color.Black;
            this.xmLeftMiddle.LocationFloat = new DevExpress.Utils.PointFloat(77.5F, 479.0833F);
            this.xmLeftMiddle.Name = "xmLeftMiddle";
            this.xmLeftMiddle.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.xmLeftMiddle.StylePriority.UseFont = false;
            this.xmLeftMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeLeftMiddle
            // 
            this.jeLeftMiddle.BackColor = System.Drawing.Color.White;
            this.jeLeftMiddle.BorderColor = System.Drawing.Color.Black;
            this.jeLeftMiddle.CanGrow = false;
            this.jeLeftMiddle.Dpi = 254F;
            this.jeLeftMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.jeLeftMiddle.ForeColor = System.Drawing.Color.Black;
            this.jeLeftMiddle.LocationFloat = new DevExpress.Utils.PointFloat(290.14F, 478.5F);
            this.jeLeftMiddle.Name = "jeLeftMiddle";
            this.jeLeftMiddle.SizeF = new System.Drawing.SizeF(127F, 46.14334F);
            this.jeLeftMiddle.StylePriority.UseFont = false;
            this.jeLeftMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmLeftBottom
            // 
            this.xmLeftBottom.BackColor = System.Drawing.Color.White;
            this.xmLeftBottom.BorderColor = System.Drawing.Color.Black;
            this.xmLeftBottom.CanGrow = false;
            this.xmLeftBottom.Dpi = 254F;
            this.xmLeftBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.xmLeftBottom.ForeColor = System.Drawing.Color.Black;
            this.xmLeftBottom.LocationFloat = new DevExpress.Utils.PointFloat(77.5F, 634.9733F);
            this.xmLeftBottom.Name = "xmLeftBottom";
            this.xmLeftBottom.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.xmLeftBottom.StylePriority.UseFont = false;
            this.xmLeftBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeLeftBottom
            // 
            this.jeLeftBottom.BackColor = System.Drawing.Color.White;
            this.jeLeftBottom.BorderColor = System.Drawing.Color.Black;
            this.jeLeftBottom.CanGrow = false;
            this.jeLeftBottom.Dpi = 254F;
            this.jeLeftBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.jeLeftBottom.ForeColor = System.Drawing.Color.Black;
            this.jeLeftBottom.LocationFloat = new DevExpress.Utils.PointFloat(290.14F, 634.9733F);
            this.jeLeftBottom.Name = "jeLeftBottom";
            this.jeLeftBottom.SizeF = new System.Drawing.SizeF(127F, 45.56F);
            this.jeLeftBottom.StylePriority.UseFont = false;
            this.jeLeftBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // brxmCenter
            // 
            this.brxmCenter.BackColor = System.Drawing.Color.White;
            this.brxmCenter.BorderColor = System.Drawing.Color.Black;
            this.brxmCenter.CanGrow = false;
            this.brxmCenter.Dpi = 254F;
            this.brxmCenter.Font = new System.Drawing.Font("宋体", 9F);
            this.brxmCenter.ForeColor = System.Drawing.Color.Black;
            this.brxmCenter.LocationFloat = new DevExpress.Utils.PointFloat(762F, 190.5F);
            this.brxmCenter.Name = "brxmCenter";
            this.brxmCenter.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.brxmCenter.StylePriority.UseFont = false;
            this.brxmCenter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // dyrqLeftBottom
            // 
            this.dyrqLeftBottom.BackColor = System.Drawing.Color.White;
            this.dyrqLeftBottom.BorderColor = System.Drawing.Color.Black;
            this.dyrqLeftBottom.CanGrow = false;
            this.dyrqLeftBottom.Dpi = 254F;
            this.dyrqLeftBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.dyrqLeftBottom.ForeColor = System.Drawing.Color.Black;
            this.dyrqLeftBottom.LocationFloat = new DevExpress.Utils.PointFloat(118.14F, 802.64F);
            this.dyrqLeftBottom.Name = "dyrqLeftBottom";
            this.dyrqLeftBottom.SizeF = new System.Drawing.SizeF(182.88F, 51.43512F);
            this.dyrqLeftBottom.StylePriority.UseFont = false;
            this.dyrqLeftBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // zdLeftBottom
            // 
            this.zdLeftBottom.BackColor = System.Drawing.Color.White;
            this.zdLeftBottom.BorderColor = System.Drawing.Color.Black;
            this.zdLeftBottom.CanGrow = false;
            this.zdLeftBottom.Dpi = 254F;
            this.zdLeftBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.zdLeftBottom.ForeColor = System.Drawing.Color.Black;
            this.zdLeftBottom.LocationFloat = new DevExpress.Utils.PointFloat(331.5F, 802.64F);
            this.zdLeftBottom.Name = "zdLeftBottom";
            this.zdLeftBottom.SizeF = new System.Drawing.SizeF(147.32F, 51.43512F);
            this.zdLeftBottom.StylePriority.UseFont = false;
            this.zdLeftBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmCenterTop
            // 
            this.xmCenterTop.BackColor = System.Drawing.Color.White;
            this.xmCenterTop.BorderColor = System.Drawing.Color.Black;
            this.xmCenterTop.CanGrow = false;
            this.xmCenterTop.Dpi = 254F;
            this.xmCenterTop.Font = new System.Drawing.Font("宋体", 9F);
            this.xmCenterTop.ForeColor = System.Drawing.Color.Black;
            this.xmCenterTop.LocationFloat = new DevExpress.Utils.PointFloat(718.82F, 308.82F);
            this.xmCenterTop.Name = "xmCenterTop";
            this.xmCenterTop.SizeF = new System.Drawing.SizeF(508.0001F, 51.01331F);
            this.xmCenterTop.StylePriority.UseFont = false;
            this.xmCenterTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeCenterTop
            // 
            this.jeCenterTop.BackColor = System.Drawing.Color.White;
            this.jeCenterTop.BorderColor = System.Drawing.Color.Black;
            this.jeCenterTop.CanGrow = false;
            this.jeCenterTop.Dpi = 254F;
            this.jeCenterTop.Font = new System.Drawing.Font("宋体", 9F);
            this.jeCenterTop.ForeColor = System.Drawing.Color.Black;
            this.jeCenterTop.LocationFloat = new DevExpress.Utils.PointFloat(1234.44F, 308.82F);
            this.jeCenterTop.Name = "jeCenterTop";
            this.jeCenterTop.SizeF = new System.Drawing.SizeF(182.88F, 51.01331F);
            this.jeCenterTop.StylePriority.UseFont = false;
            this.jeCenterTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmCenterMiddle
            // 
            this.xmCenterMiddle.BackColor = System.Drawing.Color.White;
            this.xmCenterMiddle.BorderColor = System.Drawing.Color.Black;
            this.xmCenterMiddle.CanGrow = false;
            this.xmCenterMiddle.Dpi = 254F;
            this.xmCenterMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.xmCenterMiddle.ForeColor = System.Drawing.Color.Black;
            this.xmCenterMiddle.LocationFloat = new DevExpress.Utils.PointFloat(718.82F, 479.0833F);
            this.xmCenterMiddle.Name = "xmCenterMiddle";
            this.xmCenterMiddle.SizeF = new System.Drawing.SizeF(508F, 45.56F);
            this.xmCenterMiddle.StylePriority.UseFont = false;
            this.xmCenterMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeCenterMiddle
            // 
            this.jeCenterMiddle.BackColor = System.Drawing.Color.White;
            this.jeCenterMiddle.BorderColor = System.Drawing.Color.Black;
            this.jeCenterMiddle.CanGrow = false;
            this.jeCenterMiddle.Dpi = 254F;
            this.jeCenterMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.jeCenterMiddle.ForeColor = System.Drawing.Color.Black;
            this.jeCenterMiddle.LocationFloat = new DevExpress.Utils.PointFloat(1234.44F, 478.5F);
            this.jeCenterMiddle.Name = "jeCenterMiddle";
            this.jeCenterMiddle.SizeF = new System.Drawing.SizeF(182.88F, 46.14334F);
            this.jeCenterMiddle.StylePriority.UseFont = false;
            this.jeCenterMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmCenterBottom
            // 
            this.xmCenterBottom.BackColor = System.Drawing.Color.White;
            this.xmCenterBottom.BorderColor = System.Drawing.Color.Black;
            this.xmCenterBottom.CanGrow = false;
            this.xmCenterBottom.Dpi = 254F;
            this.xmCenterBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.xmCenterBottom.ForeColor = System.Drawing.Color.Black;
            this.xmCenterBottom.LocationFloat = new DevExpress.Utils.PointFloat(718.82F, 634.9733F);
            this.xmCenterBottom.Name = "xmCenterBottom";
            this.xmCenterBottom.SizeF = new System.Drawing.SizeF(508F, 45.56F);
            this.xmCenterBottom.StylePriority.UseFont = false;
            this.xmCenterBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeCenterBottom
            // 
            this.jeCenterBottom.BackColor = System.Drawing.Color.White;
            this.jeCenterBottom.BorderColor = System.Drawing.Color.Black;
            this.jeCenterBottom.CanGrow = false;
            this.jeCenterBottom.Dpi = 254F;
            this.jeCenterBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.jeCenterBottom.ForeColor = System.Drawing.Color.Black;
            this.jeCenterBottom.LocationFloat = new DevExpress.Utils.PointFloat(1226.82F, 634.9733F);
            this.jeCenterBottom.Name = "jeCenterBottom";
            this.jeCenterBottom.SizeF = new System.Drawing.SizeF(190.5F, 45.56F);
            this.jeCenterBottom.StylePriority.UseFont = false;
            this.jeCenterBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // dyrqCenter
            // 
            this.dyrqCenter.BackColor = System.Drawing.Color.White;
            this.dyrqCenter.BorderColor = System.Drawing.Color.Black;
            this.dyrqCenter.CanGrow = false;
            this.dyrqCenter.Dpi = 254F;
            this.dyrqCenter.Font = new System.Drawing.Font("宋体", 9F);
            this.dyrqCenter.ForeColor = System.Drawing.Color.Black;
            this.dyrqCenter.LocationFloat = new DevExpress.Utils.PointFloat(972.82F, 190.5F);
            this.dyrqCenter.Name = "dyrqCenter";
            this.dyrqCenter.SizeF = new System.Drawing.SizeF(182.8799F, 35.56F);
            this.dyrqCenter.StylePriority.UseFont = false;
            this.dyrqCenter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TotalJeXxCenter
            // 
            this.TotalJeXxCenter.BackColor = System.Drawing.Color.White;
            this.TotalJeXxCenter.BorderColor = System.Drawing.Color.Black;
            this.TotalJeXxCenter.CanGrow = false;
            this.TotalJeXxCenter.Dpi = 254F;
            this.TotalJeXxCenter.Font = new System.Drawing.Font("宋体", 9F);
            this.TotalJeXxCenter.ForeColor = System.Drawing.Color.Black;
            this.TotalJeXxCenter.LocationFloat = new DevExpress.Utils.PointFloat(1247.14F, 698.5F);
            this.TotalJeXxCenter.Name = "TotalJeXxCenter";
            this.TotalJeXxCenter.SizeF = new System.Drawing.SizeF(182.88F, 35.56F);
            this.TotalJeXxCenter.StylePriority.UseFont = false;
            this.TotalJeXxCenter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // skrCenterBottom
            // 
            this.skrCenterBottom.BackColor = System.Drawing.Color.White;
            this.skrCenterBottom.BorderColor = System.Drawing.Color.Black;
            this.skrCenterBottom.CanGrow = false;
            this.skrCenterBottom.Dpi = 254F;
            this.skrCenterBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.skrCenterBottom.ForeColor = System.Drawing.Color.Black;
            this.skrCenterBottom.LocationFloat = new DevExpress.Utils.PointFloat(1310.64F, 802.64F);
            this.skrCenterBottom.Name = "skrCenterBottom";
            this.skrCenterBottom.SizeF = new System.Drawing.SizeF(147.3198F, 51.43506F);
            this.skrCenterBottom.StylePriority.UseFont = false;
            this.skrCenterBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TotalJeXxLeft
            // 
            this.TotalJeXxLeft.BackColor = System.Drawing.Color.White;
            this.TotalJeXxLeft.BorderColor = System.Drawing.Color.Black;
            this.TotalJeXxLeft.CanGrow = false;
            this.TotalJeXxLeft.Dpi = 254F;
            this.TotalJeXxLeft.Font = new System.Drawing.Font("宋体", 9F);
            this.TotalJeXxLeft.ForeColor = System.Drawing.Color.Black;
            this.TotalJeXxLeft.LocationFloat = new DevExpress.Utils.PointFloat(141F, 698.5F);
            this.TotalJeXxLeft.Name = "TotalJeXxLeft";
            this.TotalJeXxLeft.SizeF = new System.Drawing.SizeF(182.88F, 35.56F);
            this.TotalJeXxLeft.StylePriority.UseFont = false;
            this.TotalJeXxLeft.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // jeRightTop
            // 
            this.jeRightTop.BackColor = System.Drawing.Color.White;
            this.jeRightTop.BorderColor = System.Drawing.Color.Black;
            this.jeRightTop.CanGrow = false;
            this.jeRightTop.Dpi = 254F;
            this.jeRightTop.Font = new System.Drawing.Font("宋体", 9F);
            this.jeRightTop.ForeColor = System.Drawing.Color.Black;
            this.jeRightTop.LocationFloat = new DevExpress.Utils.PointFloat(1965.021F, 144.6741F);
            this.jeRightTop.Name = "jeRightTop";
            this.jeRightTop.SizeF = new System.Drawing.SizeF(182.8799F, 35.56F);
            this.jeRightTop.StylePriority.UseFont = false;
            this.jeRightTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmRightTop
            // 
            this.xmRightTop.BackColor = System.Drawing.Color.White;
            this.xmRightTop.BorderColor = System.Drawing.Color.Black;
            this.xmRightTop.CanGrow = false;
            this.xmRightTop.Dpi = 254F;
            this.xmRightTop.Font = new System.Drawing.Font("宋体", 9F);
            this.xmRightTop.ForeColor = System.Drawing.Color.Black;
            this.xmRightTop.LocationFloat = new DevExpress.Utils.PointFloat(1584.021F, 144.6741F);
            this.xmRightTop.Name = "xmRightTop";
            this.xmRightTop.SizeF = new System.Drawing.SizeF(381F, 45.56F);
            this.xmRightTop.StylePriority.UseFont = false;
            this.xmRightTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xmRightMiddle
            // 
            this.xmRightMiddle.BackColor = System.Drawing.Color.White;
            this.xmRightMiddle.BorderColor = System.Drawing.Color.Black;
            this.xmRightMiddle.CanGrow = false;
            this.xmRightMiddle.Dpi = 254F;
            this.xmRightMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.xmRightMiddle.ForeColor = System.Drawing.Color.Black;
            this.xmRightMiddle.LocationFloat = new DevExpress.Utils.PointFloat(1584.021F, 441.8542F);
            this.xmRightMiddle.Name = "xmRightMiddle";
            this.xmRightMiddle.SizeF = new System.Drawing.SizeF(381F, 45.56F);
            this.xmRightMiddle.StylePriority.UseFont = false;
            this.xmRightMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // jeRightMiddle
            // 
            this.jeRightMiddle.BackColor = System.Drawing.Color.White;
            this.jeRightMiddle.BorderColor = System.Drawing.Color.Black;
            this.jeRightMiddle.CanGrow = false;
            this.jeRightMiddle.Dpi = 254F;
            this.jeRightMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.jeRightMiddle.ForeColor = System.Drawing.Color.Black;
            this.jeRightMiddle.LocationFloat = new DevExpress.Utils.PointFloat(1965.021F, 441.8542F);
            this.jeRightMiddle.Name = "jeRightMiddle";
            this.jeRightMiddle.SizeF = new System.Drawing.SizeF(182.8799F, 46.14334F);
            this.jeRightMiddle.StylePriority.UseFont = false;
            this.jeRightMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xmRightBottom
            // 
            this.xmRightBottom.BackColor = System.Drawing.Color.White;
            this.xmRightBottom.BorderColor = System.Drawing.Color.Black;
            this.xmRightBottom.CanGrow = false;
            this.xmRightBottom.Dpi = 254F;
            this.xmRightBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.xmRightBottom.ForeColor = System.Drawing.Color.Black;
            this.xmRightBottom.LocationFloat = new DevExpress.Utils.PointFloat(1584.021F, 716.1741F);
            this.xmRightBottom.Name = "xmRightBottom";
            this.xmRightBottom.SizeF = new System.Drawing.SizeF(381F, 45.56F);
            this.xmRightBottom.StylePriority.UseFont = false;
            this.xmRightBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // jeRightBottom
            // 
            this.jeRightBottom.BackColor = System.Drawing.Color.White;
            this.jeRightBottom.BorderColor = System.Drawing.Color.Black;
            this.jeRightBottom.CanGrow = false;
            this.jeRightBottom.Dpi = 254F;
            this.jeRightBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.jeRightBottom.ForeColor = System.Drawing.Color.Black;
            this.jeRightBottom.LocationFloat = new DevExpress.Utils.PointFloat(1965.021F, 716.1741F);
            this.jeRightBottom.Name = "jeRightBottom";
            this.jeRightBottom.SizeF = new System.Drawing.SizeF(190.5F, 51.435F);
            this.jeRightBottom.StylePriority.UseFont = false;
            this.jeRightBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // skrRightTop
            // 
            this.skrRightTop.BackColor = System.Drawing.Color.White;
            this.skrRightTop.BorderColor = System.Drawing.Color.Black;
            this.skrRightTop.CanGrow = false;
            this.skrRightTop.Dpi = 254F;
            this.skrRightTop.Font = new System.Drawing.Font("宋体", 9F);
            this.skrRightTop.ForeColor = System.Drawing.Color.Black;
            this.skrRightTop.LocationFloat = new DevExpress.Utils.PointFloat(1901.521F, 228.4941F);
            this.skrRightTop.Name = "skrRightTop";
            this.skrRightTop.SizeF = new System.Drawing.SizeF(147.32F, 45.88F);
            this.skrRightTop.StylePriority.UseFont = false;
            this.skrRightTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // dyrqRightTop
            // 
            this.dyrqRightTop.BackColor = System.Drawing.Color.White;
            this.dyrqRightTop.BorderColor = System.Drawing.Color.Black;
            this.dyrqRightTop.CanGrow = false;
            this.dyrqRightTop.Dpi = 254F;
            this.dyrqRightTop.Font = new System.Drawing.Font("宋体", 9F);
            this.dyrqRightTop.ForeColor = System.Drawing.Color.Black;
            this.dyrqRightTop.LocationFloat = new DevExpress.Utils.PointFloat(1647.521F, 228.4941F);
            this.dyrqRightTop.Name = "dyrqRightTop";
            this.dyrqRightTop.SizeF = new System.Drawing.SizeF(182.88F, 48.78918F);
            this.dyrqRightTop.StylePriority.UseFont = false;
            this.dyrqRightTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // skrRightMiddle
            // 
            this.skrRightMiddle.BackColor = System.Drawing.Color.White;
            this.skrRightMiddle.BorderColor = System.Drawing.Color.Black;
            this.skrRightMiddle.CanGrow = false;
            this.skrRightMiddle.Dpi = 254F;
            this.skrRightMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.skrRightMiddle.ForeColor = System.Drawing.Color.Black;
            this.skrRightMiddle.LocationFloat = new DevExpress.Utils.PointFloat(1901.521F, 505.3542F);
            this.skrRightMiddle.Name = "skrRightMiddle";
            this.skrRightMiddle.SizeF = new System.Drawing.SizeF(147.32F, 45.88F);
            this.skrRightMiddle.StylePriority.UseFont = false;
            this.skrRightMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // dyrqRightMiddle
            // 
            this.dyrqRightMiddle.BackColor = System.Drawing.Color.White;
            this.dyrqRightMiddle.BorderColor = System.Drawing.Color.Black;
            this.dyrqRightMiddle.CanGrow = false;
            this.dyrqRightMiddle.Dpi = 254F;
            this.dyrqRightMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.dyrqRightMiddle.ForeColor = System.Drawing.Color.Black;
            this.dyrqRightMiddle.LocationFloat = new DevExpress.Utils.PointFloat(1647.521F, 505.3542F);
            this.dyrqRightMiddle.Name = "dyrqRightMiddle";
            this.dyrqRightMiddle.SizeF = new System.Drawing.SizeF(182.88F, 51.435F);
            this.dyrqRightMiddle.StylePriority.UseFont = false;
            this.dyrqRightMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // skrRightBottom
            // 
            this.skrRightBottom.BackColor = System.Drawing.Color.White;
            this.skrRightBottom.BorderColor = System.Drawing.Color.Black;
            this.skrRightBottom.CanGrow = false;
            this.skrRightBottom.Dpi = 254F;
            this.skrRightBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.skrRightBottom.ForeColor = System.Drawing.Color.Black;
            this.skrRightBottom.LocationFloat = new DevExpress.Utils.PointFloat(1901.521F, 779.6741F);
            this.skrRightBottom.Name = "skrRightBottom";
            this.skrRightBottom.SizeF = new System.Drawing.SizeF(147.32F, 45.88F);
            this.skrRightBottom.StylePriority.UseFont = false;
            this.skrRightBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // dyrqRightBottom
            // 
            this.dyrqRightBottom.BackColor = System.Drawing.Color.White;
            this.dyrqRightBottom.BorderColor = System.Drawing.Color.Black;
            this.dyrqRightBottom.CanGrow = false;
            this.dyrqRightBottom.Dpi = 254F;
            this.dyrqRightBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.dyrqRightBottom.ForeColor = System.Drawing.Color.Black;
            this.dyrqRightBottom.LocationFloat = new DevExpress.Utils.PointFloat(1647.521F, 779.6741F);
            this.dyrqRightBottom.Name = "dyrqRightBottom";
            this.dyrqRightBottom.SizeF = new System.Drawing.SizeF(182.88F, 55.87994F);
            this.dyrqRightBottom.StylePriority.UseFont = false;
            this.dyrqRightBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // brxmLeft
            // 
            this.brxmLeft.BackColor = System.Drawing.Color.White;
            this.brxmLeft.BorderColor = System.Drawing.Color.Black;
            this.brxmLeft.CanGrow = false;
            this.brxmLeft.Dpi = 254F;
            this.brxmLeft.Font = new System.Drawing.Font("宋体", 9F);
            this.brxmLeft.ForeColor = System.Drawing.Color.Black;
            this.brxmLeft.LocationFloat = new DevExpress.Utils.PointFloat(97.82F, 190.5F);
            this.brxmLeft.Name = "brxmLeft";
            this.brxmLeft.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.brxmLeft.StylePriority.UseFont = false;
            this.brxmLeft.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // brxmRightTop
            // 
            this.brxmRightTop.BackColor = System.Drawing.Color.White;
            this.brxmRightTop.BorderColor = System.Drawing.Color.Black;
            this.brxmRightTop.CanGrow = false;
            this.brxmRightTop.Dpi = 254F;
            this.brxmRightTop.Font = new System.Drawing.Font("宋体", 9F);
            this.brxmRightTop.ForeColor = System.Drawing.Color.Black;
            this.brxmRightTop.LocationFloat = new DevExpress.Utils.PointFloat(1731.341F, 81.17415F);
            this.brxmRightTop.Name = "brxmRightTop";
            this.brxmRightTop.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.brxmRightTop.StylePriority.UseFont = false;
            this.brxmRightTop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // brxmRightMiddle
            // 
            this.brxmRightMiddle.BackColor = System.Drawing.Color.White;
            this.brxmRightMiddle.BorderColor = System.Drawing.Color.Black;
            this.brxmRightMiddle.CanGrow = false;
            this.brxmRightMiddle.Dpi = 254F;
            this.brxmRightMiddle.Font = new System.Drawing.Font("宋体", 9F);
            this.brxmRightMiddle.ForeColor = System.Drawing.Color.Black;
            this.brxmRightMiddle.LocationFloat = new DevExpress.Utils.PointFloat(1731.341F, 378.3542F);
            this.brxmRightMiddle.Name = "brxmRightMiddle";
            this.brxmRightMiddle.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.brxmRightMiddle.StylePriority.UseFont = false;
            this.brxmRightMiddle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // brxmRightBottom
            // 
            this.brxmRightBottom.BackColor = System.Drawing.Color.White;
            this.brxmRightBottom.BorderColor = System.Drawing.Color.Black;
            this.brxmRightBottom.CanGrow = false;
            this.brxmRightBottom.Dpi = 254F;
            this.brxmRightBottom.Font = new System.Drawing.Font("宋体", 9F);
            this.brxmRightBottom.ForeColor = System.Drawing.Color.Black;
            this.brxmRightBottom.LocationFloat = new DevExpress.Utils.PointFloat(1731.341F, 652.6741F);
            this.brxmRightBottom.Name = "brxmRightBottom";
            this.brxmRightBottom.SizeF = new System.Drawing.SizeF(210.82F, 45.56F);
            this.brxmRightBottom.StylePriority.UseFont = false;
            this.brxmRightBottom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // LshCenter
            // 
            this.LshCenter.BackColor = System.Drawing.Color.White;
            this.LshCenter.BorderColor = System.Drawing.Color.Black;
            this.LshCenter.CanGrow = false;
            this.LshCenter.Dpi = 254F;
            this.LshCenter.Font = new System.Drawing.Font("宋体", 9F);
            this.LshCenter.ForeColor = System.Drawing.Color.Black;
            this.LshCenter.LocationFloat = new DevExpress.Utils.PointFloat(782.32F, 802.64F);
            this.LshCenter.Name = "LshCenter";
            this.LshCenter.SizeF = new System.Drawing.SizeF(350.52F, 35.56F);
            this.LshCenter.StylePriority.UseFont = false;
            this.LshCenter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // XtraReport门诊发票
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportHeader,
            this.PageHeader,
            this.ReportFooter,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(8, 13, 0, 0);
            this.PageHeight = 932;
            this.PageWidth = 2398;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 31.75F;
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel jeLeftTop;
        private DevExpress.XtraReports.UI.XRLabel xmLeftTop;
        private DevExpress.XtraReports.UI.XRLabel xmLeftMiddle;
        private DevExpress.XtraReports.UI.XRLabel jeLeftMiddle;
        private DevExpress.XtraReports.UI.XRLabel xmLeftBottom;
        private DevExpress.XtraReports.UI.XRLabel jeLeftBottom;
        private DevExpress.XtraReports.UI.XRLabel brxmCenter;
        private DevExpress.XtraReports.UI.XRLabel dyrqLeftBottom;
        private DevExpress.XtraReports.UI.XRLabel zdLeftBottom;
        private DevExpress.XtraReports.UI.XRLabel xmCenterTop;
        private DevExpress.XtraReports.UI.XRLabel jeCenterTop;
        private DevExpress.XtraReports.UI.XRLabel xmCenterMiddle;
        private DevExpress.XtraReports.UI.XRLabel jeCenterMiddle;
        private DevExpress.XtraReports.UI.XRLabel xmCenterBottom;
        private DevExpress.XtraReports.UI.XRLabel jeCenterBottom;
        private DevExpress.XtraReports.UI.XRLabel dyrqCenter;
        private DevExpress.XtraReports.UI.XRLabel TotalJeXxCenter;
        private DevExpress.XtraReports.UI.XRLabel skrCenterBottom;
        private DevExpress.XtraReports.UI.XRLabel TotalJeXxLeft;
        private DevExpress.XtraReports.UI.XRLabel jeRightTop;
        private DevExpress.XtraReports.UI.XRLabel xmRightTop;
        private DevExpress.XtraReports.UI.XRLabel xmRightMiddle;
        private DevExpress.XtraReports.UI.XRLabel jeRightMiddle;
        private DevExpress.XtraReports.UI.XRLabel xmRightBottom;
        private DevExpress.XtraReports.UI.XRLabel jeRightBottom;
        private DevExpress.XtraReports.UI.XRLabel skrRightTop;
        private DevExpress.XtraReports.UI.XRLabel dyrqRightTop;
        private DevExpress.XtraReports.UI.XRLabel skrRightMiddle;
        private DevExpress.XtraReports.UI.XRLabel dyrqRightMiddle;
        private DevExpress.XtraReports.UI.XRLabel skrRightBottom;
        private DevExpress.XtraReports.UI.XRLabel dyrqRightBottom;
        private DevExpress.XtraReports.UI.XRLabel brxmLeft;
        private DevExpress.XtraReports.UI.XRLabel brxmRightTop;
        private DevExpress.XtraReports.UI.XRLabel brxmRightMiddle;
        private DevExpress.XtraReports.UI.XRLabel brxmRightBottom;
        private DevExpress.XtraReports.UI.XRLabel LshCenter;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.XRLabel TotalJeDxCenter;
        private DevExpress.XtraReports.UI.XRLabel jeCenterTop2;
        private DevExpress.XtraReports.UI.XRLabel jeCenterMiddle2;
        private DevExpress.XtraReports.UI.XRLabel xmCenterMiddle2;
        private DevExpress.XtraReports.UI.XRLabel xmCenterTop2;
        private DevExpress.XtraReports.UI.XRLabel jeLeftMiddle2;
        private DevExpress.XtraReports.UI.XRLabel xmLeftMiddle2;
        private DevExpress.XtraReports.UI.XRLabel jeLeftTop2;
        private DevExpress.XtraReports.UI.XRLabel xmLeftTop2;

    }
}
