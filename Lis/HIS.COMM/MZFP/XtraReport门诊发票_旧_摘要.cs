﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;


namespace HIS.COMM
{
    public partial class XtraReport门诊发票_旧_摘要 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport门诊发票_旧_摘要(Class发票结构.sct发票格式 _sct发票)
        {
            InitializeComponent();

            xrLabel报销提示.Text =_sct发票.s报销信息;
            this.brxmCenter.Text =_sct发票.s病人姓名;
            this.brxmLeft.Text =_sct发票.s病人姓名;
            this.brxmRightBottom.Text =_sct发票.s病人姓名;
            this.brxmRightMiddle.Text =_sct发票.s病人姓名;
            this.brxmRightTop.Text =_sct发票.s病人姓名;

            this.dyrqCenter.Text =_sct发票.s打印日期;
            this.dyrqLeftBottom.Text =_sct发票.s打印日期;
            this.dyrqRightBottom.Text =_sct发票.s打印日期;
            this.dyrqRightMiddle.Text =_sct发票.s打印日期;
            this.dyrqRightTop.Text =_sct发票.s打印日期;

            this.LshCenter.Text = _sct发票.s流水号;

            this.zdLeftBottom.Text =_sct发票.s制单人;

            this.skrCenterBottom.Text =_sct发票.s收款人;
            this.skrRightBottom.Text =_sct发票.s收款人;
            this.skrRightMiddle.Text =_sct发票.s收款人;
            this.skrRightTop.Text =_sct发票.s收款人;

            string hjXx = Math.Round((Decimal)_sct发票.dtPrint.Compute("sum(收费金额)", ""), 1).ToString();
            string hjDx = WEISHENG.COMM.GetDaxie.GetDaXie(Convert.ToDouble(hjXx), false);

            this.TotalJeXxCenter.Text = hjXx;
            this.TotalJeXxLeft.Text = hjXx;

            this.TotalJeDx.Text = hjDx;

            try
            {
                //按从左到右，从上到下顺序填充
                this.xmLeftTop.Text =_sct发票.dtPrint.Rows[0]["收费名称"].ToString();
                this.xmCenterTop.Text =_sct发票.dtPrint.Rows[0]["收费名称"].ToString();
                this.xmRightTop.Text =_sct发票.dtPrint.Rows[0]["收费名称"].ToString();
                this.jeCenterTop.Text =_sct发票.dtPrint.Rows[0]["收费金额"].ToString();
                this.jeLeftTop.Text =_sct发票.dtPrint.Rows[0]["收费金额"].ToString();
                this.jeRightTop.Text =_sct发票.dtPrint.Rows[0]["收费金额"].ToString();

                this.xmLeftMiddle.Text =_sct发票.dtPrint.Rows[1]["收费名称"].ToString();
                this.xmCenterMiddle.Text =_sct发票.dtPrint.Rows[1]["收费名称"].ToString();
                this.xmRightMiddle.Text =_sct发票.dtPrint.Rows[1]["收费名称"].ToString();
                this.jeLeftMiddle.Text =_sct发票.dtPrint.Rows[1]["收费金额"].ToString();
                this.jeCenterMiddle.Text =_sct发票.dtPrint.Rows[1]["收费金额"].ToString();
                this.jeRightMiddle.Text =_sct发票.dtPrint.Rows[1]["收费金额"].ToString();

                this.xmLeftBottom.Text =_sct发票.dtPrint.Rows[2]["收费名称"].ToString();
                this.xmCenterBottom.Text =_sct发票.dtPrint.Rows[2]["收费名称"].ToString();
                this.xmRightBottom.Text =_sct发票.dtPrint.Rows[2]["收费名称"].ToString();
                this.jeLeftBottom.Text =_sct发票.dtPrint.Rows[2]["收费金额"].ToString();
                this.jeCenterBottom.Text =_sct发票.dtPrint.Rows[2]["收费金额"].ToString();
                this.jeRightBottom.Text =_sct发票.dtPrint.Rows[2]["收费金额"].ToString();

            }
            catch (Exception)
            {

                //   throw;
            }


        }

    }
}
