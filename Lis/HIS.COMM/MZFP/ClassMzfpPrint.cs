﻿using DevExpress.XtraEditors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WEISHENG.COMM;

namespace HIS.COMM
{
    public class ClassMzfpPrint
    {
        private DataTable formatDt = new DataTable("formatDt");
        public ClassMzfpPrint()
        {
            formatDt.Columns.Add("序号", Type.GetType("System.String"));
            formatDt.Columns.Add("收费名称", Type.GetType("System.String"));
            formatDt.Columns.Add("收费金额", Type.GetType("System.Decimal"));
            formatDt.Columns.Add("执行科室", Type.GetType("System.String"));
            formatDt.Columns.Add("数量", Type.GetType("System.Decimal"));
            formatDt.Columns.Add("单价", Type.GetType("System.Decimal"));
            formatDt.Columns.Add("药品序号", Type.GetType("System.Int64"));
            formatDt.Columns.Add("YPID", Type.GetType("System.Decimal"));
            formatDt.Columns.Add("大项名称", Type.GetType("System.String"));
        }

        private string _connstring;

        public string connstring
        {
            get { return _connstring; }
            set { _connstring = value; }
        }
        private bool _b是否发票号管理;

        public bool B是否发票号管理
        {
            get { return _b是否发票号管理; }
            set { _b是否发票号管理 = value; }
        }



        private Class发票结构.en门诊发票打印模式 _en门诊发票打印模式;

        public Class发票结构.en门诊发票打印模式 en门诊发票打印模式
        {
            get { return _en门诊发票打印模式; }
            set { _en门诊发票打印模式 = value; }
        }

        private Class发票结构.b是否重印 _b是否重印;

        public Class发票结构.b是否重印 b是否重印
        {
            get { return _b是否重印; }
            set { _b是否重印 = value; }
        }

        private DataTable _sourceDt;
        public DataTable sourceDt
        {
            get { return _sourceDt; }
            set { _sourceDt = value; }
        }

        private HIS.COMM.personYB _person;
        public HIS.COMM.personYB person
        {
            get { return _person; }
            set { _person = value; }
        }

        private Class发票结构.sct发票格式 _sct发票格式;

        public Class发票结构.sct发票格式 sct发票格式
        {
            get { return _sct发票格式; }
            set { _sct发票格式 = value; }
        }


        //医保接口数据转换为打印格式
        public void makeFormatWith医保门诊接口()
        {
            foreach (DataRow row in sourceDt.Rows)
            {
                DataRow r2 = formatDt.NewRow();
                r2["序号"] = row["医院内部编码"];
                r2["收费名称"] = row["医院内部名称"];
                r2["收费金额"] = row["金额"];
                r2["执行科室"] = row["金额"];
                r2["数量"] = row["数量"];
                r2["单价"] = row["单价"];
                r2["药品序号"] = row["ID"];
                r2["YPID"] = row["ID"];
                r2["大项名称"] = row["医院分类名"];
                formatDt.Rows.Add(r2);
            }
        }

        //药店手工录入格式转换为打印发票格式
        public void makeFormatWith药店处方内容()
        {
            foreach (DataRow row in sourceDt.Rows)
            {
                DataRow r2 = formatDt.NewRow();
                r2["序号"] = row["药品序号"];
                r2["收费名称"] = row["药品名称"];
                r2["收费金额"] = row["金额"];
                r2["执行科室"] = row["金额"];
                r2["数量"] = row["数量"];
                r2["单价"] = row["单价"];
                r2["药品序号"] = row["药品序号"];
                r2["YPID"] = row["ID"];
                r2["大项名称"] = row["药品名称"];
                formatDt.Rows.Add(r2);
            }
        }

        public void toPrint(bool _b是否打印报销信息, bool _b是否预览)
        {
            //构建门诊发票通用信息     
            string s报销信息 = "";

            int i总行数 = formatDt.Rows.Count;
            int i单据张数 = 0;
            int i当前行号 = 0;
            int i当前页号 = 1;

            switch (_en门诊发票打印模式)
            {
                case Class发票结构.en门诊发票打印模式.老格式_摘要:
                default:
                    #region RegionName
                    s报销信息 = "";//发票报销信息
                    i总行数 = formatDt.Rows.Count;
                    i单据张数 = (i总行数 + 2) / 3;
                    i当前行号 = 0;
                    i当前页号 = 1;
                    if (Convert.ToDecimal(_person.reDec统筹支付) != 0)
                    {
                        s报销信息 = "单据:" + i单据张数.ToString() + "张,总费用:" + _person.decMz处方总费用 + ",应报费用:" + _person.dec应报费用 + "\r\n报销金额:" + _person.reDec统筹支付 + " 报销人签字:";
                    }

                    DataTable _dtToPrint = formatDt.Clone();
                    foreach (DataRow row in formatDt.Rows)
                    {
                        i当前行号 += 1;
                        i当前页号 = (i当前行号 + 2) / 3;
                        _dtToPrint.Rows.Add(row.ItemArray);
                        if ((i当前行号 % 3 == 0) || i总行数 == i当前行号)
                        {
                            _sct发票格式.s报销信息 = i当前页号 == i单据张数 ? s报销信息 : "";
                            _sct发票格式.dtPrint = _dtToPrint;
                            XtraReport门诊发票_旧_摘要 mzfp = new XtraReport门诊发票_旧_摘要(_sct发票格式);

                            switch (_sct发票格式.En医保类型)
                            {
                                case ClassEnum.en收款类型.城镇职工:
                                case ClassEnum.en收款类型.城乡居民:
                                    xtraReportShow.show(mzfp, "门诊发票", true, _connstring);
                                    break;
                                case ClassEnum.en收款类型.现金自费:
                                case ClassEnum.en收款类型.账户冲减:
                                default:
                                    xtraReportShow.printToPaper(mzfp, "门诊发票", _connstring);
                                    break;
                            }
                            //写mf发票使用记录

                            updata发票使用记录();
                            mzfp.Dispose();
                            _dtToPrint.Rows.Clear();
                        }
                    }
                    break;
                #endregion
                case Class发票结构.en门诊发票打印模式.老格式_明细:
                    #region RegionName
                    try
                    {
                        DataTable dt处方内容 = formatDt;
                        if (dt处方内容.Rows.Count == 0)
                        {
                            XtraMessageBox.Show("当前无可打印数据", "确认", MessageBoxButtons.OK, MessageBoxIcon.Question);
                            return;
                        }

                        //DataRow[] rows = dt处方内容.Select("金额=0");
                        //for (int i = 0; i < rows.Length; i++)
                        //{
                        //    dt处方内容.Rows.Remove(rows[i]);
                        //}
                        //Int64 CurrentFphm = Convert.ToInt64(this.textEdit医院单据号.Text);                
                        decimal de账户冲减金额 = 0;// person.decMz账户冲减金额;//卡金支付(剩余)，当页发票打印后，还有多少卡金支付的算法               
                        decimal de统筹支付金额 = 0;// person.reDec统筹支付;//门诊统筹支付
                        decimal de当页卡金支付金额 = 0;//当页卡金支付
                        decimal de当页统筹支付金额 = 0;
                        decimal de当页账户余额 = 0;// person.decAccount账户余额;//刷卡前余额(动态)，根据每页发票的卡金支付动态减少
                        //构建打印datatable
                        try
                        {
                            dt处方内容.Columns.Add(new DataColumn("cPosition", typeof(string)));
                            dt处方内容.Columns.Add(new DataColumn("lPosition", typeof(string)));
                            dt处方内容.Columns.Add(new DataColumn("pages", typeof(string)));
                        }
                        catch (Exception ee)
                        {
                            //   throw;
                        }
                        Boolean b末页标志 = false;
                        DataTable dt单页输出内容 = dt处方内容.Clone();
                        int CurrentRow = 0;
                        int cPosition = 1;
                        int CurrentPage = 0;
                        ArrayList arrLPos = new ArrayList();//左边位置3项判断
                        for (int i = 0; i < dt处方内容.Rows.Count; i++)
                        {
                            Boolean b打印标志 = false;//打印输出标记
                            CurrentRow += 1;
                            cPosition = cPosition + 1;//发票中间位置标号
                            int iLpos = arrLPos.IndexOf(dt处方内容.Rows[i]["大项名称"].ToString());

                            //增加页码判断
                            if (iLpos == -1)//根据左边位置是否超过3项判断是否增加新页和打印输出
                            {
                                arrLPos.Add(dt处方内容.Rows[i]["大项名称"].ToString());
                                if (arrLPos.Count == 4)
                                {
                                    CurrentPage = CurrentPage + 1;
                                    cPosition = 1;
                                    arrLPos.Clear();
                                    b打印标志 = true;
                                }
                            }
                            if (cPosition == 6)//根据左边位置是否超过5项判断是否增加新页和打印输出
                            {
                                CurrentPage = CurrentPage + 1;
                                cPosition = 1;
                                arrLPos.Clear();
                                b打印标志 = true;
                            }
                            if (i == dt处方内容.Rows.Count - 1)//当最后一行时直接打印
                            {
                                b打印标志 = true;
                                b末页标志 = true;
                            }
                            dt处方内容.Rows[i]["pages"] = CurrentPage;
                            dt处方内容.Rows[i]["cPosition"] = cPosition;
                            dt单页输出内容.Rows.Add(dt处方内容.Rows[i].ItemArray);
                            if (b打印标志 == true)
                            {
                                try
                                {
                                    //构建输出格式内容
                                    DataTable dt格式化输出内容 = new DataTable();
                                    dt格式化输出内容.Columns.Add(new DataColumn("费用名称", typeof(string)));
                                    dt格式化输出内容.Columns.Add(new DataColumn("分类名称", typeof(string)));
                                    dt格式化输出内容.Columns.Add(new DataColumn("金额", typeof(decimal)));
                                    foreach (DataRow row in dt单页输出内容.Rows)
                                    {
                                        DataRow dtpRow = dt格式化输出内容.NewRow();
                                        dtpRow["费用名称"] = row["收费名称"];
                                        dtpRow["分类名称"] = row["大项名称"];
                                        dtpRow["金额"] = row["收费金额"];
                                        dt格式化输出内容.Rows.Add(dtpRow);
                                    }
                                    string fpsj = DateTime.Now.ToShortDateString();// person.dtMz刷卡时间.ToString();
                                    decimal de本页金额小计 = Convert.ToDecimal(dt格式化输出内容.Compute("sum(金额)", ""));//本页小计
                                    if (de账户冲减金额 >= de本页金额小计)
                                    {
                                        de当页卡金支付金额 = de本页金额小计;
                                        de账户冲减金额 = de账户冲减金额 - de本页金额小计;
                                    }
                                    else
                                    {
                                        de当页卡金支付金额 = de账户冲减金额;
                                        de账户冲减金额 = 0;
                                    }
                                    de当页账户余额 = de当页账户余额 - de当页卡金支付金额;
                                    string s本页摘要 = "";
                                    if (b末页标志 == true)
                                    {
                                        s本页摘要 = "";
                                        //"本页小计：" + de本页金额小计.ToString() + "卡金支付：" + de当页卡金支付金额.ToString() + "账户余额:" + de当页账户余额.ToString() +
                                        //"\r\n合计卡金支付：" + person.decMz账户冲减金额.ToString() + "合计统筹支付：" + person.reDec统筹支付.ToString() +
                                        //"合计个人自负：" + person.decMz个人自负.ToString();//摘要提示
                                    }
                                    else
                                    {
                                        s本页摘要 = "";// "本页小计：" + de本页金额小计.ToString() + "卡金支付：" + de当页卡金支付金额.ToString() + "账户余额:" + de当页账户余额.ToString();//摘要提示
                                    }
                                    _sct发票格式.s报销信息 = s本页摘要;
                                    _sct发票格式.dtPrint = dt格式化输出内容;
                                    XtraReport门诊发票_旧_明细 mzfp = new XtraReport门诊发票_旧_明细(_sct发票格式);

                                    //string sfyldy = "1";
                                    //try
                                    //{
                                    //    sfyldy = HIS.Model.Dal.SqlHelper.ExecuteScalar(Properties.Settings.Default.ybjkConnectionString, CommandType.Text,
                                    //                        " SELECT convert(int,是否打印预览) FROM GY打印设置 where 单位编码=" + baseInfo.pHospitalCode + " and 打印模块='门诊发票'").ToString();
                                    //}
                                    //catch (Exception)
                                    //{
                                    //    //throw;
                                    //}

                                    //if (sfyldy == "0")//是否预览打印=0直接输出到打印机
                                    //{
                                    //    xtraReportShow.printToPaper(mzfp, "门诊发票", true, Properties.Settings.Default.ybjkConnectionString, baseInfo.pHospitalCode);
                                    //}
                                    //else
                                    //{
                                    xtraReportShow.show(mzfp, "门诊发票", true, _connstring);
                                    //}

                                    mzfp.Dispose();
                                    b打印标志 = false;
                                    dt单页输出内容.Clear();
                                    dt格式化输出内容.Dispose();
                                }
                                catch (Exception ee)
                                {
                                    throw;
                                }

                            }


                        }
                    }
                    catch (Exception ee)
                    {
                        XtraMessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //  throw;
                    }
                    #endregion
                    break;
                case Class发票结构.en门诊发票打印模式.新格式_明细:
                    //todo:显示报销金额部分还要处理
                    #region RegionName
                    s报销信息 = "";//发票报销信息
                    i总行数 = formatDt.Rows.Count;
                    i单据张数 = (i总行数 + 19) / 20;
                    i当前行号 = 0;
                    i当前页号 = 1;
                    //todo:更改判断方式
                    switch (_person.En医保类型)
                    {
                        case ClassEnum.en收款类型.城乡居民:
                        //s报销信息 = "单据:" + i单据张数.ToString() + "张,总费用:" + _person.dec总费用 + ",应报费用:" +
                        //    _person.dec应报费用 + ",收现金:" + _person.decMz个人自负 + ",报销金额:" + _person.reDec统筹支付 + " 报销人签字:";
                        //_b是否打印报销信息 = true;
                        //break;
                        case ClassEnum.en收款类型.城镇职工:
                            person.decMz医保返回个人自负 = person.decMz处方总费用 - person.decMz医保卡银行账户冲减金额 - person.reDec统筹支付;
                            s报销信息 = "支付前余额:" + person.decAccount账户余额.ToString() +
                            " 总费用:" + person.decMz处方总费用.ToString() +
                            " 总卡金支付:" + person.decMz医保卡银行账户冲减金额.ToString() +
                            " 总统筹支付:" + person.reDec统筹支付.ToString() +
                            " 总个人自负:" + person.decMz本地计算自负.ToString() +
                            " 当前余额:" + person.decMz冲减后余额.ToString();
                            _b是否打印报销信息 = true;
                            break;
                        case ClassEnum.en收款类型.账户冲减:
                        case ClassEnum.en收款类型.现金自费:
                        default:
                            _b是否打印报销信息 = false;
                            break;
                    }

                    DataTable _dtToPrint4 = formatDt.Clone();
                    foreach (DataRow row in formatDt.Rows)
                    {
                        i当前行号 += 1;
                        i当前页号 = (i当前行号 + 19) / 20;
                        _dtToPrint4.Rows.Add(row.ItemArray);
                        if ((i当前行号 % 20 == 0) || i总行数 == i当前行号)
                        {
                            _sct发票格式.s报销信息 = i当前页号 == i单据张数 ? s报销信息 : "见末页";
                            _sct发票格式.dtPrint = _dtToPrint4;
                            _sct发票格式.s合计小写 = Math.Round((Decimal)_sct发票格式.dtPrint.Compute("sum(收费金额)", ""), 1).ToString();
                            _sct发票格式.s合计大写 = WEISHENG.COMM.GetDaxie.GetDaXie(Convert.ToDouble(_sct发票格式.s合计小写), false);

                            XtraReport门诊发票_新_摘要 mzfp_new = new XtraReport门诊发票_新_摘要(_sct发票格式, _b是否打印报销信息);
                            //switch (_sct发票格式.En医保类型)
                            //{
                            //    case ClassEnum.en医保类型.城镇职工:
                            //    case ClassEnum.en医保类型.城乡居民:
                            //        xtraReportShow.show(mzfp_new, "门诊发票_新格式", true, _connstring);
                            //        break;
                            //    case ClassEnum.en医保类型.现金自费:
                            //    case ClassEnum.en医保类型.账户冲减:
                            //    default:
                            if (_b是否预览)
                            {
                                xtraReportShow.show(mzfp_new, "门诊发票_新格式", true, _connstring);
                            }
                            else
                            {
                                xtraReportShow.printToPaper(mzfp_new, "门诊发票_新格式", _connstring);
                            }
                            //        break;
                            //}
                            updata发票使用记录();
                            mzfp_new.Dispose();
                            _dtToPrint4.Rows.Clear();
                        }
                    }
                    #endregion
                    break;
                case Class发票结构.en门诊发票打印模式.新格式_摘要:
                    #region RegionName
                    s报销信息 = "";//发票报销信息
                    i总行数 = formatDt.Rows.Count;
                    i单据张数 = (i总行数 + 19) / 20;
                    i当前行号 = 0;
                    i当前页号 = 1;
                    //todo:还需要分析门诊返回的传
                    if (Convert.ToDecimal(_person.reDec统筹支付) != 0)
                    {
                        s报销信息 = "单据:" + i单据张数.ToString() + "张,总费用:" + _person.decMz处方总费用 + ",应报费用:" +
                            _person.dec应报费用 + ",收现金:" + _person.decMz医保返回个人自负 + ",报销金额:" + _person.reDec统筹支付 + " 报销人签字:";
                    }
                    if (_person.decMz医保卡银行账户冲减金额 != 0 || _person.reDec统筹支付 != 0)
                    {
                        s报销信息 = "支付前余额:" + person.decAccount账户余额.ToString() +
                            //" 总费用:" + person.decMz处方总费用.ToString() +
                            " 总卡金支付:" + person.decMz医保卡银行账户冲减金额.ToString() +
                            //" 总统筹支付:" + person.reDec统筹支付.ToString() +
                            //" 总个人自负:" + person.decMz本地计算自负.ToString() +
                            " 当前余额:" + person.decMz冲减后余额.ToString();
                    }


                    DataTable _dtToPrint新摘 = formatDt.Clone();
                    foreach (DataRow row in formatDt.Rows)
                    {
                        i当前行号 += 1;
                        i当前页号 = (i当前行号 + 19) / 20;
                        _dtToPrint新摘.Rows.Add(row.ItemArray);
                        if ((i当前行号 % 20 == 0) || i总行数 == i当前行号)
                        {
                            _sct发票格式.s报销信息 = i当前页号 == i单据张数 ? s报销信息 : "见末页";
                            _sct发票格式.dtPrint = _dtToPrint新摘;
                            _sct发票格式.s合计小写 = Math.Round((Decimal)_sct发票格式.dtPrint.Compute("sum(收费金额)", ""), 1).ToString();
                            _sct发票格式.s合计大写 = WEISHENG.COMM.GetDaxie.GetDaXie(Convert.ToDouble(_sct发票格式.s合计小写), false);

                            XtraReport门诊发票_新_摘要 mzfp_new = new XtraReport门诊发票_新_摘要(_sct发票格式, _b是否打印报销信息);
                            switch (_sct发票格式.En医保类型)
                            {
                                case ClassEnum.en收款类型.城镇职工:
                                case ClassEnum.en收款类型.城乡居民:
                                    xtraReportShow.show(mzfp_new, "门诊发票_新格式", true, _connstring);
                                    break;
                                case ClassEnum.en收款类型.现金自费:
                                case ClassEnum.en收款类型.账户冲减:
                                default:
                                    xtraReportShow.printToPaper(mzfp_new, "门诊发票_新格式", _connstring);
                                    //       xtraReportShow.show(mzfp_new, "门诊发票_新格式", true, _connstring);
                                    break;
                            }
                            updata发票使用记录();
                            mzfp_new.Dispose();
                            _dtToPrint新摘.Rows.Clear();
                        }
                    }
                    #endregion
                    break;
                case Class发票结构.en门诊发票打印模式.药店收据:
                    #region RegionName
                    s报销信息 = "";//发票报销信息
                    i总行数 = formatDt.Rows.Count;
                    i单据张数 = (i总行数 + 19) / 20;
                    i当前行号 = 0;
                    i当前页号 = 1;

                    if (Convert.ToDecimal(_person.reDec统筹支付) != 0)
                    {
                        s报销信息 = "单据:" + i单据张数.ToString() + "张,总费用:" + _person.decMz处方总费用 + ",应报费用:" + _person.dec应报费用 + ",收现金:" + _person.decMz医保返回个人自负 + ",报销金额:" + _person.reDec统筹支付 + " 报销人签字:";
                    }
                    if (_person.decMz医保卡银行账户冲减金额 != 0 || _person.reDec统筹支付 != 0)
                    {
                        s报销信息 = "支付前余额:" + person.decAccount账户余额.ToString() +
                            " 总费用:" + person.decMz处方总费用.ToString() +
                            " 总卡金支付:" + person.decMz医保卡银行账户冲减金额.ToString() +
                            " 总统筹支付:" + person.reDec统筹支付.ToString() +
                            " 总个人自负:" + person.decMz本地计算自负.ToString() +
                            " 当前余额:" + person.decMz冲减后余额.ToString();
                    }


                    DataTable _dtToPrintYD = formatDt.Clone();
                    foreach (DataRow row in formatDt.Rows)
                    {
                        i当前行号 += 1;
                        i当前页号 = (i当前行号 + 19) / 20;
                        _dtToPrintYD.Rows.Add(row.ItemArray);
                        if ((i当前行号 % 20 == 0) || i总行数 == i当前行号)
                        {
                            _sct发票格式.s报销信息 = i当前页号 == i单据张数 ? s报销信息 : "见末页";
                            _sct发票格式.dtPrint = _dtToPrintYD;
                            _sct发票格式.s合计小写 = Math.Round((Decimal)_sct发票格式.dtPrint.Compute("sum(收费金额)", ""), 1).ToString();
                            _sct发票格式.s合计大写 = WEISHENG.COMM.GetDaxie.GetDaXie(Convert.ToDouble(_sct发票格式.s合计小写), false);

                            XtraReport门诊发票_药店 mzfp_new = new XtraReport门诊发票_药店(_sct发票格式, _b是否打印报销信息, b是否重印);
                            xtraReportShow.show(mzfp_new, "门诊发票_新格式", true, _connstring);
                            //switch (_sct发票格式.En医保类型)
                            //{
                            //    case ClassEnum.en医保类型.城镇职工:
                            //    case ClassEnum.en医保类型.城镇居民:
                            //        xtraReportShow.show(mzfp_new, "门诊发票_新格式", true, _connstring);
                            //        break;
                            //    case ClassEnum.en医保类型.新农合:
                            //    case ClassEnum.en医保类型.现金自费:
                            //    case ClassEnum.en医保类型.账户冲减:
                            //    default:
                            //        xtraReportShow.printToPaper(mzfp_new, "门诊发票_新格式", _connstring);
                            //        break;
                            //}
                            mzfp_new.Dispose();
                            _dtToPrintYD.Rows.Clear();
                        }
                    }
                    #endregion
                    break;
            }
        }

        private void updata发票使用记录()
        {
            if (_sct发票格式.i当前发票号 == 0)//未设置发票号限制的不更新发票号
            {
                return;
            }
            if (HIS.Model.Dal.SqlHelper.ExecuteNonQuery(_connstring, CommandType.Text, "insert into mf发票使用记录 (发票号码,mzid,发票费用,mac地址) values (" + _sct发票格式.i当前发票号.ToString() + "," + _person.sMZID + "," + formatDt.Compute("sum(收费金额)", "").ToString() + ",'" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "')") != 1)
            {
                XtraMessageBox.Show("更新票据使用记录异常", "数据保存失败", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            //更新一次票据号
            if (HIS.Model.Dal.SqlHelper.ExecuteNonQuery(_connstring, CommandType.Text, "update GY票据设置 set 当前号码='" + (_sct发票格式.i当前发票号 + 1).ToString() + "' where Mac地址='" + WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress + "' and 票据类型='门诊发票' and 是否当前号段=1") == 0)
            {
                XtraMessageBox.Show("更新单据号异常", "数据保存失败", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            _sct发票格式.i当前发票号 += 1;
        }
        public void makeFormatWith门诊收款()//新的生产门诊发票格式数据
        {
            //如果是摘要模式，再根据明细重新汇总到大类 
            foreach (DataRow row in _sourceDt.Rows)
            {
                makeFormatWith门诊收款(row, ref formatDt);
            }
        }

        private string get归并编码(int sfbm)
        {
            string xmbm = HIS.Model.Dal.SqlHelper.ExecuteScalar(_connstring, CommandType.Text, "select 归并编码 from GY收费小项 where 收费编码=" + sfbm).ToString();
            return xmbm;
        }
        private string get归并名称(int sfbm)
        {
            string xmbm = HIS.Model.Dal.SqlHelper.ExecuteScalar(_connstring, CommandType.Text, "select 归并名称 from GY收费小项 where 收费编码=" + sfbm).ToString();
            return xmbm;
        }


        private void makeFormatWith门诊收款(DataRow row, ref DataTable _dtToPrint)
        {
            try
            {
                string s费用序号 = row["费用序号"].ToString();
                int i小项编码 = Convert.ToInt32(s费用序号.Replace("A", ""));
                string s费用名称 = row["费用名称"].ToString();
                string s药品序号 = row["药品序号"].ToString();
                string s大项编码 = get归并编码(i小项编码);
                string s大项名称 = get归并名称(i小项编码);
                string s规格 = row["规格"].ToString();
                string s单位 = row["单位"].ToString();
                string s单价 = row["单价"].ToString();
                decimal dec数量 = Convert.ToDecimal(row["数量"]);
                decimal dec金额 = Convert.ToDecimal(row["金额"]);
                string sYPID = row["YPID"].ToString();   //因为将来要区分医生处方，会出现处方中重复的YPID记录，所以判断YPID    

                switch (_en门诊发票打印模式)
                {
                    case Class发票结构.en门诊发票打印模式.老格式_摘要:
                        if (_dtToPrint.Select("序号='" + s大项编码 + "'").Length == 0)
                        {
                            DataRow r2 = _dtToPrint.NewRow();
                            r2["序号"] = s大项编码;
                            r2["收费名称"] = s大项名称;
                            r2["收费金额"] = Math.Round(dec金额, 2);
                            _dtToPrint.Rows.Add(r2);
                        }
                        else
                        {
                            DataRow r2 = _dtToPrint.Select("序号='" + s大项编码 + "'")[0];
                            r2["收费金额"] = Math.Round((decimal)r2["收费金额"] + dec金额, 2);
                            _dtToPrint.AcceptChanges();
                        }
                        break;
                    case Class发票结构.en门诊发票打印模式.老格式_明细:

                    case Class发票结构.en门诊发票打印模式.新格式_明细://药品打印明细，收费打印摘要
                        if (Convert.ToDecimal(sYPID) != 0)
                        {
                            if (_dtToPrint.Select("YPID=" + sYPID).Length == 0)
                            {
                                DataRow r2 = _dtToPrint.NewRow();
                                r2["序号"] = s大项编码;
                                r2["药品序号"] = s药品序号;
                                r2["收费名称"] = s费用名称 + "/" + s规格 + "/" + s单位;
                                r2["数量"] = dec数量;
                                r2["单价"] = s单价;
                                r2["收费金额"] = dec金额;
                                r2["YPID"] = sYPID;
                                r2["大项名称"] = s大项名称;
                                _dtToPrint.Rows.Add(r2);
                            }
                            else
                            {
                                DataRow r2 = _dtToPrint.Select("YPID=" + sYPID)[0];
                                r2["数量"] = dec数量 + Convert.ToDecimal(r2["数量"]);
                                r2["单价"] = s单价;
                                r2["收费金额"] = dec金额 + Convert.ToDecimal(r2["收费金额"]);
                                _dtToPrint.AcceptChanges();
                            }
                        }
                        else//收费项目打印明细
                        {
                            if (_dtToPrint.Select("序号='" + s大项编码 + "'").Length == 0)
                            {
                                DataRow r2 = _dtToPrint.NewRow();
                                r2["序号"] = s费用序号;//s大项编码;
                                r2["收费名称"] = s费用名称;//s大项名称;
                                r2["收费金额"] = Math.Round(dec金额, 2);
                                r2["大项名称"] = s大项名称;
                                _dtToPrint.Rows.Add(r2);
                            }
                            else
                            {
                                DataRow r2 = _dtToPrint.Select("序号='" + s大项编码 + "'")[0];
                                r2["收费金额"] = Math.Round((decimal)r2["收费金额"] + dec金额, 2);
                                _dtToPrint.AcceptChanges();
                            }

                        }
                        break;
                    case Class发票结构.en门诊发票打印模式.新格式_摘要: //药品打印摘要，收费打印摘要

                        if (_dtToPrint.Select("序号='" + s大项编码 + "'").Length == 0)  //todo:当潘峰先录入  2个西药，一个检查后，再录入的 中药就会重复增加
                        {
                            DataRow r2 = _dtToPrint.NewRow();
                            r2["序号"] = s大项编码;
                            r2["收费名称"] = s大项名称;
                            r2["收费金额"] = Math.Round(dec金额, 2);
                            r2["大项名称"] = s大项名称;
                            _dtToPrint.Rows.Add(r2);
                        }
                        else
                        {
                            DataRow r2 = _dtToPrint.Select("序号='" + s大项编码 + "'")[0];
                            r2["收费金额"] = Math.Round((decimal)r2["收费金额"] + dec金额, 2);
                            _dtToPrint.AcceptChanges();
                        }
                        break;
                    default:
                        break;
                }

                _dtToPrint.AcceptChanges();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        //医保接口老格式发票打印
        public void mzfpToPrint(string connDbString)
        {
            try
            {
                #region 打印业务
                DataTable dt处方内容 = sourceDt;
                if (dt处方内容.Rows.Count == 0)
                {
                    XtraMessageBox.Show("当前无可打印数据", "确认", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    return;
                }

                DataRow[] rows = dt处方内容.Select("金额=0");
                for (int i = 0; i < rows.Length; i++)
                {
                    dt处方内容.Rows.Remove(rows[i]);
                }
                //Int64 CurrentFphm = Convert.ToInt64(this.textEdit医院单据号.Text);                
                decimal de账户冲减金额 = person.decMz医保卡银行账户冲减金额;//卡金支付(剩余)，当页发票打印后，还有多少卡金支付的算法               
                decimal de统筹支付金额 = person.reDec统筹支付;//门诊统筹支付
                decimal de当页卡金支付金额 = 0;//当页卡金支付
                decimal de当页统筹支付金额 = 0;
                decimal de当页账户余额 = person.decAccount账户余额;//刷卡前余额(动态)，根据每页发票的卡金支付动态减少
                //构建打印datatable
                try
                {
                    dt处方内容.Columns.Add(new DataColumn("cPosition", typeof(string)));
                    dt处方内容.Columns.Add(new DataColumn("lPosition", typeof(string)));
                    dt处方内容.Columns.Add(new DataColumn("pages", typeof(string)));
                }
                catch (Exception ee)
                {
                    //   throw;
                }
                Boolean b末页标志 = false;
                DataTable dt单页输出内容 = dt处方内容.Clone();
                //int RowCount = dtCfnr.Rows.Count;
                int CurrentRow = 0;
                int cPosition = 1;
                //int lPosition = 0;
                int CurrentPage = 0;
                ArrayList arrLPos = new ArrayList();//左边位置3项判断
                for (int i = 0; i < dt处方内容.Rows.Count; i++)
                {
                    Boolean b打印标志 = false;//打印输出标记
                    CurrentRow += 1;
                    cPosition = cPosition + 1;//发票中间位置标号
                    int iLpos = arrLPos.IndexOf(dt处方内容.Rows[i]["医院分类名"].ToString());

                    //增加页码判断
                    if (iLpos == -1)//根据左边位置是否超过3项判断是否增加新页和打印输出
                    {
                        arrLPos.Add(dt处方内容.Rows[i]["医院分类名"].ToString());
                        if (arrLPos.Count == 4)
                        {
                            CurrentPage = CurrentPage + 1;
                            cPosition = 1;
                            arrLPos.Clear();
                            b打印标志 = true;
                        }
                    }
                    if (cPosition == 6)//根据左边位置是否超过5项判断是否增加新页和打印输出
                    {
                        CurrentPage = CurrentPage + 1;
                        cPosition = 1;
                        arrLPos.Clear();
                        b打印标志 = true;
                    }
                    if (i == dt处方内容.Rows.Count - 1)//当最后一行时直接打印
                    {
                        b打印标志 = true;
                        b末页标志 = true;
                    }
                    dt处方内容.Rows[i]["pages"] = CurrentPage;
                    dt处方内容.Rows[i]["cPosition"] = cPosition;
                    dt单页输出内容.Rows.Add(dt处方内容.Rows[i].ItemArray);
                    if (b打印标志 == true)
                    {
                        try
                        {
                            //构建输出格式内容
                            DataTable dt格式化输出内容 = new DataTable();
                            dt格式化输出内容.Columns.Add(new DataColumn("费用名称", typeof(string)));
                            dt格式化输出内容.Columns.Add(new DataColumn("分类名称", typeof(string)));
                            dt格式化输出内容.Columns.Add(new DataColumn("金额", typeof(decimal)));
                            foreach (DataRow row in dt单页输出内容.Rows)
                            {
                                DataRow dtpRow = dt格式化输出内容.NewRow();
                                dtpRow["费用名称"] = row["医院内部名称"];
                                dtpRow["分类名称"] = row["医院分类名"];
                                dtpRow["金额"] = row["金额"];
                                dt格式化输出内容.Rows.Add(dtpRow);
                            }
                            string fpsj = person.dtMz刷卡时间.ToString();
                            decimal de本页金额小计 = Convert.ToDecimal(dt格式化输出内容.Compute("sum(金额)", ""));//本页小计
                            if (de账户冲减金额 >= de本页金额小计)
                            {
                                de当页卡金支付金额 = de本页金额小计;
                                de账户冲减金额 = de账户冲减金额 - de本页金额小计;
                            }
                            else
                            {
                                de当页卡金支付金额 = de账户冲减金额;
                                de账户冲减金额 = 0;
                            }
                            de当页账户余额 = de当页账户余额 - de当页卡金支付金额;

                            string s本页摘要 = "";
                            if (b末页标志 == true)
                            {
                                s本页摘要 = "本页小计：" + de本页金额小计.ToString() + "卡金支付：" + de当页卡金支付金额.ToString() + "账户余额:" + de当页账户余额.ToString() +
                                    "\r\n合计卡金支付：" + person.decMz医保卡银行账户冲减金额.ToString() + "合计统筹支付：" + person.reDec统筹支付.ToString() +
                                    "合计个人自负：" + person.decMz医保返回个人自负.ToString();//摘要提示
                            }
                            else
                            {
                                s本页摘要 = "本页小计：" + de本页金额小计.ToString() + "卡金支付：" + de当页卡金支付金额.ToString() + "账户余额:" + de当页账户余额.ToString();//摘要提示
                            }

                            XtraReport门诊发票 mzfp = new XtraReport门诊发票(person.S姓名, fpsj, person.S医院流水号,
                            HIS.COMM.zdInfo.ModelUserInfo.用户名, HIS.COMM.zdInfo.ModelUserInfo.用户名, dt格式化输出内容, s本页摘要);

                            string sfyldy = "1";
                            try
                            {
                                sfyldy = HIS.Model.Dal.SqlHelper.ExecuteScalar(connDbString, CommandType.Text,
                                                    " SELECT convert(int,是否打印预览) FROM GY打印设置 where 单位编码=" + HIS.COMM.zdInfo.Model科室信息.单位编码.ToString() + " and 打印模块='门诊发票'").ToString();
                            }
                            catch (Exception)
                            {
                                //throw;
                            }

                            if (sfyldy == "0")//是否预览打印=0直接输出到打印机
                            {
                                xtraReportShow.show(mzfp, "门诊发票", true, connDbString, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                            }
                            else
                            {
                                xtraReportShow.show(mzfp, "门诊发票", true, connDbString, HIS.COMM.zdInfo.Model科室信息.单位编码.ToString());
                            }

                            mzfp.Dispose();
                            b打印标志 = false;
                            dt单页输出内容.Clear();
                            dt格式化输出内容.Dispose();
                        }
                        catch (Exception ee)
                        {
                            throw;
                        }

                    }


                }

                #endregion
            }
            catch (Exception ee)
            {
                XtraMessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


    }
}
