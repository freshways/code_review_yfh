﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace HIS.COMM
{
    public partial class XtraReport门诊发票_药店 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport门诊发票_药店(Class发票结构.sct发票格式 _sct发票,bool b是否打印报销信息,Class发票结构.b是否重印 _b是否重印)
        {
            InitializeComponent();
            this.DataSource = _sct发票.dtPrint;
            label流水号.Text = _sct发票.s流水号;
            xrLabel医疗机构类型.Text = _sct发票.s医疗机构类型;
            xrLabel校验码.Text = _sct发票.s校验码;
            label姓名.Text = _sct发票.s病人姓名;
            xrLabel性别.Text = _sct发票.s性别;
            xrLabel医保类型.Text = _sct发票.En医保类型.ToString();
            xrLabel社会保障号码.Text = _sct发票.s社会保障号码;
            xrLabel合计大写.Text = _sct发票.s合计大写;
            xrLabel合计小写.Text = _sct发票.s合计小写;
            xrLabel医保统筹支付.Text = _sct发票.s医保统筹支付;
            xrLabel个人账户支付.Text = _sct发票.s个人账户支付;
            xrLabel其他医保支付.Text = _sct发票.s其他医保支付;
            xrLabel个人支付金额.Text = _sct发票.s个人支付金额;
            xrLabel收款单位.Text = _sct发票.s收款单位;
            xrLabel收款人.Text = _sct发票.s收款人;
            xrLabel年月日.Text = _sct发票.s年月日;
            xrLabel报销信息.Text = _sct发票.s报销信息;
            if (_b是否重印 == Class发票结构.b是否重印.是) xrLabel标题.Text = "临沂市定点药店收费单据(重印)";
            
            if (b是否打印报销信息 == true)
            {
                //医保接口调用  true
                xrLabel合计大写.Visible = false;
                xrLabel合计小写.Visible = false;
                xrLabel报销信息.Visible = true;
            }
            else
            {
                //门诊收款调用  false;
            }

        }

    }
}




//label流水号.Text = _sct发票.s流水号;
//xrLabel医疗机构类型.Text = _sct发票.s医疗机构类型;
//xrLabel校验码.Text = _sct发票.s校验码;
//label姓名.Text = _sct发票.s病人姓名;
//xrLabel性别.Text = _sct发票.s性别;
//xrLabel医保类型.Text = "";// _sct发票.s医保类型;
//xrLabel社会保障号码.Text = _sct发票.s社会保障号码;
//xrLabel合计大写.Text = _sct发票.s合计大写;
//xrLabel合计小写.Text = _sct发票.s合计小写;
//xrLabel医保统筹支付.Text = _sct发票.s医保统筹支付;
//xrLabel个人账户支付.Text = _sct发票.s个人账户支付;
//xrLabel其他医保支付.Text = _sct发票.s其他医保支付;
//xrLabel个人支付金额.Text = _sct发票.s个人支付金额;
//xrLabel收款单位.Text = _sct发票.s收款单位;
//xrLabel收款人.Text = _sct发票.s收款人;
//xrLabel年月日.Text = _sct发票.s年月日;
//xrLabel报销信息.Text = _sct发票.s报销信息;

//xrLabel医疗机构类型.Text = _sct发票.s医疗机构类型;
//xrLabel校验码.Text = _sct发票.s校验码;
//xrLabel医保类型.Text = _sct发票.s医保类型;
//xrLabel社会保障号码.Text = _sct发票.s社会保障号码;
//xrLabel医保统筹支付.Text = _sct发票.s医保统筹支付;
//xrLabel个人账户支付.Text = _sct发票.s个人账户支付;
//xrLabel其他医保支付.Text = _sct发票.s其他医保支付;
//xrLabel个人支付金额.Text = _sct发票.s个人支付金额;