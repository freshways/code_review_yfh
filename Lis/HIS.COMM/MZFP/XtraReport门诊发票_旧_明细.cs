﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections;

namespace HIS.COMM
{
    public partial class XtraReport门诊发票_旧_明细 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport门诊发票_旧_明细(Class发票结构.sct发票格式 _sct发票)
        {
            InitializeComponent();

            ArrayList arr = new System.Collections.ArrayList();
            foreach (DataRow row in _sct发票.dtPrint.Rows)
            {
                string flmc=row["分类名称"].ToString();
                if (arr.IndexOf(flmc)==-1)
                {
                    arr.Add(flmc);                    
                }
            }

            
            this.brxmCenter.Text =_sct发票.s病人姓名;
            this.brxmLeft.Text =_sct发票.s病人姓名;
            this.brxmRightBottom.Text =_sct发票.s病人姓名;
            this.brxmRightMiddle.Text =_sct发票.s病人姓名;
            this.brxmRightTop.Text =_sct发票.s病人姓名;

            this.dyrqCenter.Text =_sct发票.s打印日期;
            this.dyrqLeftBottom.Text =_sct发票.s打印日期;
            this.dyrqRightBottom.Text =_sct发票.s打印日期;
            this.dyrqRightMiddle.Text =_sct发票.s打印日期;
            this.dyrqRightTop.Text =_sct发票.s打印日期;

            this.LshCenter.Text = _sct发票.s流水号;

            this.zdLeftBottom.Text = _sct发票.s制单人;

            this.skrCenterBottom.Text =_sct发票.s收款人;
            this.skrRightBottom.Text =_sct发票.s收款人;
            this.skrRightMiddle.Text =_sct发票.s收款人;
            this.skrRightTop.Text =_sct发票.s收款人;

            string hjXx = Math.Round((Decimal)_sct发票.dtPrint.Compute("sum(金额)", ""), 1).ToString();
            string hjDx = WEISHENG.COMM.GetDaxie.GetDaXie(Convert.ToDouble(hjXx), true);

            //this.TotalJeXxCenter.Text = hjXx;
            //this.TotalJeDxCenter.Text = hjDx;
            this.TotalJeDxCenter.Text =_sct发票.s报销信息;

            this.TotalJeXxLeft.Text = hjXx;
          


            try
            {
                this.xmRightTop.Text = arr[0].ToString();
                this.jeRightTop.Text = _sct发票.dtPrint.Compute("sum(金额)", "分类名称='" + arr[0].ToString() + "'").ToString();
                if (arr.Count == 1)
                {
                    return;
                }
                this.xmRightMiddle.Text = arr[1].ToString();
                this.jeRightMiddle.Text = _sct发票.dtPrint.Compute("sum(金额)", "分类名称='" + arr[1].ToString() + "'").ToString();
                if (arr.Count == 2)
                {
                    return;
                }
                this.xmRightBottom.Text = arr[2].ToString();
                this.jeRightBottom.Text = _sct发票.dtPrint.Compute("sum(金额)", "分类名称='" + arr[2].ToString() + "'").ToString();
            }
            catch (Exception ee)
            {

                //throw;
            }
            finally
            {
                try
                {
                    //按从左到右，从上到下顺序填充
                    this.xmLeftTop.Text = _sct发票.dtPrint.Rows[0]["费用名称"].ToString();
                    this.xmCenterTop.Text = _sct发票.dtPrint.Rows[0]["费用名称"].ToString();
                    this.jeCenterTop.Text = _sct发票.dtPrint.Rows[0]["金额"].ToString();
                    this.jeLeftTop.Text = _sct发票.dtPrint.Rows[0]["金额"].ToString();
                    //if (_sct发票.dtPrint.Rows.Count == 1)
                    //{
                    //    return;
                    //}
                    this.xmLeftTop2.Text = _sct发票.dtPrint.Rows[1]["费用名称"].ToString();
                    this.xmCenterTop2.Text = _sct发票.dtPrint.Rows[1]["费用名称"].ToString();
                    this.jeLeftTop2.Text = _sct发票.dtPrint.Rows[1]["金额"].ToString();
                    this.jeCenterTop2.Text = _sct发票.dtPrint.Rows[1]["金额"].ToString();
                    //if (_sct发票.dtPrint.Rows.Count == 2)
                    //{
                    //    return;
                    //}
                    this.xmLeftMiddle.Text = _sct发票.dtPrint.Rows[2]["费用名称"].ToString();
                    this.xmCenterMiddle.Text = _sct发票.dtPrint.Rows[2]["费用名称"].ToString();
                    this.jeLeftMiddle.Text = _sct发票.dtPrint.Rows[2]["金额"].ToString();
                    this.jeCenterMiddle.Text = _sct发票.dtPrint.Rows[2]["金额"].ToString();
                    //if (_sct发票.dtPrint.Rows.Count == 3)
                    //{
                    //    return;
                    //}
                    this.xmCenterMiddle2.Text = _sct发票.dtPrint.Rows[3]["费用名称"].ToString();
                    this.xmLeftMiddle2.Text = _sct发票.dtPrint.Rows[3]["费用名称"].ToString();
                    this.jeCenterMiddle2.Text = _sct发票.dtPrint.Rows[3]["金额"].ToString();
                    this.jeLeftMiddle2.Text = _sct发票.dtPrint.Rows[3]["金额"].ToString();
                    //if (_sct发票.dtPrint.Rows.Count == 4)
                    //{
                    //    return;
                    //}
                    this.xmLeftBottom.Text = _sct发票.dtPrint.Rows[4]["费用名称"].ToString();
                    this.xmCenterBottom.Text = _sct发票.dtPrint.Rows[4]["费用名称"].ToString();
                    this.jeLeftBottom.Text = _sct发票.dtPrint.Rows[4]["金额"].ToString();
                    this.jeCenterBottom.Text = _sct发票.dtPrint.Rows[4]["金额"].ToString();


                }
                catch (Exception ee)
                {

                    //   throw;
                }
            }
            


        }

    }
}
