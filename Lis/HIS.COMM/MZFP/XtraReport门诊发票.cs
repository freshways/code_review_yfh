﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections;

namespace HIS.COMM
{
    public partial class XtraReport门诊发票 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport门诊发票(string brxm, string dyrq, string lsh, string zdr, string skr, DataTable dt,string byxj_zjzf_zhye)
        {
            InitializeComponent();

            ArrayList arr = new System.Collections.ArrayList();
            foreach (DataRow row in dt.Rows)
            {
                string flmc=row["分类名称"].ToString();
                if (arr.IndexOf(flmc)==-1)
                {
                    arr.Add(flmc);                    
                }
            }
        

            this.brxmCenter.Text = brxm;
            this.brxmLeft.Text = brxm;
            this.brxmRightBottom.Text = brxm;
            this.brxmRightMiddle.Text = brxm;
            this.brxmRightTop.Text = brxm;

            this.dyrqCenter.Text = dyrq;
            this.dyrqLeftBottom.Text = dyrq;
            this.dyrqRightBottom.Text = dyrq;
            this.dyrqRightMiddle.Text = dyrq;
            this.dyrqRightTop.Text = dyrq;

            this.LshCenter.Text = lsh;

            this.zdLeftBottom.Text = zdr;

            this.skrCenterBottom.Text = skr;
            this.skrRightBottom.Text = skr;
            this.skrRightMiddle.Text = skr;
            this.skrRightTop.Text = skr;

            string hjXx = Math.Round((Decimal)dt.Compute("sum(金额)", ""), 1).ToString();
            string hjDx = WEISHENG.COMM.GetDaxie.GetDaXie(Convert.ToDouble(hjXx), true);

            //this.TotalJeXxCenter.Text = hjXx;
            //this.TotalJeDxCenter.Text = hjDx;
            this.TotalJeDxCenter.Text = byxj_zjzf_zhye;

            this.TotalJeXxLeft.Text = hjXx;
          


            try
            {
                this.xmRightTop.Text = arr[0].ToString();
                this.jeRightTop.Text = dt.Compute("sum(金额)", "分类名称='" + arr[0].ToString() + "'").ToString();
                if (arr.Count == 1)
                {
                    return;
                }
                this.xmRightMiddle.Text = arr[1].ToString();
                this.jeRightMiddle.Text = dt.Compute("sum(金额)", "分类名称='" + arr[1].ToString() + "'").ToString();
                if (arr.Count == 2)
                {
                    return;
                }
                this.xmRightBottom.Text = arr[2].ToString();
                this.jeRightBottom.Text = dt.Compute("sum(金额)", "分类名称='" + arr[2].ToString() + "'").ToString();
            }
            catch (Exception ee)
            {

                //throw;
            }
            finally
            {
                try
                {
                    //按从左到右，从上到下顺序填充
                    this.xmLeftTop.Text = dt.Rows[0]["费用名称"].ToString();
                    this.xmCenterTop.Text = dt.Rows[0]["费用名称"].ToString();
                    this.jeCenterTop.Text = dt.Rows[0]["金额"].ToString();
                    this.jeLeftTop.Text = dt.Rows[0]["金额"].ToString();
                    //if (dt.Rows.Count == 1)
                    //{
                    //    return;
                    //}
                    this.xmLeftTop2.Text = dt.Rows[1]["费用名称"].ToString();
                    this.xmCenterTop2.Text = dt.Rows[1]["费用名称"].ToString();
                    this.jeLeftTop2.Text = dt.Rows[1]["金额"].ToString();
                    this.jeCenterTop2.Text = dt.Rows[1]["金额"].ToString();
                    //if (dt.Rows.Count == 2)
                    //{
                    //    return;
                    //}
                    this.xmLeftMiddle.Text = dt.Rows[2]["费用名称"].ToString();
                    this.xmCenterMiddle.Text = dt.Rows[2]["费用名称"].ToString();
                    this.jeLeftMiddle.Text = dt.Rows[2]["金额"].ToString();
                    this.jeCenterMiddle.Text = dt.Rows[2]["金额"].ToString();
                    //if (dt.Rows.Count == 3)
                    //{
                    //    return;
                    //}
                    this.xmCenterMiddle2.Text = dt.Rows[3]["费用名称"].ToString();
                    this.xmLeftMiddle2.Text = dt.Rows[3]["费用名称"].ToString();
                    this.jeCenterMiddle2.Text = dt.Rows[3]["金额"].ToString();
                    this.jeLeftMiddle2.Text = dt.Rows[3]["金额"].ToString();
                    //if (dt.Rows.Count == 4)
                    //{
                    //    return;
                    //}
                    this.xmLeftBottom.Text = dt.Rows[4]["费用名称"].ToString();
                    this.xmCenterBottom.Text = dt.Rows[4]["费用名称"].ToString();
                    this.jeLeftBottom.Text = dt.Rows[4]["金额"].ToString();
                    this.jeCenterBottom.Text = dt.Rows[4]["金额"].ToString();


                }
                catch (Exception ee)
                {

                    //   throw;
                }
            }
            


        }

    }
}
