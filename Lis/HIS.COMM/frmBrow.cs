using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.COMM
{
    public partial class frmBrow : DevExpress.XtraEditors.XtraForm
    {
        public frmBrow()
        {
            InitializeComponent();
        }
        public void ShowData<T>(List<T> dtDe, Boolean showGroupBar, Boolean showFilter, params string[] excludes) where T : class
        {
            List<string> list_excludes = excludes.ToList();

            gridView1.OptionsView.ShowGroupPanel = showGroupBar;
            gridView1.OptionsView.ShowFooter = showFilter;
            gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridControl1.DataSource = dtDe;
            gridView1.PopulateColumns();

            foreach (var item in gridView1.Columns)
            {
                if (list_excludes.Contains(item.ToString()))
                {
                    
                }
            }


            //for (int i = 0; i < dtDe.Columns.Count; i++)
            //{
            //    if (dtDe.Columns[i].DataType == System.Type.GetType("System.DateTime"))
            //    {
            //        gridView1.Columns[dtDe.Columns[i].ColumnName.ToString()].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            //    }
            //}


            gridView1.BestFitColumns();
        }

        public void ShowData(DataTable dtDe, Boolean showGroupBar, Boolean showFilter)
        {
            gridView1.OptionsView.ShowGroupPanel = showGroupBar;
            gridView1.OptionsView.ShowFooter = showFilter;
            gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridControl1.DataSource = dtDe;
            gridView1.PopulateColumns();
            for (int i = 0; i < dtDe.Columns.Count; i++)
            {
                if (dtDe.Columns[i].DataType == System.Type.GetType("System.DateTime"))
                {
                    gridView1.Columns[dtDe.Columns[i].ColumnName.ToString()].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
                }
            }
            gridView1.BestFitColumns();
        }
        public void ShowData(DataTable dtDe)
        {
            this.gridControl1.DataSource = dtDe;
            gridView1.PopulateColumns();
            for (int i = 0; i < dtDe.Columns.Count; i++)
            {
                if (dtDe.Columns[i].DataType == System.Type.GetType("System.DateTime"))
                {
                    gridView1.Columns[dtDe.Columns[i].ColumnName.ToString()].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
                }
            }
            gridView1.BestFitColumns();
        }
        private void simpleButton导出_Click(object sender, EventArgs e)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "导出Excel";
            saveFileDialog.Filter = "Excel文件(*.xls)|*.xls";
            DialogResult dialogResult = saveFileDialog.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions();
                gridControl1.ExportToXls(saveFileDialog.FileName);
                DevExpress.XtraEditors.XtraMessageBox.Show("保存成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView1_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void gridView1_RowCountChanged(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            if (gv.RowCount < 1000)
                gv.IndicatorWidth = 35;
            else if (gv.RowCount < 10000)
                gv.IndicatorWidth = 50;
            else if (gv.RowCount < 100000)
                gv.IndicatorWidth = 60;
            else if (gv.RowCount < 1000000)
                gv.IndicatorWidth = 70;
        }

        private void frmBrow_Load(object sender, EventArgs e)
        {
            HIS.COMM.frmGridCustomize.RegisterGrid(gridView1);
        }
    }
}