﻿namespace HIS.COMM
{
    partial class XtraFormCipherPrescription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraFormCipherPrescription));
            this.treeListPrescription = new DevExpress.XtraTreeList.TreeList();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonAffirm = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlPrescription = new DevExpress.XtraGrid.GridControl();
            this.gridViewPrescription = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton放弃 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton删除 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton保存 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem删除 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem保存 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem确认 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.treeListPrescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPrescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPrescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem删除)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem保存)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem确认)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // treeListPrescription
            // 
            this.treeListPrescription.Location = new System.Drawing.Point(12, 12);
            this.treeListPrescription.Name = "treeListPrescription";
            this.treeListPrescription.OptionsBehavior.Editable = false;
            this.treeListPrescription.OptionsSelection.MultiSelect = true;
            this.treeListPrescription.Size = new System.Drawing.Size(169, 364);
            this.treeListPrescription.TabIndex = 3;
            this.treeListPrescription.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeList协定处方_FocusedNodeChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButtonAffirm);
            this.layoutControl1.Controls.Add(this.simpleButton刷新);
            this.layoutControl1.Controls.Add(this.gridControlPrescription);
            this.layoutControl1.Controls.Add(this.simpleButton放弃);
            this.layoutControl1.Controls.Add(this.treeListPrescription);
            this.layoutControl1.Controls.Add(this.simpleButton删除);
            this.layoutControl1.Controls.Add(this.simpleButton保存);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(556, 93, 588, 360);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(811, 388);
            this.layoutControl1.TabIndex = 8;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButtonAffirm
            // 
            this.simpleButtonAffirm.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonAffirm.ImageOptions.Image")));
            this.simpleButtonAffirm.Location = new System.Drawing.Point(725, 354);
            this.simpleButtonAffirm.Name = "simpleButtonAffirm";
            this.simpleButtonAffirm.Size = new System.Drawing.Size(74, 22);
            this.simpleButtonAffirm.StyleController = this.layoutControl1;
            this.simpleButtonAffirm.TabIndex = 11;
            this.simpleButtonAffirm.Text = "确认";
            this.simpleButtonAffirm.Click += new System.EventHandler(this.simpleButtonAffirm_Click);
            // 
            // simpleButton刷新
            // 
            this.simpleButton刷新.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton刷新.ImageOptions.Image")));
            this.simpleButton刷新.Location = new System.Drawing.Point(646, 354);
            this.simpleButton刷新.Name = "simpleButton刷新";
            this.simpleButton刷新.Size = new System.Drawing.Size(75, 22);
            this.simpleButton刷新.StyleController = this.layoutControl1;
            this.simpleButton刷新.TabIndex = 10;
            this.simpleButton刷新.Text = "刷新";
            this.simpleButton刷新.Click += new System.EventHandler(this.simpleButton刷新_Click);
            // 
            // gridControlPrescription
            // 
            this.gridControlPrescription.Location = new System.Drawing.Point(190, 12);
            this.gridControlPrescription.MainView = this.gridViewPrescription;
            this.gridControlPrescription.Name = "gridControlPrescription";
            this.gridControlPrescription.Size = new System.Drawing.Size(609, 338);
            this.gridControlPrescription.TabIndex = 9;
            this.gridControlPrescription.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPrescription});
            // 
            // gridViewPrescription
            // 
            this.gridViewPrescription.GridControl = this.gridControlPrescription;
            this.gridViewPrescription.Name = "gridViewPrescription";
            this.gridViewPrescription.OptionsBehavior.Editable = false;
            this.gridViewPrescription.OptionsView.ShowFooter = true;
            this.gridViewPrescription.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButton放弃
            // 
            this.simpleButton放弃.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton放弃.ImageOptions.Image")));
            this.simpleButton放弃.Location = new System.Drawing.Point(564, 354);
            this.simpleButton放弃.Name = "simpleButton放弃";
            this.simpleButton放弃.Size = new System.Drawing.Size(78, 22);
            this.simpleButton放弃.StyleController = this.layoutControl1;
            this.simpleButton放弃.TabIndex = 8;
            this.simpleButton放弃.Text = "放弃";
            this.simpleButton放弃.Visible = false;
            this.simpleButton放弃.Click += new System.EventHandler(this.simpleButton放弃_Click);
            // 
            // simpleButton删除
            // 
            this.simpleButton删除.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton删除.ImageOptions.Image")));
            this.simpleButton删除.Location = new System.Drawing.Point(400, 354);
            this.simpleButton删除.Name = "simpleButton删除";
            this.simpleButton删除.Size = new System.Drawing.Size(78, 22);
            this.simpleButton删除.StyleController = this.layoutControl1;
            this.simpleButton删除.TabIndex = 7;
            this.simpleButton删除.Text = "删除";
            this.simpleButton删除.Click += new System.EventHandler(this.simpleButton删除_Click);
            // 
            // simpleButton保存
            // 
            this.simpleButton保存.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton保存.ImageOptions.Image")));
            this.simpleButton保存.Location = new System.Drawing.Point(482, 354);
            this.simpleButton保存.Name = "simpleButton保存";
            this.simpleButton保存.Size = new System.Drawing.Size(78, 22);
            this.simpleButton保存.StyleController = this.layoutControl1;
            this.simpleButton保存.TabIndex = 5;
            this.simpleButton保存.Text = "保存";
            this.simpleButton保存.Visible = false;
            this.simpleButton保存.Click += new System.EventHandler(this.simpleButton保存_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.splitterItem1,
            this.layoutControlItem7,
            this.emptySpaceItem2,
            this.layoutControlItem删除,
            this.layoutControlItem保存,
            this.layoutControlItem6,
            this.layoutControlItem确认,
            this.layoutControlItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(811, 388);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.treeListPrescription;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(173, 368);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(173, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 368);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gridControlPrescription;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(178, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(613, 342);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(178, 342);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(210, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem删除
            // 
            this.layoutControlItem删除.Control = this.simpleButton删除;
            this.layoutControlItem删除.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem删除.Location = new System.Drawing.Point(388, 342);
            this.layoutControlItem删除.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem删除.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem删除.Name = "layoutControlItem删除";
            this.layoutControlItem删除.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem删除.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem删除.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem删除.TextVisible = false;
            // 
            // layoutControlItem保存
            // 
            this.layoutControlItem保存.Control = this.simpleButton保存;
            this.layoutControlItem保存.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem保存.Location = new System.Drawing.Point(470, 342);
            this.layoutControlItem保存.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem保存.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem保存.Name = "layoutControlItem保存";
            this.layoutControlItem保存.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem保存.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem保存.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem保存.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton放弃;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(552, 342);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            this.layoutControlItem6.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem确认
            // 
            this.layoutControlItem确认.Control = this.simpleButtonAffirm;
            this.layoutControlItem确认.Location = new System.Drawing.Point(713, 342);
            this.layoutControlItem确认.MaxSize = new System.Drawing.Size(78, 26);
            this.layoutControlItem确认.MinSize = new System.Drawing.Size(78, 26);
            this.layoutControlItem确认.Name = "layoutControlItem确认";
            this.layoutControlItem确认.Size = new System.Drawing.Size(78, 26);
            this.layoutControlItem确认.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem确认.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem确认.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton刷新;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(634, 342);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(79, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(79, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(79, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // XtraFormCipherPrescription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 388);
            this.Controls.Add(this.layoutControl1);
            this.Name = "XtraFormCipherPrescription";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "协定处方";
            this.Load += new System.EventHandler(this.XtraForm协定处方_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeListPrescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPrescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPrescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem删除)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem保存)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem确认)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList treeListPrescription;
        private DevExpress.XtraEditors.SimpleButton simpleButton保存;
        private DevExpress.XtraEditors.SimpleButton simpleButton删除;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem删除;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem保存;
        private DevExpress.XtraEditors.SimpleButton simpleButton放弃;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraGrid.GridControl gridControlPrescription;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPrescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton simpleButton刷新;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAffirm;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem确认;
    }
}