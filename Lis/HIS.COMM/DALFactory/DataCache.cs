﻿// =================================================================== 
// 项目说明
//====================================================================
// litao@Copy Right 2006-2008
// 文件： DataCache.cs
// 创建时间：2018-06-13
// ===================================================================
using System;
using System.Web;

namespace HIS.COMM.DALFactory
{
	/// <summary>
	/// Summary description for DataCache.
	/// </summary>
	public class DataCache
	{
		public DataCache()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="CacheKey"></param>
		/// <returns></returns>
		public static object GetCache(string CacheKey)
		{

			System.Web.Caching.Cache objCache = HttpRuntime.Cache;
			return objCache[CacheKey];

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="CacheKey"></param>
		/// <param name="objObject"></param>
		public static void SetCache(string CacheKey, object objObject)
		{
			System.Web.Caching.Cache objCache = HttpRuntime.Cache;
			objCache.Insert(CacheKey, objObject);
		}
	}
}
