﻿// litao@Copy Right 2006-2016
// 文件： DataAccessFactory.cs
// 创建时间：2018-05-26
// ===================================================================
using System;
using System.Reflection;
using System.Configuration;
using HIS.COMM.IDAL;

namespace HIS.COMM.DALFactory
{
    /// <summary>
    /// 数据层工厂

    /// </summary>
    public sealed partial class DataAccess
    {

        private static readonly string _path ="HIS.COMM"; 	
		private static readonly string _dbtype ="SqlServer";// ConfigurationManager.AppSettings.Get("DbType");
		private DataAccess() { }
		private static object CreateObject(string path,string CacheKey)
		{			
			object objType = DataCache.GetCache(CacheKey);
			if (objType == null)
			{
				try
				{
					objType = Assembly.Load(path).CreateInstance(CacheKey);					
					DataCache.SetCache(CacheKey, objType);
				}
				catch(Exception ex)
                {
                    throw ex;
                }

			}
			return objType;
		}
        /// <summary>
        /// 通过反射机制，实例化GY科室设置接口对象。
        /// </summary>
        ///<returns>GY科室设置接口对象</returns>
        public static IGY科室设置 CreateGY科室设置()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.GY科室设置DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IGY科室设置)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化GY打印设置接口对象。
        /// </summary>
        ///<returns>GY打印设置接口对象</returns>
        public static IGY打印设置 CreateGY打印设置()
        {
            string CacheKey = _path + "." + _dbtype + "DAL.GY打印设置DAL";
            object objType = CreateObject(_path, CacheKey);
            return (IGY打印设置)objType;
        }

        /// <summary>
        /// 通过反射机制，实例化GY全局参数接口对象。
        /// </summary>
        ///<returns>GY全局参数接口对象</returns>
        public static IGY全局参数 CreateGY全局参数()
		{		
			string CacheKey = _path+"."+_dbtype+"DAL.GY全局参数DAL";	
			object objType=CreateObject(_path,CacheKey);			
			return (IGY全局参数)objType;		
		}

		/// <summary>
    	/// 通过反射机制，实例化GY收费大项接口对象。
    	/// </summary>
		///<returns>GY收费大项接口对象</returns>
		public static IGY收费大项 CreateGY收费大项()
		{		
			string CacheKey = _path+"."+_dbtype+"DAL.GY收费大项DAL";	
			object objType=CreateObject(_path,CacheKey);			
			return (IGY收费大项)objType;		
		}

		/// <summary>
    	/// 通过反射机制，实例化GY收费小项接口对象。
    	/// </summary>
		///<returns>GY收费小项接口对象</returns>
		public static IGY收费小项 CreateGY收费小项()
		{		
			string CacheKey = _path+"."+_dbtype+"DAL.GY收费小项DAL";	
			object objType=CreateObject(_path,CacheKey);			
			return (IGY收费小项)objType;		
		}

		
    }
}

