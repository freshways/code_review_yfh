﻿// litao@Copy Right 2006-2016
// 文件： DataAccessFactory.cs
// 创建时间：2018-06-13
// ===================================================================
using System;
using System.Reflection;
using System.Configuration;
using HIS.COMM.IDAL;

namespace HIS.COMM.DALFactory
{
    /// <summary>
    /// 数据层工厂

    /// </summary>
    public sealed partial class DataAccess
    {

        private static readonly string _path ="HIS.COMM"; 	
		private static readonly string _dbtype ="SqlServer";// ConfigurationManager.AppSettings.Get("DbType");
		private DataAccess() { }
		private static object CreateObject(string path,string CacheKey)
		{			
			object objType = DataCache.GetCache(CacheKey);
			if (objType == null)
			{
				try
				{
					objType = Assembly.Load(path).CreateInstance(CacheKey);					
					DataCache.SetCache(CacheKey, objType);
				}
				catch(Exception ex)
                {
                    throw ex;
                }

			}
			return objType;
		}
	
		
    }
}

