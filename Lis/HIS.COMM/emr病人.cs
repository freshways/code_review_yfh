﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HIS.COMM
{

    public class emr病人 : HIS.COMM.Person住院病人
    {
        //private string _docId;

        //public string docId
        //{
        //    get { return _docId; }
        //    set { _docId = value; }
        //}
        //private string _topic;

        //public string topic
        //{
        //    get { return _topic; }
        //    set { _topic = value; }
        //}
        //private string _patientId;

        //public string patientId
        //{
        //    get { return _patientId; }
        //    set { _patientId = value; }
        //}


        private string _s病案号;

        public string s病案号
        {
            get { return _s病案号; }
            set { _s病案号 = value; }
        }


       HIS.COMM.ClassEnum.enEMR文书状态 _en文书状态;

        public HIS.COMM.ClassEnum.enEMR文书状态 en文书状态
        {
            get { return _en文书状态; }
            set { _en文书状态 = value; }
        }

        private Boolean _b是否只读;//从文书状态中分离出来，方便控制时间限制等
        public Boolean b是否病历只读
        {
            get { return _b是否只读; }
            set { _b是否只读 = value; }
        }

        private HIS.COMM.ClassEnum.en在院状态 _s在院状态;
        public HIS.COMM.ClassEnum.en在院状态 s在院状态
        {
            get { return _s在院状态; }
            set { _s在院状态 = value; }
        }


        private int _i文书记录数;

        public int i文书记录数
        {
            get { return _i文书记录数; }
            set { _i文书记录数 = value; }
        }


        private string _s文书提交人;

        public string s文书提交人
        {
            get { return _s文书提交人; }
            set { _s文书提交人 = value; }
        }

        private string _s文书审核人;

        public string s文书审核人
        {
            get { return _s文书审核人; }
            set { _s文书审核人 = value; }
        }

        private DateTime _dtime文书提交时间;

        public DateTime dtime文书提交时间
        {
            get { return _dtime文书提交时间; }
            set { _dtime文书提交时间 = value; }
        }


        private DateTime _dtime文书审核时间;

        public DateTime dtime文书审核时间
        {
            get { return _dtime文书审核时间; }
            set { _dtime文书审核时间 = value; }
        }

    }
}

