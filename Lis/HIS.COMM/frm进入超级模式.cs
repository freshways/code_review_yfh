﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace HIS.COMM
{
    public partial class frm进入超级模式 : DevExpress.XtraEditors.XtraForm
    {

        public frm进入超级模式()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (txtMM1.Text == "supper" && txtMM2.Text == "admin")
            {
                this.DialogResult = DialogResult.OK;
            }
        }


        private void btnCanel_Click(object sender, EventArgs e)
        {
                this.Close();
        }


    }
}