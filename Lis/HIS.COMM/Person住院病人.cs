﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HIS.COMM
{
    public class Person住院病人 : HIS.COMM.PersonBase
    {

        //private decimal _decZyid;

        //public decimal decZyid
        //{
        //    get { return _decZyid; }//    set { _decZyid = value; }
        //}

        //private string _s床号;

        //public string s床号
        //{
        //    get { return _s床号; }
        //    set { _s床号 = value; }
        //}

        //private string _s床位名称;
        //public string s床位名称
        //{
        //    get { return _s床位名称; }
        //    set { _s床位名称 = value; }
        //}

        private string _sEmrPlugIn临床路径病种 = "";

        public string sEmrPlugIn临床路径病种
        {
            get { return _sEmrPlugIn临床路径病种; }
            set { _sEmrPlugIn临床路径病种 = value; }
        }


        private int _i床位编码 = 0;

        public int i床位编码
        {
            get { return _i床位编码; }
            set { _i床位编码 = value; }
        }
        private string _s病区名称;
        public string s病区名称
        {
            get { return _s病区名称; }
            set { _s病区名称 = value; }
        }
        //private int _i病区编号;

        //public int i病区编号
        //{
        //    get { return _i病区编号; }
        //    set { _i病区编号 = value; }
        //}
        private string _s临床路径名称;

        public string s临床路径名称
        {
            get { return _s临床路径名称; }
            set { _s临床路径名称 = value; }
        }
        private string _s临床路径编号;

        public string s临床路径编号
        {
            get { return _s临床路径编号; }
            set { _s临床路径编号 = value; }
        }
        private string _s入院时间;

        public string s入院时间
        {
            get { return _s入院时间; }
            set { _s入院时间 = value; }
        }

        private string _s科室编码;

        public string s科室编码
        {
            get { return _s科室编码; }
            set { _s科室编码 = value; }
        }
        //private string _s科室名称;

        //public string s科室名称
        //{
        //    get { return _s科室名称; }
        //    set { _s科室名称 = value; }
        //}

        private string _s科室名称;

        public string S科室名称
        {
            get { return _s科室名称; }
            set { _s科室名称 = value; }
        }
        private string _s床位名称;

        public string S床位名称
        {
            get { return _s床位名称; }
            set { _s床位名称 = value; }
        }

        private string s病区编码 = "";

        public string S病区编码
        {
            get { return s病区编码; }
            set { s病区编码 = value; }
        }


        private string _s住院号码;

        public string s住院号码
        {
            get { return _s住院号码; }
            set { _s住院号码 = value; }
        }

        private string _s主治医生编码;

        public string s主治医生编码
        {
            get { return _s主治医生编码; }
            set { _s主治医生编码 = value; }
        }

        private string _s主治医生姓名;

        public string s主治医生姓名
        {
            get { return _s主治医生姓名; }
            set { _s主治医生姓名 = value; }
        }

        private string _s父ZYID = "-1";

        public string sZYID父
        {
            get
            {
                if (_s父ZYID == "-1")
                {
                    return _sZyid;
                }
                else
                {
                    return _s父ZYID;
                }
            }
            set { _s父ZYID = value; }
        }
        private string _sZyid = "-1";

        public string sZYID
        {
            get { return _sZyid; }
            set { _sZyid = value; }
        }

        //private DateTime _dtime入院时间;

        //public DateTime dtime入院时间
        //{
        //    get { return _dtime入院时间; }
        //    set { _dtime入院时间 = value; }
        //}
        //private string _s联系人;

        //public string s联系人
        //{
        //    get { return _s联系人; }
        //    set { _s联系人 = value; }
        //}

        private Decimal _d病人欠款 = 0;//病人预交款余额(数据库初始金额)

        public Decimal d病人欠款
        {
            get { return _d病人欠款; }
            set { _d病人欠款 = value; }
        }

        private Decimal _d当前欠款 = 0;//当前余额（计算）

        private decimal _dec预交款总额;

        public decimal dec预交款总额
        {
            get { return _dec预交款总额; }
            set { _dec预交款总额 = value; }
        }
        public Decimal d当前欠款
        {
            get { return _d当前欠款; }
            set { _d当前欠款 = value; }
        }

        private Decimal _d欠款下限 = 0;

        public Decimal d欠款下限
        {
            get { return _d欠款下限; }
            set { _d欠款下限 = value; }
        }

        public static decimal get未发药金额(string _sZYID)
        {
            return Convert.ToDecimal(HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text,
                  "select isnull(sum(金额),0) from zy在院费用 where 单位编码=" + HIS.COMM.zdInfo.Model科室信息.单位编码 + " and  ZYID=" + _sZYID + " and 已发药标记=0 and YPID<>0"));

        }






        private int _i住院次数 = 1;

        public int I住院次数
        {
            get { return _i住院次数; }
            set { _i住院次数 = value; }
        }
    }
}
