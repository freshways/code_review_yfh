﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HIS.COMM
{

    public class Class医生
    {
        public static string sGET医生科室(string _s医生编码)
        {
            string SQL = "select ISNULL(科室名称,'') from pubUser where 用户编码=" + _s医生编码;
            return HIS.Model.Dal.SqlHelper.ExecuteScalar(DBConnHelper.SConnHISDb, CommandType.Text, SQL).ToString();
        }

        /// <summary>
        /// 返回可用医生，格式01,zs|张三
        /// </summary>
        /// <returns></returns>
        public static DataTable dt医生列表()
        {
            string SQL医生列表 =
                "SELECT 用户编码 AS 医生编码," + "\r\n" +
                "       拼音代码 + '|' + 用户名 医生助记码," + "\r\n" +
                "       ID" + "\r\n" +
                "  FROM pubUser" + "\r\n" +
                " WHERE (单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码 + ") AND 是否有处方权 = 1 AND 是否禁用 = 0";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL医生列表).Tables[0];
        }

        /// <summary>
        /// 返回可用医生，格式01,zs,张三
        /// </summary>
        /// <returns></returns>
        public static DataTable dt医生列表2()
        {
            string SQL医生列表 = @"select 用户编码, 用户名, 拼音代码 from pubUser where[是否有处方权]=1 and 是否禁用 = 0 order by 用户名";                
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL医生列表).Tables[0];
        }
        


        public static DataTable dt乡医列表()
        {
            string SQL医生列表 =
                "SELECT 用户编码 AS 医生编码," + "\r\n" +
                "       拼音代码 + '|' + 用户名 医生助记码," + "\r\n" +
                "       ID" + "\r\n" +
                "  FROM pubUser" + "\r\n" +
                " WHERE (单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码 + ") and 卫生室编码='" + HIS.COMM.zdInfo.YTH.sWssID + "'  ";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, SQL医生列表).Tables[0];
        }
        public static DataTable dt医生权限列表()
        {
            string sqlCmd =
                " SELECT 用户编码 AS 医生编码, aa.拼音代码+'|'+用户名 医生助记码, aa.ID " +
                " FROM pubUser aa left outer join GY科室设置 bb on aa.科室名称=bb.科室名称 " +
                " WHERE (aa.单位编码 = " + HIS.COMM.zdInfo.Model科室信息.单位编码 + ") and 是否有处方权=1 and aa.是否禁用=0 " +
                " and bb.科室编码 in (" + HIS.COMM.zdInfo.s科室数据权限 + ") ";
            return HIS.Model.Dal.SqlHelper.ExecuteDataset(DBConnHelper.SConnHISDb, CommandType.Text, sqlCmd).Tables[0];
        }
        public static void ysImageSaveToDb(byte[] image, string _s用户编码)
        {
            using (SqlConnection con = new SqlConnection(DBConnHelper.SConnHISDb))
            {
                con.Open();
                string sql = "update [pubUser] set [手签照片] = @photo  where 用户编码=" + _s用户编码;
                SqlParameter param = new SqlParameter();
                param = new SqlParameter("@photo", SqlDbType.Image);
                param.Value = image;
                SqlCommand commd = new SqlCommand(sql, con);
                commd.Parameters.Add(param);
                try
                {
                    commd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    string sErr = ex.Message;
                    WEISHENG.COMM.LogHelper.Info("更新医生手签", "错误信息", sErr);
                    MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        public static byte[] PhotoToArray(string path)
        {
            FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] bufferPhoto = new byte[stream.Length];
            stream.Read(bufferPhoto, 0, Convert.ToInt32(stream.Length));
            stream.Flush();
            stream.Close();
            return bufferPhoto;
        }


        public static System.Drawing.Image getYSimage(string _s用户编码)
        {
            System.Drawing.Image image = null;
            try
            {
                if (_s用户编码.Length == 0)
                {
                    return image;
                }
                string strSQL = "select top 1  手签照片 from pubuser where 用户编码='" + _s用户编码 + "'";
                System.Data.SqlClient.SqlDataReader reader = HIS.Model.Dal.SqlHelper.ExecuteReader(DBConnHelper.SConnHISDb,
                    CommandType.Text, strSQL);
                reader.Read();
                if (reader.HasRows)
                {
                    MemoryStream ms = new MemoryStream((byte[])reader["手签照片"]);
                    image = System.Drawing.Image.FromStream(ms, true);
                }
                else
                {
                    //  image = Properties.Resources.rytxwcj;
                }
                return image;
            }
            catch (Exception ee)
            {
                return image;
            }

        }

    }
}
