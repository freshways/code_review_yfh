﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using HIS.Model;
using HIS.Model.Pojo;
using LinqKit;
using WEISHENG.COMM;

namespace HIS.COMM
{

    public partial class XtraForm套餐选择 : DevExpress.XtraEditors.XtraForm
    {
        //public DataTable reDt套餐内容;

        //public string s套餐类型;
        //public DataTable dt处方内容;

        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public HIS.COMM.ClassEnum.en套餐类型 reEn套餐类型;
        public int i套餐数量;
        private string s药房权限;
        int i处方贴数 = 1;
        private int i组别;
        string s中药费小项编码 = HIS.COMM.comm.get收费小项编码("中药费");
        string s西药费小项编码 = HIS.COMM.comm.get收费小项编码("西药费");
        HIS.COMM.ClassEnum.en套餐调用方式 en套餐调用方式 = new HIS.COMM.ClassEnum.en套餐调用方式();
        Pojo病人信息 person = new Pojo病人信息();

        List<GY套餐摘要> list套餐摘要 = chis.GY套餐摘要.ToList();
        List<GY套餐明细> list套餐明细 = chis.GY套餐明细.ToList();
        List<GY协定处方> list协定处方内容 = chis.GY协定处方.ToList();

        public List<GY协定处方> list_选定_处方内容 = new List<GY协定处方>();
        public List<GY套餐明细> list_选定_套餐内容 = new List<GY套餐明细>();
        public string reS套餐名称;//选定套餐名称
        public XtraForm套餐选择(string _s药房权限, ref DataTable _dt明细)
        {
            InitializeComponent();
            //dt处方内容 = _dt明细;
            s药房权限 = WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 1);
            labelControl权限信息.Text = "库存选择:" + WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 0) + ";  套餐权限科室：" + HIS.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        public XtraForm套餐选择(Pojo药房权限 _s药房权限, HIS.COMM.ClassEnum.en套餐调用方式 _en套餐调用方式)
        {
            InitializeComponent();
            en套餐调用方式 = _en套餐调用方式;
            labelControl权限信息.Text = "库存选择:" + _s药房权限.科室名称 + ";  套餐权限科室：" + HIS.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        public XtraForm套餐选择(string _s药房权限, ref DataTable _dt明细, int _i处方贴数)
        {
            InitializeComponent();
            //dt处方内容 = _dt明细;
            i处方贴数 = _i处方贴数;
            s药房权限 = WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 1);
            labelControl权限信息.Text = "库存选择:" + WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 0) + ";  套餐权限科室：" + HIS.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        public XtraForm套餐选择(string _s药房权限, ref DataTable _dt明细, int _i处方贴数, HIS.COMM.ClassEnum.en套餐调用方式 _en套餐调用方式, Pojo病人信息 _person)
        {
            InitializeComponent();
            person = _person;
            en套餐调用方式 = _en套餐调用方式;
            //dt处方内容 = _dt明细;
            i处方贴数 = _i处方贴数;
            s药房权限 = WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 1);
            labelControl权限信息.Text = "库存选择:" + WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 0) + ";  套餐权限科室：" + HIS.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        //private string s医嘱类型;
        //private string s医嘱时间;
        //private string s开嘱医生编码;

        /// <summary>
        /// 住院医嘱调用套餐
        /// </summary>
        /// <param name="_s药房权限"></param>
        /// <param name="_dt明细"></param>
        /// <param name="_en套餐调用方式"></param>
        /// <param name="_person"></param>
        /// <param name="_i组别"></param>
        /// <param name="_s医嘱类型"></param>
        /// <param name="_s医嘱时间"></param>
        /// <param name="_s开嘱医生编码"></param>
        public XtraForm套餐选择(string _s药房权限, ref DataTable _dt明细, HIS.COMM.ClassEnum.en套餐调用方式 _en套餐调用方式,
            Pojo病人信息 _person, int _i组别, string _s医嘱类型, string _s医嘱时间, string _s开嘱医生编码)
        {
            InitializeComponent();
            //s医嘱时间 = _s医嘱时间;
            //s医嘱类型 = _s医嘱类型;
            person = _person;
            en套餐调用方式 = _en套餐调用方式;
            //dt处方内容 = _dt明细;
            i组别 = _i组别;
            s药房权限 = WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 1);
            //s开嘱医生编码 = _s开嘱医生编码;
            labelControl权限信息.Text = "库存选择:" + WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 0) + ";  套餐权限科室：" + HIS.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        public XtraForm套餐选择(string _s药房权限, ref DataTable _dt明细, int _i处方贴数, HIS.COMM.ClassEnum.en套餐调用方式 _en套餐调用方式, int _i组别)
        {
            InitializeComponent();
            i组别 = _i组别;
            en套餐调用方式 = _en套餐调用方式;
            //dt处方内容 = _dt明细;
            i处方贴数 = _i处方贴数;
            s药房权限 = WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 1);
            labelControl权限信息.Text = "库存选择:" + WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 0) + ";  套餐权限科室：" + HIS.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        public XtraForm套餐选择(string _s药房权限, ref DataTable _dt明细, HIS.COMM.ClassEnum.en套餐调用方式 _en套餐调用方式, int _i组别)
        {
            InitializeComponent();
            i组别 = _i组别;
            en套餐调用方式 = _en套餐调用方式;
            //dt处方内容 = _dt明细;
            s药房权限 = WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 1);
            labelControl权限信息.Text = "库存选择:" + WEISHENG.COMM.stringHelper.mySplit(_s药房权限, '|', 0) + ";  套餐权限科室：" + HIS.COMM.zdInfo.ModelUserInfo.科室名称;
        }
        private void XtraForm套餐选择_Load(object sender, EventArgs e)
        {
            foreach (var v in typeof(ClassEnum.en套餐类型).GetFields())
            {
                if (v.FieldType.IsEnum == true)
                {
                    this.cmb套餐类型.Properties.Items.Add(v.Name);
                }
            }
            try
            {
                radioGroup数据范围.SelectedIndex = Convert.ToInt16(configHelper.getKeyValue("套餐数据范围"));
                cmb套餐类型.EditValue = configHelper.getKeyValue("套餐类型");
                voidSelect套餐摘要();
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

        private void voidSelect套餐摘要()
        {
            switch (cmb套餐类型.Text)
            {
                case "协定处方":
                    var where = PredicateBuilder.New<GY协定处方>();
                    switch (radioGroup数据范围.SelectedIndex)
                    {
                        case 0:
                            where = where.And(c => c.处方类型 == "科室" && c.创建科室编码 == Convert.ToInt32(HIS.COMM.zdInfo.ModelUserInfo.科室编码));
                            break;
                        case 1:
                            where = where.And(c => c.处方类型 == "全院");
                            break;
                        case 2:
                            where = where.And(c => c.处方类型 == "个人" && c.创建人编码 == HIS.COMM.zdInfo.ModelUserInfo.用户编码);
                            break;
                    }
                    var list_选定_协定处方摘要 = list协定处方内容.Where(where).Select(c => new { c.处方名称, c.拼音代码 }).Distinct();

                    searchLookUpEdit套餐选择.Properties.DataSource = list_选定_协定处方摘要;
                    searchLookUpEdit套餐选择.Properties.View.PopulateColumns();
                    searchLookUpEdit套餐选择.Properties.DisplayMember = "处方名称";
                    searchLookUpEdit套餐选择.Properties.ValueMember = "处方名称";
                    searchLookUpEdit套餐选择.Properties.View.BestFitColumns();
                    searchLookUpEdit套餐选择.Refresh();
                    break;

                case "检查套餐":
                case "药品套餐":

                    if (radioGroup数据范围.SelectedIndex != 1)
                    {
                        MessageBox.Show("不支持的数据权限", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        radioGroup数据范围.SelectedIndex = 1;//全院
                        return;
                    }
                    var where_jc_yp = PredicateBuilder.New<GY套餐摘要>();
                    switch (radioGroup数据范围.SelectedIndex)
                    {
                        case 0://本科
                        default:
                            var ksqx = new int?[] { -1, Convert.ToInt32(HIS.COMM.zdInfo.ModelUserInfo.科室编码) };
                            where_jc_yp.And(c => ksqx.Contains(c.科室编码) && c.套餐类型 == cmb套餐类型.Text);
                            break;
                        case 1://全院
                            where_jc_yp.And(c => c.套餐类型 == cmb套餐类型.Text);
                            break;
                        case 2:

                            break;
                    }
                    //if (radioGroup数据范围.SelectedIndex == 0)//本科室
                    //{
                    //    //sqlWhere = "科室编码 in (-1," + _s科室编码 + ") and";
                    //}
                    //string SQL检查检验 = "SELECT 编号, 套餐名称, 套餐类型,  拼音代码, 科室名称 " + "\r\n" +
                    //               "FROM GY套餐摘要 where " + sqlWhere检查检验 + " 套餐类型='" + _s套餐类型 + "'";

                    var list_选定_套餐摘要 = list套餐摘要.Where(where_jc_yp).ToList();


                    searchLookUpEdit套餐选择.Properties.DataSource = list_选定_套餐摘要;
                    searchLookUpEdit套餐选择.Properties.View.PopulateColumns();
                    searchLookUpEdit套餐选择.Properties.DisplayMember = "套餐名称";
                    searchLookUpEdit套餐选择.Properties.ValueMember = "编号";
                    searchLookUpEdit套餐选择.Properties.View.BestFitColumns();
                    searchLookUpEdit套餐选择.Refresh();
                    break;
            }
        }

        private void searchLookUpEdit1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmb套餐类型.Text == HIS.COMM.ClassEnum.en套餐类型.协定处方.ToString())
                {
                    list_选定_处方内容 = list协定处方内容.Where(c => c.处方名称 == searchLookUpEdit套餐选择.EditValue.ToString()).ToList();
                    gridControl套餐内容.DataSource = list_选定_处方内容;
                    gridView1.PopulateColumns();

                    gridView1.BestFitColumns();
                }
                else
                {
                    int currBh = (int)searchLookUpEdit套餐选择.EditValue;
                    list_选定_套餐内容 = list套餐明细.Where(c => c.套餐编号 == currBh).ToList();
                    gridControl套餐内容.DataSource = list_选定_套餐内容;
                    gridView1.PopulateColumns();
                    gridView1.Columns["ID"].Visible = false;
                    gridView1.Columns["IsChanged"].Visible = false;
                    gridView1.Columns["创建人"].Visible = false;
                    gridView1.Columns["createTime"].Visible = false;
                    gridView1.Columns["科室编码"].Visible = false;
                    gridView1.Columns["科室名称"].Visible = false;
                    gridView1.BestFitColumns();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void simpleButton确定_Click(object sender, EventArgs e)
        {
            try
            {
                if (searchLookUpEdit套餐选择.Text == "请选择...")
                {
                    MessageBox.Show("请选择内容", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                reEn套餐类型 = (ClassEnum.en套餐类型)Enum.Parse(typeof(ClassEnum.en套餐类型), cmb套餐类型.Text);
                reS套餐名称 = searchLookUpEdit套餐选择.Text;
                //DataTable dt模板内容 = (DataTable)this.gridControl套餐内容.DataSource;
                //reDt套餐内容 = new DataTable();
                //reDt套餐内容 = dt模板内容.Clone();
                i套餐数量 = Convert.ToInt16(spinEdit套餐数量.EditValue);
                //DataRow[] rows套餐内容 = dt模板内容.Select("选择=1");
                //foreach (DataRow row in rows套餐内容)
                //{
                //    reDt套餐内容.Rows.Add(row.ItemArray);
                //}

                //DataTable dt套餐内容 = reDt套餐内容;
                switch (en套餐调用方式)
                {
                    case HIS.COMM.ClassEnum.en套餐调用方式.住院记费:
                        #region 住院记费
                        //foreach (DataRow row药品套餐 in dt套餐内容.Rows)
                        //{
                        //    DataRow row住院记费 = dt处方内容.NewRow();
                        //    bool b是否药品 = Convert.ToBoolean(row药品套餐["是否药品"]);
                        //    string s收费编码 = row药品套餐["收费编码"].ToString();
                        //    string s收费名称 = row药品套餐["收费名称"].ToString();
                        //    if (b是否药品)
                        //    {
                        //        decimal dec执行数量 = i套餐数量 * Convert.ToInt16(row药品套餐["数量"]) * i处方贴数;
                        //        drug drugInfo = new drug();
                        //        bool b查找库存 = HIS.COMM.comm.getYPIDWith药品序号(s收费编码, dec执行数量.ToString(), s药房权限, ref drugInfo);
                        //        if (b查找库存 == true)
                        //        {
                        //            string s归并编码 = "";
                        //            string s财务分类 = HIS.COMM.comm.get财务分类with药品序号(s收费编码);
                        //            if (s财务分类 == "中药费")
                        //            {
                        //                s归并编码 = HIS.COMM.comm.get归并编码("中药费");
                        //            }
                        //            else
                        //            {
                        //                s归并编码 = HIS.COMM.comm.get归并编码("西药费");
                        //            }
                        //            try
                        //            {
                        //                if (dec执行数量 < 0)
                        //                {
                        //                    MessageBox.Show("数量不允许出现负数", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //                    return;
                        //                }

                        //                decimal d金额 = drugInfo.Dec医院零售价 * dec执行数量;
                        //                if (Class记费.b是否超出欠款限制(d金额, person))
                        //                {
                        //                    //XtraMessageBox.Show("将要超过欠费下限设置，不能继续记费", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //                    return;
                        //                }
                        //                row住院记费["费用编码"] = s收费编码;
                        //                row住院记费["费用名称"] = drugInfo.S药品名称 + "/" + drugInfo.S药房规格 + "/" + drugInfo.S药房单位 + "/" + drugInfo.S药品产地;
                        //                row住院记费["数量"] = dec执行数量;
                        //                row住院记费["单价"] = drugInfo.Dec医院零售价;
                        //                row住院记费["金额"] = d金额;
                        //                row住院记费["进价"] = drugInfo.Dec进价;
                        //                row住院记费["药品序号"] = s收费编码;
                        //                row住院记费["YPId"] = drugInfo.Sypid;
                        //                row住院记费["医生编码"] = person.主治医生编码;
                        //                row住院记费["ZYID"] = person.ZYID;
                        //                row住院记费["住院号码"] = person.住院号码;
                        //                row住院记费["病人姓名"] = person.病人姓名;
                        //                //row住院记费["医生"] = person.s主治医生姓名;
                        //                row住院记费["项目编码"] = s归并编码;
                        //                row住院记费["药房编码"] = s药房权限;
                        //                dt处方内容.Rows.Add(row住院记费);
                        //            }
                        //            catch (Exception ex)
                        //            {

                        //                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", "用户" + HIS.COMM.zdInfo.ModelUserInfo.用户名 + "操作:" + ex.Message);
                        //                XtraMessageBox.Show(ex.Message);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            string sErr = s收费名称 + "库存不足，获取套餐组内容不完整";
                        //            WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                        //            MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //        }
                        //    }
                        //    else
                        //    {
                        //        row住院记费["费用编码"] = s收费编码;
                        //        row住院记费["费用名称"] = s收费名称;
                        //        decimal dec总量 = i套餐数量 * Convert.ToInt16(row药品套餐["数量"]) * i处方贴数;
                        //        if (dec总量 < 0)
                        //        {
                        //            MessageBox.Show("数量不允许出现负数", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //            return;
                        //        }
                        //        row住院记费["数量"] = dec总量;
                        //        row住院记费["单价"] = row药品套餐["单价"];
                        //        row住院记费["金额"] = Convert.ToDecimal(row药品套餐["单价"]) * i套餐数量 * Convert.ToInt16(row药品套餐["数量"]) * i处方贴数;
                        //        row住院记费["医生编码"] = person.主治医生编码;
                        //        row住院记费["ZYID"] = person.ZYID;
                        //        row住院记费["住院号码"] = person.住院号码;
                        //        row住院记费["病人姓名"] = person.病人姓名;
                        //        //row住院记费["医生"] = person.主治医生姓名;
                        //        row住院记费["进价"] = 0;
                        //        row住院记费["YPID"] = 0;
                        //        row住院记费["项目编码"] = HIS.COMM.comm.get归并编码(s收费名称);
                        //        row住院记费["药房编码"] = s药房权限;
                        //        dt处方内容.Rows.Add(row住院记费);
                        //    }
                        //}
                        #endregion
                        break;
                    case HIS.COMM.ClassEnum.en套餐调用方式.电子医嘱:
                        reS套餐名称 = searchLookUpEdit套餐选择.Text;
                        break;
                    case HIS.COMM.ClassEnum.en套餐调用方式.电子处方:
                        #region 电子处方
                        //if (cmb套餐类型.Text == HIS.COMM.ClassEnum.en套餐类型.协定处方.ToString())
                        //{
                        //    foreach (DataRow row协定处方 in dt套餐内容.Rows)
                        //    {
                        //        DataRow row处方明细 = dt处方内容.NewRow();
                        //        string s药品序号 = row协定处方["药品序号"].ToString();
                        //        string s收费名称 = row协定处方["药品名称"].ToString();
                        //        decimal dec执行数量 = i套餐数量 * Convert.ToInt16(row协定处方["处方总量"]) * i处方贴数;
                        //        drug drugInfo = new drug();
                        //        bool b查找库存 = HIS.COMM.comm.getYPIDWith药品序号(s药品序号, dec执行数量.ToString(), s药房权限, ref drugInfo);
                        //        if (b查找库存 == true)
                        //        {
                        //            string s费用序号 = "";
                        //            string s财务分类 = HIS.COMM.comm.get财务分类with药品序号(s药品序号);
                        //            if (s财务分类 == "中药费")
                        //            {
                        //                s费用序号 = s中药费小项编码;
                        //            }
                        //            else
                        //            {
                        //                s费用序号 = s西药费小项编码;
                        //            }
                        //            row处方明细["费用序号"] = s费用序号;
                        //            row处方明细["项目名称"] = s收费名称;
                        //            row处方明细["药品序号"] = s药品序号;
                        //            row处方明细["组别"] = i组别;
                        //            row处方明细["数量"] = dec执行数量;
                        //            row处方明细["金额"] = drugInfo.Dec医院零售价 * dec执行数量;
                        //            row处方明细["YPID"] = drugInfo.Sypid;
                        //            row处方明细["规格"] = drugInfo.S药房规格;
                        //            row处方明细["单位"] = drugInfo.S药房单位;
                        //            row处方明细["产地"] = drugInfo.S药品产地;
                        //            row处方明细["进价"] = drugInfo.Dec进价;
                        //            row处方明细["单价"] = drugInfo.Dec医院零售价;
                        //            row处方明细["频次"] = row协定处方["频次名称"].ToString();
                        //            row处方明细["用法"] = row协定处方["用法名称"].ToString();
                        //            row处方明细["剂量单位"] = row协定处方["剂量单位"].ToString();
                        //            row处方明细["剂量数量"] = row协定处方["剂量数量"].ToString();
                        //            row处方明细["财务分类"] = s财务分类;
                        //            dt处方内容.Rows.Add(row处方明细);
                        //        }
                        //        else
                        //        {
                        //            string sErr = s收费名称 + "库存不足，获取套餐组内容不完整";
                        //            WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                        //            MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    foreach (DataRow row套餐内容 in dt套餐内容.Rows)
                        //    {
                        //        DataRow row新行 = dt处方内容.NewRow();
                        //        bool b是否药品 = Convert.ToBoolean(row套餐内容["是否药品"]);
                        //        string s收费编码 = row套餐内容["收费编码"].ToString();
                        //        string s收费名称 = row套餐内容["收费名称"].ToString();
                        //        string s执行科室编码 = row套餐内容["执行科室编码"].ToString();
                        //        string _sPACS检查项 = row套餐内容["PACS检查项"].ToString() == "True" ? "1" : "0";
                        //        string _sLIS检查项 = row套餐内容["LIS检查项"].ToString() == "True" ? "1" : "0";
                        //        if (b是否药品)//药品套餐
                        //        {
                        //            decimal dec执行数量 = i套餐数量 * Convert.ToInt16(row套餐内容["数量"]) * i处方贴数;
                        //            drug drugInfo = new drug();
                        //            bool b查找库存 = HIS.COMM.comm.getYPIDWith药品序号(s收费编码, dec执行数量.ToString(), s药房权限, ref drugInfo);
                        //            if (b查找库存 == true)
                        //            {
                        //                string s费用序号 = "";
                        //                string s财务分类 = HIS.COMM.comm.get财务分类with药品序号(s收费编码);
                        //                if (s财务分类 == "中药费")
                        //                {
                        //                    s费用序号 = s中药费小项编码;
                        //                }
                        //                else
                        //                {
                        //                    s费用序号 = s西药费小项编码;
                        //                }
                        //                row新行["费用序号"] = s费用序号;
                        //                row新行["项目名称"] = s收费名称;
                        //                row新行["药品序号"] = s收费编码;
                        //                row新行["组别"] = i组别;
                        //                row新行["数量"] = dec执行数量;
                        //                row新行["金额"] = drugInfo.Dec医院零售价 * dec执行数量;
                        //                row新行["YPID"] = drugInfo.Sypid;
                        //                row新行["规格"] = drugInfo.S药房规格;
                        //                row新行["单位"] = drugInfo.S药房单位;
                        //                row新行["产地"] = drugInfo.S药品产地;
                        //                row新行["进价"] = drugInfo.Dec进价;
                        //                row新行["单价"] = drugInfo.Dec医院零售价;
                        //                dt处方内容.Rows.Add(row新行);
                        //            }
                        //            else
                        //            {
                        //                string sErr = s收费名称 + "库存不足，获取套餐组内容不完整";
                        //                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                        //                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //            }
                        //        }
                        //        else//检查套餐
                        //        {
                        //            row新行["费用序号"] = s收费编码;
                        //            row新行["项目名称"] = s收费名称;
                        //            row新行["数量"] = i套餐数量 * Convert.ToInt16(row套餐内容["数量"]) * i处方贴数;
                        //            row新行["单价"] = row套餐内容["单价"];
                        //            row新行["金额"] = Convert.ToDecimal(row套餐内容["单价"]) * i套餐数量 * Convert.ToInt16(row套餐内容["数量"]) * i处方贴数;
                        //            row新行["组别"] = i组别;
                        //            row新行["YPID"] = 0;
                        //            row新行["套餐编号"] = searchLookUpEdit套餐选择.EditValue.ToString();
                        //            row新行["套餐名称"] = searchLookUpEdit套餐选择.Text;
                        //            row新行["执行科室编码"] = s执行科室编码;
                        //            row新行["LIS检查项"] = _sLIS检查项;
                        //            row新行["PACS检查项"] = _sPACS检查项;
                        //            dt处方内容.Rows.Add(row新行);
                        //        }
                        //    }
                        //}
                        #endregion
                        break;
                    case HIS.COMM.ClassEnum.en套餐调用方式.药房划价:
                    case HIS.COMM.ClassEnum.en套餐调用方式.门诊收款:
                    default:
                        //foreach (DataRow row药品套餐 in dt套餐内容.Rows)
                        //{
                        //    DataRow row费用明细 = dt处方内容.NewRow();
                        //    bool b是否药品 = Convert.ToBoolean(row药品套餐["是否药品"]);
                        //    string s收费编码 = row药品套餐["收费编码"].ToString();
                        //    string s收费名称 = row药品套餐["收费名称"].ToString();
                        //    if (b是否药品)
                        //    {
                        //        decimal dec执行数量 = i套餐数量 * Convert.ToInt16(row药品套餐["数量"]) * i处方贴数;
                        //        drug drugInfo = new drug();
                        //        bool b查找库存 = HIS.COMM.comm.getYPIDWith药品序号(s收费编码, dec执行数量.ToString(), s药房权限, ref drugInfo);
                        //        if (b查找库存 == true)
                        //        {
                        //            string s费用序号 = "";
                        //            string s财务分类 = HIS.COMM.comm.get财务分类with药品序号(s收费编码);
                        //            if (s财务分类 == "中药费")
                        //            {
                        //                s费用序号 = s中药费小项编码;
                        //            }
                        //            else
                        //            {
                        //                s费用序号 = s西药费小项编码;
                        //            }
                        //            row费用明细["费用序号"] = s费用序号;
                        //            row费用明细["费用名称"] = s收费名称;
                        //            row费用明细["药品序号"] = s收费编码;
                        //            row费用明细["组别"] = -1;
                        //            row费用明细["数量"] = dec执行数量;
                        //            row费用明细["金额"] = drugInfo.Dec医院零售价 * dec执行数量;
                        //            row费用明细["YPID"] = drugInfo.Sypid;
                        //            row费用明细["规格"] = drugInfo.S药房规格;
                        //            row费用明细["单位"] = drugInfo.S药房单位;
                        //            row费用明细["产地"] = drugInfo.S药品产地;
                        //            row费用明细["进价"] = drugInfo.Dec进价;
                        //            row费用明细["单价"] = drugInfo.Dec医院零售价;
                        //            dt处方内容.Rows.Add(row费用明细);
                        //        }
                        //        else
                        //        {
                        //            string sErr = s收费名称 + "库存不足，获取套餐组内容不完整";
                        //            WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                        //            MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //        }
                        //    }
                        //    else
                        //    {
                        //        row费用明细["费用序号"] = s收费编码;
                        //        row费用明细["费用名称"] = s收费名称;
                        //        row费用明细["数量"] = i套餐数量 * Convert.ToInt16(row药品套餐["数量"]) * i处方贴数;
                        //        row费用明细["单价"] = row药品套餐["单价"];
                        //        row费用明细["金额"] = Convert.ToDecimal(row药品套餐["单价"]) * i套餐数量 * Convert.ToInt16(row药品套餐["数量"]) * i处方贴数;
                        //        row费用明细["组别"] = -1;
                        //        row费用明细["YPID"] = 0;
                        //        dt处方内容.Rows.Add(row费用明细);
                        //    }
                        //}
                        break;
                }
                configHelper.setKeyValue("套餐类型", cmb套餐类型.Text);
                configHelper.setKeyValue("套餐数据范围", radioGroup数据范围.SelectedIndex.ToString());
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void spinEdit套餐数量_EditValueChanged(object sender, EventArgs e)
        {
            //if (Convert.ToInt16(spinEdit套餐数量.EditValue) <= 0)
            //{
            //    spinEdit套餐数量.EditValue = 1;
            //    XtraMessageBox.Show("套餐数量必须大于0", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
        }

        private void cmb套餐类型_SelectedIndexChanged(object sender, EventArgs e)
        {
            voidSelect套餐摘要();
        }

        private void radioGroup套餐权限_SelectedIndexChanged(object sender, EventArgs e)
        {
            voidSelect套餐摘要();
        }


    }
}