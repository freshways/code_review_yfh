﻿// litao@Copy Right 2006-2016
// 文件： GY科室设置.cs
// 创建时间：2018-06-13
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.COMM.Model;
using HIS.COMM.IDAL;

namespace HIS.COMM.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.GY科室设置.
    /// </summary>
    public partial class GY科室设置DAL : IGY科室设置
    {

        private static readonly string SQL_SELECT药房权限 = string.Format(@"SELECT * FROM GY科室设置 where [是否单设药房]=1 and 科室编码 in 
                    (
                    select 科室编码 from [pub用户数据权限] where [用户编码]={0}
                    )", WEISHENG.COMM.zdInfo.ModelUserInfo.用户编码);


        public IList<GY科室设置Model> Get药房权限ByUserID()
        {
            IList<GY科室设置Model> list = new List<GY科室设置Model>();
            string sql = SQL_SELECT药房权限;           
            using (SqlDataReader dr = HIS.COMM.DbHelperSqlServer.ExecuteReader(sql))
            {
                while (dr.Read())
                {
                    list.Add(GetModelFromDr(dr));
                }
            }
            return list;
        }

    }
}

