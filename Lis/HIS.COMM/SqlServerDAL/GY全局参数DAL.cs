﻿// litao@Copy Right 2006-2016
// 文件： GY全局参数.cs
// 创建时间：2018-05-26
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.COMM.Model;
using HIS.COMM.IDAL;

namespace HIS.COMM.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.GY全局参数.
    /// </summary>
    public partial class GY全局参数DAL: IGY全局参数
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM GY全局参数 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM GY全局参数 WHERE ID=@ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM GY全局参数 WHERE ID=@ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM GY全局参数 WHERE ID=@ID";
		private static readonly string SQL_INSERTGY全局参数 = "INSERT INTO GY全局参数 (参数名称,参数值,备注,单位编码,updateTime) VALUES (@参数名称,@参数值,@备注,@单位编码,@updateTime)";
		private static readonly string SQL_UPDATE_GY全局参数_BY_PR = "UPDATE GY全局参数 SET 参数名称=@参数名称,参数值=@参数值,备注=@备注,单位编码=@单位编码,updateTime=@updateTime WHERE ID=@ID";
		private static readonly string PARM_PRM_GY全局参数 = "@ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public GY全局参数DAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="gy全局参数Model">GY全局参数实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(GY全局参数Model gy全局参数Model)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value = gy全局参数Model.ID;
			_param[1].Value = gy全局参数Model.参数名称;
			_param[2].Value = gy全局参数Model.参数值;
			_param[3].Value = gy全局参数Model.备注;
			_param[4].Value = gy全局参数Model.单位编码;
			_param[5].Value = gy全局参数Model.updateTime;
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_INSERTGY全局参数,_param);
			
		}
		/// <summary>
		/// 向数据表GY全局参数更新一条记录。

		/// </summary>
		/// <param name="gy全局参数Model">gy全局参数Model</param>
		/// <returns>影响的行数</returns>
		public int Update(GY全局参数Model gy全局参数Model)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=gy全局参数Model.ID;
			_param[1].Value=gy全局参数Model.参数名称;
			_param[2].Value=gy全局参数Model.参数值;
			_param[3].Value=gy全局参数Model.备注;
			_param[4].Value=gy全局参数Model.单位编码;
			_param[5].Value=gy全局参数Model.updateTime;
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_UPDATE_GY全局参数_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表GY全局参数中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  gy全局参数 数据实体
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>gy全局参数 数据实体</returns>
		private GY全局参数Model GetModelFromDr(DataRow row)
		{
			GY全局参数Model Obj = new GY全局参数Model();
			if(row!=null)
			{
				Obj.ID          = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.参数名称        =  row["参数名称"].ToString();
				Obj.参数值         =  row["参数值"].ToString();
				Obj.备注          =  row["备注"].ToString();
				Obj.单位编码        = (( row["单位编码"])==DBNull.Value)?0:Convert.ToInt32( row["单位编码"]);
				Obj.updateTime  = (( row["updateTime"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( row["updateTime"]);
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  gy全局参数 数据实体
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>gy全局参数 数据实体</returns>
		private GY全局参数Model GetModelFromDr(IDataReader dr)
		{
			GY全局参数Model Obj = new GY全局参数Model();
			
				Obj.ID         = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.参数名称       =  dr["参数名称"].ToString();
				Obj.参数值        =  dr["参数值"].ToString();
				Obj.备注         =  dr["备注"].ToString();
				Obj.单位编码       = (( dr["单位编码"])==DBNull.Value)?0:Convert.ToInt32( dr["单位编码"]);
				Obj.updateTime = (( dr["updateTime"])==DBNull.Value)?Convert.ToDateTime("1900-01-01"):Convert.ToDateTime( dr["updateTime"]);
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个GY全局参数对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>GY全局参数对象</returns>
		public GY全局参数Model GetGY全局参数 (int id)
		{
			GY全局参数Model _obj=null;			
			SqlParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表GY全局参数所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY全局参数Model> GetGY全局参数All()
		{			
			return GetGY全局参数All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表GY全局参数所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY全局参数Model> GetGY全局参数All(string sqlWhere)
		{
			IList<GY全局参数Model> list=new List<GY全局参数Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere;//.Replace("'","''");
			}
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			SqlParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(HIS.COMM.DbHelperSqlServer.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetPRMParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(PARM_PRM_GY全局参数);

            if (parms == null) {
                parms = new SqlParameter[] {						
					new SqlParameter("@ID",SqlDbType.Int)
				};
                HIS.COMM.DbHelperSqlServer.CacheParameters(PARM_PRM_GY全局参数, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetInsertParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_INSERTGY全局参数);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@参数名称",SqlDbType.VarChar),
					new SqlParameter("@参数值",SqlDbType.VarChar),
					new SqlParameter("@备注",SqlDbType.VarChar),
					new SqlParameter("@单位编码",SqlDbType.Int),
					new SqlParameter("@updateTime",SqlDbType.DateTime)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_INSERTGY全局参数, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetUpdateParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_UPDATE_GY全局参数_BY_PR);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@参数名称",SqlDbType.VarChar),
					new SqlParameter("@参数值",SqlDbType.VarChar),
					new SqlParameter("@备注",SqlDbType.VarChar),
					new SqlParameter("@单位编码",SqlDbType.Int),
					new SqlParameter("@updateTime",SqlDbType.DateTime)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_UPDATE_GY全局参数_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

