﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace HIS.COMM.SqlServerDAL
{
    public class Query医生DAL
    {
        public IList<Model.model医生> getData(string sqlWhere)
        {
            string SQL_SELECT_ALL = string.Format(@"SELECT 用户编码 AS 医生编码,
	                                                     拼音代码 + '|' + 用户名 医生助记码,[科室编码],[科室名称],用户名 医生姓名,
                                                    拼音代码
                                                    FROM pubUser
                                                    WHERE  是否有处方权 = 1 AND 是否禁用 = 0");
            IList<Model.model医生> list = new List<Model.model医生>();
            string sql = SQL_SELECT_ALL;
            if (!string.IsNullOrEmpty(sqlWhere))
            {
                sql += " and " + sqlWhere;
            }
            using (SqlDataReader dr = DbHelperSqlServer.ExecuteReader(sql))
            {
                while (dr.Read())
                {
                    list.Add(GetModelFromDr(dr));
                }
            }
            return list;
        }
      

        private Model.model医生 GetModelFromDr(IDataReader dr)
        {
            Model.model医生 Obj = new Model.model医生();
            try
            {
                Obj.医生编码 = Convert.ToInt32(dr["医生编码"]);
                Obj.医生姓名 = Convert.ToString(dr["医生姓名"]);
                Obj.拼音代码 = Convert.ToString(dr["拼音代码"]);
                Obj.医生助记码 = Convert.ToString(dr["医生助记码"]);
                Obj.科室编码 = Convert.ToInt32(dr["科室编码"]);
                Obj.科室名称 = Convert.ToString(dr["科室名称"]);

                return Obj;
            }
            catch 
            {
                Exception ex2 = new Exception(Obj.医生编码.ToString());
                throw ex2;
            }

        }
    }
}
