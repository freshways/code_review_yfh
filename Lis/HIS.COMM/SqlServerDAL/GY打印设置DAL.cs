﻿// litao@Copy Right 2006-2016
// 文件： GY打印设置.cs
// 创建时间：2018-05-26
// ===================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using HIS.COMM.Model;
using HIS.COMM.IDAL;

namespace HIS.COMM.SqlServerDAL
{
    /// <summary>
    /// 数据层实例化接口类 dbo.GY打印设置.
    /// </summary>
    public partial class GY打印设置DAL: IGY打印设置
    {
		private static readonly string SQL_SELECT_ALL = "SELECT * FROM GY打印设置 ";
		private static readonly string SQL_SELECT_BY_PR = "SELECT * FROM GY打印设置 WHERE ID=@ID";
		private static readonly string SQL_SELECT_EXIST = "SELECT COUNT(*) FROM GY打印设置 WHERE ID=@ID";
		private static readonly string SQL_DELETE_BY_PR = "DELETE FROM GY打印设置 WHERE ID=@ID";
		private static readonly string SQL_INSERTGY打印设置 = "INSERT INTO GY打印设置 (MAC地址,站点名称,打印模块,打印机名称,纸张名称,纸张宽度,纸张高度,上页边距,下页边距,左页边距,右页边距,是否直接打印,是否打印预览,备注,单位编码) VALUES (@MAC地址,@站点名称,@打印模块,@打印机名称,@纸张名称,@纸张宽度,@纸张高度,@上页边距,@下页边距,@左页边距,@右页边距,@是否直接打印,@是否打印预览,@备注,@单位编码)";
		private static readonly string SQL_UPDATE_GY打印设置_BY_PR = "UPDATE GY打印设置 SET MAC地址=@MAC地址,站点名称=@站点名称,打印模块=@打印模块,打印机名称=@打印机名称,纸张名称=@纸张名称,纸张宽度=@纸张宽度,纸张高度=@纸张高度,上页边距=@上页边距,下页边距=@下页边距,左页边距=@左页边距,右页边距=@右页边距,是否直接打印=@是否直接打印,是否打印预览=@是否打印预览,备注=@备注,单位编码=@单位编码 WHERE ID=@ID";
		private static readonly string PARM_PRM_GY打印设置 = "@ID";
		
		
		#region 构造函数

		/// <summary>
		/// 构造函数

		/// </summary>
		public GY打印设置DAL(){}
		#endregion

        #region -----------实例化接口函数-----------
		
		#region 添加更新删除
		/// <summary>
		/// 向数据库中插入一条新记录。
		/// </summary>
		/// <param name="gy打印设置Model">GY打印设置实体</param>
		/// <returns>新插入记录的编号</returns>
		public int Insert(GY打印设置Model gy打印设置Model)
		{
			
            SqlParameter[] _param = GetInsertParameters();
			
			_param[0].Value = gy打印设置Model.ID;
			_param[1].Value = gy打印设置Model.MAC地址;
			_param[2].Value = gy打印设置Model.站点名称;
			_param[3].Value = gy打印设置Model.打印模块;
			_param[4].Value = gy打印设置Model.打印机名称;
			_param[5].Value = gy打印设置Model.纸张名称;
			_param[6].Value = gy打印设置Model.纸张宽度;
			_param[7].Value = gy打印设置Model.纸张高度;
			_param[8].Value = gy打印设置Model.上页边距;
			_param[9].Value = gy打印设置Model.下页边距;
			_param[10].Value = gy打印设置Model.左页边距;
			_param[11].Value = gy打印设置Model.右页边距;
			_param[12].Value = gy打印设置Model.是否直接打印;
			_param[13].Value = gy打印设置Model.是否打印预览;
			_param[14].Value = gy打印设置Model.备注;
			_param[15].Value = gy打印设置Model.单位编码;
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_INSERTGY打印设置,_param);
			
		}
		/// <summary>
		/// 向数据表GY打印设置更新一条记录。

		/// </summary>
		/// <param name="gy打印设置Model">gy打印设置Model</param>
		/// <returns>影响的行数</returns>
		public int Update(GY打印设置Model gy打印设置Model)
		{
            SqlParameter[] _param = GetUpdateParameters();
			
			_param[0].Value=gy打印设置Model.ID;
			_param[1].Value=gy打印设置Model.MAC地址;
			_param[2].Value=gy打印设置Model.站点名称;
			_param[3].Value=gy打印设置Model.打印模块;
			_param[4].Value=gy打印设置Model.打印机名称;
			_param[5].Value=gy打印设置Model.纸张名称;
			_param[6].Value=gy打印设置Model.纸张宽度;
			_param[7].Value=gy打印设置Model.纸张高度;
			_param[8].Value=gy打印设置Model.上页边距;
			_param[9].Value=gy打印设置Model.下页边距;
			_param[10].Value=gy打印设置Model.左页边距;
			_param[11].Value=gy打印设置Model.右页边距;
			_param[12].Value=gy打印设置Model.是否直接打印;
			_param[13].Value=gy打印设置Model.是否打印预览;
			_param[14].Value=gy打印设置Model.备注;
			_param[15].Value=gy打印设置Model.单位编码;
			
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_UPDATE_GY打印设置_BY_PR,_param);
		}
		/// <summary>
		/// 删除数据表GY打印设置中的一条记录

		/// </summary>
	    /// <param name="ID">id</param>
		/// <returns>影响的行数</returns>
		public int Delete(int id)
		{
			SqlParameter[] _param = GetPRMParameters();
			_param[0].Value=id;
			return HIS.COMM.DbHelperSqlServer.ExecuteSql(SQL_DELETE_BY_PR,_param);
		}
		#endregion
		
		#region 数据实体
		/// <summary>
		/// 从DataRow得到  gy打印设置 数据实体
		/// </summary>
		/// <param name="row">DataRow</param>
		/// <returns>gy打印设置 数据实体</returns>
		private GY打印设置Model GetModelFromDr(DataRow row)
		{
			GY打印设置Model Obj = new GY打印设置Model();
			if(row!=null)
			{
				Obj.ID      = (( row["ID"])==DBNull.Value)?0:Convert.ToInt32( row["ID"]);
				Obj.MAC地址   =  row["MAC地址"].ToString();
				Obj.站点名称    =  row["站点名称"].ToString();
				Obj.打印模块    =  row["打印模块"].ToString();
				Obj.打印机名称   =  row["打印机名称"].ToString();
				Obj.纸张名称    =  row["纸张名称"].ToString();
				Obj.纸张宽度    = (( row["纸张宽度"])==DBNull.Value)?0:Convert.ToInt32( row["纸张宽度"]);
				Obj.纸张高度    = (( row["纸张高度"])==DBNull.Value)?0:Convert.ToInt32( row["纸张高度"]);
				Obj.上页边距    = (( row["上页边距"])==DBNull.Value)?0:Convert.ToInt32( row["上页边距"]);
				Obj.下页边距    = (( row["下页边距"])==DBNull.Value)?0:Convert.ToInt32( row["下页边距"]);
				Obj.左页边距    = (( row["左页边距"])==DBNull.Value)?0:Convert.ToInt32( row["左页边距"]);
				Obj.右页边距    = (( row["右页边距"])==DBNull.Value)?0:Convert.ToInt32( row["右页边距"]);
				Obj.是否直接打印  =  row["是否直接打印"] == DBNull.Value ? false :(bool) row["是否直接打印"];
				Obj.是否打印预览  =  row["是否打印预览"] == DBNull.Value ? false :(bool) row["是否打印预览"];
				Obj.备注      =  row["备注"].ToString();
				Obj.单位编码    = (( row["单位编码"])==DBNull.Value)?0:Convert.ToInt32( row["单位编码"]);
			}
			else
			{
				return null;
			}
			return Obj;
		}
		
        /// <summary>
		/// 从DataReader得到  gy打印设置 数据实体
		/// </summary>
		/// <param name="dr">DataReader</param>
		/// <returns>gy打印设置 数据实体</returns>
		private GY打印设置Model GetModelFromDr(IDataReader dr)
		{
			GY打印设置Model Obj = new GY打印设置Model();
			
				Obj.ID     = (( dr["ID"])==DBNull.Value)?0:Convert.ToInt32( dr["ID"]);
				Obj.MAC地址  =  dr["MAC地址"].ToString();
				Obj.站点名称   =  dr["站点名称"].ToString();
				Obj.打印模块   =  dr["打印模块"].ToString();
				Obj.打印机名称  =  dr["打印机名称"].ToString();
				Obj.纸张名称   =  dr["纸张名称"].ToString();
				Obj.纸张宽度   = (( dr["纸张宽度"])==DBNull.Value)?0:Convert.ToInt32( dr["纸张宽度"]);
				Obj.纸张高度   = (( dr["纸张高度"])==DBNull.Value)?0:Convert.ToInt32( dr["纸张高度"]);
				Obj.上页边距   = (( dr["上页边距"])==DBNull.Value)?0:Convert.ToInt32( dr["上页边距"]);
				Obj.下页边距   = (( dr["下页边距"])==DBNull.Value)?0:Convert.ToInt32( dr["下页边距"]);
				Obj.左页边距   = (( dr["左页边距"])==DBNull.Value)?0:Convert.ToInt32( dr["左页边距"]);
				Obj.右页边距   = (( dr["右页边距"])==DBNull.Value)?0:Convert.ToInt32( dr["右页边距"]);
				Obj.是否直接打印 =  dr["是否直接打印"] == DBNull.Value ? false :(bool) dr["是否直接打印"];
				Obj.是否打印预览 =  dr["是否打印预览"] == DBNull.Value ? false :(bool) dr["是否打印预览"];
				Obj.备注     =  dr["备注"].ToString();
				Obj.单位编码   = (( dr["单位编码"])==DBNull.Value)?0:Convert.ToInt32( dr["单位编码"]);
			
			return Obj;
		}
		#endregion
		
		/// <summary>
		/// 根据ID,返回一个GY打印设置对象
		/// </summary>
		/// <param name="id">id</param>
		/// <returns>GY打印设置对象</returns>
		public GY打印设置Model GetGY打印设置 (int id)
		{
			GY打印设置Model _obj=null;			
			SqlParameter[] _param = GetPRMParameters();			
			_param[0].Value=id;			
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(SQL_SELECT_BY_PR,_param))
			{
				if(dr.Read())
				{
            		_obj=GetModelFromDr(dr);
				}
			}
			return _obj;
		}
		/// <summary>
		/// 得到数据表GY打印设置所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY打印设置Model> GetGY打印设置All()
		{			
			return GetGY打印设置All("");
		}
		/// <summary>
		/// 根据查询条件得到数据表GY打印设置所有记录

		/// </summary>
		/// <returns>数据集</returns>
		public IList<GY打印设置Model> GetGY打印设置All(string sqlWhere)
		{
			IList<GY打印设置Model> list=new List<GY打印设置Model>();			
			string sql = SQL_SELECT_ALL;
			if(!string.IsNullOrEmpty(sqlWhere))
			{
				sql += " WHERE "+ sqlWhere;//.Replace("'","''");
			}
			using(SqlDataReader dr=HIS.COMM.DbHelperSqlServer.ExecuteReader(sql))
			{
				while(dr.Read())
				{
					list.Add(GetModelFromDr(dr));
				}
			}
			return list;
		}
		/// <summary>
        /// 检测是否存在根据主键

        /// </summary>
        /// <param name="id">id</param>
        /// <returns>是/否</returns>
		public bool IsExist(int id)
		{			
			SqlParameter[] _param = GetPRMParameters();
            _param[0].Value=id;
            int a = Convert.ToInt32(HIS.COMM.DbHelperSqlServer.GetSingle(SQL_SELECT_EXIST,_param));
            if(a > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
		}

        #endregion
		
		#region 私有包括（缓存功能）
		/// <summary>
        /// 缓存主键参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetPRMParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(PARM_PRM_GY打印设置);

            if (parms == null) {
                parms = new SqlParameter[] {						
					new SqlParameter("@ID",SqlDbType.Int)
				};
                HIS.COMM.DbHelperSqlServer.CacheParameters(PARM_PRM_GY打印设置, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存插入操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetInsertParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_INSERTGY打印设置);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@MAC地址",SqlDbType.VarChar),
					new SqlParameter("@站点名称",SqlDbType.VarChar),
					new SqlParameter("@打印模块",SqlDbType.VarChar),
					new SqlParameter("@打印机名称",SqlDbType.VarChar),
					new SqlParameter("@纸张名称",SqlDbType.VarChar),
					new SqlParameter("@纸张宽度",SqlDbType.Int),
					new SqlParameter("@纸张高度",SqlDbType.Int),
					new SqlParameter("@上页边距",SqlDbType.Int),
					new SqlParameter("@下页边距",SqlDbType.Int),
					new SqlParameter("@左页边距",SqlDbType.Int),
					new SqlParameter("@右页边距",SqlDbType.Int),
					new SqlParameter("@是否直接打印",SqlDbType.Bit),
					new SqlParameter("@是否打印预览",SqlDbType.Bit),
					new SqlParameter("@备注",SqlDbType.VarChar),
					new SqlParameter("@单位编码",SqlDbType.Int)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_INSERTGY打印设置, parms);
            }
            return parms;
        }
		/// <summary>
        /// 缓存更新操作参数
        /// </summary>
        /// <returns></returns>
		 private static SqlParameter[] GetUpdateParameters() {
            SqlParameter[] parms = HIS.COMM.DbHelperSqlServer.GetCachedParameters(SQL_UPDATE_GY打印设置_BY_PR);

            if (parms == null) {
                parms = new SqlParameter[] {	
					new SqlParameter("@ID",SqlDbType.Int),
					new SqlParameter("@MAC地址",SqlDbType.VarChar),
					new SqlParameter("@站点名称",SqlDbType.VarChar),
					new SqlParameter("@打印模块",SqlDbType.VarChar),
					new SqlParameter("@打印机名称",SqlDbType.VarChar),
					new SqlParameter("@纸张名称",SqlDbType.VarChar),
					new SqlParameter("@纸张宽度",SqlDbType.Int),
					new SqlParameter("@纸张高度",SqlDbType.Int),
					new SqlParameter("@上页边距",SqlDbType.Int),
					new SqlParameter("@下页边距",SqlDbType.Int),
					new SqlParameter("@左页边距",SqlDbType.Int),
					new SqlParameter("@右页边距",SqlDbType.Int),
					new SqlParameter("@是否直接打印",SqlDbType.Bit),
					new SqlParameter("@是否打印预览",SqlDbType.Bit),
					new SqlParameter("@备注",SqlDbType.VarChar),
					new SqlParameter("@单位编码",SqlDbType.Int)
					};
                HIS.COMM.DbHelperSqlServer.CacheParameters(SQL_UPDATE_GY打印设置_BY_PR, parms);
            }
            return parms;
        }			
		#endregion
		
    }
}

