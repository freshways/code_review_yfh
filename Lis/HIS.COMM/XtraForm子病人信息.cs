﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using System.Data.SqlClient;
using System.Collections;
using HIS.Model.Pojo;

namespace HIS.COMM
{

    public partial class XtraForm子病人信息 : DevExpress.XtraEditors.XtraForm
    {
        Pojo病人信息 person父;
        DataSet ds父子信息 = new DataSet();
        string SQL子病人信息 = "";

        string s父ZYID;
        string _sCurr类别 = "";

        public XtraForm子病人信息(Pojo病人信息 _person)
        {
            InitializeComponent();
            person父 = _person;
        }


        private void XtraForm子病人信息_Load(object sender, EventArgs e)
        {
            try
            {
                string SQL父病人列表 =
                 "select 住院号码, 病人姓名" + "\r\n" +
                 "from   [ZY病人信息]" + "\r\n" +
                 "where  ZYID  = " + person父.ZYID;//in (select distinct 父zyid from [ZY子病人信息]) and 已出院标记 = 0 and 病区

                ds父子信息 = HIS.Model.Dal.autoDb.dsGetData(SQL父病人列表, "dt父病人列表", HIS.COMM.DBConnHelper.SConnHISDb);
                treeList类别.DataSource = ds父子信息.Tables["dt父病人列表"];
                treeList类别.KeyFieldName = "住院号码";
                treeList类别.ParentFieldName = "住院号码";
                treeList类别.ExpandAll();

                SQL子病人信息 =
                       "SELECT [子ZYID], [住院号码], [医疗证号], [病人姓名], [性别], [出生日期], [身份证号], [父ZYID], ID" + "\r\n" +
                       "FROM   [ZY子病人信息] where 父zyid=" + person父.ZYID;
                ds父子信息 = HIS.Model.Dal.autoDb.dsGetData(SQL子病人信息, "dt类别", HIS.COMM.DBConnHelper.SConnHISDb);
                ds父子信息.Tables.Add(HIS.Model.Dal.autoDb.dsGetData(SQL子病人信息, "dt子病人信息", HIS.COMM.DBConnHelper.SConnHISDb).Tables[0].Copy());
                gridControl内容.DataSource = ds父子信息.Tables["dt子病人信息"];
                gridView内容.PopulateColumns();
                gridView内容.BestFitColumns();

            }
            catch (Exception ex)
            {
                string sErr = ex.Message;
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
                MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void simpleButton保存_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 iNewZYID = Convert.ToInt32(HIS.COMM.BLL.Class入院登记.makeNew_sZYID(HIS.COMM.zdInfo.Model单位信息.iDwid.ToString()));
                if (ds父子信息.HasChanges() == false)
                {
                    XtraMessageBox.Show("当前数据未修改", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    //在保存时生成ZYID，防止和住院处入院冲突
                    foreach (DataRow row in ds父子信息.Tables["dt子病人信息"].Rows)
                    {
                        if (row["子ZYID"].ToString() == "-1")
                        {
                            row["子ZYID"] = iNewZYID.ToString();
                            iNewZYID += 1;
                        }
                    }
                    HIS.Model.Dal.autoDb.dsUpData(ds父子信息, "dt子病人信息", SQL子病人信息, HIS.COMM.DBConnHelper.SConnHISDb);
                    XtraMessageBox.Show("保存成功", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ee)
            {
                WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", ee.Message.ToString().Replace("'", "''"));
                MessageBox.Show(ee.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void treeList子病人信息_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            //try
            //{
            //    if (ds子病人信息.Tables.Count == 1)
            //    {
            //        return;
            //    }
            //    DataRowView drv = treeList类别.GetDataRecordByNode(e.Node) as DataRowView;
            //    if (drv != null)
            //    {
            //        _sCurr类别 = (string)drv["类别"];
            //        switch (_sCurr类别)
            //        {
            //            case "医嘱项目":
            //                gridControl内容.DataSource = ds子病人信息.Tables["dt内容医嘱项目"];
            //                break;
            //            case "医嘱用法":
            //                gridControl内容.DataSource = ds子病人信息.Tables["dt内容医嘱用法"];
            //                break;
            //            case "医嘱频次":
            //                gridControl内容.DataSource = ds子病人信息.Tables["dt内容医嘱频次"];
            //                break;
            //            case "医嘱部位":
            //                gridControl内容.DataSource = ds子病人信息.Tables["dt内容医嘱部位"];
            //                break;
            //            case "医嘱剂量":
            //                gridControl内容.DataSource = ds子病人信息.Tables["dt内容医嘱剂量"];
            //                break;
            //            default:
            //                gridControl内容.DataSource = null;
            //                break;
            //        }
            //        gridView内容.PopulateColumns();
            //        //gridView内容.Columns["ID"].Visible = false;
            //        gridView内容.BestFitColumns();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    string sErr = ex.Message;
            //    WEISHENG.COMM.LogHelper.Info(this.Text, "错误信息", sErr);
            //    MessageBox.Show(sErr, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void simpleButton删除_Click(object sender, EventArgs e)
        {
            try
            {
                string sZYID = gridView内容.GetDataRow(gridView内容.FocusedRowHandle)["子ZYID"].ToString();
                int iCount = Convert.ToInt32(HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.DBConnHelper.SConnHISDb, CommandType.Text, "select count(*) from ys住院医嘱 where ZYID=" + sZYID));
                if (iCount > 0)
                {
                    MessageBox.Show("当前病人已经存在业务信息，不允许删除", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (XtraMessageBox.Show("你确定要删除选中的记录吗？", "删除提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    int iSelectRowCount = gridView内容.SelectedRowsCount;
                    if (iSelectRowCount > 0)
                    {
                        gridView内容.DeleteSelectedRows();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void simpleButton放弃_Click(object sender, EventArgs e)
        {
            if (ds父子信息.GetChanges() == null)
            {
                XtraMessageBox.Show("当前数据修改", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (XtraMessageBox.Show("您确定放弃当前全部数据修改吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            ds父子信息.RejectChanges();
        }

        private void simpleButton刷新_Click(object sender, EventArgs e)
        {
            XtraForm子病人信息_Load(null, null);
        }

        private void simpleButton增加_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList readonlyList = new ArrayList();
                string sNewZYID = "-1";//在保存时生成，避免冲突
                int i子序 = Convert.ToInt16(ds父子信息.Tables["dt子病人信息"].Compute("count(父ZYID)+1", "父ZYID=" + person父.ZYID));

                //HIS.Model.Dal.SqlHelper.ExecuteScalar(HIS.COMM.ClassDBConnstring.SConnHISDb, CommandType.Text,
                //"select count(*)+1 from zy子病人信息 where 父zyid=" + person父.sZYID)

                gridView内容.AddNewRow();
                gridView内容.SetRowCellValue(gridView内容.FocusedRowHandle, gridView内容.Columns["父ZYID"], person父.ZYID);
                gridView内容.SetRowCellValue(gridView内容.FocusedRowHandle, gridView内容.Columns["子ZYID"], sNewZYID);
                gridView内容.SetRowCellValue(gridView内容.FocusedRowHandle, gridView内容.Columns["病人姓名"], person父.病人姓名 + "之子");
                gridView内容.SetRowCellValue(gridView内容.FocusedRowHandle, gridView内容.Columns["性别"], "男");
                gridView内容.SetRowCellValue(gridView内容.FocusedRowHandle, gridView内容.Columns["出生日期"], DateTime.Now.ToShortDateString());
                gridView内容.SetRowCellValue(gridView内容.FocusedRowHandle, gridView内容.Columns["身份证号"], "");
                gridView内容.SetRowCellValue(gridView内容.FocusedRowHandle, gridView内容.Columns["住院号码"], person父.住院号码 + "." + i子序);

                readonlyList.Add("ID");
                readonlyList.Add("父ZYID");
                readonlyList.Add("子ZYID");
                readonlyList.Add("住院号码");
                if (EditRecord(gridControl内容, gridView内容, readonlyList))
                    gridView内容.UpdateCurrentRow();
                else gridView内容.CancelUpdateCurrentRow();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private bool EditRecord(DevExpress.XtraGrid.GridControl gridControlT, DevExpress.XtraGrid.Views.Grid.GridView gridViewT, ArrayList myAL)
        {
            HIS.COMM.PopupFormGridEditDataInPopupForm frm = new HIS.COMM.PopupFormGridEditDataInPopupForm();
            DataRow row = gridViewT.GetDataRow(gridViewT.FocusedRowHandle);

            frm.InitData(this.FindForm(), gridControlT, gridViewT, row, myAL);
            bool ret = frm.ShowDialog() == DialogResult.OK;
            if (ret)
            {
                int count = frm.Row.Table.Columns.Count;
                object[] itemarry = new object[count];
                for (int i = 0; i < count; i++)
                    itemarry[i] = frm.Row.ItemArray[i];

                itemarry[0] = null;//ID字段只读，曲线救国

                row.ItemArray = itemarry;//frm.Row.ItemArray;
                row.EndEdit();
            }
            return ret;
        }

        private void simpleButton编辑_Click(object sender, EventArgs e)
        {
            ArrayList readonlyList = new ArrayList();
            //switch (_sCurr类别)
            //{
            //    case "医嘱项目":
            readonlyList.Add("ID");
            readonlyList.Add("项目编号");
            EditRecord(gridControl内容, gridView内容, readonlyList);
            //        break;
            //    case "医嘱用法":
            //    case "医嘱频次":
            //    case "医嘱剂量":
            //    case "医嘱部位":
            //        readonlyList.Add("ID");
            //        EditRecord(gridControl内容, gridView内容, readonlyList);
            //        break;
            //    default:
            //        break;
            //}


        }
    }
}
