﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using HIS.Model;

namespace HIS.COMM
{
    public partial class XtraForm通知公告列表 : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        private string sConn;
        public XtraForm通知公告列表(string _sConn)
        {
            InitializeComponent();
            sConn = _sConn;
        }

        private void XtraForm通知公告列表_Load(object sender, EventArgs e)
        {
            var date = DateTime.Now.Date.AddDays(-30);
            var sql = $"select null content,createdate,createuser,ctype,iid,title,[长期显示标记],[弹窗提示标记],[附件内容],[附件文件名],[通知GUID]  from pubnoteic where createdate>='{date.ToShortDateString()}' " +
                $"or 长期显示标记=1 or 弹窗提示标记=1 order by createdate desc";
            var list通知 = chis.Database.SqlQuery<pubnoteic>(sql).ToList();//.pubnoteics.Where(c => c.createdate >= date || c.长期显示标记 == true).ToList();
            gridControl1.DataSource = list通知;
            gridView1.Columns["createdate"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            gridView1.BestFitColumns();
            //using (SqlConnection con = new SqlConnection(sConn))
            //{
            //    string strSql = "select ctype,title,createdate,iid , convert(bit,0) as bolchk from pubnoteic order by createdate desc";
            //    DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(con, CommandType.Text, strSql).Tables[0];
            //    gridControl1.DataSource = dt;
            //    gridView1.Columns["createdate"].DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            //    gridView1.BestFitColumns();
            //}
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            //object colID = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["iid"]);
            var currModel = gridView1.GetFocusedRow() as pubnoteic;
            HIS.COMM.XtraForm通知公告查看 frm = new HIS.COMM.XtraForm通知公告查看(currModel);
            frm.colID = currModel.iid.ToString();
            frm.Show();
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            gridView1.ShowFilterEditor(gridView1.CalcHitInfo(Control.MousePosition).Column);
        }
    }
}