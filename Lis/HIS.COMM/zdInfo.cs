using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Data;
using System.Linq;

namespace HIS.COMM
{
    /// <summary>
    /// 登陆站点基本信息
    /// </summary>
    public static partial class zdInfo
    {

        static zdInfo()
        {
            _Model单位信息.sDwmc = Helper.EnvironmentHelper.CommStringGet("医院名称", "", "");
            _Model单位信息.iDwid = Convert.ToInt32(Helper.EnvironmentHelper.CommStringGet("医院编码", "0", ""));
            _Model单位信息.S组织机构代码 = Helper.EnvironmentHelper.CommStringGet("组织机构代码", "", "");
            _Model单位信息.WxAppID = Helper.EnvironmentHelper.CommStringGet("微信AppID", "", "");
            _Model单位信息.Mch_ID = Helper.EnvironmentHelper.CommStringGet("微信Mch_ID", "", "");
            _Model单位信息.S医保单位编码 = Helper.EnvironmentHelper.CommStringGet("医保单位编码", "", "");
            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb)))
            {
                if (chis.GY测试站点.Where(c => c.MAC == WEISHENG.COMM.zdInfo.ModelHardInfo.MacAddress).FirstOrDefault() != null)
                {
                    _Model单位信息.sDwmc = "测试单位";
                }
            }
        }



        private static Int32 _i周期流水号 = 0;

        public static string S业务周期号
        {
            get
            {
                _i周期流水号 += 1;
                return string.Format("{0}-{1}-{2}", WEISHENG.COMM.zdInfo.ModelHardInfo.CpuID, DateTime.Now.ToString("yyyyMMddHHmmss"), _i周期流水号.ToString());
            }
            //set { baseInfo._s医院交易流水号 = value; }
        }



        /// <summary>
        /// 卫生室站点信息
        /// </summary>
        public class YTH
        {
            private static bool _b开启分级诊疗 = false;

            public static bool b开启分级诊疗
            {
                get { return YTH._b开启分级诊疗; }
                set { YTH._b开启分级诊疗 = value; }
            }
            private static string _s医院医保编码;

            public static string S医院医保编码
            {
                get { return _s医院医保编码; }
                set { _s医院医保编码 = value; }
            }

            private string _sWsyName;

            public string sWsyName
            {
                get { return _sWsyName; }
                set { _sWsyName = value; }
            }
            private static string _sWssName;

            public static string sWssName
            {
                get { return YTH._sWssName; }
                set { YTH._sWssName = value; }
            }
            private static string _sWssID;

            public static string sWssID
            {
                get { return _sWssID; }
                set { _sWssID = value; }
            }
            private static string _sWsyID;

            public static string sWsyID
            {
                get { return _sWsyID; }
                set { _sWsyID = value; }
            }
        }


        private static DataTable _dt用户权限;

        public static DataTable dt用户权限
        {
            get { return _dt用户权限; }
            set { _dt用户权限 = value; }
        }

        [Obsolete("弃用,用Model.UserInfoModel替代", false)]


        //public class Ybjk
        //{
        //    private static StringBuilder strbBaseInfo = new StringBuilder();

        //    public static StringBuilder StrbBaseInfo
        //    {
        //        get { return Ybjk.strbBaseInfo; }
        //        set { Ybjk.strbBaseInfo = value; }
        //    }

        //    private static string _sInterfaceVer;

        //    public static string SInterfaceVer
        //    {
        //        get { return _sInterfaceVer; }
        //        set { _sInterfaceVer = value; }
        //    }
        //}

        private static string _sError = "";

        public static string sError
        {
            get { return zdInfo._sError; }
            set { zdInfo._sError = value; }
        }


        /// <summary>
        /// 登录站点的药方权限，用于药方库存相关操作（划价，处方开具等）
        /// </summary>
        private static string yfqx;

        public static string sYfqx
        {
            get { return yfqx; }
            set { yfqx = value; }
        }

        private static DataTable _dtYfqx;

        //public static DataTable dt药房数据权限
        //{
        //    get { return zdInfo._dtYfqx; }
        //    set { zdInfo._dtYfqx = value; }
        //}

        private static DataTable _dtfylist;

        public static DataTable dt分院列表
        {
            get { return zdInfo._dtfylist; }
            set { zdInfo._dtfylist = value; }
        }


        /// <summary>
        /// 可以访问数据的科室，1、医嘱提示；2、病区床位管理；3、医生病人管理；
        /// </summary>
        private static DataTable _dtKsqx;

        public static DataTable dtKsqx
        {
            get { return zdInfo._dtKsqx; }
            set { zdInfo._dtKsqx = value; }
        }


        private static string _strKsqx;

        public static string s科室数据权限
        {
            get { return zdInfo._strKsqx; }
            set { zdInfo._strKsqx = value; }
        }




        //private static decimal _dec欠款下限 = 99999999M;

        //public static decimal dec医院欠款下限
        //{
        //    get { return zdInfo._dec欠款下限; }
        //    set { zdInfo._dec欠款下限 = value; }
        //}



        //private static string _xlType = string.Empty;//线路类型，移动还是联通

        //public static string s线路类型
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_xlType))
        //        {
        //            string _temp = ConfigurationManager.AppSettings.Get("线路类型");
        //            if (_temp == null)
        //            {
        //                _xlType = "联通线路";
        //                //ConfigurationManager.AppSettings.Add("线路类型", "联通线路");
        //            }
        //            else
        //            {
        //                _xlType = _temp;
        //            }
        //        }
        //        return zdInfo._xlType;
        //    }
        //    set { zdInfo._xlType = value; }
        //}



        //private static string histype;
        //public static string hisType
        //{
        //    get { return zdInfo.histype; }
        //    set { zdInfo.histype = value; }
        //}

        //public static string S程序运行目录
        //{
        //    get
        //    {
        //        return System.Windows.Forms.Application.StartupPath;
        //    }
        //}

        //public static string S应用程序文件完整名
        //{
        //    get
        //    {
        //        return System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
        //    }
        //}

        public static model单位信息 Model单位信息
        {
            get
            {
                return _Model单位信息;
            }

            set
            {
                _Model单位信息 = value;
            }
        }

        public static pubUser ModelUserInfo
        {
            get
            {
                return _ModelUserInfo;
            }

            set
            {
                _ModelUserInfo = value;
            }
        }

        public static GY科室设置 Model科室信息
        {
            get
            {
                return _Model科室信息;
            }

            set
            {
                _Model科室信息 = value;
            }
        }

        public static GY站点设置 Model站点信息
        {
            get
            {
                return _Model站点信息;
            }

            set
            {
                _Model站点信息 = value;
            }
        }


        private static GY站点设置 _Model站点信息 = new GY站点设置();
        private static GY科室设置 _Model科室信息 = new GY科室设置();
        private static pubUser _ModelUserInfo = new pubUser();
        private static model单位信息 _Model单位信息 = new model单位信息();


    }

}
