using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid;
using System.Collections.Generic;

namespace HIS.COMM
{
    /// <summary>
    /// Summary description for XtraForm项目明细.
    /// </summary>
    public partial class PopupFormGridEditDataInPopupForm : System.Windows.Forms.Form
    {
        public PopupFormGridEditDataInPopupForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            //  Add any constructor code after InitializeComponent call
            //
        }

        public DataRow Row
        {
            get { return cardView1.GetDataRow(0); }
        }

        private void InitLocation(Form frm)
        {
            this.Top = frm.Top + (frm.Height - this.Height) / 2;
            this.Left = frm.Left + (frm.Width - this.Width) / 2;
        }

        public void InitData(Form frm, GridControl grid, GridView view, DataRow row, ArrayList al只读列)
        {
            InitLocation(frm);
            foreach (GridColumn col in view.Columns)
            {
                GridColumn column = cardView1.Columns.Add();
                column.Caption = col.GetTextCaption();
                column.FieldName = col.FieldName;
                column.ColumnEdit = col.ColumnEdit;
                column.DisplayFormat.Assign(col.DisplayFormat);
                column.VisibleIndex = col.VisibleIndex;
                column.Visible = true; //col.Visible;

                for (int i = 0; i < al只读列.Count; i++) //再增加3个元素
                {
                    if (col.FieldName == Convert.ToString(al只读列[i]))
                    {
                        column.OptionsColumn.ReadOnly = true;
                        column.AppearanceCell.BackColor = Color.Silver;
                        column.AppearanceCell.BackColor2 = Color.Silver;
                    }
                }
            }

            DataTable tbl = ((DataTable)grid.DataSource).Clone();
            tbl.Rows.Add(row.ItemArray);
            gridControl1.DataSource = tbl;
            cardView1.FocusedColumn = cardView1.Columns[0];
        }
        public void InitData2<T>(Form frm, GridControl grid, GridView view, List<T> row, ArrayList al只读列)
        {
            InitLocation(frm);
            foreach (GridColumn col in view.Columns)
            {
                GridColumn column = cardView1.Columns.Add();
                column.Caption = col.GetTextCaption();
                column.FieldName = col.FieldName;
                column.ColumnEdit = col.ColumnEdit;
                column.DisplayFormat.Assign(col.DisplayFormat);
                column.VisibleIndex = col.VisibleIndex;
                column.Visible = true; //col.Visible;

                for (int i = 0; i < al只读列.Count; i++) //再增加3个元素
                {
                    if (col.FieldName == Convert.ToString(al只读列[i]))
                    {
                        column.OptionsColumn.ReadOnly = true;
                        column.AppearanceCell.BackColor = Color.Silver;
                        column.AppearanceCell.BackColor2 = Color.Silver;
                    }
                }
            }
            gridControl1.DataSource = row;
            cardView1.FocusedColumn = cardView1.Columns[0];
        }

        private void simpleButton1_Click(object sender, System.EventArgs e)
        {
            if (Row != null)
            {
                Row.EndEdit();
            }

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }
    }
}
