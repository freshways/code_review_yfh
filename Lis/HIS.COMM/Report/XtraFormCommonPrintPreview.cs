﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using HIS.COMM;
using HIS.COMM.Helper;
using HIS.Model;
using HIS.Model.Pojo;

namespace HIS.COMM.Report
{
    public partial class XtraFormCommonPrintPreview : DevExpress.XtraEditors.XtraForm
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        XtraReport report;
        PrintParams printParam;
        public XtraFormCommonPrintPreview(XtraReport report, PrintParams printParam)
        {
            InitializeComponent();
            this.report = report;
            this.printParam = printParam;
        }

        //保存站点打印设置
        private void barButtonItemSaveSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var test1 = report.Landscape;

            DevExpress.XtraPrinting.XtraPageSettingsBase settings = (DevExpress.XtraPrinting.XtraPageSettingsBase)documentViewer1.Document.PageSettings;
            var settings2 = documentViewer1.Document.PageSettings;

            PubStationPaperSetting paper = chis.PubStationPaperSettings.Where(c => c.StationID == HIS.COMM.zdInfo.Model站点信息.Mac地址 &&
            c.InvokeModuleName == printParam.invokeModuleName && c.PaperName == printParam.paperName).FirstOrDefault();
            if (paper == null)
            {
                paper = new HIS.Model.PubStationPaperSetting()
                {
                    StationID = HIS.COMM.zdInfo.Model站点信息.Mac地址,
                    StationName = HIS.COMM.zdInfo.Model站点信息.站点名称,
                    PaperName = printParam.paperName,
                    PaperWidth = report.PageWidth,
                    PaperHeight = report.PageHeight,
                    MarginTop = settings.TopMargin,
                    MarginBottom = settings.BottomMargin,
                    MarginLeft = settings.LeftMargin,
                    MarginRight = settings.RightMargin,
                    IsPreView = printParam.isPreview,
                    Remark = "添加站点自定义大小",
                    CanPrint = printParam.canPrint,
                    PrinterName = printParam.printName,
                    InvokeModuleName = printParam.invokeModuleName,
                };
            }
            else
            {
                paper.PaperWidth = report.PageWidth;
                paper.PaperHeight = report.PageHeight;

                paper.MarginTop = Convert.ToInt32(settings.TopMarginF / 1.18);
                paper.MarginBottom = Convert.ToInt32(settings.BottomMarginF / 1.18);
                paper.MarginLeft = Convert.ToInt32(settings.LeftMarginF / 1.18);
                paper.MarginRight = Convert.ToInt32(settings.RightMarginF / 1.18);

                paper.Landscape = settings.Landscape;
                if (paper.Landscape)//如果是横向，打印组件调换了报表的高和宽，如果数据库保存成了 横向，则页面长宽在数据库里要保持原料的值，否则调换 + 横向，出来的结果很纠结
                {
                    paper.PaperHeight = Convert.ToInt32(settings2.PageSize.Width / 1.18);
                    paper.PaperWidth = Convert.ToInt32(settings2.PageSize.Height / 1.18);
                }
                else
                {
                    paper.PaperHeight = Convert.ToInt32(settings2.PageSize.Height / 1.18);
                    paper.PaperWidth = Convert.ToInt32(settings2.PageSize.Width / 1.18);
                }
            }

            using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
            {
                if (paper.ID == 0)
                {
                    chis.PubStationPaperSettings.Attach(paper);
                    chis.Entry(paper).State = System.Data.Entity.EntityState.Added;
                }
                else
                {
                    chis.Entry(paper).State = System.Data.Entity.EntityState.Modified;
                }
                chis.SaveChanges();
                msgBalloonHelper.BalloonShow("保存成功");              
            }          
        }

        private void XtraForm医嘱打印_Load(object sender, EventArgs e)
        {
            documentViewer1.DocumentSource = report;
        }
    }

}

