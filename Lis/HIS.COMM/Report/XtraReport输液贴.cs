﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace HIS.COMM
{
    public partial class XtraReport输液贴 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport输液贴(DataTable _dt)
        {
            InitializeComponent();
            Set绑定数据(_dt);
        }

        public XtraReport输液贴(string _syt内容, string _s姓名, string _s频次用法)
        {
            InitializeComponent();
            xrLabel输液贴内容.Text = _syt内容;
            xrLabel病人姓名.Text = _s姓名;
            xrLabel频次用法.Text = _s频次用法;
        }

        private void Set绑定数据(DataTable dt)
        {
            DataSource = dt;
            this.xrLabel病人姓名.DataBindings.Add("Text", DataSource, "表头");
            this.xrLabel频次用法.DataBindings.Add("Text", DataSource, "频次用法");
            this.xrLabel输液贴内容.DataBindings.Add("Text", DataSource, "内容");
        }

    }
}
