﻿using DevExpress.XtraReports.UI;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Data;
using System.Linq;
using WEISHENG.COMM;

namespace HIS.COMM
{
    public partial class XtraReport长期医嘱 : DevExpress.XtraReports.UI.XtraReport
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        Pojo病人信息 pojo病人信息;

        public XtraReport长期医嘱(Pojo病人信息 person, bool _b是否续打, DateTime _dateTime界面续打时间)
        {
            InitializeComponent();

            b是否续打.Value = _b是否续打;
            pojo病人信息 = person;
            xrLabel医院名称.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc;
            xrLabel姓名.Text = "姓名:" + person.病人姓名;
            xrLabel科别.Text = "科别:" + person.科室名称;
            xrLabel床号.Text = "床号:" + person.病床;
            xrLabel病历号.Text = "病历号:" + person.住院号码;
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
            var item床位 = chis.ZY床位设置.Where(c => c.床位名称 == person.病床 && c.病区 == person.病区).FirstOrDefault();
            if (item床位!=null)
            {
                xrLabel病室.Text = "病室：" + item床位.房间号;
            }

            int out表格_表头尾_显示起始页 = 0;  // 表格表头表尾是否打印控制标记
            this.DataSource = BLL.BLL医嘱打印内容.get医嘱打印内容(person.ZYID, _b是否续打, _dateTime界面续打时间, out out表格_表头尾_显示起始页, "长期医嘱");
            var printSignParam = HIS.COMM.Helper.EnvironmentHelper.MedicalOrderSignaturePrintParam();
            if (printSignParam.Contains("开嘱医生"))
            {
                xrPictureBox开嘱医师手签.Visible = true;
            }
            else
            {
                xrPictureBox开嘱医师手签.Visible = false;
            }
            if (printSignParam.Contains("开嘱执行"))
            {
                xrPictureBox开嘱护士手签.Visible = true;
            }
            else
            {
                xrPictureBox开嘱护士手签.Visible = false;
            }

            if (printSignParam.Contains("停嘱医生"))
            {
                xrPictureBox停嘱医师手签.Visible = false;
            }
            else
            {
                xrPictureBox停嘱医师手签.Visible = false;
            }
            if (printSignParam.Contains("停嘱执行"))
            {
                xrPictureBox停嘱护士手签.Visible = true;
            }
            else
            {
                xrPictureBox停嘱护士手签.Visible = false;
            }

            //2、边框打印控制,通过格式设置  Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],'None',?)  控制早于续打时间点的医嘱不打印边框
            if (!_b是否续打)
            {
                i表格_表头尾_显示起始页.Value = 0;
            }
            else
            {
                i表格_表头尾_显示起始页.Value = out表格_表头尾_显示起始页;
            }

        }


        private void XtraReport长期医嘱_FillEmptySpace(object sender, BandEventArgs e)
        {


        }



        #region 格式控制

        string s开嘱日期;
        string s开嘱时间;
        string s用法;
        bool b下划线 = true;
        string s组别符号 = "";
        string S_医嘱内容 = string.Empty;
        private void xrTable医嘱内容_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }


        private void xrTableCell频次_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrTableCell用法_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
        int irow = 0;
        private void xrTableCell开嘱日期_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }



        private void xrTableCell开嘱时间_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
        #endregion




        private void xrTableCell医师_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrTableCell护士_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void XtraReport长期医嘱_ParametersRequestSubmit(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
        {

        }

        private void xrTableCell停嘱日期_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrTableCell停嘱时间_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void XtraReport长期医嘱_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                ZY病人信息 inpatient = chis.ZY病人信息.Where(c => c.ZYID == pojo病人信息.ZYID).FirstOrDefault();
                inpatient.长期医嘱续打时间点 = DateTime.Now;

                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
                {
                    chis.Database.Log = s => LogHelper.LogToFile("ef6调试", s);
                    chis.ZY病人信息.Attach(inpatient);
                    chis.Entry(inpatient).State = System.Data.Entity.EntityState.Modified;
                    chis.Entry(inpatient).Property(c => c.长期医嘱续打时间点).IsModified = true;
                    chis.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
        }

    }
}


