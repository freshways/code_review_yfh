﻿namespace HIS.COMM
{
    partial class XtraReport临时医嘱
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable医嘱内容 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell开嘱日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell开嘱时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell医嘱内容 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell剂量数量 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell组别符号 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell用法 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell频次 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell医师 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox开嘱医师手签 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell执行护士 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox开嘱护士手签 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableCell执行时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel病历号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel床号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel病室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel科别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel医院名称 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.b是否续打 = new DevExpress.XtraReports.Parameters.Parameter();
            this.s续打时间点 = new DevExpress.XtraReports.Parameters.Parameter();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.StyleNoVisible = new DevExpress.XtraReports.UI.XRControlStyle();
            this.StyleVisible = new DevExpress.XtraReports.UI.XRControlStyle();
            this.i表格_表头尾_显示起始页 = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable医嘱内容)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable医嘱内容});
            this.Detail.HeightF = 36F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable医嘱内容
            // 
            this.xrTable医嘱内容.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable医嘱内容.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrTable医嘱内容.Name = "xrTable医嘱内容";
            this.xrTable医嘱内容.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable医嘱内容.SizeF = new System.Drawing.SizeF(764.6248F, 36F);
            this.xrTable医嘱内容.StylePriority.UseBorders = false;
            this.xrTable医嘱内容.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTable医嘱内容_BeforePrint);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell开嘱日期,
            this.xrTableCell开嘱时间,
            this.xrTableCell医嘱内容,
            this.xrTableCell剂量数量,
            this.xrTableCell组别符号,
            this.xrTableCell用法,
            this.xrTableCell频次,
            this.xrTableCell医师,
            this.xrTableCell执行护士,
            this.xrTableCell执行时间});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell开嘱日期
            // 
            this.xrTableCell开嘱日期.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell开嘱日期.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印开嘱日期]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)")});
            this.xrTableCell开嘱日期.Name = "xrTableCell开嘱日期";
            this.xrTableCell开嘱日期.StylePriority.UseBorders = false;
            this.xrTableCell开嘱日期.StylePriority.UseTextAlignment = false;
            this.xrTableCell开嘱日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell开嘱日期.Weight = 0.27661268226798319D;
            // 
            // xrTableCell开嘱时间
            // 
            this.xrTableCell开嘱时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell开嘱时间.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印开嘱时间]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)")});
            this.xrTableCell开嘱时间.Name = "xrTableCell开嘱时间";
            this.xrTableCell开嘱时间.StylePriority.UseBorders = false;
            this.xrTableCell开嘱时间.StylePriority.UseTextAlignment = false;
            this.xrTableCell开嘱时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell开嘱时间.Weight = 0.24102843685161132D;
            // 
            // xrTableCell医嘱内容
            // 
            this.xrTableCell医嘱内容.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell医嘱内容.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Size", "Iif(Len([医嘱内容]) > 16, 8.25, ?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印医嘱内涵]")});
            this.xrTableCell医嘱内容.Name = "xrTableCell医嘱内容";
            this.xrTableCell医嘱内容.StylePriority.UseBorders = false;
            this.xrTableCell医嘱内容.StylePriority.UseTextAlignment = false;
            this.xrTableCell医嘱内容.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell医嘱内容.Weight = 0.84666655367667465D;
            // 
            // xrTableCell剂量数量
            // 
            this.xrTableCell剂量数量.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell剂量数量.BorderWidth = 1F;
            this.xrTableCell剂量数量.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印剂量数量]+[打印剂量单位]")});
            this.xrTableCell剂量数量.Name = "xrTableCell剂量数量";
            this.xrTableCell剂量数量.StylePriority.UseBorders = false;
            this.xrTableCell剂量数量.StylePriority.UseBorderWidth = false;
            this.xrTableCell剂量数量.StylePriority.UseTextAlignment = false;
            this.xrTableCell剂量数量.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell剂量数量.Weight = 0.25233475171408537D;
            this.xrTableCell剂量数量.WordWrap = false;
            // 
            // xrTableCell组别符号
            // 
            this.xrTableCell组别符号.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell组别符号.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印组别符号]")});
            this.xrTableCell组别符号.Name = "xrTableCell组别符号";
            this.xrTableCell组别符号.StylePriority.UseBorders = false;
            this.xrTableCell组别符号.StylePriority.UseTextAlignment = false;
            this.xrTableCell组别符号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell组别符号.Weight = 0.080881127956486665D;
            this.xrTableCell组别符号.WordWrap = false;
            // 
            // xrTableCell用法
            // 
            this.xrTableCell用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell用法.BorderWidth = 1F;
            this.xrTableCell用法.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印用法]")});
            this.xrTableCell用法.Name = "xrTableCell用法";
            this.xrTableCell用法.StylePriority.UseBorders = false;
            this.xrTableCell用法.StylePriority.UseBorderWidth = false;
            this.xrTableCell用法.StylePriority.UseTextAlignment = false;
            this.xrTableCell用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell用法.Weight = 0.15348594460882342D;
            this.xrTableCell用法.WordWrap = false;
            this.xrTableCell用法.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell用法_BeforePrint);
            // 
            // xrTableCell频次
            // 
            this.xrTableCell频次.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell频次.BorderWidth = 1F;
            this.xrTableCell频次.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印频次]")});
            this.xrTableCell频次.Name = "xrTableCell频次";
            this.xrTableCell频次.StylePriority.UseBorders = false;
            this.xrTableCell频次.StylePriority.UseBorderWidth = false;
            this.xrTableCell频次.StylePriority.UseTextAlignment = false;
            this.xrTableCell频次.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell频次.Weight = 0.15614233727069146D;
            this.xrTableCell频次.WordWrap = false;
            this.xrTableCell频次.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell用法_BeforePrint);
            // 
            // xrTableCell医师
            // 
            this.xrTableCell医师.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell医师.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox开嘱医师手签});
            this.xrTableCell医师.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([开小于续] = \'是\' And [Parameters.para续打] = True And [已打印标志] = 1, \'White\', ?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Borders", "Iif([Parameters.para续打] = True And [已打印标志] = 1, \'None\', [开小于续] = \'是\' And [Paramet" +
                    "ers.para续打] = True And [已打印标志] = 1, \'None\', ?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Visible", "Iif([开小于续] = \'是\' And [Parameters.para续打] = True And [已打印标志] = 1, True, ?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印开嘱医生点点]")});
            this.xrTableCell医师.Name = "xrTableCell医师";
            this.xrTableCell医师.StylePriority.UseBorders = false;
            this.xrTableCell医师.StylePriority.UseTextAlignment = false;
            this.xrTableCell医师.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell医师.Weight = 0.24863139453482216D;
            this.xrTableCell医师.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell医师_BeforePrint);
            // 
            // xrPictureBox开嘱医师手签
            // 
            this.xrPictureBox开嘱医师手签.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox开嘱医师手签.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Image", "[打印医生手签]")});
            this.xrPictureBox开嘱医师手签.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox开嘱医师手签.Name = "xrPictureBox开嘱医师手签";
            this.xrPictureBox开嘱医师手签.SizeF = new System.Drawing.SizeF(67.54161F, 36F);
            this.xrPictureBox开嘱医师手签.StylePriority.UseBorders = false;
            // 
            // xrTableCell执行护士
            // 
            this.xrTableCell执行护士.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell执行护士.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox开嘱护士手签});
            this.xrTableCell执行护士.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([开小于续] = \'是\' And [Parameters.para续打] = True And [已打印标志] = 1, \'White\', ?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Borders", "Iif([Parameters.para续打] = True And [已打印标志] = 1, \'None\', [开小于续] = \'是\' And [Paramet" +
                    "ers.para续打] = True And [已打印标志] = 1, \'None\', ?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Visible", "Iif([开小于续] = \'是\' And [Parameters.para续打] = True And [已打印标志] = 1, True, ?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印开嘱护士点点]")});
            this.xrTableCell执行护士.ForeColor = System.Drawing.Color.Black;
            this.xrTableCell执行护士.Name = "xrTableCell执行护士";
            this.xrTableCell执行护士.StylePriority.UseBorders = false;
            this.xrTableCell执行护士.StylePriority.UseForeColor = false;
            this.xrTableCell执行护士.StylePriority.UseTextAlignment = false;
            this.xrTableCell执行护士.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell执行护士.Weight = 0.26233014838435914D;
            this.xrTableCell执行护士.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell执行护士_BeforePrint);
            // 
            // xrPictureBox开嘱护士手签
            // 
            this.xrPictureBox开嘱护士手签.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox开嘱护士手签.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Image", "[打印护士手签]")});
            this.xrPictureBox开嘱护士手签.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox开嘱护士手签.Name = "xrPictureBox开嘱护士手签";
            this.xrPictureBox开嘱护士手签.SizeF = new System.Drawing.SizeF(71.26292F, 36F);
            this.xrPictureBox开嘱护士手签.StylePriority.UseBorders = false;
            // 
            // xrTableCell执行时间
            // 
            this.xrTableCell执行时间.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell执行时间.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Borders", "Iif([Arguments.PageIndex] < [Parameters].[i表格_表头尾_显示起始页],\'None\',?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[打印执行时间]")});
            this.xrTableCell执行时间.Name = "xrTableCell执行时间";
            this.xrTableCell执行时间.StylePriority.UseBorders = false;
            this.xrTableCell执行时间.StylePriority.UseTextAlignment = false;
            this.xrTableCell执行时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell执行时间.TextFormatString = "{0:HH:mm}";
            this.xrTableCell执行时间.Weight = 0.29659200984591017D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 5F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(90.67981F, 105.0833F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(618.75F, 22.99999F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "临   时   医   嘱   单";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel病历号
            // 
            this.xrLabel病历号.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrLabel病历号.LocationFloat = new DevExpress.Utils.PointFloat(641.5129F, 133.3333F);
            this.xrLabel病历号.Name = "xrLabel病历号";
            this.xrLabel病历号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel病历号.SizeF = new System.Drawing.SizeF(143.0835F, 22.99998F);
            this.xrLabel病历号.StylePriority.UseTextAlignment = false;
            this.xrLabel病历号.Text = "病历号：";
            this.xrLabel病历号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel床号
            // 
            this.xrLabel床号.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrLabel床号.LocationFloat = new DevExpress.Utils.PointFloat(470.7217F, 133.3333F);
            this.xrLabel床号.Name = "xrLabel床号";
            this.xrLabel床号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel床号.SizeF = new System.Drawing.SizeF(141.6247F, 22.99998F);
            this.xrLabel床号.StylePriority.UseTextAlignment = false;
            this.xrLabel床号.Text = "床号：";
            this.xrLabel床号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel病室
            // 
            this.xrLabel病室.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrLabel病室.LocationFloat = new DevExpress.Utils.PointFloat(307.7632F, 133.3333F);
            this.xrLabel病室.Name = "xrLabel病室";
            this.xrLabel病室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel病室.SizeF = new System.Drawing.SizeF(152.0833F, 22.99998F);
            this.xrLabel病室.StylePriority.UseTextAlignment = false;
            this.xrLabel病室.Text = "病室：";
            this.xrLabel病室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel科别
            // 
            this.xrLabel科别.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrLabel科别.LocationFloat = new DevExpress.Utils.PointFloat(160.5905F, 133.3333F);
            this.xrLabel科别.Name = "xrLabel科别";
            this.xrLabel科别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 0, 100F);
            this.xrLabel科别.SizeF = new System.Drawing.SizeF(135.7143F, 22.99998F);
            this.xrLabel科别.StylePriority.UsePadding = false;
            this.xrLabel科别.StylePriority.UseTextAlignment = false;
            this.xrLabel科别.Text = "科别：";
            this.xrLabel科别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(22.97147F, 131.3334F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(113.7083F, 23.00002F);
            this.xrLabel姓名.StylePriority.UsePadding = false;
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.Text = "姓名：";
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel医院名称
            // 
            this.xrLabel医院名称.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrLabel医院名称.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrLabel医院名称.LocationFloat = new DevExpress.Utils.PointFloat(56.26314F, 77.08327F);
            this.xrLabel医院名称.Name = "xrLabel医院名称";
            this.xrLabel医院名称.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel医院名称.SizeF = new System.Drawing.SizeF(693.75F, 23F);
            this.xrLabel医院名称.StylePriority.UseFont = false;
            this.xrLabel医院名称.StylePriority.UseTextAlignment = false;
            this.xrLabel医院名称.Text = "XX医院";
            this.xrLabel医院名称.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(20F, 161.3333F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(764.6248F, 40F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell11,
            this.xrTableCell15,
            this.xrTableCell3,
            this.xrTableCell16});
            this.xrTableRow2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "日期";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.33595909766752924D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "时间";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.29274034002676463D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "医嘱内容";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 1.8090806198367968D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "医师签名";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.30197449563698925D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "执行者";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.31861226498281187D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "执行时间";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.36022483609650069D;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 17.79165F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrPageInfo1});
            this.PageFooter.HeightF = 43.00001F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel1
            // 
            this.xrLabel1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(366.2084F, 11.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(58.33331F, 22.99998F);
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "第     页";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("PrintOnPage", "Visible", "Iif([Arguments.PageIndex] >= [Parameters].[i表格_表头尾_显示起始页],True ,False )")});
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(383.7083F, 10F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(20.83337F, 23F);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // b是否续打
            // 
            this.b是否续打.Description = "是否续打:";
            this.b是否续打.Name = "b是否续打";
            this.b是否续打.Type = typeof(bool);
            this.b是否续打.ValueInfo = "False";
            this.b是否续打.Visible = false;
            // 
            // s续打时间点
            // 
            this.s续打时间点.Description = "续打时间点";
            this.s续打时间点.Name = "s续打时间点";
            this.s续打时间点.Visible = false;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel医院名称,
            this.xrTable2,
            this.xrLabel姓名,
            this.xrLabel科别,
            this.xrLabel病室,
            this.xrLabel床号,
            this.xrLabel病历号,
            this.xrLabel8});
            this.PageHeader.HeightF = 201.3333F;
            this.PageHeader.Name = "PageHeader";
            // 
            // StyleNoVisible
            // 
            this.StyleNoVisible.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.StyleNoVisible.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.StyleNoVisible.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.StyleNoVisible.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.StyleNoVisible.Name = "StyleNoVisible";
            // 
            // StyleVisible
            // 
            this.StyleVisible.Name = "StyleVisible";
            // 
            // i表格_表头尾_显示起始页
            // 
            this.i表格_表头尾_显示起始页.Name = "i表格_表头尾_显示起始页";
            this.i表格_表头尾_显示起始页.Type = typeof(short);
            this.i表格_表头尾_显示起始页.ValueInfo = "0";
            this.i表格_表头尾_显示起始页.Visible = false;
            // 
            // XtraReport临时医嘱
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.PageHeader});
            this.Margins = new System.Drawing.Printing.Margins(6, 5, 5, 18);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.b是否续打,
            this.s续打时间点,
            this.i表格_表头尾_显示起始页});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.StyleNoVisible,
            this.StyleVisible});
            this.Version = "18.1";
            this.AfterPrint += new System.EventHandler(this.XtraReport临时医嘱_AfterPrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable医嘱内容)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel病历号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel床号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel病室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel科别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTable xrTable医嘱内容;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell开嘱日期;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell医嘱内容;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医院名称;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell执行护士;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell开嘱时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell剂量数量;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell组别符号;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell用法;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell频次;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell执行时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        public DevExpress.XtraReports.Parameters.Parameter s续打时间点;
        public DevExpress.XtraReports.Parameters.Parameter b是否续打;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRControlStyle StyleNoVisible;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell医师;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox开嘱医师手签;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox开嘱护士手签;
        private DevExpress.XtraReports.UI.XRControlStyle StyleVisible;
        private DevExpress.XtraReports.Parameters.Parameter i表格_表头尾_显示起始页;
    }
}
