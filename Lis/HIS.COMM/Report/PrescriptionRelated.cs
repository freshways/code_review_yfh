﻿using HIS.COMM.Helper;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace HIS.COMM.Report
{
    public class PrescriptionRelated
    {
        static CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));

        /// <summary>
        /// 打印MZID下面的全部处方
        /// </summary>
        /// <returns></returns>
        public static bool PrintOutpatientPrescriptionByMZID(decimal mzid, bool isPreview, PrintParams printParams)
        {
            var westernMedicinesPrescription = chis.MF处方摘要.Where(c => c.MZID == mzid && c.作废标记 == false).ToList();
            var chineseMedicinesPrescription = chis.MF中药方_处方摘要.Where(c => c.MZID == mzid && c.作废标记 == false).ToList();
            if (chineseMedicinesPrescription.Count > 0)
            {
                foreach (var item in chineseMedicinesPrescription)
                {
                    HIS.COMM.Report.PrescriptionRelated.PrintOutpatientChinesePrescription(true,
                  chis.MF门诊摘要.Where(c => c.MZID == mzid).FirstOrDefault(),
                  chis.MF中药方_处方摘要.Where(c => c.CFID == item.CFID).FirstOrDefault(),
                  chis.MF中药方_处方明细.Where(c => c.CFID == item.CFID).ToList(),
                  printParams);
                }
            }
            if (westernMedicinesPrescription.Count > 0)
            {
                foreach (var item in westernMedicinesPrescription)
                {
                    HIS.COMM.Report.PrescriptionRelated.PrintOutpatientWestPrescription(true, chis.MF门诊摘要.Where(c => c.MZID == mzid).FirstOrDefault(),
                      chis.MF处方摘要.Where(c => c.CFID == item.CFID).FirstOrDefault(),
                      chis.MF处方明细.Where(c => c.CFID == item.CFID).ToList(), printParams);
                }
            }
            return true;
        }

        /// <summary>
        /// 打印处方号下面的处方内容
        /// </summary>
        /// <param name="prescriptionID"></param>
        /// <param name="isPreview"></param>
        /// <param name="printParams"></param>
        /// <returns></returns>
        public static bool PrintOutpatientPrescriptionByCFID(decimal prescriptionID, bool isPreview, PrintParams printParams)
        {
            var westernMedicinesPrescription = chis.MF处方摘要.Where(c => c.CFID == prescriptionID && c.作废标记 == false).FirstOrDefault();
            var chineseMedicinesPrescription = chis.MF中药方_处方摘要.Where(c => c.CFID == prescriptionID && c.作废标记 == false).FirstOrDefault();
            if (chineseMedicinesPrescription != null)
            {
                var mf中药_处方摘要 = chis.MF中药方_处方摘要.Where(c => c.CFID == prescriptionID).FirstOrDefault();
                HIS.COMM.Report.PrescriptionRelated.PrintOutpatientChinesePrescription(true,
                    chis.MF门诊摘要.Where(c => c.MZID == mf中药_处方摘要.MZID).FirstOrDefault(),
                    chis.MF中药方_处方摘要.Where(c => c.CFID == prescriptionID).FirstOrDefault(),
                    chis.MF中药方_处方明细.Where(c => c.CFID == prescriptionID).ToList(),
                    printParams);
            }
            else
            {
                var mf处方摘要 = chis.MF处方摘要.Where(c => c.CFID == prescriptionID).FirstOrDefault();
                HIS.COMM.Report.PrescriptionRelated.PrintOutpatientWestPrescription(true,
                    chis.MF门诊摘要.Where(c => c.MZID == mf处方摘要.MZID).FirstOrDefault(),
                    chis.MF处方摘要.Where(c => c.CFID == prescriptionID).FirstOrDefault(),
                    chis.MF处方明细.Where(c => c.CFID == prescriptionID).ToList(),
                    printParams);
            }
            return true;
        }

        /// <summary>
        /// 打印西药门诊处方
        /// </summary>
        /// <param name="isPreview"></param>
        /// <param name="outpatientDispensing"></param>
        /// <param name="pojoOutpatientDispensingContents"></param>
        /// <param name="printParam"></param>
        /// <returns></returns>
        public static bool PrintOutpatientWestPrescription(bool isPreview,
            PojoOutpatientDispensing outpatientDispensing,
            List<PojoOutpatientDispensingContent> pojoOutpatientDispensingContents, PrintParams printParam)
        {
            try
            {
                if (pojoOutpatientDispensingContents.Count == 0)
                {
                    msgBalloonHelper.ShowInformation("处方内容为空");
                    return false;
                }
                var drugCode = pojoOutpatientDispensingContents.FirstOrDefault().药品序号;
                var drugItem = chis.YK药品信息.Where(c => c.药品序号 == drugCode).FirstOrDefault();

                if (HIS.COMM.zdInfo.Model单位信息.sDwmc.Contains("攀峰"))
                {
                    XtraReport门诊处方分组_PF westernPrescription = new XtraReport门诊处方分组_PF(outpatientDispensing, pojoOutpatientDispensingContents);

                    printParam.paperName = "门诊处方便签";
                    printParam.width = 1040;
                    printParam.height = 1900;
                    printParam.marginTop = 40;
                    printParam.marginLeft = 40;
                    printParam.marginRight = 40;
                    printParam.marginBottom = 40;
                    ReportHelper2.Print(westernPrescription, printParam);
                }
                else
                {
                    XtraReport门诊处方分组 westernPrescription = new XtraReport门诊处方分组(outpatientDispensing, pojoOutpatientDispensingContents);

                    printParam.paperName = "门诊处方便签";
                    printParam.width = 1040;
                    printParam.height = 1900;
                    printParam.marginTop = 40;
                    printParam.marginLeft = 40;
                    printParam.marginRight = 40;
                    printParam.marginBottom = 40;
                    ReportHelper2.Print(westernPrescription, printParam);
                }
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
            return true;
        }

        public static bool PrintOutpatientWestPrescription(bool isPreview,
           MF门诊摘要 mf门诊摘要, MF处方摘要 mf处方明细, List<MF处方明细> list处方明细, PrintParams printParam)
        {
            try
            {
                if (list处方明细.Count == 0)
                {
                    msgBalloonHelper.ShowInformation("处方内容为空");
                    return false;
                }
                //var drugCode = list处方明细.FirstOrDefault().药品序号;
                //var drugItem = chis.YK药品信息.Where(c => c.药品序号 == drugCode).FirstOrDefault();

                if (HIS.COMM.zdInfo.Model单位信息.sDwmc.Contains("攀峰"))
                {
                    XtraReport门诊处方分组_PF westernPrescription = new XtraReport门诊处方分组_PF(mf门诊摘要, mf处方明细, list处方明细);

                    printParam.paperName = "门诊处方便签";
                    printParam.width = 1040;
                    printParam.height = 1900;
                    printParam.marginTop = 40;
                    printParam.marginLeft = 40;
                    printParam.marginRight = 40;
                    printParam.marginBottom = 40;
                    ReportHelper2.Print(westernPrescription, printParam);
                }
                else
                {
                    XtraReport门诊处方分组 westernPrescription = new XtraReport门诊处方分组(mf门诊摘要, mf处方明细, list处方明细);

                    printParam.paperName = "门诊处方便签";
                    printParam.width = 1040;
                    printParam.height = 1900;
                    printParam.marginTop = 40;
                    printParam.marginLeft = 40;
                    printParam.marginRight = 40;
                    printParam.marginBottom = 40;
                    ReportHelper2.Print(westernPrescription, printParam);
                }
            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
            return true;
        }

        /// <summary>
        /// 打印中药门诊处方
        /// </summary>
        /// <param name="isPreview"></param>
        /// <param name="outpatientDispensing"></param>
        /// <param name="pojoOutpatientDispensingContents"></param>
        /// <param name="printParam"></param>
        /// <returns></returns>
        public static bool PrintOutpatientChinesePrescription(bool isPreview, MF门诊摘要 mf门诊摘要, MF中药方_处方摘要 mf中药处方摘要, List<MF中药方_处方明细> pojoOutpatientDispensingContents, PrintParams printParam)
        {
            try
            {
                if (pojoOutpatientDispensingContents.Count == 0)
                {
                    msgBalloonHelper.ShowInformation("处方内容为空");
                    return false;
                }
                var drugCode = pojoOutpatientDispensingContents.FirstOrDefault().药品序号;
                var drugItem = chis.YK药品信息.Where(c => c.药品序号 == drugCode).FirstOrDefault();

                XtraReport门诊处方分组中药 chinesePrescrpition = new XtraReport门诊处方分组中药(mf门诊摘要, mf中药处方摘要, pojoOutpatientDispensingContents);

                printParam.paperName = "中药处方签";
                printParam.width = 1300;
                printParam.height = 1910;
                printParam.marginTop = 40;
                printParam.marginLeft = 40;
                printParam.marginRight = 40;
                printParam.marginBottom = 40;

                ReportHelper2.Print(chinesePrescrpition, printParam);

            }
            catch (Exception ex)
            {
                msgBalloonHelper.ShowInformation(ex.Message);
            }
            return true;
        }
    }
}
