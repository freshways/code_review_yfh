﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM.Report
{
    public class PrintParams
    {
        public bool isPreview { get; set; } = true;
        public string invokeModuleName { get; set; } = "";
        public string paperName { get; set; }
        public string printName { get; set; } = "";
        public bool canPrint { get; set; } = true;

        public bool Landscape { get; set; } = false;
        public int width { get; set; }
        public int height { get; set; }
        public int marginTop { get; set; }
        public int marginBottom { get; set; }
        public int marginLeft { get; set; }
        public int marginRight { get; set; }
    }
}
