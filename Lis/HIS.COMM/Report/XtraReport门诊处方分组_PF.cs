﻿using HIS.COMM.BLL;
using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HIS.COMM.Report
{
    public partial class XtraReport门诊处方分组_PF : DevExpress.XtraReports.UI.XtraReport
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public XtraReport门诊处方分组_PF(PojoOutpatientDispensing outPatientDispensing, List<PojoOutpatientDispensingContent> list)
        {
            InitializeComponent();
            //var mf门诊摘要 = chis.MF门诊摘要.Where(c => c.MZID == outPatientDispensing.MZID).FirstOrDefault();
            xrLabelCfid.Text = "处方号：" + outPatientDispensing.CFID.ToString().Substring(9, 4);
            xrLabel科室.Text = "科室：" + outPatientDispensing.科室名称;
            xrLabel临床诊断.Text = "临床诊断:" + outPatientDispensing.临床诊断;
            //xrLabel发药人.Text = "发药人: ";// +HIS.COMM.zdInfo.ModelUserInfo.用户名;
            xrLabel年龄.Text = "年龄: " + outPatientDispensing.年龄 + outPatientDispensing.年龄单位;
            xrLabel性别.Text = "性别: " + outPatientDispensing.性别;
            xrLabel医生.Text = "医生: " + outPatientDispensing.诊治医生;
            xrLabel日期.Text = "日期:" + Convert.ToDateTime(outPatientDispensing.开方时间).ToShortDateString();
            xrLabelTitle.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc;
            xrPictureBox医生签名.Image = HIS.COMM.Class医生.getYSimage(outPatientDispensing.医生编码.ToString());
            xrLabel处方类型.Text = outPatientDispensing.处方类型;
            xrLabel姓名.Text = "患者姓名:" + outPatientDispensing.病人姓名;
            if (!string.IsNullOrWhiteSpace(outPatientDispensing.延长原因))
            {
                xrLabel当日有效.Text = "1.本处方当日有效";
                xrLabel延长原因.Text = $"2.延长处方用量时间原因:{outPatientDispensing.延长原因}";
            }
            else
            {
                xrLabel当日有效.Text = "本处方当日有效";
                xrLabel延长原因.Text = "";
            }

            this.DataSource = null;
            this.DataMember = null;
            this.DataAdapter = null;
            this.DataSource = list;
        }
        public XtraReport门诊处方分组_PF(MF门诊摘要 mf门诊摘要, MF处方摘要 mf处方摘要, List<MF处方明细> list)
        {
            InitializeComponent();
            //var mf门诊摘要 = chis.MF门诊摘要.Where(c => c.MZID == outPatientDispensing.MZID).FirstOrDefault();
            xrLabelCfid.Text = "处方号：" + mf处方摘要.CFID.ToString().Substring(9, 4);
            xrLabel科室.Text = "科室：" + CacheData.Gy科室设置.Where(c => c.科室编码 == Convert.ToInt32(mf门诊摘要.科室编码)).FirstOrDefault().科室名称; 
            xrLabel临床诊断.Text = "临床诊断:" + mf门诊摘要.临床诊断;
            //xrLabel发药人.Text = "发药人: ";// +HIS.COMM.zdInfo.ModelUserInfo.用户名;
            xrLabel年龄.Text = "年龄: " + mf门诊摘要.年龄 + mf门诊摘要.年龄单位;
            xrLabel性别.Text = "性别: " + mf门诊摘要.性别;
            xrLabel医生.Text = "医生: " + CacheData.Pubuser.Where(c => c.用户编码 == mf门诊摘要.医生编码).FirstOrDefault().用户名;
            xrLabel日期.Text = "日期:" + Convert.ToDateTime(mf处方摘要.开方时间).ToShortDateString();
            xrLabelTitle.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc;
            xrPictureBox医生签名.Image = HIS.COMM.Class医生.getYSimage(mf门诊摘要.医生编码.ToString());
            xrLabel处方类型.Text = mf处方摘要.处方类型;
            xrLabel姓名.Text = "患者姓名:" + mf门诊摘要.病人姓名;
            if (!string.IsNullOrWhiteSpace(mf处方摘要.延长原因))
            {
                xrLabel当日有效.Text = "1.本处方当日有效";
                xrLabel延长原因.Text = $"2.延长处方用量时间原因:{mf处方摘要.延长原因}";
            }
            else
            {
                xrLabel当日有效.Text = "本处方当日有效";
                xrLabel延长原因.Text = "";
            }

            this.DataSource = null;
            this.DataMember = null;
            this.DataAdapter = null;
            this.DataSource = list;
        }
    }
}
