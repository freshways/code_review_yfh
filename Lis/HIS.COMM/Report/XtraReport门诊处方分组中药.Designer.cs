﻿namespace HIS.COMM.Report
{
    partial class XtraReport门诊处方分组中药
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraReport门诊处方分组中药));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel临床诊断 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCfid = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel总金额 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox医生签名 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel付数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel发药 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel处方用法用量 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BorderWidth = 0F;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel3});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 75F;
            this.Detail.MultiColumn.ColumnCount = 3;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseBorderWidth = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[剂量单位]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([财务分类] = \'卫生材料\', \'White\', ?)")});
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(299.9471F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(35.55994F, 58.42F);
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "xrLabel8";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[剂量数量]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([财务分类] = \'卫生材料\', \'White\', ?)")});
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(225.4211F, 0F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(74.52603F, 58.42F);
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "xrLabel7";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrLabel7.TextFormatString = "{0:n0}";
            // 
            // xrLabel3
            // 
            this.xrLabel3.CanGrow = false;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([财务分类] = \'卫生材料\', \'White\', ?)"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[药品名称]")});
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(46.15855F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(179.2625F, 58.42F);
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.WordWrap = false;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(46.15855F, 182.1391F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(955.1456F, 58.42001F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "中 药 处 方 笺";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine4
            // 
            this.xrLine4.Dpi = 254F;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(45.86688F, 436.245F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(960.4376F, 5.291687F);
            // 
            // xrLabel日期
            // 
            this.xrLabel日期.Dpi = 254F;
            this.xrLabel日期.LocationFloat = new DevExpress.Utils.PointFloat(642.0002F, 240.559F);
            this.xrLabel日期.Name = "xrLabel日期";
            this.xrLabel日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel日期.SizeF = new System.Drawing.SizeF(359.3039F, 58.42F);
            this.xrLabel日期.StylePriority.UseTextAlignment = false;
            this.xrLabel日期.Text = "日期:";
            this.xrLabel日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel日期.WordWrap = false;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 254F;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(45.86688F, 366.6064F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(960.4376F, 5.291656F);
            // 
            // xrLabel临床诊断
            // 
            this.xrLabel临床诊断.Dpi = 254F;
            this.xrLabel临床诊断.LocationFloat = new DevExpress.Utils.PointFloat(46.15855F, 377.825F);
            this.xrLabel临床诊断.Name = "xrLabel临床诊断";
            this.xrLabel临床诊断.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel临床诊断.SizeF = new System.Drawing.SizeF(955.1456F, 58.41998F);
            this.xrLabel临床诊断.StylePriority.UseTextAlignment = false;
            this.xrLabel临床诊断.Text = "诊断:";
            this.xrLabel临床诊断.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel临床诊断.WordWrap = false;
            // 
            // xrLabel年龄
            // 
            this.xrLabel年龄.Dpi = 254F;
            this.xrLabel年龄.LocationFloat = new DevExpress.Utils.PointFloat(512.1234F, 308.1866F);
            this.xrLabel年龄.Name = "xrLabel年龄";
            this.xrLabel年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel年龄.SizeF = new System.Drawing.SizeF(232.8333F, 58.41998F);
            this.xrLabel年龄.StylePriority.UseTextAlignment = false;
            this.xrLabel年龄.Text = "年龄";
            this.xrLabel年龄.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.Dpi = 254F;
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(747.3043F, 308.1866F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(253.9999F, 58.42004F);
            this.xrLabel性别.StylePriority.UseTextAlignment = false;
            this.xrLabel性别.Text = "性别";
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.Dpi = 254F;
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(46.15855F, 308.1866F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(465.9648F, 58.41998F);
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.Text = "患者姓名";
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel姓名.WordWrap = false;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(45.86688F, 299.1909F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(963.0834F, 5.291656F);
            // 
            // xrLabel科室
            // 
            this.xrLabel科室.Dpi = 254F;
            this.xrLabel科室.LocationFloat = new DevExpress.Utils.PointFloat(46.15855F, 240.559F);
            this.xrLabel科室.Name = "xrLabel科室";
            this.xrLabel科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel科室.SizeF = new System.Drawing.SizeF(289.3484F, 58.42F);
            this.xrLabel科室.StylePriority.UseTextAlignment = false;
            this.xrLabel科室.Text = "科室:";
            this.xrLabel科室.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabel科室.WordWrap = false;
            // 
            // xrLabelCfid
            // 
            this.xrLabelCfid.Dpi = 254F;
            this.xrLabelCfid.LocationFloat = new DevExpress.Utils.PointFloat(364.6109F, 240.559F);
            this.xrLabelCfid.Name = "xrLabelCfid";
            this.xrLabelCfid.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCfid.SizeF = new System.Drawing.SizeF(277.3893F, 58.42F);
            this.xrLabelCfid.StylePriority.UseTextAlignment = false;
            this.xrLabelCfid.Text = "门诊号:";
            this.xrLabelCfid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrLabelCfid.WordWrap = false;
            // 
            // xrLabelTitle
            // 
            this.xrLabelTitle.Dpi = 254F;
            this.xrLabelTitle.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabelTitle.LocationFloat = new DevExpress.Utils.PointFloat(46.15855F, 122.9783F);
            this.xrLabelTitle.Name = "xrLabelTitle";
            this.xrLabelTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelTitle.SizeF = new System.Drawing.SizeF(955.1456F, 58.42F);
            this.xrLabelTitle.StylePriority.UseFont = false;
            this.xrLabelTitle.StylePriority.UseTextAlignment = false;
            this.xrLabelTitle.Text = "处方便签";
            this.xrLabelTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 25F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel医生
            // 
            this.xrLabel医生.Dpi = 254F;
            this.xrLabel医生.LocationFloat = new DevExpress.Utils.PointFloat(73.99268F, 87.78F);
            this.xrLabel医生.Name = "xrLabel医生";
            this.xrLabel医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel医生.SizeF = new System.Drawing.SizeF(117.1575F, 58.42F);
            this.xrLabel医生.Text = "医生:";
            this.xrLabel医生.WordWrap = false;
            // 
            // xrLabel总金额
            // 
            this.xrLabel总金额.Dpi = 254F;
            this.xrLabel总金额.LocationFloat = new DevExpress.Utils.PointFloat(719.3643F, 87.78F);
            this.xrLabel总金额.Name = "xrLabel总金额";
            this.xrLabel总金额.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel总金额.SizeF = new System.Drawing.SizeF(240.7708F, 58.42F);
            this.xrLabel总金额.Text = "总金额";
            this.xrLabel总金额.WordWrap = false;
            // 
            // xrPictureBox医生签名
            // 
            this.xrPictureBox医生签名.Dpi = 254F;
            this.xrPictureBox医生签名.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox医生签名.Image")));
            this.xrPictureBox医生签名.LocationFloat = new DevExpress.Utils.PointFloat(191.1503F, 80.16F);
            this.xrPictureBox医生签名.Name = "xrPictureBox医生签名";
            this.xrPictureBox医生签名.SizeF = new System.Drawing.SizeF(203.2F, 76.2F);
            this.xrPictureBox医生签名.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 254F;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(48.5128F, 65.02589F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(952.7913F, 5.291664F);
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel付数,
            this.xrLabel发药,
            this.xrLabel16,
            this.xrLabel1,
            this.xrLabel2,
            this.xrLabel处方用法用量,
            this.xrLabel医生,
            this.xrPictureBox医生签名,
            this.xrLabel总金额,
            this.xrLine3});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 237.0833F;
            this.GroupFooter1.Level = 1;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PrintAtBottom = true;
            this.GroupFooter1.RepeatEveryPage = true;
            // 
            // xrLabel付数
            // 
            this.xrLabel付数.Dpi = 254F;
            this.xrLabel付数.LocationFloat = new DevExpress.Utils.PointFloat(37.58608F, 6.605938F);
            this.xrLabel付数.Name = "xrLabel付数";
            this.xrLabel付数.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel付数.SizeF = new System.Drawing.SizeF(187.8351F, 58.42F);
            this.xrLabel付数.StylePriority.UseTextAlignment = false;
            this.xrLabel付数.Text = "xrLabel付数";
            this.xrLabel付数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel发药
            // 
            this.xrLabel发药.Dpi = 254F;
            this.xrLabel发药.LocationFloat = new DevExpress.Utils.PointFloat(719.3643F, 174.1929F);
            this.xrLabel发药.Name = "xrLabel发药";
            this.xrLabel发药.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel发药.SizeF = new System.Drawing.SizeF(241.5986F, 58.41998F);
            this.xrLabel发药.Text = "发药：";
            this.xrLabel发药.WordWrap = false;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(441.0854F, 174.1929F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(240.391F, 58.41998F);
            this.xrLabel16.Text = "核对：";
            this.xrLabel16.WordWrap = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(73.99268F, 174.1929F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(326.6019F, 58.41998F);
            this.xrLabel1.Text = "调配：";
            this.xrLabel1.WordWrap = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(441.0854F, 87.78F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(240.391F, 58.42F);
            this.xrLabel2.Text = "审核:";
            this.xrLabel2.WordWrap = false;
            // 
            // xrLabel处方用法用量
            // 
            this.xrLabel处方用法用量.Dpi = 254F;
            this.xrLabel处方用法用量.LocationFloat = new DevExpress.Utils.PointFloat(247.9827F, 6.605938F);
            this.xrLabel处方用法用量.Name = "xrLabel处方用法用量";
            this.xrLabel处方用法用量.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel处方用法用量.SizeF = new System.Drawing.SizeF(702.204F, 58.42F);
            this.xrLabel处方用法用量.StylePriority.UseTextAlignment = false;
            this.xrLabel处方用法用量.Text = "xrLabel处方用法用量";
            this.xrLabel处方用法用量.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelTitle,
            this.xrLine4,
            this.xrLabel日期,
            this.xrLine2,
            this.xrLabel临床诊断,
            this.xrLabel年龄,
            this.xrLabel性别,
            this.xrLabel姓名,
            this.xrLine1,
            this.xrLabel科室,
            this.xrLabelCfid,
            this.xrLabel13});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("组别", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.HeightF = 465F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5});
            this.GroupFooter2.Dpi = 254F;
            this.GroupFooter2.HeightF = 44F;
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.RepeatEveryPage = true;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(46.15852F, 0F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(941.9166F, 33.01995F);
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "--------------------------以 下 空 白--------------------------";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XtraReport门诊处方分组中药
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.ReportFooter,
            this.GroupFooter1,
            this.GroupHeader1,
            this.GroupFooter2});
            this.DataMember = "ys门诊处方";
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(85, 61, 0, 25);
            this.PageHeight = 1910;
            this.PageWidth = 1300;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 25F;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "18.1";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel临床诊断;
        private DevExpress.XtraReports.UI.XRLabel xrLabel年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCfid;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel总金额;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医生;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel日期;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox医生签名;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel处方用法用量;
        private DevExpress.XtraReports.UI.XRLabel xrLabel发药;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel付数;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
    }
}