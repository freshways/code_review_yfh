﻿using HIS.COMM.BLL;
using HIS.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace HIS.COMM.Report
{
    public partial class XtraReport门诊处方分组中药 : DevExpress.XtraReports.UI.XtraReport
    {
        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public XtraReport门诊处方分组中药(MF门诊摘要 mf门诊摘要, MF中药方_处方摘要 mf中药处方摘要, List<MF中药方_处方明细> list处方内容)
        {
            InitializeComponent();


            xrLabelCfid.Text = "处方号：" + mf中药处方摘要.CFID.ToString().Substring(9, 4);
            xrLabel科室.Text = "科室：" +  CacheData.Gy科室设置.Where(c => c.科室编码 == Convert.ToInt32(mf门诊摘要.科室编码)).FirstOrDefault().科室名称;
            xrLabel临床诊断.Text = "临床诊断:" + mf门诊摘要.临床诊断;
            xrLabel姓名.Text = "患者姓名:" + mf门诊摘要.病人姓名;
            xrLabel年龄.Text = "年龄: " + mf门诊摘要.年龄 + mf门诊摘要.年龄单位;
            xrLabel性别.Text = "性别: " + mf门诊摘要.性别;
            xrLabel医生.Text = "医生: " + CacheData.Pubuser.Where(c => c.用户编码 == mf门诊摘要.医生编码).FirstOrDefault().用户名;
            xrLabel日期.Text = "日期:" + Convert.ToDateTime(mf中药处方摘要.开方时间).ToShortDateString();
            xrLabelTitle.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc;
            xrPictureBox医生签名.Image = HIS.COMM.Class医生.getYSimage(mf中药处方摘要.医生编码.ToString());
            xrLabel付数.Text = mf中药处方摘要.处方贴数.ToString() + "付";
            xrLabel处方用法用量.Text = mf中药处方摘要.用法用量;
            xrLabel总金额.Text = "总金额: " + Convert.ToDecimal(list处方内容.Sum(c => c.零售价金额)).ToString("#0.00");

            this.DataSource = null;
            this.DataMember = null;
            this.DataAdapter = null;

            this.DataSource = list处方内容;
        }
    }
}
