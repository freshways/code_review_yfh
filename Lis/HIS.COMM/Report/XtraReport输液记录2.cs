﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;

namespace HIS.COMM.Report
{
    public partial class XtraReport输液记录2 : DevExpress.XtraReports.UI.XtraReport
    {
        List<HIS.Model.Pojo.Pojo输液记录单> pojos = new List<HIS.Model.Pojo.Pojo输液记录单>();
        public XtraReport输液记录2(List<HIS.Model.Pojo.Pojo输液记录单> pojos)
        {
            InitializeComponent();
            this.DataSource = null;
            this.DataMember = null;
            this.DataAdapter = null;

            this.pojos = pojos;
            this.DataSource = pojos;
            xrLabel日期.Text = "打印:" + DateTime.Now;
        }

    }
}
