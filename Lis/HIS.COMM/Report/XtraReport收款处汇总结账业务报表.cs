﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace HIS.COMM
{
    public partial class XtraReport收款处结账业务报表 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport收款处结账业务报表(string xjzy, string hzsj, string bbr, Boolean sfcy)
        {
            InitializeComponent();
            this.xrLabelHzsj.Text = "汇总时间:" + hzsj;
            this.xrLabelDysj.Text = "打印时间:" + DateTime.Now.ToString();
            this.textTitle.Text = sfcy ? "收款处汇总结账业务报表(重印)" : "收款处汇总结账业务报表";
            this.xrLabelBbr.Text = "报表人：" + bbr;
            this.xrLabel文字摘要.Text = xjzy;
        }
    }
}
