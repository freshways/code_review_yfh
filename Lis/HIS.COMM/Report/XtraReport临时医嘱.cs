﻿using HIS.Model;
using HIS.Model.Pojo;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WEISHENG.COMM;

namespace HIS.COMM
{
    public partial class XtraReport临时医嘱 : DevExpress.XtraReports.UI.XtraReport
    {
        Pojo病人信息 pojo病人信息;

        CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(HIS.COMM.DBConnHelper.SConnHISDb));
        public XtraReport临时医嘱(Pojo病人信息 person, bool _b是否续打, DateTime _dateTime界面续打时间)
        {
            InitializeComponent(); 
            CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(COMM.DBConnHelper.SConnHISDb));
            var item床位 = chis.ZY床位设置.Where(c => c.床位名称 == person.病床 && c.病区 == person.病区).FirstOrDefault();
            if (item床位 != null)
            {
                xrLabel病室.Text = "病室：" + item床位.房间号;
            }
            b是否续打.Value = _b是否续打;
            pojo病人信息 = person;
            xrLabel医院名称.Text = HIS.COMM.zdInfo.Model单位信息.sDwmc;
            xrLabel姓名.Text = "姓名:" + person.病人姓名;
            xrLabel科别.Text = "科别:" + person.科室名称;
            xrLabel床号.Text = "床号:" + person.病床;
            xrLabel病历号.Text = "病历号:" + person.住院号码;

            //续打状态下处理是否打印边框，从第几页打印到第几页，循环遍历，如果产生新的一页，则新的一页总是打印边框；包括执行时间等要续打 在续打时间点之前的行
            //1、打印数据内容控制(包括..和空白显示内容控制)
            int out表格_表头尾_显示起始页 = 0;//表格表头表尾是否打印控制标记
            this.DataSource = BLL.BLL医嘱打印内容.get医嘱打印内容(person.ZYID, _b是否续打, _dateTime界面续打时间, out out表格_表头尾_显示起始页, "临时医嘱");

            //2、边框打印控制
            if (!_b是否续打)
            {
                i表格_表头尾_显示起始页.Value = 0;
            }
            else
            {
                i表格_表头尾_显示起始页.Value = out表格_表头尾_显示起始页;
            }

            //3、日期和时间显示格式控制定义在format里面

            var printSignParam = HIS.COMM.Helper.EnvironmentHelper.MedicalOrderSignaturePrintParam();
            if (printSignParam.Contains("开嘱医生"))
            {
                xrPictureBox开嘱医师手签.Visible = true;
            }
            else
            {
                xrPictureBox开嘱医师手签.Visible = false;
            }
            if (printSignParam.Contains("开嘱执行"))
            {
                xrPictureBox开嘱护士手签.Visible = true;
            }
            else
            {
                xrPictureBox开嘱护士手签.Visible = false;
            }


        }


        string s开嘱日期;
        string s开嘱时间;
        string s执行时间;
        string s用法;
        string s组别;
        bool b横线 = true;
        string s组别符号 = "";
        string S_医嘱内容 = string.Empty;

        private void xrTableCell用法_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTableCell cell = (XRTableCell)sender;
            //if (s组别符号 == "┘" || s组别符号 == "]" || s组别符号 == "")
            //{
            //    cell.ForeColor = Color.Black;
            //}
            //else
            //{
            //    cell.ForeColor = Color.White;
            //}
        }


        private void XtraReport临时医嘱_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                ZY病人信息 inpatient = chis.ZY病人信息.Where(c => c.ZYID == pojo病人信息.ZYID).FirstOrDefault();
                inpatient.临时医嘱续打时间点 = DateTime.Now;
                using (CHISEntities chis = new CHISEntities(EF6Helper.GetDbEf6Conn(DBConnHelper.SConnHISDb)))
                {                    
                    chis.ZY病人信息.Attach(inpatient);
                    chis.Entry(inpatient).State = System.Data.Entity.EntityState.Modified;
                    chis.Entry(inpatient).Property(c => c.临时医嘱续打时间点).IsModified = true;
                    chis.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void xrTable医嘱内容_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //XRTable table = (XRTable)sender;
            ////s组别 = table.Rows[0].Cells[4].Text;//
            //s组别符号 = table.Rows[0].Cells[4].Text;//组别符号
            //S_医嘱内容 = table.Rows[0].Cells[2].Text;//医嘱内容

        }

        private void xrTableCell执行护士_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (xrTableCell开嘱时间.Text == "..")
            //{
            //    xrTableCell执行护士.Text = "..";
            //}
            //else
            //{
            //    xrTableCell执行护士.Text = "";
            //}
        }

        private void xrTableCell医师_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (xrTableCell开嘱时间.Text == "..")
            //{
            //    xrTableCell医师.Text = "..";
            //}
            //else
            //{
            //    xrTableCell医师.Text = "";
            //}
        }
    }
}
