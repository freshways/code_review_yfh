﻿namespace HIS.COMM
{
    public class drug
    {
        private string _s住院记费名称;

        public string S住院记费名称
        {
            get 
            {
                _s住院记费名称 = S药品名称;
                if (S药房规格 != "")
                {
                    _s住院记费名称 = _s住院记费名称 + "/" + S药房规格;
                }
                if (S药房单位 != "")
                {
                    _s住院记费名称 = _s住院记费名称 + "/" + S药房单位;
                }
                //if (S药品产地 != "")
                //{
                //    _s住院记费名称 = _s住院记费名称 + "/" + S药品产地;
                //}
                return _s住院记费名称;
            }
            set { _s住院记费名称 = value; }
        }
        private string _s药品序号;

        public string S药品序号
        {
            get { return _s药品序号; }
            set { _s药品序号 = value; }
        }
        private decimal _dec库存数量;

        public decimal dec库存数量
        {
            get { return _dec库存数量; }
            set { _dec库存数量 = value; }
        }
        private string s药品批号;

        public string S药品批号
        {
            get { return s药品批号; }
            set { s药品批号 = value; }
        }
        private string s药品名称;

        public string S药品名称
        {
            get { return s药品名称; }
            set { s药品名称 = value; }
        }
        private string s药品产地;

        public string S药品产地
        {
            get { return s药品产地; }
            set { s药品产地 = value; }
        }
        private string s药房规格;

        public string S药房规格
        {
            get { return s药房规格; }
            set { s药房规格 = value; }
        }

        private string s药房单位;

        public string S药房单位
        {
            get { return s药房单位; }
            set { s药房单位 = value; }
        }

        private string _sypid = "0";

        public string Sypid
        {
            get { return _sypid; }
            set { _sypid = value; }
        }
        private decimal _dec医院零售价;

        public decimal Dec医院零售价
        {
            get { return _dec医院零售价; }
            set { _dec医院零售价 = value; }
        }

        private decimal _dec进价;

        public decimal Dec进价
        {
            get { return _dec进价; }
            set { _dec进价 = value; }
        }
    }
}
