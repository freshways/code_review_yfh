﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace HIS.COMM
{
    public class ClassUpgrade
    {
        #region 基础类
        enum en升级类型
        {
            升级表结构,
            更新本地服务器升级文件
        }

        private static string connUpgradeLogServer = "Data Source=192.168.10.101,1433;Initial Catalog=upgradeDB;Persist Security Info=True;User ID=upgradeUser;Password=upgradeUser@123;Application Name=allInOne;Connect Timeout=60";

        private static bool b是否需要升级(string _s升级要素标志)
        {
            try
            {
                string SQL =
                   "SELECT count(id) " + "\r\n" +
                   "FROM upgradeDB.dbo.[pub_升级日志]" + "\r\n" +
                   "where [单位编码]='" + HIS.COMM.zdInfo.Model科室信息.单位编码 + "' and [升级成功标志]=1 and 升级要素标志='" + _s升级要素标志 + "'";
                if (HIS.Model.Dal.SqlHelper.ExecuteDataset(connUpgradeLogServer, CommandType.Text, SQL).Tables[0].Rows[0][0].ToString() == "0")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        private static void v更新升级日志(string _s升级要素标志, en升级类型 _en升级类型, string _s升级情况, bool _b升级成功标志)
        {
            try
            {
                string SQL =
                   "INSERT INTO upgradeDB.dbo.[pub_升级日志]" + "\r\n" +
                   "([升级要素标志], [升级类型], [升级情况], [单位编码],升级成功标志) " + "\r\n" +
                   "VALUES ('" + _s升级要素标志 + "', '" + _en升级类型.ToString() + "', '" + _s升级情况 + "', '" +
                   HIS.COMM.zdInfo.Model科室信息.单位编码 + "'," + Convert.ToInt16(_b升级成功标志) + ")";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(connUpgradeLogServer, CommandType.Text, SQL);
            }
            catch (Exception ex)
            {
                WEISHENG.COMM.LogHelper.Info("log\\err", "【" + _s升级要素标志 + "|" + _en升级类型.ToString() + "|" + _s升级情况 + "|" + _b升级成功标志 + "】" + ex.Message);
                
            }
        }
        #endregion

//        update aa
//set aa.科室编码=bb.科室编码
//from pubuser aa left outer join GY科室设置 bb on aa.科室名称= bb.科室名称
        public static bool b德康导入新ICD疾病()
        {
            try
            {
                if (HIS.COMM.zdInfo.Model单位信息.sDwmc.Contains("德康") == false)
                {
                    return false;
                }
                string _s升级要素标志 = "20160713修改icd10表增加备注";
                if (b是否需要升级(_s升级要素标志) == false)
                {
                    return false;
                }
                string SQL修改表结构 = "alter table emr_pub_icd10 add  备注 varchar(100) " + "\r\n" +
                    "alter table emr_pub_icd10 drop column id" + "\r\n" +
                    "alter table emr_pub_icd10 add id int IDENTITY(1,1) not null" + "\r\n" +
                    "ALTER TABLE dbo.emr_pub_icd10 ADD CONSTRAINT" + "\r\n" +
                    "	PK_emr_pub_icd10 PRIMARY KEY CLUSTERED " + "\r\n" +
                    "	(" + "\r\n" +
                    "	ID" + "\r\n" +
                    "	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]";

                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL修改表结构);
                v更新升级日志(_s升级要素标志, en升级类型.升级表结构, SQL修改表结构, true);
                string SQL新ICD =
                    "SELECT  " + "\r\n" +
                    "  [ICD10编码] ICD10," + "\r\n" +
                    "  [疾病名称] ICD10name," + "\r\n" +
                    "  '' ICD10HeadPY," + "\r\n" +
                    "  56 KindID," + "\r\n" +
                    "  'D' bigkindcode," + "\r\n" +
                    "  '' ICD10headpyall," + "\r\n" +
                    "  '' commonname," + "\r\n" +
                    "  '' commonnameheadpy," + "\r\n" +
                    "  '' commonnameheadpyall,'德康精神病提供' 备注" + "\r\n" +
                    "FROM" + "\r\n" +
                    "  [emr_pub_补充] aa " + "\r\n" +
                    "where icd10编码 not in (SELECT [ICD10] FROM [emr_pub_icd10]) ";
                DataTable dt = HIS.Model.Dal.SqlHelper.ExecuteDataset(connUpgradeLogServer, CommandType.Text, SQL新ICD).Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    string SQL写数据 =
                    "INSERT INTO emr_pub_icd10" + "\r\n" +
                    "(ICD10, ICD10Name, ICD10HeadPY, KindID, BigKindCode, ICD10HeadPYAll, CommonName, CommonNameHeadPY, CommonNameHeadPYAll, [备注]) " + "\r\n" +
                    "VALUES ( '" + row["ICD10"] + "', '" + row["ICD10Name"] + "', '" + row["ICD10HeadPY"] + "', " +
                    row["KindID"] + ", '" + row["BigKindCode"] + "', '" +
                    row["ICD10HeadPYAll"] + "', '" + row["CommonName"] + "', '" + row["CommonNameHeadPY"] + "', '" +
                    row["CommonNameHeadPYAll"] + "', '" + row["备注"] + "')";

                    HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL写数据);
                }
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, 
                    "update EMR_PUB_ICD10 set ICD10HeadPY=substring(LOWER( dbo.fun_getpym(ICD10Name)),0,20) where ICD10HeadPY=''");                
                v更新升级日志("插入精神疾病ICD", en升级类型.升级表结构, "", true);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool bPACSLIS接口改造()
        {
            try
            {
                string SQL_pacs执行科室建表 =
                "CREATE TABLE [dbo].[PACS执行科室](" + "\r\n" +
                "	[分院编码] [int] NOT NULL," + "\r\n" +
                "	[收费编码] [int] NOT NULL," + "\r\n" +
                "	[收费名称] [varchar](60) NOT NULL," + "\r\n" +
                "	[执行科室编码] [int] NULL," + "\r\n" +
                "	[执行科室名称] [nvarchar](20) NULL," + "\r\n" +
                "	[ID] [int] IDENTITY(1,1) NOT NULL," + "\r\n" +
                "	[createTime] [datetime] NULL," + "\r\n" +
                " CONSTRAINT [PK_PACS执行科室] PRIMARY KEY CLUSTERED " + "\r\n" +
                "(" + "\r\n" +
                "	[ID] ASC" + "\r\n" +
                ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
                ") ON [PRIMARY]";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_pacs执行科室建表);
                string SQL默认时间 =
    "ALTER TABLE [dbo].[PACS执行科室] ADD  CONSTRAINT [DF_PACS执行科室_createTime]  DEFAULT (getdate()) FOR [createTime]";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL默认时间);
                string SQL数据迁移 =
    "insert into PACS执行科室 (分院编码,收费编码,收费名称,执行科室编码,执行科室名称)" + "\r\n" +
    "select 1 分院编码,收费编码,收费名称,执行科室编码,bb.[科室名称] 执行科室名称 " + "\r\n" +
    "from [GY收费小项] aa left outer join [GY科室设置] bb on aa.[执行科室编码]=bb.[科室编码]" + "\r\n" +
    "where [执行科室编码] is not null and 归并名称  in ('放射费','CT检查费')";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL数据迁移);

                string SQL科室设置分院编码 = "alter table gy科室设置 add 分院编码 int";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL科室设置分院编码);

                string SQL更新分院编码 = "update [GY科室设置] set 分院编码=1";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL更新分院编码);

                string SQL医技科室 =
    "update GY科室设置  set 科室类型='医技科室'" + "\r\n" +
    "where 科室编码 in" + "\r\n" +
    "(" + "\r\n" +
    "select distinct 执行科室编码 from PACS执行科室 " + "\r\n" +
    ")";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL医技科室);

                string SQL检查项 =
    "alter table [GY收费小项] add PACS检查项 bit,LIS检查项 bit";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL检查项);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #region 升级权限框架

        public static bool b升级权限框架()
        {
            string SQL =
        "CREATE TABLE [dbo].[pubQxNew2] (" + "\r\n" +
        "[模块名] nvarchar(30) NOT NULL," + "\r\n" +
        "[键ID] int NOT NULL," + "\r\n" +
        "[父ID] int NOT NULL," + "\r\n" +
        "[ID] int IDENTITY(1, 1) NOT NULL," + "\r\n" +
        "[命名空间] varchar(50) NULL," + "\r\n" +
        "[窗口名] varchar(50) NULL," + "\r\n" +
        "[图标索引] int NULL," + "\r\n" +
        "[显示顺序] int NULL," + "\r\n" +
        "[是否显示] bit NULL," + "\r\n" +
        "[传递参数] varchar(50) NULL)" + "\r\n" +
        "ON [PRIMARY]" + "\r\n" +
        "WITH (DATA_COMPRESSION = NONE);" + "\r\n" +
        "set identity_insert [pubQxNew2] on;" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'电子病历',104,0,99,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'知识库',105,104,100,'HIS.EMR','Form知识库',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'模板管理',106,104,101,'HIS.EMR','XtraForm模板管理',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'病历借阅',107,104,102,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'编辑病历',116,104,103,'HIS.EMR','XtraForm病人列表',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'病历审核',130,104,104,'HIS.EMR','XtraForm病历审核',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'影像传输',132,104,105,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'病案管理',133,104,106,'HIS.EMR','XtraForm病案管理',1,6,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'ICD编码',137,104,107,'HIS.COMM','XtraFormICD10',1,5,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'临床路径',156,0,108,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'路径向导',157,156,109,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'方案维护',158,156,110,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医技执行',138,0,111,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医技刷次',159,138,112,'HIS.YKT','XtraForm医技刷次',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'刷次统计',139,138,113,'HIS.YKT','XtraForm刷次统计',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'刷次余量',140,138,114,'HIS.YKT','XtraForm刷次余量',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊业务',163,138,115,'HIS.YKT','XtraForm门诊业务',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊执行浏览',172,138,116,'HIS.YKT','XtraForm执行业务浏览',1,6,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药库管理',1,0,117,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'日常业务',144,1,118,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药库出库',2,144,119,'HIS.YKGL','RCYW.frm药库出库',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药库入库',3,144,120,'HIS.YKGL','RCYW.frm药库入库',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房申请',4,144,121,'HIS.YKGL','RCYW.XtraForm药房申请',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'数据维护',145,1,122,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药典维护',8,145,123,' HIS.YKGL','SJWH.XtraForm药典维护',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药学信息',64,145,124,'HIS.YKGL','SJWH.XtraForm药学信息',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药库盘点',68,145,125,'HIS.YKGL','RCYW.frm药库盘点',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药库调价',66,145,126,'HIS.YKGL','RCYW.frm药库调价',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'供货商维护',124,145,127,'HIS.YKGL','SJWH.XtraForm供货商',1,5,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'统计查询',146,1,128,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'库存查询',5,146,129,'HIS.YKGL','TJCX.XtraForm药库库存',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药库盘点记录',69,146,130,'HIS.YKGL','TJCX.frm药库盘点记录',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药库调价记录',67,146,131,'HIS.YKGL','TJCX.frm药库调价记录',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'入库统计',6,146,132,'HIS.YKGL','TJCX.XtraForm入库统计',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'出库统计',7,146,133,'HIS.YKGL','TJCX.XtraForm出库统计',1,5,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'保管员台账',65,146,134,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医生站',95,0,135,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院医嘱',96,95,136,'HIS.YSGZZ','XtraForm医嘱处理',1,3,1,'电子医嘱')" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊处方',97,95,137,'HIS.YSGZZ','XtraForm门诊处方',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院病人记费',98,95,138,'HIS.ZYGL','RCYW.XtraForm住院记费',1,5,1,'医生记费')" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'病人管理',99,95,139,'HIS.YSGZZ','XtraForm病人管理',1,5,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊处方浏览',103,95,140,'HIS.YSGZZ','XtraForm门诊处方浏览',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'角色权限',41,39,141,'HIS.WHXT','XtraForm角色权限管理',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'科室员工',42,39,142,'HIS.WHXT','XtraForm科室用户设置',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'收费项目',43,39,143,'HIS.WHXT','XtraForm收费项目设置',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医嘱项目',179,39,144,'HIS.WHXT','XtraForm医嘱项目',1,7,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'纸张设置',44,39,145,'HIS.COMM','XtraForm打印设置',1,6,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'票据领用',82,39,146,'HIS.WHXT','XtraForm票据领用',1,8,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'套餐设置',83,39,147,'HIS.WHXT','XtraForm套餐设置',1,10,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'床位设置',110,39,148,'HIS.WHXT','XtraForm床位设置',1,9,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'通用参数',113,39,149,'HIS.WHXT','XtraForm通用参数',1,11,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'同步门诊接口目录',129,39,150,'HIS.WHXT',null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'公告管理',160,39,151,'HIS.COMM','XtraForm通知公告发布',1,14,1,' ')" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'站点设置',171,39,152,'HIS.WHXT','XtraForm站点设置',1,13,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'刷新权限',45,39,153,'HIS.WHXT',null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'科室设置',46,39,155,'HIS.WHXT','XtraForm科室信息修改',1,99,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房管理',9,0,1,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'日常业务',147,9,2,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房划价',126,147,3,'HIS.YFGL','RCYW.XtraForm药房划价',1,0,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊发药',10,147,4,'HIS.YFGL','RCYW.XtraForm门诊发药',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院发药',11,147,5,'HIS.YFGL','RCYW.XtraForm住院发药',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'入库申请',13,147,6,'HIS.YFGL','KCGL.XtraForm入库申请',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'取消门诊发药',73,147,7,'HIS.YFGL','RCYW.XtraForm取消门诊发药',1,5,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'取消住院发药',74,147,8,'HIS.YFGL','RCYW.XtraForm取消住院发药',1,6,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'调拨申请',180,147,9,'HIS.YFGL','KCGL.XtraForm调拨申请',1,7,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'调拨确认',181,147,10,'HIS.YFGL','KCGL.XtraForm调拨确认',1,8,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'数据维护',148,9,11,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房初始化录入',125,148,12,'HIS.YFGL','KCGL.XtraForm药房初始化录入',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房盘点',16,148,13,'HIS.YFGL','KCGL.frm药房盘点',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房调价',71,148,14,'HIS.YFGL','KCGL.frm药房调价',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'统计查询',149,9,15,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房库存',12,149,16,'HIS.YFGL','TJCX.XtraForm药房库存',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'库存预警',175,149,17,'HIS.YFGL','TJCX.XtraForm库存预警',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房入库查询',14,149,18,'HIS.YFGL','TJCX.frm入库浏览',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊结账药费查询',15,149,19,'HIS.YFGL','TJCX.frm门诊结账查询',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房盘点记录',70,149,20,'HIS.YFGL','TJCX.frm药房盘点记录',1,5,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房调价记录',72,149,21,'HIS.YFGL','KCGL.frm调价记录',1,6,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院发药查询',75,149,22,'HIS.YFGL','TJCX.frm已发药处方',1,7,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'病区发药统计',112,149,23,'HIS.YFGL','TJCX.XtraForm病区发药统计',1,8,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊发药统计',164,149,24,'HIS.YFGL','TJCX.XtraForm门诊发药统计',1,9,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院管理',24,0,25,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'日常业务',150,24,26,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'入院登记',25,150,27,'HIS.ZYGL','RCYW.XtraForm入院登记',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'预交款录入',26,150,28,'HIS.ZYGL','RCYW.XtraForm预交款录入',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院记费',27,150,29,'HIS.ZYGL','RCYW.XtraForm住院记费',1,3,1,'住院处记费')" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'出院结算',28,150,30,'HIS.ZYGL','RCYW.XtraForm出院结算',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'费用清单打印',78,150,31,'HIS.ZYGL','TJCX.XtraForm费用清单',1,7,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'出院申请',109,150,32,'HIS.ZYGL','RCYW.XtraForm出院申请',1,6,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'未发药处方编辑',36,150,33,'HIS.ZYGL','RCYW.XtraForm未发药处方编辑',1,9,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'财务结账',153,24,34,null,null,null,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'个人结账',30,153,35,'HIS.ZYGL','RCYW.XtraForm个人结帐',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院处结账',31,153,36,'HIS.ZYGL','RCYW.XtraForm住院处结帐',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'统计查询',152,24,37,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'在院病人',33,152,38,'HIS.ZYGL','TJCX.XtraForm在院病人',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'出院病人',34,152,39,'HIS.ZYGL','TJCX.XtraForm出院病人',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'数据维护',151,24,40,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'补采身份证',77,151,41,'HIS.ZYGL','RCYW.XtraForm补采身份证',1,6,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'在院病人信息修改',35,151,42,'HIS.ZYGL','RCYW.XtraForm在院病人信息修改',1,7,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'取消出院',29,151,43,'HIS.ZYGL','RCYW.XtraForm取消出院',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'在院病人退费',173,151,44,'HIS.ZYGL','RCYW.XtraForm在院病人退费',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊收款管理',18,0,45,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊收款',19,18,46,'HIS.MZSF','RCYW.XtraForm门诊收款',1,0,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'财务结账',154,18,47,null,null,null,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'收款员结账',20,154,48,'HIS.MZSF','RCYW.XtraForm收款员结账',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'收款处结账',21,154,49,'HIS.MZSF','RCYW.XtraForm收款处结账',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'统计查询',155,18,50,null,null,null,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'业务浏览',23,155,51,'HIS.MZSF','TJCX.XtraForm已结算业务浏览',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊单据作废',76,18,52,'HIS.MZSF','RCYW.XtraForm单据作废',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医卡通业务',161,18,53,null,null,null,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医卡通业务',168,161,54,'HIS.YKT','XtraForm医卡通门诊业务',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医卡通账户浏览',166,161,55,'HIS.YKT','XtraForm医卡通账户浏览',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医卡通结账',174,161,56,'HIS.YKT','XtraForm医卡通门诊结账',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'基药实施工具',84,0,57,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'卫生室基药实施调价统计',85,84,58,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'报表中心',49,0,59,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'工作量统计',50,49,60,'HIS.YZCX','XtraForm工作量统计',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'临床路径',51,49,61,'HIS.YZCX',null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'收款员结账记录',52,49,62,'HIS.MZSF','TJCX.XtraForm收款员结账记录',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'收款处结账记录',53,49,63,'HIS.MZSF','TJCX.XtraForm收款处结账记录',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院处个人结账记录',54,49,64,'HIS.ZYGL','TJCX.XtraFrom个人结帐记录',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院处结账记录',55,49,65,'HIS.ZYGL','TJCX.XtraForm住院处结帐记录',1,5,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'核算中心报表',56,49,66,'HIS.YZCX','XtraForm核算中心报表',1,6,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药房库存报表',86,49,67,'HIS.YZCX','frm药房库存',1,7,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药库库存报表',87,49,68,'HIS.YKGL','TJCX.XtraForm药库库存',1,8,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'卫生室库存报表',88,49,69,'HIS.YZCX','XtraForm卫生室库存报表',1,9,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'药占比统计',89,49,70,'HIS.YZCX','XtraForm药占比统计',1,10,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'发票使用统计',90,49,71,'HIS.YZCX','XtraForm发票使用统计',1,11,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医生收治住院人次',91,49,72,'HIS.YZCX','XtraForm医生收治住院人次',1,12,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊发药统计',176,49,73,'HIS.YFGL','TJCX.XtraForm门诊发药统计',1,13,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'卫生室接口',59,0,74,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'卫生室调价',60,59,75,'HIS.InterFaceWSS','frm调价单',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'卫生室调价记录',61,59,76,'HIS.InterFaceWSS','frm调价记录',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'接口发药',62,59,77,'HIS.InterFaceWSS','XtraForm发药接口',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'接口发药记录',63,59,78,'HIS.InterFaceWSS','XtraForm接口发药统计',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'护士站',37,0,79,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'执行医嘱',114,37,80,'HIS.HSZH','XtraForm执行医嘱',1,1,1,' ')" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'护理记费',38,37,81,'HIS.ZYGL','RCYW.XtraForm住院记费',1,5,1,'护理记费')" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'床位管理',81,37,82,'HIS.HSZH','XtraForm床位管理',1,6,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'门诊输液贴',141,37,83,'HIS.HSZH','XtraForm门诊输液贴',1,2,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'住院输液贴',142,37,84,'HIS.HSZH','XtraForm住院输液贴',1,3,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'体温单',187,37,85,'HIS.HSZH','TWD.XtraForm批量体温',1,4,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'检验申请单',143,37,86,'HIS.HSZH','XtraForm检验申请单',1,8,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'护士工作量统计',170,37,87,'HIS.HSZH','XtraForm工作量统计',1,9,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'执行医嘱记录',80,37,88,'HIS.HSZH','XtraForm执行医嘱记录',1,10,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'医嘱打印',182,37,89,'HIS.YSGZZ','XtraForm医嘱处理',1,7,1,'护理打印医嘱')" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'输液记录',183,37,90,'HIS.HSZH','XtraForm住院输液记录',1,11,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'自动记费设置',186,37,91,'HIS.HSZH','XtraForm自动记费设置',1,12,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'PACS条码',188,37,92,'HIS.HSZH','XtraFormPacs申请单',1,13,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'接诊记录接口',184,95,93,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'诊治记录查询',185,95,94,null,null,null,null,0,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'协定处方',178,39,95,'HIS.WHXT','XtraForm协定处方',1,12,1,'管理员维护')" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'管理员维护',39,0,96,null,null,null,null,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'日志查询',40,39,97,'HIS.COMM','XtraForm日志查询',1,1,1,null)" + "\r\n" +
        "insert into [dbo].[pubQxNew2]([模块名],[键ID],[父ID],[ID],[命名空间],[窗口名],[图标索引],[显示顺序],[是否显示],[传递参数]) values (N'排班表',189,95,98,'HIS.PBB','XtraForm排班',1,1,1,null)" + "\r\n" +
        "set identity_insert [pubQxNew2] off;";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



        #endregion
        public static bool b医嘱临床路径改造()
        {
            try
            {
                string SQL =
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱') and name = '医嘱套餐编码')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱 add 医嘱套餐编码 varchar(50) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱历史') and name = '医嘱套餐编码')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱历史 add 医嘱套餐编码 varchar(50) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱') and name = '用药状态')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱 add 用药状态 varchar(5) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱历史') and name = '用药状态')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱历史 add 用药状态 varchar(5) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱') and name = '医嘱验证标记')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱 add 医嘱验证标记 varchar(5) NULL;" + "\r\n" +
            "  end" + "\r\n" +
            "" + "\r\n" +
            "if not exists" + "\r\n" +
            "     (select 1" + "\r\n" +
            "      from   syscolumns" + "\r\n" +
            "      where  id = object_id('YS住院医嘱历史') and name = '医嘱验证标记')" + "\r\n" +
            "  begin" + "\r\n" +
            "alter table YS住院医嘱历史 add 医嘱验证标记 varchar(5) NULL;" + "\r\n" +
            "  end" +
                "" + "\r\n" +
           "if not exists" + "\r\n" +
           "     (select 1" + "\r\n" +
           "      from   syscolumns" + "\r\n" +
           "      where  id = object_id('YS住院医嘱') and name = '医嘱验证标记')" + "\r\n" +
           "  begin" + "\r\n" +
           "alter table YS住院医嘱 add 医嘱验证标记 varchar(5) NULL;" + "\r\n" +
           "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b升级二次住院病人的EMR状态()
        {
            try
            {
                string SQL =
                "alter table [emr文书状态] add zyid numeric(18,0)" + "\r\n" +
                "update aa set aa.zyid=bb.zyid from [emr文书状态] aa left outer join [ZY病人信息] bb on aa.[病案号]=bb.[住院号码]" + "\r\n" +
                "where aa.zyid is null and bb.[住院次数]=1";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                string SQL2 =
                    "DROP INDEX [IX_emr病历索引_主题唯一] ON [dbo].[emr病历索引];" + "\r\n" +
                    "" + "\r\n" +
                    "CREATE UNIQUE NONCLUSTERED INDEX [IX_emr病历索引_主题唯一]" + "\r\n" +
                    "ON [dbo].[emr病历索引]" + "\r\n" +
                    "([主题] , [医院编码] , [ZYID])" + "\r\n" +
                    "" + "\r\n" +
                    "ALTER TABLE [dbo].[emr病历索引]" + "\r\n" +
                    "ALTER COLUMN [病案号] varchar(50)";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL2);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b升级通知公告增加附件内容()
        {
            try
            {
                string SQL = "alter table dbo.pubnoteic add 附件内容 image,附件文件名 varchar(30)";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



        public static bool b报表中心统计报表()
        {
            try
            {
                string SQL =
                    "ALTER VIEW [dbo].[ZY出院费用View]" + "\r\n" +
                    "AS" + "\r\n" +
                    "  SELECT cc.科室, aa.id, aa.ZYID, 项目编码, 费用编码, bb.财务分类 费用名称, 金额, 数量, 单价," + "\r\n" +
                    "         进价, 已发药标记,YPID, 药房编码, 医生编码, 记费人编码, 发药人编码, 记费时间, 发药时间," + "\r\n" +
                    "         aa.汇总时间, aa.月结时间, aa.单位编码, aa.分院编码, aa.CreateTime, 医嘱ID" + "\r\n" +
                    "  FROM   ZY出院费用 aa" + "\r\n" +
                    "         LEFT OUTER JOIN YK药品信息 bb ON aa.费用编码 = bb.药品序号" + "\r\n" +
                    "         LEFT OUTER JOIN ZY病人信息 cc ON aa.ZYID = cc.ZYID" + "\r\n" +
                    "  WHERE  ypid <> 0" + "\r\n" +
                    "  UNION ALL" + "\r\n" +
                    "  SELECT cc.科室, aa.id, aa.ZYID, 项目编码, 费用编码, 费用名称, 金额, 数量, 单价," + "\r\n" +
                    "         进价, 已发药标记, YPID, 药房编码, 医生编码, 记费人编码, 发药人编码, 记费时间, 发药时间," + "\r\n" +
                    "         aa.汇总时间, aa.月结时间, aa.单位编码, aa.分院编码, aa.CreateTime, 医嘱ID" + "\r\n" +
                    "  FROM   ZY出院费用 aa LEFT OUTER JOIN ZY病人信息 cc ON aa.ZYID = cc.ZYID" + "\r\n" +
                    "  WHERE  ypid = 0";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                string SQL1 =
                    "ALTER VIEW [dbo].[ZY在院费用View]" + "\r\n" +
                    "AS" + "\r\n" +
                    "  select cc.科室, aa.id, aa.ZYID, 项目编码, 费用编码, bb.财务分类 费用名称, 金额, 数量, 单价," + "\r\n" +
                    "         进价, 已发药标记, YPID, 药房编码, 医生编码, 记费人编码, 发药人编码, 记费时间, 发药时间," + "\r\n" +
                    "         aa.汇总时间, aa.月结时间, aa.单位编码, aa.分院编码, aa.CreateTime, 医嘱ID" + "\r\n" +
                    "  from   ZY在院费用 aa" + "\r\n" +
                    "         left outer join YK药品信息 bb on aa.费用编码 = bb.药品序号" + "\r\n" +
                    "         left outer join ZY病人信息 cc on aa.zyid = cc.zyid" + "\r\n" +
                    "  where  ypid <> 0" + "\r\n" +
                    "  union all" + "\r\n" +
                    "  select cc.科室, aa.id, aa.ZYID, 项目编码, 费用编码, 费用名称, 金额, 数量, 单价," + "\r\n" +
                    "         进价, 已发药标记, YPID, 药房编码, 医生编码, 记费人编码, 发药人编码, 记费时间, 发药时间," + "\r\n" +
                    "         aa.汇总时间, aa.月结时间, aa.单位编码, aa.分院编码, aa.CreateTime, 医嘱ID" + "\r\n" +
                    "  from   ZY在院费用 aa left outer join ZY病人信息 cc on aa.zyid = cc.zyid" + "\r\n" +
                    "  where  ypid = 0";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL1);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b药典是否禁用问题()
        {
            try
            {
                string SQL = "update YK药品信息 set 是否禁用=0 where 是否禁用 is null";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool bHL自动记费设置建表()
        {
            try
            {
                string SQL =
                    "CREATE TABLE [dbo].[HL自动记费设置](" + "\r\n" +
                    "	[组别] [varchar](50) NULL," + "\r\n" +
                    "	[费用编码] [varchar](50) NULL," + "\r\n" +
                    "	[费用名称] [varchar](50) NULL," + "\r\n" +
                    "	[数量] [numeric](18, 2) NULL," + "\r\n" +
                    "	[药品序号] [int] NULL," + "\r\n" +
                    "	[病区编码] [varchar](50) NULL," + "\r\n" +
                    "	[ID] [int] IDENTITY(1,1) NOT NULL," + "\r\n" +
                    " CONSTRAINT [PK_HL自动记费设置] PRIMARY KEY CLUSTERED " + "\r\n" +
                    "(" + "\r\n" +
                    "	[ID] ASC" + "\r\n" +
                    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
                    ") ON [PRIMARY]";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b_vw全部病人修改()
        {
            try
            {
                string SQL =
                 "ALTER VIEW [dbo].[vw全部病人]" + "\r\n" +
                 "AS" + "\r\n" +
                 "  SELECT zyid 子zyid, zyid 父zyid, 病区, 0 子病人, 住院号码, 病人姓名, 已出院标记, 在院状态, 病床" + "\r\n" +
                 "  FROM   zy病人信息" + "\r\n" +
                 "  UNION ALL" + "\r\n" +
                 "  SELECT 子zyid, 父zyid, bb.病区, 1 子病人, aa.住院号码, aa.病人姓名, bb.已出院标记, bb.在院状态, bb.病床" + "\r\n" +
                 "  FROM   zy子病人信息 aa LEFT OUTER JOIN [ZY病人信息] bb ON aa.[父ZYID] = bb.ZYID";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b_vw全部病人()
        {
            try
            {
                string SQL =
                 "CREATE VIEW [dbo].[vw全部病人]" + "\r\n" +
                 "AS" + "\r\n" +
                 "  SELECT zyid 子zyid, zyid 父zyid, 病区, 0 子病人, 住院号码, 病人姓名 FROM zy病人信息" + "\r\n" +
                 "  UNION ALL" + "\r\n" +
                 "  SELECT 子zyid, 父zyid, bb.病区, 1 子病人, aa.住院号码, aa.病人姓名" + "\r\n" +
                 "  FROM   zy子病人信息 aa LEFT OUTER JOIN [ZY病人信息] bb ON aa.[父ZYID] = bb.ZYID";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool bSQL子病人信息()
        {
            try
            {
                string SQL =
                   "CREATE TABLE [dbo].[ZY子病人信息](" + "\r\n" +
                   "  [ID]           [numeric](18, 0)" + "\r\n" +
                   "      IDENTITY (" + "\r\n" +
                   "                1," + "\r\n" +
                   "                1)" + "\r\n" +
                   "      NOT NULL," + "\r\n" +
                   "  [子ZYID]      [numeric](18, 0) NOT NULL," + "\r\n" +
                   "  [住院号码] [nvarchar](20) NOT NULL," + "\r\n" +
                   "  [医疗证号] [nvarchar](50) NULL," + "\r\n" +
                   "  [病人姓名] [nvarchar](10) NOT NULL," + "\r\n" +
                   "  [性别]       [nvarchar](50) NULL," + "\r\n" +
                   "  [出生日期] [datetime] NULL," + "\r\n" +
                   "  [身份证号] [nvarchar](20) NULL," + "\r\n" +
                   "  [父ZYID]      [numeric](18, 0) NOT NULL," + "\r\n" +
                   "  [createTime]   [datetime] NULL," + "\r\n" +
                   "  CONSTRAINT [PK_ZY子病人信息] PRIMARY KEY" + "\r\n" +
                   "    CLUSTERED" + "\r\n" +
                   "    ([ID] ASC)" + "\r\n" +
                   "    WITH (PAD_INDEX = OFF," + "\r\n" +
                   "          STATISTICS_NORECOMPUTE = OFF," + "\r\n" +
                   "          IGNORE_DUP_KEY = OFF," + "\r\n" +
                   "          ALLOW_ROW_LOCKS = ON," + "\r\n" +
                   "          ALLOW_PAGE_LOCKS = ON)" + "\r\n" +
                   "    ON [PRIMARY])" + "\r\n" +
                   "ON [PRIMARY]" + "\r\n" +
                   "" + "\r\n" +
                   "ALTER TABLE [dbo].[ZY子病人信息]  WITH CHECK ADD  CONSTRAINT [FK_ZY子病人信息_ZY病人信息] FOREIGN KEY([父ZYID])" + "\r\n" +
                   "REFERENCES [dbo].[ZY病人信息] ([ZYID])" + "\r\n" +
                   "" + "\r\n" +
                   "ALTER TABLE [dbo].[ZY子病人信息] CHECK CONSTRAINT [FK_ZY子病人信息_ZY病人信息]" + "\r\n" +
                   "" + "\r\n" +
                   "ALTER TABLE [dbo].[ZY子病人信息] ADD  CONSTRAINT [DF_ZY子病人信息_createTime]  DEFAULT (getdate()) FOR [createTime]";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region 权限更新
        public static bool b影像结果191()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '影像结果'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 95 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 191" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew2" + "\r\n" +
                    "where [键ID] = 191" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew2(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b化验结果190()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '化验结果'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 95 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 190" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew2" + "\r\n" +
                    "where [键ID] = 190" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew2(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b排班表189()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '排班表'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 95 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 189" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 189" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b日志服务器时间默认值()
        {
            try
            {
                string SQL =
   "ALTER TABLE dbo.pub_Log ADD CONSTRAINT" + "\r\n" +
   " DF_pub_Log_serverTime DEFAULT getdate() FOR serverTime";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b修复体温单和自动记费重复键ID()
        {
            string SQL =
            "delete pubQxNew where [模块名]='自动记费设置' and [键ID]=187" + "\r\n" +
            "update pubQxNew set 键ID=186 where [模块名]='自动记费设置'" + "\r\n" +
            "delete pubjsQxNew where [模块名]='自动记费设置' and [键ID]=187" + "\r\n" +
            "update pubjsQxNew set 键ID=186 where [模块名]='自动记费设置'";

            HIS.Model.Dal.SqlHelper.ExecuteNonQuery(Properties.Settings.Default.ConnHisDbString, CommandType.Text, SQL);
            return true;
        }
        public static bool b管理员维护39()
        {
            try
            {
                string SQL_权限1 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '管理员维护'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 0 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 39" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 39" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID,是否显示)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id,1)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限1);
                string SQL_权限2 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '日志查询'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 39 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 40" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 40" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID,是否显示)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id,1)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限2);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool bPACS条码188()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = 'PACS条码'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 188" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 188" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b护理体温单187()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '体温单'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 187" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 187" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b护理自动记费186()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '自动记费设置'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 186" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 186" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b删除权限117三卡合一()
        {
            try
            {
                string SQL_权限 =
                    "delete pubQxNew where 键id=117 delete pubJsQxNew where 键id=117";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b诊治记录查询185()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '诊治记录查询'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 95 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 185" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 185" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b接诊记录接口184()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '接诊记录接口'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 95 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 184" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 184" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b护理输液记录183()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '输液记录'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 183" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 183" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b护理打印医嘱182()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '医嘱打印'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 37 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 182" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 182" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool b药房调拨确认权限181()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '调拨确认'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 147 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 181" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 181" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b药房调拨申请权限180()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '调拨申请'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 147 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "set @NewId  = 180" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 180" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool b管理员维护_协定处方权限()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '协定处方'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 39 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "--select @NewId= max(键ID)+1 from pubqxNew" + "\r\n" +
                    "set @NewId  = 178" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 178" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b历史库存功能权限()
        {
            try
            {
                string SQL_权限 =
                    "declare @Name varchar(20)" + "\r\n" +
                    "set @Name   = '历史库存'" + "\r\n" +
                    "" + "\r\n" +
                    "declare @父Id int" + "\r\n" +
                    "set @父Id  = 149 --从前台查到的Tag" + "\r\n" +
                    "" + "\r\n" +
                    "declare" + "\r\n" +
                    "  @NewId  int," + "\r\n" +
                    "  @ishave int" + "\r\n" +
                    "--select @NewId= max(键ID)+1 from pubqxNew" + "\r\n" +
                    "set @NewId  = 177" + "\r\n" +
                    "select @ishave = count(*)" + "\r\n" +
                    "from pubqxnew" + "\r\n" +
                    "where [键ID] = 177" + "\r\n" +
                    "" + "\r\n" +
                    "if @ishave = 0" + "\r\n" +
                    "  begin" + "\r\n" +
                    "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                    "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                    "  end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        #endregion
        //alter table YS住院医嘱历史 alter column 医嘱内涵 varchar(200)

        public static bool b长期医嘱只执行一次()
        {
            try
            {
                string SQL_权限 = "update [YS医嘱用法] set 长期医嘱只执行一次=0 where [长期医嘱只执行一次] is null";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b输液记录打印标志()
        {
            try
            {
                string SQL_权限 = "alter table ys医嘱频次 add 输液记录打印标志 int";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool b医嘱续打时间点()
        {
            try
            {
                string SQL =
                 "alter table zy病人信息 add 长期医嘱续打时间点 datetime,临时医嘱续打时间点 datetime";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool b药房调拨()
        {
            try
            {
                string SQL = "alter table YF入库摘要 add 调拨药房编码 int,确认时间 datetime";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool b升级出院结算和取消结算()
        {
            try
            {
                string SQL =
            "CREATE TABLE [dbo].[YS住院医嘱历史](" + "\r\n" +
            "	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL," + "\r\n" +
            "	[ZYID] [numeric](18, 0) NULL," + "\r\n" +
            "	[医嘱类型] [nvarchar](10) NULL," + "\r\n" +
            "	[组别] [int] NULL," + "\r\n" +
            "	[项目类型] [nvarchar](10) NULL," + "\r\n" +
            "	[项目编码] [int] NULL," + "\r\n" +
            "	[项目名称] [nvarchar](50) NULL," + "\r\n" +
            "	[剂量数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[剂量单位] [nvarchar](10) NULL," + "\r\n" +
            "	[医嘱数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次名称] [nvarchar](50) NULL," + "\r\n" +
            "	[用法名称] [nvarchar](50) NULL," + "\r\n" +
            "	[开嘱医生编码] [int] NULL," + "\r\n" +
            "	[开嘱时间] [datetime] NULL," + "\r\n" +
            "	[开嘱执行者编码] [int] NULL," + "\r\n" +
            "	[开嘱核对者编码] [int] NULL," + "\r\n" +
            "	[执行时间] [datetime] NULL," + "\r\n" +
            "	[停嘱医生编码] [int] NULL," + "\r\n" +
            "	[停嘱执行者编码] [int] NULL," + "\r\n" +
            "	[停嘱核对者编码] [int] NULL," + "\r\n" +
            "	[停嘱时间] [datetime] NULL," + "\r\n" +
            "	[单日总量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[createTime] [datetime] NULL," + "\r\n" +
            "	[临床路径ID] [int] NULL," + "\r\n" +
            "	[组别符号] [nvarchar](6) NULL," + "\r\n" +
            "	[库存数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[药房规格] [varchar](50) NULL," + "\r\n" +
            "	[药房单位] [varchar](10) NULL," + "\r\n" +
            "	[药房编码] [int] NULL," + "\r\n" +
            "	[更新标志] [int] NULL," + "\r\n" +
            "	[单次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[财务分类] [varchar](20) NULL," + "\r\n" +
            "	[页号] [int] NULL," + "\r\n" +
            "	[行号] [int] NULL," + "\r\n" +
            "	[已打印标志] [int] NULL," + "\r\n" +
            "	[子嘱ID] [int] NULL," + "\r\n" +
            "	[医嘱内涵] [varchar](200) NULL," + "\r\n" +
            "	[医嘱备注] [varchar](50) NULL," + "\r\n" +
            "	[停嘱原因] [varchar](50) NULL," + "\r\n" +
            "	[作废原因] [varchar](50) NULL," + "\r\n" +
            "	在院医嘱ID numeric(18,0)" + "\r\n" +
            " CONSTRAINT [PK_YS住院医嘱历史] PRIMARY KEY CLUSTERED " + "\r\n" +
            "(" + "\r\n" +
            "	[ID] ASC" + "\r\n" +
            ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
            ") ON [PRIMARY]" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "CREATE TABLE [dbo].[YS住院子医嘱历史](" + "\r\n" +
            "	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL," + "\r\n" +
            "	[子嘱ID] [int] NULL," + "\r\n" +
            "	[ZYID] [numeric](18, 0) NULL," + "\r\n" +
            "	[医嘱类型] [nvarchar](10) NULL," + "\r\n" +
            "	[组别] [int] NULL," + "\r\n" +
            "	[项目类型] [nvarchar](10) NULL," + "\r\n" +
            "	[项目编码] [int] NULL," + "\r\n" +
            "	[项目名称] [nvarchar](50) NULL," + "\r\n" +
            "	[剂量数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[剂量单位] [nvarchar](10) NULL," + "\r\n" +
            "	[医嘱数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次名称] [nvarchar](50) NULL," + "\r\n" +
            "	[用法名称] [nvarchar](50) NULL," + "\r\n" +
            "	[开嘱医生编码] [int] NULL," + "\r\n" +
            "	[开嘱时间] [datetime] NULL," + "\r\n" +
            "	[开嘱执行者编码] [int] NULL," + "\r\n" +
            "	[开嘱核对者编码] [int] NULL," + "\r\n" +
            "	[执行时间] [datetime] NULL," + "\r\n" +
            "	[停嘱医生编码] [int] NULL," + "\r\n" +
            "	[停嘱执行者编码] [int] NULL," + "\r\n" +
            "	[停嘱核对者编码] [int] NULL," + "\r\n" +
            "	[停嘱时间] [datetime] NULL," + "\r\n" +
            "	[单日总量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[createTime] [datetime] NULL," + "\r\n" +
            "	[临床路径ID] [int] NULL," + "\r\n" +
            "	[组别符号] [nvarchar](6) NULL," + "\r\n" +
            "	[库存数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[药房规格] [varchar](50) NULL," + "\r\n" +
            "	[药房单位] [varchar](10) NULL," + "\r\n" +
            "	[药房编码] [int] NULL," + "\r\n" +
            "	[更新标志] [int] NULL," + "\r\n" +
            "	[单次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[财务分类] [varchar](20) NULL," + "\r\n" +
            "	[页号] [int] NULL," + "\r\n" +
            "	[行号] [int] NULL," + "\r\n" +
            "	[已打印标志] [int] NULL," + "\r\n" +
            "	[医嘱ID] [numeric](18, 0) NOT NULL," + "\r\n" +
            "	在院子医嘱ID numeric(18,0)" + "\r\n" +
            " CONSTRAINT [PK_YS住院子医嘱历史] PRIMARY KEY CLUSTERED " + "\r\n" +
            "(" + "\r\n" +
            "	[ID] ASC" + "\r\n" +
            ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
            ") ON [PRIMARY]" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "CREATE TABLE [dbo].[HL执行医嘱历史](" + "\r\n" +
            "	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL," + "\r\n" +
            "	[医嘱ID] [numeric](18, 0) NULL," + "\r\n" +
            "	[ZYID] [numeric](18, 0) NULL," + "\r\n" +
            "	[项目类型] [nvarchar](10) NULL," + "\r\n" +
            "	[项目编码] [int] NULL," + "\r\n" +
            "	[项目名称] [nvarchar](50) NULL," + "\r\n" +
            "	[医嘱数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[频次名称] [nvarchar](50) NULL," + "\r\n" +
            "	[用法名称] [nvarchar](50) NULL," + "\r\n" +
            "	[单日总量] [numeric](10, 2) NULL," + "\r\n" +
            "	[执行时间] [datetime] NULL," + "\r\n" +
            "	[执行数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[YPID] [numeric](18, 4) NULL," + "\r\n" +
            "	[执行者编码] [int] NULL," + "\r\n" +
            "	[createTime] [datetime] NULL," + "\r\n" +
            "	[组别] [int] NULL," + "\r\n" +
            "	[医嘱类型] [nvarchar](10) NULL," + "\r\n" +
            "	[库存数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[药房规格] [varchar](50) NULL," + "\r\n" +
            "	[药房单位] [varchar](10) NULL," + "\r\n" +
            "	[药房编码] [int] NULL," + "\r\n" +
            "	[剂量数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[剂量单位] [varchar](10) NULL," + "\r\n" +
            "	[单次数量] [numeric](10, 2) NULL," + "\r\n" +
            "	[财务分类] [varchar](20) NULL," + "\r\n" +
            "	在院ID numeric(18,0)" + "\r\n" +
            " CONSTRAINT [PK_HL医嘱执行历史] PRIMARY KEY CLUSTERED " + "\r\n" +
            "(" + "\r\n" +
            "	[ID] ASC" + "\r\n" +
            ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + "\r\n" +
            ") ON [PRIMARY]" + "\r\n" +
            "" + "\r\n" +
            "" + "\r\n" +
            "alter table zy出院费用 add 在院费用ID numeric(18,0)";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool b协定处方功能升级()
        {
            try
            {
                string SQL =
                  "alter TABLE [GY协定处方] add 处方类型 varchar(20)";
                string SQL2 =
                "update [GY协定处方] set 处方类型='个人' where 处方类型 is null";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL2);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public static bool b更新医嘱内涵()
        {
            try
            {
                string SQL =
                 "  update ys住院医嘱 set 医嘱内涵=项目名称 where 医嘱内涵 is null";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static bool b医嘱处理增加字段()
        {
            try
            {
                string SQL =
                 " alter table [YS住院医嘱] add 医嘱内涵 varchar(50),医嘱备注 varchar(50),停嘱原因 varchar(50),作废原因 varchar(50)";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static bool b病历索引加ZYID()
        {
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text,
                    "alter table dbo.emr病历索引 add ZYID numeric(18,0)");
            }
            catch (Exception ex)
            {
                //throw;
            }
            return true;
        }

        public static bool b子医嘱约束问题()
        {
            string SQL =
    "" + "\r\n" +
    "ALTER TABLE dbo.YS住院子医嘱 ADD CONSTRAINT" + "\r\n" +
    "	PK_YS住院子医嘱 PRIMARY KEY CLUSTERED " + "\r\n" +
    "	(" + "\r\n" +
    "	ID" + "\r\n" +
    "	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]" + "\r\n" +
    "" + "\r\n" +
    "alter table YS住院子医嘱 add  医嘱ID numeric(18,0)" + "\r\n" +
    "" + "\r\n" +
    "ALTER TABLE dbo.YS住院子医嘱 ADD CONSTRAINT" + "\r\n" +
    "	FK_YS住院子医嘱_YS住院医嘱 FOREIGN KEY" + "\r\n" +
    "	(" + "\r\n" +
    "	医嘱ID" + "\r\n" +
    "	) REFERENCES dbo.YS住院医嘱" + "\r\n" +
    "	(" + "\r\n" +
    "	ID" + "\r\n" +
    "	) ON UPDATE  CASCADE " + "\r\n" +
    "	 ON DELETE  CASCADE " + "\r\n" +
    "" + "\r\n" +
    "----删除子医嘱孤立数据" + "\r\n" +
    "delete aa from  [dbo].[YS住院子医嘱] aa left outer join " + "\r\n" +
    "ys住院医嘱 bb on aa.子嘱ID=bb.子嘱ID and aa.医嘱类型=bb.医嘱类型 and aa.ZYID=bb.ZYID" + "\r\n" +
    "where bb.ID is not null and bb.停嘱时间 is not null and aa.停嘱时间 is null" + "\r\n" +
    "" + "\r\n" +
    "" + "\r\n" +
    "----更新停嘱时间" + "\r\n" +
    "update bb set bb.停嘱时间=aa.停嘱时间 from [dbo].[YS住院子医嘱] aa left outer join " + "\r\n" +
    "ys住院医嘱 bb on aa.子嘱ID=bb.子嘱ID and aa.医嘱类型=bb.医嘱类型 and aa.ZYID=bb.ZYID" + "\r\n" +
    "where bb.ID is not null and bb.停嘱时间 is not null and aa.停嘱时间 is null" + "\r\n" +
    "" + "\r\n" +
    "----更新子表医嘱ＩＤ" + "\r\n" +
    "update aa set aa.医嘱ID=bb.id from" + "\r\n" +
    "[dbo].[YS住院子医嘱] aa left outer join ys住院医嘱 bb " + "\r\n" +
    "on aa.子嘱ID=bb.子嘱ID and aa.医嘱类型=bb.医嘱类型 and aa.ZYID=bb.ZYID" + "\r\n" +
    "where bb.ID is not null and aa.医嘱ID is null" + "\r\n" +
    "";


            //    string SQL =
            //"update aa" + "\r\n" +
            //"set    aa.停嘱时间 = bb.停嘱时间" + "\r\n" +
            //"from   YS住院子医嘱 aa left outer join YS住院医嘱 bb on aa.医嘱ID = bb.ID" + "\r\n" +
            //"where  aa.停嘱时间 is null and bb.停嘱时间 is not null";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
            }
            catch (Exception ex)
            {

                //throw;
            }
            return true;

        }
        public static bool b收款整合居民报销()
        {
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, "alter table [MF门诊摘要] add 医保结算流水号 varchar(50)");
            }
            catch (Exception ex)
            {

                //throw;
            }
            return true;
        }
        public static bool b医嘱剂量打印显示()
        {
            string SQL =
        "CREATE function [dbo].[ClearZero](@inValue varchar(50))" + "\r\n" +
        "  returns varchar(50)" + "\r\n" +
        "as" + "\r\n" +
        "  begin" + "\r\n" +
        "    declare @returnValue varchar(20)" + "\r\n" +
        "" + "\r\n" +
        "    if (@inValue = '')" + "\r\n" +
        "      set @returnValue = '' --空的时候为空" + "\r\n" +
        "    else" + "\r\n" +
        "      if (charindex('.', @inValue) = '0')" + "\r\n" +
        "        set @returnValue = @inValue --针对不含小数点的" + "\r\n" +
        "      else" + "\r\n" +
        "        if (substring(reverse(@inValue), patindex('%[^0]%', reverse(@inValue)), 1) = '.')" + "\r\n" +
        "          set @returnValue = left(@inValue, len(@inValue) - patindex('%[^0]%', reverse(@inValue))) --针对小数点后全是0的" + "\r\n" +
        "        else" + "\r\n" +
        "          set @returnValue = left(@inValue, len(@inValue) - patindex('%[^0]%.%', reverse(@inValue)) + 1) --其他任何情形" + "\r\n" +
        "" + "\r\n" +
        "    return @returnValue" + "\r\n" +
        "  end";

            return true;
        }



        public static bool b修改EMR模板类型列()//解决截断二进制字符串问题
        {
            try
            {
                string SQL =
                   "alter table [emr模板索引] alter column	[模板文件名] [nvarchar](100)" + "\r\n" +
                   "alter table [emr模板索引] alter column	[模板编码] [nvarchar](100) ";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool b增加病历类型术前讨论()
        {
            try
            {
                string SQL =
                   "declare @iCount int" + "\r\n" +
                   "select   @iCount=count(*)" + "\r\n" +
                   "from     [emr模板类型] where [类型编码]='Z1'" + "\r\n" +
                   "if  @iCount=0" + "\r\n" +
                   "begin" + "\r\n" +
                   "insert into [emr模板类型]([类型编码], [类型名称], CATALOG_TYPE, [图标索引], [被选图标索引], ONE_FILE_FLAG," + "\r\n" +
                   "              [用户类型], [模块类型], [排列顺序], [是否禁用])" + "\r\n" +
                   "VALUES      (N'Z1', N'术前讨论', N'2', 4, 5, 0, N'1', N'0', 11, '0')" + "\r\n" +
                   "end";
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool b统计报表改造()
        {
            string SQL工作量统计完善 =
                    "ALTER VIEW [dbo].[MF门诊明细View]" + "\r\n" +
                    "AS" + "\r\n" +
                    "  select   aa.ID, aa.收费编码, dd.财务分类 收费名称, SUM(cc.零售价金额) 金额, null 归并编码," + "\r\n" +
                    "           null 归并名称, aa.MZID, aa.单位编码, aa.CreateTIme" + "\r\n" +
                    "  from     MF门诊明细 aa" + "\r\n" +
                    "           left outer join MF处方摘要 bb on aa.MZID = bb.MZID" + "\r\n" +
                    "           left outer join MF处方明细 cc on bb.CFID = cc.CFID" + "\r\n" +
                    "           left outer join yk药品信息 dd on cc.药品序号 = dd.药品序号" + "\r\n" +
                    "  where    aa.收费编码 in (select 收费编码" + "\r\n" +
                    "                               from   GY收费小项" + "\r\n" +
                    "                               where  收费名称 = '西药费') and dd.财务分类 <> '中药费'" + "\r\n" +
                    "  group by aa.ID, aa.收费编码, dd.财务分类, aa.MZID, aa.单位编码, aa.CreateTIme" + "\r\n" +
                    "  union all" + "\r\n" +
                    "  SELECT ID, 收费编码, 收费名称, 金额, 归并编码, 归并名称, MZID, 单位编码, CreateTIme" + "\r\n" +
                    "  FROM   MF门诊明细" + "\r\n" +
                    "  where  收费编码 not in (select 收费编码" + "\r\n" +
                    "                              from   GY收费小项" + "\r\n" +
                    "                              where  收费名称 = '西药费')";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL工作量统计完善);
            }
            catch (Exception ex)
            {
                return false;
            }

            string SQL_报表中心门诊发药权限 =
                "declare @Name varchar(20)" + "\r\n" +
                "set @Name   = '门诊发药统计'" + "\r\n" +
                "" + "\r\n" +
                "declare @父Id int" + "\r\n" +
                "set @父Id  = 49 --从前台查到的Tag" + "\r\n" +
                "" + "\r\n" +
                "declare" + "\r\n" +
                "  @NewId  int," + "\r\n" +
                "  @ishave int" + "\r\n" +
                "--select @NewId= max(键ID)+1 from pubqxNew" + "\r\n" +
                "set @NewId  = 176" + "\r\n" +
                "select @ishave = count(*)" + "\r\n" +
                "from pubqxnew" + "\r\n" +
                "where [键ID] = 176" + "\r\n" +
                "" + "\r\n" +
                "if @ishave = 0" + "\r\n" +
                "  begin" + "\r\n" +
                "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                "  end";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_报表中心门诊发药权限);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool b曼荼罗改造1()
        {
            string SQL0 =
                 "alter table [ZY病人信息] add 医嘱出院时间 datetime,入区时间 datetime";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL0);
            }
            catch (Exception)
            {
            }
            string SQL1 =
                  "alter table [ZY病人信息] add 住院次数 int default 1";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL1);
            }
            catch (Exception)
            {
            }
            string SQL2 =
                "update [ZY病人信息] set 住院次数=1 where 住院次数 is null";
            //"update [ZY病人信息] set 入区时间=入院时间 where 入区时间 is null" + "\r\n" +
            //"update [ZY病人信息] set 医嘱出院时间=出院时间 where 医嘱出院时间 is null";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL2);
            }
            catch (Exception)
            {
            }
            string SQL3 = "alter table [ZY病人信息] add 联系人亲属关系 varchar(10), 联系人姓名 varchar(20)";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL3);
            }
            catch (Exception)
            {
            }

            return true;
        }
        /// <summary>
        /// 20140125升级药房库存预警数据库修改
        /// </summary>
        /// <returns></returns>
        public static bool b升级药房库存预警SQL()
        {
            string SQL_YF库存预警 =
              "if object_id(N'YF库存预警', N'U') is null" + "\r\n" +
              "begin" + "\r\n" +
              "  CREATE TABLE [dbo].[YF库存预警] (" + "\r\n" +
              "  [ID] numeric(18, 0) IDENTITY(1, 1) NOT NULL," + "\r\n" +
              "  [药房编码] int NULL," + "\r\n" +
              "  [药品序号] numeric(10, 0) NULL ," + "\r\n" +
              "  [高储] numeric(10, 2) NULL default 0," + "\r\n" +
              "  [低储] numeric(10, 2) NULL default 0," + "\r\n" +
              "  [单位编码] int NULL," + "\r\n" +
              "  [CreateTime] datetime NULL default getdate())" + "\r\n" +
              "  ON [PRIMARY]" + "\r\n" +
              "  WITH (DATA_COMPRESSION = NONE);" + "\r\n" +
              "end";
            string SQL_权限 =
                 "declare @Name varchar(20)" + "\r\n" +
                 "set @Name   = '库存预警'" + "\r\n" +
                 "" + "\r\n" +
                 "declare @父Id int" + "\r\n" +
                 "set @父Id  = 149 --从前台查到的Tag" + "\r\n" +
                 "" + "\r\n" +
                 "declare" + "\r\n" +
                 "  @NewId  int," + "\r\n" +
                 "  @ishave int" + "\r\n" +
                 "--select @NewId= max(键ID)+1 from pubqxNew" + "\r\n" +
                 "set @NewId  = 175" + "\r\n" +
                 "select @ishave = count(*)" + "\r\n" +
                 "from pubqxnew" + "\r\n" +
                 "where [键ID] = 175" + "\r\n" +
                 "" + "\r\n" +
                 "if @ishave = 0" + "\r\n" +
                 "  begin" + "\r\n" +
                 "    insert into pubqxNew(模块名, 键ID, 父ID)" + "\r\n" +
                 "    values      (@Name, @NewId, @父Id)" + "\r\n" +
                 "  end";
            try
            {
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_YF库存预警);
                HIS.Model.Dal.SqlHelper.ExecuteNonQuery(DBConnHelper.SConnHISDb, CommandType.Text, SQL_权限);
            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }




    }
}
