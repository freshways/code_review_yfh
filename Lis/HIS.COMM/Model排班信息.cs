﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIS.COMM
{
    /// <summary>
    /// 排班实体类
    /// </summary>
    [Serializable]
     public class Model排班信息
     {
        public string 员工编号 { get; set; }
        public string 员工姓名 { get; set; }
        public string 星期开始时间 { get; set; }
        public string 星期一 { get; set; }
        public string 星期二 { get; set; }
        public string 星期三 { get; set; }
        public string 星期四 { get; set; }
        public string 星期五 { get; set; }
        public string 星期六 { get; set; }
        public string 星期日 { get; set; }
        public string 作废标记 { get; set; }
    }
}
