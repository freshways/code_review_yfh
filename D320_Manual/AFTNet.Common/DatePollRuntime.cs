﻿using AFTNet.Database;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
namespace AFTNet.Poll
{
    public class DatePollRuntime
    {

        private string deviceName = "";
        //protected int m_LisBeginNo = 1;
        //protected int m_LisEndNo = 1;
        private List<Lis_Ins_Result> lisResults = null;

        private List<Lis_Ins_Result_Detail> lisDetails = null;

        protected DateTime SampleDate = DateTime.Now.Date;
        protected static ILog log = log4net.LogManager.GetLogger("lisnetlog");

        protected System.ComponentModel.BackgroundWorker backgroundWorker;

        public DatePollRuntime(System.ComponentModel.BackgroundWorker worker)
        {
            backgroundWorker = worker;
        }

        protected string DeviceName
        {
            get
            {
                return deviceName;
            }

            set
            {
                deviceName = value;
            }
        }

        protected List<Lis_Ins_Result> LisResults
        {
            get
            {
                return lisResults;
            }
        }

        protected List<Lis_Ins_Result_Detail> LisDetails
        {
            get
            {
                return lisDetails;
            }
        }

        protected List<Lis_Ins_Result_Img> imgs
        {
            get
            {
                return imgs;
            }
        }

        public void StartDecode()
        {
            try
            {
                LisDatabase lisDB = AftDataContext.GetLisDatabase();
                if (!lisDB.DatabaseExists())
                {
                    backgroundWorker.ReportProgress(0, "连接数据库失败，请联系信息科确认网络是否正常！！");
                    return;
                }

                lisResults = (from g in lisDB.Lis_Ins_Result where g.FDateTime >= SampleDate && g.FDateTime < SampleDate.AddDays(1) && g.FInstrID == DeviceName select g).ToList();
                lisDetails = (from r in lisDB.Lis_Ins_Result join d in lisDB.Lis_Ins_Result_Detail on r.FTaskID equals d.FTaskID where r.FDateTime >= SampleDate && r.FDateTime < SampleDate.AddDays(1) && r.FInstrID == DeviceName select d).ToList();

                DataPollProcess();
            }
            catch (Exception ex)
            {
                log.Error("DataPollRuntime--StartDecode " + ex.Message);
                backgroundWorker.ReportProgress(0, "出现此错误时请联系技术人员，错误内容："+ex.Message);
            }
        }

        protected virtual void DataPollProcess()
        {

        }
    }
}
