﻿using AFTNet.Cofing;
using AFTNet.Database;
using AFTNet.Poll;
using DataModels;
using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;

namespace AFTNet
{
    public class NF_T : DatePollRuntime
    {
        private string m_D320BeginNo = "201603270001";
        private string m_D320EndNo = "201603270001";
        public NF_T(System.ComponentModel.BackgroundWorker worker, DateTime date, int beginNo, int endNo) : base(worker)
        {
            DeviceName = Config.DeviceName;
            SampleDate = date;
            m_D320BeginNo = date.ToString("yyyyMMdd") + string.Format("{0:d4}", beginNo);
            m_D320EndNo = date.ToString("yyyyMMdd") + string.Format("{0:d4}", endNo);
        }

        protected override void DataPollProcess()
        {
            string strDateyyyy_MM_dd = SampleDate.ToString("yyyy-MM-dd");
            string strDateyyyyMMdd = SampleDate.ToString("yyyyMMdd");
            string strDateyyyyMMdd2 = SampleDate.ToString("yyyy-MM-dd").Replace('-', '/');
            LisDB sqlliteDB = new LisDB();
            var lis_record = sqlliteDB.LisRecords.Where(c => c.Testdate.Contains(strDateyyyyMMdd2)).ToList();
            backgroundWorker.ReportProgress(0, "总项目数：" + lis_record.Count.ToString());
            //backgroundWorker.ReportProgress(0, Cofing.Config.DeviceDB);

            //backgroundWorker.ReportProgress(0, DateTime.Now.ToString() + "access end");
            if (lis_record.Count > 0)
            {
                LisDatabase db = AftDataContext.GetLisDatabase();
                DateTime createTime = DateTime.Now;

                foreach (var row in lis_record)
                {
                    DateTime dateTimeOfTheObservation = DateTime.Parse(row.Testdate.ToString());
                    DateTime dateTime = dateTimeOfTheObservation.Date;
                    string fillerOrderNumber = row.Uuid.ToString().Replace("-", "").Replace("{", "").Replace("}", "");
                    string SAMPLE_ID = fillerOrderNumber;
                    Lis_Ins_Result result = LisResults.Where(c => c.FDateTime == dateTime && c.FTaskID == fillerOrderNumber).FirstOrDefault();
                    if (result == null)
                    {
                        result = new Lis_Ins_Result();
                        result.FTaskID = fillerOrderNumber;
                        result.FInstrID = Cofing.Config.DeviceName;
                        result.FResultID = row.Barcode;//样本号
                        result.FDateTime = dateTime;
                        result.FCreateTime = createTime;
                        result.FTransFlag = false;
                        db.Lis_Ins_Result.InsertOnSubmit(result);
                        LisResults.Add(result);
                        backgroundWorker.ReportProgress(0, "\n" + SAMPLE_ID);
                    }
                    addDetail("R", row.ValueR, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("K", row.ValueK, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("Angle", row.ValueAngle, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("MA", row.ValueMa, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("EPL", row.ValueEpl, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("LY30", row.ValueLy30, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("PMA", row.ValuePma, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("G", row.ValueG, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("A", row.ValueA, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("CI", row.ValueCi, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("A30", row.ValueA30, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("CL30", row.ValueCl30, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("CLT", row.ValueClt, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("TPI", row.ValueTpi, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("TMA", row.ValueTma, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("E", row.ValueE, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("SP", row.ValueSp, result, dateTimeOfTheObservation, createTime, db);
                    addDetail("LTE", row.ValueLte, result, dateTimeOfTheObservation, createTime, db);

                    Lis_Ins_Result_Img img = db.Lis_Ins_Result_Img.Where(c => c.FTaskID == result.FTaskID).FirstOrDefault();
                    if (img == null)
                    {
                        img = new Lis_Ins_Result_Img();
                        img.FImgID = Guid.NewGuid().ToString().Replace("-", "");
                        img.FTaskID = result.FTaskID;
                        if (File.Exists(row.CurvePath))
                        {
                            img.FImg = WEISHENG.COMM.Helper.ImageHelper.ImageToBytes(WEISHENG.COMM.Helper.ImageHelper.GetImaegFromFile(row.CurvePath));
                        }
                        db.Lis_Ins_Result_Img.InsertOnSubmit(img);
                    }
                }
                db.SubmitChanges();
            }
            else
            {
                backgroundWorker.ReportProgress(0, "未找到样本数据");
            }


            void addDetail(string item, string value, Lis_Ins_Result result, DateTime dateTimeOfTheObservation, DateTime createTime, LisDatabase db)
            {
                Lis_Ins_Result_Detail detail = (from g in LisDetails where g.FItem == item && g.FTaskID == result.FTaskID select g).FirstOrDefault();
                if (detail == null)
                {
                    detail = new Lis_Ins_Result_Detail();
                    detail.FID = Guid.NewGuid().ToString().Replace("-", "");
                    detail.FTaskID = result.FTaskID;
                    detail.FItem = item;
                    detail.FValue = value;
                    detail.FTime = dateTimeOfTheObservation;
                    detail.FCreateTime = createTime;
                    db.Lis_Ins_Result_Detail.InsertOnSubmit(detail);
                    LisDetails.Add(detail);
                    backgroundWorker.ReportProgress(0, result.FTaskID + "  " + item + ":" + value);
                }
                else
                {
                    if (detail.FValue != value)
                    {
                        Lis_Ins_Result_Detail newDetail = (from g in db.Lis_Ins_Result_Detail where g.FItem == item && g.FTaskID == result.FTaskID select g).FirstOrDefault();
                        if (detail.FValue != value)
                        {
                            detail.FValue = newDetail.FValue = value;

                            backgroundWorker.ReportProgress(0, result.FTaskID + "  " + item + ":" + value + "(旧值：" + detail.FValue + ")");
                        }
                        else
                        {
                            backgroundWorker.ReportProgress(0, result.FTaskID + "  " + item + ":已有一致");
                        }
                    }
                    else
                    {
                        backgroundWorker.ReportProgress(0, result.FTaskID + "  " + item + ":已有一致");
                    }
                }
            }
        }
    }
}
