﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using AFTNet.Cofing;
using DataModels;
using System.Xml.Linq;
using System.Xml.Serialization;
using AFTNet.Model;

namespace AFTNet
{
    public partial class FormMain : Form
    {
        string _s启动方式;
        enum监控方式 en启动方式;
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            _s启动方式 = WEISHENG.COMM.configHelper.getKeyValue(@"AFTNet.exe.config", "启动方式", enum监控方式.监控文件夹.ToString());
            en启动方式 = (enum监控方式)Enum.Parse(typeof(enum监控方式), _s启动方式);
            switch (en启动方式)
            {
                case enum监控方式.监控文件夹:
                    string AccessPath = WEISHENG.COMM.configHelper.getKeyValue(@"AFTNet.exe.config", "监控文件夹位置", "");
                    if (System.IO.Directory.Exists(AccessPath))
                    {
                        this.textBoxFilePath.Text = AccessPath;
                    }
                    break;
                case enum监控方式.监控sqllite数据库文件:
                    //string AccessPath = reader.ReadLine();
                    //if (File.Exists(AccessPath))
                    //{
                    //    this.textBoxFilePath.Text = AccessPath;
                    //    Config.DBFilePath = AccessPath;
                    //}
                    break;
                default:
                    break;
            }
            this.Text = Config.DeviceName + "解码器";

        }


        private void btnOpenfile_Click(object sender, EventArgs e)
        {
            switch (en启动方式)
            {
                case enum监控方式.监控文件夹:
                    FolderBrowserDialog dialog = new FolderBrowserDialog();
                    dialog.Description = "请选择文件路径";
                    dialog.SelectedPath = WEISHENG.COMM.configHelper.getKeyValue(@"AFTNet.exe.config", "监控文件夹位置", "");


                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        string foldPath = dialog.SelectedPath;
                        WEISHENG.COMM.configHelper.setKeyValue(@"AFTNet.exe.config", "监控文件夹位置", foldPath);
                        textBoxFilePath.Text = foldPath;
                    }

                    break;
                case enum监控方式.监控sqllite数据库文件:
                    openFileDialog1.Filter = "sqllite文件|*.nflis";
                    DialogResult openfileresult = openFileDialog1.ShowDialog();
                    if (DialogResult.OK == openfileresult)
                    {
                        textBoxFilePath.Text = openFileDialog1.FileName;
                        string temp = openFileDialog1.FileName;
                        Config.DBFilePath = temp;
                        WEISHENG.COMM.configHelper.setKeyValue(@"AFTNet.exe.config", "监控sqllite数据库文件名", temp);
                    }
                    break;
                default:
                    break;
            }
            this.ActiveControl = STestNO;
        }



        private void btn退出_Click(object sender, EventArgs e)
        {
            if (backgroundWorkerDecode.IsBusy)
            {
                MessageBox.Show("解码操作正在进行中，请不要关闭窗口。");
                return;
            }
            this.Close();

            //DialogResult drExit = MessageBox.Show("您要关闭程序吗？", "关闭确认", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            //if (DialogResult.Yes == drExit)
            //{
            //    this.Close();
            //}
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            //this.Show();
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
            this.Focus();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                //notifyIconTray.Visible = true;
                //this.Hide();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorkerDecode.IsBusy)
            {
                MessageBox.Show("解码操作正在进行中，请不要关闭窗口。");
                e.Cancel = true;
                return;
            }

            DialogResult drExit = MessageBox.Show("您要关闭程序吗？", "关闭确认", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (DialogResult.Yes != drExit)
            {
                e.Cancel = true;
            }
        }

        private void AppendTextToRichTextBox(string msg)
        {
            richTextBoxMsg.AppendText(msg);
            richTextBoxMsg.ScrollToCaret();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxFilePath.Text))
            {
                btnOpenfile.Focus();
            }
            else
            {
                STestNO.Focus();
            }
        }

        private void STestNO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((this.ActiveControl == STestNO) && (e.KeyChar == (char)Keys.Enter))
            {
                this.ActiveControl = ETestNO;
                ETestNO.SelectAll();
                e.Handled = true;
            }
        }

        private void ETestNO_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((this.ActiveControl == ETestNO) && (e.KeyChar == (char)Keys.Enter))
            {
                this.ActiveControl = btn开始处理;
                e.Handled = true;
            }
        }

        private void backgroundWorkerDecode_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            AppendTextToRichTextBox("处理结束。" + DateTime.Now.ToString() + "\n\n");
            SetControlEnableToTrue();
        }

        private void SetControlEnableToFalse()
        {
            bool value = false;
            this.btnOpenfile.Enabled = value;
            this.STestNO.Enabled = value;
            this.ETestNO.Enabled = value;
            this.dateTimePicker1.Enabled = value;
            this.btn开始处理.Enabled = value;
            this.btn退出.Enabled = value;
        }

        private void SetControlEnableToTrue()
        {
            bool value = true;
            this.btnOpenfile.Enabled = value;
            this.STestNO.Enabled = value;
            this.ETestNO.Enabled = value;
            this.dateTimePicker1.Enabled = value;
            this.btn开始处理.Enabled = value;
            this.btn退出.Enabled = value;
        }

        private void 退出程序ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 显示窗口ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
            this.Focus();
        }

        private void btn开始处理_Click(object sender, EventArgs e)
        {
            switch (en启动方式)
            {
                case enum监控方式.监控文件夹:
                    int iBeginTestNoQ8 = 1;
                    int iEndTestNoQ8 = 1;
                    //DirectoryInfo Dir = new DirectoryInfo(textBoxFilePath.Text);
                    //foreach (FileInfo file in Dir.GetFiles("*.xml"))
                    //{
                    //    if (File.Exists(file.FullName))
                    //    {
                    //        //todo:判断文件名，是否在指定的日期范围

                    //        //todo:根据样本号写入lis数据库
                    SetControlEnableToFalse();
                    SampleControl samQ8 = new SampleControl(this.backgroundWorkerDecode, dateTimePicker1.Value.Date, iBeginTestNoQ8, iEndTestNoQ8);
                    this.backgroundWorkerDecode.RunWorkerAsync(samQ8);
                    this.ActiveControl = STestNO;
                    STestNO.SelectAll();
                    //    }
                    //}
                    break;
                case enum监控方式.监控sqllite数据库文件:
                    string strFilePath = Config.DBFilePath;
                    //string strDateTime = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                    int iBeginTestNo = 1;
                    int iEndTestNo = 1;
                    if (!(File.Exists(strFilePath)))
                    {
                        MessageBox.Show("请确认文件的路径是否正确。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return;
                    }
                    //if (!(Regex.IsMatch(strBeginTestNo, @"^\d*$")) || !(Regex.IsMatch(strEndTestNo, @"^\d*$")))
                    //{
                    //    MessageBox.Show("样本号必需是阿拉伯数字。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    //    return;
                    //}
                    try
                    {  //目的是去掉多余的零
                        iBeginTestNo = Convert.ToInt32(this.STestNO.Text.Trim());
                        iEndTestNo = Convert.ToInt32(this.ETestNO.Text.Trim());
                        if (iBeginTestNo > 9999 || iEndTestNo > 9999 || iBeginTestNo < 0 || iEndTestNo < 0)
                        {
                            MessageBox.Show("样本号必需是介于0--9999之间的数值。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            return;
                        }
                    }
                    catch
                    {
                        MessageBox.Show("样本号必需是介于0--9999之间的数值。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return;
                    }
                    SetControlEnableToFalse();
                    SampleControl sam = new SampleControl(this.backgroundWorkerDecode, dateTimePicker1.Value.Date, iBeginTestNo, iEndTestNo);
                    this.backgroundWorkerDecode.RunWorkerAsync(sam);
                    this.ActiveControl = STestNO;
                    STestNO.SelectAll();
                    break;
                default:
                    break;
            }

        }

        private void backgroundWorkerDecode_DoWork(object sender, DoWorkEventArgs e)
        {
            this.backgroundWorkerDecode.ReportProgress(0, "开始处理：" + DateTime.Now.ToString());
            switch (en启动方式)
            {
                case enum监控方式.监控文件夹:
                    SampleControl sam_q8 = e.Argument as SampleControl;
                    Q8bll _q8 = new Q8bll(sam_q8.worker, sam_q8.date, sam_q8.strBeginNo, sam_q8.strEndNo);
                    _q8.StartDecode();
                    break;
                case enum监控方式.监控sqllite数据库文件:
                    SampleControl sam = e.Argument as SampleControl;
                    NF_T nf_t = new NF_T(sam.worker, sam.date, sam.strBeginNo, sam.strEndNo);
                    nf_t.StartDecode();
                    break;
                default:
                    break;
            }
         
  
        }

        private void backgroundWorkerDecode_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                AppendTextToRichTextBox(e.UserState.ToString() + "\n");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (dateTimePicker1.Value.Date != DateTime.Now.Date)
            {
                dateTimePicker1.Value = DateTime.Now.Date;
            }
            //btn开始处理_Click(null, null);
            btn开始处理.PerformClick();
        }

        private void button1_Click(object sender, EventArgs e)
        {


        }
        public static T DESerializerStringToEntity<T>(string strXML) where T : class
        {
            try
            {
                using (StringReader sr = new StringReader(strXML))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    return serializer.Deserialize(sr) as T;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// XML转换成实体对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T XmlToEntity<T>(string xml)
        {
            var type = typeof(T);
            //创建实例
            var t = Activator.CreateInstance<T>();
            var pr = type.GetProperties();
            var xdoc = System.Xml.Linq.XElement.Parse(xml);
            var eles = xdoc.Elements();
            var ele = eles.Where(e => new Regex(@"_\d{1,}$").IsMatch(e.Name.ToString()));//获取带下标的节点
            if (ele.Count() > 0)
            {
                var selele = ele.Select(e =>
                {
                    var temp = e.Name.ToString().Split('_');
                    var index = int.Parse(temp[temp.Length - 1]);
                    return new
                    {
                        Index = index,
                        Property = e.Name.ToString().
                    Replace("_" + index.ToString(), ""),
                        Value = e.Value
                    };
                });//转换为方便操作的匿名对象。
                var max = selele.Max(m => m.Index);//获取最大索引的值
                var infos = pr.FirstOrDefault(f => f.PropertyType.IsGenericType);
                //获取类型为泛型的属性
                if (infos != null)
                {
                    var infotype = infos.PropertyType.GetGenericArguments().First();                            //获取泛型的真实类型
                    Type listType = typeof(List<>).MakeGenericType(new[] { infotype });                         //创建泛型列表
                    var datalist = Activator.CreateInstance(listType);//创建对象
                    var infoprs = infotype.GetProperties();
                    for (int j = 0; j <= max; j++)
                    {
                        var temp = Activator.CreateInstance(infotype);
                        var list = selele.Where(s => s.Index == 0);
                        foreach (var v in list)
                        {
                            var p = infoprs.FirstOrDefault(f => f.Name == v.Property);
                            if (p == null) continue;
                            p.SetValue(temp, v.Value, null);
                        }
                        listType.GetMethod("Add").Invoke((object)datalist, new[]
                         { temp });//将对象添加到列表中
                    }
                    infos.SetValue(t, datalist, null);//最后给泛型属性赋值
                }
                ele.Remove();//将有下标的节点从集合中移除
            }
            foreach (var element in eles)
            {

                try
                {
                    var p = pr.First(f => f.Name.ToLower() == element.Name.ToString().ToLower());
                    if (p != null)
                    {
                        p.SetValue(t, Convert.ChangeType(element.Value, p.PropertyType), null);
                    }

                }
                catch (Exception ex)
                {
                    string errorMsg = "错误：";
                    if (ex.InnerException == null)
                        errorMsg += ex.Message + "，";
                    else if (ex.InnerException.InnerException == null)
                        errorMsg += ex.InnerException.Message + "，";
                    else if (ex.InnerException.InnerException.InnerException == null)
                        errorMsg += ex.InnerException.InnerException.Message;
                    WEISHENG.COMM.msgHelper.ShowInformation(errorMsg);
                }

            }
            return t;
        }
    }

    public class SampleControl
    {
        public DateTime date;
        public int strBeginNo;
        public int strEndNo;
        public BackgroundWorker worker;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="work"></param>
        /// <param name="dt">样本时间</param>
        /// <param name="begin">开始样本号</param>
        /// <param name="end">截至样本号</param>
        public SampleControl(BackgroundWorker work, DateTime dt, int begin, int end)
        {
            worker = work;
            date = dt;
            strBeginNo = begin;
            strEndNo = end;
        }
    }
}
