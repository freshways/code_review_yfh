﻿using AFTNet.Cofing;
using AFTNet.Database;
using AFTNet.Poll;
using System;
using System.Data;
using System.Data.OleDb;
using System.Linq;

namespace AFTNet
{
    public class D320 : DatePollRuntime
    {
        private string m_D320BeginNo = "201603270001";
        private string m_D320EndNo = "201603270001";
        public D320(System.ComponentModel.BackgroundWorker worker, DateTime date, int beginNo, int endNo):base(worker)
        {
            DeviceName = Config.DeviceName;
            SampleDate = date;
            m_D320BeginNo = date.ToString("yyyyMMdd") + string.Format("{0:d4}", beginNo);
            m_D320EndNo = date.ToString("yyyyMMdd") + string.Format("{0:d4}", endNo);
        }
        
        protected override void DataPollProcess()
        {
            string strDateyyyy_MM_dd = SampleDate.ToString("yyyy-MM-dd");
            string strDateyyyyMMdd = SampleDate.ToString("yyyyMMdd");
            DataTable dt = new DataTable();

            string strsql = @"select * from 
                            (
                            select SAMPLE_ID,ITEM,RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_CAL_RESULT where SAMPLE_ID>= '" + m_D320BeginNo + "' and SAMPLE_ID <='" + m_D320EndNo + @"' 
                            union all
                            select SAMPLE_ID,ITEM,RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_INPUT_RESULT where SAMPLE_ID>= '" + m_D320BeginNo + "' and SAMPLE_ID <='" + m_D320EndNo + @"' 
                            union all
                            select SAMPLE_ID,ITEM,RESULT_D as RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_PRINT_RESULT where SAMPLE_ID>= '" + m_D320BeginNo + "' and SAMPLE_ID <='" + m_D320EndNo + @"' AND DATA_TYPE=1
                            union all
                            select SAMPLE_ID,ITEM,RESULT_S as RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_PRINT_RESULT where SAMPLE_ID>= '" + m_D320BeginNo + "' and SAMPLE_ID <='" + m_D320EndNo + @"' AND DATA_TYPE=0
                            union all
                            select SAMPLE_ID,ITEM,RESULT,[TIME] from SAMPLE_ITEM_TEST_RESULT where SAMPLE_ID>= '" + m_D320BeginNo + "' and SAMPLE_ID <='" + m_D320EndNo + @"' 
                            ) as allData
                            order by SAMPLE_ID,ITEM ASC";

            #region old code 注视掉的代码
            //            string strsql = @"select * from 
            //(
            //select SAMPLE_ID,ITEM,RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_CAL_RESULT where Mid(SAMPLE_ID,1,8) = '" + strDateyyyyMMdd + @"'
            //union all
            //select SAMPLE_ID,ITEM,RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_INPUT_RESULT where Mid(SAMPLE_ID,1,8) = '" + strDateyyyyMMdd + @"'
            //union all
            //select SAMPLE_ID,ITEM,RESULT_D as RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_PRINT_RESULT where Mid(SAMPLE_ID,1,8) = '" + strDateyyyyMMdd + @"' AND DATA_TYPE=1
            //union all
            //select SAMPLE_ID,ITEM,RESULT_S as RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_PRINT_RESULT where Mid(SAMPLE_ID,1,8) = '" + strDateyyyyMMdd + @"' AND DATA_TYPE=0
            //union all
            //select SAMPLE_ID,ITEM,RESULT,[TIME] from SAMPLE_ITEM_TEST_RESULT where Mid(SAMPLE_ID,1,8) = '" + strDateyyyyMMdd + @"'
            //) as allData
            //order by SAMPLE_ID,ITEM ASC";

            //            string strsql = @"select * from 
            //(
            //select SAMPLE_ID,ITEM,RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_CAL_RESULT where SAMPLE_ID like '" + strDateyyyyMMdd + @"*' 
            //union all
            //select SAMPLE_ID,ITEM,RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_INPUT_RESULT where SAMPLE_ID like '" + strDateyyyyMMdd + @"*' 
            //union all
            //select SAMPLE_ID,ITEM,RESULT_D as RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_PRINT_RESULT where SAMPLE_ID like '" + strDateyyyyMMdd + @"*'  AND DATA_TYPE=1
            //union all
            //select SAMPLE_ID,ITEM,RESULT_S as RESULT,'" + strDateyyyy_MM_dd + "' as [TIME]  from SAMPLE_ITEM_PRINT_RESULT where SAMPLE_ID like '" + strDateyyyyMMdd + @"*'  AND DATA_TYPE=0
            //union all
            //select SAMPLE_ID,ITEM,RESULT,[TIME] from SAMPLE_ITEM_TEST_RESULT where SAMPLE_ID like '" + strDateyyyyMMdd + @"*' 
            //) as allData
            //order by SAMPLE_ID,ITEM ASC";
            #endregion
            //backgroundWorker.ReportProgress(0, Cofing.Config.DeviceDB);

            using (OleDbConnection oleConnection = new OleDbConnection(Cofing.Config.DeviceDB))
            {
                OleDbCommand oleCommand = new OleDbCommand(strsql, oleConnection);
                try
                {
                    oleConnection.Open();
                    OleDbDataAdapter oleDbAdapter = new OleDbDataAdapter(oleCommand);
                    oleDbAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                backgroundWorker.ReportProgress(0, "总项目数：" + dt.Rows.Count.ToString()+" "+ DateTime.Now.ToString());
            }
            //backgroundWorker.ReportProgress(0, DateTime.Now.ToString() + "access end");
            if (dt.Rows.Count > 0)
            {
                LisDatabase db = AftDataContext.GetLisDatabase();
                DateTime createTime = DateTime.Now;
                #region
                foreach (DataRow row in dt.Rows)
                {
                    DateTime dateTimeOfTheObservation = DateTime.Parse(row["TIME"].ToString());
                    DateTime dateTime = dateTimeOfTheObservation.Date;
                    string fillerOrderNumber = row["SAMPLE_ID"].ToString();
                    string SAMPLE_ID = fillerOrderNumber;
                    if(fillerOrderNumber.Length == 12)
                    {
                        fillerOrderNumber = fillerOrderNumber.Substring(8);
                        try
                        {
                            fillerOrderNumber = Convert.ToInt32(fillerOrderNumber).ToString();
                        }
                        catch
                        {

                        }
                    }

                    Lis_Ins_Result result = (from g in LisResults where g.FDateTime == dateTime && g.FResultID == fillerOrderNumber select g).FirstOrDefault();
                    if (result == null)
                    {
                        result = new Lis_Ins_Result();
                        result.FTaskID = Guid.NewGuid().ToString().Replace("-", "");
                        result.FInstrID = Cofing.Config.DeviceName;
                        result.FResultID = fillerOrderNumber;
                        result.FDateTime = dateTime;
                        result.FCreateTime = createTime;
                        db.Lis_Ins_Result.InsertOnSubmit(result);
                        LisResults.Add(result);

                        //
                        backgroundWorker.ReportProgress(0, "\n"+SAMPLE_ID);
                    }


                    string item = row["ITEM"].ToString();
                    string value = row["RESULT"].ToString();
                    Lis_Ins_Result_Detail detail = (from g in LisDetails where g.FItem == item && g.FTaskID == result.FTaskID select g).FirstOrDefault();
                    if (detail == null)
                    {
                        detail = new Lis_Ins_Result_Detail();
                        detail.FID = Guid.NewGuid().ToString().Replace("-", "");
                        detail.FTaskID = result.FTaskID;
                        detail.FItem = item;//obx.ObservationIdentifier.Substring(obx.ObservationIdentifier.IndexOf(msh.ComponentSeparator) + 1);
                        detail.FValue = value;
                        detail.FTime = dateTimeOfTheObservation;
                        detail.FCreateTime = createTime;
                        db.Lis_Ins_Result_Detail.InsertOnSubmit(detail);
                        LisDetails.Add(detail);

                        backgroundWorker.ReportProgress(0, SAMPLE_ID+"  "+item + ":" + value);
                    }
                    else
                    {
                        if (detail.FValue != value)
                        {
                            Lis_Ins_Result_Detail newDetail = (from g in db.Lis_Ins_Result_Detail where g.FItem == item && g.FTaskID == result.FTaskID select g).FirstOrDefault();
                            if (detail.FValue != value)
                            {
                                detail.FValue = newDetail.FValue = value;

                                backgroundWorker.ReportProgress(0, SAMPLE_ID + "  " + item + ":" + value +"(旧值："+detail.FValue+")");
                            }
                            else
                            {
                                backgroundWorker.ReportProgress(0, SAMPLE_ID + "  " + item + ":已有一致");
                            }
                        }
                        else
                        {
                            backgroundWorker.ReportProgress(0, SAMPLE_ID + "  " + item + ":已有一致");
                        }
                    }
                }
                #endregion
                db.SubmitChanges();

            }
            else
            {
                backgroundWorker.ReportProgress(0, "未找到样本数据");
            }
        }
    }
}
