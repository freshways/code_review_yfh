﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFTNet.Model
{

    /// <summary>
    /// class: Q8
    /// author: toolscat.com
    /// verison: Q8 v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Q8Model
    {
        /// <summary>
        /// Export
        /// </summary>
        //public Export Export { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ExportId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string When { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AppPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ReportTopImage { get; set; }

        /// <summary>
        /// Cassette
        /// </summary>
        public Cassette Cassette { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NumberCassettes { get; set; }

    }




    /// <summary>
    /// class: PatientName
    /// author: toolscat.com
    /// verison: PatientName v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class PatientName
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }


    /// <summary>
    /// class: Sexuality
    /// author: toolscat.com
    /// verison: Sexuality v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Sexuality
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }

    }


    /// <summary>
    /// class: Age
    /// author: toolscat.com
    /// verison: Age v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Age
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }


    /// <summary>
    /// class: AdmissionNumber
    /// author: toolscat.com
    /// verison: AdmissionNumber v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class AdmissionNumber
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }


    /// <summary>
    /// class: SampleType
    /// author: toolscat.com
    /// verison: SampleType v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class SampleType
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }

    /// <summary>
    /// class: PatientType
    /// author: toolscat.com
    /// verison: PatientType v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class PatientType
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }


    /// <summary>
    /// class: Department
    /// author: toolscat.com
    /// verison: Department v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Department
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }


    /// <summary>
    /// class: BedNumber
    /// author: toolscat.com
    /// verison: BedNumber v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class BedNumber
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }



    /// <summary>
    /// class: Inspection
    /// author: toolscat.com
    /// verison: Inspection v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Inspection
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }

    /// <summary>
    /// class: Auditor
    /// author: toolscat.com
    /// verison: Auditor v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Auditor
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

    }


    /// <summary>
    /// class: UserData
    /// author: toolscat.com
    /// verison: UserData v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class UserData
    {
        /// <summary>
        /// PatientName
        /// </summary>
        public PatientName PatientName { get; set; }

        /// <summary>
        /// Sexuality
        /// </summary>
        public Sexuality Sexuality { get; set; }

        /// <summary>
        /// Age
        /// </summary>
        public Age Age { get; set; }

        /// <summary>
        /// AdmissionNumber
        /// </summary>
        public AdmissionNumber AdmissionNumber { get; set; }

        /// <summary>
        /// SampleType
        /// </summary>
        public SampleType SampleType { get; set; }

        /// <summary>
        /// PatientType
        /// </summary>
        public PatientType PatientType { get; set; }

        /// <summary>
        /// Department
        /// </summary>
        public Department Department { get; set; }

        /// <summary>
        /// BedNumber
        /// </summary>
        public BedNumber BedNumber { get; set; }

        /// <summary>
        /// Inspection
        /// </summary>
        public Inspection Inspection { get; set; }

        /// <summary>
        /// Auditor
        /// </summary>
        public Auditor Auditor { get; set; }

    }



    /// <summary>
    /// class: Result
    /// author: toolscat.com
    /// verison: Result v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Result
    {
        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Property { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Test { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Control { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Result_Text { get; set; }

    }

    /// <summary>
    /// class: Analyte
    /// author: toolscat.com
    /// verison: Analyte v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Analyte
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Result
        /// </summary>
        public Result Result { get; set; }

        /// <summary>
        /// EOK:正常
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Reference { get; set; }

    }


    /// <summary>
    /// class: Cassette
    /// author: toolscat.com
    /// verison: Cassette v 0.1 2021-06-08 07:39:38
    /// </summary>
    public class Cassette
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// EOK:正常
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SampleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LotNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NumberAnalytes { get; set; }

        /// <summary>
        /// UserData
        /// </summary>
        public UserData UserData { get; set; }

        /// <summary>
        /// Analyte
        /// </summary>
        public Analyte Analyte { get; set; }

    }









}
