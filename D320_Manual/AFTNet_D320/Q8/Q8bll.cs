﻿using AFTNet.Cofing;
using AFTNet.Database;
using AFTNet.Model;
using AFTNet.Poll;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using WEISHENG.COMM.Helper;

namespace AFTNet
{
    public class Q8bll : DatePollRuntime
    {
        private string m_D320BeginNo = "201603270001";
        private string m_D320EndNo = "201603270001";
        public Q8bll(System.ComponentModel.BackgroundWorker worker, DateTime date, int beginNo, int endNo) : base(worker)
        {
            DeviceName = Config.DeviceName;
            SampleDate = date;
            m_D320BeginNo = date.ToString("yyyyMMdd") + string.Format("{0:d4}", beginNo);
            m_D320EndNo = date.ToString("yyyyMMdd") + string.Format("{0:d4}", endNo);
        }

        protected override void DataPollProcess()
        {
            string strDateyyyy_MM_dd = SampleDate.ToString("yyyy-MM-dd");
            string strDateyyyyMMdd = SampleDate.ToString("yyyyMMdd");
            string strDateyyyyMMdd2 = SampleDate.ToString("yyyy-MM-dd").Replace('-', '/');

            string AccessPath = WEISHENG.COMM.configHelper.getKeyValue(@"AFTNet.exe.config", "监控文件夹位置", "");
            if (!System.IO.Directory.Exists(AccessPath))
            {
                WEISHENG.COMM.msgHelper.ShowInformation("获取监控文件夹位置异常");
                return;
            }

            DirectoryInfo Dir = new DirectoryInfo(AccessPath);
            var files = Dir.GetFiles("*.xml").Where(c => c.FullName.Contains(strDateyyyy_MM_dd)).ToList();
            foreach (FileInfo file in files)
            {
                if (File.Exists(file.FullName))
                {
                    //AppendTextToRichTextBox(file.FullName);
                    XDocument document = XDocument.Load(file.FullName);
                    //1、把数据读入到dataset
                    var ds = WEISHENG.COMM.Helper.XMLHelper.ConvertXmlToDataTable(document.ToString());
                    //2、把数据转换到实体类的List
                    var _Result = Datatable2ModelHelper<Result>.FillModel(ds.Tables["Result"]).FirstOrDefault();
                    var _Analyte = Datatable2ModelHelper<Analyte>.FillModel(ds.Tables["Analyte"]).FirstOrDefault();
                    var _Cassette = Datatable2ModelHelper<Cassette>.FillModel(ds.Tables["Cassette"]).FirstOrDefault();
                    var _Q8Model = Datatable2ModelHelper<Q8Model>.FillModel(ds.Tables["Export"]).FirstOrDefault();
                    
                    backgroundWorker.ReportProgress(0, $"{file.FullName}__{_Cassette.SampleId}__{_Cassette.Name}");


                    //backgroundWorker.ReportProgress(0, "总项目数：" + lis_record.Count.ToString());
                    //backgroundWorker.ReportProgress(0, Cofing.Config.DeviceDB);
                    //backgroundWorker.ReportProgress(0, DateTime.Now.ToString() + "access end");
                    //if (lis_record.Count > 0)
                    //{
                    //    backgroundWorker.ReportProgress(0, "未找到样本数据");
                    //    return;
                    //}

                    LisDatabase db = AftDataContext.GetLisDatabase();
                    DateTime createTime = DateTime.Now;


                    DateTime dateTimeOfTheObservation = DateTime.Parse(_Q8Model.When.Replace(@"/", " "));
                    DateTime dateTime = dateTimeOfTheObservation.Date;
                    string fillerOrderNumber = $"{file.Name.Replace("-", "").Replace("_", "").Replace(".XML", "") }"; 
                    string SAMPLE_ID = fillerOrderNumber;
                    Lis_Ins_Result result = LisResults.Where(c => c.FDateTime == dateTime && c.FTaskID == fillerOrderNumber).FirstOrDefault();
                    //1、Lis_Ins_Result
                    if (result == null)
                    {
                        result = new Lis_Ins_Result();
                        result.FTaskID = fillerOrderNumber;
                        result.FInstrID = Cofing.Config.DeviceName;
                        result.FResultID = Convert.ToInt16(_Cassette.SampleId).ToString();//样本号
                        result.FDateTime = dateTime;
                        result.FCreateTime = createTime;
                        result.FTransFlag = false;
                        db.Lis_Ins_Result.InsertOnSubmit(result);
                        LisResults.Add(result);
                        backgroundWorker.ReportProgress(0, "\n" + SAMPLE_ID);
                    }
                    //2、Lis_Ins_Result_Detail
                    addDetail(_Cassette.Name, _Result.Result_Text , result, dateTimeOfTheObservation, createTime, db);   

                    //3、Lis_Ins_Result_Detail
                    //Lis_Ins_Result_Img img = db.Lis_Ins_Result_Img.Where(c => c.FTaskID == result.FTaskID).FirstOrDefault();
                    //if (img == null)
                    //{
                    //    img = new Lis_Ins_Result_Img();
                    //    img.FImgID = Guid.NewGuid().ToString().Replace("-", "");
                    //    img.FTaskID = result.FTaskID;
                    //    if (File.Exists(row.CurvePath))
                    //    {
                    //        img.FImg = WEISHENG.COMM.Helper.ImageHelper.ImageToBytes(WEISHENG.COMM.Helper.ImageHelper.GetImaegFromFile(row.CurvePath));
                    //    }
                    //    db.Lis_Ins_Result_Img.InsertOnSubmit(img);
                    //}
                    db.SubmitChanges();
                }
            }






            void addDetail(string item, string value, Lis_Ins_Result result, DateTime dateTimeOfTheObservation, DateTime createTime, LisDatabase db)
            {
                Lis_Ins_Result_Detail detail = (from g in LisDetails where g.FItem == item && g.FTaskID == result.FTaskID select g).FirstOrDefault();
                if (detail == null)
                {
                    detail = new Lis_Ins_Result_Detail();
                    detail.FID = Guid.NewGuid().ToString().Replace("-", "");
                    detail.FTaskID = result.FTaskID;
                    detail.FItem = item;
                    detail.FValue = value;
                    detail.FTime = dateTimeOfTheObservation;
                    detail.FCreateTime = createTime;
                    db.Lis_Ins_Result_Detail.InsertOnSubmit(detail);
                    LisDetails.Add(detail);
                    backgroundWorker.ReportProgress(0, result.FTaskID + "  " + item + ":" + value);
                }
                else
                {
                    if (detail.FValue != value)
                    {
                        Lis_Ins_Result_Detail newDetail = (from g in db.Lis_Ins_Result_Detail where g.FItem == item && g.FTaskID == result.FTaskID select g).FirstOrDefault();
                        if (detail.FValue != value)
                        {
                            detail.FValue = newDetail.FValue = value;

                            backgroundWorker.ReportProgress(0, result.FTaskID + "  " + item + ":" + value + "(旧值：" + detail.FValue + ")");
                        }
                        else
                        {
                            backgroundWorker.ReportProgress(0, result.FTaskID + "  " + item + ":已有一致");
                        }
                    }
                    else
                    {
                        backgroundWorker.ReportProgress(0, result.FTaskID + "  " + item + ":已有一致");
                    }
                }
            }
        }
    }
}
